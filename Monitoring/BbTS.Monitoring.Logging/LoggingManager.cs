﻿using System;
using System.Diagnostics;
using System.Security;
using System.ServiceModel;
using BbTS.Core.Configuration;
using BbTS.Core.Serialization;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.Exceptions.Transaction;
using BbTS.Domain.Models.Exceptions.WebApi;
using BbTS.Domain.Models.System.Logging;
using BbTS.Domain.Models.Transaction.Processing;
using Devart.Data.Oracle;
using Newtonsoft.Json;

namespace BbTS.Monitoring.Logging
{
    public class LoggingManager
    {
        private static LoggingManager _instance;

        private int _loggingLevel = -1;

        public int LoggingLevel
        {
            get
            {
                try
                {
                    _loggingLevel = _loggingLevel < 0 ?
                        ApplicationConfiguration.GetKeyValueAsInt("ExceptionLevelLogging", 1) :
                        _loggingLevel;
                }
                catch (Exception)
                {
                    _loggingLevel = 1;
                }
                return _loggingLevel;
            }
        }

        private bool? _timingEnabled;
        public bool TimingEnabled
        {
            get
            {
                if (_timingEnabled == null)
                {
                    _timingEnabled = ApplicationConfiguration.GetKeyValueAsBoolean("TimingEnabled");
                }
                return _timingEnabled.Value;
            }
        }

        /// <summary>
        /// The the proper exception code
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public int CodeGet(int code)
        {
            return !Enum.IsDefined(typeof(LoggingDefinitions.EventId), code) ? -999999 : code;
        }

        /// <summary>
        /// Get the message associated with the code.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <returns>The message</returns>
        public string CodeStringGet(int code)
        {
            return !Enum.IsDefined(typeof(LoggingDefinitions.EventId), code) ? "Unrecognized exception code" : ((LoggingDefinitions.EventId)code).ToString();
        }

        /// <summary>
        /// Private Constructor
        /// </summary>
        private LoggingManager()
        {
        }

        /// <summary>
        /// Get the log source
        /// </summary>
        public static string LogSource => ApplicationConfiguration.GetKeyValueAsString("LogSourceName", "Blackboard");

        /// <summary>
        /// Singleton Instance
        /// </summary>
        public static LoggingManager Instance 
        {
            get { return _instance ?? (_instance = new LoggingManager()); }
            internal set { _instance = value; }
        }

        /// <summary>
        /// Logs the specified sev.
        /// </summary>
        /// <param name="sev">The sev.</param>
        /// <param name="msg">The MSG.</param>
        /// <param name="category">Category to log under (10 'General' if no value is provided)</param>
        /// <param name="eventId">Event Id to log under (1 'General' if no value is provided)</param>
        public static void Log(
            EventLogEntryType sev,
            string msg,
            short category = (short)LoggingDefinitions.Category.General,
            int eventId = (int)LoggingDefinitions.EventId.General)
        {
            int maxMessageSize = ApplicationConfiguration.GetKeyValueAsInt("MaxLogEntryMessageSize", 10240);

            if (msg.Length > maxMessageSize)
            {
                msg = msg.Substring(0, maxMessageSize);
                msg += "...";
            }

            string source = ApplicationConfiguration.GetKeyValueAsString("LogSourceName", "Blackboard");
            const string logName = "Application";

            if (!EventLog.SourceExists(source))
            {
                EventLog.CreateEventSource(source, logName);
            }

            EventLog.WriteEntry(source, msg, sev, eventId, category);
        }

        /// <summary>
        /// Log a message
        /// </summary>
        /// <param name="message"></param>
        /// <param name="category">Category to log under (10 'General' if no value is provided)</param>
        /// <param name="eventId">Event Id to log under (1 'General' if no value is provided)</param>
        public void LogMessage(
            String message,
            short category = (short)LoggingDefinitions.Category.General,
            int eventId = (int)LoggingDefinitions.EventId.General)
        {
            Log(EventLogEntryType.Information, message, category, eventId);
        }

        /// <summary>
        /// Log a tracking message
        /// </summary>
        /// <param name="message">Message to log</param>
        /// <param name="severity">Severity level</param>
        /// <param name="category">Category to log under (10 'General' if no value is provided)</param>
        /// <param name="eventId">Event Id to log under (1 'General' if no value is provided)</param>
        public void LogTrackingMessage(
            String message,
            EventLogEntryType severity = EventLogEntryType.Information,
            short category = (short)LoggingDefinitions.Category.Tracking,
            int eventId = (int)LoggingDefinitions.EventId.General)
        {
            if (LoggingLevel == 0) Log(severity, message, category, eventId);
        }

        /// <summary>
        /// Log a debug message
        /// </summary>
        /// <param name="message">Message to log</param>
        /// <param name="severity">Severity level</param>
        /// <param name="category">Category to log under (10 'General' if no value is provided)</param>
        /// <param name="eventId">Event Id to log under (1 'General' if no value is provided)</param>
        public void LogDebugMessage(
            String message,
            short category = (short)LoggingDefinitions.Category.General,
            int eventId = (int)LoggingDefinitions.EventId.General,
            EventLogEntryType severity = EventLogEntryType.Information)
        {
            if(LoggingLevel == 0) Log(severity, message, category, eventId);
        }

        /// <summary>
        /// Log a debug message
        /// </summary>
        /// <param name="message">Message to log</param>
        /// <param name="severity">Severity level</param>
        /// <param name="category">Category to log under (10 'General' if no value is provided)</param>
        /// <param name="eventId">Event Id to log under (1 'General' if no value is provided)</param>
        public void LogDebugMessage(
            String message,
            EventLogEntryType severity = EventLogEntryType.Information,
            short category = (short)LoggingDefinitions.Category.General,
            int eventId = (int)LoggingDefinitions.EventId.General)
        {
            if (LoggingLevel == 0) Log(severity, message, category, eventId);
        }

        /// <summary>
        /// Log a method timing event if timing is enabled in the application config if TimingEnabled is set to true.
        /// </summary>
        /// <param name="message">Message to log</param>
        /// <param name="severity">Severity level</param>
        /// <param name="category">Category to log under (10 'General' if no value is provided)</param>
        /// <param name="eventId">Event Id to log under (1 'General' if no value is provided)</param>
        public void LogTimingMessage(
            String message,
            EventLogEntryType severity = EventLogEntryType.Information,
            short category = (short)LoggingDefinitions.Category.General,
            int eventId = (int)LoggingDefinitions.EventId.General)
        {
            if (TimingEnabled) Log(severity, message, category, eventId);
        }

        /// <summary>
        /// Log a message bypassing the environment loads from SystemConfiguration.xml
        /// </summary>
        /// <param name="message"></param>
        /// <param name="source"></param>
        /// <param name="category">Category to log under (10 'General' if no value is provided)</param>
        /// <param name="eventId">Event Id to log under (1 'General' if no value is provided)</param>
        public void LogMessage(
            String message,
            string source,
            short category = (short)LoggingDefinitions.Category.General,
            int eventId = (int)LoggingDefinitions.EventId.General)
        {
            const int maxMessageSize = 10240;

            if (message.Length > maxMessageSize)
            {
                message = message.Substring(0, maxMessageSize);
                message += "...";
            }
            
            const string logName = "Application";

            try
            {
                if (!EventLog.SourceExists(source))
                {
                    EventLog.CreateEventSource(source, logName);
                }
            }
            catch (SecurityException)
            {
                EventLog.CreateEventSource(source, logName);
            }
            

            EventLog.WriteEntry(source, message, EventLogEntryType.Information, eventId, category);
        }

        /// <summary>
        /// Log a message bypassing the environment loads from SystemConfiguration.xml
        /// </summary>
        /// <param name="message"></param>
        /// <param name="source"></param>
        /// <param name="severity"></param>
        /// <param name="category">Category to log under (10 'General' if no value is provided)</param>
        /// <param name="eventId">Event Id to log under (1 'General' if no value is provided)</param>
        public void LogException(
            String message,
            string source,
            EventLogEntryType severity,
            short category = (short)LoggingDefinitions.Category.General,
            int eventId = (int)LoggingDefinitions.EventId.General)
        {
            const int maxMessageSize = 10240;

            if (message.Length > maxMessageSize)
            {
                message = message.Substring(0, maxMessageSize);
                message += "...";
            }

            const string logName = "Application";

            if (!EventLog.SourceExists(source))
            {
                EventLog.CreateEventSource(source, logName);
            }

            EventLog.WriteEntry(source, message, severity, eventId, category);
        }

        /// <summary>
        /// Log a message bypassing the environment loads from SystemConfiguration.xml
        /// </summary>
        /// <param name="message"></param>
        /// <param name="severity"></param>
        /// <param name="category">Category to log under (10 'General' if no value is provided)</param>
        /// <param name="eventId">Event Id to log under (1 'General' if no value is provided)</param>
        public void LogException(
            String message,
            EventLogEntryType severity,
            short category = (short)LoggingDefinitions.Category.General,
            int eventId = (int)LoggingDefinitions.EventId.General)
        {
            string source = ApplicationConfiguration.GetKeyValueAsString("LogSourceName", "Blackboard");
            LogException(message, source, severity, category, eventId);
        }

        /// <summary>
        /// Log a message bypassing the environment loads from SystemConfiguration.xml
        /// </summary>
        /// <param name="ex">Exception to log</param>
        /// <param name="severity">Severity level to log the exception under (Information, Warning, Error)</param>
        /// <param name="category">Category to log under (10 'General' if no value is provided)</param>
        /// <param name="eventId">Event Id to log under (1 'General' if no value is provided)</param>
        public void LogException(
            Exception ex,
            EventLogEntryType severity,
            short category = (short)LoggingDefinitions.Category.General,
            int eventId = (int)LoggingDefinitions.EventId.General)
        {
            LogException(ex, "", "", severity, category, eventId);
        }

        /// <summary>
        /// Format and log a BbTsWebApiException type exception.
        /// </summary>
        /// <param name="ex">Exception to log</param>
        /// <param name="severity">Severity level to log the exception under (Information, Warning, Error)</param>
        /// <param name="category">Category to log under (10 'General' if no value is provided)</param>
        /// <param name="eventId">Event Id to log under (1 'General' if no value is provided)</param>
        public void LogWebApiException(
            WebApiException ex,
            EventLogEntryType severity,
            short category = (short)LoggingDefinitions.Category.General,
            int eventId = (int)LoggingDefinitions.EventId.General)
        {
            var prepend =
                $"ReferenceId: {ex.ReferenceId}\n" +
                $"RequestId: {ex.RequestId}\n" +
                $"ErrorCode: {ex.HttpStatusCode}";
            LogException(ex, prepend, "", severity, category, eventId);
        }

        /// <summary>
        /// Format and log a BbTsWebApiException type exception.
        /// </summary>
        /// <param name="rex"><see cref="ResourceLayerException"/> to log</param>
        /// <param name="severity">Severity level to log the exception under (Information, Warning, Error)</param>
        public void LogResourceException(ResourceLayerException rex,EventLogEntryType severity)
        {
            var prepend =
                $"ReferenceId: {rex.ReferenceId}\n" +
                $"RequestId: {rex.RequestId}\n\n";
            LogException(rex, prepend, "", severity, rex.EventCategory, rex.EventId);
        }

        /// <summary>
        /// Format and log a BbTsWebApiException type exception.
        /// </summary>
        /// <param name="lex"><see cref="LineItemProcessingException"/> to log</param>
        /// <param name="request">The request that spawned this exception.</param>
        /// <param name="severity">Severity level to log the exception under (Information, Warning, Error)</param>
        /// <param name="eventCategory">Event category to log under (10 'General' if no value is provided)</param>
        /// <param name="eventId">Event Id to log under (1 'General' if no value is provided)</param>
        public void LogLineItemProcessingException(
            LineItemProcessingException lex,
            IProcessingRequest request,
            EventLogEntryType severity,
            LoggingDefinitions.Category eventCategory = LoggingDefinitions.Category.General,
            LoggingDefinitions.EventId eventId = LoggingDefinitions.EventId.General)
        {
            var prepend = $"LineItemProcessingResult:\n{NewtonsoftJson.Serialize(lex.LineItemProcessingResult, TypeNameHandling.Auto)}\n" +
                         $"IProcessingRequest:\n{NewtonsoftJson.Serialize(request, TypeNameHandling.Auto)}\n\n";

            LogException(lex, prepend, "", severity, (short)eventCategory, (int)eventId);
        }

        /// <summary>
        /// Log a message bypassing the environment loads from SystemConfiguration.xml
        /// </summary>
        /// <param name="ex">Exception to log</param>
        /// <param name="source">Application source (to write in the event log)</param>
        /// <param name="prepend">Any text to prepend to the message</param>
        /// <param name="append">Any text to append to the message</param>
        /// <param name="severity">Severity level to log the exception under (Information, Warning, Error)</param>
        /// <param name="category">Category to log under (10 'General' if no value is provided)</param>
        /// <param name="eventId">Event Id to log under (1 'General' if no value is provided)</param>
        public void LogException(
            Exception ex,
            string source,
            String prepend,
            String append,
            EventLogEntryType severity,
            short category = (short)LoggingDefinitions.Category.General,
            int eventId = (int)LoggingDefinitions.EventId.General)
        {
            String faultExceptionAction = null;
            if (ex is FaultException)
            {

                FaultException fex = ex as FaultException;
                faultExceptionAction = fex.Action;
            }

            String message = String.Format(
                "{4}" +
                "An exception of type: {0} has occured. \r\n" +
                "Message: {1}\r\n" +
                "Additional Information: {2}\r\n" +
                "Stack Trace:\r\n{3}\r\n" +
                "{5}\r\n",
                ex.GetType().Name,
                ex.Message,
                ex.InnerException != null && !String.IsNullOrEmpty(ex.InnerException.Message) ? ex.InnerException.Message : "none",
                ex.StackTrace,
                String.IsNullOrEmpty(prepend) ? "" : prepend + "\r\n",
                append);

            if(!String.IsNullOrEmpty(faultExceptionAction))
            {
                message += "Fault Action: " + faultExceptionAction + "\n";
            }

            const string logName = "Application";

            if (!EventLog.SourceExists(source))
            {
                EventLog.CreateEventSource(source, logName);
            }

            EventLog.WriteEntry(source, message, severity, eventId, category);
        }

        /// <summary>
        /// Log a message bypassing the environment loads from SystemConfiguration.xml
        /// </summary>
        /// <param name="ex">Exception to log</param>
        /// <param name="prepend">Any text to prepend to the message</param>
        /// <param name="append">Any text to append to the message</param>
        /// <param name="severity">Severity level to log the exception under (Information, Warning, Error)</param>
        /// <param name="category">Category to log under (10 'General' if no value is provided)</param>
        /// <param name="eventId">Event Id to log under (1 'General' if no value is provided)</param>
        public void LogException(
            Exception ex,
            String prepend,
            String append,
            EventLogEntryType severity,
            short category = (short)LoggingDefinitions.Category.General,
            int eventId = (int)LoggingDefinitions.EventId.General)
        {
            string source = ApplicationConfiguration.GetKeyValueAsString("LogSourceName", "Blackboard");
            LogException(ex, source, prepend, append, severity, category, eventId);
        }

        /// <summary>
        /// Log a message bypassing the environment loads from SystemConfiguration.xml
        /// </summary>
        /// <param name="ex">Exception to log</param>
        /// <param name="category">Category to log under (10 'General' if no value is provided)</param>
        /// <param name="eventId">Event Id to log under (1 'General' if no value is provided)</param>
        public void LogException(
            Exception ex,
            short category = (short)LoggingDefinitions.Category.General,
            int eventId = (int)LoggingDefinitions.EventId.General)
        {
            string source = ApplicationConfiguration.GetKeyValueAsString("LogSourceName", "Blackboard");
            LogException(ex, source, "", "", EventLogEntryType.Error, category, eventId);
        }

        /// <summary>
        /// Log an exception
        /// </summary>
        /// <param name="code"></param>
        /// <param name="ex"></param>
        /// <param name="prepend"></param>
        /// <param name="append"></param>
        /// <param name="callingClass"></param>
        /// <param name="stackClass"></param>
        /// <param name="lineNumber"></param>
        /// <param name="category">Category to log under (10 'General' if no value is provided)</param>
        /// <param name="eventId">Event Id to log under (1 'General' if no value is provided)</param>
        public void LogException(
            Int32 code,
            Exception ex,
            String prepend,
            String append,
            String callingClass,
            String stackClass,
            String lineNumber,
            short category = (short)LoggingDefinitions.Category.General,
            int eventId = (int)LoggingDefinitions.EventId.General)
        {
            var definition = new ExceptionSeverityDefinition
            {
                Code = CodeGet(code),
                Message = CodeStringGet(code),
                Severity = 0
            };

            var globalSeverityLevel = ApplicationConfiguration.GetKeyValueAsInt("ExceptionLevelLogging", 1);

            if (definition.Severity > globalSeverityLevel) return;

            var additional =
                $"Exception Code: {definition.Code}\n" +
                $"Message: {definition.Message}\n" +
                $"Severity Level: {definition.Severity}\n";

            var combinedPrepend = $"{additional}\n{prepend}";

            _logException(
                ex,
                _severityToEventLogEntryType(definition.Severity),
                combinedPrepend,
                append,
                callingClass,
                stackClass,
                lineNumber);
        }

        /// <summary>
        /// Log an exception
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="prepend"></param>
        /// <param name="append"></param>
        /// <param name="callingClass"></param>
        /// <param name="stackClass"></param>
        /// <param name="lineNumber"></param>
        /// <param name="category">Category to log under (10 'General' if no value is provided)</param>
        /// <param name="eventId">Event Id to log under (1 'General' if no value is provided)</param>
        public void LogException(
            Exception ex,
            String prepend,
            String append,
            String callingClass,
            String stackClass,
            String lineNumber,
            short category = (short)LoggingDefinitions.Category.General,
            int eventId = (int)LoggingDefinitions.EventId.General)
        {
            int code = -999999;

            if (ex.Data.Contains("code"))
            {
                String codeString = ex.Data["code"] as string;
                int.TryParse(codeString, out code);
            }

            LogException( code, ex, prepend, append, callingClass, stackClass, lineNumber, category, eventId);
        }

        /// <summary>
        /// Log an Oracle exception
        /// </summary>
        /// <param name="oex"></param>
        /// <param name="prepend"></param>
        /// <param name="append"></param>
        /// <param name="callingClass"></param>
        /// <param name="stackClass"></param>
        /// <param name="lineNumber"></param>
        public void LogOracleException(
            OracleException oex,
            String prepend,
            String append,
            String callingClass,
            String stackClass,
            String lineNumber)
        {
            LogException(oex.Code, oex, prepend, append, callingClass, stackClass, lineNumber);
        }

        /// <summary>
        /// Log a debugging exception
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="prepend"></param>
        /// <param name="append"></param>
        /// <param name="callingClass"></param>
        /// <param name="stackClass"></param>
        /// <param name="lineNumber"></param>
        /// <param name="eventId">Event Id to log under (10 'General' if no value is provided)</param>
        /// <param name="category">Category to log under (1 'General' if no value is provided)</param>
        public void LogDebugException(
            Exception ex,
            String prepend,
            String append,
            String callingClass,
            String stackClass,
            String lineNumber,
            short category = (short)LoggingDefinitions.Category.General,
            int eventId = (int)LoggingDefinitions.EventId.General
            )
        {
            int code = -999999;

            if (ex.Data.Contains("code"))
            {
                var codeString = ex.Data["code"] as string;
                int.TryParse(codeString, out code);
            }

            if (LoggingLevel == 0) LogException(code, ex, prepend, append, callingClass, stackClass, lineNumber, category, eventId);
        }

        /// <summary>
        /// Format external client message
        /// </summary>
        /// <param name="code">code</param>
        /// <param name="errorMessage">message</param>
        /// <param name="severity">severity</param>
        /// <param name="function">function</param>
        /// <returns></returns>
        public string FormatExternalClientMessage(string code, string errorMessage, EventLogEntryType severity, string function)
        {
            var additional = $"Exception Code: {code}\n" + $"Severity Level: {severity}\n" + $"Message: {errorMessage}\n";
            var message = $"A processing error has occured in ExternalClient function {function}. \n\n {additional}";
            return message;
        }

        /// <summary>
        /// Log an exception with the calling class and the line number.  Additionally add any inner exception text.
        /// </summary>
        /// <param name="ex">The Exception to log</param>
        /// <param name="logType"></param>
        /// <param name="prepend">Information to prepend to the error message</param>
        /// <param name="append">Information to append to the error message</param>
        /// <param name="callingClass">Manually entered class to use if the stack traced class is empty</param>
        /// <param name="stackClass">Stack Traced class</param>
        /// <param name="lineNumber">Line number to report</param>
        /// <param name="category">Category to log under (10 'General' if no value is provided)</param>
        /// <param name="eventId">Event Id to log under (1 'General' if no value is provided)</param>
        private void _logException(
            Exception ex,
            EventLogEntryType logType,
            String prepend,
            String append,
            String callingClass,
            String stackClass,
            String lineNumber,
            short category = (short)LoggingDefinitions.Category.General,
            int eventId = (int)LoggingDefinitions.EventId.General)
        {
            String faultExceptionAction = null;
            if (ex is FaultException)
            {

                FaultException fex = ex as FaultException;
                faultExceptionAction = fex.Action;
            }

            String message =
                prepend +
                "Exception in " +
                (String.IsNullOrEmpty(stackClass) ? callingClass + "\n" : stackClass + "\n") +
                (String.IsNullOrEmpty(lineNumber) ? "" : " at line " + lineNumber + "\n") +
                "Type: " + ex.GetType().Name + "\n" +
                 (String.IsNullOrEmpty(faultExceptionAction) ? "" : "Fault Action : " + faultExceptionAction + "\n") +
                ex.Message + "\n" +
                ex.StackTrace + "\n" +
                append;

            if (ex.InnerException != null)
            {
                message += "\nAdditional Information: " + ex.InnerException.Message;
            }

            Log(logType, message, category, eventId);
        }

        /// <summary>
        /// Convert the severity code to an EventLogEntryType
        /// </summary>
        /// <param name="severity"></param>
        /// <returns></returns>
        private EventLogEntryType _severityToEventLogEntryType(Int32 severity)
        {
            switch (severity)
            {
                case 1: return EventLogEntryType.Error;
                case 2: return EventLogEntryType.Warning;
                default: return EventLogEntryType.Information; 
            }
        }
    }
}
