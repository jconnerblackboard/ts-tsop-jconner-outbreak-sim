﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using Newtonsoft.Json;
using OutbreakAnalysis.Models;
using OutbreakAnalysis.Utility;

namespace OutbreakAnalysisGui
{
    public partial class MainForm : Form
    {
        private OutbreakSimulation Simulation { get; set; } = new OutbreakSimulation();
        public MainForm()
        {
            InitializeComponent();
        }

        private void fileToolStripMenuItem_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                switch (e.ClickedItem.Text)
                {
                    case "&New":
                    case "S&ettings":
                    {
                        var form = new SettingsForm(Simulation.Parameters);
                        if (form.ShowDialog() == DialogResult.OK)
                        {
                            Simulation.Parameters = form.Parameters;
                        }
                    }
                        break;
                    case "&Import":
                    {
                        var openForm = new Form {TopMost = true};

                        // create and open the OpenFileDialog
                        var dialog = new OpenFileDialog
                        {
                            Title = @"Open Simulation",
                            Filter = @"JSON Files|*.json",
                            RestoreDirectory = true
                        };

                        if (dialog.ShowDialog(openForm) == DialogResult.OK)
                        {
                            var filename = dialog.FileName;
                            var jsonString = File.ReadAllText(filename);
                            Simulation = JsonConvert.DeserializeObject<OutbreakSimulation>(jsonString);
                        }
                    }
                        break;
                    case "&Save":
                        {
                            // create a form to place the openfiledialog into so it can
                            // be brought to the front, otherwise the menu sits in front
                            var saveForm = new Form { TopMost = true };

                            // create and open the OpenFileDialog
                            var dialog = new SaveFileDialog
                            {
                                Title = @"Select a JSON file",
                                Filter = @"JSON Files|*.json",
                                RestoreDirectory = true
                            };

                            if (dialog.ShowDialog(saveForm) == DialogResult.OK)
                            {
                                File.WriteAllText(dialog.FileName, JsonConvert.SerializeObject(Simulation, Formatting.Indented));
                            }
                        }
                        break;
                    case "E&xit":
                        Application.Exit();
                        break;
                }
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                MessageBox.Show(message, @"Error", MessageBoxButtons.OK);
            }
        }

        private void Button_Click(object sender, EventArgs e)
        {
            var buttonName = ((Button) sender).Name;
            try
            {
                switch (buttonName)
                {
                    case "NextButton":
                    {
                        TimeSliceSlider.Value = Math.Min(TimeSliceSlider.Maximum, TimeSliceSlider.Value +1);
                        _drawTimeSlice();
                    }
                        break;
                    case "PrevButton":
                    {
                        TimeSliceSlider.Value = Math.Max(0, TimeSliceSlider.Value - 1);
                        _drawTimeSlice();
                    }
                        break;
                    case "InitializeButton":
                    {
                        _initializeSimulationBoundaries();
                    }
                        break;
                    case "RunSimButton":
                    {
                        _initializeSimulationBoundaries();
                        Simulation = OutbreakAnalysisUtility.RunSimulation(Simulation.Parameters);
                        TimeSliceSlider.Minimum = 0;
                        TimeSliceSlider.Maximum = Math.Max(Simulation.TimeSlices.Count, 1);
                        TimeSliceSlider.Value = 0;
                        _drawTimeSlice();
                    }
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, @"Error", MessageBoxButtons.OK);
            }
        }

        private void _drawTimeSlice()
        {
            try
            {
                var index = TimeSliceSlider.Value;
                if (index < 0 || index >= Simulation.TimeSlices.Count)
                {
                    return;
                }

                //SimulationPanel.Image = DrawUtility.DrawTimeSlice(Simulation, index, DrawUtility.BeginOrEnd.End);

                SimulationPanel.Image = index == 0 ? 
                    DrawUtility.DrawTimeSlice(Simulation, index, DrawUtility.BeginOrEnd.Begin) : 
                    DrawUtility.DrawTimeSlice(Simulation, index - 1, DrawUtility.BeginOrEnd.End);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }
        }

        private void _initializeSimulationBoundaries()
        {
            var height = SimulationPanel.Height;
            var width = SimulationPanel.Width;
            Simulation.Parameters.LocationMap.OuterBoundary.Max = new Point(width, height);
            var quarterPoint = new Point((decimal)width / 4, (decimal)height / 4);
            Simulation.Parameters.LocationMap.InnerBoundary.Min = quarterPoint;
            Simulation.Parameters.LocationMap.InnerBoundary.Max = new Point(width - quarterPoint.X, height - quarterPoint.Y);
        }

        private void TimeSliceSlider_Scroll(object sender, EventArgs e)
        {
            _drawTimeSlice();
        }
    }
}
