﻿namespace OutbreakAnalysisGui
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.OkButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.PatientZeroIdTextBox = new System.Windows.Forms.TextBox();
            this.StartTimeDatePicker = new System.Windows.Forms.DateTimePicker();
            this.EndTimeDatePicker = new System.Windows.Forms.DateTimePicker();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.DataPathTextBox = new System.Windows.Forms.TextBox();
            this.BrowseButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.TimeSliceDaysNumeric = new System.Windows.Forms.NumericUpDown();
            this.TimeSliceHoursNumeric = new System.Windows.Forms.NumericUpDown();
            this.TimeSliceMinutesNumeric = new System.Windows.Forms.NumericUpDown();
            this.TimeSliceSecondsNumeric = new System.Windows.Forms.NumericUpDown();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.ExposureSecondsNumeric = new System.Windows.Forms.NumericUpDown();
            this.ExposureMinutesNumeric = new System.Windows.Forms.NumericUpDown();
            this.ExposureHoursNumeric = new System.Windows.Forms.NumericUpDown();
            this.ExposureDaysNumeric = new System.Windows.Forms.NumericUpDown();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TimeSliceDaysNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimeSliceHoursNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimeSliceMinutesNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimeSliceSecondsNumeric)).BeginInit();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ExposureSecondsNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExposureMinutesNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExposureHoursNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExposureDaysNumeric)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel1.Controls.Add(this.OkButton, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.PatientZeroIdTextBox, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.StartTimeDatePicker, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.EndTimeDatePicker, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 1, 6);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 8;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(790, 479);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // OkButton
            // 
            this.OkButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.OkButton.Location = new System.Drawing.Point(436, 453);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(75, 23);
            this.OkButton.TabIndex = 0;
            this.OkButton.Text = "Ok";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(84, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Data Path";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(54, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Patient Zero Id";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(82, 181);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Start Time";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(87, 249);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "End Time";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(82, 317);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 17);
            this.label5.TabIndex = 5;
            this.label5.Text = "Time Slice";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 385);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(135, 17);
            this.label6.TabIndex = 6;
            this.label6.Text = "Exposure Threshold";
            // 
            // PatientZeroIdTextBox
            // 
            this.PatientZeroIdTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.PatientZeroIdTextBox.Location = new System.Drawing.Point(161, 111);
            this.PatientZeroIdTextBox.Name = "PatientZeroIdTextBox";
            this.PatientZeroIdTextBox.Size = new System.Drawing.Size(626, 22);
            this.PatientZeroIdTextBox.TabIndex = 8;
            // 
            // StartTimeDatePicker
            // 
            this.StartTimeDatePicker.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.StartTimeDatePicker.Location = new System.Drawing.Point(161, 179);
            this.StartTimeDatePicker.Name = "StartTimeDatePicker";
            this.StartTimeDatePicker.Size = new System.Drawing.Size(200, 22);
            this.StartTimeDatePicker.TabIndex = 9;
            // 
            // EndTimeDatePicker
            // 
            this.EndTimeDatePicker.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.EndTimeDatePicker.Location = new System.Drawing.Point(161, 247);
            this.EndTimeDatePicker.Name = "EndTimeDatePicker";
            this.EndTimeDatePicker.Size = new System.Drawing.Size(200, 22);
            this.EndTimeDatePicker.TabIndex = 10;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 67F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel2.Controls.Add(this.DataPathTextBox, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.BrowseButton, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(161, 23);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(626, 62);
            this.tableLayoutPanel2.TabIndex = 11;
            // 
            // DataPathTextBox
            // 
            this.DataPathTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.DataPathTextBox.Location = new System.Drawing.Point(3, 20);
            this.DataPathTextBox.Name = "DataPathTextBox";
            this.DataPathTextBox.Size = new System.Drawing.Size(413, 22);
            this.DataPathTextBox.TabIndex = 8;
            // 
            // BrowseButton
            // 
            this.BrowseButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.BrowseButton.Location = new System.Drawing.Point(422, 19);
            this.BrowseButton.Name = "BrowseButton";
            this.BrowseButton.Size = new System.Drawing.Size(75, 23);
            this.BrowseButton.TabIndex = 9;
            this.BrowseButton.Text = "Browse";
            this.BrowseButton.UseVisualStyleBackColor = true;
            this.BrowseButton.Click += new System.EventHandler(this.BrowseButton_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label8, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label9, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.label10, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.TimeSliceDaysNumeric, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.TimeSliceHoursNumeric, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.TimeSliceMinutesNumeric, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.TimeSliceSecondsNumeric, 3, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(161, 295);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(626, 62);
            this.tableLayoutPanel3.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(58, 14);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 17);
            this.label7.TabIndex = 0;
            this.label7.Text = "Days";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(211, 14);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 17);
            this.label8.TabIndex = 1;
            this.label8.Text = "Hours";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(361, 14);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 17);
            this.label9.TabIndex = 2;
            this.label9.Text = "Minutes";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(515, 14);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 17);
            this.label10.TabIndex = 3;
            this.label10.Text = "Seconds";
            // 
            // TimeSliceDaysNumeric
            // 
            this.TimeSliceDaysNumeric.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TimeSliceDaysNumeric.Location = new System.Drawing.Point(3, 34);
            this.TimeSliceDaysNumeric.Name = "TimeSliceDaysNumeric";
            this.TimeSliceDaysNumeric.Size = new System.Drawing.Size(150, 22);
            this.TimeSliceDaysNumeric.TabIndex = 4;
            // 
            // TimeSliceHoursNumeric
            // 
            this.TimeSliceHoursNumeric.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TimeSliceHoursNumeric.Location = new System.Drawing.Point(159, 34);
            this.TimeSliceHoursNumeric.Name = "TimeSliceHoursNumeric";
            this.TimeSliceHoursNumeric.Size = new System.Drawing.Size(150, 22);
            this.TimeSliceHoursNumeric.TabIndex = 5;
            // 
            // TimeSliceMinutesNumeric
            // 
            this.TimeSliceMinutesNumeric.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TimeSliceMinutesNumeric.Location = new System.Drawing.Point(315, 34);
            this.TimeSliceMinutesNumeric.Name = "TimeSliceMinutesNumeric";
            this.TimeSliceMinutesNumeric.Size = new System.Drawing.Size(150, 22);
            this.TimeSliceMinutesNumeric.TabIndex = 6;
            // 
            // TimeSliceSecondsNumeric
            // 
            this.TimeSliceSecondsNumeric.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TimeSliceSecondsNumeric.Location = new System.Drawing.Point(471, 34);
            this.TimeSliceSecondsNumeric.Name = "TimeSliceSecondsNumeric";
            this.TimeSliceSecondsNumeric.Size = new System.Drawing.Size(152, 22);
            this.TimeSliceSecondsNumeric.TabIndex = 7;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 4;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.Controls.Add(this.ExposureDaysNumeric, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.ExposureHoursNumeric, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.ExposureMinutesNumeric, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.ExposureSecondsNumeric, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.label11, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.label12, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.label13, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.label14, 3, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(161, 363);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(626, 62);
            this.tableLayoutPanel4.TabIndex = 13;
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(58, 14);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 17);
            this.label11.TabIndex = 1;
            this.label11.Text = "Days";
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(211, 14);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(46, 17);
            this.label12.TabIndex = 2;
            this.label12.Text = "Hours";
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(361, 14);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(57, 17);
            this.label13.TabIndex = 3;
            this.label13.Text = "Minutes";
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(515, 14);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(63, 17);
            this.label14.TabIndex = 4;
            this.label14.Text = "Seconds";
            // 
            // ExposureSecondsNumeric
            // 
            this.ExposureSecondsNumeric.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ExposureSecondsNumeric.Location = new System.Drawing.Point(471, 34);
            this.ExposureSecondsNumeric.Name = "ExposureSecondsNumeric";
            this.ExposureSecondsNumeric.Size = new System.Drawing.Size(152, 22);
            this.ExposureSecondsNumeric.TabIndex = 5;
            // 
            // ExposureMinutesNumeric
            // 
            this.ExposureMinutesNumeric.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ExposureMinutesNumeric.Location = new System.Drawing.Point(315, 34);
            this.ExposureMinutesNumeric.Name = "ExposureMinutesNumeric";
            this.ExposureMinutesNumeric.Size = new System.Drawing.Size(150, 22);
            this.ExposureMinutesNumeric.TabIndex = 6;
            // 
            // ExposureHoursNumeric
            // 
            this.ExposureHoursNumeric.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ExposureHoursNumeric.Location = new System.Drawing.Point(159, 34);
            this.ExposureHoursNumeric.Name = "ExposureHoursNumeric";
            this.ExposureHoursNumeric.Size = new System.Drawing.Size(150, 22);
            this.ExposureHoursNumeric.TabIndex = 7;
            // 
            // ExposureDaysNumeric
            // 
            this.ExposureDaysNumeric.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ExposureDaysNumeric.Location = new System.Drawing.Point(3, 34);
            this.ExposureDaysNumeric.Name = "ExposureDaysNumeric";
            this.ExposureDaysNumeric.Size = new System.Drawing.Size(150, 22);
            this.ExposureDaysNumeric.TabIndex = 8;
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(790, 479);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "SettingsForm";
            this.Text = "Settings";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TimeSliceDaysNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimeSliceHoursNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimeSliceMinutesNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimeSliceSecondsNumeric)).EndInit();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ExposureSecondsNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExposureMinutesNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExposureHoursNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExposureDaysNumeric)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox PatientZeroIdTextBox;
        private System.Windows.Forms.DateTimePicker StartTimeDatePicker;
        private System.Windows.Forms.DateTimePicker EndTimeDatePicker;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox DataPathTextBox;
        private System.Windows.Forms.Button BrowseButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown TimeSliceDaysNumeric;
        private System.Windows.Forms.NumericUpDown TimeSliceHoursNumeric;
        private System.Windows.Forms.NumericUpDown TimeSliceMinutesNumeric;
        private System.Windows.Forms.NumericUpDown TimeSliceSecondsNumeric;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.NumericUpDown ExposureDaysNumeric;
        private System.Windows.Forms.NumericUpDown ExposureHoursNumeric;
        private System.Windows.Forms.NumericUpDown ExposureMinutesNumeric;
        private System.Windows.Forms.NumericUpDown ExposureSecondsNumeric;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
    }
}