﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OutbreakAnalysis.Models;

namespace OutbreakAnalysisGui
{
    public partial class SettingsForm : Form
    {
        public OutbreakSimulationParameters Parameters { get; set; }
        public SettingsForm(OutbreakSimulationParameters parameters)
        {
            InitializeComponent();

            Parameters = parameters;

            StartTimeDatePicker.Format = DateTimePickerFormat.Custom;
            StartTimeDatePicker.CustomFormat = @"MM/dd/yyyy HH:mm:ss";

            EndTimeDatePicker.Format = DateTimePickerFormat.Custom;
            EndTimeDatePicker.CustomFormat = @"MM/dd/yyyy HH:mm:ss";

            var slice = Parameters.TimeSliceDuration;
            TimeSliceDaysNumeric.Value = slice.Days;
            TimeSliceHoursNumeric.Value = slice.Hours;
            TimeSliceMinutesNumeric.Value = slice.Minutes;
            TimeSliceSecondsNumeric.Value = slice.Seconds;

            var exp = Parameters.ExposureThreshold;
            ExposureDaysNumeric.Value = exp.Days;
            ExposureHoursNumeric.Value = exp.Hours;
            ExposureMinutesNumeric.Value = exp.Minutes;
            ExposureSecondsNumeric.Value = exp.Seconds;

            DataPathTextBox.Text = Parameters.DataPath;
            PatientZeroIdTextBox.Text = Parameters.PatientZeroId;
            StartTimeDatePicker.Value = Parameters.StartTime;
            EndTimeDatePicker.Value = Parameters.EndTime;
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            // Update settings
            Parameters.TimeSliceDuration = new TimeSpan(
                (int)TimeSliceDaysNumeric.Value,
                (int)TimeSliceHoursNumeric.Value,
                (int)TimeSliceMinutesNumeric.Value,
                (int)TimeSliceSecondsNumeric.Value);

            Parameters.ExposureThreshold = new TimeSpan(
                (int)ExposureDaysNumeric.Value,
                (int)ExposureHoursNumeric.Value,
                (int)ExposureMinutesNumeric.Value,
                (int)ExposureSecondsNumeric.Value);
            Parameters.DataPath = DataPathTextBox.Text;
            Parameters.PatientZeroId = PatientZeroIdTextBox.Text;
            Parameters.StartTime = StartTimeDatePicker.Value;
            Parameters.EndTime = EndTimeDatePicker.Value;
            Close();
        }

        private void BrowseButton_Click(object sender, EventArgs e)
        {
            var openForm = new Form { TopMost = true };

            // create and open the OpenFileDialog
            var dialog = new OpenFileDialog
            {
                Title = @"Open data export",
                Filter = @"Json Files|*.*",
                RestoreDirectory = true
            };

            if (dialog.ShowDialog(openForm) == DialogResult.OK)
            {
                DataPathTextBox.Text = dialog.FileName;
            }
        }
    }
}
