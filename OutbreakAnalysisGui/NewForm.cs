﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OutbreakAnalysisGui
{
    public partial class NewForm : Form
    {
        public string Path { get; set; }

        public NewForm()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, EventArgs e)
        {
            var buttonName = ((Button)sender).Name;
            try
            {
                switch (buttonName)
                {
                    case "BrowseButton":
                    {
                        var openForm = new Form {TopMost = true};

                        // create and open the OpenFileDialog
                        var dialog = new OpenFileDialog
                        {
                            Title = @"Open data export",
                            Filter = @"CSV Files|*.csv",
                            RestoreDirectory = true
                        };

                        if (dialog.ShowDialog(openForm) == DialogResult.OK)
                        {
                            Path = dialog.FileName;
                        }
                    }
                        break;
                    case "OkButton":
                        {
                            Close();
                        }
                        break;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, @"Error", MessageBoxButtons.OK);
            }
        }
    }
}
