﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OutbreakAnalysis.Definition;
using OutbreakAnalysis.Models;

namespace OutbreakAnalysisGui
{
    public class DrawUtility
    {
        public enum BeginOrEnd
        {
            Begin = 0,
            End = 1
        }

        /// <summary>
        /// Draw a time slice.
        /// </summary>
        /// <param name="sim">simulation</param>
        /// <param name="index">index of time slice</param>
        /// <param name="beginOrEnd">render the before or the after</param>
        /// <returns></returns>
        public static Bitmap DrawTimeSlice(OutbreakSimulation sim, int index, BeginOrEnd beginOrEnd)
        {
            if (index < 0 || index >= sim.TimeSlices.Count)
            {
                throw new ArgumentOutOfRangeException(nameof(index), $@"Invalid index {index} for the requested simulation timeslice list with max count {sim.TimeSlices.Count}");
            }

            var slice = sim.TimeSlices[index];

            var map = new Bitmap((int)sim.Parameters.LocationMap.OuterBoundary.Max.X, (int)sim.Parameters.LocationMap.OuterBoundary.Max.Y);
            _drawBackground(map, Color.Black);
            _drawDoors(sim.Doors.ToList(), map);
            switch (beginOrEnd)
            {
                case BeginOrEnd.Begin:
                    _drawStudents(slice.StudentsBeforeTimeSlice, map);
                    break;
                case BeginOrEnd.End:
                    _drawStudents(slice.StudentsAfterTimeSlice, map);
                    break;
            }


            return map;
        }

        private static void _drawDoors(List<Door> doors, Bitmap map)
        {
            using (Graphics gfx = Graphics.FromImage(map))
            {
                foreach (var door in doors)
                {
                    gfx.DrawRectangle(
                        Pens.Blue,
                        (int)door.Location.X - 10,
                        (int)door.Location.Y - 10, 
                        20, 
                        20);
                }
            }
        }

        private static void _drawStudents(List<Student> students, Bitmap map)
        {
            using (var gfx = Graphics.FromImage(map))
            {
                foreach (var student in students)
                {
                    using (SolidBrush brush = new SolidBrush(_getStatusColor(student.Status)))
                    {
                        gfx.FillEllipse(
                            brush,
                            new Rectangle(
                                (int) student.Location.X - 3,
                                (int) student.Location.Y - 3,
                                6, 6));
                    }
                }
            }
        }

        private static void _drawBackground(Bitmap map, Color color)
        {
            using (Graphics gfx = Graphics.FromImage(map))
            using (SolidBrush brush = new SolidBrush(color))
            {
                gfx.FillRectangle(brush, 0, 0, map.Width, map.Height);
            }
        }

        private static Color _getStatusColor(OutbreakStatus status)
        {
            switch (status)
            {
                case OutbreakStatus.Exposed: return Color.Red;
                case OutbreakStatus.PatientZero: return Color.Yellow;
                case OutbreakStatus.Unexposed: return Color.Green;
            }

            return Color.Black;
        }

    }
}
