﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using BbTS.Core;
using BbTS.Core.Configuration;
using BbTS.Core.Conversion;
using BbTS.Core.Media.ImageProcessing;
using BbTS.Core.Security.Database;
using BbTS.Domain.Models.Definitions.Notification;
using BbTS.Domain.Models.Notification;
using BbTS.Resource.Database.Abstract;
using BbTS.Resource.Service.Abstract;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BbTS.Resource.Test
{
    [TestClass]
    public class NotificationUnitTests
    {
        private Configuration _configuration;
        private ResourceDatabase _databaseResource;
        private ResourceServiceAbstract _serviceResource;


        /// <summary>
        /// Test initialization.
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            string message;
            Assert.IsTrue(CustomConfiguration.LoadCustomConfiguration("App.config", out _configuration, out message), message);

            var dataSourceString = CustomConfiguration.GetKeyValueAsString("DataSource", _configuration);
            Guard.IsNotNullOrWhiteSpace(dataSourceString, "DataSource From Congfig");
            var dataSource = (DataSource)Enum.Parse(typeof (DataSource), dataSourceString);
            _databaseResource = ResourceManager.Instance.Resource(dataSource);
            _databaseResource.ConnectionString = DatabaseSecurityTool.CreateConnectionStringFromRegistry();

            var dataSourceServiceString = CustomConfiguration.GetKeyValueAsString("ServiceSource", _configuration);
            Guard.IsNotNullOrWhiteSpace(dataSourceServiceString, "DataSource From Congfig");
            var serviceSource = (ServiceSource)Enum.Parse(typeof (ServiceSource), dataSourceServiceString);
            _serviceResource = ResourceManager.Instance.ResourceService(serviceSource);

        }

        [TestMethod]
        [DeploymentItem("App.config")]
        public void NotificationCreateAndGetUnitTest()
        {
            Guard.IsNotNull(_databaseResource, "DatabaseResource");
            Guard.IsNotNullOrWhiteSpace(_databaseResource.ConnectionString, "ConnectionString");

            var customer = _databaseResource.CustomerGet(null, 501);

            string message;
            if (!_createNotification(out message))
            {
                Assert.Fail(message);
            }

            var request = new NotificationGetRequest();
            var response = _databaseResource.NotificationGet(request);
            Assert.IsTrue(response.Notifications.FirstOrDefault(n => string.Equals(n.ObjectGuid.ToString("D"), customer.CustomerGuid, StringComparison.CurrentCultureIgnoreCase)) != null);

        }

        [TestMethod]
        [DeploymentItem("App.config")]
        public void NotificationCreateTestDataUnitTest()
        {
            Guard.IsNotNull(_databaseResource, "DatabaseResource");
            Guard.IsNotNullOrWhiteSpace(_databaseResource.ConnectionString, "ConnectionString");

            string message;
            if (!_createTestData(out message))
            {
                Assert.Fail(message);
            }
        }

        [TestMethod]
        [DeploymentItem("App.config")]
        public void NotificationStatusSetToCompleteUnitTest()
        {
            Guard.IsNotNull(_databaseResource, "DatabaseResource");
            Guard.IsNotNullOrWhiteSpace(_databaseResource.ConnectionString, "ConnectionString");

            var customer = _databaseResource.CustomerGet(null, 501);

            var objectList = new List<NotificationObject>();
            string message;
            int retryCount = 3;
            while (objectList.Count == 0)
            {

                if (!_getNotifications(customer.CustomerGuid, ref objectList, out message))
                {
                    Assert.Fail(message);
                }

                if (objectList.Count == 0)
                {
                    if (!_createNotification(out message))
                    {
                        Assert.Fail(message);
                    }
                }
                Thread.Sleep(1000);
                retryCount --;
                if (retryCount == 0)
                {
                    Assert.Inconclusive("Unable to successfully create a notification in the database after multiple attempts.  Aborting test.");
                }
            }

            foreach (var item in objectList)
            {
                var request = new NotificationStatusSetRequest
                {
                    NotificationId = item.NotificationId,
                    StatusType = NotificationStatusType.Complete
                };
                _databaseResource.NotificationStatusSet(request);
            }

            objectList = new List<NotificationObject>();
            if (!_getNotifications(customer.CustomerGuid, ref objectList, out message))
            {
                Assert.Fail(message);
            }

            Assert.IsTrue(objectList.Count == 0);
        }

        [TestMethod]
        [DeploymentItem("App.config")]
        public void CustomerPhotoGetUnitTest()
        {
            var customer1 = "18697831-0C98-4DA5-8E26-A7E790A6CC7E";
            var customer2 = "C55DF474-448D-4DFF-8C3D-1067ECDC1261";
            var customer3 = "E71817C0-7DCD-45D5-8EC8-688512F3E214";

            // Customer 1
            {
                Trace.WriteLine("----------------------------------------------------");
                Trace.WriteLine("Customer 1 photo processing...");
                Trace.WriteLine("----------------------------------------------------");
                var photoInfo = _databaseResource.CustomerPhotoGet(customer1);

                if (photoInfo == null)
                {
                    Assert.Inconclusive("CustomerPhotoGet object was null.");
                }

                if (photoInfo.Photo == null)
                {
                    Trace.WriteLine("Customer photo object was null.  Null detection worked.");
                }

                try
                {
                    var convertedImage = ImageTool.Instance.ImageFromByteScaleAndConvert(photoInfo.Photo, 1024, 1024, ImageFormat.Png);
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(
                            $"Customer '{photoInfo.FirstName}, {photoInfo.LastName}' id = [{customer1}]: " + 
                            $"Photo failed image conversion.  Failure message was {Formatting.FormatException(ex)}");                    
                }
            }

            // Customer 2
            {
                Trace.WriteLine("----------------------------------------------------");
                Trace.WriteLine("Customer 2 photo processing...");
                Trace.WriteLine("----------------------------------------------------");
                var photoInfo = _databaseResource.CustomerPhotoGet(customer2);

                if (photoInfo == null)
                {
                    Assert.Inconclusive("CustomerPhotoGet object was null.");
                }

                if (photoInfo.Photo == null)
                {
                    Trace.WriteLine("Customer photo object was null.  Null detection worked.");
                }

                try
                {
                    var convertedImage = ImageTool.Instance.ImageFromByteScaleAndConvert(photoInfo.Photo, 1024, 1024, ImageFormat.Png);
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(
                            $"Customer '{photoInfo.FirstName}, {photoInfo.LastName}' id = [{customer2}]: " +
                            $"Photo failed image conversion.  Failure message was {Formatting.FormatException(ex)}");
                }
            }

            // Customer 3
            {
                Trace.WriteLine("----------------------------------------------------");
                Trace.WriteLine("Customer 3 photo processing...");
                Trace.WriteLine("----------------------------------------------------");
                var photoInfo = _databaseResource.CustomerPhotoGet(customer3);

                if (photoInfo == null)
                {
                    Assert.Inconclusive("CustomerPhotoGet object was null.");
                }

                if (photoInfo.Photo == null)
                {
                    Trace.WriteLine("Customer photo object was null.  Null detection worked.");
                }

                try
                {
                    var convertedImage = ImageTool.Instance.ImageFromByteScaleAndConvert(photoInfo.Photo, 1024, 1024, ImageFormat.Png);
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(
                            $"Customer '{photoInfo.FirstName}, {photoInfo.LastName}' id = [{customer3}]: " +
                            $"Photo failed image conversion.  Failure message was {Formatting.FormatException(ex)}");
                }

            }
        }

        [TestMethod]
        [DeploymentItem("App.config")]
        public void CustomerPhotoResizeUnitTest()
        {
            var photoInfo = _databaseResource.CustomerPhotoGet("8C6A72CD-0A1C-46D1-A0E6-2F12EF13952F");

            var convertedImage = ImageTool.Instance.ImageFromByteScaleAndConvert(photoInfo.Photo, 1024, 1024, ImageFormat.Png);
            Assert.IsTrue(!string.IsNullOrWhiteSpace(convertedImage));

            var newBytes = Convert.FromBase64String(convertedImage);

            var image = ImageTool.Instance.ImageFromBytes(newBytes);
            Assert.IsTrue(image.Width == 1024 && image.Height == 1024, $"Image dimensions are not 1024 x 1024.  Width = {image.Width}, Height = {image.Height}");
            Assert.IsTrue(image.PixelFormat == PixelFormat.Format32bppArgb, $"Image pixel format is not correct.  Expecting '{PixelFormat.Format32bppArgb}' but received '{image.PixelFormat}'");

        }

        [TestMethod]
        [DeploymentItem("App.config")]
        [DeploymentItem("JpegStringBytes.txt")]
        public void CustomerPhotoFromStringBytesResizeUnitTest()
        {
            var stringBytes = File.ReadAllText("JpegStringBytes.txt");

            var startTime = DateTime.Now;
            var bytes = Formatting.HexStringToByteArray(stringBytes);
            Trace.WriteLine($"Conversion process into byte array took {(DateTime.Now - startTime).TotalSeconds} seconds.");
            //var bytes = Encoding.UTF8.GetBytes(stringBytes);

            var convertedImage = ImageTool.Instance.ImageFromByteScaleAndConvert(bytes, 1024, 1024, ImageFormat.Png);
            Assert.IsTrue(!string.IsNullOrWhiteSpace(convertedImage));

            var newBytes = Convert.FromBase64String(convertedImage);

            var image = ImageTool.Instance.ImageFromBytes(newBytes);
            Assert.IsTrue(image.Width == 1024 && image.Height == 1024, $"Image dimensions are not 1024 x 1024.  Width = {image.Width}, Height = {image.Height}");
            Assert.IsTrue(image.PixelFormat == PixelFormat.Format32bppArgb, $"Image pixel format is not correct.  Expecting '{PixelFormat.Format32bppArgb}' but received '{image.PixelFormat}'");
        }

        #region Private functions

        /// <summary>
        /// Create a notification object in the database.
        /// </summary>
        /// <param name="message">Out results message.</param>
        /// <returns></returns>
        private bool _createNotification(out string message)
        {
            try
            {
                var request = new NotificationCreateRequest
                {
                    ObjectId = 501,
                    EventType = NotificationEventType.Updated,
                    ObjectType = NotificationObjectType.Customer
                };
                _databaseResource.NotificationCreate(request);

                message = "Success";
                return true;
            }
            catch (Exception ex)
            {
                message = Formatting.FormatException(ex);
                return false;
            }
        }

        /// <summary>
        /// Create a notification object in the database.
        /// </summary>
        /// <param name="message">Out results message.</param>
        /// <returns></returns>
        private bool _createTestData(out string message)
        {
            try
            {
                var request = new NotificationCreateRequest
                {
                    ObjectId = 501,
                    EventType = NotificationEventType.Updated,
                    ObjectType = NotificationObjectType.Customer,
                    ObjectGuid = new Guid("8C6A72CD-0A1C-46D1-A0E6-2F12EF13952F")
                };
                _databaseResource.NotificationCreate(request);

                request = new NotificationCreateRequest
                {
                    ObjectId = 501,
                    EventType = NotificationEventType.Updated,
                    ObjectType = NotificationObjectType.Card,
                    ObjectGuid = new Guid("8C6A72CD-0A1C-46D1-A0E6-2F12EF13952F")
                };
                _databaseResource.NotificationCreate(request);

                request = new NotificationCreateRequest
                {
                    ObjectId = 501,
                    EventType = NotificationEventType.Updated,
                    ObjectType = NotificationObjectType.CustomerPinHash,
                    ObjectGuid = new Guid("8C6A72CD-0A1C-46D1-A0E6-2F12EF13952F")
                };
                _databaseResource.NotificationCreate(request);

                request = new NotificationCreateRequest
                {
                    ObjectId = 501,
                    EventType = NotificationEventType.Updated,
                    ObjectType = NotificationObjectType.CustomerImage,
                    ObjectGuid = new Guid("8C6A72CD-0A1C-46D1-A0E6-2F12EF13952F")
                };
                _databaseResource.NotificationCreate(request);

                message = "Success";
                return true;
            }
            catch (Exception ex)
            {
                message = Formatting.FormatException(ex);
                return false;
            }
        }

        /// <summary>
        /// Get all relevant test notification events.
        /// </summary>
        /// <param name="customerGuid"></param>
        /// <param name="list">List to add to.</param>
        /// <param name="message">results message.</param>
        /// <returns></returns>
        private bool _getNotifications(string customerGuid, ref List<NotificationObject> list, out string message)
        {
            try
            {
                var request = new NotificationGetRequest();
                var response = _databaseResource.NotificationGet(request);
                list = response.Notifications.FindAll(
                    n =>
                        string.Equals(n.ObjectGuid.ToString("D"), customerGuid, StringComparison.CurrentCultureIgnoreCase) &&
                        n.ObjectType == NotificationObjectType.Customer &&
                        n.EventType == NotificationEventType.Updated);

                message = "Success";
                return true;
            }
            catch (Exception ex)
            {
                message = Formatting.FormatException(ex);
                return false;
            }
        }

        #endregion
    }
}
