﻿using System;
using System.Configuration;
using BbTS.Core;
using BbTS.Core.Configuration;
using BbTS.Core.Security.Database;
using BbTS.Domain.Models.Definitions.Transaction;
using BbTS.Domain.Models.Transaction;
using BbTS.Domain.Models.Transaction.Validation;
using BbTS.Resource.Database.Abstract;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BbTS.Resource.Test
{
    [TestClass]
    public class TransactionValidationUnitTests
    {
        private Configuration _configuration;
        private ResourceDatabase _databaseResource;

        public string DeviceGuid { get; set; }
        public string CustomerGuid { get; set; }

        public string OperatorGuid { get; set; }

        public string CashierId { get; set; }
        public int ProductPromoId { get; set; }

        /// <summary>
        /// Test initialization.
        /// </summary>
        [TestInitialize]
        [DeploymentItem("App.config")]
        public void Initialize()
        {
            string message;
            Assert.IsTrue(CustomConfiguration.LoadCustomConfiguration("App.config", out _configuration, out message), message);

            var dataSourceString = CustomConfiguration.GetKeyValueAsString("DataSource", _configuration);
            Guard.IsNotNullOrWhiteSpace(dataSourceString, "DataSource From Congfig");
            var dataSource = (DataSource)Enum.Parse(typeof (DataSource), dataSourceString);
            _databaseResource = ResourceManager.Instance.Resource(dataSource);
            _databaseResource.ConnectionString = DatabaseSecurityTool.CreateConnectionStringFromRegistry();

            // Initialize test values from App.Config
            DeviceGuid = CustomConfiguration.GetKeyValueAsString("DeviceId", _configuration);
            CustomerGuid = CustomConfiguration.GetKeyValueAsString("CustomerId", _configuration);
            OperatorGuid = CustomConfiguration.GetKeyValueAsString("OperatorIdHasCard", _configuration);
            CashierId = CustomConfiguration.GetKeyValueAsString("CashierId", _configuration);
            ProductPromoId = int.Parse(CustomConfiguration.GetKeyValueAsString("ProductPromoId", _configuration));
        }

        #region OperatorByOperatorGuid

        [TestMethod]
        public void OperatorByOperatorGuidValidateSuccessCase()
        {
            var response = _databaseResource.OperatorByOperatorGuidValidation(OperatorGuid, DeviceGuid);

            Assert.IsTrue(response != null, "Failed to return a result from the resource layer.");
            Assert.IsTrue(response.ErrorCode == 0, $"Expected error code 0 was not equal to actual error code '{response.ErrorCode}'.  Denied text was '{response.DeniedText}'");

        }

        [TestMethod]
        public void OperatorByOperatorGuidValidateFailOperatorGuid()
        {
            var guid = Guid.NewGuid().ToString("D");
            var response = _databaseResource.OperatorByOperatorGuidValidation(guid, DeviceGuid);

            Assert.IsTrue(response != null, "Failed to return a result from the resource layer.");
            Assert.IsTrue(response.ErrorCode == 2, $"Expected error code 2 was not equal to actual error code '{response.ErrorCode}'.  Denied text was '{response.DeniedText}'");
            Assert.IsTrue(response.DeniedText == "Cashier Not Found", $"Expected denied text 'Cashier Not Found' but received '{response.DeniedText}'");
        }

        [TestMethod]
        public void OperatorByOperatorGuidValidateFailDeviceGuid()
        {
            var guid = Guid.NewGuid().ToString("D");
            var response = _databaseResource.OperatorByOperatorGuidValidation(OperatorGuid, guid);

            Assert.IsTrue(response != null, "Failed to return a result from the resource layer.");
            Assert.IsTrue(response.ErrorCode == 1, $"Expected error code 1 was not equal to actual error code '{response.ErrorCode}'.  Denied text was '{response.DeniedText}'");
            Assert.IsTrue(response.DeniedText == "POS Not Found", $"Expected denied text 'POS Not Found' but received '{response.DeniedText}'");
        }

        #endregion

        #region ProductPromo

        [TestMethod]
        public void ProductPromoValidationSuccessCase()
        {
            var response = _databaseResource.ProductPromoValidation(ProductPromoId);

            Assert.IsTrue(response != null, "Failed to return a result from the resource layer.");
            Assert.IsTrue(response.ErrorCode == 0, $"Expected error code 0 was not equal to actual error code '{response.ErrorCode}'.  Denied text was '{response.DeniedText}'");
        }

        [TestMethod]
        public void ProductPromoValidationFailWrongId()
        {
            var response = _databaseResource.ProductPromoValidation(55555);

            Assert.IsTrue(response != null, "Failed to return a result from the resource layer.");
            Assert.IsTrue(response.ErrorCode == 8, $"Expected error code 8 was not equal to actual error code '{response.ErrorCode}'.  Denied text was '{response.DeniedText}'");
            Assert.IsTrue(response.DeniedText == "Product/Promo Not Found", $"Expected denied text 'Product/Promo Not Found' but received '{response.DeniedText}'");
        }

        #endregion

        #region TransactionAttribute

        [TestMethod]
        public void TransactionValidationSuccessCase()
        {
            var request = new TransactionAttributeValidationRequest
            {
                DeviceGuid = DeviceGuid,
                Attributes = new RetailTransactionAttributes
                {
                    PeriodNumber = 1,
                    TransactionTypeCode = 0,
                    ValidationType = 0,
                    AttendType = 0,
                    RetailTranType = 1
                }
            };
            var response = _databaseResource.TransactionAttributeValidation(request);

            Assert.IsTrue(response != null, "Failed to return a result from the resource layer.");
            Assert.IsTrue(response.ErrorCode == 0, $"Expected error code 0 was not equal to actual error code '{response.ErrorCode}'.  Denied text was '{response.DeniedText}'");
        }

        [TestMethod]
        public void TransactionValidationFailWrongDeviceGuid()
        {
            var request = new TransactionAttributeValidationRequest
            {
                DeviceGuid = Guid.NewGuid().ToString("D"),
                Attributes = new RetailTransactionAttributes
                {
                    PeriodNumber = 1,
                    TransactionTypeCode = 0,
                    ValidationType = 0,
                    AttendType = 0,
                    RetailTranType = 1
                }
            };
            var response = _databaseResource.TransactionAttributeValidation(request);

            Assert.IsTrue(response != null, "Failed to return a result from the resource layer.");
            Assert.IsTrue(response.ErrorCode == 1, $"Expected error code 1 was not equal to actual error code '{response.ErrorCode}'.  Denied text was '{response.DeniedText}'");
            Assert.IsTrue(response.DeniedText == "POS Not Found", $"Expected denied text 'POS Not Found' but received '{response.DeniedText}'");
        }

        [TestMethod]
        public void TransactionValidationFailWrongPeriodNumber()
        {
            var request = new TransactionAttributeValidationRequest
            {
                DeviceGuid = DeviceGuid,
                Attributes = new RetailTransactionAttributes
                {
                    PeriodNumber = 0,
                    TransactionTypeCode = 0,
                    ValidationType = 0,
                    AttendType = 0,
                    RetailTranType = 1
                }
            };
            var response = _databaseResource.TransactionAttributeValidation(request);

            Assert.IsTrue(response != null, "Failed to return a result from the resource layer.");
            Assert.IsTrue(response.ErrorCode == 7, $"Expected error code 7 was not equal to actual error code '{response.ErrorCode}'.  Denied text was '{response.DeniedText}'");
            Assert.IsTrue(response.DeniedText == "Outside Valid Period Time", $"Expected denied text 'Outside Valid Period Time' but received '{response.DeniedText}'");
        }

        [TestMethod]
        public void TransactionValidationFailWrongTransactionTypeCode()
        {
            var request = new TransactionAttributeValidationRequest
            {
                DeviceGuid = DeviceGuid,
                Attributes = new RetailTransactionAttributes
                {
                    PeriodNumber = 1,
                    TransactionTypeCode = 9001,
                    ValidationType = 0,
                    AttendType = 0,
                    RetailTranType = 1
                }
            };
            var response = _databaseResource.TransactionAttributeValidation(request);

            Assert.IsTrue(response != null, "Failed to return a result from the resource layer.");
            Assert.IsTrue(response.ErrorCode == 14001, $"Expected error code 14001 was not equal to actual error code '{response.ErrorCode}'.  Denied text was '{response.DeniedText}'");
        }

        [TestMethod]
        public void TransactionValidationFailWrongValicationType()
        {
            var request = new TransactionAttributeValidationRequest
            {
                DeviceGuid = DeviceGuid,
                Attributes = new RetailTransactionAttributes
                {
                    PeriodNumber = 1,
                    TransactionTypeCode = 0,
                    ValidationType = 9001,
                    AttendType = 0,
                    RetailTranType = 1
                }
            };
            var response = _databaseResource.TransactionAttributeValidation(request);

            Assert.IsTrue(response != null, "Failed to return a result from the resource layer.");
            Assert.IsTrue(response.ErrorCode == 14002, $"Expected error code 14002 was not equal to actual error code '{response.ErrorCode}'.  Denied text was '{response.DeniedText}'");
        }

        [TestMethod]
        public void TransactionValidationFailWrongAttendType()
        {
            var request = new TransactionAttributeValidationRequest
            {
                DeviceGuid = DeviceGuid,
                Attributes = new RetailTransactionAttributes
                {
                    PeriodNumber = 1,
                    TransactionTypeCode = 0,
                    ValidationType = 0,
                    AttendType = 9001,
                    RetailTranType = 1
                }
            };
            var response = _databaseResource.TransactionAttributeValidation(request);

            Assert.IsTrue(response != null, "Failed to return a result from the resource layer.");
            Assert.IsTrue(response.ErrorCode == 14003, $"Expected error code 14003 was not equal to actual error code '{response.ErrorCode}'.  Denied text was '{response.DeniedText}'");
        }

        [TestMethod]
        public void RetailTranTypeValidationFailWrongType()
        {
            var request = new TransactionAttributeValidationRequest
            {
                DeviceGuid = DeviceGuid,
                Attributes = new RetailTransactionAttributes
                {
                    PeriodNumber = 1,
                    TransactionTypeCode = 0,
                    ValidationType = 0,
                    AttendType = 0,
                    RetailTranType = 9001
                }
            };
            var response = _databaseResource.TransactionAttributeValidation(request);

            Assert.IsTrue(response != null, "Failed to return a result from the resource layer.");
            Assert.IsTrue(response.ErrorCode == 14004, $"Expected error code 14004 was not equal to actual error code '{response.ErrorCode}'.  Denied text was '{response.DeniedText}'");

        }

        #endregion

        #region RetailTransactionLineProduct Validation Tests

        [TestMethod]
        public void RetailTransactionLineProductSuccessCase()
        {
            var request = new RetailTransactionLineProductValidationRequest
            {
                ProductDetailId = 414,
                TaxScheduleId = 1,
                TaxGroupId = 1,
                UnitMeasureType = 0,
                ProductEntryMethodType = 2,
                RetailPriceEntryMethodType = 0
            };
            var response = _databaseResource.RetailTransactionLineProductValidation(request);

            Assert.IsTrue(response != null, "Failed to return a result from the resource layer.");
            Assert.IsTrue(response.ErrorCode == 0, $"Expected error code 0 was not equal to actual error code '{response.ErrorCode}'.  Denied text was '{response.DeniedText}'");
        }

        [TestMethod]
        public void RetailTransactionLineProductWrongProductDetailId()
        {
            var request = new RetailTransactionLineProductValidationRequest
            {
                ProductDetailId = 0,
                TaxScheduleId = 1,
                TaxGroupId = 1,
                UnitMeasureType = 0,
                ProductEntryMethodType = 2,
                RetailPriceEntryMethodType = 0
            };
            var response = _databaseResource.RetailTransactionLineProductValidation(request);

            Assert.IsTrue(response != null, "Failed to return a result from the resource layer.");
            Assert.IsTrue(response.ErrorCode == 14005, $"Expected error code 14005 was not equal to actual error code '{response.ErrorCode}'.  Denied text was '{response.DeniedText}'");
            Assert.IsTrue(response.DeniedText == "Product Detail Record Not Found", $"Expected denied text 'Product Detail Record Not Found' but received '{response.DeniedText}'");
        }

        [TestMethod]
        public void RetailTransactionLineProductWrongTaxScheduleId()
        {
            var request = new RetailTransactionLineProductValidationRequest
            {
                ProductDetailId = 414,
                TaxScheduleId = 0,
                TaxGroupId = 1,
                UnitMeasureType = 0,
                ProductEntryMethodType = 2,
                RetailPriceEntryMethodType = 0
            };
            var response = _databaseResource.RetailTransactionLineProductValidation(request);

            Assert.IsTrue(response != null, "Failed to return a result from the resource layer.");
            Assert.IsTrue(response.ErrorCode == 14006, $"Expected error code 14006 was not equal to actual error code '{response.ErrorCode}'.  Denied text was '{response.DeniedText}'");
            Assert.IsTrue(response.DeniedText == "Tax Schedule Not Found", $"Expected denied text 'Tax Schedule Not Found' but received '{response.DeniedText}'");
        }

        [TestMethod]
        public void RetailTransactionLineProductWrongTaxGroupId()
        {
            var request = new RetailTransactionLineProductValidationRequest
            {
                ProductDetailId = 414,
                TaxScheduleId = 1,
                TaxGroupId = 99999,
                UnitMeasureType = 0,
                ProductEntryMethodType = 2,
                RetailPriceEntryMethodType = 0
            };
            var response = _databaseResource.RetailTransactionLineProductValidation(request);

            Assert.IsTrue(response != null, "Failed to return a result from the resource layer.");
            Assert.IsTrue(response.ErrorCode == 14007, $"Expected error code 14007 was not equal to actual error code '{response.ErrorCode}'.  Denied text was '{response.DeniedText}'");
            Assert.IsTrue(response.DeniedText == "Tax Group Not Found", $"Expected denied text 'Tax Group Not Found' but received '{response.DeniedText}'");
        }

        [TestMethod]
        public void RetailTransactionLineProductWrongUnitMeasureType()
        {
            var request = new RetailTransactionLineProductValidationRequest
            {
                ProductDetailId = 414,
                TaxScheduleId = 1,
                TaxGroupId = 1,
                UnitMeasureType = 999,
                ProductEntryMethodType = 2,
                RetailPriceEntryMethodType = 0
            };
            var response = _databaseResource.RetailTransactionLineProductValidation(request);

            Assert.IsTrue(response != null, "Failed to return a result from the resource layer.");
            Assert.IsTrue(response.ErrorCode == 14008, $"Expected error code 14008 was not equal to actual error code '{response.ErrorCode}'.  Denied text was '{response.DeniedText}'");
            Assert.IsTrue(response.DeniedText == "Unit of Measure Type Not Found", $"Expected denied text 'Unit of Measure Type Not Found' but received '{response.DeniedText}'");
        }

        [TestMethod]
        public void RetailTransactionLineProductWrongProductEntryMethodType()
        {
            var request = new RetailTransactionLineProductValidationRequest
            {
                ProductDetailId = 414,
                TaxScheduleId = 1,
                TaxGroupId = 1,
                UnitMeasureType = 0,
                ProductEntryMethodType = 999,
                RetailPriceEntryMethodType = 0
            };
            var response = _databaseResource.RetailTransactionLineProductValidation(request);

            Assert.IsTrue(response != null, "Failed to return a result from the resource layer.");
            Assert.IsTrue(response.ErrorCode == 14009, $"Expected error code 14009 was not equal to actual error code '{response.ErrorCode}'.  Denied text was '{response.DeniedText}'");
            Assert.IsTrue(response.DeniedText == "Transaction Product Entry Type not found", $"Expected denied text 'Transaction Product Entry Type not found' but received '{response.DeniedText}'");
        }

        [TestMethod]
        public void RetailTransactionLineProductWrongRetailPriceEntryMethodType()
        {
            var request = new RetailTransactionLineProductValidationRequest
            {
                ProductDetailId = 414,
                TaxScheduleId = 1,
                TaxGroupId = 1,
                UnitMeasureType = 0,
                ProductEntryMethodType = 2,
                RetailPriceEntryMethodType = 999999
            };
            var response = _databaseResource.RetailTransactionLineProductValidation(request);

            Assert.IsTrue(response != null, "Failed to return a result from the resource layer.");
            Assert.IsTrue(response.ErrorCode == 14010, $"Expected error code 14010 was not equal to actual error code '{response.ErrorCode}'.  Denied text was '{response.DeniedText}'");
            Assert.IsTrue(response.DeniedText == "Transaction Retail Price Entry Type not found", $"Expected denied text 'Transaction Retail Price Entry Type not found' but received '{response.DeniedText}'");
        }

        #endregion

        #region RetailTransactionLineTender Validation Tests

        [TestMethod]
        public void RetailTransactionLineTenderSuccessCase()
        {
            var request = new RetailTransactionLineTenderValidationRequest
            {
                DeviceGuid = DeviceGuid,
                TenderType = TenderType.Cash,
                TenderId = 999999,
                RequestId = Guid.NewGuid().ToString("D")

            };
            var response = _databaseResource.RetailTranactionLineTenderValidation(request);

            Assert.IsTrue(response != null, "Failed to return a result from the resource layer.");
            Assert.IsTrue(response.ErrorCode == 0, $"Expected error code 0 was not equal to actual error code '{response.ErrorCode}'.  Denied text was '{response.DeniedText}'");
        }

        [TestMethod]
        public void RetailTransactionLineTenderWrongTenderId()
        {
            var request = new RetailTransactionLineTenderValidationRequest
            {
                DeviceGuid = DeviceGuid,
                TenderType = TenderType.Cash,
                TenderId = 999999,
                RequestId = Guid.NewGuid().ToString("D")

            };
            var response = _databaseResource.RetailTranactionLineTenderValidation(request);

            Assert.IsTrue(response != null, "Failed to return a result from the resource layer.");
            Assert.IsTrue(response.ErrorCode == 6, $"Expected error code 6 was not equal to actual error code '{response.ErrorCode}'.  Denied text was '{response.DeniedText}'");
            Assert.IsTrue(response.DeniedText == "Invalid Tender Number", $"Expected denied text 'Invalid Tender Number' but received '{response.DeniedText}'");
        }

        #endregion

        #region Customer Transaction Info Validation Tests

        [TestMethod]
        public void CustomerTransactionInfoValidationSuccess()
        {
            var request = new CustomerTransactionInfoValidationRequest
            {
                CardNumber = "0000000000000000100001",
                IssueNumber = "4444",
                IssueNumberCaptured = true,
                CustomerGuid = "F59AD6C7-8958-40C3-A238-0D59AD8D327A",
                Pin = 1001,
                TransactionDateTime = DateTime.Now
            };

            var response = _databaseResource.CustomerTransactionInfoValidation(request);

            Assert.IsTrue(response != null, "Failed to return a result from the resource layer.");
            Assert.IsTrue(response.ErrorCode == 0, $"Expected error code 0 was not equal to actual error code '{response.ErrorCode}'.  Denied text was '{response.DeniedText}'");
        }

        [TestMethod]
        public void CustomerTransactionInfoWrongCardNumber()
        {
            var request = new CustomerTransactionInfoValidationRequest
            {
                CardNumber = "9879879879879879879879",
                IssueNumber = "4444",
                IssueNumberCaptured = true,
                CustomerGuid = "F59AD6C7-8958-40C3-A238-0D59AD8D327A",
                Pin = 1001,
                TransactionDateTime = DateTime.Now
            };

            var response = _databaseResource.CustomerTransactionInfoValidation(request);

            Assert.IsTrue(response != null, "Failed to return a result from the resource layer.");
            Assert.IsTrue(response.ErrorCode == 9, $"Expected error code 9 was not equal to actual error code '{response.ErrorCode}'.  Denied text was '{response.DeniedText}'");
            Assert.IsTrue(response.DeniedText == "Customer Not Found", $"Expected denied text 'Customer Not Found' but received '{response.DeniedText}'");
        }

        [TestMethod]
        public void CustomerTransactionInfoWrongPin()
        {
            var request = new CustomerTransactionInfoValidationRequest
            {
                CardNumber = "0000000000000000100001",
                IssueNumber = "4444",
                IssueNumberCaptured = true,
                CustomerGuid = "F59AD6C7-8958-40C3-A238-0D59AD8D327A",
                Pin = 0,
                TransactionDateTime = DateTime.Now
            };

            var response = _databaseResource.CustomerTransactionInfoValidation(request);

            Assert.IsTrue(response != null, "Failed to return a result from the resource layer.");
            Assert.IsTrue(response.ErrorCode == 12001, $"Expected error code 12001 was not equal to actual error code '{response.ErrorCode}'.  Denied text was '{response.DeniedText}'");
            Assert.IsTrue(response.DeniedText == "Customer Pin is invalid", $"Expected denied text 'Customer Pin is invalid' but received '{response.DeniedText}'");
        }

        [TestMethod]
        public void CustomerTransactionInfoWrongIssueNumber()
        {
            var request = new CustomerTransactionInfoValidationRequest
            {
                CardNumber = "0000000000000000100001",
                IssueNumber = "0",
                IssueNumberCaptured = true,
                CustomerGuid = "F59AD6C7-8958-40C3-A238-0D59AD8D327A",
                Pin = 1001,
                TransactionDateTime = DateTime.Now
            };

            var response = _databaseResource.CustomerTransactionInfoValidation(request);

            Assert.IsTrue(response != null, "Failed to return a result from the resource layer.");
            Assert.IsTrue(response.ErrorCode == 12002, $"Expected error code 12002 was not equal to actual error code '{response.ErrorCode}'.  Denied text was '{response.DeniedText}'");
            Assert.IsTrue(response.DeniedText == "Customer issue number is invalid", $"Expected denied text 'Customer issue number is invalid' but received '{response.DeniedText}'");
        }

        [TestMethod]
        [Ignore]
        public void CustomerTransactionInfCustomerInactive()
        {
            var request = new CustomerTransactionInfoValidationRequest
            {
                CardNumber = "0000000000000000100001",
                IssueNumber = "0",
                IssueNumberCaptured = true,
                CustomerGuid = "F59AD6C7-8958-40C3-A238-0D59AD8D327A",
                Pin = 1001,
                TransactionDateTime = DateTime.Now
            };

            var response = _databaseResource.CustomerTransactionInfoValidation(request);

            Assert.IsTrue(response != null, "Failed to return a result from the resource layer.");
            Assert.IsTrue(response.ErrorCode == 12008, $"Expected error code 12008 was not equal to actual error code '{response.ErrorCode}'.  Denied text was '{response.DeniedText}'");
            Assert.IsTrue(response.DeniedText == "The customer is set to inactive.", $"Expected denied text 'The customer is set to inactive.' but received '{response.DeniedText}'");
        }


        [TestMethod]
        [Ignore]
        public void CustomerTransactionInfCustomerOutOfActivePeriod()
        {
            var request = new CustomerTransactionInfoValidationRequest
            {
                CardNumber = "0000000000000000100001",
                IssueNumber = "0",
                IssueNumberCaptured = true,
                CustomerGuid = "F59AD6C7-8958-40C3-A238-0D59AD8D327A",
                Pin = 1001,
                TransactionDateTime = DateTime.Now
            };

            var response = _databaseResource.CustomerTransactionInfoValidation(request);

            Assert.IsTrue(response != null, "Failed to return a result from the resource layer.");
            Assert.IsTrue(response.ErrorCode == 15, $"Expected error code 15 was not equal to actual error code '{response.ErrorCode}'.  Denied text was '{response.DeniedText}'");
            Assert.IsTrue(response.DeniedText == "Customer Outside Active Start/End Dates", $"Expected denied text 'Customer Outside Active Start/End Dates' but received '{response.DeniedText}'");
        }

        #endregion

        #region Retail Transaction Line Tender Customer Validation Tests

        [TestMethod]
        public void RetailTransactionLineTenderCustomerSuccessCase()
        {
            var request = new RetailTransactionLineTenderCustomerValidationRequest
            {
                PhysicalIdType = 0,
                CustomerEntryMethodType = 1,
                CustomerGuid = "F59AD6C7-8958-40C3-A238-0D59AD8D327A"
            };

            var response = _databaseResource.RetailTransactionLineTenderCustomerValidation(request);

            Assert.IsTrue(response != null, "Failed to return a result from the resource layer.");
            Assert.IsTrue(response.ErrorCode == 0, $"Expected error code 0 was not equal to actual error code '{response.ErrorCode}'.  Denied text was '{response.DeniedText}'");
        }

        [TestMethod]
        public void RetailTransactionLineTenderCustomerWrongPhysicalId()
        {
            var request = new RetailTransactionLineTenderCustomerValidationRequest
            {
                PhysicalIdType = -1,
                CustomerEntryMethodType = 1,
                CustomerGuid = "F59AD6C7-8958-40C3-A238-0D59AD8D327A"
            };

            var response = _databaseResource.RetailTransactionLineTenderCustomerValidation(request);


            Assert.IsTrue(response != null, "Failed to return a result from the resource layer.");
            Assert.IsTrue(response.ErrorCode == 14018, $"Expected error code 14018 was not equal to actual error code '{response.ErrorCode}'.  Denied text was '{response.DeniedText}'");
            Assert.IsTrue(response.DeniedText == "Transaction Physcial Id Type not found", $"Expected denied text 'Transaction Physcial Id Type not found' but received '{response.DeniedText}'");
        }

        [TestMethod]
        public void RetailTransactionLineTenderCustomerWrongCustomerEntryMethodType()
        {
            var request = new RetailTransactionLineTenderCustomerValidationRequest
            {
                PhysicalIdType = 0,
                CustomerEntryMethodType = -1,
                CustomerGuid = "F59AD6C7-8958-40C3-A238-0D59AD8D327A"
            };

            var response = _databaseResource.RetailTransactionLineTenderCustomerValidation(request);


            Assert.IsTrue(response != null, "Failed to return a result from the resource layer.");
            Assert.IsTrue(response.ErrorCode == 14019, $"Expected error code 14019 was not equal to actual error code '{response.ErrorCode}'.  Denied text was '{response.DeniedText}'");
            Assert.IsTrue(response.DeniedText == "Transaction Retail Customer Entry Type not found", $"Expected denied text 'Transaction Retail Customer Entry Type not found' but received '{response.DeniedText}'");
        }



        [TestMethod]
        public void RetailTransactionLineTenderCustomerWrongCustomerGuid()
        {
            var request = new RetailTransactionLineTenderCustomerValidationRequest
            {
                PhysicalIdType = 0,
                CustomerEntryMethodType = 1,
                CustomerGuid = Guid.NewGuid().ToString("D")
            };

            var response = _databaseResource.RetailTransactionLineTenderCustomerValidation(request);


            Assert.IsTrue(response != null, "Failed to return a result from the resource layer.");
            Assert.IsTrue(response.ErrorCode == 9, $"Expected error code 9 was not equal to actual error code '{response.ErrorCode}'.  Denied text was '{response.DeniedText}'");
            Assert.IsTrue(response.DeniedText == "Customer Not Found", $"Expected denied text 'Customer Not Found' but received '{response.DeniedText}'");
        }

        #endregion
    }
}
