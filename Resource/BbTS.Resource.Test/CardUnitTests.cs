﻿using System;
using System.Configuration;
using BbTS.Core;
using BbTS.Core.Configuration;
using BbTS.Core.Security.Database;
using BbTS.Domain.Models.Credential;
using BbTS.Domain.Models.Credential.Mobile;
using BbTS.Domain.Models.Definitions.Notification;
using BbTS.Resource.Database.Abstract;
using BbTS.Resource.Service.Abstract;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BbTS.Resource.Test
{
    [TestClass]
    public class CardUnitTests
    {
        private Configuration _configuration;
        private ResourceDatabase _databaseResource;
        private ResourceServiceAbstract _serviceResource;

        public int CustId { get; set; } = 501;
        public string CardNumber { get; set; } = "600001";

        /// <summary>
        /// Test initialization.
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            string message;
            Assert.IsTrue(CustomConfiguration.LoadCustomConfiguration("App.config", out _configuration, out message), message);

            var dataSourceString = CustomConfiguration.GetKeyValueAsString("DataSource", _configuration);
            Guard.IsNotNullOrWhiteSpace(dataSourceString, "DataSource From Congfig");
            var dataSource = (DataSource)Enum.Parse(typeof(DataSource), dataSourceString);
            _databaseResource = ResourceManager.Instance.Resource(dataSource);
            _databaseResource.ConnectionString = DatabaseSecurityTool.CreateConnectionStringFromRegistry();

            var dataSourceServiceString = CustomConfiguration.GetKeyValueAsString("ServiceSource", _configuration);
            Guard.IsNotNullOrWhiteSpace(dataSourceServiceString, "DataSource From Congfig");
            var serviceSource = (ServiceSource)Enum.Parse(typeof(ServiceSource), dataSourceServiceString);
            _serviceResource = ResourceManager.Instance.ResourceService(serviceSource);
            
        }

        [TestMethod]
        [DeploymentItem("App.config")]
        public void MobileCredentailGetUnitTest()
        {
            var response = _databaseResource.MobileCredentialGet(CardNumber);

            Assert.IsTrue(response.Credential != null, "Credential was null");
            
        }

        [TestMethod]
        [DeploymentItem("App.config")]
        public void MobileCredentailSetUnitTest()
        {
            var cardnum = "88880000888800008888";
            var response = _databaseResource.MobileCredentialGet(cardnum);

            if (response != null)
            {
                Assert.Inconclusive("Unable to create a card becasue it already exists.");
            }

            var request = new MobileCredentialPostRequest
            {
                Credential = new CardCredentialComplete
                {
                    ActiveEndDate = DateTimeOffset.MaxValue,
                    ActiveStartDate = DateTimeOffset.MinValue,
                    CardIdm = "54321",
                    CardName = "Unit Test Created Card",
                    CardNumber = cardnum,
                    CardStatus = NotificationCardStatus.Active,
                    CardStatusDateTime = DateTimeOffset.Now,
                    CardStatusText = "Just made it.",
                    CardType = NotificationCardType.Standard,
                    CustomerGuid = new Guid("C7683AD4-D8BE-4DFD-B3B1-CEC139861F21"),
                    DomainId = Guid.NewGuid(),
                    IssueNumber = "",
                    IssuerId = 1,
                    LostFlag = false
                }
            };

            var setResponse = _databaseResource.MobileCredentialPost(request);
            Assert.IsTrue(setResponse.ErrorCode == 0, "Return code was not 0");

            var getResponse = _databaseResource.MobileCredentialGet(cardnum);
            Assert.IsTrue(getResponse.Credential != null, "Credential was null");

        }

        [TestMethod]
        [DeploymentItem("App.config")]
        public void MobileCredentailUpdateUnitTest()
        {
            var originalGetResponse = _databaseResource.MobileCredentialGet(CardNumber);
            Assert.IsTrue(originalGetResponse.Credential != null, "Credential was null");

            var originalCardName = originalGetResponse.Credential.CardName;
            originalGetResponse.Credential.CardName = originalCardName == "Unit Test Card Name A" ? "Unit Test Card Name B" : "Unit Test Card Name A";

            var checkValue = originalGetResponse.Credential.CardName;

            var updateResponse = _databaseResource.MobileCredentialPatch(new MobileCredentialPatchRequest { Credential = originalGetResponse.Credential });
            Assert.IsTrue(updateResponse.ErrorCode == 0, "Update failed.");

            var updateGetResponse = _databaseResource.MobileCredentialGet(CardNumber);
            Assert.IsTrue(updateGetResponse.Credential.CardName == checkValue, "Update credential was null");
        }
    }
}
