﻿using System;
using System.Configuration;
using BbTS.Core;
using BbTS.Core.Configuration;
using BbTS.Core.Security.Database;
using BbTS.Domain.Models.System.Database;
using BbTS.Resource.Database.Abstract;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BbTS.Resource.Test
{

    [TestClass]
    public class DatabaseUnitTests
    {
        private Configuration _configuration;
        private ResourceDatabase _databaseResource;

        /// <summary>
        /// Test initialization.
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            string message;
            Assert.IsTrue(CustomConfiguration.LoadCustomConfiguration("App.config", out _configuration, out message), message);

            var dataSourceString = CustomConfiguration.GetKeyValueAsString("DataSource", _configuration);
            Guard.IsNotNullOrWhiteSpace(dataSourceString, "DataSource From Congfig");
            var dataSource = (DataSource)Enum.Parse(typeof(DataSource), dataSourceString);
            _databaseResource = ResourceManager.Instance.Resource(dataSource);
            _databaseResource.ConnectionString = DatabaseSecurityTool.CreateConnectionStringFromRegistry();

            var dataSourceServiceString = CustomConfiguration.GetKeyValueAsString("ServiceSource", _configuration);
            Guard.IsNotNullOrWhiteSpace(dataSourceServiceString, "DataSource From Congfig");
            var serviceSource = (ServiceSource)Enum.Parse(typeof(ServiceSource), dataSourceServiceString);
            ResourceManager.Instance.ResourceService(serviceSource);
        }

        /// <summary>
        /// Unit test to verify that the SystemFeaturesGet function works properly in the resource layer.
        /// Test will verify that LicenseNumber, CampusName and SystemCurrencyValue are all populated.
        /// </summary>
        [TestMethod]
        [DeploymentItem("App.config")]
        public void Md5HashIndexUnitTest()
        {
            const string indexExpected = "<Index TableName='CASHIER' IndexName='UK_CASHIER_CUSTID' IndexType='Unique' Tablespace='TS_INDEX' Md5Hash='042136d7d7c49ce0f70116cc0ddde95c'><Column Name='CUST_ID' Expression='' Position='1' IsFunctionBased='false' /></Index>";

            var index = _databaseResource.IndexesGet(
                new ConnectionInfo {
                    ServerName = CustomConfiguration.GetKeyValueAsString("OracleDatabase", _configuration),
                    Password = "password1",
                    Port = 1521,
                    ServiceName = "CHICO",
                    User = "Envision"
                }, 
                "Envision", 
                "Uk_Cashier_CustId"
            );

            Assert.AreEqual(indexExpected, Core.Serialization.Xml.Serialize(index.Items[0],false).Replace("\"","'"));
        }
    }
}