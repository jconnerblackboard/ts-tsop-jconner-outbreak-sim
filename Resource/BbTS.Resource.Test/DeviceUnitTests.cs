﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using BbTS.Core;
using BbTS.Core.Configuration;
using BbTS.Core.Security.Database;
using BbTS.Domain.Models.BoardPlan;
using BbTS.Domain.Models.Definitions.Device;
using BbTS.Domain.Models.Device;
using BbTS.Resource.Database.Abstract;
using BbTS.Resource.Service.Abstract;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BbTS.Resource.Test
{
    [TestClass]
    public class DeviceUnitTests
    {
        private Configuration _configuration;
        private ResourceDatabase _databaseResource;
        private ResourceServiceAbstract _serviceResource;

        public string DeviceId { get; set; }

        /// <summary>
        /// Test initialization.
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            string message;
            Assert.IsTrue(CustomConfiguration.LoadCustomConfiguration("App.config", out _configuration, out message), message);

            var dataSourceString = CustomConfiguration.GetKeyValueAsString("DataSource", _configuration);
            Guard.IsNotNullOrWhiteSpace(dataSourceString, "DataSource From Congfig");
            var dataSource = (DataSource)Enum.Parse(typeof(DataSource), dataSourceString);
            _databaseResource = ResourceManager.Instance.Resource(dataSource);
            _databaseResource.ConnectionString = DatabaseSecurityTool.CreateConnectionStringFromRegistry();

            var dataSourceServiceString = CustomConfiguration.GetKeyValueAsString("ServiceSource", _configuration);
            Guard.IsNotNullOrWhiteSpace(dataSourceServiceString, "DataSource From Congfig");
            var serviceSource = (ServiceSource)Enum.Parse(typeof(ServiceSource), dataSourceServiceString);
            _serviceResource = ResourceManager.Instance.ResourceService(serviceSource);

            // Initialize test values from App.Config
            DeviceId = CustomConfiguration.GetKeyValueAsString("DeviceId", _configuration);
        }

        #region license server tests
        [TestMethod]
        [DeploymentItem("App.config")]
        public void IpReadersGetUnitTest()
        {
            Guard.IsNotNull(_databaseResource, "DatabaseResource");
            Guard.IsNotNullOrWhiteSpace(_databaseResource.ConnectionString, "ConnectionString");

            {
                var allReaders = _databaseResource.IpReadersGet();
                Assert.IsTrue(allReaders != null, "Failed to retrieve any results from the database for all IP reader types.");
            }
            {
                var saReaders = _databaseResource.IpReadersGet(
                    new List<HardwareModel>
                    {
                        HardwareModel.MasterControllerSA3004,
                        HardwareModel.MasterControllerSA3032,
                        HardwareModel.VR4100,
                        HardwareModel.CR4100,
                        HardwareModel.LC4100
                    });
                Assert.IsTrue(saReaders != null, "Failed to retrieve any results from the database for SA3004 and SA3032.");
                var subresults = saReaders.FindAll(reader => 
                    reader.HardwareModel != HardwareModel.MasterControllerSA3004 && 
                    reader.HardwareModel != HardwareModel.MasterControllerSA3032 &&
                    reader.HardwareModel != HardwareModel.VR4100 &&
                    reader.HardwareModel != HardwareModel.CR4100 &&
                    reader.HardwareModel != HardwareModel.LC4100 );
                Assert.IsTrue(subresults.Count == 0, "IpReadersGet failed to filter out results that weren't SA3004, SA3032, VR4100, CR4100, or LC4100");
            }
        }

        [TestMethod]
        [DeploymentItem("App.config")]
        public void DsrAccessPointsGetUnitTest()
        {
            Guard.IsNotNull(_databaseResource, "DatabaseResource");
            Guard.IsNotNullOrWhiteSpace(_databaseResource.ConnectionString, "ConnectionString");

            var accessPoints = _databaseResource.AccessPointsGet(null);

            Assert.IsTrue(accessPoints != null, "Failed to retrieve any results from the database.");

            if (accessPoints.Count == 0)
            {
                Assert.Inconclusive("Failed to retrieve any results from the database for active Assa Abloy reader types.  This may be because there are no active access points.");
            }
        }

        [TestMethod]
        [DeploymentItem("App.config")]
        public void AllegionLocksGetUnitTest()
        {
            Guard.IsNotNull(_databaseResource, "DatabaseResource");
            Guard.IsNotNullOrWhiteSpace(_databaseResource.ConnectionString, "ConnectionString");

            var locks = _databaseResource.AllegionLocksGet(null);

            Assert.IsTrue(locks != null, "Failed to retrieve any results from the database.");

            if (locks.Count == 0)
            {
                Assert.Inconclusive("Failed to retrieve any results from the database for active Allegion reader types.  This may be because there are no active readers.");
            }
        }

        [TestMethod]
        [DeploymentItem("App.config")]
        public void DeviceOperationalRequirementsGetUnitTest()
        {

            Guard.IsNotNull(_databaseResource, "DatabaseResource");
            Guard.IsNotNullOrWhiteSpace(_databaseResource.ConnectionString, "ConnectionString");

            var requirements = _databaseResource.DeviceOperationalRequirementsGet(DeviceId);

            Assert.IsTrue(requirements != null, "Failed to retrieve any results from the database.");
        }
        #endregion

        #region Slate unit tests
        [TestMethod]
        [DeploymentItem("App.config")]
        public void DeviceSettingsGetUnitTest()
        {

            Guard.IsNotNull(_databaseResource, "DatabaseResource");
            Guard.IsNotNullOrWhiteSpace(_databaseResource.ConnectionString, "ConnectionString");

            var settings = new DeviceSettings
            {
                StoredValueAccounts = _databaseResource.StoredValueAccountDeviceSettingsGet(DeviceId),
                TaxSchedules = _databaseResource.TaxScheduleDeviceSettingsGet(DeviceId),
                Tenders = _databaseResource.TenderDeviceSettingsGet(DeviceId),
                MealTypes = _databaseResource.BoardMealTypesArchivesGet(null).Select(item => new MealTypeDeviceSetting(item)).ToList(),
                Periods = _databaseResource.BoardPeriodsDeviceSettingsGet(DeviceId),
                Events = _databaseResource.EventDeviceSettingsGet(DeviceId),
                CardFormats = _databaseResource.CardFormatDeviceSettingsGet(DeviceId),
                OperationalRequirements = _databaseResource.DeviceOperationalRequirementsGet(DeviceId),
                Products = _databaseResource.ProductDeviceSettingsGet(DeviceId),
                Roles = _databaseResource.OperatorRolesForDeviceGet(DeviceId),
                Operators = _databaseResource.OperatorsDeviceSettingsGet(DeviceId)
            };

            List<string> nullItems = new List<string>();
            List<string> zeroCountItems = new List<string>();

            if (settings.StoredValueAccounts == null) nullItems.Add("StoredValueAccounts");
            if (settings.StoredValueAccounts != null && settings.StoredValueAccounts.Count == 0) zeroCountItems.Add("StoredValueAccounts");

            if (settings.TaxSchedules == null) nullItems.Add("TaxSchedules");
            if (settings.TaxSchedules != null && settings.TaxSchedules.Count == 0) zeroCountItems.Add("TaxSchedules");

            if (settings.Tenders == null) nullItems.Add("Tenders");
            if (settings.Tenders != null && settings.Tenders.Count == 0) zeroCountItems.Add("Tenders");

            if (settings.MealTypes == null) nullItems.Add("MealTypes");
            if (settings.MealTypes != null && settings.MealTypes.Count == 0) zeroCountItems.Add("MealTypes");

            if (settings.Periods == null) nullItems.Add("Periods");
            if (settings.Periods != null && settings.Periods.Count == 0) zeroCountItems.Add("Periods");

            if (settings.Events == null) nullItems.Add("Events");
            if (settings.Events != null && settings.Events.Count == 0) zeroCountItems.Add("Events");

            if (settings.CardFormats == null) nullItems.Add("CardFormats");
            if (settings.CardFormats != null && settings.CardFormats.Count == 0) zeroCountItems.Add("CardFormats");

            if (settings.OperationalRequirements == null) nullItems.Add("OperationalRequirements");
           
            if (settings.Products == null) nullItems.Add("Products");
            if (settings.Products != null && settings.Products.Count == 0) zeroCountItems.Add("Products");

            if (settings.Operators == null) nullItems.Add("Operators");
            if (settings.Operators != null && settings.Operators.Count == 0) zeroCountItems.Add("Operators");

            Assert.IsTrue(nullItems.Count == 0, $"The following items were null: {string.Join(", ", nullItems)}");
            Assert.IsTrue(zeroCountItems.Count == 0, $"The following collections didn't have any members: {string.Join(", ", zeroCountItems)}");
            
        }

        [TestMethod]
        public void DevicePropertiesGetSuccessCase()
        {
            var registration = _databaseResource.DevicePropertiesGet();

            Assert.IsTrue(registration != null, "Registration items were null.");

            if (registration.Count == 0)
            {
                Assert.Inconclusive("Registration items count was 0.");
            }

            Trace.WriteLine($"Registration items count was {registration.Count}");
            
        }

        [TestMethod]
        public void BoardCashEquivalencyPeriodGetSuccessCase()
        {
            var periods = new List<BoardCashEquivalencyPeriod>();
            var requestId = Guid.NewGuid().ToString("D");

            var defaultBoardPlanId = _databaseResource.DefaultBoardPlanIdFromOriginatorGuid(DeviceId);
            if (defaultBoardPlanId == null)
            {
                Assert.Inconclusive("Default board plan id was null.  Cannot test BoardCashEquivalencyPeriodsGet without that value present.");
            }

            for (int i = 1; i < 8; ++i)
            {
                var request = new BoardCashEquivalencyPeriodsGetRequest
                {
                    DeviceGuid = DeviceId,
                    DefaultBoardPlanId = defaultBoardPlanId.Value,
                    LastPeriodNum = i - 1,
                    PeriodNum = i,
                    RequestId = requestId
                };
                var response = _databaseResource.BoardCashEquivalencyPeriodsGet(request);
                periods.Add(response.BoardCashEquivalencyPeriod);
            }
            

        }

        #endregion
    }
}
