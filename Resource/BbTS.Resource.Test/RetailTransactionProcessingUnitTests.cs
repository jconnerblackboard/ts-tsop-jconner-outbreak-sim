﻿using System;
using System.Configuration;
using BbTS.Core;
using BbTS.Core.Configuration;
using BbTS.Core.Security.Database;
using BbTS.Domain.Models.Container;
using BbTS.Domain.Models.Transaction.Processing;
using BbTS.Resource.Database.Abstract;
using BbTS.Resource.Service.Abstract;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BbTS.Resource.Test
{
    [TestClass]
    public class RetailTransactionProcessingUnitTests
    {
        private Configuration _configuration;
        private ResourceDatabase _databaseResource;

        public string DeviceGuid { get; set; }
        public string OperatorGuid { get; set; }

        public RetailTransactionBeginProcessRequest RetailTransactionBeginRequest { get; set; }

        public RetailTransactionLineProductProcessRequest RetailTransactionLineProductProcessRequest { get; set; }

        public RetailTransactionLineProductPriceModifierRequest RetailTransactionLineProductPriceModifierRequest { get; set; }

        public RetailTransactionLineProductTaxRequest RetailTransactionLineProductTaxRequest { get; set; }

        public RetailTransactionLineProductTaxOverrideRequest RetailTransactionLineProductTaxOverrideRequest { get; set; }

        public RetailTransactionLineTenderSvCardProcessRequest RetailTransactionLineTenderSvCardProcessRequest { get; set; }

        public RetailTransactionLineTenderRequest RetailTransactionLineTenderRequest { get; set; }

        public RetailTransactionEndProcessRequest RetailTransactionEndRequest { get; set; }

        /// <summary>
        /// Test initialization.
        /// </summary>
        [TestInitialize]
        [DeploymentItem("App.config")]
        public void Initialize()
        {
            string message;
            Assert.IsTrue(CustomConfiguration.LoadCustomConfiguration("App.config", out _configuration, out message), message);

            var dataSourceString = CustomConfiguration.GetKeyValueAsString("DataSource", _configuration);
            Guard.IsNotNullOrWhiteSpace(dataSourceString, "DataSource From Congfig");
            var dataSource = (DataSource)Enum.Parse(typeof(DataSource), dataSourceString);
            _databaseResource = ResourceManager.Instance.Resource(dataSource);
            _databaseResource.ConnectionString = DatabaseSecurityTool.CreateConnectionStringFromRegistry();


            RetailTransactionLineProductProcessRequest = new RetailTransactionLineProductProcessRequest();

            RetailTransactionLineTenderSvCardProcessRequest = new RetailTransactionLineTenderSvCardProcessRequest();

            RetailTransactionLineTenderRequest = new RetailTransactionLineTenderRequest();

            RetailTransactionEndRequest = new RetailTransactionEndProcessRequest();

            RetailTransactionLineProductPriceModifierRequest = new RetailTransactionLineProductPriceModifierRequest();

            RetailTransactionLineProductTaxOverrideRequest = new RetailTransactionLineProductTaxOverrideRequest();

            RetailTransactionLineProductTaxRequest =  new RetailTransactionLineProductTaxRequest();

            RetailTransactionLineTenderRequest = new RetailTransactionLineTenderRequest();


            DeviceGuid = CustomConfiguration.GetKeyValueAsString("DeviceId", _configuration);
            OperatorGuid = CustomConfiguration.GetKeyValueAsString("OperatorIdHasCard", _configuration);
        }

        #region RetailTransactionBegin

        /// <summary>
        /// Populate the test request.
        /// </summary>
        private void _populateRetailTransactionBeginRequest()
        {
            RetailTransactionBeginRequest = new RetailTransactionBeginProcessRequest
            {
                DeviceGuid = DeviceGuid,
                TransactionNumber = 1,
                TransactionDateTime = DateTime.Now,
                CancelledFlag = false,
                SuspendedFlag = false,
                SuspendedUnsuspendDatetime = null,
                TrainingFlag = false,
                KeyedOfflineFlag = false,
                AttendedType = 1,//"0 - No Cashier Present, 1 - Cashier Present(see transaction_cashier)"
                RetailTranType = 0, //"0 - Sale, 1 - Return"
                OperatorGuid = OperatorGuid,
                CashdrawerNumber = 1,
                ControlTotalBeforeTransaction = 0,
                ControlTotalAfterTransaction = 1
            };
        }

        /// <summary>
        /// Test the successful case.
        /// </summary>
        [TestMethod]
        public void RetailTransactionBeginSuccessUnitTest()
        {
            _populateRetailTransactionBeginRequest();
            var response = _databaseResource.RetailTransactionBeginProcess(RetailTransactionBeginRequest);

            Assert.IsTrue(response.ErrorCode == 0, $"Error code was expected to be 0 and was instead {response.ErrorCode}");
        }

        /// <summary>
        /// Test the case of a duplicate transaction.
        /// </summary>
        [TestMethod]
        public void RetailTransactionBeginFailDuplicateTranUnitTest()
        {
            var response = _databaseResource.RetailTransactionBeginProcess(RetailTransactionBeginRequest);

            Assert.IsTrue(response.ErrorCode == 300, $"Error code was expected to be 300 and was instead {response.ErrorCode}");
        }
        #endregion

        #region RetailTransactionLineProduct

        [TestMethod]
        public void RetailTransactionLineProductProcessSuccessUnitTest()
        {
            var response = _databaseResource.RetailTransactionLineProductProcess(RetailTransactionLineProductProcessRequest);

            Assert.IsTrue(response.ErrorCode == 0, $"Error code was expected to be 0 and was instead {response.ErrorCode}");
        }

        #endregion

        #region RetailTransactionLineProductPriceModifier

        [TestMethod]
        public void RetailTransactionLineProductPriceModifierProcessSuccessUnitTest()
        {
            var response = _databaseResource.RetailTransactionLineProductPriceModifierProcess(RetailTransactionLineProductPriceModifierRequest);

            Assert.IsTrue(response.ErrorCode == 0, $"Error code was expected to be 0 and was instead {response.ErrorCode}");
        }

        #endregion

        #region RetailTransactionLineProductTax

        [TestMethod]
        public void RetailTransactionLineProductTaxSuccessUnitTest()
        {
            var response = _databaseResource.RetailTransactionLineProductTaxProcess(RetailTransactionLineProductTaxRequest);

            Assert.IsTrue(response.ErrorCode == 0, $"Error code was expected to be 0 and was instead {response.ErrorCode}");
        }

        #endregion

        #region RetailTransactionLineProductTaxOverride

        [TestMethod]
        public void RetailTransactionLineTaxOverrideProcessSuccessUnitTest()
        {
            var response = _databaseResource.RetailTransactionLineProductTaxOverrideProcess(RetailTransactionLineProductTaxOverrideRequest);

            Assert.IsTrue(response.ErrorCode == 0, $"Error code was expected to be 0 and was instead {response.ErrorCode}");
        }

        #endregion

        #region RetailTransactionLineTenderSvCard

        [TestMethod]
        public void RetailTransactionLineTenderSvCardProcessSuccessUnitTest()
        {
            var response = _databaseResource.RetailTransactionLineTenderSvCardProcess(RetailTransactionLineTenderSvCardProcessRequest);

            Assert.IsTrue(response.ErrorCode == 0, $"Error code was expected to be 0 and was instead {response.ErrorCode}");
        }

        #endregion

        #region RetailTransactionLineTender
        
        [TestMethod]
        public void RetailTransactionLineTenderProcessSuccessUnitTest()
        {
            var response = _databaseResource.RetailTransactionLineTenderProcess(RetailTransactionLineTenderRequest);

            Assert.IsTrue(response.ErrorCode == 0, $"Error code was expected to be 0 and was instead {response.ErrorCode}");
        }

        #endregion

        #region RetailTransactionEnd

        [TestMethod]
        public void RetailTransactionEndProcessSuccessUnitTest()
        {
            var response = _databaseResource.RetailTransactionEndProcess(RetailTransactionEndRequest);

            Assert.IsTrue(response.ErrorCode == 0, $"Error code was expected to be 0 and was instead {response.ErrorCode}");
        }

        #endregion

    }
}
