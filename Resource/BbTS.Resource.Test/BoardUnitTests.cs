﻿using System;
using System.Configuration;
using System.Diagnostics;
using BbTS.Core;
using BbTS.Core.Configuration;
using BbTS.Core.Security.Database;
using BbTS.Domain.Models.BoardPlan;
using BbTS.Domain.Models.Exceptions;
using BbTS.Resource.Database.Abstract;
using BbTS.Resource.Service.Abstract;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BbTS.Resource.Test
{
    [TestClass]
    public class BoardUnitTests
    {
        private Configuration _configuration;
        private ResourceDatabase _databaseResource;
        private ResourceServiceAbstract _serviceResource;

        public string DeviceId { get; set; }
        public string CustomerId { get; set; }

        /// <summary>
        /// Test initialization.
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            string message;
            Assert.IsTrue(CustomConfiguration.LoadCustomConfiguration("App.config", out _configuration, out message), message);

            var dataSourceString = CustomConfiguration.GetKeyValueAsString("DataSource", _configuration);
            Guard.IsNotNullOrWhiteSpace(dataSourceString, "DataSource From Congfig");
            var dataSource = (DataSource)Enum.Parse(typeof(DataSource), dataSourceString);
            _databaseResource = ResourceManager.Instance.Resource(dataSource);
            _databaseResource.ConnectionString = DatabaseSecurityTool.CreateConnectionStringFromRegistry();

            var dataSourceServiceString = CustomConfiguration.GetKeyValueAsString("ServiceSource", _configuration);
            Guard.IsNotNullOrWhiteSpace(dataSourceServiceString, "DataSource From Congfig");
            var serviceSource = (ServiceSource)Enum.Parse(typeof(ServiceSource), dataSourceServiceString);
            _serviceResource = ResourceManager.Instance.ResourceService(serviceSource);

            // Initialize test values from App.Config
            DeviceId = CustomConfiguration.GetKeyValueAsString("DeviceId", _configuration);
            CustomerId = CustomConfiguration.GetKeyValueAsString("CustomerId", _configuration);
        }

        [TestMethod]
        [DeploymentItem("App.config")]
        public void BoardInformationGet()
        {
            Guard.IsNotNull(_databaseResource, "DatabaseResource");
            Guard.IsNotNullOrWhiteSpace(_databaseResource.ConnectionString, "ConnectionString");

            var request = new BoardInformationGetRequest
            {
                RequestId = Guid.Empty.ToString("D"),
                MealTypeId = 1,
                BoardPlanId = null,
                CustomerGuid = CustomerId,
                ForcePost = false,
                IsGuestMeal = false,
                OriginatorGuid = DeviceId,
                TransactionDateTime = DateTime.Now
            };

            var information = _databaseResource.BoardInformationGet(request);

            Assert.IsTrue(information != null, "Failed to retrieve any results from the database.");
        }


        [TestMethod]
        [DeploymentItem("App.config")]
        public void BoardPlanVerifyUnitTest()
        {
            Guard.IsNotNull(_databaseResource, "DatabaseResource");
            Guard.IsNotNullOrWhiteSpace(_databaseResource.ConnectionString, "ConnectionString");

            var plan = _databaseResource.BoardPlanVerifyById(5);
            Assert.IsNotNull(plan);
            Assert.IsTrue(plan.Id > 0);

            try
            {
                var nullPlan = _databaseResource.BoardPlanVerifyById(-1);
                Assert.IsNull(nullPlan);
            }
            catch (BoardPlanVerifyFailedException bex)
            {
                Trace.WriteLine($"Caught the expected exception.  Message was {bex.Message}");
            }
            catch (Exception)
            {
                Assert.Fail("Failed to catch a BoardPlanVerifyFailedException on invalid data.");
            }
        }
    }
}
