﻿using System;
using System.Configuration;
using System.Linq;
using BbTS.Core;
using BbTS.Core.Configuration;
using BbTS.Core.Security.Database;
using BbTS.Domain.Models.Definitions.Operator;
using BbTS.Resource.Database.Abstract;
using BbTS.Resource.Service.Abstract;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BbTS.Resource.Test
{
    [TestClass]
    public class OperatorUnitTests
    {
        private Configuration _configuration;
        private ResourceDatabase _databaseResource;
        private ResourceServiceAbstract _serviceResource;

        public string DeviceId { get; set; }
        public string OperatorIdNoRoles { get; set; }
        public string OperatorIdAllRoles {get; set; }
        public string OperatorIdHasCard { get; set; }

        /// <summary>
        /// Test initialization.
        /// </summary>
        [TestInitialize]
        [DeploymentItem("App.config")]
        public void Initialize()
        {
            string message;
            Assert.IsTrue(CustomConfiguration.LoadCustomConfiguration("App.config", out _configuration, out message), message);

            var dataSourceString = CustomConfiguration.GetKeyValueAsString("DataSource", _configuration);
            Guard.IsNotNullOrWhiteSpace(dataSourceString, "DataSource From Congfig");
            var dataSource = (DataSource)Enum.Parse(typeof (DataSource), dataSourceString);
            _databaseResource = ResourceManager.Instance.Resource(dataSource);
            _databaseResource.ConnectionString = DatabaseSecurityTool.CreateConnectionStringFromRegistry();

            var dataSourceServiceString = CustomConfiguration.GetKeyValueAsString("ServiceSource", _configuration);
            Guard.IsNotNullOrWhiteSpace(dataSourceServiceString, "DataSource From Congfig");
            var serviceSource = (ServiceSource)Enum.Parse(typeof (ServiceSource), dataSourceServiceString);
            _serviceResource = ResourceManager.Instance.ResourceService(serviceSource);

            // Initialize test values from App.Config
            DeviceId = CustomConfiguration.GetKeyValueAsString("DeviceId", _configuration);
            OperatorIdNoRoles = CustomConfiguration.GetKeyValueAsString("OperatorIdNoRoles", _configuration);
            OperatorIdAllRoles = CustomConfiguration.GetKeyValueAsString("OperatorIdAllRoles", _configuration);
            OperatorIdHasCard = CustomConfiguration.GetKeyValueAsString("OperatorIdHasCard", _configuration);
        }

        [TestMethod]
        [DeploymentItem("App.config")]
        public void OperatorsGetUnitTest()
        {
            Guard.IsNotNull(_databaseResource, "DatabaseResource");
            Guard.IsNotNullOrWhiteSpace(_databaseResource.ConnectionString, "ConnectionString");

            var operators = _databaseResource.OperatorsDeviceSettingsGet(DeviceId);

            Assert.IsTrue(operators != null, "List of Operators was null");
            Assert.IsTrue(operators.Count == 0, "No items found in the list of Operators");
        }

        [TestMethod]
        [DeploymentItem("App.config")]
        public void OperatorRolesGetUnitTest()
        {
            Guard.IsNotNull(_databaseResource, "DatabaseResource");
            Guard.IsNotNullOrWhiteSpace(_databaseResource.ConnectionString, "ConnectionString");

            var roles = _databaseResource.OperatorRolesForDeviceGet(DeviceId);
            Assert.IsTrue(roles != null, "List of roles was null");
            Assert.IsTrue(roles.Count == 0, "No items found in the list of Roles");

        }

        [TestMethod]
        [DeploymentItem("App.config")]
        public void OperatorGetNoRolesAssignedUnitTest()
        {
            Guard.IsNotNull(_databaseResource, "DatabaseResource");
            Guard.IsNotNullOrWhiteSpace(_databaseResource.ConnectionString, "ConnectionString");

            var test = _databaseResource.OperatorByGuidGet(OperatorIdNoRoles);
            Assert.IsTrue(test != null, "Requested operator was not found");

            var operators = _databaseResource.OperatorsDeviceSettingsGet(DeviceId);
            Assert.IsTrue(operators.FirstOrDefault(x => x.OperatorId == test.OperatorId) == null, "The operator has at least one role and should have none.");

        }

        [TestMethod]
        [DeploymentItem("App.config")]
        public void OperatorGetAllRolesAssignedUnitTest()
        {
            Guard.IsNotNull(_databaseResource, "DatabaseResource");
            Guard.IsNotNullOrWhiteSpace(_databaseResource.ConnectionString, "ConnectionString");

            var test = _databaseResource.OperatorByGuidGet(OperatorIdAllRoles);
            Assert.IsTrue(test != null, "Requested operator was not found");

            var operators = _databaseResource.OperatorsDeviceSettingsGet(DeviceId);
            Assert.IsTrue(operators != null, $"Operators for device '{DeviceId}' was empty.");
            Assert.IsTrue(operators.FirstOrDefault(x => x.OperatorId == test.OperatorId) != null, $"The operator was not found on the device '{DeviceId}'.");

            var roles = _databaseResource.OperatorRolesForDeviceGet(DeviceId);
            Assert.IsTrue(roles != null, $"Roles for device '{DeviceId}' was empty.");

            var role = roles.FirstOrDefault(x => x.RoleId == test.RoleGroup);
            Assert.IsTrue(role != null, $"Role '{role}' was not found in the list of roles for device '{DeviceId}'.");

            var enumCount = Enum.GetNames(typeof(OperatorRight)).Length - 1;
            Assert.IsTrue(role.Rights.Count >= enumCount, $"Role '{role.RoleId}' for operator '{OperatorIdAllRoles}' did not have all roles assigned.  Count was {role.Rights.Count} expecting at least {enumCount}.");
        }

        [TestMethod]
        [DeploymentItem("App.config")]
        public void OperatorByGuidGetUnitTest()
        {
            Guard.IsNotNull(_databaseResource, "DatabaseResource");
            Guard.IsNotNullOrWhiteSpace(_databaseResource.ConnectionString, "ConnectionString");

            var test = _databaseResource.OperatorByGuidGet(OperatorIdHasCard);
            Assert.IsTrue(test != null, "Requested operator was not found");

            Assert.IsTrue(!string.IsNullOrWhiteSpace(test.CardNumber), "Operator was found but their card number was null or empty.  Test is not valid unless the operator has a card number.");
        }

    }
}
