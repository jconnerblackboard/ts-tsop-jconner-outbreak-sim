﻿using System;
using System.Configuration;
using System.Diagnostics;
using BbTS.Core;
using BbTS.Core.Configuration;
using BbTS.Core.Security.Database;
using BbTS.Core.Serialization;
using BbTS.Domain.Models.Customer;
using BbTS.Resource.Database.Abstract;
using BbTS.Resource.Service.Abstract;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BbTS.Resource.Test
{
    [TestClass]
    public class CustomerUnitTests
    {
        private Configuration _configuration;
        private ResourceDatabase _databaseResource;
        private ResourceServiceAbstract _serviceResource;

        public string DeviceId { get; set; }
        public string CustomerId { get; set; }

        /// <summary>
        /// Test initialization.
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            string message;
            Assert.IsTrue(CustomConfiguration.LoadCustomConfiguration("App.config", out _configuration, out message), message);

            var dataSourceString = CustomConfiguration.GetKeyValueAsString("DataSource", _configuration);
            Guard.IsNotNullOrWhiteSpace(dataSourceString, "DataSource From Congfig");
            var dataSource = (DataSource)Enum.Parse(typeof(DataSource), dataSourceString);
            _databaseResource = ResourceManager.Instance.Resource(dataSource);
            _databaseResource.ConnectionString = DatabaseSecurityTool.CreateConnectionStringFromRegistry();

            var dataSourceServiceString = CustomConfiguration.GetKeyValueAsString("ServiceSource", _configuration);
            Guard.IsNotNullOrWhiteSpace(dataSourceServiceString, "DataSource From Congfig");
            var serviceSource = (ServiceSource)Enum.Parse(typeof(ServiceSource), dataSourceServiceString);
            _serviceResource = ResourceManager.Instance.ResourceService(serviceSource);

            // Initialize test values from App.Config
            DeviceId = CustomConfiguration.GetKeyValueAsString("DeviceId", _configuration);
            CustomerId = CustomConfiguration.GetKeyValueAsString("CustomerId", _configuration);
        }

        [TestMethod]
        [DeploymentItem("App.config")]
        public void CustomerBoardBalanceGetUnitTest()
        {
            Guard.IsNotNull(_databaseResource, "DatabaseResource");
            Guard.IsNotNullOrWhiteSpace(_databaseResource.ConnectionString, "ConnectionString");

            var boardPlans = _databaseResource.CustomerBoardBalanceGet(CustomerId, DeviceId);

            Assert.IsTrue(boardPlans != null, "Failed to retrieve any results from the database.");

            if (boardPlans.Count == 0)
            {
                Assert.Inconclusive(
                    $"Failed to retrieve any results from the database for customer '{CustomerId}' on device '{DeviceId}'.  " +
                    $"This may be because there are no board plans assigned to the customer.");
            }
        }

        [TestMethod]
        [DeploymentItem("App.config")]
        public void CustomerStoredValueBalanceGetUnitTest()
        {
            Guard.IsNotNull(_databaseResource, "DatabaseResource");
            Guard.IsNotNullOrWhiteSpace(_databaseResource.ConnectionString, "ConnectionString");

            var accounts = _databaseResource.CustomerStoredValueBalanceGet(CustomerId, DeviceId);

            Assert.IsTrue(accounts != null, "Failed to retrieve any results from the database.");

            if (accounts.Count == 0)
            {
                Assert.Inconclusive(
                    $"Failed to retrieve any results from the database for customer '{CustomerId}' on device '{DeviceId}'.  " +
                    $"This may be because there are no stored value accounts assigned to the customer.");
            }
        }

        [TestMethod]
        public void ProcessCustomerRegistrationUnitTest()
        {
            Guard.IsNotNull(_databaseResource, "DatabaseResource");
            Guard.IsNotNullOrWhiteSpace(_databaseResource.ConnectionString, "ConnectionString");

            var request = new ProcessCustomerRegistrationPostRequest
            {
                ClientId = 1,
                Custnum = "0000000000000000600003",
                Cardnum = "0000000000008999920503",
                Emailaddress = "tb600003@blackboard.com",
                Lastname = "tblastname600003",
                Firstname = "tbfirstname600003",
                Middlename = "tbmiddlename600003"
            };

            var response = _databaseResource.ProcessCustomerRegistration(request);

            Assert.IsTrue(response != null, "Failed to retrieve any results from the database.");

            var serializerSettings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto,
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                DateTimeZoneHandling = DateTimeZoneHandling.Local
            };
            serializerSettings.Converters.Add(new StringEnumConverter());

            Trace.WriteLine(NewtonsoftJson.Serialize(response, serializerSettings));

        }

        [TestMethod]
        public void ProcessSuccessfulLoginUnitTest()
        {
            Guard.IsNotNull(_databaseResource, "DatabaseResource");
            Guard.IsNotNullOrWhiteSpace(_databaseResource.ConnectionString, "ConnectionString");


            var username = "christian";
            var isSuperUser = false;
            var isAdmin = false;
            var program = "Blackboard Unit Test Cases Long Format Blah Blah Blah";
            var workstationId = 0;

             _databaseResource.ProcessSuccessfulLogin(username, isSuperUser, isAdmin, program, workstationId);
        }

        [TestMethod]
        public void CustomerGetByCustomerNumberUnitTest()
        {
            Guard.IsNotNull(_databaseResource, "DatabaseResource");
            Guard.IsNotNullOrWhiteSpace(_databaseResource.ConnectionString, "ConnectionString");

            var result = _databaseResource.CustomerGetByCustomerNumber("100001");
            Assert.IsNotNull(result);
            Assert.IsTrue(result.CustomerNumber == "100001");
        }
    }
}
