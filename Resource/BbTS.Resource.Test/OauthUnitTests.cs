﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using BbTS.Core;
using BbTS.Core.Configuration;
using BbTS.Core.Conversion;
using BbTS.Core.Security.Database;
using BbTS.Core.Security.Oauth;
using BbTS.Domain.Models.Configuration;
using BbTS.Domain.Models.Definitions.Device;
using BbTS.Domain.Models.Definitions.Security.Oauth;
using BbTS.Domain.Models.Definitions.Service;
using BbTS.Domain.Models.Device;
using BbTS.Domain.Models.General;
using BbTS.Domain.Models.Service.Request;
using BbTS.Domain.Models.System.Security.Oauth;
using BbTS.Resource.Database.Abstract;
using BbTS.Resource.Service.Abstract;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BbTS.Resource.Test
{
    [TestClass]
    public class OauthUnitTests
    {
        private CustomConfigurationModel _configuration;
        private ResourceDatabase _databaseResource;
        private ResourceServiceAbstract _serviceResource;

        /// <summary>
        /// Test initialization.
        /// </summary>
        [TestInitialize]
        [DeploymentItem("App.config")]
        public void Initialize()
        {
            string message;
            Configuration configuration;
            Assert.IsTrue(CustomConfiguration.LoadCustomConfiguration("App.config", out configuration, out message), message);

            _configuration = CustomConfigurationModel.GenerateCustomConfigurationModel(configuration);

            var dataSourceString = CustomConfiguration.GetKeyValueAsString("DataSource", _configuration);
            Guard.IsNotNullOrWhiteSpace(dataSourceString, "DataSource From Config");
            var dataSource = (DataSource)Enum.Parse(typeof(DataSource), dataSourceString);

            _databaseResource = ResourceManager.Instance.Resource(dataSource);
            _databaseResource.ConnectionString = DatabaseSecurityTool.CreateConnectionStringFromRegistry();

            var dataSourceServiceString = CustomConfiguration.GetKeyValueAsString("ServiceSource", _configuration);
            Guard.IsNotNullOrWhiteSpace(dataSourceServiceString, "DataSource From Config");
            var serviceSource = (ServiceSource)Enum.Parse(typeof(ServiceSource), dataSourceServiceString);
            _serviceResource = ResourceManager.Instance.ResourceService(serviceSource);
        }

        [TestMethod]
        public void DateTimeinQueryParameterSignatureTest()
        {
            var token = "36a5839a-5545-41c9-88f7-73af127e409d";
            var secret = "VdDHAshN3MhWGFew9XAd";
            var tokenSecret = "ys5jCcGXS4EJ6J82HbUK";
            var oauthParameters = new OAuthParameters
            {
                ConsumerKey = "1E00F0B3-FF79-4103-89C7-253CCA214403",
                Nonce = "RnBkwL",
                Timestamp = "1505775954",
                Token = token,
                CallbackUri = null,
                Version = "1.0",
                SignatureMethod = "HMAC-SHA1"
            };

            var request = new ServiceStackRequestSurrogate
            {
                AbsoluteUri = "https://jconnerw10.transactpd.net/transact/api/board/device/F9ADCC29-098C-4027-8F57-D7499E643EBA/customer/8C6A72CD-0A1C-46D1-A0E6-2F12EF13952F/information?IsGuestMeal=false&MealTypeId=1&TransactionDateTime=2017-09-18T22:52:33.887Z",
                ContentType = "application/json",
                FormData = null,
                PathInfo = "/board/device/{OriginatorGuid}/customer/{CustomerGuid}/information",
                QueryString = new NameValueCollection
                {
                    { "IsGuestMeal", "false" },
                    { "MealTypeId", "1" },
                    { "TransactionDateTime", "2017-09-17T22:52:33.887Z" }
                },
                Verb = "GET"
            };
            var generatedSignature = OAuthFunctions.GenerateSignature(secret, tokenSecret, oauthParameters, request, null);
            Trace.WriteLine(generatedSignature);
        }

        [TestMethod]
        public void AuthorizationWhiteSpaceInHeaderUnitTest()
        {
            var securityMode = "Transport";
            var preamble = securityMode == "Transport" ? @"https://" : @"http://";
            var baseUri = RegistryConfigurationTool.Instance.ValueGet("ApplicationServerHostAddress");

            var oauthParameters = new OAuthParameters
            {
                ConsumerKey = "0C014D5C-E5C6-437B-8C9A-19299871DDA7",
                ConsumerSecret = "N5RgocJQfSLTOsA5hOHZRbslUM5gMobN+8BnqHVEuVRLFZf63RVUbdaFnkpKShpT",
                Nonce = "RnBkwL",
                Timestamp = "1505775954",
                CallbackUri = null,
                Version = "1.0",
                SignatureMethod = "HMAC-SHA1",
                BaseUri = !baseUri.StartsWith(preamble) ? preamble + baseUri : baseUri
            };

            oauthParameters.BaseUri = $"{oauthParameters.BaseUri}/transact/api";

            var oauthSignatureParameters = OAuthFunctions.CreateOAuthSignatureParameters(
                    string.Empty,
                    string.Empty,
                    SupportedHttpRequestMethod.POST.ToString(),
                    UriUtilityTool.FormatBaseAndRouteIntoUri(oauthParameters.BaseUri, oauthParameters.RequestRoute),
                    RequestHeaderSupportedContentType.Json.ToString(),
                    oauthParameters.ConsumerKey,
                    oauthParameters.ConsumerSecret);
            var authorizationHeader = OAuthFunctions.BuildAuthorizationHeader(oauthSignatureParameters);

            authorizationHeader = authorizationHeader.Replace(",", "  ,    ");
            var request = new HttpRestServiceRequest<string>
            {
                AcceptType = RequestHeaderSupportedContentType.Json,
                BaseUri = oauthParameters.BaseUri,
                Body = string.Empty,
                ContentType = RequestHeaderSupportedContentType.Json,
                CustomHeaders = new List<StringPair> { new StringPair("Authorization", authorizationHeader) },
                Method = SupportedHttpRequestMethod.POST,
                Route = oauthParameters.RequestRoute
            };

            var response = _serviceResource.RestRequest(request);
            var oauthValues = OAuthFunctions.ExtractOauthReturnParameters(response.Response.Content);

            Assert.IsTrue(!string.IsNullOrWhiteSpace(oauthValues[OAuthConstants.OAuthToken]));
            Assert.IsTrue(!string.IsNullOrWhiteSpace(oauthValues[OAuthConstants.OAuthTokenSecret]));
        }
    }
}
