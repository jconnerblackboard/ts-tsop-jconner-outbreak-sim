﻿using System;
using System.Configuration;
using BbTS.Core;
using BbTS.Core.Configuration;
using BbTS.Core.Security.Database;
using BbTS.Domain.Models.Definitions;
using BbTS.Domain.Models.Definitions.Licensing;
using BbTS.Domain.Models.System.Licensing;
using BbTS.Resource.Database.Abstract;
using BbTS.Resource.Service.Abstract;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BbTS.Resource.Test.System
{
    [TestClass]
    public class SystemUnitTests
    {
        private Configuration _configuration;
        private ResourceDatabase _databaseResource;
        private ResourceServiceAbstract _serviceResource;

        /// <summary>
        /// Test initialization.
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            string message;
            Assert.IsTrue(CustomConfiguration.LoadCustomConfiguration("App.config", out _configuration, out message), message);

            var dataSourceString = CustomConfiguration.GetKeyValueAsString("DataSource", _configuration);
            Guard.IsNotNullOrWhiteSpace(dataSourceString, "DataSource From Congfig");
            var dataSource = (DataSource)Enum.Parse(typeof(DataSource), dataSourceString);
            _databaseResource = ResourceManager.Instance.Resource(dataSource);
            _databaseResource.ConnectionString = DatabaseSecurityTool.CreateConnectionStringFromRegistry();

            var dataSourceServiceString = CustomConfiguration.GetKeyValueAsString("ServiceSource", _configuration);
            Guard.IsNotNullOrWhiteSpace(dataSourceServiceString, "DataSource From Congfig");
            var serviceSource = (ServiceSource)Enum.Parse(typeof(ServiceSource), dataSourceServiceString);
            _serviceResource = ResourceManager.Instance.ResourceService(serviceSource);
        }

        /// <summary>
        /// Unit test to verify that the SystemFeaturesGet function works properly in the resource layer.
        /// Test will verify that LicenseNumber, CampusName and SystemCurrencyValue are all populated.
        /// </summary>
        [TestMethod]
        [DeploymentItem("App.config")]
        public void SystemFeaturesGetUnitTest()
        {
            Guard.IsNotNull(_databaseResource, "DatabaseResource");
            Guard.IsNotNullOrWhiteSpace(_databaseResource.ConnectionString, "ConnectionString");

            var features = _databaseResource.SystemFeaturesGet();

            Guard.IsNotNullOrWhiteSpace(features.LicenseNumber, "LicenseNumber");
            Guard.IsNotNullOrWhiteSpace(features.CampusName, "CampusName");
            Guard.IsNotNullOrWhiteSpace(features.SystemCurrencyValue, "SystemCurrencyValue");
        }

        /// <summary>
        /// Unit test to verify ControlParameterValueGet works in the resource layer.  Test will query for USD and expect 10 in response.
        /// </summary>
        [TestMethod]
        [DeploymentItem("App.config")]
        public void ControlParameterValueGetUnitTest()
        {
            Guard.IsNotNull(_databaseResource, "DatabaseResource");
            Guard.IsNotNullOrWhiteSpace(_databaseResource.ConnectionString, "ConnectionString");

            string value = _databaseResource.ControlParameterValueGet("USD");
            Assert.IsTrue(value == "10");
        }

        /// <summary>
        /// Get the transaction system from the data resource layer (Oracle)
        /// </summary>
        [TestMethod]
        [DeploymentItem("App.config")]
        public void TransactionSystemGetFromDatabaseUnitTest()
        {
            Guard.IsNotNull(_databaseResource, "DatabaseResource");
            Guard.IsNotNullOrWhiteSpace(_databaseResource.ConnectionString, "ConnectionString");

            var transactionSystem = _databaseResource.TransactionSystemGet();
            Assert.IsNotNull(transactionSystem, "A valid TransactionSystem was not found in the database resource layer.");
        }



        /// <summary>
        /// Test writing an activity log to the database resource layer.
        /// </summary>
        [TestMethod]
        [DeploymentItem("App.config")]
        public void ActivityLogSetUnitTest()
        {
            Guard.IsNotNull(_databaseResource, "DatabaseResource");
            Guard.IsNotNullOrWhiteSpace(_databaseResource.ConnectionString, "ConnectionString");

            var sampleResponse = new UploadPendingDeviceResponse(
                new UploadPendingDeviceRequest
                {
                    DeviceInformation = new LicenseServerObject()
                    {
                        CustomerKey = "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF",
                        CustomerSecondKey = "SystemUnitTests",
                        DeviceName = "TestDevice",
                        DeviceSubType = LicenseServerDeviceSubType.Unknown,
                        DeviceType = LicenseServerDeviceType.Unknown,
                        LicensingServerIpAddress = "localhost",
                        RequesterName = "UnitTests",
                        RequesterContact = "unit.tests@here.now",
                        SerialNumberOrDeviceId = "AA-BB-CC-DD-EE-FF"
                    }
                })
            {
                Successful = true,
                Message = "We were successful."
            };

            _databaseResource.ActivityLogSet(sampleResponse.Id.ToString("D"), ActivityLogOriginEntity.IntegrationMonitorOnPremises, sampleResponse);
        }
    }
}
