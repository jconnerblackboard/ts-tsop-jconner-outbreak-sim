﻿using System;
using System.Collections.Generic;
using BbTS.Core.Logging.Tracing;
using BbTS.Resource.Database.Abstract;
using BbTS.Resource.Database.Concrete.Oracle;
using BbTS.Resource.Database.Concrete.Proxy;
using BbTS.Resource.Service.Abstract;
using BbTS.Resource.Service.Concrete.ProxyServiceResource;
using BbTS.Resource.Service.Concrete.WebServiceResource;

namespace BbTS.Resource
{
    /// <summary>
    /// Manager class for retrieving Resource layer objects.
    /// </summary>
    public class ResourceManager
    {
        private readonly Dictionary<DataSource, ResourceDatabase> _resources;
        private readonly Dictionary<ServiceSource, ResourceServiceAbstract> _serviceResources;

        /// <summary>
        /// Empty constructor.  Initialized internal resources.
        /// </summary>
        private ResourceManager()
        {
            _resources = new Dictionary<DataSource, ResourceDatabase>
            {
                { DataSource.Oracle, new OracleSource() },
                { DataSource.Proxy, new ProxySource() }
            };

            _serviceResources = new Dictionary<ServiceSource, ResourceServiceAbstract>
            {
                { ServiceSource.Service, GenerateServiceResource(ServiceSource.Service) },
                { ServiceSource.Proxy, GenerateServiceResource(ServiceSource.Proxy) }
            };
        }

        /// <summary>
        /// Singleton instance
        /// </summary>
        public static ResourceManager Instance { get; internal set; } = new ResourceManager();

        /// <summary>
        /// Generate a service resource base on the source type.
        /// </summary>
        /// <param name="source">Type of service resource to generate.</param>
        /// <returns>Service source.</returns>
        public static ResourceServiceAbstract GenerateServiceResource(ServiceSource source)
        {
            switch (source)
            {
                case ServiceSource.Proxy:
                    return new ProxyServiceResource();
                case ServiceSource.Service:
                    return new WebServiceResource();
                default:
                    throw new ArgumentException($"Invalid ServiceSource type {source}");
            }
        }

        /// <summary>
        /// Retrieve a database resource.
        /// </summary>
        /// <param name="source"><see cref="DataSource"/> type to retrieve.</param>
        /// <returns><see cref="ResourceDatabase"/> object containing the requested resource.</returns>
        public ResourceDatabase Resource(DataSource source)
        {
            if (!_resources.ContainsKey(source))
                throw new ArgumentException($"Missing database resource: {source}");

            return _resources[source];
        }

        /// <summary>
        /// Retrieve a service resource.
        /// </summary>
        /// <param name="source"><see cref="ServiceSource"/> type to retrieve</param>
        /// <returns><see cref="ResourceServiceAbstract"/> object containing the requested resource.</returns>
        public ResourceServiceAbstract ResourceService(ServiceSource source)
        {
            if (!_serviceResources.ContainsKey(source))
                throw new ArgumentException($"Missing service resource: {source}");

            return _serviceResources[source];
        }

        /// <summary>
        /// Set a resource
        /// </summary>
        /// <param name="source"></param>
        /// <param name="resource"></param>
        public void ResourceDatabaseSet(DataSource source, ResourceDatabase resource)
        {
            if (_resources.ContainsKey(source))
            {
                _resources[source] = resource;
            }
            else
            {
                _resources.Add(source, resource);
            }
        }
    }

    /// <summary>
    /// Enumerations of database resource types.
    /// </summary>
    public enum DataSource
    {
        Oracle,
        Proxy
    }

    /// <summary>
    /// Enumeration of service resource types.
    /// </summary>
    public enum ServiceSource
    {
        Service,
        Proxy
    }
}
