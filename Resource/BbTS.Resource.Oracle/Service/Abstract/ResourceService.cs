﻿using System.Collections.Generic;
using BbTS.Domain.Models.General;
using BbTS.Domain.Models.Service.Request;
using BbTS.Domain.Models.Service.Response;
using BbTS.Domain.Models.System;
using BbTS.Domain.Models.System.Licensing;
using BbTS.Domain.Models.System.Security.Oauth;
using BbTS.Domain.Models.Institution;
using System;
using BbTS.Domain.Models.Definitions.Door;
using BbTS.Domain.Models.Notification;

namespace BbTS.Resource.Service.Abstract
{
    /// <summary>
    /// Abstract class definitions for the service resource layer.
    /// </summary>
    public abstract class ResourceServiceAbstract
    {
        #region BbSP

        /// <summary>
        /// Gets the Application Credentials by Institution Id.
        /// </summary>
        /// <param name="institutionId">Institution Id.</param>
        /// <param name="registrationServerBaseUri">The base URI for the registration server (most likely the BaseURI of the BbIS server)</param>
        /// <param name="authenticationServierUri">The base URI of the (probably OAuth) authentication server.  If left null the authorization header will not be populated.</param>
        /// <returns>The requested object or null</returns>
        public abstract List<SpApplication> ApplicationCredentialsGet(Guid institutionId, string registrationServerBaseUri, string authenticationServierUri = null);

        /// <summary>
        /// Gets an Institution by Institution Id.
        /// </summary>
        /// <param name="institutionId">Institution Id.</param>
        /// <param name="registrationServerBaseUri">The base URI for the registration server (most likely the BaseURI of the BbIS server)</param>
        /// <param name="authenticationServierUri">The base URI of the (probably OAuth) authentication server.  If left null the authorization header will not be populated.</param>
        /// <returns>The requested object or null</returns>
        public abstract SpInstitution InstitutionGet(Guid institutionId, string registrationServerBaseUri, string authenticationServierUri = null);

        /// <summary>
        /// Gets the Merchants by Institution Id.
        /// </summary>
        /// <param name="institutionId">Institution Id.</param>
        /// <param name="registrationServerBaseUri">The base URI for the registration server (most likely the BaseURI of the BbIS server)</param>
        /// <param name="authenticationServierUri">The base URI of the (probably OAuth) authentication server.  If left null the authorization header will not be populated.</param>
        /// <returns>The requested objects or null</returns>
        public abstract List<SpMerchant> MerchantsGet(Guid institutionId, string registrationServerBaseUri, string authenticationServierUri = null);

        /// <summary>
        /// Create a transaction system.
        /// </summary>
        /// <param name="systemFeatures"><see cref="SystemFeatures"/> object containing the instance to create.</param>
        /// <param name="registrationServerBaseUri">The base URI for the registration server (most likely the BaseURI of the BbIS server)</param>
        /// <param name="authenticationServierUri">The base URI of the (probably OAuth) authentication server.  If left null the authorization header will not be populated.</param>
        /// <returns><see cref="TransactionSystem"/> object after creation.  InstitutionId will be populated.</returns>
        public abstract TransactionSystem TransactionSystemCreate(SystemFeatures systemFeatures, string registrationServerBaseUri, string authenticationServierUri = null);

        /// <summary>
        /// Update a transaction system
        /// </summary>
        /// <param name="systemFeatures"><see cref="SystemFeatures"/> object containing the instance to update.</param>
        /// <param name="transactionSystemId">The id of the currently recorded transaction system in the database resource layer.</param>
        /// <param name="registrationServerBaseUri">The base URI for the registration server (most likely the BaseURI of the BbIS server)</param>
        /// <param name="authenticationServierUri">The base URI of the (probably OAuth) authentication server.  If left null the authorization header will not be populated.</param>
        /// <returns><see cref="TransactionSystem"/> object after update.</returns>
        public abstract TransactionSystem TransactionSystemUpdate(SystemFeatures systemFeatures, string transactionSystemId, string registrationServerBaseUri, string authenticationServierUri = null);

        /// <summary>
        /// Get a transaction system from an external resource.
        /// </summary>
        /// <param name="instanceId">Instance Id.</param>
        /// <param name="registrationServerBaseUri">The base URI for the registration server (most likely the BaseURI of the BbIS server)</param>
        /// <param name="authenticationServierUri">The base URI of the (probably OAuth) authentication server.  If left null the authorization header will not be populated.</param>
        /// <returns><see cref="TransactionSystem"/> object after retrieval.</returns>
        public abstract List<TransactionSystem> TransactionSystemGet(string instanceId, string registrationServerBaseUri, string authenticationServierUri = null);

        /// <summary>
        /// Delete a transaction system from an external resource.
        /// </summary>
        /// <param name="systemFeatures"><see cref="SystemFeatures"/> object containing identifiers for the transaction system to fetch.</param>
        /// <param name="registrationServerBaseUri">The base URI for the registration server (most likely the BaseURI of the BbIS server)</param>
        /// <param name="authenticationServierUri">The base URI of the (probably OAuth) authentication server.  If left null the authorization header will not be populated.</param>
        public abstract void TransactionSystemDelete(SystemFeatures systemFeatures, string registrationServerBaseUri, string authenticationServierUri = null);

        #endregion

        #region EAccounts System Portal Functions

        /// <summary>
        /// Get institution information from the eaccounts portal.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The result of the operation.</returns>
        public abstract InstitutionInformationGetResponse EaccountsInstitutionInformationGet(InstitutionInformationGetRequest request);

        #endregion

        #region Common Functions

        #endregion

        #region BbHost Functions
        
        /// <summary>
        /// Send a message to the BbHost.
        /// </summary>
        /// <param name="message">Message to send</param>
        /// <param name="ipAddress">Ip address of the host</param>
        /// <param name="port">listening port.  Default to 1312.</param>
        /// <returns>Response from the host.</returns>
        public abstract byte[] SendHostRequest(byte[] message, string ipAddress, int port = 1321 );

        #endregion

        #region Dsr Functions

        /// <summary>
        /// Sets assa abloy door state override
        /// </summary>
        /// <param name="doorGuid">Institution Id.</param>
        /// <param name="state">Door state override</param>
        /// <param name="sessionToken">Session token</param>
        /// <param name="dsrServiceBaseUri">The base URI for the DSR server</param>
        /// <returns>Result token</returns>
        public abstract ActionResultToken DsrDoorStateOverrideSet(Guid doorGuid, DoorAaStateChangeStateOverrideValues state, string sessionToken, string dsrServiceBaseUri);

        #endregion

        #region OauthServiceResource abstracts

        /// <summary>
        /// Acquire an oauth token from an external service.
        /// </summary>
        /// <param name="parameters">
        ///     <see cref="OAuthParameters"/> object containing necessary OAuth parameters.  
        ///     Object will be back filled with acquired parameters.
        /// </param>
        /// <returns><see cref="OAuthParameters"/> object will be back filled with acquired parameters.</returns>
        public abstract OAuthParameters AcquireOauth10AToken(OAuthParameters parameters);

        /// <summary>
        /// Create an HTTP REST style request and secure it with OAuth.
        /// </summary>
        /// <typeparam name="T">Type of the object that will be imbedded into the request body.</typeparam>
        /// <param name="request"><see cref="HttpRestServiceRequest{T}"/> containing the elements of the request.</param>
        /// <param name="oauthServerBaseUri">The base uri for the OAuth authentication server.</param>
        /// <returns><see cref="HttpRestServiceResponse{T}"/> with the response from the service</returns>
        public abstract HttpRestServiceResponse<T> OauthSecuredRestRequest<T>(HttpRestServiceRequest<T> request, string oauthServerBaseUri);

        /// <summary>
        /// Create an HTTP REST style request and secure it with OAuth.
        /// </summary>
        /// <typeparam name="T">Type of the object that will be imbedded into the request body.</typeparam>
        /// <param name="request"><see cref="HttpRestServiceRequest{T}"/> containing the elements of the request.</param>
        /// <param name="oauthServerBaseUri">The base uri for the OAuth authentication server.</param>
        /// <param name="parameters">OAuth credentials to use.</param>
        /// <returns><see cref="HttpRestServiceResponse{T}"/> with the response from the service</returns>
        public abstract HttpRestServiceResponse<T> OauthSecuredRestRequest<T>(
            HttpRestServiceRequest<T> request,
            string oauthServerBaseUri,
            OAuthParameters parameters);

        /// <summary>
        /// Create an HTTP REST style request and secure it with OAuth.
        /// </summary>
        /// <typeparam name="T">Type of the object that will be imbedded into the request body.</typeparam>
        /// <param name="request"><see cref="HttpRestServiceRequest{T}"/> containing the elements of the request.</param>
        /// <param name="oauthServerBaseUri">The base uri for the OAuth authentication server.</param>
        /// <param name="parameters">OAuth credentials to use.</param>
        /// <param name="customHeaders">Additional headers to imbed in the request.</param>
        /// <returns><see cref="HttpRestServiceResponse{T}"/> with the response from the service</returns>
        public abstract HttpRestServiceResponse<T> OauthSecuredRestRequest<T>(
            HttpRestServiceRequest<T> request,
            string oauthServerBaseUri,
            OAuthParameters parameters,
            List<StringPair> customHeaders);

        #endregion

        #region SequoiaResource abstracts

        /// <summary>
        /// Upload information about a pending device to the licensing server.
        /// </summary>
        /// <param name="request">Information about the device and the request itself.</param>
        /// <returns>The results of the request operation.</returns>
        public abstract UploadPendingDeviceResponse UploadPendingDevice(UploadPendingDeviceRequest request);

        #endregion

        #region WebServiceResource abstracts

        /// <summary>
        /// Create an HTTP REST style request.
        /// </summary>
        /// <typeparam name="T">Type of the object that will be imbedded into the request body.</typeparam>
        /// <param name="request"><see cref="HttpRestServiceRequest{T}"/> containing the elements of the request.</param>
        /// <returns><see cref="HttpRestServiceResponse{T}"/> with the response from the service</returns>
        public abstract HttpRestServiceResponse<T> RestRequest<T>(HttpRestServiceRequest<T> request);

        /// <summary>
        /// Create an HTTP REST style request.
        /// </summary>
        /// <typeparam name="T">Type of the object that will be imbedded into the request body.</typeparam>
        /// <param name="request"><see cref="HttpRestServiceRequest{T}"/> containing the elements of the request.</param>
        /// <param name="queryParameters">Query parameters to be added to the request.</param>
        /// <returns><see cref="HttpRestServiceResponse{T}"/> with the response from the service</returns>
        public abstract HttpRestServiceResponse<T> RestRequest<T>(HttpRestServiceRequest<T> request, List<StringPair> queryParameters);

        #endregion
    }
}
