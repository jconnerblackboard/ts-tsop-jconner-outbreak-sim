﻿using BbTS.Core.Conversion;
using BbTS.Core.Logging.Tracing;
using BbTS.Core.Serialization;
using BbTS.Domain.Models.Definitions.Service;
using BbTS.Domain.Models.Exceptions.Configuration;
using BbTS.Domain.Models.Exceptions.Definitions;
using BbTS.Domain.Models.Exceptions.Service;
using BbTS.Domain.Models.Service.Request;
using BbTS.Domain.Models.Service.Response;
using BbTS.Resource.Service.Abstract;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using BbTS.Core;
using BbTS.Domain.Models.General;
using Newtonsoft.Json.Converters;

namespace BbTS.Resource.Service.Concrete.WebServiceResource
{
    /// <summary>
    /// Partial class for making a live HTTP REST request to an external service.
    /// </summary>
    [Trace(AttributePriority = 2)]
    public partial class WebServiceResource : ResourceServiceAbstract
    {
        private readonly JsonSerializerSettings _serializerSettings = new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.Auto,
            Formatting = Newtonsoft.Json.Formatting.None,
            Converters = { new StringEnumConverter() },
            ContractResolver = BaseFirstContractResolver.Instance
        };

        /// <summary>
        /// Create a HTTP REST style request.
        /// </summary>
        /// <param name="request"><see cref="HttpRestServiceRequest{T}"/> containing the elements of the request.</param>
        /// <returns><see cref="HttpRestServiceResponse{T}"/> with the response from the service</returns>
        public override HttpRestServiceResponse<T> RestRequest<T>(HttpRestServiceRequest<T> request)
        {
            // Glue the base uri and route together.
            var uriPath = UriUtilityTool.FormatBaseAndRouteIntoUri(request.BaseUri, request.Route);

            string authority, endpoint, message;
            // Extract the URI Authority from the provided path.
            if (!UriUtilityTool.TryExtractSchemeAndAuthority(uriPath, out authority, out message))
            {
                throw new MalformedUriConfigurationValueException(message);
            }

            // Extract the URI endpoint (everything to the right of the authority) from the provided path.
            if (!UriUtilityTool.TryExtractRightOfAuthority(uriPath, out endpoint, out message))
            {
                throw new MalformedUriConfigurationValueException(message);
            }

            // Create the http client.
            var client = new HttpClient { BaseAddress = new Uri(authority) };
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(WebServiceDefinitions.ContentTypeAsString(request.AcceptType)));

            // create the request message
            var requestMessage = new HttpRequestMessage(WebServiceDefinitions.GetHttpMethod(request.Method), endpoint);
            requestMessage.Headers.ExpectContinue = false;

            // Add the custom headers
            foreach (var header in request.CustomHeaders)
            {
                requestMessage.Headers.Add(header.Key, header.Value);
            }

            // Set timeout if required
            if (request.Timeout.HasValue)
            {
                client.Timeout = request.Timeout.Value;
            }

            if (WebServiceDefinitions.HasBodyContent(request.Method))
            {
                // set the content type and embed the body information if appropriate.
                switch (request.ContentType)
                {
                    case RequestHeaderSupportedContentType.Json:
                        string json = NewtonsoftJson.Serialize(request.Body, _serializerSettings);  //Using the temporary variable allows easier inspection of the outgoing JSON while debugging.
                        requestMessage.Content = new StringContent(json, Encoding.UTF8, WebServiceDefinitions.ContentTypeAsString(request.ContentType));
                        break;
                    case RequestHeaderSupportedContentType.Xml:
                        string xml = Xml.Serialize(request.Body);
                        requestMessage.Content = new StringContent(xml, Encoding.UTF8, WebServiceDefinitions.ContentTypeAsString(request.ContentType));
                        break;
                    default:
                        throw new UndefinedDefinitionException("The provided request ContentType is not supported.",
                            "RestRequest", WebServiceDefinitions.ContentTypeAsString(request.ContentType));
                }
            }
            var response = new HttpRestServiceResponse<T>(request)
            {
                Response = _sendHttpRequest(client, requestMessage).Result
            };

            return response;
        }

        /// <summary>
        /// Create an HTTP REST style request.
        /// </summary>
        /// <typeparam name="T">Type of the object that will be imbedded into the request body.</typeparam>
        /// <param name="request"><see cref="HttpRestServiceRequest{T}"/> containing the elements of the request.</param>
        /// <param name="queryParameters">Query parameters to be added to the request.</param>
        /// <returns><see cref="HttpRestServiceResponse{T}"/> with the response from the service</returns>
        public override HttpRestServiceResponse<T> RestRequest<T>(HttpRestServiceRequest<T> request, List<StringPair> queryParameters)
        {
            Guard.IsNotNull(queryParameters, nameof(queryParameters));

            // Glue the base uri and route together.
            var uriPath = UriUtilityTool.FormatBaseAndRouteIntoUri(request.BaseUri, request.Route);

            var queryString = string.Empty;
            foreach (var pair in queryParameters)
            {
                queryString = $"{queryString}{pair.Key}={pair.Value}&";
            }

            queryString = queryString.Substring(0, queryString.Length - 1);
            var builder = new UriBuilder(uriPath) { Query = queryString };

            var response = new HttpRestServiceResponse<T>(request)
            {
                Response = _sendHttpRequest(builder.Uri).Result
            };

            return response;
        }

        /// <summary>
        /// Helper method to make a formulated http request to an external service.
        /// </summary>
        /// <param name="client">Formatted <see cref="HttpClient"/> with all the necessary request parameters.</param>
        /// <param name="request"><see cref="HttpRequestMessage"/> with the request message.</param>
        /// <returns><see cref="Task"/> string with the contents of the response.</returns>
        private async Task<HttpServiceResponse> _sendHttpRequest(HttpClient client, HttpRequestMessage request)
        {
            HttpResponseMessage response = await client.SendAsync(request).ConfigureAwait(false);

                if (!response.IsSuccessStatusCode)
                {
                    var responseContent = await response.Content.ReadAsStringAsync();
                    throw new WebServiceCommunicationFailedException(response.ReasonPhrase, response.StatusCode, responseContent);
                }

                var result = new HttpServiceResponse
                {
                    Content = await response.Content.ReadAsStringAsync(),
                    Headers = response.Headers,
                    Code = response.StatusCode
                };

                return result;
        }

        /// <summary>
        /// Helper method to make a formulated http request to an external service.
        /// </summary>
        /// <param name="uri">Formatted uri</param>
        /// <returns><see cref="Task"/> string with the contents of the response.</returns>
        private async Task<HttpServiceResponse> _sendHttpRequest(Uri uri)
        {
            var client = new HttpClient();
            var response = client.GetAsync(uri).Result;

            if (!response.IsSuccessStatusCode)
            {
                var responseContent = await response.Content.ReadAsStringAsync();
                throw new WebServiceCommunicationFailedException(response.ReasonPhrase, response.StatusCode, responseContent);
            }

            var result = new HttpServiceResponse
            {
                Content = await response.Content.ReadAsStringAsync(),
                Headers = response.Headers,
                Code = response.StatusCode
            };

            return result;
        }
    }
}
