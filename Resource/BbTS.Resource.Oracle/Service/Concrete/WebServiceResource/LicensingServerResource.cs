﻿using System;
using BbTS.Domain.Models.System.Licensing;
using LicenseVerify;

namespace BbTS.Resource.Service.Concrete.WebServiceResource
{
    public partial class WebServiceResource
    {
        /// <summary>
        /// Upload information about a pending device to the licensing server.
        /// </summary>
        /// <param name="request">Information about the device and the request itself.</param>
        /// <returns>The results of the request operation.</returns>
        public override UploadPendingDeviceResponse UploadPendingDevice(UploadPendingDeviceRequest request)
        {
            var result = Licensing.UploadPendingDevice(
                request.DeviceInformation.LicensingServerIpAddress,
                request.DeviceInformation.SerialNumberOrDeviceId,
                request.DeviceInformation.DeviceName,
                request.DeviceInformation.DeviceType.ToString(),
                request.DeviceInformation.DeviceSubType.ToString(),
                request.DeviceInformation.CustomerKey,
                request.DeviceInformation.CustomerSecondKey,
                request.DeviceInformation.RequesterName,
                request.DeviceInformation.RequesterContact);

            return new UploadPendingDeviceResponse(request)
            {
                Message = result.message,
                Successful = result.success
            };
        }
    }
}
