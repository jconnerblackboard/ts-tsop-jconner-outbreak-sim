﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Communication.BbHost;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.DoorAccess;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Monitoring.Logging;

namespace BbTS.Resource.Service.Concrete.WebServiceResource
{
    public partial class WebServiceResource
    {
        /// <summary>
        /// Send a message to the BbHost.
        /// </summary>
        /// <param name="message">Message to send</param>
        /// <param name="ipAddress">Ip address of the host</param>
        /// <param name="port">listening port.  Default to 1312.</param>
        /// <returns>Response from the host.</returns>
        public override byte[] SendHostRequest(byte[] message, string ipAddress, int port = 1321)
        {
            try
            {
                using (var client = new TcpClient(ipAddress, port))
                {
                    using (var sslStream = new SslStream(client.GetStream(), false, CertificateValidationCallback, CertificateSelectionCallback))
                    {
                        try
                        {
                            var cert = X509Certificate.CreateFromCertFile("ca.pem");
                            var certs = new X509CertificateCollection { cert };

                            sslStream.AuthenticateAsClient(ipAddress, certs, SslProtocols.Tls12, false);
                        }
                        catch (AuthenticationException ex)
                        {
                            var rex = new ResourceLayerException("", Formatting.FormatException(ex), LoggingDefinitions.Category.Resource, LoggingDefinitions.EventId.ServiceHostConnectionException);
                            throw rex;
                        }

                        // write the data
                        sslStream.Write(message, 0, message.Length);
                        sslStream.Flush();

                        // Read the response
                        var buffer = new byte[2048];
                        var bytes = sslStream.Read(buffer, 0, buffer.Length);
                        var responseBytes = new byte[bytes];
                        Buffer.BlockCopy(buffer, 0, responseBytes, 0, responseBytes.Length);

                        return responseBytes;
                    }
                }
            }
            catch (SocketException sockex)
            {
                var rex = new ResourceLayerException("", Formatting.FormatException(sockex), LoggingDefinitions.Category.Resource, LoggingDefinitions.EventId.ServiceHostConnectionException);
                throw rex;
            }
            catch (ResourceLayerException)
            {
                throw;
            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(
                    ex, "", "", EventLogEntryType.Error,
                    (short)LoggingDefinitions.Category.Resource,
                    (int)LoggingDefinitions.EventId.ServiceHostConnectionException);
                throw;
            }
        }

        #region Private Functions

        /// <summary>
        /// Needs definition.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="certificate"></param>
        /// <param name="chain"></param>
        /// <param name="sslPolicyErrors"></param>
        /// <returns></returns>
        private static bool CertificateValidationCallback(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        /// <summary>
        /// Needs definition.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="targetHost"></param>
        /// <param name="localCertificates"></param>
        /// <param name="remoteCertificate"></param>
        /// <param name="acceptableIssuers"></param>
        /// <returns></returns>
        private static X509Certificate CertificateSelectionCallback(
            object sender,
            string targetHost,
            X509CertificateCollection localCertificates,
            X509Certificate remoteCertificate,
            string[] acceptableIssuers)
        {
            return localCertificates[0];
        }

        #endregion
    }
}
