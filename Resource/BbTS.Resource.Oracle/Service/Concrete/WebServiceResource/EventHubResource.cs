﻿using System;
using System.Diagnostics;
using BbTS.Core.Communication.EventHub;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Definitions.Notification;
using BbTS.Domain.Models.Notification;
using BbTS.Domain.Models.Report;
using BbTS.Monitoring.Logging;

namespace BbTS.Resource.Service.Concrete.WebServiceResource
{
    public partial class WebServiceResource
    {
        /// <summary>
        /// Upload a notification to the event hub.
        /// </summary>
        /// <param name="request">The request containing the information to transmit.</param>
        /// <returns>The result of the operation.</returns>
        public override EventHubNotificationUploadResponse EventHubNotificationUpload<T>(EventHubNotificationUploadRequest<T> request)
        {
            try
            {
                MobileCredentialCommunicationUtility.Instance.SendEventHubMessage(request.EventHubObject);
            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(
                    ex, EventLogEntryType.Error,
                    (short)LoggingDefinitions.Category.EventHubAgent,
                    (int)LoggingDefinitions.EventId.EventHubAgentNotificationsHubSendFailure);

                var message = $"Transmission of event hub notification message failed.  Exception text was: '{Formatting.FormatException(ex)}'";
                return new EventHubNotificationUploadResponse { RequestId = request.RequestId, Message = message, Result = EventHubNotificationUploadResult.Failure };
            }

            return new EventHubNotificationUploadResponse { RequestId = request.RequestId, Message = "Success.", Result = EventHubNotificationUploadResult.Success };
        }

        /// <summary>
        /// Upload a customer image to the event hub.
        /// </summary>
        /// <param name="request">The request containing the information to transmit.</param>
        /// <returns>The result of the operation.</returns>
        public override EventHubCustomerImageUploadResponse EventHubCustomerImageUpload<T>(EventHubCustomerImageUploadRequest<T> request)
        {
            try
            {
                // Send the event hub message
                var response = EventHubNotificationUpload(new EventHubNotificationUploadRequest<EventHubNotificationCustomerImage>
                {
                    RequestId = request.RequestId,
                    EventHubObject = new EventHubObject<EventHubNotificationCustomerImage>
                    {
                        EventType  = request.EventHubObject.EventType,
                        EventDateTime = request.EventHubObject.EventDateTime,
                        InstitutionId = request.EventHubObject.InstitutionId,
                        ObjectGuid = request.EventHubObject.ObjectGuid,
                        ProductName = request.EventHubObject.ProductName,
                        Resource = request.EventHubObject.Resource as EventHubNotificationCustomerImage,
                        ResourceType = request.EventHubObject.ResourceType,
                        ResourceVersion = request.EventHubObject.ResourceVersion,
                        ServiceName = request.EventHubObject.ServiceName
                    } 
                });

                return new EventHubCustomerImageUploadResponse { RequestId = request.RequestId, Message = response.Message, Result = response.Result };
            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(
                    ex, EventLogEntryType.Error,
                    (short)LoggingDefinitions.Category.EventHubAgent,
                    (int)LoggingDefinitions.EventId.EventHubAgentNotificationsHubSendFailure);

                var message = $"Transmission of event hub notification message failed.  Exception text was: '{Formatting.FormatException(ex)}'";
                return new EventHubCustomerImageUploadResponse { RequestId = request.RequestId, Message = message, Result = EventHubNotificationUploadResult.Failure };
            }

            
        }
    }
}
