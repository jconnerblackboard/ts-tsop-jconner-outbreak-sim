﻿using System;
using BbTS.Core.Serialization;
using BbTS.Domain.Models.Definitions.Door;
using BbTS.Domain.Models.Definitions.Service;
using BbTS.Domain.Models.General;
using BbTS.Domain.Models.Service.Request;
using BbTS.Domain.Models.System;

namespace BbTS.Resource.Service.Concrete.WebServiceResource
{
    public partial class WebServiceResource
    {
        /// <inheritdoc />
        public override ActionResultToken DsrDoorStateOverrideSet(Guid doorGuid, DoorAaStateChangeStateOverrideValues state, string sessionToken, string dsrServiceBaseUri)
        {
            string stateUrl;
            switch (state)
            {
                case DoorAaStateChangeStateOverrideValues.Lock:
                case DoorAaStateChangeStateOverrideValues.Unlock:
                case DoorAaStateChangeStateOverrideValues.Pulse:
                    stateUrl = state.ToString();
                    break;
                case DoorAaStateChangeStateOverrideValues.PanicOn:
                    stateUrl = "panic/on";
                    break;
                case DoorAaStateChangeStateOverrideValues.PanicOff:
                    stateUrl = "panic/off";
                    break;
                default:
                    return null;
            }

            var request = new HttpRestServiceRequest<string>
            {
                AcceptType = RequestHeaderSupportedContentType.Json,
                BaseUri = dsrServiceBaseUri,
                Body = string.Empty,
                ContentType = RequestHeaderSupportedContentType.Json,
                Method = SupportedHttpRequestMethod.POST,
                Route = $"v10/{doorGuid:D}/{stateUrl}"
            };

            request.CustomHeaders.Add(new StringPair { Key = "SessionToken", Value = sessionToken });

            var result = RestRequest(request);
            return Xml.Deserialize<ActionResultToken>(result.Response.Content);
        }
    }
}
