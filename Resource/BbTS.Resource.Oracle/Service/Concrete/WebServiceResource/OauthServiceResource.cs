﻿using System.Collections.Generic;
using System.Collections.Specialized;
using BbTS.Core;
using BbTS.Core.Conversion;
using BbTS.Core.Security.Encryption;
using BbTS.Core.Security.Oauth;
using BbTS.Domain.Models.Definitions.Security.Oauth;
using BbTS.Domain.Models.Definitions.Service;
using BbTS.Domain.Models.General;
using BbTS.Domain.Models.Service.Request;
using BbTS.Domain.Models.Service.Response;
using BbTS.Domain.Models.System.Security.Oauth;

namespace BbTS.Resource.Service.Concrete.WebServiceResource
{
    public partial class WebServiceResource
    {

        #region Public functions

        /// <summary>
        /// Acquire an oauth token from an external service.
        /// </summary>
        /// <param name="parameters">
        ///     <see cref="OAuthParameters"/> object containing necessary OAuth parameters.  
        ///     Object will be back filled with acquired parameters.
        /// </param>
        /// <returns><see cref="OAuthParameters"/> object will be back filled with acquired parameters.</returns>
        public override OAuthParameters AcquireOauth10AToken(OAuthParameters parameters)
        {
            // Get a temporary token.
            parameters = _requestTemporaryToken(parameters);

            // Exchange the temporary token for an access token.
            parameters = _requestAccessToken(parameters);
            
            return parameters;
        }

        /// <summary>
        /// Create an HTTP REST style request and secure it with OAuth.
        /// </summary>
        /// <typeparam name="T">Type of the object that will be imbedded into the request body.</typeparam>
        /// <param name="request"><see cref="HttpRestServiceRequest{T}"/> containing the elements of the request.</param>
        /// <param name="oauthServerBaseUri"></param>
        /// <returns><see cref="HttpRestServiceResponse{T}"/> with the response from the service</returns>
        public override HttpRestServiceResponse<T> OauthSecuredRestRequest<T>(HttpRestServiceRequest<T> request, string oauthServerBaseUri)
        {
            var aesProvider = new AesEncryptionProvider();
            var parameters = new OAuthParameters
            {
                BaseUri = oauthServerBaseUri,
                ConsumerKey = aesProvider.Decrypt(OAuthConstants.DefaultConsumerKey.Base64Decode()),
                ConsumerSecret = aesProvider.Decrypt(OAuthConstants.DefaultConsumerSecret.Base64Decode())
            };

            OauthGuard.IsNotNullOrWhiteSpace(parameters.BaseUri, "BaseUri");

            parameters = AcquireOauth10AToken(parameters);

            OauthGuard.IsNotNullOrWhiteSpace(parameters.TokenKey, "TokenKey");
            OauthGuard.IsNotNullOrWhiteSpace(parameters.TokenSecret, "TokenSecret");

            var oauthSignatureParameters = OAuthFunctions.CreateOAuthSignatureParameters(
                parameters.TokenKey,
                parameters.TokenSecret,
                request.Method.ToString(),
                UriUtilityTool.FormatBaseAndRouteIntoUri(request.BaseUri, request.Route),
                RequestHeaderSupportedContentType.Json.ToString(),
                parameters.ConsumerKey,
                parameters.ConsumerSecret);

            int index = request.CustomHeaders.FindIndex(pair => pair.Key.ToLower() == "authorization");
            if (index < 0)
            {
                request.CustomHeaders.Add(new StringPair("Authorization",
                    OAuthFunctions.BuildAuthorizationHeader(oauthSignatureParameters)));
            }
            else
            {
                request.CustomHeaders[index] = new StringPair("Authorization",
                    OAuthFunctions.BuildAuthorizationHeader(oauthSignatureParameters));
            }

            return RestRequest(request);
        }

        /// <summary>
        /// Create an HTTP REST style request and secure it with OAuth.
        /// </summary>
        /// <typeparam name="T">Type of the object that will be imbedded into the request body.</typeparam>
        /// <param name="request"><see cref="HttpRestServiceRequest{T}"/> containing the elements of the request.</param>
        /// <param name="oauthServerBaseUri">The base uri for the OAuth authentication server.</param>
        /// <param name="parameters">OAuth credentials to use.</param>
        /// <returns><see cref="HttpRestServiceResponse{T}"/> with the response from the service</returns>
        public override HttpRestServiceResponse<T> OauthSecuredRestRequest<T>(HttpRestServiceRequest<T> request, string oauthServerBaseUri, OAuthParameters parameters)
        {
            OauthGuard.IsNotNullOrWhiteSpace(parameters.BaseUri, "BaseUri");
            OauthGuard.IsNotNullOrWhiteSpace(parameters.TokenKey, "TokenKey");
            OauthGuard.IsNotNullOrWhiteSpace(parameters.TokenSecret, "TokenSecret");

            var oauthSignatureParameters = OAuthFunctions.CreateOAuthSignatureParameters(
                parameters.TokenKey,
                parameters.TokenSecret,
                request.Method.ToString(),
                UriUtilityTool.FormatBaseAndRouteIntoUri(request.BaseUri, request.Route),
                RequestHeaderSupportedContentType.Json.ToString(),
                parameters.ConsumerKey,
                parameters.ConsumerSecret);

            var index = request.CustomHeaders.FindIndex(pair => pair.Key.ToLower() == "authorization");
            if (index < 0)
            {
                request.CustomHeaders.Add(new StringPair("Authorization",
                    OAuthFunctions.BuildAuthorizationHeader(oauthSignatureParameters)));
            }
            else
            {
                request.CustomHeaders[index] = new StringPair("Authorization",
                    OAuthFunctions.BuildAuthorizationHeader(oauthSignatureParameters));
            }

            return RestRequest(request);
        }

        /// <summary>
        /// Create an HTTP REST style request and secure it with OAuth.
        /// </summary>
        /// <typeparam name="T">Type of the object that will be imbedded into the request body.</typeparam>
        /// <param name="request"><see cref="HttpRestServiceRequest{T}"/> containing the elements of the request.</param>
        /// <param name="oauthServerBaseUri">The base uri for the OAuth authentication server.</param>
        /// <param name="parameters">OAuth credentials to use.</param>
        /// <param name="customHeaders">Additional headers to imbed in the request.</param>
        /// <returns><see cref="HttpRestServiceResponse{T}"/> with the response from the service</returns>
        public override HttpRestServiceResponse<T> OauthSecuredRestRequest<T>(HttpRestServiceRequest<T> request, string oauthServerBaseUri, OAuthParameters parameters, List<StringPair> customHeaders)
        {
            OauthGuard.IsNotNullOrWhiteSpace(parameters.BaseUri, "BaseUri");
            OauthGuard.IsNotNullOrWhiteSpace(parameters.TokenKey, "TokenKey");
            OauthGuard.IsNotNullOrWhiteSpace(parameters.TokenSecret, "TokenSecret");

            var oauthSignatureParameters = OAuthFunctions.CreateOAuthSignatureParameters(
                parameters.TokenKey,
                parameters.TokenSecret,
                request.Method.ToString(),
                UriUtilityTool.FormatBaseAndRouteIntoUri(request.BaseUri, request.Route),
                RequestHeaderSupportedContentType.Json.ToString(),
                parameters.ConsumerKey,
                parameters.ConsumerSecret);

            var index = request.CustomHeaders.FindIndex(pair => pair.Key.ToLower() == "authorization");
            if (index < 0)
            {
                request.CustomHeaders.Add(new StringPair("Authorization",
                    OAuthFunctions.BuildAuthorizationHeader(oauthSignatureParameters)));
            }
            else
            {
                request.CustomHeaders[index] = new StringPair("Authorization",
                    OAuthFunctions.BuildAuthorizationHeader(oauthSignatureParameters));
            }

            foreach (var header in customHeaders)
            {
                request.CustomHeaders.Add(header);
            }

            return RestRequest(request);
        }

        #endregion

        #region Private functions

        /// <summary>
        /// Request a temporary OAuth token.
        /// </summary>
        /// <param name="parameters">
        ///     <see cref="OAuthParameters"/> object containing necessary OAuth parameters.  
        ///     Object will be back filled with acquired parameters.
        /// </param>
        /// <returns><see cref="OAuthParameters"/> object will be back filled with acquired parameters.</returns>
        private OAuthParameters _requestTemporaryToken(OAuthParameters parameters)
        {
            OauthGuard.IsNotNullOrWhiteSpace(parameters.ConsumerKey, "ConsumerKey");
            OauthGuard.IsNotNullOrWhiteSpace(parameters.ConsumerSecret, "ConsumerSecret");
            OauthGuard.IsNotNullOrWhiteSpace(parameters.BaseUri, "BaseUri");
            OauthGuard.IsNotNullOrWhiteSpace(parameters.RequestRoute, "RequestRoute");
            
                var oauthSignatureParameters = OAuthFunctions.CreateOAuthSignatureParameters(
                    string.Empty,
                    string.Empty,
                    SupportedHttpRequestMethod.POST.ToString(),
                    UriUtilityTool.FormatBaseAndRouteIntoUri(parameters.BaseUri, parameters.RequestRoute),
                    RequestHeaderSupportedContentType.Json.ToString(),
                    parameters.ConsumerKey,
                    parameters.ConsumerSecret);

                var request = new HttpRestServiceRequest<string>
                {
                    AcceptType = RequestHeaderSupportedContentType.Json,
                    BaseUri = parameters.BaseUri,
                    Body = string.Empty,
                    ContentType = RequestHeaderSupportedContentType.Json,
                    CustomHeaders = new List<StringPair> { new StringPair( "Authorization", OAuthFunctions.BuildAuthorizationHeader(oauthSignatureParameters) ) },
                    Method = SupportedHttpRequestMethod.POST,
                    Route = parameters.RequestRoute
                };

                var response = RestRequest(request);
                var oauthValues = OAuthFunctions.ExtractOauthReturnParameters(response.Response.Content);

                parameters.TokenKey = oauthValues[OAuthConstants.OAuthToken];
                parameters.TokenSecret = oauthValues[OAuthConstants.OAuthTokenSecret];

            return parameters;
        }

        /// <summary>
        /// Request an OAuth Access Token.
        /// </summary>
        /// <param name="parameters">
        ///     <see cref="OAuthParameters"/> object containing necessary OAuth parameters.  
        ///     Object will be back filled with acquired parameters.
        /// </param>
        /// <returns><see cref="OAuthParameters"/> object will be back filled with acquired parameters.</returns>
        private OAuthParameters _requestAccessToken(OAuthParameters parameters)
        {
            OauthGuard.IsNotNullOrWhiteSpace(parameters.ConsumerKey, "ConsumerKey");
            OauthGuard.IsNotNullOrWhiteSpace(parameters.ConsumerSecret, "ConsumerSecret");
            OauthGuard.IsNotNullOrWhiteSpace(parameters.TokenKey, "TokenKey");
            OauthGuard.IsNotNullOrWhiteSpace(parameters.TokenSecret, "TokenSecret");
            OauthGuard.IsNotNullOrWhiteSpace(parameters.BaseUri, "BaseUri");
            OauthGuard.IsNotNullOrWhiteSpace(parameters.TokenRoute, "TokenRoute");

            var oauthSignatureParameters = OAuthFunctions.CreateOAuthSignatureParameters(
                parameters.TokenKey,
                parameters.TokenSecret,
                SupportedHttpRequestMethod.POST.ToString(),
                UriUtilityTool.FormatBaseAndRouteIntoUri(parameters.BaseUri, parameters.TokenRoute),
                RequestHeaderSupportedContentType.Json.ToString(),
                parameters.ConsumerKey,
                parameters.ConsumerSecret);

            var request = new HttpRestServiceRequest<string>
            {
                AcceptType = RequestHeaderSupportedContentType.Json,
                BaseUri = parameters.BaseUri,
                Body = string.Empty,
                ContentType = RequestHeaderSupportedContentType.Json,
                CustomHeaders = new List<StringPair> { new StringPair("Authorization", OAuthFunctions.BuildAuthorizationHeader(oauthSignatureParameters)) },
                Method = SupportedHttpRequestMethod.POST,
                Route = parameters.TokenRoute
            };

            var response = RestRequest(request);
            var oauthValues = OAuthFunctions.ExtractOauthReturnParameters(response.Response.Content);

            parameters.TokenKey = oauthValues[OAuthConstants.OAuthToken];
            parameters.TokenSecret = oauthValues[OAuthConstants.OAuthTokenSecret];
            return parameters;
        }

        #endregion
    }
}
