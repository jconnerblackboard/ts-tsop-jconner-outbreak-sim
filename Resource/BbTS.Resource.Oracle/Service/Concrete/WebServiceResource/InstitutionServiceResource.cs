﻿using BbTS.Core;
using BbTS.Core.Serialization;
using BbTS.Domain.Models.Definitions.Service;
using BbTS.Domain.Models.Institution;
using BbTS.Domain.Models.Service.Request;
using BbTS.Domain.Models.Service.Response;
using BbTS.Domain.Models.System;
using System;
using System.Collections.Generic;
using BbTS.Core.Conversion;
using BbTS.Core.Security.Oauth;
using BbTS.Domain.Models.General;
using BbTS.Domain.Models.System.Security.Oauth;

namespace BbTS.Resource.Service.Concrete.WebServiceResource
{
    /// <summary>
    /// Partial class to handle registration requests to an external resource (BbIS/BbSP).
    /// </summary>
    public partial class WebServiceResource
    {

        #region Application Credentials

        /// <summary>
        /// Gets the Application Credentials by Institution Id.
        /// </summary>
        /// <param name="institutionId">Institution Id.</param>
        /// <param name="registrationServerBaseUri">The base URI for the registration server (most likely the BaseURI of the BbIS server)</param>
        /// <param name="authenticationServierUri">The base URI of the (probably OAuth) authentication server.  If left null the authorization header will not be populated.</param>
        /// <returns>The requested objects or null</returns>
        public override List<SpApplication> ApplicationCredentialsGet(Guid institutionId, string registrationServerBaseUri, string authenticationServierUri = null)
        {
            var request = new HttpRestServiceRequest<string>
            {
                AcceptType = RequestHeaderSupportedContentType.Json,
                BaseUri = registrationServerBaseUri,
                Body = string.Empty,
                ContentType = RequestHeaderSupportedContentType.Json,
                Method = SupportedHttpRequestMethod.GET,
                Route = $"20140501/institution/institutions/{institutionId:D}/applications",
                Timeout = TimeSpan.FromSeconds(300)
            };

            var result = OauthSecuredRestRequest(request, authenticationServierUri);

            var content = result.Response.Content;
            var response = NewtonsoftJson.Deserialize<ApplicationListResponse>(content);

            return response?.Applications;
        }

        #endregion

        #region Institution Route

        /// <summary>
        /// Gets an Institution by Institution Id.
        /// </summary>
        /// <param name="institutionId">Institution Id.</param>
        /// <param name="registrationServerBaseUri">The base URI for the registration server (most likely the BaseURI of the BbIS server)</param>
        /// <param name="authenticationServierUri">The base URI of the (probably OAuth) authentication server.  If left null the authorization header will not be populated.</param>
        /// <returns>The requested object or null</returns>
        public override SpInstitution InstitutionGet(Guid institutionId, string registrationServerBaseUri, string authenticationServierUri = null)
        {
            var request = new HttpRestServiceRequest<string>
            {
                AcceptType = RequestHeaderSupportedContentType.Json,
                BaseUri = registrationServerBaseUri,
                Body = string.Empty,
                ContentType = RequestHeaderSupportedContentType.Json,
                Method = SupportedHttpRequestMethod.GET,
                Route = $"20140501/institution/institutions/{institutionId:D}",
                Timeout = TimeSpan.FromSeconds(300)
            };

            var result = OauthSecuredRestRequest(request, authenticationServierUri);

            var content = result.Response.Content;
            var response = NewtonsoftJson.Deserialize<InstitutionGetResponse>(content);

            return response?.Institution;
        }

        #endregion

        #region Merchant

        /// <summary>
        /// Gets the Merchants by Institution Id.
        /// </summary>
        /// <param name="institutionId">Institution Id.</param>
        /// <param name="registrationServerBaseUri">The base URI for the registration server (most likely the BaseURI of the BbIS server)</param>
        /// <param name="authenticationServierUri">The base URI of the (probably OAuth) authentication server.  If left null the authorization header will not be populated.</param>
        /// <returns>The requested objects or null</returns>
        public override List<SpMerchant> MerchantsGet(Guid institutionId, string registrationServerBaseUri, string authenticationServierUri = null)
        {
            var request = new HttpRestServiceRequest<string>
            {
                AcceptType = RequestHeaderSupportedContentType.Json,
                BaseUri = registrationServerBaseUri,
                Body = string.Empty,
                ContentType = RequestHeaderSupportedContentType.Json,
                Method = SupportedHttpRequestMethod.GET,
                Route = $"20140501/institution/institutions/{institutionId:D}/merchants",
                Timeout = TimeSpan.FromSeconds(300)
            };

            var result = OauthSecuredRestRequest(request, authenticationServierUri);

            var content = result.Response.Content;
            var response = NewtonsoftJson.Deserialize<MerchantListResponse>(content);

            return response?.Merchants;
        }

        #endregion

        #region Transaction System

        /// <summary>
        /// Create a transaction system.
        /// </summary>
        /// <param name="systemFeatures"><see cref="SystemFeatures"/> object containing the instance to create.</param>
        /// <param name="registrationServerBaseUri">The base URI for the registration server (most likely the BaseURI of the BbIS server)</param>
        /// <param name="authenticationServierUri">The base URI of the (probably OAuth) authentication server.  If left null the authorization header will not be populated.</param>
        /// <returns><see cref="TransactionSystem"/> object after creation.  InstitutionId will be populated.</returns>
        public override TransactionSystem TransactionSystemCreate(SystemFeatures systemFeatures, string registrationServerBaseUri, string authenticationServierUri = null)
        {
            Guard.IsNotNull(systemFeatures, "systemFeatures");
            Guard.IsNotNullOrWhiteSpace(systemFeatures.CampusName, "CampusName");
            Guard.IsNotNullOrWhiteSpace(systemFeatures.TransactionSystemId, "TransactionSystemId");
            Guard.IsNotNullOrWhiteSpace(systemFeatures.LicenseNumber, "LicenseNumber");

            var transactionSystemRequestPost = new TransactionSystemRequestPost
            {
                TransactionSystem = new TransactionSystem
                {
                    ExpirationDate = systemFeatures.ExpirationDate,
                    LicenseName = systemFeatures.CampusName,
                    InstanceId = new Guid(systemFeatures.TransactionSystemId),
                    LicenseNumber = systemFeatures.LicenseNumber
                }
            };

            var request = new HttpRestServiceRequest<TransactionSystemRequestPost>
            {
                AcceptType = RequestHeaderSupportedContentType.Json,
                BaseUri = registrationServerBaseUri,
                Body = transactionSystemRequestPost,
                ContentType = RequestHeaderSupportedContentType.Json,
                Method = SupportedHttpRequestMethod.POST,
                Route = "20140501/institution/transactionSystems",
                Timeout = TimeSpan.FromSeconds(300)
            };

            var result = OauthSecuredRestRequest(request, authenticationServierUri);

            var content = result.Response.Content;
            var response = NewtonsoftJson.Deserialize<TransactionSystemResponse>(content);

            return response?.TransactionSystem;
        }

        /// <summary>
        /// Update a transaction system
        /// </summary>
        /// <param name="systemFeatures"><see cref="SystemFeatures"/> object containing the instance to update.</param>
        /// <param name="transactionSystemId">The id of the currently recorded transaction system in the database resource layer.</param>
        /// <param name="registrationServerBaseUri">The base URI for the registration server (most likely the BaseURI of the BbIS server)</param>
        /// <param name="authenticationServierUri">The base URI of the (probably OAuth) authentication server.  If left null the authorization header will not be populated.</param>
        /// <returns><see cref="TransactionSystem"/> object after update.</returns>
        public override TransactionSystem TransactionSystemUpdate(SystemFeatures systemFeatures, string transactionSystemId, string registrationServerBaseUri, string authenticationServierUri = null)
        {
            Guard.IsNotNull(systemFeatures, "systemFeatures");
            Guard.IsNotNullOrWhiteSpace(systemFeatures.CampusName, "CampusName");
            Guard.IsNotNullOrWhiteSpace(transactionSystemId, "TransactionSystemId");
            Guard.IsNotNullOrWhiteSpace(systemFeatures.LicenseNumber, "LicenseNumber");

            var transactionSystemRequestPatch = new TransactionSystemRequestPatch
            {
                TransactionSystemId = new Guid(transactionSystemId),
                TransactionSystem = new PatchTransactionSystemRegistration
                {
                    ExpirationDate = systemFeatures.ExpirationDate,
                    LicenseName = systemFeatures.CampusName,
                    LicenseNumber = systemFeatures.LicenseNumber
                }
            };

            var request = new HttpRestServiceRequest<TransactionSystemRequestPatch>
            {
                AcceptType = RequestHeaderSupportedContentType.Json,
                BaseUri = registrationServerBaseUri,
                Body = transactionSystemRequestPatch,
                ContentType = RequestHeaderSupportedContentType.Json,
                Method = SupportedHttpRequestMethod.PATCH,
                Route = $"20140501/institution/transactionSystems/{transactionSystemId}",
                Timeout = TimeSpan.FromSeconds(300)
            };

            var result = OauthSecuredRestRequest(request, authenticationServierUri);

            var content = result.Response.Content;
            var response = NewtonsoftJson.Deserialize<TransactionSystemResponse>(content);

            return response?.TransactionSystem;
        }

        /// <summary>
        /// Get a transaction system from an external resource.
        /// </summary>
        /// <param name="instanceId">Instance Id.</param>
        /// <param name="registrationServerBaseUri">The base URI for the registration server (most likely the BaseURI of the BbIS server)</param>
        /// <param name="authenticationServierUri">The base URI of the (probably OAuth) authentication server.  If left null the authorization header will not be populated.</param>
        /// <returns><see cref="TransactionSystem"/> object after retrieval.</returns>
        public override List<TransactionSystem> TransactionSystemGet(string instanceId, string registrationServerBaseUri, string authenticationServierUri = null)
        {
            var request = new HttpRestServiceRequest<string>
            {
                AcceptType = RequestHeaderSupportedContentType.Json,
                BaseUri = registrationServerBaseUri,
                Body = string.Empty,
                ContentType = RequestHeaderSupportedContentType.Json,
                Method = SupportedHttpRequestMethod.GET,
                Route = $"20140501/institution/transactionSystems?InstanceId={instanceId}",
                Timeout = TimeSpan.FromSeconds(300)
            };

            var result = OauthSecuredRestRequest(request, authenticationServierUri);

            var content = result.Response.Content;
            var response = NewtonsoftJson.Deserialize<TransactionSystemGetResponse>(content);

            return response?.TransactionSystems;
        }

        /// <summary>
        /// Delete a transaction system from an external resource.
        /// </summary>
        /// <param name="systemFeatures"><see cref="SystemFeatures"/> object containing identifiers for the transaction system to fetch.</param>
        /// <param name="registrationServerBaseUri">The base URI for the registration server (most likely the BaseURI of the BbIS server)</param>
        /// <param name="authenticationServierUri">The base URI of the (probably OAuth) authentication server.  If left null the authorization header will not be populated.</param>
        public override void TransactionSystemDelete(
            SystemFeatures systemFeatures,
            string registrationServerBaseUri,
            string authenticationServierUri = null)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get institution information from the eaccounts portal.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The result of the operation.</returns>
        public override InstitutionInformationGetResponse EaccountsInstitutionInformationGet(InstitutionInformationGetRequest request)
        {
            var getRequest = new HttpRestServiceRequest<string>
            {
                AcceptType = RequestHeaderSupportedContentType.Json,
                BaseUri = $"https://{request.Hostname}/",
                Body = string.Empty,
                ContentType = RequestHeaderSupportedContentType.Json,
                Method = SupportedHttpRequestMethod.GET,
                Route = InstitutionInformationGetRequest.Route,
                Timeout = TimeSpan.FromSeconds(300)
            };

            var parameters = new OAuthParameters
            {
                BaseUri = $"https://{request.Hostname}/",
                ConsumerKey = request.ConsumerKey,
                ConsumerSecret = request.ConsumerSecret,
                Version = "1.0"
            };

            var oauthSignatureParameters = OAuthFunctions.CreateOAuthSignatureParameters(
                string.Empty,
                string.Empty,
                getRequest.Method.ToString(),
                UriUtilityTool.FormatBaseAndRouteIntoUri(getRequest.BaseUri, getRequest.Route),
                RequestHeaderSupportedContentType.Xml.ToString(),
                parameters.ConsumerKey,
                parameters.ConsumerSecret);

            oauthSignatureParameters.OauthParameters.Version = "1.0";
            oauthSignatureParameters.OauthParameters.CallbackUri = null;

            getRequest.Route = OAuthFunctions.CreateParameterizedOauthUrl(getRequest.Route, oauthSignatureParameters);
            //getRequest.Route = getRequest.Route;

            getRequest.AcceptType = RequestHeaderSupportedContentType.Xml;

            var queryParameters = new List<StringPair>
            {
                new StringPair("oauth_consumer_key", oauthSignatureParameters.OauthParameters.ConsumerKey),
                new StringPair("oauth_signature_method", oauthSignatureParameters.OauthParameters.SignatureMethod),
                new StringPair("oauth_timestamp", oauthSignatureParameters.OauthParameters.Timestamp),
                new StringPair("oauth_nonce", oauthSignatureParameters.OauthParameters.Nonce),
                new StringPair("oauth_version", oauthSignatureParameters.OauthParameters.Version),
                new StringPair("oauth_signature", OAuthFunctions.GenerateSignature(oauthSignatureParameters))
            };

            //var result = RestRequest(getRequest, queryParameters);
            var result = RestRequest(getRequest);

            var content = result.Response.Content;
            var response = Xml.Deserialize<EaccountsInstitutionList>(content);

            return new InstitutionInformationGetResponse
            {
                RequestId = request.RequestId,
                InstitutionList = response

            };
        }

        #endregion

    }
}
