﻿using BbTS.Domain.Models.Institution;
using BbTS.Domain.Models.System;
using System;
using System.Collections.Generic;

namespace BbTS.Resource.Service.Concrete.ProxyServiceResource
{
    /// <summary>
    /// Proxy class to handle mock registration requests.
    /// </summary>
    public partial class ProxyServiceResource
    {

        #region Application Credentials

        /// <summary>
        /// Gets the Application Credentials by Institution Id.
        /// </summary>
        /// <param name="institutionId">Institution Id.</param>
        /// <param name="registrationServerBaseUri">The base URI for the registration server (most likely the BaseURI of the BbIS server)</param>
        /// <param name="authenticationServierUri">The base URI of the (probably OAuth) authentication server.  If left null the authorization header will not be populated.</param>
        /// <returns>The requested object or null</returns>
        public override List<SpApplication> ApplicationCredentialsGet(Guid institutionId, string registrationServerBaseUri, string authenticationServierUri = null)
        {
            return Resource?.ApplicationCredentialsGet(institutionId, registrationServerBaseUri, authenticationServierUri);
        }

        #endregion

        #region Institution

        /// <summary>
        /// Gets an Institution by Institution Id.
        /// </summary>
        /// <param name="institutionId">Institution Id.</param>
        /// <param name="registrationServerBaseUri">The base URI for the registration server (most likely the BaseURI of the BbIS server)</param>
        /// <param name="authenticationServierUri">The base URI of the (probably OAuth) authentication server.  If left null the authorization header will not be populated.</param>
        /// <returns>The requested object or null</returns>
        public override SpInstitution InstitutionGet(Guid institutionId, string registrationServerBaseUri, string authenticationServierUri = null)
        {
            return Resource?.InstitutionGet(institutionId, registrationServerBaseUri, authenticationServierUri);
        }

        #endregion

        #region Merchant

        /// <summary>
        /// Gets the Merchants by Institution Id.
        /// </summary>
        /// <param name="institutionId">Institution Id.</param>
        /// <param name="registrationServerBaseUri">The base URI for the registration server (most likely the BaseURI of the BbIS server)</param>
        /// <param name="authenticationServierUri">The base URI of the (probably OAuth) authentication server.  If left null the authorization header will not be populated.</param>
        /// <returns>The requested objects or null</returns>
        public override List<SpMerchant> MerchantsGet(Guid institutionId, string registrationServerBaseUri, string authenticationServierUri = null)
        {
            return Resource?.MerchantsGet(institutionId, registrationServerBaseUri, authenticationServierUri);
        }

        #endregion

        #region Transaction System

        /// <summary>
        /// Create a transaction system.
        /// </summary>
        /// <param name="systemFeatures"><see cref="SystemFeatures"/> object containing the instance to create.</param>
        /// <param name="registrationServerBaseUri">The base URI for the registration server (most likely the BaseURI of the BbIS server)</param>
        /// <param name="authenticationServierUri">The base URI of the (probably OAuth) authentication server.  If left null the authorization header will not be populated.</param>
        /// <returns><see cref="TransactionSystem"/> object after creation.  InstitutionId will be populated.</returns>
        public override TransactionSystem TransactionSystemCreate(
            SystemFeatures systemFeatures,
            string registrationServerBaseUri,
            string authenticationServierUri = null)
        {
            return Resource?.TransactionSystemCreate(systemFeatures, registrationServerBaseUri, authenticationServierUri);
        }

        /// <summary>
        /// Update a transaction system
        /// </summary>
        /// <param name="systemFeatures"><see cref="SystemFeatures"/> object containing the instance to update.</param>
        /// <param name="transactionSystemId">The id of the currently recorded transaction system in the database resource layer.</param>
        /// <param name="registrationServerBaseUri">The base URI for the registration server (most likely the BaseURI of the BbIS server)</param>
        /// <param name="authenticationServierUri">The base URI of the (probably OAuth) authentication server.  If left null the authorization header will not be populated.</param>
        /// <returns><see cref="TransactionSystem"/> object after update.</returns>
        public override TransactionSystem TransactionSystemUpdate(SystemFeatures systemFeatures, string transactionSystemId, string registrationServerBaseUri, string authenticationServierUri = null)
        {
            return Resource?.TransactionSystemUpdate(systemFeatures, transactionSystemId, registrationServerBaseUri, authenticationServierUri);
        }

        /// <summary>
        /// Get a transaction system from an external resource.
        /// </summary>
        /// <param name="instanceId">Instance Id.</param>
        /// <param name="registrationServerBaseUri">The base URI for the registration server (most likely the BaseURI of the BbIS server)</param>
        /// <param name="authenticationServierUri">The base URI of the (probably OAuth) authentication server.  If left null the authorization header will not be populated.</param>
        /// <returns><see cref="TransactionSystem"/> object after retrieval.</returns>
        public override List<TransactionSystem> TransactionSystemGet(string instanceId, string registrationServerBaseUri, string authenticationServierUri = null)
        {
            return Resource?.TransactionSystemGet(instanceId, registrationServerBaseUri, authenticationServierUri);
        }

        /// <summary>
        /// Delete a transaction system from an external resource.
        /// </summary>
        /// <param name="systemFeatures"><see cref="SystemFeatures"/> object containing identifiers for the transaction system to fetch.</param>
        /// <param name="registrationServerBaseUri">The base URI for the registration server (most likely the BaseURI of the BbIS server)</param>
        /// <param name="authenticationServierUri">The base URI of the (probably OAuth) authentication server.  If left null the authorization header will not be populated.</param>
        public override void TransactionSystemDelete(
            SystemFeatures systemFeatures,
            string registrationServerBaseUri,
            string authenticationServierUri = null)
        {
            Resource?.TransactionSystemDelete(systemFeatures, registrationServerBaseUri, authenticationServierUri);
        }

        /// <summary>
        /// Get institution information from the eaccounts portal.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The result of the operation.</returns>
        public override InstitutionInformationGetResponse EaccountsInstitutionInformationGet(InstitutionInformationGetRequest request)
        {
            return Resource?.EaccountsInstitutionInformationGet(request);
        }

        #endregion

    }
}
