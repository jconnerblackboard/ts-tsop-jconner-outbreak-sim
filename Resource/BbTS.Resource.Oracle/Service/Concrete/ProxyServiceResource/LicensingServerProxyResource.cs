﻿using BbTS.Domain.Models.System.Licensing;

namespace BbTS.Resource.Service.Concrete.ProxyServiceResource
{
    public partial class ProxyServiceResource
    {
        /// <summary>
        /// Upload information about a pending device to the licensing server.
        /// </summary>
        /// <param name="request">Information about the device and the request itself.</param>
        /// <returns>The results of the request operation.</returns>
        public override UploadPendingDeviceResponse UploadPendingDevice(UploadPendingDeviceRequest request)
        {
            return Resource?.UploadPendingDevice(request);
        }
    }
}
