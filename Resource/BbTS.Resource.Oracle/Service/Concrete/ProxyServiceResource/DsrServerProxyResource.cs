﻿using System;
using BbTS.Domain.Models.Definitions.Door;
using BbTS.Domain.Models.System;

namespace BbTS.Resource.Service.Concrete.ProxyServiceResource
{
    public partial class ProxyServiceResource
    {
        /// <inheritdoc />
        public override ActionResultToken DsrDoorStateOverrideSet(Guid doorGuid, DoorAaStateChangeStateOverrideValues state, string sessionToken, string dsrServiceBaseUri)
        {
            return Resource?.DsrDoorStateOverrideSet(doorGuid, state, sessionToken, dsrServiceBaseUri);
        }
    }
}
