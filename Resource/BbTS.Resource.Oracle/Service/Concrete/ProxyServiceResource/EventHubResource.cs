﻿using BbTS.Domain.Models.Notification;

namespace BbTS.Resource.Service.Concrete.ProxyServiceResource
{
    public partial class ProxyServiceResource
    {
        /// <summary>
        /// Upload a notification to the event hub
        /// </summary>
        /// <param name="request">The request containing the information to transmit.</param>
        /// <returns>The result of the operation.</returns>
        public override EventHubNotificationUploadResponse EventHubNotificationUpload<T>(EventHubNotificationUploadRequest<T> request)
        {
            return Resource?.EventHubNotificationUpload(request);
        }

        /// <summary>
        /// Upload a customer image to the event hub.
        /// </summary>
        /// <param name="request">The request containing the information to transmit.</param>
        /// <returns>The result of the operation.</returns>
        public override EventHubCustomerImageUploadResponse EventHubCustomerImageUpload<T>(EventHubCustomerImageUploadRequest<T> request)
        {
            return Resource?.EventHubCustomerImageUpload(request);
        }
    }
}
