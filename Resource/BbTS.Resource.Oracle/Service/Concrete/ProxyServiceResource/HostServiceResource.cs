﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.DoorAccess;

namespace BbTS.Resource.Service.Concrete.ProxyServiceResource
{
    public partial class ProxyServiceResource
    {

        /// <summary>
        /// Send a message to the BbHost.
        /// </summary>
        /// <param name="message">Message to send</param>
        /// <param name="ipAddress">Ip address of the host</param>
        /// <param name="port">listening port.  Default to 1312.</param>
        /// <returns>Response from the host.</returns>
        public override byte[] SendHostRequest(byte[] message, string ipAddress, int port = 1321)
        {
            return Resource?.SendHostRequest(message, ipAddress, port);
        }
    }
}
