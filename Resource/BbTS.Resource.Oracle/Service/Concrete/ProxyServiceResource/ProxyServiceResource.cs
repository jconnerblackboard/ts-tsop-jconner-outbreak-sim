﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net.Http;
using BbTS.Core.Logging.Tracing;
using BbTS.Domain.Models.Definitions.Service;
using BbTS.Domain.Models.General;
using BbTS.Domain.Models.Service.Request;
using BbTS.Domain.Models.Service.Response;
using BbTS.Domain.Models.System.Security.Oauth;
using BbTS.Resource.Service.Abstract;

namespace BbTS.Resource.Service.Concrete.ProxyServiceResource
{
    /// <summary>
    /// Partial class to support Proxy resource types (for unit testing).
    /// </summary>
    [Trace(AttributePriority = 2)]
    public partial class ProxyServiceResource : ResourceServiceAbstract
    {
        /// <summary>
        /// Proxy resource.
        /// </summary>
        public ResourceServiceAbstract Resource { get; set; }


        /// <summary>
        /// Create a HTTP REST style request.
        /// </summary>
        /// <param name="request"><see cref="HttpRestServiceRequest{T}"/> containing the elements of the request.</param>
        /// <returns><see cref="HttpRestServiceResponse{T}"/> with the response from the service</returns>
        public override HttpRestServiceResponse<T> RestRequest<T>(HttpRestServiceRequest<T> request)
        {
            return Resource?.RestRequest(request);
        }

        /// <summary>
        /// Create an HTTP REST style request.
        /// </summary>
        /// <typeparam name="T">Type of the object that will be imbedded into the request body.</typeparam>
        /// <param name="request"><see cref="HttpRestServiceRequest{T}"/> containing the elements of the request.</param>
        /// <param name="queryParameters">Query parameters to be added to the request.</param>
        /// <returns><see cref="HttpRestServiceResponse{T}"/> with the response from the service</returns>
        public override HttpRestServiceResponse<T> RestRequest<T>(HttpRestServiceRequest<T> request, List<StringPair> queryParameters)
        {
            return Resource?.RestRequest(request, queryParameters);
        }
    }
}
