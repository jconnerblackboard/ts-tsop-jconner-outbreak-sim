﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.General;
using BbTS.Domain.Models.Service.Request;
using BbTS.Domain.Models.Service.Response;
using BbTS.Domain.Models.System.Security.Oauth;

namespace BbTS.Resource.Service.Concrete.ProxyServiceResource
{
    public partial class ProxyServiceResource
    {

        /// <summary>
        /// Acquire an oauth token from an external service.
        /// </summary>
        /// <param name="parameters">
        ///     <see cref="OAuthParameters"/> object containing necessary OAuth parameters.  
        ///     Object will be back filled with acquired parameters.
        /// </param>
        /// <returns><see cref="OAuthParameters"/> object will be back filled with acquired parameters.</returns>
        public override OAuthParameters AcquireOauth10AToken(OAuthParameters parameters)
        {
            return Resource?.AcquireOauth10AToken(parameters);
        }

        /// <summary>
        /// Create an HTTP REST style request and secure it with OAuth.
        /// </summary>
        /// <typeparam name="T">Type of the object that will be imbedded into the request body.</typeparam>
        /// <param name="request"><see cref="HttpRestServiceRequest{T}"/> containing the elements of the request.</param>
        /// <param name="oauthServerBaseUri"></param>
        /// <returns><see cref="HttpRestServiceResponse{T}"/> with the response from the service</returns>
        public override HttpRestServiceResponse<T> OauthSecuredRestRequest<T>(HttpRestServiceRequest<T> request, string oauthServerBaseUri)
        {
            return Resource?.OauthSecuredRestRequest(request, oauthServerBaseUri);
        }

        /// <summary>
        /// Create an HTTP REST style request and secure it with OAuth.
        /// </summary>
        /// <typeparam name="T">Type of the object that will be imbedded into the request body.</typeparam>
        /// <param name="request"><see cref="HttpRestServiceRequest{T}"/> containing the elements of the request.</param>
        /// <param name="oauthServerBaseUri">The base uri for the OAuth authentication server.</param>
        /// <param name="parameters">OAuth credentials to use.</param>
        /// <returns><see cref="HttpRestServiceResponse{T}"/> with the response from the service</returns>
        public override HttpRestServiceResponse<T> OauthSecuredRestRequest<T>(HttpRestServiceRequest<T> request, string oauthServerBaseUri, OAuthParameters parameters)
        {
            return Resource?.OauthSecuredRestRequest(request, oauthServerBaseUri, parameters);
        }

        /// <summary>
        /// Create an HTTP REST style request and secure it with OAuth.
        /// </summary>
        /// <typeparam name="T">Type of the object that will be imbedded into the request body.</typeparam>
        /// <param name="request"><see cref="HttpRestServiceRequest{T}"/> containing the elements of the request.</param>
        /// <param name="oauthServerBaseUri">The base uri for the OAuth authentication server.</param>
        /// <param name="parameters">OAuth credentials to use.</param>
        /// <param name="customHeaders">Additional headers to imbed in the request.</param>
        /// <returns><see cref="HttpRestServiceResponse{T}"/> with the response from the service</returns>
        public override HttpRestServiceResponse<T> OauthSecuredRestRequest<T>(HttpRestServiceRequest<T> request, string oauthServerBaseUri, OAuthParameters parameters, List<StringPair> customHeaders)
        {
            return Resource?.OauthSecuredRestRequest(request, oauthServerBaseUri, parameters, customHeaders);
        }
    }
}
