/* ShapeDefinition
{5}
ShapeDefinition */

SET SERVEROUTPUT ON;

DECLARE   iCount       INTEGER         := 0;
BEGIN
 SELECT COUNT(*)
 INTO   iCount
 FROM   USER_INDEXES U
 WHERE  U.INDEX_NAME = UPPER('{0}');

 IF (iCount != 0) THEN
  EXECUTE IMMEDIATE ('DROP INDEX {0}');
  DBMS_OUTPUT.PUT_LINE('{0} has been dropped.');
 ELSE
  DBMS_OUTPUT.PUT_LINE('{0} does not exist. No work being done.');
 END IF;
END;
/

DECLARE   iCount       INTEGER         := 0;
BEGIN
 SELECT COUNT(*)
 INTO   iCount
 FROM   USER_INDEXES U
 WHERE  U.INDEX_NAME = UPPER('{0}');

 IF (iCount = 0) THEN
  EXECUTE IMMEDIATE ('CREATE{3}INDEX {0} ON {1} ({2}) TABLESPACE {4}');
  DBMS_OUTPUT.PUT_LINE('{0} has been created.');
 ELSE
  DBMS_OUTPUT.PUT_LINE('{0} exists. No work being done.');
 END IF;
END;
/