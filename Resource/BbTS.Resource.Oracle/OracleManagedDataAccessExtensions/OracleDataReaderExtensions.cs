﻿using System;
using System.Data.Common;

namespace BbTS.Resource.OracleManagedDataAccessExtensions
{
    /// <summary>
    /// Convenience methods for managing parameters for stored procedures on OracleDataReader objects.
    /// </summary>
    public static class OracleDataReaderExtensions
    {
        /// <summary>
        /// Gets a short value by name from the DbDataReader
        /// </summary>
        /// <param name="r">DbDataReader to access</param>
        /// <param name="name">Parameter name to retrieve.</param>
        /// <returns>Value of the named parameter.</returns>
        public static short GetInt16(this DbDataReader r, string name)
        {
            var ordinal = r.GetOrdinal(name);
            return r.GetInt16(ordinal);
        }

        /// <summary>
        /// Gets an int value by name from the DbDataReader
        /// </summary>
        /// <param name="r">DbDataReader to access</param>
        /// <param name="name">Parameter name to retrieve.</param>
        /// <returns>Value of the named parameter.</returns>
        public static int GetInt32(this DbDataReader r, string name)
        {
            var ordinal = r.GetOrdinal(name);
            return r.GetInt32(ordinal);
        }

        /// <summary>
        /// Gets a long value by name from the DbDataReader
        /// </summary>
        /// <param name="r">DbDataReader to access</param>
        /// <param name="name">Parameter name to retrieve.</param>
        /// <returns>Value of the named parameter.</returns>
        public static long GetInt64(this DbDataReader r, string name)
        {
            var ordinal = r.GetOrdinal(name);
            return r.GetInt64(ordinal);
        }

        /// <summary>
        /// Gets a string value by name from the DbDataReader
        /// </summary>
        /// <param name="r">DbDataReader to access</param>
        /// <param name="name">Parameter name to retrieve.</param>
        /// <returns>Value of the named parameter.</returns>
        public static string GetString(this DbDataReader r, string name)
        {
            var ordinal = r.GetOrdinal(name);
            if (r.IsDBNull(ordinal))
            {
                return null;
            }
            return r.GetString(ordinal);
        }

        /// <summary>
        /// Gets a DateTime value by name from the DbDataReader
        /// </summary>
        /// <param name="r">DbDataReader to access</param>
        /// <param name="name">Parameter name to retrieve.</param>
        /// <returns>Value of the named parameter.</returns>
        public static DateTime GetDateTime(this DbDataReader r, string name)
        {
            var ordinal = r.GetOrdinal(name);
            return r.GetDateTime(ordinal);
        }

        /// <summary>
        /// Gets a double value by name from the DbDataReader
        /// </summary>
        /// <param name="r">DbDataReader to access</param>
        /// <param name="name">Parameter name to retrieve.</param>
        /// <returns>Value of the named parameter.</returns>
        public static double GetDouble(this DbDataReader r, string name)
        {
            var ordinal = r.GetOrdinal(name);
            return r.GetDouble(ordinal);
        }

        /// <summary>
        /// Gets a float value by name from the DbDataReader
        /// </summary>
        /// <param name="r">DbDataReader to access</param>
        /// <param name="name">Parameter name to retrieve.</param>
        /// <returns>Value of the named parameter.</returns>
        public static float GetFloat(this DbDataReader r, string name)
        {
            var ordinal = r.GetOrdinal(name);
            return r.GetFloat(ordinal);
        }

        /// <summary>
        /// Gets a decimal value by name from the DbDataReader
        /// </summary>
        /// <param name="r">DbDataReader to access</param>
        /// <param name="name">Parameter name to retrieve.</param>
        /// <returns>Value of the named parameter.</returns>
        public static decimal GetDecimal(this DbDataReader r, string name)
        {
            var ordinal = r.GetOrdinal(name);
            return r.GetDecimal(ordinal);
        }
    }
}
