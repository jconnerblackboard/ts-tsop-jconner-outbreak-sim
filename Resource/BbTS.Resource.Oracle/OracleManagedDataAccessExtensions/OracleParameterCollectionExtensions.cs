﻿using System;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;

namespace BbTS.Resource.OracleManagedDataAccessExtensions
{
    /// <summary>
    /// Convenience methods for managing parameters for stored procedures on OracleParameterCollection objects.
    /// </summary>
    public static class OracleParameterCollectionExtensions
    {
        /// <summary>
        /// Add an In parameter of string type
        /// </summary>
        /// <param name="parameters">OracleParameterCollection to use</param>
        /// <param name="name">Name of parameter</param>
        /// <param name="value">Value of parameter</param>
        /// <param name="maxLength">Optional maximum length of the string.  This is a kludge to avoid a SQL max length exception.  Value will be unceremoniously truncated to this length.</param>
        public static void AddInParam(this OracleParameterCollection parameters, string name, string value, int? maxLength = null)
        {
            //This code has just bitten you in the ass in one of two possible ways.
            //      One: You're trying to figure out why the heck text was truncated.
            //      Two: You're trying to get this beast to function and overcome a sql max length exception.
            //This is the best we could do right now. By truncating all in this one spot, it's possible to search source for calls that specify that
            //parameter and fix those fields in the database to a CLOB or whatever one at a time and realistically find the places we can adapt afterwards.
            int length = string.IsNullOrWhiteSpace(value) ? 0 : value.Length;
            string val = value;
            if (maxLength.HasValue && length > maxLength && value != null)
            {
                val = value.Substring(0, maxLength.Value);
            }

            parameters.Add(new OracleParameter() { ParameterName = name, OracleDbType = OracleDbType.Varchar2, Size = length, Value = val, Direction = ParameterDirection.Input });
        }
        /// <summary>
        /// Add an In parameter of char type
        /// </summary>
        /// <param name="parameters">OracleParameterCollection to use</param>
        /// <param name="name">Name of parameter</param>
        /// <param name="value">Value of parameter</param>
        public static void AddInParam(this OracleParameterCollection parameters, string name, char value)
        {
            parameters.Add(new OracleParameter() { ParameterName = name, OracleDbType = OracleDbType.Varchar2, Size = 1, Value = value, Direction = ParameterDirection.Input });
        }
        /// <summary>
        /// Add an In parameter of int type
        /// </summary>
        /// <param name="parameters">OracleParameterCollection to use</param>
        /// <param name="name">Name of parameter</param>
        /// <param name="value">Value of parameter</param>
        public static void AddInParam(this OracleParameterCollection parameters, string name, int value)
        {
            parameters.Add(new OracleParameter() { ParameterName = name, OracleDbType = OracleDbType.Int32, Size = 0, Value = value, Direction = ParameterDirection.Input });
        }
        /// <summary>
        /// Add an In parameter of int? type
        /// </summary>
        /// <param name="parameters">OracleParameterCollection to use</param>
        /// <param name="name">Name of parameter</param>
        /// <param name="value">Value of parameter</param>
        public static void AddInParam(this OracleParameterCollection parameters, string name, int? value)
        {
            parameters.Add(new OracleParameter() { ParameterName = name, OracleDbType = OracleDbType.Int32, Size = 0, Value = value, Direction = ParameterDirection.Input });
        }
        /// <summary>
        /// Add an In parameter of decimal type
        /// </summary>
        /// <param name="parameters">OracleParameterCollection to use</param>
        /// <param name="name">Name of parameter</param>
        /// <param name="value">Value of parameter</param>
        public static void AddInParam(this OracleParameterCollection parameters, string name, decimal value)
        {
            parameters.Add(new OracleParameter() { ParameterName = name, OracleDbType = OracleDbType.Decimal, Size = 0, Value = value, Direction = ParameterDirection.Input });
        }
        /// <summary>
        /// Add an In parameter of double type
        /// </summary>
        /// <param name="parameters">OracleParameterCollection to use</param>
        /// <param name="name">Name of parameter</param>
        /// <param name="value">Value of parameter</param>
        public static void AddInParam(this OracleParameterCollection parameters, string name, double value)
        {
            parameters.Add(new OracleParameter() { ParameterName = name, OracleDbType = OracleDbType.Double, Size = 0, Value = value, Direction = ParameterDirection.Input });
        }
        /// <summary>
        /// Add an In parameter of DateTime type
        /// </summary>
        /// <param name="parameters">OracleParameterCollection to use</param>
        /// <param name="name">Name of parameter</param>
        /// <param name="value">Value of parameter</param>
        public static void AddInParam(this OracleParameterCollection parameters, string name, DateTime value)
        {
            parameters.Add(new OracleParameter() { ParameterName = name, OracleDbType = OracleDbType.Date, Size = 0, Value = value, Direction = ParameterDirection.Input });
        }
        private static string TimeSpanToTimeZoneString(TimeSpan ts)
        {
            if (ts.TotalMinutes < 0)
            {
                ts = ts.Negate();
                return $"-{ts.Hours}:{ts.Minutes}";
            }
            return $"{ts.Hours}:{ts.Minutes}";
        }
        /// <summary>
        /// Add an In parameter of DateTimeOffset type
        /// </summary>
        /// <param name="parameters">OracleParameterCollection to use</param>
        /// <param name="name">Name of parameter</param>
        /// <param name="value">Value of parameter</param>
        public static void AddInParam(this OracleParameterCollection parameters, string name, DateTimeOffset value)
        {
            OracleTimeStampTZ oracleValue = new OracleTimeStampTZ(value.LocalDateTime, TimeSpanToTimeZoneString(value.Offset));
            parameters.Add(new OracleParameter() { ParameterName = name, OracleDbType = OracleDbType.TimeStampTZ, Size = 0, Value = oracleValue, Direction = ParameterDirection.Input });
        }
        /// <summary>
        /// Add an In parameter of TimeSpan type
        /// </summary>
        /// <param name="parameters">OracleParameterCollection to use</param>
        /// <param name="name">Name of parameter</param>
        /// <param name="value">Value of parameter</param>
        public static void AddInParam(this OracleParameterCollection parameters, string name, TimeSpan value)
        {
            parameters.Add(new OracleParameter() { ParameterName = name, OracleDbType = OracleDbType.IntervalDS, Size = 0, Value = value, Direction = ParameterDirection.Input });
        }
        /// <summary>
        /// Add an In parameter with a db null value
        /// </summary>
        /// <param name="parameters">OracleParameterCollection to use</param>
        /// <param name="name">Name of parameter</param>
        /// <param name="type">Type of parameter</param>
        public static void AddNullInParam(this OracleParameterCollection parameters, string name, OracleDbType type)
        {
            parameters.Add(new OracleParameter() { ParameterName = name, OracleDbType = type, Size = 0, Value = DBNull.Value, Direction = ParameterDirection.Input });
        }
        /// <summary>
        /// Add output parameter of specified type.
        /// </summary>
        /// <param name="parameters">OracleParameterCollection to use</param>
        /// <param name="name">Name of parameter</param>
        /// <param name="type">Type of parameter</param>
        /// <param name="size">Size of parameter (mostly for Varchar2 out params)</param>
        public static void AddOutParam(this OracleParameterCollection parameters, string name, OracleDbType type, int size)
        {
            parameters.Add(new OracleParameter() { ParameterName = name, OracleDbType = type, Size = size, Direction = ParameterDirection.Output });
        }
        /// <summary>
        /// Get the value of a string output parameter.
        /// </summary>
        /// <param name="parameters">OracleParameterCollection to use</param>
        /// <param name="name">Name of parameter</param>
        /// <returns>Value of the out param, null if database provided NULL</returns>
        public static string GetOutParamString(this OracleParameterCollection parameters, string name)
        {
            var parameter = parameters[name];
            if (parameter.Value is OracleString)
            {
                var innerValue = (OracleString)parameter.Value;
                return innerValue.IsNull ? null : (string)innerValue;
            }
            throw new ArgumentException($"Oracle Parameters type not numeric. Actual type {parameter.OracleDbType}");
        }
        /// <summary>
        /// Get the value of a int output parameter.
        /// </summary>
        /// <param name="parameters">OracleParameterCollection to use</param>
        /// <param name="name">Name of parameter</param>
        /// <returns>Value of the out param, null if database provided NULL</returns>
        public static Int32? GetOutParamInt32(this OracleParameterCollection parameters, string name)
        {
            var parameter = parameters[name];
            if (parameter.Value is OracleDecimal)
            {
                var innerValue = (OracleDecimal)parameter.Value;
                return innerValue.IsNull ? (Int32?)null : (Int32)innerValue;
            }
            throw new ArgumentException($"Oracle Parameters type not numeric. Actual type {parameter.OracleDbType}");
        }
        /// <summary>
        /// Get the value of a DateTime output parameter.
        /// </summary>
        /// <param name="parameters">OracleParameterCollection to use</param>
        /// <param name="name">Name of parameter</param>
        /// <returns>Value of the out param, null if database provided NULL</returns>
        public static DateTime? GetOutParamDateTime(this OracleParameterCollection parameters, string name)
        {
            var parameter = parameters[name];
            if (parameter.Value is OracleDate)
            {
                var innerValue = (OracleDate)parameter.Value;
                return innerValue.IsNull ? (DateTime?)null : (DateTime)innerValue;
            }
            throw new ArgumentException($"Oracle Parameters type not datetime. Actual type {parameter.OracleDbType}");
        }
        /// <summary>
        /// Get the value of a DateTimeOffset output parameter.
        /// </summary>
        /// <param name="parameters">OracleParameterCollection to use</param>
        /// <param name="name">Name of parameter</param>
        /// <returns>Value of the out param, null if database provided NULL</returns>
        public static DateTimeOffset? GetOutParamDateTimeOffset(this OracleParameterCollection parameters, string name)
        {
            var parameter = parameters[name];
            if (parameter.Value is OracleTimeStampTZ)
            {
                var innerValue = (OracleTimeStampTZ)parameter.Value;
                if (innerValue.IsNull)
                {
                    return null;
                }
                return new DateTimeOffset(innerValue.Value, innerValue.GetTimeZoneOffset());
            }
            throw new ArgumentException($"Oracle Parameters type not datetime. Actual type {parameter.OracleDbType}");
        }
        /// <summary>
        /// Get the value of a decimal output parameter.
        /// </summary>
        /// <param name="parameters">OracleParameterCollection to use</param>
        /// <param name="name">Name of parameter</param>
        /// <returns>Value of the out param, null if database provided NULL</returns>
        public static Decimal? GetOutParamDecimal(this OracleParameterCollection parameters, string name)
        {
            var parameter = parameters[name];
            if (parameter.Value is OracleDecimal)
            {
                var innerValue = (OracleDecimal)parameter.Value;
                return innerValue.IsNull ? (decimal?)null : (decimal)innerValue;
            }
            throw new ArgumentException($"Oracle Parameters type not numeric. Actual type {parameter.OracleDbType}");
        }
        /// <summary>
        /// Get the value of a ref cursor output parameter.
        /// </summary>
        /// <param name="parameters">OracleParameterCollection to use</param>
        /// <param name="name">Name of parameter</param>
        /// <returns>Value of the out param. Check value's IsNull to use.</returns>
        public static OracleRefCursor GetOutParamRefCursor(this OracleParameterCollection parameters, string name)
        {
            var parameter = parameters[name];
            if (parameter.Value is OracleRefCursor)
            {
                var innerValue = (OracleRefCursor)parameter.Value;
                return innerValue;
            }
            throw new ArgumentException($"Oracle Parameters type not datetime. Actual type {parameter.OracleDbType}");
        }
        /// <summary>
        /// Add input/output parameter of specified type.
        /// </summary>
        /// <param name="parameters">OracleParameterCollection to use</param>
        /// <param name="name">Name of parameter</param>
        /// <param name="value">Value of parameter to specify on input</param>
        /// <param name="size">Size of parameter (mostly for Varchar2 out params)</param>
        public static void AddInOutParam(this OracleParameterCollection parameters, string name, string value, int size)
        {
            parameters.Add(new OracleParameter() { ParameterName = name, OracleDbType = OracleDbType.Varchar2, Size = size, Value = value, Direction = ParameterDirection.InputOutput });
        }
    }
}
