﻿using System;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;

namespace BbTS.Resource.OracleManagedDataAccessExtensions
{
    /// <summary>
    /// Convenience methods for managing parameters for stored procedures on OracleCommand objects.
    /// </summary>
    public static class OracleCommandExtensions
    {
        /// <summary>
        /// Add an In parameter of string type
        /// </summary>
        /// <param name="command">OracleCommand to use</param>
        /// <param name="name">Name of parameter</param>
        /// <param name="value">Value of parameter</param>
        /// <param name="maxLength">Optional maximum length of the string.  This is a kludge to avoid a SQL max length exception.  Value will be unceremoniously truncated to this length.</param>
        public static void AddInParam(this OracleCommand command, string name, string value, int? maxLength = null)
        {
            command.Parameters.AddInParam(name, value, maxLength);
        }
        /// <summary>
        /// Add an In parameter of char type
        /// </summary>
        /// <param name="command">OracleCommand to use</param>
        /// <param name="name">Name of parameter</param>
        /// <param name="value">Value of parameter</param>
        public static void AddInParam(this OracleCommand command, string name, char value)
        {
            command.Parameters.AddInParam(name, value);
        }
        /// <summary>
        /// Add an In parameter of int type
        /// </summary>
        /// <param name="command">OracleCommand to use</param>
        /// <param name="name">Name of parameter</param>
        /// <param name="value">Value of parameter</param>
        public static void AddInParam(this OracleCommand command, string name, int value)
        {
            command.Parameters.AddInParam(name, value);
        }
        /// <summary>
        /// Add an In parameter of int? type
        /// </summary>
        /// <param name="command">OracleCommand to use</param>
        /// <param name="name">Name of parameter</param>
        /// <param name="value">Value of parameter</param>
        public static void AddInParam(this OracleCommand command, string name, int? value)
        {
            command.Parameters.AddInParam(name, value);
        }
        /// <summary>
        /// Add an In parameter of decimal type
        /// </summary>
        /// <param name="command">OracleCommand to use</param>
        /// <param name="name">Name of parameter</param>
        /// <param name="value">Value of parameter</param>
        public static void AddInParam(this OracleCommand command, string name, decimal value)
        {
            command.Parameters.AddInParam(name, value);
        }
        /// <summary>
        /// Add an In parameter of double type
        /// </summary>
        /// <param name="command">OracleCommand to use</param>
        /// <param name="name">Name of parameter</param>
        /// <param name="value">Value of parameter</param>
        public static void AddInParam(this OracleCommand command, string name, double value)
        {
            command.Parameters.AddInParam(name, value);
        }
        /// <summary>
        /// Add an In parameter of DateTime type
        /// </summary>
        /// <param name="command">OracleCommand to use</param>
        /// <param name="name">Name of parameter</param>
        /// <param name="value">Value of parameter</param>
        public static void AddInParam(this OracleCommand command, string name, DateTime value)
        {
            command.Parameters.AddInParam(name, value);
        }
        /// <summary>
        /// Add an In parameter of DateTimeOffset type
        /// </summary>
        /// <param name="command">OracleCommand to use</param>
        /// <param name="name">Name of parameter</param>
        /// <param name="value">Value of parameter</param>
        public static void AddInParam(this OracleCommand command, string name, DateTimeOffset value)
        {
            command.Parameters.AddInParam(name, value);
        }
        /// <summary>
        /// Add an In parameter of TimeSpan type
        /// </summary>
        /// <param name="command">OracleCommand to use</param>
        /// <param name="name">Name of parameter</param>
        /// <param name="value">Value of parameter</param>
        public static void AddInParam(this OracleCommand command, string name, TimeSpan value)
        {
            command.Parameters.AddInParam(name, value);
        }
        /// <summary>
        /// Add an In parameter with a db null value
        /// </summary>
        /// <param name="command">OracleCommand to use</param>
        /// <param name="name">Name of parameter</param>
        /// <param name="type">Type of parameter</param>
        public static void AddNullInParam(this OracleCommand command, string name, OracleDbType type)
        {
            command.Parameters.AddNullInParam(name, type);
        }
        /// <summary>
        /// Add output parameter of specified type.
        /// </summary>
        /// <param name="command">OracleCommand to use</param>
        /// <param name="name">Name of parameter</param>
        /// <param name="type">Type of parameter</param>
        /// <param name="size">Size of parameter (mostly for Varchar2 out params)</param>
        public static void AddOutParam(this OracleCommand command, string name, OracleDbType type, int size)
        {
            command.Parameters.AddOutParam(name, type, size);
        }
        /// <summary>
        /// Get the value of a string output parameter.
        /// </summary>
        /// <param name="command">OracleCommand to use</param>
        /// <param name="name">Name of parameter</param>
        /// <returns>Value of the out param, null if database provided NULL</returns>
        public static string GetOutParamString(this OracleCommand command, string name)
        {
            return command.Parameters.GetOutParamString(name);
        }
        /// <summary>
        /// Get the value of a int output parameter.
        /// </summary>
        /// <param name="command">OracleCommand to use</param>
        /// <param name="name">Name of parameter</param>
        /// <returns>Value of the out param, null if database provided NULL</returns>
        public static Int32? GetOutParamInt32(this OracleCommand command, string name)
        {
            return command.Parameters.GetOutParamInt32(name);
        }
        /// <summary>
        /// Get the value of a DateTime output parameter.
        /// </summary>
        /// <param name="command">OracleCommand to use</param>
        /// <param name="name">Name of parameter</param>
        /// <returns>Value of the out param, null if database provided NULL</returns>
        public static DateTime? GetOutParamDateTime(this OracleCommand command, string name)
        {
            return command.Parameters.GetOutParamDateTime(name);
        }
        /// <summary>
        /// Get the value of a DateTime output parameter.
        /// </summary>
        /// <param name="command">OracleCommand to use</param>
        /// <param name="name">Name of parameter</param>
        /// <returns>Value of the out param, null if database provided NULL</returns>
        public static DateTimeOffset? GetOutParamDateTimeOffset(this OracleCommand command, string name)
        {
            return command.Parameters.GetOutParamDateTimeOffset(name);
        }
        /// <summary>
        /// Get the value of a decimal output parameter.
        /// </summary>
        /// <param name="command">OracleCommand to use</param>
        /// <param name="name">Name of parameter</param>
        /// <returns>Value of the out param, null if database provided NULL</returns>
        public static Decimal? GetOutParamDecimal(this OracleCommand command, string name)
        {
            return command.Parameters.GetOutParamDecimal(name);
        }
        /// <summary>
        /// Get the value of a ref cursor output parameter.
        /// </summary>
        /// <param name="command">OracleCommand to use</param>
        /// <param name="name">Name of parameter</param>
        /// <returns>Value of the out param. Check value's IsNull to use.</returns>
        public static OracleRefCursor GetOutParamRefCursor(this OracleCommand command, string name)
        {
            return command.Parameters.GetOutParamRefCursor(name);
        }
        /// <summary>
        /// Add input/output parameter of specified type.
        /// </summary>
        /// <param name="command">OracleCommand to use</param>
        /// <param name="name">Name of parameter</param>
        /// <param name="value">Value of parameter to specify on input</param>
        /// <param name="size">Size of parameter (mostly for Varchar2 out params)</param>
        public static void AddInOutParam(this OracleCommand command, string name, string value, int size)
        {
            command.Parameters.AddInOutParam( name, value, size );
        }
    }
}
