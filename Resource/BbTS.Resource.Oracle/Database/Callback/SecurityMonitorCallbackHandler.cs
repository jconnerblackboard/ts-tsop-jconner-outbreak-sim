﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using BbTS.Core.Conversion;
using BbTS.Core.Serialization;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.DoorAccess;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.Messaging.SecurityMonitor;
using BbTS.Monitoring.Logging;
using BbTS.Resource.Database.Abstract;
using Oracle.ManagedDataAccess.Client;

namespace BbTS.Resource.Database.Callback
{
    /// <summary>
    /// Statis Change Event:
    /// Triggering this event will happen when an event is
    /// triggered in the database for alert incidents.
    /// </summary>
    public delegate void StatusChangeEvent(DoorStatus status);

    /// <summary>
    /// Tool class for handling callback messages from an OracleDependency object.
    /// </summary>
    public class SecurityMonitorCallbackHandler
    {
        private Thread _callbackThread;
        private static readonly object ResourceLock = new object();

        private static readonly List<RegisterSignalListRequest> _registrations = new List<RegisterSignalListRequest>();

        private static bool _watchDbmsAlert;

        private static ResourceDatabase _databaseResource;

        /// <summary>
        /// The handle to the resource layer.
        /// </summary>
        public static ResourceDatabase DatabaseResource
        {
            get
            {
                lock (ResourceLock)
                {
                    return _databaseResource;
                }
            }
            set
            {
                lock (ResourceLock)
                {
                    _databaseResource = value;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static event StatusChangeEvent DoorStatusChangeEvent;

        /// <summary>
        /// Singleton access.
        /// </summary>
        public static SecurityMonitorCallbackHandler Instance { get; internal set; } = new SecurityMonitorCallbackHandler();

        private SecurityMonitorCallbackHandler()
        {
        }

        /// <summary>
        /// Start the callback listener.
        /// </summary>
        /// <param name="databaseResource">handle to data layer resources</param>
        public void StartCallbackListener(ResourceDatabase databaseResource)
        {
            DatabaseResource = databaseResource;

            _watchDbmsAlert = true;

            _callbackThread = new Thread(_watchForAlerts) { IsBackground = true };
            _callbackThread.Start();
        }

        /// <summary>
        /// Register the callback listener for the requested doors.
        /// </summary>
        public void RegisterCallbackListener(RegisterSignalListRequest request)
        {
            DatabaseResource.RegisterSignalList(request);
        }

        /// <summary>
        /// Reconnect all doors to the DBMS_ALERT.
        /// </summary>
        public void ReconnectAllDoors()
        {
            foreach (var request in _registrations)
            {
                RegisterCallbackListener(request);
            }
        }

        /// <summary>
        /// Add a set of doors to the watch list.
        /// </summary>
        /// <param name="doors">The doors to watch.</param>
        /// <param name="username">The username that is requesting the registration.</param>
        public void WatchDoors(List<int> doors, string username)
        {
            var registrations = doors.Select(d => new RegisterSignalListRequest { ListId = d, Username = username, RequestId = Guid.NewGuid().ToString("D") }).ToList();
            var notInList = registrations.Except(_registrations, new RegistrationSignalListRequestComparer()).ToList();
            _registrations.AddRange(notInList);

            foreach (var request in notInList)
            {
                RegisterCallbackListener(request);
            }
        }

        /// <summary>
        /// Watch for and handle dbms_alert event callbacks.
        /// </summary>
        private void _watchForAlerts()
        {
            while (_watchDbmsAlert)
            {
                try
                {
                    var response = DatabaseResource.WaitForDaMonitorEvent();
                    if (!string.IsNullOrWhiteSpace(response.Message))
                    {
                        var resultRow = Xml.Deserialize<DbmsDoorStateSet>(response.Message);
                        var doorStatus = new DoorStatus
                        {
                            Address = resultRow.Row?.DoorAddress ?? -1,
                            ControlMode = resultRow.Row?.DoorControlMode?.ToString(),
                            DoorId = resultRow.Row?.DoorId ?? -1,
                            IsExitCycle = Formatting.TfStringToBool(resultRow.Row?.IsExitCycle),
                            IsForced = null,
                            IsHeld = null,
                            IsOpen = Formatting.TfStringToBool(resultRow.Row?.IsOpen),
                            IsTampered = null,
                            IsUnlocked = Formatting.TfStringToBool(resultRow.Row?.IsExitCycle),
                            MasterControllerId = resultRow.Row?.MasterControllerId ?? -1,
                            Name = resultRow.Row?.DoorName,
                            OverrideMode = resultRow.Row?.OverrideStatus?.ToString()
                        };
                        OnDoorStatusChangeEvent(doorStatus);
                    }
                }
                catch (ResourceLayerOracleException roex)
                {
                    if (roex.ErrorCode != 20000 && roex.ErrorCode != 10024)
                    {
                        LoggingManager.Instance.LogException(
                            roex, EventLogEntryType.Error,
                            (short)LoggingDefinitions.Category.SecurityMonitor,
                            (int)LoggingDefinitions.EventId.ResourceLayerExceptionDbmsWaitForEventFailed);
                    }
                    else
                    {
                        ReconnectAllDoors();
                    }
                }
                catch (Exception ex)
                {
                    LoggingManager.Instance.LogException(
                        ex, EventLogEntryType.Error,
                        (short)LoggingDefinitions.Category.SecurityMonitor,
                        (int)LoggingDefinitions.EventId.ResourceLayerExceptionDbmsWaitForEventFailed);
                }

                Thread.Sleep(3000);
            }
        }

        /// <summary>
        /// Event invoker for door status change
        /// </summary>
        private static void OnDoorStatusChangeEvent(DoorStatus status)
        {
            DoorStatusChangeEvent?.Invoke(status);
        }
    }
}
