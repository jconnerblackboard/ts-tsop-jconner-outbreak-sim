﻿using System;
using System.Diagnostics;
using System.Threading;
using BbTS.Core;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.Notification;
using BbTS.Monitoring.Logging;
using BbTS.Resource.Database.Abstract;
using Oracle.ManagedDataAccess.Client;

namespace BbTS.Resource.Database.Callback
{
    /// <summary>
    /// Statis Change Event:
    /// Triggering this event will happen when an event is
    /// triggered in the database for alert incidents.
    /// </summary>
    public delegate void NotificationEvent();

    /// <summary>
    /// Tool class for handling callback messages from an OracleDependency object.
    /// </summary>
    public class EventHubCallbackHandler
    {
        /// <summary>
        /// Event for callback notification.
        /// </summary>
        public static event NotificationEvent NotificationEvent;
        private static readonly object ResourceLock = new object();

        private Thread _callbackThread;
        private static bool _doWaitLoop;

        private static OracleConnection _connection;
        private static ResourceDatabase _databaseResource;

        private EventHubCallbackHandler()
        {
        }

        /// <summary>
        /// The handle to the resource layer.
        /// </summary>
        /// <summary>
        /// The handle to the resource layer.
        /// </summary>
        public static ResourceDatabase DatabaseResource
        {
            get
            {
                lock (ResourceLock)
                {
                    return _databaseResource;
                }
            }
            set
            {
                lock (ResourceLock)
                {
                    _databaseResource = value;
                    _connection = new OracleConnection(_databaseResource.ConnectionString);
                }
            }
        }

        /// <summary>
        /// Indicates whether or not the handler is registered to receive notification callbacks.
        /// </summary>
        public bool IsRegistered { get; set; }

        /// <summary>
        /// Singleton instance.
        /// </summary>
        public static EventHubCallbackHandler Instance { get; set; } = new EventHubCallbackHandler();


        /// <summary>
        /// Start the callback listener.
        /// </summary>
        public void StartCallbackListener(ResourceDatabase databaseResource)
        {
            // Make sure there is a database resource before continuing.
            Guard.IsNotNull(databaseResource, nameof(databaseResource));

            DatabaseResource = databaseResource;

            // If registration hasn't happened yet, then register for callbacks.
            if (!IsRegistered) _registerCallbackListener();

            _doWaitLoop = true;

            _callbackThread = new Thread(_listenForNotifications) { IsBackground = true };
            _callbackThread.Start();
        }

        /// <summary>
        /// Register the callback listener for the requested doors.
        /// </summary>
        private void _registerCallbackListener()
        {
            Guard.IsNotNull(DatabaseResource, nameof(DatabaseResource));
            DatabaseResource.NotificationEventRegister(new NotificationEventRegisterRequest(), _connection);
            IsRegistered = true;
        }

        /// <summary>
        /// Reestablish
        /// </summary>
        private void _reestablishConnection()
        {
            _connection.Dispose();
            _connection = new OracleConnection(_databaseResource.ConnectionString);
            IsRegistered = false;
            _registerCallbackListener();
        }

        /// <summary>
        /// Watch for and handle dbms_alert event callbacks.
        /// </summary>
        private void _listenForNotifications()
        {
            while (_doWaitLoop)
            {
                try
                {
                    var response = DatabaseResource.NotificationEventCallbackWait(new NotificationEventCallbackRequest { WaitTime = 3}, _connection);
                    if (response.Status != null && response.Status == 0)
                    {
                        OnNotificationReceived();
                    }
                }
                catch (ResourceLayerOracleException roex)
                {
                    if (roex.ErrorCode != 20000 && roex.ErrorCode != 10024)
                    {
                        LoggingManager.Instance.LogException(
                            "DMBS_ALERT connection was terminated.  Restarting the listener.\r\n\r\n" +
                            $"Additional Information:\r\n{Formatting.FormatException(roex)}",
                            EventLogEntryType.Warning,
                            (short)LoggingDefinitions.Category.EventHubAgent,
                            (int)LoggingDefinitions.EventId.EventHubAgentDbmsAlertListenerFailed);

                        // Reestablish the DBMS_ALERT listener.
                        try
                        {
                            _reestablishConnection();
                        }
                        catch (Exception ex)
                        {
                            LoggingManager.Instance.LogException(
                                ex, EventLogEntryType.Error,
                                (short)LoggingDefinitions.Category.EventHubAgent,
                                (int)LoggingDefinitions.EventId.EventHubAgentDbmsAlertRegistrationFailed);
                        }
                    }
                }
                catch (Exception ex)
                {
                    LoggingManager.Instance.LogException(
                        ex, EventLogEntryType.Error,
                        (short)LoggingDefinitions.Category.EventHubAgent,
                        (int)LoggingDefinitions.EventId.ResourceLayerExceptionDbmsWaitForEventFailed);
                }

                Thread.Sleep(1000);
            }
        }

        /// <summary>
        /// Fire an event when a notification change callback is received.
        /// </summary>
        private static void OnNotificationReceived()
        {
            NotificationEvent?.Invoke();
        }
    }
}
