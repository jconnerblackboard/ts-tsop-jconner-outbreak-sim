﻿#region Using Statements

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using BbTS.Core.Conversion;
using BbTS.Core.Security.Encryption;
using BbTS.Core.Serialization;
using BbTS.Domain.Models.BbSp;
using BbTS.Domain.Models.BoardPlan;
using BbTS.Domain.Models.Cashier;
using BbTS.Domain.Models.Configuration;
using BbTS.Domain.Models.Container;
using BbTS.Domain.Models.Credential;
using BbTS.Domain.Models.Customer;
using BbTS.Domain.Models.Definitions;
using BbTS.Domain.Models.Definitions.Container;
using BbTS.Domain.Models.Definitions.Device;
using BbTS.Domain.Models.Definitions.Licensing;
using BbTS.Domain.Models.Device;
using BbTS.Domain.Models.DoorAccess;
using BbTS.Domain.Models.EAccount;
using BbTS.Domain.Models.Event;
using BbTS.Domain.Models.Exceptions.WebApi;
using BbTS.Domain.Models.Operator;
using BbTS.Domain.Models.Policy;
using BbTS.Domain.Models.Pos;
using BbTS.Domain.Models.Product;
using BbTS.Domain.Models.Products;
using BbTS.Domain.Models.ProfitCenter;
using BbTS.Domain.Models.RetailTransaction;
using BbTS.Domain.Models.Retail_Tran;
using BbTS.Domain.Models.Retail_Tran_LineItem;
using BbTS.Domain.Models.Retail_Tran_LineItem_Comment;
using BbTS.Domain.Models.Retail_Tran_LineItem_Discount;
using BbTS.Domain.Models.Retail_Tran_LineItem_Mdb_Info;
using BbTS.Domain.Models.Retail_Tran_LineItem_Prod;
using BbTS.Domain.Models.Retail_Tran_LineItem_ProdPrcMd;
using BbTS.Domain.Models.Retail_Tran_LineItem_ProdPromo;
using BbTS.Domain.Models.Retail_Tran_LineItem_Prod_Tax;
using BbTS.Domain.Models.Retail_Tran_LineItem_Prod_Tx_E;
using BbTS.Domain.Models.Retail_Tran_LineItem_Prod_Tx_O;
using BbTS.Domain.Models.Retail_Tran_LineItem_StoredVal;
using BbTS.Domain.Models.Retail_Tran_LineItem_Surcharge;
using BbTS.Domain.Models.Retail_Tran_LineItem_Sv_Card;
using BbTS.Domain.Models.Retail_Tran_LineItem_Sv_Cust;
using BbTS.Domain.Models.Retail_Tran_LineItem_Sv_Enrich;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tax;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tender;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tender_Cc;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tender_Sv;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tndr_Card;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tndr_Cdv;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tndr_Ce;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tndr_Cgv;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tndr_Cust;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tndr_Disc;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tndr_Srch;
using BbTS.Domain.Models.Retail_Tran_Total;
using BbTS.Domain.Models.Session_Control;
using BbTS.Domain.Models.StoredValue;
using BbTS.Domain.Models.StoredValueDenial;
using BbTS.Domain.Models.Sv_Account_History_Rtl_Trn_Sv;
using BbTS.Domain.Models.Sv_Account_History_Rtl_Trn_Tdr;
using BbTS.Domain.Models.System;
using BbTS.Domain.Models.System.Database;
using BbTS.Domain.Models.System.Logging;
using BbTS.Domain.Models.System.Security;
using BbTS.Domain.Models.System.Security.Oauth;
using BbTS.Domain.Models.TiaTransactionLog;
using BbTS.Domain.Models.Transaction;
using BbTS.Domain.Models.Transaction.Processing;
using BbTS.Domain.Models.Transaction.Validation;
using BbTS.Domain.Models.TransactionObjectLog;
using BbTS.Domain.Models.Transaction_Cashier;
using BbTS.Domain.Models.Transaction_Communication;
using BbTS.Domain.Models.Transaction_External_Client;
using BbTS.Domain.Models.Transaction_Laundry_Machine;
using BbTS.Domain.Models.XmlDocument;
using Oracle.ManagedDataAccess.Client;
using BbTS.Domain.Models.Card;
using BbTS.Domain.Models.Communication.BbHost;
using BbTS.Domain.Models.Credential.Mobile;
using BbTS.Domain.Models.Customer.Management;
using BbTS.Domain.Models.Customer.Management.BoardPlan;
using BbTS.Domain.Models.Customer.Management.Email;
using BbTS.Domain.Models.Customer.Management.EventPlan;
using BbTS.Domain.Models.Customer.Management.StoredValue;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Definitions.Monitoring;
using BbTS.Domain.Models.Definitions.System;
using BbTS.Domain.Models.Merchant;
using BbTS.Domain.Models.Report;
using BbTS.Domain.Models.EventLog;
using BbTS.Domain.Models.ExternalClient;
using BbTS.Domain.Models.IdWorks;
using BbTS.Domain.Models.Institution;
using BbTS.Domain.Models.Transaction.WebApi;
using BbTS.Domain.Models.Messaging.HostMonitor;
using BbTS.Domain.Models.Messaging.SecurityMonitor;
using BbTS.Domain.Models.Messaging.Subscription;
using BbTS.Domain.Models.Messaging.Subscription.Rest;
using BbTS.Domain.Models.Notification;
using BbTS.Domain.Models.Pos.PosOptions;
using BbTS.Domain.Models.Security.Doors;
using BbTS.Domain.Models.Transaction.StoredValue;
using BbTS.Domain.Models.Definitions.Transaction;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.Security.DoorApiKeys;
using BbTS.Domain.Models.Object;
using BbTS.Domain.Models.Terminal;
using Oracle.ManagedDataAccess.Types;

#endregion

namespace BbTS.Resource.Database.Abstract
{
    /// <summary>
    /// This class contains the database resource objects
    /// </summary>
    public abstract class ResourceDatabase
    {
        #region Common Properties


        /// <summary>
        /// This represents a connection string for this resource
        /// </summary>
        public string ConnectionString { get; set; }

        #endregion

        #region CommonFunctions

        /// <summary>
        /// This method creates an Oracle database connection
        /// </summary>
        /// <param name="con">The database connection information</param>
        /// <returns></returns>
        public ConnectionInfo ConnectionInfoGet(ConnectionInfo con)
        {
            if (string.IsNullOrEmpty(con.ServerName))
                throw new ArgumentException("Missing computer name.  Please correct before continuing.");

            if (con.Port < 0 || con.Port > 65535)
                throw new ArgumentException("Port number is invalid.  Please correct before continuing.");

            if (string.IsNullOrEmpty(con.ServiceName))
                throw new ArgumentException("Missing service name.  Please correct before continuing.");

            if (string.IsNullOrEmpty(con.User))
                throw new ArgumentException("Missing user name.  Please correct before continuing.");

            if (string.IsNullOrEmpty(con.Password))
                throw new ArgumentException("Missing password.  Please correct before continuing.");

            return new ConnectionInfo { User = con.User, Password = con.Password, Port = con.Port, ServiceName = con.ServiceName, ServerName = con.ServerName };
        }

        /// <summary>
        /// This method will return an OracleConnection object
        /// </summary>
        /// <param name="info">The connection information required to create an OracleConnection object</param>
        /// <returns>An <see cref="OracleConnection"/> object</returns>
        internal static OracleConnection OracleConnectionGet(ConnectionInfo info)
        {
            return new OracleConnection
            {
                ConnectionString = $"User Id={info.User};Password={info.Password};Data Source={info.ServerName}:{info.Port}/{info.ServiceName}{(info.User.ToLower().Equals("sys") ? ";DBA Privilege=SYSDBA" : string.Empty)}"
            };
        }

        /// <summary>
        /// This method will return an OracleConnection object
        /// </summary>
        /// <param name="connectionString">Fully formed connection string including password.</param>
        /// <returns>An <see cref="OracleConnection"/> object</returns>
        internal static OracleConnection OracleConnectionGet(string connectionString)
        {
            return new OracleConnection { ConnectionString = connectionString };
        }

        /// <summary>
        /// This method takes an object and generates a string based Md5Hash value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item">The object to have an Md5Hash calculated</param>
        /// <returns>A Md5Hash string</returns>
        internal static string Md5HashGet<T>(object item)
        {
            return Formatting.ByteArrayToString(EncryptionTool.ComputeMd5Hash(new MemoryStream(Encoding.UTF8.GetBytes(Xml.Serialize((T)item))))).ToLower();
        }

        /// <summary>
        /// This method will convert from the C# type to the OracleDbType
        /// </summary>
        /// <param name="type">The C# object type</param>
        /// <returns></returns>
        internal static OracleDbType DataTableTypeToOracleDbType(string type)
        {
            switch (type.Contains(".") ? type.Split('.')[1].ToUpper() : type.ToUpper())
            {
                case "ANSISTRINGFIXEDLENGTH":
                case "STRINGFIXEDLENGTH":
                    return OracleDbType.Char;
                case "BYTE[]":
                    return OracleDbType.Raw;
                case "INT16":
                case "UINT16":
                    return OracleDbType.Int16;
                case "INT32":
                case "UINT32":
                    return OracleDbType.Int32;
                case "SINGLE":
                    return OracleDbType.Single;
                case "DOUBLE":
                    return OracleDbType.Double;
                case "DATE":
                    return OracleDbType.Date;
                case "DATETIME":
                    return OracleDbType.TimeStamp;
                case "TIME":
                    return OracleDbType.IntervalDS;
                case "BINARY":
                    return OracleDbType.Blob;
                case "INT64":
                case "UINT64":
                    return OracleDbType.Int64;
                case "VARNUMERIC":
                case "DECIMAL":
                case "CURRENCY":
                    return OracleDbType.Decimal;
                case "GUID":
                    return OracleDbType.Raw;
                default:
                    return OracleDbType.Varchar2;
            }
        }

        /// <summary>
        /// This method will convert a local date time to be UTC based
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        internal DateTime LocalDateTimeToUtc(DateTime datetime)
        {
            return TimeZoneInfo.ConvertTimeToUtc(datetime, TimeZoneInfo.Local);
        }

        /// <summary>
        /// Convert an <see cref="OracleTimeStampTZ"/> to a nullable <see cref="DateTimeOffset"/>
        /// </summary>
        /// <param name="od">Oracle timestamp value</param>
        /// <returns>DateTimeOffset or null</returns>
        internal static DateTimeOffset? DateTimeOffsetFromOracleTimeStamp(OracleTimeStampTZ od)
        {
            if(od != null)
            {
                return new DateTimeOffset(od.Year, od.Month, od.Day, od.Hour, od.Minute, od.Second, (int)od.Millisecond, od.GetTimeZoneOffset());
            }
            return null;
        }

    #endregion

        #region Authentication Functions

        /// <summary>
        /// Set an authentication oauth token in the database layer
        /// </summary>
        /// <param name="token">The token to store in the database layer.</param>
        public abstract void AuthenticationOauthTokenSet(AuthenticationOauthToken token);

        /// <summary>
        /// Get an authentication oauth token from the database layer.
        /// </summary>
        /// <param name="key">The token key to retrieve from the store.</param>
        /// <returns>Stored token.</returns>
        public abstract AuthenticationOauthToken AuthenticationOauthTokenGet(string key);

        /// <summary>
        /// Query the data layer for the first consumer key and secret.
        /// </summary>
        /// <returns>First result of the consumer key and secret.</returns>
        public abstract OAuthParameters ConsumerKeyAndSecretGetFirstValue();

        /// <summary>
        /// Query the data layer for all of the consumer key and secret information.
        /// </summary>
        /// <returns>list of consumer key and secret information.</returns>
        public abstract List<BbSpApplicationCredential> ConsumerKeyAndSecretGetAllValues();

        /// <summary>
        /// Query the data layer for all of the merchant information.
        /// </summary>
        /// <returns>List of merchants.</returns>
        public abstract List<BbSpMerchant> BbSpMerchantsGetAll();

        /// <summary>
        /// Check if object can be called with defined authentication key
        /// </summary>
        /// <param name="authenticationKey">Authentication key</param>
        /// <param name="objectName">Object to be called</param>
        /// <returns>True if the call is allowed</returns>
        public abstract bool ApiPermissionIsAllowed(string authenticationKey, string objectName);

        #endregion

        #region BbSP_Functions

        /// <summary>
        /// THis method will retrieve a TransactionSystem object
        /// </summary>
        /// <returns></returns>
        public abstract TransactionSystem TransactionSystemGet();

        /// <summary>
        /// This method will add a TransactionSystem object to the database
        /// </summary>
        /// <param name="transactionSystem"></param>
        public abstract void TransactionSystemAdd(TransactionSystem transactionSystem);

        /// <summary>
        /// Delete all transaciton systems in this database resource instance.
        /// </summary>
        public abstract void TransactionSystemsDelete();

        /// <summary>
        /// Delete all application credentials in this database resource instance.
        /// </summary>
        public abstract void ApplicationCredentialsDelete();

        /// <summary>
        /// RefreshApplicationCredentialData : Deletes all Application Credential data from database and insert application credential data received from Service Agent.
        /// </summary>
        /// <param name="applicationCredentials">The application credential data entities.</param>
        public abstract void RefreshApplicationCredentialData(IEnumerable<SpApplicationCredential> applicationCredentials);

        /// <summary>
        /// Gets application credential by consumer key.
        /// </summary>
        /// <param name="consumerKey">The consumer key on which to base the query for application credential</param>
        /// <param name="institutionId">The institution identifier.</param>
        /// <returns>The application credential by consumer key.</returns>
        public abstract SpApplicationCredential ApplicationCredentialGet(Guid consumerKey, Guid? institutionId = null);

        /// <summary>
        /// Delete all institution routes in this database resource instance.
        /// </summary>
        public abstract void InstitutionRoutesDelete();

        /// <summary>
        /// RefreshInstitutionRouteData : Deletes all institution route data from database and insert institution route data received from Service Agent.
        /// </summary>
        /// <param name="institutionRoutes">The institution route data entities.</param>
        public abstract void RefreshInstitutionRouteData(IEnumerable<SpInstitutionRouteEntity> institutionRoutes);

        /// <summary>
        /// Gets the institution Route by Route which contains Institution details.
        /// </summary>
        /// <param name="schemeTypeApiKey">The institution Route Scheme Type.</param>
        /// <param name="value">The institution Route Value.</param>
        /// <param name="institutionId">The institution identifier.</param>
        /// <returns>The institution Route which contains Institution details.</returns>
        public abstract SpInstitutionRouteEntity InstitutionRouteGet(string schemeTypeApiKey, string value, Guid? institutionId = null);

        /// <summary>
        /// Delete all merchants in this database resource instance.
        /// </summary>
        public abstract void MerchantsDelete();

        /// <summary>
        /// RefreshMerchantData : Deletes all data from database and insert merchant data received from Service Agent.
        /// </summary>
        /// <param name="merchants">The merchant data entities.</param>
        public abstract void RefreshMerchantData(IEnumerable<SpMerchantEntity> merchants);

        /// <summary>
        /// Gets the Merchant by Id.
        /// </summary>
        /// <param name="merchantId">The Merchant id.</param>
        /// <param name="institutionId">The institution identifier.</param>
        /// <returns>The Merchant</returns>
        public abstract SpMerchantEntity MerchantGet(Guid merchantId, Guid? institutionId = null);

        #endregion

        #region BoardFunctions

        /// <summary>
        /// Get a list of all board cash equivalency periods for the device.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>List of all board cash equivalency periods for the device.</returns>
        public abstract BoardCashEquivalencyPeriodsGetResponse BoardCashEquivalencyPeriodsGet(BoardCashEquivalencyPeriodsGetRequest request);

        /// <summary>
        /// Get board information for a customer for use on a specified device.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Board information for a customer for use on a specified device.</returns>
        public abstract BoardInformationGetResponse BoardInformationGet(BoardInformationGetRequest request);

        /// <summary>
        /// This method will return a list of records for the BoardMealTypes objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public abstract List<TsBoardMealTypes> BoardMealTypesArchivesGet(ConnectionInfo connection);

        /// <summary>
        /// This method will return a list of records for the BoardMealTypes objects permitted for the given deviceId
        /// </summary>
        /// <param name="deviceId">The unique identifier for the device.</param>
        /// <returns></returns>
        public abstract List<TsBoardMealTypes> BoardMealTypesPosGet(string deviceId);

        /// <summary>
        /// Get board meal types.
        /// </summary>
        /// <returns>Board meal types.</returns>
        public abstract List<MealTypeDeviceSetting> BoardMealTypesGet();

        /// <summary>
        /// Get a list of board periods for a device.
        /// </summary>
        /// <param name="deviceId">The unique identifier for the device.</param>
        /// <returns>list of <see cref="BoardPeriod"/></returns>
        public abstract List<BoardPeriod> BoardPeriodsDeviceSettingsGet(string deviceId);

        /// <summary>
        /// Get the default board plan id for the device given its unique identifier.
        /// </summary>
        /// <param name="originatorGuid">The unique identifier of the originator.</param>
        /// <returns>The default board plan id for the device.</returns>
        public abstract int? DefaultBoardPlanIdFromOriginatorGuid(string originatorGuid);

        /// <summary>
        /// Process a board transaction and provide the result.
        /// </summary>
        /// <param name="request">Board transaction to process</param>
        /// <returns>Result of the board transaction with remaining counts.</returns>
        public abstract BoardTransactionProcessResponse BoardTransactionProcess(BoardTransactionProcessRequest request);

        /// <summary>
        /// Get list of board plan summaries
        /// </summary>
        /// <returns>Board plan summaries</returns>
        public abstract List<TsBoardPlanSummary> BoardPlanSummaryGetAll();

        /// <summary>
        /// Get board plan exclusions
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <returns>Board plan exclusions</returns>
        public abstract List<BoardPlanExclusion> PeriodExclusionListGet(int customerId);

        /// <summary>
        /// Get a board plan by numerical identifier.
        /// </summary>
        /// <param name="id">numerical identifier.</param>
        /// <returns>Board plan or null if not found.</returns>
        public abstract BoardPlanVerifyResponse BoardPlanVerifyById(int id);

        /// <summary>
        /// Get a list of customer locations based on board transactions between two specific dates.
        /// </summary>
        /// <param name="startDate">The start of the window.</param>
        /// <param name="endDate">The end of the window.</param>
        /// <returns></returns>
        public abstract List<CustomerTransactionLocationLog> CustomerBoardLocationLogsGet(DateTime startDate, DateTime endDate);

        #endregion

        #region DataAccess

        /// <summary>
        /// This method will bulk load a data table into a database
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="info"></param>
        public abstract void BulkLoadTable(DataTable dataTable, ConnectionInfo info);
        /// <summary>
        /// Thos method will return a data table based on the provided query
        /// </summary>
        /// <param name="info"></param>
        /// <param name="sqlCommand"></param>
        /// <param name="queryContainsLongData"></param>
        /// <returns></returns>
        public abstract DataTable DataTableGet(ConnectionInfo info, string sqlCommand, bool queryContainsLongData = false);
        /// <summary>
        /// This method will execute a query against a database that returns no data
        /// </summary>
        /// <param name="info"></param>
        /// <param name="sqlCommand"></param>
        public abstract void ExecuteNonQuery(ConnectionInfo info, string sqlCommand);
        /// <summary>
        /// This method will execute a query against a database wrapped within a transaction
        /// </summary>
        /// <param name="info"></param>
        /// <param name="sqlCommand"></param>
        /// <param name="commit"></param>
        public abstract void ExecuteTransactionalNonQuery(ConnectionInfo info, string sqlCommand, bool commit);
        /// <summary>
        /// This method will execute a query against the database that returns a single value
        /// </summary>
        /// <param name="info"></param>
        /// <param name="sqlCommand"></param>
        /// <returns></returns>
        public abstract object ExecuteScalar(ConnectionInfo info, string sqlCommand);
        /// <summary>
        /// This method will execute a query against the database and return a list of objects
        /// </summary>
        /// <param name="sqlCommand"></param>
        /// <param name="info"></param>
        /// <returns></returns>
        public abstract List<object[]> ExecuteReaderToObjectList(string sqlCommand, ConnectionInfo info);
        /// <summary>
        /// This method is used to test database connectivity
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public abstract Boolean ConnectionTest(ConnectionInfo info);
        /// <summary>
        /// This method will return the server version for a given database
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public abstract string ServerVersionGet(ConnectionInfo info);
        /// <summary>
        /// This method checks to see if a given user exists
        /// </summary>
        /// <param name="info"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public abstract bool DatabaseUserExists(ConnectionInfo info, string name);
        /// <summary>
        /// This method checks to see if a given table exists
        /// </summary>
        /// <param name="info"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public abstract bool TableExists(ConnectionInfo info, string name);

        #endregion

        #region CardFunctions

        /// <summary>
        /// This method will retrieve a list of cards from the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public abstract List<TsCard> CardGet(ConnectionInfo connection);

        /// <summary>
        /// Get a list of card formats for a device.
        /// </summary>
        /// <param name="deviceId">The unique identifier for the device.</param>
        /// <returns>List of <see cref="CardFormatForDeviceSettings"/> for the requested device.</returns>
        public abstract List<CardFormatForDeviceSettings> CardFormatDeviceSettingsGet(string deviceId);

        /// <summary>
        /// Get a list of card formats for a device.
        /// </summary>
        /// <returns>List of <see cref="CardFormatForDeviceSettings"/> for the requested device.</returns>
        public abstract List<TsCardFormat> CardFormatGet();

        /// <summary>
        /// This method will return a list of card to customer guid mapping objects.  If connection is null, <see cref="ConnectionString"/> will be used
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public abstract List<CardToCustomerGuidMapping> CardToCustomerGuidGet(ConnectionInfo connection = null);

        /// <summary>
        /// Get a card record by card number.
        /// </summary>
        /// <param name="cardNumber">Card number, left padded to 22 characters</param>
        /// <returns><see cref="TsCard"/> instance</returns>
        public abstract TsCard CardGetByCardNumber(string cardNumber);

            /// <summary>
        /// Get a card by its guid
        /// </summary>
        /// <param name="cardGuid">Card's guid</param>
        /// <returns>Card</returns>
        public abstract TsCard CardGetByGuid(string cardGuid);

        /// <summary>
        /// Get cards
        /// </summary>
        /// <param name="request">Filtering request</param>
        /// <returns>Cards</returns>
        public abstract List<TsCard> CardGetAll(CardGetAllRequest request);

        /// <summary>
        /// Removes temporary customer card
        /// </summary>
        /// <param name="cardNumber">Card number</param>
        public abstract void CardUpdateTemporary(string cardNumber);

        /// <summary>
        /// Adds new card to customer
        /// </summary>
        /// <param name="customerNumber">Customer number</param>
        /// <param name="card">Card</param>
        /// <returns>Created customer card</returns>
        public abstract CustomerCard CardCreate(string customerNumber, CustomerCard card);

        /// <summary>
        /// Updates existing customer card
        /// </summary>
        /// <param name="customerNumber">Customer number</param>
        /// <param name="cardNumber">Card number</param>
        /// <param name="card">card</param>
        /// <returns>Updated customer card</returns>
        public abstract void CardUpdate(string customerNumber, string cardNumber, PatchCustomerCard card);

        /// <summary>
        /// Create/Update a card.
        /// </summary>
        /// <param name="credential">Full card credential information.</param>
        public abstract void CardSet(CardCredentialComplete credential);

        /// <summary>
        /// Get a mobile card credential by card number.
        /// </summary>
        /// <param name="cardNumber">The unique identifier for the card.</param>
        /// <returns>Mobile credential information for the card.</returns>
        public abstract MobileCredentialGetResponse MobileCredentialGet(string cardNumber);

        /// <summary>
        /// Patch a mobile card credential by card number.
        /// </summary>
        /// <param name="request">The patch request.</param>
        /// <returns>Results of the operation.</returns>
        public abstract MobileCredentialPatchResponse MobileCredentialPatch(MobileCredentialPatchRequest request);

        /// <summary>
        /// Post a mobile card credential by card number.
        /// </summary>
        /// <param name="request">The post request.</param>
        /// <returns>Results of the operation.</returns>
        public abstract MobileCredentialPostResponse MobileCredentialPost(MobileCredentialPostRequest request);

        /// <summary>
        /// Get unassigned temporary cards
        /// </summary>
        /// <returns>Unassigned temporary cards</returns>
        public abstract List<TsCard> CardsTemporaryUnassignedGet();

        /// <summary>
        /// Check if card number is available
        /// </summary>
        /// <returns>Availability of card number</returns>
        public abstract CardNumberAvailableResponse CardNumberAvailable(decimal cardNumber);

        /// <summary>
        /// Get card assignment history.
        /// </summary>
        /// <param name="cardNumber"></param>
        /// <returns></returns>
        public abstract List<CardAssignmentHistoryItem> CardHistoryGet(string cardNumber);

        #endregion

        #region CashierFunctions
        /// <summary>
        /// This method will return a list of records for the Cashier objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public abstract List<TsCashier> CashierArchivesGet(ConnectionInfo connection);

        /// <summary>
        /// Get the cashier id value from the cashier Guid.
        /// </summary>
        /// <param name="cashierGuid">The unique identifier assigned to the cashier.</param>
        /// <returns>The cashier id as an int.</returns>
        public abstract int CashierIdFromGuidGet(string cashierGuid);

        #endregion

        #region CredentialFunctions

        /// <summary>
        /// Retrieve a full customer credential from the credential value.
        /// </summary>
        /// <param name="credentialValue">The credential value</param>
        /// <param name="cardNumber"></param>
        /// <returns>Full credential iu</returns>
        public abstract string CredentialValidateOnCardFormat(string credentialValue, out string cardNumber);

        /// <summary>
        /// Retrieve basic card credential information, validating card format in the process.
        /// </summary>
        /// <param name="credentialValue">Raw track 2 data.</param>
        /// <param name="requestId">The unique id assigned to the request.</param>
        /// <returns>Card format pieces.  Throws WebApiException on errors.</returns>
        public abstract BasicCardCredential BasicCardCredentialGet(string credentialValue, string requestId = null);

        /// <summary>
        /// Verifies that the customer and card is valid.
        /// </summary>
        /// <param name="cardCapture"></param>
        /// <param name="pin"></param>
        /// <param name="transactionClassification"></param>
        /// <param name="posId"></param>
        /// <param name="customerId"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public abstract ProcessingResult CustomerAndCardIsValid(CardCapture cardCapture, int? pin, TransactionClassification transactionClassification, int posId, out int customerId, ConnectionInfo connection = null);

        #endregion

        #region Credit Card Functions
        /// <summary>
        /// This method will retrieve PaymentExpressGroupAccounts
        /// </summary>
        /// <returns>A List of Payment Express Group Accounts</returns>
        public abstract List<PaymentExpressGroupAccount> PaymentExpressGroupAccountGet(string name = null);

        /// <summary>
        /// Persist EMV TxnPur request messages to the data layer.
        /// </summary>
        /// <param name="requestId">Unique identifer associated with the service layer request.</param>
        /// <param name="request">The request containing the line item information for EMV.</param>
        public abstract LineItemProcessingResult EmvTxnPurRequestSet(string requestId, RetailTransactionLineTenderCreditCardRequest request);

        /// <summary>
        /// Persist EMV TxnPur response messages to the data layer
        /// </summary>
        /// <param name="requestId">Unique identifer associated with the service layer request.</param>
        /// <param name="request">The request containing the line item information for EMV.</param>
        public abstract LineItemProcessingResult EmvTxnPurResponseSet(string requestId, RetailTransactionLineTenderCreditCardRequest request);

        /// <summary>
        /// Persist EMV Get1 response messages to the data layer
        /// </summary>
        /// <param name="requestId">Unique identifer associated with the service layer request.</param>
        /// <param name="request">The request containing the line item information for EMV.</param>
        public abstract LineItemProcessingResult EmvTxnGet1ResponseSet(string requestId, RetailTransactionLineTenderCreditCardRequest request);

        /// <summary>
        /// Persist EMV Hit Transaction Record request messages to the data layer.
        /// </summary>
        /// <param name="requestId">Unique identifer associated with the service layer request.</param>
        /// <param name="request">The request containing the line item information for EMV.</param>
        public abstract LineItemProcessingResult EmvHitTransactionRecord(string requestId, RetailTransactionLineTenderCreditCardHitRequest request);


        /// <summary>
        /// Retrieve EMV settings for HIT operations.
        /// </summary>
        /// <param name="PosGuid"></param>
        /// <returns>EmvHitSettings for POS.</returns>
        public abstract EmvHitSettings EmvGatewaySettingsHitGet(string PosGuid);


        #endregion

        #region CreditCardTypeFunctions
        /// <summary>
        /// This method will return a list of records for the CreditCardType objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public abstract List<TsCredit_Card_Type> CreditCardTypeArchivesGet(ConnectionInfo connection);
        #endregion

        #region CustomerFuctions
        /// <summary>
        /// This method will return the list of customers names in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public abstract List<TsCustomerArchive> CustomerArchivesGet(ConnectionInfo connection);

        /// <summary>
        /// This method is provided to establish a CDF value
        /// </summary>
        /// <param name="custId"></param>
        /// <param name="customerDefFieldDefId"></param>
        /// <param name="fieldValue"></param>
        public abstract void CustomerDefinedFieldValueSet(int custId, int customerDefFieldDefId, string fieldValue);
        
        /// <summary>
        /// This method is provided to establsh a CDG value
        /// </summary>
        /// <param name="custId"></param>
        /// <param name="customerDefGrpDefId"></param>
        /// <param name="customerDefGrpDefItemId"></param>
        public abstract void CustomerDefinedGroupValueSet(int custId, int customerDefGrpDefId, int customerDefGrpDefItemId);

        /// <summary>
        /// Get the cusotmer id from the provded email address.
        /// </summary>
        /// <param name="emailAddress">Customer email address.</param>
        /// <returns>Customer Guid or null if not found.  Throws <see cref="WebApiException"/> on error.</returns>
        public abstract string CustomerGuidFromEmailAddress(string emailAddress);

        /// <summary>
        /// Get the cusotmer id from the provded customer card number.
        /// </summary>
        /// <param name="cardNumber">Customer card number.</param>
        /// <returns>Customer Guid or null if not found.  Throws <see cref="WebApiException"/> on error.</returns>
        public abstract CustomerGuidFromCardNumberResponse CustomerGuidFromCardNumber(string cardNumber);

        /// <summary>
        /// Get the customer numerical id (cust_id) from their unique identifier (CustomerGuid).
        /// </summary>
        /// <param name="guid">The unique identifier for the customer (CustomerGuid).</param>
        /// <returns>The customer numerical id (cust_id)</returns>
        public abstract int CustomerIdFromCustomerGuidGet(string guid);

        /// <summary>
        /// Get the unique identifier (CustomerGuid) for the customer from their customer numerical id (cust_id).
        /// </summary>
        /// <param name="id">The customer numerical id (cust_id).</param>
        /// <returns>The unique identifier (CustomerGuid) for the customer</returns>
        public abstract string CustomerGuidFromCustomerIdGet(int id);

        /// <summary>
        /// Get the unique customer identifier (guid) from the customer card number.
        /// </summary>
        /// <param name="cardNumber">Customer card number.</param>
        /// <returns>The unique customer identifier (guid).</returns>
        public abstract CredentialVerifyCustomer CustomerInformationFromCardNumberGet(string cardNumber);


        /// <summary>
        /// Get the unique customer identifier (guid) from the customer card number.
        /// </summary>
        /// <param name="customerGuid">Customer guid.</param>
        /// <returns>A shortened list of customer information needed by device login.</returns>
        public abstract CredentialVerifyCustomer CustomerInformationFromCustomerGuidGet(string customerGuid);

        /// <summary>
        /// Get the customer's PIN.
        /// </summary>
        /// <param name="customerGuid">The unique (guid) identifier for the customer.</param>
        /// <returns>Customer's PIN or null if not found.</returns>
        public abstract string CustomerPinGet(string customerGuid);

        /// <summary>
        /// Get the customer defined group item ids for the specified customerGuid.  If customerGuid is null, all customers will be considered.
        /// </summary>
        /// <param name="customerGuid"></param>
        /// <returns></returns>
        public abstract List<CustomerDefinedGroupItemIdsForCustomerGuid> CustomerDefinedGroupIdsForCustomerGuidGet(Guid? customerGuid);

        /// <summary>
        /// Get the customer defined group item ids for the specified customerGuid.  If customerGuid is null or empty, all customers will be considered.
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="customerGuid"></param>
        /// <returns></returns>
        public abstract List<CustomerDefinedGroupItemIdsForCustomerGuid> CustomerDefinedGroupIdsForCustomerGuidGet(ConnectionInfo connection, string customerGuid);

        /// <summary>
        /// Gets the available balance for the specified customer's stored value accounts.  If return value is null, the customer has no authorized stored value accounts.
        /// </summary>
        /// <param name="evaluationDateTime">The evaluation date/time.  (This equals the TimestampType.Start entry in the transaction's TransactionTimestamps list).</param>
        /// <param name="customerId">Customer ID.</param>
        /// <param name="storedValueAccountTypeIds">The account type IDs to be included in the balance calculation.</param>
        /// <param name="transactionLimitIds">The transaction limit IDs to be used in the balance calculation.</param>
        /// <returns></returns>
        public abstract CustomerAvailableBalance AvailableBalanceForCustomerGet(Guid customerId, DateTimeOffset evaluationDateTime, List<int> storedValueAccountTypeIds, List<int> transactionLimitIds);

        /// <summary>
        /// Get a customer's board balance.
        /// </summary>
        /// <param name="customerId">The unique (Guid) identifier for the customer.</param>
        /// <param name="deviceId">The unique (Guid) identifier for the device.</param>
        /// <returns>List of board plans assigned to the customer.</returns>
        public abstract List<CustomerBoardPlan> CustomerBoardBalanceGet(string customerId, string deviceId);

        /// <summary>
        /// Get a customer's event balance.
        /// </summary>
        /// <param name="customerId">The unique (Guid) identifier for the customer.</param>
        /// <param name="deviceId">The unique (Guid) identifier for the device.</param>
        /// <returns>List of event balances assigned to the customer.</returns>
        public abstract List<CustomerEventBalance> CustomerEventBalanceGet(string customerId, string deviceId);

        /// <summary>
        /// Get a customer's stored value balance.
        /// </summary>
        /// <param name="customerId">The unique (Guid) identifier for the customer.</param>
        /// <param name="deviceId">The unique (Guid) identifier for the device.</param>
        /// <returns>List of stored value account balances assigned to the customer.</returns>
        public abstract List<CustomerStoredValueBalance> CustomerStoredValueBalanceGet(string customerId, string deviceId);

        /// <summary>
        /// Set a customer
        /// </summary>
        /// <param name="customer">Customer</param>
        public abstract CustomerPostResponse CustomerSet(TsCustomerPost customer);

        /// <summary>
        /// Get customer number availability
        /// </summary>
        /// <param name="customerNumber">Customer number</param>
        /// <returns>Indicates if customer is available or not</returns>
        public abstract bool CustomerNumberAvailable(decimal customerNumber);

        /// <summary>
        /// Get a customer by his guid
        /// </summary>
        /// <param name="customerGuid">Customer's guid</param>
        /// <returns>Customer</returns>
        public abstract TsCustomer CustomerGet(string customerGuid);

        /// <summary>
        /// Gets the customer by id or number
        /// </summary>
        /// <param name="customerNumber">The customer number.</param>
        /// <param name="customerId">The customer id.</param>
        /// <returns>The Customer</returns>
        public abstract TsCustomer CustomerGet(string customerNumber, int? customerId);

        /// <summary>
        /// Get a customer based upon the field 'CustomerNumber' in the Customer table.
        /// </summary>
        /// <param name="customerNumber">Corresponds to a value in the CustomerNumber field.</param>
        /// <returns>Customer object or null if not found</returns>
        public abstract TsCustomer CustomerGetByCustomerNumber(string customerNumber);

        /// <summary>
        /// Creates new customer
        /// </summary>
        /// <param name="customer">Customer data</param>
        public abstract CustomerManagementData CustomerCreate(PostCustomer customer);

        /// <summary>
        /// Updates new customer
        /// </summary>
        /// <param name="customerNumber"></param>
        /// <param name="customer">Customer data</param>
        public abstract void CustomerUpdate(string customerNumber, PatchCustomer customer);

        /// <summary>
        /// Get a customers
        /// </summary>
        /// <param name="customerId">Customer's id</param>
        /// <returns>Customers</returns>
        public abstract List<TsCustomer> CustomersGet(int? customerId = null);

        /// <summary>
        /// Get all customers
        /// </summary>
        /// <param name="request">Filter</param>
        /// <returns>Filtered customers</returns>
        public abstract List<TsCustomer> CustomerGetAll(CustomerGetAllRequest request);

        /// <summary>
        /// Get all customers
        /// </summary>
        /// <param name="request">Filter</param>
        /// <returns>Filtered customers</returns>
        public abstract CustomerListGetResponse CustomerListGet(CustomerListGetRequest request);

        /// <summary> 
        /// Delete customer 
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        public abstract void CustomerDelete(int customerId);

        /// <summary>
        /// Checks if customer can be deleted
        /// </summary>
        /// <param name="customerGuid">Customer guid</param>
        /// <returns>Customer delete result</returns>
        public abstract CustomerDeleteResponse CustomerDeleteAllowedGet(Guid customerGuid);

        /// <summary>
        /// Get customer board used
        /// </summary>
        /// <param name="customerGuid">Customer guid</param>
        /// <param name="boardPlanGuid">Board guid</param>
        /// <returns>Customer board used</returns>
        public abstract TsCustomerBoardDetailUsed CustomerBoardUsedGet(string customerGuid, string boardPlanGuid);

        /// <summary>
        /// Get all customer board used
        /// </summary>
        /// <param name="request">Filter</param>
        /// <returns>Filtered customer boards</returns>
        public abstract List<TsCustomerBoardDetailUsed> CustomerBoardUsedGetAll(CustomerBoardUsedGetAllRequest request);

        /// <summary>
        /// Get customer board
        /// </summary>
        /// <param name="customerGuid">Customer guid</param>
        /// <param name="boardPlanGuid">Board guid</param>
        /// <returns>Customer board</returns>
        public abstract TsCustomerBoardDetail CustomerBoardGet(string customerGuid, string boardPlanGuid);

        /// <summary>
        /// Get customer boards
        /// </summary>
        /// <param name="request">Filter</param>
        /// <returns>Filtered customer boards</returns>
        public abstract List<TsCustomerBoardDetail> CustomerBoardGetAll(CustomerBoardGetAllRequest request);

        /// <summary>
        /// Get customer cards
        /// </summary>
        /// <param name="customerGuid">Customer guid</param>
        /// <returns>Customer cards</returns>
        public abstract List<TsCard> CustomerCardsGet(string customerGuid);

        /// <summary>
        /// Get customer cards
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <returns>Customer cards</returns>
        public abstract List<TsCard> CustomerCardsGet(int customerId);

        /// <summary>
        /// Get customer photo
        /// </summary>
        /// <param name="customerGuid">Customer guid</param>
        /// <returns>Customer photo</returns>
        public abstract TsCustomerPhoto CustomerPhotoGet(string customerGuid);

        /// <summary>
        /// Get customer emails
        /// </summary>
        /// <returns>Customer emails</returns>
        public abstract List<TsCustomerEmail> CustomerEmailsGet(int customerId);

        /// <summary>
        /// Sets customer email address
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="email">Email</param>
        public abstract void CustomerEmailSet(int customerId, CustomerEmailAddressBase email);

        /// <summary>
        /// Get list of customer email types
        /// </summary>
        /// <param name="customerEmailTypeId">Id filter</param>
        /// <returns>Customer email types</returns>
        public abstract List<TsCustomerEmailType> CustomerEmailTypeGet(int? customerEmailTypeId = null);

        /// <summary>
        /// Get list of customer addresses
        /// </summary>
        /// <param name="customerId">Customer id</param>
        /// <returns>Customer addresses</returns>
        public abstract List<TsAddress> CustomerAddressesGet(int customerId);

        /// <summary>
        /// Get customer event plans
        /// </summary>
        /// <param name="customerNumber">Customer number</param>
        /// <returns>List of event plans</returns>
        public abstract List<TsCustomerEventPlan> CustomerEventPlansGet(string customerNumber);

        /// <summary>
        /// Deletes customer event plan
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        public abstract void CustomerEventPlanDelete(int customerId);

        /// <summary>
        /// Creates or updates customer event plan
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="eventPlan">Event plan</param>
        /// <returns>Created or update event plan</returns>
        public abstract void CustomerEventPlanSet(int customerId, CustomerEventPlan eventPlan);

        /// <summary>
        /// Get customer event plan overrides
        /// </summary>
        /// <param name="customerNumber">Customer number</param>
        /// <returns>Customer event plan overrides</returns>
        public abstract List<CustomerEventPlanOverride> CustomerEventPlanOverridesGet(string customerNumber);

        /// <summary>
        /// Deletes customer event plan overrides
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="eventId">Event Id</param>
        public abstract void CustomerEventPlanOverrideDelete(int customerId, int? eventId = null);

        /// <summary>
        /// Creates customer event plan override
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="eventPlanOverride">Event plan override</param>
        /// <returns>Customer event plan override</returns>
        public abstract void CustomerEventPlanOverrideCreate(int customerId, CustomerEventPlanOverride eventPlanOverride);

        /// <summary>
        /// Updates customer event plan override
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="eventId">Event Id</param>
        /// <param name="eventPlanOverride">Event plan override</param>
        /// <returns>Customer event plan override</returns>
        public abstract void CustomerEventPlanOverrideUpdate(int customerId, int eventId, PutCustomerEventPlanOverride eventPlanOverride);

        /// <summary>
        /// Get customer defaults
        /// </summary>
        /// <returns>List of customer defaults</returns>
        public abstract List<TsCustomerDefault> CustomerDefaultsGet();

        /// <summary>
        /// Get customer board plans
        /// </summary>
        /// <param name="customerNumber">Customer number</param>
        /// <returns>Customer board plans</returns>
        public abstract List<CustomerManagementBoardPlan> CustomerBoardsGet(string customerNumber);

        /// <summary>
        /// Adds new boardplan to customer
        /// </summary>
        /// <param name="customerId">Customer number</param>
        /// <param name="bp">Board plan</param>
        /// <returns>Customer board plan</returns>
        public abstract void CustomerBoardPlanCreate(int customerId, PostCustomerBoardPlan bp);

        /// <summary>
        /// Adds new boardplan to customer
        /// </summary>
        /// <param name="customerNumber">Customer number</param>
        /// <param name="boardPlanId"></param>
        /// <param name="bp">Board plan</param>
        /// <returns>Customer board plan</returns>
        public abstract void CustomerBoardPlanUpdate(string customerNumber, int boardPlanId, PatchCustomerBoardPlan bp);

        /// <summary>
        /// Deletes boardplans from customer
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="boardPlanId">Board plan Id (optional)</param>
        public abstract void CustomerBoardPlanDelete(int customerId, int? boardPlanId = null);

        /// <summary>
        /// Get customer door access plan Ids
        /// </summary>
        /// <param name="customerNumber">Customer number</param>
        /// <returns>Door access plan Ids</returns>
        public abstract List<int> CustomerDoorAccessPlansGet(string customerNumber);

        /// <summary>
        /// Assigns door access plan to customer
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="doorAccessPlanId">Door access plan Id</param>
        public abstract void CustomerDoorAccessPlanCreate(int customerId, int doorAccessPlanId);

        /// <summary>
        /// Removes door access plan from customer
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="doorAccessPlanId">Door access plan Id</param>
        public abstract void CustomerDoorAccessPlanDelete(int customerId, int? doorAccessPlanId = null);

        /// <summary>
        /// Get customer stored value accounts
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="svAccountNumber">Stored value account number</param>
        /// <returns>Customer stored value accounts</returns>
        public abstract List<CustomerSvAccount> CustomerStoredValueAccountsGet(int customerId, Int64? svAccountNumber = null);

        /// <summary>
        /// Creates new customer stored value account
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="svAccount">SV account</param>
        /// <returns>Customer stored value account number</returns>
        public abstract Int64 CustomerStoredValueAccountCreate(int customerId, PostCustomerSvAccount svAccount);

        /// <summary>
        /// Updates existing customer stored value account
        /// </summary>
        /// <param name="svAccount">SV account</param>
        public abstract void CustomerStoredValueAccountUpdate(CustomerSvAccount svAccount);

        /// <summary>
        /// Deletes customer stored value account
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="svAccountTypeId">Account Type Id</param>
        public abstract void CustomerStoredValueAccountDelete(int customerId, int svAccountTypeId);

        /// <summary>
        /// Get customer pos display rules
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <returns>Customer pos display rules</returns>
        public abstract List<int> CustomerPosDisplayRulesGet(int customerId);

        /// <summary>
        /// Get customer pos message
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <returns>Customer pos message</returns>
        public abstract CustomerPosMessage CustomerPosMessageGet(int customerId);

        /// <summary>
        /// Get customer plans
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <returns>Customer plans</returns>
        public abstract CustomerPlans CustomerPlansGet(int customerId);

        /// <summary>
        /// Get customer board plans
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <returns>Customer board plans</returns>
        public abstract List<TsCustomerBoardPlan> CustomerBoardPlansGet(int customerId);

        /// <summary>
        /// Get customer board plans by customer number
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns>Customer board plans</returns>
        public abstract List<int> CustomerBoardPlanIdsGet(int customerId);

        /// <summary>
        /// Set customer board plans
        /// </summary>
        /// <param name="customerGuid">Customer Guid</param>
        /// <param name="request">Post request</param>
        /// <returns>Operation result</returns>
        public abstract void CustomerBoardPlansSet(Guid customerGuid, CustomerBoardPlansPostRequest request);

        /// <summary>
        /// Set exclusion for transactions
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="transactionNumbers">Transactions which exclusion should be removed</param>
        /// <param name="exclusionEnabled">Exclusion enabled</param>
        /// <returns>Operation result</returns>
        public abstract void CustomerBoardExclusionsSet(int customerId, List<int> transactionNumbers, bool exclusionEnabled);

        /// <summary>
        /// Set customer door access plans
        /// </summary>
        /// <param name="customerGuid">Customer Guid</param>
        /// <param name="daPlanIds">DA Plan Ids</param>
        public abstract void CustomerAccessPlansSet(Guid customerGuid, List<int> daPlanIds);

        /// <summary>
        /// Get customer transaction history
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <returns>Customer transaction history</returns>
        public abstract List<TsCustomerTransaction> CustomerTransactionHistoryGet(int customerId, DateTime? startDate, DateTime? endDate);

        /// <summary>
        /// Get customer door access 
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="doorId">Door Id</param>
        /// <param name="date">Date</param>
        /// <returns>Customer door access</returns>
        public abstract CustomerDoorAccess CustomerDoorAccessGet(int customerId, int doorId, DateTime? date);

        /// <summary>
        /// Set customer door overrides
        /// </summary>
        /// <param name="customerGuid">Customer Guid</param>
        /// <param name="request">request</param>
        public abstract void CustomerDoorOverridesSet(Guid customerGuid, DoorOverridesSetRequest request);

        #endregion

        #region CustomerDefFieldDefFunctions

        /// <summary>
        /// This method will return a list of records for the CustomerDefFieldDef objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public abstract List<TsCustomer_Def_Field_Def> CustomerDefFieldDefArchivesGet(ConnectionInfo connection);

        /// <summary>
        /// This method will return a list of records for the CustomerDefFieldDef objects in the system
        /// </summary>
        /// <param name="customerDefinedFieldId">Id filter</param>
        /// <returns>List of CustomerDefFieldDef</returns>
        public abstract List<TsCustomer_Def_Field_Def> CustomerDefFieldDefGet(int? customerDefinedFieldId = null);

        /// <summary>
        /// This method will return a list of records for the TsCustomerDefFieldValue objects in the system
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <returns>List of TsCustomerDefFieldValue</returns>
        public abstract List<TsCustomerDefFieldValue> CustomerDefFieldValueGet(int customerId);

        /// <summary>
        /// Get Customer Defined Group Item Ids
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <returns>Customer Defined Group Item Ids</returns>
        public abstract List<int> CustomerDefGroupItemIdsGet(int customerId);

        #endregion

        #region CustomerDefGrpDefFunctions

        /// <summary>
        /// This method will return a list of records for the CustomerDefGrpDef objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public abstract List<TsCustomer_Def_Grp_Def> CustomerDefGrpDefArchivesGet(ConnectionInfo connection);

        /// <summary>
        /// This method will return a list of records for the CustomerDefGrpDef objects in the system
        /// </summary>
        /// <param name="customerDefGroupDefId">Id filter</param>
        /// <returns>List of CustomerDefGrpDef</returns>
        public abstract List<TsCustomerDefGroupDef> CustomerDefGrpDefGet(int? customerDefGroupDefId = null);

        /// <summary>
        /// This method will return a list of records for the TsCustomerDefGroupDef objects in the system
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <returns>List of TsCustomerDefGroupDef</returns>
        public abstract List<TsCustomerDefGroupDef> CustomerDefGrpValueGet(int customerId);

        #endregion

        #region CustomerDefGrpDefItemFunctions
        /// <summary>
        /// This method will return a list of records for the CustomerDefGrpDefItem objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public abstract List<TsCustomer_Def_Grp_Def_Item> CustomerDefGrpDefItemArchivesGet(ConnectionInfo connection);
        #endregion

        #region DeniedMessageFunctions
        /// <summary>
        /// This method will return a list of records for the DeniedMessage objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public abstract List<TsDenied_Message> DeniedMessageArchivesGet(ConnectionInfo connection);
        #endregion

        #region Device Functions

        /// <summary>
        /// Get a list of IP Readers from the V_IP_READER view.
        /// </summary>
        /// <returns>List of IP Readers.</returns>
        public abstract List<IpReaderViewObject> IpReadersGet(List<HardwareModel> hardwareModelFilter = null);

        /// <summary>
        /// Get all (or one) DSR Access Point from the database layer.
        /// </summary>
        /// <param name="id">The access point id or access point guid.</param>
        /// <returns>List of active DsrAccessPoints in the database layer.</returns>
        public abstract List<DsrAccessPoint> AccessPointsGet(string id);

        /// <summary>
        /// Get all Allegion locks from the database layer.
        /// </summary>
        /// <param name="modelFilter"></param>
        /// <returns>List of active Allegion locks in the database layer.</returns>
        public abstract List<AllegionLock> AllegionLocksGet(List<LicenseServerDeviceSubType> modelFilter = null);

        /// <summary>
        /// Verify that a device has been registered.
        /// </summary>
        /// <param name="deviceGuid">The unique (guid) identifier for the device.</param>
        /// <returns>Registration status.</returns>
        public abstract bool DeviceIsRegistered(string deviceGuid);

        /// <summary>
        /// Get (all or single) device registration information.
        /// </summary>
        /// <param name="deviceGuid">Identifier of the device (for single) or null (for all).</param>
        /// <returns>List of device registration information.</returns>
        public abstract List<DeviceProperties> DevicePropertiesGet(string deviceGuid = null);

        /// <summary>
        /// Gets device registration information
        /// </summary>
        /// <param name="deviceGuid">Identifier of the device (for single) or null (for all).</param>
        /// <returns>Device registration information</returns>
        public abstract DeviceInfo DeviceGet(string deviceGuid);

        /// <summary>
        /// Update a device registration.
        /// </summary>
        /// <param name="deviceGuid">The unique (guid) identifier for the device.</param>
        /// <param name="registration">The registration information to use for an update.</param>
        /// <returns>The updated device registration information.</returns>
        public abstract DeviceProperties DevicePropertiesUpdate(string deviceGuid, DeviceProperties registration);

        /// <summary>
        /// Delete a device registration information.
        /// </summary>
        /// <param name="deviceGuid">Identifier of the device (for single) or null (for all).</param>
        public abstract void DevicePropertiesDelete(string deviceGuid);

        /// <summary>
        /// Log a heartbeat request from the device.
        /// </summary>
        /// <param name="request">Heartbeat information from the device.</param>
        public abstract void DeviceHeartbeatLog(DeviceHeartbeatRequest request);

        /// <summary>
        /// Get the operational requirements for a specific device.
        /// </summary>
        /// <param name="deviceId">The unique (guid) identifier for the device.</param>
        /// <returns>The operational requirements for the specific device.</returns>
        public abstract OperationalRequirements DeviceOperationalRequirementsGet(string deviceId);

        /// <summary>
        /// Process and log a device login request.
        /// </summary>
        /// <param name="request">Device login information.</param>
        public abstract DeviceLoginResponse ProcessDeviceLogin(DeviceLoginRequest request);

        /// <summary>
        /// Process and log a device logout request.
        /// </summary>
        /// <param name="request">Device logout information.</param>
        public abstract DeviceLogoutResponse ProcessDeviceLogout(DeviceLogoutRequest request);

        /// <summary>
        /// Register a (slate) device.
        /// </summary>
        /// <param name="request">Device registration information.</param>
        /// <returns></returns>
        public abstract DeviceRegistrationPostResponse DeviceSet(DeviceRegistrationPostRequest request);

        /// <summary>
        /// Register a device as a pos.
        /// </summary>
        /// <param name="deviceGuid">The unique identifier for the device.</param>
        /// <param name="posGuid">The unique identifier for the pos.</param>
        public abstract void DevicePosRegister(string deviceGuid, string posGuid);

        /// <summary>
        /// Get the period number of the timestamp from the device guid.
        /// </summary>
        /// <param name="deviceGuid">The device requesting the period number.</param>
        /// <param name="timestamp">The date and time to resolve against.</param>
        /// <returns>Period number based on the the date and time and the profit center the device resides in.</returns>
        public abstract int PeriodNumberFromDeviceGuidGet(string deviceGuid, DateTime timestamp);

        /// <summary>
        /// Meals count report for Device
        /// </summary>
        /// <param name="deviceGuid">Device guid</param>
        /// <returns>Meals count report</returns>
        public abstract MealsCountReport DeviceMealsCountReportGet(Guid deviceGuid);

        /// <summary>
        /// Get enhanced reporting list
        /// </summary>
        /// <returns>Enhanced reporting list</returns>
        public abstract List<DeviceReader> EnhancedReportingListGet();

        /// <summary>
        /// Get enhanced domain data
        /// </summary>
        /// <returns>Enhanced domain data</returns>
        public abstract EnhancedReportingDomainDataGetResponse EnhancedReportingDomainDataGet();

        #endregion

        #region DoorFunctions
        /// <summary>
        /// This method will return a list of Area objects from the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public abstract List<TsArea> AreaGet(ConnectionInfo connection);

        /// <summary>
        /// This method will return a list of building objects from the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public abstract List<TsBuilding> BuildingsGet(ConnectionInfo connection);

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void CustomerDaRequestHistoryByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> CustomerDaRequestHistoryDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsCustomer_Da_Request_History> CustomerDaRequestHistoryListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void DaDeniedTransactionByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsDa_DeniedTransaction> DaDeniedTransactionListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> DaDeniedTransactionDaysFilteredByDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void DaDeniedTransactionCardByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsDa_DeniedTransaction_Card> DaDeniedTransactionCardListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> DaDeniedTransactionCardDaysFilteredByDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void DaDoorAlarmAckInfoByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsDa_DoorAlarm_AckInfo> DaDoorAlarmAckInfoListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> DaDoorAlarmAckInfoDaysFilteredByDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void DaDoorAlarmLogByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsDa_DoorAlarmLog> DaDoorAlarmLogListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> DaDoorAlarmLogDaysFilteredByDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void DaDoorCurrentStatusByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsDa_DoorCurrentStatus> DaDoorCurrentStatusListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> DaDoorCurrentStatusDaysFilteredByDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return records from the Da_DoorEventLog table
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">The Julian date to filter by</param>
        public abstract void DaDoorEventLogByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of Da_DoorEventLog records
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">The Julian date to filter by</param>
        /// <returns></returns>
        public abstract List<TsDa_DoorEventLog> DaDoorEventLogListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> DaDoorEventLogDaysFilteredByDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// Get Da plans by customer
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <returns>Da plans</returns>
        public abstract List<TsDoorAccessPlan> DaPlansGetByCustomer(int customerId);

        /// <summary>
        /// Get door overrides by customer
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <returns>Door overrides</returns>
        public abstract List<TsDoorOverride> DoorOverridesGetByCustomer(int customerId);

        /// <summary>
        /// Get a set of records from the DA_Transaction table for customer access during a date range.
        /// </summary>
        /// <param name="startDate">Start date.</param>
        /// <param name="endDate">End Date.</param>
        /// <returns></returns>
        public abstract List<CustomerDoorAccessLog> CustomerDoorAccessLogsGet(DateTime startDate, DateTime endDate);

        /// <summary>
        /// This method will return a list of Door objects from the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public abstract List<TsDoor> DoorsGet(ConnectionInfo connection);

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void DaDoorStateOverrideByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsDa_DoorStateOverride> DaDoorStateOverrideListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> DaDoorStateOverrideDaysFilteredByDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public abstract List<TsMasterController> MasterControllersGet(ConnectionInfo connection);

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void DaTransactionByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsDa_Transaction> DaTransactionListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> DaTransactionDaysFilteredByDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void DaTransactionCardByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsDa_Transaction_Card> DaTransactionCardListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> DaTransactionCardDaysFilteredByDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// Get a list of door access plans
        /// </summary>
        /// <param name="doorAccessPlanId">Id filter</param>
        /// <returns>Door access plans</returns>
        public abstract List<TsDoorAccessPlan> DoorAccessPlanGet(int? doorAccessPlanId = null);

        /// <summary>
        /// Get reuse delays
        /// </summary>
        /// <returns>Reuse delays</returns>
        public abstract List<TsReuseDelay> DaReuseDelaysGet();

        /// <summary>
        /// Get door access permissions by merchant id
        /// </summary>
        /// <param name="merchantId">Merchant's Id</param>
        /// <returns>Merchant's da permissions</returns>
        public abstract List<TsDaPermissionBase> DaPermissionsGet(Int16 merchantId);

        /// <summary>
        /// Get door groups
        /// </summary>
        /// <param name="merchantId">Merchant's Id</param>
        /// <returns>Merchant's door groups</returns>
        public abstract List<TsDoorGroup> DoorGroupsGet(Int16 merchantId);

        /// <summary>
        /// Get event groups
        /// </summary>
        /// <param name="merchantId">Merchant's Id</param>
        /// <returns>Merchant's event groups</returns>
        public abstract List<TsEventGroup> EventGroupsGet(Int16 merchantId);

        /// <summary>
        /// Get buildings with areas
        /// </summary>
        /// <returns>Buildigns with areas</returns>
        public abstract List<Building> BuildingAreasGet();

        #endregion

        #region DsProductDetailFunctions
        /// <summary>
        /// This method will return a list of records for the DsProductDetail objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public abstract List<TsDsProductDetail> DsProductDetailArchivesGet(ConnectionInfo connection);
        #endregion

        #region DsProductPromoFunctions
        /// <summary>
        /// This method will return a list of records for the DsProductPromo objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public abstract List<TsDsProductPromo> DsProductPromoArchivesGet(ConnectionInfo connection);
        #endregion

        #region EAccountFunctions
        /// <summary>
        /// This method will preauthorize a deposit from EAccounts
        /// </summary>
        /// <param name="request">Details of deposit to pre-authorize</param>
        /// <returns></returns>
        public abstract PreauthDepositResponse EAccountPreauthDeposit(PreauthDepositRequest request);

        /// <summary>
        /// This method will perform a deposit from EAccounts
        /// </summary>
        /// <param name="request">Details of deposit to perform</param>
        /// <returns></returns>
        public abstract DepositResponse EAccountPerformDeposit(DepositRequest request);

        /// <summary>
        /// Look up external client customer guid from other information.
        /// </summary>
        /// <param name="request">Customer informatino known for anon deposit.</param>
        /// <returns></returns>
        public abstract GetExternalCustGuidFromAnonResponse GetExternalCustGuidFromAnon(GetExternalCustGuidFromAnonRequest request);
        #endregion

        #region Event Functions (Attendance)

        /// <summary>
        /// Process an attendance transaction and provide the result.
        /// </summary>
        /// <param name="request">Attendance transaction to process</param>
        /// <returns>Result of the attendance transaction.</returns>
        public abstract AttendanceTransactionProcessResponse AttendanceTransactionProcess(AttendanceTransactionProcessRequest request);

        /// <summary>
        /// Get a list of events for a device.
        /// </summary>
        /// <param name="deviceId">the unique identifier for the device.</param>
        /// <returns>List of <see cref="EventDeviceSetting"/></returns>
        public abstract List<EventDeviceSetting> EventDeviceSettingsGet(string deviceId);

        /// <summary>
        /// Get all event activities
        /// </summary>
        /// <returns>Event activities</returns>
        public abstract List<TsEventActivity> EventActivitiesGetAll();

        /// <summary>
        /// Get list of event plans
        /// </summary>
        /// <param name="eventPlanId">Id filter</param>
        /// <returns>Event plans</returns>
        public abstract List<TsEventPlan> EventPlanGet(int? eventPlanId = null);

        /// <summary>
        /// Get information about current attendance at an event.
        /// </summary>
        /// <param name="eventId">eventId to query.</param>
        /// <returns>Event Attendance</returns>
        public abstract EventAttendanceGetResponse EventAttendanceGet(int eventId);

        /// <summary>
        /// Get customer events
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <returns>Customer events</returns>
        public abstract CustomerEventsGetResponse CustomerEventsGet(int customerId);

        /// <summary>
        /// Set customer events
        /// </summary>
        /// <param name="customerGuid">Customer guid</param>
        /// <param name="request">Request</param>
        /// <returns>Customer events</returns>
        public abstract void CustomerEventsSet(Guid customerGuid, CustomerEventsPostRequest request);

        /// <summary>
        /// Get events by event plan
        /// </summary>
        /// <param name="eventPlanId">Event Plan Id</param>
        /// <returns>Events</returns>
        public abstract List<TsCustomerEvent> EventsForCustomerGetByEventPlan(int eventPlanId);

        /// <summary>
        /// Get event list
        /// </summary>
        /// <param name="request">Request</param>
        /// <returns>Events</returns>
        public abstract EventListGetResponse EventListGet(EventListGetRequest request);

        /// <summary>
        /// Get events by event group
        /// </summary>
        /// <param name="eventGroupId">Event group Id</param>
        /// <returns>Events</returns>
        public abstract List<TsEventInfo> EventsByEventGroup(int eventGroupId);

        #endregion

        #region Event Log Functions

        /// <summary>
        /// Creates an event log
        /// </summary>
        public abstract void EventLogSet(EventLogPostRequest request);

        /// <summary>
        /// Gets an event log
        /// </summary>
        /// <param name="eventLogId"></param>
        /// <returns>Event log</returns>
        public abstract TsEventLog EventLogGet(int eventLogId);

        /// <summary>
        /// This will return an EventLog object
        /// </summary>
        /// <param name="connectionInfo"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        public abstract IEnumerable<EventLog> EventLogGet(ConnectionInfo connectionInfo, string source);

        /// <summary>
        /// Gets summary of event logs
        /// </summary>
        /// <param name="request">Filter</param>
        /// <returns>Filtered event logs</returns>
        public abstract List<TsEventLogSummary> EventLogGetSummary(EventLogGetSummaryRequest request);

        /// <summary>
        /// Gets lastest log by event task category
        /// </summary>
        /// <param name="eventId">Event id</param>
        /// <param name="taskCategory">Task category</param>
        /// <param name="providerName">Provider name</param>
        /// <param name="computerName">Computer name</param>
        /// <returns>Event log</returns>
        public abstract TsEventLog EventLogLatestByTaskGet(int eventId, short taskCategory, string providerName = null, string computerName = null);

        /// <summary>
        /// Get a list of customer locations based on event attendance between two specific dates.
        /// </summary>
        /// <param name="startDate">The start of the window.</param>
        /// <param name="endDate">The end of the window.</param>
        /// <returns></returns>
        public abstract List<CustomerTransactionLocationLog> CustomerEventLocationLogsGet(DateTime startDate, DateTime endDate);

        #endregion

        #region External Client Functions

        /// <summary>
        /// Get external client customer data
        /// </summary>
        /// <param name="customerClientGuid">Client customer guid</param>
        /// <returns>External client customer data</returns>
        public abstract ExternalClientCustomer ExternalClientCustomerGet(Guid customerClientGuid);

        /// <summary>
        /// Process an external client customer registration request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>
        /// Response containing either:
        /// 1. The newly created external client registration information.
        /// 2. The existing registration information for the client if it had already been created.
        /// </returns>
        public abstract ProcessCustomerRegistrationPostResponse ProcessCustomerRegistration(ProcessCustomerRegistrationPostRequest request);

        #endregion

        #region Host Monitor Functions

        /// <summary>
        /// Write a host monitor message to the database.
        /// </summary>
        /// <param name="message">Message to write.</param>
        public abstract void HostMonitorMessageWrite(HostMonitorMessage message);

        /// <summary>
        /// Get all host monitor messages from the database.
        /// </summary>
        /// <returns>List of host monitor messages.</returns>
        public abstract List<HostMonitorMessage> HostMonitorMessagesGetAll();

        #endregion

        #region IndexFunctions

        #region Abstracts
        /// <summary>
        /// This method will retreive an indexes object from the system.
        /// </summary>
        /// <param name="connectionInfo">The database connection information</param>
        /// <param name="ownerName"></param>
        /// <param name="indexName"></param>
        /// <returns></returns>
        public abstract Indexes IndexesGet(ConnectionInfo connectionInfo, string ownerName, string indexName);

        #endregion

        #region Non-Abstracts

        /// <summary>
        /// Given an index, script it out
        /// </summary>
        /// <param name="index">The index object to script out</param>
        /// <returns>A text block representing an index creation script</returns>
        public string IndexCreateScriptGet(Index index)
        {
            string columns = string.Empty;

            foreach (var item in index.Columns.OrderBy(x => x.Position))
            {
                columns += $"{(item.IsFunctionBased ? item.Expression : item.Name)},";
            }
            columns = columns.TrimEnd(',');

            return string.Format(Properties.Resource.IndexCreateScript, index.IndexName, index.TableName, columns, index.IndexType.Equals(IndexType.Unique) ? " UNIQUE " : " ", index.Tablespace, Xml.Serialize(index));

        }

        /// <summary>
        /// This method will return a well formed rename index script
        /// </summary>
        /// <param name="oldIndex"></param>
        /// <param name="newIndex"></param>
        /// <returns></returns>
        public string IndexRenameScriptGet(Index oldIndex, Index newIndex)
        {
            return string.Format(Properties.Resource.IndexRenameScript, oldIndex.IndexName, newIndex.IndexName);
        }

        /// <summary>
        /// This method will return a string containing the query for all of the indexes in the system owned by the owner name and restricted by indexname (wild-card % allowed).
        /// </summary>
        /// <param name="ownerName">The owner of the objects</param>
        /// <param name="indexName">The name of the index to use as a filter.  % allowed as a wild card.</param>
        /// <returns>A string representing a Sql Query</returns>
        internal static string IndexesQueryGet(string ownerName, string indexName)
        {
            if (string.IsNullOrEmpty(ownerName))
                throw new ArgumentException("ownerName is a required parameter.");

            // Just in case NULL was passed in, just default it to % for the Sql query
            if (string.IsNullOrEmpty(indexName))
                indexName += "%";

            // The command to be run against the db
            return $@"SELECT        IDX.Table_Name        AS TableName,
                                    IDX.Index_Name        AS IndexName,            
                                    IDX.Uniqueness        AS IndexType,
                                    IDX.Tablespace_Name   AS Tablespace,
                                    COL.Column_Position   AS ColumnPosition,
                                    COL.Column_Name       AS ColumnName,
                                    IXE.Column_Expression AS Expression
                        FROM        All_Indexes         IDX
                        INNER JOIN  All_ind_columns     COL ON  IDX.Owner           = COL.Index_Owner
                                                            AND IDX.Table_Name      = COL.Table_Name
                                                            AND IDX.Index_Name      = COL.Index_Name
                        LEFT JOIN   All_ind_expressions IXE ON  IDX.Owner           = IXE.Index_Owner
                                                            AND COL.Table_Name      = IXE.Table_Name
                                                            AND COL.Index_Name      = IXE.Index_Name
                                                            AND COL.Column_Position = IXE.Column_Position
                        LEFT JOIN   All_Constraints     CON ON  IDX.Owner           = CON.Owner
                                                            AND COL.Table_Name      = CON.Table_Name
                                                            AND COL.Index_Name      = CON.Index_Name
                        WHERE       IDX.Owner = '{ownerName.ToUpper()}'
                        AND         IDX.Index_Name LIKE '{indexName.ToUpper()}'
                        AND         NVL(CON.Constraint_Type,'.') != 'P'
                        ORDER BY    IDX.Table_Name,
                                    IDX.Index_Name,
                                    COL.Column_Position";
        }

        #endregion

        #endregion

        #region Merchant Functions

        /// <summary>
        /// Get merchant
        /// </summary>
        /// <param name="merchantGuid">Merchant's guid</param>
        /// <returns>Merchant</returns>
        public abstract TsMerchant MerchantGet(string merchantGuid);

        /// <summary>
        /// Get all merchants
        /// </summary>
        /// <returns>All merchants</returns>
        public abstract List<TsMerchant> MerchantGetAll();

        #endregion

        #region Notification Functions (Camden, et. all)

        /// <summary>
        /// Create a notification.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public abstract NotificationCreateResponse NotificationCreate(NotificationCreateRequest request);

        /// <summary>
        /// Get all notificaitons stored in the database layer.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public abstract NotificationGetResponse NotificationGet(NotificationGetRequest request);

        /// <summary>
        /// Set the status for processing of a notification event.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public abstract NotificationStatusSetResponse NotificationStatusSet(NotificationStatusSetRequest request);

        /// <summary>
        /// Register to receive notification events.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="connection">Persistent oracle connection.</param>
        /// <returns></returns>
        public abstract NotificationEventRegisterResponse NotificationEventRegister(NotificationEventRegisterRequest request, OracleConnection connection);

        /// <summary>
        /// Wait for a notification event callback.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="connection">Persistent oracle connection.</param>
        /// <returns></returns>
        public abstract NotificationEventCallbackResponse NotificationEventCallbackWait(NotificationEventCallbackRequest request, OracleConnection connection);

        /// <summary>
        /// Kick off a nightly reporting metrics gather and process request in the data layer.
        /// </summary>
        /// <param name="requestId">The unique identifier for the request.</param>
        /// <returns></returns>
        public abstract ProcessingResult ReportingMetricsDailyProcessRun(string requestId);

        #endregion

        #region Object Functions

        /// <summary>
        /// Get object field value
        /// </summary>
        /// <param name="objectId">Object Id</param>
        /// <param name="definedFieldId">Object defined field Id</param>
        /// <returns>Object field value</returns>
        public abstract List<ObjectDefFieldValue> ObjectFieldValueGet(int? objectId, int? definedFieldId);

        /// <summary>
        /// Deletes object field value
        /// </summary>
        /// <param name="objectId">Object Id</param>
        /// <param name="definedFieldId">Object defined field Id</param>
        public abstract void ObjectFieldValueDelete(int objectId, int definedFieldId);

        /// <summary>
        /// Set object field value
        /// </summary>
        /// <param name="objectFieldValue">Object def field value to be set</param>
        public abstract void ObjectFieldValueSet(ObjectDefFieldValue objectFieldValue);

        #endregion

        #region Operator Functions

        /// <summary>
        /// Validate operator card number credentials.
        /// </summary>
        /// <param name="request">Validation request.</param>
        /// <returns>Validation response.</returns>
        public abstract ValidateOperatorCardNumberResponse ValidateOperatorCardNumber(ValidateOperatorCardNumberRequest request);

        /// <summary>
        /// Change an operator pin.
        /// </summary>
        /// <param name="request">PIN change information.</param>
        /// <param name="cashierId">The numberical id of the cashier (required by the stored proc).</param>
        /// <returns>Pin change response</returns>
        public abstract OperatorChangePinResponse ChangeOperatorPin(OperatorChangePinRequest request, int cashierId);

        /// <summary>
        /// Process an operator session begin request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The result of the operation.</returns>
        public abstract OperatorSessionBeginEndProcessResponse OperatorSessionBeginProcess(OperatorSessionBeginEndProcessRequest request);

        /// <summary>
        /// Process an operator session end request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The result of the operation.</returns>
        public abstract OperatorSessionBeginEndProcessResponse OperatorSessionEndProcess(OperatorSessionBeginEndProcessRequest request);

        /// <summary>
        /// Get an operator id (guid) from an e-mail address (assigned to the customer associated with the operator).
        /// </summary>
        /// <param name="email">Customer e-mail address.</param>
        /// <returns>Operator Id (Guid)</returns>
        public abstract string OperatorIdFromEmailAddress(string email);

        /// <summary>
        /// Get an operator id (guid) from basic credential information (assigned to the customer associated with the operator).
        /// </summary>
        /// <param name="credential">Basic credential information, including card number and issue number (needed by this function).</param>
        /// <returns>Operator Id (Guid)</returns>
        public abstract string OperatorIdFromCardNumber(BasicCardCredential credential);

        /// <summary>
        /// Get the operator PIN.
        /// </summary>
        /// <param name="operatorId">The unique (Guid) identifier for the operator.</param>
        /// <returns>Operator's PIN or null if not found.</returns>
        public abstract string OperatorPinGet(string operatorId);

        /// <summary>
        /// Get the operator's email address.
        /// </summary>
        /// <param name="operatorId">The unique (Guid) identifier for the operator.</param>
        /// <returns>Operator's email address or null if not found.</returns>
        public abstract string OperatorEmailGet(string operatorId);

        /// <summary>
        /// Get the operator's card number.
        /// </summary>
        /// <param name="operatorId">The unique (Guid) identifier for the operator.</param>
        /// <returns>Operator's card number or null if not found.</returns>
        public abstract string OperatorCardNumberGet(string operatorId);

        /// <summary>
        /// Get an operator's basic credential information.
        /// </summary>
        /// <param name="operatorId">The unique (Guid) identifier for the operator.</param>
        /// <returns>Credential for the operator or null if not found.</returns>
        public abstract BasicCardCredential OperateCardNumberAndIssueNumberGet(string operatorId);

        /// <summary>
        /// Get all operators for a specific device.
        /// </summary>
        /// <param name="deviceId">The unique (Guid) identifier for the device.</param>
        /// <returns>List of <see cref="Operator"/>s on a device.</returns>
        public abstract List<Operator> OperatorsDeviceSettingsGet(string deviceId);

        /// <summary>
        /// Get a list of all operator roles for a specific device.
        /// </summary>
        /// <param name="deviceId">The unique (Guid) identifier for the device.</param>
        /// <returns>List of operator roles.</returns>
        public abstract List<OperatorRole> OperatorRolesForDeviceGet(string deviceId);

        /// <summary>
        /// Get an operator by their unique (guid) identifier.
        /// </summary>
        /// <param name="operatorGuid">The unique (guid) identifier for the operator.</param>
        /// <returns>The operator or null if not found.</returns>
        public abstract Operator OperatorByGuidGet(string operatorGuid);

        /// <summary>
        /// Get operators by merchant guid
        /// </summary>
        /// <param name="merchantGuid">Operator's guid</param>
        /// <returns>Merchant's operators</returns>
        public abstract List<TsOperator> OperatorsByMerchantGet(Guid merchantGuid);

        #endregion

        #region Policy Functions
        /// <summary>
        /// Loads the system root policy for BbTS.
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        public abstract PolicySet SystemRootPolicyLoad(ConnectionInfo connection = null);

        /// <summary>
        /// Gets a list of stored value tender IDs allowed for the specified originator.
        /// </summary>
        /// <param name="originatorGuid"></param>
        /// <param name="tenderIds"></param>
        /// <param name="connection"></param>
        public abstract void ProcessPolicyTendersGet(Guid originatorGuid, out List<int> tenderIds, ConnectionInfo connection = null);

        /// <summary>
        /// Process policy for a customer using the stored proc "Validate.ValidateSystemPolicy"
        /// </summary>
        /// <param name="cardNum"></param>
        /// <param name="issueNumber"></param>
        /// <param name="issueNumberCaptured"></param>
        /// <param name="posId"></param>
        /// <param name="tenderId"></param>
        /// <param name="evaluationDateTime"></param>
        /// <param name="custId"></param>
        /// <param name="availableBalance"></param>
        /// <param name="taxExempt"></param>
        /// <param name="discountSurcharges"></param>
        /// <param name="errorCode"></param>
        /// <param name="deniedText"></param>
        /// <param name="connection"></param>
        public abstract void ValidateSystemRootPolicy(
            string cardNum,
            string issueNumber,
            bool issueNumberCaptured,
            int posId,
            int tenderId,
            DateTimeOffset evaluationDateTime,
            out int custId,
            out decimal availableBalance,
            out bool taxExempt,
            out List<DiscountSurchargeRule> discountSurcharges,
            out int errorCode,
            out string deniedText,
            ConnectionInfo connection = null);
        #endregion        

        #region PosFunctions
        /// <summary>
        /// This method will return a list of records for the Pos objects in the system.  If connection is null, <see cref="ConnectionString"/> will be used
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public abstract List<TsPos> PosArchivesGet(ConnectionInfo connection = null);

        /// <summary>
        /// Get the profit center the device belongs to from the device guid.
        /// </summary>
        /// <param name="deviceGuid">The unique identifier for the device.</param>
        /// <returns>The id (int) of the profit center</returns>
        public abstract int ProfitCenterFromDeviceGuidGet(string deviceGuid);

        /// <summary>
        /// Get pos properties for a specified guid (or null for all).
        /// </summary>
        /// <param name="posGuid">the unique identifier for the pos (null for all pos properties).</param>
        /// <returns>A list of pos properties.</returns>
        public abstract List<PosProperties> PosPropertiesGet(string posGuid = null);

        /// <summary>
        /// Set pos properties for a specified guid (null to create).
        /// </summary>
        /// <param name="request">The request with all the pos properties to create.</param>
        public abstract PosProperties PosPropertiesCreate(PosPropertiesPostRequest request);

        /// <summary>
        /// This method will delete a Pos record
        /// </summary>
        /// <param name="posId">The identity of the POS record to delete</param>
        public abstract void PosDelete(int posId);

        /// <summary>
        /// This method will create a PosTt record
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public abstract PosTt PosTtCreate(PosTtPostRequest request);

        /// <summary>
        /// This method will return the list of Pos_Tt records
        /// </summary>
        /// <param name="posGuid">A filtering criteria</param>
        /// <returns></returns>
        public abstract List<PosTt> PosTtListGet(string posGuid = null);

        /// <summary>
        /// This method will return the list of Pos_Tt_Setup records
        /// </summary>
        /// <param name="posTtSetupId"></param>
        /// <returns></returns>
        public abstract List<PosTtSetup> PosTtSetupListGet(int? posTtSetupId = null);

        /// <summary>
        /// This method will return the list of Pos_Option records
        /// </summary>
        /// <param name="id">A filtering criteria</param>
        /// <returns></returns>
        public abstract List<PosOption> PosOptionGet(string id = null);

        /// <summary>
        /// Method will return the Pos option for specifid POS
        /// </summary>
        /// <param name="posGuid">POS Guid</param>
        /// <returns>POS Option</returns>
        public abstract PosOptionDeviceSettings PosOptionDeviceSettingsGet(Guid posGuid);

        /// <summary>
        /// Get the pos guid from the originator guid.
        /// </summary>
        /// <param name="originatorGuid">The unique identifier of the originator.</param>
        /// <returns>The pos guid for the associated pos.</returns>
        public abstract Guid PosGuidFromOriginatorGuidGet(Guid originatorGuid);

            /// <summary>
        /// Get the pos id from the originator guid.
        /// </summary>
        /// <param name="deviceGuid">The unique identifier of the originator.</param>
        /// <returns>The numerical identifier for the associated pos.</returns>
        public abstract int PosIdFromOriginatorGuidGet(string deviceGuid);

        /// <summary>
        /// Get the pos id from the originator guid.
        /// </summary>
        /// <param name="originatorGuid">The unique identifier of the originator.</param>
        /// <returns>The numerical identifier for the associated pos.  Null is returned if no pos could be found in the resource layer or no appropriate association has been made to a POS entity.</returns>
        public abstract int? PosIdFromOriginatorGuidGet(Guid originatorGuid);

        /// <summary>
        /// Get the OriginatorTypeId from the originator guid.
        /// </summary>
        /// <param name="deviceGuid">The unique identifier of the originator.</param>
        /// <returns>The numerical OriginatorTypeId.</returns>
        public abstract int OriginatorTypeIdFromOriginatorGuidGet(string deviceGuid);

        /// <summary>
        /// Get pos guid
        /// </summary>
        /// <param name="workstationName">Workstation (computer) name</param>
        /// <param name="profitCenterId">Profit center Id</param>
        /// <returns>Pos guid</returns>
        public abstract PosGuidGetResponse PosForWorkstationGetOrCreate(string workstationName, int profitCenterId);

        #endregion

        #region PosGroupFunctions
        /// <summary>
        /// This method will return a list of records for the Pos Group objects in the system.  If connection is null, <see cref="ConnectionString"/> will be used
        /// </summary>
        /// <param name="connection">The database connection information.</param>
        /// <returns></returns>
        public abstract List<TsPos_Group> PosGroupArchivesGet(ConnectionInfo connection = null);

        /// <summary>
        /// This method will return the various POS Groups
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public abstract List<TsPosGroup> PosGroupGet(int? id = null);

        #endregion

        #region PosLaundryMachineFunctions
        /// <summary>
        /// This method will return a list of records for the PosLaundryMachine objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public abstract List<TsPos_Laundry_Machine> PosLaundryMachineArchivesGet(ConnectionInfo connection);
        #endregion

        #region Product Functions

        /// <summary>
        /// Get a list of products for a specific device.
        /// </summary>
        /// <param name="deviceId">The unique identifier for the device.</param>
        /// <returns>A list of <see cref="Product"/> for the device.</returns>
        public abstract List<Product> ProductDeviceSettingsGet(string deviceId);

        #endregion

        #region Profit Center Functions

        /// <summary>
        /// This method will return all or filtered profit centers
        /// </summary>
        /// <param name="id">The ProfitCenterId filter (if desired)</param>
        /// <returns></returns>
        public abstract List<TsProfitCenter> ProfitCenterGet(int? id = null);


        /// <summary>
        /// This will return all profit centers for a given user.
        /// </summary>
        /// <param name="userId">The user that is associated to a set of profit centers</param>
        /// <param name="id">The ProfitCenterId filter (if desired)</param>
        /// <returns></returns>
        public abstract List<TsProfitCenter> ProfitCenterForUserGet(string userId, int? id = null);

        /// <summary>
        /// Meals count report for PC
        /// </summary>
        /// <param name="profitCenterGuid">Profit center guid</param>
        /// <returns>Meals count report</returns>
        public abstract MealsCountReport ProfitCenterMealsCountGet(Guid profitCenterGuid);

        #endregion

        #region Report Functions

        /// <summary>
        /// Gets drawer audit
        /// </summary>
        /// <param name="sessionGuid"></param>
        /// <returns>Drawer audit</returns>
        public abstract TsDrawerReport DrawerAuditGet(string sessionGuid);

        /// <summary>
        /// Get a pos audit
        /// </summary>
        /// <param name="originatorGuid"></param>
        /// <param name="businessDayType"></param>
        /// <returns>Report</returns>
        public abstract TsDailyReport PosAuditGet(string originatorGuid, int? businessDayType);

        /// <summary>
        /// Get a profit center audit
        /// </summary>
        /// <param name="profitCenterGuid"></param>
        /// <param name="businessDayType"></param>
        /// <returns>Report</returns>
        public abstract TsDailyReport ProfitCenterAuditGet(string profitCenterGuid, int? businessDayType);

        /// <summary>
        /// Get originator operator sessions
        /// </summary>
        /// <param name="originatorGuid">Originator guid</param>
        /// <param name="cashDrawerNumber">Cash drawer number</param>
        /// <returns>Originator operator sessions</returns>
        public abstract List<OperatorSession> OperatorSessionsGet(Guid originatorGuid, int cashDrawerNumber);

        #endregion

        #region RetailTransaction Functions
        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void TransactionByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> TransactionDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsTransaction> TransactionListByDateGet(ConnectionInfo connection, int oleAutomationDate);
        #endregion

        #region Retail Transaction Functions (Slate)

        /// <summary>
        /// Process a retail transaction begin request against the resource layer.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The results of the operation.</returns>
        public abstract RetailTransactionBeginProcessResponse RetailTransactionBeginProcess(RetailTransactionBeginProcessRequest request);

        /// <summary>
        /// Process a retail transaction line item product price modifier request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The results of the operation.</returns>
        public abstract RetailTransactionLineProductPriceModifierResponse RetailTransactionLineProductPriceModifierProcess(RetailTransactionLineProductPriceModifierRequest request);

        /// <summary>
        /// Process a retail transaction line item product request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The results of the operation.</returns>
        public abstract RetailTransactionLineProductProcessResponse RetailTransactionLineProductProcess(RetailTransactionLineProductProcessRequest request);

        /// <summary>
        /// Process a retail transaction line item product tax request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The results of the operation.</returns>
        public abstract RetailTransactionLineProductTaxResponse RetailTransactionLineProductTaxProcess(RetailTransactionLineProductTaxRequest request);

        /// <summary>
        /// Process a retail transaction line item product tax request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The results of the operation.</returns>
        public abstract RetailTransactionLineProductTaxOverrideResponse RetailTransactionLineProductTaxOverrideProcess(RetailTransactionLineProductTaxOverrideRequest request);

        /// <summary>
        /// Process a retail transaction line item tender process request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The results of the operation.</returns>
        public abstract RetailTransactionLineTenderResponse RetailTransactionLineTenderProcess(RetailTransactionLineTenderRequest request);

        /// <summary>
        /// Process a retail transaction line tender SV Card request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The results of the operation.</returns>
        public abstract RetailTransactionLineTenderSvCardProcessResponse RetailTransactionLineTenderSvCardProcess(RetailTransactionLineTenderSvCardProcessRequest request);

        /// <summary>
        /// Process a retail transaction line tender Credit Card request.
        /// </summary>
        /// <param name="transactionId">Transaction Id</param>
        /// <param name="request">The request.</param>
        /// <returns>The results of the operation.</returns>
        public abstract RetailTransactionLineTenderCreditCardResponse RetailTransactionLineTenderCreditCardProcess(int transactionId, RetailTransactionLineTenderCreditCardRequest request);

        /// <summary>
        /// Process a retail transaction line tender Credit Card HIT request.
        /// </summary>
        /// <param name="transactionId">Transaction Id</param>
        /// <param name="request">The request.</param>
        /// <returns>The results of the operation.</returns>
        public abstract RetailTransactionLineTenderCreditCardHitResponse RetailTransactionLineTenderCreditCardHitProcess(int transactionId, RetailTransactionLineTenderCreditCardHitRequest request);

        /// <summary>
        /// Process a retail transaction line tender cash equiv request.
        /// </summary>
        /// <param name="transactionId">Transaction Id</param>
        /// <param name="request">The request.</param>
        /// <returns>The results of the operation.</returns>
        public abstract RetailTransactionLineTenderCashEquivResponse RetailTransactionLineTenderCashEquivProcess(int transactionId, RetailTransactionLineTenderCashEquivRequest request);

        /// <summary>
        /// Process a retail transaction line tender discount request.
        /// </summary>
        /// <param name="transactionId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public abstract RetailTransactionLineTenderDiscountResponse RetailTransactionLineTenderDiscountProcess(int transactionId, RetailTransactionLineTenderDiscountRequest request);

        /// <summary>
        /// Process a retail transaction line tender surcharge request.
        /// </summary>
        /// <param name="transactionId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public abstract RetailTransactionLineTenderSurchargeResponse RetailTransactionLineTenderSurchargeProcess(int transactionId, RetailTransactionLineTenderSurchargeRequest request);

        /// <summary>
        /// Process a retail transaction line stored value type card request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The results of the operation.</returns>
        public abstract RetailTransactionLineStoredValueTypeCardResponse RetailTransactionLineStoredValueTypeCardProcess(RetailTransactionLineStoredValueTypeCardRequest request);

        /// <summary>
        /// Process a retail transaction line stored value type card request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The results of the operation.</returns>
        public abstract RetailTransactionLineStoredValueEnrichmentResponse RetailTransactionLineStoredValueEnrichmentProcess(RetailTransactionLineStoredValueEnrichmentRequest request);

        /// <summary>
        /// Process a retail transaction begin request against the resource layer.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The results of the operation.</returns>
        public abstract RetailTransactionEndProcessResponse RetailTransactionEndProcess(RetailTransactionEndProcessRequest request);

        #endregion        

        #region Retail_Tran Functions
        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Retail_TranByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Retail_TranDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsRetail_Tran> Retail_TranListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Retail_Tran_LineItemByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Retail_Tran_LineItemDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsRetail_Tran_LineItem> Retail_Tran_LineItemListByDateGet(ConnectionInfo connection, int oleAutomationDate);
        
        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Retail_Tran_LineItem_CommentByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Retail_Tran_LineItem_CommentDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsRetail_Tran_LineItem_Comment> Retail_Tran_LineItem_CommentListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Retail_Tran_LineItem_DiscountByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Retail_Tran_LineItem_DiscountDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsRetail_Tran_LineItem_Discount> Retail_Tran_LineItem_DiscountListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region Retail_Tran_LineItem_Mdb_Info

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Retail_Tran_LineItem_Mdb_InfoByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Retail_Tran_LineItem_Mdb_InfoDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsRetail_Tran_LineItem_Mdb_Info> Retail_Tran_LineItem_Mdb_InfoListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region Retail_Tran_LineItem_Prod

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Retail_Tran_LineItem_ProdByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Retail_Tran_LineItem_ProdDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsRetail_Tran_LineItem_Prod> Retail_Tran_LineItem_ProdListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region Retail_Tran_LineItem_Prod_Tax Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Retail_Tran_LineItem_Prod_TaxByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Retail_Tran_LineItem_Prod_TaxDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsRetail_Tran_LineItem_Prod_Tax> Retail_Tran_LineItem_Prod_TaxListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region Retail_Tran_LineItem_Prod_Tx_E Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Retail_Tran_LineItem_Prod_Tx_EByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Retail_Tran_LineItem_Prod_Tx_EDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsRetail_Tran_LineItem_Prod_Tx_E> Retail_Tran_LineItem_Prod_Tx_EListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region Retail_Tran_LineItem_Prod_Tx_O Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Retail_Tran_LineItem_Prod_Tx_OByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Retail_Tran_LineItem_Prod_Tx_ODateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsRetail_Tran_LineItem_Prod_Tx_O> Retail_Tran_LineItem_Prod_Tx_OListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region Retail_Tran_LineItem_ProdPrcMd Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Retail_Tran_LineItem_ProdPrcMdByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Retail_Tran_LineItem_ProdPrcMdDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsRetail_Tran_LineItem_ProdPrcMd> Retail_Tran_LineItem_ProdPrcMdListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region Retail_Tran_LineItem_ProdPromo Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Retail_Tran_LineItem_ProdPromoByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Retail_Tran_LineItem_ProdPromoDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsRetail_Tran_LineItem_ProdPromo> Retail_Tran_LineItem_ProdPromoListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region Retail_Tran_LineItem_StoredVal Functions
        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Retail_Tran_LineItem_StoredValByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Retail_Tran_LineItem_StoredValDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsRetail_Tran_LineItem_StoredVal> Retail_Tran_LineItem_StoredValListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region Retail_Tran_LineItem_Surcharge Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Retail_Tran_LineItem_SurchargeByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Retail_Tran_LineItem_SurchargeDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsRetail_Tran_LineItem_Surcharge> Retail_Tran_LineItem_SurchargeListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region Retail_Tran_LineItem_Sv_Card Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Retail_Tran_LineItem_Sv_CardByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Retail_Tran_LineItem_Sv_CardDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsRetail_Tran_LineItem_Sv_Card> Retail_Tran_LineItem_Sv_CardListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region Retail_Tran_LineItem_Sv_Cust Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Retail_Tran_LineItem_Sv_CustByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Retail_Tran_LineItem_Sv_CustDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsRetail_Tran_LineItem_Sv_Cust> Retail_Tran_LineItem_Sv_CustListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region Retail_Tran_LineItem_Sv_Enrich Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Retail_Tran_LineItem_Sv_EnrichByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Retail_Tran_LineItem_Sv_EnrichDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsRetail_Tran_LineItem_Sv_Enrich> Retail_Tran_LineItem_Sv_EnrichListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region Retail_Tran_LineItem_Tax Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Retail_Tran_LineItem_TaxByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Retail_Tran_LineItem_TaxDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsRetail_Tran_LineItem_Tax> Retail_Tran_LineItem_TaxListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region Retail_Tran_LineItem_Tender Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Retail_Tran_LineItem_TenderByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Retail_Tran_LineItem_TenderDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsRetail_Tran_LineItem_Tender> Retail_Tran_LineItem_TenderListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region Retail_Tran_LineItem_Tender_Cc Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Retail_Tran_LineItem_Tender_CcByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Retail_Tran_LineItem_Tender_CcDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsRetail_Tran_LineItem_Tender_Cc> Retail_Tran_LineItem_Tender_CcListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region Retail_Tran_LineItem_Tender_Sv Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Retail_Tran_LineItem_Tender_SvByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Retail_Tran_LineItem_Tender_SvDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsRetail_Tran_LineItem_Tender_Sv> Retail_Tran_LineItem_Tender_SvListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region Retail_Tran_LineItem_Tndr_Card Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Retail_Tran_LineItem_Tndr_CardByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Retail_Tran_LineItem_Tndr_CardDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsRetail_Tran_LineItem_Tndr_Card> Retail_Tran_LineItem_Tndr_CardListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region Retail_Tran_LineItem_Tndr_Cdv Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Retail_Tran_LineItem_Tndr_CdvByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Retail_Tran_LineItem_Tndr_CdvDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsRetail_Tran_LineItem_Tndr_Cdv> Retail_Tran_LineItem_Tndr_CdvListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region Retail_Tran_LineItem_Tndr_Ce Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Retail_Tran_LineItem_Tndr_CeByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Retail_Tran_LineItem_Tndr_CeDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsRetail_Tran_LineItem_Tndr_Ce> Retail_Tran_LineItem_Tndr_CeListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region Retail_Tran_LineItem_Tndr_Cgv Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Retail_Tran_LineItem_Tndr_CgvByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Retail_Tran_LineItem_Tndr_CgvDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsRetail_Tran_LineItem_Tndr_Cgv> Retail_Tran_LineItem_Tndr_CgvListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region Retail_Tran_LineItem_Tndr_Cust Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Retail_Tran_LineItem_Tndr_CustByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Retail_Tran_LineItem_Tndr_CustDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsRetail_Tran_LineItem_Tndr_Cust> Retail_Tran_LineItem_Tndr_CustListByDateGet(ConnectionInfo connection, int oleAutomationDate);


        #endregion

        #region Retail_Tran_LineItem_Tndr_Disc Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Retail_Tran_LineItem_Tndr_DiscByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Retail_Tran_LineItem_Tndr_DiscDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsRetail_Tran_LineItem_Tndr_Disc> Retail_Tran_LineItem_Tndr_DiscListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region Retail_Tran_LineItem_Tndr_Srch Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Retail_Tran_LineItem_Tndr_SrchByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Retail_Tran_LineItem_Tndr_SrchDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsRetail_Tran_LineItem_Tndr_Srch> Retail_Tran_LineItem_Tndr_SrchListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region Retail_Tran_Total Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Retail_Tran_TotalByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Retail_Tran_TotalDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsRetail_Tran_Total> Retail_Tran_TotalListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region Reuse Delay Functions

        /// <summary>
        /// Get customer reuse delay log list
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <returns>List of customer reuse delays</returns>
        public abstract List<TsReuseDelayLog> ReuseDelayLogListGet(int customerId);

        /// <summary>
        /// Reset delay log
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="reuseDelayLogIds">Rd Log Id</param>
        public abstract void ReuseDelayLogReset(int customerId, List<int> reuseDelayLogIds);

        #endregion

        #region Security Functions

        /// <summary>
        /// Check with the database if it is ok to bypass a password check in validate
        /// </summary>
        /// <returns></returns>
        public abstract bool BypassUserPasswordCheck();

        /// <summary>
        /// Get a user's password history from Oracle
        /// </summary>
        /// <returns></returns>
        public abstract List<PasswordHistoryItem> PasswordHistoryGet(string username);

        /// <summary>
        /// Add a user password entry into an oracle database
        /// </summary>
        /// <param name="username">username attached to the history entry</param>
        /// <param name="dateTime">password expiration date time</param>
        /// <param name="hash">hashed password as a string of interpreted bytes (ie. string representation of bytes).  See BbTS.Security.CommonFunctions.cs:ByteArrayToString.)</param>
        /// <param name="salt">string representation of the salt</param>
        /// <param name="hashType">hash type 1=Sha1 (should not be used), 2=PBK, 3=DHE</param>
        /// <param name="iterationCount">iteration count for hashing function</param>
        public abstract void PasswordHistorySet(string username, DateTime dateTime, byte[] hash, byte[] salt, int hashType, int iterationCount);

        /// <summary>
        /// Set a user password in an oracle database
        /// </summary>
        /// <param name="username">username attached to the history entry</param>
        /// <param name="dateTime">password expiration date time</param>
        /// <param name="hash">hashed password as a string of interpreted bytes (ie. string representation of bytes).  See BbTS.Security.CommonFunctions.cs:ByteArrayToString.)</param>
        /// <param name="salt">string representation of the salt</param>
        /// <param name="hashType">hash type 1=Sha1 (should not be used), 2=PBK, 3=DHE</param>
        /// <param name="iterationCount">iteration count for hashing function</param>
        public abstract void PasswordSet(string username, DateTime dateTime, byte[] hash, byte[] salt, int hashType, int iterationCount);

        /// <summary>
        /// Process a failed user login against the Transact system
        /// </summary>
        /// <param name="userName">User name</param>
        /// <param name="isSuperUser">Has elevated privileges</param>
        /// <param name="isAdmin">Is an admin.</param>
        /// <param name="program">Calling application.</param>
        /// <param name="workstationId">Id of the workstation.</param>

        public abstract void ProcessFailedLogin(string userName, bool isSuperUser, bool isAdmin, string program, int workstationId);

        /// <summary>
        /// Process a successful user login against the Transact system.
        /// </summary>
        /// <param name="userName">User name</param>
        /// <param name="isSuperUser">Has elevated privileges</param>
        /// <param name="isAdmin">Is an admin.</param>
        /// <param name="program">Calling application.</param>
        /// <param name="workstationId">Id of the workstation.</param>
        public abstract void ProcessSuccessfulLogin(string userName, bool isSuperUser, bool isAdmin, string program, int workstationId);

        /// <summary>
        /// Resource layer function for setting the session token.
        /// </summary>
        /// <param name="token">The token to set.</param>
        public abstract void SessionTokenSet(SessionToken token);

        /// <summary>
        /// Fetch the user login session token from the provided token id
        /// </summary>
        /// <param name="tokenId">Id of the token</param>
        /// <returns>SessionToken object matching the id</returns>
        public abstract SessionToken SessionTokenGet(string tokenId);

        /// <summary>
        /// Get the user accounts rules from an oracle database
        /// </summary>
        /// <returns>user accounts rules as stored in an oracle database</returns>
        public abstract UserAccountRules UserAccountRulesGet();

        /// <summary>
        /// Set user account rules
        /// </summary>
        /// <param name="rules">UserAccountRules object</param>
        public abstract void UserAccountRulesSet(UserAccountRules rules);

        /// <summary>
        /// Get door states
        /// </summary>
        /// <param name="doorId">Optional filter on door id</param>
        /// <returns>List of door states</returns>
        public abstract List<DoorEventLog> DoorStatesGet(int? doorId = null);

        /// <summary>
        /// Get assa abloy door states
        /// </summary>
        /// <param name="doorId">Optional filter on door id</param>
        /// <returns>List of assa abbloy door states</returns>
        public abstract List<DoorAaEventLog> DoorAaStatesGet(int? doorId = null);

        /// <summary>
        /// Get assa abloy doors connection info
        /// </summary>
        /// <param name="doorIds">Door Ids</param>
        /// <returns>List of ssa abloy doors connection info</returns>
        public abstract List<DoorAaConnection> DoorAaConnectionGet(List<int> doorIds);

        /// <summary>
        /// Get door alarms
        /// </summary>
        /// <param name="doorId">Door filter</param>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <returns>Alarm logs</returns>
        public abstract List<DoorAlarmLog> DoorAlarmsGet(int? doorId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// Get active door alarms
        /// </summary>
        /// <returns>Alarm logs</returns>
        public abstract List<DoorAlarmLog> ActiveAlarmsGet();

        /// <summary>
        /// Get active stale door alarms
        /// </summary>
        /// <returns>Alarm logs</returns>
        public abstract List<DoorAlarmLog> ActiveStaleAlarmsGet();

        /// <summary>
        /// Get doors by user
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="accessPlanActive">Optional access plan active filter</param>
        /// <returns>User doors</returns>
        public abstract List<UserDoor> DoorsByUserGet(int customerId, bool? accessPlanActive = null);

        /// <summary>
        /// Get doors by user
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="accessPlanActive">Optional access plan active filter</param>
        /// <returns>User doors</returns>
        public abstract List<UserDoorAa> DoorsAaByUserGet(int customerId, bool? accessPlanActive = null);

        /// <summary>
        /// Get users by door
        /// </summary>
        /// <param name="doorId">Door id</param>
        /// <param name="customerActive">Optional customer active filter</param>
        /// <param name="accessPlanActive">Optional access plan active filter</param>
        /// <returns>Door users</returns>
        public abstract List<DoorUser> UsersByDoorGet(int doorId, bool? customerActive = null, bool? accessPlanActive = null);

        /// <summary>
        /// Get users by assa abloy door
        /// </summary>
        /// <param name="doorId">Door id</param>
        /// <param name="customerActive">Optional customer active filter</param>
        /// <param name="accessPlanActive">Optional access plan active filter</param>
        /// <returns>Assa abloy door users</returns>
        public abstract List<DoorAaUser> UsersByDoorAaGet(int doorId, bool? customerActive = null, bool? accessPlanActive = null);

        /// <summary>
        /// Get door transaction by either door Id or customer Id
        /// </summary>
        /// <param name="doorId">Door Id</param>
        /// <param name="customerId">Customer Id</param>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <returns>Door transactions</returns>
        public abstract List<DoorTransaction> DoorTransactionsGet(int? doorId, int? customerId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// Get door transaction by building Id
        /// </summary>
        /// <param name="buildingId">Building Id</param>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <returns>Door transactions</returns>
        public abstract List<DoorTransaction> DoorTransactionsGet(int buildingId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// Get assa abloy door transaction by either door Id or customer Id or building Id
        /// </summary>
        /// <param name="doorId">Door Id</param>
        /// <param name="customerId">Customer Id</param>
        /// <param name="buildingId">Building Id</param>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <returns>Assa abloy door transactions</returns>
        public abstract List<DoorAaTransaction> DoorAaTransactionsGet(int? doorId, int? customerId, int? buildingId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// Get door history
        /// </summary>
        /// <param name="doorId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns>Door history</returns>
        public abstract List<DoorActivity> DoorHistoryGet(int doorId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// Set master controller
        /// </summary>
        /// <param name="masterControllerId">MC Id</param>
        /// <param name="masterController">MC</param>
        public abstract void MasterControllerSet(int? masterControllerId, MasterController masterController);

        /// <summary>
        /// Set door
        /// </summary>
        /// <param name="doorId">Door Id</param>
        /// <param name="door">Door</param>
        public abstract void DoorSet(int? doorId, Domain.Models.Security.Doors.Door door);

        /// <summary>
        /// Set Aa door
        /// </summary>
        /// <param name="doorId">Door Id</param>
        /// <param name="door">Door</param>
        public abstract void DoorAaSet(int? doorId, DoorAa door);

        /// <summary>
        /// Get all doors
        /// </summary>
        /// <returns>Doors</returns>
        public abstract List<DoorAccessPoint> DoorsGet();

        /// <summary>
        /// Set ackowledgement of door alarm
        /// </summary>
        /// <param name="alarmLogId">Log id</param>
        /// <param name="ackInfo">Ackowledgement info</param>
        public abstract void DoorAlarmAcknowledgeSet(Int64 alarmLogId, DoorAcknowledgeAlarm ackInfo);

        /// <summary>
        /// Ackowledge door alarms
        /// </summary>
        /// <param name="doorId">Door id</param>
        /// <param name="ackInfo">Ackowledgement info</param>
        public abstract void DoorAlarmAcknowledge(int? doorId, DoorAcknowledgeAlarmInfo ackInfo);

        /// <summary>
        /// Get all doors
        /// </summary>
        /// <param name="request">request</param>
        /// <returns>Doors</returns>
        public abstract DoorListGetResponse DoorListGet(DoorListGetRequest request);

        /// <summary>
        /// Get doors
        /// </summary>
        /// <param name="request">request</param>
        /// <returns>Doors</returns>
        public abstract List<DoorInfo> DoorInfoGet(DoorInfoGetRequest request);

        /// <summary>
        /// Door api keys
        /// </summary>
        /// <returns>Door api keys</returns>
        public abstract DoorApiKeyGetResponse DoorApiKeyGet();

        /// <summary>
        /// Set door api keys
        /// </summary>
        /// <param name="request">Request</param>
        /// <returns>Operation result</returns>
        public abstract void DoorApiKeySet(DoorApiKeySetRequest request);

        #endregion

        #region Security Monitor Functions

        /// <summary>
        /// Get the host connection from a door id.
        /// </summary>
        /// <param name="doorId">the numberical identifier for the door.</param>
        /// <returns></returns>
        public abstract StatsConnection HostConnectionFromDoorIdGet(int doorId);

        /// <summary>
        /// Get a list of host connection information for a set of doors defined by numerical id.
        /// </summary>
        /// <param name="request">List of the the numberical identifier for the doors.</param>
        /// <returns></returns>
        public abstract List<StatsConnection> HostConnectionListGet(DoorStateChangeRequest request);

        /// <summary>
        /// Register a list of doors for a DBMS_ALERT event.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        public abstract RegisterSignalListResponse RegisterSignalList(RegisterSignalListRequest request);

        /// <summary>
        /// Wait for a DBMS_ALERT door access monitor event.
        /// </summary>
        /// <returns>Wait response.</returns>
        public abstract DbmsCallbackResponse WaitForDaMonitorEvent();

        /// <summary>
        /// Create an association between doors and a registered security monitor subscribing client.
        /// </summary>
        /// <param name="request">The request for association.</param>
        /// <returns>The result of the operation.</returns>
        public abstract DoorClientAssociationtResponse DoorClientAssociationCreate (DoorClientAssociationRequest request);

        /// <summary>
        /// Remove an association between doors and a registered security monitor subscribing client.
        /// </summary>
        /// <param name="request">The request for disassociation.</param>
        /// <returns>The result of the operation.</returns>
        public abstract DoorClientAssociationtResponse DoorClientAssociationRemove(DoorClientAssociationRequest request);

        #endregion

        #region Session_Control Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Session_ControlByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Session_ControlDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsSession_Control> Session_ControlListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region StoredValueAccount functions

        /// <summary>
        /// This method will return the list of Sv_Account records in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public abstract List<TsSv_Account> SvAccountArchivesGet(ConnectionInfo connection);

        /// <summary>
        /// Retrieve the stored value account settings for a device.
        /// </summary>
        /// <param name="deviceId">The unique identifier for the device.</param>
        /// <returns>List of stored value accounts for the device.</returns>
        public abstract List<StoredValueAccountDeviceSetting> StoredValueAccountDeviceSettingsGet(string deviceId);

        /// <summary>
        /// Get list of stored value account types
        /// </summary>
        /// <param name="svAccountTypeId">Id filter</param>
        /// <returns>Stored value account types</returns>
        public abstract List<TsStoredValueAccountType> SvAccountTypeGet(int? svAccountTypeId = null);

        /// <summary>
        /// Get list of stored value account types
        /// </summary>
        /// <returns>Stored value account types</returns>
        public abstract List<StoredValueAccountType> SvAccountTypeGetAll();

        /// <summary>
        /// Get Sv enrichments for sv account type
        /// </summary>
        /// <param name="svAccountTypeId">Sv account type Id</param>
        /// <returns>List of Sv enrichments</returns>
        public abstract List<StoredValueEnrichment> SvEnrichmentsGet(int? svAccountTypeId = null);

        #endregion

        #region Stored Value Deposit Functions

        /// <summary>
        /// Process a stored value deposit request.
        /// </summary>
        /// <param name="transaction">The request to process</param>
        /// <returns></returns>
        public abstract Transaction ProcessStoredValueDepositRequest(Transaction transaction);

        /// <summary>
        /// Process a stored value deposit return request.
        /// </summary>
        /// <param name="transaction">The request to process</param>
        /// <returns></returns>
        public abstract Transaction ProcessStoredValueDepositReturnRequest(Transaction transaction);

        #endregion

        #region StoredValueDenial Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void StoredValueDenialByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> StoredValueDenialDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsStoredValueDenial> StoredValueDenialListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region Stored Value Transactions Extraction Functions

        /// <summary>
        /// Initializes the DB facility for retrieving recent transactions.
        /// </summary>
        /// <param name="initExtractionStart">Extraction Start to be used as an override of the internal DB value, may be null</param>
        /// <param name="initExtractionEnd">Extraction End to be used as an override of the internal DB value, may be null</param>
        /// <returns>void</returns>
        public abstract void InitializeStoredValueTransactionsExtraction(DateTime? initExtractionStart, DateTime? initExtractionEnd);

        /// <summary>
        /// Commits that the extracted transactions were successfully processed and the DB facility can update its internal state.
        /// </summary>
        /// <returns>void</returns>
        public abstract void FinalizeStoredValueTransactionsExtraction();

        /// <summary>
        /// Resets the facility for retrieving the transactions from DB.
        /// </summary>
        /// <returns>void</returns>
        public abstract void ResetStoredValueTransactionsExtraction();

        /// <summary>
        /// Context for chunked retrieval of transactions, created in <see cref="ResourceDatabase.ExtractStoredValueTransactionsBegin"/> 
        /// and used in <see cref="ResourceDatabase.ExtractStoredValueTransactionsGetNext"/>
        /// </summary>
        public interface IExtractStoredValueTransactionsContext
        {
            Boolean HasMoreData();
        }

        /// <summary>
        /// Requests start of transaction retrieval. Returned context is used in consequent calls of ExtractStoredValueTransactionsGetNext,
        /// which in turn returns a chunk of transactions. This method should be called after InitializeStoredValueTransactionsExtraction.
        /// Returned context needs to be used in consequent calls of <see cref="ResourceDatabase.ExtractStoredValueTransactionsGetNext"/>
        /// Once all the data are retrieved, FinalizeStoredValueTransactionsExtraction must be called to commit that this round was successful
        /// and to advance to new data.
        /// <param name="extractionStart">output parameter - extraction start</param>
        /// <param name="extractionEnd">output parameter - extraction end</param>
        /// </summary>
        /// <returns>context to be used for chunked retrieval, <see cref="IExtractStoredValueTransactionsContext"/>.</returns>
        public abstract IExtractStoredValueTransactionsContext ExtractStoredValueTransactionsBegin(out DateTime extractionStart, out DateTime extractionEnd);

        /// <summary>
        /// The context indicates whether there are more data or not. If there are more data then this method needs to be called again.
        /// </summary>
        /// <param name="context"><see cref="IExtractStoredValueTransactionsContext"/> - context for chunked retrieval, returned by <see cref="ResourceDatabase.ExtractStoredValueTransactionsBegin"/></param>
        /// <param name="maxItems"></param>
        /// <returns>List of <see cref="StoredValueTransaction"/> transactions.</returns>
        public abstract List<StoredValueTransaction> ExtractStoredValueTransactionsGetNext(IExtractStoredValueTransactionsContext context, int maxItems);
        #endregion

        #region Subscription Functions

        /// <summary>
        /// Get the subscription registration information for a client and service.
        /// </summary>
        /// <param name="clientGuid">The unique identifier for the client.  Null for all clients.</param>
        /// <param name="subscriptionType">The type of service. <see cref="SubscriptionServiceType.None"/> for all service types.</param>
        /// <returns>List of subscription registration information.</returns>
        public abstract List<SubscriptionRegistration> SubscriptionRegistrationGet(string clientGuid = null, SubscriptionServiceType subscriptionType = SubscriptionServiceType.None);

        /// <summary>
        /// Register for a subscription service.
        /// </summary>
        /// <param name="request">The registration request.</param>
        /// <param name="username">Username</param>
        /// <returns>The results of the operation.</returns>
        public abstract SubscriptionRegistrationResponse SubscriptionServiceRegister(SubscriptionRegistrationRequest request, string username);

        /// <summary>
        /// Unregister for a subscription service.
        /// </summary>
        /// <param name="request">The registration request.</param>
        /// <returns>The results of the operation.</returns>
        public abstract SubscriptionRegistrationResponse SubscriptionServiceUnregister(SubscriptionRegistrationRequest request);

        /// <summary>
        /// Update the last communicated date and time for a client and service.
        /// </summary>
        /// <param name="clientGuid">The unique identifier for the client.</param>
        /// <param name="type">The type of service.</param>
        /// <param name="lastCommunicated">The date and time the client was last communicated with.</param>
        public abstract void SubscriptionServiceUpdateLastCommunicated(string clientGuid, SubscriptionServiceType type, DateTimeOffset lastCommunicated);

        /// <summary>
        /// Toggle the setting in the data layer that indicates whether or not to send all stored messages on the next callback.
        /// </summary>
        /// <param name="clientGuid">The unique identifier for the client.</param>
        /// <param name="type">The type of service.</param>
        /// <param name="sendAllOnNextCallback">Send all or not.</param>
        public abstract void SubscriptionServiceToggleReceiveAll(string clientGuid, SubscriptionServiceType type, bool sendAllOnNextCallback);

        #endregion

        #region Sv_Account_History_Rtl_Trn_Sv Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Sv_Account_History_Rtl_Trn_SvByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Sv_Account_History_Rtl_Trn_SvDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsSv_Account_History_Rtl_Trn_Sv> Sv_Account_History_Rtl_Trn_SvListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region Sv_Account_History_Rtl_Trn_Tdr Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Sv_Account_History_Rtl_Trn_TdrByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Sv_Account_History_Rtl_Trn_TdrDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsSv_Account_History_Rtl_Trn_Tdr> Sv_Account_History_Rtl_Trn_TdrListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region SystemFunctions

        /// <summary>
        /// Write an activity log to the database resource layer.
        /// </summary>
        /// <param name="activityLogGuid">The unique identifier for the action.</param>
        /// <param name="origin">The entity making the log entry.</param>
        /// <param name="data">The data to log.</param>
        public abstract void ActivityLogSet<T>(string activityLogGuid, ActivityLogOriginEntity origin, T data);

        /// <summary>
        /// Write an audit log entry in an oracle database.
        /// </summary>
        /// <param name="logItem"><see cref="AuditLogItem"/> object to log.</param>
        public abstract void AuditLogSet(AuditLogItem logItem);

        /// <summary>
        /// This method will return the control parameter value for the passed in name
        /// </summary>
        /// <param name="controlParameterName">The name of the control parameter to retrieve</param>
        /// <returns></returns>
        public abstract string ControlParameterValueGet(string controlParameterName);

        /// <summary>
        /// Get the value of a domain data entry in an oracle database.
        /// </summary>
        /// <param name="domainName">Domain name of the value</param>
        /// <param name="domainId">Id of the domain value</param>
        /// <returns>value as a string</returns>
        public abstract string DomainDataValueGet(string domainName, int domainId);

        /// <summary>
        /// Get the value of a domain data entry in an oracle database.
        /// </summary>
        /// <param name="domainTypeId">Domain TypeId of the domain</param>
        /// <param name="domainId">Id of the domain value</param>
        /// <returns>value as a string</returns>
        public abstract string DomainDataValueGet(int domainTypeId, int domainId);

        /// <summary>
        /// Get a list of domain values matching the requested domain.
        /// </summary>
        /// <returns>List of <see cref="DomainViewObject"/>s pertaining to the requested domain.</returns>
        public abstract List<DomainViewObject> DomainListGet(string domain);

        /// <summary>
        /// This method will return the list of host names in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public abstract List<TsHostNames> HostNamesGet(ConnectionInfo connection);

        /// <summary>
        /// This method is used to record the installation package just installed in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="packageId"></param>
        /// <param name="packageNumber"></param>
        /// <param name="packageName"></param>
        /// <param name="packageVersion"></param>
        /// <param name="databaseName"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="databaseSchema"></param>
        public abstract void InstallPackageSet(ConnectionInfo connection, String packageId, String packageNumber, String packageName, String packageVersion, String databaseName, DateTime startDate, DateTime endDate, String databaseSchema);

        /// <summary>
        /// This method will test connectivity to the database
        /// </summary>
        /// <returns></returns>
        public abstract ActionResultToken PingDatabase();

        /// <summary>
        /// This method will return a system features object
        /// </summary>
        /// <returns></returns>
        public abstract SystemFeatures SystemFeaturesGet();

        /// <summary>
        /// Get a User object from a username from the data layer
        /// </summary>
        /// <param name="userName">user to fetch</param>
        /// <returns><see cref="User"/> that was retrieved.</returns>
        public abstract User UserByNameGet(string userName);

        /// <summary>
        /// Set or modify user information in the data layer
        /// </summary>
        /// <param name="user">The user to modify.</param>
        public abstract void UserSet(User user);

        /// <summary>
        /// Gets the application credential by consumer key from the transact api stores. 
        /// </summary>
        /// <param name="consumerKey">The consumer key.</param>
        /// <returns>The transact api application credential from the database resource layer.</returns>
        public abstract GetApplicationCredentialByConsumerKeyResponse GetTransactApiApplicationCredential(Guid consumerKey);

        /// <summary>
        /// Gets all control parameter groups
        /// </summary>
        /// <returns>A list of all control parameter groups</returns>
        public abstract List<string> GroupParameterListGet();

        /// <summary>
        /// Gets control parameters by the name of group
        /// </summary>
        /// <param name="groupParameterName">Control parameter group name</param>
        /// <returns>A list of control parameters that belongs to specified group</returns>
        public abstract List<ControlParameter> ParameterListGet(string groupParameterName);

        /// <summary>
        /// Sets the control parameter in the resource layer
        /// </summary>
        /// <param name="request">The request containing the control parameter to set</param>
        /// <returns>Control parameter that has been set</returns>
        public abstract ControlParameter ParameterSet(ControlParameterPatchRequest request);

        /// <summary>
        /// Gets the control paramater by name
        /// </summary>
        /// <param name="parameterName">Name of the control parameter</param>
        /// <returns>Control parameter that matches the name</returns>
        public abstract ControlParameter ParameterGet(string parameterName);

        /// <summary>
        /// Delete a control parameter by name.
        /// </summary>
        /// <param name="parameterName">Name of the control parameter</param>
        public abstract void ParameterDelete(string parameterName);

        #endregion

        #region TaxGroupsFunctions
        /// <summary>
        /// This method will return a list of records for the TaxGroups objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public abstract List<TsTaxGroups> TaxGroupsArchivesGet(ConnectionInfo connection);
        #endregion

        #region TaxScheduleFunctions
        /// <summary>
        /// This method will return a list of records for the given objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public abstract List<TsTaxSchedule> TaxScheduleArchivesGet(ConnectionInfo connection);

        /// <summary>
        /// Get the Tax Schedules for a device.
        /// </summary>
        /// <param name="deviceId">The unique identifier for the Device.</param>
        /// <returns>List of tax schedules for a device.</returns>
        public abstract List<TaxScheduleDeviceSetting> TaxScheduleDeviceSettingsGet(string deviceId);

        /// <summary>
        /// Get all tenders associated with a tax schedule.
        /// </summary>
        /// <param name="taxScheduleId">The unique identifier for the tax schedule</param>
        /// <returns>List of tenders and tax rates associated with the tax schedule.</returns>
        public abstract List<TaxScheduleTender> TaxScheduleTendersGet(int taxScheduleId);

        #endregion

        #region TenderFunctions

        /// <summary>
        /// This method will return a list of records for the Tender objects in the system.  If connection is null, <see cref="ConnectionString"/> will be used
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public abstract List<TsTender> TenderArchivesGet(ConnectionInfo connection = null);

        /// <summary>
        /// Get the tenders assigned to a device.
        /// </summary>
        /// <param name="deviceId">The unique identifier for the device.</param>
        /// <returns>List of tenders assigned to the device.</returns>
        public abstract List<TenderDeviceSetting> TenderDeviceSettingsGet(string deviceId);

        /// <summary>
        /// This method will return a list of records for the Stored Value Tender objects that are available at the specified POS.  If connection is null, <see cref="ConnectionString"/> will be used
        /// </summary>
        /// <param name="posId"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public abstract List<TsTender> StoredValueTendersForPos(int posId, ConnectionInfo connection = null);

        /// <summary>
        /// Get tenders by merchant
        /// </summary>
        /// <param name="tenderType">Tender Type</param>
        /// <returns>Merchant tenders</returns>
        public abstract List<TsTender2> TenderGet(TenderType? tenderType);

        /// <summary>
        /// Transaction history get
        /// </summary>
        /// <param name="request">Request</param>
        /// <returns>Transaction history</returns>
        public abstract TransactionHistoryListGetResponse TransactionHistoryListGet(TransactionHistoryListGetRequest request);

        #endregion

        #region TiaTransactionLog Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void TiaTransactionLogByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> TiaTransactionLogDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsTiaTransactionLog> TiaTransactionLogListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region Transaction_Cashier Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Transaction_CashierByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Transaction_CashierDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsTransaction_Cashier> Transaction_CashierListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region Transaction_Communication Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Transaction_CommunicationByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Transaction_CommunicationDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsTransaction_Communication> Transaction_CommunicationListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region Transaction_External_Client Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Transaction_External_ClientByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Transaction_External_ClientDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsTransaction_External_Client> Transaction_External_ClientListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region Transaction_Laundry_Machine Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void Transaction_Laundry_MachineByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> Transaction_Laundry_MachineDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsTransaction_Laundry_Machine> Transaction_Laundry_MachineListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        #endregion

        #region TransactionObjectLog Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public abstract void TransactionObjectLogByDateDelete(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<int> TransactionObjectLogDateListGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public abstract List<TsTransactionObjectLog> TransactionObjectLogListByDateGet(ConnectionInfo connection, int oleAutomationDate);

        /// <summary>
        /// This method will return a list of records for the given table
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public abstract List<TsTransactionObjectLog> TransactionObjectLogListGet(ConnectionInfo connection);
        #endregion

        #region Transaction Functions

        /// <summary>
        /// Get the last transaction number for the specified originator.
        /// </summary>
        /// <param name="originatorId"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public abstract int? LastTransactionNumberGet(Guid originatorId, ConnectionInfo connection = null);

        /// <summary>
        /// Insert a denied transaction into the database.
        /// </summary>
        /// <param name="errorCode"></param>
        /// <param name="deniedText"></param>
        /// <param name="requestGuid"></param>
        /// <param name="transactionClassification"></param>
        /// <param name="transactionGuid"></param>
        /// <param name="originatorGuid"></param>
        /// <param name="forcePost"></param>
        /// <param name="startTimeStamp"></param>
        /// <param name="transactionNumber"></param>
        /// <param name="customerCard"></param>
        /// <param name="swiped"></param>
        /// <param name="customerGuid"></param>
        /// <param name="operatorGuid"></param>
        /// <param name="tenderId"></param>
        /// <param name="deniedAmount"></param>
        /// <param name="attendedEventId"></param>
        /// <param name="attendedEventTransactionType"></param>
        /// <param name="boardMealTypeId"></param>
        /// <param name="connection"></param>
        public abstract void TransactionDenialSet(
            //Required
            int errorCode,
            string deniedText,
            Guid requestGuid,
            TransactionClassification transactionClassification,
            Guid transactionGuid,
            Guid originatorGuid,
            bool forcePost,
            DateTimeOffset startTimeStamp,
            //Optional
            int? transactionNumber,
            CardCapture customerCard,
            bool? swiped,
            Guid? customerGuid,
            Guid? operatorGuid,
            int? tenderId,
            decimal? deniedAmount,
            int? attendedEventId,
            AttendedEventTranType? attendedEventTransactionType,
            int? boardMealTypeId,
            ConnectionInfo connection = null);

        /// <summary>
        /// Get a list of logs relating to customer transactions at a given location for the provided time frame.
        /// </summary>
        /// <param name="startDate">Start of the log request period</param>
        /// <param name="endDate">End of the log request period</param>
        /// <returns></returns>
        public abstract List<CustomerTransactionLocationLog> CustomerTransactionLocationLogsGet(DateTime startDate,
            DateTime endDate);

        #endregion

        #region Transaction Log Functions

        /// <summary>
        /// Resource class for setting a transaction log in an oracle database.
        /// </summary>
        /// <param name="transaction"><see cref="Transaction"/> object to set.</param>
        [Obsolete("This should only be used for MF4100 backward-compatibility purposes.")]
        public abstract void TransactionLogSet(Transaction transaction);

        /// <summary>
        /// Resource class for setting a transaction log in an oracle database.  (Transaction MF4100 View Version 1)
        /// </summary>
        /// <param name="transactionMf4100ViewV01"><see cref="TransactionMf4100ViewV01"/> object to set.</param>
        /// <param name="requestId">The request ID, if available.</param>
        public abstract void TransactionLogSet(TransactionMf4100ViewV01 transactionMf4100ViewV01, Guid? requestId);

        /// <summary>
        /// Resource class for setting a transaction log in an oracle database.  (Transaction View Version 1)
        /// </summary>
        /// <param name="transactionViewV01"><see cref="TransactionViewV01"/> object to set.</param>
        /// <param name="requestId">The request ID, if available.</param>
        public abstract void TransactionLogSet(TransactionViewV01 transactionViewV01, Guid? requestId);

        /// <summary>
        /// Resource class for setting a transaction log in an oracle database.  (Transaction View Version 2)
        /// </summary>
        /// <param name="transactionViewV02"><see cref="TransactionViewV02"/> object to set.</param>
        /// <param name="requestId">The request ID, if available.</param>
        public abstract void TransactionLogSet(TransactionViewV02 transactionViewV02, Guid? requestId);

        /// <summary>
        /// Resource class for setting a transaction log in an oracle database when the transaction information is not parsable.  This should only be used when incoming JSON cannot be deserialized.
        /// </summary>
        /// <param name="clob">The string clob to write to the data layer.</param>
        /// <param name="disposition">The disposition to set the clob to.  Default to <see cref="Disposition.Unknown"/>.</param>
        /// <param name="requestId">The request ID, if available.</param>
        public abstract void TransactionLogSet(string clob, Disposition disposition, Guid? requestId = null);

        #endregion

        #region Transaction Validation Functions

        /// <summary>
        /// Validate the board meal type id.
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Result of the validation operation.</returns>
        public abstract BoardMealTypeValidationResponse BoardMealTypeValidate(BoardMealTypeValidationRequest request);

        /// <summary>
        /// Validate the customer transaction info portion of a transaction.
        /// </summary>
        /// <param name="request">The customer transaction info.</param>
        /// <returns>The result of the operation.</returns>
        public abstract CustomerTransactionInfoValidationResponse CustomerTransactionInfoValidation(CustomerTransactionInfoValidationRequest request);

        /// <summary>
        /// Validate a cashier (operator) component of a transaction.
        /// </summary>
        /// <param name="cashierGuid">The unique (Guid) identification value assigned to the cashier (operator).</param>
        /// <param name="deviceGuid">The unique (Guid) identifier assigned to the device.</param>
        /// <param name="forcePost">Force post flag, defaults to false</param>
        /// <returns>The results of the operation.</returns>
        public abstract OperatorByOperatorGuidValidationResponse OperatorByOperatorGuidValidation(string cashierGuid, string deviceGuid, bool forcePost = false);


        /// <summary>
        /// Validate a product promo component of a transaction.
        /// </summary>
        /// <param name="productPromoId">Id of the product promo.</param>
        /// <returns>Result of a product promo validation request.</returns>
        public abstract ProductPromoValidationResponse ProductPromoValidation(int productPromoId);

        /// <summary>
        /// Validate a retail transaction line product price modify component of a transaction.
        /// </summary>
        /// <param name="request">The request that spawned this response.</param>
        /// <returns>Result of the validation process</returns>
        public abstract RetailTransactionLineProductPriceModifierResponse RetailTransactionLineProductPriceModifyValidation(RetailTransactionLineProductPriceModifierRequest request);

        /// <summary>
        /// Validate a retail transaction line product component of a transaction.
        /// </summary>
        /// <param name="request">The request that spawned this response.</param>
        /// <returns>Result of the validation process</returns>
        public abstract RetailTransactionLineProductValidationResponse RetailTransactionLineProductValidation(RetailTransactionLineProductValidationRequest request);

        /// <summary>
        /// Validate a retail transaction line product tax component of a transaction.
        /// </summary>
        /// <param name="request">The request that spawned this response.</param>
        /// <returns>Result of a retail transaction line product validation request.</returns>
        public abstract RetailTransactionLineProductTaxResponse RetailTransactionLineProductTaxValidation(RetailTransactionLineProductTaxRequest request);

        /// <summary>
        /// Validate a retail transaction line product tax override component of a transaction.
        /// </summary>
        /// <param name="request">The request that spawned this response.</param>
        /// <returns>Result of the validation process</returns>
        public abstract RetailTransactionLineProductTaxOverrideResponse RetailTransactionLineProductTaxOverrideValidation(RetailTransactionLineProductTaxOverrideRequest request);

        /// <summary>
        /// Validate a retail transaction line tender component of a transaction.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Result of the validation process</returns>
        public abstract RetailTransactionLineTenderValidationResponse RetailTranactionLineTenderValidation(RetailTransactionLineTenderValidationRequest request);

        /// <summary>
        /// Validate a retail transaction line tender customer validation component of a transaction.
        /// </summary>
        /// <param name="request">The information necessary for validation.</param>
        /// <returns>Result of the validation process</returns>
        public abstract RetailTransactionLineTenderCustomerValidationResponse RetailTransactionLineTenderCustomerValidation(RetailTransactionLineTenderCustomerValidationRequest request);

        /// <summary>
        /// Validate a retail transaction line tender credit card validation component of a transaction.
        /// </summary>
        /// <param name="request">The information necessary for validation.</param>
        /// <returns>Result of the validation process</returns>
        public abstract RetailTransactionLineTenderCreditCardResponse RetailTransactionLineTenderCreditCardValidation(RetailTransactionLineTenderCreditCardRequest request);

        /// <summary>
        /// Validate the attributes of a transaction.
        /// </summary>
        /// <param name="request">Request object with attributes to validate.</param>
        /// <returns>Results of the operation.</returns>
        public abstract TransactionAttributeValidationResponse TransactionAttributeValidation(TransactionAttributeValidationRequest request);

        

        #endregion

        #region UserFunctions

        /// <summary>
        /// This method will delete the password history for a user
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="databaseSchema"></param>
        /// <param name="user"></param>
        public abstract void UserPasswordHistoryDelete(ConnectionInfo connection, string databaseSchema, User user);
        /// <summary>
        /// This method will return the list of users for the given schema
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="databaseSchema"></param>
        /// <returns></returns>
        public abstract List<User> UsersGet(ConnectionInfo connection, string databaseSchema);
        /// <summary>
        /// This method will create a user in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="databaseSchema"></param>
        /// <param name="user"></param>
        public abstract void UserSet(ConnectionInfo connection, string databaseSchema, User user);

        #endregion
        
        #region XmlDocument Functions

        /// <summary>
        /// This method will return the string representation of the xml document stored in the Envision database
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="documentType">The document type to retrieve</param>
        /// <returns></returns>
        public abstract TsXmlDocument XmlDocumentByTypeGet(ConnectionInfo connection, string documentType);


        /// <summary>
        /// This method will return the string representation of the xml document stored in the Envision database
        /// </summary>
        /// <param name="documentType">The document type to retrieve</param>
        /// <returns></returns>
        public abstract TsXmlDocument XmlDocumentGet(string documentType);

        /// <summary>
        /// This method will save an XmlDocument to the Envision database.
        /// </summary>
        /// <param name="document">The document type to save</param>
        /// <returns></returns>
        public abstract TsXmlDocument XmlDocumentSet(TsXmlDocument document);

        /// <summary>
        /// This method will save an XmlDocument to the Envision database.
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="document">The document type to save</param>
        /// <returns></returns>
        public abstract TsXmlDocument XmlDocumentSet(ConnectionInfo connection, TsXmlDocument document);

        /// <summary>
        /// This method will delete an XmlDocument from the Envision database.
        /// </summary>
        /// <param name="documentType">The document type to delete</param>
        /// <returns></returns>
        public abstract void XmlDocumentDelete(string documentType);

        #endregion

        #region IdWorks Functions

        /// <summary>
        /// This method will return a list of IdW_Card objects
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="source"></param>
        /// <returns></returns>
        public abstract IEnumerable<Idw_Card> IdwCardsGet(ConnectionInfo connection, string source);

        /// <summary>
        /// Set the IDWorks CSN Caputure information.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The result of the operation.</returns>
        public abstract IdWorksCsnCaptureResponse IdWorksCsnCaptureSet(IdWorksCsnCaptureRequest request);

        #endregion

        #region IfMan Functions

        /// <summary>
        /// This method will return a list of Clone_Card objects
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="source"></param>
        /// <returns></returns>
        public abstract IEnumerable<Domain.Models.IfMan.Clone_Card> CloneCardsGet(ConnectionInfo connection, string source);
        /// <summary>
        /// This method will return a list of IfAgents objects
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="source"></param>
        /// <returns></returns>
        public abstract IEnumerable<Domain.Models.IfMan.IfmAgents> IfmAgentsGet(ConnectionInfo connection, string source);
        /// <summary>
        /// This method will return a list of IfmAgentsAvailable objects
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="source"></param>
        /// <returns></returns>
        public abstract IEnumerable<Domain.Models.IfMan.IfmAgentsAvailable> IfmAgentsAvailableGet(ConnectionInfo connection, string source);
        /// <summary>
        /// This method will return a list of IfmMasterFieldList objects
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="source"></param>
        /// <returns></returns>
        public abstract IEnumerable<Domain.Models.IfMan.IfmMasterFieldList> IfmMasterFieldListGet(ConnectionInfo connection, string source);
        /// <summary>
        /// This method will return a list of IfmMasterQueue_Change objects
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="source"></param>
        /// <returns></returns>
        public abstract IEnumerable<Domain.Models.IfMan.IfmMasterQueue_Change> IfmMasterQueueChangeGet(ConnectionInfo connection, string source);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="source"></param>
        /// <returns></returns>
        public abstract IEnumerable<Domain.Models.IfMan.IfmMasterQueue_Fields> IfmMasterQueueFieldsGet(ConnectionInfo connection, string source);
        /// <summary>
        /// This method will return a list of IfmMasterQueue_KeyFields objects
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="source"></param>
        /// <returns></returns>
        public abstract IEnumerable<Domain.Models.IfMan.IfmMasterQueue_KeyFields> IfmMasterQueueKeyFieldsGet(ConnectionInfo connection, string source);
        /// <summary>
        /// This method will return a list of IfmMasterQueue_Photo objects
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="source"></param>
        /// <returns></returns>
        public abstract IEnumerable<Domain.Models.IfMan.IfmMasterQueue_Photo> IfmMasterQueuePhotoGet(ConnectionInfo connection, string source);
        /// <summary>
        /// This method will return a list of IfmMasterQueue_SendTo objects
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="source"></param>
        /// <returns></returns>
        public abstract IEnumerable<Domain.Models.IfMan.IfmMasterQueue_SendTo> IfmMasterQueueSendToGet(ConnectionInfo connection, string source);
        /// <summary>
        /// This method will return a list of IfmRecordPool objects
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="source"></param>
        /// <returns></returns>
        public abstract IEnumerable<Domain.Models.IfMan.IfmRecordPool> IfmRecordPoolGet(ConnectionInfo connection, string source);
        /// <summary>
        /// This method will return a list of IfmRecordPool_Fields objects
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="source"></param>
        /// <returns></returns>
        public abstract IEnumerable<Domain.Models.IfMan.IfmRecordPool_Fields> IfmRecordPoolFieldsGet(ConnectionInfo connection, string source);
        /// <summary>
        /// This method will return a list of IfmRecordPool_KeyFields objects
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="source"></param>
        /// <returns></returns>
        public abstract IEnumerable<Domain.Models.IfMan.IfmRecordPool_KeyFields> IfmRecordPoolKeyFieldsGet(ConnectionInfo connection, string source);
        /// <summary>
        /// This method will return a list of IfmUserAgentRights objects
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="source"></param>
        /// <returns></returns>
        public abstract IEnumerable<Domain.Models.IfMan.IfmUserAgentRights> IfmUserAgentRightsGet(ConnectionInfo connection, string source);
        /// <summary>
        /// This method will return a list of IfmUserLog objects
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="source"></param>
        /// <returns></returns>
        public abstract IEnumerable<Domain.Models.IfMan.IfmUserLog> IfmUserLogGet(ConnectionInfo connection, string source);
        /// <summary>
        /// This method will return a list of IfmUserSetup objects
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="source"></param>
        /// <returns></returns>
        public abstract IEnumerable<Domain.Models.IfMan.IfmUserSetup> IfmUserSetupGet(ConnectionInfo connection, string source);

        #endregion

    }
}