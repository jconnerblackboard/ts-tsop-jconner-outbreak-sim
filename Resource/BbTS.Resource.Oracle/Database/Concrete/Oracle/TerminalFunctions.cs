﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using BbTS.Domain.Models.PaymentExpress;
using BbTS.Domain.Models.System;
using BbTS.Domain.Models.Terminal;
using Oracle.ManagedDataAccess.Client;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        /// <inheritdoc />
        public override List<Pos> PosBbPaygateConfiguredGet()
        {
            var terminalList = new List<Pos>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandType = CommandType.Text, CommandText = @"SELECT * FROM Envision.PosCreditCardList WHERE CreditCardProcessingMethodId = 1" })
                {
                    con.Open();
                    using (var r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            terminalList.Add(new Pos
                            {
                                PosId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                Name = !r.IsDBNull(1) ? r.GetString(1) : null,
                                PosType = !r.IsDBNull(2) ? (PosType)r.GetInt32(2) : 0,
                                MacAddress = !r.IsDBNull(3) ? r.GetString(3) : null,
                                ProfitCenterName = !r.IsDBNull(4) ? r.GetString(4) : null,
                                MerchantName = !r.IsDBNull(5) ? r.GetString(5) : null,
                                DeviceGroup = !r.IsDBNull(6) ? r.GetString(6) : null
                            });
                        }
                    }
                }
            }
            return terminalList;
        }

        /// <inheritdoc />
        public override List<PaymentExpressRequestResponseView> EmvReconciliationTransactionGet(string userName, string id)
        {
            var unreconciledList = new List<PaymentExpressRequestResponseView>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandType = CommandType.Text, CommandText = $"SELECT * FROM PaymentExpressReqRespView WHERE PaymentExpress.UserInGroupAccountRoleGroup(pUserName => '{userName}', pPaymentExpressGroupAccountId => PaymentExpressGroupAccountId ) = 1 AND {(string.IsNullOrEmpty(id) ? "ReconciliationStateId = 1 AND InProcess = 0 ORDER BY PaymentExpressReqRespId" : $"PaymentExpressReqRespId = {id}")}" })
                {
                    con.Open();
                    using (var r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            unreconciledList.Add(new PaymentExpressRequestResponseView
                            {
                                PaymentExpressReqRespId = !r.IsDBNull(0) ? r.GetInt64(0) : -1,
                                TransactionReference = !r.IsDBNull(1) ? r.GetString(1) : null,
                                TerminalId = !r.IsDBNull(2) ? r.GetInt32(2) : -1,
                                TerminalName = !r.IsDBNull(3) ? r.GetString(3) : null,
                                MerchantReference = !r.IsDBNull(4) ? r.GetString(4) : null,
                                PaymentExpressGroupAccountId = !r.IsDBNull(5) ? r.GetInt64(5) : -1,
                                PaymentExpressGroupAccountName = !r.IsDBNull(6) ? r.GetString(6) : null,
                                EmvDeviceId = !r.IsDBNull(7) ? r.GetString(7) : null,
                                AmountRequested = !r.IsDBNull(8) ? r.GetDecimal(8) : -1,
                                AmountAuthorized = !r.IsDBNull(9) ? r.GetDecimal(9) : -1,
                                // VoidAmountRequested
                                PurchaseResponseCode = !r.IsDBNull(11) ? r.GetString(11) : null,
                                PurchaseResponseCodeValue = !r.IsDBNull(12) ? r.GetString(12) : null,
                                DpsTransactionReference = !r.IsDBNull(13) ? r.GetString(13) : null,
                                MaskedPan = !r.IsDBNull(14) ? r.GetString(14) : null,
                                CardSuffix = !r.IsDBNull(15) ? r.GetString(15) : null,
                                CardId = !r.IsDBNull(16) ? r.GetInt32(16) : -1,
                                CardName = !r.IsDBNull(17) ? r.GetString(17) : null,
                                CommandSequence = !r.IsDBNull(18) ? r.GetInt32(18) : -1,
                                TransactionStateId = !r.IsDBNull(19) ? r.GetInt32(19) : -1,
                                TransactionStateName = !r.IsDBNull(20) ? r.GetString(20) : null,
                                Stan = !r.IsDBNull(21) ? r.GetInt32(21) : -1,
                                SettlementDate = !r.IsDBNull(22) ? r.GetDateTime(22) : DateTime.MinValue,
                                AuthorizationCode = !r.IsDBNull(23) ? r.GetString(23) : null,
                                RequestDateTime = !r.IsDBNull(24) ? r.GetDateTime(24) : DateTime.MinValue,
                                ResponseDateTime = !r.IsDBNull(25) ? r.GetDateTime(25) : DateTime.MinValue,
                                TransactionResponseCode = !r.IsDBNull(26) ? r.GetString(26) : null,
                                TransactionResponseValue = !r.IsDBNull(27) ? r.GetString(27) : null,
                                TransactionTypeId = !r.IsDBNull(28) ? r.GetInt32(28) : -1,
                                TransactionTypeName = !r.IsDBNull(29) ? r.GetString(29) : null,
                                RespActionResultTokenDomain = !r.IsDBNull(30) ? r.GetInt32(30) : -1,
                                RespActionResultTokenDomainId = !r.IsDBNull(31) ? r.GetInt32(31) : -1,
                                RespActionResultTokenId = !r.IsDBNull(32) ? r.GetString(32) : null,
                                RespActionResultTokenMessage = !r.IsDBNull(33) ? r.GetString(33) : null,
                                Get1DateTime = !r.IsDBNull(34) ? r.GetDateTime(34) : DateTime.MinValue,
                                Get1ActionResultTokenDomain = r.IsDBNull(35) ? r.GetInt32(35) : -1,
                                Get1ActionResultTokenDomainId = r.IsDBNull(36) ? r.GetInt32(36) : -1,
                                Get1ActionResultTokenId = !r.IsDBNull(37) ? r.GetString(37) : null,
                                Get1ActionResultTokenMessage = !r.IsDBNull(38) ? r.GetString(38) : null,
                                VoidResponseCode = !r.IsDBNull(39) ? r.GetString(39) : null,
                                VoidResponseValue = !r.IsDBNull(40) ? r.GetString(40) : null,
                                VoidRequestDateTime = !r.IsDBNull(41) ? r.GetDateTime(41) : DateTime.MinValue,
                                VoidResponseDateTime = !r.IsDBNull(42) ? r.GetDateTime(42) : DateTime.MinValue,
                                VoidActionResultTokenDomain = r.IsDBNull(43) ? r.GetInt32(43) : -1,
                                VoidActionResultTokenDomainId = r.IsDBNull(44) ? r.GetInt32(44) : -1,
                                VoidActionResultTokenId = !r.IsDBNull(45) ? r.GetString(46) : null,
                                VoidActionResultTokenMessage = !r.IsDBNull(46) ? r.GetString(46) : null,
                                TransactionId = r.IsDBNull(47) ? r.GetInt64(47) : -1,
                                InProcess = r.IsDBNull(48) ? r.GetInt32(48) : -1,
                                ReconciliationStateId = r.IsDBNull(49) ? r.GetInt32(49) : -1,
                                ReconciliationStateName = !r.IsDBNull(50) ? r.GetString(50) : null
                            });
                        }
                    }
                }
            }

            return unreconciledList;
        }

        /// <inheritdoc />
        public override void EmvReconciliationTransactionSet(PaymentExpressRequestResponseView value)
        {
            EmvTxnVoidRequestSet(value.TerminalId,
                new BbTxnVoidRequestLog
                {
                    TxnRef = value.TransactionReference,
                    TimeStamp = value.VoidRequestDateTime
                }
            );

            EmvTxnVoidResponseSet(value.TerminalId,
                new BbTxnVoidResponseLog
                {
                    TxnRef = value.TransactionReference,
                    ReCo = value.VoidResponseCode,
                    TimeStamp = value.VoidResponseDateTime,
                    Result = new ActionResultToken
                    {
                        ResultDomain = value.VoidActionResultTokenDomain,
                        ResultDomainId = value.VoidActionResultTokenDomainId,
                        Id = value.VoidActionResultTokenId,
                        Message = value.VoidActionResultTokenMessage
                    }
                }
            );
        }

        /// <inheritdoc />
        public override EmvSettings EmvSettingsGet(int terminalId)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "SystemFunctions.EmvGatewaySettingsGet", CommandType = CommandType.StoredProcedure })
                {
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pReturnAll", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = 0 });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pTerminalId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = terminalId });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });

                    con.Open();
                    using (var r = cmd.ExecuteReader())
                    {
                        if (r.Read())
                        {
                            return new EmvSettings()
                            {
                                Identity = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                Name = !r.IsDBNull(1) ? r.GetString(1) : null,
                                Address = !r.IsDBNull(2) ? r.GetString(2) : null,
                                Type = !r.IsDBNull(3) ? r.GetString(3) : null,
                                Port = !r.IsDBNull(4) ? r.GetString(4) : null,
                                Priority = !r.IsDBNull(5) ? r.GetString(5) : null,
                                Enabled = !r.IsDBNull(6) ? r.GetInt32(6) : -1,
                                CurrencyCode = !r.IsDBNull(7) ? r.GetString(7) : null,
                                DeviceId = !r.IsDBNull(8) ? r.GetString(8) : null,
                                VendorId = !r.IsDBNull(9) ? r.GetString(9) : null,
                                IdleDisconnectTimeout = !r.IsDBNull(10) ? int.Parse(r.GetString(10)) : -1
                            };
                        }
                    }
                }
            }
            return new EmvSettings();
        }

        /// <inheritdoc />
        public override void EmvTxnPurRequestSet(int terminalId, BbTxnPurRequestLog request)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "PaymentExpress.PurchaseRequest", CommandType = CommandType.StoredProcedure })
                {
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionReference", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = request.TxnRef });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pAmountRequested", OracleDbType = OracleDbType.Int32, Size = 7, Direction = ParameterDirection.Input, Value = request.Amount });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pMerchantReference", OracleDbType = OracleDbType.Varchar2, Size = 64, Direction = ParameterDirection.Input, Value = request.MerchantReference });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pTerminalId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = terminalId });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pRequestDateTime", OracleDbType = OracleDbType.TimeStamp, Direction = ParameterDirection.Input, Value = request.TimeStamp.ToLocalTime() });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDeviceId", OracleDbType = OracleDbType.Varchar2, Size = 16, Direction = ParameterDirection.Input, Value = request.DeviceId });

                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <inheritdoc />
        public override void EmvTxnPurResponseSet(int terminalId, BbTxnPurResponseLog response)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "PaymentExpress.PurchaseResponse", CommandType = CommandType.StoredProcedure })
                {
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionReference", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = response.TxnRef });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pPurchaseResponseCode", OracleDbType = OracleDbType.Varchar2, Size = 2, Direction = ParameterDirection.Input, Value = response.ReCo });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pAmountAuthorized", OracleDbType = OracleDbType.Int32, Size = 7, Direction = ParameterDirection.Input, Value = response.Amount });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDpsTransactionReference", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = response.DpsTxnRef });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pTerminalId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = terminalId });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pRequestDateTime", OracleDbType = OracleDbType.TimeStamp, Direction = ParameterDirection.Input, Value = response.TimeStamp.ToLocalTime() });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pRespActionResultTokenDomain", OracleDbType = OracleDbType.Int32, Size = 38, Direction = ParameterDirection.Input, Value = response.Result.ResultDomain });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pRespActionResultTokenDomainId", OracleDbType = OracleDbType.Int32, Size = 38, Direction = ParameterDirection.Input, Value = response.Result.ResultDomainId });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pRespActionResultTokenId", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = response.Result.Id });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pRespActionResultTokenMessage", OracleDbType = OracleDbType.Varchar2, Size = 500, Direction = ParameterDirection.Input, Value = response.Result.Message });

                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <inheritdoc />
        public override void EmvTxnGet1ResponseSet(int terminalId, BbTxnGet1ResponseLog response)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "PaymentExpress.PurchaseComplete", CommandType = CommandType.StoredProcedure })
                {
                    DateTime settlementDateTime;
                    DateTime.TryParseExact(response.SettlementDate, "yyyyMMdd", new CultureInfo("en-US"), DateTimeStyles.AssumeUniversal, out settlementDateTime);

                    DateTime transactionDateTime;
                    DateTime.TryParseExact(response.TxnTime, "yyyyMMddHHmmss", new CultureInfo("en-US"), DateTimeStyles.AssumeUniversal, out transactionDateTime);

                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionReference", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = response.TxnRef });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pCommandSequence", OracleDbType = OracleDbType.Int32, Size = 6, Direction = ParameterDirection.Input, Value = string.IsNullOrEmpty(response.CmdSeq) ? null : (int?)int.Parse(response.CmdSeq) });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pPurchaseResponseCode", OracleDbType = OracleDbType.Varchar2, Size = 2, Direction = ParameterDirection.Input, Value = response.ReCo });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pCardSuffix", OracleDbType = OracleDbType.Varchar2, Size = 4, Direction = ParameterDirection.Input, Value = response.CardSuffix });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pCardId", OracleDbType = OracleDbType.Varchar2, Size = 10, Direction = ParameterDirection.Input, Value = response.CardId });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pAmountRequested", OracleDbType = OracleDbType.Int32, Size = 7, Direction = ParameterDirection.Input, Value = response.AmountRequested });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pAmountAuthorized", OracleDbType = OracleDbType.Int32, Size = 7, Direction = ParameterDirection.Input, Value = response.AmountAuthorized });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionStateId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = string.IsNullOrEmpty(response.TxnState) ? null : (int?)int.Parse(response.TxnState) });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pStan", OracleDbType = OracleDbType.Int32, Size = 6, Direction = ParameterDirection.Input, Value = string.IsNullOrEmpty(response.Stan) ? null : (int?)int.Parse(response.Stan) });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pSettlementDate", OracleDbType = OracleDbType.TimeStamp, Direction = ParameterDirection.Input, Value = settlementDateTime.ToLocalTime() });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pAuthorizationCode", OracleDbType = OracleDbType.Varchar2, Size = 6, Direction = ParameterDirection.Input, Value = response.AuthCode });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDpsTransactionReference", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = response.DpsTxnRef });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionResponseCode", OracleDbType = OracleDbType.Varchar2, Size = 2, Direction = ParameterDirection.Input, Value = response.TxnReCo });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pMerchantReference", OracleDbType = OracleDbType.Varchar2, Size = 64, Direction = ParameterDirection.Input, Value = response.MerchantReference });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionType", OracleDbType = OracleDbType.Varchar2, Size = 255, Direction = ParameterDirection.Input, Value = response.TxnType });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionTime", OracleDbType = OracleDbType.TimeStamp, Direction = ParameterDirection.Input, Value = transactionDateTime.ToLocalTime() });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pMaskedPan", OracleDbType = OracleDbType.Varchar2, Size = 16, Direction = ParameterDirection.Input, Value = response.MaskedPan });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pTerminalId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = terminalId });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pGet1DateTime", OracleDbType = OracleDbType.TimeStamp, Direction = ParameterDirection.Input, Value = response.TimeStamp.ToLocalTime() });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pGet1ActionResultTokenDomain", OracleDbType = OracleDbType.Int32, Size = 38, Direction = ParameterDirection.Input, Value = response.Result.ResultDomain });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pGet1ActionResultTokenDomainId", OracleDbType = OracleDbType.Int32, Size = 38, Direction = ParameterDirection.Input, Value = response.Result.ResultDomainId });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pGet1ActionResultTokenId", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = response.Result.Id });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pGet1ActionResultTokenMessage", OracleDbType = OracleDbType.Varchar2, Size = 500, Direction = ParameterDirection.Input, Value = response.Result.Message });

                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <inheritdoc />
        public override void EmvTxnVoidRequestSet(int terminalId, BbTxnVoidRequestLog request)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "PaymentExpress.VoidRequest", CommandType = CommandType.StoredProcedure })
                {
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionReference", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = request.TxnRef });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pTerminalId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = terminalId });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pVoidRequestDateTime", OracleDbType = OracleDbType.TimeStamp, Direction = ParameterDirection.Input, Value = request.TimeStamp.ToLocalTime() });

                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <inheritdoc />
        public override void EmvTxnVoidResponseSet(int terminalId, BbTxnVoidResponseLog response)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "PaymentExpress.VoidResponse", CommandType = CommandType.StoredProcedure })
                {
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionReference", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = response.TxnRef });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pVoidResponseCode", OracleDbType = OracleDbType.Varchar2, Size = 2, Direction = ParameterDirection.Input, Value = response.ReCo });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pTerminalId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = terminalId });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pVoidResponseDateTime", OracleDbType = OracleDbType.TimeStamp, Direction = ParameterDirection.Input, Value = response.TimeStamp.ToLocalTime() });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pVoidActionResultTokenDomain", OracleDbType = OracleDbType.Int32, Size = 38, Direction = ParameterDirection.Input, Value = response.Result.ResultDomain });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pVoidActionResultTokenDomainId", OracleDbType = OracleDbType.Int32, Size = 38, Direction = ParameterDirection.Input, Value = response.Result.ResultDomainId });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pVoidActionResultTokenId", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = response.Result.Id });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pVoidActionResultTokenMessage", OracleDbType = OracleDbType.Varchar2, Size = 500, Direction = ParameterDirection.Input, Value = response.Result.Message });

                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <inheritdoc />
        public override void EmvTxnRefRequestSet(int terminalId, BbTxnRefRequestLog request)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "PaymentExpress.RefundRequest", CommandType = CommandType.StoredProcedure })
                {
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionReference", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = request.TxnRef });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pAmountRequested", OracleDbType = OracleDbType.Int32, Size = 7, Direction = ParameterDirection.Input, Value = request.Amount });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pMerchantReference", OracleDbType = OracleDbType.Varchar2, Size = 64, Direction = ParameterDirection.Input, Value = request.MerchantReference });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pTerminalId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = terminalId });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pRequestDateTime", OracleDbType = OracleDbType.TimeStamp, Direction = ParameterDirection.Input, Value = request.TimeStamp.ToLocalTime() });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDeviceId", OracleDbType = OracleDbType.Varchar2, Size = 16, Direction = ParameterDirection.Input, Value = request.DeviceId });

                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <inheritdoc />
        public override void EmvTxnRefResponseSet(int terminalId, BbTxnRefResponseLog response)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "PaymentExpress.RefundResponse", CommandType = CommandType.StoredProcedure })
                {
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionReference", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = response.TxnRef });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pRefundResponseCode", OracleDbType = OracleDbType.Varchar2, Size = 2, Direction = ParameterDirection.Input, Value = response.ReCo });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pAmountAuthorized", OracleDbType = OracleDbType.Int32, Size = 7, Direction = ParameterDirection.Input, Value = response.Amount });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDpsTransactionReference", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = response.DpsTxnRef });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pTerminalId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = terminalId });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pRequestDateTime", OracleDbType = OracleDbType.TimeStamp, Direction = ParameterDirection.Input, Value = response.TimeStamp.ToLocalTime() });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pRespActionResultTokenDomain", OracleDbType = OracleDbType.Int32, Size = 38, Direction = ParameterDirection.Input, Value = response.Result.ResultDomain });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pRespActionResultTokenDomainId", OracleDbType = OracleDbType.Int32, Size = 38, Direction = ParameterDirection.Input, Value = response.Result.ResultDomainId });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pRespActionResultTokenId", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = response.Result.Id });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pRespActionResultTokenMessage", OracleDbType = OracleDbType.Varchar2, Size = 500, Direction = ParameterDirection.Input, Value = response.Result.Message });

                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <inheritdoc />
        public override void EmvTxnSigRequestSet(int terminalId, BbTxnSigRequestLog request)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "PaymentExpress.SignatureRequest", CommandType = CommandType.StoredProcedure })
                {
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionReference", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = request.TxnRef });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pSignatureResponseCode", OracleDbType = OracleDbType.Varchar2, Size = 2, Direction = ParameterDirection.Input, Value = request.SignatureResult });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pTerminalId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = terminalId });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pSigResponseDateTime", OracleDbType = OracleDbType.TimeStamp, Direction = ParameterDirection.Input, Value = request.TimeStamp.ToLocalTime() });

                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <inheritdoc />
        public override void EmvTxnSigResponseSet(int terminalId, BbTxnSigResponseLog response)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "PaymentExpress.SignatureResponse", CommandType = CommandType.StoredProcedure })
                {
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionReference", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = response.TxnRef });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pSignatureResponseCode", OracleDbType = OracleDbType.Varchar2, Size = 2, Direction = ParameterDirection.Input, Value = response.ReCo });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pTerminalId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = terminalId });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pSigResponseDateTime", OracleDbType = OracleDbType.TimeStamp, Direction = ParameterDirection.Input, Value = response.TimeStamp.ToLocalTime() });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pSigActionResultTokenDomain", OracleDbType = OracleDbType.Int32, Size = 38, Direction = ParameterDirection.Input, Value = response.Result.ResultDomain });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pSigActionResultTokenDomainId", OracleDbType = OracleDbType.Int32, Size = 38, Direction = ParameterDirection.Input, Value = response.Result.ResultDomainId });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pSigActionResultTokenId", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = response.Result.Id });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pSigActionResultTokenMessage", OracleDbType = OracleDbType.Varchar2, Size = 500, Direction = ParameterDirection.Input, Value = response.Result.Message });

                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}