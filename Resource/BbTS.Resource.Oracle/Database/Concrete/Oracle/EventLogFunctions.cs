﻿using BbTS.Core;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Monitoring.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using BbTS.Domain.Models.System.Database;
using BbTS.Domain.Models.EventLog;
using BbTS.Domain.Models.Transaction;
using EventLog = BbTS.Domain.Models.EventLog.EventLog;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        /// <summary>
        /// Create event log
        /// </summary>
        /// <returns>Response to the request</returns>
        public override void EventLogSet(EventLogPostRequest request)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "EventLogFunctions.EventLogSet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pProviderName", OracleDbType = OracleDbType.Varchar2, Size = 50, Direction = ParameterDirection.Input, Value = request.ProviderName.Length > 50 ? request.ProviderName.Substring(0, 50) : request.ProviderName });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pComputerName", OracleDbType = OracleDbType.Varchar2, Size = 255, Direction = ParameterDirection.Input, Value = request.ComputerName.Length > 255 ? request.ComputerName.Substring(0, 255) : request.ComputerName });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pEventId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = request.EventId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pSeverity", OracleDbType = OracleDbType.Int32, Size = 2, Direction = ParameterDirection.Input, Value = request.Severity });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pTaskCategory", OracleDbType = OracleDbType.Int32, Size = 6, Direction = ParameterDirection.Input, Value = request.TaskCategory });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pMessage", OracleDbType = OracleDbType.Clob, Size = 0, Direction = ParameterDirection.Input, Value = request.Message });
                        con.Open();

                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Gets an event log
        /// </summary>
        /// <param name="eventLogId"></param>
        /// <returns>Event log</returns>
        public override TsEventLog EventLogGet(int eventLogId)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "EventLogFunctions.EventLogGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pEventLogId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = eventLogId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            if (r.Read())
                            {
                                return new TsEventLog()
                                {
                                    EventLogId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    ProviderName = !r.IsDBNull(1) ? r.GetString(1) : string.Empty,
                                    ComputerName = !r.IsDBNull(2) ? r.GetString(2) : string.Empty,
                                    EventId = !r.IsDBNull(3) ? r.GetInt32(3) : -1,
                                    Severity = !r.IsDBNull(4) ? r.GetInt32(4) : -1,
                                    TaskCategory = !r.IsDBNull(5) ? r.GetInt32(5) : -1,
                                    CreatedDateTime = !r.IsDBNull(6) ? r.GetDateTime(6) : DateTime.MinValue,
                                    Message = !r.IsDBNull(7) ? r.GetString(7) : string.Empty
                                };
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return null;
        }

        /// <inheritdoc />
        public override IEnumerable<EventLog> EventLogGet(ConnectionInfo connectionInfo, string source)
        {
            var list = new List<EventLog>();

            using (OracleConnection con = OracleConnectionGet(connectionInfo))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = source, CommandType = CommandType.TableDirect })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new EventLog();

                                if (!r.IsDBNull(0)) item.EventLogId = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.ProviderName = r.GetString(1);
                                if (!r.IsDBNull(2)) item.ComputerName = r.GetString(2);
                                if (!r.IsDBNull(3)) item.EventId = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.Severity = r.GetInt32(4);
                                if (!r.IsDBNull(5)) item.TaskCategory = r.GetInt32(5);
                                if (!r.IsDBNull(6)) item.CreatedDateTime = r.GetDateTime(6);
                                if (!r.IsDBNull(7)) item.Message = r.GetString(7);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            return list;
        }

        /// <summary>
        /// Gets summary of event logs
        /// </summary>
        /// <param name="request">Filter</param>
        /// <returns>Filtered event logs</returns>
        public override List<TsEventLogSummary> EventLogGetSummary(EventLogGetSummaryRequest request)
        {
            var list = new List<TsEventLogSummary>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "EventLogFunctions.EventLogGetSummary", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pProviderName", OracleDbType = OracleDbType.Varchar2, Size = 50, Direction = ParameterDirection.Input, Value = request?.ProviderName });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pComputerName", OracleDbType = OracleDbType.Varchar2, Size = 255, Direction = ParameterDirection.Input, Value = request?.ComputerName });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pSeverity", OracleDbType = OracleDbType.Int32, Size = 2, Direction = ParameterDirection.Input, Value = request?.Severity });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCreatedFromDate", OracleDbType = OracleDbType.TimeStampTZ, Size = 2, Direction = ParameterDirection.Input, Value = request?.CreatedFromDate });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCreatedUntilDate", OracleDbType = OracleDbType.TimeStampTZ, Size = 2, Direction = ParameterDirection.Input, Value = request?.CreatedUntilDate });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsEventLogSummary()
                                {
                                    EventLogId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    ProviderName = !r.IsDBNull(1) ? r.GetString(1) : string.Empty,
                                    ComputerName = !r.IsDBNull(2) ? r.GetString(2) : string.Empty,
                                    EventId = !r.IsDBNull(3) ? r.GetInt32(3) : -1,
                                    Severity = !r.IsDBNull(4) ? r.GetInt32(4) : -1,
                                    CreatedDateTime = !r.IsDBNull(5) ? r.GetDateTime(5) : DateTime.MinValue
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <summary>
        /// Gets lastest log by event task category
        /// </summary>
        /// <param name="eventId">Event id</param>
        /// <param name="taskCategory">Task category</param>
        /// <param name="providerName">Provider name</param>
        /// <param name="computerName">Computer name</param>
        /// <returns>Event log</returns>
        public override TsEventLog EventLogLatestByTaskGet(int eventId, short taskCategory, string providerName = null, string computerName = null)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "EventLogFunctions.EventLogLatestByTaskGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pEventId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = eventId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pTaskCategory", OracleDbType = OracleDbType.Int32, Size = 6, Direction = ParameterDirection.Input, Value = taskCategory });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pProviderName", OracleDbType = OracleDbType.Varchar2, Size = 50, Direction = ParameterDirection.Input, Value = providerName });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pComputerName", OracleDbType = OracleDbType.Varchar2, Size = 255, Direction = ParameterDirection.Input, Value = computerName });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            if (r.Read())
                            {
                                return new TsEventLog()
                                {
                                    EventLogId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    ProviderName = !r.IsDBNull(1) ? r.GetString(1) : string.Empty,
                                    ComputerName = !r.IsDBNull(2) ? r.GetString(2) : string.Empty,
                                    EventId = !r.IsDBNull(3) ? r.GetInt32(3) : -1,
                                    Severity = !r.IsDBNull(4) ? r.GetInt32(4) : -1,
                                    TaskCategory = !r.IsDBNull(5) ? r.GetInt32(5) : -1,
                                    CreatedDateTime = !r.IsDBNull(6) ? r.GetDateTime(6) : DateTime.MinValue,
                                    Message = !r.IsDBNull(7) ? r.GetString(7) : string.Empty
                                };
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return null;
        }

        ///<inheritdoc />
        public override List<CustomerTransactionLocationLog> CustomerEventLocationLogsGet(DateTime startDate, DateTime endDate)
        {
            var list = new List<CustomerTransactionLocationLog>();

            using (var con = OracleConnectionGet(ConnectionString))
            {
                var cmdText = $@"
                    SELECT         DISTINCT
                                   BTS.InstitutionId,
                                   CUS.CustomerGuid,
                                   CUS.Custnum,
                                   MER.Name As MerchantName,
                                   PFC.Name As ProfitCenterName,
                                   POS.Name As PosName,
                                   POS.PosGuid as PosId,
                                   ETX.ActualDateTime AS DateTime
                    FROM           EventTransactions ETX
                    INNER JOIN     Customer CUS ON CUS.Cust_Id = ETX.Cust_Id
                    INNER JOIN     POS ON POS.Pos_Id = ETX.Pos_Id
                    INNER JOIN     ProfitCenter PFC ON PFC.ProfitCenter_Id = ETX.ProfitCenter_Id
                    INNER JOIN     Merchant MER ON MER.Merchant_Id = PFC.Merchant_Id
                    INNER JOIN     BbSPTransactionSystem BTS ON BTS.IsValid = 'T'
                    WHERE          ETX.ActualDateTime > {startDate.ToOADate()}
                    AND            ETX.ActualDateTime < {endDate.ToOADate()}
                    ORDER BY       DateTime";
                using (var cmd = new OracleCommand { Connection = con, CommandText = cmdText, CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new CustomerTransactionLocationLog();
                                if (!r.IsDBNull(0)) item.InstitutionId = r.GetString(0);
                                if (!r.IsDBNull(1)) item.CustomerGuid = r.GetString(1);
                                if (!r.IsDBNull(2)) item.CustNum = r.GetString(2);
                                if (!r.IsDBNull(3)) item.MerchantName = r.GetString(3);
                                if (!r.IsDBNull(4)) item.ProfitCenterName = r.GetString(4);
                                if (!r.IsDBNull(5)) item.PosName = r.GetString(5);
                                if (!r.IsDBNull(6)) item.PosId = r.GetString(6);
                                if (!r.IsDBNull(7)) item.EntryDateTime = new DateTimeOffset(DateTime.FromOADate(r.GetDouble(7)), TimeZoneInfo.Local.BaseUtcOffset);
                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }
    }
}
 