﻿using System;
using System.Data;
using System.Diagnostics;
using BbTS.Core;
using BbTS.Core.Conversion;
using BbTS.Core.Logging.Tracing;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Definitions.Notification;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.Notification;
using BbTS.Domain.Models.Transaction;
using BbTS.Monitoring.Logging;
using Oracle.ManagedDataAccess.Client;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        /// <summary>
        /// Create a notification.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public override NotificationCreateResponse NotificationCreate(NotificationCreateRequest request)
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");

            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "NotificationFunctions.NotificationCreate", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pObjectId", OracleDbType.Int32, 10, request.ObjectId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pEventType", OracleDbType.Int32, 1, (int)request.EventType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pObjectType", OracleDbType.Int32, 1, (int)request.ObjectType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pObjectGuid", OracleDbType.Varchar2, 36, request.ObjectGuid.ToString("D"), ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();

                        return new NotificationCreateResponse { RequestId = request.RequestId };
                    }
                    catch (Exception ex)
                    {
                        var rex = new ResourceLayerException(request.RequestId.ToString("D"), Formatting.FormatException(ex));
                        LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                        throw rex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Get all notificaitons stored in the database layer.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public override NotificationGetResponse NotificationGet(NotificationGetRequest request)
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");

            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "NotificationFunctions.NotificationGet", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        //cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Size = 0, Direction = ParameterDirection.Output, Value = string.Empty });
                        con.Open();

                        var response = new NotificationGetResponse { RequestId = request.RequestId };
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var notificationObject = new NotificationObject();
                                if (!r.IsDBNull(0)) notificationObject.NotificationId = r.GetInt32(0);
                                if (!r.IsDBNull(1)) notificationObject.InstitutionId = new Guid(r.GetString(1));
                                if (!r.IsDBNull(2)) notificationObject.ObjectGuid = new Guid(r.GetString(2));
                                if (!r.IsDBNull(3)) notificationObject.CustomerNumber = r.GetString(3);
                                if (!r.IsDBNull(4)) notificationObject.EventType = (NotificationEventType)r.GetInt32(4);
                                if (!r.IsDBNull(5)) notificationObject.ObjectType = (NotificationObjectType)r.GetInt32(5);
                                if (!r.IsDBNull(6)) notificationObject.EventDateTime = r.GetDateTime(6).ToLocalTime();
                                if (!r.IsDBNull(7)) notificationObject.Data = r.GetOracleClob(7).Value;
                                if (!r.IsDBNull(8)) notificationObject.ExternalClientIds = r.GetString(8);
                                if (!r.IsDBNull(9)) notificationObject.Status = (NotificationStatusType)r.GetInt32(9);
                                if (!r.IsDBNull(10)) notificationObject.StatusDateTime = r.GetDateTime(10).ToLocalTime();
                                response.Notifications.Add(notificationObject);
                            }
                        }
                        return response;
                    }
                    catch (Exception ex)
                    {
                        var rex = new ResourceLayerException(request.RequestId.ToString("D"), Formatting.FormatException(ex));
                        LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                        throw rex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Set the status for processing of a notification event.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public override NotificationStatusSetResponse NotificationStatusSet(NotificationStatusSetRequest request)
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");

            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "NotificationFunctions.NotificationStatusSet", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pNotificationId", OracleDbType.Int32, 10, request.NotificationId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pStatus", OracleDbType.Int32, 1, (int)request.StatusType, ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();

                        return new NotificationStatusSetResponse { RequestId = request.RequestId, Result = NotificationStatusSetResult.Success, Message = "Success."};
                    }
                    catch (Exception ex)
                    {
                        LoggingManager.Instance.LogException(ex, EventLogEntryType.Error, (short)LoggingDefinitions.Category.Resource);
                        var rex = new ResourceLayerException(request.RequestId.ToString("D"), Formatting.FormatException(ex));
                        throw rex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Register to receive notification events.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public override NotificationEventRegisterResponse NotificationEventRegister(NotificationEventRegisterRequest request, OracleConnection connection)
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");

            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "NotificationFunctions.RegisterForNotifications", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        var rex = new ResourceLayerException(request.RequestId.ToString("D"), Formatting.FormatException(ex));
                        LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                        throw rex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return new NotificationEventRegisterResponse
            {
                RequestId = request.RequestId
            };
        }

        /// <summary>
        /// Wait for a notification event callback.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="connection"></param>
        /// <returns></returns>
        [Trace(AttributeExclude = true, AttributePriority = 2)]
        public override NotificationEventCallbackResponse NotificationEventCallbackWait(NotificationEventCallbackRequest request, OracleConnection connection)
        {
            if (connection.State != ConnectionState.Open)
            {
                connection.Open();
            }

            using (OracleCommand cmd = new OracleCommand { Connection = connection, CommandText = "NotificationFunctions.WaitOneNotificationEvent", CommandType = CommandType.StoredProcedure })
            {
                try
                {
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pWaitTime", OracleDbType = OracleDbType.Int32, Size = 10, Value = request.WaitTime, Direction = ParameterDirection.Input });
                    cmd.Parameters.Add(new OracleParameter("pMessage", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 1800 });
                    cmd.Parameters.Add(new OracleParameter("pStatus", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });

                    cmd.ExecuteNonQuery();

                    var message = cmd.Parameters["pMessage"].Value.ToString();
                    var status = cmd.Parameters["pStatus"].Value.ToString();

                    return new NotificationEventCallbackResponse
                    {
                        Status = string.IsNullOrEmpty(status) || status == "null" ? 0 : Convert.ToInt32(status),
                        Message = !string.IsNullOrWhiteSpace(message) && message != "null" ? message : null
                    };
                }
                catch (OracleException oex)
                {
                    throw new ResourceLayerOracleException(Formatting.FormatException(oex), oex.ErrorCode);
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException(request.RequestId.ToString("D"), Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
            }
        }

        /// <summary>
        /// Kick off a nightly reporting metrics gather and process request in the data layer.
        /// </summary>
        /// <param name="requestId">The unique identifier for the request.</param>
        /// <returns></returns>
        public override ProcessingResult ReportingMetricsDailyProcessRun(string requestId)
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");

            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "NotificationFunctions.ReportingMetricsSet", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        var rex = new ResourceLayerException(requestId, Formatting.FormatException(ex));
                        LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                        throw rex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return new ProcessingResult
            {
                RequestId = requestId,
                ErrorCode = (int)LoggingDefinitions.EventId.Success,
                DeniedText = "Success."
            };
        }
    }
}
