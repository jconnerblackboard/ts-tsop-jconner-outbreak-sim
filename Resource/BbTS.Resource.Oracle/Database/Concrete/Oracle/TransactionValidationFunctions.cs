﻿using System;
using System.Data;
using System.Diagnostics;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.Transaction.Processing;
using BbTS.Domain.Models.Transaction.Validation;
using BbTS.Monitoring.Logging;
using BbTS.Resource.OracleManagedDataAccessExtensions;
using Oracle.ManagedDataAccess.Client;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        /// <summary>
        /// Validate the board meal type id.
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Result of the validation operation.</returns>
        public override BoardMealTypeValidationResponse BoardMealTypeValidate(BoardMealTypeValidationRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "Validation.BoardMealType", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pBoardmtId", OracleDbType.Int32, 6, request.BoardMealTypeId, ParameterDirection.Input));
                        
                        cmd.Parameters.Add(new OracleParameter("pErrorCode", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pDeniedText", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 75 });

                        con.Open();
                        cmd.ExecuteNonQuery();

                        var deniedText = cmd.Parameters["pDeniedText"].Value.ToString();
                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);
                        return new BoardMealTypeValidationResponse
                        {
                            Request = request,
                            DeniedText = deniedText,
                            ErrorCode = errorCode
                        };
                    }
                    catch (Exception ex)
                    {
                        var rex = new ResourceLayerException(request.RequestId, Formatting.FormatException(ex));
                        throw rex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>cmd.Parameters["pDeniedText"].Value.ToString()
        /// Validate the customer transaction info portion of a transaction.
        /// </summary>
        /// <param name="request">The customer transaction info.</param>
        /// <returns>The result of the operation.</returns>
        public override CustomerTransactionInfoValidationResponse CustomerTransactionInfoValidation(CustomerTransactionInfoValidationRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "Validation.CustTransactionInfo", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        string paddedCardNumber = Formatting.PadCardNumber(request.CardNumber, 22, '0');
                        cmd.Parameters.Add(new OracleParameter("pCardnum", OracleDbType.Varchar2, 22, paddedCardNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIssueNumber", OracleDbType.Varchar2, 6, request.IssueNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIssueNumberCaptured", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.IssueNumberCaptured), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pPinNumber", OracleDbType.Int32, 10, request.Pin, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTransactionDateTime", OracleDbType.Double, 14, request.TransactionDateTime.ToOADate(), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pCustomerGuid", OracleDbType.Varchar2, 36, request.CustomerGuid, ParameterDirection.Input));

                        cmd.Parameters.Add(new OracleParameter("pErrorCode", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pDeniedText", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 75 });

                        con.Open();
                        cmd.ExecuteNonQuery();

                        var deniedText = cmd.Parameters["pDeniedText"].Value.ToString();
                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);
                        return new CustomerTransactionInfoValidationResponse(request)
                        {
                            DeniedText = deniedText,
                            ErrorCode = errorCode
                        };
                    }
                    catch (Exception ex)
                    {
                        var rex = new ResourceLayerException(request.RequestId, Formatting.FormatException(ex));
                        throw rex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Validate a cashier (operator) component of a transaction.
        /// </summary>
        /// <param name="cashierGuid">The unique (Guid) identification value assigned to the cashier (operator).</param>
        /// <param name="deviceGuid">The unique (Guid) identifier assigned to the device.</param>
        /// <param name="forcePost">Force post flag, defaults to false</param>
        /// <returns>The results of the operation.</returns>
        public override OperatorByOperatorGuidValidationResponse OperatorByOperatorGuidValidation(string cashierGuid, string deviceGuid, bool forcePost)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "Validation.OperatorByOperatorGuid", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.AddInParam("pOperatorGuid", cashierGuid);
                        cmd.AddInParam("pDeviceGuid", deviceGuid);
                        cmd.AddInParam("pForcepost", forcePost ? "T" : "F");
                        
                        cmd.AddOutParam("pErrorCode", OracleDbType.Int32, 10);
                        cmd.AddOutParam("pDeniedText", OracleDbType.Varchar2, 75);

                        con.Open();
                        cmd.ExecuteNonQuery();
                        
                        var deniedText = cmd.Parameters["pDeniedText"].Value.ToString();
                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);
                        return new OperatorByOperatorGuidValidationResponse
                        {
                            OperatorGuid = cashierGuid,
                            DeniedText = deniedText,
                            DeviceGuid = deviceGuid,
                            ErrorCode = errorCode
                        };
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Validate a product promo component of a transaction.
        /// </summary>
        /// <param name="productPromoId"></param>
        /// <returns>Result of a product promo validation request.</returns>
        public override ProductPromoValidationResponse ProductPromoValidation(int productPromoId)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "Validation.ProductPromo", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pProductPromoId", OracleDbType.Int32, 10, productPromoId, ParameterDirection.Input));

                        cmd.Parameters.Add(new OracleParameter("pErrorCode", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pDeniedText", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 75 });

                        con.Open();
                        cmd.ExecuteNonQuery();

                        var deniedText = cmd.Parameters["pDeniedText"].Value.ToString();
                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);
                        return new ProductPromoValidationResponse(productPromoId)
                        {
                            DeniedText = deniedText,
                            ErrorCode = errorCode
                        };
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Validate a retail transaction line product price modify component of a transaction.
        /// </summary>
        /// <param name="request">The request that spawned this response.</param>
        /// <returns>Result of the validation process</returns>
        public override RetailTransactionLineProductPriceModifierResponse RetailTransactionLineProductPriceModifyValidation(RetailTransactionLineProductPriceModifierRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "Validation.RtlTranLineProdPrcMod", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pRetailPriceModifierType", OracleDbType.Int32, 4, request.RetailPriceModifierType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pProdPromoFlag", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.ProductPromoFlag), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pProdPromoId", OracleDbType.Int32, 6, request.ProductPromoId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pCalculationMethodType", OracleDbType.Int32, 4, request.CalculationMethodType, ParameterDirection.Input));
                        
                        cmd.Parameters.Add(new OracleParameter("pErrorCode", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pDeniedText", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 75 });

                        con.Open();
                        cmd.ExecuteNonQuery();

                        var deniedText = cmd.Parameters["pDeniedText"].Value.ToString();
                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);

                        return new RetailTransactionLineProductPriceModifierResponse
                        {
                            RequestId = request.RequestId,
                            DeniedText = deniedText,
                            ErrorCode = errorCode
                        };
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Validate a retail transaction line product component of a transaction.
        /// </summary>
        /// <param name="request">The request that spawned this response.</param>
        /// <returns>Result of a retail transaction line product validation request.</returns>
        public override RetailTransactionLineProductValidationResponse RetailTransactionLineProductValidation(RetailTransactionLineProductValidationRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "Validation.RtlTranLineProduct", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        
                        cmd.Parameters.Add(new OracleParameter("pProdDetailId", OracleDbType.Int32, 10, request.ProductDetailId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTaxScheduleId", OracleDbType.Int32, 10, request.TaxScheduleId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTaxGroupId", OracleDbType.Int32, 10, request.TaxGroupId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pUnitMeasureType", OracleDbType.Int32, 10, request.UnitMeasureType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pProductEntryMethodType", OracleDbType.Int32, 10, request.ProductEntryMethodType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pRetailPriceEntryMethodType", OracleDbType.Int32, 10, request.RetailPriceEntryMethodType, ParameterDirection.Input));
                        
                        cmd.Parameters.Add(new OracleParameter("pErrorCode", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pDeniedText", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 75 });

                        con.Open();
                        cmd.ExecuteNonQuery();

                        var deniedText = cmd.Parameters["pDeniedText"].Value.ToString();
                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);
                        return new RetailTransactionLineProductValidationResponse(request)
                        {
                            DeniedText = deniedText,
                            ErrorCode = errorCode
                        };
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Validate a retail transaction line product tax component of a transaction.
        /// </summary>
        /// <param name="request">The request that spawned this response.</param>
        /// <returns>Result of a retail transaction line product validation request.</returns>
        public override RetailTransactionLineProductTaxResponse RetailTransactionLineProductTaxValidation(RetailTransactionLineProductTaxRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "Validation.RtlTranLineProductTax", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pTaxScheduleId", OracleDbType.Int32, 6, request.TaxScheduleId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTaxExemptOverrideType", OracleDbType.Int32, 4, request.TaxExemptOverrideType, ParameterDirection.Input));
                        
                        cmd.Parameters.Add(new OracleParameter("pErrorCode", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pDeniedText", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 75 });

                        con.Open();
                        cmd.ExecuteNonQuery();

                        var deniedText = cmd.Parameters["pDeniedText"].Value.ToString();
                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);
                        return new RetailTransactionLineProductTaxResponse
                        {
                            RequestId = request.RequestId,
                            DeniedText = deniedText,
                            ErrorCode = errorCode
                        };
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Validate a retail transaction line product tax override component of a transaction.
        /// </summary>
        /// <param name="request">The request that spawned this response.</param>
        /// <returns>Result of the validation process</returns>
        public override RetailTransactionLineProductTaxOverrideResponse RetailTransactionLineProductTaxOverrideValidation(RetailTransactionLineProductTaxOverrideRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "Validation.RtlTranLineProdTaxOvrd", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pTaxOverrideReasonType", OracleDbType.Int32, 4, request.TaxOverrideReasonType, ParameterDirection.Input));
                        
                        cmd.Parameters.Add(new OracleParameter("pErrorCode", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pDeniedText", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 75 });

                        con.Open();
                        cmd.ExecuteNonQuery();

                        var deniedText = cmd.Parameters["pDeniedText"].Value.ToString();
                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);
                        return new RetailTransactionLineProductTaxOverrideResponse
                        {
                            RequestId = request.RequestId,
                            DeniedText = deniedText,
                            ErrorCode = errorCode
                        };
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Validate a retail transaction line tender component of a transaction.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Result of a retail transaction line tender validation request.</returns>
        public override RetailTransactionLineTenderValidationResponse RetailTranactionLineTenderValidation(RetailTransactionLineTenderValidationRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "Validation.OriginatorTenderType", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pOriginatorGuid", OracleDbType.Varchar2, 36, request.DeviceGuid, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTenderId", OracleDbType.Int32, 6, request.TenderId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTenderType", OracleDbType.Int32, 4, (int)request.TenderType, ParameterDirection.Input));

                        cmd.Parameters.Add(new OracleParameter("pErrorCode", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pDeniedText", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 75 });

                        con.Open();
                        cmd.ExecuteNonQuery();

                        var deniedText = cmd.Parameters["pDeniedText"].Value.ToString();
                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);
                        return new RetailTransactionLineTenderValidationResponse
                        {
                            RequestId = request.RequestId,
                            DeniedText = deniedText,
                            ErrorCode = errorCode
                        };
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Validate a retail transaction line tender customer validation component of a transaction.
        /// </summary>
        /// <param name="request">The information necessary for validation.</param>
        /// <returns>The result of the operation.</returns>
        public override RetailTransactionLineTenderCustomerValidationResponse RetailTransactionLineTenderCustomerValidation(RetailTransactionLineTenderCustomerValidationRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "Validation.RtlTranLineTenderCust", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pPhysicalIdType", OracleDbType.Int32, 10, request.PhysicalIdType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pCustEntryMethodType", OracleDbType.Int32, 10, request.CustomerEntryMethodType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pCustomerGuid", OracleDbType.Varchar2, 36, request.CustomerGuid, ParameterDirection.Input));

                        cmd.Parameters.Add(new OracleParameter("pErrorCode", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pDeniedText", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 75 });

                        con.Open();
                        cmd.ExecuteNonQuery();

                        var deniedText = cmd.Parameters["pDeniedText"].Value.ToString();
                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);
                        return new RetailTransactionLineTenderCustomerValidationResponse(request)
                        {
                            DeniedText = deniedText,
                            ErrorCode = errorCode
                        };
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Validate a retail transaction line tender credit card validation component of a transaction.
        /// </summary>
        /// <param name="request">The information necessary for validation.</param>
        /// <returns>Result of the validation process</returns>
        public override RetailTransactionLineTenderCreditCardResponse RetailTransactionLineTenderCreditCardValidation(RetailTransactionLineTenderCreditCardRequest request)
        {
            throw new NotImplementedException();
            //using (OracleConnection con = OracleConnectionGet(ConnectionString))
            //{
            //    using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "Validation.RtlTranLineTenderCust", CommandType = CommandType.StoredProcedure })
            //    {
            //        try
            //        {
            //            cmd.Parameters.Add(new OracleParameter("pPhysicalIdType", OracleDbType.Int32, 10, request.PhysicalIdType, ParameterDirection.Input));
            //            cmd.Parameters.Add(new OracleParameter("pCustEntryMethodType", OracleDbType.Int32, 10, request.CustomerEntryMethodType, ParameterDirection.Input));
            //            cmd.Parameters.Add(new OracleParameter("pCustomerGuid", OracleDbType.Varchar2, 36, request.CustomerGuid, ParameterDirection.Input));

            //            cmd.Parameters.Add(new OracleParameter("pErrorCode", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
            //            cmd.Parameters.Add(new OracleParameter("pDeniedText", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 75 });

            //            con.Open();
            //            cmd.ExecuteNonQuery();

            //            var deniedText = cmd.Parameters["pDeniedText"].Value.ToString();
            //            var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);
            //            return new RetailTransactionLineTenderCustomerValidationResponse(request)
            //            {
            //                DeniedText = deniedText,
            //                ErrorCode = errorCode
            //            };
            //        }
            //        catch (Exception ex)
            //        {
            //            var rex = new ResourceLayerException(string.Empty, Formatting.FormatException(ex));
            //            LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
            //            throw rex;
            //        }
            //        finally
            //        {
            //            con.Close();
            //        }
            //    }
            //}
        }

        /// <summary>
        /// Validate the attributes of a transaction.
        /// </summary>
        /// <param name="request">Request object with attributes to validate.</param>
        /// <returns>Results of the operation.</returns>
        public override TransactionAttributeValidationResponse TransactionAttributeValidation(TransactionAttributeValidationRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "Validation.TransactionAttribute", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pDeviceGuid", OracleDbType.Varchar2, 36, request.DeviceGuid, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pPeriodNumber", OracleDbType.Int32, 10, request.Attributes.PeriodNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTransactionTypeCode", OracleDbType.Int32, 10, request.Attributes.TransactionTypeCode, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pValidationType", OracleDbType.Int32, 10, request.Attributes.ValidationType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAttendedType", OracleDbType.Int32, 10, request.Attributes.AttendType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pRetailTranType", OracleDbType.Int32, 10, request.Attributes.RetailTranType, ParameterDirection.Input));

                        cmd.Parameters.Add(new OracleParameter("pErrorCode", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pDeniedText", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 75 });

                        con.Open();
                        cmd.ExecuteNonQuery();

                        var deniedText = cmd.Parameters["pDeniedText"].Value.ToString();
                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);
                        return new TransactionAttributeValidationResponse(request)
                        {
                            DeniedText = deniedText,
                            ErrorCode = errorCode
                        };
                    }
                    catch (Exception ex)
                    {
                        var rex = new ResourceLayerException(request.RequestId, Formatting.FormatException(ex));
                        throw rex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }
    }
}
