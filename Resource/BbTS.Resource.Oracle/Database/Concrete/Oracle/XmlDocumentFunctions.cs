﻿using System;
using System.Data;
using BbTS.Core.Conversion;
using BbTS.Core.Logging.Tracing;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.System.Database;
using BbTS.Domain.Models.XmlDocument;
using Oracle.ManagedDataAccess.Client;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    /// <summary>
    /// This class exposes the various functions that are contained in the XmlDocument space of Envision
    /// </summary>
    [Trace(AttributePriority = 2)]
    public partial class OracleSource
    {
        /// <summary>
        /// This method will return the string representation of the xml document stored in the Envision database
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="documentType">The document type to retrieve</param>
        /// <returns></returns>
        public override TsXmlDocument XmlDocumentByTypeGet(ConnectionInfo connection, string documentType)
        {
            var item = new TsXmlDocument();

            using (var con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT * FROM Envision.XmlDocument WHERE DocumentType = '{documentType}'", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                if (!r.IsDBNull(0)) item.XmlDocumentId = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.DocumentType = r.GetString(1);
                                if (!r.IsDBNull(2)) item.Document = r.GetString(2);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return item;
        }

        /// <summary>
        /// This method will return the string representation of the xml document stored in the Envision database
        /// </summary>
        /// <param name="documentType">The document type to retrieve</param>
        /// <returns></returns>
        public override TsXmlDocument XmlDocumentGet(string documentType)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "Xml.DocumentGet", CommandType = CommandType.StoredProcedure })
                {

                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDocumentType", OracleDbType = OracleDbType.Varchar2, Size = 100, Direction = ParameterDirection.Input, Value = documentType });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Size = 0, Direction = ParameterDirection.Output, Value = string.Empty });

                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsXmlDocument();

                                if (!r.IsDBNull(0)) item.DocumentType = r.GetString(0);
                                if (!r.IsDBNull(1)) item.Document = r.GetString(1);

                                return item;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            return null;
        }


        /// <summary>
        /// This method will save an XmlDocument to the Envision database based on the passed in connection object.
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="document">The document type to save</param>
        /// <returns></returns>
        public override TsXmlDocument XmlDocumentSet(ConnectionInfo connection, TsXmlDocument document)
        {
            using (var con = OracleConnectionGet(connection))
            {
                var cmd = new OracleCommand { Connection = con, CommandText = "Xml.DocumentSet", CommandType = CommandType.StoredProcedure };

                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDocumentType", OracleDbType = OracleDbType.Varchar2, Size = 100, Direction = ParameterDirection.Input, Value = document.DocumentType });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDocument", OracleDbType = OracleDbType.Clob, Direction = ParameterDirection.Input, Value = document.Document });

                try
                {
                    // Open the database connection
                    con.Open();
                    // Execute the query
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return document;
        }

        /// <summary>
        /// This method will save an XmlDocument to the Envision database.
        /// </summary>
        /// <param name="document">The document type to save</param>
        /// <returns></returns>
        public override TsXmlDocument XmlDocumentSet(TsXmlDocument document)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "Xml.DocumentSet", CommandType = CommandType.StoredProcedure };

                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDocumentType", OracleDbType = OracleDbType.Varchar2, Size = 100, Direction = ParameterDirection.Input, Value = document.DocumentType });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDocument", OracleDbType = OracleDbType.Clob, Direction = ParameterDirection.Input, Value = document.Document });
                
                try
                {
                    // Open the database connection
                    con.Open();
                    // Execute the query
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return document;
        }

        /// <summary>
        /// This method will delete an XmlDocument from the Envision database.
        /// </summary>
        /// <param name="documentType">The document type to delete</param>
        /// <returns></returns>
        public override void XmlDocumentDelete(string documentType)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "Xml.DocumentDelete", CommandType = CommandType.StoredProcedure };

                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDocumentType", OracleDbType = OracleDbType.Varchar2, Size = 100, Direction = ParameterDirection.Input, Value = documentType });
                
                try
                {
                    // Open the database connection
                    con.Open();
                    // Execute the query
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }
    }
}
