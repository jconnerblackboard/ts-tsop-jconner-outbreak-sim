﻿using BbTS.Core;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Definitions.ArtsDataModel;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.Transaction.StoredValue;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Data;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        /// <summary>
        /// Initializes the DB facility for retrieving recent transactions.
        /// </summary>
        /// <param name="initExtractionStart">Extraction Start to be used as an override of the internal DB value, may be null</param>
        /// <param name="initExtractionEnd">Extraction End to be used as an override of the internal DB value, may be null</param>
        /// <returns>void</returns>
        public override void InitializeStoredValueTransactionsExtraction(DateTime? initExtractionStart, DateTime? initExtractionEnd)
        {
            using (OracleConnection con = new OracleConnection(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "ExternalClientFunctions.BbOneDataExtractInitialize", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdentity", OracleDbType = OracleDbType.Decimal, Direction = ParameterDirection.Input, Value = 1 });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pThisFrom", OracleDbType = OracleDbType.TimeStamp, IsNullable = true, Direction = ParameterDirection.Input, Value = initExtractionStart });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pThisThrough", OracleDbType = OracleDbType.TimeStamp, IsNullable = true, Direction = ParameterDirection.Input, Value = initExtractionEnd });
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Commits that the extracted transactions were successfully processed and the DB facility can update its internal state.
        /// </summary>
        /// <returns>void</returns>
        public override void FinalizeStoredValueTransactionsExtraction()
        {
            using (OracleConnection con = new OracleConnection(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "ExternalClientFunctions.BbOneDataExtractFinalize", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdentity", OracleDbType = OracleDbType.Decimal, Direction = ParameterDirection.Input, Value = 1 });
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Resets the facility for retrieving the transactions from DB.
        /// </summary>
        /// <returns>void</returns>
        public override void ResetStoredValueTransactionsExtraction()
        {
            using (OracleConnection con = new OracleConnection(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "ExternalClientFunctions.BbOneDataExtractReset", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pIdentity", OracleDbType = OracleDbType.Decimal, Direction = ParameterDirection.Input, Value = 1 });
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Context for chunked retrieval of transactions, created in <see cref="ExtractStoredValueTransactionsBegin"/> 
        /// and used in <see cref="ExtractStoredValueTransactionsGetNext"/>
        /// </summary>
        internal class ExtractStoredValueTransactionsContext : IExtractStoredValueTransactionsContext
        {
            internal OracleConnection Connection { get; }
            internal OracleDataReader Reader { get; }

            internal DateTime ExtractionStart { get; }
            internal DateTime ExtractionEnd { get; }

            private Boolean hasMoreData = true;
            internal ExtractStoredValueTransactionsContext(OracleConnection Connection, OracleDataReader Reader, DateTime ExtractionStart, DateTime ExtractionEnd)
            {
                this.Connection = Connection;
                this.Reader = Reader;
                this.ExtractionStart = ExtractionStart;
                this.ExtractionEnd = ExtractionEnd;
            }
            public Boolean HasMoreData()
            {
                return hasMoreData;
            }
            internal void SetMoreData(Boolean hasMoreData)
            {
                this.hasMoreData = hasMoreData;
            }
        }

        /// <summary>
        /// Requests start of transaction retrieval. Returned context is used in consequent calls of ExtractStoredValueTransactionsGetNext,
        /// which in turn returns a chunk of transactions. This method should be called after InitializeStoredValueTransactionsExtraction.
        /// Returned context needs to be used in consequent calls of <see cref="ExtractStoredValueTransactionsGetNext"/>
        /// Once all the data are retrieved, FinalizeStoredValueTransactionsExtraction must be called to commit that this round was successful
        /// and to advance to new data.
        /// </summary>
        /// <param name="extractionStart">output parameter - extraction start</param>
        /// <param name="extractionEnd">output parameter - extraction end</param>
        /// <returns>context to be used for chunked retrieval, <see cref="IExtractStoredValueTransactionsContext"/>.</returns>
        public override IExtractStoredValueTransactionsContext ExtractStoredValueTransactionsBegin(out DateTime extractionStart, out DateTime extractionEnd)
        {
            OracleConnection con = new OracleConnection(ConnectionString);
            try
            {
                Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "ExternalClientFunctions.BbOneDataExtractGet", CommandType = CommandType.StoredProcedure })
                {
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pId", OracleDbType = OracleDbType.Decimal, Direction = ParameterDirection.Input, Value = 1 });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pStartTime", OracleDbType = OracleDbType.TimeStamp, Direction = ParameterDirection.Output });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pEndTime", OracleDbType = OracleDbType.TimeStamp, Direction = ParameterDirection.Output });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                    con.Open();
                    OracleDataReader r = cmd.ExecuteReader();

                    extractionStart = ((OracleTimeStamp)(cmd.Parameters["pStartTime"].Value)).Value;
                    extractionEnd = ((OracleTimeStamp)(cmd.Parameters["pEndTime"].Value)).Value;

                    return new ExtractStoredValueTransactionsContext(con, r, extractionStart, extractionEnd);
                }
            }
            catch (Exception ex)
            {
                con.Close();
                throw new ResourceLayerException("", Formatting.FormatException(ex));
            }
        }

        /// <summary>
        /// Extract up to maxItem items. The context indicates whether there are more data or not. If there are more data then this method needs to be called again.
        /// </summary>
        /// <param name="context"><see cref="IExtractStoredValueTransactionsContext"/> - context for chunked retrieval, returned by <see cref="ResourceDatabase.ExtractStoredValueTransactionsBegin"/></param>
        /// <param name="maxItems"> - maximum number of items to be retrieved in this call</param>
        /// <returns>List of <see cref="StoredValueTransaction"/> transactions.</returns>
        public override List<StoredValueTransaction> ExtractStoredValueTransactionsGetNext(IExtractStoredValueTransactionsContext context, int maxItems)
        {
            ExtractStoredValueTransactionsContext ctx = context as ExtractStoredValueTransactionsContext;
            if (ctx == null)
            {
                throw new ResourceLayerException(String.Empty, "Given context has wrong type.");
            }
            if (maxItems <= 0)
            {
                throw new ResourceLayerException(String.Empty, "maxItem must be greater than zero.");
            }

            OracleDataReader r = ctx.Reader;
            OracleConnection con = ctx.Connection;
            DateTime extractionStart = ctx.ExtractionStart;
            DateTime extractionEnd = ctx.ExtractionEnd;

            bool exceptionThrown = false;
            var result = new List<StoredValueTransaction>();
            try
            {
                bool dataRead;
                int count = 0;
                while (dataRead = r.Read())
                {
                    int posId = -1;
                    string cardNum = string.Empty;
                    string custNum = string.Empty;

                    DateTime datetime = DateTime.MinValue;
                    string totalAmount = string.Empty;
                    string itemAmount = string.Empty;
                    string txnType = string.Empty;
                    long merchantId = -1;
                    string merchantName = string.Empty;
                    long transactionId = -1;
                    long transactionItemId = -1;
                    posId = !r.IsDBNull(0) ? r.GetInt32(0) : -1;
                    cardNum = !r.IsDBNull(1) ? r.GetString(1) : string.Empty;
                    custNum = !r.IsDBNull(2) ? r.GetString(2) : string.Empty;
                    datetime = !r.IsDBNull(3) ? r.GetDateTime(3) : DateTime.MinValue;
                    totalAmount = !r.IsDBNull(4) ? r.GetString(4) : string.Empty;
                    itemAmount = !r.IsDBNull(5) ? r.GetString(5) : string.Empty;
                    txnType = !r.IsDBNull(6) ? r.GetString(6) : string.Empty;
                    merchantId = !r.IsDBNull(7) ? r.GetInt64(7) : -1;
                    merchantName = !r.IsDBNull(8) ? r.GetString(8) : string.Empty;
                    transactionId = !r.IsDBNull(9) ? r.GetInt64(9) : -1;
                    transactionItemId = !r.IsDBNull(10) ? r.GetInt64(10) : -1;
                    
                    var txn = new StoredValueTransaction()
                    {
                        PosId = posId,
                        CardNumber = cardNum,
                        CustomerNumber = custNum,
                        CreatedDateTime = datetime,
                        TransactionTotalAmount = totalAmount,
                        TransactionItemAmount = itemAmount,
                        DebitCreditType = txnType.CompareTo("Sale") == 0 ? DebitCreditType.Debit : DebitCreditType.Credit,
                        MerchantId = merchantId,
                        MerchantName = merchantName,
                        TransactionId = transactionId,
                        TransactionItemId = transactionItemId
                    };

                    //sanity checks
                    if (extractionStart > txn.CreatedDateTime)
                    {
                        var difference = extractionStart.Ticks - txn.CreatedDateTime.Ticks;
                        throw new ResourceLayerException(String.Empty, $"StoredValue.GetRecentTransactions: transaction's 'created' date is smaller than the extractionStart by {difference} ticks: id = {txn.TransactionId}, CreatedDateTime = {txn.CreatedDateTime}, extractionStart = {extractionStart}, extractionEnd = {extractionEnd}.");
                    }
                    if (extractionEnd < txn.CreatedDateTime)
                    {
                        var difference = txn.CreatedDateTime.Ticks - extractionEnd.Ticks;
                        throw new ResourceLayerException(String.Empty, $"StoredValue.GetRecentTransactions: transaction's 'created' date is bigger than the extractionEnd by {difference} ticks: id = {txn.TransactionId}, CreatedDateTime = {txn.CreatedDateTime}, extractionStart = {extractionStart}, extractionEnd = {extractionEnd}.");
                    }
                    result.Add(txn);
                    count++;

                    //break after the maximum of txns are reached
                    if (count == maxItems)
                    {
                        break;
                    }
                }
                ctx.SetMoreData(dataRead);
            }
            catch (Exception ex)
            {
                exceptionThrown = true;
                throw new ResourceLayerException(String.Empty, Formatting.FormatException(ex));
            }
            finally
            {
                //close connection if an exception was thrown or if there is no more data
                if (exceptionThrown || !ctx.HasMoreData())
                {
                    con.Close();
                }
            }
            return result;
        }
    }
}
