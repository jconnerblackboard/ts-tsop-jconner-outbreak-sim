﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Credential;
using BbTS.Domain.Models.Definitions.Container;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.Exceptions.WebApi;
using BbTS.Domain.Models.System.Database;
using BbTS.Domain.Models.Transaction;
using Oracle.ManagedDataAccess.Client;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    /// <summary>
    /// Resource layer class for handling storage and retrieval of Credential data.
    /// </summary>
    public partial class OracleSource
    {
        /// <summary>
        /// Retrieve a full customer credential from the credential value.
        /// </summary>
        /// <param name="credentialValue">The credential value</param>
        /// <param name="cardNumber">The card number associated with the supplied credential</param>
        /// <returns>Error code.  0 = success.</returns>
        public override string CredentialValidateOnCardFormat(string credentialValue, out string cardNumber)
        {
            cardNumber = null;
            using (OracleConnection con = new OracleConnection(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "Validation.CardFormatIsValid", CommandType = CommandType.StoredProcedure })
                {

                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pTrack2CardNumber", OracleDbType = OracleDbType.Varchar2, Size = 22, Direction = ParameterDirection.Input, Value = credentialValue });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pSiteCode", OracleDbType = OracleDbType.Varchar2, Size = 12, Direction = ParameterDirection.Output });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pCardNumber", OracleDbType = OracleDbType.Varchar2, Size = 22, Direction = ParameterDirection.Output });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pIssueNumber", OracleDbType = OracleDbType.Varchar2, Size = 6, Direction = ParameterDirection.Output });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pIsValid", OracleDbType = OracleDbType.Varchar2, Size = 10, Direction = ParameterDirection.Output });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pErrorCode", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Output });

                    try
                    {
                        con.Open();
                        cmd.ExecuteNonQuery();

                        int errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);

                        if (errorCode == 0)
                        {
                            cardNumber = cmd.Parameters["pCardNumber"].Value.ToString();
                            return errorCode.ToString();
                        }

                        return errorCode.ToString();

                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Retrieve basic card credential information, validating card format in the process.
        /// </summary>
        /// <param name="credentialValue">Raw track 2 data.</param>
        /// <param name="requestId"></param>
        /// <returns>Card format pieces.  Throws WebApiException on errors.</returns>
        public override BasicCardCredential BasicCardCredentialGet(string credentialValue, string requestId = null)
        {
            using (OracleConnection con = new OracleConnection(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "Validation.CardFormatMatches", CommandType = CommandType.StoredProcedure })
                {

                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pTrack2CardNumber", OracleDbType = OracleDbType.Varchar2, Size = 22, Direction = ParameterDirection.Input, Value = credentialValue });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pSiteCode", OracleDbType = OracleDbType.Varchar2, Size = 12, Direction = ParameterDirection.Output });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pCardNumber", OracleDbType = OracleDbType.Varchar2, Size = 22, Direction = ParameterDirection.Output });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pIssueNumber", OracleDbType = OracleDbType.Varchar2, Size = 6, Direction = ParameterDirection.Output });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pIsValid", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Output });

                    try
                    {
                        con.Open();
                        cmd.ExecuteNonQuery();

                        
                        var siteCode = cmd.Parameters["pSiteCode"].Value?.ToString();
                        var cardNumber = cmd.Parameters["pCardNumber"].Value?.ToString();
                        var issueNumber = cmd.Parameters["pIssueNumber"].Value?.ToString();
                        var isValid = Formatting.TfStringToBool(cmd.Parameters["pIsValid"].Value?.ToString());

                        return new BasicCardCredential
                        {
                            CardNumber = cardNumber == "null" ? null : cardNumber,
                            IssueNumber = issueNumber == "null" ? null : issueNumber,
                            IsValid = isValid,
                            RawTrack2 = credentialValue,
                            SiteCode = siteCode == "null" ? null : siteCode
                        };

                    }
                    catch (WebApiException)
                    {
                        throw;
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException(requestId, Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Verifies that the customer and card is valid.
        /// </summary>
        /// <param name="cardCapture"></param>
        /// <param name="pin"></param>
        /// <param name="transactionClassification"></param>
        /// <param name="posId"></param>
        /// <param name="customerId"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public override ProcessingResult CustomerAndCardIsValid(CardCapture cardCapture, int? pin, TransactionClassification transactionClassification, int posId, out int customerId, ConnectionInfo connection = null)
        {
            customerId = 0;
            var processingResult = BbtsErrorDefinitions.ErrorGet(LoggingDefinitions.EventId.TransactionClassificationUnknown);

            string classificationText;
            switch (transactionClassification)
            {
                case TransactionClassification.Unknown:
                    return processingResult;
                case TransactionClassification.Arts:
                    classificationText = "StoredValue";
                    break;
                case TransactionClassification.Attendance:
                    classificationText = "Event";
                    break;
                case TransactionClassification.Board:
                    classificationText = "Board";
                    break;
                case TransactionClassification.Control:
                    return BbtsErrorDefinitions.ErrorWithAdditionalInfoGet(LoggingDefinitions.EventId.TransactionClassificationInvalid, "Control");
                default:
                    throw new ArgumentOutOfRangeException(nameof(transactionClassification), transactionClassification, null);
            }

            using (var con = connection == null ? OracleConnectionGet(ConnectionString) : OracleConnectionGet(connection))
            {
                string paddedCardNumber = Formatting.PadCardNumber(cardCapture.CardNumber, 22, '0');
                var cmd = new OracleCommand { Connection = con, CommandText = "Validation.CustomerAndCardIsValid", CommandType = CommandType.StoredProcedure };
                //Input
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCardNumber", OracleDbType = OracleDbType.Varchar2, Size = 22, Direction = ParameterDirection.Input, Value = paddedCardNumber });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIssueNumber", OracleDbType = OracleDbType.Varchar2, Size = 4, Direction = ParameterDirection.Input, Value = cardCapture.IssueNumber });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIssueNumberCaptured", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = Formatting.BooleanToTf(cardCapture.IssueNumberCaptured) });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pPinNumber", OracleDbType = OracleDbType.Int32, Size = 4, Direction = ParameterDirection.Input, Value = pin });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionType", OracleDbType = OracleDbType.Varchar2, Size = 12, Direction = ParameterDirection.Input, Value = classificationText });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pPosId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = posId });
                //Output
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Output});
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pErrorCode", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Output });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pErrorText", OracleDbType = OracleDbType.Varchar2, Size = 75, Direction = ParameterDirection.Output });

                try
                {
                    con.Open();
                    cmd.ExecuteNonQuery();

                    customerId = TryGetIntValue(cmd.Parameters["pCustomerId"], 0);
                    int errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);
                    string errorText = cmd.Parameters["pErrorText"].Value.ToString();

                    processingResult.ErrorCode = errorCode;
                    processingResult.DeniedText = errorText;
                    return processingResult;
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }
    }
}
