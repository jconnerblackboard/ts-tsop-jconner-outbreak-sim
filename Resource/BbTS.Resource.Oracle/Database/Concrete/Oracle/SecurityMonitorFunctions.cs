﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Core;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.Messaging.SecurityMonitor;
using BbTS.Domain.Models.Transaction.Validation;
using BbTS.Monitoring.Logging;
using BbTS.Resource.Database.Callback;
using Oracle.ManagedDataAccess.Client;
using BbTS.Core.Logging.Tracing;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {

        /// <summary>
        /// Register a list of doors for a DBMS_ALERT event.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        public override RegisterSignalListResponse RegisterSignalList(RegisterSignalListRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "Doors.RegisterSignalList", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pUserName", OracleDbType.Varchar2, 15, request.Username, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pListID", OracleDbType.Int32, 6, request.ListId, ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();

                        return new RegisterSignalListResponse
                        {
                            RequestId = request.RequestId
                        };
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Wait for a DBMS_ALERT door access monitor event.
        /// </summary>
        /// <returns>Wait response.</returns>
        [Trace(AttributeExclude = true)]
        public override DbmsCallbackResponse WaitForDaMonitorEvent()
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "WAITFORDAMONITOREVENT", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("nWAITRESULT", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("vWAITNAME", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 75 });
                        cmd.Parameters.Add(new OracleParameter("vWAITMESSAGE", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 2000 });

                        con.Open();
                        cmd.ExecuteNonQuery();

                        var result = cmd.Parameters["nWAITRESULT"].Value.ToString();
                        var name = cmd.Parameters["vWAITNAME"].Value.ToString();
                        var message = cmd.Parameters["vWAITMESSAGE"].Value.ToString();

                        return new DbmsCallbackResponse
                        {
                            Result = string.IsNullOrEmpty(result) || result == "null" ? 0 : Convert.ToInt32(result),
                            Name = !string.IsNullOrWhiteSpace(name) && name != "null" ? name : null,
                            Message = !string.IsNullOrWhiteSpace(message) && message != "null" ? message : null
                        };
                    }
                    catch (OracleException oex)
                    {
                        throw new ResourceLayerOracleException(Formatting.FormatException(oex), oex.Number);
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }
    }
}
