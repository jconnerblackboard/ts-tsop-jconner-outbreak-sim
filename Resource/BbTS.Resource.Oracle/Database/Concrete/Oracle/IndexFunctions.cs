﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using BbTS.Domain.Models.System.Database;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        #region Index Methods

        /// <summary>
        /// This method will retrieve the index objects as filtered by name
        /// </summary>
        /// <param name="connectionInfo">The connection details for the database</param>
        /// <param name="ownerName">The owner of the index</param>
        /// <param name="indexName">The name of the index to be retrieved</param>
        /// <returns>A List of Indexes</returns>
        public override Indexes IndexesGet(ConnectionInfo connectionInfo, string ownerName, string indexName)
        {
            // New up the base return object
            Indexes indexList = new Indexes { Items = new List<Index>() };

            // Grab all the records based on what was passed in
            IEnumerable<DataRow> dataTable = new OracleSource().DataTableGet(connectionInfo,IndexesQueryGet(ownerName, indexName),true).AsEnumerable();

            // For each group of records (by index name), create index records
            foreach (var table in dataTable.GroupBy(x => x.Field<string>("IndexName")).AsEnumerable())
            {
                // Make the base index - grab the name and table while we are here
                var index = new Index { Columns = new List<IndexColumn>(), TableName = table.FirstOrDefault().ItemArray[0].ToString(), IndexType = (IndexType)Enum.Parse(typeof(IndexType), table.FirstOrDefault().ItemArray[2].ToString(), true), Tablespace = table.FirstOrDefault().ItemArray[3].ToString() };

                // Now add the child column items
                foreach (DataRow row in table)
                {
                    index.Columns.Add(new IndexColumn
                    {
                        Position = Convert.ToInt32(row.ItemArray[4]),
                        Name = string.IsNullOrEmpty(row.ItemArray[6].ToString()) ? row.ItemArray[5].ToString() : "FunctionBasedIndex",
                        Expression = row.ItemArray[6].ToString(),
                        IsFunctionBased = !string.IsNullOrEmpty(row.ItemArray[6].ToString())
                    });
                }

                // Calculate the Md5 hash before adding the index name - This way the hash is just the "shape" of the index, irrespective of the name
                index.Md5Hash = Md5HashGet<Index>(index);

                // Now add the index name 
                index.IndexName = table.FirstOrDefault().ItemArray[1].ToString();

                // Add the index to the return list
                indexList.Items.Add(index);
            }

            // Return the data to the caller
            return indexList;
        }

        #endregion


    }
}