﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using BbTS.Core;
using BbTS.Core.Conversion;
using BbTS.Core.Media.ImageProcessing;
using BbTS.Core.Serialization;
using BbTS.Domain.Models.Customer;
using BbTS.Domain.Models.Customer.Management;
using BbTS.Domain.Models.Customer.Management.BoardPlan;
using BbTS.Domain.Models.Customer.Management.Email;
using BbTS.Domain.Models.Customer.Management.EventPlan;
using BbTS.Domain.Models.Customer.Management.StoredValue;
using BbTS.Domain.Models.Customer.Management.Xml;
using BbTS.Domain.Models.Definitions.Card;
using BbTS.Domain.Models.Definitions.StoredValue;
using BbTS.Domain.Models.Definitions.Transaction;
using BbTS.Domain.Models.DoorAccess;
using BbTS.Domain.Models.Event;
using BbTS.Domain.Models.Exceptions;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.Policy;
using BbTS.Domain.Models.System.Database;
using BbTS.Monitoring.Logging;
using BbTS.Resource.OracleManagedDataAccessExtensions;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        #region Customer Defined Fields

        /// <summary>
        /// This method will return the list of customers names in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsCustomerArchive> CustomerArchivesGet(ConnectionInfo connection)
        {
            var list = new List<TsCustomerArchive>();

            using (var con = OracleConnectionGet(connection))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = @"SELECT  Cust_Id,
                                          CustNum,
                                          DefaultCardNum,
                                          FirstName,
                                          MiddleName,
                                          LastName,
                                          BirthDate,
                                          Sex,
                                          PinNumber,
                                          Is_Active,
                                          Active_Start_Date,
                                          Active_End_Date,
                                          OpenDateTime,
                                          LastMod_DateTime,
                                          LastMod_UserName,
                                          DoorExtendedUnlockAllowed
                                  FROM    Envision.Customer",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsCustomerArchive();

                                if (!r.IsDBNull(0)) item.Cust_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.CustNum = r.GetString(1);
                                if (!r.IsDBNull(2)) item.DefaultCardNum = r.GetString(2);
                                if (!r.IsDBNull(3)) item.FirstName = r.GetString(3);
                                if (!r.IsDBNull(4)) item.MiddleName = r.GetString(4);
                                if (!r.IsDBNull(5)) item.LastName = r.GetString(5);
                                if (!r.IsDBNull(6)) item.BirthDate = DateTime.FromOADate(r.GetDouble(6));
                                if (!r.IsDBNull(7)) item.Sex = r.GetString(7);
                                if (!r.IsDBNull(8)) item.PinNumber = r.GetInt32(8);
                                if (!r.IsDBNull(9)) item.Is_Active = r.GetString(9);
                                if (!r.IsDBNull(10)) item.Active_Start_Date = DateTime.FromOADate(r.GetDouble(10));
                                if (!r.IsDBNull(11)) item.Active_End_Date = DateTime.FromOADate(r.GetDouble(11));
                                if (!r.IsDBNull(12)) item.OpenDateTime = DateTime.FromOADate(r.GetDouble(12));
                                if (!r.IsDBNull(13)) item.LastMod_DateTime = DateTime.FromOADate(r.GetDouble(13));
                                if (!r.IsDBNull(14)) item.LastMod_UserName = r.GetString(14);
                                if (!r.IsDBNull(15)) item.DoorExtendedUnlockAllowed = r.GetString(15);
                                if (!r.IsDBNull(1)) item.CustomerNumber = Convert.ToDecimal(r.GetString(1));

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Set a value for a customer defined field using the stored proc "CustomerFunctions.CustomerDefFieldValueSet"
        /// </summary>
        /// <param name="custId">CUST_ID field on the CUSTOMER_DEF_FIELD_VALUE table</param>
        /// <param name="customerDefFieldDefId">CUSTOMER_DEF_FIELD_DEF_ID field on the CUSTOMER_DEF_FIELD_VALUE table</param>
        /// <param name="fieldValue">FIELD_VALUE field on the CUSTOMER_DEF_FIELD_VALUE table</param>
        public override void CustomerDefinedFieldValueSet(int custId, int customerDefFieldDefId, string fieldValue)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerDefFieldValueSet", CommandType = CommandType.StoredProcedure };

                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustId", OracleDbType = OracleDbType.Int64, Size = 10, Direction = ParameterDirection.Input, Value = custId });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerDefFieldDefId", OracleDbType = OracleDbType.Int64, Size = 10, Direction = ParameterDirection.Input, Value = customerDefFieldDefId });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pFieldValue", OracleDbType = OracleDbType.Varchar2, Size = 4000, Direction = ParameterDirection.Input, Value = fieldValue });

                try
                {
                    // Open the database connection
                    con.Open();

                    // Execute the query
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Set a value for a customer defined group using the stored proc "CustomerFunctions.CustomerDefGroupValueSet"
        /// </summary>
        /// <param name="custId">CUST_ID field on the CUSTOMER_DEF_GRP_VALUE table</param>
        /// <param name="customerDefGrpDefId">CUSTOMER_DEF_GRP_DEF_ID field on the CUSTOMER_DEF_GRP_DEF_ITEM table</param>
        /// <param name="customerDefGrpDefItemId">CUSTOMER_DEF_GRP_DEF_ITEM_ID field on the CUSTOMER_DEF_GRP_DEF_ITEM table</param>
        public override void CustomerDefinedGroupValueSet(int custId, int customerDefGrpDefId, int customerDefGrpDefItemId)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerDefGroupValueSet", CommandType = CommandType.StoredProcedure };

                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustId", OracleDbType = OracleDbType.Int64, Size = 10, Direction = ParameterDirection.Input, Value = custId });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerDefFieldDefId", OracleDbType = OracleDbType.Int64, Size = 10, Direction = ParameterDirection.Input, Value = customerDefGrpDefId });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerDefGroupDefItemId", OracleDbType = OracleDbType.Int64, Size = 10, Direction = ParameterDirection.Input, Value = customerDefGrpDefItemId });

                try
                {
                    // Open the database connection
                    con.Open();

                    // Execute the query
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Get the cusotmer id from the provded email address.
        /// </summary>
        /// <param name="emailAddress">Customer email address.</param>
        /// <returns>Customer Id or null if not found.</returns>
        public override string CustomerGuidFromEmailAddress(string emailAddress)
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                string query = @"
                        SELECT          CST.CustomerGuid
                        FROM            Envision.Customer CST
                        INNER JOIN      Envision.Customer_Email EML on EML.Cust_ID = CST.Cust_ID
                        WHERE           EML.EMAIL_ADDRESS = :pValue";

                using (OracleCommand cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = query,
                    CommandType = CommandType.Text
                })
                {

                    cmd.Parameters.Add(
                        new OracleParameter { ParameterName = "pValue", OracleDbType = OracleDbType.Varchar2, Size = 255, Value = emailAddress, Direction = ParameterDirection.Input });

                    try
                    {
                        con.Open();
                        using (OracleDataReader r = cmd.ExecuteReader())
                        {
                            string operatorId = null;
                            while (r.Read())
                            {
                                operatorId = !r.IsDBNull(0) ? r.GetString(0) : null;
                            }
                            return operatorId;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Get the unique customer identifier (guid) from the customer card number.
        /// </summary>
        /// <param name="customerGuid">Customer guid.</param>
        /// <returns>A shortened list of customer information needed by device login.</returns>
        public override CredentialVerifyCustomer CustomerInformationFromCustomerGuidGet(string customerGuid)
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                string query = @"
                        SELECT          CUS.FirstName, CUS.LastName, CUS.BirthDate, CUS.Email_Address, PHO.ThumbNail, PHO.Photo,
                                        CM.Is_Active, CDR.AttendedShowAcctBalance, CDR.AttendedPrintAcctBalance, CDR.UnattendedShowAcctBalance, CDR.UnattendedPrintAcctBalance,
                                        CDR.CustomerShowName, CDR.CustomerShowBirthday, CDR.CustomerPrintName, CDR.CustomerPrintNumber, CDR.CustomerPrintCardNumber
                        FROM            (
                          SELECT          Distinct CST.Cust_Id, CST.Firstname, CST.Lastname, CST.Birthdate, EAD.Email_Address
                          FROM            Envision.Customer CST
                          LEFT JOIN      Envision.Customer_Email EAD ON EAD.cust_id = CST.cust_id
                          WHERE           CST.CustomerGuid = :pValue
                        ) CUS
                        LEFT JOIN   Envision.Customer_Photo PHO ON PHO.cust_id = CUS.cust_id
                        LEFT JOIN   Envision.CustomerMessage CM  ON  CUS.Cust_Id = CM.Cust_Id
                                                                        AND Udf_Functions.Now() BETWEEN NVL(CM.Start_DateTime,0) AND NVL(NULLIF(CM.End_DateTime,0),99999)
                                                                        AND CM.Is_Active = 'T'
                                                                        AND CM.Display_Count < CM.Max_Display_Count
                        LEFT JOIN   Envision.CustomerPosDisplayRule CDR ON  CUS.Cust_Id = CDR.Cust_Id";

                using (OracleCommand cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = query,
                    CommandType = CommandType.Text
                })
                {
                    if (!string.IsNullOrEmpty(customerGuid))
                        cmd.Parameters.Add(
                            new OracleParameter
                            {
                                ParameterName = "pValue",
                                OracleDbType = OracleDbType.Varchar2,
                                Size = 36,
                                Value = customerGuid,
                                Direction = ParameterDirection.Input
                            });

                    try
                    {
                        con.Open();
                        var customer = new CredentialVerifyCustomer();
                        using (OracleDataReader r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var firstName = !r.IsDBNull(0) ? r.GetString(0) : string.Empty;
                                var lastName = !r.IsDBNull(1) ? r.GetString(1) : string.Empty;
                                double? birthdate = null;
                                if (!r.IsDBNull(2)) birthdate = r.GetDouble(2);
                                var email = !r.IsDBNull(3) ? r.GetString(3) : string.Empty;
                                var blob = !r.IsDBNull(4) ? (byte[])r.GetValue(4) : null;
                                var posMessageWaiting = !r.IsDBNull(6) && Formatting.TfStringToBool(r.GetString(6));
                                var attendedShowAcctBalance = r.IsDBNull(7) || Formatting.TfStringToBool(r.GetString(7));
                                var attendedPrintAcctBalance = r.IsDBNull(8) || Formatting.TfStringToBool(r.GetString(8));
                                var unattendedShowAcctBalance = r.IsDBNull(9) || Formatting.TfStringToBool(r.GetString(9));
                                var unattendedPrintAcctBalance = r.IsDBNull(10) || Formatting.TfStringToBool(r.GetString(10));
                                var showName = r.IsDBNull(11) || Formatting.TfStringToBool(r.GetString(11));
                                var showBirthday = r.IsDBNull(12) || Formatting.TfStringToBool(r.GetString(12));
                                var printName = r.IsDBNull(13) || Formatting.TfStringToBool(r.GetString(13));
                                var printNumber = r.IsDBNull(14) || Formatting.TfStringToBool(r.GetString(14));
                                var printCardNumber = r.IsDBNull(15) || Formatting.TfStringToBool(r.GetString(15));

                                if (blob != null)
                                {
                                    customer.Thumbnail.Image = _getCustomerImage(blob, false);
                                    customer.Thumbnail.Type = "png";
                                }
                                else
                                {
                                    blob = !r.IsDBNull(5) ? (byte[])r.GetValue(5) : null;
                                    if (blob != null)
                                    {
                                        customer.Thumbnail.Image = _getCustomerImage(blob, true);
                                        customer.Thumbnail.Type = "png";
                                    }
                                }

                                customer.CustomerGuid = customerGuid;
                                customer.FirstName = firstName;
                                customer.LastName = lastName;
                                customer.Birthdate = birthdate.HasValue ? (DateTime?)DateTime.FromOADate(birthdate.Value) : null;
                                customer.EmailAddresses.Add(email);
                                customer.PosMessageWaiting = posMessageWaiting;
                                customer.AttendedShowAccountBalance = attendedShowAcctBalance;
                                customer.AttendedPrintAccountBalance = attendedPrintAcctBalance;
                                customer.UnattendedShowAccountBalance = unattendedShowAcctBalance;
                                customer.UnattendedPrintAccountBalance = unattendedPrintAcctBalance;
                                customer.ShowName = showName;
                                customer.ShowBirthday = showBirthday;
                                customer.PrintName = printName;
                                customer.PrintCustomerNumber = printNumber;
                                customer.PrintCardNumber = printCardNumber;
                            }
                            return customer;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Get the customer's PIN.
        /// </summary>
        /// <param name="customerGuid">The unique (guid) identifier for the customer.</param>
        /// <returns>Customer's PIN or null if not found.</returns>
        public override string CustomerPinGet(string customerGuid)
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                string query = @"SELECT CST.PinNumber FROM Envision.Customer CST WHERE CST.CustomerGuid = :pValue";

                using (OracleCommand cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = query,
                    CommandType = CommandType.Text
                })
                {

                    cmd.Parameters.Add(
                        new OracleParameter { ParameterName = "pValue", OracleDbType = OracleDbType.Varchar2, Size = 36, Value = customerGuid, Direction = ParameterDirection.Input });

                    try
                    {
                        con.Open();
                        using (OracleDataReader r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                return !r.IsDBNull(0) ? r.GetInt32(0).ToString() : null;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Get a customer's board balance.
        /// </summary>
        /// <param name="customerId">The unique (Guid) identifier for the customer.</param>
        /// <param name="deviceId">The unique (Guid) identifier for the device.</param>
        /// <returns>List of board plans assigned to the customer.</returns>
        public override List<CustomerBoardPlan> CustomerBoardBalanceGet(string customerId, string deviceId)
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");

            var list = new List<CustomerBoardPlan>();
            using (OracleConnection con = new OracleConnection(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.BoardBalancesGet", CommandType = CommandType.StoredProcedure })
                {
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDeviceGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = deviceId });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = customerId });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });

                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new CustomerBoardPlan();

                                if (!r.IsDBNull(0)) item.BoardPlanId = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.BoardPlanName = r.GetString(1);
                                if (!r.IsDBNull(2)) item.BoardPlanPriority = r.GetInt32(2);

                                if (!r.IsDBNull(3)) item.IsActive = Formatting.TfStringToBool(r.GetString(3));
                                if (!r.IsDBNull(4)) item.RegularGridLeft = r.GetInt32(4) != -2 ? r.GetInt32(4) : 999999;
                                if (!r.IsDBNull(5)) item.GuestGridLeft = r.GetInt32(5) != -2 ? r.GetInt32(5) : 999999;

                                if (!r.IsDBNull(6)) item.PeriodLeft = r.GetInt32(6) != -2 ? r.GetInt32(6) : 999999;
                                if (!r.IsDBNull(7)) item.DayLeft = r.GetInt32(7) != -2 ? r.GetInt32(7) : 999999;
                                if (!r.IsDBNull(8)) item.WeekLeft = r.GetInt32(8) != -2 ? r.GetInt32(8) : 999999;

                                if (!r.IsDBNull(9)) item.MonthLeft = r.GetInt32(9) != -2 ? r.GetInt32(9) : 999999;
                                if (!r.IsDBNull(10)) item.SemesterQuarterLeft = r.GetInt32(10) != -2 ? r.GetInt32(10) : 999999; 
                                if (!r.IsDBNull(11)) item.YearLeft = r.GetInt32(11) != -2 ? r.GetInt32(11) : 999999; 

                                if (!r.IsDBNull(12)) item.GuestTotalLeft = r.GetInt32(12) != -2 ? r.GetInt32(12) : 999999; 
                                if (!r.IsDBNull(13)) item.ExtraLeft = r.GetInt32(13) != -2 ? r.GetInt32(13) : 999999; 
                                if (!r.IsDBNull(14)) item.TransferLeft = r.GetInt32(14) != -2 ? r.GetInt32(14) : 999999; 

                                if (!r.IsDBNull(15)) item.TransferMealsBy = r.GetString(15);
                                if (!r.IsDBNull(16)) item.GuestReset = r.GetString(16);
                                if (!r.IsDBNull(17)) item.ExtraReset = r.GetString(17);
                                if (!r.IsDBNull(18)) item.LimitPeriodBy = r.GetString(18);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        var rex = new ResourceLayerException(customerId, Formatting.FormatException(ex));
                        LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                        return null;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            return list;
        }

        /// <summary>
        /// Get a customer's event balance.
        /// </summary>
        /// <param name="customerId">The unique (Guid) identifier for the customer.</param>
        /// <param name="deviceId">The unique (Guid) identifier for the device.</param>
        /// <returns>List of event balances assigned to the customer.</returns>
        public override List<CustomerEventBalance> CustomerEventBalanceGet(string customerId, string deviceId)
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");

            var list = new List<CustomerEventBalance>();
            using (OracleConnection con = new OracleConnection(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.EventBalancesGet", CommandType = CommandType.StoredProcedure })
                {
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDeviceGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = deviceId });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = customerId });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });

                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new CustomerEventBalance();

                                if (!r.IsDBNull(0)) item.EventId = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.EventNumber = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.EventName = r.GetString(2);
                                if (!r.IsDBNull(3)) item.RegularAllowed = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.GuestAllowed = r.GetInt32(4);
                                if (!r.IsDBNull(5)) item.RegularRemaining = r.GetInt32(5);
                                if (!r.IsDBNull(6)) item.GuestRemaining = r.GetInt32(6);
                                if (!r.IsDBNull(7)) item.LimitBy = r.GetString(7);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        var rex = new ResourceLayerException(customerId, Formatting.FormatException(ex));
                        LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                        return null;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            return list;
        }

        /// <summary>
        /// Get a customer's stored value balance.
        /// </summary>
        /// <param name="customerId">The unique (Guid) identifier for the customer.</param>
        /// <param name="deviceId">The unique (Guid) identifier for the device.</param>
        /// <returns>List of stored value account balances assigned to the customer.</returns>
        public override List<CustomerStoredValueBalance> CustomerStoredValueBalanceGet(string customerId, string deviceId)
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");

            var list = new List<CustomerStoredValueBalance>();
            using (var con = new OracleConnection(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.StoredValueBalancesGet", CommandType = CommandType.StoredProcedure })
                {
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDeviceGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = deviceId });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = customerId });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });

                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new CustomerStoredValueBalance();

                                if (!r.IsDBNull(0)) item.SvAccountTypeName = r.GetString(0);
                                if (!r.IsDBNull(1)) item.SvAccountShortName = r.GetString(1);
                                if (!r.IsDBNull(2)) item.Balance = r.GetDecimal(2) / 1000m;
                                if (!r.IsDBNull(3)) item.Credit = r.GetDecimal(3) / 1000m;

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        var rex = new ResourceLayerException(customerId, Formatting.FormatException(ex));
                        LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                        return null;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            return list;
        }

        /// <summary>
        /// This method will return a list of records for the CustomerDefFieldDef objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsCustomer_Def_Field_Def> CustomerDefFieldDefArchivesGet(ConnectionInfo connection)
        {
            var list = new List<TsCustomer_Def_Field_Def>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = @"SELECT  Customer_Def_Field_Def_Id,
                                          Title,
                                          FieldType,
                                          MaxLength,
                                          Store_Per_Transaction,
                                          Ui_Display_Top,
                                          Ui_Display_Left,
                                          Ui_Display_Height,
                                          Ui_Display_Width,
                                          IsReportEnabled,
                                          ReportTemplateFieldGuid,
                                          ReportTemplateFieldAggrGuid,
                                          IsIndexed
                                  FROM    Envision.Customer_Def_Field_Def",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsCustomer_Def_Field_Def();

                                if (!r.IsDBNull(0)) item.Customer_Def_Field_Def_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Title = r.GetString(1);
                                if (!r.IsDBNull(2)) item.FieldType = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.MaxLength = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.Store_Per_Transaction = r.GetString(4);
                                if (!r.IsDBNull(5)) item.Ui_Display_Top = r.GetInt32(5);
                                if (!r.IsDBNull(6)) item.Ui_Display_Left = r.GetInt32(6);
                                if (!r.IsDBNull(7)) item.Ui_Display_Height = r.GetInt32(7);
                                if (!r.IsDBNull(8)) item.Ui_Display_Width = r.GetInt32(8);
                                if (!r.IsDBNull(9)) item.IsReportEnabled = r.GetString(9);
                                if (!r.IsDBNull(10)) item.ReportTemplateFieldGuid = Formatting.ByteArrayToString(r.GetValue(10) as byte[]);
                                if (!r.IsDBNull(11)) item.ReportTemplateFieldAggrGuid = Formatting.ByteArrayToString(r.GetValue(11) as byte[]);
                                if (!r.IsDBNull(12)) item.IsIndexed = r.GetString(12);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }


        /// <summary>
        /// This method will return a list of records for the CustomerDefGrpDef objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsCustomer_Def_Grp_Def> CustomerDefGrpDefArchivesGet(ConnectionInfo connection)
        {
            var list = new List<TsCustomer_Def_Grp_Def>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = @"SELECT  Customer_Def_Grp_Def_Id,
                                          Name,
                                          Store_Per_Transaction,
                                          Ui_Display_Top,
                                          Ui_Display_Left,
                                          Ui_Display_Height,
                                          Ui_Display_Width,
                                          IsReportEnabled,
                                          ReportTemplateFieldGuid,
                                          ReportTemplateFieldAggrGuid,
                                          IsIndexed
                                  FROM    Envision.Customer_Def_Grp_Def",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsCustomer_Def_Grp_Def();

                                if (!r.IsDBNull(0)) item.Customer_Def_Grp_Def_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Name = r.GetString(1);
                                if (!r.IsDBNull(2)) item.Store_Per_Transaction = r.GetString(2);
                                if (!r.IsDBNull(3)) item.Ui_Display_Top = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.Ui_Display_Left = r.GetInt32(4);
                                if (!r.IsDBNull(5)) item.Ui_Display_Height = r.GetInt32(5);
                                if (!r.IsDBNull(6)) item.Ui_Display_Width = r.GetInt32(6);
                                if (!r.IsDBNull(7)) item.IsReportEnabled = r.GetString(7);
                                if (!r.IsDBNull(8)) item.ReportTemplateFieldGuid = Formatting.ByteArrayToString(r.GetValue(8) as byte[]);
                                if (!r.IsDBNull(9)) item.ReportTemplateFieldAggrGuid = Formatting.ByteArrayToString(r.GetValue(9) as byte[]);
                                if (!r.IsDBNull(10)) item.IsIndexed = r.GetString(10);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the CustomerDefGrpDefItem objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsCustomer_Def_Grp_Def_Item> CustomerDefGrpDefItemArchivesGet(ConnectionInfo connection)
        {
            var list = new List<TsCustomer_Def_Grp_Def_Item>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = @"SELECT  Customer_Def_Grp_Def_Item_Id,
                                          Customer_Def_Grp_Def_Id,
                                          Name,
                                          Position
                                  FROM    Envision.Customer_Def_Grp_Def_Item",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsCustomer_Def_Grp_Def_Item();

                                if (!r.IsDBNull(0)) item.Customer_Def_Grp_Def_Item_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Customer_Def_Grp_Def_Id = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Name = r.GetString(2);
                                if (!r.IsDBNull(3)) item.Position = r.GetInt32(3);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Get the cusotmer id from the provded customer card number.
        /// </summary>
        /// <param name="cardNumber">Customer card number.</param>
        /// <returns>Customer Guid or null if not found.</returns>
        public override CustomerGuidFromCardNumberResponse CustomerGuidFromCardNumber(string cardNumber)
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");

            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                string query = @"
                        SELECT      CST.CustomerGuid,
                                    CRD.Issue_Number,
                                    CRD.Lost_Flag,
                                    CRD.Card_Status,
                                    CRD.ActiveStartDate CardActiveStartDate,
                                    CRD.ActiveEndDate CardActiveEndDate,
                                    CST.Is_Active,
                                    CST.Active_Start_Date CustActiveStartDate,
                                    CST.Active_End_Date CustActiveEndDate,
                                    CST.PinNumber
                        FROM        Customer CST
                        INNER JOIN  Card CRD on CRD.Cust_ID = CST.Cust_ID
                        WHERE       CRD.CardNum = LPAD(:pCardNum, 22,'0')";

                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = query, CommandType = CommandType.Text })
                {
                    cmd.Parameters.Add(
                        new OracleParameter { ParameterName = "pCardNum", OracleDbType = OracleDbType.Varchar2, Size = 22, Value = cardNumber, Direction = ParameterDirection.Input });

                    try
                    {
                        con.Open();
                        using (OracleDataReader r = cmd.ExecuteReader())
                        {
                            if (r.Read())
                            {
                                return new CustomerGuidFromCardNumberResponse
                                {
                                    CustomerGuid = !r.IsDBNull(0) ? r.GetString(0) : null,
                                    IssueNumber = !r.IsDBNull(1) ? r.GetString(1) : null,
                                    CardIsLost = Formatting.TfStringToBool(r.GetString(2)),
                                    CardStatus = !r.IsDBNull(3) ? (CardStatusType)r.GetInt32(3) : CardStatusType.Unknown,
                                    CardActiveStartDate = !r.IsDBNull(4) ? (DateTime?)r.GetDateTime(4) : null,
                                    CardActiveEndDate = !r.IsDBNull(5) ? (DateTime?)r.GetDateTime(5) : null,
                                    CustomerIsActive = Formatting.TfStringToBool(r.GetString(6)),
                                    CustomerActiveStartDate = !r.IsDBNull(7) ? (DateTime?)DateTime.FromOADate(r.GetDouble(7)) : null,
                                    CustomerActiveEndDate = !r.IsDBNull(8) ? (DateTime?)DateTime.FromOADate(r.GetDouble(8)) : null,
                                    PinNumber = !r.IsDBNull(9) ? r.GetInt32(9) : default(int?)
                                };
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Get the customer numerical id (cust_id) from their unique identifier (CustomerGuid).
        /// </summary>
        /// <param name="guid">The unique identifier for the customer (CustomerGuid).</param>
        /// <returns>The customer numerical id (cust_id)</returns>
        public override int CustomerIdFromCustomerGuidGet(string guid)
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                string query = @"Select cust_id From Customer Where customerGuid = :pValue";

                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = query,
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        cmd.Parameters.Add(
                        new OracleParameter
                        {
                            ParameterName = "pValue",
                            OracleDbType = OracleDbType.Varchar2,
                            Size = 36,
                            Value = new Guid(guid).ToString("D"),
                            Direction = ParameterDirection.Input
                        });

                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            var customerId = -1;
                            while (r.Read())
                            {
                                customerId = !r.IsDBNull(0) ? r.GetInt32(0) : -1;
                            }
                            if (customerId == -1)
                            {
                                throw new ResourceLayerException("", $"Unable to match a customer id to the provided value '{guid}'.");
                            }
                            return customerId;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Get the unique identifier (CustomerGuid) for the customer from their customer numerical id (cust_id).
        /// </summary>
        /// <param name="id">The customer numerical id (cust_id).</param>
        /// <returns>The unique identifier (CustomerGuid) for the customer</returns>
        public override string CustomerGuidFromCustomerIdGet(int id)
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                string query = @"Select customerGuid From Customer Where cust_id = :pValue";

                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = query,
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pValue", OracleDbType = OracleDbType.Int32, Size = 10, Value = id, Direction = ParameterDirection.Input });

                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            var customerGuid = string.Empty;

                            while (r.Read())
                            {
                                customerGuid = !r.IsDBNull(0) ? r.GetString(0) : string.Empty;
                            }

                            return customerGuid;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Get the customer information required by the credential verify process from the customer card number.
        /// </summary>
        /// <param name="cardNumber">Customer card number.</param>
        /// <returns>The customer information.</returns>
        public override CredentialVerifyCustomer CustomerInformationFromCardNumberGet(string cardNumber)
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                string query = @"
                        SELECT      CustomerGuid, Firstname, Lastname, Birthdate, EAD.Email_Address, PHO.Thumbnail, PHO.Photo,
                                    CM.Is_Active, CDR.AttendedShowAcctBalance, CDR.AttendedPrintAcctBalance, CDR.UnattendedShowAcctBalance, CDR.UnattendedPrintAcctBalance,
                                    CDR.CustomerShowName, CDR.CustomerShowBirthday, CDR.CustomerPrintName, CDR.CustomerPrintNumber, CDR.CustomerPrintCardNumber
                        FROM        Envision.Customer CST
                        INNER JOIN  Envision.Card CRD ON CRD.cust_id = CST.cust_id
                        INNER JOIN  Envision.Customer_Email EAD ON EAD.cust_id = CST.cust_id
                        INNER JOIN  Envision.Customer_Photo PHO ON PHO.cust_id = CST.cust_id
                        LEFT JOIN   Envision.CustomerMessage CM  ON  CUS.Cust_Id = CM.Cust_Id
                                                                        AND Udf_Functions.Now() BETWEEN NVL(CM.Start_DateTime,0) AND NVL(NULLIF(CM.End_DateTime,0),99999)
                                                                        AND CM.Is_Active = 'T'
                                                                        AND CM.Display_Count < CM.Max_Display_Count
                        LEFT JOIN   Envision.CustomerPosDisplayRule CDR ON  CUS.Cust_Id = CDR.Cust_Id
                        WHERE           CRD.CardNumber = :pValue";

                using (OracleCommand cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = query,
                    CommandType = CommandType.Text
                })
                {
                    if (!string.IsNullOrEmpty(cardNumber))
                        cmd.Parameters.Add(
                            new OracleParameter
                            {
                                ParameterName = "pValue",
                                OracleDbType = OracleDbType.Varchar2,
                                Size = cardNumber.Length,
                                Value = cardNumber,
                                Direction = ParameterDirection.Input
                            });

                    try
                    {
                        con.Open();
                        var customer = new CredentialVerifyCustomer();
                        using (OracleDataReader r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var customerGuid = !r.IsDBNull(0) ? r.GetString(0) : string.Empty;
                                var firstName = !r.IsDBNull(1) ? r.GetString(1) : string.Empty;
                                var lastName = !r.IsDBNull(2) ? r.GetString(2) : string.Empty;
                                var birthdate = !r.IsDBNull(3) ? r.GetDouble(3) : 0;
                                var email = !r.IsDBNull(4) ? r.GetString(4) : string.Empty;
                                var blob = !r.IsDBNull(5) ? (byte[])r.GetValue(5) : null;
                                var posMessageWaiting = !r.IsDBNull(7) && Formatting.TfStringToBool(r.GetString(7));
                                var attendedShowAcctBalance = r.IsDBNull(8) || Formatting.TfStringToBool(r.GetString(8));
                                var attendedPrintAcctBalance = r.IsDBNull(9) || Formatting.TfStringToBool(r.GetString(9));
                                var unattendedShowAcctBalance = r.IsDBNull(10) || Formatting.TfStringToBool(r.GetString(10));
                                var unattendedPrintAcctBalance = r.IsDBNull(11) || Formatting.TfStringToBool(r.GetString(11));
                                var showName = r.IsDBNull(12) || Formatting.TfStringToBool(r.GetString(12));
                                var showBirthday = r.IsDBNull(13) || Formatting.TfStringToBool(r.GetString(13));
                                var printName = r.IsDBNull(14) || Formatting.TfStringToBool(r.GetString(14));
                                var printNumber = r.IsDBNull(15) || Formatting.TfStringToBool(r.GetString(15));
                                var printCardNumber = r.IsDBNull(16) || Formatting.TfStringToBool(r.GetString(16));

                                if (blob != null)
                                {
                                    customer.Thumbnail.Image = _getCustomerImage(blob, false);
                                    customer.Thumbnail.Type = "png";
                                }
                                else
                                {
                                    blob = !r.IsDBNull(6) ? (byte[])r.GetValue(6) : null;
                                    if (blob != null)
                                    {
                                        customer.Thumbnail.Image = _getCustomerImage(blob, true);
                                        customer.Thumbnail.Type = "png";
                                    }
                                }

                                customer.CustomerGuid = customerGuid;
                                customer.FirstName = firstName;
                                customer.LastName = lastName;
                                customer.Birthdate = DateTime.FromOADate(birthdate);
                                customer.EmailAddresses.Add(email);
                                customer.PosMessageWaiting = posMessageWaiting;
                                customer.AttendedShowAccountBalance = attendedShowAcctBalance;
                                customer.AttendedPrintAccountBalance = attendedPrintAcctBalance;
                                customer.UnattendedShowAccountBalance = unattendedShowAcctBalance;
                                customer.UnattendedPrintAccountBalance = unattendedPrintAcctBalance;
                                customer.ShowName = showName;
                                customer.ShowBirthday = showBirthday;
                                customer.PrintName = printName;
                                customer.PrintCustomerNumber = printNumber;
                                customer.PrintCardNumber = printCardNumber;
                            }
                            return customer;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Get the customer defined group item ids for the specified customerGuid.  If customerGuid is null, all customers will be considered.
        /// </summary>
        /// <param name="customerGuid"></param>
        /// <returns></returns>
        public override List<CustomerDefinedGroupItemIdsForCustomerGuid> CustomerDefinedGroupIdsForCustomerGuidGet(Guid? customerGuid)
        {
            var list = new List<CustomerDefinedGroupItemIdsForCustomerGuid>();

            using (var con = OracleConnectionGet(ConnectionString))
            {
                string where = customerGuid == null ? string.Empty : $" WHERE         CUS.CustomerGuid = '{customerGuid.Value.ToString("D").ToUpper()}'";

                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = @"SELECT          CUS.CustomerGuid,
                                                    GRP.Customer_Def_Grp_Def_Item_Id
                                      FROM          Customer                        CUS
                                INNER JOIN          Customer_Def_Grp_Value          GRP   ON CUS.Cust_Id = GRP.Cust_Id
                                INNER JOIN          Policy_Condition_Value_CustGrp  POL   ON GRP.Customer_Def_Grp_Def_Item_Id = POL.Customer_Def_Grp_Def_Item_Id" +
                                where,
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            string lastGuid = string.Empty;
                            CustomerDefinedGroupItemIdsForCustomerGuid item = null;
                            while (r.Read())
                            {
                                if (r.IsDBNull(0) || r.IsDBNull(1)) continue;

                                string currentGuid = r.GetString(0);
                                int customerDefGrpDefItemId = r.GetInt32(1);

                                if (currentGuid != lastGuid)
                                {
                                    //Row has a different guid, so we need to add the current item (if it isn't null) and start a new one.
                                    if (item != null) list.Add(item);

                                    item = new CustomerDefinedGroupItemIdsForCustomerGuid
                                    {
                                        CustomerId = Guid.Parse(currentGuid),
                                        CustomerDefinedGroupDefinitionItemIdList = new List<int> { customerDefGrpDefItemId }
                                    };

                                    lastGuid = currentGuid;
                                }
                                else
                                {
                                    //Row has the same guid, so we'll add this customerDefGrpDefItemId to the list and carry on.
                                    item?.CustomerDefinedGroupDefinitionItemIdList.Add(customerDefGrpDefItemId);
                                }
                            }

                            //Add the final item
                            if (item != null) list.Add(item);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Get the customer defined group item ids for the specified customerGuid.  If customerGuid is null or empty, all customers will be considered.
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="customerGuid"></param>
        /// <returns></returns>
        public override List<CustomerDefinedGroupItemIdsForCustomerGuid> CustomerDefinedGroupIdsForCustomerGuidGet(ConnectionInfo connection, string customerGuid)
        {
            var list = new List<CustomerDefinedGroupItemIdsForCustomerGuid>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                var where = string.IsNullOrEmpty(customerGuid) ? string.Empty : $" WHERE         CUS.CustomerGuid = '{Guid.Parse(customerGuid).ToString("D").ToUpper()}'";

                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = @"SELECT          CUS.CustomerGuid,
                                                    GRP.Customer_Def_Grp_Def_Item_Id
                                      FROM          Customer                        CUS
                                INNER JOIN          Customer_Def_Grp_Value          GRP   ON CUS.Cust_Id = GRP.Cust_Id
                                INNER JOIN          Policy_Condition_Value_CustGrp  POL   ON GRP.Customer_Def_Grp_Def_Item_Id = POL.Customer_Def_Grp_Def_Item_Id" +
                                where,
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            string lastGuid = string.Empty;
                            CustomerDefinedGroupItemIdsForCustomerGuid item = null;
                            while (r.Read())
                            {
                                if (r.IsDBNull(0) || r.IsDBNull(1)) continue;

                                var currentGuid = r.GetString(0);
                                var customerDefGrpDefItemId = r.GetInt32(1);

                                if (currentGuid != lastGuid)
                                {
                                    //Row has a different guid, so we need to add the current item (if it isn't null) and start a new one.
                                    if (item != null) list.Add(item);

                                    item = new CustomerDefinedGroupItemIdsForCustomerGuid
                                    {
                                        CustomerId = Guid.Parse(currentGuid),
                                        CustomerDefinedGroupDefinitionItemIdList = new List<int> { customerDefGrpDefItemId }
                                    };

                                    lastGuid = currentGuid;
                                }
                                else
                                {
                                    //Row has the same guid, so we'll add this customerDefGrpDefItemId to the list and carry on.
                                    item?.CustomerDefinedGroupDefinitionItemIdList.Add(customerDefGrpDefItemId);
                                }
                            }

                            //Add the final item
                            if (item != null) list.Add(item);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <inheritdoc />
        public override CustomerAvailableBalance AvailableBalanceForCustomerGet(Guid customerId, DateTimeOffset evaluationDateTime, List<int> storedValueAccountTypeIds, List<int> transactionLimitIds)
        {
            CustomerAvailableBalance result = null;

            var accountTypeIdsParameterValue = new StringBuilder();
            string comma = string.Empty;
            foreach (int storedValueAccountTypeId in storedValueAccountTypeIds)
            {
                accountTypeIdsParameterValue.Append($"{comma}{storedValueAccountTypeId}");
                if (comma == string.Empty) comma = ",";
            }

            var limitIdsParameterValue = new StringBuilder();
            comma = string.Empty;
            foreach (int transactionLimitId in transactionLimitIds)
            {
                limitIdsParameterValue.Append($"{comma}{transactionLimitId}");
                if (comma == string.Empty) comma = ",";
            }

            var accounts = new List<StoredValueAccountInfoForPolicy>();
            var transactionLimits = new List<StoredValueTransactionLimitForPolicy>();

            using (var con = OracleConnectionGet(ConnectionString))
            {
                var cmd = new OracleCommand { Connection = con, CommandText = "PackTranPolicy.ProcessPolicyCustAccountsGet", CommandType = CommandType.StoredProcedure };
                //Input
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = customerId });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionDateTime", OracleDbType = OracleDbType.Double, Size = 14, Direction = ParameterDirection.Input, Value = evaluationDateTime.DateTime.ToOADate() });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pSVAccountTypeIds", OracleDbType = OracleDbType.Varchar2, Size = 200, Direction = ParameterDirection.Input, Value = accountTypeIdsParameterValue.ToString() });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pSVLimitIds", OracleDbType = OracleDbType.Varchar2, Size = 200, Direction = ParameterDirection.Input, Value = limitIdsParameterValue.ToString() });
                //Output
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pSVAcccounts", OracleDbType = OracleDbType.RefCursor, Size = 0, Direction = ParameterDirection.Output });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pSVLimits", OracleDbType = OracleDbType.RefCursor, Size = 0, Direction = ParameterDirection.Output });

                try
                {
                    // Open the database connection
                    con.Open();

                    using (var r = cmd.ExecuteReader())
                    {
                        //Accounts
                        while (r.Read())
                        {
                            var account = new StoredValueAccountInfoForPolicy();

                            if (!r.IsDBNull(0)) account.AccountTypeId = r.GetInt32(0);
                            if (!r.IsDBNull(1)) account.IsTaxExempt = r.GetString(1) == "T";
                            if (!r.IsDBNull(2)) account.AccountId = r.GetInt32(2);
                            if (!r.IsDBNull(3)) account.DepletionOrder = r.GetInt32(3);
                            if (!r.IsDBNull(4)) account.Balance = r.GetDecimal(4) / 1000m;
                            if (!r.IsDBNull(5)) account.CreditLimit = r.GetDecimal(5) / 1000m;
                            if (!r.IsDBNull(6)) account.AvailableBalance = r.GetDecimal(6) / 1000m;
                            if (!r.IsDBNull(7)) account.OriginalAvailableBalance = r.GetDecimal(7) / 1000m;

                            accounts.Add(account);
                        }

                        r.NextResult();
                        
                        //Transaction Limits
                        while (r.Read())
                        {
                            var limit = new StoredValueTransactionLimitForPolicy();

                            if (!r.IsDBNull(0)) limit.SvTransactionLimitId = r.GetInt32(0);
                            if (!r.IsDBNull(1)) limit.SvAccountTypeId = r.GetInt32(1);
                            if (!r.IsDBNull(2)) limit.PurchaseAmountLimit = r.GetDecimal(2) / 1000m;
                            if (!r.IsDBNull(3)) limit.PurchaseCountLimit = r.GetInt32(3);
                            if (!r.IsDBNull(4)) limit.AmountLimitConsumed = r.GetDecimal(4) / 1000m;
                            if (!r.IsDBNull(5)) limit.CountLimitConsumed = r.GetInt32(5);

                            transactionLimits.Add(limit);
                        }

                        if (accounts.Count > 0)
                        {
                            result = new CustomerAvailableBalance { AvailableBalance = 0.00m };
                            foreach (var account in accounts)
                            {
                                decimal purchaseAmountRemaining = decimal.MaxValue;

                                var limit = transactionLimits.Find(l => l.SvAccountTypeId == account.AccountTypeId);
                                if (limit != null)
                                {
                                    purchaseAmountRemaining = limit.PurchaseAmountLimit - limit.AmountLimitConsumed;
                                    int count = limit.PurchaseCountLimit - limit.CountLimitConsumed;

                                    if (count <= 0 || purchaseAmountRemaining <= 0.00m)
                                    {
                                        purchaseAmountRemaining = 0.00m;
                                    }
                                }

                                result.AvailableBalance += Math.Min(account.AvailableBalance, purchaseAmountRemaining);
                                if (account.IsTaxExempt) result.TaxExempt = true;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return result;
        }

        /// <inheritdoc />
        public override CustomerPostResponse CustomerSet(TsCustomerPost customer)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerSetXml", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pXml", OracleDbType = OracleDbType.Clob, Direction = ParameterDirection.Input, Value = Xml.Serialize(customer) });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerId", OracleDbType = OracleDbType.Int32, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Output });

                        con.Open();
                        cmd.ExecuteNonQuery();

                        return new CustomerPostResponse()
                        {
                            CustomerGuid = Guid.Parse(cmd.Parameters["pCustomerGuid"].Value.ToString()),
                            CustomerId = int.Parse(cmd.Parameters["pCustomerId"].Value.ToString())
                        };
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override bool CustomerNumberAvailable(decimal customerNumber)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandType = CommandType.Text, CommandText = $"SELECT count(*) FROM Customer WHERE CustomerNumber = {customerNumber}" })
                    {
                        con.Open();
                        return int.Parse(cmd.ExecuteScalar().ToString()) == 0;
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Get a customer by his guid
        /// </summary>
        /// <param name="customerGuid">Customer's guid</param>
        /// <returns>Customer</returns>
        public override TsCustomer CustomerGet(string customerGuid)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = customerGuid });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });

                        try
                        {
                            con.Open();

                            using (var r = cmd.ExecuteReader())
                            {
                                if (r.Read())
                                {
                                    return new TsCustomer
                                    {
                                        CustomerId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                        CustomerNumber = !r.IsDBNull(1) ? r.GetString(1).TrimStart('0') : string.Empty,
                                        DefaultCardNumber = !r.IsDBNull(2) ? r.GetString(2) : string.Empty,
                                        FirstName = !r.IsDBNull(3) ? r.GetString(3) : string.Empty,
                                        MiddleName = !r.IsDBNull(4) ? r.GetString(4) : string.Empty,
                                        LastName = !r.IsDBNull(5) ? r.GetString(5) : string.Empty,
                                        BirthDate = !r.IsDBNull(6) ? DateTime.FromOADate(r.GetDouble(6)) : DateTime.MinValue,
                                        Sex = !r.IsDBNull(7) ? r.GetString(7) : string.Empty,
                                        PinNumber = !r.IsDBNull(8) ? r.GetInt32(8) : -1,
                                        IsActive = !r.IsDBNull(9) && Formatting.TfStringToBool(r.GetString(9)),
                                        ActiveStartDate = !r.IsDBNull(10) ? DateTime.FromOADate(r.GetDouble(10)) : DateTime.MinValue,
                                        ActiveEndDate = !r.IsDBNull(11) ? DateTime.FromOADate(r.GetDouble(11)) : DateTime.MinValue,
                                        OpenDateTime = !r.IsDBNull(12) ? DateTime.FromOADate(r.GetDouble(12)) : DateTime.MinValue,
                                        LastModifiedDateTime = !r.IsDBNull(13) ? DateTime.FromOADate(r.GetDouble(13)) : DateTime.MinValue,
                                        LastModifiedUsername = !r.IsDBNull(14) ? r.GetString(14) : string.Empty,
                                        DoorExtendedUnlockAllowed = !r.IsDBNull(15) && Formatting.TfStringToBool(r.GetString(15)),
                                        // CustomerNumber skipped
                                        CustomerGuid = !r.IsDBNull(17) ? r.GetString(17) : string.Empty,
                                        MessageText = !r.IsDBNull(18) ? r.GetString(18) : string.Empty,
                                        AttendedShowAcctBalance = r.IsDBNull(19) || Formatting.TfStringToBool(r.GetString(19)),
                                        AttendedPrintAcctBalance = r.IsDBNull(20) || Formatting.TfStringToBool(r.GetString(20)),
                                        UnattendedShowAcctBalance = r.IsDBNull(21) || Formatting.TfStringToBool(r.GetString(21)),
                                        UnattendedPrintAcctBalance = r.IsDBNull(22) || Formatting.TfStringToBool(r.GetString(22)),
                                        CustomerShowName = r.IsDBNull(23) || Formatting.TfStringToBool(r.GetString(23)),
                                        CustomerShowBirthday = r.IsDBNull(24) || Formatting.TfStringToBool(r.GetString(24)),
                                        CustomerPrintName = r.IsDBNull(25) || Formatting.TfStringToBool(r.GetString(25)),
                                        CustomerPrintNumber = r.IsDBNull(26) || Formatting.TfStringToBool(r.GetString(26)),
                                        CustomerPrintCardNumber = r.IsDBNull(27) || Formatting.TfStringToBool(r.GetString(27))
                                    };
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new ResourceLayerException("", Formatting.FormatException(ex));
                        }
                        finally
                        {
                            con.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the customer by id or number
        /// </summary>
        /// <param name="customerNumber">The customer number.</param>
        /// <param name="customerId">The customer id.</param>
        /// <returns>The Customer</returns>
        public override TsCustomer CustomerGet(string customerNumber, int? customerId)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerNumber", OracleDbType = OracleDbType.Varchar2, Size = 22, Direction = ParameterDirection.Input, Value = customerNumber?.PadLeft(22, '0') });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });

                        try
                        {
                            con.Open();

                            using (var r = cmd.ExecuteReader())
                            {
                                if (r.Read())
                                {
                                    return new TsCustomer
                                    {
                                        CustomerId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                        CustomerNumber = !r.IsDBNull(1) ? r.GetString(1).TrimStart('0') : string.Empty,
                                        DefaultCardNumber = !r.IsDBNull(2) ? r.GetString(2) : null,
                                        FirstName = !r.IsDBNull(3) ? r.GetString(3) : null,
                                        MiddleName = !r.IsDBNull(4) ? r.GetString(4) : null,
                                        LastName = !r.IsDBNull(5) ? r.GetString(5) : null,
                                        BirthDate = !r.IsDBNull(6) ? DateTime.FromOADate(r.GetDouble(6)) : (DateTime?)null,
                                        Sex = !r.IsDBNull(7) ? r.GetString(7) : null,
                                        PinNumber = !r.IsDBNull(8) ? r.GetInt32(8) : -1,
                                        IsActive = !r.IsDBNull(9) && Formatting.TfStringToBool(r.GetString(9)),
                                        ActiveStartDate = !r.IsDBNull(10) ? DateTime.FromOADate(r.GetDouble(10)) : (DateTime?)null,
                                        ActiveEndDate = !r.IsDBNull(11) ? DateTime.FromOADate(r.GetDouble(11)) : (DateTime?)null,
                                        OpenDateTime = !r.IsDBNull(12) ? DateTime.FromOADate(r.GetDouble(12)) : DateTime.MinValue,
                                        LastModifiedDateTime = !r.IsDBNull(13) ? DateTime.FromOADate(r.GetDouble(13)) : DateTime.MinValue,
                                        LastModifiedUsername = !r.IsDBNull(14) ? r.GetString(14) : null,
                                        DoorExtendedUnlockAllowed = !r.IsDBNull(15) && Formatting.TfStringToBool(r.GetString(15)),
                                        // CustomerNumber skipped
                                        CustomerGuid = !r.IsDBNull(17) ? r.GetString(17) : null
                                    };
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new ResourceLayerException("", Formatting.FormatException(ex));
                        }
                        finally
                        {
                            con.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return null;
        }

        /// <summary>
        /// Get a customer based upon the field 'CustomerNumber' in the Customer table.
        /// </summary>
        /// <param name="customerNumber">Corresponds to a value in the CustomerNumber field.</param>
        /// <returns>Customer object or null if not found</returns>
        public override TsCustomer CustomerGetByCustomerNumber(string customerNumber)
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");

            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand
                {
                    Connection = con,
                    CommandType = CommandType.Text,
                    CommandText = @"
                                    SELECT  *
                                    FROM    Envision.Customer
                                    WHERE   CustomerNumber = :pValue"
                })
                {
                    try
                    {
                        cmd.AddInParam("pValue", customerNumber);
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            if (r.Read())
                            {
                                return new TsCustomer
                                {
                                    CustomerId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    CustomerNumber = !r.IsDBNull(1) ? r.GetString(1).TrimStart('0') : string.Empty,
                                    DefaultCardNumber = !r.IsDBNull(2) ? r.GetString(2) : string.Empty,
                                    FirstName = !r.IsDBNull(3) ? r.GetString(3) : string.Empty,
                                    MiddleName = !r.IsDBNull(4) ? r.GetString(4) : string.Empty,
                                    LastName = !r.IsDBNull(5) ? r.GetString(5) : string.Empty,
                                    BirthDate = !r.IsDBNull(6) ? DateTime.FromOADate(r.GetDouble(6)) : DateTime.MinValue,
                                    Sex = !r.IsDBNull(7) ? r.GetString(7) : string.Empty,
                                    PinNumber = !r.IsDBNull(8) ? r.GetInt32(8) : -1,
                                    IsActive = !r.IsDBNull(9) && Formatting.TfStringToBool(r.GetString(9)),
                                    ActiveStartDate = !r.IsDBNull(10) ? DateTime.FromOADate(r.GetDouble(10)) : DateTime.MinValue,
                                    ActiveEndDate = !r.IsDBNull(11) ? DateTime.FromOADate(r.GetDouble(11)) : DateTime.MinValue,
                                    OpenDateTime = !r.IsDBNull(12) ? DateTime.FromOADate(r.GetDouble(12)) : DateTime.MinValue,
                                    LastModifiedDateTime = !r.IsDBNull(13) ? DateTime.FromOADate(r.GetDouble(13)) : DateTime.MinValue,
                                    LastModifiedUsername = !r.IsDBNull(14) ? r.GetString(14) : string.Empty,
                                    DoorExtendedUnlockAllowed = !r.IsDBNull(15) && Formatting.TfStringToBool(r.GetString(15)),
                                    // CustomerNumber skipped
                                    CustomerGuid = !r.IsDBNull(17) ? r.GetString(17) : string.Empty
                                };
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new BoardPlanVerifyFailedException(Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Creates new customer
        /// </summary>
        /// <param name="customer">Customer data</param>
        public override CustomerManagementData CustomerCreate(PostCustomer customer)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerSetXML", CommandType = CommandType.StoredProcedure })
                    {
                        using (var writer = new StringWriter())
                        {
                            new XmlSerializer(typeof(PostCustomer)).Serialize(writer, customer);
                            cmd.Parameters.Add(new OracleParameter { ParameterName = "pUpdateOfCustomerNumber", OracleDbType = OracleDbType.Decimal, Size = 22, Direction = ParameterDirection.Input, Value = null });
                            cmd.Parameters.Add(new OracleParameter { ParameterName = "pXml", OracleDbType = OracleDbType.Clob, Direction = ParameterDirection.Input, Value = writer.ToString() });
                        }

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return CustomerManagementData.CopyFromBase(customer);
        }

        /// <summary>
        /// Updates new customer
        /// </summary>
        /// <param name="customerNumber"></param>
        /// <param name="customer">Customer data</param>
        public override void CustomerUpdate(string customerNumber, PatchCustomer customer)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerSetXML", CommandType = CommandType.StoredProcedure })
                    {
                        using (var writer = new StringWriter())
                        {
                            new XmlSerializer(typeof(PatchCustomerXml)).Serialize(writer, new PatchCustomerXml(customer));
                            decimal custNum;
                            var custNumResult = decimal.TryParse(customerNumber, out custNum);
                            cmd.Parameters.Add(new OracleParameter { ParameterName = "pUpdateOfCustomerNumber", OracleDbType = OracleDbType.Decimal, Size = 22, Direction = ParameterDirection.Input, Value = custNumResult ? (decimal?)custNum : null });
                            cmd.Parameters.Add(new OracleParameter { ParameterName = "pXml", OracleDbType = OracleDbType.Clob, Direction = ParameterDirection.Input, Value = writer.ToString() });
                        }

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc /> 
        public override void CustomerDelete(int customerId)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandType = CommandType.StoredProcedure, CommandText = "CustomerFunctions.CustomerDelete" })
                {
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerId });

                    try
                    {
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                        LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                        throw rex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <inheritdoc /> 
        public override CustomerDeleteResponse CustomerDeleteAllowedGet(Guid customerGuid)
        {
            string[] tables = { "CustBoardDayPeriodUsed", "BoardTransactions", "PendingBoardTrans", "PendingEventTrans" };
            string[] friendlyTableNames = { "Customer period and Day ", "Board Transactions", "Pending Board Transactions", "Pending Event Transactions" };

            var customerId = CustomerGet(customerGuid.ToString("D"))?.CustomerId;
            if (!customerId.HasValue)
                return new CustomerDeleteResponse() { ErrorMessage = "Customer not found." };

            var errors = new List<string>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandType = CommandType.Text })
                    {
                        con.Open();
                        for (var i = 0; i < tables.Length; i++)
                        {
                            cmd.CommandText = $"SELECT count(*) FROM {tables[i]} WHERE Cust_Id = {customerId.Value}";
                            if (int.Parse(cmd.ExecuteScalar().ToString()) > 0)
                                errors.Add(friendlyTableNames[i]);
                        }

                        cmd.CommandText = $"SELECT count(*) FROM DsrLogEntry WHERE ObjectType = 'CustomerId' AND ObjectId = {customerId.Value}";
                        if (int.Parse(cmd.ExecuteScalar().ToString()) > 0)
                            errors.Add("Assa Abloy Transactions");

                        return new CustomerDeleteResponse() { CustomerId = customerId.Value, ErrorMessage = errors.Any() ? $"Customer in use by: \"{string.Join("; ", errors)}\"" : null };
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Get a customers
        /// </summary>
        /// <param name="customerId">Customer's id</param>
        /// <returns>Customers</returns>
        public override List<TsCustomer> CustomersGet(int? customerId = null)
        {
            var list = new List<TsCustomer>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomersGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsCustomer()
                                {
                                    CustomerId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    CustomerNumber = !r.IsDBNull(1) ? r.GetString(1).TrimStart('0') : string.Empty,
                                    DefaultCardNumber = !r.IsDBNull(2) ? r.GetString(2) : string.Empty,
                                    FirstName = !r.IsDBNull(3) ? r.GetString(3) : string.Empty,
                                    MiddleName = !r.IsDBNull(4) ? r.GetString(4) : string.Empty,
                                    LastName = !r.IsDBNull(5) ? r.GetString(5) : string.Empty,
                                    BirthDate = !r.IsDBNull(6) ? DateTime.FromOADate(r.GetDouble(6)) : DateTime.MinValue,
                                    Sex = !r.IsDBNull(7) ? r.GetString(7) : string.Empty,
                                    PinNumber = !r.IsDBNull(8) ? r.GetInt32(8) : -1,
                                    IsActive = !r.IsDBNull(9) && Formatting.TfStringToBool(r.GetString(9)),
                                    ActiveStartDate = !r.IsDBNull(10) ? DateTime.FromOADate(r.GetDouble(10)) : DateTime.MinValue,
                                    ActiveEndDate = !r.IsDBNull(11) ? DateTime.FromOADate(r.GetDouble(11)) : DateTime.MinValue,
                                    OpenDateTime = !r.IsDBNull(12) ? DateTime.FromOADate(r.GetDouble(12)) : DateTime.MinValue,
                                    LastModifiedDateTime = !r.IsDBNull(13) ? DateTime.FromOADate(r.GetDouble(13)) : DateTime.MinValue,
                                    LastModifiedUsername = !r.IsDBNull(14) ? r.GetString(14) : string.Empty,
                                    DoorExtendedUnlockAllowed = !r.IsDBNull(15) && Formatting.TfStringToBool(r.GetString(15))
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <summary>
        /// Get all customers
        /// </summary>
        /// <param name="request">Filter</param>
        /// <returns>Filtered customers</returns>
        public override List<TsCustomer> CustomerGetAll(CustomerGetAllRequest request)
        {
            var list = new List<TsCustomer>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerGetAll", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerNumber", OracleDbType = OracleDbType.Int64, Size = 22, Direction = ParameterDirection.Input, Value = request?.CustomerNumber });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pLastName", OracleDbType = OracleDbType.Varchar2, Size = 30, Direction = ParameterDirection.Input, Value = request?.LastName });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pFirstName", OracleDbType = OracleDbType.Varchar2, Size = 30, Direction = ParameterDirection.Input, Value = request?.FirstName });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pMiddleName", OracleDbType = OracleDbType.Varchar2, Size = 30, Direction = ParameterDirection.Input, Value = request?.MiddleName });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDefaultCardNumber", OracleDbType = OracleDbType.Varchar2, Size = 22, Direction = ParameterDirection.Input, Value = request?.DefaultCardNumber });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pIsActive", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = request?.IsActive != null ? Formatting.BooleanToTf(request.IsActive.Value) : null });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsCustomer()
                                {
                                    CustomerId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    CustomerNumber = !r.IsDBNull(1) ? r.GetString(1).TrimStart('0') : string.Empty,
                                    DefaultCardNumber = !r.IsDBNull(2) ? r.GetString(2) : string.Empty,
                                    FirstName = !r.IsDBNull(3) ? r.GetString(3) : string.Empty,
                                    MiddleName = !r.IsDBNull(4) ? r.GetString(4) : string.Empty,
                                    LastName = !r.IsDBNull(5) ? r.GetString(5) : string.Empty,
                                    BirthDate = !r.IsDBNull(6) ? DateTime.FromOADate(r.GetDouble(6)) : DateTime.MinValue,
                                    Sex = !r.IsDBNull(7) ? r.GetString(7) : string.Empty,
                                    PinNumber = !r.IsDBNull(8) ? r.GetInt32(8) : -1,
                                    IsActive = !r.IsDBNull(9) && Formatting.TfStringToBool(r.GetString(9)),
                                    ActiveStartDate = !r.IsDBNull(10) ? DateTime.FromOADate(r.GetDouble(10)) : DateTime.MinValue,
                                    ActiveEndDate = !r.IsDBNull(11) ? DateTime.FromOADate(r.GetDouble(11)) : DateTime.MinValue,
                                    OpenDateTime = !r.IsDBNull(12) ? DateTime.FromOADate(r.GetDouble(12)) : DateTime.MinValue,
                                    LastModifiedDateTime = !r.IsDBNull(13) ? DateTime.FromOADate(r.GetDouble(13)) : DateTime.MinValue,
                                    LastModifiedUsername = !r.IsDBNull(14) ? r.GetString(14) : string.Empty,
                                    DoorExtendedUnlockAllowed = !r.IsDBNull(15) && Formatting.TfStringToBool(r.GetString(15)),
                                    // CustomerNumber skipped
                                    CustomerGuid = !r.IsDBNull(17) ? r.GetString(17) : string.Empty
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <summary>
        /// Get all customers
        /// </summary>
        /// <param name="request">Filter</param>
        /// <returns>Filtered customers</returns>
        public override CustomerListGetResponse CustomerListGet(CustomerListGetRequest request)
        {
            var result = new CustomerListGetResponse()
            {
                Customers = new List<TsCustomerListInfo>()
            };

            int? count = null;
            string countQuery = null;
            var query = @"SELECT CST.Cust_Id, CST.CustNum, CST.DefaultCardNum, CST.FirstName, CST.MiddleName, CST.LastName, CST.CustomerGuid
                        FROM            Customer CST
                        LEFT JOIN       Card ON CST.DefaultCardNum = Card.CardNum
                        WHERE           CST.CustNum LIKE :pCustomerNumber
                        AND             NVL(CST.LastName, '-1') LIKE :pLastName
                        AND             NVL(CST.FirstName, '-1') LIKE :pFirstName
                        AND             NVL(CST.MiddleName, '-1') LIKE :pMiddleName
                        AND             NVL(CST.DefaultCardNum, '-1') LIKE :pDefaultCardNumber";

            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandType = CommandType.Text })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerNumber", OracleDbType = OracleDbType.Varchar2, Size = 23, Direction = ParameterDirection.Input, Value = $"%{request.CustomerNumber}" });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pLastName", OracleDbType = OracleDbType.Varchar2, Size = 31, Direction = ParameterDirection.Input, Value = $"{request.LastName}%" });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pFirstName", OracleDbType = OracleDbType.Varchar2, Size = 31, Direction = ParameterDirection.Input, Value = $"{request.FirstName}%" });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pMiddleName", OracleDbType = OracleDbType.Varchar2, Size = 31, Direction = ParameterDirection.Input, Value = $"{request.MiddleName}%" });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDefaultCardNumber", OracleDbType = OracleDbType.Varchar2, Size = 23, Direction = ParameterDirection.Input, Value = $"%{request.CardNumber}" });

                        if (request.PageSize.HasValue)
                        {
                            countQuery = $"SELECT count(*) FROM ({query})";
                            query = $"{query} OFFSET {request.Offset} ROWS FETCH NEXT {request.PageSize} ROWS ONLY";
                        }

                        cmd.CommandText = query;
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                result.Customers.Add(new TsCustomerListInfo()
                                {
                                    CustomerId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    CustomerNumber = !r.IsDBNull(1) ? r.GetString(1) : string.Empty,
                                    DefaultCardNumber = !r.IsDBNull(2) ? r.GetString(2) : string.Empty,
                                    FirstName = !r.IsDBNull(3) ? r.GetString(3) : string.Empty,
                                    MiddleName = !r.IsDBNull(4) ? r.GetString(4) : string.Empty,
                                    LastName = !r.IsDBNull(5) ? r.GetString(5) : string.Empty,
                                    CustomerGuid = !r.IsDBNull(6) ? r.GetString(6) : string.Empty
                                });
                            }
                        }

                        if (!string.IsNullOrEmpty(countQuery))
                        {
                            cmd.CommandText = countQuery;
                            count = int.Parse(cmd.ExecuteScalar().ToString());
                        }
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }

            result.TotalCustomerCount = count ?? result.Customers.Count;
            return result;
        }

        /// <summary>
        /// Get customer board used
        /// </summary>
        /// <param name="customerGuid">Customer guid</param>
        /// <param name="boardPlanGuid">Board guid</param>
        /// <returns>Customer board used</returns>
        public override TsCustomerBoardDetailUsed CustomerBoardUsedGet(string customerGuid, string boardPlanGuid)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerBoardUsedGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = customerGuid });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pBoardPlanGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = boardPlanGuid });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            if (r.Read())
                            {
                                return new TsCustomerBoardDetailUsed()
                                {
                                    CustomerId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    LastBoardAccess = !r.IsDBNull(1) ? DateTime.FromOADate(r.GetDouble(1)) : DateTime.MinValue,
                                    WeekUsed = !r.IsDBNull(2) ? r.GetInt32(2) : -1,
                                    MonthUsed = !r.IsDBNull(3) ? r.GetInt32(3) : -1,
                                    SemQtrUsed = !r.IsDBNull(4) ? r.GetInt32(4) : -1,
                                    YearUsed = !r.IsDBNull(5) ? r.GetInt32(5) : -1,
                                    ExtraUsed = !r.IsDBNull(6) ? r.GetInt32(6) : -1,
                                    GuestUsed = !r.IsDBNull(7) ? r.GetInt32(7) : -1,
                                    WeekRolled = !r.IsDBNull(8) ? DateTime.FromOADate(r.GetDouble(8)) : DateTime.MinValue,
                                    MonthRolled = !r.IsDBNull(9) ? DateTime.FromOADate(r.GetDouble(9)) : DateTime.MinValue,
                                    SemQtrRolled = !r.IsDBNull(10) ? DateTime.FromOADate(r.GetDouble(10)) : DateTime.MinValue,
                                    YearRolled = !r.IsDBNull(11) ? DateTime.FromOADate(r.GetDouble(11)) : DateTime.MinValue,
                                    LastHostUpdate = !r.IsDBNull(12) ? DateTime.FromOADate(r.GetDouble(12)) : DateTime.MinValue,
                                    UsedPer1 = !r.IsDBNull(13) ? r.GetInt32(13) : -1,
                                    UsedPer2 = !r.IsDBNull(14) ? r.GetInt32(14) : -1,
                                    UsedPer3 = !r.IsDBNull(15) ? r.GetInt32(15) : -1,
                                    UsedPer4 = !r.IsDBNull(16) ? r.GetInt32(16) : -1,
                                    UsedPer5 = !r.IsDBNull(17) ? r.GetInt32(17) : -1,
                                    UsedPer6 = !r.IsDBNull(18) ? r.GetInt32(18) : -1,
                                    UsedDow1 = !r.IsDBNull(19) ? r.GetInt32(19) : -1,
                                    UsedDow2 = !r.IsDBNull(20) ? r.GetInt32(20) : -1,
                                    UsedDow3 = !r.IsDBNull(21) ? r.GetInt32(21) : -1,
                                    UsedDow4 = !r.IsDBNull(22) ? r.GetInt32(22) : -1,
                                    UsedDow5 = !r.IsDBNull(23) ? r.GetInt32(23) : -1,
                                    UsedDow6 = !r.IsDBNull(24) ? r.GetInt32(24) : -1,
                                    UsedDow7 = !r.IsDBNull(25) ? r.GetInt32(25) : -1,
                                    BoardPlanId = !r.IsDBNull(26) ? r.GetInt32(26) : -1
                                };
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return null;
        }

        /// <summary>
        /// Get all customer board used
        /// </summary>
        /// <param name="request">Filter</param>
        /// <returns>Filtered customer boards</returns>
        public override List<TsCustomerBoardDetailUsed> CustomerBoardUsedGetAll(CustomerBoardUsedGetAllRequest request)
        {
            var list = new List<TsCustomerBoardDetailUsed>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerBoardUsedGetAll", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = request?.CustomerGuid });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pBoardPlanGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = request?.BoardPlanGuid });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsCustomerBoardDetailUsed()
                                {
                                    CustomerId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    LastBoardAccess = !r.IsDBNull(1) ? DateTime.FromOADate(r.GetDouble(1)) : DateTime.MinValue,
                                    WeekUsed = !r.IsDBNull(2) ? r.GetInt32(2) : -1,
                                    MonthUsed = !r.IsDBNull(3) ? r.GetInt32(3) : -1,
                                    SemQtrUsed = !r.IsDBNull(4) ? r.GetInt32(4) : -1,
                                    YearUsed = !r.IsDBNull(5) ? r.GetInt32(5) : -1,
                                    ExtraUsed = !r.IsDBNull(6) ? r.GetInt32(6) : -1,
                                    GuestUsed = !r.IsDBNull(7) ? r.GetInt32(7) : -1,
                                    WeekRolled = !r.IsDBNull(8) ? DateTime.FromOADate(r.GetDouble(8)) : DateTime.MinValue,
                                    MonthRolled = !r.IsDBNull(9) ? DateTime.FromOADate(r.GetDouble(9)) : DateTime.MinValue,
                                    SemQtrRolled = !r.IsDBNull(10) ? DateTime.FromOADate(r.GetDouble(10)) : DateTime.MinValue,
                                    YearRolled = !r.IsDBNull(11) ? DateTime.FromOADate(r.GetDouble(11)) : DateTime.MinValue,
                                    LastHostUpdate = !r.IsDBNull(12) ? DateTime.FromOADate(r.GetDouble(12)) : DateTime.MinValue,
                                    UsedPer1 = !r.IsDBNull(13) ? r.GetInt32(13) : -1,
                                    UsedPer2 = !r.IsDBNull(14) ? r.GetInt32(14) : -1,
                                    UsedPer3 = !r.IsDBNull(15) ? r.GetInt32(15) : -1,
                                    UsedPer4 = !r.IsDBNull(16) ? r.GetInt32(16) : -1,
                                    UsedPer5 = !r.IsDBNull(17) ? r.GetInt32(17) : -1,
                                    UsedPer6 = !r.IsDBNull(18) ? r.GetInt32(18) : -1,
                                    UsedDow1 = !r.IsDBNull(19) ? r.GetInt32(19) : -1,
                                    UsedDow2 = !r.IsDBNull(20) ? r.GetInt32(20) : -1,
                                    UsedDow3 = !r.IsDBNull(21) ? r.GetInt32(21) : -1,
                                    UsedDow4 = !r.IsDBNull(22) ? r.GetInt32(22) : -1,
                                    UsedDow5 = !r.IsDBNull(23) ? r.GetInt32(23) : -1,
                                    UsedDow6 = !r.IsDBNull(24) ? r.GetInt32(24) : -1,
                                    UsedDow7 = !r.IsDBNull(25) ? r.GetInt32(25) : -1,
                                    BoardPlanId = !r.IsDBNull(26) ? r.GetInt32(26) : -1
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <summary>
        /// Get customer board
        /// </summary>
        /// <param name="customerGuid">Customer guid</param>
        /// <param name="boardPlanGuid">Board guid</param>
        /// <returns>Customer board</returns>
        public override TsCustomerBoardDetail CustomerBoardGet(string customerGuid, string boardPlanGuid)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerBoardGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = customerGuid });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pBoardPlanGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = boardPlanGuid });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            if (r.Read())
                            {
                                var board = new TsCustomerBoardDetail()
                                {
                                    CustomerId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    BoardPlanId = !r.IsDBNull(1) ? r.GetInt32(1) : -1,
                                    HomeProfitCenterId = !r.IsDBNull(2) ? r.GetInt32(2) : -1,
                                    IsActive = !r.IsDBNull(3) && Formatting.TfStringToBool(r.GetString(3)),
                                    WeekAllowed = !r.IsDBNull(4) ? r.GetInt32(4) : -1,
                                    MonthAllowed = !r.IsDBNull(5) ? r.GetInt32(5) : -1,
                                    SemesterQuarterAllowed = !r.IsDBNull(6) ? r.GetInt32(6) : -1,
                                    YearAllowed = !r.IsDBNull(7) ? r.GetInt32(7) : -1,
                                    ExtraAllowed = !r.IsDBNull(8) ? r.GetInt32(8) : -1,
                                    GuestAllowed = !r.IsDBNull(9) ? r.GetInt32(9) : -1,
                                    ShowBoardCount = !r.IsDBNull(10) && Formatting.TfStringToBool(r.GetString(10)),
                                    PrintBoardCount = !r.IsDBNull(11) && Formatting.TfStringToBool(r.GetString(11)),
                                    AllowedPeriod1 = !r.IsDBNull(103) ? r.GetInt32(103) : -1,
                                    AllowedPeriod2 = !r.IsDBNull(104) ? r.GetInt32(104) : -1,
                                    AllowedPeriod3 = !r.IsDBNull(105) ? r.GetInt32(105) : -1,
                                    AllowedPeriod4 = !r.IsDBNull(106) ? r.GetInt32(106) : -1,
                                    AllowedPeriod5 = !r.IsDBNull(107) ? r.GetInt32(107) : -1,
                                    AllowedPeriod6 = !r.IsDBNull(108) ? r.GetInt32(108) : -1,
                                    TransferAvailable = !r.IsDBNull(109) ? r.GetInt32(109) : -1,
                                    EndDate = !r.IsDBNull(110) ? DateTime.FromOADate(r.GetDouble(110)) : DateTime.MinValue,
                                    StartDate = !r.IsDBNull(111) ? DateTime.FromOADate(r.GetDouble(111)) : DateTime.MinValue,
                                    Priority = !r.IsDBNull(112) ? r.GetInt32(112) : -1,
                                    CustomerGuid = customerGuid,
                                    BoardPlanGuid = boardPlanGuid
                                };

                                int index = 12;
                                var boardType = board.GetType();
                                for (int i = 1; i <= 7; i++)
                                {
                                    var prop = boardType.GetProperty($"AllowedDayOfWeek{i}");
                                    prop?.SetValue(board, !r.IsDBNull(95 + i) ? r.GetInt32(95 + i) : -1);

                                    for (int j = 1; j <= 6; j++)
                                    {
                                        prop = boardType.GetProperty($"RegularAllowedDayOfWeek{i}Period{j}");
                                        prop?.SetValue(board, !r.IsDBNull(index) ? r.GetInt32(index) : -1);
                                        prop = boardType.GetProperty($"GuestAllowedDayOfWeek{i}Period{j}");
                                        prop?.SetValue(board, !r.IsDBNull(index + 1) ? r.GetInt32(index + 1) : -1);

                                        index += 2;
                                    }
                                }

                                return board;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return null;
        }

        /// <summary>
        /// Get customer boards
        /// </summary>
        /// <param name="request">Filter</param>
        /// <returns>Filtered customer boards</returns>
        public override List<TsCustomerBoardDetail> CustomerBoardGetAll(CustomerBoardGetAllRequest request)
        {
            var list = new List<TsCustomerBoardDetail>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerBoardGetAll", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = request?.CustomerGuid });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pBoardPlanGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = request?.BoardPlanGuid });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var board = new TsCustomerBoardDetail()
                                {
                                    CustomerId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    BoardPlanId = !r.IsDBNull(1) ? r.GetInt32(1) : -1,
                                    HomeProfitCenterId = !r.IsDBNull(2) ? r.GetInt32(2) : -1,
                                    IsActive = !r.IsDBNull(3) && Formatting.TfStringToBool(r.GetString(3)),
                                    WeekAllowed = !r.IsDBNull(4) ? r.GetInt32(4) : -1,
                                    MonthAllowed = !r.IsDBNull(5) ? r.GetInt32(5) : -1,
                                    SemesterQuarterAllowed = !r.IsDBNull(6) ? r.GetInt32(6) : -1,
                                    YearAllowed = !r.IsDBNull(7) ? r.GetInt32(7) : -1,
                                    ExtraAllowed = !r.IsDBNull(8) ? r.GetInt32(8) : -1,
                                    GuestAllowed = !r.IsDBNull(9) ? r.GetInt32(9) : -1,
                                    ShowBoardCount = !r.IsDBNull(10) && Formatting.TfStringToBool(r.GetString(10)),
                                    PrintBoardCount = !r.IsDBNull(11) && Formatting.TfStringToBool(r.GetString(11)),
                                    AllowedPeriod1 = !r.IsDBNull(103) ? r.GetInt32(103) : -1,
                                    AllowedPeriod2 = !r.IsDBNull(104) ? r.GetInt32(104) : -1,
                                    AllowedPeriod3 = !r.IsDBNull(105) ? r.GetInt32(105) : -1,
                                    AllowedPeriod4 = !r.IsDBNull(106) ? r.GetInt32(106) : -1,
                                    AllowedPeriod5 = !r.IsDBNull(107) ? r.GetInt32(107) : -1,
                                    AllowedPeriod6 = !r.IsDBNull(108) ? r.GetInt32(108) : -1,
                                    TransferAvailable = !r.IsDBNull(109) ? r.GetInt32(109) : -1,
                                    EndDate = !r.IsDBNull(110) ? DateTime.FromOADate(r.GetDouble(110)) : DateTime.MinValue,
                                    StartDate = !r.IsDBNull(111) ? DateTime.FromOADate(r.GetDouble(111)) : DateTime.MinValue,
                                    Priority = !r.IsDBNull(112) ? r.GetInt32(112) : -1,
                                    BoardPlanGuid = !r.IsDBNull(113) ? r.GetString(113) : string.Empty,
                                    CustomerNumber = !r.IsDBNull(114) ? r.GetInt64(114) : -1,
                                    CustomerGuid = !r.IsDBNull(115) ? r.GetString(115) : string.Empty
                                };

                                int index = 12;
                                var boardType = board.GetType();
                                for (int i = 1; i <= 7; i++)
                                {
                                    var prop = boardType.GetProperty($"AllowedDayOfWeek{i}");
                                    prop?.SetValue(board, !r.IsDBNull(95 + i) ? r.GetInt32(95 + i) : -1);

                                    for (int j = 1; j <= 6; j++)
                                    {
                                        prop = boardType.GetProperty($"RegularAllowedDayOfWeek{i}Period{j}");
                                        prop?.SetValue(board, !r.IsDBNull(index) ? r.GetInt32(index) : -1);
                                        prop = boardType.GetProperty($"GuestAllowedDayOfWeek{i}Period{j}");
                                        prop?.SetValue(board, !r.IsDBNull(index + 1) ? r.GetInt32(index + 1) : -1);

                                        index += 2;
                                    }
                                }

                                list.Add(board);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <summary>
        /// Get customer cards
        /// </summary>
        /// <param name="customerGuid">Customer guid</param>
        /// <returns>Customer cards</returns>
        public override List<TsCard> CustomerCardsGet(string customerGuid)
        {
            var list = new List<TsCard>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerCardsGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = customerGuid });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsCard()
                                {
                                    CardNum = !r.IsDBNull(0) ? r.GetString(0) : string.Empty,
                                    Issue_Number = !r.IsDBNull(1) ? r.GetString(1) : string.Empty,
                                    Cust_Id = !r.IsDBNull(2) ? r.GetInt32(2) : -1,
                                    Card_Type = !r.IsDBNull(3) ? r.GetInt32(3) : -1,
                                    Card_Status = !r.IsDBNull(4) ? r.GetInt32(4) : -1,
                                    Card_Status_Text = !r.IsDBNull(5) ? r.GetString(5) : string.Empty,
                                    Card_Status_Datetime = !r.IsDBNull(6) ? DateTime.FromOADate(r.GetDouble(6)) : DateTime.MinValue,
                                    Lost_Flag = !r.IsDBNull(7) ? r.GetString(7) : string.Empty,
                                    Card_Idm = !r.IsDBNull(8) ? r.GetString(8) : string.Empty,
                                    DomainId = !r.IsDBNull(9) ? r.GetString(9) : string.Empty,
                                    IssuerId = !r.IsDBNull(10) ? r.GetInt32(10) : -1,
                                    CreatedDateTime = !r.IsDBNull(11) ? r.GetDateTime(11) : DateTime.MinValue,
                                    ModifiedDateTime = !r.IsDBNull(12) ? r.GetDateTime(12) : DateTime.MinValue,
                                    CardNumber = !r.IsDBNull(13) ? r.GetDecimal(13) : -1,
                                    CardNumberLength = !r.IsDBNull(14) ? r.GetInt32(14) : -1
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <summary>
        /// Get customer cards
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <returns>Customer cards</returns>
        public override List<TsCard> CustomerCardsGet(int customerId)
        {
            var list = new List<TsCard>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerCardsGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsCard()
                                {
                                    CardNum = !r.IsDBNull(0) ? r.GetString(0) : string.Empty,
                                    Issue_Number = !r.IsDBNull(1) ? r.GetString(1) : null,
                                    Cust_Id = !r.IsDBNull(2) ? r.GetInt32(2) : -1,
                                    Card_Type = !r.IsDBNull(3) ? r.GetInt32(3) : -1,
                                    Card_Status = !r.IsDBNull(4) ? r.GetInt32(4) : -1,
                                    Card_Status_Text = !r.IsDBNull(5) ? r.GetString(5) : null,
                                    Card_Status_Datetime = !r.IsDBNull(6) ? DateTime.FromOADate(r.GetDouble(6)) : DateTime.MinValue,
                                    Lost_Flag = !r.IsDBNull(7) ? r.GetString(7) : null,
                                    Card_Idm = !r.IsDBNull(8) ? r.GetString(8) : null,
                                    DomainId = !r.IsDBNull(9) ? r.GetString(9) : null,
                                    IssuerId = !r.IsDBNull(10) ? r.GetInt32(10) : -1,
                                    CreatedDateTime = !r.IsDBNull(11) ? r.GetDateTime(11) : DateTime.MinValue,
                                    ModifiedDateTime = !r.IsDBNull(12) ? r.GetDateTime(12) : DateTime.MinValue,
                                    CardNumber = !r.IsDBNull(13) ? r.GetDecimal(13) : -1,
                                    CardNumberLength = !r.IsDBNull(14) ? r.GetInt32(14) : -1
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <summary>
        /// Get customer photo
        /// </summary>
        /// <param name="customerGuid">Customer guid</param>
        /// <returns>Customer photo</returns>
        public override TsCustomerPhoto CustomerPhotoGet(string customerGuid)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerByGuidPhotoGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = customerGuid });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pFirstName", OracleDbType = OracleDbType.Varchar2, Size = 30, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pLastName", OracleDbType = OracleDbType.Varchar2, Size = 30, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pActiveEndDate", OracleDbType = OracleDbType.TimeStamp, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPhoto", OracleDbType = OracleDbType.Blob, Direction = ParameterDirection.Output });
                        con.Open();
                        cmd.ExecuteNonQuery();

                        var activeEndDate = (OracleTimeStamp)cmd.Parameters[3].Value;
                        var photo = cmd.Parameters[4].Value as OracleBlob;
                        var customerPhoto = new TsCustomerPhoto()
                        {
                            FirstName = cmd.Parameters[1].Value?.ToString(),
                            LastName = cmd.Parameters[2].Value?.ToString(),
                            ActiveEndDate = activeEndDate.IsNull ? DateTime.MinValue : activeEndDate.Value,
                            Photo = photo?.IsNull ?? true ? null : photo.Value
                        };

                        return customerPhoto;
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Get customer emails
        /// </summary>
        /// <returns>Customer emails</returns>
        public override List<TsCustomerEmail> CustomerEmailsGet(int customerId)
        {
            var list = new List<TsCustomerEmail>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerEmailsGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsCustomerEmail()
                                {
                                    CustomerId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    EmailAddress = !r.IsDBNull(1) ? r.GetString(1) : string.Empty,
                                    DefaultDisplayOverride = !r.IsDBNull(2) && Formatting.TfStringToBool(r.GetString(2)),
                                    CustomerEmailTypeId = !r.IsDBNull(3) ? r.GetInt32(3) : -1,
                                    CustomerEmailType = new TsCustomerEmailType()
                                    {
                                        Id = !r.IsDBNull(3) ? r.GetInt32(3) : -1,
                                        Name = !r.IsDBNull(4) ? r.GetString(4) : string.Empty,
                                        RequireUniqueEmail = !r.IsDBNull(5) && Formatting.TfStringToBool(r.GetString(5)),
                                        Default = !r.IsDBNull(6) && Formatting.TfStringToBool(r.GetString(6))
                                    }
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <summary>
        /// Sets customer email address
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="email">Email</param>
        public override void CustomerEmailSet(int customerId, CustomerEmailAddressBase email)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerEmailSet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerEmailTypeID", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = email.Id });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pEmailAddress", OracleDbType = OracleDbType.Varchar2, Size = 255, Direction = ParameterDirection.Input, Value = email.EmailAddress });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDefaultDisplayOverride", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = email.DefaultOverride });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pReplaceExisting", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = Formatting.BooleanToTf(true) });

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }

                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Get list of customer email types
        /// </summary>
        /// <param name="customerEmailTypeId">Id filter</param>
        /// <returns>Customer email types</returns>
        public override List<TsCustomerEmailType> CustomerEmailTypeGet(int? customerEmailTypeId = null)
        {
            var list = new List<TsCustomerEmailType>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerEmailTypeGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerEmailTypeId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerEmailTypeId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsCustomerEmailType()
                                {
                                    Id = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    Name = !r.IsDBNull(1) ? r.GetString(1) : string.Empty,
                                    RequireUniqueEmail = !r.IsDBNull(2) && Formatting.TfStringToBool(r.GetString(2)),
                                    Default = !r.IsDBNull(3) && Formatting.TfStringToBool(r.GetString(3))
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <summary>
        /// This method will return a list of records for the CustomerDefFieldDef objects in the system
        /// </summary>
        /// <param name="customerDefinedFieldId">Id filter</param>
        /// <returns>List of CustomerDefFieldDef</returns>
        public override List<TsCustomer_Def_Field_Def> CustomerDefFieldDefGet(int? customerDefinedFieldId = null)
        {
            var list = new List<TsCustomer_Def_Field_Def>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerDefFieldDefGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerDefFieldDefId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerDefinedFieldId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerDefFieldDefs", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsCustomer_Def_Field_Def()
                                {
                                    Customer_Def_Field_Def_Id = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    Title = !r.IsDBNull(1) ? r.GetString(1) : string.Empty,
                                    FieldType = !r.IsDBNull(2) ? r.GetInt32(2) : -1,
                                    MaxLength = !r.IsDBNull(3) ? r.GetInt32(3) : -1,
                                    Store_Per_Transaction = !r.IsDBNull(4) ? r.GetString(4) : string.Empty,
                                    Ui_Display_Top = !r.IsDBNull(5) ? r.GetInt32(5) : -1,
                                    Ui_Display_Left = !r.IsDBNull(6) ? r.GetInt32(6) : -1,
                                    Ui_Display_Height = !r.IsDBNull(7) ? r.GetInt32(7) : -1,
                                    Ui_Display_Width = !r.IsDBNull(8) ? r.GetInt32(8) : -1,
                                    IsReportEnabled = !r.IsDBNull(9) ? r.GetString(9) : string.Empty
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the TsCustomerDefFieldValue objects in the system
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <returns>List of TsCustomerDefFieldValue</returns>
        public override List<TsCustomerDefFieldValue> CustomerDefFieldValueGet(int customerId)
        {
            var list = new List<TsCustomerDefFieldValue>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerDefFieldValueGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsCustomerDefFieldValue()
                                {
                                    DefinedField = new TsCustomer_Def_Field_Def()
                                    {
                                        Customer_Def_Field_Def_Id = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                        Title = !r.IsDBNull(1) ? r.GetString(1) : string.Empty,
                                        FieldType = !r.IsDBNull(2) ? r.GetInt32(2) : -1,
                                        MaxLength = !r.IsDBNull(3) ? r.GetInt32(3) : -1,
                                        Store_Per_Transaction = !r.IsDBNull(4) ? r.GetString(4) : string.Empty,
                                        Ui_Display_Top = !r.IsDBNull(5) ? r.GetInt32(5) : -1,
                                        Ui_Display_Left = !r.IsDBNull(6) ? r.GetInt32(6) : -1,
                                        Ui_Display_Height = !r.IsDBNull(7) ? r.GetInt32(7) : -1,
                                        Ui_Display_Width = !r.IsDBNull(8) ? r.GetInt32(8) : -1,
                                        IsReportEnabled = !r.IsDBNull(9) ? r.GetString(9) : string.Empty
                                    },
                                    CustomerId = !r.IsDBNull(10) ? r.GetInt32(10) : -1,
                                    FieldValue = !r.IsDBNull(11) ? r.GetString(11) : string.Empty
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <summary>
        /// Get Customer Defined Group Item Ids
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <returns>Customer Defined Group Item Ids</returns>
        public override List<int> CustomerDefGroupItemIdsGet(int customerId)
        {
            var list = new List<int>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerDefGroupItemIdsGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustId", OracleDbType = OracleDbType.Decimal, Size = 10, Direction = ParameterDirection.Input, Value = customerId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                if (!r.IsDBNull(0))
                                    list.Add(r.GetInt32(0));
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the CustomerDefGrpDef objects in the system
        /// </summary>
        /// <param name="customerDefGroupDefId">Id filter</param>
        /// <returns>List of CustomerDefGrpDef</returns>
        public override List<TsCustomerDefGroupDef> CustomerDefGrpDefGet(int? customerDefGroupDefId = null)
        {
            var list = new List<TsCustomerDefGroupDef>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerDefGroupDefGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerDefGroupDefId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerDefGroupDefId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerDefGroupDefs", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            var dynamicList = new List<dynamic>();
                            while (r.Read())
                            {
                                dynamicList.Add(new
                                {
                                    CustomerDefGrpDefId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    Name = !r.IsDBNull(1) ? r.GetString(1) : string.Empty,
                                    StorePerTransaction = !r.IsDBNull(2) && Formatting.TfStringToBool(r.GetString(2)),
                                    UiDisplayTop = !r.IsDBNull(3) ? r.GetInt32(3) : -1,
                                    UiDisplayLeft = !r.IsDBNull(4) ? r.GetInt32(4) : -1,
                                    UiDisplayHeight = !r.IsDBNull(5) ? r.GetInt32(5) : -1,
                                    UiDisplayWidth = !r.IsDBNull(6) ? r.GetInt32(6) : -1,
                                    CustomerDefGrpDefItemId = !r.IsDBNull(7) ? r.GetInt32(7) : -1,
                                    ItemName = !r.IsDBNull(8) ? r.GetString(8) : string.Empty,
                                    ItemPosition = !r.IsDBNull(9) ? r.GetInt32(9) : -1
                                });
                            }

                            if (dynamicList.Any())
                            {
                                list = dynamicList.GroupBy(x => x.CustomerDefGrpDefId).Select(x => new TsCustomerDefGroupDef()
                                {
                                    Id = x.Key,
                                    Name = x.First().Name,
                                    StorePerTransaction = x.First().StorePerTransaction,
                                    UiDisplayTop = x.First().UiDisplayTop,
                                    UiDisplayLeft = x.First().UiDisplayLeft,
                                    UiDisplayHeight = x.First().UiDisplayHeight,
                                    UiDisplayWidth = x.First().UiDisplayWidth,
                                    GroupItems = x.Select(y => new TsCustomerDefGroupDefItem()
                                    {
                                        Id = y.CustomerDefGrpDefItemId,
                                        CustomerDefGroupDefId = x.Key,
                                        Name = y.ItemName,
                                        Position = y.ItemPosition
                                    }).ToList()
                                }).ToList();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the TsCustomerDefGroupDef objects in the system
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <returns>List of TsCustomerDefGroupDef</returns>
        public override List<TsCustomerDefGroupDef> CustomerDefGrpValueGet(int customerId)
        {
            var list = new List<TsCustomerDefGroupDef>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerDefGroupValueGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            var dynamicList = new List<dynamic>();
                            while (r.Read())
                            {
                                dynamicList.Add(new
                                {
                                    CustomerDefGrpDefId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    Name = !r.IsDBNull(1) ? r.GetString(1) : string.Empty,
                                    StorePerTransaction = !r.IsDBNull(2) && Formatting.TfStringToBool(r.GetString(2)),
                                    UiDisplayTop = !r.IsDBNull(3) ? r.GetInt32(3) : -1,
                                    UiDisplayLeft = !r.IsDBNull(4) ? r.GetInt32(4) : -1,
                                    UiDisplayHeight = !r.IsDBNull(5) ? r.GetInt32(5) : -1,
                                    UiDisplayWidth = !r.IsDBNull(6) ? r.GetInt32(6) : -1,
                                    CustomerDefGrpDefItemId = !r.IsDBNull(7) ? r.GetInt32(7) : -1,
                                    ItemName = !r.IsDBNull(8) ? r.GetString(8) : null,
                                    ItemPosition = !r.IsDBNull(9) ? r.GetInt32(9) : -1
                                });
                            }

                            if (dynamicList.Any())
                            {
                                list = dynamicList.GroupBy(x => x.CustomerDefGrpDefId).Select(x => new TsCustomerDefGroupDef()
                                {
                                    Id = x.Key,
                                    Name = x.First().Name,
                                    StorePerTransaction = x.First().StorePerTransaction,
                                    UiDisplayTop = x.First().UiDisplayTop,
                                    UiDisplayLeft = x.First().UiDisplayLeft,
                                    UiDisplayHeight = x.First().UiDisplayHeight,
                                    UiDisplayWidth = x.First().UiDisplayWidth,
                                    GroupItems = x.Select(y => new TsCustomerDefGroupDefItem()
                                    {
                                        Id = y.CustomerDefGrpDefItemId,
                                        CustomerDefGroupDefId = x.Key,
                                        Name = y.ItemName,
                                        Position = y.ItemPosition
                                    }).Where(y=>y.Name != null).ToList()
                                }).ToList();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <summary>
        /// Get list of customer addresses
        /// </summary>
        /// <param name="customerId">Customer id</param>
        /// <returns>Customer addresses</returns>
        public override List<TsAddress> CustomerAddressesGet(int customerId)
        {
            var list = new List<TsAddress>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerAddressesGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsAddress()
                                {
                                    Id = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    Name = !r.IsDBNull(1) ? r.GetString(1) : null,
                                    Relationship = !r.IsDBNull(2) ? r.GetString(2) : null,
                                    Street1 = !r.IsDBNull(3) ? r.GetString(3) : null,
                                    Street2 = !r.IsDBNull(4) ? r.GetString(4) : null,
                                    City = !r.IsDBNull(5) ? r.GetString(5) : null,
                                    State = !r.IsDBNull(6) ? r.GetString(6) : null,
                                    ZipCode = !r.IsDBNull(7) ? r.GetString(7) : null,
                                    Country = !r.IsDBNull(8) ? r.GetString(8) : null,
                                    Phone1 = !r.IsDBNull(9) ? r.GetString(9) : null,
                                    Phone2 = !r.IsDBNull(10) ? r.GetString(10) : null,
                                    Notes = !r.IsDBNull(11) ? r.GetString(11) : null,
                                    AddressType = new TsAddressType()
                                    {
                                        Id = !r.IsDBNull(12) ? r.GetInt32(12) : -1,
                                        Name = !r.IsDBNull(13) ? r.GetString(13) : string.Empty,
                                        DeriveName = !r.IsDBNull(14) && Formatting.TfStringToBool(r.GetString(14)),
                                        DeriveRelationship = !r.IsDBNull(15) && Formatting.TfStringToBool(r.GetString(15))
                                    }
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <summary>
        /// Get customer event plans
        /// </summary>
        /// <param name="customerNumber">Customer number</param>
        /// <returns>List of event plans</returns>
        public override List<TsCustomerEventPlan> CustomerEventPlansGet(string customerNumber)
        {
            var list = new List<TsCustomerEventPlan>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerEventPlansGet", CommandType = CommandType.StoredProcedure })
                    {
                        decimal custNum;
                        var custNumResult = decimal.TryParse(customerNumber, out custNum);
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerNumber", OracleDbType = OracleDbType.Decimal, Size = 22, Direction = ParameterDirection.Input, Value = custNumResult ? (decimal?)custNum : null });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsCustomerEventPlan()
                                {
                                    CustomerId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    EventPlanEnabled = !r.IsDBNull(1) && Formatting.TfStringToBool(r.GetString(1)),
                                    EventPlanId = !r.IsDBNull(2) ? r.GetInt32(2) : -1
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <summary>
        /// Deletes customer event plan
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        public override void CustomerEventPlanDelete(int customerId)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerEventPlanDelete", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCust_Id", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerId });

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Creates or updates customer event plan
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="eventPlan">Event plan</param>
        /// <returns>Created or update event plan</returns>
        public override void CustomerEventPlanSet(int customerId, CustomerEventPlan eventPlan)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerEventPlanSet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCust_Id", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pEventPlan_Enabled", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = Formatting.BooleanToTf(eventPlan.Active) });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pEventPlan_Id", OracleDbType = OracleDbType.Int32, Size = 6, Direction = ParameterDirection.Input, Value = eventPlan.Id });

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Get customer event plan overrides
        /// </summary>
        /// <param name="customerNumber">Customer number</param>
        /// <returns>Customer event plan overrides</returns>
        public override List<CustomerEventPlanOverride> CustomerEventPlanOverridesGet(string customerNumber)
        {
            var list = new List<CustomerEventPlanOverride>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerEventPlanOverridesGet", CommandType = CommandType.StoredProcedure })
                    {
                        decimal custNum;
                        var custNumResult = decimal.TryParse(customerNumber, out custNum);
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerNumber", OracleDbType = OracleDbType.Decimal, Size = 22, Direction = ParameterDirection.Input, Value = custNumResult ? (decimal?)custNum : null });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new CustomerEventPlanOverride()
                                {
                                    Id = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    // CustomerId skipped
                                    RegularAccesses = !r.IsDBNull(2) ? (int?)r.GetInt32(2) : null,
                                    GuestAccesses = !r.IsDBNull(3) ? (int?)r.GetInt32(3) : null,
                                    ResetDateTime = !r.IsDBNull(4) ? (DateTime?)DateTime.FromOADate(r.GetDouble(4)) : null,
                                    EventOverrideDurationLimitType = Formatting.EventLimitTypeStrToEnum(!r.IsDBNull(5) ? r.GetString(5) : null)
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <summary>
        /// Deletes customer event plan overrides
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="eventId">Event Id</param>
        public override void CustomerEventPlanOverrideDelete(int customerId, int? eventId = null)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerEventAllowedDelete", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCust_Id", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pEvent_Id", OracleDbType = OracleDbType.Int32, Size = 6, Direction = ParameterDirection.Input, Value = eventId });

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Creates customer event plan override
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="eventPlanOverride">Event plan override</param>
        /// <returns>Customer event plan override</returns>
        public override void CustomerEventPlanOverrideCreate(int customerId, CustomerEventPlanOverride eventPlanOverride)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerEventAllowedCreate", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCust_Id", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pEvent_Id", OracleDbType = OracleDbType.Int32, Size = 6, Direction = ParameterDirection.Input, Value = eventPlanOverride.Id });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pRegularAllowed", OracleDbType = OracleDbType.Int32, Size = 8, Direction = ParameterDirection.Input, Value = eventPlanOverride.RegularAccesses });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pGuestAllowed", OracleDbType = OracleDbType.Int32, Size = 8, Direction = ParameterDirection.Input, Value = eventPlanOverride.GuestAccesses });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pResetDate", OracleDbType = OracleDbType.Double, Size = 10, Direction = ParameterDirection.Input, Value = eventPlanOverride.ResetDateTime?.ToOADate() });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pLimitBy", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = Formatting.EventLimitTypeEnumToStr(eventPlanOverride.EventOverrideDurationLimitType) });

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Updates customer event plan override
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="eventId">Event Id</param>
        /// <param name="eventPlanOverride">Event plan override</param>
        /// <returns>Customer event plan override</returns>
        public override void CustomerEventPlanOverrideUpdate(int customerId, int eventId, PutCustomerEventPlanOverride eventPlanOverride)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerEventAllowedUpdate", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCust_Id", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pEvent_Id", OracleDbType = OracleDbType.Int32, Size = 6, Direction = ParameterDirection.Input, Value = eventId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pRegularAllowed", OracleDbType = OracleDbType.Int32, Size = 8, Direction = ParameterDirection.Input, Value = eventPlanOverride.RegularAccesses });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pGuestAllowed", OracleDbType = OracleDbType.Int32, Size = 8, Direction = ParameterDirection.Input, Value = eventPlanOverride.GuestAccesses });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pResetDate", OracleDbType = OracleDbType.Double, Size = 10, Direction = ParameterDirection.Input, Value = eventPlanOverride.ResetDateTime?.ToOADate() });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pLimitBy", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = Formatting.EventLimitTypeEnumToStr(eventPlanOverride.EventOverrideDurationLimitType) });

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Get customer defaults
        /// </summary>
        /// <returns>List of customer defaults</returns>
        public override List<TsCustomerDefault> CustomerDefaultsGet()
        {
            var list = new List<TsCustomerDefault>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerDefaultsGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsCustomerDefault()
                                {
                                    Id = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    Active = !r.IsDBNull(1) && Formatting.TfStringToBool(r.GetString(1)),
                                    ActiveStartDate = !r.IsDBNull(2) ? DateTime.FromOADate(r.GetDouble(2)) : DateTime.MinValue,
                                    ActiveEndDate = !r.IsDBNull(3) ? DateTime.FromOADate(r.GetDouble(3)) : DateTime.MinValue,
                                    AddCardNumberEqualToNewCustomerNumber = !r.IsDBNull(4) && Formatting.TfStringToBool(r.GetString(4)),
                                    DefaultIssueNumber = !r.IsDBNull(5) ? r.GetString(5) : string.Empty,
                                    DefaultStoredValueAccountTypes = !r.IsDBNull(6) ? r.GetString(6).Split(',').Select(int.Parse).ToList() : new List<int>()
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <summary>
        /// Get customer board plans
        /// </summary>
        /// <param name="customerNumber">Customer number</param>
        /// <returns>Customer board plans</returns>
        public override List<CustomerManagementBoardPlan> CustomerBoardsGet(string customerNumber)
        {
            var list = new List<CustomerManagementBoardPlan>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerBoardsGet", CommandType = CommandType.StoredProcedure })
                    {
                        decimal custNum;
                        var custNumResult = decimal.TryParse(customerNumber, out custNum);
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerNumber", OracleDbType = OracleDbType.Decimal, Size = 22, Direction = ParameterDirection.Input, Value = custNumResult ? (decimal?)custNum : null });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new CustomerManagementBoardPlan()
                                {
                                    Id = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    Priority = !r.IsDBNull(1) ? r.GetInt32(1) : -1,
                                    Active = !r.IsDBNull(2) && Formatting.TfStringToBool(r.GetString(2)),
                                    OverridePlanStartDate = !r.IsDBNull(3) ? (DateTime?)DateTime.FromOADate(r.GetDouble(3)) : null,
                                    OverridePlanStopDate = !r.IsDBNull(4) ? (DateTime?)DateTime.FromOADate(r.GetDouble(4)) : null,
                                    PosOptionPrintCounts = !r.IsDBNull(5) && Formatting.TfStringToBool(r.GetString(5)),
                                    PosOptionShowCounts = !r.IsDBNull(6) && Formatting.TfStringToBool(r.GetString(6))
                                });
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <summary>
        /// Adds new boardplan to customer
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="bp">Board plan</param>
        /// <returns>Customer board plan</returns>
        public override void CustomerBoardPlanCreate(int customerId, PostCustomerBoardPlan bp)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerBoardInsert", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pBoardPlanId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = bp.BoardPlanId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPriority", OracleDbType = OracleDbType.Int16, Size = 4, Direction = ParameterDirection.Input, Value = bp.Priority });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pBoardActive", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = bp.Active });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pActiveFrom", OracleDbType = OracleDbType.Double, Size = 10, Direction = ParameterDirection.Input, Value = bp.OverridePlanStartDate?.ToOADate() });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pActiveTo", OracleDbType = OracleDbType.Double, Size = 10, Direction = ParameterDirection.Input, Value = bp.OverridePlanStopDate?.ToOADate() });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pShowBoardCount", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = Formatting.BooleanToTf(bp.PosOptionShowCounts) });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPrintBoardCount", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = Formatting.BooleanToTf(bp.PosOptionPrintCounts) });

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }

                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Adds new boardplan to customer
        /// </summary>
        /// <param name="customerNumber">Customer number</param>
        /// <param name="boardPlanId"></param>
        /// <param name="bp">Board plan</param>
        /// <returns>Customer board plan</returns>
        public override void CustomerBoardPlanUpdate(string customerNumber, int boardPlanId, PatchCustomerBoardPlan bp)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerBoardUpdate", CommandType = CommandType.StoredProcedure })
                    {
                        decimal custNum;
                        var custNumResult = decimal.TryParse(customerNumber, out custNum);
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerNumber", OracleDbType = OracleDbType.Decimal, Size = 22, Direction = ParameterDirection.Input, Value = custNumResult ? (decimal?)custNum : null });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pBoardPlanId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = boardPlanId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pNewBoardPlanId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = bp.NewBoardPlanId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPriority", OracleDbType = OracleDbType.Int16, Size = 4, Direction = ParameterDirection.Input, Value = bp.Priority });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPrioritySet", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = bp.PriorityIsSet });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pBoardActive", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = bp.Active });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pBoardActiveSet", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = bp.ActiveIsSet });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pActiveFrom", OracleDbType = OracleDbType.Double, Size = 10, Direction = ParameterDirection.Input, Value = bp.OverridePlanStartDate?.ToOADate() });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pActiveFromSet", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = bp.OverridePlanStartDateIsSet });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pActiveTo", OracleDbType = OracleDbType.Double, Size = 10, Direction = ParameterDirection.Input, Value = bp.OverridePlanStopDate?.ToOADate() });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pActiveToSet", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = bp.OverridePlanStopDateIsSet });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pShowBoardCount", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = bp.PosOptionShowCounts.HasValue ? Formatting.BooleanToTf(bp.PosOptionShowCounts.Value) : null });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pShowBoardCountSet", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = bp.PosOptionShowCountsIsSet });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPrintBoardCount", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = bp.PosOptionPrintCounts.HasValue ? Formatting.BooleanToTf(bp.PosOptionPrintCounts.Value) : null });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPrintBoardCountSet", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = bp.PosOptionPrintCountsIsSet });

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }

                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Deletes boardplans from customer
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="boardPlanId">Board plan Id (optional)</param>
        public override void CustomerBoardPlanDelete(int customerId, int? boardPlanId = null)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerBoardDelete", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pBoardPlanId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = boardPlanId });

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }

                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Get customer door access plan Ids
        /// </summary>
        /// <param name="customerNumber">Customer number</param>
        /// <returns>Door access plan Ids</returns>
        public override List<int> CustomerDoorAccessPlansGet(string customerNumber)
        {
            var list = new List<int>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustDaAccessPlansGet", CommandType = CommandType.StoredProcedure })
                    {
                        decimal custNum;
                        var custNumResult = decimal.TryParse(customerNumber, out custNum);
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerNumber", OracleDbType = OracleDbType.Decimal, Size = 22, Direction = ParameterDirection.Input, Value = custNumResult ? (decimal?)custNum : null });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                if (!r.IsDBNull(0))
                                    list.Add(r.GetInt32(0));
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <summary>
        /// Assigns door access plan to customer
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="doorAccessPlanId">Door access plan Id</param>
        public override void CustomerDoorAccessPlanCreate(int customerId, int doorAccessPlanId)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustDaAccessPlanSet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pAccessPlanId", OracleDbType = OracleDbType.Int32, Size = 6, Direction = ParameterDirection.Input, Value = doorAccessPlanId });

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }

                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Removes door access plan from customer
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="doorAccessPlanId">Door access plan Id</param>
        public override void CustomerDoorAccessPlanDelete(int customerId, int? doorAccessPlanId = null)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustDaAccessPlanDelete", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pAccessPlanId", OracleDbType = OracleDbType.Int32, Size = 6, Direction = ParameterDirection.Input, Value = doorAccessPlanId });

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }

                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Get customer stored value accounts
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="svAccountNumber">Stored value account number</param>
        /// <returns>Customer stored value accounts</returns>
        public override List<CustomerSvAccount> CustomerStoredValueAccountsGet(int customerId, Int64? svAccountNumber = null)
        {
            var list = new List<CustomerSvAccount>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerSvAccountGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerId", OracleDbType = OracleDbType.Decimal, Size = 22, Direction = ParameterDirection.Input, Value = customerId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pSvAccountNumber", OracleDbType = OracleDbType.Int64, Size = 12, Direction = ParameterDirection.Input, Value = svAccountNumber });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new CustomerSvAccount()
                                {
                                    Id = !r.IsDBNull(0) ? r.GetInt64(0) : -1,
                                    StoredValueAccountTypeId = !r.IsDBNull(1) ? r.GetInt32(1) : -1,
                                    AccountNumber = !r.IsDBNull(2) ? r.GetInt64(2) : -1,
                                    StoredValueAccountAssociationType = !r.IsDBNull(3) ? (StoredValueAccountAssociationType)r.GetInt16(3) : StoredValueAccountAssociationType.Unknown,
                                    Name = !r.IsDBNull(4) ? r.GetString(4) : string.Empty,
                                    BalanceForward = !r.IsDBNull(5) ? r.GetInt64(5) / 1000d : -1,
                                    Balance = !r.IsDBNull(6) ? r.GetInt64(6) / 1000d : -1,
                                    CreditLimit = !r.IsDBNull(7) ? r.GetInt64(7) / 1000d : -1,
                                    StoredValueAccountShareType = !r.IsDBNull(8) ? (StoredValueAccountShareType)r.GetInt16(8) : StoredValueAccountShareType.Unknown,
                                    TypeName = !r.IsDBNull(9) ? r.GetString(9) : string.Empty
                                });
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <summary>
        /// Creates new customer stored value account
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="svAccount">SV account</param>
        /// <returns>Customer stored value account number</returns>
        public override Int64 CustomerStoredValueAccountCreate(int customerId, PostCustomerSvAccount svAccount)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerSvAccountCreate", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pSv_Account_Type_Id", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = svAccount.StoredValueAccountTypeId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pName", OracleDbType = OracleDbType.Varchar2, Size = 30, Direction = ParameterDirection.Input, Value = svAccount.Name });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pBalance_Forward", OracleDbType = OracleDbType.Int64, Size = 12, Direction = ParameterDirection.Input, Value = svAccount.BalanceForward * 1000 });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCredit_Limit", OracleDbType = OracleDbType.Int64, Size = 12, Direction = ParameterDirection.Input, Value = svAccount.CreditLimit * 1000 });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pIndividual_Or_Joint", OracleDbType = OracleDbType.Int16, Size = 4, Direction = ParameterDirection.Input, Value = (int)svAccount.StoredValueAccountAssociationType });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pSvAccountNumber", OracleDbType = OracleDbType.Int64, Size = 12, Direction = ParameterDirection.Output });

                        con.Open();
                        cmd.ExecuteNonQuery();

                        Int64 result;
                        return Int64.TryParse(cmd.Parameters["pSvAccountNumber"].Value?.ToString(), out result) ? result : -1;
                    }

                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Updates existing customer stored value account
        /// </summary>
        /// <param name="svAccount">SV account</param>
        public override void CustomerStoredValueAccountUpdate(CustomerSvAccount svAccount)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerSvAccountUpdate", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pSvAccountId", OracleDbType = OracleDbType.Int64, Size = 15, Direction = ParameterDirection.Input, Value = svAccount.Id });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pName", OracleDbType = OracleDbType.Varchar2, Size = 30, Direction = ParameterDirection.Input, Value = svAccount.Name });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCredit_Limit", OracleDbType = OracleDbType.Int64, Size = 12, Direction = ParameterDirection.Input, Value = svAccount.CreditLimit * 1000 });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pIndividual_Or_Joint", OracleDbType = OracleDbType.Int16, Size = 4, Direction = ParameterDirection.Input, Value = (int)svAccount.StoredValueAccountAssociationType });

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }

                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Deletes customer stored value account
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="svAccountTypeId">Account Type Id</param>
        public override void CustomerStoredValueAccountDelete(int customerId, int svAccountTypeId)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerSvAccountDelete", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCust_Id", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pSv_Account_Type_Id", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = svAccountTypeId });

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }

                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override List<int> CustomerPosDisplayRulesGet(int customerId)
        {
            var list = new List<int>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerPosDisplayRulesGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                if (!r.IsDBNull(0))
                                    list.Add(r.GetInt32(0));
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <inheritdoc />
        public override CustomerPosMessage CustomerPosMessageGet(int customerId)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerPosMessageGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            if (!r.Read())
                                return null;

                            return new CustomerPosMessage()
                            {
                                CustomerId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                MessageId = !r.IsDBNull(1) ? r.GetInt32(1) : -1,
                                StartDate = !r.IsDBNull(2) ? (DateTime?)DateTime.FromOADate(r.GetDouble(2)) : null,
                                EndDate = !r.IsDBNull(3) ? (DateTime?)DateTime.FromOADate(r.GetDouble(3)) : null,
                                MaxDisplayCount = !r.IsDBNull(4) ? r.GetInt32(4) : -1,
                                MessageText = !r.IsDBNull(5) ? r.GetString(5) : null
                            };
                        }
                    }

                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override CustomerPlans CustomerPlansGet(int customerId)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerPlansGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pBoardPlansCount", OracleDbType = OracleDbType.Int32, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDoorAccessPlansCount", OracleDbType = OracleDbType.Int32, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDoorOverridesCount", OracleDbType = OracleDbType.Int32, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pReuseDelayCount", OracleDbType = OracleDbType.Int32, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pBoardExclusionsCount", OracleDbType = OracleDbType.Int32, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pEventPlan", OracleDbType = OracleDbType.Varchar2, Size = 20, Direction = ParameterDirection.Output });
                        con.Open();
                        cmd.ExecuteNonQuery();

                        var eventPlan = cmd.Parameters["pEventPlan"].Value.ToString();
                        return new CustomerPlans()
                        {
                            EventPlan = eventPlan == "null" ? null : eventPlan,
                            BoardPlansCount = int.Parse(cmd.Parameters["pBoardPlansCount"].Value.ToString()),
                            DoorAccessPlansCount = int.Parse(cmd.Parameters["pDoorAccessPlansCount"].Value.ToString()),
                            DoorOverridesCount = int.Parse(cmd.Parameters["pDoorOverridesCount"].Value.ToString()),
                            ReuseDelayCount = int.Parse(cmd.Parameters["pReuseDelayCount"].Value.ToString()),
                            BoardExclusionsCount = int.Parse(cmd.Parameters["pBoardExclusionsCount"].Value.ToString())
                        };
                    }

                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override List<TsCustomerBoardPlan> CustomerBoardPlansGet(int customerId)
        {
            var list = new List<TsCustomerBoardPlan>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerBoardPlansGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsCustomerBoardPlan()
                                {
                                    BoardPlanId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    WeekUsed = !r.IsDBNull(1) ? r.GetInt32(1) : -1,
                                    MonthUsed = !r.IsDBNull(2) ? r.GetInt32(2) : -1,
                                    SemesterUsed = !r.IsDBNull(3) ? r.GetInt32(3) : -1,
                                    YearUsed = !r.IsDBNull(4) ? r.GetInt32(4) : -1,
                                    BoardPlanName = !r.IsDBNull(5) ? r.GetString(5) : null,
                                    WeekAllowed = !r.IsDBNull(6) && r.GetInt32(6) != -2 ? (int?)r.GetInt32(6) : null,
                                    MonthAllowed = !r.IsDBNull(7) && r.GetInt32(7) != -2 ? (int?)r.GetInt32(7) : null,
                                    SemesterAllowed = !r.IsDBNull(8) && r.GetInt32(8) != -2 ? (int?)r.GetInt32(8) : null,
                                    YearAllowed = !r.IsDBNull(9) && r.GetInt32(9) != -2 ? (int?)r.GetInt32(9) : null,
                                    ActiveFrom = !r.IsDBNull(10) ? (DateTime?)DateTime.FromOADate(r.GetDouble(10)) : null,
                                    ActiveTo = !r.IsDBNull(11) ? (DateTime?)DateTime.FromOADate(r.GetDouble(11)) : null,
                                    IsActive = !r.IsDBNull(12) && Formatting.TfStringToBool(r.GetString(12)),
                                    Priority = !r.IsDBNull(13) ? r.GetInt32(13) : -1,
                                    IsAssigned = !r.IsDBNull(14) && Formatting.TfStringToBool(r.GetString(14)),
                                    IsRemoved = !r.IsDBNull(15) && Formatting.TfStringToBool(r.GetString(15))
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <inheritdoc />
        public override List<int> CustomerBoardPlanIdsGet(int customerId)
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
            var boardList = new List<int>();

            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand
                {
                    Connection = con,
                    CommandType = CommandType.Text,
                    CommandText = @"SELECT CB.BoardPlan_Id FROM CustomerBoard CB WHERE Cust_Id = :pValue"
                })
                {
                    try
                    {
                        cmd.AddInParam("pValue", customerId);
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                if (!r.IsDBNull(0))
                                {
                                    boardList.Add(r.GetInt32(0));
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new BoardPlanVerifyFailedException(Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return boardList;
        }

        /// <inheritdoc />
        public override void CustomerBoardPlansSet(Guid customerGuid, CustomerBoardPlansPostRequest request)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerBoardPlansSet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = customerGuid.ToString("D") });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pXml", OracleDbType = OracleDbType.Clob, Direction = ParameterDirection.Input, Value = Xml.Serialize(request) });

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override void CustomerBoardExclusionsSet(int customerId, List<int> transactionNumbers, bool exclusionEnabled)
        {
            // Nothing to set
            if (transactionNumbers == null || !transactionNumbers.Any())
                return;

            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");

                    string query = $@"
                        UPDATE  BoardTransactions
                        SET     IsExclusionEnabled = {(exclusionEnabled ? 1 : 0)}
                        WHERE   Cust_Id = {customerId}
                        AND     TranNumber IN ({string.Join(",", transactionNumbers)})
                        AND     ActualDateTime >= Udf_Functions.NowBusinessStartDateTime";

                    using (var cmd = new OracleCommand { Connection = con, CommandText = query, CommandType = CommandType.Text })
                    {
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override void CustomerAccessPlansSet(Guid customerGuid, List<int> daPlanIds)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerAccessPlansSet", CommandType = CommandType.StoredProcedure })
                    {
                        var daPlanIdsStr = daPlanIds != null ? string.Join(",", daPlanIds) : string.Empty;
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = customerGuid.ToString("D") });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPlanIdList", OracleDbType = OracleDbType.Varchar2, Size = daPlanIdsStr.Length, Direction = ParameterDirection.Input, Value = daPlanIdsStr });
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override List<TsCustomerTransaction> CustomerTransactionHistoryGet(int customerId, DateTime? startDate, DateTime? endDate)
        {
            var list = new List<TsCustomerTransaction>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerTransactionHistoryGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pStartDate", OracleDbType = OracleDbType.Double, Direction = ParameterDirection.Input, Value = startDate?.ToOADate() });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pEndDate", OracleDbType = OracleDbType.Double, Direction = ParameterDirection.Input, Value = endDate?.ToOADate() });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsCustomerTransaction()
                                {
                                    TransactionNumber = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    TransactionKey = !r.IsDBNull(1) ? r.GetString(1) : null,
                                    TransactionViewer = !r.IsDBNull(2) ? r.GetString(2) : null,
                                    IsDenied = !r.IsDBNull(3) && Formatting.TfStringToBool(r.GetString(3)),
                                    TransactionDateTime = !r.IsDBNull(4) ? DateTime.FromOADate(r.GetDouble(4)) : DateTime.MinValue,
                                    ProfitCenterName = !r.IsDBNull(5) ? r.GetString(5) : null,
                                    PosName = !r.IsDBNull(6) ? r.GetString(6) : null,
                                    TransactionType = !r.IsDBNull(7) ? r.GetString(7) : null,
                                    PlanAccount = !r.IsDBNull(8) ? r.GetString(8) : null,
                                    Amount = !r.IsDBNull(9) ? r.GetDecimal(9) : -1,
                                    IsOnline = !r.IsDBNull(10) && Formatting.TfStringToBool(r.GetString(10))
                                });
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <inheritdoc />
        public override CustomerDoorAccess CustomerDoorAccessGet(int customerId, int doorId, DateTime? date)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustomerDoorAccessGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDoorId", OracleDbType = OracleDbType.Int32, Size = 6, Direction = ParameterDirection.Input, Value = doorId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDate", OracleDbType = OracleDbType.Int32, Direction = ParameterDirection.Input, Value = date?.ToOADate() });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pBinPermDayScheduleDow1", OracleDbType = OracleDbType.Varchar2, Size = 1440, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pBinPermDayScheduleDow2", OracleDbType = OracleDbType.Varchar2, Size = 1440, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pBinPermDayScheduleDow3", OracleDbType = OracleDbType.Varchar2, Size = 1440, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pBinPermDayScheduleDow4", OracleDbType = OracleDbType.Varchar2, Size = 1440, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pBinPermDayScheduleDow5", OracleDbType = OracleDbType.Varchar2, Size = 1440, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pBinPermDayScheduleDow6", OracleDbType = OracleDbType.Varchar2, Size = 1440, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pBinPermDayScheduleDow7", OracleDbType = OracleDbType.Varchar2, Size = 1440, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDoorStateSchedule", OracleDbType = OracleDbType.Varchar2, Size = 32000, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDoorStateDayScheduleDow1", OracleDbType = OracleDbType.Varchar2, Size = 32000, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDoorStateDayScheduleDow2", OracleDbType = OracleDbType.Varchar2, Size = 32000, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDoorStateDayScheduleDow3", OracleDbType = OracleDbType.Varchar2, Size = 32000, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDoorStateDayScheduleDow4", OracleDbType = OracleDbType.Varchar2, Size = 32000, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDoorStateDayScheduleDow5", OracleDbType = OracleDbType.Varchar2, Size = 32000, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDoorStateDayScheduleDow6", OracleDbType = OracleDbType.Varchar2, Size = 32000, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDoorStateDayScheduleDow7", OracleDbType = OracleDbType.Varchar2, Size = 32000, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pErrorCode", OracleDbType = OracleDbType.Int32, Direction = ParameterDirection.Output });
                        con.Open();
                        cmd.ExecuteNonQuery();

                        int errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);
                        return new CustomerDoorAccess
                        {
                            PermissionDayScheduleDow1 = CustomerDoorAccess.PermissionsFromBinaryString(cmd.Parameters["pBinPermDayScheduleDow1"].Value?.ToString()),
                            PermissionDayScheduleDow2 = CustomerDoorAccess.PermissionsFromBinaryString(cmd.Parameters["pBinPermDayScheduleDow2"].Value?.ToString()),
                            PermissionDayScheduleDow3 = CustomerDoorAccess.PermissionsFromBinaryString(cmd.Parameters["pBinPermDayScheduleDow3"].Value?.ToString()),
                            PermissionDayScheduleDow4 = CustomerDoorAccess.PermissionsFromBinaryString(cmd.Parameters["pBinPermDayScheduleDow4"].Value?.ToString()),
                            PermissionDayScheduleDow5 = CustomerDoorAccess.PermissionsFromBinaryString(cmd.Parameters["pBinPermDayScheduleDow5"].Value?.ToString()),
                            PermissionDayScheduleDow6 = CustomerDoorAccess.PermissionsFromBinaryString(cmd.Parameters["pBinPermDayScheduleDow6"].Value?.ToString()),
                            PermissionDayScheduleDow7 = CustomerDoorAccess.PermissionsFromBinaryString(cmd.Parameters["pBinPermDayScheduleDow7"].Value?.ToString()),
                            DoorStateSchedule = CustomerDoorAccess.StatesFromString(cmd.Parameters["pDoorStateSchedule"]?.Value.ToString()),
                            DoorStateDayScheduleDow1 = CustomerDoorAccess.StatesFromString(cmd.Parameters["pDoorStateDayScheduleDow1"].Value?.ToString()),
                            DoorStateDayScheduleDow2 = CustomerDoorAccess.StatesFromString(cmd.Parameters["pDoorStateDayScheduleDow2"].Value?.ToString()),
                            DoorStateDayScheduleDow3 = CustomerDoorAccess.StatesFromString(cmd.Parameters["pDoorStateDayScheduleDow3"].Value?.ToString()),
                            DoorStateDayScheduleDow4 = CustomerDoorAccess.StatesFromString(cmd.Parameters["pDoorStateDayScheduleDow4"].Value?.ToString()),
                            DoorStateDayScheduleDow5 = CustomerDoorAccess.StatesFromString(cmd.Parameters["pDoorStateDayScheduleDow5"].Value?.ToString()),
                            DoorStateDayScheduleDow6 = CustomerDoorAccess.StatesFromString(cmd.Parameters["pDoorStateDayScheduleDow6"].Value?.ToString()),
                            DoorStateDayScheduleDow7 = CustomerDoorAccess.StatesFromString(cmd.Parameters["pDoorStateDayScheduleDow7"].Value?.ToString()),
                            ErrorCode = errorCode
                        };
                    }

                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override void CustomerDoorOverridesSet(Guid customerGuid, DoorOverridesSetRequest request)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CustomerFunctions.CustDoorOverridesSet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = customerGuid.ToString("D") });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pXml", OracleDbType = OracleDbType.Clob, Direction = ParameterDirection.Input, Value = Xml.Serialize(request) });

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        #endregion

        #region Private Functions

        /// <summary>
        /// Get a base 64 string representation of the customer image.
        /// </summary>
        /// <param name="bytes">bytes containing the customer image.</param>
        /// <param name="makeThumbnail">Scale the image to 100x200</param>
        /// <returns></returns>
        private string _getCustomerImage(byte[] bytes, bool makeThumbnail)
        {
            if (!makeThumbnail)
            {
                return ImageTool.Instance.ImageAsBase64String(ImageTool.Instance.ImageFromBytes(bytes));
            }

            var thumbnail = ImageTool.Instance.StandardThumbnailFromBytes(bytes);
            return ImageTool.Instance.ImageAsBase64String(thumbnail);
        }

        #endregion
    }
}
