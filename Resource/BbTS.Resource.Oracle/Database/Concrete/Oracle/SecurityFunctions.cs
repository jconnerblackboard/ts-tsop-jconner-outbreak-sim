﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using BbTS.Core;
using BbTS.Core.Conversion;
using BbTS.Core.Logging.Tracing;
using BbTS.Domain.Models.Definitions.Door;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.General;
using BbTS.Domain.Models.Security.DoorApiKeys;
using BbTS.Domain.Models.Security.Doors;
using BbTS.Domain.Models.System;
using BbTS.Domain.Models.System.Security;
using BbTS.Domain.Models.System.Security.Oauth;
using BbTS.Monitoring.Logging;
using Oracle.ManagedDataAccess.Client;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        /// <summary>
        /// Check with the database if it is ok to bypass a password check in validate
        /// </summary>
        /// <returns></returns>
        public override bool BypassUserPasswordCheck()
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand
                    {
                        Connection = con,
                        CommandType = CommandType.Text,
                        CommandText = @"
                        WITH UsersCount AS (
                          SELECT  COUNT(*) AS UserCount
                          FROM    Users
                        ),
                        PasswordHashIsEmpty AS (
                              SELECT  COUNT(*) PasswordHashIsEmptyCount
                              FROM    Users
                              WHERE   Password_Hash = '00'  
                        )
                        SELECT    UserCount,
                                  PasswordHashIsEmptyCount
                        FROM      UsersCount,
                                  PasswordHashIsEmpty"
                    })
                    {
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var userCount = !r.IsDBNull(0) ? r.GetInt32(0) : -1;
                                var passwordHashIsEmpty = !r.IsDBNull(1) ? r.GetInt32(1) : -1;

                                return userCount == 1 && passwordHashIsEmpty == 1;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            throw new ResourceLayerException(string.Empty, @"Query for BypassUserPasswordCheck has failed for an unknown reason.");
        }

        /// <summary>
        /// Get a user's password history from Oracle
        /// </summary>
        /// <returns></returns>
        public override List<PasswordHistoryItem> PasswordHistoryGet(string username)
        {
            var history = new List<PasswordHistoryItem>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "UserMaintenance.PasswordHistoryGet", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pUserName", OracleDbType.Varchar2, username.Length, username, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pList", OracleDbType.RefCursor, 0, ParameterDirection.Output));
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var id = !r.IsDBNull(0) ? r.GetInt32(0) : -1;
                                var storedUsername = !r.IsDBNull(1) ? r.GetString(1) : null;
                                var datetime = !r.IsDBNull(2) ? DateTime.FromOADate(r.GetDouble(2)) : DateTime.MinValue;
                                var passwordBytes = !r.IsDBNull(3) ? r.GetValue(3) as byte[] : null;
                                var passwordHash = passwordBytes == null ? "" : Formatting.ByteArrayToString(passwordBytes);
                                var salt = !r.IsDBNull(4) ? r.GetString(4) : null;
                                var hashType = !r.IsDBNull(5) ? r.GetInt32(5) : -1;
                                var iterationCount = !r.IsDBNull(6) ? r.GetInt32(6) : -1;
                                
                                if (id == -1 || hashType == -1 || iterationCount == -1 || 
                                    storedUsername == null || passwordBytes == null || passwordHash == null || salt == null)
                                {
                                    throw new ResourceLayerException(string.Empty, "One or more password values were incorrect");
                                }

                                history.Add(
                                    new PasswordHistoryItem
                                    {
                                        DateTime = datetime,
                                        HashType = hashType,
                                        Id = id,
                                        IterationCount = iterationCount,
                                        PasswordHash = passwordHash,
                                        PasswordHashSalt = salt,
                                        Username = storedUsername
                                    });
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            return history;
        }

        /// <summary>
        /// Add a user password entry into an oracle database
        /// </summary>
        /// <param name="username">username attached to the history entry</param>
        /// <param name="dateTime">password expiration date time</param>
        /// <param name="hash">hashed password as a string of interpreted bytes (ie. string representation of bytes).  See BbTS.Security.CommonFunctions.cs:ByteArrayToString.)</param>
        /// <param name="salt">string representation of the salt</param>
        /// <param name="hashType">hash type 1=Sha1 (should not be used), 2=PBK, 3=DHE</param>
        /// <param name="iterationCount">iteration count for hashing function</param>
        public override void PasswordHistorySet(string username, DateTime dateTime, byte[] hash, byte[] salt, int hashType, int iterationCount)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "UserMaintenance.PasswordHistorySet", CommandType = CommandType.StoredProcedure })
                    {
                        var hashString = Formatting.ByteArrayToString(hash);
                        var saltString = Formatting.ByteArrayToCharAsString(salt);

                        cmd.Parameters.Add(new OracleParameter("pUserName", OracleDbType.Varchar2, 15, username, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pPasswordHash", OracleDbType.Varchar2, hashString.Length, hashString, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pPasswordHashSalt", OracleDbType.Varchar2, saltString.Length, saltString, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pHashType", OracleDbType.Int32, 0, hashType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIterationCount", OracleDbType.Int32, 0, iterationCount, ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Set a user password in an oracle database
        /// </summary>
        /// <param name="username">username attached to the history entry</param>
        /// <param name="dateTime">password expiration date time</param>
        /// <param name="hash">hashed password as a string of interpreted bytes (ie. string representation of bytes).  See BbTS.Security.CommonFunctions.cs:ByteArrayToString.)</param>
        /// <param name="salt">string representation of the salt</param>
        /// <param name="hashType">hash type 1=Sha1 (should not be used), 2=PBK, 3=DHE</param>
        /// <param name="iterationCount">iteration count for hashing function</param>
        public override void PasswordSet(string username, DateTime dateTime, byte[] hash, byte[] salt, int hashType, int iterationCount)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "UserMaintenance.PasswordSet", CommandType = CommandType.StoredProcedure })
                    {
                        var hashString = Formatting.ByteArrayToString(hash);
                        var saltString = Formatting.ByteArrayToCharAsString(salt);

                        cmd.Parameters.Add(new OracleParameter("pUserName", OracleDbType.Varchar2, 15, username, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pPasswordHash", OracleDbType.Varchar2, hashString.Length, hashString, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pPasswordHashSalt", OracleDbType.Varchar2, saltString.Length, saltString, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pHashType", OracleDbType.Int32, 0, hashType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIterationCount", OracleDbType.Int32, 0, iterationCount, ParameterDirection.Input));
                        
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Process a failed user login against the Transact system
        /// </summary>
        /// <param name="userName">User name</param>
        /// <param name="isSuperUser">Has elevated privileges</param>
        /// <param name="isAdmin">Is an admin.</param>
        /// <param name="program">Calling application.</param>
        /// <param name="workstationId">Id of the workstation.</param>
        public override void ProcessFailedLogin(string userName, bool isSuperUser, bool isAdmin, string program, int workstationId)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "Packsecurity.ProcessFailedLogin", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        var isSuperUserString = isSuperUser ? "T" : "F";
                        var isAdminString = isAdmin ? "T" : "F";

                        cmd.Parameters.Add(new OracleParameter("vUserName", OracleDbType.Varchar2, 15, userName, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("vIsSuperUser", OracleDbType.Varchar2, 1, isSuperUserString, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("vIsAdmin", OracleDbType.Varchar2, 1, isAdminString, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("vProgram", OracleDbType.Varchar2, 255, program, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("nWorkStationId", OracleDbType.Int32, 10, workstationId, ParameterDirection.Input));

                        cmd.Parameters.Add(new OracleParameter("nErrorCode", OracleDbType.Int32, 10, ParameterDirection.Output));
                        cmd.Parameters.Add(new OracleParameter("vDeniedText", OracleDbType.Varchar2, 75, ParameterDirection.Output));

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Process a successful user login against the Transact system.
        /// </summary>
        /// <param name="userName">User name</param>
        /// <param name="isSuperUser">Has elevated privileges</param>
        /// <param name="isAdmin">Is an admin.</param>
        /// <param name="program">Calling application.</param>
        /// <param name="workstationId">Id of the workstation.</param>
        public override void ProcessSuccessfulLogin(string userName, bool isSuperUser, bool isAdmin, string program, int workstationId)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "Packsecurity.ProcessSuccessfulLogin", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        var isSuperUserString = isSuperUser ? "T" : "F";
                        var isAdminString = isAdmin ? "T" : "F";

                        cmd.Parameters.Add(new OracleParameter("vUserName", OracleDbType.Varchar2, 15, userName, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("vIsSuperUser", OracleDbType.Varchar2, 1, isSuperUserString, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("vIsAdmin", OracleDbType.Varchar2, 1, isAdminString, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("vProgram", OracleDbType.Varchar2, 255, program, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("nWorkStationId", OracleDbType.Int32, 10, workstationId, ParameterDirection.Input));

                        cmd.Parameters.Add(new OracleParameter("nErrorCode", OracleDbType.Int32, 10, ParameterDirection.Output));
                        cmd.Parameters.Add(new OracleParameter("vDeniedText", OracleDbType.Varchar2, 75, ParameterDirection.Output));

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Resource layer function for setting the session token.
        /// </summary>
        /// <param name="token">The token to set.</param>
        public override void SessionTokenSet(SessionToken token)
        {
            Guard.IsNotNull(token, "token");
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");

            using (var con = new OracleConnection(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "AuthenticationFunctions.SessionTokenSet", CommandType = CommandType.StoredProcedure })
                {
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pUserName", OracleDbType = OracleDbType.Varchar2, Size = 15, Direction = ParameterDirection.Input, Value = token.Username });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pClient", OracleDbType = OracleDbType.Varchar2, Size = 32, Direction = ParameterDirection.Input, Value = token.ClientId });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pToken", OracleDbType = OracleDbType.Varchar2, Size = 32, Direction = ParameterDirection.Input, Value = token.TokenId });

                    try
                    {
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Fetch the user login session token from the provided token id
        /// </summary>
        /// <param name="tokenId">Id of the token</param>
        /// <returns>SessionToken object matching the id</returns>
        public override SessionToken SessionTokenGet(string tokenId)
        {
            using (var con = new OracleConnection(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "AuthenticationFunctions.SessionTokenGet", CommandType = CommandType.StoredProcedure })
                {

                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pToken", OracleDbType = OracleDbType.Varchar2, Size = 32, Direction = ParameterDirection.Input, Value = tokenId });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pUserName", OracleDbType = OracleDbType.Varchar2, Size = 15, Direction = ParameterDirection.Output });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pClient", OracleDbType = OracleDbType.Varchar2, Size = 32, Direction = ParameterDirection.Output });

                    try
                    {
                        con.Open();
                        cmd.ExecuteNonQuery();

                        var token = new SessionToken
                        {
                            TokenId = tokenId,
                            ClientId = cmd.Parameters["pClient"].Value.ToString(),
                            Username = cmd.Parameters["pUserName"].Value.ToString()
                        };

                        return token;

                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Get the user accounts rules from an oracle database
        /// </summary>
        /// <returns>user accounts rules as stored in an oracle database</returns>
        public override UserAccountRules UserAccountRulesGet()
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");

            UserAccountRules returnValue = null;
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "AuthenticationFunctions.UserAccountRulesGet", CommandType = CommandType.StoredProcedure })
                {
                    cmd.Parameters.Add(new OracleParameter("pList", OracleDbType.RefCursor, ParameterDirection.Output));
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                returnValue = new UserAccountRules
                                {
                                    UserAccountRuleId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    PasswordLifeTimeDays = !r.IsDBNull(1) ? r.GetInt32(1) : -1,
                                    PasswordMaxAttempt = !r.IsDBNull(2) ? r.GetInt32(2) : -1,
                                    PasswordHistoryCount = !r.IsDBNull(3) ? r.GetInt32(3) : -1,
                                    PasswordLengthMinimum = !r.IsDBNull(4) ? r.GetInt32(4) : -1,
                                    PasswordExpireWarningDays = !r.IsDBNull(5) ? r.GetInt32(5) : -1,
                                    LockoutDurationMinutes = !r.IsDBNull(6) ? r.GetInt32(6) : -1,
                                    SessionIdleTimeoutMinutes = !r.IsDBNull(7) ? r.GetInt32(7) : -1,
                                    InactiveAccountDisableDays = !r.IsDBNull(8) ? r.GetInt32(8) : -1,
                                    InactiveAccountDisableDaysAllowOverride = !r.IsDBNull(9) && Formatting.TfStringToBool(r.GetString(9))
                                };
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            return returnValue;
        }

        /// <summary>
        /// Set user account rules
        /// </summary>
        /// <param name="rules">UserAccountRules object</param>
        public override void UserAccountRulesSet(UserAccountRules rules)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "UserMaintenance.UserAccountRuleSet", CommandType = CommandType.StoredProcedure })
                    {
                        var inactiveDaysAllowOverride = rules.InactiveAccountDisableDaysAllowOverride ? "T" : "F";

                        cmd.Parameters.Add(new OracleParameter("pUserAccountRuleId", OracleDbType.Int32, 10, rules.UserAccountRuleId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pPasswordLifeTimeDays", OracleDbType.Int32, 4, rules.PasswordLifeTimeDays, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pPasswordMaxAttempt", OracleDbType.Int32, 4, rules.PasswordMaxAttempt, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pPasswordHistoryCount", OracleDbType.Int32, 4, rules.PasswordHistoryCount, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pPasswordLengthMinimum", OracleDbType.Int32, 4, rules.PasswordLengthMinimum, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pPasswordExpireWarningDays", OracleDbType.Int32, 4, rules.PasswordExpireWarningDays, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pLockoutDurationMinutes", OracleDbType.Int32, 4, rules.LockoutDurationMinutes, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pSessionIdleTimeoutMinutes", OracleDbType.Int32, 4, rules.SessionIdleTimeoutMinutes, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pInactiveAccountDisableDays", OracleDbType.Int32, 4, rules.InactiveAccountDisableDays, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pInactiveAccountDdAllowOvr", OracleDbType.Varchar2, 1, inactiveDaysAllowOverride, ParameterDirection.Input));


                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Gets the application credential by consumer key from the transact api stores. 
        /// </summary>
        /// <param name="consumerKey">The consumer key.</param>
        /// <returns>The transact api application credential from the database resource layer.</returns>
        [Trace(AttributeExclude = true, AttributePriority = 1)]
        public override GetApplicationCredentialByConsumerKeyResponse GetTransactApiApplicationCredential(Guid consumerKey)
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandType = CommandType.Text,
                    CommandText = @"SELECT Secret FROM authentication WHERE KEY = :pValue"
                })
                {
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pValue", OracleDbType = OracleDbType.Varchar2, Size = 36, Value = consumerKey.ToString("D"), Direction = ParameterDirection.Input });
                    con.Open();

                    using (var r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            var secret = !r.IsDBNull(0) ? r.GetString(0) : null;
                            if (secret != null)
                            {
                                var timeToLive = ControlParameterValueGet("SessionAgeSecondsMax");

                                return new GetApplicationCredentialByConsumerKeyResponse
                                {
                                    ApplicationCredential = new ApplicationCredential
                                    {
                                        ConsumerKey = consumerKey,
                                        ConsumerSecret = secret,
                                        TimeToLive = new TimeSpan(0, 0, int.Parse(timeToLive))
                                    }
                                };
                            }
                        }
                    }
                }
            }
            return null;
        }

        #region Doors

        /// <inheritdoc />
        public override List<DoorEventLog> DoorStatesGet(int? doorId = null)
        {
            var list = new List<DoorEventLog>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "SecurityFunctions.DoorStatusGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pDoorId", OracleDbType.Int32, 6, doorId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pList", OracleDbType.RefCursor, ParameterDirection.Output));
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new DoorEventLog()
                                {
                                    DoorId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    LogId = !r.IsDBNull(1) ? r.GetInt64(1) : -1,
                                    ComputerId = !r.IsDBNull(2) ? (int?)r.GetInt32(2) : null,
                                    ActualDateTime = !r.IsDBNull(3) ? DateTime.FromOADate(r.GetDouble(3)) : DateTime.MinValue,
                                    PostDateTime = !r.IsDBNull(4) ? DateTime.FromOADate(r.GetDouble(4)) : DateTime.MinValue,
                                    DoorControlMode = (DoorControlMode)(!r.IsDBNull(5) ? r.GetInt32(5) : 0),
                                    IsOnline = !r.IsDBNull(6) && Formatting.TfStringToBool(r.GetString(6)),
                                    IsExitCycle = !r.IsDBNull(7) && Formatting.TfStringToBool(r.GetString(7)),
                                    IsUnlocked = !r.IsDBNull(8) && Formatting.TfStringToBool(r.GetString(8)),
                                    IsOpen = !r.IsDBNull(9) && Formatting.TfStringToBool(r.GetString(9)),
                                    IsHeld = !r.IsDBNull(10) && Formatting.TfStringToBool(r.GetString(10)),
                                    IsForced = !r.IsDBNull(11) && Formatting.TfStringToBool(r.GetString(11)),
                                    IsTampered = !r.IsDBNull(12) && Formatting.TfStringToBool(r.GetString(12)),
                                    RequiresPin = !r.IsDBNull(13) && Formatting.TfStringToBool(r.GetString(13)),
                                    DoorControlCommLost = !r.IsDBNull(14) && Formatting.TfStringToBool(r.GetString(14)),
                                    Tb1AuxCsTamper = !r.IsDBNull(15) && Formatting.TfStringToBool(r.GetString(15)),
                                    Tb2AuxCsTamper = !r.IsDBNull(16) && Formatting.TfStringToBool(r.GetString(16)),
                                    Serial1CsCommLost = !r.IsDBNull(17) && Formatting.TfStringToBool(r.GetString(17)),
                                    Serial2CsCommLost = !r.IsDBNull(18) && Formatting.TfStringToBool(r.GetString(18)),
                                    DoorControlAcPowerFail = !r.IsDBNull(19) && Formatting.TfStringToBool(r.GetString(19)),
                                    DoorControlBatteryLow = !r.IsDBNull(20) && Formatting.TfStringToBool(r.GetString(20)),
                                    OverrideStatus = (DoorOverrideStatus)(!r.IsDBNull(21) ? r.GetInt32(21) : 0),
                                    MasterControllerId = !r.IsDBNull(22) ? (int?)r.GetInt32(22) : null,
                                    McTransactionSequenceNumber = !r.IsDBNull(23) ? r.GetInt64(23) : -1
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <inheritdoc />
        public override List<DoorAaEventLog> DoorAaStatesGet(int? doorId = null)
        {
            var list = new List<DoorAaEventLog>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "SecurityFunctions.DoorAaStatusGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pDoorId", OracleDbType.Int32, 6, doorId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pList", OracleDbType.RefCursor, ParameterDirection.Output));
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new DoorAaEventLog()
                                {
                                    DoorId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    DsrServiceId = !r.IsDBNull(1) ? (int?)r.GetInt32(1) : null,
                                    DsrAccessPointGuid = !r.IsDBNull(2) ? (Guid?)Guid.Parse(r.GetString(2)) : null,
                                    IsConfirmed = !r.IsDBNull(3) && Formatting.TfStringToBool(r.GetString(3)),
                                    IsOnline = !r.IsDBNull(4) && Formatting.TfStringToBool(r.GetString(4)),
                                    DsrAccessPointTypeId = !r.IsDBNull(5) ? (Int16?)r.GetInt16(5) : null,
                                    SynchStatus = !r.IsDBNull(6) ? r.GetString(6) : null,
                                    LastSeen = !r.IsDBNull(7) ? (DateTime?)DateTime.FromOADate(decimal.ToDouble(r.GetDecimal(7))) : null,
                                    LastCommnunicationError = !r.IsDBNull(8) ? (DateTime?)DateTime.FromOADate(decimal.ToDouble(r.GetDecimal(8))) : null,
                                    Attributes = new List<StringPair>()
                                };

                                var attributeGroups = !r.IsDBNull(9) ? r.GetString(9).Split(';') : new string[0];
                                foreach (var attributeGroup in attributeGroups)
                                {
                                    var attr = attributeGroup.Split(',');
                                    if (attr.Length == 2 && (!string.IsNullOrEmpty(attr[0]) || !string.IsNullOrEmpty(attr[1])))
                                        item.Attributes.Add(new StringPair(attr[0], attr[1]));
                                }

                                list.Add(item);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <inheritdoc />
        public override List<DoorAaConnection> DoorAaConnectionGet(List<int> doorIds)
        {
            var list = new List<DoorAaConnection>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                var groupedDoors = doorIds.Select((x, index) => new { DoorId = x, Index = index })
                    .GroupBy(x => x.Index / 1000)   // Max limit for one WHERE IN clause
                    .Select(x => $"ap.DsrAccessPointId IN ({string.Join(",", x.Select(y => y.DoorId))})");

                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = $@"SELECT     ap.DsrAccessPointId,
                                                ap.DsrAccessPointGuid,
                                                s.ComputerName 
                                    FROM        DsrAccessPoint ap
                                    INNER JOIN  DsrService s ON s.DsrServiceId = ap.DsrServiceId
                                    WHERE       ap.DsrAccessPointGuid IS NOT NULL
                                    AND         ({string.Join(" OR ", groupedDoors)})",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new DoorAaConnection()
                                {
                                    DoorId = r.GetInt32(0),
                                    DoorGuid = Guid.Parse(r.GetString(1)),
                                    DsrServiceComputer = r.GetString(2)
                                });
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <inheritdoc />
        public override List<DoorAlarmLog> DoorAlarmsGet(int? doorId, DateTime startDate, DateTime endDate)
        {
            var list = new List<DoorAlarmLog>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "SecurityFunctions.AlarmGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pDoorId", OracleDbType.Int32, 6, doorId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pStartDate", OracleDbType.Double, startDate.ToOADate(), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pEndDate", OracleDbType.Double, endDate.ToOADate(), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pList", OracleDbType.RefCursor, ParameterDirection.Output));
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new DoorAlarmLog()
                                {
                                    LogId = !r.IsDBNull(0) ? r.GetInt64(0) : -1,
                                    ComputerId = !r.IsDBNull(1) ? (int?)r.GetInt32(1) : null,
                                    DoorId = !r.IsDBNull(2) ? r.GetInt32(2) : -1,
                                    DoorName = !r.IsDBNull(3) ? r.GetString(3) : null,
                                    MerchantId = !r.IsDBNull(4) ? (Int16?)r.GetInt16(4) : null,
                                    DoorAlarmType = !r.IsDBNull(5) ? r.GetString(5) : null,
                                    StartMcTransEqNum = !r.IsDBNull(6) ? r.GetInt64(6) : -1,
                                    StartActualDateTime = !r.IsDBNull(7) ? DateTime.FromOADate(r.GetDouble(7)) : DateTime.MinValue,
                                    StartIsOnline = !r.IsDBNull(8) && Formatting.TfStringToBool(r.GetString(8)),
                                    StartPostDateTime = !r.IsDBNull(9) ? DateTime.FromOADate(r.GetDouble(9)) : DateTime.MinValue,
                                    IsActive = !r.IsDBNull(10) && Formatting.TfStringToBool(r.GetString(10)),
                                    StopMcTransEqNum = !r.IsDBNull(11) ? (Int64?)r.GetInt64(11) : null,
                                    StopActualDateTime = !r.IsDBNull(12) ? (DateTime?)DateTime.FromOADate(r.GetDouble(12)) : null,
                                    StopIsOnline = !r.IsDBNull(13) ? (bool?)Formatting.TfStringToBool(r.GetString(13)) : null,
                                    StopPostDateTime = !r.IsDBNull(14) ? (DateTime?)DateTime.FromOADate(r.GetDouble(14)) : null,
                                    MasterControllerId = !r.IsDBNull(15) ? (int?)r.GetInt32(15) : null,
                                    IsAcked = !r.IsDBNull(16) && Formatting.TfStringToBool(r.GetString(16)),
                                    DoorAlarmAckInfoId = !r.IsDBNull(17) ? (int?)r.GetInt32(17) : null,
                                    DomainId = !r.IsDBNull(18) ? Guid.Parse(r.GetString(18)) : Guid.Empty,
                                    DoorType = (DoorType)(!r.IsDBNull(19) ? r.GetInt32(19) : 0)
                                };

                                if (!string.IsNullOrEmpty(item.DoorAlarmType) && item.DoorType == DoorType.Blackboard)
                                    item.DoorAlarmType = Enum.Parse(typeof(DoorAlarmLogType), item.DoorAlarmType).ToString();

                                list.Add(item);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <inheritdoc />
        public override List<DoorAlarmLog> ActiveAlarmsGet()
        {
            var list = new List<DoorAlarmLog>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "SecurityFunctions.ActiveAlarmsGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pList", OracleDbType.RefCursor, ParameterDirection.Output));
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new DoorAlarmLog()
                                {
                                    LogId = !r.IsDBNull(0) ? r.GetInt64(0) : -1,
                                    ComputerId = !r.IsDBNull(1) ? (int?)r.GetInt32(1) : null,
                                    DoorId = !r.IsDBNull(2) ? r.GetInt32(2) : -1,
                                    DoorName = !r.IsDBNull(3) ? r.GetString(3) : null,
                                    MerchantId = !r.IsDBNull(4) ? (Int16?)r.GetInt16(4) : null,
                                    DoorAlarmType = !r.IsDBNull(5) ? r.GetString(5) : null,
                                    StartMcTransEqNum = !r.IsDBNull(6) ? r.GetInt64(6) : -1,
                                    StartActualDateTime = !r.IsDBNull(7) ? DateTime.FromOADate(r.GetDouble(7)) : DateTime.MinValue,
                                    StartIsOnline = !r.IsDBNull(8) && Formatting.TfStringToBool(r.GetString(8)),
                                    StartPostDateTime = !r.IsDBNull(9) ? DateTime.FromOADate(r.GetDouble(9)) : DateTime.MinValue,
                                    IsActive = !r.IsDBNull(10) && Formatting.TfStringToBool(r.GetString(10)),
                                    StopMcTransEqNum = !r.IsDBNull(11) ? (Int64?)r.GetInt64(11) : null,
                                    StopActualDateTime = !r.IsDBNull(12) ? (DateTime?)DateTime.FromOADate(r.GetDouble(12)) : null,
                                    StopIsOnline = !r.IsDBNull(13) ? (bool?)Formatting.TfStringToBool(r.GetString(13)) : null,
                                    StopPostDateTime = !r.IsDBNull(14) ? (DateTime?)DateTime.FromOADate(r.GetDouble(14)) : null,
                                    MasterControllerId = !r.IsDBNull(15) ? (int?)r.GetInt32(15) : null,
                                    IsAcked = !r.IsDBNull(16) && Formatting.TfStringToBool(r.GetString(16)),
                                    DoorAlarmAckInfoId = !r.IsDBNull(17) ? (int?)r.GetInt32(17) : null,
                                    DomainId = !r.IsDBNull(18) ? Guid.Parse(r.GetString(18)) : Guid.Empty,
                                    DoorType = (DoorType)(!r.IsDBNull(19) ? r.GetInt32(19) : 0)
                                };

                                if (!string.IsNullOrEmpty(item.DoorAlarmType) && item.DoorType == DoorType.Blackboard)
                                    item.DoorAlarmType = Enum.Parse(typeof(DoorAlarmLogType), item.DoorAlarmType).ToString();

                                list.Add(item);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <inheritdoc />
        public override List<DoorAlarmLog> ActiveStaleAlarmsGet()
        {
            var list = new List<DoorAlarmLog>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "SecurityFunctions.ActiveStaleDoorAlarmsGetAll", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pList", OracleDbType.RefCursor, ParameterDirection.Output));
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new DoorAlarmLog()
                                {
                                    LogId = !r.IsDBNull(0) ? r.GetInt64(0) : -1,
                                    ComputerId = !r.IsDBNull(1) ? (int?)r.GetInt32(1) : null,
                                    DoorId = !r.IsDBNull(2) ? r.GetInt32(2) : -1,
                                    DoorName = !r.IsDBNull(3) ? r.GetString(3) : null,
                                    MerchantId = !r.IsDBNull(4) ? (Int16?)r.GetInt16(4) : null,
                                    DoorAlarmType = !r.IsDBNull(5) ? r.GetString(5) : null,
                                    StartMcTransEqNum = !r.IsDBNull(6) ? r.GetInt64(6) : -1,
                                    StartActualDateTime = !r.IsDBNull(7) ? DateTime.FromOADate(r.GetDouble(7)) : DateTime.MinValue,
                                    StartIsOnline = !r.IsDBNull(8) && Formatting.TfStringToBool(r.GetString(8)),
                                    StartPostDateTime = !r.IsDBNull(9) ? DateTime.FromOADate(r.GetDouble(9)) : DateTime.MinValue,
                                    IsActive = !r.IsDBNull(10) && Formatting.TfStringToBool(r.GetString(10)),
                                    StopMcTransEqNum = !r.IsDBNull(11) ? (Int64?)r.GetInt64(11) : null,
                                    StopActualDateTime = !r.IsDBNull(12) ? (DateTime?)DateTime.FromOADate(r.GetDouble(12)) : null,
                                    StopIsOnline = !r.IsDBNull(13) ? (bool?)Formatting.TfStringToBool(r.GetString(13)) : null,
                                    StopPostDateTime = !r.IsDBNull(14) ? (DateTime?)DateTime.FromOADate(r.GetDouble(14)) : null,
                                    MasterControllerId = !r.IsDBNull(15) ? (int?)r.GetInt32(15) : null,
                                    IsAcked = !r.IsDBNull(16) && Formatting.TfStringToBool(r.GetString(16)),
                                    DoorAlarmAckInfoId = !r.IsDBNull(17) ? (int?)r.GetInt32(17) : null,
                                    DomainId = !r.IsDBNull(18) ? Guid.Parse(r.GetString(18)) : Guid.Empty,
                                    DoorType = (DoorType)(!r.IsDBNull(19) ? r.GetInt32(19) : 0)
                                };

                                if (!string.IsNullOrEmpty(item.DoorAlarmType) && item.DoorType == DoorType.Blackboard)
                                    item.DoorAlarmType = Enum.Parse(typeof(DoorAlarmLogType), item.DoorAlarmType).ToString();

                                list.Add(item);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <inheritdoc />
        public override List<UserDoor> DoorsByUserGet(int customerId, bool? accessPlanActive = null)
        {
            var list = new List<UserDoor>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "SecurityFunctions.DoorsByUserGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pCustId", OracleDbType.Int32, 10, customerId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAccessPlanActive", OracleDbType.Varchar2, 1, accessPlanActive != null ? Formatting.BooleanToTf(accessPlanActive.Value) : null, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pList", OracleDbType.RefCursor, ParameterDirection.Output));
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new UserDoor()
                                {
                                    DoorId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    DoorIdentifier = !r.IsDBNull(1) ? r.GetString(1) : null,
                                    ScheduleId = !r.IsDBNull(2) ? r.GetInt32(2) : -1,
                                    ScheduleName = !r.IsDBNull(3) ? r.GetString(3) : null,
                                    HolidayScheduleId = !r.IsDBNull(4) ? (int?)r.GetInt32(4) : null,
                                    HolidayScheduleName = !r.IsDBNull(5) ? r.GetString(5) : null,
                                    Dow1ScheduleId = !r.IsDBNull(6) ? r.GetInt32(6) : -1,
                                    Dow1ScheduleName = !r.IsDBNull(7) ? r.GetString(7) : null,
                                    Dow2ScheduleId = !r.IsDBNull(8) ? r.GetInt32(8) : -1,
                                    Dow2ScheduleName = !r.IsDBNull(9) ? r.GetString(9) : null,
                                    Dow3ScheduleId = !r.IsDBNull(10) ? r.GetInt32(10) : -1,
                                    Dow3ScheduleName = !r.IsDBNull(11) ? r.GetString(11) : null,
                                    Dow4ScheduleId = !r.IsDBNull(12) ? r.GetInt32(12) : -1,
                                    Dow4ScheduleName = !r.IsDBNull(13) ? r.GetString(13) : null,
                                    Dow5ScheduleId = !r.IsDBNull(14) ? r.GetInt32(14) : -1,
                                    Dow5ScheduleName = !r.IsDBNull(15) ? r.GetString(15) : null,
                                    Dow6ScheduleId = !r.IsDBNull(16) ? r.GetInt32(16) : -1,
                                    Dow6ScheduleName = !r.IsDBNull(17) ? r.GetString(17) : null,
                                    Dow7ScheduleId = !r.IsDBNull(18) ? r.GetInt32(18) : -1,
                                    Dow7ScheduleName = !r.IsDBNull(19) ? r.GetString(19) : null,
                                    AccessPlanActive = !r.IsDBNull(20) && Formatting.TfStringToBool(r.GetString(20))
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <inheritdoc />
        public override List<UserDoorAa> DoorsAaByUserGet(int customerId, bool? accessPlanActive = null)
        {
            var list = new List<UserDoorAa>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "SecurityFunctions.DoorsAaByUserGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pCustId", OracleDbType.Int32, 10, customerId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAccessPlanActive", OracleDbType.Varchar2, 1, accessPlanActive != null ? Formatting.BooleanToTf(accessPlanActive.Value) : null, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pList", OracleDbType.RefCursor, ParameterDirection.Output));
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new UserDoorAa()
                                {
                                    DoorId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    DoorIdentifier = !r.IsDBNull(1) ? r.GetString(1) : null,
                                    ScheduleId = !r.IsDBNull(2) ? r.GetInt32(2) : -1,
                                    ScheduleName = !r.IsDBNull(3) ? r.GetString(3) : null,
                                    DayPeriods = !r.IsDBNull(4) ? r.GetString(4).Split(';').ToList() : null,
                                    AccessPlanActive = !r.IsDBNull(5) && Formatting.TfStringToBool(r.GetString(5))
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <inheritdoc />
        public override List<DoorUser> UsersByDoorGet(int doorId, bool? customerActive = null, bool? accessPlanActive = null)
        {
            var list = new List<DoorUser>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "SecurityFunctions.UsersByDoorGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pDoorId", OracleDbType.Int32, 6, doorId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pCustomerActive", OracleDbType.Varchar2, 1, customerActive != null ? Formatting.BooleanToTf(customerActive.Value) : null, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAccessPlanActive", OracleDbType.Varchar2, 1, accessPlanActive != null ? Formatting.BooleanToTf(accessPlanActive.Value) : null, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pList", OracleDbType.RefCursor, ParameterDirection.Output));
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new DoorUser()
                                {
                                    CustomerId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    CustomerNumber = !r.IsDBNull(1) ? r.GetDecimal(1) : -1,
                                    CustomerGuid = !r.IsDBNull(2) ? Guid.Parse(r.GetString(2)) : Guid.Empty,
                                    CustomerActive = !r.IsDBNull(3) && Formatting.TfStringToBool(r.GetString(3)),
                                    ScheduleId = !r.IsDBNull(4) ? r.GetInt32(4) : -1,
                                    ScheduleName = !r.IsDBNull(5) ? r.GetString(5) : null,
                                    HolidayScheduleId = !r.IsDBNull(6) ? (int?)r.GetInt32(6) : null,
                                    HolidayScheduleName = !r.IsDBNull(7) ? r.GetString(7) : null,
                                    Dow1ScheduleId = !r.IsDBNull(8) ? r.GetInt32(8) : -1,
                                    Dow1ScheduleName = !r.IsDBNull(9) ? r.GetString(9) : null,
                                    Dow2ScheduleId = !r.IsDBNull(10) ? r.GetInt32(10) : -1,
                                    Dow2ScheduleName = !r.IsDBNull(11) ? r.GetString(11) : null,
                                    Dow3ScheduleId = !r.IsDBNull(12) ? r.GetInt32(12) : -1,
                                    Dow3ScheduleName = !r.IsDBNull(13) ? r.GetString(13) : null,
                                    Dow4ScheduleId = !r.IsDBNull(14) ? r.GetInt32(14) : -1,
                                    Dow4ScheduleName = !r.IsDBNull(15) ? r.GetString(15) : null,
                                    Dow5ScheduleId = !r.IsDBNull(16) ? r.GetInt32(16) : -1,
                                    Dow5ScheduleName = !r.IsDBNull(17) ? r.GetString(17) : null,
                                    Dow6ScheduleId = !r.IsDBNull(18) ? r.GetInt32(18) : -1,
                                    Dow6ScheduleName = !r.IsDBNull(19) ? r.GetString(19) : null,
                                    Dow7ScheduleId = !r.IsDBNull(20) ? r.GetInt32(20) : -1,
                                    Dow7ScheduleName = !r.IsDBNull(21) ? r.GetString(21) : null,
                                    AccessPlanActive = !r.IsDBNull(22) && Formatting.TfStringToBool(r.GetString(22))
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <inheritdoc />
        public override List<DoorAaUser> UsersByDoorAaGet(int doorId, bool? customerActive = null, bool? accessPlanActive = null)
        {
            var list = new List<DoorAaUser>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "SecurityFunctions.UsersByDoorAaGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pDoorId", OracleDbType.Int32, 6, doorId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pCustomerActive", OracleDbType.Varchar2, 1, customerActive != null ? Formatting.BooleanToTf(customerActive.Value) : null, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAccessPlanActive", OracleDbType.Varchar2, 1, accessPlanActive != null ? Formatting.BooleanToTf(accessPlanActive.Value) : null, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pList", OracleDbType.RefCursor, ParameterDirection.Output));
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new DoorAaUser()
                                {
                                    CustomerId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    CustomerNumber = !r.IsDBNull(1) ? r.GetDecimal(1) : -1,
                                    CustomerGuid = !r.IsDBNull(2) ? Guid.Parse(r.GetString(2)) : Guid.Empty,
                                    CustomerActive = !r.IsDBNull(3) && Formatting.TfStringToBool(r.GetString(3)),
                                    ScheduleId = !r.IsDBNull(4) ? r.GetInt32(4) : -1,
                                    ScheduleName = !r.IsDBNull(5) ? r.GetString(5) : null,
                                    DayPeriods = !r.IsDBNull(6) ? r.GetString(6).Split(';').ToList() : null,
                                    AccessPlanActive = !r.IsDBNull(7) && Formatting.TfStringToBool(r.GetString(7))
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <inheritdoc />
        public override List<DoorTransaction> DoorTransactionsGet(int? doorId, int? customerId, DateTime startDate, DateTime endDate)
        {
            var list = new List<DoorTransaction>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "SecurityFunctions.DoorTransactionsGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pDoorId", OracleDbType.Int32, 6, doorId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pCustomerId", OracleDbType.Int32, 10, customerId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pStartDate", OracleDbType.Double, startDate.ToOADate(), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pEndDate", OracleDbType.Double, endDate.ToOADate(), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pList", OracleDbType.RefCursor, ParameterDirection.Output));
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new DoorTransaction()
                                {
                                    TransactionId = !r.IsDBNull(0) ? r.GetInt64(0) : -1,
                                    McTransactionSequenceNumber = !r.IsDBNull(1) ? r.GetInt64(1) : -1,
                                    IsOnline = !r.IsDBNull(2) && Formatting.TfStringToBool(r.GetString(2)),
                                    ValidationType = (DoorTransactionValidationType)(!r.IsDBNull(3) ? r.GetInt32(3) : 0),
                                    ActualDateTime = !r.IsDBNull(4) ? DateTime.FromOADate(r.GetDouble(4)) : DateTime.MinValue,
                                    PostDateTime = !r.IsDBNull(5) ? DateTime.FromOADate(r.GetDouble(5)) : DateTime.MinValue,
                                    TenderTypeId = !r.IsDBNull(6) ? r.GetInt32(6) : -1,
                                    DoorId = !r.IsDBNull(7) ? r.GetInt32(7) : -1,
                                    CustomerIdentificationType = (CustomerIdentificationType)(!r.IsDBNull(8) ? r.GetInt32(8) : 0),
                                    CustomerId = !r.IsDBNull(9) ? (int?)r.GetInt32(9) : null,
                                    MasterControllerId = !r.IsDBNull(10) ? (int?)r.GetInt32(10) : null,
                                    HostComputerId = !r.IsDBNull(11) ? (int?)r.GetInt32(11) : null,
                                    TransactionType = !r.IsDBNull(12) ? r.GetString(12) : null
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <inheritdoc />
        public override List<DoorTransaction> DoorTransactionsGet(int buildingId, DateTime startDate, DateTime endDate)
        {
            var list = new List<DoorTransaction>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "SecurityFunctions.DoorTransactionsGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pBuildingId", OracleDbType.Int32, 6, buildingId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pStartDate", OracleDbType.Double, startDate.ToOADate(), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pEndDate", OracleDbType.Double, endDate.ToOADate(), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pList", OracleDbType.RefCursor, ParameterDirection.Output));
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new DoorTransaction()
                                {
                                    TransactionId = !r.IsDBNull(0) ? r.GetInt64(0) : -1,
                                    McTransactionSequenceNumber = !r.IsDBNull(1) ? r.GetInt64(1) : -1,
                                    IsOnline = !r.IsDBNull(2) && Formatting.TfStringToBool(r.GetString(2)),
                                    ValidationType = (DoorTransactionValidationType)(!r.IsDBNull(3) ? r.GetInt32(3) : 0),
                                    ActualDateTime = !r.IsDBNull(4) ? DateTime.FromOADate(r.GetDouble(4)) : DateTime.MinValue,
                                    PostDateTime = !r.IsDBNull(5) ? DateTime.FromOADate(r.GetDouble(5)) : DateTime.MinValue,
                                    TenderTypeId = !r.IsDBNull(6) ? r.GetInt32(6) : -1,
                                    DoorId = !r.IsDBNull(7) ? r.GetInt32(7) : -1,
                                    CustomerIdentificationType = (CustomerIdentificationType)(!r.IsDBNull(8) ? r.GetInt32(8) : 0),
                                    CustomerId = !r.IsDBNull(9) ? (int?)r.GetInt32(9) : null,
                                    MasterControllerId = !r.IsDBNull(10) ? (int?)r.GetInt32(10) : null,
                                    HostComputerId = !r.IsDBNull(11) ? (int?)r.GetInt32(11) : null,
                                    TransactionType = !r.IsDBNull(12) ? r.GetString(12) : null
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <inheritdoc />
        public override List<DoorAaTransaction> DoorAaTransactionsGet(int? doorId, int? customerId, int? buildingId, DateTime startDate, DateTime endDate)
        {
            var list = new List<DoorAaTransaction>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "SecurityFunctions.DoorAaTransactionsGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pDoorId", OracleDbType.Int32, 6, doorId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pCustomerId", OracleDbType.Int32, 10, customerId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pBuildingId", OracleDbType.Int32, 6, buildingId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pStartDate", OracleDbType.Double, startDate.ToOADate(), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pEndDate", OracleDbType.Double, endDate.ToOADate(), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pList", OracleDbType.RefCursor, ParameterDirection.Output));
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new DoorAaTransaction()
                                {
                                    TransactionId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    CustomerId = !r.IsDBNull(1) ? (int?)r.GetInt32(1) : null,
                                    Result = !r.IsDBNull(2) ? r.GetString(2) : null,
                                    DoorId = !r.IsDBNull(3) ? r.GetInt32(3) : -1,
                                    MerchantId = !r.IsDBNull(4) ? (Int16?)r.GetInt16(4) : null,
                                    Description = !r.IsDBNull(5) ? r.GetString(5) : null,
                                    ActualDateTime = !r.IsDBNull(6) ? DateTime.FromOADate(r.GetDouble(6)) : DateTime.MinValue,
                                    PostDateTime = !r.IsDBNull(7) ? DateTime.FromOADate(r.GetDouble(7)) : DateTime.MinValue,
                                    CardNumber = !r.IsDBNull(8) ? r.GetString(8) : null,
                                    TransactionType = !r.IsDBNull(9) ? r.GetString(9) : null
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <inheritdoc />
        public override List<DoorActivity> DoorHistoryGet(int doorId, DateTime startDate, DateTime endDate)
        {
            var list = new List<DoorActivity>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "SecurityFunctions.DoorHistoryGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pDoorId", OracleDbType.Int32, 6, doorId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pStartDate", OracleDbType.Double, startDate.ToOADate(), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pEndDate", OracleDbType.Double, endDate.ToOADate(), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pList", OracleDbType.RefCursor, ParameterDirection.Output));
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new DoorActivity()
                                {
                                    TransactionType = !r.IsDBNull(0) ? r.GetString(0) : null,
                                    EventLogId = !r.IsDBNull(1) ? (Int64?)r.GetInt64(1) : null,
                                    McTransactionSequenceNumber = !r.IsDBNull(2) ? (Int64?)r.GetInt64(2) : null,
                                    ActualDateTime = !r.IsDBNull(3) ? (DateTime?)DateTime.FromOADate(r.GetDouble(3)) : null,
                                    DoorIdentifier = !r.IsDBNull(4) ? r.GetString(4) : null,
                                    MerchantId = !r.IsDBNull(5) ? r.GetInt16(5) : (Int16)(-1),
                                    BuildingName = !r.IsDBNull(6) ? r.GetString(6) : null,
                                    AreaName = !r.IsDBNull(7) ? r.GetString(7) : null,
                                    Description = !r.IsDBNull(8) ? r.GetString(8) : null,
                                    DoorId = !r.IsDBNull(9) ? r.GetInt32(9) : -1,
                                    DoorType = (DoorType)(!r.IsDBNull(10) ? r.GetInt32(10) : 0)
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <inheritdoc />
        public override void MasterControllerSet(int? masterControllerId, MasterController masterController)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "SecurityFunctions.MasterControllerSet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pMasterControllerId", OracleDbType.Int32, 6, masterControllerId, ParameterDirection.InputOutput));
                        cmd.Parameters.Add(new OracleParameter("pName", OracleDbType.Varchar2, 30, masterController.Name, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAbbreviatedName", OracleDbType.Varchar2, 10, masterController.AbbreviatedName, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pMultiplexorEnabled", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(masterController.MultiplexorEnabled), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pPinpadEnabled", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(masterController.PinpadEnabled), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pDADownloadScheduleId", OracleDbType.Int32, 10, masterController.DoorAccessDownloadScheduleId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pHardwareModel", OracleDbType.Int32, 10, masterController.HardwareModel, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pCommunicationType", OracleDbType.Int32, 10, masterController.CommunicationType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIsActive", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(masterController.IsActive), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pMacAddress", OracleDbType.Varchar2, 17, masterController.MacAddress, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pConnectionId", OracleDbType.Int32, 10, masterController.ConnectionId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIpConverterId", OracleDbType.Int32, 10, masterController.IpConverterId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIpConverterAddress", OracleDbType.Int32, 10, masterController.IpConverterAddress, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pDownloadType", OracleDbType.Int32, 10, masterController.DownloadType, ParameterDirection.Input));
                        

                        con.Open();
                        cmd.ExecuteNonQuery();

                        masterController.Id = masterControllerId ?? int.Parse(cmd.Parameters["pMasterControllerId"].Value.ToString());
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override void DoorSet(int? doorId, Door door)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "SecurityFunctions.DoorSet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pDoorId", OracleDbType.Int32, 6, doorId, ParameterDirection.InputOutput));
                        cmd.Parameters.Add(new OracleParameter("pMastercontrollerId", OracleDbType.Int32, 6, door.MasterControllerId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAddress", OracleDbType.Int32, 6, door.Address, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pMerchantId", OracleDbType.Int16, 4, door.MerchantId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pDoorIdentifier", OracleDbType.Varchar2, 10, door.DoorIdentifier, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pDescription", OracleDbType.Varchar2, 30, door.Description, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pBuildingId", OracleDbType.Int32, 6, door.BuildingId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAreaId", OracleDbType.Int32, 6, door.AreaId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pDoorGroupId", OracleDbType.Int32, 6, door.DoorGroupId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pUseDoorDefault", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(door.UseDoorDefault), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pDoorUnlockTimeSeconds", OracleDbType.Byte, 3, door.DoorUnlockSeconds, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pMinSirenTimeSeconds", OracleDbType.Byte, 3, door.MinSirenSeconds, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pHeldAlarmType", OracleDbType.Int32, 2, (int)door.HeldAlarmType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pHeldAlarmDelayTimeSeconds", OracleDbType.Byte, 3, door.HeldAlarmDelayTimeSeconds, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pForceAlarmType", OracleDbType.Int32, 2, (int)door.ForceAlarmType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pForceAlarmDelayTimeSeconds", OracleDbType.Byte, 3, door.ForceAlarmDelayTimeSeconds, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pUseDoorDefaultSchedule", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(door.UseDoorDefaultSchedule), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pScheduleId", OracleDbType.Int32, 6, door.ScheduleId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pOfflineOperationType", OracleDbType.Int32, 2, (int)door.OfflineOperationType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAlarmInstructions", OracleDbType.Varchar2, 255, door.AlarmInstruction, ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();

                        door.Id = doorId ?? int.Parse(cmd.Parameters["pDoorId"].Value.ToString());
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override void DoorAaSet(int? doorId, DoorAa door)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "SecurityFunctions.DoorAaSet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pDoorId", OracleDbType.Int32, 10, doorId, ParameterDirection.InputOutput));
                        cmd.Parameters.Add(new OracleParameter("pSerialNumber", OracleDbType.Varchar2, 30, door.SerialNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pName", OracleDbType.Varchar2, 30, door.Name, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pDescription", OracleDbType.Varchar2, 255, door.Description, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pDsrServiceId", OracleDbType.Int32, 10, door.DsrServiceId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIsConfirmed", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(door.IsConfirmed), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIsOnline", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(door.IsOnline), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pSynchStatus", OracleDbType.Varchar2, 30, door.SynchStatus, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pLastSeen", OracleDbType.Decimal, 22, (decimal?)door.LastSeen?.ToOADate(), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pLastCommunicationError", OracleDbType.Decimal, 22, (decimal?)door.LastCommnunicationError?.ToOADate(), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pDsrHardwareSettingGroupId", OracleDbType.Int32, 10, door.DsrHardwareSettingGroupId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pBuildingId", OracleDbType.Int32, 6, door.BuildingId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAreaId", OracleDbType.Int32, 6, door.AreaId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pDoorGroupId", OracleDbType.Int32, 6, door.DoorGroupId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pCryptoAlgorithmTypeId", OracleDbType.Int16, 4, door.CryptoAlgorithmTypeId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pSharedSecretKeyHexString", OracleDbType.Varchar2, 32, door.SharedSecretKeyHexString, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pSharedSecretKeyLifeSpanSec", OracleDbType.Int32, 6, door.SharedSecretKeyLifeSpanSeconds, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pDsrAccessPointTypeId", OracleDbType.Int16, 4, door.DsrAccessPointTypeId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pMerchantId", OracleDbType.Int16, 4, door.MerchantId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pDsrAccessPointGuid", OracleDbType.Varchar2, 36, door.DsrAccessPointGuid?.ToString("D"), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAccessModeId", OracleDbType.Int32, 10, door.AccessModeId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIsActive", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(door.IsActive), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIsDirty", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(door.IsDirty), ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();

                        door.Id = doorId ?? int.Parse(cmd.Parameters["pDoorId"].Value.ToString());
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override List<DoorAccessPoint> DoorsGet()
        {
            var list = new List<DoorAccessPoint>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "SecurityFunctions.DoorGetAll", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pList", OracleDbType.RefCursor, ParameterDirection.Output));
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new DoorAccessPoint()
                                {
                                    Id = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    MasterControllerId = !r.IsDBNull(1) ? (int?)r.GetInt32(1) : null,
                                    MerchantId = !r.IsDBNull(2) ? (Int16?)r.GetInt16(2) : null,
                                    DoorIdentifier = !r.IsDBNull(3) ? r.GetString(3) : null,
                                    Description = !r.IsDBNull(4) ? r.GetString(4) : null,
                                    BuildingId = !r.IsDBNull(5) ? (int?)r.GetInt32(5) : null,
                                    AreaId = !r.IsDBNull(6) ? (int?)r.GetInt32(6) : null,
                                    DoorGroupId = !r.IsDBNull(7) ? (int?)r.GetInt32(7) : null,
                                    DoorUnlockSeconds = !r.IsDBNull(8) ? (byte?)r.GetByte(8) : null,
                                    MinSirenSeconds = !r.IsDBNull(9) ? (byte?)r.GetByte(9) : null,
                                    HeldAlarmType = !r.IsDBNull(10) ? (DoorAlarmType?)r.GetInt32(10) : null,
                                    HeldAlarmDelayTimeSeconds = !r.IsDBNull(11) ? (byte?)r.GetByte(11) : null,
                                    ForceAlarmType = !r.IsDBNull(12) ? (DoorAlarmType?)r.GetInt32(12) : null,
                                    ForceAlarmDelayTimeSeconds = !r.IsDBNull(13) ? (byte?)r.GetByte(13) : null,
                                    ScheduleId = !r.IsDBNull(14) ? (int?)r.GetInt32(14) : null,
                                    AlarmInstruction = !r.IsDBNull(15) ? r.GetString(15) : null,
                                    DoorType = (DoorType)(!r.IsDBNull(16) ? r.GetInt32(16) : 0),
                                    Connectivity = !r.IsDBNull(17) ? (DoorConnectivity?)r.GetInt32(17) : null,
                                    DsrServiceId = !r.IsDBNull(18) ? (int?)r.GetInt32(18) : null
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <inheritdoc />
        public override void DoorAlarmAcknowledgeSet(Int64 alarmLogId, DoorAcknowledgeAlarm ackInfo)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "SecurityFunctions.AlarmLogSet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pDaDoorAlarmLogId", OracleDbType.Int64, 15, alarmLogId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIsAcknowledged", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(ackInfo.IsAcknowledged), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pUserName", OracleDbType.Varchar2, 15, ackInfo.UserName, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAllPrevious", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(ackInfo.AckowledgeAllPrevious), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAcknowledgeNote", OracleDbType.Varchar2, 255, ackInfo.Note, ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override void DoorAlarmAcknowledge(int? doorId, DoorAcknowledgeAlarmInfo ackInfo)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "SecurityFunctions.AcknowledgeAlarms", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pDoorId", OracleDbType.Int32, 6, doorId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pUserName", OracleDbType.Varchar2, 15, ackInfo.UserName, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAllPrevious", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(ackInfo.AckowledgeAllPrevious), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAcknowledgeNote", OracleDbType.Varchar2, 255, ackInfo.Note, ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override DoorListGetResponse DoorListGet(DoorListGetRequest request)
        {
            var result = new DoorListGetResponse()
            {
                Doors = new List<DoorInfo>()
            };

            int? count = null;
            string countQuery = null;
            var query = @"SELECT    Door.Door_Id,
                                    Door.Door_Identifier,
                                    Door.Description,
                                    MCH.Merchant_Id,
                                    MCH.Name AS MerchantName,
                                    BLD.Name AS BuildingName,
                                    Area.Name AS AreaName,
                                    DG.Name AS DoorGroupName
                        FROM        Door
                        INNER JOIN  Merchant MCH ON Door.Merchant_Id = MCH.Merchant_Id
                        LEFT JOIN   Building BLD ON Door.Building_Id = BLD.Building_Id
                        LEFT JOIN   Area ON Door.Area_Id = Area.Area_Id
                        LEFT JOIN   Door_Group DG ON Door.Door_Group_Id = DG.Door_Group_Id
                        WHERE       Door.Merchant_Id = NVL(:pMerchantId, Door.Merchant_Id)
                        AND         Door.Door_Identifier LIKE :pDoorName
                        AND         Door.Description LIKE :pDescription
                        AND         BLD.Name LIKE :pBuildingName
                        AND         Area.Name LIKE :pAreaName";

            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandType = CommandType.Text })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pMerchantId", OracleDbType = OracleDbType.Int16, Size = 4, Direction = ParameterDirection.Input, Value = request?.MerchantId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDoorName", OracleDbType = OracleDbType.Varchar2, Size = 11, Direction = ParameterDirection.Input, Value = $"{request?.DoorName}%" });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDescription", OracleDbType = OracleDbType.Varchar2, Size = 31, Direction = ParameterDirection.Input, Value = $"{request?.Description}%" });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pBuildingName", OracleDbType = OracleDbType.Varchar2, Size = 31, Direction = ParameterDirection.Input, Value = $"{request?.Building}%" });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pAreaName", OracleDbType = OracleDbType.Varchar2, Size = 31, Direction = ParameterDirection.Input, Value = $"{request?.Area}%" });
                        
                        if (request.PageSize.HasValue)
                        {
                            countQuery = $"SELECT count(*) FROM ({query})";
                            query = $"{query} OFFSET {request.Offset} ROWS FETCH NEXT {request.PageSize} ROWS ONLY";
                        }

                        cmd.CommandText = query;
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                result.Doors.Add(new DoorInfo()
                                {
                                    DoorId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    DoorName = !r.IsDBNull(1) ? r.GetString(1) : string.Empty,
                                    Description = !r.IsDBNull(2) ? r.GetString(2) : string.Empty,
                                    MerchantId = !r.IsDBNull(3) ? r.GetInt16(3) : (Int16)(-1),
                                    Merchant = !r.IsDBNull(4) ? r.GetString(4) : string.Empty,
                                    Building = !r.IsDBNull(5) ? r.GetString(5) : string.Empty,
                                    Area = !r.IsDBNull(6) ? r.GetString(6) : string.Empty,
                                    DoorGroupName = !r.IsDBNull(7) ? r.GetString(7) : string.Empty
                                });
                            }
                        }

                        if (!string.IsNullOrEmpty(countQuery))
                        {
                            cmd.CommandText = countQuery;
                            count = int.Parse(cmd.ExecuteScalar().ToString());
                        }
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }

            result.TotalDoorCount = count ?? result.Doors.Count;
            return result;
        }

        /// <inheritdoc />
        public override List<DoorInfo> DoorInfoGet(DoorInfoGetRequest request)
        {
            var list = new List<DoorInfo>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "SecurityFunctions.DoorGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pBuildingId", OracleDbType.Int32, 6, request?.BuildingId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAreaId", OracleDbType.Int32, 6, request?.AreaId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pDoorGroupId", OracleDbType.Int32, 6, request?.DoorGroupId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pList", OracleDbType.RefCursor, ParameterDirection.Output));
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new DoorInfo()
                                {
                                    DoorId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    DoorName = !r.IsDBNull(1) ? r.GetString(1) : string.Empty,
                                    Description = !r.IsDBNull(2) ? r.GetString(2) : string.Empty,
                                    MerchantId = !r.IsDBNull(3) ? r.GetInt16(3) : (Int16)(-1),
                                    Merchant = !r.IsDBNull(4) ? r.GetString(4) : string.Empty,
                                    Building = !r.IsDBNull(5) ? r.GetString(5) : string.Empty,
                                    Area = !r.IsDBNull(6) ? r.GetString(6) : string.Empty,
                                    DoorGroupName = !r.IsDBNull(7) ? r.GetString(7) : string.Empty
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <inheritdoc />
        public override DoorApiKeyGetResponse DoorApiKeyGet()
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "SecurityFunctions.DoorApiKeyGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pReadOnlyKey", OracleDbType.Varchar2, 50, null, ParameterDirection.Output));
                        cmd.Parameters.Add(new OracleParameter("pReadOnlySecret", OracleDbType.Varchar2, 256, null, ParameterDirection.Output));
                        cmd.Parameters.Add(new OracleParameter("pReadWriteKey", OracleDbType.Varchar2, 50, null, ParameterDirection.Output));
                        cmd.Parameters.Add(new OracleParameter("pReadWriteSecret", OracleDbType.Varchar2, 256, null, ParameterDirection.Output));
                        con.Open();
                        cmd.ExecuteNonQuery();

                        return new DoorApiKeyGetResponse()
                        {
                            ReadOnlyKey = cmd.Parameters["pReadOnlyKey"].Value?.ToString(),
                            ReadOnlySecret = cmd.Parameters["pReadOnlySecret"].Value?.ToString(),
                            ReadWriteKey = cmd.Parameters["pReadWriteKey"].Value?.ToString(),
                            ReadWriteSecret = cmd.Parameters["pReadWriteSecret"].Value?.ToString()
                        };
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override void DoorApiKeySet(DoorApiKeySetRequest request)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "SecurityFunctions.DoorApiKeySet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pReadOnlyKey", OracleDbType.Varchar2, 50, request.ReadOnlyKey, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pReadOnlySecret", OracleDbType.Varchar2, 256, request.ReadOnlySecret, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pReadWriteKey", OracleDbType.Varchar2, 50, request.ReadWriteKey, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pReadWriteSecret", OracleDbType.Varchar2, 256, request.ReadWriteSecret, ParameterDirection.Input));
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        #endregion
    }
}
