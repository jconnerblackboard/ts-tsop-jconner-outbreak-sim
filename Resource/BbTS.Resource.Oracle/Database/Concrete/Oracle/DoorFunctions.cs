﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using BbTS.Core;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Communication.BbHost;
using BbTS.Domain.Models.Definitions.Door;
using BbTS.Domain.Models.Definitions.Event;
using BbTS.Domain.Models.DoorAccess;
using BbTS.Domain.Models.Event;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.System.Database;
using BbTS.Monitoring.Logging;
using Oracle.ManagedDataAccess.Client;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        /// <summary>
        /// This method will return a list of Area objects from the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsArea> AreaGet(ConnectionInfo connection)
        {
            var list = new List<TsArea>();

            using (var con = OracleConnectionGet(connection))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "Area", CommandType = CommandType.TableDirect })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsArea();
                                if (!r.IsDBNull(0)) item.Area_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Building_Id = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Name = r.GetString(2);
                                if (!r.IsDBNull(3)) item.DomainId = r.GetString(3);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of Building objects from the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsBuilding> BuildingsGet(ConnectionInfo connection)
        {
            var list = new List<TsBuilding>();

            using (var con = OracleConnectionGet(connection))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "Building", CommandType = CommandType.TableDirect })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsBuilding();

                                if (!r.IsDBNull(0)) item.Building_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Name = r.GetString(1);
                                if (!r.IsDBNull(2)) item.DomainId = r.GetString(2);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void CustomerDaRequestHistoryByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Customer_Da_Request_History WHERE TRUNC(ActualDateTime) = {oleAutomationDate}", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> CustomerDaRequestHistoryDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TRUNC(ActualDateTime) ActualDateTime FROM Envision.Customer_Da_Request_History WHERE ActualDateTime < {oleAutomationDate} ORDER BY ActualDateTime ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsCustomer_Da_Request_History> CustomerDaRequestHistoryListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsCustomer_Da_Request_History>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT * FROM Envision.Customer_Da_Request_History WHERE ActualDateTime BETWEEN {oleAutomationDate}.0 AND {oleAutomationDate}.9999999999", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsCustomer_Da_Request_History();

                                if (!r.IsDBNull(0)) item.Customer_Da_Request_History_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Cust_Id = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Mc_TranSeqNum = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.ActualDateTime = DateTime.FromOADate(r.GetDouble(3));
                                if (!r.IsDBNull(4)) item.PostDateTime = DateTime.FromOADate(r.GetDouble(4));
                                if (!r.IsDBNull(5)) item.ResultCode = r.GetInt32(5);
                                if (!r.IsDBNull(6)) item.Cust_Identification_Type = r.GetInt32(6);
                                if (!r.IsDBNull(7)) item.CardNum = r.GetString(7);
                                if (!r.IsDBNull(8)) item.Issue_Number = r.GetString(8);
                                if (!r.IsDBNull(9)) item.Issue_Number_Captured = r.GetString(9);
                                if (!r.IsDBNull(10)) item.Door_Group_Id = r.GetInt32(10);
                                if (!r.IsDBNull(11)) item.Merchant_Id = r.GetInt32(11);
                                if (!r.IsDBNull(12)) item.Building_Id = r.GetInt32(12);
                                if (!r.IsDBNull(13)) item.Area_Id = r.GetInt32(13);
                                if (!r.IsDBNull(14)) item.Door_Group_Id = r.GetInt32(14);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void DaDeniedTransactionByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Da_DeniedTransaction WHERE TRUNC(ActualDateTime) = {oleAutomationDate}", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsDa_DeniedTransaction> DaDeniedTransactionListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsDa_DeniedTransaction>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT * FROM Envision.Da_DeniedTransaction WHERE TRUNC(ActualDateTime) = {oleAutomationDate}", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsDa_DeniedTransaction();

                                if (!r.IsDBNull(0)) item.DeniedTran_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Mc_TranSeqNum = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.IsOnline = r.GetString(2);
                                if (!r.IsDBNull(3)) item.Validation_Type = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.ActualDateTime = LocalDateTimeToUtc(DateTime.FromOADate(r.GetDouble(4)));
                                if (!r.IsDBNull(5)) item.PostDateTime = LocalDateTimeToUtc(DateTime.FromOADate(r.GetDouble(5)));
                                if (!r.IsDBNull(6)) item.Tender_Type_Id = r.GetInt32(6);
                                if (!r.IsDBNull(7)) item.MasterController_Id = r.GetInt32(7);
                                if (!r.IsDBNull(8)) item.Door_Id = r.GetInt32(8);
                                if (!r.IsDBNull(9)) item.Cust_Identification_Type = r.GetInt32(9);
                                if (!r.IsDBNull(10)) item.Cust_Id = r.GetInt32(10);
                                if (!r.IsDBNull(11)) item.HostComp_Id = r.GetInt32(11);
                                if (!r.IsDBNull(12)) item.DeniedCode = r.GetInt32(12);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> DaDeniedTransactionDaysFilteredByDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TRUNC(ActualDateTime) ActualDateTime FROM Envision.Da_DeniedTransaction WHERE TRUNC(ActualDateTime) < {oleAutomationDate} ORDER BY TRUNC(ActualDateTime) ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void DaDeniedTransactionCardByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Da_DeniedTransaction_Card WHERE DeniedTran_Id IN (SELECT DeniedTran_Id FROM Envision.Da_DeniedTransaction WHERE TRUNC(ActualDateTime) = {oleAutomationDate})", true);
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsDa_DeniedTransaction_Card> DaDeniedTransactionCardListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsDa_DeniedTransaction_Card>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT * FROM Envision.Da_DeniedTransaction_Card WHERE DeniedTran_Id IN (SELECT DeniedTran_Id FROM Envision.Da_DeniedTransaction WHERE TRUNC(ActualDateTime) = {oleAutomationDate})", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsDa_DeniedTransaction_Card();

                                if (!r.IsDBNull(0)) item.DeniedTran_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.CardNum = r.GetString(1);
                                if (!r.IsDBNull(2)) item.Issue_Number = r.GetString(2);
                                if (!r.IsDBNull(3)) item.Issue_Number_Captured = r.GetString(3);
                                if (!r.IsDBNull(4)) item.Swiped = r.GetString(4);
                                if (!r.IsDBNull(5)) item.Pin_Entered = r.GetString(5);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> DaDeniedTransactionCardDaysFilteredByDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TRUNC(ActualDateTime) ActualDateTime FROM Envision.Da_DeniedTransaction WHERE TRUNC(ActualDateTime) < {oleAutomationDate} ORDER BY TRUNC(ActualDateTime) ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void DaDoorAlarmAckInfoByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Da_DoorAlarm_AckInfo WHERE TRUNC(Ack_DateTime) = {oleAutomationDate} AND Da_DoorAlarm_AckInfo_Id NOT IN (SELECT Da_DoorAlarm_AckInfo_Id FROM Da_DoorAlarmLog WHERE Da_DoorAlarm_AckInfo_Id IS NOT NULL)", true);
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsDa_DoorAlarm_AckInfo> DaDoorAlarmAckInfoListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsDa_DoorAlarm_AckInfo>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT * FROM Envision.Da_DoorAlarm_AckInfo WHERE TRUNC(Ack_DateTime) = {oleAutomationDate}", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsDa_DoorAlarm_AckInfo();

                                if (!r.IsDBNull(0)) item.Da_DoorAlarm_AckInfo_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.UserName = r.GetString(1);
                                if (!r.IsDBNull(2)) item.Ack_DateTime = LocalDateTimeToUtc(DateTime.FromOADate(r.GetDouble(2)));
                                if (!r.IsDBNull(3)) item.Ack_AllPrev = r.GetString(3);
                                if (!r.IsDBNull(4)) item.Ack_Note = r.GetString(4);
                                if (!r.IsDBNull(5)) item.DomainId = r.GetString(5);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> DaDoorAlarmAckInfoDaysFilteredByDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TRUNC(Ack_DateTime) Ack_DateTime FROM Envision.Da_DoorAlarm_AckInfo WHERE Ack_DateTime < {oleAutomationDate} ORDER BY Ack_DateTime ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void DaDoorAlarmLogByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Da_DoorAlarmlog WHERE TRUNC(Start_ActualDateTime) = {oleAutomationDate}", true);
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsDa_DoorAlarmLog> DaDoorAlarmLogListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsDa_DoorAlarmLog>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT * FROM Envision.Da_DoorAlarmLog WHERE TRUNC(Start_ActualDateTime) = {oleAutomationDate}", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsDa_DoorAlarmLog();

                                if (!r.IsDBNull(0)) item.Da_DoorAlarmLog_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Computer_Id = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Door_Id = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.DoorAlarmType = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.Start_McTranSeqNum = r.GetInt32(4);
                                if (!r.IsDBNull(5)) item.Start_ActualDateTime = LocalDateTimeToUtc(DateTime.FromOADate(r.GetDouble(5)));
                                if (!r.IsDBNull(6)) item.Start_IsOnline = r.GetString(6);
                                if (!r.IsDBNull(7)) item.Start_PostDateTime = LocalDateTimeToUtc(DateTime.FromOADate(r.GetDouble(7)));
                                if (!r.IsDBNull(8)) item.Is_Active = r.GetString(8);
                                if (!r.IsDBNull(9)) item.Stop_McTranSeqNum = r.GetInt32(9);
                                if (!r.IsDBNull(10)) item.Stop_ActualDateTime = LocalDateTimeToUtc(DateTime.FromOADate(r.GetDouble(10)));
                                if (!r.IsDBNull(11)) item.Stop_IsOnline = r.GetString(11);
                                if (!r.IsDBNull(12)) item.Stop_PostDateTime = LocalDateTimeToUtc(DateTime.FromOADate(r.GetDouble(12)));
                                if (!r.IsDBNull(13)) item.MasterController_Id = r.GetInt32(13);
                                if (!r.IsDBNull(14)) item.Is_Acked = r.GetString(14);
                                if (!r.IsDBNull(15)) item.Da_DoorAlarm_AckInfo_Id = r.GetInt32(15);
                                if (!r.IsDBNull(16)) item.DomainId = r.GetString(16);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> DaDoorAlarmLogDaysFilteredByDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TRUNC(Start_ActualDateTime) Start_ActualDateTime FROM Envision.Da_DoorAlarmlog WHERE Start_ActualDateTime < {oleAutomationDate} ORDER BY Start_ActualDateTime ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void DaDoorCurrentStatusByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Da_DoorCurrentStatus WHERE TRUNC(LastUpdateDt) = {oleAutomationDate}", true);
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsDa_DoorCurrentStatus> DaDoorCurrentStatusListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsDa_DoorCurrentStatus>();

            using (var con = OracleConnectionGet(connection))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = $"SELECT * FROM Envision.Da_DoorCurrentStatus WHERE TRUNC(LastUpdateDt) = {oleAutomationDate}", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsDa_DoorCurrentStatus();

                                if (!r.IsDBNull(0)) item.Door_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Mc_TranSeqNum = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Is_Online = r.GetString(2);
                                if (!r.IsDBNull(3)) item.Door_ControlMode = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.Is_ExitCycle = r.GetString(4);
                                if (!r.IsDBNull(5)) item.Is_Unlocked = r.GetString(5);
                                if (!r.IsDBNull(6)) item.Is_Open = r.GetString(6);
                                if (!r.IsDBNull(7)) item.Requires_Pin = r.GetString(7);
                                if (!r.IsDBNull(8)) item.Override_Status = r.GetInt32(8);
                                if (!r.IsDBNull(9)) item.LastUpdateDt = LocalDateTimeToUtc(DateTime.FromOADate(r.GetDouble(9)));
                                if (!r.IsDBNull(10)) item.Computer_Id = r.GetInt32(10);
                                if (!r.IsDBNull(11)) item.MasterController_Id = r.GetInt32(11);
                                if (!r.IsDBNull(12)) item.Da_DoorStateOverride_Id = r.GetInt32(12);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> DaDoorCurrentStatusDaysFilteredByDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TRUNC(LastUpdateDt) LastUpdateDt FROM Envision.Da_DoorCurrentStatus WHERE LastUpdateDt < {oleAutomationDate} ORDER BY LastUpdateDt ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void DaDoorEventLogByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Da_Dooreventlog WHERE TRUNC(ActualDateTime) = {oleAutomationDate}", true);
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsDa_DoorEventLog> DaDoorEventLogListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsDa_DoorEventLog>();

            using (var con = OracleConnectionGet(connection))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = $"SELECT * FROM Envision.Da_DoorEventLog WHERE TRUNC(ActualDateTime) = {oleAutomationDate}", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                // AttributeClause 
                                var item = new TsDa_DoorEventLog();


                                if (!r.IsDBNull(0)) item.Log_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Computer_Id = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Door_Id = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.Mc_TranSeqNum = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.Is_Online = r.GetString(4);
                                if (!r.IsDBNull(5)) item.ActualDateTime = LocalDateTimeToUtc(DateTime.FromOADate(r.GetDouble(5)));
                                if (!r.IsDBNull(6)) item.PostDateTime = LocalDateTimeToUtc(DateTime.FromOADate(r.GetDouble(6)));
                                if (!r.IsDBNull(7)) item.Door_ControlMode = r.GetInt32(7);
                                if (!r.IsDBNull(8)) item.Is_Exit_Cycle = r.GetString(8);
                                if (!r.IsDBNull(9)) item.Is_Unlocked = r.GetString(9);
                                if (!r.IsDBNull(10)) item.Is_Open = r.GetString(10);
                                if (!r.IsDBNull(11)) item.Is_Held = r.GetString(11);
                                if (!r.IsDBNull(12)) item.Is_Forced = r.GetString(12);
                                if (!r.IsDBNull(13)) item.Is_Tampered = r.GetString(13);
                                if (!r.IsDBNull(14)) item.Requires_Pin = r.GetString(14);
                                if (!r.IsDBNull(15)) item.DoorCtrl_Comm_Lost = r.GetString(15);
                                if (!r.IsDBNull(16)) item.Tb_1_Aux_Cs_Tamper = r.GetString(16);
                                if (!r.IsDBNull(17)) item.Tb_2_Aux_Cs_Tamper = r.GetString(17);
                                if (!r.IsDBNull(18)) item.Serial_1_Cs_Comm_Lost = r.GetString(18);
                                if (!r.IsDBNull(19)) item.Serial_2_Cs_Comm_Lost = r.GetString(19);
                                if (!r.IsDBNull(20)) item.DoorCtrl_Ac_Power_Fail = r.GetString(20);
                                if (!r.IsDBNull(21)) item.DoorCtrl_Battery_Low = r.GetString(21);
                                if (!r.IsDBNull(22)) item.Override_Status = r.GetInt32(22);
                                if (!r.IsDBNull(23)) item.MasterController_Id = r.GetInt32(23);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> DaDoorEventLogDaysFilteredByDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TRUNC(ActualDateTime) ActualDate FROM Envision.Da_Dooreventlog WHERE TRUNC(ActualDateTime) < {oleAutomationDate} ORDER BY TRUNC(ActualDateTime) ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <inheritdoc />
        public override List<CustomerDoorAccessLog> CustomerDoorAccessLogsGet(DateTime startDate, DateTime endDate)
        {
            var list = new List<CustomerDoorAccessLog>();

            using (var con = OracleConnectionGet(ConnectionString))
            {
                var cmdText = $@"
                    select  DISTINCT
                            BTS.InstitutionId,
                            CUS.CustomerGuid,
                            CUS.Custnum,
                            BLD.Name AS BuildingName,
                            AR.Name AS AreaName,  
                            DOR.DomainId,
                            DOR.Door_Identifier,
                            DOR.Description,
                            DAT.actualdatetime AS EntryDateTime
                    from DA_Transaction DAT
                    inner join Customer CUS on CUS.Cust_Id = DAT.Cust_Id
                    inner join Door DOR on Dor.Door_Id = DAT.Door_Id
                    inner join Building BLD on BLD.Building_ID = DOR.Building_Id
                    inner join Area AR on AR.Area_Id = DOR.Area_Id
                    inner join BbSPTransactionSystem BTS ON BTS.IsValid = 'T'
                    where DAT.ActualDateTime > {startDate.ToOADate()}
                    AND DAT.ActualDateTime < {endDate.ToOADate()}
                    Order By EntryDateTime";
                using (var cmd = new OracleCommand { Connection = con, CommandText = cmdText, CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new CustomerDoorAccessLog();
                                if (!r.IsDBNull(0)) item.InstitutionId = r.GetString(0);
                                if (!r.IsDBNull(1)) item.CustomerGuid = r.GetString(1);
                                if (!r.IsDBNull(2)) item.CustNum = r.GetString(2);
                                if (!r.IsDBNull(3)) item.BuildingName = r.GetString(3);
                                if (!r.IsDBNull(4)) item.AreaName = r.GetString(4);
                                if (!r.IsDBNull(5)) item.DoorId = new Guid(r.GetString(5));
                                if (!r.IsDBNull(6)) item.DoorIdentifier = r.GetString(6);
                                if (!r.IsDBNull(7)) item.DoorDescription = r.GetString(7);
                                if (!r.IsDBNull(8)) item.EntryDateTime = new DateTimeOffset(DateTime.FromOADate(r.GetDouble(8)), TimeZoneInfo.Local.BaseUtcOffset);
                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of Door objects from the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsDoor> DoorsGet(ConnectionInfo connection)
        {
            var list = new List<TsDoor>();

            using (var con = OracleConnectionGet(connection))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "Door", CommandType = CommandType.TableDirect })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsDoor();

                                if (!r.IsDBNull(0)) item.Door_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.MasterController_Id = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Address = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.Merchant_Id = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.Door_Identifier = r.GetString(4);
                                if (!r.IsDBNull(5)) item.Description = r.GetString(5);
                                if (!r.IsDBNull(6)) item.Building_Id = r.GetInt32(6);
                                if (!r.IsDBNull(7)) item.Area_Id = r.GetInt32(7);
                                if (!r.IsDBNull(8)) item.Door_Group_Id = r.GetInt32(8);
                                if (!r.IsDBNull(9)) item.Use_Door_Default = r.GetString(9);
                                if (!r.IsDBNull(10)) item.Door_Unlock_Time_Seconds = r.GetInt32(10);
                                if (!r.IsDBNull(11)) item.Min_Siren_Time_Seconds = r.GetInt32(11);
                                if (!r.IsDBNull(12)) item.Held_Alarm_Type = r.GetInt32(12);
                                if (!r.IsDBNull(13)) item.Held_Alarm_Delay_Time_Seconds = r.GetInt32(13);
                                if (!r.IsDBNull(14)) item.Force_Alarm_Type = r.GetInt32(14);
                                if (!r.IsDBNull(15)) item.Force_Alarm_Delay_Time_Seconds = r.GetInt32(15);
                                if (!r.IsDBNull(16)) item.Use_Door_Default_Schedule = r.GetString(16);
                                if (!r.IsDBNull(17)) item.Schedule_Id = r.GetInt32(17);
                                if (!r.IsDBNull(18)) item.Offline_Operation_Type = r.GetInt32(18);
                                if (!r.IsDBNull(19)) item.Alarm_Instructions = r.GetString(19);
                                if (!r.IsDBNull(20)) item.DomainId = r.GetString(20);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void DaDoorStateOverrideByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Da_DoorStateOverride WHERE TRUNC(ToHost_Dt) = {oleAutomationDate}", true);
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsDa_DoorStateOverride> DaDoorStateOverrideListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsDa_DoorStateOverride>();

            using (var con = OracleConnectionGet(connection))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = $"SELECT * FROM Envision.Da_DoorStateOverride WHERE TRUNC(ToHost_Dt) = {oleAutomationDate}", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsDa_DoorStateOverride();

                                if (!r.IsDBNull(0)) item.Da_DoorStateOverride_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.UserName = r.GetString(1);
                                if (!r.IsDBNull(2)) item.Door_Id = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.Computer_Id = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.Override_Type = r.GetInt32(4);
                                if (!r.IsDBNull(5)) item.Override_Door_State = r.GetInt32(5);
                                if (!r.IsDBNull(6)) item.Override_Status = r.GetInt32(6);
                                if (!r.IsDBNull(7)) item.Retry_Secs = r.GetInt32(7);
                                if (!r.IsDBNull(8)) item.ToHost_Dt = LocalDateTimeToUtc(DateTime.FromOADate(r.GetDouble(8)));
                                if (!r.IsDBNull(9)) item.ToController_Dt = LocalDateTimeToUtc(DateTime.FromOADate(r.GetDouble(9)));
                                if (!r.IsDBNull(10)) item.Log_Id = r.GetInt32(10);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> DaDoorStateOverrideDaysFilteredByDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TRUNC(ToHost_Dt) ToHost_Dt FROM Envision.Da_DoorStateOverride WHERE ToHost_Dt < {oleAutomationDate} ORDER BY ToHost_Dt ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of MasterController objects from the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsMasterController> MasterControllersGet(ConnectionInfo connection)
        {
            var list = new List<TsMasterController>();

            using (var con = OracleConnectionGet(connection))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "MasterController", CommandType = CommandType.TableDirect })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsMasterController();

                                if (!r.IsDBNull(0)) item.MasterController_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Name = r.GetString(1);
                                if (!r.IsDBNull(2)) item.Abbreviated_Name = r.GetString(2);
                                if (!r.IsDBNull(3)) item.Multiplexor_Enabled = r.GetString(3);
                                if (!r.IsDBNull(4)) item.Pinpad_Enabled = r.GetString(4);
                                if (!r.IsDBNull(5)) item.Door_Access_Download_Sched_Id = r.GetInt32(5);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void DaTransactionByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Da_Transaction WHERE TRUNC(ActualDateTime) = {oleAutomationDate}", true);
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsDa_Transaction> DaTransactionListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsDa_Transaction>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT * FROM Envision.Da_Transaction WHERE TRUNC(ActualDateTime) = {oleAutomationDate}", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsDa_Transaction();

                                if (!r.IsDBNull(0)) item.Tran_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Mc_TranSeqNum = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.IsOnline = r.GetString(2);
                                if (!r.IsDBNull(3)) item.Validation_Type = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.ActualDateTime = LocalDateTimeToUtc(DateTime.FromOADate(r.GetDouble(4)));
                                if (!r.IsDBNull(5)) item.PostDateTime = LocalDateTimeToUtc(DateTime.FromOADate(r.GetDouble(5)));
                                if (!r.IsDBNull(6)) item.Tender_Type_Id = r.GetInt32(6);
                                if (!r.IsDBNull(7)) item.Door_Id = r.GetInt32(7);
                                if (!r.IsDBNull(8)) item.Cust_Identification_Type = r.GetInt32(8);
                                if (!r.IsDBNull(9)) item.Cust_Id = r.GetInt32(9);
                                if (!r.IsDBNull(10)) item.MasterController_Id = r.GetInt32(10);
                                if (!r.IsDBNull(11)) item.HostComp_Id = r.GetInt32(11);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> DaTransactionDaysFilteredByDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TRUNC(ActualDateTime) ActualDateTime FROM Envision.Da_Transaction WHERE TRUNC(ActualDateTime) < {oleAutomationDate} ORDER BY TRUNC(ActualDateTime) ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void DaTransactionCardByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Da_Transaction_Card WHERE Tran_Id IN (SELECT Tran_Id FROM Envision.Da_Transaction WHERE TRUNC(ActualDateTime) = {oleAutomationDate})", true);
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsDa_Transaction_Card> DaTransactionCardListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsDa_Transaction_Card>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT * FROM Envision.Da_Transaction_Card WHERE Tran_Id IN (SELECT Tran_Id FROM Envision.Da_Transaction WHERE TRUNC(ActualDateTime) = {oleAutomationDate})", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsDa_Transaction_Card();

                                if (!r.IsDBNull(0)) item.Tran_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.CardNum = r.GetString(1);
                                if (!r.IsDBNull(2)) item.Issue_Number = r.GetString(2);
                                if (!r.IsDBNull(3)) item.Issue_Number_Captured = r.GetString(3);
                                if (!r.IsDBNull(4)) item.Swiped = r.GetString(4);
                                if (!r.IsDBNull(5)) item.Pin_Entered = r.GetString(5);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> DaTransactionCardDaysFilteredByDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TRUNC(ActualDateTime) ActualDateTime FROM Envision.Da_Transaction WHERE TRUNC(ActualDateTime) < {oleAutomationDate} ORDER BY TRUNC(ActualDateTime) ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Get a list of door access plans
        /// </summary>
        /// <param name="doorAccessPlanId">Id filter</param>
        /// <returns>Door access plans</returns>
        public override List<TsDoorAccessPlan> DoorAccessPlanGet(int? doorAccessPlanId = null)
        {
            var list = new List<TsDoorAccessPlan>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "DoorAccess.PlanGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pAccessPlanId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = doorAccessPlanId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsDoorAccessPlan()
                                {
                                    Id = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    Name = !r.IsDBNull(1) ? r.GetString(1) : string.Empty,
                                    Active = !r.IsDBNull(2) && r.GetString(2) == "T",
                                    StartDate = !r.IsDBNull(3) ? DateTime.FromOADate(r.GetInt32(3)).ToString("yyyy-MM-dd") : null,
                                    StopDate = !r.IsDBNull(4) ? DateTime.FromOADate(r.GetInt32(4)).ToString("yyyy-MM-dd") : null,
                                    DsrAuthorizationTypeId = !r.IsDBNull(5) ? r.GetInt32(5) : -1
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <inheritdoc />
        public override List<TsReuseDelay> DaReuseDelaysGet()
        {
            var list = new List<TsReuseDelay>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "DoorAccess.DaReuseDelaysGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsReuseDelay()
                                {
                                    Id = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    Name = !r.IsDBNull(1) ? r.GetString(1) : null,
                                    Time = !r.IsDBNull(2) ? DateTime.FromOADate(r.GetDouble(2)).TimeOfDay : TimeSpan.Zero
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <inheritdoc />
        public override List<TsDoorAccessPlan> DaPlansGetByCustomer(int customerId)
        {
            var list = new List<TsDoorAccessPlan>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "DoorAccess.PlansGetByCustomer", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsDoorAccessPlan()
                                {
                                    Id = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    Name = !r.IsDBNull(1) ? r.GetString(1) : string.Empty,
                                    Active = !r.IsDBNull(2) && r.GetString(2) == "T",
                                    StartDate = !r.IsDBNull(3) ? DateTime.FromOADate(r.GetInt32(3)).ToString("MM/dd/yyyy") : null,
                                    StopDate = !r.IsDBNull(4) ? DateTime.FromOADate(r.GetInt32(4)).ToString("MM/dd/yyyy") : null,
                                    DsrAuthorizationTypeId = !r.IsDBNull(5) ? r.GetInt32(5) : -1
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <inheritdoc />
        public override List<TsDoorOverride> DoorOverridesGetByCustomer(int customerId)
        {
            var list = new List<TsDoorOverride>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "DoorAccess.DoorOverridesGetByCustomer", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsDoorOverride()
                                {
                                    Id = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    DoorId = !r.IsDBNull(1) ? r.GetInt32(1) : -1,
                                    StartDate = !r.IsDBNull(2) ? (DateTime?)DateTime.FromOADate(r.GetInt32(2)) : null,
                                    StopDate = !r.IsDBNull(3) ? (DateTime?)DateTime.FromOADate(r.GetInt32(3)) : null,
                                    OverrideType = (DoorOverrideType)(!r.IsDBNull(4) ? r.GetInt32(4) : -1),
                                    ScheduleId = !r.IsDBNull(5) ? r.GetInt32(5) : -1,
                                    DoorName = !r.IsDBNull(6) ? r.GetString(6) : null,
                                    DoorDescription = !r.IsDBNull(7) ? r.GetString(7) : null,
                                    DoorGroupName = !r.IsDBNull(8) ? r.GetString(8) : null,
                                    MerchantId = !r.IsDBNull(9) ? r.GetInt16(9) : (Int16)(-1),
                                    MerchantName = !r.IsDBNull(10) ? r.GetString(10) : null,
                                    BuildingName = !r.IsDBNull(11) ? r.GetString(11) : null,
                                    AreaName = !r.IsDBNull(12) ? r.GetString(12) : null,
                                    ScheduleName = !r.IsDBNull(13) ? r.GetString(13) : null,
                                    ReuseDelayId = !r.IsDBNull(14) ? (int?)r.GetInt32(14) : null,
                                    ReuseDelayName = !r.IsDBNull(15) ? r.GetString(15) : null,
                                    ReuseDelayObjectId = !r.IsDBNull(16) ? (int?)r.GetInt32(16) : null,
                                    ReuseDelayActive = !r.IsDBNull(17) && Formatting.TfStringToBool(r.GetString(17))
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <inheritdoc />
        public override List<TsDaPermissionBase> DaPermissionsGet(Int16 merchantId)
        {
            var list = new List<TsDaPermissionBase>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "DoorAccess.DaPermissionsGetByMerchant", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pMerchantId", OracleDbType = OracleDbType.Int16, Size = 4, Direction = ParameterDirection.Input, Value = merchantId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsDaPermissionBase()
                                {
                                    ScheduleId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    ScheduleName = !r.IsDBNull(1) ? r.GetString(1) : null
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <inheritdoc />
        public override List<TsDoorGroup> DoorGroupsGet(Int16 merchantId)
        {
            var list = new List<TsDoorGroup>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "DoorAccess.DoorGroupsGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pMerchantId", OracleDbType = OracleDbType.Int16, Size = 4, Direction = ParameterDirection.Input, Value = merchantId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsDoorGroup()
                                {
                                    Id = !r.IsDBNull(0) ? (int?)r.GetInt32(0) : null,
                                    Name = !r.IsDBNull(1) ? r.GetString(1) : null,
                                    DoorCount = !r.IsDBNull(2) ? r.GetInt32(2) : 0,
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <inheritdoc />
        public override List<TsEventGroup> EventGroupsGet(Int16 merchantId)
        {
            var list = new List<TsEventGroup>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "EventFunctions.EventGroupsGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pMerchantId", OracleDbType = OracleDbType.Int16, Size = 4, Direction = ParameterDirection.Input, Value = merchantId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsEventGroup()
                                {
                                    Id = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    Name = !r.IsDBNull(1) ? r.GetString(1) : null,
                                    Type = (EventGroupType)(!r.IsDBNull(2) ? r.GetInt32(2) : 0),
                                    IsActive = !r.IsDBNull(3) && Formatting.TfStringToBool(r.GetString(3)),
                                    CreationDate = !r.IsDBNull(4) ? (DateTime?)DateTime.FromOADate(r.GetDouble(4)) : null,
                                    MerchantId = merchantId
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <inheritdoc />
        public override List<Building> BuildingAreasGet()
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "DoorAccess.BuildingAreasGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            var list = new List<dynamic>();
                            while (r.Read())
                            {
                                list.Add(new
                                {
                                    BuildingId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    BuildingName = !r.IsDBNull(1) ? r.GetString(1) : null,
                                    AreaId = !r.IsDBNull(2) ? r.GetInt32(2) : -1,
                                    AreaName = !r.IsDBNull(3) ? r.GetString(3) : null
                                });
                            }

                            return list.GroupBy(x => x.BuildingId).Select(x => new Building()
                            {
                                Id = x.Key,
                                Name = x.First().BuildingName,
                                Areas = x.Select(y => new Area()
                                {
                                    Id = y.AreaId,
                                    Name = y.AreaName
                                }).ToList()
                            }).ToList();
                        }
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Get the host connection from a door id.
        /// </summary>
        /// <param name="doorId">the numberical identifier for the door.</param>
        /// <returns></returns>
        public override StatsConnection HostConnectionFromDoorIdGet(int doorId)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = @"
                        SELECT      HCN.IpAddress,
                                    DOR.Door_Id,
                                    RMC.MasterController_Id,
                                    DOR.Door_Identifier,
                                    DOR.Address  
                        FROM        HostConnections_Network HCN
                        INNER JOIN  HostConnections HCON          ON HCN.Connection_Id = HCON.CONNECTION_ID
                        INNER JOIN  Reader_Communication_Mac RCM  ON RCM.Connection_Id = HCN.CONNECTION_ID
                        INNER JOIN  Reader_MasterController RMC   ON RMC.Reader_Id = RCM.Reader_Id
                        INNER JOIN  Door DOR                      ON DOR.MasterController_Id = RMC.MasterController_Id
                        WHERE       HCON.ModuleChar = 'R'
                        AND         HCON.DeviceType = 8
                        AND         DOR.Door_Id =:pValue",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        cmd.Parameters.Add(
                            new OracleParameter { ParameterName = "pValue", OracleDbType = OracleDbType.Int32, Size = 6, Value = doorId, Direction = ParameterDirection.Input });
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            StatsConnection statsConnection = null;
                            while (r.Read())
                            {
                                var ipAddress = !r.IsDBNull(0) ? r.GetString(0) : string.Empty;
                                var fetchedDoorId = !r.IsDBNull(1) ? r.GetInt32(1) : -1;
                                var masterControllerId = !r.IsDBNull(2) ? r.GetInt32(2) : -1;
                                var doorName = !r.IsDBNull(3) ? r.GetString(3) : string.Empty;
                                var doorAddress = !r.IsDBNull(4) ? r.GetInt32(4) : -1;

                                statsConnection = new StatsConnection
                                {
                                    HostConnectionIpAddress = ipAddress,
                                    DoorId = fetchedDoorId,
                                    DoorName = doorName,
                                    MasterControllerId = masterControllerId,
                                    DoorAddress = (short)doorAddress
                                };
                            }
                            return statsConnection;
                        }
                    }
                    catch (Exception ex)
                    {
                        var rex = new ResourceLayerException(doorId.ToString(), Formatting.FormatException(ex));
                        throw rex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Get a list of host connection information for a set of doors defined by numerical id.
        /// </summary>
        /// <param name="request">List of the the numberical identifier for the doors.</param>
        /// <returns></returns>
        public override List<StatsConnection> HostConnectionListGet(DoorStateChangeRequest request)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                var command =
                    @"
                    SELECT HCN.IpAddress, 
                                HCN.IPPORT, 
                                DOR.Door_Id, 
                                RMC.MasterController_Id, 
                                DOR.Door_Identifier, 
                                DOR.Address 
                    FROM HostConnections_Network HCN 
                    INNER JOIN HostConnections HCON ON HCN.CONNECTION_ID = HCON.CONNECTION_ID 
                    INNER JOIN HostConnections HCON_BB ON HCON_BB.COMPUTER_ID = HCON.COMPUTER_ID 
                    INNER JOIN Reader_Communication_Mac RCM ON RCM.Connection_Id = HCON_BB.CONNECTION_ID 
                    INNER JOIN Reader_MasterController RMC ON RMC.Reader_Id = RCM.Reader_Id 
                    INNER JOIN Door DOR ON DOR.MasterController_Id = RMC.MasterController_Id 
                    WHERE HCON_BB.ModuleChar = 'R' 
                    AND HCON_BB.DeviceType = 8 
                    AND HCON.ModuleChar = 'R' 
                    AND HCON.DEVICETYPE = 11 
                    AND HCON.ISACTIVE = 'T'";
                command= $"{command}" +
                  $"AND         DOR.Door_Id in ({string.Join(",", request.DoorIds)})";
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = command,
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            var list = new List<StatsConnection>();
                            while (r.Read())
                            {
                                var ipAddress = !r.IsDBNull(0) ? r.GetString(0) : string.Empty;
                                var port = !r.IsDBNull(1) ? r.GetInt32(1) : -1;
                                var fetchedDoorId = !r.IsDBNull(2) ? r.GetInt32(2) : -1;
                                var masterControllerId = !r.IsDBNull(3) ? r.GetInt32(3) : -1;
                                var doorName = !r.IsDBNull(4) ? r.GetString(4) : string.Empty;
                                var doorAddress = !r.IsDBNull(5) ? r.GetInt32(5) : -1;

                                list.Add(new StatsConnection
                                {
                                    HostConnectionIpAddress = ipAddress,
                                    HostConnectionPort = port,
                                    DoorId = fetchedDoorId,
                                    DoorName = doorName,
                                    MasterControllerId = masterControllerId,
                                    DoorAddress = (short)doorAddress
                                });
                            }
                            return list;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }
    }
}
