﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using BbTS.Core;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Definitions.Transaction;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.Exceptions.Transaction;
using BbTS.Domain.Models.RetailTransaction;
using BbTS.Domain.Models.Retail_Tran;
using BbTS.Domain.Models.Retail_Tran_LineItem;
using BbTS.Domain.Models.Retail_Tran_LineItem_Comment;
using BbTS.Domain.Models.Retail_Tran_LineItem_Discount;
using BbTS.Domain.Models.Retail_Tran_LineItem_Mdb_Info;
using BbTS.Domain.Models.Retail_Tran_LineItem_Prod;
using BbTS.Domain.Models.Retail_Tran_LineItem_ProdPrcMd;
using BbTS.Domain.Models.Retail_Tran_LineItem_ProdPromo;
using BbTS.Domain.Models.Retail_Tran_LineItem_Prod_Tax;
using BbTS.Domain.Models.Retail_Tran_LineItem_Prod_Tx_E;
using BbTS.Domain.Models.Retail_Tran_LineItem_Prod_Tx_O;
using BbTS.Domain.Models.Retail_Tran_LineItem_StoredVal;
using BbTS.Domain.Models.Retail_Tran_LineItem_Surcharge;
using BbTS.Domain.Models.Retail_Tran_LineItem_Sv_Card;
using BbTS.Domain.Models.Retail_Tran_LineItem_Sv_Cust;
using BbTS.Domain.Models.Retail_Tran_LineItem_Sv_Enrich;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tax;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tender;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tender_Cc;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tender_Sv;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tndr_Card;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tndr_Cdv;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tndr_Ce;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tndr_Cgv;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tndr_Cust;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tndr_Disc;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tndr_Srch;
using BbTS.Domain.Models.Retail_Tran_Total;
using BbTS.Domain.Models.Session_Control;
using BbTS.Domain.Models.StoredValueDenial;
using BbTS.Domain.Models.Sv_Account_History_Rtl_Trn_Sv;
using BbTS.Domain.Models.Sv_Account_History_Rtl_Trn_Tdr;
using BbTS.Domain.Models.System.Database;
using BbTS.Domain.Models.TiaTransactionLog;
using BbTS.Domain.Models.Transaction;
using BbTS.Domain.Models.Transaction.Processing;
using BbTS.Domain.Models.TransactionObjectLog;
using BbTS.Domain.Models.Transaction_Cashier;
using BbTS.Domain.Models.Transaction_Communication;
using BbTS.Domain.Models.Transaction_External_Client;
using BbTS.Domain.Models.Transaction_Laundry_Machine;
using BbTS.Monitoring.Logging;
using Oracle.ManagedDataAccess.Client;
using BbTS.Domain.Models.Definitions.Container;
using BbTS.Resource.OracleManagedDataAccessExtensions;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        #region Credit_Card_Type

        /// <summary>
        /// This method will return a list of records for the CreditCardType objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsCredit_Card_Type> CreditCardTypeArchivesGet(ConnectionInfo connection)
        {
            var list = new List<TsCredit_Card_Type>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = @"SELECT  Credit_Card_Type_Id,
                                          Name,
                                          Description
                                  FROM    Envision.Credit_Card_Type",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsCredit_Card_Type();

                                if (!r.IsDBNull(0)) item.Credit_Card_Type_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Name = r.GetString(1);
                                if (!r.IsDBNull(2)) item.Description = r.GetString(2);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Retail_Tran Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_TranByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Retail_Tran WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_TranDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Retail_Tran SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran> Retail_TranListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsRetail_Tran>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.Retail_Tran_Type,
                                                                                                            T.Cashdrawer_Num,
                                                                                                            T.Unit_Count,
                                                                                                            T.LineItems_Scanned_Count,
                                                                                                            T.LineItems_Scanned_Percent,
                                                                                                            T.LineItems_Keyed_Count,
                                                                                                            T.LineItems_Keyed_Percent,
                                                                                                            T.Receipt_DateTime
                                                                                                 FROM       Envision.Retail_Tran T
                                                                                                 WHERE      T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsRetail_Tran();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Retail_Tran_Type = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Cashdrawer_Num = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.Unit_Count = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.LineItems_Scanned_Count = r.GetInt32(4);
                                if (!r.IsDBNull(5)) item.LineItems_Scanned_Percent = r.GetInt32(5);
                                if (!r.IsDBNull(6)) item.LineItems_Keyed_Count = r.GetInt32(6);
                                if (!r.IsDBNull(7)) item.LineItems_Keyed_Percent = r.GetInt32(7);
                                if (!r.IsDBNull(8)) item.Receipt_DateTime = DateTime.FromOADate(r.GetDouble(8));

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }


        #endregion

        #region Retail_Tran_LineItem Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItemByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Retail_Tran_LineItem WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItemDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Retail_Tran_LineItem SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem> Retail_Tran_LineItemListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsRetail_Tran_LineItem>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.LineItem_SequenceNumber,
                                                                                                            T.Void_Flag
                                                                                                  FROM      Envision.Retail_Tran_LineItem T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsRetail_Tran_LineItem();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.LineItem_SequenceNumber = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Void_Flag = r.GetString(2);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Retail_Tran_LineItem

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_CommentByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Retail_Tran_LineItem_Comment WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_CommentDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Retail_Tran_LineItem_Comment SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }


        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Comment> Retail_Tran_LineItem_CommentListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsRetail_Tran_LineItem_Comment>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.LineItem_SequenceNumber,
                                                                                                            T.Comment_Text
                                                                                                  FROM      Envision.Retail_Tran_LineItem_Comment T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsRetail_Tran_LineItem_Comment();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.LineItem_SequenceNumber = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Comment_Text = r.GetString(2);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }


        #endregion

        #region Retail_Tran_LineItem_Discount

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_DiscountByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Retail_Tran_LineItem_Discount WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_DiscountDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Retail_Tran_LineItem_Discount SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Discount> Retail_Tran_LineItem_DiscountListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsRetail_Tran_LineItem_Discount>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.LineItem_SequenceNumber,
                                                                                                            T.KeyNum,
                                                                                                            T.Percentage,
                                                                                                            T.RoundingAmount,
                                                                                                            T.Amount,
                                                                                                            T.Prorated_Flag,
                                                                                                            T.Calculation_Type
                                                                                                  FROM      Envision.Retail_Tran_LineItem_Discount T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsRetail_Tran_LineItem_Discount();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.LineItem_SequenceNumber = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.KeyNum = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.Percentage = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.RoundingAmount = DateTime.FromOADate(r.GetDouble(4));
                                if (!r.IsDBNull(5)) item.Amount = r.GetInt32(5);
                                if (!r.IsDBNull(6)) item.Prorated_Flag = r.GetString(6);
                                if (!r.IsDBNull(7)) item.Calculation_Type = r.GetInt32(7);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Retail_Tran_LineItem_Mdb_Info

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_Mdb_InfoByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Retail_Tran_LineItem_Mdb_Info WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_Mdb_InfoDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Retail_Tran_LineItem_Mdb_Info SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Mdb_Info> Retail_Tran_LineItem_Mdb_InfoListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsRetail_Tran_LineItem_Mdb_Info>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.Lineitem_SequenceNumber,
                                                                                                            T.Itemnum_Value
                                                                                                  FROM      Envision.Retail_Tran_LineItem_Mdb_Info T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsRetail_Tran_LineItem_Mdb_Info();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Lineitem_SequenceNumber = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Itemnum_Value = r.GetString(2);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Retail_Tran_LineItem_Prod Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_ProdByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Retail_Tran_LineItem_Prod WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_ProdDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Retail_Tran_LineItem_Prod SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Prod> Retail_Tran_LineItem_ProdListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsRetail_Tran_LineItem_Prod>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.LineItem_SequenceNumber,
                                                                                                            T.ProdDetail_Id,
                                                                                                            T.TaxSchedule_Id,
                                                                                                            T.TaxGroup_Id,
                                                                                                            T.RetailPrice,
                                                                                                            T.RetailPrice_Quantity,
                                                                                                            T.ActualUnit_Price,
                                                                                                            T.ActualUnit_Price_Quantity,
                                                                                                            T.Quantity,
                                                                                                            T.Units,
                                                                                                            T.Unit_MeasureType,
                                                                                                            T.Extended_Amount,
                                                                                                            T.Unit_Discount_Amount,
                                                                                                            T.Extended_Discount_Amount,
                                                                                                            T.Product_EntryMethod_Type,
                                                                                                            T.RetailPrice_EntryMethod_Type,
                                                                                                            T.Unit_CostPrice,
                                                                                                            T.Unit_CostPrice_Quantity,
                                                                                                            T.Unit_ListPrice,
                                                                                                            T.Unit_ListPrice_Quantity,
                                                                                                            T.ProdPromo_Flag,
                                                                                                            T.ProdPromo_Price,
                                                                                                            T.ProdPromo_Id,
                                                                                                            T.RetailPrice_Modified_Flag
                                                                                                  FROM      Envision.Retail_Tran_LineItem_Prod T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsRetail_Tran_LineItem_Prod();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.LineItem_SequenceNumber = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.ProdDetail_Id = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.TaxSchedule_Id = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.TaxGroup_Id = r.GetInt32(4);
                                if (!r.IsDBNull(5)) item.RetailPrice = r.GetInt32(5);
                                if (!r.IsDBNull(6)) item.RetailPrice_Quantity = r.GetInt32(6);
                                if (!r.IsDBNull(7)) item.ActualUnit_Price = r.GetInt32(7);
                                if (!r.IsDBNull(8)) item.ActualUnit_Price_Quantity = r.GetInt32(8);
                                if (!r.IsDBNull(9)) item.Quantity = r.GetInt32(9);
                                if (!r.IsDBNull(10)) item.Units = r.GetInt32(10);
                                if (!r.IsDBNull(11)) item.Unit_MeasureType = r.GetInt32(11);
                                if (!r.IsDBNull(12)) item.Extended_Amount = r.GetInt32(12);
                                if (!r.IsDBNull(13)) item.Unit_Discount_Amount = r.GetInt32(13);
                                if (!r.IsDBNull(14)) item.Extended_Discount_Amount = r.GetInt32(14);
                                if (!r.IsDBNull(15)) item.Product_EntryMethod_Type = r.GetInt32(15);
                                if (!r.IsDBNull(16)) item.RetailPrice_EntryMethod_Type = r.GetInt32(16);
                                if (!r.IsDBNull(17)) item.Unit_CostPrice = r.GetInt32(17);
                                if (!r.IsDBNull(18)) item.Unit_CostPrice_Quantity = r.GetInt32(18);
                                if (!r.IsDBNull(19)) item.Unit_ListPrice = r.GetInt32(19);
                                if (!r.IsDBNull(20)) item.Unit_ListPrice_Quantity = r.GetInt32(20);
                                if (!r.IsDBNull(21)) item.ProdPromo_Flag = r.GetString(21);
                                if (!r.IsDBNull(22)) item.ProdPromo_Price = r.GetInt32(22);
                                if (!r.IsDBNull(23)) item.ProdPromo_Id = r.GetInt32(23);
                                if (!r.IsDBNull(24)) item.RetailPrice_Modified_Flag = r.GetString(24);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }


        #endregion

        #region Retail_Tran_LineItem_Prod_Tax Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_Prod_TaxByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Retail_Tran_LineItem_Prod_Tax WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_Prod_TaxDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Retail_Tran_LineItem_Prod_Tax SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Prod_Tax> Retail_Tran_LineItem_Prod_TaxListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsRetail_Tran_LineItem_Prod_Tax>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.LineItem_SequenceNumber,
                                                                                                            T.TaxSchedule_Id,
                                                                                                            T.Taxable_Percent,
                                                                                                            T.Tax_Amount,
                                                                                                            T.Tax_Percent,
                                                                                                            T.Tax_Amount,
                                                                                                            T.Tax_Exempt_Override_Type
                                                                                                  FROM      Envision.Retail_Tran_LineItem_Prod_Tax T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsRetail_Tran_LineItem_Prod_Tax();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.LineItem_SequenceNumber = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.TaxSchedule_Id = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.Taxable_Percent = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.Tax_Amount = r.GetInt32(4);
                                if (!r.IsDBNull(5)) item.Tax_Percent = r.GetInt32(5);
                                if (!r.IsDBNull(6)) item.Tax_Amount = r.GetInt32(6);
                                if (!r.IsDBNull(7)) item.Tax_Exempt_Override_Type = r.GetInt32(7);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Retail_Tran_LineItem_Prod_Tx_E Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_Prod_Tx_EByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Retail_Tran_LineItem_Prod_Tx_E WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_Prod_Tx_EDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Retail_Tran_LineItem_Prod_Tx_E SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Prod_Tx_E> Retail_Tran_LineItem_Prod_Tx_EListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsRetail_Tran_LineItem_Prod_Tx_E>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.LineItem_SequenceNumber,
                                                                                                            T.TaxSchedule_Id,
                                                                                                            T.Tax_Exempt_Reason_Type,
                                                                                                            T.Exempt_Taxable_Amount,
                                                                                                            T.Exempt_Tax_Amount
                                                                                                  FROM      Envision.Retail_Tran_LineItem_Prod_Tx_E T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsRetail_Tran_LineItem_Prod_Tx_E();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.LineItem_SequenceNumber = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.TaxSchedule_Id = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.Tax_Exempt_Reason_Type = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.Exempt_Taxable_Amount = r.GetInt32(4);
                                if (!r.IsDBNull(5)) item.Exempt_Tax_Amount = r.GetInt32(5);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Retail_Tran_LineItem_Prod_Tx_O Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_Prod_Tx_OByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Retail_Tran_LineItem_Prod_Tx_O WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_Prod_Tx_ODateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Retail_Tran_LineItem_Prod_Tx_O SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Prod_Tx_O> Retail_Tran_LineItem_Prod_Tx_OListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsRetail_Tran_LineItem_Prod_Tx_O>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.LineItem_SequenceNumber,
                                                                                                            T.TaxSchedule_Id,
                                                                                                            T.Tax_Override_Reason_Type,
                                                                                                            T.Taxable_Amount,
                                                                                                            T.Original_Tax_Amount,
                                                                                                            T.New_Tax_Amount,
                                                                                                            T.Original_Tax_Percent,
                                                                                                            T.New_Tax_Percent
                                                                                                  FROM      Envision.Retail_Tran_LineItem_Prod_Tx_O T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsRetail_Tran_LineItem_Prod_Tx_O();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.LineItem_SequenceNumber = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.TaxSchedule_Id = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.Tax_Override_Reason_Type = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.Taxable_Amount = r.GetInt32(4);
                                if (!r.IsDBNull(5)) item.Original_Tax_Amount = r.GetInt32(5);
                                if (!r.IsDBNull(6)) item.New_Tax_Amount = r.GetInt32(6);
                                if (!r.IsDBNull(7)) item.Original_Tax_Percent = r.GetInt32(7);
                                if (!r.IsDBNull(8)) item.New_Tax_Percent = r.GetInt32(8);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Retail_Tran_LineItem_ProdPrCmd Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_ProdPrcMdByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Retail_Tran_LineItem_ProdPrcMd WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_ProdPrcMdDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Retail_Tran_LineItem_ProdPrcMd SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_ProdPrcMd> Retail_Tran_LineItem_ProdPrcMdListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsRetail_Tran_LineItem_ProdPrcMd>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.LineItem_SequenceNumber,
                                                                                                            T.PriceModifier_SequenceNumber,
                                                                                                            T.RetailPrice_Modifier_Type,
                                                                                                            T.ProdPromo_Id,
                                                                                                            T.Previous_Price,
                                                                                                            T.Percent,
                                                                                                            T.Amount,
                                                                                                            T.Calculation_Method_Type,
                                                                                                            T.New_Price
                                                                                                  FROM      Envision.Retail_Tran_LineItem_ProdPrcMd T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsRetail_Tran_LineItem_ProdPrcMd();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.LineItem_SequenceNumber = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.PriceModifier_SequenceNumber = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.RetailPrice_Modifier_Type = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.ProdPromo_Id = r.GetInt32(4);
                                if (!r.IsDBNull(5)) item.Previous_Price = r.GetInt32(5);
                                if (!r.IsDBNull(6)) item.Percent = r.GetInt32(6);
                                if (!r.IsDBNull(7)) item.Amount = r.GetInt32(7);
                                if (!r.IsDBNull(8)) item.Calculation_Method_Type = r.GetInt32(8);
                                if (!r.IsDBNull(9)) item.New_Price = r.GetInt32(9);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Retail_Tran_LineItem_ProdPromo

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_ProdPromoByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Retail_Tran_LineItem_ProdPromo WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_ProdPromoDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Retail_Tran_LineItem_ProdPromo SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_ProdPromo> Retail_Tran_LineItem_ProdPromoListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsRetail_Tran_LineItem_ProdPromo>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.LineItem_SequenceNumber,
                                                                                                            T.ProdPromo_Id
                                                                                                  FROM      Envision.Retail_Tran_LineItem_ProdPromo T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsRetail_Tran_LineItem_ProdPromo();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.LineItem_SequenceNumber = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.ProdPromo_Id = r.GetInt32(2);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Retail_Tran_LineItem_StoredVal Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_StoredValByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Retail_Tran_LineItem_StoredVal WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_StoredValDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Retail_Tran_LineItem_StoredVal SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_StoredVal> Retail_Tran_LineItem_StoredValListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsRetail_Tran_LineItem_StoredVal>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.LineItem_SequenceNumber,
                                                                                                            T.StoredValue_Amount,
                                                                                                            T.Card_Purchase_Fee_Amount,
                                                                                                            T.ActualUnit_Price,
                                                                                                            T.UnitCost_Price,
                                                                                                            T.Sv_Account_Id,
                                                                                                            T.Enrichment_Flag,
                                                                                                            T.Enrichment_Parent_LineItem_Seq
                                                                                                  FROM      Envision.Retail_Tran_LineItem_StoredVal T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsRetail_Tran_LineItem_StoredVal();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.LineItem_SequenceNumber = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.StoredValue_Amount = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.Card_Purchase_Fee_Amount = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.ActualUnit_Price = r.GetInt32(4);
                                if (!r.IsDBNull(5)) item.UnitCost_Price = r.GetInt32(5);
                                if (!r.IsDBNull(6)) item.Sv_Account_Id = r.GetInt32(6);
                                if (!r.IsDBNull(7)) item.Enrichment_Flag = r.GetString(7);
                                if (!r.IsDBNull(8)) item.Enrichment_Parent_LineItem_Seq = r.GetInt32(8);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }


        #endregion

        #region Retail_Tran_LineItem_Surcharge Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_SurchargeByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Retail_Tran_LineItem_Surcharge WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }


        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_SurchargeDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Retail_Tran_LineItem_Surcharge SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }


        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Surcharge> Retail_Tran_LineItem_SurchargeListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsRetail_Tran_LineItem_Surcharge>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.LineItem_SequenceNumber,
                                                                                                            T.KeyNum,
                                                                                                            T.Percentage,
                                                                                                            T.RoundingAmount,
                                                                                                            T.Amount,
                                                                                                            T.Prorated_Flag,
                                                                                                            T.Calculation_Type
                                                                                                  FROM      Envision.Retail_Tran_LineItem_Surcharge T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsRetail_Tran_LineItem_Surcharge();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.LineItem_SequenceNumber = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.KeyNum = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.Percentage = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.RoundingAmount = DateTime.FromOADate(r.GetDouble(4));
                                if (!r.IsDBNull(5)) item.Amount = r.GetInt32(5);
                                if (!r.IsDBNull(6)) item.Prorated_Flag = r.GetString(6);
                                if (!r.IsDBNull(7)) item.Calculation_Type = r.GetInt32(7);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Retail_Tran_LineItem_Sv_Card Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_Sv_CardByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Retail_Tran_LineItem_Sv_Card WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_Sv_CardDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Retail_Tran_LineItem_Sv_Card SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Sv_Card> Retail_Tran_LineItem_Sv_CardListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsRetail_Tran_LineItem_Sv_Card>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.LineItem_SequenceNumber,
                                                                                                            T.CardNum,
                                                                                                            T.Issue_Number,
                                                                                                            T.Issue_Number_Captured
                                                                                                  FROM      Envision.Retail_Tran_LineItem_Sv_Card T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsRetail_Tran_LineItem_Sv_Card();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.LineItem_SequenceNumber = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.CardNum = r.GetString(2);
                                if (!r.IsDBNull(3)) item.Issue_Number = r.GetString(3);
                                if (!r.IsDBNull(4)) item.Issue_Number_Captured = r.GetString(4);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Retail_Tran_LineItem_Sv_Cust Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_Sv_CustByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Retail_Tran_LineItem_Sv_Cust WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_Sv_CustDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Retail_Tran_LineItem_Sv_Cust SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Sv_Cust> Retail_Tran_LineItem_Sv_CustListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsRetail_Tran_LineItem_Sv_Cust>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.LineItem_SequenceNumber,
                                                                                                            T.Physical_Id_Type,
                                                                                                            T.Cust_EntryMethod_Type,
                                                                                                            T.Secondary_Auth_Entered,
                                                                                                            T.Cust_Id
                                                                                                  FROM      Envision.Retail_Tran_LineItem_Sv_Cust T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsRetail_Tran_LineItem_Sv_Cust();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.LineItem_SequenceNumber = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Physical_Id_Type = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.Cust_EntryMethod_Type = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.Secondary_Auth_Entered = r.GetString(4);
                                if (!r.IsDBNull(5)) item.Cust_Id = r.GetInt32(5);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Retail_Tran_LineItem_Sv_Enrich Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_Sv_EnrichByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Retail_Tran_LineItem_Sv_Enrich WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_Sv_EnrichDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Retail_Tran_LineItem_Sv_Enrich SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Sv_Enrich> Retail_Tran_LineItem_Sv_EnrichListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsRetail_Tran_LineItem_Sv_Enrich>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.LineItem_SequenceNumber,
                                                                                                            T.Sv_Enrich_SequenceNumber,
                                                                                                            T.Percentage,
                                                                                                            T.RoundingAmount,
                                                                                                            T.Amount
                                                                                                  FROM      Envision.Retail_Tran_LineItem_Sv_Enrich T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsRetail_Tran_LineItem_Sv_Enrich();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.LineItem_SequenceNumber = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Sv_Enrich_SequenceNumber = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.Percentage = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.RoundingAmount = DateTime.FromOADate(r.GetDouble(4));
                                if (!r.IsDBNull(5)) item.Amount = r.GetInt32(5);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Retail_Tran_LineItem_Tax Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_TaxByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Retail_Tran_LineItem_Tax WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_TaxDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Retail_Tran_LineItem_Tax SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Tax> Retail_Tran_LineItem_TaxListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsRetail_Tran_LineItem_Tax>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.LineItem_SequenceNumber,
                                                                                                            T.Tax_KeyNum,
                                                                                                            T.Tax_Type,
                                                                                                            T.Taxable_Percent,
                                                                                                            T.Taxable_Amount,
                                                                                                            T.Tax_Percent,
                                                                                                            T.Tax_Amount
                                                                                                  FROM      Envision.Retail_Tran_LineItem_Tax T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsRetail_Tran_LineItem_Tax();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.LineItem_SequenceNumber = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Tax_KeyNum = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.Tax_Type = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.Taxable_Percent = r.GetInt32(4);
                                if (!r.IsDBNull(5)) item.Taxable_Amount = r.GetInt32(5);
                                if (!r.IsDBNull(6)) item.Tax_Percent = r.GetInt32(6);
                                if (!r.IsDBNull(7)) item.Tax_Amount = r.GetInt32(7);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Retail_Tran_LineItem_Tender Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_TenderByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Retail_Tran_LineItem_Tender WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_TenderDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Retail_Tran_LineItem_Tender SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Tender> Retail_Tran_LineItem_TenderListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsRetail_Tran_LineItem_Tender>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.LineItem_SequenceNumber,
                                                                                                            T.Tender_Id,
                                                                                                            T.Tender_Amount,
                                                                                                            T.Tip_Amount,
                                                                                                            T.RoundingAmount
                                                                                                  FROM      Envision.Retail_Tran_LineItem_Tender T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsRetail_Tran_LineItem_Tender();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.LineItem_SequenceNumber = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Tender_Id = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.Tender_Amount = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.Tip_Amount = r.GetInt32(4);
                                if (!r.IsDBNull(5)) item.RoundingAmount = r.GetInt32(5);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Retail_Tran_LineItem_Tender_Cc Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_Tender_CcByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Retail_Tran_LineItem_Tender_Cc WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_Tender_CcDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Retail_Tran_LineItem_Tender_Cc SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Tender_Cc> Retail_Tran_LineItem_Tender_CcListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsRetail_Tran_LineItem_Tender_Cc>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.LineItem_SequenceNumber,
                                                                                                            T.Credit_Card_Type_Id,
                                                                                                            T.Credit_Card_Iin,
                                                                                                            T.Credit_Card_Last4,
                                                                                                            T.CreditCardReferenceId,
                                                                                                            T.CreditCardProcessingMethodId
                                                                                                  FROM      Envision.Retail_Tran_LineItem_Tender_Cc T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsRetail_Tran_LineItem_Tender_Cc();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.LineItem_SequenceNumber = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Credit_Card_Type_Id = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.Credit_Card_Iin = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.Credit_Card_Last4 = r.GetInt32(4);
                                if (!r.IsDBNull(5)) item.CreditCardReferenceId = r.GetInt32(5);
                                if (!r.IsDBNull(6)) item.CreditCardProcessingMethodId = r.GetInt32(6);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Retail_Tran_LineItem_Tender_Sv Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_Tender_SvByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Retail_Tran_LineItem_Tender_Sv WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_Tender_SvDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Retail_Tran_LineItem_Tender_Sv SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Tender_Sv> Retail_Tran_LineItem_Tender_SvListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsRetail_Tran_LineItem_Tender_Sv>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.LineItem_SequenceNumber
                                                                                                  FROM      Envision.Retail_Tran_LineItem_Tender_Sv T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsRetail_Tran_LineItem_Tender_Sv();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.LineItem_SequenceNumber = r.GetInt32(1);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Retail_Tran_LineItem_Tndr_Card Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_Tndr_CardByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Retail_Tran_LineItem_Tndr_Card WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_Tndr_CardDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Retail_Tran_LineItem_Tndr_Card SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Tndr_Card> Retail_Tran_LineItem_Tndr_CardListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsRetail_Tran_LineItem_Tndr_Card>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.LineItem_SequenceNumber,
                                                                                                            T.CardNum,
                                                                                                            T.Issue_Number,
                                                                                                            T.Issue_Number_Captured
                                                                                                  FROM      Envision.Retail_Tran_LineItem_Tndr_Card T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsRetail_Tran_LineItem_Tndr_Card();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.LineItem_SequenceNumber = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.CardNum = r.GetString(2);
                                if (!r.IsDBNull(3)) item.Issue_Number = r.GetString(3);
                                if (!r.IsDBNull(4)) item.Issue_Number_Captured = r.GetString(4);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Retail_Tran_LineItem_Tndr_Cdv Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_Tndr_CdvByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Retail_Tran_LineItem_Tndr_Cdv WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_Tndr_CdvDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Retail_Tran_LineItem_Tndr_Cdv SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Tndr_Cdv> Retail_Tran_LineItem_Tndr_CdvListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsRetail_Tran_LineItem_Tndr_Cdv>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.LineItem_SequenceNumber,
                                                                                                            T.Customer_Def_Field_Def_Id,
                                                                                                            T.Field_Value
                                                                                                  FROM      Envision.Retail_Tran_LineItem_Tndr_Cdv T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsRetail_Tran_LineItem_Tndr_Cdv();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.LineItem_SequenceNumber = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Customer_Def_Field_Def_Id = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.Field_Value = r.GetString(3);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Retail_Tran_LineItem_Tndr_Ce Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_Tndr_CeByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Retail_Tran_LineItem_Tndr_Ce WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_Tndr_CeDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Retail_Tran_LineItem_Tndr_Ce SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Tndr_Ce> Retail_Tran_LineItem_Tndr_CeListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsRetail_Tran_LineItem_Tndr_Ce>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.LineItem_SequenceNumber,
                                                                                                            T.CashEquiv_Amount_Limit,
                                                                                                            T.BoardMt_Id,
                                                                                                            T.GuestMeal
                                                                                                  FROM      Envision.Retail_Tran_LineItem_Tndr_Ce T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsRetail_Tran_LineItem_Tndr_Ce();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.LineItem_SequenceNumber = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.CashEquiv_Amount_Limit = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.BoardMt_Id = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.GuestMeal = r.GetString(4);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Retail_Tran_LineItem_Tndr_Cgv Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_Tndr_CgvByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Retail_Tran_LineItem_Tndr_Cgv WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_Tndr_CgvDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Retail_Tran_LineItem_Tndr_Cgv SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Tndr_Cgv> Retail_Tran_LineItem_Tndr_CgvListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsRetail_Tran_LineItem_Tndr_Cgv>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.LineItem_SequenceNumber,
                                                                                                            T.Customer_Def_Grp_Def_Id,
                                                                                                            T.Customer_Def_Grp_Def_Item_Id
                                                                                                  FROM      Envision.Retail_Tran_LineItem_Tndr_Cgv T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsRetail_Tran_LineItem_Tndr_Cgv();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.LineItem_SequenceNumber = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Customer_Def_Grp_Def_Id = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.Customer_Def_Grp_Def_Item_Id = r.GetInt32(3);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Retail_Tran_LineItem_Tndr_Cust Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_Tndr_CustByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Retail_Tran_LineItem_Tndr_Cust WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_Tndr_CustDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Retail_Tran_LineItem_Tndr_Cust SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Tndr_Cust> Retail_Tran_LineItem_Tndr_CustListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsRetail_Tran_LineItem_Tndr_Cust>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.LineItem_SequenceNumber,
                                                                                                            T.Physical_Id_Type,
                                                                                                            T.Cust_EntryMethod_Type,
                                                                                                            T.Secondary_Auth_Entered,
                                                                                                            T.Cust_Id
                                                                                                  FROM      Envision.Retail_Tran_LineItem_Tndr_Cust T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsRetail_Tran_LineItem_Tndr_Cust();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.LineItem_SequenceNumber = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Physical_Id_Type = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.Cust_EntryMethod_Type = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.Secondary_Auth_Entered = r.GetString(4);
                                if (!r.IsDBNull(5)) item.Cust_Id = r.GetInt32(5);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Retail_Tran_LineItem_Tndr_Disc Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_Tndr_DiscByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Retail_Tran_LineItem_Tndr_Disc WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_Tndr_DiscDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Retail_Tran_LineItem_Tndr_Disc SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Tndr_Disc> Retail_Tran_LineItem_Tndr_DiscListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsRetail_Tran_LineItem_Tndr_Disc>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.LineItem_SequenceNumber,
                                                                                                            T.Tndr_Disc_SequenceNumber,
                                                                                                            T.Discount_Reason_Type,
                                                                                                            T.Percentage,
                                                                                                            T.RoundingAmount,
                                                                                                            T.Amount,
                                                                                                            T.Prorated_Flag,
                                                                                                            T.Calculation_Type
                                                                                                  FROM      Envision.Retail_Tran_LineItem_Tndr_Disc T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsRetail_Tran_LineItem_Tndr_Disc();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.LineItem_SequenceNumber = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Tndr_Disc_SequenceNumber = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.Discount_Reason_Type = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.Percentage = r.GetInt32(4);
                                if (!r.IsDBNull(5)) item.RoundingAmount = DateTime.FromOADate(r.GetDouble(5));
                                if (!r.IsDBNull(6)) item.Amount = r.GetInt32(6);
                                if (!r.IsDBNull(7)) item.Prorated_Flag = r.GetString(7);
                                if (!r.IsDBNull(8)) item.Calculation_Type = r.GetInt32(8);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Retail_Tran_LineItem_Tndr_Srch Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_Tndr_SrchByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Retail_Tran_LineItem_Tndr_Srch WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_Tndr_SrchDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Retail_Tran_LineItem_Tndr_Srch SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Tndr_Srch> Retail_Tran_LineItem_Tndr_SrchListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsRetail_Tran_LineItem_Tndr_Srch>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.LineItem_SequenceNumber,
                                                                                                            T.Tndr_Srch_SequenceNumber,
                                                                                                            T.Surcharge_Reason_Type,
                                                                                                            T.Percentage,
                                                                                                            T.RoundingAmount,
                                                                                                            T.Amount,
                                                                                                            T.Prorated_Flag,
                                                                                                            T.Calculation_Type
                                                                                                  FROM      Envision.Retail_Tran_LineItem_Tndr_Srch T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsRetail_Tran_LineItem_Tndr_Srch();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.LineItem_SequenceNumber = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Tndr_Srch_SequenceNumber = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.Surcharge_Reason_Type = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.Percentage = r.GetInt32(4);
                                if (!r.IsDBNull(5)) item.RoundingAmount = DateTime.FromOADate(r.GetDouble(5));
                                if (!r.IsDBNull(6)) item.Amount = r.GetInt32(6);
                                if (!r.IsDBNull(7)) item.Prorated_Flag = r.GetString(7);
                                if (!r.IsDBNull(8)) item.Calculation_Type = r.GetInt32(8);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Retail_Tran_Total Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_TotalByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Retail_Tran_Total WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_TotalDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Retail_Tran_Total SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_Total> Retail_Tran_TotalListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsRetail_Tran_Total>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.Amount
                                                                                                  FROM      Envision.Retail_Tran_Total T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsRetail_Tran_Total();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Amount = r.GetInt32(1);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Retail Transaction Processing functions (Slate functions)

        /// <summary>
        /// Process a retail transaction begin request against the resource layer.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The results of the operation.</returns>
        public override RetailTransactionBeginProcessResponse RetailTransactionBeginProcess(RetailTransactionBeginProcessRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "TransactionFunctions.RetailTransactionBegin", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        double? suspendUnsuspendDateTime = null;
                        if (request.SuspendedUnsuspendDatetime != null)
                        {
                            suspendUnsuspendDateTime = request.SuspendedUnsuspendDatetime.Value.ToOADate();
                        }
                        cmd.Parameters.Add(new OracleParameter("pDeviceGuid", OracleDbType.Varchar2, 36, request.DeviceGuid, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTransactionNumber", OracleDbType.Int32, 10, request.TransactionNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTransactionDateTime", OracleDbType.Double, 14, request.TransactionDateTime.ToOADate(), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pCancelledFlag", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.CancelledFlag), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pSuspendedFlag", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.SuspendedFlag), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pSuspendedUnsuspendDatetime", OracleDbType.Double, 14, suspendUnsuspendDateTime, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTrainingFlag", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.TrainingFlag), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pKeyedOfflineFlag", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.KeyedOfflineFlag), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAttendedType", OracleDbType.Int32, 10, request.AttendedType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pRetailTranType", OracleDbType.Int32, 10, request.RetailTranType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pOperatorGuid", OracleDbType.Varchar2, 36, request.OperatorGuid, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pCashdrawerNumber", OracleDbType.Int32, 10, request.CashdrawerNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pControlTotal", OracleDbType.Decimal, 19, request.ControlTotalAfterTransaction, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pChangeInControlTotal", OracleDbType.Decimal, 10, request.ChangeInControlTotal, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTransactionId", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pErrorCode", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pDeniedText", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 75 });

                        cmd.Parameters.Add(new OracleParameter("pTransactionGuid", OracleDbType.Varchar2, 36, request.TransactionGuid, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pSessionGuid", OracleDbType.Varchar2, 36, request.SessionGuid, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pControlTotalBeforeTransaction", OracleDbType.Decimal, 19, request.ControlTotalBeforeTransaction, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pControlTotalAfterTransaction", OracleDbType.Decimal, 19, request.ControlTotalAfterTransaction, ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();

                        var transactionId = cmd.Parameters["pTransactionId"].Value.ToString();
                        var deniedText = cmd.Parameters["pDeniedText"].Value.ToString();
                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);
                        return new RetailTransactionBeginProcessResponse(request.RequestId)
                        {
                            DeniedText = deniedText,
                            ErrorCode = errorCode,
                            TransactionId = int.Parse(transactionId)
                        };
                    }
                    catch (Exception ex)
                    {
                        var message = Formatting.FormatException(ex);
                        var lex = new LineItemProcessingException(
                            new LineItemProcessingResult(request.RequestId, 1005, message, TransactionLineItemType.LineItemRetailTransactionBegin),
                            message);
                        LoggingManager.Instance.LogLineItemProcessingException(
                            lex, request, EventLogEntryType.Error,
                            LoggingDefinitions.Category.Resource, LoggingDefinitions.EventId.WebApiTransactionProcessing);
                        throw lex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Process a retail transaction line item product price modifier request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The results of the operation.</returns>
        public override RetailTransactionLineProductPriceModifierResponse RetailTransactionLineProductPriceModifierProcess(RetailTransactionLineProductPriceModifierRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "TransactionFunctions.ProcessRtlTranLineProdPrcMod", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pTransactionId", OracleDbType.Int32, 15, request.TransactionId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pLineItemSequenceNumber", OracleDbType.Int32, 4, request.LineItemSequenceNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pPriceModifierSequenceNumber", OracleDbType.Int32, 4, request.PriceModifierSequenceNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pRetailPriceModifierType", OracleDbType.Int32, 4, request.RetailPriceModifierType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pProdPromoId", OracleDbType.Int32, 6, request.ProductPromoId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pPreviousPrice", OracleDbType.Int32, 12, request.PreviousPrice * 1000, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pPercent", OracleDbType.Int32, 6, request.Percent, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAmount", OracleDbType.Int32, 12, request.Amount * 1000, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pCalculationMethodType", OracleDbType.Int32, 4, request.CalculationMethodType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pNewPrice", OracleDbType.Int32, 12, request.NewPrice * 1000, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pOnSuccessCommit", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.OnSuccessCommit), ParameterDirection.Input));

                        cmd.Parameters.Add(new OracleParameter("pErrorCode", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pDeniedText", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 75 });

                        con.Open();
                        cmd.ExecuteNonQuery();
                        
                        var deniedText = cmd.Parameters["pDeniedText"].Value.ToString();
                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);

                        return new RetailTransactionLineProductPriceModifierResponse
                        {
                            RequestId =  request.RequestId,
                            DeniedText = deniedText,
                            ErrorCode = errorCode
                        };
                    }
                    catch (Exception ex)
                    {
                        var message = Formatting.FormatException(ex);
                        var lex = new LineItemProcessingException(
                            new LineItemProcessingResult(request.RequestId, 1005, message, TransactionLineItemType.LineItemProduct),
                            message);
                        LoggingManager.Instance.LogLineItemProcessingException(
                            lex, request, EventLogEntryType.Error,
                            LoggingDefinitions.Category.Resource, LoggingDefinitions.EventId.WebApiTransactionProcessing);
                        throw lex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Process a retail transaction line item product process request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The results of the operation.</returns>
        public override RetailTransactionLineProductProcessResponse RetailTransactionLineProductProcess(RetailTransactionLineProductProcessRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "TransactionFunctions.ProcessRtlTranLineProduct", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pTransactionId", OracleDbType.Int32, 15, request.TransactionId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pLineItemSequenceNumber", OracleDbType.Int32, 4, request.LineItemSequenceNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pVoidFlad", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.VoidFlag), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pProdDetailId", OracleDbType.Int32, 6, request.ProdDetailId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTaxScheduleId", OracleDbType.Int32, 6, request.TaxScheduleId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTaxGroupId", OracleDbType.Int32, 6, request.TaxGroupId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pRetailPrice", OracleDbType.Int32, 12, (int)(request.RetailPrice * 1000), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pRetailPriceQty", OracleDbType.Int32, 4, request.RetailPriceQty, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pActualUnitPrice", OracleDbType.Int32, 12, (int)(request.ActualUnitPrice * 1000), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pActualUnitPriceQty", OracleDbType.Int32, 4, request.ActualUnitPriceQty, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pQuantity", OracleDbType.Int32, 4, request.Quantity, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pUnits", OracleDbType.Double, 8, (double)request.Units, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pUnitMeasureType", OracleDbType.Int32, 4, (int)request.UnitMeasureType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pExtendedAmount", OracleDbType.Int32, 12, (int)(request.ExtendedAmount * 1000), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pUnitDiscountAmount", OracleDbType.Int32, 12, (int)(request.UnitDiscountAmount * 1000), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pExtendedDiscountAmount", OracleDbType.Int32, 12, (int)(request.ExtendedDiscountAmount * 1000), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pProductEntryMethodType", OracleDbType.Int32, 4, (int)request.ProductEntryMethodType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pRetailPriceEntryMethodType", OracleDbType.Int32, 4, (int)request.RetailPriceEntryMethodType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pUnitListPrice", OracleDbType.Int32, 12, (int)(request.UnitListPrice * 1000), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pUnitListPriceQty", OracleDbType.Int32, 4, request.UnitListPriceQty, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pProdPromoFlag", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.ProdPromoFlag), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pProdPromoPrice", OracleDbType.Int32, 12, (int)(request.ProdPromoPrice * 1000), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pProdPromoId", OracleDbType.Int32, 6, request.ProdPromoId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pRetailPriceModifiedFlag", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.RetailPriceModifiedFlag), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pRemotePrinterId", OracleDbType.Int32, ParameterDirection.Output) {Size = 6});
                        cmd.Parameters.Add(new OracleParameter("pProductName", OracleDbType.Varchar2, ParameterDirection.Output) {Size = 25});
                        cmd.Parameters.Add(new OracleParameter("pOnSuccessCommit", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.OnSuccessCommit), ParameterDirection.Input));

                        cmd.Parameters.Add(new OracleParameter("pErrorCode", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pDeniedText", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 75 });

                        con.Open();
                        cmd.ExecuteNonQuery();

                        var productName = cmd.Parameters["pProductName"].Value.ToString();
                        var remotePrinterId = cmd.Parameters["pRemotePrinterId"].Value.ToString();
                        var deniedText = cmd.Parameters["pDeniedText"].Value.ToString();
                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);

                        return new RetailTransactionLineProductProcessResponse(request.RequestId)
                        {
                            DeniedText = deniedText,
                            ErrorCode = errorCode,
                            ProductName = productName,
                            RemotePrinterId = string.IsNullOrEmpty(remotePrinterId) || remotePrinterId == "null" ? -1 : Convert.ToInt32(remotePrinterId)
                        };
                    }
                    catch (Exception ex)
                    {
                        var message = Formatting.FormatException(ex);
                        var lex = new LineItemProcessingException(
                            new LineItemProcessingResult(request.RequestId, 1005, message, TransactionLineItemType.LineItemProduct),
                            message);
                        LoggingManager.Instance.LogLineItemProcessingException(
                            lex, request, EventLogEntryType.Error,
                            LoggingDefinitions.Category.Resource, LoggingDefinitions.EventId.WebApiTransactionProcessing);
                        throw lex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Process a retail transaction line item product tax request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The results of the operation.</returns>
        public override RetailTransactionLineProductTaxResponse RetailTransactionLineProductTaxProcess(RetailTransactionLineProductTaxRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "TransactionFunctions.ProcessRtlTranLineProductTax", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pTransactionId", OracleDbType.Int32, 15, request.TransactionId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pLineItemSequenceNumber", OracleDbType.Int32, 4, request.LineItemSequenceNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTaxScheduleId", OracleDbType.Int32, 6, request.TaxScheduleId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTaxablePercent", OracleDbType.Int32, 6, request.TaxablePercent, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTaxableAmount", OracleDbType.Int32, 6, request.TaxableAmount * 1000, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTaxPercent", OracleDbType.Int32, 6, request.TaxPercent, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTaxAmount", OracleDbType.Int32, 12, request.TaxAmount * 1000, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTaxExemptOverrideType", OracleDbType.Int32, 4, request.TaxExemptOverrideType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pOnSuccessCommit", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.OnSuccessCommit), ParameterDirection.Input));

                        cmd.Parameters.Add(new OracleParameter("pErrorCode", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pDeniedText", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 75 });

                        con.Open();
                        cmd.ExecuteNonQuery();

                        var deniedText = cmd.Parameters["pDeniedText"].Value.ToString();
                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);
                        return new RetailTransactionLineProductTaxResponse
                        {
                            RequestId = request.RequestId,
                            DeniedText = deniedText,
                            ErrorCode = errorCode
                        };
                    }
                    catch (Exception ex)
                    {
                        var message = Formatting.FormatException(ex);
                        var lex = new LineItemProcessingException(
                            new LineItemProcessingResult(request.RequestId, 1005, message, TransactionLineItemType.LineItemTenderWithCustomer),
                            message);
                        LoggingManager.Instance.LogLineItemProcessingException(
                            lex, request, EventLogEntryType.Error,
                            LoggingDefinitions.Category.Resource, LoggingDefinitions.EventId.WebApiTransactionProcessing);
                        throw lex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Process a retail transaction line tender SV Card request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The results of the operation.</returns>
        public override RetailTransactionLineTenderSvCardProcessResponse RetailTransactionLineTenderSvCardProcess(RetailTransactionLineTenderSvCardProcessRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "TransactionFunctions.ProcessRtlTranLineTenderSVCard", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        string paddedCardNumber = Formatting.PadCardNumber(request.CardNumber, 22, '0');
                        cmd.Parameters.Add(new OracleParameter("pTransactionId", OracleDbType.Int32, 15, request.TransactionId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pLineItemSequenceNumber", OracleDbType.Int32, 4, request.LineItemSequenceNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pVoidFlad", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.VoidFlag), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTenderId", OracleDbType.Int32, 6, request.TenderId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTenderAmount", OracleDbType.Decimal, 12, request.TenderAmount * 1000, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTipAmount", OracleDbType.Decimal, 12, request.TipAmount * 1000, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTaxAmount", OracleDbType.Decimal, 12, request.TaxAmount ?? request.TaxAmount * 1000, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pCardNum", OracleDbType.Varchar2, 22, paddedCardNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIssueNumber", OracleDbType.Varchar2, 4, request.IssueNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIssueNumberCaptured", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.IssueNumberCaptured), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pPhysicalIdType", OracleDbType.Int32, 6, request.PhysicalIdType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pCustomerEntryMethod", OracleDbType.Int32, 4, request.CustEntryMethodType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pSecondaryAuthEntered", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.SecondaryAuthEntered), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pOnSuccessCommit", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.OnSuccessCommit), ParameterDirection.Input));
                        
                        cmd.Parameters.Add(new OracleParameter("pErrorCode", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pDeniedText", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 75 });

                        con.Open();
                        cmd.ExecuteNonQuery();

                        var deniedText = cmd.Parameters["pDeniedText"].Value.ToString();
                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);
                        return new RetailTransactionLineTenderSvCardProcessResponse(request.RequestId)
                        {
                            RequestId = request.RequestId,
                            DeniedText = deniedText,
                            ErrorCode = errorCode
                        };
                    }
                    catch (Exception ex)
                    {
                        var message = Formatting.FormatException(ex);
                        var lex = new LineItemProcessingException(
                            new LineItemProcessingResult(request.RequestId, 1005, message, TransactionLineItemType.LineItemTenderWithCustomer),
                            message);
                        LoggingManager.Instance.LogLineItemProcessingException(
                            lex, request, EventLogEntryType.Error,
                            LoggingDefinitions.Category.Resource, LoggingDefinitions.EventId.WebApiTransactionProcessing);
                        throw lex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <inheritdoc />
        public override RetailTransactionLineTenderCreditCardResponse RetailTransactionLineTenderCreditCardProcess(int transactionId, RetailTransactionLineTenderCreditCardRequest request)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "TransactionFunctions.ProcessRtlTranLineTenderCC", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pTransactionId", OracleDbType.Int32, 15, transactionId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pLineItemSequenceNumber", OracleDbType.Int32, 4, request.LineItemTenderEmv.LineItemSequenceNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pVoidFlad", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.LineItemTenderEmv.VoidFlag), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTenderId", OracleDbType.Int32, 6, request.LineItemTenderEmv.TenderId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTenderAmount", OracleDbType.Decimal, 12, request.LineItemTenderEmv.TenderAmount * 1000, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTipAmount", OracleDbType.Decimal, 12, 0, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTaxAmount", OracleDbType.Decimal, 12, request.LineItemTenderEmv.TaxAmount ?? request.LineItemTenderEmv.TaxAmount * 1000, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pOnSuccessCommit", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.OnSuccessCommit), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pOrderId", OracleDbType.Varchar2, 36, request.LineItemTenderEmv.TransactionReference, ParameterDirection.Input));
                        
                        cmd.Parameters.Add(new OracleParameter("pErrorCode", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pDeniedText", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 75 });

                        con.Open();
                        cmd.ExecuteNonQuery();

                        var deniedText = cmd.Parameters["pDeniedText"].Value.ToString();
                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);
                        return new RetailTransactionLineTenderCreditCardResponse
                        {
                            RequestId = request.RequestId,
                            DeniedText = deniedText,
                            ErrorCode = errorCode
                        };
                    }
                    catch (Exception ex)
                    {
                        var message = Formatting.FormatException(ex);
                        var lex = new LineItemProcessingException(
                            new LineItemProcessingResult(request.RequestId, 1005, message, TransactionLineItemType.LineItemTenderEmv),
                            message);
                        LoggingManager.Instance.LogLineItemProcessingException(
                            lex, request, EventLogEntryType.Error,
                            LoggingDefinitions.Category.Resource, LoggingDefinitions.EventId.WebApiTransactionProcessing);
                        throw lex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <inheritdoc />
        public override RetailTransactionLineTenderCreditCardHitResponse RetailTransactionLineTenderCreditCardHitProcess(int transactionId, RetailTransactionLineTenderCreditCardHitRequest request)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "TransactionFunctions.ProcessRtlTranLineTenderCC", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pTransactionId", OracleDbType.Int32, 15, transactionId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pLineItemSequenceNumber", OracleDbType.Int32, 4, request.LineItemTenderEmvHit.LineItemSequenceNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pVoidFlad", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.LineItemTenderEmvHit.VoidFlag), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTenderId", OracleDbType.Int32, 6, request.LineItemTenderEmvHit.TenderId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTenderAmount", OracleDbType.Decimal, 12, request.LineItemTenderEmvHit.TenderAmount * 1000, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTipAmount", OracleDbType.Decimal, 12, 0, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTaxAmount", OracleDbType.Decimal, 12, request.LineItemTenderEmvHit.TaxAmount ?? request.LineItemTenderEmvHit.TaxAmount * 1000, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pOnSuccessCommit", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.OnSuccessCommit), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pOrderId", OracleDbType.Varchar2, 36, request.LineItemTenderEmvHit.TransactionReference, ParameterDirection.Input));

                        cmd.Parameters.Add(new OracleParameter("pErrorCode", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pDeniedText", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 75 });

                        con.Open();
                        cmd.ExecuteNonQuery();

                        var deniedText = cmd.Parameters["pDeniedText"].Value.ToString();
                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);
                        return new RetailTransactionLineTenderCreditCardHitResponse
                        {
                            RequestId = request.RequestId,
                            DeniedText = deniedText,
                            ErrorCode = errorCode
                        };
                    }
                    catch (Exception ex)
                    {
                        var message = Formatting.FormatException(ex);
                        var lex = new LineItemProcessingException(
                            new LineItemProcessingResult(request.RequestId, 1005, message, TransactionLineItemType.LineItemTenderEmv),
                            message);
                        LoggingManager.Instance.LogLineItemProcessingException(
                            lex, request, EventLogEntryType.Error,
                            LoggingDefinitions.Category.Resource, LoggingDefinitions.EventId.WebApiTransactionProcessing);
                        throw lex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <inheritdoc />
        public override RetailTransactionLineTenderCashEquivResponse RetailTransactionLineTenderCashEquivProcess(int transactionId, RetailTransactionLineTenderCashEquivRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "TransactionFunctions.ProcessRtlTranLineTenderCECard", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        string paddedCardNumber = Formatting.PadCardNumber(request.LineItemTenderCashEquivalence.CustomerCredential.CustomerCardInfo.CardNumber, 22, '0');
                        cmd.Parameters.Add(new OracleParameter("pTransactionId", OracleDbType.Int32, 15, transactionId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pLineItemSequenceNumber", OracleDbType.Int32, 4, request.LineItemTenderCashEquivalence.LineItemSequenceNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pVoidFlad", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.LineItemTenderCashEquivalence.VoidFlag), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTenderId", OracleDbType.Int32, 6, request.LineItemTenderCashEquivalence.TenderId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTenderAmount", OracleDbType.Decimal, 12, request.LineItemTenderCashEquivalence.TenderAmount * 1000, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTipAmount", OracleDbType.Decimal, 12, request.LineItemTenderCashEquivalence.TipAmount * 1000, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTaxAmount", OracleDbType.Decimal, 12, request.LineItemTenderCashEquivalence.TaxAmount ?? request.LineItemTenderCashEquivalence.TaxAmount * 1000, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pCashEquivAmountLimit", OracleDbType.Int32, 12, request.LineItemTenderCashEquivalence.CashEquivalenceAmountLimit * 1000, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pBoardmtId", OracleDbType.Int32, 6, request.LineItemTenderCashEquivalence.BoardMealTypeId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pGuestMeal", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.LineItemTenderCashEquivalence.IsGuestMeal), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pCardNum", OracleDbType.Varchar2, 22, paddedCardNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIssueNumber", OracleDbType.Varchar2, 4, request.LineItemTenderCashEquivalence.CustomerCredential.CustomerCardInfo.IssueNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIssueNumberCaptured", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.LineItemTenderCashEquivalence.CustomerCredential.CustomerCardInfo.IssueNumberCaptured), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pPhysicalIdType", OracleDbType.Int32, 6, (int)request.LineItemTenderCashEquivalence.CustomerCredential.PhysicalIdType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pCustomerEntryMethod", OracleDbType.Int32, 4, (int)request.LineItemTenderCashEquivalence.CustomerCredential.EntryMethodType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pSecondaryAuthEntered", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.LineItemTenderCashEquivalence.CustomerCredential.SecondaryAuthEntered), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pOnSuccessCommit", OracleDbType.Varchar2, 1, 'T', ParameterDirection.Input));

                        cmd.Parameters.Add(new OracleParameter("pErrorCode", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pDeniedText", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 75 });

                        con.Open();
                        cmd.ExecuteNonQuery();

                        var deniedText = cmd.Parameters["pDeniedText"].Value.ToString();
                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);
                        return new RetailTransactionLineTenderCashEquivResponse
                        {
                            RequestId = request.RequestId,
                            DeniedText = deniedText,
                            ErrorCode = errorCode
                        };
                    }
                    catch (Exception ex)
                    {
                        var message = Formatting.FormatException(ex);
                        var lex = new LineItemProcessingException(
                            new LineItemProcessingResult(request.RequestId, 1005, message, TransactionLineItemType.LineItemTenderWithCustomer),
                            message);
                        LoggingManager.Instance.LogLineItemProcessingException(
                            lex, request, EventLogEntryType.Error,
                            LoggingDefinitions.Category.Resource, LoggingDefinitions.EventId.WebApiTransactionProcessing);
                        throw lex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Process a retail transaction line tender discount request.
        /// </summary>
        /// <param name="transactionId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public override RetailTransactionLineTenderDiscountResponse RetailTransactionLineTenderDiscountProcess(int transactionId, RetailTransactionLineTenderDiscountRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "PackTran.ProcessRtlTranLineTenderDisc", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pTransactionId", OracleDbType.Int32, 15, transactionId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pLineItemSequenceNumber", OracleDbType.Int32, 4, request.LineItemTenderDiscount.LineItemSequenceNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTndrDiscSequenceNumber", OracleDbType.Int32, 4, request.LineItemTenderDiscount.TenderLineItemSequenceNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pDiscountReasonType", OracleDbType.Int32, 4, (int)request.LineItemTenderDiscount.DiscountSurchargeReason, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pPercentage", OracleDbType.Int32, 6, request.LineItemTenderDiscount.Rate * 100000, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pRoundingAmount", OracleDbType.Double, Decimal.ToDouble(request.LineItemTenderDiscount.RoundingAmount), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAmount", OracleDbType.Int32, 12, request.LineItemTenderDiscount.Amount * 1000, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pProratedFlag", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.LineItemTenderDiscount.ProratedFlag), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pCalculationType", OracleDbType.Int32, 4, (int)request.LineItemTenderDiscount.CalculationType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pOnSuccessCommit", OracleDbType.Varchar2, 1, 'F', ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pErrorCode", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pDeniedText", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 75 });

                        con.Open();
                        cmd.ExecuteNonQuery();

                        var deniedText = cmd.Parameters["pDeniedText"].Value.ToString();
                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);
                        return new RetailTransactionLineTenderDiscountResponse
                        {
                            RequestId = request.RequestId,
                            DeniedText = deniedText,
                            ErrorCode = errorCode
                        };
                    }
                    catch (Exception ex)
                    {
                        var message = Formatting.FormatException(ex);
                        var lex = new LineItemProcessingException(
                            new LineItemProcessingResult(request.RequestId, 1005, message, TransactionLineItemType.LineItemTenderDiscount),
                            message);
                        LoggingManager.Instance.LogLineItemProcessingException(
                            lex, request, EventLogEntryType.Error,
                            LoggingDefinitions.Category.Resource, LoggingDefinitions.EventId.WebApiTransactionProcessing);
                        throw lex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Process a retail transaction line tender surcharge request.
        /// </summary>
        /// <param name="transactionId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public override RetailTransactionLineTenderSurchargeResponse RetailTransactionLineTenderSurchargeProcess(int transactionId, RetailTransactionLineTenderSurchargeRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "PackTran.ProcessRtlTranLineTenderSurch", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pTransactionId", OracleDbType.Int32, 15, transactionId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pLineItemSequenceNumber", OracleDbType.Int32, 4, request.LineItemTenderSurcharge.LineItemSequenceNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTndrSrchSequenceNumber", OracleDbType.Int32, 4, request.LineItemTenderSurcharge.TenderLineItemSequenceNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pSurchargeReasonType", OracleDbType.Int32, 4, (int)request.LineItemTenderSurcharge.DiscountSurchargeReason, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pPercentage", OracleDbType.Int32, 6, request.LineItemTenderSurcharge.Rate * 100000, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pRoundingAmount", OracleDbType.Double, Decimal.ToDouble(request.LineItemTenderSurcharge.RoundingAmount), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAmount", OracleDbType.Int32, 12, request.LineItemTenderSurcharge.Amount * 1000, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pProratedFlag", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.LineItemTenderSurcharge.ProratedFlag), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pCalculationType", OracleDbType.Int32, 4, (int)request.LineItemTenderSurcharge.CalculationType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pOnSuccessCommit", OracleDbType.Varchar2, 1, 'F', ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pErrorCode", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pDeniedText", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 75 });

                        con.Open();
                        cmd.ExecuteNonQuery();

                        var deniedText = cmd.Parameters["pDeniedText"].Value.ToString();
                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);
                        return new RetailTransactionLineTenderSurchargeResponse
                        {
                            RequestId = request.RequestId,
                            DeniedText = deniedText,
                            ErrorCode = errorCode
                        };
                    }
                    catch (Exception ex)
                    {
                        var message = Formatting.FormatException(ex);
                        var lex = new LineItemProcessingException(
                            new LineItemProcessingResult(request.RequestId, 1005, message, TransactionLineItemType.LineItemTenderSurcharge),
                            message);
                        LoggingManager.Instance.LogLineItemProcessingException(
                            lex, request, EventLogEntryType.Error,
                            LoggingDefinitions.Category.Resource, LoggingDefinitions.EventId.WebApiTransactionProcessing);
                        throw lex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Process a retail transaction line stored value type card request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The results of the operation.</returns>
        public override RetailTransactionLineStoredValueTypeCardResponse RetailTransactionLineStoredValueTypeCardProcess(RetailTransactionLineStoredValueTypeCardRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "TransactionFunctions.ProcessRtlTranLineSVTypeCard", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        string paddedCardNumber = Formatting.PadCardNumber(request.CardNumber, 22, '0');
                        cmd.AddInParam("pTransactionId", request.TransactionId);
                        cmd.AddInParam("pLineItemSequenceNumber", request.LineItemSequenceNumber);
                        cmd.AddInParam("pVoidFlag", Formatting.BooleanToTf(request.VoidFlag));
                        cmd.AddInParam("pStoredValueAmount", request.StoredValueAmount * 1000);
                        cmd.AddInParam("pCardPurchaseFeeAmount", request.CardPurchaseFeeAmount * 1000);
                        cmd.AddInParam("pActualUnitPrice", request.ActualUnitProce * 1000);
                        cmd.AddInParam("pUnitCostPrice", request.UnitCostPrice * 1000);
                        cmd.AddInParam("pSVAccountTypeId", request.SvAccountTypeId);
                        cmd.AddInParam("pCardNum", paddedCardNumber);
                        cmd.AddInParam("pIssueNumber", request.IssueNumber);
                        cmd.AddInParam("pIssueNumberCaptured", Formatting.BooleanToTf(request.IssueNumberCaptured));
                        cmd.AddInParam("pPhysicalIdType", (int)request.PhysicalIdType);
                        cmd.AddInParam("pCustomerEntryMethod", (int)request.CustomerEntryMethodType);
                        cmd.AddInParam("pSecondaryAuthEntered", Formatting.BooleanToTf(request.SecondaryAuthEntered));
                        cmd.AddInParam("pEnrichmentFlag", Formatting.BooleanToTf(request.EnrichmentFlag));
                        cmd.AddInParam("pEnrichmentParentLineItemSeq", request.EnrichmentParentLineItemSequence);
                        cmd.AddInParam("pOnSuccessCommit", Formatting.BooleanToTf(request.OnSuccessCommit));
                        cmd.AddInParam("pForcePost", Formatting.BooleanToTf(request.ForcePost));

                        cmd.AddOutParam("pErrorCode", OracleDbType.Int32, 10);
                        cmd.AddOutParam("pDeniedText", OracleDbType.Varchar2, 75);

                        con.Open();
                        cmd.ExecuteNonQuery();

                        var deniedText = cmd.Parameters["pDeniedText"].Value.ToString();
                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);
                        return new RetailTransactionLineStoredValueTypeCardResponse
                        {
                            RequestId = request.RequestId,
                            DeniedText = deniedText,
                            ErrorCode = errorCode
                        };
                    }
                    catch (Exception ex)
                    {
                        var message = Formatting.FormatException(ex);
                        var lex = new LineItemProcessingException(
                            new LineItemProcessingResult(request.RequestId, 1005, message, TransactionLineItemType.LineItemTenderWithCustomer),
                            message);
                        LoggingManager.Instance.LogLineItemProcessingException(
                            lex, request, EventLogEntryType.Error,
                            LoggingDefinitions.Category.Resource, LoggingDefinitions.EventId.WebApiTransactionProcessing);
                        throw lex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <inheritdoc />
        public override RetailTransactionLineStoredValueEnrichmentResponse RetailTransactionLineStoredValueEnrichmentProcess(RetailTransactionLineStoredValueEnrichmentRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "TransactionFunctions.ProcessRtlTranLineSVEnrich", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pTransactionId", OracleDbType.Int32, 15, request.TransactionId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pLineItemSequenceNumber", OracleDbType.Int32, 4, request.LineItemSequenceNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pSVEnrichSequenceNumber", OracleDbType.Int32, 4, request.StoredValueEnrichmentSequence, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pPercentage", OracleDbType.Int32, 6, request.Percentage * 100000, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pRoundingAmount", OracleDbType.Double, Decimal.ToDouble(request.RoundingAmount), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAmount", OracleDbType.Int32, 12, request.Amount * 1000, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pOnSuccessCommit", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.OnSuccessCommit), ParameterDirection.Input));

                        cmd.Parameters.Add(new OracleParameter("pErrorCode", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pDeniedText", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 75 });

                        con.Open();
                        cmd.ExecuteNonQuery();

                        var deniedText = cmd.Parameters["pDeniedText"].Value.ToString();
                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);
                        return new RetailTransactionLineStoredValueEnrichmentResponse
                        {
                            RequestId = request.RequestId,
                            DeniedText = deniedText,
                            ErrorCode = errorCode
                        };
                    }
                    catch (Exception ex)
                    {
                        var message = Formatting.FormatException(ex);
                        var lex = new LineItemProcessingException(
                            new LineItemProcessingResult(request.RequestId, 1005, message, TransactionLineItemType.LineItemTenderWithCustomer),
                            message);
                        LoggingManager.Instance.LogLineItemProcessingException(
                            lex, request, EventLogEntryType.Error,
                            LoggingDefinitions.Category.Resource, LoggingDefinitions.EventId.WebApiTransactionProcessing);
                        throw lex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Process a retail transaction line item product tax override request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The results of the operation.</returns>
        public override RetailTransactionLineProductTaxOverrideResponse RetailTransactionLineProductTaxOverrideProcess(RetailTransactionLineProductTaxOverrideRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "TransactionFunctions.ProcessRtlTranLineProdTaxOvrd", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pTransactionId", OracleDbType.Int32, 15, request.TransactionId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pLineItemSequenceNumber", OracleDbType.Int32, 4, request.LineItemSequenceNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTaxScheduleId", OracleDbType.Int32, 6, request.TaxScheduleId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTaxOverrideReasonType", OracleDbType.Int32, 4, request.TaxOverrideReasonType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTaxableAmount", OracleDbType.Int32, 12, request.TaxableAmount * 1000, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pOriginalTaxAmount", OracleDbType.Int32, 12, request.OriginalTaxableAmount * 1000, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pNewTaxAmount", OracleDbType.Int32, 12, request.NewTaxAmount * 1000, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pOriginalTaxPercent", OracleDbType.Int32, 6, request.OriginalTaxPercent, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pNewTaxPercent", OracleDbType.Int32, 6, request.NewTaxPercent, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pOnSuccessCommit", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.OnSuccessCommit), ParameterDirection.Input));

                        cmd.Parameters.Add(new OracleParameter("pErrorCode", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pDeniedText", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 75 });

                        con.Open();
                        cmd.ExecuteNonQuery();

                        var deniedText = cmd.Parameters["pDeniedText"].Value.ToString();
                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);
                        return new RetailTransactionLineProductTaxOverrideResponse
                        {
                            RequestId = request.RequestId,
                            DeniedText = deniedText,
                            ErrorCode = errorCode
                        };
                    }
                    catch (Exception ex)
                    {
                        var message = Formatting.FormatException(ex);
                        var lex = new LineItemProcessingException(
                            new LineItemProcessingResult(request.RequestId, 1005, message, TransactionLineItemType.LineItemTenderWithCustomer),
                            message);
                        LoggingManager.Instance.LogLineItemProcessingException(
                            lex, request, EventLogEntryType.Error,
                            LoggingDefinitions.Category.Resource, LoggingDefinitions.EventId.WebApiTransactionProcessing);
                        throw lex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Process a retail transaction line tender request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The results of the operation.</returns>
        public override RetailTransactionLineTenderResponse RetailTransactionLineTenderProcess(RetailTransactionLineTenderRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "TransactionFunctions.ProcessRtlTranLineTender", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pTransactionId", OracleDbType.Int32, 15, request.TransactionId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pLineItemSequenceNumber", OracleDbType.Int32, 4, request.LineItemSequenceNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pVoidFlad", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.VoidFlag), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTenderId", OracleDbType.Int32, 6, request.TenderId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTenderAmount", OracleDbType.Decimal, 12, request.TenderAmount * 1000, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTipAmount", OracleDbType.Decimal, 12, request.TipAmount * 1000, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTaxAmount", OracleDbType.Decimal, 12, request.TaxAmount ?? request.TaxAmount * 1000, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pRoundingAmount", OracleDbType.Decimal, 12, request.RoundingAmount * 1000, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pOnSuccessCommit", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.OnSuccessCommit), ParameterDirection.Input));

                        cmd.Parameters.Add(new OracleParameter("pErrorCode", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pDeniedText", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 75 });

                        con.Open();
                        cmd.ExecuteNonQuery();

                        var deniedText = cmd.Parameters["pDeniedText"].Value.ToString();
                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);
                        return new RetailTransactionLineTenderResponse
                        {
                            RequestId = request.RequestId,
                            DeniedText = deniedText,
                            ErrorCode = errorCode
                        };
                    }
                    catch (Exception ex)
                    {
                        var message = Formatting.FormatException(ex);
                        var lex = new LineItemProcessingException(
                            new LineItemProcessingResult(request.RequestId, 1005, message, TransactionLineItemType.LineItemTenderWithCustomer),
                            message);
                        LoggingManager.Instance.LogLineItemProcessingException(
                            lex, request, EventLogEntryType.Error,
                            LoggingDefinitions.Category.Resource, LoggingDefinitions.EventId.WebApiTransactionProcessing);
                        throw lex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Process a retail transaction begin request against the resource layer.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The results of the operation.</returns>
        public override RetailTransactionEndProcessResponse RetailTransactionEndProcess(RetailTransactionEndProcessRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "TransactionFunctions.ProcessRetailTransactionEnd", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pTransactionId", OracleDbType.Int32, 15, request.TransactionId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTransactionDateTime", OracleDbType.Double, 14, request.TransactionDateTime.ToOADate(), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTransactionTotal", OracleDbType.Decimal, 12, request.TransactionTotal * 1000, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pUnitCount", OracleDbType.Int32, 4, request.UnitCount, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pLineItemsScannedCount", OracleDbType.Int32, 4, request.LineItemsScannedCount, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pLineItemsScannedPercent", OracleDbType.Int32, 6, request.LineItemsScannedPercent * 1000, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pLineItemsKeyedCount", OracleDbType.Int32, 4, request.LineItemsKeyedCount, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pLineItemsKeyedPercent", OracleDbType.Int32, 6, request.LineItemsKeyedPercent * 1000, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pReceiptDateTime", OracleDbType.Double, 14, request.ReceiptDateTime.ToOADate(), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pValidationType", OracleDbType.Int32, 10, (int)request.ValidationType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pValidationCode", OracleDbType.Int32, 10, request.ValidationCode, ParameterDirection.Input));

                        cmd.Parameters.Add(new OracleParameter("crCustomerPosInfo", OracleDbType.RefCursor, ParameterDirection.Output));
                        cmd.Parameters.Add(new OracleParameter("crSVAccountLog", OracleDbType.RefCursor, ParameterDirection.Output));
                        cmd.Parameters.Add(new OracleParameter("pErrorCode", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pDeniedText", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 75 });

                        con.Open();
                        cmd.ExecuteNonQuery();
                        //var customerPosInfoList = new List<CustomerPosInfo>();
                        //var customerStoredValueAccountList = new List<CustomerStoredValueAccount>();
                        //using (var r = cmd.ExecuteReader())
                        //{
                            // NOTES:
                            // Commenting this out for now because there is a typing issue in the database that needs to be 
                            // addressed where if there is no data for displaySvBal, it is returned as in Int32 and if there
                            // is data, it is returned as a VarChar2.  Also the information isn't returned to the client at 
                            // the moment anyways.

                            //while (r.Read())
                            //{
                            //    var lastName = !r.IsDBNull(0) ? r.GetString(0) : string.Empty;
                            //    var custNum = !r.IsDBNull(1) ? r.GetString(1) : string.Empty;
                            //    var cardNum = !r.IsDBNull(2) ? r.GetString(2) : string.Empty;
                            //    var isBirthday = !r.IsDBNull(3) && Formatting.TfStringToBool(r.GetString(3));
                            //    var displaySvBal = !r.IsDBNull(4) ? new decimal?(r.GetDecimal(4)) : null;
                            //    var printSvBal = !r.IsDBNull(5) ? new decimal?(r.GetDecimal(5)) : null;
                            //    var displayCustName = !r.IsDBNull(6) ? r.GetString(6) : null;
                            //    var displayBirthday = !r.IsDBNull(7) ? new DateTime?(DateTime.FromOADate(r.GetDouble(7))) : null;
                            //    var printCustName = !r.IsDBNull(8) ? r.GetString(8) : null;
                            //    var printCardNum = !r.IsDBNull(9) ? r.GetString(9) : null;
                            //    var printCustNum = !r.IsDBNull(10) ? r.GetString(10) : null;
                            //    var msgWaiting = !r.IsDBNull(11) && Formatting.TfStringToBool(r.GetString(11));
                            //    var custId = !r.IsDBNull(12) ? r.GetInt32(12) : -1;

                            //    customerPosInfoList.Add(new CustomerPosInfo
                            //    {
                            //        LastName = lastName,
                            //        CustomerNumber = custNum,
                            //        CardNumber = cardNum,
                            //        IsBirthday = isBirthday,
                            //        DisplayCustomerName = displayCustName,
                            //        DisplayBirthday = displayBirthday,
                            //        DisplaySvBalance = displaySvBal,
                            //        PrintSvBalance = printSvBal,
                            //        PrintCustomerName = printCustName,
                            //        PrintCustomerCardNumber = printCardNum,
                            //        PrintCustomerNumber = printCustNum,
                            //        MessageWaiting = msgWaiting,
                            //        CustomerId = custId
                            //    });
                            //}

                            //r.NextResult();
                            //while (r.Read())
                            //{
                            //    var custId = !r.IsDBNull(0) ? r.GetInt32(0) : -1;
                            //    var debitCreditType = !r.IsDBNull(1) ? r.GetInt32(1) : -1;
                            //    var amount = !r.IsDBNull(2) ? r.GetInt32(2) : -1;
                            //    var svAccountName = !r.IsDBNull(3) ? r.GetString(3) : string.Empty;
                            //    var balanceAfterTran = !r.IsDBNull(4) ? r.GetInt32(4) : -1;

                            //    customerStoredValueAccountList.Add(
                            //        new CustomerStoredValueAccount
                            //        {
                            //            CustomerId = custId,
                            //            DebitCreditType = (DebitCreditType)debitCreditType,
                            //            Amount = amount,
                            //            StoredValueAccountName = svAccountName,
                            //            BalanceAfterTransaction = balanceAfterTran
                            //        });
                            //}
                        //}
                        var deniedText = cmd.Parameters["pDeniedText"].Value.ToString();
                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);
                        return new RetailTransactionEndProcessResponse(request.RequestId)
                        {
                            RequestId = request.RequestId,
                            DeniedText = deniedText,
                            ErrorCode = errorCode,
                            CustomerPosInfoList = null,
                            CustomerStoredValueAccounts = null
                        };
                    }
                    catch (Exception ex)
                    {
                        var message = Formatting.FormatException(ex);
                        var lex = new LineItemProcessingException(
                            new LineItemProcessingResult(request.RequestId, 1005, message, TransactionLineItemType.LineItemRetailTransactionEnd),
                            message);
                        LoggingManager.Instance.LogLineItemProcessingException(
                            lex, request, EventLogEntryType.Error,
                            LoggingDefinitions.Category.Resource, LoggingDefinitions.EventId.WebApiTransactionProcessing);
                        throw lex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
    }

        #endregion

        #region Session_Control Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Session_ControlByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Session_Control WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Session_ControlDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Session_Control SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsSession_Control> Session_ControlListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsSession_Control>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.Cashdrawer_Num,
                                                                                                            T.Session_Open,
                                                                                                            T.SessionEnd_Transaction_Id,
                                                                                                            T.SessionStart_DateTime,
                                                                                                            T.SessionEnd_DateTime,
                                                                                                            T.Session_ControlTotal
                                                                                                  FROM      Envision.Session_Control T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsSession_Control();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Cashdrawer_Num = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Session_Open = r.GetString(2);
                                if (!r.IsDBNull(3)) item.SessionEnd_Transaction_Id = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.SessionStart_DateTime = DateTime.FromOADate(r.GetDouble(4));
                                if (!r.IsDBNull(5)) item.SessionEnd_DateTime = DateTime.FromOADate(r.GetDouble(5));
                                if (!r.IsDBNull(6)) item.Session_ControlTotal = r.GetInt32(6);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region StoredValueDenial Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void StoredValueDenialByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.StoredValueDenial WHERE TransactionId IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.StoredValueDenial T WHERE TransactionId IS NULL AND T.ModifiedDate_Number >= {oleAutomationDate} AND T.ModifiedDate_Number < ({oleAutomationDate}+1)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> StoredValueDenialDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.TransactionId FROM Envision.StoredValueDenial SUB) " +
                                                                                               $"UNION " +
                                                                                               $"SELECT DISTINCT TRUNC(SVD.ModifiedDate_Number) FROM Envision.StoredValueDenial SVD WHERE SVD.TransactionId IS NULL AND SVD.ModifiedDate_Number < ({oleAutomationDate}) " +
                                                                                               $"ORDER BY 1 ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsStoredValueDenial> StoredValueDenialListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsStoredValueDenial>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.StoredValueDenialId,
                                                                                                            T.CustomerId,
                                                                                                            T.PosId,
                                                                                                            T.TenderId,
                                                                                                            T.TransactionId,
                                                                                                            T.IsOnline,
                                                                                                            T.DeniedMessageId,
                                                                                                            T.AvailableBalance,
                                                                                                            T.ModifiedDate,
                                                                                                            T.CardNumber
                                                                                                  FROM      Envision.StoredValueDenial T
                                                                                                  WHERE     T.TransactionId IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0)
                                                                                                  UNION ALL
                                                                                                  SELECT    T.StoredValueDenialId,
                                                                                                            T.CustomerId,
                                                                                                            T.PosId,
                                                                                                            T.TenderId,
                                                                                                            T.TransactionId,
                                                                                                            T.IsOnline,
                                                                                                            T.DeniedMessageId,
                                                                                                            T.AvailableBalance,
                                                                                                            T.ModifiedDate,
                                                                                                            T.CardNumber
                                                                                                  FROM      Envision.StoredValueDenial T
                                                                                                  WHERE     T.TransactionId IS NULL
                                                                                                    AND     T.ModifiedDate_Number >= {oleAutomationDate}
                                                                                                    AND     T.ModifiedDate_Number < ({oleAutomationDate}+1)
                                                                                               ", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsStoredValueDenial();

                                if (!r.IsDBNull(0)) item.StoredValueDenialId = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.CustomerId = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.PosId = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.TenderId = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.TransactionId = r.GetInt32(4);
                                if (!r.IsDBNull(5)) item.IsOnline = r.GetString(5);
                                if (!r.IsDBNull(6)) item.DeniedMessageId = r.GetInt32(6);
                                if (!r.IsDBNull(7)) item.AvailableBalance = r.GetInt32(7);
                                if (!r.IsDBNull(8)) item.ModifiedDate = r.GetDateTime(8);
                                if (!r.IsDBNull(9)) item.CardNumber = r.GetString(9);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Sv_Account_History_Rtl_Trn_Sv Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Sv_Account_History_Rtl_Trn_SvByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Sv_Account_History_Rtl_Trn_Sv WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Sv_Account_History_Rtl_Trn_SvDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Sv_Account_History_Rtl_Trn_Sv SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsSv_Account_History_Rtl_Trn_Sv> Sv_Account_History_Rtl_Trn_SvListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsSv_Account_History_Rtl_Trn_Sv>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Sv_Account_History_Id,
                                                                                                            T.Transaction_Id,
                                                                                                            T.LineItem_SequenceNumber
                                                                                                  FROM      Envision.Sv_Account_History_Rtl_Trn_Sv T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsSv_Account_History_Rtl_Trn_Sv();

                                if (!r.IsDBNull(0)) item.Sv_Account_History_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Transaction_Id = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.LineItem_SequenceNumber = r.GetInt32(2);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Sv_Account_History_Rtl_Trn_Tdr Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Sv_Account_History_Rtl_Trn_TdrByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Sv_Account_History_Rtl_Trn_Tdr WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Sv_Account_History_Rtl_Trn_TdrDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Sv_Account_History_Rtl_Trn_Tdr SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsSv_Account_History_Rtl_Trn_Tdr> Sv_Account_History_Rtl_Trn_TdrListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsSv_Account_History_Rtl_Trn_Tdr>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Sv_Account_History_Id,
                                                                                                            T.Transaction_Id,
                                                                                                            T.LineItem_SequenceNumber,
                                                                                                            T.Depletion_Order,
                                                                                                            T.Split_Tender_Flag
                                                                                                  FROM      Envision.Sv_Account_History_Rtl_Trn_Tdr T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsSv_Account_History_Rtl_Trn_Tdr();

                                if (!r.IsDBNull(0)) item.Sv_Account_History_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Transaction_Id = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.LineItem_SequenceNumber = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.Depletion_Order = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.Split_Tender_Flag = r.GetString(4);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Tender

        /// <summary>
        /// This method will return a list of records for the Tender objects in the system.  If connection is null, <see cref="ConnectionString"/> will be used
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsTender> TenderArchivesGet(ConnectionInfo connection = null)
        {
            var list = new List<TsTender>();

            using (var con = connection == null ? OracleConnectionGet(ConnectionString) : OracleConnectionGet(connection))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = @"SELECT  Tender_Id,
                                          Auth_Source,
                                          Tender_Type,
                                          Name,
                                          ShortName,
                                          Code_Name,
                                          KeyTop1,
                                          KeyTop2
                                  FROM    Envision.Tender",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsTender();

                                if (!r.IsDBNull(0)) item.Tender_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Auth_Source = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Tender_Type = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.Name = r.GetString(3);
                                if (!r.IsDBNull(4)) item.ShortName = r.GetString(4);
                                if (!r.IsDBNull(5)) item.Code_Name = r.GetString(5);
                                if (!r.IsDBNull(6)) item.KeyTop1 = r.GetString(6);
                                if (!r.IsDBNull(7)) item.KeyTop2 = r.GetString(7);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <inheritdoc />
        public override List<TsTender2> TenderGet(TenderType? tenderType)
        {
            var list = new List<TsTender2>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "TenderFunctions.TenderGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pTenderType", OracleDbType = OracleDbType.Int16, Size = 4, Direction = ParameterDirection.Input, Value = (int?)tenderType });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsTender2();
                                if (!r.IsDBNull(0)) item.Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.AuthSource = (AuthorizationSource)r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Type = (TenderType)r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.Name = r.GetString(3);
                                if (!r.IsDBNull(4)) item.ShortName = r.GetString(4);
                                if (!r.IsDBNull(5)) item.CodeName = r.GetString(5);
                                if (!r.IsDBNull(6)) item.KeyTop1 = r.GetString(6);
                                if (!r.IsDBNull(7)) item.KeyTop2 = r.GetString(7);

                                list.Add(item);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <inheritdoc />
        public override TransactionHistoryListGetResponse TransactionHistoryListGet(TransactionHistoryListGetRequest request)
        {
            var result = new TransactionHistoryListGetResponse()
            {
                Transactions = new List<TsTransactionHistory>()
            };

            int? count = null;
            string countQuery = null;
            var query = @"SELECT Transaction_Info.Transaction_Id,
                             Transaction_Info.Transaction_Number,
                             DECODE(Transaction_Info.VALIDATION_TYPE,0,'F',1,'F','T') AS Denied,
                             Transaction_Info.END_DATETIME AS Transaction_Time,
                             POSPC.PCNAME as PCName,
                             POSPC.POSNAME as POSNAME,
                             RETAIL_TRAN.RETAIL_TRAN_TYPE as Type,
                             GetRetailTranTenderList(Transaction_Info.transaction_id) AS Tenders,
                             RETAIL_TRAN_TOTAL.AMOUNT AS Amount,
                             Transaction_Info.KEYEDOFFLINE_FLAG AS IsOffline,
                             POSPC.MERCHANT_ID AS Merchant_Id
                        FROM
                        (
                        Select TRANSACTION_ID,TRANSACTION_NUMBER,POS_ID,END_DATETIME,KEYEDOFFLINE_FLAG,VALIDATION_TYPE
                        from Transaction
                         Where TRANSACTIONTYPE_CODE = 0 AND CANCELLED_FLAG = 'F' AND VOIDED_FLAG = 'F' AND SUSPENDED_FLAG = 'F' AND TRAINING_FLAG = 'F' AND ( (VALIDATION_TYPE IN (0,1) AND INPROCESS_FLAG = 'F') OR VALIDATION_TYPE IN (2,3))
                         and not exists
                         (
                           Select 1
                           from StoredValueDenial SVD
                           where SVD.TransactionId = Transaction.Transaction_Id
                           AND NOT EXISTS (SELECT 1 FROM Retail_Tran_Lineitem_Sv_Cust cst WHERE cst.Transaction_Id = SVD.TransactionId)
                         )
                        UNION ALL
                         Select -STOREDVALUEDENIALID TRANSACTION_ID,STOREDVALUEDENIALID TRANSACTION_NUMBER,POSID POS_ID,udf_functions.TimeStampTzToNumber(MODIFIEDDATE) END_DATETIME,Decode(ISONLINE,'T','F','T') KEYEDOFFLINE_FLAG, 2 VALIDATION_TYPE
                         from StoredValueDenial
                         WHERE NOT EXISTS (SELECT 1 FROM Retail_Tran_Lineitem_Sv_Cust cst WHERE cst.Transaction_Id = StoredValueDenial.TransactionId)
                        ) Transaction_Info
                        LEFT OUTER JOIN (Select POS.POS_ID, POS.NAME POSNAME, PC.NAME PCNAME, PC.MERCHANT_ID from POS, PROFITCENTER PC where POS.PROFITCENTER_ID = PC.PROFITCENTER_ID) POSPC ON POSPC.Pos_ID = Transaction_Info.Pos_Id
                        LEFT OUTER JOIN RETAIL_TRAN ON RETAIL_TRAN.Transaction_ID = Transaction_Info.Transaction_ID
                        LEFT OUTER JOIN RETAIL_TRAN_TOTAL ON RETAIL_TRAN_TOTAL.Transaction_ID = Transaction_Info.Transaction_ID
                        WHERE   Transaction_Number LIKE :pTransactionNumber
                        AND     DECODE(Transaction_Info.VALIDATION_TYPE,0,'F',1,'F','T') LIKE :pDenied
                        AND     NVL(POSPC.PCNAME, '-1') LIKE :pPCName
                        AND     NVL(POSPC.POSNAME, '-1') LIKE :pPosName
                        AND     NVL(RETAIL_TRAN.RETAIL_TRAN_TYPE, '-1') LIKE :pType
                        AND     NVL(GetRetailTranTenderList(Transaction_Info.transaction_id), '-1') LIKE :pTenders
                        AND     Transaction_Info.END_DATETIME BETWEEN :pStartDate AND :pEndDate";

            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandType = CommandType.Text })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionNumber", OracleDbType = OracleDbType.Varchar2, Size = 8, Direction = ParameterDirection.Input, Value = $"%{request.TransactionNumber}" });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDenied", OracleDbType = OracleDbType.Varchar2, Size = 2, Direction = ParameterDirection.Input, Value = request.IsDenied.HasValue ? $"{Formatting.BooleanToTf(request.IsDenied.Value)}" : "%" });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPCName", OracleDbType = OracleDbType.Varchar2, Size = 31, Direction = ParameterDirection.Input, Value = $"{request.ProfitCenterName}%" });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPosName", OracleDbType = OracleDbType.Varchar2, Size = 31, Direction = ParameterDirection.Input, Value = $"{request.PosName}%" });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pType", OracleDbType = OracleDbType.Varchar2, Size = 2, Direction = ParameterDirection.Input, Value = $"{(int?)request.TransactionType}%" });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pTenders", OracleDbType = OracleDbType.Varchar2, Size = (request.Tenders?.Length ?? 0) + 1, Direction = ParameterDirection.Input, Value = $"{request.Tenders}%" });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pStartDate", OracleDbType = OracleDbType.Double, Direction = ParameterDirection.Input, Value = request.StartDate.ToOADate() });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pEndDate", OracleDbType = OracleDbType.Double, Direction = ParameterDirection.Input, Value = request.EndDate.ToOADate() });

                        if (request.PageSize.HasValue)
                        {
                            countQuery = $"SELECT count(*) FROM ({query})";
                            query = $"{query} OFFSET {request.Offset} ROWS FETCH NEXT {request.PageSize} ROWS ONLY";
                        }

                        cmd.CommandText = query;
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                result.Transactions.Add(new TsTransactionHistory()
                                {
                                    TransactionId = !r.IsDBNull(0) ? r.GetInt64(0) : -1,
                                    TransactionNumber = !r.IsDBNull(1) ? r.GetInt32(1) : -1,
                                    IsDenied = !r.IsDBNull(2) && Formatting.TfStringToBool(r.GetString(2)),
                                    TransactionDateTime = !r.IsDBNull(3) ? DateTime.FromOADate(r.GetDouble(3)) : DateTime.MinValue,
                                    ProfitCenterName = !r.IsDBNull(4) ? r.GetString(4) : string.Empty,
                                    PosName = !r.IsDBNull(5) ? r.GetString(5) : string.Empty,
                                    TransactionType = !r.IsDBNull(6) ? (RetailTranType?)r.GetInt32(6) : null,
                                    Tenders = !r.IsDBNull(7) ? r.GetString(7) : string.Empty,
                                    Amount = !r.IsDBNull(8) ? r.GetInt64(8) / 1000m : 0,
                                    IsOnline = !r.IsDBNull(9) && !Formatting.TfStringToBool(r.GetString(9)),
                                    MerchantId = !r.IsDBNull(10) ? (Int16?)r.GetInt16(10) : null
                                });
                            }
                        }

                        if (!string.IsNullOrEmpty(countQuery))
                        {
                            cmd.CommandText = countQuery;
                            count = int.Parse(cmd.ExecuteScalar().ToString());
                        }
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }

            result.TotalCount = count ?? result.Transactions.Count;
            return result;
        }

        /// <summary>
        /// Get the tenders assigned to a device.
        /// </summary>
        /// <param name="deviceId">The unique identifier for the device.</param>
        /// <returns>List of tenders assigned to the device.</returns>
        public override List<TenderDeviceSetting> TenderDeviceSettingsGet(string deviceId)
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
            Guard.IsNotNullOrWhiteSpace(deviceId, "deviceId");

            var list = new List<TenderDeviceSetting>();

            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = @"TenderFunctions.DeviceTendersGet", CommandType = CommandType.StoredProcedure })
                {

                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDeviceGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = deviceId });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Size = 0, Direction = ParameterDirection.Output, Value = string.Empty });

                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {

                                var tender = new TenderDeviceSetting();

                                if (!r.IsDBNull(0)) tender.Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) tender.Type = (TenderType)r.GetInt32(1);
                                if (!r.IsDBNull(2)) tender.Name = r.GetString(2);
                                if (!r.IsDBNull(3)) tender.ShortName = r.GetString(3);
                                if (!r.IsDBNull(4)) tender.RoundingIncrement = r.GetDecimal(4) / 1000m;

                                list.Add(tender);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            return list;
        }


        /// <summary>
        /// This method will return a list of records for the Stored Value Tender objects that are available at the specified POS.  If connection is null, <see cref="ConnectionString"/> will be used
        /// </summary>
        /// <param name="posId"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public override List<TsTender> StoredValueTendersForPos(int posId, ConnectionInfo connection = null)
        {
            var list = new List<TsTender>();

            using (var con = connection == null ? OracleConnectionGet(ConnectionString) : OracleConnectionGet(connection))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = $@"SELECT     TEN.Tender_Id,
                                                TEN.Auth_Source,
                                                TEN.Tender_Type,
                                                TEN.Name,
                                                TEN.ShortName,
                                                TEN.Code_Name,
                                                TEN.KeyTop1,
                                                TEN.KeyTop2
                                       FROM     Tender              TEN
                                 INNER JOIN     Merchant_Tender_Sv  MTS ON  TEN.Tender_Id       = MTS.Tender_Id
                                 INNER JOIN     Merchant            MER ON  MTS.Merchant_Id     = MER.Merchant_Id
                                 INNER JOIN     ProfitCenter        PC  ON  MER.Merchant_Id     = PC.Merchant_Id
                                 INNER JOIN     POS                 P   ON  PC.ProfitCenter_Id  = P.ProfitCenter_Id
                                      WHERE     P.Pos_Id = {posId}",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsTender();

                                if (!r.IsDBNull(0)) item.Tender_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Auth_Source = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Tender_Type = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.Name = r.GetString(3);
                                if (!r.IsDBNull(4)) item.ShortName = r.GetString(4);
                                if (!r.IsDBNull(5)) item.Code_Name = r.GetString(5);
                                if (!r.IsDBNull(6)) item.KeyTop1 = r.GetString(6);
                                if (!r.IsDBNull(7)) item.KeyTop2 = r.GetString(7);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region TiaTransactionLog Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void TiaTransactionLogByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.TiaTransactionLog WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> TiaTransactionLogDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.TiaTransactionLog SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsTiaTransactionLog> TiaTransactionLogListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsTiaTransactionLog>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.TransactionGuid,
                                                                                                            T.TransactionId,
                                                                                                            T.CustomerId,
                                                                                                            T.AvailableBalance,
                                                                                                            T.TenderId,
                                                                                                            T.PosId,
                                                                                                            T.ErrorId,
                                                                                                            T.PostedDateTime
                                                                                                  FROM      Envision.TiaTransactionLog T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsTiaTransactionLog();

                                if (!r.IsDBNull(0)) item.TransactionGuid = r.GetString(0);
                                if (!r.IsDBNull(1)) item.TransactionId = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.CustomerId = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.AvailableBalance = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.TenderId = r.GetInt32(4);
                                if (!r.IsDBNull(5)) item.PosId = r.GetInt32(5);
                                if (!r.IsDBNull(6)) item.ErrorId = r.GetInt32(6);
                                if (!r.IsDBNull(7)) item.PostedDateTime = r.GetInt32(7);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Transaction_Cashier Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Transaction_CashierByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Transaction_Cashier WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Transaction_CashierDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Transaction_Cashier SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsTransaction_Cashier> Transaction_CashierListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsTransaction_Cashier>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.Cashier_Id
                                                                                                  FROM      Envision.Transaction_Cashier T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsTransaction_Cashier();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Cashier_Id = r.GetInt32(1);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Transaction_Communication Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Transaction_CommunicationByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Transaction_Communication WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Transaction_CommunicationDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Transaction_Communication SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsTransaction_Communication> Transaction_CommunicationListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsTransaction_Communication>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.Host_Computer_Id,
                                                                                                            T.Protocol_Version,
                                                                                                            T.Request_Payload,
                                                                                                            T.Response_Payload
                                                                                                  FROM      Envision.Transaction_Communication T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsTransaction_Communication();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Host_Computer_Id = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Protocol_Version = r.GetString(2);
                                if (!r.IsDBNull(3)) item.Request_Payload = r.GetString(3);
                                if (!r.IsDBNull(4)) item.Response_Payload = r.GetString(4);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Transaction_External_Client Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Transaction_External_ClientByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Transaction_External_Client WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Transaction_External_ClientDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Transaction_External_Client SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsTransaction_External_Client> Transaction_External_ClientListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsTransaction_External_Client>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.External_Client_Transactn_Guid
                                                                                                  FROM      Envision.Transaction_External_Client T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsTransaction_External_Client();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.External_Client_Transactn_Guid = r.GetString(1);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Transaction_Laundry_Machine Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Transaction_Laundry_MachineByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Transaction_Laundry_Machine WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Transaction_Laundry_MachineDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Transaction_Laundry_Machine SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsTransaction_Laundry_Machine> Transaction_Laundry_MachineListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsTransaction_Laundry_Machine>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.Transaction_Id,
                                                                                                            T.MachineNum,
                                                                                                            T.Pos_Id
                                                                                                  FROM      Envision.Transaction_Laundry_Machine T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsTransaction_Laundry_Machine();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.MachineNum = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Pos_Id = r.GetInt32(2);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region TransactionObjectLog Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void TransactionObjectLogByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.TransactionObjectLog WHERE Transaction_Id IN (SELECT Transaction_Id FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0)", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> TransactionObjectLogDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.TransactionObjectLog SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsTransactionObjectLog> TransactionObjectLogListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsTransactionObjectLog>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.TransactionObjectLogId,
                                                                                                            T.Object,
                                                                                                            T.Disposition,
                                                                                                            T.TransactionId,
                                                                                                            T.CreatedDateTime,
                                                                                                            T.ModifiedDateTime
                                                                                                  FROM      Envision.TransactionObjectLog T
                                                                                                  WHERE     T.Transaction_Id IN (
                                                                                                    SELECT  S.Transaction_Id
                                                                                                    FROM    Envision.Transaction S
                                                                                                    WHERE   S.BusinessDay_Date = {oleAutomationDate}
                                                                                                    AND     S.TransactionType_Code = 0
                                                                                                 )", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsTransactionObjectLog();

                                if (!r.IsDBNull(0)) item.TransactionObjectLogId = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Object = r.GetString(1);
                                if (!r.IsDBNull(2)) item.Disposition = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.TransactionId = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.CreatedDateTime = r.GetDateTime(4);
                                if (!r.IsDBNull(5)) item.ModifiedDateTime = r.GetDateTime(5);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsTransactionObjectLog> TransactionObjectLogListGet(ConnectionInfo connection)
        {
            var list = new List<TsTransactionObjectLog>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $@"SELECT    T.TransactionObjectLogId,
                                                                                                            T.Object,
                                                                                                            T.Disposition,
                                                                                                            T.TransactionId,
                                                                                                            T.CreatedDateTime,
                                                                                                            T.ModifiedDateTime
                                                                                                  FROM      Envision.TransactionObjectLog T
                                                                                                 ", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsTransactionObjectLog();

                                if (!r.IsDBNull(0)) item.TransactionObjectLogId = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Object = r.GetString(1);
                                if (!r.IsDBNull(2)) item.Disposition = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.TransactionId = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.CreatedDateTime = r.GetDateTime(4);
                                if (!r.IsDBNull(5)) item.ModifiedDateTime = r.GetDateTime(5);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Transaction Functions

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void TransactionByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            ExecuteTransactionalNonQuery(connection, $"DELETE FROM Envision.Transaction WHERE BusinessDay_Date = {oleAutomationDate} AND TransactionType_Code = 0", true);
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> TransactionDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<int>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = $"SELECT DISTINCT TAB.BusinessDay_Date FROM Envision.Transaction TAB WHERE TAB.BusinessDay_Date < {oleAutomationDate} AND TAB.TransactionType_Code = 0 AND TAB.Transaction_Id IN (SELECT SUB.Transaction_Id FROM Envision.Transaction SUB) ORDER BY TAB.BusinessDay_Date ASC", CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(r.GetInt32(0));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsTransaction> TransactionListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            var list = new List<TsTransaction>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = $@"SELECT     T.Transaction_Id,
                                                T.Pos_Id,
                                                T.BusinessDay_date,
                                                T.Period_Number,
                                                T.Transaction_Number,
                                                T.Sequence_Number,
                                                T.TransactionType_Code,
                                                T.Begin_DateTime,
                                                T.End_DateTime,
                                                T.Cancelled_Flag,
                                                T.Voided_Flag,
                                                T.Suspended_Flag,
                                                T.Suspended_Unsuspend_DateTime,
                                                T.Training_Flag,
                                                T.KeyedOffline_Flag,
                                                T.DbRecord_Create_DateTime,
                                                T.Validation_Type,
                                                T.Validation_Code,
                                                T.Attended_Type,
                                                T.InProcess_Flag
                                     FROM       Envision.Transaction T
                                     WHERE      T.BusinessDay_Date = {oleAutomationDate}
                                     AND        T.TransactionType_Code = 0",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsTransaction();

                                if (!r.IsDBNull(0)) item.Transaction_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Pos_Id = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.BusinessDay_date = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.Period_Number = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.Transaction_Number = r.GetInt32(4);
                                if (!r.IsDBNull(5)) item.Sequence_Number = r.GetInt32(5);
                                if (!r.IsDBNull(6)) item.TransactionType_Code = r.GetInt32(6);
                                if (!r.IsDBNull(7)) item.Begin_DateTime = DateTime.FromOADate(r.GetDouble(7));
                                if (!r.IsDBNull(8)) item.End_DateTime = DateTime.FromOADate(r.GetDouble(8));
                                if (!r.IsDBNull(9)) item.Cancelled_Flag = r.GetString(9);
                                if (!r.IsDBNull(10)) item.Voided_Flag = r.GetString(10);
                                if (!r.IsDBNull(11)) item.Suspended_Flag = r.GetString(11);
                                if (!r.IsDBNull(12)) item.Suspended_Unsuspend_DateTime = DateTime.FromOADate(r.GetDouble(12));
                                if (!r.IsDBNull(13)) item.Training_Flag = r.GetString(13);
                                if (!r.IsDBNull(14)) item.KeyedOffline_Flag = r.GetString(14);
                                if (!r.IsDBNull(15)) item.DbRecord_Create_DateTime = DateTime.FromOADate(r.GetDouble(15));
                                if (!r.IsDBNull(16)) item.Validation_Type = r.GetInt32(16);
                                if (!r.IsDBNull(17)) item.Validation_Code = r.GetInt32(17);
                                if (!r.IsDBNull(18)) item.Attended_Type = r.GetInt32(18);
                                if (!r.IsDBNull(19)) item.InProcess_Flag = r.GetString(19);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion
    }
}
