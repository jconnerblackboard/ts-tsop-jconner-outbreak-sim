﻿using System;
using System.Collections.Generic;
using System.Data;
using BbTS.Core;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Agent;
using BbTS.Domain.Models.Cashier;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.System.Database;
using Oracle.ManagedDataAccess.Client;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        #region Cashier

        /// <summary>
        /// This method will return a list of records for the Cashier objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsCashier> CashierArchivesGet(ConnectionInfo connection)
        {
            var list = new List<TsCashier>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (var cmd = new OracleCommand {
                  Connection = con,
                  CommandText = @"SELECT  Cashier_Id,
                                          Merchant_Id,
                                          CashierNumber,
                                          Is_Active,
                                          Pin,
                                          FullName,
                                          Manager,
                                          CashDrawer,
                                          Cust_Id,
                                          PinRequired,
                                          PinChangeRequired
                                  FROM    Envision.Cashier",
                  CommandType = CommandType.Text
                })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsCashier();

                                if (!r.IsDBNull(0)) item.Cashier_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Merchant_Id = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.CashierNumber = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.Is_Active = r.GetString(3);
                                if (!r.IsDBNull(4)) item.Pin = r.GetInt32(4);
                                if (!r.IsDBNull(5)) item.FullName = r.GetString(5);
                                if (!r.IsDBNull(6)) item.Manager = r.GetString(6);
                                if (!r.IsDBNull(7)) item.CashDrawer = r.GetInt32(7);
                                if (!r.IsDBNull(8)) item.Cust_Id = r.GetInt32(8);
                                if (!r.IsDBNull(9)) item.PinRequired = r.GetString(9);
                                if (!r.IsDBNull(10)) item.PinChangeRequired = r.GetString(10);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Get the cashier id value from the cashier Guid.
        /// </summary>
        /// <param name="cashierGuid">The unique identifier assigned to the cashier.</param>
        /// <returns>The cashier id as an int.</returns>
        public override int CashierIdFromGuidGet(string cashierGuid)
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand
                {
                    Connection = con,
                    CommandType = CommandType.Text,
                    CommandText = @"SELECT Cashier_Id FROM Envision.Cashier WHERE CashierGuid=:pValue"
                })
                {
                    try
                    {
                        con.Open();
                        if (!string.IsNullOrEmpty(cashierGuid))
                            cmd.Parameters.Add(
                                new OracleParameter
                                {
                                    ParameterName = "pValue",
                                    OracleDbType = OracleDbType.Varchar2,
                                    Size = 36,
                                    Value = new Guid(cashierGuid).ToString("D"),
                                    Direction = ParameterDirection.Input
                                });

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var cashierId = !r.IsDBNull(0) ? r.GetInt32(0) : 0;
                                return cashierId;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            return 0;
        }

        #endregion
    }
}
