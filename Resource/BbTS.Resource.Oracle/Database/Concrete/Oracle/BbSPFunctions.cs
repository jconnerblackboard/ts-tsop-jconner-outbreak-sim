﻿using BbTS.Core;
using BbTS.Core.Logging.Tracing;
using BbTS.Core.Security.Encryption;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.Institution;
using BbTS.Domain.Models.System;
using BbTS.Domain.Models.System.Security.Oauth;
using BbTS.Monitoring.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using BbTS.Domain.Models.BbSp;
using Formatting = BbTS.Core.Conversion.Formatting;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    /// <summary>
    /// This class exposes various methods to update system objects in Transact
    /// </summary>
    [Trace(AttributePriority = 2)]
    public partial class OracleSource
    {

        #region Application Credential

        /// <summary>
        /// Delete all application credentials in this database resource instance.
        /// </summary>
        public override void ApplicationCredentialsDelete()
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandType = CommandType.Text, CommandText = @"Delete From  BbSPApplicationCredential" })
                    {
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// RefreshApplicationCredentialData : Deletes all Application Credential data from database and insert application credential data received from Service Agent.
        /// </summary>
        /// <param name="applicationCredentials">The application credential data entities.</param>
        public override void RefreshApplicationCredentialData(IEnumerable<SpApplicationCredential> applicationCredentials)
        {
            ApplicationCredentialsDelete();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");

                try
                {
                    con.Open();
                    var encryptor = new AesEncryptionProvider();

                    foreach (var appCredential in applicationCredentials)
                    {
                        var consumerKey = encryptor.EncryptToBase64String(appCredential.ConsumerKey.ToString("D"));
                        var consumerSecret = encryptor.EncryptToBase64String(appCredential.ConsumerSecret);

                        using (var cmd = new OracleCommand { Connection = con, CommandText = "BbSP_Functions.ApplicationCredentialSet", CommandType = CommandType.StoredProcedure })
                        {
                            cmd.Parameters.Add(new OracleParameter { ParameterName = "pGuid", OracleDbType = OracleDbType.Char, Size = 36, Value = appCredential.ApplicationCredentialId.ToString("D"), Direction = ParameterDirection.Input });
                            cmd.Parameters.Add(new OracleParameter { ParameterName = "pConsumerKey", OracleDbType = OracleDbType.NVarchar2, Size = 2000, Value = consumerKey, Direction = ParameterDirection.Input });
                            cmd.Parameters.Add(new OracleParameter { ParameterName = "pConsumerSecret", OracleDbType = OracleDbType.NVarchar2, Size = 2000, Value = consumerSecret, Direction = ParameterDirection.Input });
                            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIsEnabled", OracleDbType = OracleDbType.Char, Size = 1, Value = Formatting.BooleanToTf(appCredential.IsEnabled), Direction = ParameterDirection.Input });
                            cmd.Parameters.Add(new OracleParameter { ParameterName = "pApplicationId", OracleDbType = OracleDbType.Char, Size = 36, Value = appCredential.ApplicationId.ToString("D"), Direction = ParameterDirection.Input });
                            cmd.Parameters.Add(new OracleParameter { ParameterName = "pApplicationName", OracleDbType = OracleDbType.Varchar2, Size = 2000, Value = appCredential.ApplicationName, Direction = ParameterDirection.Input });
                            cmd.Parameters.Add(new OracleParameter { ParameterName = "pAuthTokenExpirationSeconds", OracleDbType = OracleDbType.Int32, Size = 10, Value = appCredential.AuthTokenExpirationSeconds, Direction = ParameterDirection.Input });
                            cmd.Parameters.Add(new OracleParameter { ParameterName = "pInstitutionId", OracleDbType = OracleDbType.Char, Size = 36, Value = appCredential.InstitutionId.ToString("D"), Direction = ParameterDirection.Input });
                            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDomainId", OracleDbType = OracleDbType.Char, Size = 36, Value = appCredential.DomainId.ToString("D"), Direction = ParameterDirection.Input });

                            cmd.ExecuteNonQuery();
                        }

                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Gets application credential by consumer key.
        /// </summary>
        /// <param name="consumerKey">The consumer key on which to base the query for application credential</param>
        /// <param name="institutionId">The institution identifier.</param>
        /// <returns>The application credential by consumer key.</returns>
        public override SpApplicationCredential ApplicationCredentialGet(Guid consumerKey, Guid? institutionId = null)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    var aes = new AesEncryptionProvider();
                    var consumerKeyEncrypted = aes.EncryptToBase64String(consumerKey.ToString("D"));

                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "BbSP_Functions.ApplicationCredentialGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pConsumerKey", OracleDbType = OracleDbType.Varchar2, Size = 2000, Value = consumerKeyEncrypted, Direction = ParameterDirection.Input });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pInstitutionId", OracleDbType = OracleDbType.Char, Size = 36, Value = institutionId?.ToString("D"), Direction = ParameterDirection.Input });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            if (r.Read())
                            {
                                return new SpApplicationCredential()
                                {
                                    ApplicationCredentialId = !r.IsDBNull(0) ? Guid.Parse(r.GetString(0)) : Guid.Empty,
                                    ConsumerKey = !r.IsDBNull(1) ? Guid.Parse(aes.DecryptFromBase64String(r.GetString(1))) : Guid.Empty,
                                    ConsumerSecret = !r.IsDBNull(2) ? aes.DecryptFromBase64String(r.GetString(2)) : string.Empty,
                                    IsEnabled = !r.IsDBNull(3) && Formatting.TfStringToBool(r.GetString(3)),
                                    ApplicationId = !r.IsDBNull(4) ? Guid.Parse(r.GetString(4)) : Guid.Empty,
                                    ApplicationName = !r.IsDBNull(5) ? r.GetString(5) : string.Empty,
                                    AuthTokenExpirationSeconds = !r.IsDBNull(6) ? r.GetInt32(6) : -1,
                                    InstitutionId = !r.IsDBNull(7) ? Guid.Parse(r.GetString(7)) : Guid.Empty,
                                    DomainId = !r.IsDBNull(8) ? Guid.Parse(r.GetString(8)) : Guid.Empty
                                };
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return null;
        }

        /// <summary>
        /// Query the data layer for the first consumer key and secret.
        /// </summary>
        /// <returns>First result of the consumer key and secret.</returns>
        public override OAuthParameters ConsumerKeyAndSecretGetFirstValue()
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand{ Connection = con, CommandType = CommandType.Text, CommandText = @"SELECT ConsumerKey, ConsumerSecret, IsEnabled FROM BbSPApplicationCredential" })
                {
                    con.Open();

                    using (var r = cmd.ExecuteReader())
                    {
                        var decryptor = new AesEncryptionProvider();
                        while (r.Read())
                        {
                            var consumerKey = !r.IsDBNull(0) ? decryptor.DecryptFromBase64String(r.GetString(0)) : string.Empty;
                            Guard.IsNotNullOrWhiteSpace(consumerKey, "consumerKey");

                            var consumerSecret = !r.IsDBNull(1) ? decryptor.DecryptFromBase64String(r.GetString(1)) : string.Empty;
                            Guard.IsNotNullOrWhiteSpace(consumerSecret, "consumerSecret");

                            var isEnabled = !r.IsDBNull(2) && Formatting.TfStringToBool(r.GetString(2));
                            if (isEnabled)
                            {
                                return new OAuthParameters
                                {
                                    ConsumerKey = consumerKey,
                                    ConsumerSecret = consumerSecret
                                };
                            }
                        }
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Query the data layer for all of the consumer key and secret information.
        /// </summary>
        /// <returns>Consumer key and secret information.</returns>
        public override List<BbSpApplicationCredential> ConsumerKeyAndSecretGetAllValues()
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");

            var list = new List<BbSpApplicationCredential>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandType = CommandType.Text,
                    CommandText = @"
                        SELECT  GUID ,
                                CONSUMERKEY ,
                                CONSUMERSECRET ,
                                ISENABLED ,
                                APPLICATIONID ,
                                APPLICATIONNAME ,
                                AUTHTOKENEXPIRATIONSECONDS ,
                                INSTITUTIONID ,
                                DOMAINID
                        FROM    BbSPApplicationCredential"
                })
                {
                    con.Open();

                    using (var r = cmd.ExecuteReader())
                    {
                        var decryptor = new AesEncryptionProvider();
                        while (r.Read())
                        {
                            var guid = !r.IsDBNull(0) ? r.GetString(0) : string.Empty;
                            var consumerKey = !r.IsDBNull(1) ? decryptor.DecryptFromBase64String(r.GetString(1)) : string.Empty;
                            var consumerSecret = !r.IsDBNull(2) ? decryptor.DecryptFromBase64String(r.GetString(2)) : string.Empty;
                            var isEnabled = !r.IsDBNull(3) && Formatting.TfStringToBool(r.GetString(3));
                            var applicationId = !r.IsDBNull(4) ? r.GetString(4) : string.Empty;
                            var applicationName = !r.IsDBNull(5) ? r.GetString(5) : string.Empty;
                            var expiry = !r.IsDBNull(6) ? r.GetInt32(6) : 0;
                            var institutionId = !r.IsDBNull(7) ? r.GetString(7) : string.Empty;
                            var domainId = !r.IsDBNull(8) ? r.GetString(8) : string.Empty;

                            if (isEnabled)
                            {
                                list.Add(new BbSpApplicationCredential
                                {
                                    Guid = guid,
                                    ConsumerKey = consumerKey,
                                    ConsumerSecret = consumerSecret,
                                    IsEnabled = isEnabled,
                                    ApplicationId = applicationId,
                                    ApplicationName = applicationName,
                                    AuthTokenExpirationSeconds = expiry,
                                    InstitutionId = institutionId,
                                    DomainId = domainId
                                });
                            }
                        }
                    }
                }
            }
            return list;
        }

        /// <summary>
        /// Query the data layer for all of the merchant information.
        /// </summary>
        /// <returns>List of merchants.</returns>
        public override List<BbSpMerchant> BbSpMerchantsGetAll()
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");

            var list = new List<BbSpMerchant>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandType = CommandType.Text,
                    CommandText = @"
                        SELECT  GUID ,
                                NAME ,
                                KEY ,
                                SECRET ,
                                ISKEYENABLED ,
                                INSTITUTIONID ,
                                DOMAINID  
                        FROM    BbSPMerchant"
                })
                {
                    con.Open();

                    using (var r = cmd.ExecuteReader())
                    {
                        var decryptor = new AesEncryptionProvider();
                        while (r.Read())
                        {
                            var guid = !r.IsDBNull(0) ? r.GetString(0) : string.Empty;
                            var name = !r.IsDBNull(1) ? r.GetString(1) : string.Empty;
                            var consumerKey = !r.IsDBNull(2) ? decryptor.DecryptFromBase64String(r.GetString(2)) : string.Empty;
                            var consumerSecret = !r.IsDBNull(3) ? decryptor.DecryptFromBase64String(r.GetString(3)) : string.Empty;
                            var isEnabled = !r.IsDBNull(4) && Formatting.TfStringToBool(r.GetString(4));
                            var institutionId = !r.IsDBNull(5) ? r.GetString(5) : string.Empty;
                            var domainId = !r.IsDBNull(6) ? r.GetString(6) : string.Empty;

                            if (isEnabled)
                            {
                                list.Add(new BbSpMerchant
                                {
                                    Guid = guid,
                                    Name = name,
                                    Key = consumerKey,
                                    Secret = consumerSecret,
                                    IsKeyEnabled = isEnabled,
                                    InstitutionId = institutionId,
                                    DomainId = domainId
                                });
                            }
                        }
                    }
                }
            }
            return list;
        }

        #endregion

        #region Institution

        /// <summary>
        /// Delete all institution routes in this database resource instance.
        /// </summary>
        public override void InstitutionRoutesDelete()
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandType = CommandType.Text, CommandText = @"Delete From  BbSPInstitutionRoute" })
                    {
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// RefreshInstitutionRouteData : Deletes all institution route data from database and insert institution route data received from Service Agent.
        /// </summary>
        /// <param name="institutionRoutes">The institution route data entities.</param>
        public override void RefreshInstitutionRouteData(IEnumerable<SpInstitutionRouteEntity> institutionRoutes)
        {
            InstitutionRoutesDelete();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                try
                {
                    con.Open();

                    foreach (var institutionRoute in institutionRoutes)
                    {
                        using (var cmd = new OracleCommand { Connection = con, CommandText = "BbSP_Functions.InstitutionRouteSet", CommandType = CommandType.StoredProcedure })
                        {
                            cmd.Parameters.Add(new OracleParameter { ParameterName = "pGuid", OracleDbType = OracleDbType.Char, Size = 36, Value = institutionRoute.InstitutionRouteId.ToString("D"), Direction = ParameterDirection.Input });
                            cmd.Parameters.Add(new OracleParameter { ParameterName = "pSchemeTypeApiKey", OracleDbType = OracleDbType.Varchar2, Size = 2000, Value = institutionRoute.InstitutionRouteSchemeTypeApiKey, Direction = ParameterDirection.Input });
                            cmd.Parameters.Add(new OracleParameter { ParameterName = "pValue", OracleDbType = OracleDbType.Varchar2, Size = 2000, Value = institutionRoute.Value, Direction = ParameterDirection.Input });
                            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIsEnabled", OracleDbType = OracleDbType.Char, Size = 1, Value = Formatting.BooleanToTf(institutionRoute.Enabled), Direction = ParameterDirection.Input });
                            cmd.Parameters.Add(new OracleParameter { ParameterName = "pInstitutionId", OracleDbType = OracleDbType.Char, Size = 36, Value = institutionRoute.InstitutionId.ToString("D"), Direction = ParameterDirection.Input });
                            cmd.Parameters.Add(new OracleParameter { ParameterName = "pInstitutionName", OracleDbType = OracleDbType.Varchar2, Size = 2000, Value = institutionRoute.InstitutionName, Direction = ParameterDirection.Input });
                            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDomainId", OracleDbType = OracleDbType.Char, Size = 36, Value = institutionRoute.DomainId.ToString("D"), Direction = ParameterDirection.Input });

                            cmd.ExecuteNonQuery();
                        }

                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Gets the institution Route by Route which contains Institution details.
        /// </summary>
        /// <param name="schemeTypeApiKey">The institution Route Scheme Type.</param>
        /// <param name="value">The institution Route Value.</param>
        /// <param name="institutionId">The institution identifier.</param>
        /// <returns>The institution Route which contains Institution details.</returns>
        public override SpInstitutionRouteEntity InstitutionRouteGet(string schemeTypeApiKey, string value, Guid? institutionId = null)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "BbSP_Functions.InstitutionRouteGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pSchemeTypeApiKey", OracleDbType = OracleDbType.Varchar2, Size = 2000, Value = schemeTypeApiKey, Direction = ParameterDirection.Input });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pValue", OracleDbType = OracleDbType.Varchar2, Size = 2000, Value = value, Direction = ParameterDirection.Input });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pInstitutionId", OracleDbType = OracleDbType.Char, Size = 36, Value = institutionId?.ToString("D"), Direction = ParameterDirection.Input });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            if (r.Read())
                            {
                                return new SpInstitutionRouteEntity()
                                {
                                    InstitutionRouteId = !r.IsDBNull(0) ? Guid.Parse(r.GetString(0)) : Guid.Empty,
                                    InstitutionRouteSchemeTypeApiKey = !r.IsDBNull(1) ? r.GetString(1) : string.Empty,
                                    Value = !r.IsDBNull(2) ? r.GetString(2) : string.Empty,
                                    Enabled = !r.IsDBNull(3) && Formatting.TfStringToBool(r.GetString(3)),
                                    InstitutionId = !r.IsDBNull(4) ? Guid.Parse(r.GetString(4)) : Guid.Empty,
                                    InstitutionName = !r.IsDBNull(5) ? r.GetString(5) : string.Empty,
                                    DomainId = !r.IsDBNull(6) ? Guid.Parse(r.GetString(6)) : Guid.Empty
                                };
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return null;
        }

        #endregion

        #region Merchant

        /// <summary>
        /// Delete all merchants in this database resource instance.
        /// </summary>
        public override void MerchantsDelete()
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandType = CommandType.Text, CommandText = @"Delete From  BbSPMerchant" })
                    {
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// RefreshMerchantData : Deletes all data from database and insert merchant data received from Service Agent.
        /// </summary>
        /// <param name="merchants">The merchant data entities.</param>
        public override void RefreshMerchantData(IEnumerable<SpMerchantEntity> merchants)
        {
            MerchantsDelete();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                try
                {
                    con.Open();

                    var aes = new AesEncryptionProvider();
                    foreach (var merchant in merchants)
                    {
                        
                        var merchantKeyEncrypted = aes.EncryptToBase64String(merchant.MerchantKey.ToString("D"));
                        var merchantSecretEncrypted = aes.EncryptToBase64String(merchant.MerchantSecret);
                        using (var cmd = new OracleCommand { Connection = con, CommandText = "BbSP_Functions.MerchantSet", CommandType = CommandType.StoredProcedure })
                        {
                            cmd.Parameters.Add(new OracleParameter { ParameterName = "pGuid", OracleDbType = OracleDbType.Char, Size = 36, Value = merchant.MerchantId.ToString("D"), Direction = ParameterDirection.Input });
                            cmd.Parameters.Add(new OracleParameter { ParameterName = "pName", OracleDbType = OracleDbType.Varchar2, Size = 2000, Value = merchant.Name, Direction = ParameterDirection.Input });
                            cmd.Parameters.Add(new OracleParameter { ParameterName = "pKey", OracleDbType = OracleDbType.NVarchar2, Size = 2000, Value = merchantKeyEncrypted, Direction = ParameterDirection.Input });
                            cmd.Parameters.Add(new OracleParameter { ParameterName = "pSecret", OracleDbType = OracleDbType.NVarchar2, Size = 2000, Value = merchantSecretEncrypted, Direction = ParameterDirection.Input });
                            cmd.Parameters.Add(new OracleParameter { ParameterName = "pIsKeyEnabled", OracleDbType = OracleDbType.Char, Size = 1, Value = Formatting.BooleanToTf(merchant.MerchantKeyEnabled), Direction = ParameterDirection.Input });
                            cmd.Parameters.Add(new OracleParameter { ParameterName = "pInstitutionId", OracleDbType = OracleDbType.Char, Size = 36, Value = merchant.InstitutionId.ToString("D"), Direction = ParameterDirection.Input });
                            cmd.Parameters.Add(new OracleParameter { ParameterName = "pDomainId", OracleDbType = OracleDbType.Char, Size = 36, Value = merchant.DomainId.ToString("D"), Direction = ParameterDirection.Input });

                            cmd.ExecuteNonQuery();
                        }

                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Gets the Merchant by Id.
        /// </summary>
        /// <param name="merchantId">The Merchant id.</param>
        /// <param name="institutionId">The institution identifier.</param>
        /// <returns>The Merchant</returns>
        public override SpMerchantEntity MerchantGet(Guid merchantId, Guid? institutionId = null)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "BbSP_Functions.MerchantGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pGuid", OracleDbType = OracleDbType.Char, Size = 36, Value = merchantId.ToString("D"), Direction = ParameterDirection.Input });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pInstitutionId", OracleDbType = OracleDbType.Char, Size = 36, Value = institutionId?.ToString("D"), Direction = ParameterDirection.Input });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            if (r.Read())
                            {
                                var aes = new AesEncryptionProvider();
                                return new SpMerchantEntity()
                                {
                                    MerchantId = !r.IsDBNull(0) ? Guid.Parse(r.GetString(0)) : Guid.Empty,
                                    Name = !r.IsDBNull(1) ? r.GetString(1) : string.Empty,
                                    MerchantKey = !r.IsDBNull(2) ? Guid.Parse(aes.DecryptFromBase64String(r.GetString(2))) : Guid.Empty,
                                    MerchantSecret = !r.IsDBNull(3) ? aes.DecryptFromBase64String(r.GetString(3)) : string.Empty,
                                    MerchantKeyEnabled = !r.IsDBNull(4) && Formatting.TfStringToBool(r.GetString(4)),
                                    InstitutionId = !r.IsDBNull(5) ? Guid.Parse(r.GetString(5)) : Guid.Empty,
                                    DomainId = !r.IsDBNull(6) ? Guid.Parse(r.GetString(6)) : Guid.Empty
                                };
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return null;
        }

        #endregion

        #region Transaction System

        /// <summary>
        /// Retrieve the transaction system associated with the data layer
        /// </summary>
        /// <returns><see cref="TransactionSystem"/> object</returns>
        public override TransactionSystem TransactionSystemGet()
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    var transactInstanceGuid = Guid.Parse(ControlParameterValueGet("TransactInstanceGuid"));
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "BbSP_Functions.TransactionSystemGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDomainId", OracleDbType = OracleDbType.Char, Size = 36, Direction = ParameterDirection.Input, Value = transactInstanceGuid });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            if (r.Read())
                            {
                                return new TransactionSystem()
                                {
                                    TransactionSystemId = !r.IsDBNull(0) ? Guid.Parse(r.GetString(0)) : Guid.Empty,
                                    InstitutionId = !r.IsDBNull(1) ? Guid.Parse(r.GetString(1)) : Guid.Empty,
                                    InstanceId = transactInstanceGuid
                                };
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return null;
        }

        /// <summary>
        /// Add a transaction system to this database resource instance.
        /// </summary>
        /// <param name="transactionSystem"><see cref="TransactionSystem"/> object to add.</param>
        public override void TransactionSystemAdd(TransactionSystem transactionSystem)
        {
            TransactionSystemsDelete();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "BbSP_Functions.TransactionSystemSet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pGuid", OracleDbType = OracleDbType.Char, Size = 36, Value = transactionSystem.TransactionSystemId.ToString("D"), Direction = ParameterDirection.Input });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pInstitutionId", OracleDbType = OracleDbType.Char, Size = 36, Value = transactionSystem.InstitutionId.ToString("D"), Direction = ParameterDirection.Input });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pIsValid", OracleDbType = OracleDbType.Char, Size = 1, Value = Formatting.BooleanToTf(transactionSystem.IsValid), Direction = ParameterDirection.Input });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDomainId", OracleDbType = OracleDbType.Char, Size = 36, Value = transactionSystem.InstanceId.ToString("D"), Direction = ParameterDirection.Input });

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Delete all transaciton systems in this database resource instance.
        /// </summary>
        public override void TransactionSystemsDelete()
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandType = CommandType.Text, CommandText = @"Delete From  BbSPTransactionSystem" })
                    {
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        #endregion

    }
}
