﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using BbTS.Core;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.DoorAccess;
using BbTS.Domain.Models.Exceptions.Resource;
using Oracle.ManagedDataAccess.Client;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        /// <inheritdoc />
        public override List<TsReuseDelayLog> ReuseDelayLogListGet(int customerId)
        {
            var list = new List<TsReuseDelayLog>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "ReuseDelayFunction.CustomerReuseDelayLogListGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pRecordCount", OracleDbType = OracleDbType.Int32, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsReuseDelayLog()
                                {
                                    Id = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    ReuseDelayName = !r.IsDBNull(1) ? r.GetString(1) : null,
                                    ObjectId = !r.IsDBNull(2) ? r.GetInt32(2) : -1,
                                    ObjectType = !r.IsDBNull(3) ? r.GetString(3) : null,
                                    ObjectName = !r.IsDBNull(4) ? r.GetString(4) : null,
                                    LastAccess = !r.IsDBNull(5) ? r.GetString(5) : null,
                                    DelayTime = !r.IsDBNull(6) ? r.GetString(6) : null,
                                    TimeRemaining = !r.IsDBNull(7) ? r.GetString(7) : null
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <inheritdoc />
        public override void ReuseDelayLogReset(int customerId, List<int> reuseDelayLogIds)
        {
            if (reuseDelayLogIds == null || !reuseDelayLogIds.Any())
                return;

            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand
                    {
                        Connection = con,
                        CommandText = $@"UPDATE  ReuseDelayLog
                                        SET     LogDateTime = -1
                                        WHERE   ReuseDelayLogId IN (
                                          SELECT      SIB.ReuseDelayLogId
                                          FROM        ReuseDelayLog     RDL
                                          INNER JOIN  ReuseDelayObject  RDO ON  RDL.ReuseDelayObjectId = RDO.ReuseDelayObjectId
                                          INNER JOIN  ReuseDelay        RD  ON  RDO.ReuseDelayId       = RD.ReuseDelayId
                                          INNER JOIN  ReuseDelayLog     SIB ON  RDL.ReuseDelayObjectId = SIB.ReuseDelayObjectId
                                                                            AND RDL.Cust_Id            = SIB.Cust_Id
                                          WHERE       RDL.ReuseDelayLogId IN ({string.Join(",", reuseDelayLogIds)})
                                          AND         RDL.Cust_Id = :pCustomerId
                                          AND         SIB.LogDateTime + RD.Time > Udf_Functions.Now()
                                        )",
                        CommandType = CommandType.Text
                    })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerId });
                        con.Open();

                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }
    }
}