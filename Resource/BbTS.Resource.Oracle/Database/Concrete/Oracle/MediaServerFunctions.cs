﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Exceptions.Resource;
using Oracle.ManagedDataAccess.Client;
using BbTS.Domain.Models.MediaServer;
using BbTS.Monitoring.Logging;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {

        #region Device

        /// <inheritdoc />
        public override void DeviceDelete(int deviceId, string identity, string authenticationKey)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "MediaServer.DeviceDelete", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pDeviceId", OracleDbType.Int32, deviceId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIdentity", OracleDbType.Varchar2, identity?.Length ?? 0, identity, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAuthenticationKey", OracleDbType.Varchar2, 50, authenticationKey, ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException(string.Empty, Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override List<DeviceView> DeviceGet(int? deviceId, string macAddress, string identity, string authenticationKey)
        {
            var returnValue = new List<DeviceView>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "MediaServer.DeviceGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pDeviceId", OracleDbType.Int32, deviceId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pMacAddress", OracleDbType.Varchar2, 17, macAddress, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIdentity", OracleDbType.Varchar2, identity?.Length ?? 0, identity, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAuthenticationKey", OracleDbType.Varchar2, 50, authenticationKey, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pList", OracleDbType.RefCursor, ParameterDirection.Output));

                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                returnValue.Add(new DeviceView
                                {
                                    Id = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    Name = !r.IsDBNull(1) ? r.GetString(1) : null,
                                    DeviceGroupId = !r.IsDBNull(2) ? r.GetInt32(2) : -1,
                                    PosId = !r.IsDBNull(3) ? r.GetInt32(3) : -1,
                                    DeviceGroupName = !r.IsDBNull(4) ? r.GetString(4) : null,
                                    Type = !r.IsDBNull(5) ? r.GetInt32(5) : -1,
                                    MacAddress = !r.IsDBNull(6) ? r.GetString(6) : null,
                                    ProfitCenterName = !r.IsDBNull(7) ? r.GetString(7) : null
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException(string.Empty, Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
            return returnValue;
        }

        /// <inheritdoc />
        public override void DeviceSet(int? deviceId, Device device, string identity, string authenticationKey)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "MediaServer.DeviceSet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pDeviceId", OracleDbType.Int32, deviceId, ParameterDirection.InputOutput));
                        cmd.Parameters.Add(new OracleParameter("pName", OracleDbType.Varchar2, 50, device.Name, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pDeviceGroupId", OracleDbType.Int32, device.DeviceGroupId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pPosId", OracleDbType.Int32, device.PosId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIdentity", OracleDbType.Varchar2, 30, identity, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAuthenticationKey", OracleDbType.Varchar2, 50, authenticationKey, ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();

                        if (!deviceId.HasValue)
                            device.Id = Convert.ToInt32(cmd.Parameters["pDeviceId"].Value.ToString());
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException(string.Empty, Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override void DeviceGroupDelete(int deviceGroupId, string identity, string authenticationKey)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "MediaServer.DeviceGroupDelete", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pDeviceGroupId", OracleDbType.Int32, deviceGroupId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIdentity", OracleDbType.Varchar2, identity?.Length ?? 0, identity, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAuthenticationKey", OracleDbType.Varchar2, 50, authenticationKey, ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException(string.Empty, Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override List<DeviceGroup> DeviceGroupGet(int? deviceGroupId, string identity, string authenticationKey)
        {
            var returnValue = new List<DeviceGroup>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "MediaServer.DeviceGroupGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pDeviceGroupId", OracleDbType.Int32, deviceGroupId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIdentity", OracleDbType.Varchar2, identity?.Length ?? 0, identity, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAuthenticationKey", OracleDbType.Varchar2, 50, authenticationKey, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pList", OracleDbType.RefCursor, ParameterDirection.Output));
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                returnValue.Add(new DeviceGroup
                                {
                                    Id = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    Name = !r.IsDBNull(1) ? r.GetString(1) : null,
                                    PlayListId = !r.IsDBNull(2) ? r.GetInt32(2) : -1,
                                    BackgroundColor = !r.IsDBNull(3) ? r.GetString(3) : null,
                                    ForegroundColor = !r.IsDBNull(4) ? r.GetString(4) : null,
                                    ScrollOverrideMessage = !r.IsDBNull(5) ? r.GetString(5) : null,
                                    OverrideMessageBackgroundColor = !r.IsDBNull(6) ? r.GetString(6) : null,
                                    OverrideMessageForegroundColor = !r.IsDBNull(7) ? r.GetString(7) : null
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException(string.Empty, Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
            return returnValue;
        }

        /// <inheritdoc />
        public override void DeviceGroupSet(int? deviceGroupId, DeviceGroup deviceGroup, string identity, string authenticationKey)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "MediaServer.DeviceGroupSet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pDeviceGroupId", OracleDbType.Int32, deviceGroupId, ParameterDirection.InputOutput));
                        cmd.Parameters.Add(new OracleParameter("pName", OracleDbType.Varchar2, 50, deviceGroup.Name, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pPlaylistId", OracleDbType.Int32, deviceGroup.PlayListId.Equals(0) ? new int?() : deviceGroup.PlayListId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pBackgroundColor", OracleDbType.Varchar2, 7, deviceGroup.BackgroundColor, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pForegroundColor", OracleDbType.Varchar2, 7, deviceGroup.ForegroundColor, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pScrollOverrideMessage", OracleDbType.Varchar2, 100, deviceGroup.ScrollOverrideMessage, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pOverrideMessageBackgrndColor", OracleDbType.Varchar2, 7, deviceGroup.OverrideMessageBackgroundColor, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pOverrideMessageForegrndColor", OracleDbType.Varchar2, 7, deviceGroup.OverrideMessageForegroundColor, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIdentity", OracleDbType.Varchar2, identity?.Length ?? 0, identity, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAuthenticationKey", OracleDbType.Varchar2, 50, authenticationKey, ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();

                        if (deviceGroup.Id == 0)
                            deviceGroup.Id = Convert.ToInt32(cmd.Parameters["pDeviceGroupId"].Value.ToString());
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException(string.Empty, Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override void DeviceGroupRssFeedDelete(int deviceGroupId, int rssFeedId, string identity, string authenticationKey)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "MediaServer.DeviceGroupRssFeedDelete", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pDeviceGroupId", OracleDbType.Int32, deviceGroupId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pRssFeedId", OracleDbType.Int32, rssFeedId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIdentity", OracleDbType.Varchar2, identity?.Length ?? 0, identity, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAuthenticationKey", OracleDbType.Varchar2, 50, authenticationKey, ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException(string.Empty, Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override List<DeviceGroupRssFeed> DeviceGroupRssFeedGet(int? deviceGroupId, int? rssFeedId, string identity, string authenticationKey)
        {
            var returnValue = new List<DeviceGroupRssFeed>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "MediaServer.DeviceGroupRssFeedGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pDeviceGroupId", OracleDbType.Int32, deviceGroupId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pRssFeedId", OracleDbType.Int32, rssFeedId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIdentity", OracleDbType.Varchar2, identity?.Length ?? 0, identity, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAuthenticationKey", OracleDbType.Varchar2, 50, authenticationKey, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pList", OracleDbType.RefCursor, ParameterDirection.Output));

                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                returnValue.Add(new DeviceGroupRssFeed
                                {
                                    DeviceGroupId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    RssFeedId = !r.IsDBNull(1) ? r.GetInt32(1) : -1
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException(string.Empty, Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
            return returnValue;
        }

        /// <inheritdoc />
        public override void DeviceGroupRssFeedSet(DeviceGroupRssFeed deviceGroupRssFeed, string identity, string authenticationKey)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "MediaServer.DeviceGroupRssFeedSet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pDeviceGroupId", OracleDbType.Int32, deviceGroupRssFeed.DeviceGroupId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pRssFeedId", OracleDbType.Int32, deviceGroupRssFeed.RssFeedId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIdentity", OracleDbType.Varchar2, identity?.Length ?? 0, identity, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAuthenticationKey", OracleDbType.Varchar2, 50, authenticationKey, ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException(string.Empty, Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        #endregion

        #region Media Files

        /// <inheritdoc />
        public override void MediaFileDelete(int mediaFileId, string identity, string authenticationKey)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "MediaServer.MediaFileDelete", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pMediaFileId", OracleDbType.Int32, mediaFileId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIdentity", OracleDbType.Varchar2, identity?.Length ?? 0, identity, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAuthenticationKey", OracleDbType.Varchar2, 50, authenticationKey, ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException(string.Empty, Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override List<MediaFile> MediaFileGet(int? mediaFileId, string identity, string authenticationKey)
        {
            var returnValue = new List<MediaFile>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "MediaServer.MediaFileGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pMediaFileId", OracleDbType.Int32, mediaFileId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIdentity", OracleDbType.Varchar2, identity?.Length ?? 0, identity, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAuthenticationKey", OracleDbType.Varchar2, 50, authenticationKey, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pList", OracleDbType.RefCursor, ParameterDirection.Output));

                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                returnValue.Add(new MediaFile
                                {
                                    Id = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    Name = !r.IsDBNull(1) ? r.GetString(1) : null,
                                    FileSizeBytes = !r.IsDBNull(2) ? r.GetInt32(2) : -1,
                                    FileName = !r.IsDBNull(3) ? r.GetString(3) : null,
                                    ContentType = !r.IsDBNull(4) ? r.GetString(4) : null,
                                    TargetDeviceType = !r.IsDBNull(5) ? r.GetInt32(5) : -1,
                                    IsConversionComplete = !r.IsDBNull(6) && r.GetInt32(6) == 1
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException(string.Empty, Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
            return returnValue;
        }

        /// <inheritdoc />
        public override void MediaFileSet(int? mediaFileId, MediaFile mediaFile, string identity, string authenticationKey)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "MediaServer.MediaFileSet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pMediaFileId", OracleDbType.Int32, mediaFileId, ParameterDirection.InputOutput));
                        cmd.Parameters.Add(new OracleParameter("pName", OracleDbType.Varchar2, 100, mediaFile.Name, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pFileName", OracleDbType.Varchar2, 255, mediaFile.FileName, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pFileSizeBytes", OracleDbType.Int32, mediaFile.FileSizeBytes, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pContentType", OracleDbType.Varchar2, 30, mediaFile.ContentType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTargetDeviceType", OracleDbType.Int32, mediaFile.TargetDeviceType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIdentity", OracleDbType.Varchar2, identity?.Length ?? 0, identity, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAuthenticationKey", OracleDbType.Varchar2, 50, authenticationKey, ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();

                        if (mediaFileId.HasValue)
                            return;

                        mediaFile.Id = Convert.ToInt32(cmd.Parameters["pMediaFileId"].Value.ToString());
                        MediaFileDetailSet(null,new MediaFileDetail()
                        {
                            ParentId = mediaFile.Id,
                            TargetDeviceType = mediaFile.TargetDeviceType
                        }, identity, authenticationKey);
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException(string.Empty, Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override void MediaFileDetailDelete(int mediaFileDetailId, string identity, string authenticationKey)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "MediaServer.MediaFileDetailDelete", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pMediaFileDetailId", OracleDbType.Int32, mediaFileDetailId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIdentity", OracleDbType.Varchar2, identity?.Length ?? 0, identity, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAuthenticationKey", OracleDbType.Varchar2, 50, authenticationKey, ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException(string.Empty, Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override void MediaFileDetailSet(int? mediaFileDetailId, MediaFileDetail mediaFileDetail, string identity, string authenticationKey)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "MediaServer.MediaFileDetailSet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pMediaFileDetailId", OracleDbType.Int32, mediaFileDetailId, ParameterDirection.InputOutput));
                        cmd.Parameters.Add(new OracleParameter("pMediaFileId", OracleDbType.Int32, mediaFileDetail.ParentId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTargetDeviceType", OracleDbType.Int32, mediaFileDetail.TargetDeviceType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pFileSizeBytes", OracleDbType.Int32, mediaFileDetail.FileSizeBytes, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pMD5Hash", OracleDbType.Varchar2, 32, mediaFileDetail.Md5Hash, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pLocation", OracleDbType.Varchar2, 255, mediaFileDetail.Location, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pVideoLength", OracleDbType.Int32, 0, mediaFileDetail.VideoLength, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pContentType", OracleDbType.Varchar2, 30, mediaFileDetail.ContentType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIdentity", OracleDbType.Varchar2, identity?.Length ?? 0, identity, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAuthenticationKey", OracleDbType.Varchar2, 50, authenticationKey, ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();

                        if (!mediaFileDetailId.HasValue)
                            mediaFileDetail.Id = Convert.ToInt32(cmd.Parameters["pMediaFileDetailId"].Value.ToString());
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException(string.Empty, Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override List<MediaFileDetail> MediaFileDetailGet(int? mediaFileDetailId, int? mediaFileId, string identity, string authenticationKey)
        {
            var returnValue = new List<MediaFileDetail>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "MediaServer.MediaFileDetailGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pMediaFileDetailId", OracleDbType.Int32, mediaFileDetailId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pMediaFileId", OracleDbType.Int32, mediaFileId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIdentity", OracleDbType.Varchar2, identity?.Length ?? 0, identity, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAuthenticationKey", OracleDbType.Varchar2, 50, authenticationKey, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pList", OracleDbType.RefCursor, ParameterDirection.Output));

                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                returnValue.Add(new MediaFileDetail
                                {
                                    Id = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    ParentId = !r.IsDBNull(1) ? r.GetInt32(1) : -1,
                                    Name = !r.IsDBNull(2) ? r.GetString(2) : null,
                                    FileName = !r.IsDBNull(3) ? r.GetString(3) : null,
                                    TargetDeviceType = !r.IsDBNull(4) ? r.GetInt32(4) : -1,
                                    FileSizeBytes = !r.IsDBNull(5) ? r.GetInt32(5) : -1,
                                    Md5Hash = !r.IsDBNull(6) ? r.GetString(6) : null,
                                    Location = !r.IsDBNull(7) ? r.GetString(7) : null,
                                    VideoLength = !r.IsDBNull(8) ? r.GetInt32(8) : -1,
                                    ContentType = !r.IsDBNull(9) ? r.GetString(9) : null
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException(string.Empty, Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
            return returnValue;
        }

        /// <inheritdoc />
        public override List<NameValuePair> SupportedFileTypeGet(string identity, string authenticationKey)
        {
            var returnValue = new List<NameValuePair>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "MediaServer.SupportedFileTypeGet", CommandType = CommandType.StoredProcedure })
                {
                    cmd.Parameters.Add(new OracleParameter("pIdentity", OracleDbType.Varchar2, 15, identity, ParameterDirection.Input));
                    cmd.Parameters.Add(new OracleParameter("pAuthenticationKey", OracleDbType.Varchar2, 50, authenticationKey, ParameterDirection.Input));
                    cmd.Parameters.Add(new OracleParameter("pList", OracleDbType.RefCursor, ParameterDirection.Output));

                    con.Open();
                    using (var r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            returnValue.Add(new NameValuePair
                            {
                                Name = !r.IsDBNull(0) ? r.GetString(0) : null,
                                Value = !r.IsDBNull(1) ? r.GetString(1) : null
                            });
                        }
                    }
                }
            }
            return returnValue;
        }

        #endregion

        #region Playlist

        /// <inheritdoc />
        public override void PlaylistDelete(int playlistId, string identity, string authenticationKey)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "MediaServer.PlaylistDelete", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pPlaylistId", OracleDbType.Int32, playlistId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIdentity", OracleDbType.Varchar2, identity?.Length ?? 0, identity, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAuthenticationKey", OracleDbType.Varchar2, 50, authenticationKey, ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException(string.Empty, Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override List<Playlist> PlaylistGet(int? playlistId, string identity, string authenticationKey)
        {
            var returnValue = new List<Playlist>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "MediaServer.PlaylistGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pPlaylistId", OracleDbType.Int32, playlistId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIdentity", OracleDbType.Varchar2, identity?.Length ?? 0, identity, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAuthenticationKey", OracleDbType.Varchar2, 50, authenticationKey, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pList", OracleDbType.RefCursor, ParameterDirection.Output));

                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                returnValue.Add(new Playlist
                                {
                                    Id = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    Name = !r.IsDBNull(1) ? r.GetString(1) : null
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException(string.Empty, Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
            return returnValue;
        }

        /// <inheritdoc />
        public override void PlaylistSet(int? playlistId, Playlist playlist, string identity, string authenticationKey)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "MediaServer.PlaylistSet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pPlaylistId", OracleDbType.Int32, playlistId, ParameterDirection.InputOutput));
                        cmd.Parameters.Add(new OracleParameter("pName", OracleDbType.Varchar2, 100, playlist.Name, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIdentity", OracleDbType.Varchar2, identity?.Length ?? 0, identity, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAuthenticationKey", OracleDbType.Varchar2, 50, authenticationKey, ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();

                        if (!playlistId.HasValue)
                            playlist.Id = Convert.ToInt32(cmd.Parameters["pPlaylistId"].Value.ToString());
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException(string.Empty, Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override void PlaylistItemDelete(int playlistItemId, string identity, string authenticationKey)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "MediaServer.PlaylistItemDelete", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pPlaylistItemId", OracleDbType.Int32, playlistItemId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIdentity", OracleDbType.Varchar2, identity?.Length ?? 0, identity, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAuthenticationKey", OracleDbType.Varchar2, 50, authenticationKey, ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException(string.Empty, Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override List<PlaylistItem> PlaylistItemGet(int? playlistItemId, string identity, string authenticationKey)
        {
            var returnValue = new List<PlaylistItem>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "MediaServer.PlaylistItemGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pPlaylistItemId", OracleDbType.Int32, playlistItemId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIdentity", OracleDbType.Varchar2, identity?.Length ?? 0, identity, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAuthenticationKey", OracleDbType.Varchar2, 50, authenticationKey, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pList", OracleDbType.RefCursor, ParameterDirection.Output));

                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                returnValue.Add(new PlaylistItem
                                {
                                    Id = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    PlaylistId = !r.IsDBNull(1) ? r.GetInt32(1) : -1,
                                    MediaFileId = !r.IsDBNull(2) ? r.GetInt32(2) : -1,
                                    MediaName = !r.IsDBNull(3) ? r.GetString(3) : null,
                                    Volume = !r.IsDBNull(4) ? r.GetInt32(4) : -1,
                                    PlaySequenceId = !r.IsDBNull(5) ? r.GetInt32(5) : -1,
                                    StartDateTime = !r.IsDBNull(6) ? r.GetDateTime(6) : DateTime.MinValue,
                                    EndDateTime = !r.IsDBNull(7) ? r.GetDateTime(7) : DateTime.MinValue,
                                    DisplayDuration = !r.IsDBNull(8) ? r.GetInt32(8) : -1,
                                    FileSizeMb = !r.IsDBNull(9) ? r.GetDecimal(9) : -1
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException(string.Empty, Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
            return returnValue;
        }

        /// <inheritdoc />
        public override List<PlaylistItem> PlaylistItemsGet(int? playlistId, string identity, string authenticationKey)
        {
            var returnValue = new List<PlaylistItem>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "MediaServer.PlaylistItemsGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pPlaylistId", OracleDbType.Int32, playlistId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIdentity", OracleDbType.Varchar2, identity?.Length ?? 0, identity, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAuthenticationKey", OracleDbType.Varchar2, 50, authenticationKey, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pList", OracleDbType.RefCursor, ParameterDirection.Output));

                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                returnValue.Add(new PlaylistItem
                                {
                                    Id = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    PlaylistId = !r.IsDBNull(1) ? r.GetInt32(1) : -1,
                                    MediaFileId = !r.IsDBNull(2) ? r.GetInt32(2) : -1,
                                    MediaName = !r.IsDBNull(3) ? r.GetString(3) : null,
                                    Volume = !r.IsDBNull(4) ? r.GetInt32(4) : -1,
                                    PlaySequenceId = !r.IsDBNull(5) ? r.GetInt32(5) : -1,
                                    StartDateTime = !r.IsDBNull(6) ? r.GetDateTime(6) : DateTime.MinValue,
                                    EndDateTime = !r.IsDBNull(7) ? r.GetDateTime(7) : DateTime.MinValue,
                                    DisplayDuration = !r.IsDBNull(8) ? r.GetInt32(8) : -1,
                                    FileSizeMb = !r.IsDBNull(9) ? r.GetDecimal(9) : -1
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException(string.Empty, Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
            return returnValue;
        }

        /// <inheritdoc />
        public override void PlaylistItemSet(int? playlistItemId, PlaylistItem playlistItem, string identity, string authenticationKey)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "MediaServer.PlaylistItemSet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pPlaylistItemId", OracleDbType.Int32, playlistItemId, ParameterDirection.InputOutput));
                        cmd.Parameters.Add(new OracleParameter("pPlaylistId", OracleDbType.Int32, playlistItem.PlaylistId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pMediaFileId", OracleDbType.Int32, playlistItem.MediaFileId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pVolume", OracleDbType.Int32, playlistItem.Volume, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pPlaySequenceId", OracleDbType.Int32, playlistItem.PlaySequenceId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pStartDateTime", OracleDbType.Date, playlistItem.StartDateTime, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pEndDateTime", OracleDbType.Date, playlistItem.EndDateTime, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pDisplayDuration", OracleDbType.Int32, playlistItem.DisplayDuration, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIdentity", OracleDbType.Varchar2, identity?.Length ?? 0, identity, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAuthenticationKey", OracleDbType.Varchar2, 50, authenticationKey, ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();

                        if (!playlistItemId.HasValue)
                            playlistItem.Id = Convert.ToInt32(cmd.Parameters["pPlaylistItemId"].Value.ToString());
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException(string.Empty, Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override void PlaylistItemSequenceSet(int playlistId, string sequence, string identity, string authenticationKey)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "MediaServer.PlaylistItemSequenceSet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pPlaylistId", OracleDbType.Int32, playlistId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pPlaylistItemSequence", OracleDbType.Varchar2, sequence.Length, sequence, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIdentity", OracleDbType.Varchar2, identity?.Length ?? 0, identity, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAuthenticationKey", OracleDbType.Varchar2, 50, authenticationKey, ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException(string.Empty, Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        #endregion

        #region Rss Feed

        /// <inheritdoc />
        public override List<RssFeed> RssFeedGet(int? rssFeedId, string identity, string authenticationKey)
        {
            var returnValue = new List<RssFeed>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "MediaServer.RssFeedGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pRssFeedId", OracleDbType.Int32, rssFeedId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIdentity", OracleDbType.Varchar2, identity?.Length ?? 0, identity, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAuthenticationKey", OracleDbType.Varchar2, 50, authenticationKey, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pList", OracleDbType.RefCursor, ParameterDirection.Output));

                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                returnValue.Add(new RssFeed
                                {
                                    Id = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    Url = !r.IsDBNull(1) ? r.GetString(1) : null,
                                    CallBackFrequency = !r.IsDBNull(2) ? r.GetInt32(2) : -1,
                                    BackgroundColor = !r.IsDBNull(3) ? r.GetString(3) : null,
                                    ForegroundColor = !r.IsDBNull(4) ? r.GetString(4) : null,
                                    MaxItemCount = !r.IsDBNull(5) ? r.GetInt32(5) : -1,
                                    MaxLengthTitle = !r.IsDBNull(6) ? r.GetInt32(6) : -1,
                                    MaxLengthDescription = !r.IsDBNull(7) ? r.GetInt32(7) : -1,
                                    DisplayTitle = !r.IsDBNull(8) ? r.GetInt32(8) : -1,
                                    DisplayDescription = !r.IsDBNull(9) ? r.GetInt32(9) : -1,
                                    Priority = !r.IsDBNull(10) ? r.GetInt32(10) : -1,
                                    Alert = !r.IsDBNull(11) ? r.GetInt32(11) : -1
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException(string.Empty, Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
            return returnValue;
        }

        /// <inheritdoc />
        public override void RssFeedDelete(int rssFeedId, string identity, string authenticationKey)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "MediaServer.RssFeedDelete", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pRssFeedId", OracleDbType.Int32, rssFeedId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIdentity", OracleDbType.Varchar2, identity?.Length ?? 0, identity, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAuthenticationKey", OracleDbType.Varchar2, 50, authenticationKey, ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException(string.Empty, Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override void RssFeedSet(int? rssFeedId, RssFeed rssFeed, string identity, string authenticationKey)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "MediaServer.RssFeedSet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter("pRssFeedId", OracleDbType.Int32, rssFeedId, ParameterDirection.InputOutput));
                        cmd.Parameters.Add(new OracleParameter("pUrl", OracleDbType.Varchar2, 255, rssFeed.Url, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pCallBackFrequency", OracleDbType.Int32, rssFeed.CallBackFrequency, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pBackgroundColor", OracleDbType.Varchar2, 255, rssFeed.BackgroundColor, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pForegroundColor", OracleDbType.Varchar2, 255, rssFeed.ForegroundColor, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pMaxItemCount", OracleDbType.Int32, rssFeed.MaxItemCount, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pMaxLengthTitle", OracleDbType.Int32, rssFeed.MaxLengthTitle, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pMaxLengthDescription", OracleDbType.Int32, rssFeed.MaxLengthDescription, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pDisplayTitle", OracleDbType.Int32, rssFeed.DisplayTitle, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pDisplayDescription", OracleDbType.Int32, rssFeed.DisplayDescription, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pPriority", OracleDbType.Int32, rssFeed.Priority, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAlert", OracleDbType.Int32, rssFeed.Alert, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIdentity", OracleDbType.Varchar2, identity?.Length ?? 0, identity, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAuthenticationKey", OracleDbType.Varchar2, 50, authenticationKey, ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();

                        if (!rssFeedId.HasValue)
                            rssFeed.Id = Convert.ToInt32(cmd.Parameters["pRssFeedId"].Value.ToString());
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException(string.Empty, Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        #endregion

        #region User

        /// <inheritdoc />
        public override UserValidate UserGet(string identity, string authenticationKey)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "MediaServer.UserGet", CommandType = CommandType.StoredProcedure })
                {
                    cmd.Parameters.Add(new OracleParameter("pIdentity", OracleDbType.Varchar2, identity?.Length ?? 0, identity, ParameterDirection.Input));
                    cmd.Parameters.Add(new OracleParameter("pAuthenticationKey", OracleDbType.Varchar2, 50, authenticationKey, ParameterDirection.Input));
                    cmd.Parameters.Add(new OracleParameter("pList", OracleDbType.RefCursor, ParameterDirection.Output));

                    con.Open();
                    using (var r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            var user = new UserValidate
                            {
                                UserName = !r.IsDBNull(0) ? r.GetString(0) : null,
                                FullName = !r.IsDBNull(1) ? r.GetString(1) : null,
                                PasswordHash = !r.IsDBNull(2) ? (byte[])r.GetValue(2) : null,
                                PasswordHashSalt = !r.IsDBNull(3) ? r.GetString(3) : null,
                                IsActive = !r.IsDBNull(4) && Formatting.TfStringToBool(r.GetString(4)),
                                PasswordChangeRequired = !r.IsDBNull(5) && Formatting.TfStringToBool(r.GetString(5)),
                                // Is Admin
                                IsLocked = !r.IsDBNull(7) && Formatting.TfStringToBool(r.GetString(7)),
                                AccountStatus = new AccountStatus()
                            };

                            if (string.IsNullOrEmpty(user.FullName))
                                user.FullName = user.UserName;

                            return user;
                        }
                    }
                }
            }

            return null;
        }

        /// <inheritdoc />
        public override List<RolePermission> UserResourcePermissionGet(string roleResourceName, string identity, string authenticationKey)
        {
            var returnValue = new List<RolePermission>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "RoleSecurity.UserResourcePermissionGet", CommandType = CommandType.StoredProcedure })
                {
                    cmd.Parameters.Add(new OracleParameter("pRoleResourceNameFilter", OracleDbType.Varchar2, 100, roleResourceName, ParameterDirection.Input));
                    cmd.Parameters.Add(new OracleParameter("pIdentity", OracleDbType.Varchar2, identity?.Length ?? 0, identity, ParameterDirection.Input));
                    cmd.Parameters.Add(new OracleParameter("pAuthenticationKey", OracleDbType.Varchar2, 50, authenticationKey, ParameterDirection.Input));
                    cmd.Parameters.Add(new OracleParameter("pList", OracleDbType.RefCursor, ParameterDirection.Output));

                    con.Open();
                    using (var r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            returnValue.Add(new RolePermission
                            {
                                RoleResourceName = !r.IsDBNull(0) ? r.GetString(0) : null,
                                RolePermissionName = !r.IsDBNull(1) ? r.GetString(1).ToUpper() : null,
                                MerchantId = !r.IsDBNull(2) ? r.GetInt32(2) : -1
                            });
                        }
                    }
                }
            }
            return returnValue;
        }

        #endregion

    }
}