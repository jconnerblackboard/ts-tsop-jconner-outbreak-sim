﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net;
using BbTS.Core;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Definitions.Credential;
using BbTS.Domain.Models.Definitions.Device;
using BbTS.Domain.Models.Definitions.Licensing;
using BbTS.Domain.Models.Definitions.Report;
using BbTS.Domain.Models.Device;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.Exceptions.WebApi;
using BbTS.Domain.Models.Report;
using BbTS.Domain.Models.System;
using BbTS.Monitoring.Logging;
using BbTS.Resource.OracleManagedDataAccessExtensions;
using Oracle.ManagedDataAccess.Client;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        /// <summary>
        /// Get a list of IP Readers from the V_IP_READER view.
        /// </summary>
        /// <returns>List of IP Readers.</returns>
        public override List<IpReaderViewObject> IpReadersGet(List<HardwareModel> hardwareModelFilter = null)
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
            List<IpReaderViewObject> results = new List<IpReaderViewObject>();

            string whereClause = string.Empty;
            if (hardwareModelFilter != null)
            {
                whereClause = "WHERE HARDWARE_MODEL IN (";
                foreach (var model in hardwareModelFilter)
                {
                    whereClause += $"{(int)model},";
                }
                whereClause = $"{whereClause.TrimEnd(',')})";
            }
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand
                {
                    Connection = con,
                    CommandType = CommandType.Text,
                    CommandText = $"SELECT * FROM V_IP_READER {whereClause} ORDER BY Hardware_Model"
                })
                {
                    try
                    {
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var macAddress = !r.IsDBNull(0) ? r.GetString(0) : string.Empty;
                                var connectionId = !r.IsDBNull(1) ? r.GetInt32(1) : -1;
                                var name = !r.IsDBNull(2) ? r.GetString(2) : string.Empty;
                                var readerType = !r.IsDBNull(3) ? r.GetInt32(3) : -1;
                                var posIdMcId = !r.IsDBNull(4) ? r.GetInt32(4) : -1;
                                var readerId = !r.IsDBNull(5) ? r.GetInt32(5) : -1;
                                var hardwareType = !r.IsDBNull(6) ? r.GetInt32(6) : -1;
                                var hardwareModel = !r.IsDBNull(7) ? r.GetInt32(7) : -1;
                                var communicationType = !r.IsDBNull(8) ? r.GetInt32(8) : -1;
                                var isActive = !r.IsDBNull(9) ? r.GetString(9) : string.Empty;

                                results.Add(new IpReaderViewObject
                                {
                                    CommunicationType = communicationType,
                                    ConnectionId = connectionId,
                                    HardwareModel = (HardwareModel)hardwareModel,
                                    HardwareType = hardwareType,
                                    IsActive = Formatting.TfStringToBool(isActive),
                                    MacAddress = macAddress,
                                    Name = name,
                                    PosIdMasterControllerId = posIdMcId,
                                    ReaderId = readerId,
                                    ReaderType = readerType
                                });
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            return results;
        }

        /// <summary>
        /// Get all (or one) DSR Access Point from the database layer.
        /// </summary>
        /// <param name="id">The access point id or access point guid.</param>
        /// <returns>List of active DsrAccessPoints in the database layer.</returns>
        public override List<DsrAccessPoint> AccessPointsGet(string id)
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
            List<DsrAccessPoint> list = new List<DsrAccessPoint>();
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    var formattedId = string.IsNullOrEmpty(id) ? "" : $" AND {Formatting.ColumnNameFromIdType(id, "SerialNumber", "DsrAccessPointGuid")} = :pValue";
                    string query = $" SELECT * FROM Envision.DsrAccessPoint DAP WHERE DAP.IsActive = 'T' {formattedId}";
                    using (OracleCommand cmd = new OracleCommand
                    {
                        Connection = con,
                        CommandText = query,
                        CommandType = CommandType.Text
                    })
                    {
                        if (!string.IsNullOrEmpty(id))
                            cmd.Parameters.Add(
                                new OracleParameter
                                {
                                    ParameterName = "pValue",
                                    OracleDbType = OracleDbType.Varchar2,
                                    Size = id.Replace("-", "").Length,
                                    Value = id.Replace("-", "").ToUpper(), Direction = ParameterDirection.Input
                                });

                        con.Open();

                        using (OracleDataReader r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var dsrAccessPointId = !r.IsDBNull(0) ? r.GetInt32(0) : -1;
                                var serialNumber = !r.IsDBNull(1) ? r.GetString(1) : string.Empty;
                                var name = !r.IsDBNull(2) ? r.GetString(2) : string.Empty;
                                var description = !r.IsDBNull(3) ? r.GetString(3) : string.Empty;
                                var dsrServiceId = !r.IsDBNull(4) ? r.GetInt32(4) : -1;
                                var isConfirmed = !r.IsDBNull(5) ? r.GetString(5) : string.Empty;
                                var isOnline = !r.IsDBNull(6) ? r.GetString(6) : string.Empty;
                                var synchStatus = !r.IsDBNull(7) ? r.GetString(7) : "Unknown";
                                var lastSeen = !r.IsDBNull(8) ? r.GetDouble(8) : -1;
                                var lastCommunicationError = !r.IsDBNull(9) ? r.GetDouble(9) : -1;
                                var dsrHardwareSettingGroupId = !r.IsDBNull(10) ? r.GetInt32(10) : -1;
                                var buildingId = !r.IsDBNull(11) ? r.GetInt32(11) : -1;
                                var areaId = !r.IsDBNull(12) ? r.GetInt32(12) : -1;
                                var doorGroupId = !r.IsDBNull(13) ? r.GetInt32(13) : -1;
                                var cryptoAlgorithmTypeId = !r.IsDBNull(14) ? r.GetInt32(14) : -1;
                                var sharedSecretKeyHexString = !r.IsDBNull(15) ? r.GetString(15) : string.Empty;
                                var sharedSecretKeyLifespanSeconds = !r.IsDBNull(16) ? r.GetInt32(16) : -1;
                                var dsrAccessPointTypeId = !r.IsDBNull(17) ? r.GetInt32(17) : -1;
                                var merchantId = !r.IsDBNull(18) ? r.GetInt32(18) : -1;
                                var dsrAccessPointGuid = !r.IsDBNull(19) ? r.GetString(19) : string.Empty;
                                var accessModeId = !r.IsDBNull(20) ? r.GetInt32(20) : -9999;
                                var isActive = !r.IsDBNull(21) ? r.GetString(21) : string.Empty;

                                DsrAccessPoint dsrAccessPoint = new DsrAccessPoint
                                {
                                    DsrAccessPointId = dsrAccessPointId,
                                    SerialNumber = serialNumber,
                                    Name = name,
                                    Description = description,
                                    DsrServiceId = dsrServiceId,
                                    IsConfirmed = Formatting.TfStringToBool(isConfirmed),
                                    IsOnline = Formatting.TfStringToBool(isOnline),
                                    SynchStatus = synchStatus,
                                    LastSeen = lastSeen >= 0 ? new double?(lastSeen) : null,
                                    LastCommunicationError = lastCommunicationError >= 0 ? new double?(lastCommunicationError) : null,
                                    DsrHardwareSettingGroupId = dsrHardwareSettingGroupId >= 0 ? new int?(dsrHardwareSettingGroupId) : null,
                                    BuildingId = dsrHardwareSettingGroupId >= 0 ? new int?(buildingId) : null,
                                    AreaId = dsrHardwareSettingGroupId >= 0 ? new int?(areaId) : null,
                                    DoorGroupId = dsrHardwareSettingGroupId >= 0 ? new int?(doorGroupId) : null,
                                    CryptoAlgorithmTypeId = cryptoAlgorithmTypeId,
                                    SharedSecretKeyHexString = sharedSecretKeyHexString,
                                    SharedSecretKeyLifespanSeconds = sharedSecretKeyLifespanSeconds,
                                    DsrAccessPointTypeId = dsrAccessPointTypeId >= 0 ? new int?(dsrAccessPointTypeId) : null,
                                    MerchantId = merchantId >= 0 ? new int?(merchantId) : null,
                                    DsrAccessPointGuid = string.IsNullOrEmpty(dsrAccessPointGuid) ? new Guid() : new Guid(dsrAccessPointGuid),
                                    AccessModeId = accessModeId >= -2 ? new int?(accessModeId) : null,
                                    IsActive = Formatting.TfStringToBool(isActive)
                                };

                                list.Add(dsrAccessPoint);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <summary>
        /// Get all Allegion locks from the database layer.
        /// </summary>
        /// <returns>List of active Allegion locks in the database layer.</returns>
        public override List<AllegionLock> AllegionLocksGet(List<LicenseServerDeviceSubType> modelFilter = null)
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
            var locks = new List<AllegionLock>();

            string whereClause = string.Empty;
            if (modelFilter != null)
            {
                whereClause = "WHERE LOCK_MODEL IN (";
                foreach (var model in modelFilter)
                {
                    whereClause += $"{model},";
                }
                whereClause = $"{whereClause.TrimEnd(',')})";
            }
            var query = @"
                SELECT DISTINCT   VMC.MASTERCONTROLLEREQUIPMENT_ID,
                                  VMC.MasterController_Id,
                                  VMC.ADDRESS,
                                  VMC.UPDATEDATE,
                                  VMC.LATESTDATE,
                                  VMC.LOCK_MODEL,
                                  VMC.LOCK_SERIAL_NUMBER,
                                  VMC.COMM_MODEL,
                                  VMC.COMM_SERIAL_NUMBER,
                                  D.DOOR_IDENTIFIER
                FROM              V_MASTERCONTROLLER_EQUP_CURR VMC
                LEFT JOIN         DOOR D on D.MASTERCONTROLLER_ID = VMC.MASTERCONTROLLER_ID AND
                                  D.ADDRESS = VMC.Address";
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                try
                {

                    using (OracleCommand cmd = new OracleCommand
                    {
                        Connection = con,
                        CommandType = CommandType.Text,
                        CommandText = $"{query} {whereClause} ORDER BY LOCK_MODEL"
                    })
                    {
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var masterControllerEquipmentId = !r.IsDBNull(0) ? r.GetInt32(0) : -1;
                                var masterControllerId = !r.IsDBNull(1) ? r.GetInt32(1) : -1;
                                var address = !r.IsDBNull(2) ? r.GetInt32(2) : -1;
                                var updateDate = !r.IsDBNull(3) ? r.GetDateTime(3) : new DateTime();
                                var latestDate = !r.IsDBNull(4) ? r.GetDateTime(4) : new DateTime();
                                var lockModel = !r.IsDBNull(5) ? r.GetString(5) : string.Empty;
                                var lockSerialNumber = !r.IsDBNull(6) ? r.GetString(6) : string.Empty;
                                var commModel = !r.IsDBNull(7) ? r.GetString(7) : string.Empty;
                                var commSerialNumber = !r.IsDBNull(8) ? r.GetString(8) : string.Empty;
                                var doorIdentifier = !r.IsDBNull(9) ? r.GetString(9) : string.Empty;

                                locks.Add(new AllegionLock
                                {
                                    MasterControllerEquipmentId = masterControllerEquipmentId,
                                    MasterControllerId = masterControllerId,
                                    Address = address,
                                    UpdateDate = updateDate,
                                    LatestDate = latestDate,
                                    LockModel = lockModel,
                                    LockSerialNumber = lockSerialNumber,
                                    CommModel = commModel,
                                    CommSerialNumber = commSerialNumber,
                                    DeviceName = doorIdentifier
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return locks;
        }

        /// <summary>
        /// Verify that a device has been registered.
        /// </summary>
        /// <param name="deviceGuid">The unique (guid) identifier for the device.</param>
        /// <returns>Registration status.</returns>
        public override bool DeviceIsRegistered(string deviceGuid)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "DeviceFunctions.DeviceIsRegistered", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDeviceGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = deviceGuid });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pIsRegistered", OracleDbType = OracleDbType.Int32, Direction = ParameterDirection.Output });
                        con.Open();
                        cmd.ExecuteNonQuery();
                        var isRegistered = cmd.Parameters["pIsRegistered"].Value.ToString();
                        return isRegistered != "0";
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Get (all or single) device registration information.
        /// </summary>
        /// <param name="deviceGuid">Identifier of the device (for single) or null (for all).</param>
        /// <returns>List of device registration information.</returns>
        public override List<DeviceProperties> DevicePropertiesGet(string deviceGuid = null)
        {
            var list = new List<DeviceProperties>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "DeviceFunctions.DeviceGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDeviceGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = deviceGuid });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var description = !r.IsDBNull(0) ? r.GetString(0) : string.Empty;
                                var fethcedDeviceGuid = !r.IsDBNull(1) ? r.GetString(1) : string.Empty;
                                var macAddress = !r.IsDBNull(2) ? r.GetString(2) : string.Empty;
                                var serialNumber = !r.IsDBNull(3) ? r.GetString(3) : string.Empty;
                                var deviceTypeId = !r.IsDBNull(4) ? r.GetInt32(4) : -1;
                                var deviceId = !r.IsDBNull(5) ? r.GetInt32(5) : -1;
                                var licenseExpiration = !r.IsDBNull(6) ? new DateTime?(r.GetDateTime(6)) : null;
                                var isRegistered = !r.IsDBNull(7) ? r.GetInt32(7) : -1;

                                var device = new DeviceProperties
                                {
                                    Description = description,
                                    DeviceGuid = fethcedDeviceGuid,
                                    DeviceType = (DeviceRegistrationDeviceType)deviceTypeId,
                                    MacAddress = macAddress,
                                    SerialNumber = serialNumber,
                                    DeviceId = deviceId,
                                    LicenseExpirationDate = licenseExpiration,
                                    IsRegistered = isRegistered == 1
                                };
                                list.Add(device);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <summary>
        /// Gets device registration information
        /// </summary>
        /// <param name="deviceGuid">Identifier of the device (for single) or null (for all).</param>
        /// <returns>Device registration information</returns>
        public override DeviceInfo DeviceGet(string deviceGuid)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "DeviceFunctions.DeviceGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.AddInParam("pDeviceGuid", deviceGuid);
                        cmd.AddOutParam("pList", OracleDbType.RefCursor, 0);
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            if (r.Read())
                            {
                                return new DeviceInfo()
                                {
                                    Description = !r.IsDBNull(0) ? r.GetString(0) : string.Empty,
                                    DeviceGuid = !r.IsDBNull(1) ? r.GetString(1) : string.Empty,
                                    MacAddress = !r.IsDBNull(2) ? r.GetString(2) : string.Empty,
                                    SerialNumber = !r.IsDBNull(3) ? r.GetString(3) : string.Empty,
                                    DeviceType = (DeviceRegistrationDeviceType)(!r.IsDBNull(4) ? r.GetInt32(4) : -1),
                                    DeviceId = !r.IsDBNull(5) ? r.GetInt32(5) : -1,
                                    LicenseExpirationDate = !r.IsDBNull(6) ? new DateTime?(r.GetDateTime(6)) : null,
                                    IsRegistered = !r.IsDBNull(7) && r.GetInt32(7) == 1,
                                    PosGuid = !r.IsDBNull(8) ? Guid.Parse(r.GetString(8)) : Guid.Empty,
                                    ProfitCenterGuid = !r.IsDBNull(9) ? Guid.Parse(r.GetString(9)) : Guid.Empty,
                                    MerchantGuid = !r.IsDBNull(10) ? Guid.Parse(r.GetString(10)) : Guid.Empty,
                                    PosName = !r.IsDBNull(11) ? r.GetString(11) : string.Empty,
                                    ProfitCenterName = !r.IsDBNull(12) ? r.GetString(12) : string.Empty,
                                    MerchantName = !r.IsDBNull(13) ? r.GetString(13) : string.Empty,
                                    SettingsUpdateTime = !r.IsDBNull(14) ? r.GetTimeSpan(14) : new TimeSpan(2, 0, 0)
                                };
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return null;
        }

        /// <summary>
        /// Update a device registration.
        /// </summary>
        /// <param name="deviceGuid">The unique (guid) identifier for the device.</param>
        /// <param name="registration">The registration information to use for an update.</param>
        /// <returns>The updated device registration information.</returns>
        public override DeviceProperties DevicePropertiesUpdate(string deviceGuid, DeviceProperties registration)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    var query = @"
                        UPDATE  Originator 
                        SET     Description=:pNameValue, 
                                MacAddress=:pMacAddressValue, 
                                SerialNumber=:pSerialNumberValue, 
                                DeviceTypeId=:pDeviceTypeIdValue
                        WHERE   DeviceGuid=:pDeviceGuidValue";

                    using (var cmd = new OracleCommand
                    {
                        Connection = con,
                        CommandType = CommandType.Text,
                        CommandText = query
                    })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pNameValue", OracleDbType = OracleDbType.Varchar2, Size = 100, Value = registration.Description, Direction = ParameterDirection.Input });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pMacAddressValue", OracleDbType = OracleDbType.Varchar2, Size = 17, Value = registration.MacAddress, Direction = ParameterDirection.Input });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pSerialNumberValue", OracleDbType = OracleDbType.Varchar2, Size = 30, Value = registration.SerialNumber, Direction = ParameterDirection.Input });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDeviceTypeIdValue", OracleDbType = OracleDbType.Int32, Size = 10, Value = (int)registration.DeviceType, Direction = ParameterDirection.Input });

                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDeviceGuidValue", OracleDbType = OracleDbType.Varchar2, Size = 36, Value = deviceGuid, Direction = ParameterDirection.Input });

                        con.Open();
                        cmd.ExecuteNonQuery();

                        var list = DevicePropertiesGet(deviceGuid);
                        if (list.Count > 0)
                        {
                            return list.FirstOrDefault();
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return null;
        }

        /// <summary>
        /// Delete a device registration information.
        /// </summary>
        /// <param name="deviceGuid">Identifier of the device (for single) or null (for all).</param>
        public override void DevicePropertiesDelete(string deviceGuid)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "DeviceFunctions.DeviceDelete", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDeviceGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = deviceGuid });
                        
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Log a heartbeat request from the device.
        /// </summary>
        /// <param name="request">Heartbeat information from the device.</param>
        public override void DeviceHeartbeatLog(DeviceHeartbeatRequest request)
        {
            Guard.IsNotNull(request, "request");
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");

            using (OracleConnection con = new OracleConnection(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "DeviceFunctions.HeartbeatSet", CommandType = CommandType.StoredProcedure })
                {
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDeviceId", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = request.DeviceId });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDeviceType", OracleDbType = OracleDbType.Int32, Size = 3, Direction = ParameterDirection.Input, Value = request.DeviceType });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDateTimeUtc", OracleDbType = OracleDbType.Date, Direction = ParameterDirection.Input, Value = request.DateTimeUTC });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDateTimeOffset", OracleDbType = OracleDbType.Varchar2, Size = 6, Direction = ParameterDirection.Input, Value = request.DateTimeOffset });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pSoftwareVersion", OracleDbType = OracleDbType.Varchar2, Size = 20, Direction = ParameterDirection.Input, Value = request.SoftwareVersion });

                    try
                    {
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        var rex = new ResourceLayerException(request.RequestId, Formatting.FormatException(ex));
                        throw rex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Get the operational requirements for a specific device.
        /// </summary>
        /// <param name="deviceId">The unique (guid) identifier for the device.</param>
        /// <returns>The operational requirements for the specific device or null if not found.</returns>
        public override OperationalRequirements DeviceOperationalRequirementsGet(string deviceId)
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = "DeviceFunctions.OperationalRequirementsGet",
                    CommandType = CommandType.StoredProcedure
                })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDeviceGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = new Guid(deviceId).ToString("D").ToUpper() });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pVersion", OracleDbType = OracleDbType.Varchar2, Size = 30, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pOperatorRequirementsList", OracleDbType = OracleDbType.RefCursor, Size = 0, Direction = ParameterDirection.Output, Value = string.Empty });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerRequirementsList", OracleDbType = OracleDbType.RefCursor, Size = 0, Direction = ParameterDirection.Output, Value = string.Empty });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pOperationalParametersList", OracleDbType = OracleDbType.RefCursor, Size = 0, Direction = ParameterDirection.Output, Value = string.Empty });

                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            var requirements = new OperationalRequirements
                            {
                                TsopProductVersion = cmd.Parameters["pVersion"].Value.ToString()
                            };

                            while (r.Read())
                            {
                                if (!r.IsDBNull(0))
                                {
                                    var credential = (CredentialType)Enum.Parse(typeof (CredentialType), r.GetString(0));
                                    requirements.OperatorLoginRequirements.Add(credential);
                                }
                            }

                            // On to the next cursor
                            r.NextResult();

                            while (r.Read())
                            {
                                var credential = (CredentialType)Enum.Parse(typeof (CredentialType), r.GetString(0));
                                requirements.CustomerIdentificationRequirement.Add(credential);
                            }

                            // On to the next cursor
                            r.NextResult();

                            while (r.Read())
                            {
                                if(!r.IsDBNull(0)) requirements.ShowMealPeriodCountReport = Formatting.TfStringToBool(r.GetString(0));
                            }
                            return requirements;
                        }
                    }
                    catch (Exception ex)
                    {
                        var rex = new ResourceLayerException(deviceId, Formatting.FormatException(ex));
                        LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                        throw rex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Process and log a device login request.
        /// </summary>
        /// <param name="request">Device login information.</param>
        public override DeviceLoginResponse ProcessDeviceLogin(DeviceLoginRequest request)
        {
            return new DeviceLoginResponse(request.RequestId);
        }

        /// <summary>
        /// Process and log a device logout request.
        /// </summary>
        /// <param name="request">Device logout information.</param>
        public override DeviceLogoutResponse ProcessDeviceLogout(DeviceLogoutRequest request)
        {
            return new DeviceLogoutResponse(request.RequestId);
        }

        /// <summary>
        /// Register a (slate) device.
        /// </summary>
        /// <param name="request">Device registration information.</param>
        /// <returns></returns>
        public override DeviceRegistrationPostResponse DeviceSet(DeviceRegistrationPostRequest request)
        {
            Guard.IsNotNull(request, "request");
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");

            

            using (OracleConnection con = new OracleConnection(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "DeviceFunctions.DeviceSet", CommandType = CommandType.StoredProcedure })
                {
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDeviceGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.InputOutput, Value = null});
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDescription", OracleDbType = OracleDbType.Varchar2, Size = 100, Direction = ParameterDirection.Input, Value = request.Name });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pMacAddress", OracleDbType = OracleDbType.Varchar2, Size = 17, Direction = ParameterDirection.Input, Value = request.MacAddress });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pSerialNumber", OracleDbType = OracleDbType.Varchar2, Size = 30, Direction = ParameterDirection.Input, Value = request.SerialNumber });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDeviceTypeId", OracleDbType = OracleDbType.Int32, Direction = ParameterDirection.Input, Value = (int)request.DeviceType });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pLicenseExpirationDate", OracleDbType = OracleDbType.Date, Direction = ParameterDirection.Input, Value = null });

                    try
                    {
                        con.Open();
                        cmd.ExecuteNonQuery();

                        var deviceguid = cmd.Parameters["pDeviceGuid"].Value.ToString();
                        Guid guid;
                        if (!Guid.TryParse(deviceguid, out guid))
                        {
                            var message = $"Unable to parse a valid guid value from {deviceguid}";
                            var rex = new ResourceLayerException(request.RequestId, message);
                            LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                            throw rex;
                        }
                        var response = new DeviceRegistrationPostResponse(request.RequestId)
                        {
                            DeviceId = guid.ToString("D")
                        };
                        return response;
                    }
                    catch (Exception ex)
                    {
                        var rex = new ResourceLayerException(request.RequestId, Formatting.FormatException(ex));
                        throw rex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Register a device as a pos.
        /// </summary>
        /// <param name="deviceGuid">The unique identifier for the device.</param>
        /// <param name="posGuid">The unique identifier for the pos.</param>
        public override void DevicePosRegister(string deviceGuid, string posGuid)
        {
            using (OracleConnection con = new OracleConnection(ConnectionString))
            {
                // If the Guid is present, we are doing a "Set" operation - otherwise a "Delete" operation
                using (OracleCommand cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = !string.IsNullOrEmpty(posGuid) ? "DeviceFunctions.DevicePosRegistrationSet" : "DeviceFunctions.DevicePosRegistrationDelete",
                    CommandType = CommandType.StoredProcedure
                })
                {
                    var deviceId = new Guid(deviceGuid).ToString("D");
                    string posId = null;

                    if (!string.IsNullOrEmpty(posGuid))
                        posId = new Guid(posGuid).ToString("D");
                    
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDeviceGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = deviceId });

                    // If the guid was empty to begin with, this is a delete operation and this parm is not included.
                    if (!string.IsNullOrEmpty(posGuid))
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPosGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = posId });

                    try
                    {
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                    catch (OracleException oex)
                    {
                        if (oex.Errors.Count > 0 && oex.Errors[0].Number == 20101)
                        {
                            var message = $"An exception occured at the data layer.  {Formatting.FormatException(oex)}.";
                            throw new WebApiException(deviceGuid, message, HttpStatusCode.BadRequest);
                        }

                        var rex = new ResourceLayerException(deviceGuid, Formatting.FormatException(oex));
                        LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                        throw rex;
                    }
                    catch (Exception ex)
                    {
                        var rex = new ResourceLayerException(deviceGuid, Formatting.FormatException(ex));
                        throw rex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <inheritdoc />
        public override MealsCountReport DeviceMealsCountReportGet(Guid deviceGuid)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "DeviceFunctions.MealCountGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDeviceGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = deviceGuid.ToString("D") });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPosName", OracleDbType = OracleDbType.Varchar2, Size = 30, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCurrentPeriod", OracleDbType = OracleDbType.Int16, Size = 4, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPeriodName", OracleDbType = OracleDbType.Varchar2, Size = 15, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDayMealsServed", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPeriodMealsServed", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Output });
                        con.Open();

                        cmd.ExecuteNonQuery();
                        return new MealsCountReport()
                        {
                            BoardPeriod = (Int16)TryGetIntValue(cmd.Parameters["pCurrentPeriod"], 0),
                            BoardPeriodName = cmd.Parameters["pPeriodName"].Value?.ToString(),
                            Location = cmd.Parameters["pPosName"].Value?.ToString(),
                            MealsCountDay = TryGetIntValue(cmd.Parameters["pDayMealsServed"], 0),
                            MealsCountPeriod = TryGetIntValue(cmd.Parameters["pPeriodMealsServed"], 0)
                        };
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override List<DeviceReader> EnhancedReportingListGet()
        {
            var list = new List<DeviceReader>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "DeviceFunctions.EnhancedReportingListGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new DeviceReader
                                {
                                    ObjectId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    ObjectName = !r.IsDBNull(1) ? r.GetString(1) : null,
                                    ObjectType = !r.IsDBNull(2) ? (ObjectTypes)r.GetInt32(2) : ObjectTypes.Undefined,
                                    ObjectTypeName = !r.IsDBNull(3) ? r.GetString(3) : null,
                                    MerchantId = !r.IsDBNull(4) ? r.GetInt16(4) : (Int16)(-1),
                                    MerchantName = !r.IsDBNull(5) ? r.GetString(5) : null,
                                    ProfitCenterId = !r.IsDBNull(6) ? (int?)r.GetInt32(6) : null,
                                    ProfitCenterName = !r.IsDBNull(7) ? r.GetString(7) : null,
                                    BuildingId = !r.IsDBNull(8) ? (int?)r.GetInt32(8) : null,
                                    BuildingName = !r.IsDBNull(9) ? r.GetString(9) : null,
                                    AreaId = !r.IsDBNull(10) ? (int?)r.GetInt32(10) : null,
                                    AreaName = !r.IsDBNull(11) ? r.GetString(11) : null,
                                    SvPaymentId = !r.IsDBNull(12) ? (int?)r.GetInt32(12) : null,
                                    SvPaymentCode = !r.IsDBNull(13) ? r.GetString(13) : null,
                                    SvPaymentName = !r.IsDBNull(14) ? r.GetString(14) : null,
                                    SvPaymentObjDefFieldId = !r.IsDBNull(15) ? r.GetInt32(15) : -1,
                                    PrivEventId = !r.IsDBNull(16) ? (int?)r.GetInt32(16) : null,
                                    PrivEventCode = !r.IsDBNull(17) ? r.GetString(17) : null,
                                    PrivEventName = !r.IsDBNull(18) ? r.GetString(18) : null,
                                    PrivEventObjDefFieldId = !r.IsDBNull(19) ? r.GetInt32(19) : -1,
                                    DoorAccessId = !r.IsDBNull(20) ? (int?)r.GetInt32(20) : null,
                                    DoorAccessCode = !r.IsDBNull(21) ? r.GetString(21) : null,
                                    DoorAccessName = !r.IsDBNull(22) ? r.GetString(22) : null,
                                    DoorAccessObjDefFieldId = !r.IsDBNull(23) ? r.GetInt32(23) : -1
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <inheritdoc />
        public override EnhancedReportingDomainDataGetResponse EnhancedReportingDomainDataGet()
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "DeviceFunctions.EnhancedReportingDomainDataGet", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pSvPaymentsList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPrivEventList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDoorAccessList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });

                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            var report = new EnhancedReportingDomainDataGetResponse()
                            {
                                DoorAccesses = new List<DomainView>(),
                                PrivEvents = new List<DomainView>(),
                                SvPayments = new List<DomainView>()
                            };

                            while (r.Read())
                            {
                                report.SvPayments.Add(new DomainView()
                                {
                                    DomainType = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    Id = !r.IsDBNull(1) ? r.GetInt32(1) : -1,
                                    Domain = !r.IsDBNull(2) ? r.GetString(2) : null,
                                    Name = !r.IsDBNull(3) ? r.GetString(3) : null,
                                    Description = !r.IsDBNull(4) ? r.GetString(4) : null
                                });
                            }

                            // On to the next cursor
                            r.NextResult();

                            while (r.Read())
                            {
                                report.PrivEvents.Add(new DomainView()
                                {
                                    DomainType = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    Id = !r.IsDBNull(1) ? r.GetInt32(1) : -1,
                                    Domain = !r.IsDBNull(2) ? r.GetString(2) : null,
                                    Name = !r.IsDBNull(3) ? r.GetString(3) : null,
                                    Description = !r.IsDBNull(4) ? r.GetString(4) : null
                                });
                            }
                            // On to the next cursor
                            r.NextResult();

                            while (r.Read())
                            {
                                report.DoorAccesses.Add(new DomainView()
                                {
                                    DomainType = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    Id = !r.IsDBNull(1) ? r.GetInt32(1) : -1,
                                    Domain = !r.IsDBNull(2) ? r.GetString(2) : null,
                                    Name = !r.IsDBNull(3) ? r.GetString(3) : null,
                                    Description = !r.IsDBNull(4) ? r.GetString(4) : null
                                });
                            }
                            return report;
                        }
                    }
                    catch (Exception ex)
                    {
                        var rex = new ResourceLayerException(string.Empty, Formatting.FormatException(ex));
                        LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                        throw rex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }
    }
}
