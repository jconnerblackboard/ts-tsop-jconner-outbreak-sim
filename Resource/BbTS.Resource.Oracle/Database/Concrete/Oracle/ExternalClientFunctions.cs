﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Text;
using BbTS.Core;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Customer;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Definitions.System;
using BbTS.Domain.Models.Exceptions.EAccount;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.ExternalClient;
using BbTS.Domain.Models.System;
using BbTS.Monitoring.Logging;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        /// <summary>
        /// Get external client customer data
        /// </summary>
        /// <param name="customerClientGuid">Client customer guid</param>
        /// <returns>External client customer data</returns>
        public override ExternalClientCustomer ExternalClientCustomerGet(Guid customerClientGuid)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "ExternalClientFunctions.ExternalClientCustomerGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerClientGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = customerClientGuid.ToString("D") });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            if (r.Read())
                            {
                                return new ExternalClientCustomer()
                                {
                                    ExternalClientId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    CustomerId = !r.IsDBNull(1) ? r.GetInt32(1) : -1,
                                    CustomerClientGuid = customerClientGuid
                                };
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return null;
        }


        /// <inheritdoc />
        public override ProcessCustomerRegistrationPostResponse ProcessCustomerRegistration(ProcessCustomerRegistrationPostRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "ExternalClientFunctions.CustomerRegistration", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pClientId", OracleDbType.Int32, 4, request.ClientId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pCardNumber", OracleDbType.Varchar2, 22, request.Cardnum, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pEmailAddress", OracleDbType.Varchar2, 255, request.Emailaddress, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pCustomerNumber", OracleDbType.Varchar2, 22, request.Custnum, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pLastName", OracleDbType.Varchar2, 30, request.Lastname, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pFirstName", OracleDbType.Varchar2, 30, request.Firstname, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pMiddleName", OracleDbType.Varchar2, 30, request.Middlename, ParameterDirection.Input));

                        cmd.Parameters.Add(new OracleParameter("pCustomerGuid", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 36 });
                        cmd.Parameters.Add(new OracleParameter("pClientCustomerGuid", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 36 });
                        cmd.Parameters.Add(new OracleParameter("pErrorCode", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });

                        con.Open();
                        cmd.ExecuteNonQuery();

                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);

                        if (errorCode != 0)
                        {
                            var message = BbtsErrorDefinitions.ErrorMessageGet(errorCode);
                            throw new CustomerRegistrationException(
                                $"{message}",
                                errorCode);
                        }

                        Guid customerGuid, externalClientGuid;

                        if (!Guid.TryParse(cmd.Parameters["pCustomerGuid"].Value.ToString(), out customerGuid))
                        {
                            throw new CustomerRegistrationException(
                                @"Unable to parse customer guid value from the result set.",
                                errorCode);
                        }

                        if (!Guid.TryParse(cmd.Parameters["pClientCustomerGuid"].Value.ToString(), out externalClientGuid))
                        {
                            throw new CustomerRegistrationException(
                                @"Unable to parse client customer guid value from the result set.",
                                errorCode);
                        }

                        return new ProcessCustomerRegistrationPostResponse
                        {
                            RequestId = request.RequestId,
                            CustomerGuid = customerGuid,
                            ExternalClientGuid = externalClientGuid,
                            ErrorCode = errorCode
                        };
                    }
                    catch (CustomerRegistrationException)
                    {
                        throw;
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException(request.RequestId, Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }
    }
}