﻿using System;
using System.Collections.Generic;
using System.Data;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Credential;
using BbTS.Domain.Models.Definitions.Container;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.System.Database;
using BbTS.Domain.Models.Transaction;
using BbTS.Resource.OracleManagedDataAccessExtensions;
using Oracle.ManagedDataAccess.Client;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        /// <summary>
        /// Get the last transaction number for the specified originator.
        /// </summary>
        /// <param name="originatorId"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public override int? LastTransactionNumberGet(Guid originatorId, ConnectionInfo connection = null)
        {
            int? posId = PosIdFromOriginatorGuidGet(originatorId);
            if (posId == null) return null;

            using (var con = connection == null ? OracleConnectionGet(ConnectionString) : OracleConnectionGet(connection))
            {
                var cmd = new OracleCommand { Connection = con, CommandText = "GetTranNum", CommandType = CommandType.StoredProcedure };
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pPosId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = posId });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionNumber", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Output });

                int transactionNumber;
                try
                {
                    con.Open();
                    cmd.ExecuteNonQuery();

                    transactionNumber = TryGetIntValue(cmd.Parameters["pTransactionNumber"], 0);
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }

                if (transactionNumber <= 0) return null;

                return transactionNumber;
            }
        }

        /// <summary>
        /// Verifies that the error code is a database error code.  If it is, the error code offset is removed.  If not, then the error is set to <see cref="LoggingDefinitions.EventId.MiscellaneousError"/>.
        /// </summary>
        /// <param name="errorCode"></param>
        private static void ErrorCodeDenormalize(ref int errorCode)
        {
            if (errorCode == 0) return;

            //Check that this error code corresponds to a value in the DENIED_MESSAGE table
            bool errorCodeExists = false;
            foreach (var eventId in Enum.GetValues(typeof(LoggingDefinitions.EventId)))
            {
                if (errorCode != (int)eventId) continue;
                if ((int)eventId <= LoggingDefinitions.DatabaseErrorCodeOffset || (int)eventId > LoggingDefinitions.DatabaseErrorCodeMaxValue) continue;
                errorCodeExists = true;
                break;
            }

            errorCode = errorCodeExists ? errorCode - LoggingDefinitions.DatabaseErrorCodeOffset : (int)LoggingDefinitions.EventId.MiscellaneousError;
        }

        /// <summary>
        /// Insert a denied transaction into the database.
        /// </summary>
        /// <param name="errorCode"></param>
        /// <param name="deniedText"></param>
        /// <param name="requestGuid"></param>
        /// <param name="transactionClassification"></param>
        /// <param name="transactionGuid"></param>
        /// <param name="originatorGuid"></param>
        /// <param name="forcePost"></param>
        /// <param name="startTimeStamp"></param>
        /// <param name="transactionNumber"></param>
        /// <param name="customerCard"></param>
        /// <param name="swiped"></param>
        /// <param name="customerGuid"></param>
        /// <param name="operatorGuid"></param>
        /// <param name="tenderId"></param>
        /// <param name="deniedAmount"></param>
        /// <param name="attendedEventId"></param>
        /// <param name="attendedEventTransactionType"></param>
        /// <param name="boardMealTypeId"></param>
        /// <param name="connection"></param>
        public override void TransactionDenialSet(
            //Required
            int errorCode,
            string deniedText,
            Guid requestGuid,
            TransactionClassification transactionClassification,
            Guid transactionGuid,
            Guid originatorGuid,
            bool forcePost,
            DateTimeOffset startTimeStamp,
            //Optional
            int? transactionNumber,
            CardCapture customerCard,
            bool? swiped,
            Guid? customerGuid,
            Guid? operatorGuid,
            int? tenderId,
            decimal? deniedAmount,
            int? attendedEventId,
            AttendedEventTranType? attendedEventTransactionType,
            int? boardMealTypeId,
            ConnectionInfo connection = null)
        {
            ErrorCodeDenormalize(ref errorCode);

            using (var con = connection == null ? OracleConnectionGet(ConnectionString) : OracleConnectionGet(connection))
            {
                char moduleChar;
                switch (transactionClassification)
                {
                    case TransactionClassification.Unknown:
                        moduleChar = 'U';
                        break;
                    case TransactionClassification.Arts:
                        moduleChar = 'R';
                        break;
                    case TransactionClassification.Attendance:
                        moduleChar = 'A';
                        break;
                    case TransactionClassification.Board:
                        moduleChar = 'D';
                        break;
                    case TransactionClassification.Control:
                        moduleChar = 'C';
                        break;
                    default:
                        moduleChar = 'U';
                        break;
                }

                string paddedCardNumber = null;
                if (customerCard != null)
                {
                    paddedCardNumber = Formatting.PadCardNumber(customerCard.CardNumber, 22, '0');
                }

                int? attendedEventTranType = null;
                if (attendedEventTransactionType != null)
                {
                    attendedEventTranType = (int)attendedEventTransactionType;
                }

                int? deniedMillidollarAmount = null;
                if (deniedAmount != null)
                {
                    deniedMillidollarAmount = (int)(deniedAmount.Value * 1000);
                }

                var cmd = new OracleCommand { Connection = con, CommandText = "TransactionFunctions.TransactionDenialSet", CommandType = CommandType.StoredProcedure };
                //Input
                cmd.AddInParam("pRequestGuid", requestGuid.ToString("D").ToUpperInvariant());
                cmd.AddInParam("pModuleChar", moduleChar);
                cmd.AddInParam("pOriginatorGuid", originatorGuid.ToString("D").ToUpperInvariant());
                cmd.AddInParam("pTransactionGuid", transactionGuid.ToString("D").ToUpperInvariant());
                cmd.AddInParam("pCustomerGuid", customerGuid?.ToString("D").ToUpperInvariant());
                cmd.AddInParam("pOperatorGuid", operatorGuid?.ToString("D").ToUpperInvariant());
                cmd.AddInParam("pTransactionNumber", transactionNumber);
                cmd.AddInParam("pEventId", attendedEventId);
                cmd.AddInParam("pEventTranType", attendedEventTranType);
                cmd.AddInParam("pTenderId", tenderId);
                cmd.AddInParam("pBoardMT_Id", boardMealTypeId);
                cmd.AddInParam("pCustomerCard", paddedCardNumber);
                cmd.AddInParam("pIssueNumber", customerCard?.IssueNumber);
                cmd.AddInParam("pIssueNumberCaptured", customerCard != null ? Formatting.BooleanToTf(customerCard.IssueNumberCaptured) : null);
                cmd.AddInParam("pSwiped", swiped.HasValue ? Formatting.BooleanToTf(swiped.Value) : null);
                cmd.AddInParam("pStartTimeStamp", startTimeStamp.DateTime.ToOADate());
                cmd.AddInParam("pDeniedAmount", deniedMillidollarAmount);
                cmd.AddInParam("pForcePost", Formatting.BooleanToTf(forcePost));
                cmd.AddInParam("pErrorCode", errorCode);
                cmd.AddInParam("pDeniedText", deniedText, 75);

                try
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override List<CustomerTransactionLocationLog> CustomerTransactionLocationLogsGet(DateTime startDate, DateTime endDate)
        {
            var list = new List<CustomerTransactionLocationLog>();

            using (var con = OracleConnectionGet(ConnectionString))
            {
                var cmdText = $@"
                    SELECT         DISTINCT
                                   BTS.InstitutionId,
                                   CUS.CustomerGuid,
                                   CUS.Custnum,
                                   MER.Name As MerchantName,
                                   PFC.Name As ProfitCenterName,
                                   POS.Name As PosName,
                                   POS.PosGuid as PosId,
                                   TRN.Begin_DateTime AS DateTime
                    FROM           Transaction TRN
                    INNER JOIN     Retail_Tran_LineItem_SV_Cust RTL ON RTL.Transaction_Id = TRN.Transaction_Id
                    INNER JOIN     Customer CUS ON CUS.Cust_Id = RTL.Cust_Id
                    INNER JOIN     POS ON POS.Pos_Id = TRN.Pos_Id
                    INNER JOIN     ProfitCenter PFC ON PFC.ProfitCenter_Id = POS.ProfitCenter_Id
                    INNER JOIN     Merchant MER ON MER.Merchant_Id = PFC.Merchant_Id
                    INNER JOIN     BbSPTransactionSystem BTS ON BTS.IsValid = 'T'
                    WHERE          TRN.Begin_DateTime > {startDate.ToOADate()}
                    AND            TRN.Begin_DateTime < {endDate.ToOADate()}
                    ORDER BY       DateTime";
                using (var cmd = new OracleCommand { Connection = con, CommandText = cmdText, CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new CustomerTransactionLocationLog();
                                if (!r.IsDBNull(0)) item.InstitutionId = r.GetString(0);
                                if (!r.IsDBNull(1)) item.CustomerGuid = r.GetString(1);
                                if (!r.IsDBNull(2)) item.CustNum = r.GetString(2);
                                if (!r.IsDBNull(3)) item.MerchantName = r.GetString(3);
                                if (!r.IsDBNull(4)) item.ProfitCenterName = r.GetString(4);
                                if (!r.IsDBNull(5)) item.PosName = r.GetString(5);
                                if (!r.IsDBNull(6)) item.PosId = r.GetString(6);
                                if (!r.IsDBNull(7)) item.EntryDateTime = new DateTimeOffset(DateTime.FromOADate(r.GetDouble(7)), TimeZoneInfo.Local.BaseUtcOffset);
                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }
    }
}
