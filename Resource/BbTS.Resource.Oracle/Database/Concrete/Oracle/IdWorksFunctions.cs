﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.IdWorks;
using BbTS.Domain.Models.System.Database;
using BbTS.Monitoring.Logging;
using Oracle.ManagedDataAccess.Client;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        public override IEnumerable<Idw_Card> IdwCardsGet(ConnectionInfo connection, string source)
        {
            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = source, CommandType = CommandType.TableDirect })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                Idw_Card item = new Idw_Card();

                                if (!r.IsDBNull(0)) item.Record_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Person_Number = r.GetString(1);
                                if (!r.IsDBNull(2)) item.Card_Number = r.GetString(2);
                                if (!r.IsDBNull(3)) item.Card_Status = r.GetString(3);
                                if (!r.IsDBNull(4)) item.Card_Format = r.GetString(4);
                                if (!r.IsDBNull(5)) item.Description = r.GetString(5);
                                if (!r.IsDBNull(6)) item.Creation_Date = r.GetDateTime(6);
                                if (!r.IsDBNull(7)) item.Start_Date = r.GetDateTime(7);
                                if (!r.IsDBNull(8)) item.End_Date = r.GetDateTime(8);
                                if (!r.IsDBNull(9)) item.Print_Date = r.GetDateTime(9);
                                if (!r.IsDBNull(10)) item.Cancelled_Date = r.GetDateTime(10);
                                if (!r.IsDBNull(11)) item.Prints_Count = r.GetInt32(11);
                                if (!r.IsDBNull(12)) item.First_Name = r.GetString(12);
                                if (!r.IsDBNull(13)) item.Middle_Name = r.GetString(13);
                                if (!r.IsDBNull(14)) item.Last_Name = r.GetString(14);
                                if (!r.IsDBNull(15)) item.Library_Number = r.GetString(15);
                                if (!r.IsDBNull(16)) item.Card_Date = r.GetString(16);
                                if (!r.IsDBNull(17)) item.Title = r.GetString(17);
                                if (!r.IsDBNull(18)) item.Department = r.GetString(18);
                                if (!r.IsDBNull(19)) item.Address = r.GetString(19);
                                if (!r.IsDBNull(20)) item.City = r.GetString(20);
                                if (!r.IsDBNull(21)) item.State = r.GetString(21);
                                if (!r.IsDBNull(22)) item.Province = r.GetString(22);
                                if (!r.IsDBNull(23)) item.Zip_Code = r.GetString(23);
                                if (!r.IsDBNull(24)) item.Country = r.GetString(24);
                                if (!r.IsDBNull(25)) item.Company = r.GetString(25);
                                if (!r.IsDBNull(26)) item.Office = r.GetString(26);
                                if (!r.IsDBNull(27)) item.Email_Address = r.GetString(27);
                                if (!r.IsDBNull(28)) item.Postal_Code = r.GetString(28);
                                if (!r.IsDBNull(29)) item.Social_Security = r.GetString(29);
                                if (!r.IsDBNull(30)) item.Date_Of_Birth = r.GetDateTime(30);
                                if (!r.IsDBNull(31)) item.Photo_Blob = (byte[])r.GetValue(31);
                                if (!r.IsDBNull(32)) item.Photo_File = r.GetString(32);
                                if (!r.IsDBNull(33)) item.Sign_Blob = (byte[])r.GetValue(33);
                                if (!r.IsDBNull(34)) item.Sign_File = r.GetString(34);
                                if (!r.IsDBNull(35)) item.CustomField1 = r.GetString(35);
                                if (!r.IsDBNull(36)) item.CustomField2 = r.GetString(36);
                                if (!r.IsDBNull(37)) item.CustomField3 = r.GetString(37);
                                if (!r.IsDBNull(38)) item.CustomField4 = r.GetString(38);
                                if (!r.IsDBNull(39)) item.CustomField5 = r.GetString(39);
                                if (!r.IsDBNull(40)) item.CustomField6 = r.GetString(40);
                                if (!r.IsDBNull(41)) item.CustomField7 = r.GetString(41);
                                if (!r.IsDBNull(42)) item.CustomField8 = r.GetString(42);
                                if (!r.IsDBNull(43)) item.CustomField9 = r.GetString(43);
                                if (!r.IsDBNull(44)) item.CustomField10 = r.GetString(44);
                                if (!r.IsDBNull(45)) item.CustomField11 = r.GetString(45);
                                if (!r.IsDBNull(46)) item.CustomField12 = r.GetString(46);
                                if (!r.IsDBNull(47)) item.CustomField13 = r.GetString(47);
                                if (!r.IsDBNull(48)) item.CustomField14 = r.GetString(48);
                                if (!r.IsDBNull(49)) item.CustomField15 = r.GetString(49);
                                if (!r.IsDBNull(50)) item.CustomField16 = r.GetString(50);
                                if (!r.IsDBNull(51)) item.CustomField17 = r.GetString(51);
                                if (!r.IsDBNull(52)) item.CustomField18 = r.GetString(52);
                                if (!r.IsDBNull(53)) item.CustomField19 = r.GetString(53);
                                if (!r.IsDBNull(54)) item.CustomField20 = r.GetString(54);
                                if (!r.IsDBNull(55)) item.Card_Idm = r.GetString(55);

                                yield return item;
                            }
                        }
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Set the IDWorks CSN Caputure information.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The result of the operation.</returns>
        public override IdWorksCsnCaptureResponse IdWorksCsnCaptureSet(IdWorksCsnCaptureRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "CardFunctions.CardSerialNumberSet", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pTrack2Data", OracleDbType.Varchar2, 36, request.Track2Data, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pSerialNumber", OracleDbType.Varchar2, 16, request.CardSerialNumber, ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();

                        return new IdWorksCsnCaptureResponse
                        {
                            RequestId = request.RequestId,
                            ResponseCode = "0",
                            Message = "Success."
                        };

                    }
                    catch (Exception ex)
                    {
                        var rex = new ResourceLayerException(new Guid().ToString("D"), Formatting.FormatException(ex));
                        throw rex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }
    }
}
