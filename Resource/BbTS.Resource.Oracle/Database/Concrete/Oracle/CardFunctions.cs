﻿using System;
using System.Collections.Generic;
using System.Data;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Credential;
using BbTS.Domain.Models.Customer;
using BbTS.Domain.Models.System.Database;
using Oracle.ManagedDataAccess.Client;
using BbTS.Core;
using BbTS.Domain.Models.Exceptions.Resource;
using System.Diagnostics;
using BbTS.Monitoring.Logging;
using BbTS.Domain.Models.Card;
using BbTS.Domain.Models.Credential.Mobile;
using BbTS.Domain.Models.Customer.Management;
using BbTS.Domain.Models.Definitions.Card;
using BbTS.Domain.Models.Definitions.Notification;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        /// <summary>
        /// Get a list of all cards.
        /// </summary>
        /// <param name="connection">Connection information for oracle.</param>
        /// <returns>List of all cards.</returns>
        public override List<TsCard> CardGet(ConnectionInfo connection)
        {
            var list = new List<TsCard>();

            using (var con = OracleConnectionGet(connection))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "Card", CommandType = CommandType.TableDirect })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsCard();

                                if (!r.IsDBNull(0)) item.CardNum = r.GetString(0);
                                if (!r.IsDBNull(1)) item.Issue_Number = r.GetString(1);
                                if (!r.IsDBNull(2)) item.Cust_Id = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.Card_Type = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.Card_Status = r.GetInt32(4);
                                if (!r.IsDBNull(5)) item.Card_Status_Text = r.GetString(5);
                                if (!r.IsDBNull(6)) item.Card_Status_Datetime = LocalDateTimeToUtc(DateTime.FromOADate(r.GetDouble(6)));
                                if (!r.IsDBNull(7)) item.Lost_Flag = r.GetString(7);
                                if (!r.IsDBNull(8)) item.Card_Idm = r.GetString(8);
                                if (!r.IsDBNull(9)) item.DomainId = r.GetString(9);
                                if (!r.IsDBNull(10)) item.IssuerId = r.GetInt32(10);
                                if (!r.IsDBNull(11)) item.CreatedDateTime = LocalDateTimeToUtc(r.GetDateTime(11));
                                if (!r.IsDBNull(12)) item.ModifiedDateTime = LocalDateTimeToUtc(r.GetDateTime(12));
                                if (!r.IsDBNull(13)) item.CardNumber = r.GetDecimal(13);
                                if (!r.IsDBNull(14)) item.CardNumberLength = r.GetInt32(14);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Get a list of card formats for a device.
        /// </summary>
        /// <param name="deviceId">The unique identifier for the device.</param>
        /// <returns>List of <see cref="CardFormatForDeviceSettings"/> for the requested device.</returns>
        public override List<CardFormatForDeviceSettings> CardFormatDeviceSettingsGet(string deviceId)
        {
            var list = new List<CardFormatForDeviceSettings>();

            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = @"
                        SELECT      CARDFORMATGUID,
                                    NAME,
                                    PRIORITY_SEQUENCE,
                                    SITE_CODE,
                                    SITE_CODE_START_POSITION,
                                    SITE_CODE_START_TYPE,
                                    SITE_CODE_LENGTH,
                                    SITE_CODE_CHECK_ONLINE,
                                    SITE_CODE_CHECK_OFFLINE,
                                    ID_NUMBER_START_POSITION,
                                    ID_NUMBER_START_TYPE,
                                    ID_NUMBER_LENGTH,
                                    ISSUE_NUMBER_START,
                                    ISSUE_NUMBER_START_TYPE,
                                    ISSUE_NUMBER_LENGTH,
                                    MINIMUM_DIGITS_TRACK_2,
                                    MAXIMUM_DIGITS_TRACK_2
                        FROM        Envision.Card_Format
                        WHERE       Is_Active = 'T'
                        ORDER BY    Priority_Sequence",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new CardFormatForDeviceSettings();

                                if (!r.IsDBNull(0)) item.CardFormatGuid = r.GetString(0);
                                if (!r.IsDBNull(1)) item.Name = r.GetString(1);
                                if (!r.IsDBNull(2)) item.PrioritySequence = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.SiteCode = r.GetString(3);
                                if (!r.IsDBNull(4)) item.SiteCodeStartPosition = r.GetInt32(4);
                                if (!r.IsDBNull(5)) item.SiteCodeStartType = (CardFormatStartType)r.GetInt32(5);
                                if (!r.IsDBNull(6)) item.SiteCodeLength = r.GetInt32(6);
                                if (!r.IsDBNull(7)) item.SiteCodeCheckOnline = Formatting.TfStringToBool(r.GetString(7));
                                if (!r.IsDBNull(8)) item.SiteCodeCheckOffline = Formatting.TfStringToBool(r.GetString(8));
                                if (!r.IsDBNull(9)) item.IdNumberStartPosition = r.GetInt32(9);
                                if (!r.IsDBNull(10)) item.IdNumberStartType = (CardFormatStartType)r.GetInt32(10);
                                if (!r.IsDBNull(11)) item.IdNumberLength = r.GetInt32(11);
                                if (!r.IsDBNull(12)) item.IssueNumberStart = r.GetInt32(12);
                                if (!r.IsDBNull(13)) item.IssueNumberStartType = (CardFormatStartType)r.GetInt32(13);
                                if (!r.IsDBNull(14)) item.IssueNumberLength = r.GetInt32(14);
                                if (!r.IsDBNull(15)) item.MinimumDigitsTrack2 = r.GetInt32(15);
                                if (!r.IsDBNull(16)) item.MaximumDigitsTrack2 = r.GetInt32(16);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Get a list of card formats for a device.
        /// </summary>
        /// <returns>List of <see cref="CardFormatForDeviceSettings"/> for the requested device.</returns>
        public override List<TsCardFormat> CardFormatGet()
        {
            var list = new List<TsCardFormat>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = @"
                        SELECT      CARDFORMATGUID,
                                    NAME,
                                    PRIORITY_SEQUENCE,
                                    SITE_CODE,
                                    SITE_CODE_START_POSITION,
                                    SITE_CODE_START_TYPE,
                                    SITE_CODE_LENGTH,
                                    SITE_CODE_CHECK_ONLINE,
                                    SITE_CODE_CHECK_OFFLINE,
                                    ID_NUMBER_START_POSITION,
                                    ID_NUMBER_START_TYPE,
                                    ID_NUMBER_LENGTH,
                                    ISSUE_NUMBER_START,
                                    ISSUE_NUMBER_START_TYPE,
                                    ISSUE_NUMBER_LENGTH,
                                    MINIMUM_DIGITS_TRACK_2,
                                    MAXIMUM_DIGITS_TRACK_2
                        FROM        Envision.Card_Format
                        WHERE       Is_Active = 'T'
                        ORDER BY    Priority_Sequence",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsCardFormat()
                                {
                                    CardFormatGuid = !r.IsDBNull(0) ? r.GetString(0) : string.Empty,
                                    Name = !r.IsDBNull(1) ? r.GetString(1) : string.Empty,
                                    PrioritySequence = !r.IsDBNull(2) ? r.GetInt32(2) : -1,
                                    SiteCode = !r.IsDBNull(3) ? r.GetString(3) : string.Empty,
                                    SiteCodeStartPosition = !r.IsDBNull(4) ? r.GetInt32(4) : -1,
                                    SiteCodeStartType = !r.IsDBNull(5) ? r.GetInt32(5) : -1,
                                    SiteCodeLength = !r.IsDBNull(6) ? r.GetInt32(6) : -1,
                                    SiteCodeCheckOnline = !r.IsDBNull(7) && Formatting.TfStringToBool(r.GetString(7)),
                                    SiteCodeCheckOffline = !r.IsDBNull(8) && Formatting.TfStringToBool(r.GetString(8)),
                                    IdNumberStartPosition = !r.IsDBNull(9) ? r.GetInt32(9) : -1,
                                    IdNumberStartType = !r.IsDBNull(10) ? r.GetInt32(10) : -1,
                                    IdNumberLength = !r.IsDBNull(11) ? r.GetInt32(11) : -1,
                                    IssueNumberStart = !r.IsDBNull(12) ? r.GetInt32(12) : -1,
                                    IssueNumberStartType = !r.IsDBNull(13) ? r.GetInt32(13) : -1,
                                    IssueNumberLength = !r.IsDBNull(14) ? r.GetInt32(14) : -1,
                                    MinimumDigitsTrack2 = !r.IsDBNull(15) ? r.GetInt32(15) : -1,
                                    MaximumDigitsTrack2 = !r.IsDBNull(16) ? r.GetInt32(16) : -1
                                });
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of card to customer guid mapping objects.  If connection is null, <see cref="ConnectionString"/> will be used
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<CardToCustomerGuidMapping> CardToCustomerGuidGet(ConnectionInfo connection = null)
        {
            var list = new List<CardToCustomerGuidMapping>();

            using (var con = connection == null ? OracleConnectionGet(ConnectionString) : OracleConnectionGet(connection))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = @"SELECT      card.Cardnum,
                                                card.Issue_Number,
                                                customer.CustomerGuid,
                                                card.Cust_Id
                                  FROM          Envision.Card card
                                  INNER JOIN    Envision.Customer customer ON card.Cust_Id = customer.Cust_Id",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new CardToCustomerGuidMapping();

                                if (!r.IsDBNull(0)) item.CardNumber = r.GetString(0);
                                if (!r.IsDBNull(1)) item.IssueCode = r.GetString(1);
                                if (!r.IsDBNull(2)) item.CustomerGuid = r.GetString(2);
                                if (!r.IsDBNull(3)) item.CustomerId = r.GetInt32(3);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Get a card record by card number.
        /// </summary>
        /// <param name="cardNumber">Card number, left padded to 22 characters</param>
        /// <returns><see cref="TsCard"/> instance</returns>
        public override TsCard CardGetByCardNumber(string cardNumber)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");

                    var query = @"SELECT
                                    CardNum,
                                    Issue_Number,
                                    Cust_Id,
                                    Card_Type,
                                    Card_Status,
                                    Card_Status_Text,
                                    Card_Status_DateTime,
                                    Lost_Flag,
                                    Card_Idm,
                                    DomainId,
                                    IssuerId,
                                    CreatedDateTime,
                                    ModifiedDateTime,
                                    CardNumber,
                                    CardNumberLength,
                                    ActiveStartDate,
                                    ActiveEndDate,
                                    CardName
                                FROM
                                    Card
                                WHERE
                                    CardNum = :pCardNum";

                    using (var cmd = new OracleCommand { Connection = con, CommandType = CommandType.Text, CommandText = query })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCardNum", OracleDbType = OracleDbType.Varchar2, Size = 22, Direction = ParameterDirection.Input, Value = cardNumber });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            if (r.Read())
                            {
                                return new TsCard
                                {
                                    CardNum = !r.IsDBNull(0) ? r.GetString(0) : string.Empty,
                                    Issue_Number = !r.IsDBNull(1) ? r.GetString(1) : string.Empty,
                                    Cust_Id = !r.IsDBNull(2) ? r.GetInt32(2) : -1,
                                    Card_Type = !r.IsDBNull(3) ? r.GetInt32(3) : -1,
                                    Card_Status = !r.IsDBNull(4) ? r.GetInt32(4) : -1,
                                    Card_Status_Text = !r.IsDBNull(5) ? r.GetString(5) : string.Empty,
                                    Card_Status_Datetime = !r.IsDBNull(6) ? DateTime.FromOADate(r.GetFloat(6)) : DateTime.MinValue,
                                    Lost_Flag = !r.IsDBNull(7) ? r.GetString(7) : string.Empty,
                                    Card_Idm = !r.IsDBNull(8) ? r.GetString(8) : string.Empty,
                                    DomainId = !r.IsDBNull(9) ? r.GetString(9) : string.Empty,
                                    IssuerId = !r.IsDBNull(10) ? r.GetInt32(10) : -1,
                                    CreatedDateTime = !r.IsDBNull(11) ? r.GetDateTime(11) : DateTime.MinValue,
                                    ModifiedDateTime = !r.IsDBNull(12) ? r.GetDateTime(12) : DateTime.MinValue,
                                    CardNumber = !r.IsDBNull(13) ? r.GetDecimal(13) : -1,
                                    CardNumberLength = !r.IsDBNull(14) ? r.GetInt32(14) : -1,
                                    ActiveStartDate = !r.IsDBNull(15) ? r.GetDateTime(15) : DateTimeOffset.MinValue,
                                    ActiveEndDate = !r.IsDBNull(16) ? r.GetDateTime(16) : DateTimeOffset.MinValue,
                                    CardName = !r.IsDBNull(17) ? r.GetString(17) : string.Empty,
                                };
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return null;
        }

        /// <summary>
        /// Get a card by its guid
        /// </summary>
        /// <param name="cardGuid">Card's guid</param>
        /// <returns>Card</returns>
        public override TsCard CardGetByGuid(string cardGuid)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "CardFunctions.CardGetByGuid", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCardGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = cardGuid });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            if (r.Read())
                            {
                                return new TsCard()
                                {
                                    CardNum = !r.IsDBNull(0) ? r.GetString(0) : string.Empty,
                                    Issue_Number = !r.IsDBNull(1) ? r.GetString(1) : string.Empty,
                                    Cust_Id = !r.IsDBNull(2) ? r.GetInt32(2) : -1,
                                    Card_Type = !r.IsDBNull(3) ? r.GetInt32(3) : -1,
                                    Card_Status = !r.IsDBNull(4) ? r.GetInt32(4) : -1,
                                    Card_Status_Text = !r.IsDBNull(5) ? r.GetString(5) : string.Empty,
                                    Card_Status_Datetime = !r.IsDBNull(6) ? DateTime.FromOADate(r.GetFloat(6)) : DateTime.MinValue,
                                    Lost_Flag = !r.IsDBNull(7) ? r.GetString(7) : string.Empty,
                                    Card_Idm = !r.IsDBNull(8) ? r.GetString(8) : string.Empty,
                                    DomainId = !r.IsDBNull(9) ? r.GetString(9) : string.Empty,
                                    IssuerId = !r.IsDBNull(10) ? r.GetInt32(10) : -1,
                                    CreatedDateTime = !r.IsDBNull(11) ? r.GetDateTime(11) : DateTime.MinValue,
                                    ModifiedDateTime = !r.IsDBNull(12) ? r.GetDateTime(12) : DateTime.MinValue,
                                    CardNumber = !r.IsDBNull(13) ? r.GetDecimal(13) : -1,
                                    CardNumberLength = !r.IsDBNull(14) ? r.GetInt32(14) : -1
                                };
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return null;
        }

        /// <summary>
        /// Get all cards
        /// </summary>
        /// <param name="request">Filtering request</param>
        /// <returns>Cards</returns>
        public override List<TsCard> CardGetAll(CardGetAllRequest request)
        {
            var list = new List<TsCard>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "CardFunctions.CardGetAll", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCardNumber", OracleDbType = OracleDbType.Varchar2, Size = 22, Direction = ParameterDirection.Input, Value = request?.CardNumber?.PadLeft(22, '0') });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCardType", OracleDbType = OracleDbType.Int32, Size = 4, Direction = ParameterDirection.Input, Value = request?.CardType });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = request?.CustomerGuid });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCardStatus", OracleDbType = OracleDbType.Int32, Size = 4, Direction = ParameterDirection.Input, Value = request?.CardStatus });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCardIdm", OracleDbType = OracleDbType.Varchar2, Size = 16, Direction = ParameterDirection.Input, Value = request?.CardIdm });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsCard()
                                {
                                    CardNum = !r.IsDBNull(0) ? r.GetString(0) : string.Empty,
                                    Issue_Number = !r.IsDBNull(1) ? r.GetString(1) : string.Empty,
                                    Cust_Id = !r.IsDBNull(2) ? r.GetInt32(2) : -1,
                                    Card_Type = !r.IsDBNull(3) ? r.GetInt32(3) : -1,
                                    Card_Status = !r.IsDBNull(4) ? r.GetInt32(4) : -1,
                                    Card_Status_Text = !r.IsDBNull(5) ? r.GetString(5) : string.Empty,
                                    Card_Status_Datetime = !r.IsDBNull(6) ? DateTime.FromOADate(r.GetFloat(6)) : DateTime.MinValue,
                                    Lost_Flag = !r.IsDBNull(7) ? r.GetString(7) : string.Empty,
                                    Card_Idm = !r.IsDBNull(8) ? r.GetString(8) : string.Empty,
                                    DomainId = !r.IsDBNull(9) ? r.GetString(9) : string.Empty,
                                    IssuerId = !r.IsDBNull(10) ? r.GetInt32(10) : -1,
                                    CreatedDateTime = !r.IsDBNull(11) ? r.GetDateTime(11) : DateTime.MinValue,
                                    ModifiedDateTime = !r.IsDBNull(12) ? r.GetDateTime(12) : DateTime.MinValue,
                                    CardNumber = !r.IsDBNull(13) ? r.GetDecimal(13) : -1,
                                    CardNumberLength = !r.IsDBNull(14) ? r.GetInt32(14) : -1
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <summary>
        /// Removes temporary customer card
        /// </summary>
        /// <param name="cardNumber">Card number</param>
        public override void CardUpdateTemporary(string cardNumber)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CardFunctions.CardUpdateTemporary", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCardNum", OracleDbType = OracleDbType.Varchar2, Size = 22, Direction = ParameterDirection.Input, Value = cardNumber?.PadLeft(22, '0') });
                        con.Open();

                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Adds new card to customer
        /// </summary>
        /// <param name="customerNumber">Customer number</param>
        /// <param name="card">Card</param>
        /// <returns>Created customer card</returns>
        public override CustomerCard CardCreate(string customerNumber, CustomerCard card)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    card.CardNumber = card.CardNumber?.PadLeft(22, '0');
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CardFunctions.CardCreate", CommandType = CommandType.StoredProcedure })
                    {
                        decimal custNum;
                        var custNumResult = decimal.TryParse(customerNumber, out custNum);
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerNumber", OracleDbType = OracleDbType.Decimal, Size = 22, Direction = ParameterDirection.Input, Value = custNumResult ? (decimal?)custNum : null });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCardNum", OracleDbType = OracleDbType.Varchar2, Size = 22, Direction = ParameterDirection.Input, Value = card.CardNumber?.PadLeft(22, '0') });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pIssue_Number", OracleDbType = OracleDbType.Varchar2, Size = 4, Direction = ParameterDirection.Input, Value = card.IssueNumber });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCard_Idm", OracleDbType = OracleDbType.Varchar2, Size = 16, Direction = ParameterDirection.Input, Value = card.IdmNumber });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCard_Type", OracleDbType = OracleDbType.Int16, Size = 4, Direction = ParameterDirection.Input, Value = (int)card.CardType });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCard_Status", OracleDbType = OracleDbType.Int16, Size = 4, Direction = ParameterDirection.Input, Value = (int)card.CardStatusType });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCard_Status_Text", OracleDbType = OracleDbType.Varchar2, Size = 30, Direction = ParameterDirection.Input, Value = card.CommentText });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPrimary", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = Formatting.BooleanToTf(card.Primary) });
                        con.Open();

                        cmd.ExecuteNonQuery();
                        return card;
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Updates existing customer card
        /// </summary>
        /// <param name="customerNumber">Customer number</param>
        /// <param name="cardNumber">Card number</param>
        /// <param name="card">card</param>
        /// <returns>Updated customer card</returns>
        public override void CardUpdate(string customerNumber, string cardNumber, PatchCustomerCard card)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CardFunctions.CardUpdate", CommandType = CommandType.StoredProcedure })
                    {
                        decimal custNum;
                        var custNumResult = decimal.TryParse(customerNumber, out custNum);
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerNumber", OracleDbType = OracleDbType.Decimal, Size = 22, Direction = ParameterDirection.Input, Value = custNumResult ? (decimal?)custNum : null });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCardNum", OracleDbType = OracleDbType.Varchar2, Size = 22, Direction = ParameterDirection.Input, Value = cardNumber?.PadLeft(22, '0') });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pIssue_Number", OracleDbType = OracleDbType.Varchar2, Size = 4, Direction = ParameterDirection.Input, Value = card.IssueNumber });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pIssue_NumberSet", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = Formatting.BooleanToTf(card.IssueNumberIsSet) });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCard_Idm", OracleDbType = OracleDbType.Varchar2, Size = 16, Direction = ParameterDirection.Input, Value = card.IdmNumber });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCard_IdmSet", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = Formatting.BooleanToTf(card.IdmNumberIsSet) });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCard_Status", OracleDbType = OracleDbType.Int16, Size = 4, Direction = ParameterDirection.Input, Value = (int)card.CardStatusType });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCard_StatusSet", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = Formatting.BooleanToTf(card.CardStatusTypeIsSet) });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCard_Status_Text", OracleDbType = OracleDbType.Varchar2, Size = 30, Direction = ParameterDirection.Input, Value = card.CommentText });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCard_Status_TextSet", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = Formatting.BooleanToTf(card.CommentTextIsSet) });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pLost_Flag", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = Formatting.BooleanToTf(card.Lost) });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pLost_FlagSet", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = Formatting.BooleanToTf(card.LostIsSet) });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPrimary", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = Formatting.BooleanToTf(card.Primary) });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPrimarySet", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = Formatting.BooleanToTf(card.PrimaryIsSet) });
                        con.Open();

                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Create/Update a card.
        /// </summary>
        /// <param name="credential">Full card credential information.</param>
        public override void CardSet(CardCredentialComplete credential)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CardFunctions.CardSet", CommandType = CommandType.StoredProcedure })
                    {
                        var custId = credential.CustomerGuid == null ? null : new int?(CustomerIdFromCustomerGuidGet(credential.CustomerGuid.Value.ToString("D")));

                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCardNum", OracleDbType = OracleDbType.Varchar2, Size = 22, Direction = ParameterDirection.Input, Value = credential.CardNumber?.PadLeft(22, '0') });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pIssueNumber", OracleDbType = OracleDbType.Varchar2, Size = 4, Direction = ParameterDirection.Input, Value = credential.IssueNumber });

                        cmd.Parameters.Add(custId.HasValue ? 
                            new OracleParameter { ParameterName = "pCustId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = custId } :
                            new OracleParameter { ParameterName = "pCustId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = null });

                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCardType", OracleDbType = OracleDbType.Int16, Size = 4, Direction = ParameterDirection.Input, Value = (int)credential.CardType });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCardStatus", OracleDbType = OracleDbType.Int16, Size = 4, Direction = ParameterDirection.Input, Value = (int)credential.CardStatus });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCardStatusText", OracleDbType = OracleDbType.Varchar2, Size = 30, Direction = ParameterDirection.Input, Value = credential.CardStatusText });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCardStatusDateTime", OracleDbType = OracleDbType.Double, Size = 10, Direction = ParameterDirection.Input, Value = credential.CardStatusDateTime?.DateTime.ToOADate() });

                        var lostFlag = credential.LostFlag ?? false;
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pLostFlag", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = Formatting.BooleanToTf(lostFlag) });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCardIdm", OracleDbType = OracleDbType.Varchar2, Size = 16, Direction = ParameterDirection.Input, Value = credential.CardIdm });

                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pActiveStartDate", OracleDbType = OracleDbType.TimeStampTZ, Size = 6, Direction = ParameterDirection.Input, Value = credential.ActiveStartDate?.ToLocalTime() });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pActiveEndDate", OracleDbType = OracleDbType.TimeStampTZ, Size = 6, Direction = ParameterDirection.Input, Value = credential.ActiveEndDate?.ToLocalTime() });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCardName", OracleDbType = OracleDbType.Varchar2, Size = 30, Direction = ParameterDirection.Input, Value = credential.CardName });

                        var isPrimary = credential.IsPrimary ?? false;
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pIsPrimary", OracleDbType = OracleDbType.Int32, Size = 1, Direction = ParameterDirection.Input, Value = isPrimary ? 1 : 0 });
                        
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Get a mobile card credential by card number.
        /// </summary>
        /// <param name="cardNumber">The unique identifier for the card.</param>
        /// <returns>Mobile credential information for the card.</returns>
        public override MobileCredentialGetResponse MobileCredentialGet(string cardNumber)
        {
            var cardnum = cardNumber.PadLeft(22, '0');
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = @"
                        SELECT      CRD.CARDNUM,
                                    CRD.ISSUE_NUMBER,
                                    CRD.CUST_ID,
                                    CRD.CARD_TYPE,
                                    CRD.CARD_STATUS,
                                    CRD.CARD_STATUS_TEXT,
                                    CRD.CARD_STATUS_DATETIME,
                                    CRD.LOST_FLAG,
                                    CRD.CARD_IDM,
                                    CRD.DOMAINID,
                                    CRD.ISSUERID,
                                    CRD.ACTIVESTARTDATE,
                                    CRD.ACTIVEENDDATE,
                                    CRD.CARDNAME,
                                    DECODE(CUS.DefaultCardNum,CRD.CardNum,'T','F') AS IsPrimary
                        FROM        Envision.Card CRD
                        LEFT JOIN   Envision.Customer CUS ON CUS.Cust_Id = CRD.Cust_Id
                        WHERE       CRD.CARDNUM = :pValue",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter
                        {
                            ParameterName = "pValue",
                            OracleDbType = OracleDbType.Varchar2,
                            Size = 22,
                            Value = cardnum,
                            Direction = ParameterDirection.Input
                        });

                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            MobileCredentialGetResponse response = null;
                            while (r.Read())
                            {
                                var cardNumberRead = !r.IsDBNull(0) ? r.GetString(0) : string.Empty;
                                var issueNumber = !r.IsDBNull(1) ? r.GetString(1) : string.Empty;
                                var customerId = !r.IsDBNull(2) ? r.GetInt32(2) : -1;
                                var cardType = !r.IsDBNull(3) ? (NotificationCardType)r.GetInt32(3) : NotificationCardType.Unknown;
                                var cardStatus = !r.IsDBNull(4) ? (NotificationCardStatus)r.GetInt32(4) : NotificationCardStatus.Unknown;
                                var cardStatusText = !r.IsDBNull(5) ? r.GetString(5) : "";
                                var cardStatusDateTime = !r.IsDBNull(6) ? DateTime.FromOADate(r.GetDouble(6)) : DateTime.MinValue;
                                var lostFlag = !r.IsDBNull(7) && Formatting.TfStringToBool(r.GetString(7));
                                var cardIdm = !r.IsDBNull(8) ? r.GetString(8) : "";
                                var domainId = !r.IsDBNull(9) ? new Guid(r.GetString(9)) : new Guid();
                                var issuerId = !r.IsDBNull(10) ? r.GetInt32(10) : -1;
                                var activeStartDate = !r.IsDBNull(11) ? DateTimeOffsetFromOracleTimeStamp(r.GetOracleTimeStampTZ(11)) : null;
                                var activeEndDate = !r.IsDBNull(12) ? DateTimeOffsetFromOracleTimeStamp(r.GetOracleTimeStampTZ(12)) : null;
                                var cardName = !r.IsDBNull(13) ? r.GetString(13) : "";
                                var isPrimary = !r.IsDBNull(14) && Formatting.TfStringToBool(r.GetString(14));

                                var guid = customerId == -1 ? null : CustomerGuidFromCustomerIdGet(customerId);

                                var credential = new CardCredentialComplete
                                {
                                    CardNumber = cardNumberRead,
                                    IssueNumber = issueNumber,
                                    CardType = cardType,
                                    CardStatus = cardStatus,
                                    CardStatusText = cardStatusText,
                                    CardStatusDateTime = cardStatusDateTime,
                                    LostFlag = lostFlag,
                                    CardIdm = cardIdm,
                                    DomainId = domainId,
                                    IssuerId = issuerId,
                                    ActiveStartDate = activeStartDate,
                                    ActiveEndDate = activeEndDate,
                                    CardName = cardName,
                                    IsPrimary = isPrimary
                                };

                                if (string.IsNullOrWhiteSpace(guid))
                                {
                                    credential.CustomerGuid = null;
                                }
                                else
                                {
                                    credential.CustomerGuid = new Guid(guid);
                                }

                                response = new MobileCredentialGetResponse
                                {
                                    Credential = credential
                                };
                            }
                            return response;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Patch a mobile card credential by card number.
        /// </summary>
        /// <param name="request">The patch request.</param>
        /// <returns>Results of the operation.</returns>
        public override MobileCredentialPatchResponse MobileCredentialPatch(MobileCredentialPatchRequest request)
        {
            try
            {
                CardSet(request.Credential);

                return new MobileCredentialPatchResponse
                {
                    RequestId = request.RequestId.ToString("D"),
                    DeniedText = "",
                    ErrorCode = 0
                };
            }
            catch (Exception ex)
            {
                throw new ResourceLayerException(request.RequestId.ToString("D"), Formatting.FormatException(ex));
            }
        }

        /// <summary>
        /// Post a mobile card credential by card number.
        /// </summary>
        /// <param name="request">The post request.</param>
        /// <returns>Results of the operation.</returns>
        public override MobileCredentialPostResponse MobileCredentialPost(MobileCredentialPostRequest request)
        {
            try
            {
                CardSet(request.Credential);

                return new MobileCredentialPostResponse
                {
                    RequestId = request.RequestId.ToString("D"),
                    DeniedText = "",
                    ErrorCode = 0
                };
            }
            catch (Exception ex)
            {
                throw new ResourceLayerException(request.RequestId.ToString("D"), Formatting.FormatException(ex));
            }
        }

        /// <inheritdoc />
        public override List<TsCard> CardsTemporaryUnassignedGet()
        {
            var list = new List<TsCard>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CardFunctions.CardTemporaryUnassignedGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsCard()
                                {
                                    CardNum = !r.IsDBNull(0) ? r.GetString(0) : string.Empty,
                                    Issue_Number = !r.IsDBNull(1) ? r.GetString(1) : string.Empty,
                                    Cust_Id = !r.IsDBNull(2) ? r.GetInt32(2) : -1,
                                    Card_Type = !r.IsDBNull(3) ? r.GetInt32(3) : -1,
                                    Card_Status = !r.IsDBNull(4) ? r.GetInt32(4) : -1,
                                    Card_Status_Text = !r.IsDBNull(5) ? r.GetString(5) : string.Empty,
                                    Card_Status_Datetime = !r.IsDBNull(6) ? DateTime.FromOADate(r.GetFloat(6)) : DateTime.MinValue,
                                    Lost_Flag = !r.IsDBNull(7) ? r.GetString(7) : string.Empty,
                                    Card_Idm = !r.IsDBNull(8) ? r.GetString(8) : string.Empty,
                                    DomainId = !r.IsDBNull(9) ? r.GetString(9) : string.Empty,
                                    IssuerId = !r.IsDBNull(10) ? r.GetInt32(10) : -1,
                                    CreatedDateTime = !r.IsDBNull(11) ? r.GetDateTime(11) : DateTime.MinValue,
                                    ModifiedDateTime = !r.IsDBNull(12) ? r.GetDateTime(12) : DateTime.MinValue,
                                    CardNumber = !r.IsDBNull(13) ? r.GetDecimal(13) : -1,
                                    CardNumberLength = !r.IsDBNull(14) ? r.GetInt32(14) : -1
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <inheritdoc />
        public override CardNumberAvailableResponse CardNumberAvailable(decimal cardNumber)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CardFunctions.CardNumberAvailable", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCardNumber", OracleDbType = OracleDbType.Decimal, Size = 22, Direction = ParameterDirection.Input, Value = cardNumber });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pIsAvailable", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pIsTemporaryCard", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Output });
                        con.Open();
                        cmd.ExecuteNonQuery();

                        return new CardNumberAvailableResponse()
                        {
                            CardNumber = cardNumber,
                            Available = Formatting.TfStringToBool(cmd.Parameters["pIsAvailable"].Value.ToString()),
                            Temporary = Formatting.TfStringToBool(cmd.Parameters["pIsTemporaryCard"].Value.ToString())
                        };
                    }

                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override List<CardAssignmentHistoryItem> CardHistoryGet(string cardNumber)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CardFunctions.CardHistoryGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCardNum", OracleDbType = OracleDbType.Varchar2, Size = 22, Direction = ParameterDirection.Input, Value = cardNumber?.PadLeft(22, '0') });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            var list = new List<CardAssignmentHistoryItem>();
                            while (r.Read())
                            {
                                list.Add(new CardAssignmentHistoryItem()
                                {
                                    CardHistoryId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    CardNumber = !r.IsDBNull(1) ? r.GetString(1) : string.Empty,
                                    IssueNumber = !r.IsDBNull(2) ? r.GetString(2) : string.Empty,
                                    CustomerId = !r.IsDBNull(3) ? r.GetInt32(3) : (int?)null,
                                    StartDateTime = !r.IsDBNull(4) ? DateTime.FromOADate(r.GetFloat(4)) : DateTime.MinValue,
                                });
                            }
                            return list;
                        }
                    }

                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }
    }
}