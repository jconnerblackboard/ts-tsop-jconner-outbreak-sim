﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Definitions.Monitoring;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.Messaging.SecurityMonitor;
using BbTS.Domain.Models.Messaging.Subscription;
using BbTS.Domain.Models.Messaging.Subscription.Rest;
using BbTS.Monitoring.Logging;
using Oracle.ManagedDataAccess.Client;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        /// <summary>
        /// Create an association between doors and a registered security monitor subscribing client.
        /// </summary>
        /// <param name="request">The request for association.</param>
        /// <returns>The result of the operation.</returns>
        public override DoorClientAssociationtResponse DoorClientAssociationCreate (DoorClientAssociationRequest request)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Remove an association between doors and a registered security monitor subscribing client.
        /// </summary>
        /// <param name="request">The request for disassociation.</param>
        /// <returns>The result of the operation.</returns>
        public override DoorClientAssociationtResponse DoorClientAssociationRemove(DoorClientAssociationRequest request)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get a list of all subscription registrations in the data layer.
        /// </summary>
        /// <returns>List of all subscription registrations.</returns>
        public override List<SubscriptionRegistration> SubscriptionRegistrationGet(string clientGuid = null, SubscriptionServiceType subscriptionType = SubscriptionServiceType.None)
        {
            var list = new List<SubscriptionRegistration>();
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "Messaging.MessageSubscriptionGet", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        var subType = subscriptionType == SubscriptionServiceType.None ? null : (int?)subscriptionType;
                        cmd.Parameters.Add(new OracleParameter("pClientGuid", OracleDbType.Varchar2, 36, clientGuid, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pSubscriptionServiceType", OracleDbType.Int32, 4, subType, ParameterDirection.Input));

                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Size = 0, Direction = ParameterDirection.Output, Value = string.Empty });
                        
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var registration = new SubscriptionRegistration();

                                if (!r.IsDBNull(0)) registration.ClientGuid = r.GetString(0);
                                if (!r.IsDBNull(1)) registration.CallbackUri = r.GetString(1);
                                if (!r.IsDBNull(2)) registration.RetrieveAllMessagesInQueue = Formatting.TfStringToBool(r.GetString(2));
                                if (!r.IsDBNull(3)) registration.SubscriptionServiceType = (SubscriptionServiceType)r.GetInt32(3);
                                if (!r.IsDBNull(4)) registration.SubscriptionDateTime = r.GetDateTime(4);
                                if (!r.IsDBNull(5)) registration.LastCommunicated = r.GetDateTime(5);

                                list.Add(registration);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        var rex = new ResourceLayerException(new Guid().ToString("D"), Formatting.FormatException(ex));
                        throw rex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            return list;
        }

        /// <summary>
        /// Register for a subscription service.
        /// </summary>
        /// <param name="request">The registration request.</param>
        /// <param name="username">Username</param>
        /// <returns>The results of the operation.</returns>
        public override SubscriptionRegistrationResponse SubscriptionServiceRegister(SubscriptionRegistrationRequest request, string username)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "Messaging.MessageSubscriptionSet", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pClientGuid", OracleDbType.Varchar2, 36, request.ClientGuid, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pCallbackUri", OracleDbType.Varchar2, 2000, request.CallbackUri, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pSendAllOnFirstCallback", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.RetrieveAllMessagesInQueue), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pSubscriptionServiceType", OracleDbType.Int32, 4, (int)request.SubscriptionServiceType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pUserName", OracleDbType.Varchar2, 15, username, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pSubscriptionDateTime", OracleDbType.TimeStampTZ, DateTimeOffset.Now, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pLastCommunicated", OracleDbType.TimeStampTZ, request.RequestReceivedDateTime, ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();

                        return new SubscriptionRegistrationResponse
                        {
                            RequestId = request.RequestId,
                            ErrorCode = 0,
                            DeniedText = "Success."
                        };

                    }
                    catch (Exception ex)
                    {
                        var rex = new ResourceLayerException(new Guid().ToString("D"), Formatting.FormatException(ex));
                        throw rex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Unregister for a subscription service.
        /// </summary>
        /// <param name="request">The registration request.</param>
        /// <returns>The results of the operation.</returns>
        public override SubscriptionRegistrationResponse SubscriptionServiceUnregister(SubscriptionRegistrationRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "Messaging.MessageSubscriptionDelete", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pClientGuid", OracleDbType.Varchar2, 36, request.ClientGuid, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pSubscriptionServiceType", OracleDbType.Int32, 4, (int)request.SubscriptionServiceType, ParameterDirection.Input));
                        
                        con.Open();
                        cmd.ExecuteNonQuery();

                        return new SubscriptionRegistrationResponse
                        {
                            RequestId = request.RequestId,
                            ErrorCode = 0,
                            DeniedText = "Success."
                        };
                    }
                    catch (Exception ex)
                    {
                        var rex = new ResourceLayerException(new Guid().ToString("D"), Formatting.FormatException(ex));
                        throw rex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Update the last communicated date and time for a client and service.
        /// </summary>
        /// <param name="clientGuid">The unique identifier for the client.</param>
        /// <param name="type">The type of service.</param>
        /// <param name="lastCommunicated">The date and time the client was last communicated with.</param>
        public override void SubscriptionServiceUpdateLastCommunicated(string clientGuid, SubscriptionServiceType type, DateTimeOffset lastCommunicated)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "Messaging.UpdateLastCommunicated", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pClientGuid", OracleDbType.Varchar2, 36, clientGuid, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pSubscriptionServiceType", OracleDbType.Int32, 4, (int)type, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pLastCommunicated", OracleDbType.TimeStampTZ, lastCommunicated, ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();

                    }
                    catch (Exception ex)
                    {
                        var rex = new ResourceLayerException(new Guid().ToString("D"), Formatting.FormatException(ex));
                        throw rex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Toggle the setting in the data layer that indicates whether or not to send all stored messages on the next callback.
        /// </summary>
        /// <param name="clientGuid">The unique identifier for the client.</param>
        /// <param name="type">The type of service.</param>
        /// <param name="sendAllOnNextCallback">Send all or not.</param>
        public override void SubscriptionServiceToggleReceiveAll(string clientGuid, SubscriptionServiceType type, bool sendAllOnNextCallback)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "Messaging.UpdateSendAllFlag", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pClientGuid", OracleDbType.Varchar2, 36, clientGuid, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pSubscriptionServiceType", OracleDbType.Int32, 4, (int)type, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pSendAllOnFirstCallback", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(sendAllOnNextCallback), ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        var rex = new ResourceLayerException(new Guid().ToString("D"), Formatting.FormatException(ex));
                        throw rex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }
    }
}
