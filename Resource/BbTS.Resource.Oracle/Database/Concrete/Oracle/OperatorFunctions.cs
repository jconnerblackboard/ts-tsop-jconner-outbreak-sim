﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net;
using BbTS.Core;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Credential;
using BbTS.Domain.Models.Definitions.Card;
using BbTS.Domain.Models.Definitions.Container;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Definitions.Operator;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.Exceptions.WebApi;
using BbTS.Domain.Models.Operator;
using BbTS.Domain.Models.Transaction;
using BbTS.Monitoring.Logging;
using Oracle.ManagedDataAccess.Client;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {

        /// <summary>
        /// Validate operator card number credentials.
        /// </summary>
        /// <param name="request">Validation request.</param>
        /// <returns>Validation response.</returns>
        public override ValidateOperatorCardNumberResponse ValidateOperatorCardNumber(ValidateOperatorCardNumberRequest request)
        {
            return new ValidateOperatorCardNumberResponse(request.RequestId)
            {
                IsValid = true
            };
        }

        /// <summary>
        /// Change an operator pin.
        /// </summary>
        /// <param name="request">PIN change information.</param>
        /// <param name="cashierId">The numberical id of the cashier (required by the stored proc).</param>
        /// <returns>Pin change response</returns>
        public override OperatorChangePinResponse ChangeOperatorPin(OperatorChangePinRequest request, int cashierId)
        {
            Guard.IsNotNull(request, "request");
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");

            using (OracleConnection con = new OracleConnection(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "CashierFunctions.CashierPinSet", CommandType = CommandType.StoredProcedure })
                {
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pCashierId", OracleDbType = OracleDbType.Int32, Size = 6, Direction = ParameterDirection.Input, Value = cashierId });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pOldPin", OracleDbType = OracleDbType.Int32, Size = 4, Direction = ParameterDirection.Input, Value = int.Parse(request.OldPin) });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pNewPin", OracleDbType = OracleDbType.Int32, Size = 4, Direction = ParameterDirection.Input, Value = int.Parse(request.NewPin) });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pErrorCode", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Output });

                    try
                    {
                        con.Open();
                        cmd.ExecuteNonQuery();

                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);

                        if (errorCode == 0)
                        {
                            return new OperatorChangePinResponse(request.RequestId);
                        }

                        var result = BbtsErrorDefinitions.ErrorWithAdditionalInfoGet(errorCode, $"Error Code [{errorCode}]: Error while processing CashierPinSet request.");
                        throw new WebApiException(result, HttpStatusCode.BadRequest);
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException(request.RequestId, Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Process an operator session begin request.
        /// </summary>
        /// <param name="request">The request.</param>
        public override OperatorSessionBeginEndProcessResponse OperatorSessionBeginProcess(OperatorSessionBeginEndProcessRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "TransactionFunctions.CashierSessionBegin", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        var posId = PosIdFromOriginatorGuidGet(request.DeviceGuid);
                        var cashierId = CashierIdFromGuidGet(request.OperatorGuid);

                        cmd.Parameters.Add(new OracleParameter("nPosId", OracleDbType.Int32, 10, posId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("nCashierId", OracleDbType.Int32, 6, cashierId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("nCashDrawerNum", OracleDbType.Int32, 10, request.CashDrawerNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("nTransactionNumber", OracleDbType.Int32, 10, request.TransactionNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("nTransactionDateTime", OracleDbType.Double, 14, request.TransactionDateTime.ToOADate(), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("vKeyedOfflineFlag", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.KeyedOfflineFlag), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("vOnSuccessCommit", OracleDbType.Varchar2, 1, "T", ParameterDirection.Input));

                        cmd.Parameters.Add(new OracleParameter("pErrorCode", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pDeniedText", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 75 });
                        cmd.Parameters.Add(new OracleParameter("pSessionGuid", OracleDbType.Varchar2, 36, request.SessionGuid, ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();

                        var deniedText = cmd.Parameters["pDeniedText"].Value.ToString();
                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);
                        return new OperatorSessionBeginEndProcessResponse
                        {
                            RequestId = request.RequestId,
                            DeniedText = deniedText,
                            ErrorCode = errorCode
                        };
                    }
                    catch (Exception ex)
                    {
                        var message = Formatting.FormatException(ex);
                        var rex = new ResourceLayerException(string.Empty, message);
                        throw rex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Process an operator session end request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The result of the operation.</returns>
        public override OperatorSessionBeginEndProcessResponse OperatorSessionEndProcess(OperatorSessionBeginEndProcessRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "TransactionFunctions.CashierSessionEnd", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        var posId = PosIdFromOriginatorGuidGet(request.DeviceGuid);
                        var cashierId = CashierIdFromGuidGet(request.OperatorGuid);

                        cmd.Parameters.Add(new OracleParameter("nPosId", OracleDbType.Int32, 10, posId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("nCashierId", OracleDbType.Int32, 6, cashierId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("nCashDrawerNum", OracleDbType.Int32, 10, request.CashDrawerNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("nTransactionNumber", OracleDbType.Int32, 10, request.TransactionNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("nTransactionDateTime", OracleDbType.Double, 14, request.TransactionDateTime.ToOADate(), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("vKeyedOfflineFlag", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.KeyedOfflineFlag), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("vOnSuccessCommit", OracleDbType.Varchar2, 1, "T", ParameterDirection.Input));

                        cmd.Parameters.Add(new OracleParameter("pErrorCode", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pDeniedText", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 75 });
                        cmd.Parameters.Add(new OracleParameter("pSessionGuid", OracleDbType.Varchar2, 36, request.SessionGuid, ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();

                        var deniedText = cmd.Parameters["pDeniedText"].Value.ToString();
                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);
                        return new OperatorSessionBeginEndProcessResponse
                        {
                            RequestId = request.RequestId,
                            DeniedText = deniedText,
                            ErrorCode = errorCode
                        };
                    }
                    catch (Exception ex)
                    {
                        var message = Formatting.FormatException(ex);
                        var rex = new ResourceLayerException(string.Empty, message);
                        throw rex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Get an operator id (guid) from an e-mail address (assigned to the customer associated with the operator).
        /// </summary>
        /// <param name="email">Customer e-mail address.</param>
        /// <returns>Operator Id (Guid) or null if not found.</returns>
        public override string OperatorIdFromEmailAddress(string email)
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                string query = @"
                        SELECT          CSH.CashierGuid
                        FROM            Envision.Cashier CSH
                        INNER JOIN      Envision.Customer_Email EML on EML.Cust_ID = CSH.Cust_ID
                        WHERE           EML.EMAIL_ADDRESS = :pValue";

                using (OracleCommand cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = query,
                    CommandType = CommandType.Text
                })
                {

                    cmd.Parameters.Add(
                        new OracleParameter { ParameterName = "pValue", OracleDbType = OracleDbType.Varchar2, Size = 255, Value = email, Direction = ParameterDirection.Input });
                    try
                    {
                        con.Open();
                        using (OracleDataReader r = cmd.ExecuteReader())
                        {
                            string operatorId = null;
                            while (r.Read())
                            {
                                operatorId = !r.IsDBNull(0) ? r.GetString(0) : null;
                            }
                            return operatorId;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Get an operator id (guid) from basic credential information (assigned to the customer associated with the operator).
        /// </summary>
        /// <param name="credential">Basic credential information, including card number and issue number (needed by this function).</param>
        /// <returns>Operator Id (Guid)</returns>
        public override string OperatorIdFromCardNumber(BasicCardCredential credential)
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");

            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                string query = @"
                        SELECT      CSH.CashierGuid
                        FROM        Card CRD
                        INNER JOIN  Cashier CSH on CRD.Cust_ID = CSH.Cust_ID
                        WHERE       CRD.CardNum = LPAD(:pCardNum, 22,'0')
                        AND         NVL2(:pIssueNum, CRD.Issue_Number, '0') = NVL(:pIssueNum,'0')";

                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = query, CommandType = CommandType.Text })
                {
                    cmd.Parameters.Add(
                        new OracleParameter { ParameterName = "pCardNum", OracleDbType = OracleDbType.Varchar2, Size = 22, Value = credential.CardNumber, Direction = ParameterDirection.Input });
                    cmd.Parameters.Add(
                        new OracleParameter { ParameterName = "pIssueNum", OracleDbType = OracleDbType.Varchar2, Size = 4, Value = credential.IssueNumber, Direction = ParameterDirection.Input });

                    try
                    {
                        con.Open();
                        using (OracleDataReader r = cmd.ExecuteReader())
                        {
                            string operatorId = null;
                            while (r.Read())
                            {
                                operatorId = !r.IsDBNull(0) ? r.GetString(0) : null;
                            }
                            return operatorId;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Get the operator PIN.
        /// </summary>
        /// <param name="operatorId">The unique (Guid) identifier for the operator.</param>
        /// <returns>Operator's PIN or null if not found.</returns>
        public override string OperatorPinGet(string operatorId)
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                string query = @"SELECT CSH.Pin FROM Envision.Cashier CSH WHERE CSH.CashierGuid = :pValue";

                using (OracleCommand cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = query,
                    CommandType = CommandType.Text
                })
                {

                    cmd.Parameters.Add(
                        new OracleParameter { ParameterName = "pValue", OracleDbType = OracleDbType.Varchar2, Size = 36, Value = operatorId, Direction = ParameterDirection.Input });

                    try
                    {
                        con.Open();
                        using (OracleDataReader r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                return !r.IsDBNull(0) ? r.GetInt32(0).ToString() : null;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Get the operator's email address.
        /// </summary>
        /// <param name="operatorId">The unique (Guid) identifier for the operator.</param>
        /// <returns>Operator's email address or null if not found.</returns>
        public override string OperatorEmailGet(string operatorId)
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                string query = @"
                    SELECT          EML.Email_Address
                    FROM            Envision.Cashier CSH
                    INNER JOIN      Envision.Customer_Email EML on EML.Cust_ID = CSH.Cust_ID
                    WHERE           CSH.CashierGuid = :pValue";

                using (OracleCommand cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = query,
                    CommandType = CommandType.Text
                })
                {

                    cmd.Parameters.Add(
                        new OracleParameter { ParameterName = "pValue", OracleDbType = OracleDbType.Varchar2, Size = 36, Value = operatorId, Direction = ParameterDirection.Input });

                    try
                    {
                        con.Open();
                        using (OracleDataReader r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                return !r.IsDBNull(0) ? r.GetString(0) : null;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Get the operator's card number.
        /// </summary>
        /// <param name="operatorId">The unique (Guid) identifier for the operator.</param>
        /// <returns>Operator's card number or null if not found.</returns>
        public override string OperatorCardNumberGet(string operatorId)
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                string query = @"
                    SELECT          CRD.Cardnum
                    FROM            Envision.Cashier CSH
                    INNER JOIN      Envision.Card CRD on CSH.CUST_ID = CRD.CUST_ID
                    WHERE           CSH.CashierGuid = :pValue";

                using (OracleCommand cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = query,
                    CommandType = CommandType.Text
                })
                {

                    cmd.Parameters.Add(
                        new OracleParameter { ParameterName = "pValue", OracleDbType = OracleDbType.Varchar2, Size = 36, Value = operatorId, Direction = ParameterDirection.Input });

                    try
                    {
                        con.Open();
                        using (OracleDataReader r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                return !r.IsDBNull(0) ? r.GetString(0) : null;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Get an operator's basic credential information.
        /// </summary>
        /// <param name="operatorId">The unique (Guid) identifier for the operator.</param>
        /// <returns>Credential for the operator or null if not found.</returns>
        public override BasicCardCredential OperateCardNumberAndIssueNumberGet(string operatorId)
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                string query = @"
                    SELECT          CRD.Cardnum,
                                    CRD.Issue_Number
                    FROM            Envision.Card CRD
                    INNER JOIN      Envision.Cashier CSH on CSH.CUST_ID = CRD.CUST_ID
                    WHERE           CSH.CashierGuid = :pValue";

                using (OracleCommand cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = query,
                    CommandType = CommandType.Text
                })
                {

                    cmd.Parameters.Add(
                        new OracleParameter { ParameterName = "pValue", OracleDbType = OracleDbType.Varchar2, Size = 36, Value = operatorId, Direction = ParameterDirection.Input });

                    try
                    {
                        con.Open();
                        using (OracleDataReader r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new BasicCardCredential();

                                if (!r.IsDBNull(0)) item.CardNumber = r.GetString(0);
                                if (!r.IsDBNull(1)) item.IssueNumber = r.GetString(1);
                                return item;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Get all operators for a specific device.
        /// </summary>
        /// <param name="deviceId">The unique (Guid) identifier for the device.</param>
        /// <returns>List of <see cref="Operator"/>s on a device.</returns>
        public override List<Operator> OperatorsDeviceSettingsGet(string deviceId)
        {
            var list = new List<Operator>();

            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "OriginatorFunctions.OperatorsForDeviceGet", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDeviceGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Value = deviceId, Direction = ParameterDirection.Input });
                        cmd.Parameters.Add(new OracleParameter("pList", OracleDbType.RefCursor, ParameterDirection.Output));

                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                try
                                {
                                    var item = new Operator();

                                    if (!r.IsDBNull(0)) item.OperatorId = r.GetString(0);
                                    if (!r.IsDBNull(1)) item.OperatorNumber = r.GetInt32(1);
                                    if (!r.IsDBNull(2)) item.FullName = r.GetString(2);
                                    if (!r.IsDBNull(3)) item.Pin = r.GetInt32(3).ToString();
                                    if (!r.IsDBNull(4)) item.IsManager = Formatting.TfStringToBool(r.GetString(4));
                                    if (!r.IsDBNull(5)) item.CashDrawer = r.GetInt32(5);
                                    if (!r.IsDBNull(6)) item.PinRequired = Formatting.TfStringToBool(r.GetString(6));
                                    if (!r.IsDBNull(7)) item.PinChangeRequired = Formatting.TfStringToBool(r.GetString(7));
                                    if (!r.IsDBNull(8)) item.CardNumber = r.GetString(8);
                                    if (!r.IsDBNull(9)) item.IssueNumber = r.GetString(9);
                                    if (!r.IsDBNull(10)) item.RoleGroup = r.GetString(10);
                                    if (!r.IsDBNull(11)) item.CardStatus = (CardStatusType)r.GetInt16(11);
                                    if (!r.IsDBNull(12)) item.CardActiveStartDate = r.GetDateTime(12);
                                    if (!r.IsDBNull(13)) item.CardActiveEndDate = r.GetDateTime(13);
                                    if (!r.IsDBNull(14)) item.CardLost = Formatting.TfStringToBool(r.GetString(14));
                                    

                                    item.RoleGroup = new Guid(item.RoleGroup).ToString("D");

                                    list.Add(item);
                                }
                                catch (Exception ex)
                                {
                                    var rex = new ResourceLayerException(deviceId, Formatting.FormatException(ex));
                                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException(deviceId, Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Get a list of all operator roles for a specific device.
        /// </summary>
        /// <param name="deviceId">The unique (Guid) identifier for the device.</param>
        /// <returns>List of operator roles.</returns>
        public override List<OperatorRole> OperatorRolesForDeviceGet(string deviceId)
        {
            var list = new List<OperatorRole>();

            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = @"
                        SELECT      DISTINCT
                                    RGH.PermissionGroup,
                                    RGH.Permission
                        FROM        Pos                     POS
                        INNER JOIN  Originator              DEV ON  POS.OriginatorId     = DEV.OriginatorId
                        INNER JOIN  ProfitCenter            PC  ON  POS.ProfitCenter_Id  = PC.ProfitCenter_Id
                        INNER JOIN  Cashier                 CSH ON  CSH.Merchant_Id      = PC.Merchant_Id
                        INNER JOIN  Cashier_Rights          CR  ON  CSH.Cashier_Id       = CR.Cashier_Id
                        INNER JOIN  CashierRights           RGH ON  CR.PermissionHash    = RGH.PermissionGroup
                        INNER JOIN  CashierProfitCenters    CPC ON  CSH.Cashier_Id       = CPC.Cashier_Id
                                                                AND PC.ProfitCenter_Id   = CPC.ProfitCenter_Id
                        WHERE       DEV.OriginatorGuid = :pValue
                        AND         RGH.PermissionParent IS NOT NULL
                        AND         CSH.Is_Active = 'T'
                        ORDER BY    RGH.PermissionGroup,
                                    RGH.Permission",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        cmd.Parameters.Add(
                            new OracleParameter
                            {
                                ParameterName = "pValue",
                                OracleDbType = OracleDbType.Varchar2,
                                Size = 36,
                                Value = deviceId,
                                Direction = ParameterDirection.Input
                            });

                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                try
                                {
                                    var permissionGroup = !r.IsDBNull(0) ? new Guid(r.GetString(0)).ToString("D") : null;
                                    var permission = !r.IsDBNull(1) ? (OperatorRight)Enum.Parse(typeof(OperatorRight), r.GetString(1)) : OperatorRight.Undefined;

                                    if (permission == OperatorRight.Undefined)
                                    {
                                        LoggingManager.Instance.LogWebApiException(
                                            new WebApiException(
                                                deviceId,
                                                @"Null Permission value was found when retrieving a operator role permission from the data layer.  Verify data integrity.",
                                                HttpStatusCode.InternalServerError),
                                            EventLogEntryType.Error,
                                            (short)LoggingDefinitions.Category.Resource,
                                            (int)LoggingDefinitions.EventId.ResourceInvalidEnumerationParse
                                            );
                                        continue;
                                    }

                                    if (!string.IsNullOrWhiteSpace(permissionGroup))
                                    {
                                        var index = list.FindIndex(x => x.RoleId == permissionGroup);
                                        if (index >= 0 && !list[index].Rights.Contains(permission))
                                        {
                                            list[index].Rights.Add(permission);
                                        }
                                        else
                                        {
                                            list.Add(new OperatorRole { RoleId = permissionGroup, Rights = new List<OperatorRight> { permission } });
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    var rex = new ResourceLayerException(deviceId, Formatting.FormatException(ex));
                                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        var rex = new ResourceLayerException(deviceId, Formatting.FormatException(ex));
                        throw rex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Get an operator by their unique (guid) identifier.
        /// </summary>
        /// <param name="operatorGuid">The unique (guid) identifier for the operator.</param>
        /// <returns>The operator or null if not found.</returns>
        public override Operator OperatorByGuidGet(string operatorGuid)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = @"
                       SELECT       CSH.CashierGuid         AS OperatorId,
                                    CSH.CashierNumber       AS OperatorNumber,
                                    CSH.FullName,
                                    CSH.Pin,
                                    CSH.Manager             AS IsManager,
                                    CSH.CashDrawer,
                                    CSH.PinRequired,
                                    CSH.PinChangeRequired,
                                    CRD.CardNum             AS CardNumber,
                                    CRD.Issue_Number        AS IssueNumber,
                                    CRD.Card_Status,
                                    CRD.ActiveStartDate,
                                    CRD.ActiveEndDate,
                                    CRD.Lost_Flag,
                                    CR.PermissionHash       AS PermissionGroup
                        FROM        Cashier         CSH
                        INNER JOIN  Cashier_Rights  CR  ON  CSH.Cashier_Id       = CR.Cashier_Id
                        LEFT JOIN   Customer        CUS ON  CSH.Cust_Id          = CUS.Cust_Id
                        LEFT JOIN   Card            CRD ON  CUS.DefaultCardNum  = CRD.CardNum
                                                        AND CUS.Cust_Id         = CRD.Cust_Id
                        WHERE       CSH.CashierGuid = :pValue",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        cmd.Parameters.Add(
                            new OracleParameter
                            {
                                ParameterName = "pValue",
                                OracleDbType = OracleDbType.Varchar2,
                                Size = 36,
                                Value = operatorGuid,
                                Direction = ParameterDirection.Input
                            });


                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                try
                                {
                                    var item = new Operator();

                                    if (!r.IsDBNull(0)) item.OperatorId = r.GetString(0);
                                    if (!r.IsDBNull(1)) item.OperatorNumber = r.GetInt32(1);
                                    if (!r.IsDBNull(2)) item.FullName = r.GetString(2);
                                    if (!r.IsDBNull(3)) item.Pin = r.GetInt32(3).ToString();
                                    if (!r.IsDBNull(4)) item.IsManager = Formatting.TfStringToBool(r.GetString(4));
                                    if (!r.IsDBNull(5)) item.CashDrawer = r.GetInt32(5);
                                    if (!r.IsDBNull(6)) item.PinRequired = Formatting.TfStringToBool(r.GetString(6));
                                    if (!r.IsDBNull(7)) item.PinChangeRequired = Formatting.TfStringToBool(r.GetString(7));
                                    if (!r.IsDBNull(8)) item.CardNumber = r.GetString(8);
                                    if (!r.IsDBNull(9)) item.IssueNumber = r.GetString(9);
                                    if (!r.IsDBNull(10)) item.CardStatus = (CardStatusType)r.GetInt16(10);
                                    if (!r.IsDBNull(11)) item.CardActiveStartDate = r.GetDateTime(11);
                                    if (!r.IsDBNull(12)) item.CardActiveEndDate = r.GetDateTime(12);
                                    if (!r.IsDBNull(13)) item.CardLost = Formatting.TfStringToBool(r.GetString(13));
                                    if (!r.IsDBNull(14)) item.RoleGroup = r.GetString(14);

                                    item.RoleGroup = new Guid(item.RoleGroup).ToString("D");

                                    return item;
                                }
                                catch (Exception ex)
                                {
                                    var rex = new ResourceLayerException(operatorGuid, Formatting.FormatException(ex));
                                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        var rex = new ResourceLayerException(operatorGuid, Formatting.FormatException(ex));
                        throw rex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            return null;
        }

        /// <inheritdoc />
        public override List<TsOperator> OperatorsByMerchantGet(Guid merchantGuid)
        {
            var list = new List<TsOperator>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "CashierFunctions.CashiersGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pMerchantGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = merchantGuid.ToString("D") });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsOperator()
                                {
                                    Id = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    MerchantId = !r.IsDBNull(1) ? r.GetInt16(1) : (Int16)(-1),
                                    OperatorNumber = !r.IsDBNull(2) ? r.GetInt32(2) : -1,
                                    IsActive = !r.IsDBNull(3) && Formatting.TfStringToBool(r.GetString(3)),
                                    Pin = !r.IsDBNull(4) ? (Int16?)r.GetInt16(4) : null,
                                    FullName = !r.IsDBNull(5) ? r.GetString(5) : null,
                                    Manager = !r.IsDBNull(6) && Formatting.TfStringToBool(r.GetString(6)),
                                    CashDrawer = !r.IsDBNull(7) ? (CashDrawerNumber)r.GetInt32(7) : CashDrawerNumber.NotSpecified,
                                    CustomerId = !r.IsDBNull(8) ? (int?)r.GetInt32(8) : null,
                                    PinRequired = !r.IsDBNull(9) && Formatting.TfStringToBool(r.GetString(9)),
                                    PinChangeRequired = !r.IsDBNull(10) && Formatting.TfStringToBool(r.GetString(10)),
                                    Guid = !r.IsDBNull(11) ? Guid.Parse(r.GetString(11)) : Guid.Empty,
                                    Rights = !r.IsDBNull(12) ? r.GetString(12).Split(';').Select(x =>
                                    {
                                        OperatorRight permission;
                                        return Enum.TryParse(x, out permission) ? permission : OperatorRight.Undefined;
                                    }).Distinct().ToList() : new List<OperatorRight>()
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }
    }
}
