﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using BbTS.Core;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.Exceptions.WebApi;
using BbTS.Domain.Models.Pos;
using BbTS.Domain.Models.System.Database;
using Oracle.ManagedDataAccess.Client;
using BbTS.Domain.Models.Definitions.Pos;
using BbTS.Domain.Models.Pos.PosOptions;
using BbTS.Resource.OracleManagedDataAccessExtensions;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        #region Pos

        /// <summary>
        /// This method will return a list of records for the Pos objects in the system.  If connection is null, <see cref="ConnectionString"/> will be used
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsPos> PosArchivesGet(ConnectionInfo connection = null)
        {
            var list = new List<TsPos>();

            using (var con = connection == null ? OracleConnectionGet(ConnectionString) : OracleConnectionGet(connection))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = @"SELECT  Pos_Id,
                                          ProfitCenter_Id,
                                          Name,
                                          PosType,
                                          Pos_Group_Id,
                                          Bb_Paygate_Config_Store_Id,
                                          CreditCardProcessingMethodId,
                                          PaymentExpressGroupAccountId,
                                          EmvDeviceIdSuffix
                                  FROM    Envision.Pos",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsPos();

                                if (!r.IsDBNull(0)) item.Pos_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.ProfitCenter_Id = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Name = r.GetString(2);
                                if (!r.IsDBNull(3)) item.PosType = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.Pos_Group_Id = r.GetInt32(4);
                                if (!r.IsDBNull(5)) item.Bb_Paygate_Config_Store_Id = r.GetInt32(5);
                                if (!r.IsDBNull(6)) item.CreditCardProcessingMethodId = r.GetInt32(6);
                                if (!r.IsDBNull(7)) item.PaymentExpressGroupAccountId = r.GetInt32(7);
                                if (!r.IsDBNull(8)) item.EmvDeviceIdSuffix = r.GetString(8);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Get the profit center the device belongs to from the device guid.
        /// </summary>
        /// <param name="deviceGuid">The unique identifier for the device.</param>
        /// <returns>The id (int) of the profit center</returns>
        public override int ProfitCenterFromDeviceGuidGet(string deviceGuid)
        {
            Guard.IsNotNullOrWhiteSpace(deviceGuid, "deviceGuid");

            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = @"
                        SELECT      ProfitCenter_Id
                        FROM        POS P
                        INNER JOIN  Originator DEV on DEV.OriginatorId = P.OriginatorId
                        WHERE       DEV.OriginatorGuid = :pValue",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        cmd.Parameters.Add(
                            new OracleParameter
                            {
                                ParameterName = "pValue",
                                OracleDbType = OracleDbType.Varchar2,
                                Size = 36,
                                Value = new Guid(deviceGuid).ToString("D"),
                                Direction = ParameterDirection.Input
                            });

                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                if (!r.IsDBNull(0))
                                {
                                    return r.GetInt32(0);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            throw new WebApiException(deviceGuid, $"DeviceGuid {deviceGuid} was not found in the resource layer or no appropriate association has been made to a POS entity.", HttpStatusCode.BadRequest);
        }

        /// <inheritdoc />
        public override void PosDelete(int posId)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "PosFunctions.PosDelete", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPos_Id", OracleDbType = OracleDbType.Int32, Size = 0, Direction = ParameterDirection.Input, Value = posId });
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }


        /// <summary>
        /// Get pos properties for a specified guid (or null for all).
        /// </summary>
        /// <param name="posGuid">the unique identifier for the pos (null for all pos properties).</param>
        /// <returns>A list of pos properties.</returns>
        public override List<PosProperties> PosPropertiesGet(string posGuid = null)
        {
            var list = new List<PosProperties>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "PosFunctions.PosGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.AddInParam("pPosGuid", posGuid);
                        cmd.AddOutParam("pList", OracleDbType.RefCursor, 0);
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var posId = !r.IsDBNull(0) ? r.GetInt32(0) : -1;
                                var profitCenterId = !r.IsDBNull(1) ? r.GetInt32(1) : -1;
                                var posName = !r.IsDBNull(2) ? r.GetString(2) : string.Empty;
                                var posType = !r.IsDBNull(3) ? r.GetInt32(3) : -1;
                                var posGroupId = !r.IsDBNull(4) ? r.GetInt32(4) : new int?();
                                var bbPaygateConfigStoreId = !r.IsDBNull(5) ? r.GetInt32(5) : new int?();
                                var creditCardProcessingMethodId = !r.IsDBNull(6) ? r.GetInt32(6) : new int?();
                                var paymentExpressGroupAccountId = !r.IsDBNull(7) ? r.GetInt32(7) : new int?();
                                var emvSuffix = !r.IsDBNull(8) ? r.GetString(8) : string.Empty;
                                var posDeviceId = !r.IsDBNull(9) ? new int?(r.GetInt32(9)) : null;
                                var posGuidTableValue = !r.IsDBNull(10) ? r.GetString(10) : string.Empty;
                                var magstripeEnabled = !r.IsDBNull(11) && Formatting.TfStringToBool(r.GetString(11));
                                var feliCaEnabled = !r.IsDBNull(12) && Formatting.TfStringToBool(r.GetString(12));
                                var mifareClassicEnabled = !r.IsDBNull(13) && Formatting.TfStringToBool(r.GetString(13));
                                var mifareDesFireEnabled = !r.IsDBNull(14) && Formatting.TfStringToBool(r.GetString(14));
                                var mobileEnabled = !r.IsDBNull(15) && Formatting.TfStringToBool(r.GetString(15));
                                var biometricEnabled = !r.IsDBNull(16) && Formatting.TfStringToBool(r.GetString(16));
                                var emvStation = !r.IsDBNull(17) ? r.GetString(17) : string.Empty;
                                var settingsUpdateTime = !r.IsDBNull(18) ? r.GetTimeSpan(18) : new TimeSpan(2, 0, 0);

                                var device = new PosProperties
                                {
                                    PosId = posId,
                                    ProfitCenterId = profitCenterId,
                                    PosName = posName,
                                    PosGroupId = posGroupId,
                                    PosType = posType,
                                    BbPaygateStoreId = bbPaygateConfigStoreId,
                                    CreditCardProcessingMethodId = creditCardProcessingMethodId,
                                    PaymentExpressGroupAccountId = paymentExpressGroupAccountId,
                                    EmvDeviceSuffix = emvSuffix,
                                    EmvStation = emvStation,
                                    PosDeviceid = posDeviceId,
                                    PosGuid = posGuidTableValue,
                                    PosCredentialSettings = new PosCredentialSettings()
                                    {
                                        MagstripeEnabled = magstripeEnabled,
                                        FeliCaEnabled = feliCaEnabled,
                                        MifareClassicEnabled = mifareClassicEnabled,
                                        MifareDesFireEnabled = mifareDesFireEnabled,
                                        MobileEnabled = mobileEnabled,
                                        BiometricEnabled = biometricEnabled
                                    },
                                    SettingsUpdateTime = settingsUpdateTime
                                };
                                list.Add(device);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <summary>
        /// Set pos properties for a specified guid (null to create).
        /// </summary>
        /// <param name="request"></param>
        public override PosProperties PosPropertiesCreate(PosPropertiesPostRequest request)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "PosFunctions.PosSet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.AddInParam("pPos_Id", request.PosId.Equals(0) ? new int?() : request.PosId);
                        cmd.AddInParam("pProfitCenter_Id", request.ProfitCenterId);
                        cmd.AddInParam("pName", request.PosName);
                        cmd.AddInParam("pPosType", request.PosType);
                        cmd.AddInParam("pPos_Group_Id", request.PosGroupId);
                        cmd.AddInParam("pBb_Paygate_Config_Store_Id", request.BbPaygateStoreId);
                        cmd.AddInParam("pCreditCardProcessingMethodId", request.CreditCardProcessingMethodId);
                        cmd.AddInParam("pPaymentExpressGroupAccountId", request.PaymentExpressGroupAccountId);
                        cmd.AddInParam("pEmvDeviceIdSuffix", request.EmvDeviceSuffix);
                        cmd.AddInParam("pEmvHitStation", request.EmvStation);
                        cmd.AddInParam("pSettingsUpdateTime", request.SettingsUpdateTime ?? new TimeSpan(2, 00, 00));

                        cmd.AddInOutParam("pPosGuid", request.PosGuid, 36);

                        con.Open();
                        cmd.ExecuteNonQuery();

                        var posGuid = cmd.Parameters["pPosGuid"].Value.ToString() == "null" ? null : cmd.Parameters["pPosGuid"].Value.ToString();
                        if (request.PosCredentialSettings != null)
                        {
                            cmd.Parameters.Clear();
                            cmd.CommandText = "PosFunctions.PosCredentialSettingsSet";

                            cmd.AddInParam("pPosGuid", posGuid);
                            cmd.AddInParam("pMagstripeEnabled", Formatting.BooleanToTf(request.PosCredentialSettings.MagstripeEnabled));
                            cmd.AddInParam("pFelicaEnabled", Formatting.BooleanToTf(request.PosCredentialSettings.FeliCaEnabled));
                            cmd.AddInParam("pMifareClassicEnabled", Formatting.BooleanToTf(request.PosCredentialSettings.MifareClassicEnabled));
                            cmd.AddInParam("pMifareDesFireEnabled", Formatting.BooleanToTf(request.PosCredentialSettings.MifareDesFireEnabled));
                            cmd.AddInParam("pMobileEnabled", Formatting.BooleanToTf(request.PosCredentialSettings.MobileEnabled));
                            cmd.AddInParam("pBiometricEnabled", Formatting.BooleanToTf(request.PosCredentialSettings.BiometricEnabled));

                            cmd.ExecuteNonQuery();
                        }

                        return PosPropertiesGet(posGuid).FirstOrDefault();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Get the pos guid from the originator guid.
        /// </summary>
        /// <param name="originatorGuid">The unique identifier of the originator.</param>
        /// <returns>The pos guid for the associated pos.</returns>
        public override Guid PosGuidFromOriginatorGuidGet(Guid originatorGuid)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = @"
                        SELECT      POS.PosGuid
                        FROM        POS
                        INNER JOIN  Originator ORG ON POS.OriginatorId = ORG.OriginatorId
                        WHERE       ORG.OriginatorGuid = :pValue",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        cmd.Parameters.Add(
                            new OracleParameter
                            {
                                ParameterName = "pValue",
                                OracleDbType = OracleDbType.Varchar2,
                                Size = 36,
                                Value = originatorGuid.ToString("D"),
                                Direction = ParameterDirection.Input
                            });

                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                if (r.IsDBNull(0)) continue;

                                string posGuid = r.GetString(0);
                                return new Guid(posGuid);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            var message = $"Originator identifier '{originatorGuid:D}' was not found in the resource layer or no appropriate association has been made to a POS entity.";
            throw new ResourceLayerException(originatorGuid.ToString("D"), message);
        }

        /// <summary>
        /// Get the pos id from the originator guid.
        /// </summary>
        /// <param name="deviceGuid">The unique identifier of the originator.</param>
        /// <returns>The numerical identifier for the associated pos.</returns>
        public override int PosIdFromOriginatorGuidGet(string deviceGuid)
        {
            Guard.IsNotNullOrWhiteSpace(deviceGuid, "deviceGuid");

            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = @"
                        SELECT      Pos_Id
                        FROM        POS P
                        INNER JOIN  Originator DEV on DEV.OriginatorId = P.OriginatorId
                        WHERE       DEV.OriginatorGuid = :pValue",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        cmd.Parameters.Add(
                            new OracleParameter
                            {
                                ParameterName = "pValue",
                                OracleDbType = OracleDbType.Varchar2,
                                Size = 36,
                                Value = new Guid(deviceGuid).ToString("D"),
                                Direction = ParameterDirection.Input
                            });

                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                if (!r.IsDBNull(0))
                                {
                                    return r.GetInt32(0);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            var message = $"Originator identifier '{deviceGuid}' was not found in the resource layer or no appropriate association has been made to a POS entity.";
            throw new ResourceLayerException(deviceGuid, message);
        }

        /// <summary>
        /// Get the pos id from the originator guid.
        /// </summary>
        /// <param name="originatorGuid">The unique identifier of the originator.</param>
        /// <returns>The numerical identifier for the associated pos.  Null is returned if no pos could be found in the resource layer or no appropriate association has been made to a POS entity.</returns>
        public override int? PosIdFromOriginatorGuidGet(Guid originatorGuid)
        {
            int? posId = null;

            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = @"
                        SELECT      Pos_Id
                        FROM        POS P
                        INNER JOIN  Originator DEV on DEV.OriginatorId = P.OriginatorId
                        WHERE       DEV.OriginatorGuid = :pValue",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        cmd.Parameters.Add(
                            new OracleParameter
                            {
                                ParameterName = "pValue",
                                OracleDbType = OracleDbType.Varchar2,
                                Size = 36,
                                Value = originatorGuid.ToString("D"),
                                Direction = ParameterDirection.Input
                            });

                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                if (!r.IsDBNull(0))
                                {
                                    posId = r.GetInt32(0);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return posId;
        }

        /// <summary>
        /// Get the OriginatorTypeId from the originator guid.
        /// </summary>
        /// <param name="deviceGuid">The unique identifier of the originator.</param>
        /// <returns>The numerical OriginatorTypeId.</returns>
        public override int OriginatorTypeIdFromOriginatorGuidGet(string deviceGuid)
        {
            Guard.IsNotNullOrWhiteSpace(deviceGuid, "deviceGuid");

            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = @"
                        SELECT      OriginatorTypeId
                        FROM        Originator DEV
                        WHERE       upper(DEV.OriginatorGuid) = upper(:pValue)",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        cmd.Parameters.Add(
                            new OracleParameter
                            {
                                ParameterName = "pValue",
                                OracleDbType = OracleDbType.Varchar2,
                                Size = 36,
                                Value = new Guid(deviceGuid).ToString("D"),
                                Direction = ParameterDirection.Input
                            });

                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                if (!r.IsDBNull(0))
                                {
                                    return r.GetInt32(0);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            var message = $"Originator identifier '{deviceGuid}' was not found in the resource layer.";
            throw new ResourceLayerException(deviceGuid, message);
        }

        /// <inheritdoc />
        public override PosGuidGetResponse PosForWorkstationGetOrCreate(string workstationName, int profitCenterId)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "PosFunctions.PosForWorkstationGetOrCreate", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pWorkStationName", OracleDbType = OracleDbType.Varchar2, Size = 255, Direction = ParameterDirection.Input, Value = workstationName });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pProfitCenterId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = profitCenterId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pWorkStationID", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPosGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPosID", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPosName", OracleDbType = OracleDbType.Varchar2, Size = 30, Direction = ParameterDirection.Output });
                        con.Open();

                        cmd.ExecuteNonQuery();
                        var posGuid = cmd.Parameters["pPosGuid"].Value?.ToString();
                        return new PosGuidGetResponse()
                        {
                            WorkstationId = TryGetIntValue(cmd.Parameters["pWorkStationID"], 0),
                            WorkstationName = workstationName,
                            ProfitCenterId = profitCenterId,
                            PosGuid = !string.IsNullOrEmpty(posGuid) ? Guid.Parse(posGuid) : Guid.Empty,
                            PosId = TryGetIntValue(cmd.Parameters["pPosID"], 0),
                            PosName = cmd.Parameters["pPosName"].Value?.ToString()
                    };
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        #endregion

        #region Pos_Group

        /// <inheritdoc />
        public override List<TsPos_Group> PosGroupArchivesGet(ConnectionInfo connection = null)
        {
            var list = new List<TsPos_Group>();

            using (var con = connection == null ? OracleConnectionGet(ConnectionString) : OracleConnectionGet(connection))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = @"SELECT  Pos_Group_Id,
                                          Merchant_Id,
                                          Name,
                                          Parent_Pos_Group_Id
                                  FROM    Envision.Pos_Group",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsPos_Group();

                                if (!r.IsDBNull(0)) item.Pos_Group_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Merchant_Id = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Name = r.GetString(2);
                                if (!r.IsDBNull(3)) item.Parent_Pos_Group_Id = r.GetInt32(3);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <inheritdoc />
        public override List<TsPosGroup> PosGroupGet(int? id = null)
        {
            var list = new List<TsPosGroup>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "PosFunctions.PosGroupGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPosGroupId", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = id });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var posGroupId = !r.IsDBNull(0) ? r.GetInt32(0) : -1;
                                var merchantId = !r.IsDBNull(1) ? r.GetInt32(1) : -1;
                                var name = !r.IsDBNull(2) ? r.GetString(2) : string.Empty;
                                var parentPosGroupId = !r.IsDBNull(3) ? r.GetInt32(3) : -1;

                                var posGroup = new TsPosGroup()
                                {

                                    PosGroupId = posGroupId,
                                    MerchantId = merchantId,
                                    Name = name,
                                    ParentGroupId = parentPosGroupId
                                };
                                list.Add(posGroup);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        #endregion

        #region Pos_Laundry_Machine

        /// <summary>
        /// This method will return a list of records for the PosLaundryMachine objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsPos_Laundry_Machine> PosLaundryMachineArchivesGet(ConnectionInfo connection)
        {
            var list = new List<TsPos_Laundry_Machine>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = @"SELECT  MachineNum,
                                          Pos_Id,
                                          Stacked_With_Enabled,
                                          Stacked_With_MachineNum,
                                          Machine_Enabled,
                                          Laundry_Mach_Typ_Definition_Id
                                  FROM    Envision.Pos_Laundry_Machine",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsPos_Laundry_Machine();

                                if (!r.IsDBNull(0)) item.MachineNum = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Pos_Id = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Stacked_With_Enabled = r.GetString(2);
                                if (!r.IsDBNull(3)) item.Stacked_With_MachineNum = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.Machine_Enabled = r.GetString(4);
                                if (!r.IsDBNull(5)) item.Laundry_Mach_Typ_Definition_Id = r.GetInt32(5);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Pos_Tt

        /// <inheritdoc />
        public override List<PosTt> PosTtListGet(string posGuid = null)
        {
            var list = new List<PosTt>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "PosFunctions.PosTtGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPosGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = posGuid });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var posId = !r.IsDBNull(0) ? r.GetInt32(0) : -1;
                                var useCashDrawer = !r.IsDBNull(1) ? r.GetString(1) : string.Empty;
                                var usePrinter = !r.IsDBNull(2) ? r.GetString(2) : string.Empty;
                                var turnstileRelayEnabled = !r.IsDBNull(3) ? r.GetString(3) : string.Empty;
                                var posOptionId = !r.IsDBNull(4) ? r.GetInt32(4) : new int?();
                                var posTtSetupId = !r.IsDBNull(5) ? r.GetInt32(5) : new int?();
                                var cardUtilityEnabled = !r.IsDBNull(6) ? r.GetString(6) : string.Empty;
                                var terminalType = !r.IsDBNull(7) ? r.GetInt32(7) : -1;

                                var device = new PosTt
                                {
                                    Pos_Id = posId,
                                    Use_Cash_Drawer = useCashDrawer,
                                    Use_Printer = usePrinter,
                                    TurnstileRelay_Enabled = turnstileRelayEnabled,
                                    Pos_Option_Id = posOptionId,
                                    Pos_Tt_Setup_Id = posTtSetupId,
                                    Card_Utility_Enabled = cardUtilityEnabled,
                                    Terminal_Type = terminalType
                                };
                                list.Add(device);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <inheritdoc />
        public override PosTt PosTtCreate(PosTtPostRequest request)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "PosFunctions.PosTtSet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPos_Id", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = request.Properties.Pos_Id });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pUse_Cash_Drawer", OracleDbType = OracleDbType.Char, Size = 1, Direction = ParameterDirection.Input, Value = request.Properties.Use_Cash_Drawer });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pUse_Printer", OracleDbType = OracleDbType.Char, Size = 1, Direction = ParameterDirection.Input, Value = request.Properties.Use_Printer });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pTurnstileRelay_Enabled", OracleDbType = OracleDbType.Char, Size = 1, Direction = ParameterDirection.Input, Value = request.Properties.TurnstileRelay_Enabled });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPos_Option_Id", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = request.Properties.Pos_Option_Id });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPos_Tt_Setup_Id", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = request.Properties.Pos_Tt_Setup_Id });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCard_Utility_Enabled", OracleDbType = OracleDbType.Char, Size = 1, Direction = ParameterDirection.Input, Value = request.Properties.Card_Utility_Enabled });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pTerminal_Type", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = request.Properties.Terminal_Type });

                        con.Open();
                        cmd.ExecuteNonQuery();
                        
                        var list = PosTtListGet(request.PosGuid);
                        return list.FirstOrDefault();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// This method will return a list of PosTtSetup records
        /// </summary>
        /// <param name="posTtSetupId">A filtering parameter</param>
        /// <returns></returns>
        public override List<PosTtSetup> PosTtSetupListGet(int? posTtSetupId = null)
        {
            var list = new List<PosTtSetup>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "PosFunctions.PosTtSetupGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPosTtSetupId", OracleDbType = OracleDbType.Int32, Direction = ParameterDirection.Input, Value = posTtSetupId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new PosTtSetup();

                                if (!r.IsDBNull(0)) item.Pos_Tt_Setup_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Merchant_Id = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Name = r.GetString(2);
                                if (!r.IsDBNull(3)) item.Data_Output_Send_On_Approve = r.GetString(3);
                                if (!r.IsDBNull(4)) item.Data_Output_Send_On_Deny = r.GetString(4);
                                if (!r.IsDBNull(5)) item.Data_Output_Data_To_Send_Type = r.GetInt32(5);
                                if (!r.IsDBNull(6)) item.Data_Output_Data_To_Send_Id = r.GetInt32(6);
                                if (!r.IsDBNull(7)) item.Data_Output_Send_Data_Offline = r.GetString(7);
                                if (!r.IsDBNull(8)) item.Data_Output_Serial_Baud_Type = r.GetInt32(8);
                                if (!r.IsDBNull(9)) item.Data_Output_Serial_Baud_Id = r.GetInt32(9);
                                if (!r.IsDBNull(10)) item.Data_Output_Serial_Bits_Type = r.GetInt32(10);
                                if (!r.IsDBNull(11)) item.Data_Output_Serial_Bits_Id = r.GetInt32(11);
                                if (!r.IsDBNull(12)) item.Data_Output_Serial_Stop_Type = r.GetInt32(12);
                                if (!r.IsDBNull(13)) item.Data_Output_Serial_Stop_Id = r.GetInt32(13);
                                if (!r.IsDBNull(14)) item.Data_Output_Serial_Parity_Type = r.GetInt32(14);
                                if (!r.IsDBNull(15)) item.Data_Output_Serial_Parity_Id = r.GetInt32(15);
                                if (!r.IsDBNull(16)) item.Data_Output_Prefix_Type = r.GetInt32(16);
                                if (!r.IsDBNull(17)) item.Data_Output_Prefix_Id = r.GetInt32(17);
                                if (!r.IsDBNull(18)) item.Data_Output_Suffix_Type = r.GetInt32(18);
                                if (!r.IsDBNull(19)) item.Data_Output_Suffix_Id = r.GetInt32(19);
                                if (!r.IsDBNull(20)) item.Data_Output_Padding_Type = r.GetInt32(20);
                                if (!r.IsDBNull(21)) item.Data_Output_Padding_Id = r.GetInt32(21);
                                if (!r.IsDBNull(22)) item.Data_Output_Pad_Length = r.GetInt32(22);
                                if (!r.IsDBNull(23)) item.Data_Output_Pad_Value = r.GetString(23);
                                if (!r.IsDBNull(24)) item.Data_Output_Cust_Def_Fld_Id = r.GetInt32(24);
                                if (!r.IsDBNull(25)) item.Data_Output_Cust_Def_Grp_Id = r.GetInt32(25);
                                if (!r.IsDBNull(26)) item.Data_Output_Cdf_Bool_True = r.GetString(26);
                                if (!r.IsDBNull(27)) item.Data_Output_Cdf_Bool_False = r.GetString(27);
                                if (!r.IsDBNull(28)) item.Data_Output_Cdf_Dol_Prefix = r.GetString(28);
                                if (!r.IsDBNull(29)) item.Data_Output_Cdf_Dol_Decimal = r.GetString(29);
                                if (!r.IsDBNull(30)) item.Data_Output_Cdf_Text_Len = r.GetInt32(30);
                                if (!r.IsDBNull(31)) item.Data_Output_LTrim_CustNumber = r.GetString(31);

                                list.Add(item);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }
        #endregion

        #region Pos_Option

        /// <inheritdoc />
        public override List<PosOption> PosOptionGet(string posGuid = null)
        {
            var list = new List<PosOption>();

            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "PosFunctions.PosOptionGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPosGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = posGuid });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new PosOption();

                                if (!r.IsDBNull(0)) item.Pos_Option_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Merchant_Id = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Name = r.GetString(2);
                                if (!r.IsDBNull(3)) item.Receipt_Header_Active = r.GetString(3);
                                if (!r.IsDBNull(4)) item.Receipt_Header1 = r.GetString(4);
                                if (!r.IsDBNull(5)) item.Receipt_Header2 = r.GetString(5);
                                if (!r.IsDBNull(6)) item.Receipt_Header3 = r.GetString(6);
                                if (!r.IsDBNull(7)) item.Receipt_Header4 = r.GetString(7);
                                if (!r.IsDBNull(8)) item.Receipt_Coupon_Active = r.GetString(8);
                                if (!r.IsDBNull(9)) item.Receipt_Coupon1 = r.GetString(9);
                                if (!r.IsDBNull(10)) item.Receipt_Coupon2 = r.GetString(10);
                                if (!r.IsDBNull(11)) item.Receipt_Coupon3 = r.GetString(11);
                                if (!r.IsDBNull(12)) item.Receipt_Coupon4 = r.GetString(12);
                                if (!r.IsDBNull(13)) item.Receipt_Footer_Active = r.GetString(13);
                                if (!r.IsDBNull(14)) item.Receipt_Footer1 = r.GetString(14);
                                if (!r.IsDBNull(15)) item.Receipt_Footer2 = r.GetString(15);
                                if (!r.IsDBNull(16)) item.Receipt_Footer3 = r.GetString(16);
                                if (!r.IsDBNull(17)) item.Receipt_Footer4 = r.GetString(17);
                                if (!r.IsDBNull(18)) item.Stored_Value_Cash_Entry_Type = r.GetInt32(18);
                                if (!r.IsDBNull(19)) item.Force_Close_Cash_Drawer = r.GetString(19);
                                if (!r.IsDBNull(20)) item.Allow_Subtotal_Exit = r.GetString(20);
                                if (!r.IsDBNull(21)) item.Board_Auto_Continue = r.GetString(21);
                                if (!r.IsDBNull(22)) item.Cardnum_Manual_Entry_Enabled = r.GetString(22);
                                if (!r.IsDBNull(23)) item.Limit_Transactions = r.GetString(23);
                                if (!r.IsDBNull(24)) item.Limit_Transactions_Amount = r.GetInt32(24);
                                if (!r.IsDBNull(25)) item.Limit_Cash = r.GetString(25);
                                if (!r.IsDBNull(26)) item.Limit_Cash_Amount = r.GetInt32(26);
                                if (!r.IsDBNull(27)) item.Auto_Logout = r.GetString(27);
                                if (!r.IsDBNull(28)) item.Allow_Offline_Transactions = r.GetString(28);
                                if (!r.IsDBNull(29)) item.Allow_Offline_Payments = r.GetString(29);
                                if (!r.IsDBNull(30)) item.Allow_Payments = r.GetString(30);
                                if (!r.IsDBNull(31)) item.Freeze_Customer_Account = r.GetString(31);
                                if (!r.IsDBNull(32)) item.Balance_Check = r.GetString(32);
                                if (!r.IsDBNull(33)) item.Split_Tender = r.GetString(33);
                                if (!r.IsDBNull(34)) item.Display_Meals_Served = r.GetString(34);
                                if (!r.IsDBNull(35)) item.Allow_Check_Attendance = r.GetString(35);
                                if (!r.IsDBNull(36)) item.Allow_Reverse = r.GetString(36);
                                if (!r.IsDBNull(37)) item.Allow_Event_Transaction = r.GetString(37);
                                if (!r.IsDBNull(38)) item.Rcpt_Prn_Agree_Credit_Card = r.GetString(38);
                                if (!r.IsDBNull(39)) item.Receipt_Print_Audit = r.GetString(39);
                                if (!r.IsDBNull(40)) item.Receipt_Print_Customer_Name = r.GetString(40);
                                if (!r.IsDBNull(41)) item.Receipt_Print_Customer_Number = r.GetString(41);
                                if (!r.IsDBNull(42)) item.Receipt_Print_Customer_Balance = r.GetString(42);
                                if (!r.IsDBNull(43)) item.Receipt_Print_Card_Number = r.GetString(43);
                                if (!r.IsDBNull(44)) item.Receipt_Print_Dollars = r.GetString(44);
                                if (!r.IsDBNull(45)) item.Receipt_Print_Trans_Comment = r.GetString(45);
                                if (!r.IsDBNull(46)) item.Receipt_Print_Balance_Below = r.GetString(46);
                                if (!r.IsDBNull(47)) item.Receipt_Balance_Below_Amount = r.GetInt32(47);
                                if (!r.IsDBNull(48)) item.Force_Print_Cash = r.GetString(48);
                                if (!r.IsDBNull(49)) item.Force_Print_Check = r.GetString(49);
                                if (!r.IsDBNull(50)) item.Force_Prnt_Stored_Value_Trans = r.GetString(50);
                                if (!r.IsDBNull(51)) item.Force_Print_Cash_Equivalent = r.GetString(51);
                                if (!r.IsDBNull(52)) item.Force_Print_Paid_Out = r.GetString(52);
                                if (!r.IsDBNull(53)) item.Force_Print_Freeze_Account = r.GetString(53);
                                if (!r.IsDBNull(54)) item.Force_Print_Overrings = r.GetString(54);
                                if (!r.IsDBNull(55)) item.Force_Prnt_Stored_Val_Pymnt = r.GetString(55);
                                if (!r.IsDBNull(56)) item.Force_Print_Board = r.GetString(56);
                                if (!r.IsDBNull(57)) item.Force_Print_Event = r.GetString(57);
                                if (!r.IsDBNull(58)) item.Printer_On_By_Default = r.GetString(58);
                                if (!r.IsDBNull(59)) item.Message_Delay = r.GetInt32(59);
                                if (!r.IsDBNull(60)) item.Confirmation_Beep = r.GetString(60);
                                if (!r.IsDBNull(61)) item.Crdt_Crd_Offln_Call_Cntr_Auth = r.GetString(61);
                                if (!r.IsDBNull(62)) item.Meals_Left_By_Type = r.GetInt32(62);
                                if (!r.IsDBNull(63)) item.Discount_Key_Type1 = r.GetInt32(63);
                                if (!r.IsDBNull(64)) item.Discount_Key_Amount1 = r.GetInt32(64);
                                if (!r.IsDBNull(65)) item.Discount_Key_Type2 = r.GetInt32(65);
                                if (!r.IsDBNull(66)) item.Discount_Key_Amount2 = r.GetInt32(66);
                                if (!r.IsDBNull(67)) item.Discount_Key_Type3 = r.GetInt32(67);
                                if (!r.IsDBNull(68)) item.Discount_Key_Amount3 = r.GetInt32(68);
                                if (!r.IsDBNull(69)) item.Discount_Key_Type4 = r.GetInt32(69);
                                if (!r.IsDBNull(70)) item.Discount_Key_Amount4 = r.GetInt32(70);
                                if (!r.IsDBNull(71)) item.Surcharge_Type1 = r.GetInt32(71);
                                if (!r.IsDBNull(72)) item.Surcharge_Amount1 = r.GetInt32(72);
                                if (!r.IsDBNull(73)) item.Surcharge_Type2 = r.GetInt32(73);
                                if (!r.IsDBNull(74)) item.Surcharge_Amount2 = r.GetInt32(74);
                                if (!r.IsDBNull(75)) item.Tax_Type1 = r.GetInt32(75);
                                if (!r.IsDBNull(76)) item.Tax_Amount1 = r.GetInt32(76);
                                if (!r.IsDBNull(77)) item.Tax_Type2 = r.GetInt32(77);
                                if (!r.IsDBNull(78)) item.Tax_Amount2 = r.GetInt32(78);
                                if (!r.IsDBNull(79)) item.Product_Mask = r.GetString(79);
                                if (!r.IsDBNull(80)) item.Screen_Saver = r.GetString(80);
                                if (!r.IsDBNull(81)) item.Screen_Saver_Timeout = r.GetInt32(81);
                                if (!r.IsDBNull(82)) item.Pole_Screen_Saver = r.GetString(82);
                                if (!r.IsDBNull(83)) item.Pole_Display_Timeout = r.GetInt32(83);
                                if (!r.IsDBNull(84)) item.Default_Board_Meal_Type_Id = r.GetInt32(84);
                                if (!r.IsDBNull(85)) item.Dflt_Equiv_Board_Meal_Type_Id = r.GetInt32(85);
                                if (!r.IsDBNull(86)) item.Default_Boardplan_Id = r.GetInt32(86);
                                if (!r.IsDBNull(87)) item.Receipt_Group_Product = r.GetString(87);
                                if (!r.IsDBNull(88)) item.Product_Id = r.GetInt32(88);
                                if (!r.IsDBNull(89)) item.Allow_No_Tax = r.GetString(89);
                                if (!r.IsDBNull(90)) item.Allow_Count_Display = r.GetString(90);
                                if (!r.IsDBNull(91)) item.CashierLoginType = r.GetInt32(91);
                                if (!r.IsDBNull(92)) item.TtDiscountType1 = r.GetInt32(92);
                                if (!r.IsDBNull(93)) item.TtDiscountType2 = r.GetInt32(93);
                                if (!r.IsDBNull(94)) item.TtDiscountAmount1 = r.GetInt32(94);
                                if (!r.IsDBNull(95)) item.TtDiscountAmount2 = r.GetInt32(95);
                                if (!r.IsDBNull(96)) item.TtTaxType1 = r.GetInt32(96);
                                if (!r.IsDBNull(97)) item.TtTaxType2 = r.GetInt32(97);
                                if (!r.IsDBNull(98)) item.TtTaxAmount1 = r.GetInt32(98);
                                if (!r.IsDBNull(99)) item.TtTaxAmount2 = r.GetInt32(99);
                                if (!r.IsDBNull(100)) item.TtSurchargeType1 = r.GetInt32(100);
                                if (!r.IsDBNull(101)) item.TtSurchargeType2 = r.GetInt32(101);
                                if (!r.IsDBNull(102)) item.TtSurchargeAmount1 = r.GetInt32(102);
                                if (!r.IsDBNull(103)) item.TtSurchargeAmount2 = r.GetInt32(103);
                                if (!r.IsDBNull(104)) item.Print_Board_Cash_Value_Limit = r.GetString(104);

                                list.Add(item);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <inheritdoc />
        public override PosOptionDeviceSettings PosOptionDeviceSettingsGet(Guid posGuid)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "PosFunctions.PosOptionDeviceSettingsGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPosGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = posGuid.ToString("D") });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            if (r.Read())
                            {
                                var item = new PosOptionDeviceSettings()
                                {
                                    DefaultValues = new PosOptionDefaultValues(),
                                    Constraints = new PosOptionConstraints(),
                                    FeatureAvailability = new PosOptionFeatureAvailability(),
                                    CashDrawerProperties = new PosOptionCashDrawerProperties(),
                                    PrinterProperties = new PosOptionPrinterProperties(),
                                    OverrideKeys = new PosOptionOverrideKeys()
                                };
                                if (!r.IsDBNull(0)) item.CashDrawerProperties.UseCashDrawer = Formatting.TfStringToBool(r.GetString(0));
                                if (!r.IsDBNull(1)) item.PrinterProperties.UsePrinter = Formatting.TfStringToBool(r.GetString(1));
                                if (!r.IsDBNull(2)) item.FeatureAvailability.TurnstilerelayEnabled = Formatting.TfStringToBool(r.GetString(2));
                                if (!r.IsDBNull(3)) item.FeatureAvailability.CardUtilityEnabled = Formatting.TfStringToBool(r.GetString(3));
                                if (!r.IsDBNull(4)) item.Name = r.GetString(4);
                                if (!r.IsDBNull(5)) item.PrinterProperties.Header.Active = Formatting.TfStringToBool(r.GetString(5));
                                if (!r.IsDBNull(6)) item.PrinterProperties.Header.Line1 = r.GetString(6);
                                if (!r.IsDBNull(7)) item.PrinterProperties.Header.Line2 = r.GetString(7);
                                if (!r.IsDBNull(8)) item.PrinterProperties.Header.Line3 = r.GetString(8);
                                if (!r.IsDBNull(9)) item.PrinterProperties.Header.Line4 = r.GetString(9);
                                if (!r.IsDBNull(10)) item.PrinterProperties.Coupon.Active = Formatting.TfStringToBool(r.GetString(10));
                                if (!r.IsDBNull(11)) item.PrinterProperties.Coupon.Line1 = r.GetString(11);
                                if (!r.IsDBNull(12)) item.PrinterProperties.Coupon.Line2 = r.GetString(12);
                                if (!r.IsDBNull(13)) item.PrinterProperties.Coupon.Line3 = r.GetString(13);
                                if (!r.IsDBNull(14)) item.PrinterProperties.Coupon.Line4 = r.GetString(14);
                                if (!r.IsDBNull(15)) item.PrinterProperties.Footer.Active = Formatting.TfStringToBool(r.GetString(15));
                                if (!r.IsDBNull(16)) item.PrinterProperties.Footer.Line1 = r.GetString(16);
                                if (!r.IsDBNull(17)) item.PrinterProperties.Footer.Line2 = r.GetString(17);
                                if (!r.IsDBNull(18)) item.PrinterProperties.Footer.Line3 = r.GetString(18);
                                if (!r.IsDBNull(19)) item.PrinterProperties.Footer.Line4 = r.GetString(19);
                                if (!r.IsDBNull(20)) item.Constraints.StoredValueCashEntryType = (PurchaseEntryType)r.GetInt32(20);
                                if (!r.IsDBNull(21)) item.CashDrawerProperties.ForceCloseCashDrawer = Formatting.TfStringToBool(r.GetString(21));
                                if (!r.IsDBNull(22)) item.Constraints.AllowSubtotalExit = Formatting.TfStringToBool(r.GetString(22));
                                if (!r.IsDBNull(23)) item.FeatureAvailability.BoardAutoContinue = Formatting.TfStringToBool(r.GetString(23));
                                if (!r.IsDBNull(24)) item.CardnumManualEntryEnabled = Formatting.TfStringToBool(r.GetString(24));
                                if (!r.IsDBNull(25)) item.Constraints.LimitTransactions = Formatting.TfStringToBool(r.GetString(25));
                                if (!r.IsDBNull(26) && item.Constraints.LimitTransactions) item.Constraints.LimitTransactionsAmount = r.GetDecimal(26) / 1000m;
                                if (!r.IsDBNull(27)) item.Constraints.LimitCash = Formatting.TfStringToBool(r.GetString(27));
                                if (!r.IsDBNull(28) && item.Constraints.LimitCash) item.Constraints.LimitCashAmount = r.GetDecimal(28) / 1000m;
                                if (!r.IsDBNull(29)) item.FeatureAvailability.AutoLogout = Formatting.TfStringToBool(r.GetString(29));
                                if (!r.IsDBNull(30)) item.FeatureAvailability.AllowOfflineTransactions = Formatting.TfStringToBool(r.GetString(30));
                                if (!r.IsDBNull(31)) item.FeatureAvailability.AllowOfflinePayments = Formatting.TfStringToBool(r.GetString(31));
                                if (!r.IsDBNull(32)) item.FeatureAvailability.AllowPayments = Formatting.TfStringToBool(r.GetString(32));
                                if (!r.IsDBNull(33)) item.FeatureAvailability.FreezeCustomerAccount = Formatting.TfStringToBool(r.GetString(33));
                                if (!r.IsDBNull(34)) item.FeatureAvailability.BalanceCheck = Formatting.TfStringToBool(r.GetString(34));
                                if (!r.IsDBNull(35)) item.FeatureAvailability.DisplayMealsServed = Formatting.TfStringToBool(r.GetString(35));
                                if (!r.IsDBNull(36)) item.FeatureAvailability.AllowCheckAttendance = Formatting.TfStringToBool(r.GetString(36));
                                if (!r.IsDBNull(37)) item.FeatureAvailability.AllowReverse = Formatting.TfStringToBool(r.GetString(37));
                                if (!r.IsDBNull(38)) item.FeatureAvailability.AllowEventTransaction = Formatting.TfStringToBool(r.GetString(38));
                                if (!r.IsDBNull(39)) item.PrinterProperties.ReceiptPrintFlags.ReceiptPrintAgreeCreditCard = Formatting.TfStringToBool(r.GetString(39));
                                if (!r.IsDBNull(40)) item.PrinterProperties.ReceiptPrintFlags.ReceiptPrintAudit = Formatting.TfStringToBool(r.GetString(40));
                                if (!r.IsDBNull(41)) item.PrinterProperties.ReceiptPrintFlags.ReceiptPrintCustomerName = Formatting.TfStringToBool(r.GetString(41));
                                if (!r.IsDBNull(42)) item.PrinterProperties.ReceiptPrintFlags.ReceiptPrintCustomerNumber = Formatting.TfStringToBool(r.GetString(42));
                                if (!r.IsDBNull(43)) item.PrinterProperties.ReceiptPrintFlags.ReceiptPrintCustomerBalance = Formatting.TfStringToBool(r.GetString(43));
                                if (!r.IsDBNull(44)) item.PrinterProperties.ReceiptPrintFlags.ReceiptPrintCardNumber = Formatting.TfStringToBool(r.GetString(44));
                                if (!r.IsDBNull(45)) item.PrinterProperties.ReceiptPrintFlags.ReceiptPrintDollars = Formatting.TfStringToBool(r.GetString(45));
                                if (!r.IsDBNull(46)) item.PrinterProperties.ReceiptPrintFlags.ReceiptPrintTransComment = Formatting.TfStringToBool(r.GetString(46));
                                if (!r.IsDBNull(47)) item.PrinterProperties.ReceiptPrintFlags.ReceiptPrintBalanceBelow = Formatting.TfStringToBool(r.GetString(47));
                                if (!r.IsDBNull(48) && item.PrinterProperties.ReceiptPrintFlags.ReceiptPrintBalanceBelow) item.PrinterProperties.ReceiptPrintFlags.ReceiptBalanceBelowAmount = r.GetDecimal(48) / 1000m;
                                if (!r.IsDBNull(49)) item.PrinterProperties.ForcePrintFlags.ForcePrintCash = Formatting.TfStringToBool(r.GetString(49));
                                if (!r.IsDBNull(50)) item.PrinterProperties.ForcePrintFlags.ForcePrintCheck = Formatting.TfStringToBool(r.GetString(50));
                                if (!r.IsDBNull(51)) item.PrinterProperties.ForcePrintFlags.ForcePrintStoredValueTrans = Formatting.TfStringToBool(r.GetString(51));
                                if (!r.IsDBNull(52)) item.PrinterProperties.ForcePrintFlags.ForcePrintCashEquivalent = Formatting.TfStringToBool(r.GetString(52));
                                if (!r.IsDBNull(53)) item.PrinterProperties.ForcePrintFlags.ForcePrintPaidOut = Formatting.TfStringToBool(r.GetString(53));
                                if (!r.IsDBNull(54)) item.PrinterProperties.ForcePrintFlags.ForcePrintFreezeAccount = Formatting.TfStringToBool(r.GetString(54));
                                if (!r.IsDBNull(55)) item.PrinterProperties.ForcePrintFlags.ForcePrintOverrings = Formatting.TfStringToBool(r.GetString(55));
                                if (!r.IsDBNull(56)) item.PrinterProperties.ForcePrintFlags.ForcePrintStoredValuePayment = Formatting.TfStringToBool(r.GetString(56));
                                if (!r.IsDBNull(57)) item.PrinterProperties.ForcePrintFlags.ForcePrintBoard = Formatting.TfStringToBool(r.GetString(57));
                                if (!r.IsDBNull(58)) item.PrinterProperties.ForcePrintFlags.ForcePrintEvent = Formatting.TfStringToBool(r.GetString(58));
                                if (!r.IsDBNull(59)) item.PrinterProperties.PrinterOnByDefault = Formatting.TfStringToBool(r.GetString(59));
                                if (!r.IsDBNull(60)) item.Constraints.MessageDelay = r.GetInt32(60);
                                if (!r.IsDBNull(61)) item.Constraints.ConfirmationBeep = Formatting.TfStringToBool(r.GetString(61));
                                if (!r.IsDBNull(62)) item.FeatureAvailability.CreditCardOfflineCallCenterAuth = Formatting.TfStringToBool(r.GetString(62));
                                if (!r.IsDBNull(63)) item.OverrideKeys.DiscountKey1.Type = (PaymentType)r.GetInt32(63);
                                if (!r.IsDBNull(64)) item.OverrideKeys.DiscountKey1.Amount = item.OverrideKeys.DiscountKey1.Type == PaymentType.Rate ? r.GetDecimal(64) / 100000m : r.GetDecimal(64) / 1000m;
                                if (!r.IsDBNull(65)) item.OverrideKeys.DiscountKey2.Type = (PaymentType)r.GetInt32(65);
                                if (!r.IsDBNull(66)) item.OverrideKeys.DiscountKey2.Amount = item.OverrideKeys.DiscountKey2.Type == PaymentType.Rate ? r.GetDecimal(66) / 100000m : r.GetDecimal(66) / 1000m;
                                if (!r.IsDBNull(67)) item.OverrideKeys.DiscountKey3.Type = (PaymentType)r.GetInt32(67);
                                if (!r.IsDBNull(68)) item.OverrideKeys.DiscountKey3.Amount = item.OverrideKeys.DiscountKey3.Type == PaymentType.Rate ? r.GetDecimal(68) / 100000m : r.GetDecimal(68) / 1000m;
                                if (!r.IsDBNull(69)) item.OverrideKeys.DiscountKey4.Type = (PaymentType)r.GetInt32(69);
                                if (!r.IsDBNull(70)) item.OverrideKeys.DiscountKey4.Amount = item.OverrideKeys.DiscountKey4.Type == PaymentType.Rate ? r.GetDecimal(70) / 100000m : r.GetDecimal(70) / 1000m;
                                if (!r.IsDBNull(71)) item.OverrideKeys.SurchargeKey1.Type = (PaymentType)r.GetInt32(71);
                                if (!r.IsDBNull(72)) item.OverrideKeys.SurchargeKey1.Amount = item.OverrideKeys.SurchargeKey1.Type == PaymentType.Rate ? r.GetDecimal(72) / 100000m : r.GetDecimal(72) / 1000m;
                                if (!r.IsDBNull(73)) item.OverrideKeys.SurchargeKey2.Type = (PaymentType)r.GetInt32(73);
                                if (!r.IsDBNull(74)) item.OverrideKeys.SurchargeKey2.Amount = item.OverrideKeys.SurchargeKey2.Type == PaymentType.Rate ? r.GetDecimal(74) / 100000m : r.GetDecimal(74) / 1000m;
                                if (!r.IsDBNull(75)) item.OverrideKeys.TaxKey1.Type = (PaymentType)r.GetInt32(75);
                                if (!r.IsDBNull(76)) item.OverrideKeys.TaxKey1.Amount = item.OverrideKeys.TaxKey1.Type == PaymentType.Rate ? r.GetDecimal(76) / 100000m : r.GetDecimal(76) / 1000m;
                                if (!r.IsDBNull(77)) item.OverrideKeys.TaxKey2.Type = (PaymentType)r.GetInt32(77);
                                if (!r.IsDBNull(78)) item.OverrideKeys.TaxKey2.Amount = item.OverrideKeys.TaxKey2.Type == PaymentType.Rate ? r.GetDecimal(78) / 100000m : r.GetDecimal(78) / 1000m;
                                if (!r.IsDBNull(79)) item.Constraints.ProductMask = r.GetString(79);
                                if (!r.IsDBNull(80)) item.FeatureAvailability.ScreenSaver = Formatting.TfStringToBool(r.GetString(80));
                                if (!r.IsDBNull(81)) item.FeatureAvailability.ScreenSaverTimeout = r.GetInt32(81);
                                if (!r.IsDBNull(82)) item.FeatureAvailability.PoleScreenSaver = Formatting.TfStringToBool(r.GetString(82));
                                if (!r.IsDBNull(83)) item.FeatureAvailability.PoleDisplayTimeout = r.GetInt32(83);
                                if (!r.IsDBNull(84)) item.DefaultValues.DefaultBoardMealTypeId = r.GetInt32(84);
                                if (!r.IsDBNull(85)) item.DefaultValues.DefaultEquivBoardMealTypeId = r.GetInt32(85);
                                if (!r.IsDBNull(86)) item.DefaultValues.DefaultBoardPlanId = r.GetInt32(86);
                                if (!r.IsDBNull(87)) item.PrinterProperties.ReceiptGroupProduct = Formatting.TfStringToBool(r.GetString(87));
                                if (!r.IsDBNull(88)) item.DefaultValues.ProductId = r.GetInt32(88);
                                if (!r.IsDBNull(89)) item.FeatureAvailability.AllowNoTax = Formatting.TfStringToBool(r.GetString(89));
                                if (!r.IsDBNull(90)) item.FeatureAvailability.AllowCountDisplay = Formatting.TfStringToBool(r.GetString(90));
                                if (!r.IsDBNull(91)) item.PrinterProperties.ReceiptPrintFlags.PrintBoardCashValueLimit = Formatting.TfStringToBool(r.GetString(91));

                                return item;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return null;
        }
        #endregion
    }
}