﻿using System;
using System.Collections.Generic;
using System.Data;
using BbTS.Core.Conversion;
using Oracle.ManagedDataAccess.Client;
using BbTS.Core;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.Object;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        /// <inheritdoc />
        public override List<ObjectDefFieldValue> ObjectFieldValueGet(int? objectId, int? definedFieldId)
        {
            var list = new List<ObjectDefFieldValue>();

            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "ObjectFunctions.ObjectFieldValueGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pObjectId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = objectId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pObjectDefinedFieldId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = definedFieldId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new ObjectDefFieldValue()
                                {
                                    ObjectId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    DefinedFieldId = !r.IsDBNull(1) ? r.GetInt32(1) : -1,
                                    Value = !r.IsDBNull(2) ? r.GetString(2) : null,
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <inheritdoc />
        public override void ObjectFieldValueDelete(int objectId, int definedFieldId)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "ObjectFunctions.ObjectFieldValueDelete", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pObjectId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = objectId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pObjectDefinedFieldId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = definedFieldId });

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Set object field value
        /// </summary>
        /// <param name="objectFieldValue">Object def field value to be set</param>
        public override void ObjectFieldValueSet(ObjectDefFieldValue objectFieldValue)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "ObjectFunctions.ObjectFieldValueSet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pObjectId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = objectFieldValue.ObjectId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pObjectDefinedFieldId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = objectFieldValue.DefinedFieldId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pValue", OracleDbType = OracleDbType.Varchar2, Size = 4000, Direction = ParameterDirection.Input, Value = objectFieldValue.Value });

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }
    }
}
