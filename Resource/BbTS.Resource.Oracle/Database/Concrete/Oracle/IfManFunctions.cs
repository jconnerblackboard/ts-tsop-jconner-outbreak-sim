﻿using System.Collections.Generic;
using System.Data;
using BbTS.Domain.Models.IfMan;
using BbTS.Domain.Models.System.Database;
using Oracle.ManagedDataAccess.Client;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        public override IEnumerable<Clone_Card> CloneCardsGet(ConnectionInfo connection, string source)
        {
            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = source, CommandType = CommandType.TableDirect })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                Clone_Card item = new Clone_Card();

                                if (!r.IsDBNull(0))     item.Record_Id              = r.GetInt32(0);
                                if (!r.IsDBNull(1))     item.B2M_Modify             = r.GetString(1);
                                if (!r.IsDBNull(2))     item.Person_Number          = r.GetString(2);
                                if (!r.IsDBNull(3))     item.Customer_Status        = r.GetString(3);
                                if (!r.IsDBNull(4))     item.Card_Number            = r.GetString(4);
                                if (!r.IsDBNull(5))     item.Card_Status            = r.GetString(5);
                                if (!r.IsDBNull(6))     item.Card_Format            = r.GetString(6);
                                if (!r.IsDBNull(7))     item.System_Active_Date     = r.GetDateTime(7);
                                if (!r.IsDBNull(8))     item.System_Cancel_Date     = r.GetDateTime(8);
                                if (!r.IsDBNull(9))     item.Open_Date              = r.GetDateTime(9);
                                if (!r.IsDBNull(10))    item.First_Name             = r.GetString(10);
                                if (!r.IsDBNull(11))    item.Middle_Name            = r.GetString(11);
                                if (!r.IsDBNull(12))    item.Last_Name              = r.GetString(12);
                                if (!r.IsDBNull(13))    item.Library_Id             = r.GetString(13);
                                if (!r.IsDBNull(14))    item.Social_Security_Number = r.GetString(14);
                                if (!r.IsDBNull(15))    item.Birth_Date             = r.GetDateTime(15);
                                if (!r.IsDBNull(16))    item.Local_Street_1         = r.GetString(16);
                                if (!r.IsDBNull(17))    item.Local_Street_2         = r.GetString(17);
                                if (!r.IsDBNull(18))    item.Local_City             = r.GetString(18);
                                if (!r.IsDBNull(19))    item.Local_State            = r.GetString(19);
                                if (!r.IsDBNull(20))    item.Local_Zipcode          = r.GetString(20);
                                if (!r.IsDBNull(21))    item.Local_Country          = r.GetString(21);
                                if (!r.IsDBNull(22))    item.Local_Phone            = r.GetString(22);
                                if (!r.IsDBNull(23))    item.Perm_Street_1          = r.GetString(23);
                                if (!r.IsDBNull(24))    item.Perm_Street_2          = r.GetString(24);
                                if (!r.IsDBNull(25))    item.Perm_City              = r.GetString(25);
                                if (!r.IsDBNull(26))    item.Perm_State             = r.GetString(26);
                                if (!r.IsDBNull(27))    item.Perm_Zipcode           = r.GetString(27);
                                if (!r.IsDBNull(28))    item.Perm_Country           = r.GetString(28);
                                if (!r.IsDBNull(29))    item.Perm_Phone             = r.GetString(29);
                                if (!r.IsDBNull(30))    item.Validation_Date        = r.GetDateTime(30);
                                if (!r.IsDBNull(31))    item.Sex                    = r.GetString(31);
                                if (!r.IsDBNull(32))    item.License                = r.GetString(32);
                                if (!r.IsDBNull(33))    item.Pin_Number             = r.GetString(33);
                                if (!r.IsDBNull(34))    item.CustomField1           = r.GetString(34);
                                if (!r.IsDBNull(35))    item.CustomField2           = r.GetString(35);
                                if (!r.IsDBNull(36))    item.CustomField3           = r.GetString(36);
                                if (!r.IsDBNull(37))    item.CustomField4           = r.GetString(37);
                                if (!r.IsDBNull(38))    item.CustomField5           = r.GetString(38);
                                if (!r.IsDBNull(39))    item.CustomField6           = r.GetString(39);
                                if (!r.IsDBNull(40))    item.CustomField7           = r.GetString(40);
                                if (!r.IsDBNull(41))    item.CustomField8           = r.GetString(41);
                                if (!r.IsDBNull(42))    item.CustomField9           = r.GetString(42);
                                if (!r.IsDBNull(43))    item.CustomField10          = r.GetString(43);
                                if (!r.IsDBNull(44))    item.CustomField11          = r.GetString(44);
                                if (!r.IsDBNull(45))    item.CustomField12          = r.GetString(45);
                                if (!r.IsDBNull(46))    item.CustomField13          = r.GetString(46);
                                if (!r.IsDBNull(47))    item.CustomField14          = r.GetString(47);
                                if (!r.IsDBNull(48))    item.CustomField15          = r.GetString(48);
                                if (!r.IsDBNull(49))    item.CustomField16          = r.GetString(49);
                                if (!r.IsDBNull(50))    item.CustomField17          = r.GetString(50);
                                if (!r.IsDBNull(51))    item.CustomField18          = r.GetString(51);
                                if (!r.IsDBNull(52))    item.CustomField19          = r.GetString(52);
                                if (!r.IsDBNull(53))    item.CustomField20          = r.GetString(53);
                                if (!r.IsDBNull(54))    item.CustomField21          = r.GetString(54);
                                if (!r.IsDBNull(55))    item.CustomField22          = r.GetString(55);
                                if (!r.IsDBNull(56))    item.CustomField23          = r.GetString(56);
                                if (!r.IsDBNull(57))    item.CustomField24          = r.GetString(57);
                                if (!r.IsDBNull(58))    item.CustomField25          = r.GetString(58);
                                if (!r.IsDBNull(59))    item.CustomField26          = r.GetString(59);
                                if (!r.IsDBNull(60))    item.CustomField27          = r.GetString(60);
                                if (!r.IsDBNull(61))    item.CustomField28          = r.GetString(61);
                                if (!r.IsDBNull(62))    item.CustomField29          = r.GetString(62);
                                if (!r.IsDBNull(63))    item.CustomField30          = r.GetString(63);
                                if (!r.IsDBNull(64))    item.CustomField31          = r.GetString(64);
                                if (!r.IsDBNull(65))    item.CustomField32          = r.GetString(65);
                                if (!r.IsDBNull(66))    item.CustomField33          = r.GetString(66);
                                if (!r.IsDBNull(67))    item.CustomField34          = r.GetString(67);
                                if (!r.IsDBNull(68))    item.CustomField35          = r.GetString(68);
                                if (!r.IsDBNull(69))    item.CustomField36          = r.GetString(69);
                                if (!r.IsDBNull(70))    item.CustomField37          = r.GetString(70);
                                if (!r.IsDBNull(71))    item.CustomField38          = r.GetString(71);
                                if (!r.IsDBNull(72))    item.CustomField39          = r.GetString(72);
                                if (!r.IsDBNull(73))    item.CustomField40          = r.GetString(73);
                                if (!r.IsDBNull(74))    item.CustomField41          = r.GetString(74);
                                if (!r.IsDBNull(75))    item.CustomField42          = r.GetString(75);
                                if (!r.IsDBNull(76))    item.CustomField43          = r.GetString(76);
                                if (!r.IsDBNull(77))    item.CustomField44          = r.GetString(77);
                                if (!r.IsDBNull(78))    item.CustomField45          = r.GetString(78);
                                if (!r.IsDBNull(79))    item.CustomField46          = r.GetString(79);
                                if (!r.IsDBNull(80))    item.CustomField47          = r.GetString(80);
                                if (!r.IsDBNull(81))    item.CustomField48          = r.GetString(81);
                                if (!r.IsDBNull(82))    item.CustomField49          = r.GetString(82);
                                if (!r.IsDBNull(83))    item.CustomField50          = r.GetString(83);
                                if (!r.IsDBNull(84))    item.CustomField51          = r.GetString(84);
                                if (!r.IsDBNull(85))    item.CustomField52          = r.GetString(85);
                                if (!r.IsDBNull(86))    item.CustomField53          = r.GetString(86);
                                if (!r.IsDBNull(87))    item.CustomField54          = r.GetString(87);
                                if (!r.IsDBNull(88))    item.CustomField55          = r.GetString(88);
                                if (!r.IsDBNull(89))    item.CustomField56          = r.GetString(89);
                                if (!r.IsDBNull(90))    item.CustomField57          = r.GetString(90);
                                if (!r.IsDBNull(91))    item.CustomField58          = r.GetString(91);
                                if (!r.IsDBNull(92))    item.CustomField59          = r.GetString(92);
                                if (!r.IsDBNull(93))    item.CustomField60          = r.GetString(93);
                                if (!r.IsDBNull(94))    item.CustomField61          = r.GetString(94);
                                if (!r.IsDBNull(95))    item.CustomField62          = r.GetString(95);
                                if (!r.IsDBNull(96))    item.CustomField63          = r.GetString(96);
                                if (!r.IsDBNull(97))    item.CustomField67          = r.GetString(97);
                                if (!r.IsDBNull(98))    item.CustomField65          = r.GetString(98);
                                if (!r.IsDBNull(99))    item.CustomField66          = r.GetString(99);
                                if (!r.IsDBNull(100))   item.CustomField67          = r.GetString(100);
                                if (!r.IsDBNull(101))   item.CustomField68          = r.GetString(101);
                                if (!r.IsDBNull(102))   item.CustomField69          = r.GetString(102);
                                if (!r.IsDBNull(103))   item.CustomField70          = r.GetString(103);
                                if (!r.IsDBNull(104))   item.CustomField71          = r.GetString(104);
                                if (!r.IsDBNull(105))   item.CustomField72          = r.GetString(105);
                                if (!r.IsDBNull(106))   item.CustomField73          = r.GetString(106);
                                if (!r.IsDBNull(107))   item.CustomField74          = r.GetString(107);
                                if (!r.IsDBNull(108))   item.CustomField75          = r.GetString(108);
                                if (!r.IsDBNull(109))   item.CustomField76          = r.GetString(109);
                                if (!r.IsDBNull(110))   item.CustomField77          = r.GetString(110);
                                if (!r.IsDBNull(111))   item.CustomField78          = r.GetString(111);
                                if (!r.IsDBNull(112))   item.CustomField79          = r.GetString(112);
                                if (!r.IsDBNull(113))   item.CustomField80          = r.GetString(113);
                                if (!r.IsDBNull(114))   item.CustomField81          = r.GetString(114);
                                if (!r.IsDBNull(115))   item.CustomField82          = r.GetString(115);
                                if (!r.IsDBNull(116))   item.CustomField83          = r.GetString(116);
                                if (!r.IsDBNull(117))   item.CustomField84          = r.GetString(117);
                                if (!r.IsDBNull(118))   item.CustomField85          = r.GetString(118);
                                if (!r.IsDBNull(119))   item.CustomField86          = r.GetString(119);
                                if (!r.IsDBNull(120))   item.CustomField87          = r.GetString(120);
                                if (!r.IsDBNull(121))   item.CustomField88          = r.GetString(121);
                                if (!r.IsDBNull(122))   item.CustomField89          = r.GetString(122);
                                if (!r.IsDBNull(123))   item.CustomField90          = r.GetString(123);
                                if (!r.IsDBNull(124))   item.CustomField91          = r.GetString(124);
                                if (!r.IsDBNull(125))   item.CustomField92          = r.GetString(125);
                                if (!r.IsDBNull(126))   item.CustomField93          = r.GetString(126);
                                if (!r.IsDBNull(127))   item.CustomField94          = r.GetString(127);
                                if (!r.IsDBNull(128))   item.CustomField95          = r.GetString(128);
                                if (!r.IsDBNull(129))   item.CustomField96          = r.GetString(129);
                                if (!r.IsDBNull(130))   item.CustomField97          = r.GetString(130);
                                if (!r.IsDBNull(131))   item.CustomField98          = r.GetString(131);
                                if (!r.IsDBNull(132))   item.CustomField99          = r.GetString(132);
                                if (!r.IsDBNull(133))   item.CustomField100         = r.GetString(133);

                                yield return item;
                            }
                        }
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }
        public override IEnumerable<IfmAgents> IfmAgentsGet(ConnectionInfo connection, string source)
        {
            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = source, CommandType = CommandType.TableDirect })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                IfmAgents item = new IfmAgents();

                                if (!r.IsDBNull(0)) item.Agent_Id       = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.AgentName      = r.GetString(1);
                                if (!r.IsDBNull(2)) item.AgentType_Id   = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.FileName       = r.GetString(3);
                                if (!r.IsDBNull(4)) item.WaitTime       = r.GetInt32(4);
                                if (!r.IsDBNull(5)) item.WaitTimeType   = r.GetInt32(5);
                                if (!r.IsDBNull(6)) item.SendChanges    = r.GetString(6);
                                if (!r.IsDBNull(7)) item.GetChanges     = r.GetString(7);
                                if (!r.IsDBNull(8)) item.Status         = r.GetInt32(8);
                                if (!r.IsDBNull(9)) item.Image_Enabled  = r.GetString(9);

                                yield return item;
                            }
                        }
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }
        public override IEnumerable<IfmAgentsAvailable> IfmAgentsAvailableGet(ConnectionInfo connection, string source)
        {
            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = source, CommandType = CommandType.TableDirect })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                IfmAgentsAvailable item = new IfmAgentsAvailable();

                                if (!r.IsDBNull(0)) item.AgentType_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.AgentTypeName = r.GetString(1);
                                if (!r.IsDBNull(2)) item.AgentDllFile = r.GetString(2);
                                if (!r.IsDBNull(3)) item.CanSendChanges = r.GetString(3);
                                if (!r.IsDBNull(4)) item.CanGetChanges = r.GetString(4);

                                yield return item;
                            }
                        }
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }
        public override IEnumerable<IfmMasterFieldList> IfmMasterFieldListGet(ConnectionInfo connection, string source)
        {
            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = source, CommandType = CommandType.TableDirect })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                IfmMasterFieldList item = new IfmMasterFieldList();

                                if (!r.IsDBNull(0)) item.FieldNumber = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.FieldName = r.GetString(1);
                                if (!r.IsDBNull(2)) item.FieldType = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.FieldOrder = r.GetInt32(3);

                                yield return item;
                            }
                        }
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }
        public override IEnumerable<IfmMasterQueue_Change> IfmMasterQueueChangeGet(ConnectionInfo connection, string source)
        {
            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = source, CommandType = CommandType.TableDirect })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                IfmMasterQueue_Change item = new IfmMasterQueue_Change();

                                if (!r.IsDBNull(0)) item.Change_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.FromAgent_Id = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.ChangeType = r.GetString(2);
                                if (!r.IsDBNull(3)) item.ChangeDt = r.GetDouble(3);
                                if (!r.IsDBNull(4)) item.PostDt = r.GetDouble(4);
                                if (!r.IsDBNull(5)) item.Image_Recieved = r.GetString(5);

                                yield return item;
                            }
                        }
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }
        public override IEnumerable<IfmMasterQueue_Fields> IfmMasterQueueFieldsGet(ConnectionInfo connection, string source)
        {
            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = source, CommandType = CommandType.TableDirect })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                IfmMasterQueue_Fields item = new IfmMasterQueue_Fields();

                                if (!r.IsDBNull(0)) item.Change_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Master_Field = r.GetString(1);
                                if (!r.IsDBNull(2)) item.FieldValue = r.GetString(2);
                                if (!r.IsDBNull(3)) item.MaxFieldLength = r.GetInt32(3);

                                yield return item;
                            }
                        }
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }
        public override IEnumerable<IfmMasterQueue_KeyFields> IfmMasterQueueKeyFieldsGet(ConnectionInfo connection, string source)
        {
            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = source, CommandType = CommandType.TableDirect })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                IfmMasterQueue_KeyFields item = new IfmMasterQueue_KeyFields();

                                if (!r.IsDBNull(0)) item.Change_Id      = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.KeyFieldType   = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.KeyValue       = r.GetString(2);
                                if (!r.IsDBNull(3)) item.NewKeyValue    = r.GetString(3);

                                yield return item;
                            }
                        }
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }
        public override IEnumerable<IfmMasterQueue_Photo> IfmMasterQueuePhotoGet(ConnectionInfo connection, string source)
        {
            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = source, CommandType = CommandType.TableDirect })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                IfmMasterQueue_Photo item = new IfmMasterQueue_Photo();

                                if (!r.IsDBNull(0)) item.Change_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Photo_Blob = (byte[])r.GetValue(1);

                                yield return item;
                            }
                        }
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }
        public override IEnumerable<IfmMasterQueue_SendTo> IfmMasterQueueSendToGet(ConnectionInfo connection, string source)
        {
            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = source, CommandType = CommandType.TableDirect })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                IfmMasterQueue_SendTo item = new IfmMasterQueue_SendTo();

                                if (!r.IsDBNull(0)) item.Change_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Agent_Id = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.SentDt = r.GetDouble(2);
                                if (!r.IsDBNull(3)) item.PostDt = r.GetDouble(3);
                                if (!r.IsDBNull(4)) item.ResultCode = r.GetInt32(4);
                                if (!r.IsDBNull(5)) item.Result_ExtraMsg = r.GetString(5);
                                if (!r.IsDBNull(6)) item.Image_Sent = r.GetString(6);

                                yield return item;
                            }
                        }
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }
        public override IEnumerable<IfmRecordPool> IfmRecordPoolGet(ConnectionInfo connection, string source)
        {
            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = source, CommandType = CommandType.TableDirect })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                IfmRecordPool item = new IfmRecordPool();

                                if (!r.IsDBNull(0)) item.Record_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Agent_Id = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.LastChangeDt = r.GetDouble(2);

                                yield return item;
                            }
                        }
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }
        public override IEnumerable<IfmRecordPool_Fields> IfmRecordPoolFieldsGet(ConnectionInfo connection, string source)
        {
            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = source, CommandType = CommandType.TableDirect })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                IfmRecordPool_Fields item = new IfmRecordPool_Fields();

                                if (!r.IsDBNull(0)) item.Record_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.FieldNumber = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.FieldValue = r.GetString(2);
                                if (!r.IsDBNull(3)) item.MaxFieldLength = r.GetInt32(3);

                                yield return item;
                            }
                        }
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }
        public override IEnumerable<IfmRecordPool_KeyFields> IfmRecordPoolKeyFieldsGet(ConnectionInfo connection, string source)
        {
            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = source, CommandType = CommandType.TableDirect })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                IfmRecordPool_KeyFields item = new IfmRecordPool_KeyFields();

                                if (!r.IsDBNull(0)) item.Record_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.KeyFieldType = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Agent_Id = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.KeyValue = r.GetString(3);
                                if (!r.IsDBNull(4)) item.NewKeyValue = r.GetString(4);

                                yield return item;
                            }
                        }
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }
        public override IEnumerable<IfmUserAgentRights> IfmUserAgentRightsGet(ConnectionInfo connection, string source)
        {
            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = source, CommandType = CommandType.TableDirect })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                IfmUserAgentRights item = new IfmUserAgentRights();

                                if (!r.IsDBNull(0)) item.User_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.AgentType_Id = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Rights = r.GetString(2);

                                yield return item;
                            }
                        }
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }
        public override IEnumerable<IfmUserLog> IfmUserLogGet(ConnectionInfo connection, string source)
        {
            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = source, CommandType = CommandType.TableDirect })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                IfmUserLog item = new IfmUserLog();

                                if (!r.IsDBNull(0)) item.DateTime = r.GetDouble(0);
                                if (!r.IsDBNull(1)) item.LoginName = r.GetString(1);
                                if (!r.IsDBNull(2)) item.OsUser = r.GetString(2);
                                if (!r.IsDBNull(3)) item.Machine = r.GetString(3);
                                if (!r.IsDBNull(4)) item.Program = r.GetString(4);
                                if (!r.IsDBNull(5)) item.Description = r.GetString(5);

                                yield return item;
                            }
                        }
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }
        public override IEnumerable<IfmUserSetup> IfmUserSetupGet(ConnectionInfo connection, string source)
        {
            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = source, CommandType = CommandType.TableDirect })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                IfmUserSetup item = new IfmUserSetup();

                                if (!r.IsDBNull(0)) item.User_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.LoginName = r.GetString(1);
                                if (!r.IsDBNull(2)) item.FullName = r.GetString(2);
                                if (!r.IsDBNull(3)) item.Description = r.GetString(3);
                                if (!r.IsDBNull(4)) item.HashedPassword = (byte[])r.GetValue(4);
                                if (!r.IsDBNull(5)) item.Rights = r.GetString(5);

                                yield return item;
                            }
                        }
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }
    }
}