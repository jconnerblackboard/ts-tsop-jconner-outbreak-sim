﻿using System;
using System.Data;
using BbTS.Core.Serialization;
using BbTS.Domain.Models.Container;
using BbTS.Domain.Models.Definitions.Container;
using BbTS.Domain.Models.Definitions.Transaction;
using BbTS.Domain.Models.Exceptions.Resource;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Oracle.ManagedDataAccess.Client;
using Formatting = BbTS.Core.Conversion.Formatting;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        private readonly JsonSerializerSettings _transactionLogSerializerSettings = new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.Auto,
            Formatting = Newtonsoft.Json.Formatting.None,
            Converters = { new StringEnumConverter() },
            ContractResolver = BaseFirstContractResolver.Instance
        };

        /// <summary>
        /// Resource class for setting a transaction log in an oracle database.
        /// </summary>
        /// <param name="transactionMf4100"><see cref="Transaction"/> object to set.</param>
        [Obsolete("This should only be used for MF4100 backward-compatibility purposes.")]
        public override void TransactionLogSet(Transaction transactionMf4100)
        {
            using (OracleConnection con = new OracleConnection(ConnectionString))
            using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "TransactionFunctions.ObjectLogSet", CommandType = CommandType.StoredProcedure })
            {
                var content = NewtonsoftJson.Serialize(transactionMf4100, _transactionLogSerializerSettings);
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionObjectLogId", OracleDbType = OracleDbType.Int32, Direction = ParameterDirection.Input, Value = null });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pObject", OracleDbType = OracleDbType.Clob, Direction = ParameterDirection.Input, Value = content });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDisposition", OracleDbType = OracleDbType.Int32, Direction = ParameterDirection.Input, Value = (int)transactionMf4100.Disposition });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionId", OracleDbType = OracleDbType.Int32, Direction = ParameterDirection.Input, Value = null });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pRequestGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = null });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = transactionMf4100.Id.ToString("D").ToUpperInvariant() });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pOriginatorGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = null });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pObjectType", OracleDbType = OracleDbType.Varchar2, Size = 200, Direction = ParameterDirection.Input, Value = TransactionObjectDefinitions.TransactionObjectTypeGet(TransactionObjectType.TransactionMf4100) });

                try
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Resource class for setting a transaction log in an oracle database.  (Transaction MF4100 View Version 1)
        /// </summary>
        /// <param name="transactionMf4100ViewV01"><see cref="TransactionMf4100ViewV01"/> object to set.</param>
        /// <param name="requestId">The request ID, if available.</param>
        /// <exception cref="ResourceLayerException"></exception>
        public override void TransactionLogSet(TransactionMf4100ViewV01 transactionMf4100ViewV01, Guid? requestId)
        {
            using (OracleConnection con = new OracleConnection(ConnectionString))
            using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "TransactionFunctions.ObjectLogSet", CommandType = CommandType.StoredProcedure })
            {
                var content = NewtonsoftJson.Serialize(transactionMf4100ViewV01, _transactionLogSerializerSettings);
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionObjectLogId", OracleDbType = OracleDbType.Int32, Direction = ParameterDirection.Input, Value = null });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pObject", OracleDbType = OracleDbType.Clob, Direction = ParameterDirection.Input, Value = content });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDisposition", OracleDbType = OracleDbType.Int32, Direction = ParameterDirection.Input, Value = (int)transactionMf4100ViewV01.Disposition });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionId", OracleDbType = OracleDbType.Int32, Direction = ParameterDirection.Input, Value = null });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pRequestGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = requestId?.ToString("D").ToUpperInvariant() });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = transactionMf4100ViewV01.Id.ToString("D").ToUpperInvariant() });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pOriginatorGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = null });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pObjectType", OracleDbType = OracleDbType.Varchar2, Size = 200, Direction = ParameterDirection.Input, Value = TransactionObjectDefinitions.TransactionObjectTypeGet(TransactionObjectType.TransactionMf4100ViewV01) });

                try
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Resource class for setting a transaction log in an oracle database.  (Transaction View Version 1)
        /// </summary>
        /// <param name="transactionViewV01"><see cref="TransactionViewV01"/> object to set.</param>
        /// <param name="requestId">The request ID, if available.</param>
        /// <exception cref="ResourceLayerException"></exception>
        public override void TransactionLogSet(TransactionViewV01 transactionViewV01, Guid? requestId)
        {
            using (OracleConnection con = new OracleConnection(ConnectionString))
            using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "TransactionFunctions.ObjectLogSet", CommandType = CommandType.StoredProcedure })
            {
                var content = NewtonsoftJson.Serialize(transactionViewV01, _transactionLogSerializerSettings);
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionObjectLogId", OracleDbType = OracleDbType.Int32, Direction = ParameterDirection.Input, Value = null });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pObject", OracleDbType = OracleDbType.Clob, Direction = ParameterDirection.Input, Value = content });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDisposition", OracleDbType = OracleDbType.Int32, Direction = ParameterDirection.Input, Value = (int)transactionViewV01.Disposition });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionId", OracleDbType = OracleDbType.Int32, Direction = ParameterDirection.Input, Value = null });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pRequestGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = requestId?.ToString("D").ToUpperInvariant() });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = transactionViewV01.Id.ToString("D").ToUpperInvariant() });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pOriginatorGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = transactionViewV01.OriginatorId.ToString("D").ToUpperInvariant() });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pObjectType", OracleDbType = OracleDbType.Varchar2, Size = 200, Direction = ParameterDirection.Input, Value = TransactionObjectDefinitions.TransactionObjectTypeGet(TransactionObjectType.TransactionViewV01) });

                try
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Resource class for setting a transaction log in an oracle database.  (Transaction View Version 2)
        /// </summary>
        /// <param name="transactionViewV02"><see cref="TransactionViewV02"/> object to set.</param>
        /// <param name="requestId">The request ID, if available.</param>
        /// <exception cref="ResourceLayerException"></exception>
        public override void TransactionLogSet(TransactionViewV02 transactionViewV02, Guid? requestId)
        {
            using (OracleConnection con = new OracleConnection(ConnectionString))
            using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "TransactionFunctions.ObjectLogSet", CommandType = CommandType.StoredProcedure })
            {
                var content = NewtonsoftJson.Serialize(transactionViewV02, _transactionLogSerializerSettings);
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionObjectLogId", OracleDbType = OracleDbType.Int32, Direction = ParameterDirection.Input, Value = null });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pObject", OracleDbType = OracleDbType.Clob, Direction = ParameterDirection.Input, Value = content });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDisposition", OracleDbType = OracleDbType.Int32, Direction = ParameterDirection.Input, Value = (int)transactionViewV02.Disposition });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionId", OracleDbType = OracleDbType.Int32, Direction = ParameterDirection.Input, Value = null });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pRequestGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = requestId?.ToString("D").ToUpperInvariant() });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = transactionViewV02.Id.ToString("D").ToUpperInvariant() });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pOriginatorGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = transactionViewV02.OriginatorId.ToString("D").ToUpperInvariant() });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pObjectType", OracleDbType = OracleDbType.Varchar2, Size = 200, Direction = ParameterDirection.Input, Value = TransactionObjectDefinitions.TransactionObjectTypeGet(TransactionObjectType.TransactionViewV02) });

                try
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Resource class for setting a transaction log in an oracle database when the transaction information is not parsable.  This should only be used when incoming JSON cannot be deserialized.
        /// </summary>
        /// <param name="clob">The string clob to write to the data layer.</param>
        /// <param name="disposition">The disposition to set the clob to.  Default to <see cref="Disposition.Unknown"/>.</param>
        /// <param name="requestId">The request ID, if available.</param>
        public override void TransactionLogSet(string clob, Disposition disposition, Guid? requestId = null)
        {
            using (OracleConnection con = new OracleConnection(ConnectionString))
            using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "TransactionFunctions.ObjectLogSet", CommandType = CommandType.StoredProcedure })
            {
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionObjectLogId", OracleDbType = OracleDbType.Int32, Direction = ParameterDirection.Input, Value = null });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pObject", OracleDbType = OracleDbType.Clob, Direction = ParameterDirection.Input, Value = clob });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDisposition", OracleDbType = OracleDbType.Int32, Direction = ParameterDirection.Input, Value = (int)disposition });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionId", OracleDbType = OracleDbType.Int32, Direction = ParameterDirection.Input, Value = null });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pRequestGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = null });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = null });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pOriginatorGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = null });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pObjectType", OracleDbType = OracleDbType.Varchar2, Size = 200, Direction = ParameterDirection.Input, Value = TransactionObjectDefinitions.TransactionObjectTypeGet(TransactionObjectType.Unknown) });

                try
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }
    }
}
