﻿using System;
using System.Collections.Generic;
using System.Data;
using BbTS.Core.Conversion;
using Oracle.ManagedDataAccess.Client;
using BbTS.Core;
using BbTS.Domain.Models.Exceptions.Resource;
using System.Diagnostics;
using BbTS.Monitoring.Logging;
using BbTS.Domain.Models.Report;
using System.Xml.Serialization;
using System.IO;
using Oracle.ManagedDataAccess.Types;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        /// <summary>
        /// Gets drawer audit
        /// </summary>
        /// <param name="sessionGuid"></param>
        /// <returns>Drawer audit</returns>
        public override TsDrawerReport DrawerAuditGet(string sessionGuid)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "ReportFunctions.DrawerAuditGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pSessionGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = sessionGuid });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pXml", OracleDbType = OracleDbType.Clob, Direction = ParameterDirection.Output });
                        con.Open();

                        cmd.ExecuteNonQuery();
                        var xml = (cmd.Parameters[1].Value as OracleClob)?.Value;
                        if (string.IsNullOrEmpty(xml))
                            return null;

                        using (var reader = new StringReader(xml))
                        {
                            var report = (TsDrawerReport)new XmlSerializer(typeof(TsDrawerReport), new XmlRootAttribute("Report")).Deserialize(reader);
                            report.ReportType = "Drawer";
                            return report;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Get a pos audit
        /// </summary>
        /// <param name="originatorGuid"></param>
        /// <param name="businessDayType"></param>
        /// <returns>Report</returns>
        public override TsDailyReport PosAuditGet(string originatorGuid, int? businessDayType)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "ReportFunctions.PosAuditGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pOriginatorGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = originatorGuid });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pBusinessDayType", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = businessDayType });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pXml", OracleDbType = OracleDbType.Clob, Direction = ParameterDirection.Output });
                        con.Open();

                        cmd.ExecuteNonQuery();
                        var xml = (cmd.Parameters[2].Value as OracleClob)?.Value;
                        if (string.IsNullOrEmpty(xml))
                            return null;

                        using (var reader = new StringReader(xml))
                        {
                            var report = (TsDailyReport)new XmlSerializer(typeof(TsDailyReport), new XmlRootAttribute("Report")).Deserialize(reader);
                            report.ReportType = "POS";
                            return report;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Get a profit center audit
        /// </summary>
        /// <param name="profitCenterGuid"></param>
        /// <param name="businessDayType"></param>
        /// <returns>Report</returns>
        public override TsDailyReport ProfitCenterAuditGet(string profitCenterGuid, int? businessDayType)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "ReportFunctions.ProfitCenterAuditGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pProfitCenterGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = profitCenterGuid });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pBusinessDayType", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = businessDayType });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pXml", OracleDbType = OracleDbType.Clob, Direction = ParameterDirection.Output });
                        con.Open();

                        cmd.ExecuteNonQuery();
                        var xml = (cmd.Parameters[2].Value as OracleClob)?.Value;
                        if (string.IsNullOrEmpty(xml))
                            return null;

                        using (var reader = new StringReader(xml))
                        {
                            var report = (TsDailyReport)new XmlSerializer(typeof(TsDailyReport), new XmlRootAttribute("Report")).Deserialize(reader);
                            report.ReportType = "ProfitCenter";
                            return report;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override List<OperatorSession> OperatorSessionsGet(Guid originatorGuid, int cashDrawerNumber)
        {
            var list = new List<OperatorSession>();

            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "ReportFunctions.OperatorSessionListGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pOriginatorGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = originatorGuid.ToString("D") });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "nCashDrawerNum", OracleDbType = OracleDbType.Int32, Size = 6, Direction = ParameterDirection.Input, Value = cashDrawerNumber });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new OperatorSession()
                                {
                                    SessionId = !r.IsDBNull(0) ? Guid.Parse(r.GetString(0)) : Guid.Empty,
                                    StartDateTime = !r.IsDBNull(1) ? (DateTime?)DateTime.FromOADate(r.GetDouble(1)) : null,
                                    EndDateTime = !r.IsDBNull(2) ? (DateTime?)DateTime.FromOADate(r.GetDouble(2)) : null,
                                    CashierNumber = !r.IsDBNull(3) ? r.GetInt32(3) : -1,
                                    OperatorName = !r.IsDBNull(4) ? r.GetString(4) : null,
                                    OperatorGuid = !r.IsDBNull(5) ? Guid.Parse(r.GetString(5)) : Guid.Empty
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }
    }
}