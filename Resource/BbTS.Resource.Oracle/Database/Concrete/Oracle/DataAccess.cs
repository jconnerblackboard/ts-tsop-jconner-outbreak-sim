﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.System.Database;
using BbTS.Resource.Database.Abstract;
using Oracle.ManagedDataAccess.Client;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    /// <summary>
    /// This class contains various DataAccess methods
    /// </summary>
    public partial class OracleSource : ResourceDatabase
    {
        /// <summary>
        /// Normalizes an error code coming from the database.
        /// </summary>
        /// <param name="databaseErrorCode"></param>
        /// <returns></returns>
        private static int ErrorCodeNormalize(OracleParameter databaseErrorCode)
        {
            int errorCode = TryGetIntValue(databaseErrorCode, 0);
            return errorCode == 0 ? errorCode : errorCode + LoggingDefinitions.DatabaseErrorCodeOffset;
        }

        #region Generic Execute Methods 

        /// <summary>
        /// This mehods will load a data table into the target table using array binding
        /// </summary>
        /// <param name="dataTable">The data table to load</param>
        /// <param name="info">The connection info</param>
        public override void BulkLoadTable(DataTable dataTable, ConnectionInfo info)
        {
            if (!dataTable.Rows.Count.Equals(0))
            {
                // Local variables
                var columnList = string.Empty;
                var insertList = string.Empty;

                // Build the column lists
                foreach (var column in dataTable.Columns)
                {
                    columnList += $"{column},";
                    insertList += $":{column},";
                }

                // Open the connection and upload the data
                using (var con = OracleConnectionGet(info))
                {
                    con.Open();
                    try
                    {
                        // The size of the batches to be sent to the db.  100k was the fastest of reasonable size @ 3.1secs per batch
                        const int batchSize = 100000;

                        // Chunk the table up into bite sized pieces
                        for (var i = 0; i < dataTable.Rows.Count; i += batchSize)
                        {
                            var bulkTable = dataTable.Select().Skip(i).Take(batchSize).ToList();

                            // Create the oracle command
                            using (var cmd = new OracleCommand($"INSERT INTO {dataTable.TableName} ({columnList.TrimEnd(',')}) VALUES ({insertList.TrimEnd(',')})", con)
                            {
                                CommandType = CommandType.Text,
                                BindByName = true,
                                ArrayBindCount = bulkTable.Count
                            })
                            {
                                // Create a parameter for each of the columns in the data table
                                for (Int32 x = 0; x < dataTable.Columns.Count; x++)
                                {
                                    OracleParameter parm = new OracleParameter
                                    {
                                        ParameterName = $":{dataTable.Columns[x]}",
                                        Direction = ParameterDirection.Input,
                                        OracleDbType = DataTableTypeToOracleDbType(dataTable.Columns[x].DataType.ToString())
                                    };

                                    object[] obj = new object[bulkTable.Count];

                                    for (Int32 row = 0; row < bulkTable.Count; row++)
                                    {
                                        obj[row] = bulkTable[row].ItemArray[x];
                                    }

                                    // Add the value to the parm and the parm to the command
                                    parm.Value = obj;
                                    cmd.Parameters.Add(parm);
                                }

                                // Execute the command                               
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                        con.Dispose();
                    }
                }
            }
        }

        /// <summary>
        /// This method executes a reader command and returns a data table of the data
        /// </summary>
        /// <param name="info">The server to connect to</param>
        /// <param name="sqlCommand">The command to be executed</param>
        /// <param name="queryContainsLongData">Whether the query contains long data</param>
        /// <returns>A DataTable representing the results of the sqlCommand</returns>
        public override DataTable DataTableGet(ConnectionInfo info, string sqlCommand, bool queryContainsLongData = false)
        {
            // Create the return object
            DataTable dataTable = new DataTable();

            // Execute the command and fill the data table
            using (var con = OracleConnectionGet(info))
            {
                con.Open();
                try
                {
                    using (OracleCommand cmd = new OracleCommand(sqlCommand, con) { CommandType = CommandType.Text })
                    {
                        if (queryContainsLongData)
                        {
                            cmd.InitialLONGFetchSize = -1;
                        }

                        dataTable.Load(cmd.ExecuteReader());
                        return dataTable;
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// This method will execute a non-query command against the passed oracle connection
        /// </summary>
        /// <param name="info">The connection information required to create an OracleConnection object</param>
        /// <param name="sqlCommand">The command to execute</param>
        public override void ExecuteNonQuery(ConnectionInfo info, string sqlCommand)
        {
            using (OracleConnection con = OracleConnectionGet(info))
            {
                try
                {
                    con.Open();
                    using (OracleCommand cmd = new OracleCommand(sqlCommand, con) { CommandType = CommandType.Text })
                    {
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
            }
        }

        /// <summary>
        /// This method will execute a non-query command against the passed oracle connection
        /// </summary>
        /// <param name="info">The connection information required to create an OracleConnection object</param>
        /// <param name="sqlCommand">The command to execute</param>
        public override object ExecuteScalar(ConnectionInfo info, string sqlCommand)
        {
            using (var con = OracleConnectionGet(info))
            {
                try
                {
                    con.Open();
                    using (OracleCommand cmd = new OracleCommand(sqlCommand, con) { CommandType = CommandType.Text })
                    {
                        var result = cmd.ExecuteScalar();
                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
            }
        }

        /// <summary>
        /// This method will execute a command and return a List object
        /// </summary>
        /// <param name="sqlCommand">The command to execute</param>
        /// <param name="info">The connection info</param>
        /// <returns></returns>
        public override List<object[]> ExecuteReaderToObjectList(string sqlCommand, ConnectionInfo info)
        {
            List<object[]> resultList = new List<object[]>();

            using (var con = OracleConnectionGet(info))
            {
                try
                {
                    con.Open();

                    using (var cmd = new OracleCommand(sqlCommand, con) { CommandType = CommandType.Text })
                    {
                        using (OracleDataReader reader = cmd.ExecuteReader())
                        {
                            int fields = reader.FieldCount - 1;

                            while (reader.Read())
                            {
                                object[] row = new object[reader.FieldCount];

                                for (int i = 0; i <= fields; i++)
                                {
                                    row[i] = reader[i];
                                }
                                resultList.Add(row);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return resultList;
        }

        /// <summary>
        /// This method will execute the provided command within a transactional wrapper
        /// </summary>
        /// <param name="connection">The connection information required to create an OracleConnection object</param>
        /// <param name="command">The command to execute</param>
        /// <param name="commit">Whether to command the transaction or not</param>
        public override void ExecuteTransactionalNonQuery(ConnectionInfo connection, string command, bool commit)
        {
            using (var con = OracleConnectionGet(connection))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = command, CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        var tran = con.BeginTransaction(IsolationLevel.ReadCommitted);
                        cmd.Transaction = tran;
                        cmd.ExecuteNonQuery();

                        if (commit)
                            tran.Commit();
                        else
                            tran.Rollback();
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        #endregion

        #region Diagnostic Methods

        /// <summary>
        /// This method will test the connectivity to the database and execute a super basic command
        /// </summary>
        /// <param name="info">The connection information required to create an OracleConnection object</param>
        public override Boolean ConnectionTest(ConnectionInfo info)
        {
            var result = ExecuteScalar(info, "SELECT 'Ping' FROM DUAL");
            return result.Equals("Ping");
        }

        /// <summary>
        /// This method will return the version of the server
        /// </summary>
        /// <param name="info">The connection information required to create an OracleConnection object</param>
        /// <returns>The server version</returns>
        public override string ServerVersionGet(ConnectionInfo info)
        {
            using (OracleConnection con = OracleConnectionGet(info))
            {
                con.Open();
                return con.ServerVersion;
            }
        }

        /// <summary>
        /// Ensure the user/schema exists in the system
        /// </summary>
        /// <param name="info"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public override bool DatabaseUserExists(ConnectionInfo info, string name)
        {
            var result = ExecuteScalar(info, $"SELECT COUNT(*) FROM Dba_Users WHERE UserName = UPPER('{name}')");
            return result.ToString().Equals("1");
        }

        /// <summary>
        /// This method determines if a table exists or not
        /// </summary>
        /// <param name="info"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public override bool TableExists(ConnectionInfo info, string name)
        {
            string query;
            if (name.Contains('.'))
            {
                string[] nameParts = name.Split('.');
                query = $"SELECT COUNT(*) FROM All_Tables WHERE Owner = UPPER('{nameParts[0]}') AND Table_Name = UPPER('{nameParts[1]}')";
            }
            else
                query = $"SELECT COUNT(*) FROM All_Tables WHERE Table_Name = UPPER('{name}')";

            var result = ExecuteScalar(info, query);
            return result.ToString().Equals("1");
        }

        #endregion
    }
}