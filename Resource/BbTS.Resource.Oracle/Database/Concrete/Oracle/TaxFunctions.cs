﻿using System;
using System.Collections.Generic;
using System.Data;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.RetailTransaction;
using BbTS.Domain.Models.System.Database;
using Oracle.ManagedDataAccess.Client;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        #region TaxGroups

        /// <summary>
        /// This method will return a list of records for the TaxGroups objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsTaxGroups> TaxGroupsArchivesGet(ConnectionInfo connection)
        {
            var list = new List<TsTaxGroups>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (var cmd = new OracleCommand {
                  Connection = con,
                  CommandText = @"SELECT  TaxGroup_Id,
                                          Merchant_Id,
                                          TaxGroup
                                  FROM    Envision.TaxGroups",
                  CommandType = CommandType.Text
                })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsTaxGroups();

                                if (!r.IsDBNull(0)) item.TaxGroup_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Merchant_Id = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.TaxGroup = r.GetString(2);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }
        #endregion

        #region TaxSchedule

        /// <summary>
        /// This method will return a list of records for the TaxSchedule objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsTaxSchedule> TaxScheduleArchivesGet(ConnectionInfo connection)
        {
            var list = new List<TsTaxSchedule>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (var cmd = new OracleCommand {
                  Connection = con,
                  CommandText = @"SELECT  TaxSchedule_Id,
                                          Link_TaxSchedule_Id,
                                          Merchant_Id,
                                          TaxAccount_Id,
                                          Name,
                                          Codename,
                                          TaxExempt_Quantity_Enabled,
                                          TaxExempt_Quantity,
                                          Taxable_Amount_Minimum_Enabled,
                                          Taxable_Amount_Minimum,
                                          Taxable_Amount_Maximum_Enabled,
                                          Taxable_Amount_Maximum,
                                          TaxLink_TaxSchedule_Id,
                                          TaxLink_Tax_Taxes
                                  FROM    Envision.TaxSchedule",
                  CommandType = CommandType.Text
                })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsTaxSchedule();

                                if (!r.IsDBNull(0)) item.TaxSchedule_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Link_TaxSchedule_Id = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Merchant_Id = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.TaxAccount_Id = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.Name = r.GetString(4);
                                if (!r.IsDBNull(5)) item.CodeName = r.GetString(5);
                                if (!r.IsDBNull(6)) item.TaxExempt_Quantity_Enabled = r.GetString(6);
                                if (!r.IsDBNull(7)) item.TaxExempt_Quantity = r.GetInt32(7);
                                if (!r.IsDBNull(8)) item.Taxable_Amount_Minimum_Enabled = r.GetString(8);
                                if (!r.IsDBNull(9)) item.Taxable_Amount_Minimum = r.GetInt32(9);
                                if (!r.IsDBNull(10)) item.Taxable_Amount_Maximum_Enabled = r.GetString(10);
                                if (!r.IsDBNull(11)) item.Taxable_Amount_Maximum = r.GetInt32(11);
                                if (!r.IsDBNull(12)) item.TaxLink_TaxSchedule_Id = r.GetInt32(12);
                                if (!r.IsDBNull(13)) item.TaxLink_Tax_Taxes = r.GetString(13);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Get the Tax Schedules for a device.
        /// </summary>
        /// <param name="deviceId">The unique identifier for the Device.</param>
        /// <returns>List of tax schedules for a device.</returns>
        public override List<TaxScheduleDeviceSetting> TaxScheduleDeviceSettingsGet(string deviceId)
        {
            var list = new List<TaxScheduleDeviceSetting>();

            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = @"
                        SELECT          TSH.TAXSCHEDULE_ID,
                                        TSH.NAME,
                                        TSH.TAXACCOUNT_ID,
                                        TAC.NAME,
                                        TSH.TAXEXEMPT_QUANTITY_ENABLED,
                                        TSH.TAXEXEMPT_QUANTITY,
                                        TSH.TAXABLE_AMOUNT_MINIMUM_ENABLED,
                                        TSH.TAXABLE_AMOUNT_MINIMUM,
                                        TSH.TAXABLE_AMOUNT_MAXIMUM_ENABLED,
                                        TSH.TAXABLE_AMOUNT_MAXIMUM,
                                        TSH.TAXLINK_TAXSCHEDULE_ID,
                                        TSH.TAXLINK_TAX_TAXES
                        FROM            TAXSCHEDULE TSH
                        INNER JOIN      TaxAccount                TAC ON  TAC.TAXACCOUNT_ID       = TSH.TAXACCOUNT_ID
                        INNER JOIN      Merchant                  MER ON  MER.Merchant_Id         = TSH.Merchant_Id
                        INNER JOIN      ProfitCenter              PC  ON  MER.Merchant_Id         = PC.Merchant_Id
                        INNER JOIN      Pos                       POS ON  PC.ProfitCenter_Id      = POS.ProfitCenter_Id  
                        INNER JOIN      Originator                DEV ON  POS.OriginatorId        = DEV.OriginatorId
                        WHERE           DEV.OriginatorGuid = :pValue",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        cmd.Parameters.Add(
                            new OracleParameter
                            {
                                ParameterName = "pValue",
                                OracleDbType = OracleDbType.Varchar2,
                                Size = 36,
                                Value = new Guid(deviceId).ToString("D"),
                                Direction = ParameterDirection.Input
                            });
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TaxScheduleDeviceSetting();

                                if (!r.IsDBNull(0)) item.TaxScheduleId = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Name = r.GetString(1);
                                if (!r.IsDBNull(2)) item.TaxAccountId = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.TaxAccountName = r.GetString(3);
                                if (!r.IsDBNull(4)) item.TaxExemptQuantityEnabled = Formatting.TfStringToBool(r.GetString(4));
                                if (!r.IsDBNull(5)) item.TaxExemptQuantity = r.GetInt32(5);
                                if (!r.IsDBNull(6)) item.TaxableAmountMinimumEnabled = Formatting.TfStringToBool(r.GetString(6));
                                if (!r.IsDBNull(7)) item.TaxableAmountMinimum = r.GetInt32(7) / 1000m;
                                if (!r.IsDBNull(8)) item.TaxableAmountMaximumEnabled = Formatting.TfStringToBool(r.GetString(8));
                                if (!r.IsDBNull(9)) item.TaxableAmountMaximum = r.GetInt32(9) / 1000m;
                                if (!r.IsDBNull(10)) item.TaxLinkTaxScheduleId = r.GetInt32(10);
                                if (!r.IsDBNull(11)) item.TaxLinkTaxTaxes = Formatting.TfStringToBool(r.GetString(11));

                                item.TaxScheduleTenders.AddRange(TaxScheduleTendersGet(item.TaxScheduleId));

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Get all tenders associated with a tax schedule.
        /// </summary>
        /// <param name="taxScheduleId">The unique identifier for the tax schedule</param>
        /// <returns>List of tenders and tax rates associated with the tax schedule.</returns>
        public override List<TaxScheduleTender> TaxScheduleTendersGet(int taxScheduleId)
        {
            var list = new List<TaxScheduleTender>();

            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = @"
                        SELECT  TAXSCHEDULE_ID, TENDER_ID, TAX_PERCENT 
                        FROM    TaxSchedule_Tender 
                        WHERE   TAXSCHEDULE_ID = :pValue",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pValue", OracleDbType = OracleDbType.Int32, Value = taxScheduleId, Direction = ParameterDirection.Input });

                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TaxScheduleTender();

                                if (!r.IsDBNull(0)) item.TaxScheduleId = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.TenderId = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.TaxRate = r.GetDecimal(2) / 100000m;

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion
    }
}