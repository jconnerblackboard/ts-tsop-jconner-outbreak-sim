﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Net;
using System.Text;
using BbTS.Core;
using BbTS.Core.Logging.Tracing;
using BbTS.Core.Security.Encryption;
using BbTS.Core.Serialization;
using BbTS.Domain.Models.Definitions;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.Exceptions.WebApi;
using BbTS.Domain.Models.System;
using BbTS.Domain.Models.System.Database;
using BbTS.Domain.Models.System.Logging;
using BbTS.Domain.Models.System.Security.Oauth;
using BbTS.Monitoring.Logging;
using Newtonsoft.Json;
using Oracle.ManagedDataAccess.Client;
using Formatting = BbTS.Core.Conversion.Formatting;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    /// <summary>
    /// This class exposes various methods to update system objects in Transact
    /// </summary>
    [Trace(AttributePriority = 2)]
    public partial class OracleSource
    {

        /// <summary>
        /// Get the value of a domain data entry in an oracle database.
        /// </summary>
        /// <param name="domainName">Domain name of the value</param>
        /// <param name="domainId">Id of the domain value</param>
        /// <returns>value as a string</returns>
        public override string DomainDataValueGet(string domainName, int domainId)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "DomainData.ValueGet", CommandType = CommandType.StoredProcedure })
                    {

                        cmd.Parameters.Add(new OracleParameter("pDomainName", OracleDbType.Varchar2, domainName.Length, domainName, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pDomainId", OracleDbType.Int32, 0, domainId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pValue", OracleDbType.Varchar2, 100, ParameterDirection.Output));
                        con.Open();
                        cmd.ExecuteNonQuery();
                        return (string)(cmd.Parameters["pValue"].Value);
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Get the value of a domain data entry in an oracle database.
        /// </summary>
        /// <param name="domainTypeId">Domain TypeId of the domain</param>
        /// <param name="domainId">Id of the domain value</param>
        /// <returns>value as a string</returns>
        public override string DomainDataValueGet(int domainTypeId, int domainId)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (OracleCommand cmd = new OracleCommand
                    {
                        Connection = con,
                        CommandType = CommandType.Text,
                        CommandText = @"SELECT DV.Value FROM DomainView DV WHERE DV.Domain_Type = :pDomainTypeId AND DV.DOMAIN_ID = :pDomainId"
                    })
                    {
                        cmd.Parameters.Add(new OracleParameter("pDomainTypeId", OracleDbType.Int32, 0, domainTypeId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pDomainId", OracleDbType.Int32, 0, domainId, ParameterDirection.Input));
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var nextVal = !r.IsDBNull(0) ? r.GetString(0) : null;
                                return nextVal;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return null;
        }

        /// <summary>
        /// Get the password complexity rules from an oracle database
        /// </summary>
        /// <returns>List of DomainViewObjects containing the password complexity rules as stored in Oracle</returns>
        public override List<DomainViewObject> DomainListGet(string domain)
        {
            var rules = new List<DomainViewObject>();
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "DomainData.ListGet", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {

                        cmd.Parameters.Add(
                            new OracleParameter("pDomainName", OracleDbType.Varchar2, domain.Length)
                            {
                                Direction = ParameterDirection.Input,
                                Value = domain
                            });

                        cmd.Parameters.Add(new OracleParameter("pList", OracleDbType.RefCursor, ParameterDirection.Output));
                        con.Open();

                        using (OracleDataReader r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var id = !r.IsDBNull(0) ? r.GetInt32(0) : -1;
                                var name = !r.IsDBNull(1) ? r.GetString(1) : null;
                                var description = !r.IsDBNull(2) ? r.GetString(2) : null;

                                rules.Add(new DomainViewObject { Id = id, Name = name, Description = description });
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            return rules;
        }

        /// <summary>
        /// Write an activity log to the database resource layer.
        /// </summary>
        /// <param name="activityLogGuid">The unique identifier for the action.</param>
        /// <param name="origin">The entity making the log entry.</param>
        /// <param name="data">The data to log.</param>
        public override void ActivityLogSet<T>(string activityLogGuid, ActivityLogOriginEntity origin, T data)
        {
            using (OracleConnection con = new OracleConnection(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "SystemFunctions.ActivityLogSet", CommandType = CommandType.StoredProcedure })
                {
                    var content = NewtonsoftJson.Serialize(data, TypeNameHandling.Auto);

                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pActivityLogId", OracleDbType = OracleDbType.Int32, Direction = ParameterDirection.Input, Value = null });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pActivityLogGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = activityLogGuid });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pData", OracleDbType = OracleDbType.Clob, Direction = ParameterDirection.Input, Value = content });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pOriginId", OracleDbType = OracleDbType.Int32, Direction = ParameterDirection.Input, Value = (int)origin });

                    try
                    {
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Write an audit log entry in an oracle database.
        /// </summary>
        /// <param name="logItem"><see cref="AuditLogItem"/> object to log.</param>
        public override void AuditLogSet(AuditLogItem logItem)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "Logging.AuditLogSet", CommandType = CommandType.StoredProcedure })
                    {
                        string successFlag = logItem.SuccessFlag ? "T" : "F";
                        int eventType = (int)logItem.EventTypeId;
                        int entityId = (int)logItem.EntityId;

                        cmd.Parameters.Add(new OracleParameter("pUserName", OracleDbType.Varchar2, logItem.Username.Length, logItem.Username, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pEventTypeId", OracleDbType.Int32, 0, eventType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pEntityId", OracleDbType.Int32, 0, entityId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAffectedDataDescription", OracleDbType.Varchar2, logItem.AffectedDataDescription.Length, logItem.AffectedDataDescription, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAffectedDataId", OracleDbType.Varchar2, logItem.AffectedDataId.Length, logItem.AffectedDataId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pSuccessFlag", OracleDbType.Varchar2, successFlag.Length, successFlag, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pProgram", OracleDbType.Varchar2, logItem.Program.Length, logItem.Program, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pOsHostName", OracleDbType.Varchar2, logItem.OsHostName.Length, logItem.OsHostName, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIpAddress", OracleDbType.Varchar2, logItem.IpAddress.Length, logItem.IpAddress, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pOsUser", OracleDbType.Varchar2, logItem.OsUser.Length, logItem.OsUser, ParameterDirection.Input));
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        #region Denied_Message

        /// <summary>
        /// This method will return a list of records for the DeniedMessage objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsDenied_Message> DeniedMessageArchivesGet(ConnectionInfo connection)
        {
            var list = new List<TsDenied_Message>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = @"SELECT  Denied_Message_Id,
                                          Denied_Message_Name,
                                          Denied_Message_Text,
                                          Denied_Message_Short_Text,
                                          Denied_Category
                                  FROM    Envision.Denied_Message",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsDenied_Message();

                                if (!r.IsDBNull(0)) item.Denied_Message_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Denied_Message_Name = r.GetString(1);
                                if (!r.IsDBNull(2)) item.Denied_Message_Text = r.GetString(2);
                                if (!r.IsDBNull(3)) item.Denied_Message_Short_Text = r.GetString(3);
                                if (!r.IsDBNull(4)) item.Denied_Category = r.GetString(4);

                                list.Add(item);
                            }
                        }
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

        /// <summary>
        /// Get a User object from a username from the data layer
        /// </summary>
        /// <param name="userName">user to fetch</param>
        /// <returns><see cref="User"/> that was retrieved.</returns>
        public override User UserByNameGet(string userName)
        {
            User returnValue = null;
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "UserMaintenance.AllUsersGet", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pUserName", OracleDbType.Varchar2, userName.Length, userName, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pUsers", OracleDbType.RefCursor, userName.Length, ParameterDirection.Output));
                        con.Open();
                        using (OracleDataReader r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var username = !r.IsDBNull(0) ? r.GetString(0) : null;
                                var fullName = !r.IsDBNull(1) ? r.GetString(1) : null;
                                var description = !r.IsDBNull(2) ? r.GetString(2) : null;
                                var passwordHash = !r.IsDBNull(3) ? (byte[])r.GetValue(3) : null;
                                var passwordHashSalt = !r.IsDBNull(4) ? Encoding.ASCII.GetBytes(r.GetString(4)) : null;
                                var isActive = !r.IsDBNull(5) && Formatting.TfStringToBool(r.GetString(5));
                                var isLocked = !r.IsDBNull(6) && Formatting.TfStringToBool(r.GetString(6));
                                var passwordChangeRequired = !r.IsDBNull(7) && Formatting.TfStringToBool(r.GetString(7));
                                var passwordCreateDatetime = !r.IsDBNull(8) ? DateTime.FromOADate(r.GetFloat(8)) : DateTime.MinValue;
                                var activeStartDate = !r.IsDBNull(9) ? new DateTime?(DateTime.FromOADate(r.GetFloat(9))) : null;
                                var activeEndDate = !r.IsDBNull(10) ? new DateTime?(DateTime.FromOADate(r.GetFloat(10))) : null;
                                var ignoreMerchantrightsCust = !r.IsDBNull(12) && Formatting.TfStringToBool(r.GetString(12));
                                var isAdmin = !r.IsDBNull(13) && Formatting.TfStringToBool(r.GetString(13));
                                var inactiveAcctDisableDaysOverride = !r.IsDBNull(14) ? new int?(r.GetInt32(14)) : null;
                                var hashType = !r.IsDBNull(15) ? r.GetInt32(15) : -1;
                                var iterationCount = !r.IsDBNull(16) ? r.GetInt32(16) : -1;
                                var externalClientId = !r.IsDBNull(17) ? r.GetInt32(17).ToString() : null;
                                var clientId = !r.IsDBNull(18) ? r.GetInt32(18).ToString() : null;
                                var domainId = !r.IsDBNull(19) ? r.GetString(19) : null;
                                var lastLogoutDatetime = !r.IsDBNull(20) ? DateTime.FromOADate(r.GetFloat(20)) : DateTime.MinValue;
                                var accountLockedOutDateTime = !r.IsDBNull(21) ? DateTime.FromOADate(r.GetFloat(21)) : DateTime.MinValue;
                                var invalidLoginAttempts = !r.IsDBNull(22) ? r.GetInt32(22) : -1;
                                // ReSharper disable once InconsistentNaming
                                var useSSOLogin = !r.IsDBNull(23) && Formatting.TfStringToBool(r.GetString(23));


                                returnValue = new User
                                {
                                    Username = username,
                                    FullName = fullName,
                                    Description = description,
                                    PasswordHash = passwordHash,
                                    PasswordHashSalt = passwordHashSalt,
                                    IsAdmin = isAdmin,
                                    IsActive = isActive,
                                    IsLocked = isLocked,
                                    PasswordChangeRequired = passwordChangeRequired,
                                    PasswordCreateDatetime = passwordCreateDatetime,
                                    ActiveStartDate = activeStartDate,
                                    ActiveEndDate = activeEndDate,
                                    IgnoreMerchantrightsCust = ignoreMerchantrightsCust,
                                    InactiveAcctDisableDaysOverride = inactiveAcctDisableDaysOverride,
                                    HashType = hashType,
                                    IterationCount = iterationCount,
                                    LastLogoutDatetime = lastLogoutDatetime,
                                    AccountLockedOutDateTime = accountLockedOutDateTime,
                                    InvalidLoginAttempts = invalidLoginAttempts,
                                    ExternalClientId = externalClientId,
                                    ClientId = clientId,
                                    DomainId = domainId,
                                    UseSSOLogin = useSSOLogin
                                };
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
                return returnValue;
            }
        }

        /// <summary>
        /// Set or modify user information in the data layer.
        /// </summary>
        /// <param name="user">The user to modify.</param>
        public override void UserSet(User user)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "UserMaintenance.UsersSet", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        var saltString = Formatting.ByteArrayToCharAsString(user.PasswordHashSalt);
                        var isActive = user.IsActive ? "T" : "F";
                        var isLocked = user.IsLocked ? "T" : "F";
                        var passwordChangeRequired = user.PasswordChangeRequired ? "T" : "F";
                        var ignoreMerchantRights = user.IgnoreMerchantrightsCust ? "T" : "F";
                        var isAdmin = user.IsAdmin ? "T" : "F";

                        cmd.Parameters.Add(new OracleParameter("pUserName", OracleDbType.Varchar2, 15, user.Username, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pFullName", OracleDbType.Varchar2, 50, user.FullName, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pDescription", OracleDbType.Varchar2, 255, user.Description, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPasswordHash", OracleDbType = OracleDbType.Raw, Size = 32, Direction = ParameterDirection.Input, Value = user.PasswordHash });
                        cmd.Parameters.Add(new OracleParameter("pPasswordHashSalt", OracleDbType.Varchar2, 20, saltString, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIsActive", OracleDbType.Varchar2, 1, isActive, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIsLocked", OracleDbType.Varchar2, 1, isLocked, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pPasswordChangeRequired ", OracleDbType.Varchar2, 1, passwordChangeRequired, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pPasswordCreateDatetime", OracleDbType.Double, 14, user.PasswordCreateDatetime.ToOADate(), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pActiveStartDate", OracleDbType.Double, 14, user.ActiveStartDate?.ToOADate(), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pActiveEndDate", OracleDbType.Double, 14, user.ActiveEndDate?.ToOADate(), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pLastLogoutDatetime", OracleDbType.Double, 14, user.LastLogoutDatetime.ToOADate(), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIgnoreMerchantRightsCust", OracleDbType.Varchar2, 1, ignoreMerchantRights, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIsAdmin", OracleDbType.Varchar2, 13, isAdmin, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pInactiveAcctDisableDaysOvr", OracleDbType.Decimal, 4, user.InactiveAcctDisableDaysOverride, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pHashType", OracleDbType.Decimal, 38, user.HashType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIterationCount", OracleDbType.Decimal, 38, user.IterationCount, ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// This method sets the values into the Installed_Package table in Oracle
        /// </summary>
        /// <param name="connection">The database connection parameters</param>
        /// <param name="packageId">The package identity</param>
        /// <param name="packageNumber">The package number</param>
        /// <param name="packageName">The package name</param>
        /// <param name="packageVersion">The package version</param>
        /// <param name="databaseName">The database name</param>
        /// <param name="startDate">The start date of the install</param>
        /// <param name="endDate">The end date of the install</param>
        /// <param name="databaseSchema">The database schema</param>
        public override void InstallPackageSet(ConnectionInfo connection, String packageId, String packageNumber, String packageName, String packageVersion, String databaseName, DateTime startDate, DateTime endDate, String databaseSchema)
        {
            using (OracleConnection con = OracleConnectionGet(connection))
            {
                OracleCommand cmd = new OracleCommand { Connection = con, CommandText = string.Format("{0}.ToolFunctions.InstallPackageSet", databaseSchema), CommandType = CommandType.StoredProcedure };

                cmd.Parameters.Add(new OracleParameter { ParameterName = "pPackageId", OracleDbType = OracleDbType.Int64, Size = 20, Direction = ParameterDirection.Input, Value = packageId });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pPackageNumber", OracleDbType = OracleDbType.Int64, Size = 20, Direction = ParameterDirection.Input, Value = packageNumber });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDatabaseName", OracleDbType = OracleDbType.Varchar2, Size = 20, Direction = ParameterDirection.Input, Value = databaseName });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pPackageName", OracleDbType = OracleDbType.Varchar2, Size = 75, Direction = ParameterDirection.Input, Value = packageName });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pPackageVersion", OracleDbType = OracleDbType.Varchar2, Size = 10, Direction = ParameterDirection.Input, Value = packageVersion });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pInstallDateTimeStart", OracleDbType = OracleDbType.Date, Size = 0, Direction = ParameterDirection.Input, Value = startDate.ToString(CultureInfo.InvariantCulture) });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pInstallDateTimeEnd", OracleDbType = OracleDbType.Date, Size = 0, Direction = ParameterDirection.Input, Value = endDate.ToString(CultureInfo.InvariantCulture) });

                try
                {
                    // Open the database connection
                    con.Open();
                    // Execute the query
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Send a ping command to the database to ensure connectivity.  Ping command is "SELECT 'Ping' FROM DUAL".
        /// </summary>
        /// <returns><see cref="ActionResultToken"/> object with the results of the operation</returns>
        public override ActionResultToken PingDatabase()
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");

            // Start by assuming the worst
            ActionResultToken token = new ActionResultToken { ResultDomain = (int)DomainValue.DomainTransactApi, ResultDomainId = (int)Domain.Models.Definitions.ConnectionTest.Failure, Id = Guid.NewGuid().ToString(), Message = "Unable to connect." };

            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    using (OracleCommand cmd = new OracleCommand
                    {
                        Connection = con,
                        CommandType = CommandType.Text,
                        CommandText = @"SELECT 'Ping' FROM DUAL"
                    })
                    {
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                token.Message = r.GetString(0);
                                token.ResultDomainId = (int)Domain.Models.Definitions.ConnectionTest.Success;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    token.Message = Formatting.FormatException(ex);
                }
                finally
                {
                    con.Close();
                }
            }
            return token;
        }

        /// <summary>
        /// Fetch the System Features from the Oracle Database.
        /// </summary>
        /// <returns><see cref="SystemFeatures"/> object with the system features.</returns>
        public override SystemFeatures SystemFeaturesGet()
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");

            // Start by assuming the worst
            var features = new SystemFeatures();

            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand
                {
                    Connection = con,
                    CommandType = CommandType.Text,
                    CommandText = @"SELECT * FROM Envision.SystemFeatures"
                })
                {
                    try
                    {
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                if (!r.IsDBNull(0)) features.LicenseNumber = r.GetString(0);
                                if (!r.IsDBNull(1)) features.CampusName = r.GetString(1);
                                if (!r.IsDBNull(2)) features.NitelyPro = DateTime.FromOADate(r.GetDouble(2));
                                if (!r.IsDBNull(3)) features.CustNumMask = r.GetString(3);
                                if (!r.IsDBNull(4)) features.CardNumMask = r.GetString(4);
                                if (!r.IsDBNull(5)) features.MinLoginNameLength = r.GetInt32(5);
                                if (!r.IsDBNull(6)) features.CardDisplayMaskRightOffset = r.GetInt32(6);
                                if (!r.IsDBNull(7)) features.CardDisplayMaskLength = r.GetInt32(7);
                                if (!r.IsDBNull(8)) features.PersonaAccessPointLimit = r.GetInt32(8);
                                if (!r.IsDBNull(9)) features.SystemCurrencyValue = r.GetString(9);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return features;
        }

        #region Control Parameter

        /// <summary>
        /// Get a ControlParameter value from the ControlParameter ParamaterName.
        /// </summary>
        /// <param name="controlParameterName">Name of the parameter</param>
        /// <returns>Correspoding value as a <see cref="string"/></returns>
        public override string ControlParameterValueGet(string controlParameterName)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "ControlFunctions.ParameterValueGet", CommandType = CommandType.StoredProcedure })
                {
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pParameterName", OracleDbType = OracleDbType.Varchar2, Size = controlParameterName.Length, Direction = ParameterDirection.Input, Value = controlParameterName });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pParameterValue", OracleDbType = OracleDbType.Varchar2, Size = 2000, Direction = ParameterDirection.Output });

                    try
                    {
                        con.Open();
                        cmd.ExecuteNonQuery();

                        return cmd.Parameters["pParameterValue"].Value.ToString();
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Gets all control parameter groups
        /// </summary>
        /// <returns>A list of all control parameter groups</returns>
        public override List<string> GroupParameterListGet()
        {
            var list = new List<string>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "ControlFunctions.GroupParameterListGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(!r.IsDBNull(0) ? r.GetString(0) : string.Empty);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <summary>
        /// Gets control parameters by the name of group
        /// </summary>
        /// <param name="groupParameterName">Control parameter group name</param>
        /// <returns>A list of control parameters that belongs to specified group</returns>
        public override List<ControlParameter> ParameterListGet(string groupParameterName)
        {
            var list = new List<ControlParameter>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "ControlFunctions.ParameterListGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pGroupParameterName", OracleDbType = OracleDbType.Varchar2, Size = 100, Direction = ParameterDirection.Input, Value = groupParameterName });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new ControlParameter()
                                {
                                    ControlParameterId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    ParentControlParameterId = !r.IsDBNull(1) ? r.GetInt32(1) : -1,
                                    ParameterName = !r.IsDBNull(2) ? r.GetString(2) : string.Empty,
                                    ParameterValue = !r.IsDBNull(3) ? r.GetString(3) : string.Empty,
                                    ParameterDescription = !r.IsDBNull(4) ? r.GetString(4) : string.Empty,
                                    ParameterType = !r.IsDBNull(5) ? r.GetString(5) : string.Empty,
                                    ObjectId = !r.IsDBNull(6) ? r.GetInt32(6) : -1,
                                    ObjectName = !r.IsDBNull(7) ? r.GetString(7) : string.Empty,
                                    ModifiedDate = !r.IsDBNull(8) ? r.GetDateTime(8) : DateTime.MinValue
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <summary>
        /// Sets the control parameter in the resource layer
        /// </summary>
        /// <param name="request">The request containing the control parameter to set</param>
        /// <returns>Control parameter that has been set</returns>
        public override ControlParameter ParameterSet(ControlParameterPatchRequest request)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "ControlFunctions.ParameterSet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pParentControlParameterId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = request.ParentControlParameterId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pParameterName", OracleDbType = OracleDbType.Varchar2, Size = 100, Direction = ParameterDirection.Input, Value = request.ParameterName });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pParameterValue", OracleDbType = OracleDbType.Varchar2, Size = 2000, Direction = ParameterDirection.Input, Value = request.ParameterValue });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pParameterDescription", OracleDbType = OracleDbType.Varchar2, Size = 2000, Direction = ParameterDirection.Input, Value = request.ParameterDescription });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pParameterType", OracleDbType = OracleDbType.Varchar2, Size = 100, Direction = ParameterDirection.Input, Value = request.ParameterType });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pObjectId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = request.ObjectId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pObjectName", OracleDbType = OracleDbType.Varchar2, Size = 100, Direction = ParameterDirection.Input, Value = request.ObjectName });

                        con.Open();
                        cmd.ExecuteNonQuery();

                        return ParameterGet(request.ParameterName);
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException(request.RequestId ?? "", Formatting.FormatException(ex));
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Gets the control paramater by name
        /// </summary>
        /// <param name="parameterName">Name of the control parameter</param>
        /// <returns>Control parameter that matches the name</returns>
        public override ControlParameter ParameterGet(string parameterName)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "ControlFunctions.ParameterGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pParameterName", OracleDbType = OracleDbType.Varchar2, Size = 100, Direction = ParameterDirection.Input, Value = parameterName });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            if (r.Read())
                            {
                                return new ControlParameter()
                                {
                                    ControlParameterId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    ParentControlParameterId = !r.IsDBNull(1) ? r.GetInt32(1) : -1,
                                    ParameterName = !r.IsDBNull(2) ? r.GetString(2) : string.Empty,
                                    ParameterValue = !r.IsDBNull(3) ? r.GetString(3) : string.Empty,
                                    ParameterDescription = !r.IsDBNull(4) ? r.GetString(4) : string.Empty,
                                    ParameterType = !r.IsDBNull(5) ? r.GetString(5) : string.Empty,
                                    ObjectId = !r.IsDBNull(6) ? r.GetInt32(6) : -1,
                                    ObjectName = !r.IsDBNull(7) ? r.GetString(7) : string.Empty,
                                    ModifiedDate = !r.IsDBNull(8) ? r.GetDateTime(8) : DateTime.MinValue
                                };
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return null;
        }

        /// <summary>
        /// Delete a control parameter by name.
        /// </summary>
        /// <param name="parameterName">Name of the control parameter</param>
        public override void ParameterDelete(string parameterName)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "ControlFunctions.ParameterDelete", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pParameterName", OracleDbType = OracleDbType.Varchar2, Size = 100, Direction = ParameterDirection.Input, Value = parameterName });
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        #endregion

        #region Common object gets

        public override List<TsHostNames> HostNamesGet(ConnectionInfo connection)
        {
            List<TsHostNames> list = new List<TsHostNames>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "HostNames", CommandType = CommandType.TableDirect })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                // AttributeClause
                                TsHostNames item = new TsHostNames();
                                if (!r.IsDBNull(0)) item.Computer_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.ComputerName = r.GetString(1);
                                if (!r.IsDBNull(2)) item.StartingCommPort = r.GetInt32(2);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion


    }
}