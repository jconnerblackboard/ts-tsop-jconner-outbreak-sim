﻿using BbTS.Resource.OracleManagedDataAccessExtensions;
using System;
using System.Data;
using System.Diagnostics;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.EAccount;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Monitoring.Logging;
using Oracle.ManagedDataAccess.Client;


namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        /// <summary>
        /// This method will preauthorize a deposit from EAccounts
        /// </summary>
        /// <param name="request">Details of deposit to pre-authorize</param>
        /// <returns></returns>
        public override PreauthDepositResponse EAccountPreauthDeposit(PreauthDepositRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand("TransactionFunctions.ProcessTranDepositPreAuth",con) {CommandType = CommandType.StoredProcedure})
                {
                    try
                    {
                        cmd.AddInParam("pTerminalNumber", request.TerminalNumber);
                        if (request.ClientCustomerGuid != null)
                        {
                            cmd.AddInParam("pClientCustomerGuid", request.ClientCustomerGuid.Value.ToString("D"));
                        }
                        else
                        {
                            cmd.AddNullInParam("pClientCustomerGuid",OracleDbType.Varchar2);
                        }
                        if (request.CustId != null)
                        {
                            cmd.AddInParam("pCustId", request.CustId.Value);
                        }
                        else
                        {
                            cmd.AddNullInParam("pCustId",OracleDbType.Decimal);
                        }
                        cmd.AddInParam("pSVAccountTypeId", request.SVAccountTypeId);
                        cmd.AddInParam("pTransactionDate", request.TransactionDateTime.LocalDateTime);
                        cmd.AddInParam("pTransactionAmount", request.TransactionAmount);

                        cmd.AddOutParam("pAvailableBalance", OracleDbType.Int64, 10);
                        cmd.AddOutParam("pMerchantName", OracleDbType.Varchar2, 256 );
                        cmd.AddOutParam("pProfitCenterName", OracleDbType.Varchar2, 256);
                        cmd.AddOutParam("pPosName", OracleDbType.Varchar2, 256 );
                        cmd.AddOutParam("pCustNum", OracleDbType.Varchar2, 256 );
                        cmd.AddOutParam("pCardNum", OracleDbType.Varchar2, 256 );
                        cmd.AddOutParam("pEmailAddress", OracleDbType.Varchar2, 256 );
                        cmd.AddOutParam("pLastName", OracleDbType.Varchar2, 256 );
                        cmd.AddOutParam("pFirstName", OracleDbType.Varchar2, 256 );
                        cmd.AddOutParam("pMiddleName", OracleDbType.Varchar2, 256 );
                        cmd.AddOutParam("pErrorCode", OracleDbType.Int32, 10 );
                        cmd.AddOutParam("pDeniedText", OracleDbType.Varchar2, 256 );


                        con.Open();
                        cmd.ExecuteNonQuery();

                        var deniedText = cmd.GetOutParamString("pDeniedText");
                        var errorCode = cmd.GetOutParamInt32("pErrorCode") ?? 0;
                        if (errorCode != 0)
                        {
                            return new PreauthDepositResponse()
                            {
                                RequestId = request.RequestId,
                                DeniedText = deniedText,
                                ErrorCode = errorCode,
                            };
                        }
                        else
                        {
                            return new PreauthDepositResponse()
                            {
                                RequestId = request.RequestId,
                                DeniedText = deniedText,
                                ErrorCode = errorCode,
                                AvailableBalance = cmd.GetOutParamDecimal("pAvailableBalance") ?? 0,
                                MerchantName = cmd.GetOutParamString("pMerchantName"),
                                ProfitCenterName = cmd.GetOutParamString("pProfitCenterName"),
                                PosName = cmd.GetOutParamString("pPosName"),
                                CustNum = cmd.GetOutParamString("pCustNum"),
                                CardNum = cmd.GetOutParamString("pCardNum"),
                                EmailAddress = cmd.GetOutParamString("pEmailAddress"),
                                LastName = cmd.GetOutParamString("pLastName"),
                                FirstName = cmd.GetOutParamString("pFirstName"),
                                MiddleName = cmd.GetOutParamString("pMiddleName")
                            };
                        }
                    }
                    catch (Exception ex)
                    {
                        var message = Formatting.FormatException(ex);
                        var rex = new ResourceLayerException(string.Empty, message);
                        LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);

                        return new PreauthDepositResponse()
                        {
                            RequestId = request.RequestId
                        };
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// This method will perform a deposit from EAccounts
        /// </summary>
        /// <param name="request">Details of deposit to perform</param>
        /// <returns></returns>
        public override DepositResponse EAccountPerformDeposit(DepositRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand("TransactionFunctions.ProcessTransDeposit", con) { CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.AddInParam("pTerminalNumber", request.TerminalNumber);
                        if (request.ClientCustomerGuid != null)
                        {
                            cmd.AddInParam("pClientCustomerGuid", request.ClientCustomerGuid.Value.ToString("D"));
                        }
                        else
                        {
                            cmd.AddNullInParam("pClientCustomerGuid", OracleDbType.Varchar2);
                        }
                        if (request.CustId != null)
                        {
                            cmd.AddInParam("pCustId", request.CustId.Value);
                        }
                        else
                        {
                            cmd.AddNullInParam("pCustId", OracleDbType.Decimal);
                        }
                        cmd.AddInParam("pSVAccountTypeId", request.SVAccountTypeId);
                        cmd.AddInParam("pTransactionDate", request.TransactionDateTime.LocalDateTime);
                        cmd.AddInParam("pTransactionAmount", request.TransactionAmount);
                        cmd.AddInParam("pIsOnline", request.IsOnline ? "T" : "F");
                        cmd.AddInParam("pExternalClientTransactionGuid", request.ExternalClientTransactionGuid.ToString("D"));
                        cmd.AddInParam("pPaymentMethod",(int)request.PaymentMethod);
                        cmd.AddInParam("pGatewayTransactionReference", request.GatewayTransactionReference);
                        cmd.AddInParam("pMaskedCreditCardNumber", request.MaskedCreditCardNumber);

                        cmd.AddOutParam("pTransactionTotal", OracleDbType.Int64, 10);
                        cmd.AddOutParam("pTransactionNumber", OracleDbType.Int32, 7);
                        cmd.AddOutParam("pLocation",OracleDbType.Varchar2,256);
                        cmd.AddOutParam("pRetailTranCategoryText",OracleDbType.Varchar2,256);
                        cmd.AddOutParam("pRetailTranTypeText",OracleDbType.Varchar2,256);
                        cmd.AddOutParam("curTranSVAccounts",OracleDbType.RefCursor,0);
                        cmd.AddOutParam("pMerchantName", OracleDbType.Varchar2, 256);
                        cmd.AddOutParam("pProfitCenterName", OracleDbType.Varchar2, 256);
                        cmd.AddOutParam("pPosName", OracleDbType.Varchar2, 256);
                        cmd.AddOutParam("pCustNum", OracleDbType.Varchar2, 256);
                        cmd.AddOutParam("pCardNum", OracleDbType.Varchar2, 256);
                        cmd.AddOutParam("pEmailAddress", OracleDbType.Varchar2, 256);
                        cmd.AddOutParam("pLastName", OracleDbType.Varchar2, 256);
                        cmd.AddOutParam("pFirstName", OracleDbType.Varchar2, 256);
                        cmd.AddOutParam("pMiddleName", OracleDbType.Varchar2, 256);
                        cmd.AddOutParam("pPostTransactionDate", OracleDbType.Date, 0);
                        cmd.AddOutParam("pErrorCode", OracleDbType.Int32, 10);
                        cmd.AddOutParam("pDeniedText", OracleDbType.Varchar2, 256);


                        con.Open();
                        cmd.ExecuteNonQuery();

                        var deniedText = cmd.GetOutParamString("pDeniedText");
                        var errorCode = cmd.GetOutParamInt32("pErrorCode") ?? 0;
                        if (errorCode != 0)
                        {
                            return new DepositResponse()
                            {
                                RequestId = request.RequestId,
                                DeniedText = deniedText,
                                ErrorCode = errorCode,
                            };
                        }
                        else
                        {
                            var returnValue = new DepositResponse()
                            {
                                RequestId = request.RequestId,
                                DeniedText = deniedText,
                                ErrorCode = errorCode,
                                TransactionTotal = cmd.GetOutParamDecimal("pTransactionTotal").GetValueOrDefault(0),
                                TranNumber = cmd.GetOutParamInt32("pTransactionNumber")?.ToString(),
                                Location = cmd.GetOutParamString("pLocation"),
                                RetailTranCategoryText= cmd.GetOutParamString("pRetailTranCategoryText"),
                                RetailTranTypeText = cmd.GetOutParamString("pRetailTranTypeText"),
                                MerchantName = cmd.GetOutParamString("pMerchantName"),
                                ProfitCenterName = cmd.GetOutParamString("pProfitCenterName"),
                                PosName = cmd.GetOutParamString("pPosName"),
                                CustNum = cmd.GetOutParamString("pCustNum"),
                                CardNum = cmd.GetOutParamString("pCardNum"),
                                EmailAddress = cmd.GetOutParamString("pEmailAddress"),
                                LastName = cmd.GetOutParamString("pLastName"),
                                FirstName = cmd.GetOutParamString("pFirstName"),
                                MiddleName = cmd.GetOutParamString("pMiddleName"),
                                PostTransactionDateTime = new DateTimeOffset(cmd.GetOutParamDateTime("pPostTransactionDate")??DateTime.MinValue)
                            };
                            var curTranSvAccounts = cmd.GetOutParamRefCursor("curTranSVAccounts");
                            if (!curTranSvAccounts.IsNull)
                            {
                                using (var reader = curTranSvAccounts.GetDataReader())
                                {
                                    while (reader.Read())
                                    {
                                        var accountInfo = new SVAccountInfo
                                        {
                                            SVAccountTypeId = reader.GetInt32(0),
                                            Amount = reader.GetDecimal(1),
                                            DebitCreditType = reader.GetInt32(2),
                                            EnrichmentApplied = reader.GetString(3) == "T",
                                            EndingBalance = reader.GetDecimal(4)
                                        };
                                        returnValue.TransactionAccountInfo.Add(accountInfo);
                                    }
                                }
                            }
                            return returnValue;
                        }
                    }
                    catch (Exception ex)
                    {
                        var message = Formatting.FormatException(ex);
                        var rex = new ResourceLayerException(string.Empty, message);
                        LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);

                        return new DepositResponse()
                        {
                            RequestId = request.RequestId
                        };
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }
        /// <summary>
        /// Look up external client customer guid from other information.
        /// </summary>
        /// <param name="request">Customer informatino known for anon deposit.</param>
        /// <returns></returns>
        public override GetExternalCustGuidFromAnonResponse GetExternalCustGuidFromAnon(GetExternalCustGuidFromAnonRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand("TransactionFunctions.GetExternalCustGuidFromAnon", con) { CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.AddInParam("pTerminalNumber", request.TerminalNumber);
                        cmd.AddInOutParam("pCardNum", request.CardNum, 256);
                        cmd.AddInOutParam("pEmailAddress", request.EmailAddress, 256);
                        cmd.AddInOutParam("pCustNum", request.CustNum, 256);
                        cmd.AddInOutParam("pLastName", request.LastName, 256);
                        cmd.AddInOutParam("pFirstName", request.FirstName, 256);
                        cmd.AddInOutParam("pMiddleName", request.MiddleName, 256);
                        
                        cmd.AddOutParam("pClientCustomerGuid", OracleDbType.Varchar2, 256);
                        cmd.AddOutParam("pCustId", OracleDbType.Decimal, 10);
                        cmd.AddOutParam("pErrorCode", OracleDbType.Int32, 10);
                        cmd.AddOutParam("pDeniedText", OracleDbType.Varchar2, 256);


                        con.Open();
                        cmd.ExecuteNonQuery();

                        var deniedText = cmd.GetOutParamString("pDeniedText");
                        var errorCode = cmd.GetOutParamInt32("pErrorCode") ?? 0;
                        if (errorCode != 0)
                        {
                            return new GetExternalCustGuidFromAnonResponse()
                            {
                                DeniedText = deniedText,
                                ErrorCode = errorCode,
                            };
                        }
                        else
                        {
                            string clientCustomerString = cmd.GetOutParamString("pClientCustomerGuid");
                            return new GetExternalCustGuidFromAnonResponse()
                            {
                                DeniedText = deniedText,
                                ErrorCode = errorCode,
                                ClientCustomerGuid = clientCustomerString != null ? Guid.Parse(clientCustomerString) : (Guid?)null,
                                CustId = cmd.GetOutParamDecimal("pCustId")
                            };
                        }
                    }
                    catch (Exception ex)
                    {
                        var message = Formatting.FormatException(ex);
                        var rex = new ResourceLayerException(string.Empty, message);
                        LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);

                        return new GetExternalCustGuidFromAnonResponse();
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }
    }
}
