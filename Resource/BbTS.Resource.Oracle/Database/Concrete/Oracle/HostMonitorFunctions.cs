﻿using System;
using System.Collections.Generic;
using BbTS.Domain.Models.Messaging.HostMonitor;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        /// <summary>
        /// Write a host monitor message to the database.
        /// </summary>
        /// <param name="message">Message to write.</param>
        public override void HostMonitorMessageWrite(HostMonitorMessage message)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get all host monitor messages from the database.
        /// </summary>
        /// <returns>List of host monitor messages.</returns>
        public override List<HostMonitorMessage> HostMonitorMessagesGetAll()
        {
            throw new NotImplementedException();
        }
    }
}
