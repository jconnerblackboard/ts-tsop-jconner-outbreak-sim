﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using BbTS.Core;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.BoardPlan;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.System.Database;
using BbTS.Domain.Models.Transaction.Processing;
using Oracle.ManagedDataAccess.Client;
using BbTS.Domain.Models.Definitions.Transaction;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.Exceptions.Transaction;
using BbTS.Monitoring.Logging;
using Oracle.ManagedDataAccess.Types;
using BbTS.Domain.Models.Definitions.Pos;
using BbTS.Domain.Models.Exceptions;
using BbTS.Domain.Models.Transaction;
using BbTS.Resource.OracleManagedDataAccessExtensions;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        #region BoardMealTypes

        /// <summary>
        /// Get a list of all board cash equivalency periods for the device.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>List of all board cash equivalency periods for the device.</returns>
        public override BoardCashEquivalencyPeriodsGetResponse BoardCashEquivalencyPeriodsGet(BoardCashEquivalencyPeriodsGetRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "GETBRDCEPERIODINFO", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        var posId = PosIdFromOriginatorGuidGet(request.DeviceGuid);

                        cmd.Parameters.Add(new OracleParameter("POSNUM", OracleDbType.Int32, 10, posId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("InMODULE", OracleDbType.Varchar2, 1, "D", ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("DEFAULTBOARDPLAN_ID", OracleDbType.Int32, 6, request.DefaultBoardPlanId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("LASTPERIODNUM", OracleDbType.Int32, 4, request.LastPeriodNum, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("ERRORCODE", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("TOTALCOUNT", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("PERIODNUM", OracleDbType.Int32, 4, request.PeriodNum, ParameterDirection.InputOutput));

                        cmd.Parameters.Add(new OracleParameter("PERIODNAMEDAY1", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 30 });
                        cmd.Parameters.Add(new OracleParameter("BRDPERIODSTARTDAY1", OracleDbType.Double, ParameterDirection.Output) { Size = 14 });
                        cmd.Parameters.Add(new OracleParameter("BRDPERIODSTOPDAY1", OracleDbType.Double, ParameterDirection.Output) { Size = 14 });
                        cmd.Parameters.Add(new OracleParameter("CEPERIODSTARTDAY1", OracleDbType.Double, ParameterDirection.Output) { Size = 14 });
                        cmd.Parameters.Add(new OracleParameter("CEPERIODSTOPDAY1", OracleDbType.Double, ParameterDirection.Output) { Size = 14 });

                        cmd.Parameters.Add(new OracleParameter("PERIODNAMEDAY2", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 30 });
                        cmd.Parameters.Add(new OracleParameter("BRDPERIODSTARTDAY2", OracleDbType.Double, ParameterDirection.Output) { Size = 14 });
                        cmd.Parameters.Add(new OracleParameter("BRDPERIODSTOPDAY2", OracleDbType.Double, ParameterDirection.Output) { Size = 14 });
                        cmd.Parameters.Add(new OracleParameter("CEPERIODSTARTDAY2", OracleDbType.Double, ParameterDirection.Output) { Size = 14 });
                        cmd.Parameters.Add(new OracleParameter("CEPERIODSTOPDAY2", OracleDbType.Double, ParameterDirection.Output) { Size = 14 });

                        cmd.Parameters.Add(new OracleParameter("PERIODNAMEDAY3", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 30 });
                        cmd.Parameters.Add(new OracleParameter("BRDPERIODSTARTDAY3", OracleDbType.Double, ParameterDirection.Output) { Size = 14 });
                        cmd.Parameters.Add(new OracleParameter("BRDPERIODSTOPDAY3", OracleDbType.Double, ParameterDirection.Output) { Size = 14 });
                        cmd.Parameters.Add(new OracleParameter("CEPERIODSTARTDAY3", OracleDbType.Double, ParameterDirection.Output) { Size = 14 });
                        cmd.Parameters.Add(new OracleParameter("CEPERIODSTOPDAY3", OracleDbType.Double, ParameterDirection.Output) { Size = 14 });

                        cmd.Parameters.Add(new OracleParameter("PERIODNAMEDAY4", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 30 });
                        cmd.Parameters.Add(new OracleParameter("BRDPERIODSTARTDAY4", OracleDbType.Double, ParameterDirection.Output) { Size = 14 });
                        cmd.Parameters.Add(new OracleParameter("BRDPERIODSTOPDAY4", OracleDbType.Double, ParameterDirection.Output) { Size = 14 });
                        cmd.Parameters.Add(new OracleParameter("CEPERIODSTARTDAY4", OracleDbType.Double, ParameterDirection.Output) { Size = 14 });
                        cmd.Parameters.Add(new OracleParameter("CEPERIODSTOPDAY4", OracleDbType.Double, ParameterDirection.Output) { Size = 14 });

                        cmd.Parameters.Add(new OracleParameter("PERIODNAMEDAY5", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 30 });
                        cmd.Parameters.Add(new OracleParameter("BRDPERIODSTARTDAY5", OracleDbType.Double, ParameterDirection.Output) { Size = 14 });
                        cmd.Parameters.Add(new OracleParameter("BRDPERIODSTOPDAY5", OracleDbType.Double, ParameterDirection.Output) { Size = 14 });
                        cmd.Parameters.Add(new OracleParameter("CEPERIODSTARTDAY5", OracleDbType.Double, ParameterDirection.Output) { Size = 14 });
                        cmd.Parameters.Add(new OracleParameter("CEPERIODSTOPDAY5", OracleDbType.Double, ParameterDirection.Output) { Size = 14 });

                        cmd.Parameters.Add(new OracleParameter("PERIODNAMEDAY6", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 30 });
                        cmd.Parameters.Add(new OracleParameter("BRDPERIODSTARTDAY6", OracleDbType.Double, ParameterDirection.Output) { Size = 14 });
                        cmd.Parameters.Add(new OracleParameter("BRDPERIODSTOPDAY6", OracleDbType.Double, ParameterDirection.Output) { Size = 14 });
                        cmd.Parameters.Add(new OracleParameter("CEPERIODSTARTDAY6", OracleDbType.Double, ParameterDirection.Output) { Size = 14 });
                        cmd.Parameters.Add(new OracleParameter("CEPERIODSTOPDAY6", OracleDbType.Double, ParameterDirection.Output) { Size = 14 });

                        cmd.Parameters.Add(new OracleParameter("PERIODNAMEDAY7", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 30 });
                        cmd.Parameters.Add(new OracleParameter("BRDPERIODSTARTDAY7", OracleDbType.Double, ParameterDirection.Output) { Size = 14 });
                        cmd.Parameters.Add(new OracleParameter("BRDPERIODSTOPDAY7", OracleDbType.Double, ParameterDirection.Output) { Size = 14 });
                        cmd.Parameters.Add(new OracleParameter("CEPERIODSTARTDAY7", OracleDbType.Double, ParameterDirection.Output) { Size = 14 });
                        cmd.Parameters.Add(new OracleParameter("CEPERIODSTOPDAY7", OracleDbType.Double, ParameterDirection.Output) { Size = 14 });

                        cmd.Parameters.Add(new OracleParameter("OFFLNCEVALUE", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });

                        con.Open();
                        cmd.ExecuteNonQuery();
                        
                        var errorCode = cmd.Parameters["ERRORCODE"].Value.ToString();
                        
                        var periods = new BoardCashEquivalencyPeriodDay[7];
                        for (var i = 0; i < 7; ++i)
                        {
                            var name = cmd.Parameters[$"PERIODNAMEDAY{i + 1}"].Value.ToString();
                            var bstart = cmd.Parameters[$"BRDPERIODSTARTDAY{i + 1}"].Value.ToString();
                            var bend = cmd.Parameters[$"BRDPERIODSTOPDAY{i + 1}"].Value.ToString();
                            var cepstart = cmd.Parameters[$"CEPERIODSTARTDAY{i + 1}"].Value.ToString();
                            var cepend = cmd.Parameters[$"CEPERIODSTOPDAY{i + 1}"].Value.ToString();

                            bstart = string.IsNullOrWhiteSpace(bstart) || bstart == "null" ? null : bstart;
                            bend = string.IsNullOrWhiteSpace(bend) || bend == "null" ? null : bend;
                            cepstart = string.IsNullOrWhiteSpace(cepstart) || cepstart == "null" ? null : cepstart;
                            cepend = string.IsNullOrWhiteSpace(cepend) || cepend == "null" ? null : cepend;

                            periods[i] = new BoardCashEquivalencyPeriodDay
                            {
                                Name = name,
                                BoardPeriodStartTime = bstart != null ? new DateTime?(DateTime.FromOADate(double.Parse(bstart))) : null,
                                BoardPeriodStopTime = bend != null ? new DateTime?(DateTime.FromOADate(double.Parse(bend))) : null,
                                CashEquivPeriodStartTime = cepstart != null ? new DateTime?(DateTime.FromOADate(double.Parse(cepstart))) : null,
                                CashEquivPeriodStopTime = cepend != null ? new DateTime?(DateTime.FromOADate(double.Parse(cepend))) : null
                            };
                        }

                        var period = new BoardCashEquivalencyPeriod
                        {
                            PeriodNumber = request.PeriodNum,
                            Sunday = periods[0],
                            Monday = periods[1],
                            Tuesday = periods[2],
                            Wednesday = periods[3],
                            Thursday = periods[4],
                            Friday = periods[5],
                            Saturday = periods[6],
                            OfflineCashEquivValue = int.Parse(cmd.Parameters["OFFLNCEVALUE"].Value.ToString()) / 1000m
                        };
                        return new BoardCashEquivalencyPeriodsGetResponse
                        {
                            RequestId = request.RequestId,
                            ErrorCode = string.IsNullOrEmpty(errorCode) || errorCode == "null" ? 0 : Convert.ToInt32(errorCode),
                            BoardCashEquivalencyPeriod = period
                        };
                    }
                    catch (Exception ex)
                    {
                        var message = Formatting.FormatException(ex);
                        var lex = new LineItemProcessingException(
                            new LineItemProcessingResult(request.RequestId, 1005, message, TransactionLineItemType.LineItemTenderWithCustomer),
                            message);
                        LoggingManager.Instance.LogLineItemProcessingException(
                            lex, request, EventLogEntryType.Error,
                            LoggingDefinitions.Category.Resource, LoggingDefinitions.EventId.WebApiTransactionProcessing);
                        throw lex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Get board information for a customer for use on a specified device.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Board information for a customer for use on a specified device.</returns>
        public override BoardInformationGetResponse BoardInformationGet(BoardInformationGetRequest request)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "BoardFunctions.BoardInformationGet", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pOrignatorGuid", OracleDbType.Varchar2, 36, request.OriginatorGuid, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pCustomerGuid", OracleDbType.Varchar2, 36, request.CustomerGuid, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pForcePost", OracleDbType.Char, 1, Formatting.BooleanToTf(request.ForcePost), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pMealTypeId", OracleDbType.Int32, 6, request.MealTypeId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pDateTime", OracleDbType.Decimal, 14, request.TransactionDateTime.ToOADate(), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIsGuestMeal", OracleDbType.Char, 1, Formatting.BooleanToTf(request.IsGuestMeal), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pBoardPlanId", OracleDbType.Int32, 6, request.BoardPlanId, ParameterDirection.Input));

                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });

                        con.Open();
                        var tran = con.BeginTransaction(IsolationLevel.ReadCommitted);
                        cmd.Transaction = tran;
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new CustomerBoardCount();

                                if (!r.IsDBNull(0)) item.BoardPlanId = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.BoardPlanName = r.GetString(1);
                                if (!r.IsDBNull(2)) item.Priority = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.BoardPlanIsActive = Formatting.TfStringToBool(r.GetString(3));
                                if (!r.IsDBNull(4)) item.BoardPlanActiveFrom = DateTime.FromOADate(r.GetInt32(4));
                                if (!r.IsDBNull(5)) item.BoardPlanActiveTo = DateTime.FromOADate(r.GetInt32(5));
                                if (!r.IsDBNull(6)) item.CustomerBoardIsActive = Formatting.TfStringToBool(r.GetString(6));
                                if (!r.IsDBNull(7)) item.CustomerBoardActiveFrom = DateTime.FromOADate(r.GetInt32(7));
                                if (!r.IsDBNull(8)) item.CustomerBoardActiveTo = DateTime.FromOADate(r.GetInt32(8));
                                if (!r.IsDBNull(9)) item.RegularGridAllowed = r.GetInt32(9) != -2 ? r.GetInt32(9) : 999999;
                                if (!r.IsDBNull(10)) item.RegularGridUsed = r.GetInt32(10);
                                if (!r.IsDBNull(11)) item.RegularGridLeft = r.GetInt32(11) != -2 ? r.GetInt32(11) : 999999;
                                if (!r.IsDBNull(12)) item.RegularGridConsumed = r.GetInt32(12);
                                if (!r.IsDBNull(13)) item.GuestGridAllowed = r.GetInt32(13) != -2 ? r.GetInt32(13) : 999999;
                                if (!r.IsDBNull(14)) item.GuestGridUsed = r.GetInt32(14);
                                if (!r.IsDBNull(15)) item.GuestGridLeft = r.GetInt32(15) != -2 ? r.GetInt32(15) : 999999;
                                if (!r.IsDBNull(16)) item.GuestGridConsumed = r.GetInt32(16);
                                if (!r.IsDBNull(17)) item.PeriodAllowed = r.GetInt32(17) != -2 ? r.GetInt32(17) : 999999;
                                if (!r.IsDBNull(18)) item.PeriodUsed = r.GetInt32(18);
                                if (!r.IsDBNull(19)) item.PeriodLeft = r.GetInt32(19) != -2 ? r.GetInt32(19) : 999999;
                                if (!r.IsDBNull(20)) item.PeriodConsumed = r.GetInt32(20);
                                if (!r.IsDBNull(21)) item.DayAllowed = r.GetInt32(21) != -2 ? r.GetInt32(21) : 999999;
                                if (!r.IsDBNull(22)) item.DayUsed = r.GetInt32(22);
                                if (!r.IsDBNull(23)) item.DayLeft = r.GetInt32(23) != -2 ? r.GetInt32(23) : 999999;
                                if (!r.IsDBNull(24)) item.DayConsumed = r.GetInt32(24);
                                if (!r.IsDBNull(25)) item.WeekAllowed = r.GetInt32(25) != -2 ? r.GetInt32(25) : 999999;
                                if (!r.IsDBNull(26)) item.WeekUsed = r.GetInt32(26);
                                if (!r.IsDBNull(27)) item.WeekLeft = r.GetInt32(27) != -2 ? r.GetInt32(27) : 999999;
                                if (!r.IsDBNull(28)) item.WeekConsumed = r.GetInt32(28);
                                if (!r.IsDBNull(29)) item.MonthAllowed = r.GetInt32(29) != -2 ? r.GetInt32(29) : 999999;
                                if (!r.IsDBNull(30)) item.MonthUsed = r.GetInt32(30);
                                if (!r.IsDBNull(31)) item.MonthLeft = r.GetInt32(31) != -2 ? r.GetInt32(31) : 999999;
                                if (!r.IsDBNull(32)) item.MonthConsumed = r.GetInt32(32);
                                if (!r.IsDBNull(33)) item.SemesterQuarterAllowed = r.GetInt32(33) != -2 ? r.GetInt32(33) : 999999;
                                if (!r.IsDBNull(34)) item.SemesterQuarterUsed = r.GetInt32(34);
                                if (!r.IsDBNull(35)) item.SemesterQuarterLeft = r.GetInt32(35) != -2 ? r.GetInt32(35) : 999999;
                                if (!r.IsDBNull(36)) item.SemesterQuarterConsumed = r.GetInt32(36);
                                if (!r.IsDBNull(37)) item.YearAllowed = r.GetInt32(37) != -2 ? r.GetInt32(37) : 999999;
                                if (!r.IsDBNull(38)) item.YearUsed = r.GetInt32(38);
                                if (!r.IsDBNull(39)) item.YearLeft = r.GetInt32(39) != -2 ? r.GetInt32(39) : 999999;
                                if (!r.IsDBNull(40)) item.YearConsumed = r.GetInt32(40);
                                if (!r.IsDBNull(41)) item.GuestTotalAllowed = r.GetInt32(41) != -2 ? r.GetInt32(41) : 999999;
                                if (!r.IsDBNull(42)) item.GuestTotalUsed = r.GetInt32(42);
                                if (!r.IsDBNull(43)) item.GuestTotalLeft = r.GetInt32(43) != -2 ? r.GetInt32(43) : 999999;
                                if (!r.IsDBNull(44)) item.GuestTotalConsumed = r.GetInt32(44);
                                if (!r.IsDBNull(45)) item.ExtraAllowed = r.GetInt32(45) != -2 ? r.GetInt32(45) : 999999;
                                if (!r.IsDBNull(46)) item.ExtraUsed = r.GetInt32(46);
                                if (!r.IsDBNull(47)) item.ExtraLeft = r.GetInt32(47) != -2 ? r.GetInt32(47) : 999999;
                                if (!r.IsDBNull(48)) item.ExtraConsumed = r.GetInt32(48);
                                if (!r.IsDBNull(49)) item.TransferLeft = r.GetInt32(49) != -2 ? r.GetInt32(49) : 999999;
                                if (!r.IsDBNull(50)) item.TransferConsumed = r.GetInt32(50);
                                if (!r.IsDBNull(51)) item.ExtraTransferConsumedtype = r.GetInt32(51);
                                if (!r.IsDBNull(52)) item.TransferMealsBy = Formatting.ResetFrequencyStrToEnum(r.GetString(52));
                                if (!r.IsDBNull(53)) item.GuestReset = Formatting.ResetFrequencyStrToEnum(r.GetString(53));
                                if (!r.IsDBNull(54)) item.ExtraReset = Formatting.ResetFrequencyStrToEnum(r.GetString(54));
                                if (!r.IsDBNull(55)) item.LimitPeriodBy = Formatting.ResetFrequencyStrToEnum(r.GetString(55));
                                if (!r.IsDBNull(56)) item.ExtraIfExceeded = Formatting.ResetFrequencyStrToEnum(r.GetString(56));
                                if (!r.IsDBNull(57)) item.ExtraIfExceededControl = r.GetInt32(57);
                                if (!r.IsDBNull(58)) item.BpCashequivforGuest = Formatting.TfStringToBool(r.GetString(58));
                                if (!r.IsDBNull(59)) item.BpRegMealsBeforeGuest = Formatting.TfStringToBool(r.GetString(59));
                                if (!r.IsDBNull(60)) item.CashEquivalentAmount = r.GetInt32(60) / 1000m;
                                if (!r.IsDBNull(61)) item.BoardCountMaxAvailable = r.GetInt32(61);
                                if (!r.IsDBNull(62)) item.CurrentPeriod = r.GetInt32(62);
                                if (!r.IsDBNull(63)) item.ActualPeriod = r.GetInt32(63);
                                if (!r.IsDBNull(64)) item.DayOfWeek = r.GetInt32(64);
                                if (!r.IsDBNull(65)) item.TypeUsed = r.GetInt32(65);
                                if (!r.IsDBNull(66)) item.PeriodRolled = Formatting.TfStringToBool(r.GetString(66));
                                if (!r.IsDBNull(67)) item.DayRolled = Formatting.TfStringToBool(r.GetString(67));
                                if (!r.IsDBNull(68)) item.WeekRolled = Formatting.TfStringToBool(r.GetString(68));
                                if (!r.IsDBNull(69)) item.WeekRolledDate = DateTime.FromOADate(r.GetInt32(69));
                                if (!r.IsDBNull(70)) item.MonthRolled = Formatting.TfStringToBool(r.GetString(70));
                                if (!r.IsDBNull(71)) item.MonthRolledDate = DateTime.FromOADate(r.GetInt32(71));
                                if (!r.IsDBNull(72)) item.SemesterQuarterRolled = Formatting.TfStringToBool(r.GetString(72));
                                if (!r.IsDBNull(73)) item.SemesterQuarterRolledDate = DateTime.FromOADate(r.GetInt32(73));
                                if (!r.IsDBNull(74)) item.YearRolled = Formatting.TfStringToBool(r.GetString(74));
                                if (!r.IsDBNull(75)) item.YearRolledDate = DateTime.FromOADate(r.GetInt32(75));
                                if (!r.IsDBNull(76)) item.GuestRolled = Formatting.TfStringToBool(r.GetString(76));
                                if (!r.IsDBNull(77)) item.ExtraRolled = Formatting.TfStringToBool(r.GetString(77));
                                if (!r.IsDBNull(78)) item.ShowBoardCount = Formatting.TfStringToBool(r.GetString(78));
                                if (!r.IsDBNull(79)) item.PrintBoardCount = Formatting.TfStringToBool(r.GetString(79));
                                if (!r.IsDBNull(80)) item.MaxCountContribution = r.GetInt32(80);
                                if (!r.IsDBNull(81)) item.IsOnline = Formatting.TfStringToBool(r.GetString(81));
                                if (!r.IsDBNull(82)) item.ErrorCode = r.GetInt32(82);
                                if (!r.IsDBNull(83)) item.ErrorText = r.GetString(83);

                                return new BoardInformationGetResponse
                                {
                                    RequestId = request.RequestId,
                                    CustomerBoardCount = item
                                };
                            }
                            tran.Commit();
                        }

                    }
                    catch (Exception ex)
                    {
                        var message = Formatting.FormatException(ex);
                        var rex = new ResourceLayerException(request.RequestId, message);
                        throw rex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            var errorMessage = 
                @"Unable to locate board information for " +
                $"customer '{request.CustomerGuid}' on " +
                $"originator '{request.OriginatorGuid}' for " +
                $"board plan '{request.BoardPlanId}' and " +
                $"meal type '{request.MealTypeId}'.";
            throw new ResourceLayerException(
                request.RequestId,
                errorMessage,
                LoggingDefinitions.Category.Resource,
                LoggingDefinitions.EventId.ResourceLayerExceptionBoardInformationGetReturnedNoResults);
        }

        /// <summary>
        /// This method will return a list of records for the BoardMealTypes objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsBoardMealTypes> BoardMealTypesArchivesGet(ConnectionInfo connection)
        {
            var list = new List<TsBoardMealTypes>();

            using (OracleConnection con = connection == null ? OracleConnectionGet(ConnectionString) : OracleConnectionGet(connection))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = @"SELECT  BoardMt_Id,
                                          MtName,
                                          ApplyTo
                                  FROM    Envision.BoardMealTypes",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsBoardMealTypes();

                                if (!r.IsDBNull(0)) item.BoardMt_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.MtName = r.GetString(1);
                                if (!r.IsDBNull(2)) item.ApplyTo = r.GetInt32(2);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// This method will return a list of records for the BoardMealTypes objects permitted for the given deviceId
        /// </summary>
        /// <param name="deviceId">The unique identifier for the device.</param>
        /// <returns></returns>
        public override List<TsBoardMealTypes> BoardMealTypesPosGet(string deviceId)
        {
            var list = new List<TsBoardMealTypes>();

            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = @"
                        WITH POS_OPTION_ID AS (
                        SELECT    NVL(pos_tt.pos_option_id,pos_cr.pos_option_id) POS_OPTION_ID
                        FROM      Envision.Pos POS
                        JOIN      Envision.Originator       DEV ON  POS.OriginatorId         = DEV.OriginatorId
                        LEFT JOIN pos_tt ON POS.POS_ID = POS_TT.POS_ID
                        LEFT JOIN pos_cr ON POS.POS_ID = POS_CR.POS_ID
                        WHERE     DEV.OriginatorGuid = :pValue )
                        select BoardMt_Id,
                               MtName,
                               ApplyTo
                        from   boardmealtypes bmt
                        where  bmt.boardmt_id not in (select boardmt_id
                                                      from   pos_option_meal_type_disallow dis
                                                      join   POS_OPTION_ID ON dis.pos_option_id = pos_option_id.pos_option_id)",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        cmd.Parameters.Add(
                            new OracleParameter
                            {
                                ParameterName = "pValue",
                                OracleDbType = OracleDbType.Varchar2,
                                Size = 36,
                                Value = new Guid(deviceId).ToString("D"),
                                Direction = ParameterDirection.Input
                            });
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsBoardMealTypes();

                                if (!r.IsDBNull(0)) item.BoardMt_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.MtName = r.GetString(1);
                                if (!r.IsDBNull(2)) item.ApplyTo = r.GetInt32(2);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <inheritdoc />
        public override List<MealTypeDeviceSetting> BoardMealTypesGet()
        {
            var list = new List<MealTypeDeviceSetting>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "BoardFunctions.BoardMealTypesGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new MealTypeDeviceSetting()
                                {
                                    Id = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    Name = !r.IsDBNull(1) ? r.GetString(1) : null,
                                    ApplyTo = !r.IsDBNull(2) ? r.GetInt32(2) : -1
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <summary>
        /// Get a list of board periods for a device.
        /// </summary>
        /// <param name="deviceId">The unique identifier for the device.</param>
        /// <returns>list of <see cref="BoardPeriod"/></returns>
        public override List<BoardPeriod> BoardPeriodsDeviceSettingsGet(string deviceId)
        {
            var list = new List<BoardPeriod>();

            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = @"
                        SELECT      BPD.PERIOD,
                                    BPD.BPDAY,
                                    BPD.PROFITCENTER_ID,
                                    BPD.BOARDPERIODNAME,
                                    BPD.BOARDPERIODSTART,
                                    BPD.BOARDPERIODSTOP,
                                    BPD.CEPERIODSTART,
                                    BPD.CEPERIODSTOP 
                        FROM        Envision.BoardPeriods     BPD
                        INNER JOIN  Envision.Pos              POS ON  BPD.ProfitCenter_Id      = POS.ProfitCenter_Id  
                        INNER JOIN  Envision.Originator       DEV ON  POS.OriginatorId         = DEV.OriginatorId
                        WHERE       DEV.OriginatorGuid = :pValue",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        cmd.Parameters.Add(
                            new OracleParameter
                            {
                                ParameterName = "pValue",
                                OracleDbType = OracleDbType.Varchar2,
                                Size = 36,
                                Value = new Guid(deviceId).ToString("D"),
                                Direction = ParameterDirection.Input
                            });
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new BoardPeriod();

                                if (!r.IsDBNull(0)) item.Period = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.BoardPeriodDay = (DayOfWeek)(r.GetInt32(1) - 1);
                                if (!r.IsDBNull(2)) item.ProfitCenterId = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.BoardPeriodName = r.GetString(3);
                                if (!r.IsDBNull(4)) item.BoardPeriodStartDateTime = DateTime.FromOADate(r.GetDouble(4));
                                if (!r.IsDBNull(5)) item.BoardPeriodStopDateTime = DateTime.FromOADate(r.GetDouble(5));
                                if (!r.IsDBNull(6)) item.CePeriodStartDateTime = DateTime.FromOADate(r.GetDouble(6));
                                if (!r.IsDBNull(7)) item.CePeriodStopDateTime = DateTime.FromOADate(r.GetDouble(7));

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Get the default board plan id for the device given its unique identifier.
        /// </summary>
        /// <param name="originatorGuid">The unique identifier of the originator.</param>
        /// <returns>The default board plan id for the device.</returns>
        public override int? DefaultBoardPlanIdFromOriginatorGuid(string originatorGuid)
        {
            Guard.IsNotNullOrWhiteSpace(originatorGuid, "originatorGuid");

            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = @"
                        SELECT        OPT.DEFAULT_BOARDPLAN_ID
                        FROM          POS_OPTION            OPT
                        INNER JOIN    POS_TT                TT    ON TT.POS_OPTION_ID = OPT.POS_OPTION_ID
                        INNER JOIN    POS                   POS   ON POS.POS_ID = TT.POS_ID
                        INNER JOIN    Originator            ORG   ON ORG.OriginatorId = POS.OriginatorId
                        WHERE         ORG.OriginatorGuid = :pValue",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        cmd.Parameters.Add(
                            new OracleParameter
                            {
                                ParameterName = "pValue",
                                OracleDbType = OracleDbType.Varchar2,
                                Size = 36,
                                Value = new Guid(originatorGuid).ToString("D"),
                                Direction = ParameterDirection.Input
                            });

                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                if (!r.IsDBNull(0))
                                {
                                    return r.GetInt32(0);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return null;
        }

        #endregion

        #region BoardTransaction
        private static int TryGetIntValue(OracleParameter o,int defaultValue)
        {
            int? value = OptionalIntValue(o);
            if (value != null)
            {
                return value.Value;
            }
            else
            {
                return defaultValue;
            }
        }

        private static int? OptionalIntValue(OracleParameter o)
        {
            if ((o.OracleDbType == OracleDbType.Decimal || o.OracleDbType == OracleDbType.Int16 || o.OracleDbType == OracleDbType.Int32
                && o.Value is OracleDecimal))
            {
                var typedO = (OracleDecimal)o.Value;
                return typedO.IsNull ? null : (int?)typedO.ToInt32();
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Process a board transaction in the TS database.
        /// </summary>
        /// <param name="request">Request input parameters.</param>
        /// <returns>Response data for completing the transaction.</returns>
        public override BoardTransactionProcessResponse BoardTransactionProcess(BoardTransactionProcessRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "TransactionFunctions.BoardTransactionProcess", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        string paddedCardNumber = Formatting.PadCardNumber(request.CardNumber, 22, '0');
                        cmd.Parameters.Add(new OracleParameter("pDeviceGuid", OracleDbType.Varchar2, 36, request.DeviceGuid, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTransactionNumber", OracleDbType.Int32, 10, request.TransactionNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTransactionDateTime", OracleDbType.Double, 14, request.TransactionDateTime.DateTime.ToOADate(), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pKeyedOfflineFlag", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.KeyedOfflineFlag), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pOperatorGuid", OracleDbType.Varchar2, 36, request.OperatorGuid, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pCardNum", OracleDbType.Varchar2, 22, paddedCardNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIssueNumber", OracleDbType.Varchar2, 4, request.IssueNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIssueNumberCaptured", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.IssueNumberCaptured), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pCustEntryMethodType", OracleDbType.Int32, 4, request.CustEntryMethodType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pMealType", OracleDbType.Int32, 4, request.MealType, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pGuestMealFlag", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.GuestMealFlag), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pCountUsed", OracleDbType.Int32, 4, request.CountUsed, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pForcePost", OracleDbType.Varchar2, Formatting.BooleanToTf(request.IsForcePost), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pErrorCode", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pDeniedText", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 75 });
                        cmd.Parameters.Add(new OracleParameter("pRegGridLeft", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pGuestGridLeft", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pPeriodLeft", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pDayLeft", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pWeekLeft", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pMonthLeft", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pSemQtrLeft", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pYearLeft", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pGuestTotalLeft", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pExtraLeft", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pXFerLeft", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pTypeUsed", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pMealsLeftBy", OracleDbType.Int32, ParameterDirection.Output) { Size = 4 });
                        cmd.Parameters.Add(new OracleParameter("pShowBoardCount", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 1 });
                        cmd.Parameters.Add(new OracleParameter("pPrintBoardCount", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 1 });

                        con.Open();
                        cmd.ExecuteNonQuery();

                        var deniedText = cmd.Parameters["pDeniedText"].Value;
                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);
                        if (errorCode != 0)
                        {
                            return new BoardTransactionProcessResponse(request.RequestId)
                            {
                                DeniedText = deniedText.ToString(),
                                ErrorCode = errorCode
                            };
                        }
                        else
                        {
                            return new BoardTransactionProcessResponse(request.RequestId)
                            {
                                DeniedText = deniedText.ToString(),
                                ErrorCode = errorCode,
                                RegularGridLeft = TryGetIntValue(cmd.Parameters["pRegGridLeft"], 0),
                                GuestGridLeft = TryGetIntValue(cmd.Parameters["pGuestGridLeft"], 0),
                                PeriodLeft = TryGetIntValue(cmd.Parameters["pPeriodLeft"], 0),

                                DayLeft = TryGetIntValue(cmd.Parameters["pDayLeft"], 0),
                                WeekLeft = TryGetIntValue(cmd.Parameters["pWeekLeft"], 0),
                                MonthLeft = TryGetIntValue(cmd.Parameters["pMonthLeft"], 0),
                                SemesterQuarterLeft = TryGetIntValue(cmd.Parameters["pSemQtrLeft"], 0),
                                YearLeft = TryGetIntValue(cmd.Parameters["pYearLeft"], 0),
                                GuestTotalLeft = TryGetIntValue(cmd.Parameters["pGuestTotalLeft"], 0),
                                ExtraLeft = TryGetIntValue(cmd.Parameters["pExtraLeft"], 0),
                                TransferLeft = TryGetIntValue(cmd.Parameters["pXFerLeft"], 0),
                                TypeUsed = TryGetIntValue(cmd.Parameters["pTypeUsed"], 0),
                                DisplayMealsLeftBy = (MealsLeftBy)TryGetIntValue(cmd.Parameters["pMealsLeftBy"], 0),
                                ShowBoardCount = Formatting.TfStringToBool(cmd.Parameters["pShowBoardCount"].Value?.ToString()),
                                PrintBoardCount = Formatting.TfStringToBool(cmd.Parameters["pPrintBoardCount"].Value?.ToString())
                            };
                        }
                    }
                    catch (Exception ex)
                    {
                        var message = Formatting.FormatException(ex);
                        var rex = new ResourceLayerException(string.Empty, message);
                        LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);


                        return new BoardTransactionProcessResponse(request.RequestId)
                        {
                            DeniedText = message,
                            ErrorCode = 1005
                        };
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        #endregion

        #region Board plan

        /// <summary>
        /// Get list of board plan summaries
        /// </summary>
        /// <returns>Board plan summaries</returns>
        public override List<TsBoardPlanSummary> BoardPlanSummaryGetAll()
        {
            var list = new List<TsBoardPlanSummary>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "BoardFunctions.BoardPlanSummaryGetAll", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                               list.Add(new TsBoardPlanSummary()
                               {
                                   Id = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                   Name = !r.IsDBNull(1) ? r.GetString(1) : string.Empty,
                                   Active = !r.IsDBNull(2) ? (bool?)(r.GetString(2) == "T") : null,
                                   StartDate = !r.IsDBNull(3) ? (DateTime?)DateTime.FromOADate(r.GetFloat(3)) : null,
                                   StopDate = !r.IsDBNull(4) ? (DateTime?)DateTime.FromOADate(r.GetFloat(4)) : null
                               });
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <inheritdoc />
        public override List<BoardPlanExclusion> PeriodExclusionListGet(int customerId)
        {
            var list = new List<BoardPlanExclusion>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "BoardFunctions.PeriodExclusionListGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new BoardPlanExclusion()
                                {
                                    TransactionDateTime = !r.IsDBNull(0) ? (DateTime?)DateTime.FromOADate(r.GetDouble(0)) : null,
                                    TransactionNumber = !r.IsDBNull(1) ? r.GetInt32(1) : -1,
                                    PeriodExclusionId = !r.IsDBNull(2) ? r.GetDecimal(2) : -1,
                                    PeriodExclusionName = !r.IsDBNull(3) ? r.GetString(3) : null,
                                    ProfitCenterId = !r.IsDBNull(4) ? r.GetInt32(4) : -1,
                                    ProfitCenterName = !r.IsDBNull(5) ? r.GetString(5) : null,
                                    BoardPlanId = !r.IsDBNull(6) ? r.GetInt32(6) : -1,
                                    BoardPlanName = !r.IsDBNull(7) ? r.GetString(7) : null,
                                    MealPeriodId = !r.IsDBNull(8) ? r.GetInt32(8) : -1,
                                    MealPeriodName = !r.IsDBNull(9) ? r.GetString(9) : null,
                                    ExcludedProfitCenterList = !r.IsDBNull(10) ? r.GetString(10) : null,
                                    ExcludedBoardPlanList = !r.IsDBNull(11) ? r.GetString(11) : null,
                                    ExcludedMealPeriodList = !r.IsDBNull(12) ? r.GetString(12) : null
                                });
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <summary>
        /// Get a board plan by numerical identifier.
        /// </summary>
        /// <param name="id">numerical identifier.</param>
        /// <returns>Board plan or null if not found.</returns>
        public override BoardPlanVerifyResponse BoardPlanVerifyById(int id)
        {
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
            
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand
                {
                    Connection = con,
                    CommandType = CommandType.Text,
                    CommandText = @"
                                    SELECT  BP.BOARDPLAN_ID,
                                            BP.BOARDPLAN,
                                            BP.ISACTIVE,
                                            BP.ACTIVEFROM,
                                            BP.ACTIVETO,
                                            BP.BOARDPLANGUID
                                    FROM    Envision.BoardPlan BP
                                    WHERE   BP.BOARDPLAN_ID = :pValue"
                })
                {
                    try
                    {
                        cmd.AddInParam("pValue", id);
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var boardPlan = new BoardPlanVerifyResponse
                                {
                                    Id = -1,
                                    Name = null,
                                    IsActive = null,
                                    ActiveFrom = null,
                                    ActiveTo = null,
                                    BoardPlanGuid = null
                                };

                                if (!r.IsDBNull(0)) boardPlan.Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) boardPlan.Name = r.GetString(1);
                                if (!r.IsDBNull(2)) boardPlan.IsActive = Formatting.TfStringToBool(r.GetString(2));
                                if (!r.IsDBNull(3)) boardPlan.ActiveFrom = DateTime.FromOADate(r.GetDouble(3));
                                if (!r.IsDBNull(4)) boardPlan.ActiveTo = DateTime.FromOADate(r.GetDouble(4));
                                if (!r.IsDBNull(5)) boardPlan.BoardPlanGuid = new Guid(r.GetString(5));

                                if (boardPlan.Id == -1)
                                {
                                    throw new BoardPlanVerifyFailedException($"Board plan '{id}' was not found.");
                                }

                                return boardPlan;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new BoardPlanVerifyFailedException(Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return null;
        }


        /// <inheritdoc />
        public override List<CustomerTransactionLocationLog> CustomerBoardLocationLogsGet(DateTime startDate, DateTime endDate)
        {
            var list = new List<CustomerTransactionLocationLog>();

            using (var con = OracleConnectionGet(ConnectionString))
            {
                var cmdText = $@"
                    SELECT         DISTINCT
                                   BTS.InstitutionId,
                                   CUS.CustomerGuid,
                                   CUS.Custnum,
                                   MER.Name As MerchantName,
                                   PFC.Name As ProfitCenterName,
                                   POS.Name As PosName,
                                   POS.PosGuid as PosId,
                                   BRD.ActualDateTime AS DateTime
                    FROM           BoardTransactions BRD
                    INNER JOIN     Customer CUS ON CUS.Cust_Id = BRD.Cust_Id
                    INNER JOIN     POS ON POS.Pos_Id = BRD.Pos_Id
                    INNER JOIN     ProfitCenter PFC ON PFC.ProfitCenter_Id = BRD.ProfitCenter_Id
                    INNER JOIN     Merchant MER ON MER.Merchant_Id = PFC.Merchant_Id
                    INNER JOIN     BbSPTransactionSystem BTS ON BTS.IsValid = 'T'
                    WHERE          BRD.ActualDateTime > {startDate.ToOADate()}
                    AND            BRD.ActualDateTime < {endDate.ToOADate()}
                    ORDER BY       DateTime";
                using (var cmd = new OracleCommand { Connection = con, CommandText = cmdText, CommandType = CommandType.Text })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new CustomerTransactionLocationLog();
                                if (!r.IsDBNull(0)) item.InstitutionId = r.GetString(0);
                                if (!r.IsDBNull(1)) item.CustomerGuid = r.GetString(1);
                                if (!r.IsDBNull(2)) item.CustNum = r.GetString(2);
                                if (!r.IsDBNull(3)) item.MerchantName = r.GetString(3);
                                if (!r.IsDBNull(4)) item.ProfitCenterName = r.GetString(4);
                                if (!r.IsDBNull(5)) item.PosName = r.GetString(5);
                                if (!r.IsDBNull(6)) item.PosId = r.GetString(6);
                                if (!r.IsDBNull(7)) item.EntryDateTime = new DateTimeOffset(DateTime.FromOADate(r.GetDouble(7)), TimeZoneInfo.Local.BaseUtcOffset);
                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

    }
}
