﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Net;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Definitions.Transaction;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.Exceptions.WebApi;
using BbTS.Domain.Models.Product;
using BbTS.Domain.Models.Products;
using BbTS.Domain.Models.System.Database;
using BbTS.Monitoring.Logging;
using Oracle.ManagedDataAccess.Client;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        #region DsProductDetail

        /// <summary>
        /// This method will return a list of records for the DsProductDetail objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsDsProductDetail> DsProductDetailArchivesGet(ConnectionInfo connection)
        {
            var list = new List<TsDsProductDetail>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (var cmd = new OracleCommand {
                  Connection = con,
                  CommandText = @"SELECT  ProdDetail_Id,
                                          DsProductStatus_Id,
                                          Product_Id,
                                          ProfitCenter_Id,
                                          Location_Id,
                                          RetailPrice,
                                          RetailPrice_VariablePrice_Min,
                                          RetailPrice_VariablePrice_Max,
                                          UnitCost,
                                          Contribution,
                                          ForceReceipt,
                                          AllowOverride,
                                          DiscountQty1,
                                          DiscountPrice1,
                                          DiscountQty2,
                                          DiscountPrice2,
                                          DiscountQty3,
                                          DiscountPrice3,
                                          LastModifiedDateTime,
                                          AllowAutoDiscnt,
                                          AllowAutoSurchg,
                                          KeyTop1,
                                          KeyTop2,
                                          UseDefRemotePrn,
                                          RemotePrinter_Id
                                  FROM    Envision.DsProductDetail",
                  CommandType = CommandType.Text
                })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsDsProductDetail();

                                if (!r.IsDBNull(0)) item.ProdDetail_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.DsProductStatus_Id = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Product_Id = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.ProfitCenter_Id = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.Location_Id = r.GetInt32(4);
                                if (!r.IsDBNull(5)) item.RetailPrice = r.GetInt32(5);
                                if (!r.IsDBNull(6)) item.RetailPrice_VariablePrice_Min = r.GetInt32(6);
                                if (!r.IsDBNull(7)) item.RetailPrice_VariablePrice_Max = r.GetInt32(7);
                                if (!r.IsDBNull(8)) item.UnitCost = r.GetInt32(8);
                                if (!r.IsDBNull(9)) item.Contribution = r.GetInt32(9);
                                if (!r.IsDBNull(10)) item.ForceReceipt = r.GetString(10);
                                if (!r.IsDBNull(11)) item.AllowOverride = r.GetString(11);
                                if (!r.IsDBNull(12)) item.DiscountQty1 = r.GetInt32(12);
                                if (!r.IsDBNull(13)) item.DiscountPrice1 = r.GetInt32(13);
                                if (!r.IsDBNull(14)) item.DiscountQty2 = r.GetInt32(14);
                                if (!r.IsDBNull(15)) item.DiscountPrice2 = r.GetInt32(15);
                                if (!r.IsDBNull(16)) item.DiscountQty3 = r.GetInt32(16);
                                if (!r.IsDBNull(17)) item.DiscountPrice3 = r.GetInt32(17);
                                if (!r.IsDBNull(18)) item.LastModifiedDateTime = DateTime.FromOADate(r.GetDouble(18));
                                if (!r.IsDBNull(19)) item.AllowAutoDiscnt = r.GetString(19);
                                if (!r.IsDBNull(20)) item.AllowAutoSurchg = r.GetString(20);
                                if (!r.IsDBNull(21)) item.KeyTop1 = r.GetString(21);
                                if (!r.IsDBNull(22)) item.KeyTop2 = r.GetString(22);
                                if (!r.IsDBNull(23)) item.UseDefRemotePrn = r.GetString(23);
                                if (!r.IsDBNull(24)) item.RemotePrinter_Id = r.GetInt32(24);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }
        #endregion

        #region DsProductPromo

        /// <summary>
        /// This method will return a list of records for the DsProductPromo objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsDsProductPromo> DsProductPromoArchivesGet(ConnectionInfo connection)
        {
            var list = new List<TsDsProductPromo>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (var cmd = new OracleCommand {
                  Connection = con,
                  CommandText = @"SELECT  ProdPromo_Id,
                                          ProdPromo,
                                          ProfitCenter_Id,
                                          KeyTop1,
                                          KeyTop2
                                  FROM    Envision.DsProductPromo",
                  CommandType = CommandType.Text
                })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsDsProductPromo();

                                if (!r.IsDBNull(0)) item.ProdPromo_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.ProdPromo = r.GetString(1);
                                if (!r.IsDBNull(2)) item.ProfitCenter_Id = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.KeyTop1 = r.GetString(3);
                                if (!r.IsDBNull(4)) item.KeyTop2 = r.GetString(4);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Get a list of products for a specific device.
        /// </summary>
        /// <param name="deviceId">The unique identifier for the device.</param>
        /// <returns>A list of <see cref="Product"/> for the device.</returns>
        public override List<Product> ProductDeviceSettingsGet(string deviceId)
        {
            var list = new List<Product>();

            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = @"
                        SELECT          prod.product_id,
                                        proddetail_id,
                                        prodnum,
                                        proddescription,
                                        prodtype,
                                        taxschedule_id,
                                        taxgroup_id,
                                        retailprice,
                                        retailprice_variableprice_min,
                                        retailprice_variableprice_max,   
                                        forcereceipt,
                                        allowoverride,
                                        discountqty1,
                                        discountprice1,
                                        discountqty2,
                                        discountprice2,
                                        discountqty3,
                                        discountprice3,
                                        lastmodifieddatetime,
                                        allowautodiscnt,
                                        allowautosurchg 
                        FROM            dsproductdetail dtl
                        INNER JOIN      dsproducts prod on dtl.product_id = prod.product_id
                        INNER JOIN      dsproductstatus stat on dtl.dsproductstatus_id = stat.dsproductstatus_id
                        WHERE           dtl.profitcenter_id = :pValue",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        var profitCenterId = ProfitCenterFromDeviceGuidGet(deviceId);
                        cmd.Parameters.Add(
                            new OracleParameter
                            {
                                ParameterName = "pValue",
                                OracleDbType = OracleDbType.Int32,
                                Value = profitCenterId,
                                Direction = ParameterDirection.Input
                            });

                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                try
                                {
                                    var item = new Product();

                                    if (!r.IsDBNull(0)) item.ProductId = r.GetInt32(0);
                                    if (!r.IsDBNull(1)) item.ProductDetailId = r.GetInt32(1);
                                    if (!r.IsDBNull(2)) item.ProductNumber = r.GetString(2);
                                    if (!r.IsDBNull(3)) item.ProductDescription = r.GetString(3);
                                    if (!r.IsDBNull(4)) item.ProductType = (ProductType)r.GetInt32(4);
                                    if (!r.IsDBNull(5)) item.TaxScheduleId = r.GetInt32(5);
                                    if (!r.IsDBNull(6)) item.TaxGroupId = r.GetInt32(6);
                                    if (!r.IsDBNull(7)) item.RetailPrice = r.GetDecimal(7) / 1000m;
                                    if (!r.IsDBNull(8)) item.VariablePriceMinimum = r.GetDecimal(8) / 1000m;
                                    if (!r.IsDBNull(9)) item.VariablePriceMaximum = r.GetDecimal(9) / 1000m;
                                    if (!r.IsDBNull(10)) item.ForceReceipt = Formatting.TfStringToBool(r.GetString(10));
                                    if (!r.IsDBNull(11)) item.AllowOverride = Formatting.TfStringToBool(r.GetString(11));
                                    if (!r.IsDBNull(12)) item.DiscountQuantity1 = r.GetInt32(12);
                                    if (!r.IsDBNull(13)) item.DiscountPrice1 = r.GetDecimal(13) / 1000m;
                                    if (!r.IsDBNull(14)) item.DiscountQuantity2 = r.GetInt32(14);
                                    if (!r.IsDBNull(15)) item.DiscountPrice2 = r.GetDecimal(15) / 1000m;
                                    if (!r.IsDBNull(16)) item.DiscountQuantity3 = r.GetInt32(16);
                                    if (!r.IsDBNull(17)) item.DiscountPrice3 = r.GetDecimal(17) / 1000m;
                                    if (!r.IsDBNull(18)) item.LastModified = DateTime.FromOADate(r.GetDouble(18));
                                    if (!r.IsDBNull(19)) item.AllowPolicyDiscount = Formatting.TfStringToBool(r.GetString(19));
                                    if (!r.IsDBNull(20)) item.AllowPolicySurcharge = Formatting.TfStringToBool(r.GetString(20));

                                    list.Add(item);
                                }
                                catch (Exception ex)
                                {
                                    var rex = new ResourceLayerException(deviceId, Formatting.FormatException(ex));
                                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        #endregion

    }
}