﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using BbTS.Core;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Configuration;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Definitions.Transaction;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.Exceptions.Transaction;
using BbTS.Domain.Models.Terminal;
using BbTS.Domain.Models.Transaction.Processing;
using BbTS.Monitoring.Logging;
using BbTS.Resource.OracleManagedDataAccessExtensions;
using Oracle.ManagedDataAccess.Client;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        /// <inheritdoc />
        public override List<PaymentExpressGroupAccount> PaymentExpressGroupAccountGet(string name = null)
        {
            var list = new List<PaymentExpressGroupAccount>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "PaymentExpress.GroupAccountListGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pName", OracleDbType = OracleDbType.Varchar2, Size = 32, Direction = ParameterDirection.Input, Value = name });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var paymentExpressGroupAccountId = !r.IsDBNull(0) ? r.GetInt32(0) : -1;
                                var groupName = !r.IsDBNull(1) ? r.GetString(1) : string.Empty;
                                var devicePrefix = !r.IsDBNull(2) ? r.GetString(2) : string.Empty;

                                var paymentExpressGroupAccount = new PaymentExpressGroupAccount()
                                {
                                    PaymentExpressGroupAccountId = paymentExpressGroupAccountId,
                                    Name = groupName,
                                    DevicePrefix = devicePrefix
                                };
                                list.Add(paymentExpressGroupAccount);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <summary>
        /// Persist EMV TxnPur request messages to the data layer.
        /// </summary>
        /// <param name="requestId">Unique identifer associated with the service layer request.</param>
        /// <param name="request">The request containing the line item information for EMV.</param>
        public override LineItemProcessingResult EmvTxnPurRequestSet(string requestId, RetailTransactionLineTenderCreditCardRequest request)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "PaymentExpress.PurchaseRequest", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pTransactionReference", OracleDbType.Varchar2, 36, request.LineItemTenderEmv.TransactionReference, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAmountRequested", OracleDbType.Int32, 7, request.LineItemTenderEmv.AmountRequested * 100, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pMerchantReference", OracleDbType.Varchar2, 64, request.LineItemTenderEmv.TransactionReference, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTerminalId", OracleDbType.Int32, 10, request.TerminalId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pRequestDateTime", OracleDbType.TimeStamp, 6, request.LineItemTenderEmv.PaymentExpressRequestDateTime.ToLocalTime(), ParameterDirection.Input));
                        cmd.AddInParam("pDeviceId", request.LineItemTenderEmv.EmvDeviceId);

                        con.Open();
                        cmd.ExecuteNonQuery();

                        return new LineItemProcessingResult(requestId, 0, "Success.", TransactionLineItemType.LineItemTenderEmvTxnReq);
                    }
                    catch (Exception ex)
                    {
                        var message = Formatting.FormatException(ex);
                        var lex = new LineItemProcessingException(
                            new LineItemProcessingResult(requestId, 1005, message, TransactionLineItemType.LineItemTenderEmvTxnReq),
                            message);
                        LoggingManager.Instance.LogLineItemProcessingException(
                            lex, request, EventLogEntryType.Error,
                            LoggingDefinitions.Category.Resource, LoggingDefinitions.EventId.WebApiTransactionProcessing);
                        throw lex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Persist EMV TxnPur response messages to the data layer
        /// </summary>
        /// <param name="requestId">Unique identifer associated with the service layer request.</param>
        /// <param name="request">The request containing the line item information for EMV.</param>
        public override LineItemProcessingResult EmvTxnPurResponseSet(string requestId, RetailTransactionLineTenderCreditCardRequest request)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "PaymentExpress.PurchaseResponse", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pTransactionReference", OracleDbType.Varchar2, 36, request.LineItemTenderEmv.TransactionReference, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pPurchaseResponseCode", OracleDbType.Char, 2, request.LineItemTenderEmv.PurchaseResponseCode, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAmountRequested", OracleDbType.Int32, 7, request.LineItemTenderEmv.AmountRequested * 100, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pDpsTransactionReference", OracleDbType.Varchar2, 36, request.LineItemTenderEmv.DpsTransactionReference, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTerminalId", OracleDbType.Int32, 10, request.TerminalId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pResponseDateTime", OracleDbType.TimeStamp, 6, request.LineItemTenderEmv.PaymentExpressResponseDateTime.ToLocalTime(), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pRespActionResultTokenDomain", OracleDbType.Int32, 7, request.LineItemTenderEmv.Result.ResultDomain, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pRespActionResultTokenDomainId", OracleDbType.Int32, 7, request.LineItemTenderEmv.Result.ResultDomainId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pRespActionResultTokenId", OracleDbType.Varchar2, 36, request.LineItemTenderEmv.Result.Id, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pRespActionResultTokenMessage", OracleDbType.Varchar2, 500, request.LineItemTenderEmv.Result.Message, ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();

                        return new LineItemProcessingResult(requestId, 0, "Success.", TransactionLineItemType.LineItemTenderEmvTxnResp);
                    }
                    catch (Exception ex)
                    {
                        var message = Formatting.FormatException(ex);
                        var lex = new LineItemProcessingException(
                            new LineItemProcessingResult(requestId, 1005, message, TransactionLineItemType.LineItemTenderEmvTxnResp),
                            message);
                        LoggingManager.Instance.LogLineItemProcessingException(
                            lex, request, EventLogEntryType.Error,
                            LoggingDefinitions.Category.Resource, LoggingDefinitions.EventId.WebApiTransactionProcessing);
                        throw lex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Persist EMV Get1 response messages to the data layer
        /// </summary>
        /// <param name="requestId">Unique identifer associated with the service layer request.</param>
        /// <param name="request">The request containing the line item information for EMV.</param>
        public override LineItemProcessingResult EmvTxnGet1ResponseSet(string requestId, RetailTransactionLineTenderCreditCardRequest request)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "PaymentExpress.PurchaseComplete", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pTransactionReference", OracleDbType.Varchar2, 36, request.LineItemTenderEmv.TransactionReference, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pCommandSequence", OracleDbType.Int32, 6, request.LineItemTenderEmv.CommandSequence, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pPurchaseResponseCode", OracleDbType.Char, 2, request.LineItemTenderEmv.PurchaseResponseCode, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pCardSuffix", OracleDbType.Char, 4, request.LineItemTenderEmv.CardSuffix, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pCardId", OracleDbType.Int32, 10, request.LineItemTenderEmv.CardId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAmountRequested", OracleDbType.Int32, 7, request.LineItemTenderEmv.AmountRequested * 100, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAmountAuthorized", OracleDbType.Int32, 7, null, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTransactionStateId", OracleDbType.Int32, 10, null, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pStan", OracleDbType.Int32, 6, request.LineItemTenderEmv.Stan, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pSettlementDate", OracleDbType.TimeStamp, 6, request.LineItemTenderEmv.SettlementDateTime.ToLocalTime(), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pAuthorizationCode", OracleDbType.Char, 6, request.LineItemTenderEmv.AuthorizationCode, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pDpsTransactionReference", OracleDbType.Varchar2, 36, request.LineItemTenderEmv.DpsTransactionReference, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTransactionResponseCode", OracleDbType.Char, 2, null, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pMerchantReference", OracleDbType.Varchar2, 64, request.LineItemTenderEmv.TransactionReference, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTransactionType", OracleDbType.Int32, 10, null, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTransactionTime", OracleDbType.TimeStamp, 6, request.TransactionDateTime.ToLocalTime(), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pMaskedPan", OracleDbType.Varchar2, 16, request.LineItemTenderEmv.MaskedPan, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTerminalId", OracleDbType.Int32, 10, request.TerminalId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pGet1DateTime", OracleDbType.TimeStamp, 6, request.LineItemTenderEmv.Get1DateTime.ToLocalTime(), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pGet1ActionResultTokenDomain", OracleDbType.Int32, 7, request.LineItemTenderEmv.Get1Result.ResultDomain, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pGet1ActionResultTokenDomainId", OracleDbType.Int32, 7, request.LineItemTenderEmv.Get1Result.ResultDomainId, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pGet1ActionResultTokenId", OracleDbType.Varchar2, 36, request.LineItemTenderEmv.Get1Result.Id, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pGet1ActionResultTokenMessage", OracleDbType.Varchar2, 500, request.LineItemTenderEmv.Get1Result.Message, ParameterDirection.Input));

                        con.Open();
                        cmd.ExecuteNonQuery();

                        return new LineItemProcessingResult(requestId, 0, "Success.", TransactionLineItemType.LineItemTenderEmvGet1Resp);
                    }
                    catch (Exception ex)
                    {
                        var message = Formatting.FormatException(ex);
                        var lex = new LineItemProcessingException(
                            new LineItemProcessingResult(requestId, 1005, message, TransactionLineItemType.LineItemTenderEmvGet1Resp),
                            message);
                        LoggingManager.Instance.LogLineItemProcessingException(
                            lex, request, EventLogEntryType.Error,
                            LoggingDefinitions.Category.Resource, LoggingDefinitions.EventId.WebApiTransactionProcessing);
                        throw lex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <inheritdoc/>
        public override LineItemProcessingResult EmvHitTransactionRecord(string requestId, RetailTransactionLineTenderCreditCardHitRequest request)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "PaymentExpress.HitTransactionRecord", CommandType = CommandType.StoredProcedure, BindByName = true })
                {
                    try
                    {
                        cmd.Parameters.AddInParam("pTransactionReference",request.LineItemTenderEmvHit.TransactionReference);
                        cmd.Parameters.AddInParam("pPurchaseResponseCode", request.LineItemTenderEmvHit.PurchaseResponseCode ?? "--");
                        cmd.Parameters.AddInParam("pCardId",(int)request.LineItemTenderEmvHit.CardId);
                        cmd.Parameters.AddInParam("pAmountRequested",request.LineItemTenderEmvHit.AmountRequested * 100);
                        cmd.Parameters.AddInParam("pAmountAuthorized",request.LineItemTenderEmvHit.AmountAuthorized * 100);
                        cmd.Parameters.AddNullInParam("pTransactionStateId",OracleDbType.Int32);
                        cmd.Parameters.AddInParam("pStan", request.LineItemTenderEmvHit.SystemTraceAuditNumber);
                        cmd.Parameters.AddInParam("pSettlementDate", request.LineItemTenderEmvHit.SettlementDateTime);
                        cmd.Parameters.AddInParam("pAuthorizationCode", request.LineItemTenderEmvHit.AuthorizationCode);
                        cmd.Parameters.AddInParam("pDpsTransactionReference", request.LineItemTenderEmvHit.DpsTransactionReference);
                        cmd.Parameters.AddInParam("pMerchantReference", request.LineItemTenderEmvHit.MerchantReference);
                        cmd.Parameters.AddInParam("pMaskedPan", request.LineItemTenderEmvHit.MaskedPan);
                        cmd.Parameters.AddInParam("pTerminalId", request.TerminalId);
                        cmd.Parameters.AddInParam("pDeviceId", request.LineItemTenderEmvHit.EmvDeviceId);

                        cmd.AddInParam("pRequestDateTime", request.LineItemTenderEmvHit.PaymentExpressRequestDateTime);
                        cmd.Parameters.AddInParam("pResponseDateTime", request.LineItemTenderEmvHit.PaymentExpressResponseDateTime);

                        cmd.AddOutParam("pErrorCode",OracleDbType.Int32,0);
                        cmd.AddOutParam("pErrorText",OracleDbType.Varchar2,255);

                        con.Open();
                        cmd.ExecuteNonQuery();

                        return new LineItemProcessingResult(requestId, 0, "Success.", TransactionLineItemType.LineItemTenderEmvHitRecord);
                    }
                    catch (Exception ex)
                    {
                        var message = Formatting.FormatException(ex);
                        var lex = new LineItemProcessingException(
                            new LineItemProcessingResult(requestId, 1005, message, TransactionLineItemType.LineItemTenderEmvHitRecord),
                            message);
                        LoggingManager.Instance.LogLineItemProcessingException(
                            lex, request, EventLogEntryType.Error,
                            LoggingDefinitions.Category.Resource, LoggingDefinitions.EventId.WebApiTransactionProcessing);
                        throw lex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Retrieve EMV settings for HIT operations.
        /// </summary>
        /// <param name="PosGuid"></param>
        /// <returns>EmvHitSettings for POS.</returns>
        public override EmvHitSettings EmvGatewaySettingsHitGet(string PosGuid)
        {
            EmvHitSettings settings = new EmvHitSettings();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "SystemFunctions.EmvGatewaySettingsHITGet", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.AddInParam("pReturnAll", "0");
                        cmd.AddInParam("pPosGuid", PosGuid);
                        cmd.AddOutParam("pList", OracleDbType.RefCursor, 0);
                        con.Open();
                        using (OracleDataReader r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                settings.Environment = r.GetString("Environment");
                                settings.Url = r.GetString("Url");
                                settings.HitUserName = r.GetString("HitUserName");
                                settings.HitKey = r.GetString("HitKey");
                                settings.Station = r.GetString("Station");
                                settings.CurrencyCode = r.GetString("CurrencyCode");
                                settings.DeviceId = r.GetString("DeviceId");
                                settings.VendorId = r.GetString("VendorId");
                            }
                        }
                    }
                    catch (Exception )
                    {
                        return null;
                    }
                }
            }
            return settings;
        }
    }
}