﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.System;
using BbTS.Domain.Models.System.Database;
using Oracle.ManagedDataAccess.Client;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        /// <summary>
        /// This method deletes user password history
        /// </summary>
        /// <param name="connection">The database connection parameters</param>
        /// <param name="databaseSchema">The schema of the database</param>
        /// <param name="user">The user's history to be deleted</param>
        public override void UserPasswordHistoryDelete(ConnectionInfo connection, string databaseSchema, User user)
        {
            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = string.Format("{0}.UserMaintenance.UsersPasswordHistoryDelete", databaseSchema), CommandType = CommandType.StoredProcedure })
                {
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pUserName", OracleDbType = OracleDbType.Varchar2, Size = 15, Direction = ParameterDirection.Input, Value = user.Username });

                    try
                    {
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// This method retrieves the list of users from the database
        /// </summary>
        /// <param name="connection">The database connection parameters</param>
        /// <param name="databaseSchema">The database schema</param>
        /// <returns></returns>
        public override List<User> UsersGet(ConnectionInfo connection, string databaseSchema)
        {
            List<User> users = new List<User>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = string.Format("{0}.UserMaintenance.UsersGet", databaseSchema), CommandType = CommandType.StoredProcedure })
                {

                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pUserName", OracleDbType = OracleDbType.Varchar2, Size = 15, Direction = ParameterDirection.Input, Value = string.Empty });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pUsers", OracleDbType = OracleDbType.RefCursor, Size = 0, Direction = ParameterDirection.Output, Value = string.Empty });

                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {

                                User user = new User();

                                if (!r.IsDBNull(0))
                                    user.Username = r.GetString(0);

                                if (!r.IsDBNull(1))
                                    user.FullName = r.GetString(1);

                                if (!r.IsDBNull(2))
                                    user.Description = r.GetString(2);

                                if (!r.IsDBNull(3))
                                    user.PasswordHash = (byte[])r.GetValue(3);

                                if (!r.IsDBNull(4))
                                    user.PasswordHashSalt = Encoding.ASCII.GetBytes(r.GetString(4));

                                if (!r.IsDBNull(5))
                                    user.IsActive = r.GetString(5).Equals("T");

                                if (!r.IsDBNull(6))
                                    user.IsLocked = r.GetString(6).Equals("T");

                                if (!r.IsDBNull(7))
                                    user.PasswordChangeRequired = r.GetString(7).Equals("T");

                                if (!r.IsDBNull(8))
                                    user.PasswordCreateDatetime = DateTime.FromOADate(r.GetDouble(8));

                                if (!r.IsDBNull(9))
                                    user.ActiveStartDate = DateTime.FromOADate(r.GetDouble(9));

                                if (!r.IsDBNull(10))
                                    user.ActiveEndDate = DateTime.FromOADate(r.GetDouble(10));

                                if (!r.IsDBNull(17))
                                    user.LastLogoutDatetime = DateTime.FromOADate(r.GetDouble(17));

                                if (!r.IsDBNull(12))
                                    user.IgnoreMerchantrightsCust = r.GetString(12).Equals("T");

                                if (!r.IsDBNull(13))
                                    user.IsAdmin = r.GetString(13).Equals("T");

                                if (!r.IsDBNull(14))
                                    user.InactiveAcctDisableDaysOverride = r.GetInt32(14);

                                if (!r.IsDBNull(15))
                                    user.HashType = r.GetInt32(15);

                                if (!r.IsDBNull(16))
                                    user.IterationCount = r.GetInt32(16);

                                if (!r.IsDBNull(18))
                                    user.AccountLockedOutDateTime = DateTime.FromOADate(r.GetDouble(18));

                                if (!r.IsDBNull(19))
                                    user.InvalidLoginAttempts = r.GetInt32(19);

                                users.Add(user);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            return users;
        }

        /// <summary>
        /// This method sets the values for a user
        /// </summary>
        /// <param name="connection">The database connection parameters</param>
        /// <param name="databaseSchema">The schema of the database</param>
        /// <param name="user">The user object</param>
        public override void UserSet(ConnectionInfo connection, string databaseSchema, User user)
        {
            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = string.Format("{0}.UserMaintenance.UsersSet", databaseSchema), CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pUserName", OracleDbType = OracleDbType.Varchar2, Size = 15, Direction = ParameterDirection.Input, Value = user.Username });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pFullName", OracleDbType = OracleDbType.Varchar2, Size = 50, Direction = ParameterDirection.Input, Value = !string.IsNullOrEmpty(user.FullName) ? user.FullName : string.Empty });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDescription", OracleDbType = OracleDbType.Varchar2, Size = 225, Direction = ParameterDirection.Input, Value = !string.IsNullOrEmpty(user.Description) ? user.Description : string.Empty });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPasswordHash", OracleDbType = OracleDbType.Raw, Size = 32, Direction = ParameterDirection.Input, Value = user.PasswordHash });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPasswordHashSalt", OracleDbType = OracleDbType.Varchar2, Size = 20, Direction = ParameterDirection.Input, Value = new string(user.PasswordHashSalt.Select(Convert.ToChar).ToArray()) });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pIsActive", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = user.IsActive.Equals(true) ? "T" : "F" });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pIsLocked", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = user.IsLocked.Equals(true) ? "T" : "F" });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPasswordChangeRequired", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = user.IsLocked.Equals(true) ? "T" : "F" });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPasswordCreateDatetime", OracleDbType = OracleDbType.Decimal, Size = 126, Direction = ParameterDirection.Input, Value = DateTime.Parse(user.PasswordCreateDatetime.ToString()).ToOADate() });

                        if (user.ActiveStartDate != null)
                            cmd.Parameters.Add(new OracleParameter { ParameterName = "pActiveStartDate", OracleDbType = OracleDbType.Decimal, Size = 126, Direction = ParameterDirection.Input, Value = DateTime.Parse(user.ActiveStartDate.ToString()).ToOADate() });
                        else
                            cmd.Parameters.Add(new OracleParameter { ParameterName = "pActiveStartDate", OracleDbType = OracleDbType.Decimal, Size = 126, Direction = ParameterDirection.Input });

                        if (user.ActiveEndDate != null)
                            cmd.Parameters.Add(new OracleParameter { ParameterName = "pActiveEndDate", OracleDbType = OracleDbType.Decimal, Size = 126, Direction = ParameterDirection.Input, Value = DateTime.Parse(user.ActiveEndDate.ToString()).ToOADate() });
                        else
                            cmd.Parameters.Add(new OracleParameter { ParameterName = "pActiveEndDate", OracleDbType = OracleDbType.Decimal, Size = 126, Direction = ParameterDirection.Input });

                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pLastLogoutDatetime", OracleDbType = OracleDbType.Decimal, Size = 126, Direction = ParameterDirection.Input, Value = DateTime.Parse(user.LastLogoutDatetime.ToString()).ToOADate() });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pIgnoreMerchantRightsCust", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = user.IgnoreMerchantrightsCust.Equals(true) ? "T" : "F" });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pIsAdmin", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = user.IsAdmin.Equals(true) ? "T" : "F" });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pInactiveAcctDisableDaysOvr", OracleDbType = OracleDbType.Int32, Size = 4, Direction = ParameterDirection.Input, Value = user.InactiveAcctDisableDaysOverride });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pHashType", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = user.HashType });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pIterationCount", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = user.IterationCount });

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }
    }
}