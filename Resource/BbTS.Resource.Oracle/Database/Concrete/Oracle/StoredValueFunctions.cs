﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using BbTS.Core;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Container;
using BbTS.Domain.Models.Definitions.Transaction;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.StoredValue;
using BbTS.Domain.Models.System.Database;
using BbTS.Monitoring.Logging;
using Oracle.ManagedDataAccess.Client;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        /// <summary>
        /// This method will return the list of Sv_Account records in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsSv_Account> SvAccountArchivesGet(ConnectionInfo connection)
        {
            var list = new List<TsSv_Account>();

            using (OracleConnection con = OracleConnectionGet(connection))
            {
                using (var cmd = new OracleCommand {
                  Connection = con,
                  CommandText = @"SELECT  Sv_Account_Id,
                                          Sv_Account_Type_Id,
                                          Name,
                                          Account_Number,
                                          Balance_Forward,
                                          Balance,
                                          Credit_Limit,
                                          Individual_Or_Joint
                                  FROM    Envision.Sv_Account",
                  CommandType = CommandType.Text
                })
                {
                    try
                    {
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new TsSv_Account();

                                if (!r.IsDBNull(0)) item.Sv_Account_Id = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Sv_Account_Type_Id = r.GetInt32(1);
                                if (!r.IsDBNull(2)) item.Name = r.GetString(2);
                                if (!r.IsDBNull(3)) item.Account_Number = r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.Balance_Forward = r.GetInt32(4);
                                if (!r.IsDBNull(5)) item.Balance = r.GetInt32(5);
                                if (!r.IsDBNull(6)) item.Credit_Limit = r.GetInt32(6);
                                if (!r.IsDBNull(7)) item.Individual_Or_Joint = r.GetInt32(7);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Retrieve the stored value account settings for a device.
        /// </summary>
        /// <param name="deviceId">The unique identifier for the device.</param>
        /// <returns>List of stored value accounts for the device.</returns>
        public override List<StoredValueAccountDeviceSetting> StoredValueAccountDeviceSettingsGet(string deviceId)
        {
            var list = new List<StoredValueAccountDeviceSetting>();

            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                var query = @"
                        SELECT      SAT.Sv_Account_Type_Id,
                                    SAT.Name
                        FROM        Sv_Account_Type               SAT
                        INNER JOIN  Merchant_Sv_Account_Type      MAT ON  SAT.Sv_Account_Type_Id  = MAT.Sv_Account_Type_Id
                        INNER JOIN  Merchant                      MER ON  MAT.Merchant_Id         = MER.Merchant_Id
                        INNER JOIN  ProfitCenter                  PC  ON  MER.Merchant_Id         = PC.Merchant_Id
                        INNER JOIN  Pos                           POS ON  PC.ProfitCenter_Id      = POS.ProfitCenter_Id  
                        INNER JOIN  Originator                    DEV ON  POS.OriginatorId        = DEV.OriginatorId
                        INNER JOIN  Pos_Tt                        TT  ON  POS.Pos_Id              = TT.Pos_Id
                        INNER JOIN  Pos_Tt_Setup_Sv_Account_Type  TTA ON  TT.Pos_Tt_Setup_Id      = TTA.Pos_Tt_Setup_Id
                                                                      AND SAT.Sv_Account_Type_Id  = TTA.Sv_Account_Type_Id
                        WHERE       DEV.OriginatorGuid = :pValue";

                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = query,
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        cmd.Parameters.Add(
                            new OracleParameter
                            {
                                ParameterName = "pValue",
                                OracleDbType = OracleDbType.Varchar2,
                                Size = 36,
                                Value = new Guid(deviceId).ToString("D"),
                                Direction = ParameterDirection.Input
                            });
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new StoredValueAccountDeviceSetting();

                                if (!r.IsDBNull(0)) item.TypeId = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.Name = r.GetString(1);

                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Process a stored value deposit request.
        /// </summary>
        /// <param name="transaction">The transaction to process</param>
        /// <returns></returns>
        public override Transaction ProcessStoredValueDepositRequest(Transaction transaction)
        {
            return transaction;
        }

        /// <summary>
        /// Process a stored value deposit return request.
        /// </summary>
        /// <param name="transaction">The transaction to process</param>
        /// <returns></returns>
        public override Transaction ProcessStoredValueDepositReturnRequest(Transaction transaction)
        {
            return transaction;
        }

        /// <summary>
        /// Get list of stored value account types
        /// </summary>
        /// <param name="svAccountTypeId">Id filter</param>
        /// <returns>Stored value account types</returns>
        public override List<TsStoredValueAccountType> SvAccountTypeGet(int? svAccountTypeId = null)
        {
            var list = new List<TsStoredValueAccountType>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "StoredValue.SvAccountTypeGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pEventPlanId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = svAccountTypeId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsStoredValueAccountType()
                                {
                                    Id = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    Name = !r.IsDBNull(1) ? r.GetString(1) : string.Empty,
                                    NameTerminalDisplay = !r.IsDBNull(2) ? r.GetString(2) : string.Empty,
                                    NameCode = !r.IsDBNull(3) ? r.GetString(3) : string.Empty,
                                    TaxExempt = !r.IsDBNull(4) ? r.GetString(4) : string.Empty,
                                    StoredValueAccountShareType = !r.IsDBNull(5) ? (StoredValueAccountShareType)r.GetInt32(5) : StoredValueAccountShareType.Unknown,
                                    GroupAccountId = !r.IsDBNull(6) ? r.GetInt32(6) : -1
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <inheritdoc />
        public override List<StoredValueAccountType> SvAccountTypeGetAll()
        {
            var list = new List<StoredValueAccountType>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "StoredValue.SvAccountTypeGetAll", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new StoredValueAccountType()
                                {
                                    Guid = !r.IsDBNull(0) ? Guid.Parse(r.GetString(0)) : Guid.Empty,
                                    Name = !r.IsDBNull(1) ? r.GetString(1) : string.Empty,
                                    NameTerminalDisplay = !r.IsDBNull(2) ? r.GetString(2) : string.Empty,
                                    NameCode = !r.IsDBNull(3) ? r.GetString(3) : string.Empty,
                                    TaxExempt = !r.IsDBNull(4) && Formatting.TfStringToBool(r.GetString(4)),
                                    StoredValueAccountShareType = !r.IsDBNull(5) ? (StoredValueAccountShareType)r.GetInt32(5) : StoredValueAccountShareType.Unknown,
                                    Id = !r.IsDBNull(6) ? r.GetInt32(6) : -1
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <inheritdoc />
        public override List<StoredValueEnrichment> SvEnrichmentsGet(int? svAccountTypeId = null)
        {
            var list = new List<StoredValueEnrichment>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "StoredValue.EnrichmentsGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pSVAccountTypeId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = svAccountTypeId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new StoredValueEnrichment()
                                {
                                    SvEnrichmentAccountTypeId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    SvAccountTypeId = !r.IsDBNull(1) ? r.GetInt32(1) : -1,
                                    MinimumAmount = !r.IsDBNull(2) ? r.GetInt64(2) / 1000m : 0,
                                    Percentage = !r.IsDBNull(3) ? r.GetInt32(3) / 100000m : 0,
                                    IsActive = true,
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }
    }
}
