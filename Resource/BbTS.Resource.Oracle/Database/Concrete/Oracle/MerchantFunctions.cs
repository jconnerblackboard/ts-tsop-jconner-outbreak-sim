﻿using System;
using System.Collections.Generic;
using System.Data;
using BbTS.Core.Conversion;
using Oracle.ManagedDataAccess.Client;
using BbTS.Core;
using BbTS.Domain.Models.Exceptions.Resource;
using System.Diagnostics;
using BbTS.Monitoring.Logging;
using BbTS.Domain.Models.Merchant;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        /// <summary>
        /// Get merchant
        /// </summary>
        /// <param name="merchantGuid">Merchant's guid</param>
        /// <returns>Merchant</returns>
        public override TsMerchant MerchantGet(string merchantGuid)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "MerchantFunctions.MerchantGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pMerchantGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = merchantGuid });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            if (r.Read())
                            {
                                return new TsMerchant()
                                {
                                    MerchantId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    Name = !r.IsDBNull(1) ? r.GetString(1) : string.Empty,
                                    MerchantNumber = !r.IsDBNull(2) ? r.GetString(2) : string.Empty,
                                    MinimumCommission = !r.IsDBNull(3) ? r.GetInt32(3) / 1000m : -1,
                                    PaymentsAllowed = !r.IsDBNull(4) && r.GetString(4) == "T",
                                    FineDining = !r.IsDBNull(5) && r.GetString(5) == "T",
                                    EnableDiscountProgram = !r.IsDBNull(6) && r.GetString(6) == "T",
                                    DiscountPercent = !r.IsDBNull(7) ? r.GetInt32(7) / 1000m : -1,
                                    DoorAccessEnabled = !r.IsDBNull(8) && r.GetString(8) == "T",
                                    DomainId = !r.IsDBNull(9) ? r.GetString(9) : string.Empty
                                };
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return null;
        }

        /// <summary>
        /// Get merchants
        /// </summary>
        /// <returns>Merchants</returns>
        public override List<TsMerchant> MerchantGetAll()
        {
            var list = new List<TsMerchant>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "MerchantFunctions.MerchantGetAll", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsMerchant()
                                {
                                    MerchantId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    Name = !r.IsDBNull(1) ? r.GetString(1) : string.Empty,
                                    MerchantNumber = !r.IsDBNull(2) ? r.GetString(2) : string.Empty,
                                    MinimumCommission = !r.IsDBNull(3) ? r.GetInt32(3) / 1000m : -1,
                                    PaymentsAllowed = !r.IsDBNull(4) && r.GetString(4) == "T",
                                    FineDining = !r.IsDBNull(5) && r.GetString(5) == "T",
                                    EnableDiscountProgram = !r.IsDBNull(6) && r.GetString(6) == "T",
                                    DiscountPercent = !r.IsDBNull(7) ? r.GetInt32(7) / 1000m : -1,
                                    DoorAccessEnabled = !r.IsDBNull(8) && r.GetString(8) == "T",
                                    DomainId = !r.IsDBNull(9) ? r.GetString(9) : string.Empty
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }
    }
}
