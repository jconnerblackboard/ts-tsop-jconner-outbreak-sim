﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Net;
using BbTS.Core;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Definitions.Device;
using BbTS.Domain.Models.Event;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.Transaction.Processing;
using BbTS.Monitoring.Logging;
using Oracle.ManagedDataAccess.Client;
using BbTS.Domain.Models.Exceptions.WebApi;
using BbTS.Domain.Models.Definitions.Event;
using BbTS.Core.Serialization;
using BbTS.Domain.Models.Definitions.Logging;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        /// <summary>
        /// Process an attendance transaction in the TS database.
        /// </summary>
        /// <param name="request">Request input parameters.</param>
        /// <returns>Response data for completing the transaction.</returns>
        public override AttendanceTransactionProcessResponse AttendanceTransactionProcess(AttendanceTransactionProcessRequest request)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "TransactionFunctions.ProcessEventTran", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        var paddedCardNumber = request.CardNumber == null ? null : Formatting.PadCardNumber(request.CardNumber, 22, '0');
                        cmd.Parameters.Add(new OracleParameter("pOriginatorGuid", OracleDbType.Varchar2, 36, request.DeviceGuid, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTransactionDate", OracleDbType.Double, 14, request.TransactionDateTime.DateTime.ToOADate(), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pForcePost", OracleDbType.Varchar2, 1, Formatting.BooleanToTf(request.ForcePost), ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTransactionNumber", OracleDbType.Int32, 10, request.TransactionNumber, ParameterDirection.InputOutput));
                        cmd.Parameters.Add(new OracleParameter("pCashierGuid", OracleDbType.Varchar2, 36, request.OperatorGuid, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTransactionType", OracleDbType.Int16, 4, (int)request.TranType, ParameterDirection.InputOutput));
                        cmd.Parameters.Add(new OracleParameter("pCustomerGuid", OracleDbType.Varchar2, 36, request.CustomerGuid, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pEventNumber", OracleDbType.Int32, 10, request.EventNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pCardNum", OracleDbType.Varchar2, 22, paddedCardNumber, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pIssueNumber", OracleDbType.Varchar2, 4, request.IssueNumber, ParameterDirection.Input));

                        cmd.Parameters.Add(new OracleParameter("pErrorCode", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pDeniedText", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 75 });
                        cmd.Parameters.Add(new OracleParameter("pDisplayAttendance", OracleDbType.Varchar2, 1, ParameterDirection.Output) { Size = 1 });
                        cmd.Parameters.Add(new OracleParameter("pAttendance", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pCapacity", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });

                        con.Open();
                        cmd.ExecuteNonQuery();

                        var deniedText = cmd.Parameters["pDeniedText"].Value;
                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);
                        if (errorCode != 0)
                        {
                            return new AttendanceTransactionProcessResponse(request.RequestId)
                            {
                                DeniedText = deniedText.ToString(),
                                ErrorCode = errorCode
                            };
                        }
                        else
                        {
                            return new AttendanceTransactionProcessResponse(request.RequestId)
                            {
                                DeniedText = deniedText.ToString(),
                                ErrorCode = errorCode,
                                DisplayAttendance = Formatting.TfStringToBool(cmd.Parameters["pDisplayAttendance"].Value?.ToString()),
                                Attendance = TryGetIntValue(cmd.Parameters["pAttendance"], 0),
                                Capacity = TryGetIntValue(cmd.Parameters["pCapacity"], 0),
                            };
                        }
                    }
                    catch (Exception ex)
                    {
                        var message = Formatting.FormatException(ex);
                        var rex = new ResourceLayerException(string.Empty, message);
                        LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);

                        return new AttendanceTransactionProcessResponse(request.RequestId)
                        {
                            DeniedText = message,
                            ErrorCode = 1005
                        };
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Get a list of events for a device.
        /// </summary>
        /// <param name="deviceId">the unique identifier for the device.</param>
        /// <returns>List of <see cref="EventDeviceSetting"/></returns>
        public override List<EventDeviceSetting> EventDeviceSettingsGet(string deviceId)
        {
            var list = new List<EventDeviceSetting>();

            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (var cmd = new OracleCommand
                {
                    Connection = con,
                    CommandText = @"
                        SELECT      EVN.EVENT_ID,
                                    EVN.EVENTNAME,
                                    EVN.EVENTNUMBER,
                                    EVN.SCHEDULETYPE,
                                    EVN.CAPACITY,
                                    EVN.EndDate,
                                    CASE WHEN EVN.ScheduleType != 4 THEN EventFunctions.EventStartStopDateTimeGet(EVN.Event_Id,Udf_Functions.NowBusiness).StartDateTime ELSE NULL END AS StartDateTime,
                                    CASE WHEN EVN.ScheduleType != 4 THEN EventFunctions.EventStartStopDateTimeGet(EVN.Event_Id,Udf_Functions.NowBusiness).EndDateTime ELSE NULL END AS EndDateTime,
                                    EVN.AllowNonCust,
                                    EVN.DisplayAttendance,
                                    EVN.ExpectedAttendance,
                                    EVN.Threshold,
                                    EVN.MinimumAge,
                                    EventFunctions.EventFrequencyGet(EVN.Event_Id),
                                    CASE WHEN EVN.ScheduleType = 4 THEN (SELECT LISTAGG(StartDateTime || ',' || StopDateTime, ';') WITHIN GROUP (ORDER BY StartDateTime) FROM EventScheduleCustom WHERE Event_Id = EVN.Event_Id AND StopDateTime > Udf_Functions.NowBusiness) ELSE NULL END
                        FROM        Envision.Events                    EVN
                        INNER JOIN  Envision.EventStatus               EVS ON EVN.EventStatus_Id       = EVS.EventSTatus_Id
                        INNER JOIN  Envision.Merchant                  MER ON  EVN.Merchant_Id         = MER.Merchant_Id
                        INNER JOIN  Envision.ProfitCenter              PC  ON  MER.Merchant_Id         = PC.Merchant_Id
                        INNER JOIN  Envision.EventCentersAllowed       ECA ON ECA.Event_Id = EVN.Event_Id AND PC.ProfitCenter_Id = ECA.ProfitCenter_Id
                        INNER JOIN  Envision.Pos                       POS ON  PC.ProfitCenter_Id      = POS.ProfitCenter_Id  
                        INNER JOIN  Envision.Originator                DEV ON  POS.OriginatorId        = DEV.OriginatorId
                        WHERE       DEV.OriginatorGuid = :pValue
                        AND         EVN.ScheduleType != -2
                        AND         EVS.IsActive = 'T'
                        AND         ECA.Allow = 'T'
                        AND         (EVN.EndDate IS NULL OR EVN.EndDate >= Udf_Functions.NowBusiness)
                        ORDER BY EVN.EventNumber",
                    CommandType = CommandType.Text
                })
                {
                    try
                    {
                        cmd.Parameters.Add(
                            new OracleParameter
                            {
                                ParameterName = "pValue",
                                OracleDbType = OracleDbType.Varchar2,
                                Size = 36,
                                Value = new Guid(deviceId).ToString("D"),
                                Direction = ParameterDirection.Input
                            });
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var item = new EventDeviceSetting();

                                if (!r.IsDBNull(0)) item.EventId = r.GetInt32(0);
                                if (!r.IsDBNull(1)) item.EventName = r.GetString(1);
                                if (!r.IsDBNull(2)) item.EventNumber = r.GetInt32(2);
                                if (!r.IsDBNull(3)) item.ScheduleType = (EventDeviceSettingScheduleType)r.GetInt32(3);
                                if (!r.IsDBNull(4)) item.Capacity = r.GetInt32(4);
                                var endDate = !r.IsDBNull(5) ? (DateTime?)DateTime.FromOADate(r.GetDouble(5)) : null;
                                var startDateTime = !r.IsDBNull(6) ? DateTime.FromOADate(r.GetDouble(6)) : DateTime.MinValue;
                                var endDateTime = !r.IsDBNull(7) ? DateTime.FromOADate(r.GetDouble(7)) : DateTime.MinValue;
                                if (!r.IsDBNull(8)) item.AllowNonCustomerEntry = Formatting.TfStringToBool(r.GetString(8));
                                if (!r.IsDBNull(9)) item.DisplayAttendanceAfterEachEntry = Formatting.TfStringToBool(r.GetString(9));
                                if (!r.IsDBNull(10)) item.ExpectedAttendance = r.GetInt32(10);
                                if (!r.IsDBNull(11)) item.Threshold = r.GetInt32(11);
                                if (!r.IsDBNull(12)) item.MinimumAge = r.GetInt32(12);
                                var eventFrequency = !r.IsDBNull(13) ? r.GetString(13) : null;
                                var customDates = !r.IsDBNull(14) ? r.GetString(14) : null;

                                item.PopulateDates(startDateTime, endDateTime, item.ScheduleType == EventDeviceSettingScheduleType.Custom ? customDates : eventFrequency, endDate);
                                list.Add(item);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Get all event activities
        /// </summary>
        /// <returns>Event activities</returns>
        public override List<TsEventActivity> EventActivitiesGetAll()
        {
            var list = new List<TsEventActivity>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "EventFunctions.EventActivitiesGetAll", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsEventActivity()
                                {
                                    Id = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    MerchantNumber = !r.IsDBNull(1) ? r.GetInt32(1) : -1,
                                    Number = !r.IsDBNull(2) ? r.GetInt32(2) : -1,
                                    Name = !r.IsDBNull(3) ? r.GetString(3) : string.Empty
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <summary>
        /// Get list of event plans
        /// </summary>
        /// <param name="eventPlanId">Id filter</param>
        /// <returns>Event plans</returns>
        public override List<TsEventPlan> EventPlanGet(int? eventPlanId = null)
        {
            var list = new List<TsEventPlan>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "EventFunctions.EventPlanGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pEventPlanId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = eventPlanId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsEventPlan()
                                {
                                    Id = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    Name = !r.IsDBNull(1) ? r.GetString(1) : string.Empty,
                                    Active = !r.IsDBNull(2) && r.GetString(2) == "T",
                                    StartDate = !r.IsDBNull(3) ? (DateTime?)DateTime.FromOADate(r.GetFloat(3)) : null,
                                    StopDate = !r.IsDBNull(4) ? (DateTime?)DateTime.FromOADate(r.GetFloat(4)) : null,
                                    UseHolidayCalendar = !r.IsDBNull(5) && r.GetString(5) == "T"
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <inheritdoc />
        public override EventAttendanceGetResponse EventAttendanceGet(int eventId)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "EventFunctions.AttendanceGet", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pEventId", OracleDbType.Int32, 10, eventId, ParameterDirection.Input));

                        cmd.Parameters.Add(new OracleParameter("pAttendance", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pCapacity", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pThreshold", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pExpectedAttendance", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });

                        cmd.Parameters.Add(new OracleParameter("pErrorCode", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });
                        cmd.Parameters.Add(new OracleParameter("pDeniedText", OracleDbType.Varchar2, ParameterDirection.Output) { Size = 75 });

                        con.Open();
                        cmd.ExecuteNonQuery();

                        var deniedText = cmd.Parameters["pDeniedText"].Value;
                        var errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);
                        if (errorCode != 0)
                        {
                            var result = BbtsErrorDefinitions.ErrorWithAdditionalInfoGet(errorCode, $"EventAttendanceGet for EventId {eventId} failed with error code {errorCode}. DeniedText: {deniedText}");
                            throw new WebApiException(result, HttpStatusCode.BadRequest);
                        }
                        else
                        {
                            return new EventAttendanceGetResponse
                            {
                                EventId = eventId,
                                Attendance = TryGetIntValue(cmd.Parameters["pAttendance"], 0),
                                Capacity = TryGetIntValue(cmd.Parameters["pCapacity"], 0),
                                Threshold = TryGetIntValue(cmd.Parameters["pThreshold"], 0),
                                ExpectedAttendance = TryGetIntValue(cmd.Parameters["pExpectedAttendance"], 0)
                            };
                        }
                    }
                    catch (Exception ex)
                    {
                        var message = Formatting.FormatException(ex);
                        var rex = new ResourceLayerException(string.Empty, message);
                        LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);

                        throw new WebApiException(
                            null,
                            $"EventAttendanceGet for EventId {eventId} failed exception: {message}",
                            HttpStatusCode.BadRequest);
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <inheritdoc />
        public override CustomerEventsGetResponse CustomerEventsGet(int customerId)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "EventFunctions.EventsGetByCustomer", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = customerId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pEventPlanId", OracleDbType = OracleDbType.Int32, Size = 6, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pEventPlanActive", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        var list = new List<TsCustomerEvent>();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsCustomerEvent()
                                {
                                    MerchantId = !r.IsDBNull(0) ? r.GetInt16(0) : (Int16)(-1),
                                    MerchantName = !r.IsDBNull(1) ? r.GetString(1) : null,
                                    EventId = !r.IsDBNull(2) ? r.GetInt32(2) : -1,
                                    EventPlanId = !r.IsDBNull(3) ? r.GetInt32(3) : -1,
                                    EventNumber = !r.IsDBNull(4) ? r.GetInt32(4) : -1,
                                    EventName = !r.IsDBNull(5) ? r.GetString(5) : null,
                                    RegularAllowed = !r.IsDBNull(6) ? r.GetInt32(6) : -1,
                                    GuestAllowed = !r.IsDBNull(7) ? r.GetInt32(7) : -1,
                                    IsActive = !r.IsDBNull(8) && Formatting.TfStringToBool(r.GetString(8)),
                                    EventLimitType = !r.IsDBNull(9) ? Formatting.EventLimitTypeStrToEnum(r.GetString(9)) : EventLimitType.None,
                                    ResetDate = !r.IsDBNull(10) && r.GetDouble(10) != 0 ? (DateTime?)DateTime.FromOADate(r.GetDouble(10)) : null,
                                    IsCustom = !r.IsDBNull(11) && Formatting.TfStringToBool(r.GetString(11))
                                });
                            }
                        }

                        return new CustomerEventsGetResponse()
                        {
                            CustomerEvents = list,
                            EventPlanId = TryGetIntValue(cmd.Parameters["pEventPlanId"], 0),
                            EventPlanActive = Formatting.TfStringToBool(cmd.Parameters["pEventPlanActive"].Value?.ToString())
                        };
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override void CustomerEventsSet(Guid customerGuid, CustomerEventsPostRequest request)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "EventFunctions.CustomerEventsSet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustomerGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = customerGuid.ToString("D") });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pEventPlanId", OracleDbType = OracleDbType.Int32, Size = 6, Direction = ParameterDirection.Input, Value = request.EventPlanId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pEventPlanActive", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = Formatting.BooleanToTf(request.EventPlanActive) });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pXml", OracleDbType = OracleDbType.Clob, Direction = ParameterDirection.Input, Value = Xml.Serialize(request) });

                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <inheritdoc />
        public override List<TsCustomerEvent> EventsForCustomerGetByEventPlan(int eventPlanId)
        {
            var list = new List<TsCustomerEvent>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "EventFunctions.EventsForCustomerByEventPlan", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pEventPlanId", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Input, Value = eventPlanId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsCustomerEvent()
                                {
                                    EventId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    EventNumber = !r.IsDBNull(1) ? r.GetInt32(1) : -1,
                                    EventName = !r.IsDBNull(2) ? r.GetString(2) : null,
                                    MerchantId = !r.IsDBNull(3) ? r.GetInt16(3) : (Int16)(-1),
                                    MerchantName = !r.IsDBNull(4) ? r.GetString(4) : null,
                                    RegularAllowed = !r.IsDBNull(5) ? r.GetInt32(5) : -1,
                                    GuestAllowed = !r.IsDBNull(6) ? r.GetInt32(6) : -1,
                                    EventLimitType = !r.IsDBNull(7) ? Formatting.EventLimitTypeStrToEnum(r.GetString(7)) : EventLimitType.None,
                                    EventPlanId = eventPlanId
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <inheritdoc />
        public override EventListGetResponse EventListGet(EventListGetRequest request)
        {
            var result = new EventListGetResponse()
            {
                Events = new List<TsEventInfo>()
            };

            int? count = null;
            string countQuery = null;
            var query = @"SELECT    EVT.Event_Id,
                                    EVT.EventNumber,
                                    EVT.EventName,
                                    EVT.Merchant_Id,
                                    MCH.Name
                        FROM        Events EVT
                        INNER JOIN  Merchant MCH ON EVT.Merchant_Id = MCH.Merchant_Id
                        WHERE       EVT.Merchant_Id = NVL(:pMerchantId, EVT.Merchant_Id)
                        AND         EVT.EventNumber = NVL(:pEventNumber, EVT.EventNumber)
                        AND         EVT.EventName LIKE :pEventName";

            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandType = CommandType.Text })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pMerchantId", OracleDbType = OracleDbType.Int16, Size = 4, Direction = ParameterDirection.Input, Value = request?.MerchantId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pEventNumber", OracleDbType = OracleDbType.Int32, Size = 6, Direction = ParameterDirection.Input, Value = request?.EventNumber });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pEventName", OracleDbType = OracleDbType.Varchar2, Size = 31, Direction = ParameterDirection.Input, Value = $"{request?.EventName}%" });

                        if (request.PageSize.HasValue)
                        {
                            countQuery = $"SELECT count(*) FROM ({query})";
                            query = $"{query} OFFSET {request.Offset} ROWS FETCH NEXT {request.PageSize} ROWS ONLY";
                        }

                        cmd.CommandText = query;
                        con.Open();
                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                result.Events.Add(new TsEventInfo()
                                {
                                    EventId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    EventNumber = !r.IsDBNull(1) ? r.GetInt32(1) : -1,
                                    EventName = !r.IsDBNull(2) ? r.GetString(2) : null,
                                    MerchantId = !r.IsDBNull(3) ? r.GetInt16(3) : (Int16)(-1),
                                    MerchantName = !r.IsDBNull(4) ? r.GetString(4) : null
                                });
                            }
                        }

                        if (!string.IsNullOrEmpty(countQuery))
                        {
                            cmd.CommandText = countQuery;
                            count = int.Parse(cmd.ExecuteScalar().ToString());
                        }
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException("", Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }

            result.TotalEventCount = count ?? result.Events.Count;
            return result;
        }

        /// <inheritdoc />
        public override List<TsEventInfo> EventsByEventGroup(int eventGroupId)
        {
            var list = new List<TsEventInfo>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "EventFunctions.EventsByEventGroup", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pEventGroupId", OracleDbType = OracleDbType.Int32, Size = 6, Direction = ParameterDirection.Input, Value = eventGroupId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();


                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                list.Add(new TsEventInfo()
                                {
                                    EventId = !r.IsDBNull(0) ? r.GetInt32(0) : -1,
                                    EventNumber = !r.IsDBNull(1) ? r.GetInt32(1) : -1,
                                    EventName = !r.IsDBNull(2) ? r.GetString(2) : null,
                                    MerchantId = !r.IsDBNull(3) ? r.GetInt16(3) : (Int16)(-1),
                                    MerchantName = !r.IsDBNull(4) ? r.GetString(4) : null
                                });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }
    }
}
