﻿using System;
using System.Data;
using System.Diagnostics;
using BbTS.Core;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.System.Security.Oauth;
using BbTS.Monitoring.Logging;
using Oracle.ManagedDataAccess.Client;
using BbTS.Core.Logging.Tracing;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        /// <summary>
        /// Set an authentication oauth token in the database layer
        /// </summary>
        /// <param name="token">The token to store in the database layer.</param>
        [Trace(AttributeExclude = true, AttributePriority = 1)]
        public override void AuthenticationOauthTokenSet(AuthenticationOauthToken token)
        {
            Guard.IsNotNull(token, "token");
            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");

            using (OracleConnection con = new OracleConnection(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "AuthenticationFunctions.AuthenticationOauthTokenSet", CommandType = CommandType.StoredProcedure })
                {
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pKey", OracleDbType = OracleDbType.Varchar2, Size = 255, Direction = ParameterDirection.Input, Value = token.Key });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pValue", OracleDbType = OracleDbType.Clob, Direction = ParameterDirection.Input, Value = token.Value });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pExpirationDateTime", OracleDbType = OracleDbType.Date, Direction = ParameterDirection.Input, Value = token.ExpirationDateTime });

                    try
                    {
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }
    

        /// <summary>
        /// Get an authentication oauth token from the database layer.
        /// </summary>
        /// <param name="key">The token key to retrieve from the store.</param>
        /// <returns>Stored token.</returns>
        [Trace(AttributeExclude = true, AttributePriority = 1)]
        public override AuthenticationOauthToken AuthenticationOauthTokenGet(string key)
        {
            // if there is no key then there is no token.
            if (string.IsNullOrEmpty(key)) return null;

            Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand
                {
                    Connection = con,
                    CommandType = CommandType.Text,
                    CommandText = $"SELECT * FROM Envision.AuthenticationOauthToken WHERE Key='{key}'"
                })
                {
                    try
                    {
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var tableKey = !r.IsDBNull(0) ? r.GetString(0) : string.Empty;
                                var value = !r.IsDBNull(1) ? r.GetString(1) : string.Empty;
                                var expirationDateTime = !r.IsDBNull(2) ? r.GetDateTime(2) : new DateTime();

                                return new AuthenticationOauthToken
                                {
                                    Key = tableKey,
                                    Value = value,
                                    ExpirationDateTime = expirationDateTime
                                };
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            return null;
        }

        /// <inheritdoc />
        public override bool ApiPermissionIsAllowed(string authenticationKey, string objectName)
        {
            using (var con = new OracleConnection(ConnectionString))
            {
                using (var cmd = new OracleCommand { Connection = con, CommandText = "AuthenticationFunctions.ApiPermissionIsAllowed", CommandType = CommandType.StoredProcedure })
                {

                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pAuthenticationKey", OracleDbType = OracleDbType.Varchar2, Size = 50, Direction = ParameterDirection.Input, Value = authenticationKey });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pObjectName", OracleDbType = OracleDbType.Varchar2, Size = 255, Direction = ParameterDirection.Input, Value = objectName });
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pIsAllowed", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Output });

                    try
                    {
                        con.Open();
                        cmd.ExecuteNonQuery();
                        return Formatting.TfStringToBool(cmd.Parameters["pIsAllowed"].Value.ToString());

                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }
    }
}
