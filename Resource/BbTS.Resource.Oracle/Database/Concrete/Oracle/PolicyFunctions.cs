﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using BbTS.Core.Conversion;
using BbTS.Core.Policy;
using BbTS.Domain.Models.Customer;
using BbTS.Domain.Models.Definitions.ArtsDataModel;
using BbTS.Domain.Models.Definitions.Policy;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.Policy;
using BbTS.Domain.Models.Policy.Actions;
using BbTS.Domain.Models.Policy.Conditions;
using BbTS.Domain.Models.System.Database;
using BbTS.Monitoring.Logging;
using Oracle.ManagedDataAccess.Client;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        #region Policy

        private static List<PolicyGroup> PolicyGroupsGet(OracleConnection con)
        {
            var list = new List<PolicyGroup>();

            using (var cmd = new OracleCommand
            {
                Connection = con,
                CommandText = @"SELECT          PST.Policy_Set_Id,
                                                PST.Name,
                                                PST.Caption,
                                                PST.Description,
                                                PST.Policy_Decision_Strategy,
                                                PST.Policy_Keywords,
                                                PST.Reusable_Policy_Container_Id
                                  FROM          Policy_Set                      PST
                            INNER JOIN          Policy_Group                    GRP     ON PST.Policy_Set_Id = GRP.Policy_Set_Id",
                CommandType = CommandType.Text
            })
            {
                try
                {
                    con.Open();
                    using (var r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            var item = new TsPolicy_Set();

                            if (!r.IsDBNull(0)) item.Policy_Set_Id = r.GetInt32(0);
                            if (!r.IsDBNull(1)) item.Name = r.GetString(1);
                            if (!r.IsDBNull(2)) item.Caption = r.GetString(2);
                            if (!r.IsDBNull(3)) item.Description = r.GetString(3);
                            if (!r.IsDBNull(4)) item.Policy_Decision_Strategy = r.GetInt32(4);
                            if (!r.IsDBNull(5)) item.Policy_Keywords = r.GetString(5);
                            if (!r.IsDBNull(6)) item.Reusable_Policy_Container_Id = r.GetInt32(6);

                            var policyGroup = PolicyBuilder.NewPolicyGroup(item.Name);
                            policyGroup.PolicySetId = item.Policy_Set_Id;
                            policyGroup.Caption = item.Caption;
                            policyGroup.Description = item.Description;
                            policyGroup.DecisionStrategy = (PolicyDecisionStrategy)item.Policy_Decision_Strategy;
                            policyGroup.PolicyKeywords = item.Policy_Keywords;

                            list.Add(policyGroup);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        private static List<PolicyRule> PolicyRulesGet(OracleConnection con, List<PolicyCondition> condtions)
        {
            var list = new List<PolicyRule>();

            using (var cmd = new OracleCommand
            {
                Connection = con,
                CommandText = @"SELECT          PST.Policy_Set_Id,
                                                PST.Name,
                                                PST.Caption,
                                                PST.Description,
                                                PST.Policy_Decision_Strategy,
                                                PST.Policy_Keywords,
                                                PST.Reusable_Policy_Container_Id,

                                                RUL.Enabled_Status,
                                                RUL.Rule_Usage,
                                                RUL.Mandatory,
                                                RUL.Sequenced_Action,
                                                RUL.Policy_Condition_Id,
                                                RUL.Execution_Strategy_Type_Id
                                  FROM          Policy_Set                         PST
                            INNER JOIN          Policy_Rule                        RUL  ON PST.Policy_Set_Id = RUL.Policy_Set_Id",
                CommandType = CommandType.Text
            })
            {
                try
                {
                    con.Open();
                    using (var r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            var item = new TsPolicy_Rule();

                            if (!r.IsDBNull(0)) item.Policy_Set_Id = r.GetInt32(0);
                            if (!r.IsDBNull(1)) item.Name = r.GetString(1);
                            if (!r.IsDBNull(2)) item.Caption = r.GetString(2);
                            if (!r.IsDBNull(3)) item.Description = r.GetString(3);
                            if (!r.IsDBNull(4)) item.Policy_Decision_Strategy = r.GetInt32(4);
                            if (!r.IsDBNull(5)) item.Policy_Keywords = r.GetString(5);
                            if (!r.IsDBNull(6)) item.Reusable_Policy_Container_Id = r.GetInt32(6);

                            if (!r.IsDBNull(7)) item.Enabled_Status = r.GetInt32(7);
                            if (!r.IsDBNull(8)) item.Rule_Usage = r.GetString(8);
                            if (!r.IsDBNull(9)) item.Mandatory = r.GetString(9) == "T" ? 'T' : 'F';
                            if (!r.IsDBNull(10)) item.Sequenced_Action = r.GetInt32(10);
                            if (!r.IsDBNull(11)) item.Policy_Condition_Id = r.GetInt32(11);
                            if (!r.IsDBNull(12)) item.Execution_Strategy_Type_Id = r.GetInt32(12);

                            var policyRule = PolicyBuilder.NewPolicyRule(item.Name);
                            policyRule.PolicySetId = item.Policy_Set_Id;
                            policyRule.Caption = item.Caption;
                            policyRule.Description = item.Description;
                            policyRule.DecisionStrategy = (PolicyDecisionStrategy)item.Policy_Decision_Strategy;
                            policyRule.PolicyKeywords = item.Policy_Keywords;

                            policyRule.EnabledStatus = (EnabledStatus)item.Enabled_Status;
                            policyRule.RuleUsage = item.Rule_Usage;
                            policyRule.Mandatory = item.Mandatory == 'T';
                            policyRule.SequencedActions = (SequencedActions)item.Sequenced_Action;
                            policyRule.Conditions = (CompoundPolicyCondition)condtions.Find(c => c.PolicyConditionId == item.Policy_Condition_Id);
                            policyRule.ExecutionStrategyType = (ExecutionStrategyType)item.Execution_Strategy_Type_Id;

                            list.Add(policyRule);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        private static List<PolicyCondition> PolicyConditionsGet(OracleConnection con)
        {
            var list = new List<PolicyCondition>();

            #region Base condition info
            using (var cmd = new OracleCommand
            {
                Connection = con,
                CommandText = @"SELECT          Policy_Condition_Id,
                                                Reusable_Policy_Container_Id,
                                                Name,
                                                Caption,
                                                Description,
                                                Policy_Condition_Type_Id,
                                                Policy_Keywords
                                  FROM          Policy_Condition",
                CommandType = CommandType.Text
            })
            {
                try
                {
                    con.Open();
                    using (var r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            var item = new TsPolicy_Condition();

                            if (!r.IsDBNull(0)) item.Policy_Condition_Id = r.GetInt32(0);
                            if (!r.IsDBNull(1)) item.Reusable_Policy_Container_Id = r.GetInt32(1);
                            if (!r.IsDBNull(2)) item.Name = r.GetString(2);
                            if (!r.IsDBNull(3)) item.Caption = r.GetString(3);
                            if (!r.IsDBNull(4)) item.Description = r.GetString(4);
                            if (!r.IsDBNull(5)) item.Policy_Condition_Type_Id = r.GetInt32(5);
                            if (!r.IsDBNull(6)) item.Policy_Keywords = r.GetString(6);

                            PolicyCondition condition = null;
                            switch ((PolicyConditionType)item.Policy_Condition_Type_Id)
                            {
                                case PolicyConditionType.Compound:
                                    condition = new CompoundPolicyCondition();
                                    break;
                                case PolicyConditionType.True:
                                    condition = new TrueCondition();
                                    break;
                                case PolicyConditionType.False:
                                    condition = new FalseCondition();
                                    break;
                                case PolicyConditionType.Match:
                                    //not used
                                    break;
                                case PolicyConditionType.ReaderInGroup:
                                    condition = new PosInGroupCondition();
                                    break;
                                case PolicyConditionType.CustomerInGroup:
                                    condition = new CustomerInGroupCondition();
                                    break;
                                case PolicyConditionType.Tender:
                                    condition = new TenderUsedCondition();
                                    break;
                                case PolicyConditionType.TimePeriod:
                                    condition = new TimePeriodCondition();
                                    break;
                                default:
                                    throw new ArgumentOutOfRangeException();
                            }

                            if (condition != null)
                            {
                                condition.PolicyConditionId = item.Policy_Condition_Id;
                                condition.Name = item.Name;
                                condition.Caption = item.Caption;
                                condition.Description = item.Description;
                                condition.PolicyKeywords = item.Policy_Keywords;

                                list.Add(condition);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            #endregion

            #region Customer in Group
            using (var cmd = new OracleCommand
            {
                Connection = con,
                CommandText = @"SELECT          Policy_Condition_Id,
                                                Customer_Def_Grp_Def_Item_Id
                                  FROM          Policy_Condition_Value_CustGrp",
                CommandType = CommandType.Text
            })
            {
                try
                {
                    con.Open();
                    using (var r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            int policyConditionId = 0;
                            int customerDefGrpDefItemId = 0;

                            if (!r.IsDBNull(0)) policyConditionId = r.GetInt32(0);
                            if (!r.IsDBNull(1)) customerDefGrpDefItemId = r.GetInt32(1);

                            var customerInGroup = (CustomerInGroupCondition)list.Find(c => c.PolicyConditionId == policyConditionId);
                            customerInGroup.CustomerDefinedGroupItemId = customerDefGrpDefItemId;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            #endregion

            #region Pos in Group
            using (var cmd = new OracleCommand
            {
                Connection = con,
                CommandText = @"SELECT          Policy_Condition_Id,
                                                Pos_Group_Id
                                  FROM          Policy_Condition_Value_Pos_Grp",
                CommandType = CommandType.Text
            })
            {
                try
                {
                    con.Open();
                    using (var r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            int policyConditionId = 0;
                            int posGroupId = 0;

                            if (!r.IsDBNull(0)) policyConditionId = r.GetInt32(0);
                            if (!r.IsDBNull(1)) posGroupId = r.GetInt32(1);

                            var posInGroup = (PosInGroupCondition)list.Find(c => c.PolicyConditionId == policyConditionId);
                            posInGroup.PosGroupId = posGroupId;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            #endregion

            #region Tender used
            using (var cmd = new OracleCommand
            {
                Connection = con,
                CommandText = @"SELECT          Policy_Condition_Id,
                                                Tender_Id
                                  FROM          Policy_Condition_Value_Tender",
                CommandType = CommandType.Text
            })
            {
                try
                {
                    con.Open();
                    using (var r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            int policyConditionId = 0;
                            int tenderId = 0;

                            if (!r.IsDBNull(0)) policyConditionId = r.GetInt32(0);
                            if (!r.IsDBNull(1)) tenderId = r.GetInt32(1);

                            var tenderUsed = (TenderUsedCondition)list.Find(c => c.PolicyConditionId == policyConditionId);
                            tenderUsed.TenderId = tenderId;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            #endregion

            #region Time period condition
            using (var cmd = new OracleCommand
            {
                Connection = con,
                CommandText = @"SELECT          Policy_Condition_Id,
                                                TimePeriod_Begin_Date,
                                                TimePeriod_End_Date,
                                                Month_Of_Year_Mask,
                                                Day_Of_Month_Mask,
                                                Day_Of_Week_Mask,
                                                Start_Time_Of_Day,
                                                End_Time_Of_Day
                                  FROM          Policy_TimePeriod_Condition",
                CommandType = CommandType.Text
            })
            {
                try
                {
                    con.Open();
                    using (var r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            var item = new TsPolicy_TimePeriod_Condition();

                            if (!r.IsDBNull(0)) item.Policy_Condition_Id = r.GetInt32(0);
                            item.TimePeriod_Begin_Date = !r.IsDBNull(1) ? (DateTime?)r.GetDateTime(1) : null;
                            item.TimePeriod_End_Date = !r.IsDBNull(2) ? (DateTime?)r.GetDateTime(2) : null;
                            if (!r.IsDBNull(3)) item.Month_Of_Year_Mask = r.GetString(3);
                            if (!r.IsDBNull(4)) item.Day_Of_Month_Mask = r.GetString(4);
                            if (!r.IsDBNull(5)) item.Day_Of_Week_Mask = r.GetString(5);
                            item.Start_Time_Of_Day = !r.IsDBNull(6) ? r.GetInt32(6) : 0;
                            item.End_Time_Of_Day = !r.IsDBNull(7) ? r.GetInt32(7) : 235959;

                            int startHour = item.Start_Time_Of_Day / 10000;
                            int startMinute = (item.Start_Time_Of_Day - (startHour * 10000)) / 100;
                            int startSecond = (item.Start_Time_Of_Day - (startMinute * 100));
                            int endHour = item.End_Time_Of_Day / 10000;
                            int endMinute = (item.End_Time_Of_Day - (endHour * 10000)) / 100;
                            int endSecond = (item.End_Time_Of_Day - (endMinute * 100));

                            var timePeriod = (TimePeriodCondition)list.Find(c => c.PolicyConditionId == item.Policy_Condition_Id);
                            timePeriod.TimePeriodBegin = item.TimePeriod_Begin_Date;
                            timePeriod.TimePeriodEnd = item.TimePeriod_End_Date;
                            timePeriod.MonthOfYearMask = item.Month_Of_Year_Mask;
                            timePeriod.DayOfMonthForwardMask = item.Day_Of_Month_Mask.Substring(0, 31);
                            timePeriod.DayOfMonthBackwardMask = item.Day_Of_Month_Mask.Substring(31, 31);
                            timePeriod.DayOfWeekMask = item.Day_Of_Week_Mask;
                            timePeriod.TimeOfDayBegin = new TimeSpan(startHour, startMinute, startSecond);
                            timePeriod.TimeOfDayEnd = new TimeSpan(endHour, endMinute, endSecond);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            #endregion

            return list;
        }

        private static List<PolicyAction> PolicyActionsGet(OracleConnection con)
        {
            var list = new List<PolicyAction>();

            #region Base action info
            using (var cmd = new OracleCommand
            {
                Connection = con,
                CommandText = @"SELECT          Policy_Action_Id,
                                                Reusable_Policy_Container_Id,
                                                Name,
                                                Caption,
                                                Description,
                                                Policy_Keywords,
                                                Policy_Action_Type_Id
                                  FROM          Policy_Action",
                CommandType = CommandType.Text
            })
            {
                try
                {
                    con.Open();
                    using (var r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            var item = new TsPolicy_Action();

                            if (!r.IsDBNull(0)) item.Policy_Action_Id = r.GetInt32(0);
                            if (!r.IsDBNull(1)) item.Reusable_Policy_Container_Id = r.GetInt32(1);
                            if (!r.IsDBNull(2)) item.Name = r.GetString(2);
                            if (!r.IsDBNull(3)) item.Caption = r.GetString(3);
                            if (!r.IsDBNull(4)) item.Description = r.GetString(4);
                            if (!r.IsDBNull(5)) item.Policy_Keywords = r.GetString(5);
                            if (!r.IsDBNull(6)) item.Policy_Action_Type_Id = r.GetInt32(6);

                            PolicyAction action;
                            switch ((PolicyActionType)item.Policy_Action_Type_Id)
                            {
                                case PolicyActionType.DenyTransaction:
                                    action = new DenyTransactionAction();
                                    break;
                                case PolicyActionType.AllowTransaction:
                                    action = new AllowTransactionAction();
                                    break;
                                case PolicyActionType.ApplyDiscount:
                                    action = new ApplyDiscountAction();
                                    break;
                                case PolicyActionType.ApplySurcharge:
                                    action = new ApplySurchargeAction();
                                    break;
                                case PolicyActionType.StoredValueAccountType:
                                    action = new UseStoredValueAccounts { StoredValueAccountTypeIds = new List<int>() };
                                    break;
                                case PolicyActionType.ApplyStoredValueTranLimit:
                                    action = new ApplyStoredValueTransactionLimit();
                                    break;
                                default:
                                    throw new ArgumentOutOfRangeException();
                            }

                            action.PolicyActionId = item.Policy_Action_Id;
                            action.Name = item.Name;
                            action.Caption = item.Caption;
                            action.Description = item.Description;
                            action.PolicyKeywords = item.Policy_Keywords;

                            list.Add(action);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            #endregion

            #region Apply discount
            using (var cmd = new OracleCommand
            {
                Connection = con,
                CommandText = @"SELECT          Policy_Action_Id,
                                                Discount_Percent
                                  FROM          Policy_Action_Discount_Percent",
                CommandType = CommandType.Text
            })
            {
                try
                {
                    con.Open();
                    using (var r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            int policyActionId = 0;
                            int discountPercent = 0;

                            if (!r.IsDBNull(0)) policyActionId = r.GetInt32(0);
                            if (!r.IsDBNull(1)) discountPercent = r.GetInt32(1);  //Discount Percentage (stored as millipercent)

                            var applyDiscount = (ApplyDiscountAction)list.Find(a => a.PolicyActionId == policyActionId);
                            applyDiscount.Rate = discountPercent / 100000m;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            #endregion

            #region Apply surcharge
            using (var cmd = new OracleCommand
            {
                Connection = con,
                CommandText = @"SELECT          Policy_Action_Id,
                                                Surcharge_Percent
                                  FROM          Policy_Action_Surch_Percent",
                CommandType = CommandType.Text
            })
            {
                try
                {
                    con.Open();
                    using (var r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            int policyActionId = 0;
                            int surchargePercent = 0;

                            if (!r.IsDBNull(0)) policyActionId = r.GetInt32(0);
                            if (!r.IsDBNull(1)) surchargePercent = r.GetInt32(1);  //Surcharge Percentage (stored as millipercent)

                            var applySurcharge = (ApplySurchargeAction)list.Find(a => a.PolicyActionId == policyActionId);
                            applySurcharge.Rate = surchargePercent / 100000m;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            #endregion

            #region Use stored value accounts
            using (var cmd = new OracleCommand
            {
                Connection = con,
                CommandText = @"SELECT          Policy_Action_Id,
                                                Sv_Account_Type_Id
                                  FROM          Policy_Action_Sv_Account_Type
                              ORDER BY          Policy_Action_Id, Depletion_Order",
                CommandType = CommandType.Text
            })
            {
                try
                {
                    con.Open();
                    using (var r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            int policyActionId = 0;
                            int storedValueAccountTypeId = 0;

                            if (!r.IsDBNull(0)) policyActionId = r.GetInt32(0);
                            if (!r.IsDBNull(1)) storedValueAccountTypeId = r.GetInt32(1);

                            var useStoredValueAccounts = (UseStoredValueAccounts)list.Find(a => a.PolicyActionId == policyActionId);
                            useStoredValueAccounts.StoredValueAccountTypeIds.Add(storedValueAccountTypeId);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            #endregion

            #region Apply stored value transaction limit
            using (var cmd = new OracleCommand
            {
                Connection = con,
                CommandText = @"SELECT          Policy_Action_Id,
                                                Sv_Transaction_Limit_Id
                                  FROM          Policy_Action_Sv_Tran_Limit",
                CommandType = CommandType.Text
            })
            {
                try
                {
                    con.Open();
                    using (var r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            int policyActionId = 0;
                            int storedValueTransactionLimitId = 0;

                            if (!r.IsDBNull(0)) policyActionId = r.GetInt32(0);
                            if (!r.IsDBNull(1)) storedValueTransactionLimitId = r.GetInt32(1);

                            var applyStoredValueTransactionLimit = (ApplyStoredValueTransactionLimit)list.Find(a => a.PolicyActionId == policyActionId);
                            applyStoredValueTransactionLimit.StoredValueTransactionLimitId = storedValueTransactionLimitId;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            #endregion

            return list;
        }

        private static List<TsPolicy_Set_Component> PolicySetComponentsGet(OracleConnection con)
        {
            var list = new List<TsPolicy_Set_Component>();

            using (var cmd = new OracleCommand
            {
                Connection = con,
                CommandText = @"SELECT          Parent_Policy_Set_Id,
                                                Child_Policy_Set_Id,
                                                Priority
                                  FROM          Policy_Set_Component",
                CommandType = CommandType.Text
            })
            {
                try
                {
                    con.Open();
                    using (var r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            var item = new TsPolicy_Set_Component();

                            if (!r.IsDBNull(0)) item.Parent_Policy_Set_Id = r.GetInt32(0);
                            if (!r.IsDBNull(1)) item.Child_Policy_Set_Id = r.GetInt32(1);
                            if (!r.IsDBNull(2)) item.Priority = r.GetInt32(2);

                            list.Add(item);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        private static List<TsPolicy_Cond_In_Policy_Cond> PolicyConditionInPolicyConditionListGet(OracleConnection con)
        {
            var list = new List<TsPolicy_Cond_In_Policy_Cond>();

            using (var cmd = new OracleCommand
            {
                Connection = con,
                CommandText = @"SELECT          Parent_Policy_Condition_Id,
                                                Child_Policy_Condition_Id,
                                                Display_Order,
                                                Condition_Negated
                                  FROM          Policy_Cond_In_Policy_Cond",
                CommandType = CommandType.Text
            })
            {
                try
                {
                    con.Open();
                    using (var r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            var item = new TsPolicy_Cond_In_Policy_Cond();

                            if (!r.IsDBNull(0)) item.Parent_Policy_Condition_Id = r.GetInt32(0);
                            if (!r.IsDBNull(1)) item.Child_Policy_Condition_Id = r.GetInt32(1);
                            if (!r.IsDBNull(2)) item.Display_Order = r.GetInt32(2);
                            if (!r.IsDBNull(3)) item.Condition_Negated = r.GetString(3) == "T" ? 'T' : 'F';

                            list.Add(item);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        private static List<TsCompound_Policy_Condition> ConditionListTypeValuesGet(OracleConnection con)
        {
            var list = new List<TsCompound_Policy_Condition>();

            using (var cmd = new OracleCommand
            {
                Connection = con,
                CommandText = @"SELECT          Policy_Condition_Id,
                                                Condition_List_Type
                                  FROM          Compound_Policy_Condition",
                CommandType = CommandType.Text
            })
            {
                try
                {
                    con.Open();
                    using (var r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            var item = new TsCompound_Policy_Condition();

                            if (!r.IsDBNull(0)) item.Policy_Condition_Id = r.GetInt32(0);
                            if (!r.IsDBNull(1)) item.Condition_List_Type = r.GetInt32(1);

                            list.Add(item);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        private static List<TsPolicy_Action_In_Policy_Rule> PolicyActionInPolicyRuleListGet(OracleConnection con)
        {
            var list = new List<TsPolicy_Action_In_Policy_Rule>();

            using (var cmd = new OracleCommand
            {
                Connection = con,
                CommandText = @"SELECT          Child_Policy_Action_Id,
                                                Policy_Set_Id,
                                                Action_Order
                                  FROM          Policy_Action_In_Policy_Rule",
                CommandType = CommandType.Text
            })
            {
                try
                {
                    con.Open();
                    using (var r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            var item = new TsPolicy_Action_In_Policy_Rule();

                            if (!r.IsDBNull(0)) item.Child_Policy_Action_Id = r.GetInt32(0);
                            if (!r.IsDBNull(1)) item.Policy_Set_Id = r.GetInt32(1);
                            if (!r.IsDBNull(2)) item.Action_Order = r.GetInt32(2);

                            list.Add(item);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        private static List<TsPolicy_Rule_Validity_Period> PolicyRuleValidityPeriodsGet(OracleConnection con)
        {
            var list = new List<TsPolicy_Rule_Validity_Period>();

            using (var cmd = new OracleCommand
            {
                Connection = con,
                CommandText = @"SELECT          Policy_Condition_Id,
                                                Policy_Set_Id
                                  FROM          Policy_Rule_Validity_Period",
                CommandType = CommandType.Text
            })
            {
                try
                {
                    con.Open();
                    using (var r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            var item = new TsPolicy_Rule_Validity_Period();

                            if (!r.IsDBNull(0)) item.Policy_Condition_Id = r.GetInt32(0);
                            if (!r.IsDBNull(1)) item.Policy_Set_Id = r.GetInt32(1);

                            list.Add(item);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }

            return list;
        }

        /// <summary>
        /// Recursively assembles the policy tree in a pre-order traversal
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="policySets"></param>
        /// <param name="components"></param>
        private static void AssemblePolicyTree(PolicySet parent, List<PolicySet> policySets, List<TsPolicy_Set_Component> components)
        {
            var childComponents = components.Where(s => s.Parent_Policy_Set_Id == parent.PolicySetId).OrderBy(s => s.Priority);
            foreach (var component in childComponents)
            {
                var child = policySets.Find(ps => ps.PolicySetId == component.Child_Policy_Set_Id);
                parent.AddChild(child);
                AssemblePolicyTree(child, policySets, components);
            }
        }

        /// <summary>
        /// Loads the system root policy for BbTS.
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        public override PolicySet SystemRootPolicyLoad(ConnectionInfo connection = null)
        {
            #region Load from database
            //Load a flat list of the various policy objects
            List<PolicyGroup> groups;
            List<PolicyRule> rules;
            List<PolicyCondition> conditions;
            List<PolicyAction> actions;

            List<TsPolicy_Rule_Validity_Period> validityStructure;
            List<TsPolicy_Set_Component> policySetStructure;
            List<TsPolicy_Action_In_Policy_Rule> actionStructure;
            List<TsPolicy_Cond_In_Policy_Cond> conditionStructure;
            List<TsCompound_Policy_Condition> conditionListTypeValues;

            using (var con = connection == null ? OracleConnectionGet(ConnectionString) : OracleConnectionGet(connection))
            {
                conditions = PolicyConditionsGet(con);
                actions = PolicyActionsGet(con);

                groups = PolicyGroupsGet(con);
                rules = PolicyRulesGet(con, conditions);

                policySetStructure = PolicySetComponentsGet(con);
                validityStructure = PolicyRuleValidityPeriodsGet(con);
                actionStructure = PolicyActionInPolicyRuleListGet(con);
                conditionStructure = PolicyConditionInPolicyConditionListGet(con);
                conditionListTypeValues = ConditionListTypeValuesGet(con);
            }

            var policySets = new List<PolicySet>();
            policySets.AddRange(groups);
            policySets.AddRange(rules);
            #endregion

            #region Assemble policy tree
            //Policy set children - add child policy sets to the parent policy sets
            var root = policySets.Find(policySet => (policySet.Parents == null) || (policySet.Parents.Count == 0));
            if (root == null) return null;

            AssemblePolicyTree(root, policySets, policySetStructure);

            //Validity Periods - add validity periods to policy sets
            foreach (var rule in rules)
            {
                var childValidityPeriods = validityStructure.Where(s => s.Policy_Set_Id == rule.PolicySetId);
                foreach (var childValidityPeriod in childValidityPeriods)
                {
                    rule.AddValidityPeriod((TimePeriodCondition)conditions.Find(c => c.PolicyConditionId == childValidityPeriod.Policy_Condition_Id));
                }
            }

            //Conditions - add conditions to compound conditions (the policy set's base compound condition has already been linked)
            foreach (var condition in conditions)
            {
                var parentCondition = condition as CompoundPolicyCondition;
                if (parentCondition == null) continue;

                int conditionListType = conditionListTypeValues.Find(t => t.Policy_Condition_Id == parentCondition.PolicyConditionId).Condition_List_Type;
                parentCondition.ConditionListType = (ConditionListType)conditionListType;
                var childConditions = conditionStructure.Where(s => s.Parent_Policy_Condition_Id == parentCondition.PolicyConditionId).OrderBy(s => s.Display_Order);
                foreach (var childCondition in childConditions)
                {
                    parentCondition.AddCondition(conditions.Find(c => c.PolicyConditionId == childCondition.Child_Policy_Condition_Id), childCondition.Condition_Negated == 'T');
                }
            }

            //Actions - add actions to policy sets
            foreach (var rule in rules)
            {
                var childActions = actionStructure.Where(s => s.Policy_Set_Id == rule.PolicySetId).OrderBy(s => s.Action_Order);
                foreach (var childAction in childActions)
                {
                    rule.AddAction(actions.Find(a => a.PolicyActionId == childAction.Child_Policy_Action_Id));
                }
            }
            #endregion

            return root;
        }

        /// <summary>
        /// Gets a list of stored value tender IDs allowed for the specified originator.
        /// </summary>
        /// <param name="originatorGuid"></param>
        /// <param name="tenderIds"></param>
        /// <param name="connection"></param>
        public override void ProcessPolicyTendersGet(Guid originatorGuid, out List<int> tenderIds, ConnectionInfo connection = null)
        {
            tenderIds = new List<int>();

            using (var con = connection == null ? OracleConnectionGet(ConnectionString) : OracleConnectionGet(connection))
            {
                var cmd = new OracleCommand { Connection = con, CommandText = "PackTranPolicy.ProcessPolicyTendersGet", CommandType = CommandType.StoredProcedure };
                //Input
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pOriginatorGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = originatorGuid });
                //Output
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pAllowedTenders", OracleDbType = OracleDbType.RefCursor, Size = 0, Direction = ParameterDirection.Output });

                try
                {
                    // Open the database connection
                    con.Open();

                    using (var r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            tenderIds.Add(r.GetInt32(0));
                        }
                    }
                }
                catch (Exception ex)
                {
                    var rex = new ResourceLayerException(string.Empty, Formatting.FormatException(ex));
                    LoggingManager.Instance.LogResourceException(rex, EventLogEntryType.Error);
                    throw rex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Process policy for a customer using the stored proc "Validate.ValidateSystemPolicy"
        /// </summary>
        /// <param name="cardNum"></param>
        /// <param name="issueNumber"></param>
        /// <param name="issueNumberCaptured"></param>
        /// <param name="posId"></param>
        /// <param name="tenderId"></param>
        /// <param name="evaluationDateTime"></param>
        /// <param name="custId"></param>
        /// <param name="availableBalance"></param>
        /// <param name="taxExempt"></param>
        /// <param name="discountSurcharges"></param>
        /// <param name="errorCode"></param>
        /// <param name="deniedText"></param>
        /// <param name="connection"></param>
        public override void ValidateSystemRootPolicy(
            string cardNum,
            string issueNumber,
            bool issueNumberCaptured,
            int posId,
            int tenderId,
            DateTimeOffset evaluationDateTime,
            out int custId,
            out decimal availableBalance,
            out bool taxExempt,
            out List<DiscountSurchargeRule> discountSurcharges,
            out int errorCode,
            out string deniedText,
            ConnectionInfo connection = null)
        {
            custId = 0;
            availableBalance = 0;
            taxExempt = false;
            discountSurcharges = new List<DiscountSurchargeRule>();
            errorCode = 0;
            deniedText = string.Empty;

            using (var con = connection == null ? OracleConnectionGet(ConnectionString) : OracleConnectionGet(connection))
            {
                string paddedCardNumber = Formatting.PadCardNumber(cardNum, 22, '0');
                OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "Validation.ValidateSystemPolicy", CommandType = CommandType.StoredProcedure };
                //Input
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCardNum", OracleDbType = OracleDbType.Varchar2, Size = 22, Direction = ParameterDirection.Input, Value = paddedCardNumber });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIssueNumber", OracleDbType = OracleDbType.Varchar2, Size = 4, Direction = ParameterDirection.Input, Value = issueNumber });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pIssueNumberCaptured", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Input, Value = issueNumberCaptured ? "T" : "F" });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pPosId", OracleDbType = OracleDbType.Int64, Size = 0, Direction = ParameterDirection.Input, Value = posId });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTenderId", OracleDbType = OracleDbType.Int64, Size = 0, Direction = ParameterDirection.Input, Value = tenderId });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTransactionDateTime", OracleDbType = OracleDbType.Double, Size = 0, Direction = ParameterDirection.Input, Value = evaluationDateTime.LocalDateTime.ToOADate() });
                //Output
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pCustId", OracleDbType = OracleDbType.Int64, Size = 0, Direction = ParameterDirection.Output });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pAvailableBalance", OracleDbType = OracleDbType.Int64, Size = 10, Direction = ParameterDirection.Output });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pTaxExempt", OracleDbType = OracleDbType.Varchar2, Size = 1, Direction = ParameterDirection.Output });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDiscountSurcharges", OracleDbType = OracleDbType.RefCursor, Size = 0, Direction = ParameterDirection.Output });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pErrorCode", OracleDbType = OracleDbType.Int64, Size = 0, Direction = ParameterDirection.Output });
                cmd.Parameters.Add(new OracleParameter { ParameterName = "pDeniedText", OracleDbType = OracleDbType.Varchar2, Size = 1000, Direction = ParameterDirection.Output });

                try
                {
                    // Open the database connection
                    con.Open();

                    using (var r = cmd.ExecuteReader())
                    {
                        errorCode = ErrorCodeNormalize(cmd.Parameters["pErrorCode"]);
                        deniedText = cmd.Parameters["pDeniedText"].Value.ToString();
                        if (errorCode != 0) return;

                        custId = Convert.ToInt32(cmd.Parameters["pCustId"].Value.ToString());
                        availableBalance = Convert.ToDecimal(cmd.Parameters["pAvailableBalance"].Value.ToString()) / 1000m;
                        taxExempt = cmd.Parameters["pTaxExempt"].Value.ToString() == "T";
                        while (r.Read())
                        {
                            var item = new DiscountSurchargeRule();

                            if (!r.IsDBNull(0)) item.Type = (DiscountSurchargeType)r.GetInt32(0);
                            if (!r.IsDBNull(1)) item.Rate = r.GetInt32(1) / 100000m;

                            discountSurcharges.Add(item);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }

        #endregion
    }
}
