﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using BbTS.Core;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.ProfitCenter;
using BbTS.Domain.Models.Report;
using BbTS.Monitoring.Logging;
using Oracle.ManagedDataAccess.Client;

namespace BbTS.Resource.Database.Concrete.Oracle
{
    public partial class OracleSource
    {
        /// <inheritdoc />
        public override int PeriodNumberFromDeviceGuidGet(string deviceGuid, DateTime timestamp)
        {
            using (OracleConnection con = OracleConnectionGet(ConnectionString))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "TransactionFunctions.ProfitCenterTimePeriodGet", CommandType = CommandType.StoredProcedure })
                {
                    try
                    {
                        cmd.Parameters.Add(new OracleParameter("pDeviceGuid", OracleDbType.Varchar2, 36, deviceGuid, ParameterDirection.Input));
                        cmd.Parameters.Add(new OracleParameter("pTransactionDateTime", OracleDbType.Double, 14, timestamp.ToOADate(), ParameterDirection.Input));

                        cmd.Parameters.Add(new OracleParameter("pTimePeriod", OracleDbType.Int32, ParameterDirection.Output) { Size = 10 });

                        con.Open();
                        cmd.ExecuteNonQuery();
                        
                        return Convert.ToInt32(cmd.Parameters["pTimePeriod"].Value.ToString());
                    }
                    catch (Exception ex)
                    {
                        throw new ResourceLayerException("", Formatting.FormatException(ex));
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }

        /// <inheritdoc />
        public override List<TsProfitCenter> ProfitCenterGet(int? id = null)
        {
            var list = new List<TsProfitCenter>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "ProfitFunctions.ProfitCenterGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pProfitCenter_Id", OracleDbType = OracleDbType.Int16, Direction = ParameterDirection.Input, Value = id });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var profitCenterId = !r.IsDBNull(0) ? r.GetInt32(0) : -1;
                                var merchantId = !r.IsDBNull(1) ? r.GetInt32(1) : -1;
                                var name = !r.IsDBNull(2) ? r.GetString(2) : string.Empty;
                                var codeName = !r.IsDBNull(3) ? r.GetString(3) : string.Empty;
                                var guid = !r.IsDBNull(4) ? r.GetString(4) : string.Empty;

                                var profitCenter = new TsProfitCenter()
                                {

                                    ProfitCenter_Id = profitCenterId,
                                    Merchant_Id = merchantId,
                                    Name = name,
                                    Code_Name = codeName,
                                    ProfitCenterGuid = guid
                                };
                                list.Add(profitCenter);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <inheritdoc />
        public override List<TsProfitCenter> ProfitCenterForUserGet(string userId, int? id = null)
        {
            var list = new List<TsProfitCenter>();
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "ProfitFunctions.ProfitCenterForUserGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pUserName", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = userId });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pProfitCenter_Id", OracleDbType = OracleDbType.Int16, Direction = ParameterDirection.Input, Value = id });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pList", OracleDbType = OracleDbType.RefCursor, Direction = ParameterDirection.Output });
                        con.Open();

                        using (var r = cmd.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                var profitCenterId = !r.IsDBNull(0) ? r.GetInt32(0) : -1;
                                var merchantId = !r.IsDBNull(1) ? r.GetInt32(1) : -1;
                                var name = !r.IsDBNull(2) ? r.GetString(2) : string.Empty;
                                var codeName = !r.IsDBNull(3) ? r.GetString(3) : string.Empty;
                                var guid = !r.IsDBNull(4) ? r.GetString(4) : string.Empty;

                                var profitCenter = new TsProfitCenter
                                {

                                    ProfitCenter_Id = profitCenterId,
                                    Merchant_Id = merchantId,
                                    Name = name,
                                    Code_Name = codeName,
                                    ProfitCenterGuid = guid
                                };
                                list.Add(profitCenter);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
            return list;
        }

        /// <inheritdoc />
        public override MealsCountReport ProfitCenterMealsCountGet(Guid profitCenterGuid)
        {
            using (var con = OracleConnectionGet(ConnectionString))
            {
                try
                {
                    Guard.IsNotNullOrWhiteSpace(ConnectionString, "ConnectionString");
                    using (var cmd = new OracleCommand { Connection = con, CommandText = "ProfitFunctions.MealCountGet", CommandType = CommandType.StoredProcedure })
                    {
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pProfitCenterGuid", OracleDbType = OracleDbType.Varchar2, Size = 36, Direction = ParameterDirection.Input, Value = profitCenterGuid.ToString("D") });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pProfitCenterName", OracleDbType = OracleDbType.Varchar2, Size = 30, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pCurrentPeriod", OracleDbType = OracleDbType.Int16, Size = 4, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPeriodName", OracleDbType = OracleDbType.Varchar2, Size = 15, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pDayMealsServed", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new OracleParameter { ParameterName = "pPeriodMealsServed", OracleDbType = OracleDbType.Int32, Size = 10, Direction = ParameterDirection.Output });
                        con.Open();

                        cmd.ExecuteNonQuery();
                        return new MealsCountReport()
                        {
                            BoardPeriod = (Int16)TryGetIntValue(cmd.Parameters["pCurrentPeriod"], 0),
                            BoardPeriodName = cmd.Parameters["pPeriodName"].Value?.ToString(),
                            Location = cmd.Parameters["pProfitCenterName"].Value?.ToString(),
                            MealsCountDay = TryGetIntValue(cmd.Parameters["pDayMealsServed"], 0),
                            MealsCountPeriod = TryGetIntValue(cmd.Parameters["pPeriodMealsServed"], 0)
                        };
                    }
                }
                catch (Exception ex)
                {
                    throw new ResourceLayerException("", Formatting.FormatException(ex));
                }
                finally
                {
                    con.Close();
                }
            }
        }
    }
}
