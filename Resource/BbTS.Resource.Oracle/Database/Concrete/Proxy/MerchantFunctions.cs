﻿using BbTS.Domain.Models.Merchant;
using System.Collections.Generic;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {

        /// <summary>
        /// Get merchant
        /// </summary>
        /// <param name="merchantGuid">Merchant's guid</param>
        /// <returns>Merchant</returns>
        public override TsMerchant MerchantGet(string merchantGuid)
        {
            return Resource?.MerchantGet(merchantGuid);
        }

        /// <summary>
        /// Get merchants
        /// </summary>
        /// <returns>Merchants</returns>
        public override List<TsMerchant> MerchantGetAll()
        {
            return Resource?.MerchantGetAll();
        }
    }
}
