﻿using System;
using BbTS.Domain.Models.Credential;
using BbTS.Domain.Models.Definitions.Container;
using BbTS.Domain.Models.System.Database;
using BbTS.Domain.Models.Transaction;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    /// <summary>
    /// Resource layer class for handling storage and retrieval of Credential data.
    /// </summary>
    public partial class ProxySource
    {
        /// <summary>
        /// Retrieve a full customer credential from the credential value.
        /// </summary>
        /// <param name="credentialValue">The credential value</param>
        /// <param name="cardNumber">The card number associated with the supplied credential</param>
        /// <returns>Error code.  0 = success.</returns>
        public override string CredentialValidateOnCardFormat(string credentialValue, out string cardNumber)
        {
            cardNumber = null;
            return Resource?.CredentialValidateOnCardFormat(credentialValue, out cardNumber);
        }

        /// <summary>
        /// Retrieve basic card credential information, validating card format in the process.
        /// </summary>
        /// <param name="credentialValue">Raw track 2 data.</param>
        /// <param name="requestId">The unique id assigned to the request.</param>
        /// <returns>Card format pieces.  Throws WebApiException on errors.</returns>
        public override BasicCardCredential BasicCardCredentialGet(string credentialValue, string requestId = null)
        {
            return Resource?.BasicCardCredentialGet(credentialValue);
        }

        /// <summary>
        /// Verifies that the customer and card is valid.
        /// </summary>
        /// <param name="cardCapture"></param>
        /// <param name="pin"></param>
        /// <param name="transactionClassification"></param>
        /// <param name="posId"></param>
        /// <param name="customerId"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public override ProcessingResult CustomerAndCardIsValid(CardCapture cardCapture, int? pin, TransactionClassification transactionClassification, int posId, out int customerId, ConnectionInfo connection = null)
        {
            throw new NotImplementedException();
        }
    }
}
