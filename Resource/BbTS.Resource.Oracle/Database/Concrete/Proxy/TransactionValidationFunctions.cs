﻿using BbTS.Domain.Models.Transaction.Processing;
using BbTS.Domain.Models.Transaction.Validation;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        /// <summary>
        /// Process a retail transaction begin request against the resource layer.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The results of the operation.</returns>
        public override RetailTransactionBeginProcessResponse RetailTransactionBeginProcess(RetailTransactionBeginProcessRequest request)
        {
            return Resource?.RetailTransactionBeginProcess(request);
        }

        /// <summary>
        /// Validate the board meal type id.
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Result of the validation operation.</returns>
        public override BoardMealTypeValidationResponse BoardMealTypeValidate(BoardMealTypeValidationRequest request)
        {
            return Resource?.BoardMealTypeValidate(request);
        }

        /// <summary>
        /// Validate the customer transaction info portion of a transaction.
        /// </summary>
        /// <param name="request">The customer transaction info.</param>
        /// <returns>The result of the operation.</returns>
        public override CustomerTransactionInfoValidationResponse CustomerTransactionInfoValidation(CustomerTransactionInfoValidationRequest request)
        {
            return Resource?.CustomerTransactionInfoValidation(request);
        }

        /// <summary>
        /// Validate a cashier (operator) component of a transaction.
        /// </summary>
        /// <param name="cashierGuid">The unique (Guid) identification value assigned to the cashier (operator).</param>
        /// <param name="deviceGuid">The unique (Guid) identifier assigned to the device.</param>
        /// <param name="forcePost">Force post flag, defaults to false</param>
        /// <returns>The results of the operation.</returns>
        public override OperatorByOperatorGuidValidationResponse OperatorByOperatorGuidValidation(string cashierGuid, string deviceGuid, bool forcePost)
        {
            return Resource?.OperatorByOperatorGuidValidation(cashierGuid, deviceGuid, forcePost);
        }

        /// <summary>
        /// Validate a product promo component of a transaction.
        /// </summary>
        /// <param name="productPromoId"></param>
        /// <returns>Result of a product promo validation request.</returns>
        public override ProductPromoValidationResponse ProductPromoValidation(int productPromoId)
        {
            return Resource?.ProductPromoValidation(productPromoId);
        }

        /// <summary>
        /// Validate a retail transaction line product price modify component of a transaction.
        /// </summary>
        /// <param name="request">The request that spawned this response.</param>
        /// <returns>Result of the validation process</returns>
        public override RetailTransactionLineProductPriceModifierResponse RetailTransactionLineProductPriceModifyValidation(RetailTransactionLineProductPriceModifierRequest request)
        {
            return Resource?.RetailTransactionLineProductPriceModifyValidation(request);
        }

        /// <summary>
        /// Validate a retail transaction line product component of a transaction.
        /// </summary>
        /// <param name="request">The request that spawned this response.</param>
        /// <returns>Result of a retail transaction line product validation request.</returns>
        public override RetailTransactionLineProductValidationResponse RetailTransactionLineProductValidation(RetailTransactionLineProductValidationRequest request)
        {
            return Resource?.RetailTransactionLineProductValidation(request);
        }

        /// <summary>
        /// Validate a retail transaction line product tax component of a transaction.
        /// </summary>
        /// <param name="request">The request that spawned this response.</param>
        /// <returns>Result of a retail transaction line product validation request.</returns>
        public override RetailTransactionLineProductTaxResponse RetailTransactionLineProductTaxValidation(RetailTransactionLineProductTaxRequest request)
        {
            return Resource?.RetailTransactionLineProductTaxValidation(request);
        }

        /// <summary>
        /// Validate a retail transaction line product tax override component of a transaction.
        /// </summary>
        /// <param name="request">The request that spawned this response.</param>
        /// <returns>Result of the validation process</returns>
        public override RetailTransactionLineProductTaxOverrideResponse RetailTransactionLineProductTaxOverrideValidation(RetailTransactionLineProductTaxOverrideRequest request)
        {
            return Resource?.RetailTransactionLineProductTaxOverrideValidation(request);
        }

        /// <summary>
        /// Validate a retail transaction line tender component of a transaction.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Result of the validation process</returns>
        public override RetailTransactionLineTenderValidationResponse RetailTranactionLineTenderValidation(RetailTransactionLineTenderValidationRequest request)
        {
            return Resource?.RetailTranactionLineTenderValidation(request);
        }

        /// <summary>
        /// Validate a retail transaction line tender customer validation component of a transaction.
        /// </summary>
        /// <param name="request">The information necessary for validation.</param>
        /// <returns>The result of the operation.</returns>
        public override RetailTransactionLineTenderCustomerValidationResponse RetailTransactionLineTenderCustomerValidation(RetailTransactionLineTenderCustomerValidationRequest request)
        {
            return Resource?.RetailTransactionLineTenderCustomerValidation(request);
        }

        /// <summary>
        /// Validate a retail transaction line tender credit card validation component of a transaction.
        /// </summary>
        /// <param name="request">The information necessary for validation.</param>
        /// <returns>Result of the validation process</returns>
        public override RetailTransactionLineTenderCreditCardResponse RetailTransactionLineTenderCreditCardValidation(RetailTransactionLineTenderCreditCardRequest request)
        {
            return Resource?.RetailTransactionLineTenderCreditCardValidation(request);
        }

        /// <summary>
        /// Validate the attributes of a transaction.
        /// </summary>
        /// <param name="request">Request object with attributes to validate.</param>
        /// <returns>Results of the operation.</returns>
        public override TransactionAttributeValidationResponse TransactionAttributeValidation(TransactionAttributeValidationRequest request)
        {
            return Resource?.TransactionAttributeValidation(request);
        }
    }
}
