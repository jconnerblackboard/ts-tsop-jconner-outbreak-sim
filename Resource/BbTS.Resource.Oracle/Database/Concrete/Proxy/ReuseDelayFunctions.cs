﻿using System.Collections.Generic;
using BbTS.Domain.Models.DoorAccess;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        /// <inheritdoc />
        public override List<TsReuseDelayLog> ReuseDelayLogListGet(int customerId)
        {
            return Resource?.ReuseDelayLogListGet(customerId);
        }

        /// <inheritdoc />
        public override void ReuseDelayLogReset(int customerId, List<int> reuseDelayLogIds)
        {
            Resource?.ReuseDelayLogReset(customerId, reuseDelayLogIds);
        }
    }
}