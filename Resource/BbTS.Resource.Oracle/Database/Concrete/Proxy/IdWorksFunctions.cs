﻿using System.Collections.Generic;
using BbTS.Domain.Models.IdWorks;
using BbTS.Domain.Models.System.Database;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        public override IEnumerable<Idw_Card> IdwCardsGet(ConnectionInfo connection, string source)
        {
            return Resource?.IdwCardsGet(connection, source);
        }

        /// <summary>
        /// Set the IDWorks CSN Caputure information.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The result of the operation.</returns>
        public override IdWorksCsnCaptureResponse IdWorksCsnCaptureSet(IdWorksCsnCaptureRequest request)
        {
            return Resource?.IdWorksCsnCaptureSet(request);
        }
    }
}
