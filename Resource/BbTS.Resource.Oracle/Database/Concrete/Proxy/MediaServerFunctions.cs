﻿using System.Collections.Generic;
using BbTS.Domain.Models.MediaServer;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {

        /// <inheritdoc />
        public override void DeviceDelete(int deviceId, string identity, string authenticationKey)
        {
            Resource?.DeviceDelete(deviceId, identity, authenticationKey);
        }

        /// <inheritdoc />
        public override List<DeviceView> DeviceGet(int? deviceId, string macAddress, string identity, string authenticationKey)
        {
            return Resource?.DeviceGet(deviceId, macAddress, identity, authenticationKey);
        }

        /// <inheritdoc />
        public override void DeviceSet(int? deviceId, Device device, string identity, string authenticationKey)
        {
            Resource?.DeviceSet(deviceId, device, identity, authenticationKey);
        }

        /// <inheritdoc />
        public override void DeviceGroupDelete(int deviceGroupId, string identity, string authenticationKey)
        {
            Resource?.DeviceGroupDelete(deviceGroupId, identity, authenticationKey);
        }

        /// <inheritdoc />
        public override List<DeviceGroup> DeviceGroupGet(int? deviceGroupId, string identity, string authenticationKey)
        {
            return Resource?.DeviceGroupGet(deviceGroupId, identity, authenticationKey);
        }

        /// <inheritdoc />
        public override void DeviceGroupSet(int? deviceGroupId, DeviceGroup deviceGroup, string identity, string authenticationKey)
        {
            Resource?.DeviceGroupSet(deviceGroupId, deviceGroup, identity, authenticationKey);
        }

        /// <inheritdoc />
        public override void DeviceGroupRssFeedDelete(int deviceGroupId, int rssFeedId, string identity, string authenticationKey)
        {
            Resource?.DeviceGroupRssFeedDelete(deviceGroupId, rssFeedId, identity, authenticationKey);
        }

        /// <inheritdoc />
        public override List<DeviceGroupRssFeed> DeviceGroupRssFeedGet(int? deviceGroupId, int? rssFeedId, string identity, string authenticationKey)
        {
            return Resource?.DeviceGroupRssFeedGet(deviceGroupId, rssFeedId, identity, authenticationKey);
        }

        /// <inheritdoc />
        public override void DeviceGroupRssFeedSet(DeviceGroupRssFeed deviceGroupRssFeed, string identity, string authenticationKey)
        {
            Resource?.DeviceGroupRssFeedSet(deviceGroupRssFeed, identity, authenticationKey);
        }

        /// <inheritdoc />
        public override void PlaylistDelete(int playlistId, string identity, string authenticationKey)
        {
            Resource?.PlaylistDelete(playlistId, identity, authenticationKey);
        }

        /// <inheritdoc />
        public override List<Playlist> PlaylistGet(int? playlistId, string identity, string authenticationKey)
        {
            return Resource?.PlaylistGet(playlistId, identity, authenticationKey);
        }

        /// <inheritdoc />
        public override void PlaylistSet(int? playlistId, Playlist playlist, string identity, string authenticationKey)
        {
            Resource?.PlaylistSet(playlistId, playlist, identity, authenticationKey);
        }

        /// <inheritdoc />
        public override void PlaylistItemDelete(int playlistItemId, string identity, string authenticationKey)
        {
            Resource?.PlaylistItemDelete(playlistItemId, identity, authenticationKey);
        }

        /// <inheritdoc />
        public override List<PlaylistItem> PlaylistItemGet(int? playlistItemId, string identity, string authenticationKey)
        {
            return Resource?.PlaylistItemGet(playlistItemId, identity, authenticationKey);
        }

        /// <inheritdoc />
        public override List<PlaylistItem> PlaylistItemsGet(int? playlistId, string identity, string authenticationKey)
        {
            return Resource?.PlaylistItemsGet(playlistId, identity, authenticationKey);
        }

        /// <inheritdoc />
        public override void PlaylistItemSet(int? playlistItemId, PlaylistItem playlistItem, string identity, string authenticationKey)
        {
            Resource?.PlaylistItemSet(playlistItemId, playlistItem, identity, authenticationKey);
        }

        /// <inheritdoc />
        public override void PlaylistItemSequenceSet(int playlistId, string sequence, string identity, string authenticationKey)
        {
            Resource?.PlaylistItemSequenceSet(playlistId, sequence, identity, authenticationKey);
        }

        /// <inheritdoc />
        public override void MediaFileDelete(int mediaFileId, string identity, string authenticationKey)
        {
            Resource?.MediaFileDelete(mediaFileId, identity, authenticationKey);
        }

        /// <inheritdoc />
        public override List<MediaFile> MediaFileGet(int? mediaFileId, string identity, string authenticationKey)
        {
            return Resource?.MediaFileGet(mediaFileId, identity, authenticationKey);
        }

        /// <inheritdoc />
        public override void MediaFileSet(int? mediaFileId, MediaFile mediaFile, string identity, string authenticationKey)
        {
            Resource?.MediaFileSet(mediaFileId, mediaFile, identity, authenticationKey);
        }

        /// <inheritdoc />
        public override void MediaFileDetailDelete(int mediaFileDetailId, string identity, string authenticationKey)
        {
            Resource?.MediaFileDetailDelete(mediaFileDetailId, identity, authenticationKey);
        }

        /// <inheritdoc />
        public override void MediaFileDetailSet(int? mediaFileDetailId, MediaFileDetail mediaFileDetail, string identity, string authenticationKey)
        {
            Resource?.MediaFileDetailSet(mediaFileDetailId, mediaFileDetail, identity, authenticationKey);
        }

        /// <inheritdoc />
        public override List<MediaFileDetail> MediaFileDetailGet(int? mediaFileDetailId, int? mediaFileId, string identity, string authenticationKey)
        {
            return Resource?.MediaFileDetailGet(mediaFileDetailId, mediaFileId, identity, authenticationKey);
        }

        /// <inheritdoc />
        public override List<NameValuePair> SupportedFileTypeGet(string identity, string authenticationKey)
        {
            return Resource?.SupportedFileTypeGet(identity, authenticationKey);
        }

        /// <inheritdoc />
        public override List<RssFeed> RssFeedGet(int? rssFeedId, string identity, string authenticationKey)
        {
            return Resource?.RssFeedGet(rssFeedId, identity, authenticationKey);
        }

        /// <inheritdoc />
        public override void RssFeedDelete(int rssFeedId, string identity, string authenticationKey)
        {
            Resource?.RssFeedDelete(rssFeedId, identity, authenticationKey);
        }

        /// <inheritdoc />
        public override void RssFeedSet(int? rssFeedId, RssFeed rssFeed, string identity, string authenticationKey)
        {
            Resource?.RssFeedSet(rssFeedId, rssFeed, identity, authenticationKey);
        }

        /// <inheritdoc />
        public override UserValidate UserGet(string identity, string authenticationKey)
        {
            return Resource?.UserGet(identity, authenticationKey);
        }

        /// <inheritdoc />
        public override List<RolePermission> UserResourcePermissionGet(string roleResourceName, string identity, string authenticationKey)
        {
            return Resource?.UserResourcePermissionGet(roleResourceName, identity, authenticationKey);
        }
    }
}
