﻿using System;
using BbTS.Domain.Models.Container;
using BbTS.Domain.Models.Definitions.Container;
using BbTS.Domain.Models.Exceptions.Resource;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        /// <summary>
        /// Resource class for setting a transaction log in an oracle database.
        /// </summary>
        /// <param name="transactionMf4100"><see cref="Transaction"/> object to set.</param>
        [Obsolete("This should only be used for MF4100 backward-compatibility purposes.")]
        public override void TransactionLogSet(Transaction transactionMf4100)
        {
            Resource?.TransactionLogSet(transactionMf4100);
        }

        /// <summary>
        /// Resource class for setting a transaction log in an oracle database.  (Transaction MF4100 View Version 1)
        /// </summary>
        /// <param name="transactionMf4100ViewV01"><see cref="TransactionMf4100ViewV01"/> object to set.</param>
        /// <param name="requestId">The request ID, if available.</param>
        /// <exception cref="ResourceLayerException"></exception>
        public override void TransactionLogSet(TransactionMf4100ViewV01 transactionMf4100ViewV01, Guid? requestId)
        {
            Resource?.TransactionLogSet(transactionMf4100ViewV01, requestId);
        }

        /// <summary>
        /// Resource class for setting a transaction log in an oracle database.  (Transaction View Version 1)
        /// </summary>
        /// <param name="transactionViewV01"><see cref="TransactionViewV01"/> object to set.</param>
        /// <param name="requestId">The request ID, if available.</param>
        /// <exception cref="ResourceLayerException"></exception>
        public override void TransactionLogSet(TransactionViewV01 transactionViewV01, Guid? requestId)
        {
            Resource?.TransactionLogSet(transactionViewV01, requestId);
        }

        /// <summary>
        /// Resource class for setting a transaction log in an oracle database.  (Transaction View Version 2)
        /// </summary>
        /// <param name="transactionViewV02"><see cref="TransactionViewV02"/> object to set.</param>
        /// <param name="requestId">The request ID, if available.</param>
        /// <exception cref="ResourceLayerException"></exception>
        public override void TransactionLogSet(TransactionViewV02 transactionViewV02, Guid? requestId)
        {
            Resource?.TransactionLogSet(transactionViewV02, requestId);
        }

        /// <summary>
        /// Resource class for setting a transaction log in an oracle database when the transaction information is not parsable.  This should only be used when incoming JSON cannot be deserialized.
        /// </summary>
        /// <param name="clob">The string clob to write to the data layer.</param>
        /// <param name="disposition">The disposition to set the clob to.  Default to <see cref="Disposition.Unknown"/>.</param>
        /// <param name="requestId">The request ID, if available.</param>
        public override void TransactionLogSet(string clob, Disposition disposition, Guid? requestId)
        {
            Resource?.TransactionLogSet(clob, disposition);
        }
    }
}
