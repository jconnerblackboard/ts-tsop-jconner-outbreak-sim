﻿using System;
using BbTS.Domain.Models.Customer;
using BbTS.Domain.Models.ExternalClient;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        /// <summary>
        /// Get external client customer data
        /// </summary>
        /// <param name="customerClientGuid">Client customer guid</param>
        /// <returns>External client customer data</returns>
        public override ExternalClientCustomer ExternalClientCustomerGet(Guid customerClientGuid)
        {
            return Resource?.ExternalClientCustomerGet(customerClientGuid);
        }

        /// <inheritdoc />
        public override ProcessCustomerRegistrationPostResponse ProcessCustomerRegistration(ProcessCustomerRegistrationPostRequest request)
        {
            return Resource?.ProcessCustomerRegistration(request);
        }
    }
}