﻿using System;
using System.Collections.Generic;
using BbTS.Domain.Models.Credential;
using BbTS.Domain.Models.Customer;
using BbTS.Domain.Models.System.Database;
using BbTS.Domain.Models.Card;
using BbTS.Domain.Models.Credential.Mobile;
using BbTS.Domain.Models.Customer.Management;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        public override List<TsCard> CardGet(ConnectionInfo connection)
        {
            return Resource?.CardGet(connection);
        }

        /// <summary>
        /// Get a list of card formats for a device.
        /// </summary>
        /// <param name="deviceId">The unique identifier for the device.</param>
        /// <returns>List of <see cref="CardFormatForDeviceSettings"/> for the requested device.</returns>
        public override List<CardFormatForDeviceSettings> CardFormatDeviceSettingsGet(string deviceId)
        {
            return Resource?.CardFormatDeviceSettingsGet(deviceId);
        }

        /// <summary>
        /// Get a list of card formats for a device.
        /// </summary>
        /// <returns>List of <see cref="TsCardFormat"/> for the requested device.</returns>
        public override List<TsCardFormat> CardFormatGet()
        {
            return Resource?.CardFormatGet();
        }

        /// <summary>
        /// This method will return a list of card to customer guid mapping objects.  If connection is null, ConnectionString will be used
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<CardToCustomerGuidMapping> CardToCustomerGuidGet(ConnectionInfo connection = null)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get a card record by card number.
        /// </summary>
        /// <param name="cardNumber">Card number, left padded to 22 characters</param>
        /// <returns><see cref="TsCard"/> instance</returns>
        public override TsCard CardGetByCardNumber(string cardNumber)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get a card by its guid
        /// </summary>
        /// <param name="cardGuid">Card's guid</param>
        /// <returns>Card</returns>
        public override TsCard CardGetByGuid(string cardGuid)
        {
            return Resource?.CardGetByGuid(cardGuid);
        }

        /// <summary>
        /// Get cards
        /// </summary>
        /// <param name="request">Filtering request</param>
        /// <returns>Cards</returns>
        public override List<TsCard> CardGetAll(CardGetAllRequest request)
        {
            return Resource?.CardGetAll(request);
        }

        /// <summary>
        /// Remotes temporary customer card
        /// </summary>
        /// <param name="cardNumber">Card number</param>
        public override void CardUpdateTemporary(string cardNumber)
        {
            Resource?.CardUpdateTemporary(cardNumber);
        }

        /// <summary>
        /// Adds new card to customer
        /// </summary>
        /// <param name="customerNumber">Customer number</param>
        /// <param name="card">Card</param>
        /// <returns>Created customer card</returns>
        public override CustomerCard CardCreate(string customerNumber, CustomerCard card)
        {
            return Resource?.CardCreate(customerNumber, card);
        }

        /// <summary>
        /// Updates existing customer card
        /// </summary>
        /// <param name="customerNumber">Customer number</param>
        /// <param name="cardNumber">Card number</param>
        /// <param name="card">card</param>
        /// <returns>Updated customer card</returns>
        public override void CardUpdate(string customerNumber, string cardNumber, PatchCustomerCard card)
        {
            Resource?.CardUpdate(customerNumber, cardNumber, card);
        }

        /// <summary>
        /// Create/Update a card.
        /// </summary>
        /// <param name="credential">Full card credential information.</param>
        public override void CardSet(CardCredentialComplete credential)
        {
            Resource?.CardSet(credential);
        }

        /// <summary>
        /// Get a mobile card credential by card number.
        /// </summary>
        /// <param name="cardNumber">The unique identifier for the card.</param>
        /// <returns>Mobile credential information for the card.</returns>
        public override MobileCredentialGetResponse MobileCredentialGet(string cardNumber)
        {
            return Resource?.MobileCredentialGet(cardNumber);
        }

        /// <summary>
        /// Patch a mobile card credential by card number.
        /// </summary>
        /// <param name="request">The patch request.</param>
        /// <returns>Results of the operation.</returns>
        public override MobileCredentialPatchResponse MobileCredentialPatch(MobileCredentialPatchRequest request)
        {
            return Resource?.MobileCredentialPatch(request);
        }

        /// <summary>
        /// Post a mobile card credential by card number.
        /// </summary>
        /// <param name="request">The post request.</param>
        /// <returns>Results of the operation.</returns>
        public override MobileCredentialPostResponse MobileCredentialPost(MobileCredentialPostRequest request)
        {
            return Resource?.MobileCredentialPost(request);
        }

        /// <inheritdoc />
        public override List<TsCard> CardsTemporaryUnassignedGet()
        {
            return Resource?.CardsTemporaryUnassignedGet();
        }

        /// <inheritdoc />
        public override CardNumberAvailableResponse CardNumberAvailable(decimal cardNumber)
        {
            return Resource?.CardNumberAvailable(cardNumber);
        }

        /// <inheritdoc />
        public override List<CardAssignmentHistoryItem> CardHistoryGet(string cardNumber)
        {
            return Resource?.CardHistoryGet(cardNumber);
        }
    }
}