﻿using BbTS.Domain.Models.Transaction.StoredValue;
using System;
using System.Collections.Generic;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        /// <summary>
        /// Initializes the DB facility for retrieving recent transactions.
        /// </summary>
        /// <param name="initExtractionStart">Extraction Start to be used as an override of the internal DB value, may be null</param>
        /// <param name="initExtractionEnd">Extraction End to be used as an override of the internal DB value, may be null</param>
        /// <returns>void</returns>
        public override void InitializeStoredValueTransactionsExtraction(DateTime? initExtractionStart, DateTime? initExtractionEnd)
        {
            Resource?.InitializeStoredValueTransactionsExtraction(initExtractionStart, initExtractionEnd);
        }

        /// <summary>
        /// Commits that the extracted transactions were successfully processed and the DB facility can update its internal state.
        /// </summary>
        /// <returns>void</returns>
        public override void FinalizeStoredValueTransactionsExtraction()
        {
            Resource?.FinalizeStoredValueTransactionsExtraction();
        }

        /// <summary>
        /// Resets the facility for retrieving the transactions from DB.
        /// </summary>
        /// <returns>void</returns>
        public override void ResetStoredValueTransactionsExtraction()
        {
            Resource?.ResetStoredValueTransactionsExtraction();
        }

        /// <summary>
        /// Requests start of transaction retrieval. Returned context is used in consequent calls of ExtractStoredValueTransactionsGetNext,
        /// which in turn returns a chunk of transactions. This method should be called after InitializeStoredValueTransactionsExtraction.
        /// Returned context needs to be used in consequent calls of <see cref="ExtractStoredValueTransactionsGetNext"/>
        /// Once all the data are retrieved, FinalizeStoredValueTransactionsExtraction must be called to commit that this round was successful
        /// and to advance to new time frame.
        /// <param name="extractionStart">output parameter - extraction start</param>
        /// <param name="extractionEnd">output parameter - extraction end</param>
        /// </summary>
        /// <returns>context to be used for chunked retrieval, <see cref="IExtractStoredValueTransactionsContext"/>.</returns>
        public override IExtractStoredValueTransactionsContext ExtractStoredValueTransactionsBegin(out DateTime extractionStart, out DateTime extractionEnd)
        {
            extractionStart = extractionEnd = DateTime.MinValue;
            return Resource?.ExtractStoredValueTransactionsBegin(out extractionStart, out extractionEnd);
        }

        /// <summary>
        /// Extract up to maxItem items. The context indicates whether there are more data or not. If there are more data then this method needs to be called again.
        /// </summary>
        /// <param name="context"><see cref="IExtractStoredValueTransactionsContext"/> - context for chunked retrieval, returned by <see cref="ResourceDatabase.ExtractStoredValueTransactionsBegin"/></param>
        /// <param name="maxItems"> - maximum number of items to be retrieved in this call</param>
        /// <returns>List of <see cref="StoredValueTransaction"/> transactions.</returns>
        public override List<StoredValueTransaction> ExtractStoredValueTransactionsGetNext(IExtractStoredValueTransactionsContext context, int maxItems) {
            return Resource?.ExtractStoredValueTransactionsGetNext(context, maxItems);
        }
    }
}
