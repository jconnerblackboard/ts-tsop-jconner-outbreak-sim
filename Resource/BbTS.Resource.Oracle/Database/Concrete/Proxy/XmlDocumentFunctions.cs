﻿using BbTS.Core.Logging.Tracing;
using BbTS.Domain.Models.System.Database;
using BbTS.Domain.Models.XmlDocument;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    /// <summary>
    /// This class exposes the various functions that are contained in the XmlDocument space of Envision
    /// </summary>
    [Trace(AttributePriority = 2)]
    public partial class ProxySource
    {
        /// <summary>
        /// This method will return the string representation of the xml document stored in the Envision database
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="documentType">The document type to retrieve</param>
        /// <returns></returns>
        public override TsXmlDocument XmlDocumentByTypeGet(ConnectionInfo connection, string documentType)
        {
            return Resource?.XmlDocumentByTypeGet(connection, documentType);
        }

        /// <summary>
        /// This method will return the string representation of the xml document stored in the Envision database
        /// </summary>
        /// <param name="documentType">The document type to retrieve</param>
        /// <returns></returns>
        public override TsXmlDocument XmlDocumentGet(string documentType)
        {
            return Resource?.XmlDocumentGet(documentType);
        }



        /// <summary>
        /// This method will save an XmlDocument to the Envision database.
        /// </summary>
        /// <param name="document">The document type to save</param>
        /// <returns></returns>
        public override TsXmlDocument XmlDocumentSet(TsXmlDocument document)
        {
            return Resource?.XmlDocumentSet(document);
        }

        /// <summary>
        /// This method will save an XmlDocument to the Envision database for the given connection object
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="document">The document type to save</param>
        /// <returns></returns>
        public override TsXmlDocument XmlDocumentSet(ConnectionInfo connection, TsXmlDocument document)
        {
            return Resource?.XmlDocumentSet(connection, document);
        }

        /// <summary>
        /// This method will delete an XmlDocument from the Envision database.
        /// </summary>
        /// <param name="documentType">The document type to delete</param>
        /// <returns></returns>
        public override void XmlDocumentDelete(string documentType)
        {
            Resource?.XmlDocumentDelete(documentType);
        }
    }
}
