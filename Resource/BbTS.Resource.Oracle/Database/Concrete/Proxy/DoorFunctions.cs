﻿using System;
using System.Collections.Generic;
using BbTS.Domain.Models.Communication.BbHost;
using BbTS.Domain.Models.DoorAccess;
using BbTS.Domain.Models.Event;
using BbTS.Domain.Models.System.Database;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        /// <summary>
        /// This method will return a list of Area objects from the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsArea> AreaGet(ConnectionInfo connection)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of Building objects from the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsBuilding> BuildingsGet(ConnectionInfo connection)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void CustomerDaRequestHistoryByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> CustomerDaRequestHistoryDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            return Resource?.CustomerDaRequestHistoryDateListGet(connection, oleAutomationDate);
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsCustomer_Da_Request_History> CustomerDaRequestHistoryListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            return Resource?.CustomerDaRequestHistoryListByDateGet(connection, oleAutomationDate);
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void DaDeniedTransactionByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsDa_DeniedTransaction> DaDeniedTransactionListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> DaDeniedTransactionDaysFilteredByDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void DaDeniedTransactionCardByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsDa_DeniedTransaction_Card> DaDeniedTransactionCardListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> DaDeniedTransactionCardDaysFilteredByDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void DaDoorAlarmAckInfoByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsDa_DoorAlarm_AckInfo> DaDoorAlarmAckInfoListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> DaDoorAlarmAckInfoDaysFilteredByDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void DaDoorAlarmLogByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsDa_DoorAlarmLog> DaDoorAlarmLogListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> DaDoorAlarmLogDaysFilteredByDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void DaDoorCurrentStatusByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsDa_DoorCurrentStatus> DaDoorCurrentStatusListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> DaDoorCurrentStatusDaysFilteredByDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connectionInfo"></param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void DaDoorEventLogByDateDelete(ConnectionInfo connectionInfo, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsDa_DoorEventLog> DaDoorEventLogListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> DaDoorEventLogDaysFilteredByDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public override List<TsDoorAccessPlan> DaPlansGetByCustomer(int customerId)
        {
            return Resource?.DaPlansGetByCustomer(customerId);
        }

        /// <inheritdoc />
        public override List<TsDoorOverride> DoorOverridesGetByCustomer(int customerId)
        {
            return Resource?.DoorOverridesGetByCustomer(customerId);
        }

        /// <inheritdoc />
        public override List<CustomerDoorAccessLog> CustomerDoorAccessLogsGet(DateTime startDate, DateTime endDate)
        {
            return Resource?.CustomerDoorAccessLogsGet(startDate, endDate);
        }

        /// <inheritdoc />
        public override List<TsDaPermissionBase> DaPermissionsGet(Int16 merchantId)
        {
            return Resource?.DaPermissionsGet(merchantId);
        }

        /// <inheritdoc />
        public override List<TsDoorGroup> DoorGroupsGet(Int16 merchantId)
        {
            return Resource?.DoorGroupsGet(merchantId);
        }

        /// <inheritdoc />
        public override List<TsEventGroup> EventGroupsGet(Int16 merchantId)
        {
            return Resource?.EventGroupsGet(merchantId);
        }

        /// <inheritdoc />
        public override List<Building> BuildingAreasGet()
        {
            return Resource?.BuildingAreasGet();
        }

        /// <summary>
        /// This method will return a list of Doors objects from the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsDoor> DoorsGet(ConnectionInfo connection)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connectionInfo"></param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void DaDoorStateOverrideByDateDelete(ConnectionInfo connectionInfo, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsDa_DoorStateOverride> DaDoorStateOverrideListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> DaDoorStateOverrideDaysFilteredByDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of MasterController objects from the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsMasterController> MasterControllersGet(ConnectionInfo connection)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void DaTransactionByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsDa_Transaction> DaTransactionListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> DaTransactionDaysFilteredByDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void DaTransactionCardByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsDa_Transaction_Card> DaTransactionCardListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> DaTransactionCardDaysFilteredByDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get a list of door access plans
        /// </summary>
        /// <param name="doorAccessPlanId">Id filter</param>
        /// <returns>Door access plans</returns>
        public override List<TsDoorAccessPlan> DoorAccessPlanGet(int? doorAccessPlanId = null)
        {
            return Resource?.DoorAccessPlanGet(doorAccessPlanId);
        }

        /// <inheritdoc />
        public override List<TsReuseDelay> DaReuseDelaysGet()
        {
            return Resource?.DaReuseDelaysGet();
        }

        /// <summary>
        /// Get the host connection from a door id.
        /// </summary>
        /// <param name="doorId">the numberical identifier for the door.</param>
        /// <returns></returns>
        public override StatsConnection HostConnectionFromDoorIdGet(int doorId)
        {
            return Resource?.HostConnectionFromDoorIdGet(doorId);
        }

        /// <summary>
        /// Get a list of host connection information for a set of doors defined by numerical id.
        /// </summary>
        /// <param name="request">List of the the numberical identifier for the doors.</param>
        /// <returns></returns>
        public override List<StatsConnection> HostConnectionListGet(DoorStateChangeRequest request)
        {
            return Resource?.HostConnectionListGet(request);
        }
    }
}
