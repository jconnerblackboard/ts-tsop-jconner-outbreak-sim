﻿using System;
using System.Collections.Generic;
using BbTS.Domain.Models.Report;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        /// <summary>
        /// Gets drawer audit
        /// </summary>
        /// <param name="sessionGuid"></param>
        /// <returns>Drawer audit</returns>
        public override TsDrawerReport DrawerAuditGet(string sessionGuid)
        {
            return Resource?.DrawerAuditGet(sessionGuid);
        }

        /// <summary>
        /// Get a pos audit
        /// </summary>
        /// <param name="originatorGuid"></param>
        /// <param name="businessDayType"></param>
        /// <returns>Report</returns>
        public override TsDailyReport PosAuditGet(string originatorGuid, int? businessDayType)
        {
            return Resource?.PosAuditGet(originatorGuid, businessDayType);
        }

        /// <summary>
        /// Get a profit center audit
        /// </summary>
        /// <param name="profitCenterGuid"></param>
        /// <param name="businessDayType"></param>
        /// <returns>Report</returns>
        public override TsDailyReport ProfitCenterAuditGet(string profitCenterGuid, int? businessDayType)
        {
            return Resource?.ProfitCenterAuditGet(profitCenterGuid, businessDayType);
        }

        /// <inheritdoc />
        public override List<OperatorSession> OperatorSessionsGet(Guid originatorGuid, int cashDrawerNumber)
        {
            return Resource?.OperatorSessionsGet(originatorGuid, cashDrawerNumber);
        }
    }
}
