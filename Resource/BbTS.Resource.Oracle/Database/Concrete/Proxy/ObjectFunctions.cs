﻿using System.Collections.Generic;
using BbTS.Domain.Models.Object;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        /// <inheritdoc />
        public override List<ObjectDefFieldValue> ObjectFieldValueGet(int? objectId, int? definedFieldId)
        {
            return Resource?.ObjectFieldValueGet(objectId, definedFieldId);
        }

        /// <inheritdoc />
        public override void ObjectFieldValueDelete(int objectId, int definedFieldId)
        {
            Resource?.ObjectFieldValueDelete(objectId, definedFieldId);
        }

        /// <summary>
        /// Set object field value
        /// </summary>
        /// <param name="objectFieldValue">Object def field value to be set</param>
        public override void ObjectFieldValueSet(ObjectDefFieldValue objectFieldValue)
        {
            Resource?.ObjectFieldValueSet(objectFieldValue);
        }
    }
}
