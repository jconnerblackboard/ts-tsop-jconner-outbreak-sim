﻿using System;
using System.Collections.Generic;
using BbTS.Domain.Models.Definitions.Transaction;
using BbTS.Domain.Models.RetailTransaction;
using BbTS.Domain.Models.Retail_Tran;
using BbTS.Domain.Models.Retail_Tran_LineItem;
using BbTS.Domain.Models.Retail_Tran_LineItem_Comment;
using BbTS.Domain.Models.Retail_Tran_LineItem_Discount;
using BbTS.Domain.Models.Retail_Tran_LineItem_Mdb_Info;
using BbTS.Domain.Models.Retail_Tran_LineItem_Prod;
using BbTS.Domain.Models.Retail_Tran_LineItem_ProdPrcMd;
using BbTS.Domain.Models.Retail_Tran_LineItem_ProdPromo;
using BbTS.Domain.Models.Retail_Tran_LineItem_Prod_Tax;
using BbTS.Domain.Models.Retail_Tran_LineItem_Prod_Tx_E;
using BbTS.Domain.Models.Retail_Tran_LineItem_Prod_Tx_O;
using BbTS.Domain.Models.Retail_Tran_LineItem_StoredVal;
using BbTS.Domain.Models.Retail_Tran_LineItem_Surcharge;
using BbTS.Domain.Models.Retail_Tran_LineItem_Sv_Card;
using BbTS.Domain.Models.Retail_Tran_LineItem_Sv_Cust;
using BbTS.Domain.Models.Retail_Tran_LineItem_Sv_Enrich;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tax;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tender;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tender_Cc;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tender_Sv;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tndr_Card;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tndr_Cdv;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tndr_Ce;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tndr_Cgv;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tndr_Cust;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tndr_Disc;
using BbTS.Domain.Models.Retail_Tran_LineItem_Tndr_Srch;
using BbTS.Domain.Models.Retail_Tran_Total;
using BbTS.Domain.Models.Session_Control;
using BbTS.Domain.Models.StoredValueDenial;
using BbTS.Domain.Models.Sv_Account_History_Rtl_Trn_Sv;
using BbTS.Domain.Models.Sv_Account_History_Rtl_Trn_Tdr;
using BbTS.Domain.Models.System.Database;
using BbTS.Domain.Models.TiaTransactionLog;
using BbTS.Domain.Models.Transaction;
using BbTS.Domain.Models.Transaction.Processing;
using BbTS.Domain.Models.TransactionObjectLog;
using BbTS.Domain.Models.Transaction_Cashier;
using BbTS.Domain.Models.Transaction_Communication;
using BbTS.Domain.Models.Transaction_External_Client;
using BbTS.Domain.Models.Transaction_Laundry_Machine;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        /// <summary>
        /// This method will return a list of records for the CreditCardType objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsCredit_Card_Type> CreditCardTypeArchivesGet(ConnectionInfo connection)
        {
            return Resource?.CreditCardTypeArchivesGet(connection);
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_TranByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_TranDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override List<TsRetail_Tran> Retail_TranListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItemByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem> Retail_Tran_LineItemListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItemDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_CommentByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Comment> Retail_Tran_LineItem_CommentListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_CommentDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_DiscountByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Discount> Retail_Tran_LineItem_DiscountListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_DiscountDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Process a retail transaction line item product price modifier request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The results of the operation.</returns>
        public override RetailTransactionLineProductPriceModifierResponse RetailTransactionLineProductPriceModifierProcess(RetailTransactionLineProductPriceModifierRequest request)
        {
            return Resource?.RetailTransactionLineProductPriceModifierProcess(request);
        }

        /// <summary>
        /// Process a retail transaction line item product process request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The results of the operation.</returns>
        public override RetailTransactionLineProductProcessResponse RetailTransactionLineProductProcess(RetailTransactionLineProductProcessRequest request)
        {
            return Resource?.RetailTransactionLineProductProcess(request);
        }

        /// <summary>
        /// Process a retail transaction line item product tax request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The results of the operation.</returns>
        public override RetailTransactionLineProductTaxResponse RetailTransactionLineProductTaxProcess(RetailTransactionLineProductTaxRequest request)
        {
            return Resource?.RetailTransactionLineProductTaxProcess(request);
        }

        /// <summary>
        /// Process a retail transaction line item product tax override request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The results of the operation.</returns>
        public override RetailTransactionLineProductTaxOverrideResponse RetailTransactionLineProductTaxOverrideProcess(RetailTransactionLineProductTaxOverrideRequest request)
        {
            return Resource?.RetailTransactionLineProductTaxOverrideProcess(request);
        }

        /// <summary>
        /// Process a retail transaction line item tender process request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The results of the operation.</returns>
        public override RetailTransactionLineTenderResponse RetailTransactionLineTenderProcess(RetailTransactionLineTenderRequest request)
        {
            return Resource?.RetailTransactionLineTenderProcess(request);
        }

        /// <summary>
        /// Process a retail transaction line tender SV Card request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The results of the operation.</returns>
        public override RetailTransactionLineTenderSvCardProcessResponse RetailTransactionLineTenderSvCardProcess(RetailTransactionLineTenderSvCardProcessRequest request)
        {
            return Resource?.RetailTransactionLineTenderSvCardProcess(request);
        }

        /// <inheritdoc />
        public override RetailTransactionLineTenderCreditCardResponse RetailTransactionLineTenderCreditCardProcess(int transactionId, RetailTransactionLineTenderCreditCardRequest request)
        {
            return Resource?.RetailTransactionLineTenderCreditCardProcess(transactionId, request);
        }

        /// <inheritdoc />
        public override RetailTransactionLineTenderCreditCardHitResponse RetailTransactionLineTenderCreditCardHitProcess(int transactionId, RetailTransactionLineTenderCreditCardHitRequest request)
        {
            return Resource?.RetailTransactionLineTenderCreditCardHitProcess(transactionId, request);
        }

        /// <inheritdoc />
        public override RetailTransactionLineTenderCashEquivResponse RetailTransactionLineTenderCashEquivProcess(int transactionId, RetailTransactionLineTenderCashEquivRequest request)
        {
            return Resource?.RetailTransactionLineTenderCashEquivProcess(transactionId, request);
        }

        /// <summary>
        /// Process a retail transaction line tender discount request.
        /// </summary>
        /// <param name="transactionId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public override RetailTransactionLineTenderDiscountResponse RetailTransactionLineTenderDiscountProcess(int transactionId, RetailTransactionLineTenderDiscountRequest request)
        {
            return Resource?.RetailTransactionLineTenderDiscountProcess(transactionId, request);
        }

        /// <summary>
        /// Process a retail transaction line tender surcharge request.
        /// </summary>
        /// <param name="transactionId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public override RetailTransactionLineTenderSurchargeResponse RetailTransactionLineTenderSurchargeProcess(int transactionId, RetailTransactionLineTenderSurchargeRequest request)
        {
            return Resource?.RetailTransactionLineTenderSurchargeProcess(transactionId, request);
        }

        /// <summary>
        /// Process a retail transaction line stored value type card request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The results of the operation.</returns>
        public override RetailTransactionLineStoredValueTypeCardResponse RetailTransactionLineStoredValueTypeCardProcess(RetailTransactionLineStoredValueTypeCardRequest request)
        {
            return Resource?.RetailTransactionLineStoredValueTypeCardProcess(request);
        }

        /// <inheritdoc />
        public override RetailTransactionLineStoredValueEnrichmentResponse RetailTransactionLineStoredValueEnrichmentProcess(RetailTransactionLineStoredValueEnrichmentRequest request)
        {
            return Resource?.RetailTransactionLineStoredValueEnrichmentProcess(request);
        }

        /// <summary>
        /// Process a retail transaction begin request against the resource layer.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The results of the operation.</returns>
        public override RetailTransactionEndProcessResponse RetailTransactionEndProcess(RetailTransactionEndProcessRequest request)
        {
            return Resource?.RetailTransactionEndProcess(request);
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_Mdb_InfoByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Mdb_Info> Retail_Tran_LineItem_Mdb_InfoListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_Mdb_InfoDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_ProdByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Prod> Retail_Tran_LineItem_ProdListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_ProdDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_Prod_TaxByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Prod_Tax> Retail_Tran_LineItem_Prod_TaxListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_Prod_TaxDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_Prod_Tx_EByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Prod_Tx_E> Retail_Tran_LineItem_Prod_Tx_EListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_Prod_Tx_EDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_Prod_Tx_OByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Prod_Tx_O> Retail_Tran_LineItem_Prod_Tx_OListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_Prod_Tx_ODateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_ProdPrcMdByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_ProdPrcMd> Retail_Tran_LineItem_ProdPrcMdListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_ProdPrcMdDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_ProdPromoByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_ProdPromo> Retail_Tran_LineItem_ProdPromoListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_ProdPromoDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_StoredValByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_StoredVal> Retail_Tran_LineItem_StoredValListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_StoredValDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_SurchargeByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Surcharge> Retail_Tran_LineItem_SurchargeListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_SurchargeDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_Sv_CardByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Sv_Card> Retail_Tran_LineItem_Sv_CardListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_Sv_CardDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_Sv_CustByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Sv_Cust> Retail_Tran_LineItem_Sv_CustListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_Sv_CustDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_Sv_EnrichByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Sv_Enrich> Retail_Tran_LineItem_Sv_EnrichListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_Sv_EnrichDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_TaxByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Tax> Retail_Tran_LineItem_TaxListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_TaxDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_TenderByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Tender> Retail_Tran_LineItem_TenderListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_TenderDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_Tender_CcByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Tender_Cc> Retail_Tran_LineItem_Tender_CcListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_Tender_CcDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_Tender_SvByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Tender_Sv> Retail_Tran_LineItem_Tender_SvListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_Tender_SvDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_Tndr_CardByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Tndr_Card> Retail_Tran_LineItem_Tndr_CardListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_Tndr_CardDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_Tndr_CdvByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Tndr_Cdv> Retail_Tran_LineItem_Tndr_CdvListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_Tndr_CdvDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_Tndr_CeByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Tndr_Ce> Retail_Tran_LineItem_Tndr_CeListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_Tndr_CeDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_Tndr_CgvByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Tndr_Cgv> Retail_Tran_LineItem_Tndr_CgvListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_Tndr_CgvDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_Tndr_CustByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Tndr_Cust> Retail_Tran_LineItem_Tndr_CustListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_Tndr_CustDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_Tndr_DiscByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Tndr_Disc> Retail_Tran_LineItem_Tndr_DiscListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_Tndr_DiscDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_LineItem_Tndr_SrchByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_LineItem_Tndr_Srch> Retail_Tran_LineItem_Tndr_SrchListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_LineItem_Tndr_SrchDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Retail_Tran_TotalByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsRetail_Tran_Total> Retail_Tran_TotalListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Retail_Tran_TotalDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Session_ControlByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsSession_Control> Session_ControlListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Session_ControlDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void StoredValueDenialByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsStoredValueDenial> StoredValueDenialListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> StoredValueDenialDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Sv_Account_History_Rtl_Trn_SvByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsSv_Account_History_Rtl_Trn_Sv> Sv_Account_History_Rtl_Trn_SvListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Sv_Account_History_Rtl_Trn_SvDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Sv_Account_History_Rtl_Trn_TdrByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsSv_Account_History_Rtl_Trn_Tdr> Sv_Account_History_Rtl_Trn_TdrListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Sv_Account_History_Rtl_Trn_TdrDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the Tender objects in the system.  If connection is null, ConnectionString will be used
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsTender> TenderArchivesGet(ConnectionInfo connection = null)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public override List<TsTender2> TenderGet(TenderType? tenderType)
        {
            return Resource?.TenderGet(tenderType);
        }

        /// <inheritdoc />
        public override TransactionHistoryListGetResponse TransactionHistoryListGet(TransactionHistoryListGetRequest request)
        {
            return Resource?.TransactionHistoryListGet(request);
        }

        /// <summary>
        /// Get the tenders assigned to a device.
        /// </summary>
        /// <param name="deviceId">The unique identifier for the device.</param>
        /// <returns>List of tenders assigned to the device.</returns>
        public override List<TenderDeviceSetting> TenderDeviceSettingsGet(string deviceId)
        {
            return Resource?.TenderDeviceSettingsGet(deviceId);
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void TiaTransactionLogByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsTiaTransactionLog> TiaTransactionLogListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> TiaTransactionLogDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Transaction_CashierByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsTransaction_Cashier> Transaction_CashierListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Transaction_CashierDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Transaction_CommunicationByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsTransaction_Communication> Transaction_CommunicationListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Transaction_CommunicationDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Transaction_External_ClientByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsTransaction_External_Client> Transaction_External_ClientListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Transaction_External_ClientDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void Transaction_Laundry_MachineByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsTransaction_Laundry_Machine> Transaction_Laundry_MachineListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> Transaction_Laundry_MachineDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will delete a group objects from the given table based on a provided day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        public override void TransactionObjectLogByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsTransactionObjectLog> TransactionObjectLogListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsTransactionObjectLog> TransactionObjectLogListGet(ConnectionInfo connection)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> TransactionObjectLogDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<TsTransaction> TransactionListByDateGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        public override void TransactionByDateDelete(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of dates where records exist for the given table based on a provided Julian formatted day
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <param name="oleAutomationDate">A Julian formatted date</param>
        /// <returns></returns>
        public override List<int> TransactionDateListGet(ConnectionInfo connection, int oleAutomationDate)
        {
            throw new NotImplementedException();
        }
    }
}
