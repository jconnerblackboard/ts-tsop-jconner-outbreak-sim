﻿using BbTS.Domain.Models.System.Security.Oauth;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        /// <summary>
        /// Set an authentication oauth token in the database layer
        /// </summary>
        /// <param name="token">The token to store in the database layer.</param>
        public override void AuthenticationOauthTokenSet(AuthenticationOauthToken token)
        {
            Resource?.AuthenticationOauthTokenSet(token);
        }

        /// <summary>
        /// Get an authentication oauth token from the database layer.
        /// </summary>
        /// <param name="key">The token key to retrieve from the store.</param>
        /// <returns>Stored token.</returns>
        public override AuthenticationOauthToken AuthenticationOauthTokenGet(string key)
        {
            return Resource?.AuthenticationOauthTokenGet(key);
        }

        /// <inheritdoc />
        public override bool ApiPermissionIsAllowed(string authenticationKey, string objectName)
        {
            return Resource?.ApiPermissionIsAllowed(authenticationKey, objectName) ?? false;
        }
    }
}
