﻿using System;
using System.Collections.Generic;
using BbTS.Domain.Models.IfMan;
using BbTS.Domain.Models.System.Database;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        public override IEnumerable<Clone_Card> CloneCardsGet(ConnectionInfo connection, string source)
        {
            return Resource?.CloneCardsGet(connection, source);
        }
        public override IEnumerable<IfmAgents> IfmAgentsGet(ConnectionInfo connection, string source)
        {
            return Resource?.IfmAgentsGet(connection, source);
        }
        public override IEnumerable<IfmAgentsAvailable> IfmAgentsAvailableGet(ConnectionInfo connection, string source)
        {
            return Resource?.IfmAgentsAvailableGet(connection, source);
        }
        public override IEnumerable<IfmMasterFieldList> IfmMasterFieldListGet(ConnectionInfo connection, string source)
        {
            return Resource?.IfmMasterFieldListGet(connection, source);
        }
        public override IEnumerable<IfmMasterQueue_Change> IfmMasterQueueChangeGet(ConnectionInfo connection, string source)
        {
            return Resource?.IfmMasterQueueChangeGet(connection, source);
        }
        public override IEnumerable<IfmMasterQueue_Fields> IfmMasterQueueFieldsGet(ConnectionInfo connection, string source)
        {
            return Resource?.IfmMasterQueueFieldsGet(connection, source);
        }
        public override IEnumerable<IfmMasterQueue_KeyFields> IfmMasterQueueKeyFieldsGet(ConnectionInfo connection, string source)
        {
            return Resource?.IfmMasterQueueKeyFieldsGet(connection, source);
        }
        public override IEnumerable<IfmMasterQueue_Photo> IfmMasterQueuePhotoGet(ConnectionInfo connection, string source)
        {
            return Resource?.IfmMasterQueuePhotoGet(connection, source);
        }
        public override IEnumerable<IfmMasterQueue_SendTo> IfmMasterQueueSendToGet(ConnectionInfo connection, string source)
        {
            return Resource?.IfmMasterQueueSendToGet(connection, source);
        }
        public override IEnumerable<IfmRecordPool> IfmRecordPoolGet(ConnectionInfo connection, string source)
        {
            return Resource?.IfmRecordPoolGet(connection, source);
        }
        public override IEnumerable<IfmRecordPool_Fields> IfmRecordPoolFieldsGet(ConnectionInfo connection, string source)
        {
            return Resource?.IfmRecordPoolFieldsGet(connection, source);
        }
        public override IEnumerable<IfmRecordPool_KeyFields> IfmRecordPoolKeyFieldsGet(ConnectionInfo connection, string source)
        {
            return Resource?.IfmRecordPoolKeyFieldsGet(connection, source);
        }
        public override IEnumerable<IfmUserAgentRights> IfmUserAgentRightsGet(ConnectionInfo connection, string source)
        {
            return Resource?.IfmUserAgentRightsGet(connection, source);
        }
        public override IEnumerable<IfmUserLog> IfmUserLogGet(ConnectionInfo connection, string source)
        {
            return Resource?.IfmUserLogGet(connection, source);
        }
        public override IEnumerable<IfmUserSetup> IfmUserSetupGet(ConnectionInfo connection, string source)
        {
            return Resource?.IfmUserSetupGet(connection, source);
        }
    }
}