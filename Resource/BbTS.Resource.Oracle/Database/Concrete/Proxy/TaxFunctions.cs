﻿using System.Collections.Generic;
using BbTS.Domain.Models.RetailTransaction;
using BbTS.Domain.Models.System.Database;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        /// <summary>
        /// This method will return a list of records for the TaxGroups objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsTaxGroups> TaxGroupsArchivesGet(ConnectionInfo connection)
        {
            return Resource?.TaxGroupsArchivesGet(connection);
        }

        /// <summary>
        /// This method will return a list of records for the TaxSchedule objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsTaxSchedule> TaxScheduleArchivesGet(ConnectionInfo connection)
        {
            return Resource?.TaxScheduleArchivesGet(connection);
        }

        /// <summary>
        /// Get the Tax Schedules for a device.
        /// </summary>
        /// <param name="deviceId">The unique identifier for the Device.</param>
        /// <returns>List of tax schedules for a device.</returns>
        public override List<TaxScheduleDeviceSetting> TaxScheduleDeviceSettingsGet(string deviceId)
        {
            return Resource?.TaxScheduleDeviceSettingsGet(deviceId);
        }

        /// <summary>
        /// Get all tenders associated with a tax schedule.
        /// </summary>
        /// <param name="taxScheduleId">The unique identifier for the tax schedule</param>
        /// <returns>List of tenders and tax rates associated with the tax schedule.</returns>
        public override List<TaxScheduleTender> TaxScheduleTendersGet(int taxScheduleId)
        {
            return Resource?.TaxScheduleTendersGet(taxScheduleId);
        }
    }
}