﻿using System;
using BbTS.Domain.Models.Customer;
using BbTS.Domain.Models.Exceptions.WebApi;
using BbTS.Domain.Models.System.Database;
using System.Collections.Generic;
using BbTS.Domain.Models.Customer.Management;
using BbTS.Domain.Models.Customer.Management.BoardPlan;
using BbTS.Domain.Models.Customer.Management.Email;
using BbTS.Domain.Models.Customer.Management.EventPlan;
using BbTS.Domain.Models.Customer.Management.StoredValue;
using BbTS.Domain.Models.Event;
using BbTS.Domain.Models.DoorAccess;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        #region Customer Defined Fields

        /// <summary>
        /// This method will return the list of customers names in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsCustomerArchive> CustomerArchivesGet(ConnectionInfo connection)
        {
            return Resource?.CustomerArchivesGet(connection);
        }

        /// <summary>
        /// Set a value for a customer defined field using the stored proc "CustomerFunctions.CustomerDefFieldValueSet"
        /// </summary>
        /// <param name="custId">CUST_ID field on the CUSTOMER_DEF_FIELD_VALUE table</param>
        /// <param name="customerDefFieldDefId">CUSTOMER_DEF_FIELD_DEF_ID field on the CUSTOMER_DEF_FIELD_VALUE table</param>
        /// <param name="fieldValue">FIELD_VALUE field on the CUSTOMER_DEF_FIELD_VALUE table</param>
        public override void CustomerDefinedFieldValueSet(int custId, int customerDefFieldDefId, string fieldValue)
        {
            Resource?.CustomerDefinedFieldValueSet(custId, customerDefFieldDefId, fieldValue);
        }

        /// <summary>
        /// Set a value for a customer defined group using the stored proc "CustomerFunctions.CustomerDefGroupValueSet"
        /// </summary>
        /// <param name="custId">CUST_ID field on the CUSTOMER_DEF_GRP_VALUE table</param>
        /// <param name="customerDefGrpDefId">CUSTOMER_DEF_GRP_DEF_ID field on the CUSTOMER_DEF_GRP_DEF_ITEM table</param>
        /// <param name="customerDefGrpDefItemId">CUSTOMER_DEF_GRP_DEF_ITEM_ID field on the CUSTOMER_DEF_GRP_DEF_ITEM table</param>
        public override void CustomerDefinedGroupValueSet(int custId, int customerDefGrpDefId, int customerDefGrpDefItemId)
        {
            Resource?.CustomerDefinedGroupValueSet(custId, customerDefGrpDefId, customerDefGrpDefItemId);
        }

        /// <summary>
        /// Get the cusotmer id from the provded email address.
        /// </summary>
        /// <param name="emailAddress">Customer email address.</param>
        /// <returns>Customer Id.  Throws <see cref="WebApiException"/> on error.</returns>
        public override string CustomerGuidFromEmailAddress(string emailAddress)
        {
            return Resource?.CustomerGuidFromEmailAddress(emailAddress);
        }

        /// <summary>
        /// Get the unique customer identifier (guid) from the customer card number.
        /// </summary>
        /// <param name="customerGuid">Customer guid.</param>
        /// <returns>A shortened list of customer information needed by device login.</returns>
        public override CredentialVerifyCustomer CustomerInformationFromCustomerGuidGet(string customerGuid)
        {
            return Resource?.CustomerInformationFromCustomerGuidGet(customerGuid);
        }

        /// <summary>
        /// Get the customer's PIN.
        /// </summary>
        /// <param name="customerGuid">The unique (guid) identifier for the customer.</param>
        /// <returns>Customer's PIN or null if not found.</returns>
        public override string CustomerPinGet(string customerGuid)
        {
            return Resource?.CustomerPinGet(customerGuid);
        }

        /// <summary>
        /// Get a customer's board balance.
        /// </summary>
        /// <param name="customerId">The unique (Guid) identifier for the customer.</param>
        /// <param name="deviceId">The unique (Guid) identifier for the device.</param>
        /// <returns>List of board plans assigned to the customer.</returns>
        public override List<CustomerBoardPlan> CustomerBoardBalanceGet(string customerId, string deviceId)
        {
            return Resource?.CustomerBoardBalanceGet(customerId, deviceId);
        }


        /// <summary>
        /// Get a customer's event balance.
        /// </summary>
        /// <param name="customerId">The unique (Guid) identifier for the customer.</param>
        /// <param name="deviceId">The unique (Guid) identifier for the device.</param>
        /// <returns>List of event balances assigned to the customer.</returns>
        public override List<CustomerEventBalance> CustomerEventBalanceGet(string customerId, string deviceId)
        {
            return Resource?.CustomerEventBalanceGet(customerId, deviceId);
        }

        /// <summary>
        /// Get a customer's stored value balance.
        /// </summary>
        /// <param name="customerId">The unique (Guid) identifier for the customer.</param>
        /// <param name="deviceId">The unique (Guid) identifier for the device.</param>
        /// <returns>List of stored value account balances assigned to the customer.</returns>
        public override List<CustomerStoredValueBalance> CustomerStoredValueBalanceGet(string customerId, string deviceId)
        {
            return Resource?.CustomerStoredValueBalanceGet(customerId, deviceId);
        }

        /// <summary>
        /// This method will return a list of records for the CustomerDefFieldDef objects in the system
        /// </summary>
        /// <param name="customerDefinedFieldId">Id filter</param>
        /// <returns>List of CustomerDefFieldDef</returns>
        public override List<TsCustomer_Def_Field_Def> CustomerDefFieldDefGet(int? customerDefinedFieldId = null)
        {
            return Resource?.CustomerDefFieldDefGet(customerDefinedFieldId);
        }

        /// <summary>
        /// This method will return a list of records for the TsCustomerDefFieldValue objects in the system
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <returns>List of TsCustomerDefFieldValue</returns>
        public override List<TsCustomerDefFieldValue> CustomerDefFieldValueGet(int customerId)
        {
            return Resource?.CustomerDefFieldValueGet(customerId);
        }

        /// <summary>
        /// Get Customer Defined Group Item Ids
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <returns>Customer Defined Group Item Ids</returns>
        public override List<int> CustomerDefGroupItemIdsGet(int customerId)
        {
            return Resource?.CustomerDefGroupItemIdsGet(customerId);
        }

        /// <summary>
        /// This method will return a list of records for the CustomerDefGrpDef objects in the system
        /// </summary>
        /// <param name="customerDefGroupDefId">Id filter</param>
        /// <returns>List of CustomerDefGrpDef</returns>
        public override List<TsCustomerDefGroupDef> CustomerDefGrpDefGet(int? customerDefGroupDefId = null)
        {
            return Resource?.CustomerDefGrpDefGet(customerDefGroupDefId);
        }

        /// <summary>
        /// This method will return a list of records for the TsCustomerDefGroupDef objects in the system
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <returns>List of TsCustomerDefGroupDef</returns>
        public override List<TsCustomerDefGroupDef> CustomerDefGrpValueGet(int customerId)
        {
            return Resource?.CustomerDefGrpValueGet(customerId);
        }

        /// <summary>
        /// This method will return a list of records for the CustomerDefFieldDef objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsCustomer_Def_Field_Def> CustomerDefFieldDefArchivesGet(ConnectionInfo connection)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the CustomerDefGrpDef objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsCustomer_Def_Grp_Def> CustomerDefGrpDefArchivesGet(ConnectionInfo connection)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the CustomerDefGrpDefItem objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsCustomer_Def_Grp_Def_Item> CustomerDefGrpDefItemArchivesGet(ConnectionInfo connection)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Get the cusotmer id from the provded customer card number.
        /// </summary>
        /// <param name="cardNumber">Customer card number.</param>
        /// <param name="issueNumber">Issue number for the card.</param>
        /// <returns>Customer Guid or null if not found.  Throws <see cref="WebApiException"/> on error.</returns>
        public override CustomerGuidFromCardNumberResponse CustomerGuidFromCardNumber(string cardNumber)
        {
            return Resource?.CustomerGuidFromCardNumber(cardNumber);
        }

        /// <summary>
        /// Get the customer numerical id (cust_id) from their unique identifier (CustomerGuid).
        /// </summary>
        /// <param name="guid">The unique identifier for the customer (CustomerGuid).</param>
        /// <returns>The customer numerical id (cust_id)</returns>
        public override int CustomerIdFromCustomerGuidGet(string guid)
        {
            return Resource.CustomerIdFromCustomerGuidGet(guid);
        }

        /// <summary>
        /// Get the unique identifier (CustomerGuid) for the customer from their customer numerical id (cust_id).
        /// </summary>
        /// <param name="id">The customer numerical id (cust_id).</param>
        /// <returns>The unique identifier (CustomerGuid) for the customer</returns>
        public override string CustomerGuidFromCustomerIdGet(int id)
        {
            return Resource?.CustomerGuidFromCustomerIdGet(id);
        }

        /// <summary>
        /// Get the customer information required by the credential verify process from the customer card number.
        /// </summary>
        /// <param name="cardNumber">Customer card number.</param>
        /// <returns>The customer information.</returns>
        public override CredentialVerifyCustomer CustomerInformationFromCardNumberGet(string cardNumber)
        {
            return Resource?.CustomerInformationFromCardNumberGet(cardNumber);
        }

        /// <summary>
        /// Get the customer defined group item ids for the specified customerGuid.  If customerGuid is null or empty, all customers will be considered.
        /// </summary>
        /// <param name="customerGuid"></param>
        /// <returns></returns>
        public override List<CustomerDefinedGroupItemIdsForCustomerGuid> CustomerDefinedGroupIdsForCustomerGuidGet(Guid? customerGuid)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Get the customer defined group item ids for the specified customerGuid.  If customerGuid is null or empty, all customers will be considered.
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="customerGuid"></param>
        /// <returns></returns>
        public override List<CustomerDefinedGroupItemIdsForCustomerGuid> CustomerDefinedGroupIdsForCustomerGuidGet(ConnectionInfo connection, string customerGuid)
        {
            throw new System.NotImplementedException();
        }

        /// <inheritdoc />
        public override CustomerAvailableBalance AvailableBalanceForCustomerGet(Guid customerId, DateTimeOffset evaluationDateTime, List<int> storedValueAccountTypeIds, List<int> transactionLimitIds)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public override CustomerPostResponse CustomerSet(TsCustomerPost customer)
        {
            return Resource?.CustomerSet(customer);
        }

        /// <inheritdoc />
        public override bool CustomerNumberAvailable(decimal customerNumber)
        {
            return Resource?.CustomerNumberAvailable(customerNumber) ?? true;
        }

        /// <summary>
        /// Get a customer by his guid
        /// </summary>
        /// <param name="customerGuid">Customer's guid</param>
        /// <returns>Customer</returns>
        public override TsCustomer CustomerGet(string customerGuid)
        {
            return Resource?.CustomerGet(customerGuid);
        }

        /// <summary>
        /// Gets the customer by id or number
        /// </summary>
        /// <param name="customerNumber">The customer number.</param>
        /// <param name="customerId">The customer id.</param>
        /// <returns>The Customer</returns>
        public override TsCustomer CustomerGet(string customerNumber, int? customerId)
        {
            return Resource?.CustomerGet(customerNumber, customerId);
        }

        /// <summary>
        /// Get a customer based upon the field 'CustomerNumber' in the Customer table.
        /// </summary>
        /// <param name="customerNumber">Corresponds to a value in the CustomerNumber field.</param>
        /// <returns>Customer object or null if not found</returns>
        public override TsCustomer CustomerGetByCustomerNumber(string customerNumber)
        {
            return Resource?.CustomerGetByCustomerNumber(customerNumber);
        }

        /// <summary>
        /// Creates new customer
        /// </summary>
        /// <param name="customer">Customer data</param>
        public override CustomerManagementData CustomerCreate(PostCustomer customer)
        {
            return Resource?.CustomerCreate(customer);
        }

        /// <summary>
        /// Updates new customer
        /// </summary>
        /// <param name="customerNumber"></param>
        /// <param name="customer">Customer data</param>
        public override void CustomerUpdate(string customerNumber, PatchCustomer customer)
        {
            Resource?.CustomerUpdate(customerNumber, customer);
        }

        /// <inheritdoc /> 
        public override void CustomerDelete(int customerId)
        {
            Resource?.CustomerDelete(customerId);
        }

        /// <inheritdoc /> 
        public override CustomerDeleteResponse CustomerDeleteAllowedGet(Guid customerGuid)
        {
            return Resource?.CustomerDeleteAllowedGet(customerGuid);
        }

        /// <summary>
        /// Get a customers
        /// </summary>
        /// <param name="customerId">Customer's id</param>
        /// <returns>Customers</returns>
        public override List<TsCustomer> CustomersGet(int? customerId = null)
        {
            return Resource?.CustomersGet(customerId);
        }

        /// <summary>
        /// Get all customers
        /// </summary>
        /// <param name="request">Filter</param>
        /// <returns>Filtered customers</returns>
        public override List<TsCustomer> CustomerGetAll(CustomerGetAllRequest request)
        {
            return Resource?.CustomerGetAll(request);
        }

        /// <summary>
        /// Get all customers
        /// </summary>
        /// <param name="request">Filter</param>
        /// <returns>Filtered customers</returns>
        public override CustomerListGetResponse CustomerListGet(CustomerListGetRequest request)
        {
            return Resource?.CustomerListGet(request);
        }

        /// <summary>
        /// Get customer board used
        /// </summary>
        /// <param name="customerGuid">Customer guid</param>
        /// <param name="boardPlanGuid">Board guid</param>
        /// <returns>Customer board used</returns>
        public override TsCustomerBoardDetailUsed CustomerBoardUsedGet(string customerGuid, string boardPlanGuid)
        {
            return Resource?.CustomerBoardUsedGet(customerGuid, boardPlanGuid);
        }

        /// <summary>
        /// Get all customer board used
        /// </summary>
        /// <param name="request">Filter</param>
        /// <returns>Filtered customer boards</returns>
        public override List<TsCustomerBoardDetailUsed> CustomerBoardUsedGetAll(CustomerBoardUsedGetAllRequest request)
        {
            return Resource?.CustomerBoardUsedGetAll(request);
        }

        /// <summary>
        /// Get customer board
        /// </summary>
        /// <param name="customerGuid">Customer guid</param>
        /// <param name="boardPlanGuid">Board guid</param>
        /// <returns>Customer board</returns>
        public override TsCustomerBoardDetail CustomerBoardGet(string customerGuid, string boardPlanGuid)
        {
            return Resource?.CustomerBoardGet(customerGuid, boardPlanGuid);
        }

        /// <summary>
        /// Get customer boards
        /// </summary>
        /// <param name="request">Filter</param>
        /// <returns>Filtered customer boards</returns>
        public override List<TsCustomerBoardDetail> CustomerBoardGetAll(CustomerBoardGetAllRequest request)
        {
            return Resource?.CustomerBoardGetAll(request);
        }

        /// <summary>
        /// Get customer cards
        /// </summary>
        /// <param name="customerGuid">Customer guid</param>
        /// <returns>Customer cards</returns>
        public override List<TsCard> CustomerCardsGet(string customerGuid)
        {
            return Resource?.CustomerCardsGet(customerGuid);
        }

        /// <summary>
        /// Get customer cards
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <returns>Customer cards</returns>
        public override List<TsCard> CustomerCardsGet(int customerId)
        {
            return Resource?.CustomerCardsGet(customerId);
        }

        /// <summary>
        /// Get customer photo
        /// </summary>
        /// <param name="customerGuid">Customer guid</param>
        /// <returns>Customer photo</returns>
        public override TsCustomerPhoto CustomerPhotoGet(string customerGuid)
        {
            return Resource?.CustomerPhotoGet(customerGuid);
        }

        /// <summary>
        /// Get customer emails
        /// </summary>
        /// <returns>Customer emails</returns>
        public override List<TsCustomerEmail> CustomerEmailsGet(int customerId)
        {
            return Resource?.CustomerEmailsGet(customerId);
        }

        /// <summary>
        /// Sets customer email address
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="email">Email</param>
        public override void CustomerEmailSet(int customerId, CustomerEmailAddressBase email)
        {
            Resource?.CustomerEmailSet(customerId, email);
        }

        /// <summary>
        /// Get list of customer email types
        /// </summary>
        /// <param name="customerEmailTypeId">Id filter</param>
        /// <returns>Customer email types</returns>
        public override List<TsCustomerEmailType> CustomerEmailTypeGet(int? customerEmailTypeId = null)
        {
            return Resource?.CustomerEmailTypeGet(customerEmailTypeId);
        }

        /// <summary>
        /// Get list of customer addresses
        /// </summary>
        /// <param name="customerId">Customer id</param>
        /// <returns>Customer addresses</returns>
        public override List<TsAddress> CustomerAddressesGet(int customerId)
        {
            return Resource?.CustomerAddressesGet(customerId);
        }

        /// <summary>
        /// Get customer event plans
        /// </summary>
        /// <param name="customerNumber">Customer number</param>
        /// <returns>List of event plans</returns>
        public override List<TsCustomerEventPlan> CustomerEventPlansGet(string customerNumber)
        {
            return Resource?.CustomerEventPlansGet(customerNumber);
        }

        /// <summary>
        /// Deletes customer event plan
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        public override void CustomerEventPlanDelete(int customerId)
        {
            Resource?.CustomerEventPlanDelete(customerId);
        }

        /// <summary>
        /// Creates or updates customer event plan
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="eventPlan">Event plan</param>
        /// <returns>Created or update event plan</returns>
        public override void CustomerEventPlanSet(int customerId, CustomerEventPlan eventPlan)
        {
            Resource?.CustomerEventPlanSet(customerId, eventPlan);
        }

        /// <summary>
        /// Get customer event plan overrides
        /// </summary>
        /// <param name="customerNumber">Customer number</param>
        /// <returns>Customer event plan overrides</returns>
        public override List<CustomerEventPlanOverride> CustomerEventPlanOverridesGet(string customerNumber)
        {
            return Resource?.CustomerEventPlanOverridesGet(customerNumber);
        }

        /// <summary>
        /// Deletes customer event plan overrides
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="eventId">Event Id</param>
        public override void CustomerEventPlanOverrideDelete(int customerId, int? eventId = null)
        {
            Resource?.CustomerEventPlanOverrideDelete(customerId, eventId);
        }

        /// <summary>
        /// Creates customer event plan override
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="eventPlanOverride">Event plan override</param>
        /// <returns>Customer event plan override</returns>
        public override void CustomerEventPlanOverrideCreate(int customerId, CustomerEventPlanOverride eventPlanOverride)
        {
            Resource?.CustomerEventPlanOverrideCreate(customerId, eventPlanOverride);
        }

        /// <summary>
        /// Updates customer event plan override
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="eventId">Event Id</param>
        /// <param name="eventPlanOverride">Event plan override</param>
        /// <returns>Customer event plan override</returns>
        public override void CustomerEventPlanOverrideUpdate(int customerId, int eventId, PutCustomerEventPlanOverride eventPlanOverride)
        {
            Resource?.CustomerEventPlanOverrideUpdate(customerId, eventId, eventPlanOverride);
        }

        /// <summary>
        /// Get customer defaults
        /// </summary>
        /// <returns>List of customer defaults</returns>
        public override List<TsCustomerDefault> CustomerDefaultsGet()
        {
            return Resource?.CustomerDefaultsGet();
        }

        /// <summary>
        /// Get customer board plans
        /// </summary>
        /// <param name="customerNumber">Customer number</param>
        /// <returns>Customer board plans</returns>
        public override List<CustomerManagementBoardPlan> CustomerBoardsGet(string customerNumber)
        {
            return Resource?.CustomerBoardsGet(customerNumber);
        }

        /// <summary>
        /// Adds new boardplan to customer
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="bp">Board plan</param>
        /// <returns>Customer board plan</returns>
        public override void CustomerBoardPlanCreate(int customerId, PostCustomerBoardPlan bp)
        {
            Resource?.CustomerBoardPlanCreate(customerId, bp);
        }

        /// <summary>
        /// Adds new boardplan to customer
        /// </summary>
        /// <param name="customerNumber">Customer number</param>
        /// <param name="boardPlanId"></param>
        /// <param name="bp">Board plan</param>
        /// <returns>Customer board plan</returns>
        public override void CustomerBoardPlanUpdate(string customerNumber, int boardPlanId, PatchCustomerBoardPlan bp)
        {
            Resource?.CustomerBoardPlanUpdate(customerNumber, boardPlanId, bp);
        }

        /// <summary>
        /// Deletes boardplans from customer
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="boardPlanId">Board plan Id (optional)</param>
        public override void CustomerBoardPlanDelete(int customerId, int? boardPlanId = null)
        {
            Resource?.CustomerBoardPlanDelete(customerId, boardPlanId);
        }

        /// <summary>
        /// Get customer door access plan Ids
        /// </summary>
        /// <param name="customerNumber">Customer number</param>
        /// <returns>Door access plan Ids</returns>
        public override List<int> CustomerDoorAccessPlansGet(string customerNumber)
        {
            return Resource?.CustomerDoorAccessPlansGet(customerNumber);
        }

        /// <summary>
        /// Assigns door access plan to customer
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="doorAccessPlanId">Door access plan Id</param>
        public override void CustomerDoorAccessPlanCreate(int customerId, int doorAccessPlanId)
        {
            Resource?.CustomerDoorAccessPlanCreate(customerId, doorAccessPlanId);
        }

        /// <summary>
        /// Removes door access plan from customer
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="doorAccessPlanId">Door access plan Id</param>
        public override void CustomerDoorAccessPlanDelete(int customerId, int? doorAccessPlanId = null)
        {
            Resource?.CustomerDoorAccessPlanDelete(customerId, doorAccessPlanId);
        }

        /// <summary>
        /// Get customer stored value accounts
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="svAccountNumber">Stored value account number</param>
        /// <returns>Customer stored value accounts</returns>
        public override List<CustomerSvAccount> CustomerStoredValueAccountsGet(int customerId, Int64? svAccountNumber = null)
        {
            return Resource?.CustomerStoredValueAccountsGet(customerId, svAccountNumber);
        }

        /// <summary>
        /// Creates new customer stored value account
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="svAccount">SV account</param>
        /// <returns>Customer stored value account number</returns>
        public override Int64 CustomerStoredValueAccountCreate(int customerId, PostCustomerSvAccount svAccount)
        {
            return Resource?.CustomerStoredValueAccountCreate(customerId, svAccount) ?? -1;
        }

        /// <summary>
        /// Updates existing customer stored value account
        /// </summary>
        /// <param name="svAccount">SV account</param>
        public override void CustomerStoredValueAccountUpdate(CustomerSvAccount svAccount)
        {
            Resource?.CustomerStoredValueAccountUpdate(svAccount);
        }

        /// <summary>
        /// Deletes customer stored value account
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="svAccountTypeId">Account Type Id</param>
        public override void CustomerStoredValueAccountDelete(int customerId, int svAccountTypeId)
        {
            Resource?.CustomerStoredValueAccountDelete(customerId, svAccountTypeId);
        }

        /// <inheritdoc />
        public override List<int> CustomerPosDisplayRulesGet(int customerId)
        {
            return Resource?.CustomerPosDisplayRulesGet(customerId);
        }

        /// <inheritdoc />
        public override CustomerPosMessage CustomerPosMessageGet(int customerId)
        {
            return Resource?.CustomerPosMessageGet(customerId);
        }

        /// <inheritdoc />
        public override CustomerPlans CustomerPlansGet(int customerId)
        {
            return Resource?.CustomerPlansGet(customerId);
        }

        /// <inheritdoc />
        public override List<TsCustomerBoardPlan> CustomerBoardPlansGet(int customerId)
        {
            return Resource?.CustomerBoardPlansGet(customerId);
        }

        /// <inheritdoc />
        public override List<int> CustomerBoardPlanIdsGet(int customerId)
        {
            return Resource?.CustomerBoardPlanIdsGet(customerId);
        }

        /// <inheritdoc />
        public override void CustomerBoardPlansSet(Guid customerGuid, CustomerBoardPlansPostRequest request)
        {
            Resource?.CustomerBoardPlansSet(customerGuid, request);
        }

        /// <inheritdoc />
        public override void CustomerBoardExclusionsSet(int customerId, List<int> transactionNumbers, bool exclusionEnabled)
        {
            Resource?.CustomerBoardExclusionsSet(customerId, transactionNumbers, exclusionEnabled);
        }

        /// <inheritdoc />
        public override void CustomerAccessPlansSet(Guid customerGuid, List<int> daPlanIds)
        {
            Resource?.CustomerAccessPlansSet(customerGuid, daPlanIds);
        }

        /// <inheritdoc />
        public override List<TsCustomerTransaction> CustomerTransactionHistoryGet(int customerId, DateTime? startDate, DateTime? endDate)
        {
            return Resource?.CustomerTransactionHistoryGet(customerId, startDate, endDate);
        }

        /// <inheritdoc />
        public override CustomerDoorAccess CustomerDoorAccessGet(int customerId, int doorId, DateTime? date)
        {
            return Resource?.CustomerDoorAccessGet(customerId, doorId, date);
        }

        /// <inheritdoc />
        public override void CustomerDoorOverridesSet(Guid customerGuid, DoorOverridesSetRequest request)
        {
            Resource?.CustomerDoorOverridesSet(customerGuid, request);
        }

        #endregion
    }
}
