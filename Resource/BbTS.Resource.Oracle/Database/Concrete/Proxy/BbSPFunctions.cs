﻿using BbTS.Core.Logging.Tracing;
using BbTS.Domain.Models.Institution;
using BbTS.Domain.Models.System;
using System;
using System.Collections.Generic;
using BbTS.Domain.Models.BbSp;
using BbTS.Domain.Models.System.Security.Oauth;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    [Trace(AttributePriority = 2)]
    public partial class ProxySource
    {
        /// <summary>
        /// Query the data layer for the first consumer key and secret.
        /// </summary>
        /// <returns>First result of the consumer key and secret.</returns>
        public override OAuthParameters ConsumerKeyAndSecretGetFirstValue()
        {
            return Resource?.ConsumerKeyAndSecretGetFirstValue();
        }

        /// <summary>
        /// Query the data layer for all of the consumer key and secret information.
        /// </summary>
        /// <returns>Consumer key and secret information.</returns>
        public override List<BbSpApplicationCredential> ConsumerKeyAndSecretGetAllValues()
        {
            return Resource?.ConsumerKeyAndSecretGetAllValues();
        }

        /// <summary>
        /// Query the data layer for all of the merchant information.
        /// </summary>
        /// <returns>List of merchants.</returns>
        public override List<BbSpMerchant> BbSpMerchantsGetAll()
        {
            return Resource?.BbSpMerchantsGetAll();
        }

        /// <summary>
        /// Retrieve the transaction system associated with the data layer
        /// </summary>
        /// <returns><see cref="TransactionSystem"/> object</returns>
        public override TransactionSystem TransactionSystemGet()
        {
            return Resource?.TransactionSystemGet();
        }

        /// <summary>
        /// Add a transaction system to this database resource instance.
        /// </summary>
        /// <param name="transactionSystem"><see cref="TransactionSystem"/> object to add.</param>
        public override void TransactionSystemAdd(TransactionSystem transactionSystem)
        {
            Resource?.TransactionSystemAdd(transactionSystem);
        }

        /// <summary>
        /// Delete all transaciton systems in this database resource instance.
        /// </summary>
        public override void TransactionSystemsDelete()
        {
            Resource?.TransactionSystemsDelete();
        }

        /// <summary>
        /// Delete all application credentials in this database resource instance.
        /// </summary>
        public override void ApplicationCredentialsDelete()
        {
            Resource?.ApplicationCredentialsDelete();
        }

        /// <summary>
        /// RefreshApplicationCredentialData : Deletes all Application Credential data from database and insert application credential data received from Service Agent.
        /// </summary>
        /// <param name="applicationCredentials">The application credential data entities.</param>
        public override void RefreshApplicationCredentialData(IEnumerable<SpApplicationCredential> applicationCredentials)
        {
            Resource?.RefreshApplicationCredentialData(applicationCredentials);
        }

        /// <summary>
        /// Gets application credential by consumer key.
        /// </summary>
        /// <param name="consumerKey">The consumer key on which to base the query for application credential</param>
        /// <param name="institutionId">The institution identifier.</param>
        /// <returns>The application credential by consumer key.</returns>
        public override SpApplicationCredential ApplicationCredentialGet(Guid consumerKey, Guid? institutionId = null)
        {
            return Resource?.ApplicationCredentialGet(consumerKey, institutionId);
        }

        /// <summary>
        /// Delete all institution routes in this database resource instance.
        /// </summary>
        public override void InstitutionRoutesDelete()
        {
            Resource?.InstitutionRoutesDelete();
        }

        /// <summary>
        /// RefreshInstitutionRouteData : Deletes all institution route data from database and insert institution route data received from Service Agent.
        /// </summary>
        /// <param name="institutionRoutes">The institution route data entities.</param>
        public override void RefreshInstitutionRouteData(IEnumerable<SpInstitutionRouteEntity> institutionRoutes)
        {
            Resource?.RefreshInstitutionRouteData(institutionRoutes);
        }

        /// <summary>
        /// Gets the institution Route by Route which contains Institution details.
        /// </summary>
        /// <param name="schemeTypeApiKey">The institution Route Scheme Type.</param>
        /// <param name="value">The institution Route Value.</param>
        /// <param name="institutionId">The institution identifier.</param>
        /// <returns>The institution Route which contains Institution details.</returns>
        public override SpInstitutionRouteEntity InstitutionRouteGet(string schemeTypeApiKey, string value, Guid? institutionId = null)
        {
            return Resource?.InstitutionRouteGet(schemeTypeApiKey, value, institutionId);
        }

        /// <summary>
        /// Delete all merchants in this database resource instance.
        /// </summary>
        public override void MerchantsDelete()
        {
            Resource?.MerchantsDelete();
        }

        /// <summary>
        /// RefreshMerchantData : Deletes all data from database and insert merchant data received from Service Agent.
        /// </summary>
        /// <param name="merchants">The merchant data entities.</param>
        public override void RefreshMerchantData(IEnumerable<SpMerchantEntity> merchants)
        {
            Resource?.RefreshMerchantData(merchants);
        }

        /// <summary>
        /// Gets the Merchant by Id.
        /// </summary>
        /// <param name="merchantId">The Merchant id.</param>
        /// <param name="institutionId">The institution identifier.</param>
        /// <returns>The Merchant</returns>
        public override SpMerchantEntity MerchantGet(Guid merchantId, Guid? institutionId = null)
        {
            return Resource?.MerchantGet(merchantId, institutionId);
        }
    }
}