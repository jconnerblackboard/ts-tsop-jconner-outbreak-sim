﻿using System;
using System.Collections.Generic;
using BbTS.Domain.Models.Event;
using BbTS.Domain.Models.Transaction.Processing;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        /// <summary>
        /// Process an attendance transaction.
        /// </summary>
        /// <param name="request">Request input parameters.</param>
        /// <returns>Response data for completing the transaction.</returns>
        public override AttendanceTransactionProcessResponse AttendanceTransactionProcess(AttendanceTransactionProcessRequest request)
        {
            return Resource?.AttendanceTransactionProcess(request);
        }

        /// <summary>
        /// Get a list of events for a device.
        /// </summary>
        /// <param name="deviceId">the unique identifier for the device.</param>
        /// <returns>List of <see cref="EventDeviceSetting"/></returns>
        public override List<EventDeviceSetting> EventDeviceSettingsGet(string deviceId)
        {
            return Resource?.EventDeviceSettingsGet(deviceId);
        }

        /// <summary>
        /// Get all event activities
        /// </summary>
        /// <returns>Event activities</returns>
        public override List<TsEventActivity> EventActivitiesGetAll()
        {
            return Resource?.EventActivitiesGetAll();
        }

        /// <summary>
        /// Get list of event plans
        /// </summary>
        /// <param name="eventPlanId">Id filter</param>
        /// <returns>Event plans</returns>
        public override List<TsEventPlan> EventPlanGet(int? eventPlanId = null)
        {
            return Resource?.EventPlanGet(eventPlanId);
        }

        /// <inheritdoc />
        public override EventAttendanceGetResponse EventAttendanceGet(int eventId)
        {
            return Resource?.EventAttendanceGet(eventId);
        }

        /// <inheritdoc />
        public override CustomerEventsGetResponse CustomerEventsGet(int customerId)
        {
            return Resource?.CustomerEventsGet(customerId);
        }

        /// <inheritdoc />
        public override void CustomerEventsSet(Guid customerGuid, CustomerEventsPostRequest request)
        {
            Resource?.CustomerEventsSet(customerGuid, request);
        }

        /// <inheritdoc />
        public override List<TsCustomerEvent> EventsForCustomerGetByEventPlan(int eventPlanId)
        {
            return Resource?.EventsForCustomerGetByEventPlan(eventPlanId);
        }

        /// <inheritdoc />
        public override EventListGetResponse EventListGet(EventListGetRequest request)
        {
            return Resource?.EventListGet(request);
        }

        /// <inheritdoc />
        public override List<TsEventInfo> EventsByEventGroup(int eventGroupId)
        {
            return Resource?.EventsByEventGroup(eventGroupId);
        }
    }
}
