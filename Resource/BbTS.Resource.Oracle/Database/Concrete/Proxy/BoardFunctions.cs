﻿using System;
using System.Collections.Generic;
using BbTS.Domain.Models.BoardPlan;
using BbTS.Domain.Models.Customer.Management;
using BbTS.Domain.Models.System.Database;
using BbTS.Domain.Models.Transaction;
using BbTS.Domain.Models.Transaction.Processing;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        /// <summary>
        /// Get a list of all board cash equivalency periods for the device.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>List of all board cash equivalency periods for the device.</returns>
        public override BoardCashEquivalencyPeriodsGetResponse BoardCashEquivalencyPeriodsGet(BoardCashEquivalencyPeriodsGetRequest request)
        {
            return Resource?.BoardCashEquivalencyPeriodsGet(request);
        }

        /// <summary>
        /// Get board information for a customer for use on a specified device.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Board information for a customer for use on a specified device.</returns>
        public override BoardInformationGetResponse BoardInformationGet(BoardInformationGetRequest request)
        {
            return Resource?.BoardInformationGet(request);
        }

        /// <summary>
        /// This method will return a list of records for the BoardMealTypes objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsBoardMealTypes> BoardMealTypesArchivesGet(ConnectionInfo connection)
        {
            return Resource?.BoardMealTypesArchivesGet(connection);
        }

        /// <summary>
        /// This method will return a list of records for the BoardMealTypes objects permitted for the given deviceId
        /// </summary>
        /// <param name="deviceId">The unique identifier for the device.</param>
        /// <returns></returns>
        public override List<TsBoardMealTypes> BoardMealTypesPosGet(string deviceId)
        {
            return Resource?.BoardMealTypesPosGet(deviceId);
        }

        /// <inheritdoc />
        public override List<MealTypeDeviceSetting> BoardMealTypesGet()
        {
            return Resource?.BoardMealTypesGet();
        }

        /// <summary>
        /// Get a list of board periods for a device.
        /// </summary>
        /// <param name="deviceId">The unique identifier for the device.</param>
        /// <returns>list of <see cref="BoardPeriod"/></returns>
        public override List<BoardPeriod> BoardPeriodsDeviceSettingsGet(string deviceId)
        {
            return Resource?.BoardPeriodsDeviceSettingsGet(deviceId);
        }

        /// <summary>
        /// Get the default board plan id for the device given its unique identifier.
        /// </summary>
        /// <param name="originatorGuid">The unique identifier of the originator.</param>
        /// <returns>The default board plan id for the device.</returns>
        public override int? DefaultBoardPlanIdFromOriginatorGuid(string originatorGuid)
        {
            return Resource.DefaultBoardPlanIdFromOriginatorGuid(originatorGuid);
        }

        /// <summary>
        /// Process a board transaction and provide the result.
        /// </summary>
        /// <param name="request">Board transaction to process</param>
        /// <returns>Result of the board transaction with remaining counts.</returns>
        public override BoardTransactionProcessResponse BoardTransactionProcess(BoardTransactionProcessRequest request)
        {
            return Resource?.BoardTransactionProcess(request);
        }

        /// <summary>
        /// Get list of board plan summaries
        /// </summary>
        /// <returns>Board plan summaries</returns>
        public override List<TsBoardPlanSummary> BoardPlanSummaryGetAll()
        {
            return Resource?.BoardPlanSummaryGetAll();
        }


        /// <inheritdoc />
        public override List<BoardPlanExclusion> PeriodExclusionListGet(int customerId)
        {
            return Resource?.PeriodExclusionListGet(customerId);
        }

        /// <summary>
        /// Get a board plan by numerical identifier.
        /// </summary>
        /// <param name="id">numerical identifier.</param>
        /// <returns>Board plan or null if not found.</returns>
        public override BoardPlanVerifyResponse BoardPlanVerifyById(int id)
        {
            return Resource?.BoardPlanVerifyById(id);
        }

        /// <inheritdoc />
        public override List<CustomerTransactionLocationLog> CustomerBoardLocationLogsGet(DateTime startDate, DateTime endDate)
        {
            return Resource?.CustomerBoardLocationLogsGet(startDate, endDate);
        }
    }
}
