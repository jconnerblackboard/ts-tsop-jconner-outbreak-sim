﻿using System.Collections.Generic;
using BbTS.Domain.Models.PaymentExpress;
using BbTS.Domain.Models.Terminal;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        /// <inheritdoc />
        public override List<Pos> PosBbPaygateConfiguredGet()
        {
            return Resource?.PosBbPaygateConfiguredGet();
        }

        /// <inheritdoc />
        public override List<PaymentExpressRequestResponseView> EmvReconciliationTransactionGet(string userName, string id)
        {
            return Resource?.EmvReconciliationTransactionGet(userName, id);
        }

        /// <inheritdoc />
        public override void EmvReconciliationTransactionSet(PaymentExpressRequestResponseView value)
        {
            Resource?.EmvReconciliationTransactionSet(value);
        }

        /// <inheritdoc />
        public override EmvSettings EmvSettingsGet(int terminalId)
        {
            return Resource?.EmvSettingsGet(terminalId);
        }

        /// <inheritdoc />
        public override void EmvTxnPurRequestSet(int terminalId, BbTxnPurRequestLog request)
        {
            Resource?.EmvTxnPurRequestSet(terminalId, request);
        }

        /// <inheritdoc />
        public override void EmvTxnPurResponseSet(int terminalId, BbTxnPurResponseLog response)
        {
            Resource?.EmvTxnPurResponseSet(terminalId, response);
        }

        /// <inheritdoc />
        public override void EmvTxnGet1ResponseSet(int terminalId, BbTxnGet1ResponseLog response)
        {
            Resource?.EmvTxnGet1ResponseSet(terminalId, response);
        }

        /// <inheritdoc />
        public override void EmvTxnVoidRequestSet(int terminalId, BbTxnVoidRequestLog request)
        {
            Resource?.EmvTxnVoidRequestSet(terminalId, request);
        }

        /// <inheritdoc />
        public override void EmvTxnVoidResponseSet(int terminalId, BbTxnVoidResponseLog response)
        {
            Resource?.EmvTxnVoidResponseSet(terminalId, response);
        }

        /// <inheritdoc />
        public override void EmvTxnRefRequestSet(int terminalId, BbTxnRefRequestLog request)
        {
            Resource?.EmvTxnRefRequestSet(terminalId, request);
        }

        /// <inheritdoc />
        public override void EmvTxnRefResponseSet(int terminalId, BbTxnRefResponseLog response)
        {
            Resource?.EmvTxnRefResponseSet(terminalId, response);
        }

        /// <inheritdoc />
        public override void EmvTxnSigRequestSet(int terminalId, BbTxnSigRequestLog request)
        {
            Resource?.EmvTxnSigRequestSet(terminalId, request);
        }

        /// <inheritdoc />
        public override void EmvTxnSigResponseSet(int terminalId, BbTxnSigResponseLog response)
        {
            Resource?.EmvTxnSigResponseSet(terminalId, response);
        }
    }
}
