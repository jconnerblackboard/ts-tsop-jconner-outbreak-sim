﻿using System;
using System.Collections.Generic;
using BbTS.Domain.Models.ArtsDataModel;
using BbTS.Domain.Models.Configuration;
using BbTS.Domain.Models.Terminal;
using BbTS.Domain.Models.Transaction.Processing;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        /// <summary>
        /// Set a payment express group account.
        /// </summary>
        /// <param name="name">String to use as the name.</param>
        /// <returns>List of group accounts.</returns>
        public override List<PaymentExpressGroupAccount> PaymentExpressGroupAccountGet(string name = null)
        {
            return Resource?.PaymentExpressGroupAccountGet(name);
        }

        /// <summary>
        /// Persist EMV TxnPur request messages to the data layer.
        /// </summary>
        /// <param name="requestId">Unique identifer associated with the service layer request.</param>
        /// <param name="request">The request containing the line item information for EMV.</param>
        public override LineItemProcessingResult EmvTxnPurRequestSet(string requestId, RetailTransactionLineTenderCreditCardRequest request)
        {
            return Resource?.EmvTxnPurRequestSet(requestId, request);
        }

        /// <summary>
        /// Persist EMV TxnPur response messages to the data layer
        /// </summary>
        /// <param name="requestId">Unique identifer associated with the service layer request.</param>
        /// <param name="request">The request containing the line item information for EMV.</param>
        public override LineItemProcessingResult EmvTxnPurResponseSet(string requestId, RetailTransactionLineTenderCreditCardRequest request)
        {
            return Resource?.EmvTxnPurResponseSet(requestId, request);
        }

        /// <summary>
        /// Persist EMV Get1 response messages to the data layer
        /// </summary>
        /// <param name="requestId">Unique identifer associated with the service layer request.</param>
        /// <param name="request">The request containing the line item information for EMV.</param>
        public override LineItemProcessingResult EmvTxnGet1ResponseSet(string requestId, RetailTransactionLineTenderCreditCardRequest request)
        {
            return Resource?.EmvTxnGet1ResponseSet(requestId, request);
        }

        /// <inheritdoc/>
        public override LineItemProcessingResult EmvHitTransactionRecord(string requestId,RetailTransactionLineTenderCreditCardHitRequest request)
        {
            return Resource?.EmvHitTransactionRecord(requestId, request);
        }

        /// <summary>
        /// Retrieve EMV settings for HIT operations.
        /// </summary>
        /// <param name="PosGuid"></param>
        /// <returns>EmvHitSettings for POS.</returns>
        public override EmvHitSettings EmvGatewaySettingsHitGet(string PosGuid)
        {
            return Resource?.EmvGatewaySettingsHitGet(PosGuid);
        }
    }
}
