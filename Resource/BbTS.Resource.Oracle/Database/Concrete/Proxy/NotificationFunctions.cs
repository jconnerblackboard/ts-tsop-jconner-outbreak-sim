﻿using BbTS.Domain.Models.Notification;
using BbTS.Domain.Models.Transaction;
using Oracle.ManagedDataAccess.Client;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        /// <summary>
        /// Create a notification.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public override NotificationCreateResponse NotificationCreate(NotificationCreateRequest request)
        {
            return Resource?.NotificationCreate(request);
        }

        /// <summary>
        /// Get all notificaitons stored in the database layer.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public override NotificationGetResponse NotificationGet(NotificationGetRequest request)
        {
            return Resource?.NotificationGet(request);
        }

        /// <summary>
        /// Set the status for processing of a notification event.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public override NotificationStatusSetResponse NotificationStatusSet(NotificationStatusSetRequest request)
        {
            return Resource?.NotificationStatusSet(request);
        }

        /// <summary>
        /// Register to receive notification events.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public override NotificationEventRegisterResponse NotificationEventRegister(NotificationEventRegisterRequest request, OracleConnection connection)
        {
            return Resource?.NotificationEventRegister(request, connection);
        }

        /// <summary>
        /// Wait for a notification event callback.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public override NotificationEventCallbackResponse NotificationEventCallbackWait(NotificationEventCallbackRequest request, OracleConnection connection)
        {
            return Resource?.NotificationEventCallbackWait(request, connection);
        }

        /// <summary>
        /// Kick off a nightly reporting metrics gather and process request in the data layer.
        /// </summary>
        /// <param name="requestId">The unique identifier for the request.</param>
        /// <returns></returns>
        public override ProcessingResult ReportingMetricsDailyProcessRun(string requestId)
        {
            return Resource?.ReportingMetricsDailyProcessRun(requestId);
        }
    }
}
