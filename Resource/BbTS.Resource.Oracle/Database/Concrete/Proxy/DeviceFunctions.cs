﻿using System;
using System.Collections.Generic;
using BbTS.Domain.Models.Definitions.Device;
using BbTS.Domain.Models.Definitions.Licensing;
using BbTS.Domain.Models.Device;
using BbTS.Domain.Models.Report;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        /// <summary>
        /// Get a list of IP Readers from the V_IP_READER view.
        /// </summary>
        /// <returns>List of IP Readers.</returns>
        public override List<IpReaderViewObject> IpReadersGet(List<HardwareModel> hardwareModelFilter = null)
        {
            return Resource?.IpReadersGet(hardwareModelFilter);
        }

        /// <summary>
        /// Get all (or one) DSR Access Point from the database layer.
        /// </summary>
        /// <param name="id">The access point id or access point guid.</param>
        /// <returns>List of active DsrAccessPoints in the database layer.</returns>
        public override List<DsrAccessPoint> AccessPointsGet(string id)
        {
            return Resource?.AccessPointsGet(id);
        }

        /// <summary>
        /// Get all Allegion locks from the database layer.
        /// </summary>
        /// <param name="modelFilter"></param>
        /// <returns>List of active Allegion locks in the database layer.</returns>
        public override List<AllegionLock> AllegionLocksGet(List<LicenseServerDeviceSubType> modelFilter = null)
        {
            return Resource?.AllegionLocksGet();
        }

        /// <summary>
        /// Verify that a device has been registered.
        /// </summary>
        /// <param name="deviceGuid">The unique (guid) identifier for the device.</param>
        /// <returns>Registration status.</returns>
        public override bool DeviceIsRegistered(string deviceGuid)
        {
            return Resource.DeviceIsRegistered(deviceGuid);
        }

        /// <summary>
        /// Get (all or single) device registration information.
        /// </summary>
        /// <param name="deviceGuid">Identifier of the device (for single) or null (for all).</param>
        /// <returns>List of device registration information.</returns>
        public override List<DeviceProperties> DevicePropertiesGet(string deviceGuid = null)
        {
            return Resource?.DevicePropertiesGet(deviceGuid);
        }

        /// <summary>
        /// Gets device registration information
        /// </summary>
        /// <param name="deviceGuid">Identifier of the device (for single) or null (for all).</param>
        /// <returns>Device registration information</returns>
        public override DeviceInfo DeviceGet(string deviceGuid)
        {
            return Resource?.DeviceGet(deviceGuid);
        }

        /// <summary>
        /// Update a device registration.
        /// </summary>
        /// <param name="deviceGuid">The unique (guid) identifier for the device.</param>
        /// <param name="registration">The registration information to use for an update.</param>
        /// <returns>The updated device registration information.</returns>
        public override DeviceProperties DevicePropertiesUpdate(string deviceGuid, DeviceProperties registration)
        {
            return Resource?.DevicePropertiesUpdate(deviceGuid, registration);
        }

        /// <summary>
        /// Delete a device registration information.
        /// </summary>
        /// <param name="deviceGuid">Identifier of the device (for single) or null (for all).</param>
        public override void DevicePropertiesDelete(string deviceGuid)
        {
            Resource?.DevicePropertiesDelete(deviceGuid);
        }

        /// <summary>
        /// Log a heartbeat request from the device.
        /// </summary>
        /// <param name="request">Heartbeat information from the device.</param>
        public override void DeviceHeartbeatLog(DeviceHeartbeatRequest request)
        {
            Resource?.DeviceHeartbeatLog(request);
        }

        /// <summary>
        /// Get the operational requirements for a specific device.
        /// </summary>
        /// <param name="deviceId">The unique (guid) identifier for the device.</param>
        /// <returns>The operational requirements for the specific device.</returns>
        public override OperationalRequirements DeviceOperationalRequirementsGet(string deviceId)
        {
            return Resource?.DeviceOperationalRequirementsGet(deviceId);
        }

        /// <summary>
        /// Process and log a device login request.
        /// </summary>
        /// <param name="request">Device login information.</param>
        public override DeviceLoginResponse ProcessDeviceLogin(DeviceLoginRequest request)
        {
            return Resource?.ProcessDeviceLogin(request);
        }

        /// <summary>
        /// Process and log a device logout request.
        /// </summary>
        /// <param name="request">Device logout information.</param>
        public override DeviceLogoutResponse ProcessDeviceLogout(DeviceLogoutRequest request)
        {
            return Resource?.ProcessDeviceLogout(request);
        }

        /// <summary>
        /// Register a (slate) device.
        /// </summary>
        /// <param name="request">Device registration information.</param>
        /// <returns></returns>
        public override DeviceRegistrationPostResponse DeviceSet(DeviceRegistrationPostRequest request)
        {
            return Resource?.DeviceSet(request);
        }

        /// <summary>
        /// Register a device as a pos.
        /// </summary>
        /// <param name="deviceGuid">The unique identifier for the device.</param>
        /// <param name="posGuid">The unique identifier for the pos.</param>
        public override void DevicePosRegister(string deviceGuid, string posGuid)
        {
            Resource?.DevicePosRegister(deviceGuid, posGuid);
        }

        /// <inheritdoc />
        public override MealsCountReport DeviceMealsCountReportGet(Guid deviceGuid)
        {
            return Resource?.DeviceMealsCountReportGet(deviceGuid);
        }

        /// <inheritdoc />
        public override List<DeviceReader> EnhancedReportingListGet()
        {
            return Resource?.EnhancedReportingListGet();
        }

        /// <inheritdoc />
        public override EnhancedReportingDomainDataGetResponse EnhancedReportingDomainDataGet()
        {
            return Resource?.EnhancedReportingDomainDataGet();
        }
    }
}
