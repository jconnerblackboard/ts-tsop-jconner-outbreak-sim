﻿using System.Collections.Generic;
using BbTS.Domain.Models.Cashier;
using BbTS.Domain.Models.System.Database;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        /// <summary>
        /// This method will return a list of records for the Cashier objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsCashier> CashierArchivesGet(ConnectionInfo connection)
        {
            return Resource?.CashierArchivesGet(connection);
        }

        /// <summary>
        /// Get the cashier id value from the cashier Guid.
        /// </summary>
        /// <param name="cashierGuid">The unique identifier assigned to the cashier.</param>
        /// <returns>The cashier id as an int.</returns>
        public override int CashierIdFromGuidGet(string cashierGuid)
        {
            return Resource.CashierIdFromGuidGet(cashierGuid);
        }
    }
}
