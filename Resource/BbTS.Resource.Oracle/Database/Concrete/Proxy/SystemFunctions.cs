﻿using System;
using System.Collections.Generic;
using BbTS.Core.Logging.Tracing;
using BbTS.Domain.Models.Definitions;
using BbTS.Domain.Models.System;
using BbTS.Domain.Models.System.Database;
using BbTS.Domain.Models.System.Logging;
using BbTS.Domain.Models.System.Security.Oauth;
using BbTS.Resource.Database.Abstract;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    [Trace(AttributePriority = 2)]
    public partial class ProxySource
    {
        /// <summary>
        /// 
        /// </summary>
        public ResourceDatabase Resource { get; set; }

        /// <summary>
        /// This method will return a list of records for the DeniedMessage objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsDenied_Message> DeniedMessageArchivesGet(ConnectionInfo connection)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="packageId"></param>
        /// <param name="packageNumber"></param>
        /// <param name="packageName"></param>
        /// <param name="packageVersion"></param>
        /// <param name="databaseName"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="databaseSchema"></param>
        public override void InstallPackageSet(ConnectionInfo connection, string packageId, string packageNumber,string packageName, string packageVersion, string databaseName, DateTime startDate, DateTime endDate, string databaseSchema)
        {
            Resource?.InstallPackageSet(connection, packageId, packageNumber, packageName, packageVersion,databaseName, startDate, endDate, databaseSchema);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override ActionResultToken PingDatabase()
        {
            return Resource?.PingDatabase();
        }

        /// <summary>
        /// Fetch the System Features from the Proxied Resource.
        /// </summary>
        /// <returns><see cref="SystemFeatures"/> object with the system features.</returns>
        public override SystemFeatures SystemFeaturesGet()
        {
            return Resource?.SystemFeaturesGet();
        }

        /// <summary>
        /// Get the value of a domain data entry in an oracle database.
        /// </summary>
        /// <param name="domainName">Domain name of the value</param>
        /// <param name="domainId">Id of the domain value</param>
        /// <returns>value as a string</returns>
        public override string DomainDataValueGet(string domainName, int domainId)
        {
            return Resource?.DomainDataValueGet(domainName, domainId);
        }

        /// <summary>
        /// Get the value of a domain data entry in an oracle database.
        /// </summary>
        /// <param name="domainTypeId">Domain TypeId of the domain</param>
        /// <param name="domainId">Id of the domain value</param>
        /// <returns>value as a string</returns>
        public override string DomainDataValueGet(int domainTypeId, int domainId)
        {
            return Resource?.DomainDataValueGet(domainTypeId, domainId);
        }

        /// <summary>
        /// Get a list of domain values matching the requested domain.
        /// </summary>
        /// <returns>List of <see cref="DomainViewObject"/>s pertaining to the requested domain.</returns>
        public override List<DomainViewObject> DomainListGet(string domain)
        {
            return Resource?.DomainListGet(domain);
        }

        /// <summary>
        /// Write an activity log to the database resource layer.
        /// </summary>
        /// <param name="activityLogGuid">The unique identifier for the action.</param>
        /// <param name="origin">The entity making the log entry.</param>
        /// <param name="data">The data to log.</param>
        public override void ActivityLogSet<T>(string activityLogGuid, ActivityLogOriginEntity origin, T data)
        {
            Resource?.ActivityLogSet(activityLogGuid, origin, data);
        }

        /// <summary>
        /// Write an audit log entry in an oracle database.
        /// </summary>
        /// <param name="logItem"><see cref="AuditLogItem"/> object to log.</param>
        public override void AuditLogSet(AuditLogItem logItem)
        {
            Resource?.AuditLogSet(logItem);
        }

        /// <summary>
        /// Get a ControlParameter value from the ControlParameter ParamaterName.
        /// </summary>
        /// <param name="controlParameterName">Name of the parameter</param>
        /// <returns>Correspoding value as a <see cref="string"/></returns>
        public override string ControlParameterValueGet(string controlParameterName)
        {
            return Resource?.ControlParameterValueGet(controlParameterName);
        }

        /// <summary>
        /// Get a User object from a username from the data layer
        /// </summary>
        /// <param name="userName">user to fetch</param>
        /// <returns><see cref="User"/> that was retrieved.</returns>
        public override User UserByNameGet(string userName)
        {
            return Resource?.UserByNameGet(userName);
        }

        /// <summary>
        /// Set or modify user information in the data layer
        /// </summary>
        /// <param name="user">The user to modify.</param>
        public override void UserSet(User user)
        {
            Resource?.UserSet(user);
        }

        #region Common object gets

        public override List<TsHostNames> HostNamesGet(ConnectionInfo connection)
        {
            throw new NotImplementedException();
        }
        #endregion

        /// <summary>
        /// Gets all control parameter groups
        /// </summary>
        /// <returns>A list of all control parameter groups</returns>
        public override List<string> GroupParameterListGet()
        {
            return Resource?.GroupParameterListGet();
        }

        /// <summary>
        /// Gets control parameters by the name of group
        /// </summary>
        /// <param name="groupParameterName">Control parameter group name</param>
        /// <returns>A list of control parameters that belongs to specified group</returns>
        public override List<ControlParameter> ParameterListGet(string groupParameterName)
        {
            return Resource?.ParameterListGet(groupParameterName);
        }

        /// <summary>
        /// Sets the control parameter in the resource layer
        /// </summary>
        /// <param name="request">The request containing the control parameter to set</param>
        /// <returns>Control parameter that has been set</returns>
        public override ControlParameter ParameterSet(ControlParameterPatchRequest request)
        {
            return Resource?.ParameterSet(request);
        }

        /// <summary>
        /// Gets the control paramater by name
        /// </summary>
        /// <param name="parameterName">Name of the control parameter</param>
        /// <returns>Control parameter that matches the name</returns>
        public override ControlParameter ParameterGet(string parameterName)
        {
            return Resource?.ParameterGet(parameterName);
        }

        /// <summary>
        /// Delete a control parameter by name.
        /// </summary>
        /// <param name="parameterName">Name of the control parameter</param>
        public override void ParameterDelete(string parameterName)
        {
            Resource?.ParameterDelete(parameterName);
        }
    }
}