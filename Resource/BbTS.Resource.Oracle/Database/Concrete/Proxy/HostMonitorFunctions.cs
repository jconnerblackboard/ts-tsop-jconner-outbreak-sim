﻿using System.Collections.Generic;
using BbTS.Domain.Models.Messaging.HostMonitor;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        /// <summary>
        /// Write a host monitor message to the database.
        /// </summary>
        /// <param name="message">Message to write.</param>
        public override void HostMonitorMessageWrite(HostMonitorMessage message)
        {
            Resource?.HostMonitorMessageWrite(message);
        }

        /// <summary>
        /// Get all host monitor messages from the database.
        /// </summary>
        /// <returns>List of host monitor messages.</returns>
        public override List<HostMonitorMessage> HostMonitorMessagesGetAll()
        {
            return Resource?.HostMonitorMessagesGetAll();
        }
    }
}
