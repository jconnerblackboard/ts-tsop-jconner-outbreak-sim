﻿using System;
using System.Collections.Generic;
using BbTS.Domain.Models.Customer;
using BbTS.Domain.Models.Policy;
using BbTS.Domain.Models.System.Database;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        #region Policy

        /// <summary>
        /// Loads the system root policy for BbTS.
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        public override PolicySet SystemRootPolicyLoad(ConnectionInfo connection = null)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets a list of stored value tender IDs allowed for the specified originator.
        /// </summary>
        /// <param name="originatorGuid"></param>
        /// <param name="tenderIds"></param>
        /// <param name="connection"></param>
        public override void ProcessPolicyTendersGet(Guid originatorGuid, out List<int> tenderIds, ConnectionInfo connection = null)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Process policy for a customer using the stored proc "Validate.ValidateSystemPolicy"
        /// </summary>
        /// <param name="cardNum"></param>
        /// <param name="issueNumber"></param>
        /// <param name="issueNumberCaptured"></param>
        /// <param name="posId"></param>
        /// <param name="tenderId"></param>
        /// <param name="evaluationDateTime"></param>
        /// <param name="custId"></param>
        /// <param name="availableBalance"></param>
        /// <param name="taxExempt"></param>
        /// <param name="discountSurcharges"></param>
        /// <param name="errorCode"></param>
        /// <param name="deniedText"></param>
        /// <param name="connection"></param>
        public override void ValidateSystemRootPolicy(
            string cardNum,
            string issueNumber,
            bool issueNumberCaptured,
            int posId,
            int tenderId,
            DateTimeOffset evaluationDateTime,
            out int custId,
            out decimal availableBalance,
            out bool taxExempt,
            out List<DiscountSurchargeRule> discountSurcharges,
            out int errorCode,
            out string deniedText,
            ConnectionInfo connection = null)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
