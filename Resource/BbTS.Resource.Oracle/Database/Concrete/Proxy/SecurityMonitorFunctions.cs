﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Messaging.SecurityMonitor;
using Oracle.ManagedDataAccess.Client;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        /// <summary>
        /// Register a list of doors for a DBMS_ALERT event.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        public override RegisterSignalListResponse RegisterSignalList(RegisterSignalListRequest request)
        {
            return Resource?.RegisterSignalList(request);
        }

        /// <summary>
        /// Wait for a DBMS_ALERT door access monitor event.
        /// </summary>
        /// <returns>Wait response.</returns>
        public override DbmsCallbackResponse WaitForDaMonitorEvent()
        {
            return Resource?.WaitForDaMonitorEvent();
        }
    }
}
