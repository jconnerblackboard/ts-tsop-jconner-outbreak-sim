﻿using System;
using System.Collections.Generic;
using BbTS.Domain.Models.Security.DoorApiKeys;
using BbTS.Domain.Models.Security.Doors;
using BbTS.Domain.Models.System;
using BbTS.Domain.Models.System.Security;
using BbTS.Domain.Models.System.Security.Oauth;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        /// <summary>
        /// Check with the database if it is ok to bypass a password check in validate
        /// </summary>
        /// <returns></returns>
        public override bool BypassUserPasswordCheck()
        {
            return Resource.BypassUserPasswordCheck();
        }

        /// <summary>
        /// Get a user's password history from Oracle
        /// </summary>
        /// <returns></returns>
        public override List<PasswordHistoryItem> PasswordHistoryGet(string username)
        {
            return Resource?.PasswordHistoryGet(username);
        }

        /// <summary>
        /// Add a user password entry into an oracle database
        /// </summary>
        /// <param name="username">username attached to the history entry</param>
        /// <param name="dateTime">password expiration date time</param>
        /// <param name="hash">hashed password as a string of interpreted bytes (ie. string representation of bytes).  See BbTS.Security.CommonFunctions.cs:ByteArrayToString.)</param>
        /// <param name="salt">string representation of the salt</param>
        /// <param name="hashType">hash type 1=Sha1 (should not be used), 2=PBK, 3=DHE</param>
        /// <param name="iterationCount">iteration count for hashing function</param>
        public override void PasswordHistorySet(string username, DateTime dateTime, byte[] hash, byte[] salt, int hashType, int iterationCount)
        {
            Resource?.PasswordHistorySet(username, dateTime, hash, salt, hashType, iterationCount);
        }

        /// <summary>
        /// Set a user password in an oracle database
        /// </summary>
        /// <param name="username">username attached to the history entry</param>
        /// <param name="dateTime">password expiration date time</param>
        /// <param name="hash">hashed password as a string of interpreted bytes (ie. string representation of bytes).  See BbTS.Security.CommonFunctions.cs:ByteArrayToString.)</param>
        /// <param name="salt">string representation of the salt</param>
        /// <param name="hashType">hash type 1=Sha1 (should not be used), 2=PBK, 3=DHE</param>
        /// <param name="iterationCount">iteration count for hashing function</param>
        public override void PasswordSet(string username, DateTime dateTime, byte[] hash, byte[] salt, int hashType, int iterationCount)
        {
            Resource?.PasswordSet(username, dateTime, hash, salt, hashType, iterationCount);
        }

        /// <summary>
        /// Process a failed user login against the Transact system
        /// </summary>
        /// <param name="userName">User name</param>
        /// <param name="isSuperUser">Has elevated privileges</param>
        /// <param name="isAdmin">Is an admin.</param>
        /// <param name="program">Calling application.</param>
        /// <param name="workstationId">Id of the workstation.</param>
        public override void ProcessFailedLogin(string userName, bool isSuperUser, bool isAdmin, string program, int workstationId)
        {
            Resource?.ProcessSuccessfulLogin(userName, isSuperUser, isAdmin, program, workstationId);
        }

        /// <summary>
        /// Process a successful user login against the Transact system.
        /// </summary>
        /// <param name="userName">User name</param>
        /// <param name="isSuperUser">Has elevated privileges</param>
        /// <param name="isAdmin">Is an admin.</param>
        /// <param name="program">Calling application.</param>
        /// <param name="workstationId">Id of the workstation.</param>
        public override void ProcessSuccessfulLogin(string userName, bool isSuperUser, bool isAdmin, string program, int workstationId)
        {
            Resource?.ProcessSuccessfulLogin(userName, isSuperUser, isAdmin, program, workstationId);
        }

        /// <summary>
        /// Resource layer function for setting the session token.
        /// </summary>
        /// <param name="token">The token to set.</param>
        public override void SessionTokenSet(SessionToken token)
        {
            Resource?.SessionTokenSet(token);
        }

        /// <summary>
        /// Fetch the user login session token from the provided token id
        /// </summary>
        /// <param name="tokenId">Id of the token</param>
        /// <returns>SessionToken object matching the id</returns>
        public override SessionToken SessionTokenGet(string tokenId)
        {
            return Resource?.SessionTokenGet(tokenId);
        }

        /// <summary>
        /// Get the user accounts rules from an oracle database
        /// </summary>
        /// <returns>user accounts rules as stored in an oracle database</returns>
        public override UserAccountRules UserAccountRulesGet()
        {
            return Resource?.UserAccountRulesGet();
        }

        /// <summary>
        /// Set user account rules
        /// </summary>
        /// <param name="rules">UserAccountRules object</param>
        public override void UserAccountRulesSet(UserAccountRules rules)
        {
            Resource?.UserAccountRulesSet(rules);
        }

        /// <summary>
        /// Gets the application credential by consumer key from the transact api stores. 
        /// </summary>
        /// <param name="consumerKey">The consumer key.</param>
        /// <returns>The transact api application credential from the database resource layer.</returns>
        public override GetApplicationCredentialByConsumerKeyResponse GetTransactApiApplicationCredential(Guid consumerKey)
        {
            return Resource?.GetTransactApiApplicationCredential(consumerKey);
        }

        #region Doors

        /// <inheritdoc />
        public override List<DoorEventLog> DoorStatesGet(int? doorId = null)
        {
            return Resource?.DoorStatesGet(doorId);
        }

        /// <inheritdoc />
        public override List<DoorAaEventLog> DoorAaStatesGet(int? doorId = null)
        {
            return Resource?.DoorAaStatesGet(doorId);
        }

        /// <inheritdoc />
        public override List<DoorAaConnection> DoorAaConnectionGet(List<int> doorIds)
        {
            return Resource?.DoorAaConnectionGet(doorIds);
        }

        /// <inheritdoc />
        public override List<DoorAlarmLog> DoorAlarmsGet(int? doorId, DateTime startDate, DateTime endDate)
        {
            return Resource?.DoorAlarmsGet(doorId, startDate, endDate);
        }

        /// <inheritdoc />
        public override List<DoorAlarmLog> ActiveAlarmsGet()
        {
            return Resource?.ActiveAlarmsGet();
        }

        /// <inheritdoc />
        public override List<DoorAlarmLog> ActiveStaleAlarmsGet()
        {
            return Resource?.ActiveStaleAlarmsGet();
        }

        /// <inheritdoc />
        public override List<UserDoor> DoorsByUserGet(int customerId, bool? accessPlanActive = null)
        {
            return Resource?.DoorsByUserGet(customerId, accessPlanActive);
        }

        /// <inheritdoc />
        public override List<UserDoorAa> DoorsAaByUserGet(int customerId, bool? accessPlanActive = null)
        {
            return Resource?.DoorsAaByUserGet(customerId, accessPlanActive);
        }

        /// <inheritdoc />
        public override List<DoorUser> UsersByDoorGet(int doorId, bool? customerActive = null, bool? accessPlanActive = null)
        {
            return Resource?.UsersByDoorGet(doorId, customerActive, accessPlanActive);
        }

        /// <inheritdoc />
        public override List<DoorAaUser> UsersByDoorAaGet(int doorId, bool? customerActive = null, bool? accessPlanActive = null)
        {
            return Resource?.UsersByDoorAaGet(doorId, customerActive, accessPlanActive);
        }

        /// <inheritdoc />
        public override List<DoorTransaction> DoorTransactionsGet(int? doorId, int? customerId, DateTime startDate, DateTime endDate)
        {
            return Resource?.DoorTransactionsGet(doorId, customerId, startDate, endDate);
        }

        /// <inheritdoc />
        public override List<DoorTransaction> DoorTransactionsGet(int buildingId, DateTime startDate, DateTime endDate)
        {
            return Resource?.DoorTransactionsGet(buildingId, startDate, endDate);
        }

        /// <inheritdoc />
        public override List<DoorAaTransaction> DoorAaTransactionsGet(int? doorId, int? customerId, int? buildingId, DateTime startDate, DateTime endDate)
        {
            return Resource?.DoorAaTransactionsGet(doorId, customerId, buildingId, startDate, endDate);
        }

        /// <inheritdoc />
        public override List<DoorActivity> DoorHistoryGet(int doorId, DateTime startDate, DateTime endDate)
        {
            return Resource?.DoorHistoryGet(doorId, startDate, endDate);
        }

        /// <inheritdoc />
        public override void MasterControllerSet(int? masterControllerId, MasterController masterController)
        {
            Resource?.MasterControllerSet(masterControllerId, masterController);
        }

        /// <inheritdoc />
        public override void DoorSet(int? doorId, Door door)
        {
            Resource?.DoorSet(doorId, door);
        }

        /// <inheritdoc />
        public override void DoorAaSet(int? doorId, DoorAa door)
        {
            Resource?.DoorAaSet(doorId, door);
        }

        /// <inheritdoc />
        public override List<DoorAccessPoint> DoorsGet()
        {
            return Resource?.DoorsGet();
        }

        /// <inheritdoc />
        public override void DoorAlarmAcknowledgeSet(Int64 alarmLogId, DoorAcknowledgeAlarm ackInfo)
        {
            Resource?.DoorAlarmAcknowledgeSet(alarmLogId, ackInfo);
        }

        /// <inheritdoc />
        public override void DoorAlarmAcknowledge(int? doorId, DoorAcknowledgeAlarmInfo ackInfo)
        {
            Resource?.DoorAlarmAcknowledge(doorId, ackInfo);
        }

        /// <inheritdoc />
        public override DoorListGetResponse DoorListGet(DoorListGetRequest request)
        {
            return Resource?.DoorListGet(request);
        }

        /// <inheritdoc />
        public override List<DoorInfo> DoorInfoGet(DoorInfoGetRequest request)
        {
            return Resource?.DoorInfoGet(request);
        }

        /// <inheritdoc />
        public override DoorApiKeyGetResponse DoorApiKeyGet()
        {
            return Resource?.DoorApiKeyGet();
        }

        /// <inheritdoc />
        public override void DoorApiKeySet(DoorApiKeySetRequest request)
        {
            Resource?.DoorApiKeySet(request);
        }

        #endregion
    }
}
