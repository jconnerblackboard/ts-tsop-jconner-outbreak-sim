﻿using System.Collections.Generic;
using BbTS.Domain.Models.System.Database;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        public override Indexes IndexesGet(ConnectionInfo connectionInfo, string ownerName, string indexName)
        {
            // New up a sample index
            var index = new Index { TableName = "Test", IndexType = IndexType.Unique, Tablespace = "TableSpace", Columns = new List<IndexColumn>() };
            var column = new IndexColumn { Name = "Test", Position = 1, Expression = "TRUNC(&quot;DATETIME&quot;", IsFunctionBased = true };
            index.Columns.Add(column);
            
            // Calculate the Md5 hash before adding the index name - This way the hash is just the "shape" of the index, irrespective of the name
            index.Md5Hash = Md5HashGet<Index>(index);

            // Add the name
            index.IndexName = "Test";

            // Create a list and add the index to the list
            var indexList = new Indexes { Items = new List<Index>() };
            indexList.Items.Add(index);

            // Return the data to the caller
            return indexList;
        }
    }
}