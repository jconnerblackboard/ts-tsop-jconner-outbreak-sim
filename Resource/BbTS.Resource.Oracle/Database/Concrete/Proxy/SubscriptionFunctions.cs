﻿using System;
using System.Collections.Generic;
using BbTS.Domain.Models.Definitions.Monitoring;
using BbTS.Domain.Models.Messaging.SecurityMonitor;
using BbTS.Domain.Models.Messaging.Subscription;
using BbTS.Domain.Models.Messaging.Subscription.Rest;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        /// <summary>
        /// Create an association between doors and a registered security monitor subscribing client.
        /// </summary>
        /// <param name="request">The request for association.</param>
        /// <returns>The result of the operation.</returns>
        public override DoorClientAssociationtResponse DoorClientAssociationCreate (DoorClientAssociationRequest request)
        {
            return Resource?.DoorClientAssociationCreate(request);
        }

        /// <summary>
        /// Remove an association between doors and a registered security monitor subscribing client.
        /// </summary>
        /// <param name="request">The request for disassociation.</param>
        /// <returns>The result of the operation.</returns>
        public override DoorClientAssociationtResponse DoorClientAssociationRemove(DoorClientAssociationRequest request)
        {
            return Resource?.DoorClientAssociationRemove(request);
        }

        /// <summary>
        /// Get a list of all subscription registrations in the data layer.
        /// </summary>
        /// <returns>List of all subscription registrations.</returns>
        public override List<SubscriptionRegistration> SubscriptionRegistrationGet(string clientGuid = null, SubscriptionServiceType subscriptionType = SubscriptionServiceType.None)
        {
            return Resource?.SubscriptionRegistrationGet(clientGuid, subscriptionType);
        }

        /// <summary>
        /// Register for a subscription service.
        /// </summary>
        /// <param name="request">The registration request.</param>
        /// <param name="username">Username</param>
        /// <returns>The results of the operation.</returns>
        public override SubscriptionRegistrationResponse SubscriptionServiceRegister(SubscriptionRegistrationRequest request, string username)
        {
            return Resource?.SubscriptionServiceRegister(request, username);
        }

        /// <summary>
        /// Unregister for a subscription service.
        /// </summary>
        /// <param name="request">The registration request.</param>
        /// <returns>The results of the operation.</returns>
        public override SubscriptionRegistrationResponse SubscriptionServiceUnregister(SubscriptionRegistrationRequest request)
        {
            return Resource?.SubscriptionServiceUnregister(request);
        }

        /// <summary>
        /// Update the last communicated date and time for a client and service.
        /// </summary>
        /// <param name="clientGuid">The unique identifier for the client.</param>
        /// <param name="type">The type of service.</param>
        /// <param name="lastCommunicated">The date and time the client was last communicated with.</param>
        public override void SubscriptionServiceUpdateLastCommunicated(string clientGuid, SubscriptionServiceType type, DateTimeOffset lastCommunicated)
        {
            Resource?.SubscriptionServiceUpdateLastCommunicated(clientGuid, type, lastCommunicated);
        }

        /// <summary>
        /// Toggle the setting in the data layer that indicates whether or not to send all stored messages on the next callback.
        /// </summary>
        /// <param name="clientGuid">The unique identifier for the client.</param>
        /// <param name="type">The type of service.</param>
        /// <param name="sendAllOnNextCallback">Send all or not.</param>
        public override void SubscriptionServiceToggleReceiveAll(string clientGuid, SubscriptionServiceType type, bool sendAllOnNextCallback)
        {
            Resource?.SubscriptionServiceToggleReceiveAll(clientGuid, type, sendAllOnNextCallback);
        }
    }
}
