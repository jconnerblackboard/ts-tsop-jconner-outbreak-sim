﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Credential;
using BbTS.Domain.Models.Operator;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {

        /// <summary>
        /// Validate operator card number credentials.
        /// </summary>
        /// <param name="request">Validation request.</param>
        /// <returns>Validation response.</returns>
        public override ValidateOperatorCardNumberResponse ValidateOperatorCardNumber(ValidateOperatorCardNumberRequest request)
        {
            return Resource?.ValidateOperatorCardNumber(request);
        }

        /// <summary>
        /// Change an operator pin.
        /// </summary>
        /// <param name="request">PIN change information.</param>
        /// <param name="cashierId">The numberical id of the cashier (required by the stored proc).</param>
        /// <returns>Pin change response</returns>
        public override OperatorChangePinResponse ChangeOperatorPin(OperatorChangePinRequest request, int cashierId)
        {
            return Resource?.ChangeOperatorPin(request, cashierId);
        }

        /// <summary>
        /// Process an operator session begin request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The result of the operation.</returns>
        public override OperatorSessionBeginEndProcessResponse OperatorSessionBeginProcess(OperatorSessionBeginEndProcessRequest request)
        {
            return Resource?.OperatorSessionBeginProcess(request);
        }

        /// <summary>
        /// Process an operator session end request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The result of the operation.</returns>
        public override OperatorSessionBeginEndProcessResponse OperatorSessionEndProcess(OperatorSessionBeginEndProcessRequest request)
        {
            return Resource?.OperatorSessionEndProcess(request);
        }

        /// <summary>
        /// Get an operator id (guid) from an e-mail address (assigned to the customer associated with the operator).
        /// </summary>
        /// <param name="email">Customer e-mail address.</param>
        /// <returns>Operator Id (Guid)</returns>
        public override string OperatorIdFromEmailAddress(string email)
        {
            return Resource?.OperatorIdFromEmailAddress(email);
        }

        /// <summary>
        /// Get an operator id (guid) from a customer card number (assigned to the customer associated with the operator).
        /// </summary>
        /// <param name="credential"></param>
        /// <returns>Operator Id (Guid)</returns>
        public override string OperatorIdFromCardNumber(BasicCardCredential credential)
        {
            return Resource?.OperatorIdFromCardNumber(credential);
        }

        /// <summary>
        /// Get the operator PIN.
        /// </summary>
        /// <param name="operatorId">The unique (Guid) identifier for the operator.</param>
        /// <returns>Operator's PIN or null if not found.</returns>
        public override string OperatorPinGet(string operatorId)
        {
            return Resource?.OperatorPinGet(operatorId);
        }

        /// <summary>
        /// Get the operator's email address.
        /// </summary>
        /// <param name="operatorId">The unique (Guid) identifier for the operator.</param>
        /// <returns>Operator's email address or null if not found.</returns>
        public override string OperatorEmailGet(string operatorId)
        {
            return Resource?.OperatorEmailGet(operatorId);
        }

        /// <summary>
        /// Get the operator's card number.
        /// </summary>
        /// <param name="operatorId">The unique (Guid) identifier for the operator.</param>
        /// <returns>Operator's card number or null if not found.</returns>
        public override string OperatorCardNumberGet(string operatorId)
        {
            return Resource?.OperatorCardNumberGet(operatorId);
        }

        /// <summary>
        /// Get an operator's basic credential information.
        /// </summary>
        /// <param name="operatorId">The unique (Guid) identifier for the operator.</param>
        /// <returns>Credential for the operator or null if not found.</returns>
        public override BasicCardCredential OperateCardNumberAndIssueNumberGet(string operatorId)
        {
            return Resource?.OperateCardNumberAndIssueNumberGet(operatorId);
        }

        /// <summary>
        /// Get all operators for a specific device.
        /// </summary>
        /// <param name="deviceId">The unique (Guid) identifier for the device.</param>
        /// <returns>List of <see cref="Operator"/>s on a device.</returns>
        public override List<Operator> OperatorsDeviceSettingsGet(string deviceId)
        {
            return Resource?.OperatorsDeviceSettingsGet(deviceId);
        }

        /// <summary>
        /// Get a list of all operator roles for a specific device.
        /// </summary>
        /// <param name="deviceId">The unique (Guid) identifier for the device.</param>
        /// <returns>List of operator roles.</returns>
        public override List<OperatorRole> OperatorRolesForDeviceGet(string deviceId)
        {
            return Resource?.OperatorRolesForDeviceGet(deviceId);
        }

        /// <summary>
        /// Get an operator by their unique (guid) identifier.
        /// </summary>
        /// <param name="operatorGuid">The unique (guid) identifier for the operator.</param>
        /// <returns>The operator or null if not found.</returns>
        public override Operator OperatorByGuidGet(string operatorGuid)
        {
            return Resource?.OperatorByGuidGet(operatorGuid);
        }

        /// <inheritdoc />
        public override List<TsOperator> OperatorsByMerchantGet(Guid merchantGuid)
        {
            return Resource?.OperatorsByMerchantGet(merchantGuid);
        }
    }
}
