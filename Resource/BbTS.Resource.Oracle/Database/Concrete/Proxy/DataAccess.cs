﻿using System;
using System.Collections.Generic;
using System.Data;
using BbTS.Domain.Models.System.Database;
using BbTS.Resource.Database.Abstract;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource : ResourceDatabase
    {
        public override void BulkLoadTable(DataTable dataTable, ConnectionInfo info)
        {
            Resource?.BulkLoadTable(dataTable, info);
        }

        public override DataTable DataTableGet(ConnectionInfo info, string sqlCommand, bool queryContainsLongData = false)
        {
            return Resource?.DataTableGet(info, sqlCommand, queryContainsLongData);
        }

        public override void ExecuteNonQuery(ConnectionInfo info, string sqlCommand)
        {
            Resource?.ExecuteNonQuery(info, sqlCommand);
        }

        public override object ExecuteScalar(ConnectionInfo info, string sqlCommand)
        {
            return Resource?.ExecuteScalar(info, sqlCommand);
        }

        public override List<object[]> ExecuteReaderToObjectList(string sqlCommand, ConnectionInfo info)
        {
            return Resource?.ExecuteReaderToObjectList(sqlCommand, info);
        }

        public override void ExecuteTransactionalNonQuery(ConnectionInfo info, string sqlCommand, bool commit)
        {
            throw new NotImplementedException();
        }

        public override bool ConnectionTest(ConnectionInfo info)
        {
            return Resource != null && ConnectionTest(info);
        }

        public override string ServerVersionGet(ConnectionInfo info)
        {
            return Resource?.ServerVersionGet(info);
        }

        public override bool DatabaseUserExists(ConnectionInfo info, string name)
        {
            return Resource != null && DatabaseUserExists(info, name);
        }

        public override bool TableExists(ConnectionInfo info, string name)
        {
            return Resource != null && TableExists(info, name);
        }
    }
}