﻿using System;
using System.Collections.Generic;
using BbTS.Domain.Models.Credential;
using BbTS.Domain.Models.Definitions.Container;
using BbTS.Domain.Models.System.Database;
using BbTS.Domain.Models.Transaction;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        /// <summary>
        /// Get the last transaction number for the specified originator.
        /// </summary>
        /// <param name="originatorId"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public override int? LastTransactionNumberGet(Guid originatorId, ConnectionInfo connection = null)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Insert a denied transaction into the database.
        /// </summary>
        /// <param name="errorCode"></param>
        /// <param name="deniedText"></param>
        /// <param name="requestGuid"></param>
        /// <param name="transactionClassification"></param>
        /// <param name="transactionGuid"></param>
        /// <param name="originatorGuid"></param>
        /// <param name="forcePost"></param>
        /// <param name="startTimeStamp"></param>
        /// <param name="transactionNumber"></param>
        /// <param name="customerCard"></param>
        /// <param name="swiped"></param>
        /// <param name="customerGuid"></param>
        /// <param name="operatorGuid"></param>
        /// <param name="tenderId"></param>
        /// <param name="deniedAmount"></param>
        /// <param name="attendedEventId"></param>
        /// <param name="attendedEventTransactionType"></param>
        /// <param name="boardMealTypeId"></param>
        /// <param name="connection"></param>
        public override void TransactionDenialSet(
            //Required
            int errorCode,
            string deniedText,
            Guid requestGuid,
            TransactionClassification transactionClassification,
            Guid transactionGuid,
            Guid originatorGuid,
            bool forcePost,
            DateTimeOffset startTimeStamp,
            //Optional
            int? transactionNumber,
            CardCapture customerCard,
            bool? swiped,
            Guid? customerGuid,
            Guid? operatorGuid,
            int? tenderId,
            decimal? deniedAmount,
            int? attendedEventId,
            AttendedEventTranType? attendedEventTransactionType,
            int? boardMealTypeId,
            ConnectionInfo connection = null)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public override List<CustomerTransactionLocationLog> CustomerTransactionLocationLogsGet(DateTime startDate, DateTime endDate)
        {
            return Resource?.CustomerTransactionLocationLogsGet(startDate, endDate);
        }
    }
}
