﻿using System.Collections.Generic;
using BbTS.Domain.Models.Product;
using BbTS.Domain.Models.Products;
using BbTS.Domain.Models.System.Database;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        /// <summary>
        /// This method will return a list of records for the DsProductDetail objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsDsProductDetail> DsProductDetailArchivesGet(ConnectionInfo connection)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the DsProductPromo objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsDsProductPromo> DsProductPromoArchivesGet(ConnectionInfo connection)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Get a list of products for a specific device.
        /// </summary>
        /// <param name="deviceId">The unique identifier for the device.</param>
        /// <returns>A list of <see cref="Product"/> for the device.</returns>
        public override List<Product> ProductDeviceSettingsGet(string deviceId)
        {
            return Resource?.ProductDeviceSettingsGet(deviceId);
        }
    }
}