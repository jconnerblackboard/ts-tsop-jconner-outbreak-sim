﻿using System;
using BbTS.Domain.Models.EventLog;
using System.Collections.Generic;
using BbTS.Domain.Models.System.Database;
using BbTS.Domain.Models.Transaction;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        /// <summary>
        /// Create event log
        /// </summary>
        /// <returns>Response to the request</returns>
        public override void EventLogSet(EventLogPostRequest request)
        {
            Resource?.EventLogSet(request);
        }

        /// <summary>
        /// Gets an event log
        /// </summary>
        /// <param name="eventLogId"></param>
        /// <returns>Event log</returns>
        public override TsEventLog EventLogGet(int eventLogId)
        {
            return Resource?.EventLogGet(eventLogId);
        }
        
        /// <inheritdoc />
        public override IEnumerable<EventLog> EventLogGet(ConnectionInfo connectionInfo, string source)
        {
            return Resource?.EventLogGet(connectionInfo, source);
        }
        
        /// <summary>
        /// Gets summary of event logs
        /// </summary>
        /// <param name="request">Filter</param>
        /// <returns>Filtered event logs</returns>
        public override List<TsEventLogSummary> EventLogGetSummary(EventLogGetSummaryRequest request)
        {
            return Resource?.EventLogGetSummary(request);
        }

        /// <summary>
        /// Gets lastest log by event task category
        /// </summary>
        /// <param name="eventId">Event id</param>
        /// <param name="taskCategory">Task category</param>
        /// <param name="providerName">Provider name</param>
        /// <param name="computerName">Computer name</param>
        /// <returns>Event log</returns>
        public override TsEventLog EventLogLatestByTaskGet(int eventId, short taskCategory, string providerName = null, string computerName = null)
        {
            return Resource?.EventLogLatestByTaskGet(eventId, taskCategory, providerName, computerName);
        }

        ///<inheritdoc />
        public override List<CustomerTransactionLocationLog> CustomerEventLocationLogsGet(DateTime startDate, DateTime endDate)
        {
            return Resource?.CustomerEventLocationLogsGet(startDate, endDate);
        }
    }
}
