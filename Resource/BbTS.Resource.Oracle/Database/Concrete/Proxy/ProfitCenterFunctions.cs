﻿using System;
using System.Collections.Generic;
using BbTS.Domain.Models.ProfitCenter;
using BbTS.Domain.Models.Report;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        /// <inheritdoc />
        public override int PeriodNumberFromDeviceGuidGet(string deviceGuid, DateTime timestamp)
        {
            return Resource.PeriodNumberFromDeviceGuidGet(deviceGuid, timestamp);
        }

        /// <inheritdoc />
        public override List<TsProfitCenter> ProfitCenterGet(int? id = null)
        {
            return Resource?.ProfitCenterGet(id);
        }

        /// <inheritdoc />
        public override List<TsProfitCenter> ProfitCenterForUserGet(string userId, int? id = null)
        {
            return Resource?.ProfitCenterForUserGet(userId, id);
        }

        /// <inheritdoc />
        public override MealsCountReport ProfitCenterMealsCountGet(Guid profitCenterGuid)
        {
            return Resource?.ProfitCenterMealsCountGet(profitCenterGuid);
        }
    }
}
