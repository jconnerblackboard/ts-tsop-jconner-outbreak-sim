﻿using System;
using System.Collections.Generic;
using BbTS.Domain.Models.System;
using BbTS.Domain.Models.System.Database;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        public override void UserPasswordHistoryDelete(ConnectionInfo connection, string databaseSchema, User user)
        {
            Resource?.UserPasswordHistoryDelete(connection, databaseSchema, user);
        }

        public override List<User> UsersGet(ConnectionInfo connection, string databaseSchema)
        {
            return Resource?.UsersGet(connection, databaseSchema);
        }
        public override void UserSet(ConnectionInfo connection, string databaseSchema, User user)
        {
            Resource?.UserSet(connection, databaseSchema, user);
        }
    }
}