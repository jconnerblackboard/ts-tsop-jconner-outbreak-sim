﻿using System;
using System.Collections.Generic;
using BbTS.Domain.Models.Pos;
using BbTS.Domain.Models.Pos.PosOptions;
using BbTS.Domain.Models.RetailTransaction;
using BbTS.Domain.Models.System.Database;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        #region  Pos
        /// <summary>
        /// This method will return a list of records for the Pos objects in the system.  If connection is null, ConnectionString will be used
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsPos> PosArchivesGet(ConnectionInfo connection = null)
        {
            return Resource?.PosArchivesGet(connection);
        }

        /// <inheritdoc />
        public override void PosDelete(int posId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get the profit center the device belongs to from the device guid.
        /// </summary>
        /// <param name="deviceGuid">The unique identifier for the device.</param>
        /// <returns>The id (int) of the profit center</returns>
        public override int ProfitCenterFromDeviceGuidGet(string deviceGuid)
        {
            return Resource.ProfitCenterFromDeviceGuidGet(deviceGuid);
        }

        /// <summary>
        /// Get pos properties for a specified guid (or null for all).
        /// </summary>
        /// <param name="posGuid">the unique identifier for the pos (null for all pos properties).</param>
        /// <returns>A list of pos properties.</returns>
        public override List<PosProperties> PosPropertiesGet(string posGuid = null)
        {
            return Resource?.PosPropertiesGet(posGuid);
        }

        /// <summary>
        /// Set pos properties for a specified guid (null to create).
        /// </summary>
        /// <param name="request"></param>
        public override PosProperties PosPropertiesCreate(PosPropertiesPostRequest request)
        {
            return Resource?.PosPropertiesCreate(request);
        }

        /// <summary>
        /// Get the pos guid from the originator guid.
        /// </summary>
        /// <param name="originatorGuid">The unique identifier of the originator.</param>
        /// <returns>The pos guid for the associated pos.</returns>
        public override Guid PosGuidFromOriginatorGuidGet(Guid originatorGuid)
        {
            return Resource.PosGuidFromOriginatorGuidGet(originatorGuid);
        }

        /// <summary>
        /// Get the pos id from the originator guid.
        /// </summary>
        /// <param name="deviceGuid">The unique identifier of the originator.</param>
        /// <returns>The numerical identifier for the associated pos.</returns>
        public override int PosIdFromOriginatorGuidGet(string deviceGuid)
        {
            return Resource.PosIdFromOriginatorGuidGet(deviceGuid);
        }

        /// <summary>
        /// Get the pos id from the originator guid.
        /// </summary>
        /// <param name="originatorGuid">The unique identifier of the originator.</param>
        /// <returns>The numerical identifier for the associated pos.  Null is returned if no pos could be found in the resource layer or no appropriate association has been made to a POS entity.</returns>
        public override int? PosIdFromOriginatorGuidGet(Guid originatorGuid)
        {
            return Resource.PosIdFromOriginatorGuidGet(originatorGuid);
        }

        /// <summary>
        /// Get the OriginatorTypeId from the originator guid.
        /// </summary>
        /// <param name="deviceGuid">The unique identifier of the originator.</param>
        /// <returns>The numerical OriginatorTypeId.</returns>
        public override int OriginatorTypeIdFromOriginatorGuidGet(string deviceGuid)
        {
            return Resource.OriginatorTypeIdFromOriginatorGuidGet(deviceGuid);
        }

        /// <inheritdoc />
        public override PosGuidGetResponse PosForWorkstationGetOrCreate(string workstationName, int profitCenterId)
        {
            return Resource?.PosForWorkstationGetOrCreate(workstationName, profitCenterId);
        }

        #endregion

        #region Pos Group

        /// <inheritdoc />
        public override List<TsPos_Group> PosGroupArchivesGet(ConnectionInfo connection = null)
        {
            throw new System.NotImplementedException();
        }

        /// <inheritdoc />
        public override List<TsPosGroup> PosGroupGet(int? id = null)
        {
            return Resource?.PosGroupGet(id);
        }

        #endregion

        #region Pos_Laundry_Machine
        /// <summary>
        /// This method will return a list of records for the PosLaundryMachine objects in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsPos_Laundry_Machine> PosLaundryMachineArchivesGet(ConnectionInfo connection)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// This method will return a list of records for the Stored Value Tender objects that are available at the specified POS.  If connection is null, ConnectionString will be used
        /// </summary>
        /// <param name="posId"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public override List<TsTender> StoredValueTendersForPos(int posId, ConnectionInfo connection = null)
        {
            throw new System.NotImplementedException();
        }

        #endregion

        #region Pos_Tt

        /// <inheritdoc />
        public override PosTt PosTtCreate(PosTtPostRequest request)
        {
            return Resource?.PosTtCreate(request);
        }

        /// <inheritdoc />
        public override List<PosTt> PosTtListGet(string posGuid = null)
        {
            return Resource?.PosTtListGet(posGuid);
        }


        /// <inheritdoc />
        public override List<PosTtSetup> PosTtSetupListGet(int? posTtSetupId = null)
        {
            return Resource?.PosTtSetupListGet(posTtSetupId);
        }

        #endregion

        #region Pos_Option

        /// <inheritdoc />
        public override List<PosOption> PosOptionGet(string id = null)
        {
            return Resource?.PosOptionGet(id);
        }

        /// <inheritdoc />
        public override PosOptionDeviceSettings PosOptionDeviceSettingsGet(Guid posGuid)
        {
            return Resource?.PosOptionDeviceSettingsGet(posGuid);
        }

        #endregion
    }
}