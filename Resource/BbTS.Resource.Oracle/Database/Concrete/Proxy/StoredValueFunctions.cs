﻿using System.Collections.Generic;
using BbTS.Domain.Models.Container;
using BbTS.Domain.Models.StoredValue;
using BbTS.Domain.Models.System.Database;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    public partial class ProxySource
    {
        /// <summary>
        /// Retrieve the stored value account settings for a device.
        /// </summary>
        /// <param name="deviceId">The unique identifier for the device.</param>
        /// <returns>List of stored value accounts for the device.</returns>
        public override List<StoredValueAccountDeviceSetting> StoredValueAccountDeviceSettingsGet(string deviceId)
        {
            return Resource?.StoredValueAccountDeviceSettingsGet(deviceId);
        }

        /// <summary>
        /// Process a stored value deposit request.
        /// </summary>
        /// <param name="transaction">The transaction to process</param>
        /// <returns></returns>
        public override Transaction ProcessStoredValueDepositRequest(Transaction transaction)
        {
            return Resource?.ProcessStoredValueDepositRequest(transaction);
        }

        /// <summary>
        /// Process a stored value deposit reurn request.
        /// </summary>
        /// <param name="transaction">The transaction to process</param>
        /// <returns></returns>
        public override Transaction ProcessStoredValueDepositReturnRequest(Transaction transaction)
        {
            return Resource?.ProcessStoredValueDepositReturnRequest(transaction);
        }

        /// <summary>
        /// This method will return the list of Sv_Account records in the system
        /// </summary>
        /// <param name="connection">The database connection information</param>
        /// <returns></returns>
        public override List<TsSv_Account> SvAccountArchivesGet(ConnectionInfo connection)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Get list of stored value account types
        /// </summary>
        /// <param name="svAccountTypeId">Id filter</param>
        /// <returns>Stored value account types</returns>
        public override List<TsStoredValueAccountType> SvAccountTypeGet(int? svAccountTypeId = null)
        {
            return Resource?.SvAccountTypeGet(svAccountTypeId);
        }

        /// <inheritdoc />
        public override List<StoredValueAccountType> SvAccountTypeGetAll()
        {
            return Resource?.SvAccountTypeGetAll();
        }

        /// <inheritdoc />
        public override List<StoredValueEnrichment> SvEnrichmentsGet(int? svAccountTypeId = null)
        {
            return Resource?.SvEnrichmentsGet(svAccountTypeId);
        }
    }
}