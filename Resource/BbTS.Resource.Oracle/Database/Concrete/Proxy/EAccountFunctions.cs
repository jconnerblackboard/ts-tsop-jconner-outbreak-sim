﻿using BbTS.Core.Logging.Tracing;
using BbTS.Domain.Models.EAccount;

namespace BbTS.Resource.Database.Concrete.Proxy
{
    [Trace(AttributePriority = 2)]
    public partial class ProxySource
    {
        /// <summary>
        /// This method will preauthorize a deposit from EAccounts
        /// </summary>
        /// <param name="request">Details of deposit to pre-authorize</param>
        /// <returns></returns>
        public override PreauthDepositResponse EAccountPreauthDeposit(PreauthDepositRequest request)
        {
            return Resource?.EAccountPreauthDeposit(request);
        }

        /// <summary>
        /// This method will perform a deposit from EAccounts
        /// </summary>
        /// <param name="request">Details of deposit to perform</param>
        /// <returns></returns>
        public override DepositResponse EAccountPerformDeposit(DepositRequest request)
        {
            return Resource?.EAccountPerformDeposit(request);
        }

        /// <summary>
        /// Look up external client customer guid from other information.
        /// </summary>
        /// <param name="request">Customer informatino known for anon deposit.</param>
        /// <returns></returns>
        public override GetExternalCustGuidFromAnonResponse GetExternalCustGuidFromAnon(GetExternalCustGuidFromAnonRequest request)
        {
            return Resource?.GetExternalCustGuidFromAnon(request);
        }
    }
}