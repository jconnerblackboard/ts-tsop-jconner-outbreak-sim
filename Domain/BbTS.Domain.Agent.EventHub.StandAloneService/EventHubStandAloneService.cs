﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Reflection;
using System.ServiceProcess;
using BbTS.Core.Configuration;
using BbTS.Domain.Agent.EventHub.Code;
using BbTS.Domain.Models.Configuration;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Exceptions.Configuration;
using BbTS.Monitoring.Logging;

namespace BbTS.Domain.Agent.EventHub.StandAloneService
{
    public partial class EventHubStandAloneService : ServiceBase
    {
        public EventHubStandAloneService()
        {
            InitializeComponent();
        }


        /// <summary>
        /// Start the service.
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {
            try
            {
                var assemblyName = Assembly.GetExecutingAssembly().GetName().Name;
                Configuration configuration;
                string configLoadMessage;
                var configPath = $"{assemblyName}.exe.config";
                if (!CustomConfiguration.LoadCustomConfiguration(configPath, out configuration, out configLoadMessage))
                {
                    string errorMessage =
                        $"{assemblyName} is unable to load the custom configuration file because: \r\n{configLoadMessage}.";
                    LoggingManager.Instance.LogException(
                        errorMessage,
                        EventLogEntryType.Error,
                        (short)LoggingDefinitions.Category.Service,
                        (int)LoggingDefinitions.EventId.ServiceStartException);

                    throw new CustomConfigurationException(errorMessage);
                }

                // Store the location of the path to the configuration file in case the plugin needs it later
                var key = $"{new EventHubAgent().Id()}_PathToConfig";
                RegistryConfigurationTool.Instance.ValueSet(key, configPath);

                if (!EventHubAgentManager.Instance.Start(CustomConfigurationModel.GenerateCustomConfigurationModel(configuration)))
                {
                    LoggingManager.Instance.LogException(
                        $"{LoggingManager.LogSource} failed to start.",
                        EventLogEntryType.Error,
                        (short)LoggingDefinitions.Category.Service,
                        (int)LoggingDefinitions.EventId.ServiceUsermodeFailedToStart);
                }
            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(
                    ex, "", "", EventLogEntryType.Error,
                    (short)LoggingDefinitions.Category.Service,
                    (int)LoggingDefinitions.EventId.ServiceUsermodeException);
            }
        }

        /// <summary>
        /// Stop the service.
        /// </summary>
        protected override void OnStop()
        {
            if (!EventHubAgentManager.Instance.Stop())
            {
                LoggingManager.Instance.LogException(
                    $"{LoggingManager.LogSource} failed to stop.",
                    EventLogEntryType.Error,
                    (short)LoggingDefinitions.Category.Service,
                    (int)LoggingDefinitions.EventId.ServiceUsermodeFailedToStart);
            }
        }
    }
}
