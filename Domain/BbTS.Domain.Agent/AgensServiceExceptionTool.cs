﻿using System;
using System.Reflection;
using System.Web.Http;
using BbTS.Domain.Models.Exceptions.Agent;

namespace BbTS.Domain.Agent
{
    public class AgensServiceExceptionTool
    {
        public static HttpError Create<T>(AgentServiceBusinessException exception) where T : Exception
        {
            var properties = exception.GetType().GetProperties(BindingFlags.Instance
                                                             | BindingFlags.Public
                                                             | BindingFlags.DeclaredOnly);
            var error = new HttpError();
            foreach (var propertyInfo in properties)
            {
                error.Add(propertyInfo.Name, propertyInfo.GetValue(exception, null));
            }
            return error;
        }
    }
}
