using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using BbTS.Client.WebApi.Oauth;
using BbTS.Client.WebApi.Validation;
using BbTS.Core;
using BbTS.Core.Logging.Tracing;
using BbTS.Core.Serialization;
using BbTS.Domain.Models.Agent;
using BbTS.Domain.Models.Configuration;
using BbTS.Domain.Models.Definitions.Service;
using BbTS.Domain.Models.Exceptions.WebApi;
using BbTS.Domain.Models.General;
using BbTS.Domain.Models.Service.Request;
using BbTS.Domain.Models.Service.Response;
using BbTS.Domain.Models.System.Database;
using BbTS.Domain.Models.System.Security.Oauth;
using BbTS.Domain.Models.Transaction;
using BbTS.Domain.Security;
using BbTS.Resource.Database.Abstract;
using BbTS.Resource.Service.Abstract;

namespace BbTS.Domain.Agent
{
    /// <summary>
    /// Domain controller class for managing agent plugins through a service.
    /// </summary>
    [Trace(AttributePriority = 2)]
    public class AgentPluginManagerDomainController
    {
        private readonly ResourceDatabase _databaseResource;

        // ReSharper disable once NotAccessedField.Local - Left in for consistancy across domain controllers and for future needs.
        private readonly ResourceServiceAbstract _serviceResource;

        // ReSharper disable once NotAccessedField.Local - Left in for consistancy across domain controllers and for future needs.
        private readonly CustomConfigurationModel _configuration;

        // JSC: commenting these out because they are never used.
        // private readonly OauthClient _oauthClient;
        // private readonly ValidationClient _validationClient;

        private static readonly object DoorIdLock = new object();
        private readonly object _doorAaLock = new object();

        /// <summary>
        /// Parameterized constructor utilizing constructor injection for needed resources.
        /// </summary>
        /// <param name="configuration">Custom configuraiton object.</param>
        /// <param name="database">Database resource.</param>
        /// <param name="service">Service resource.</param>
        [Trace(AttributeExclude = true, AttributePriority = 2)]
        public AgentPluginManagerDomainController(CustomConfigurationModel configuration, ResourceDatabase database, ResourceServiceAbstract service)
        {
            _databaseResource = database;
            _serviceResource = service;
            _configuration = configuration;
        }

        /// <summary>
        /// Get a list of view model agents for display in the UI.
        /// </summary>
        /// <param name="agentServiceFqdn">The FQDN of the agent service to query.</param>
        /// <param name="sessionToken">Validated session token.</param>
        /// <param name="parameters">Oauth parameters.  If token is invalid a new one will be requested using the application credentials included within.</param>
        /// <returns>List of view model agents.</returns>
        public List<AgentViewModel> AgentViewModelListGet(string agentServiceFqdn, OAuthParameters parameters, string sessionToken)
        {
            var list = new List<AgentViewModel>();

            var installed = InstalledAgentListGet(agentServiceFqdn, parameters, sessionToken).Select(agent => new AgentViewModel(agent) { InstalledVersion = agent.Version }).ToList();
            var updates = UpdateAgentListGet(agentServiceFqdn, parameters, sessionToken).Select(agent => new AgentViewModel(agent) { UpdateVersion = agent.Version }).ToList();

            var installedWithUpdate = installed.Intersect(updates, new AgentViewModelComparer());
            foreach (var agent in installedWithUpdate)
            {
                var update = updates.FirstOrDefault(u => u.Id == agent.Id);
                if (update != null)
                {
                    agent.UpdateVersion = update.UpdateVersion;
                    list.Add(agent);
                }
            }

            list.AddRange(installed.Except(updates, new AgentViewModelComparer()));
            list.AddRange(updates.Except(installed, new AgentViewModelComparer()));

            return list;
        }

        /// <summary>
        /// Get a list of all installed agents at the provided agent service url.
        /// </summary>
        /// <param name="agentServiceFqdn">The FQDN of the agent service to query.</param>
        /// <param name="sessionToken">Validated session token.</param>
        /// <param name="parameters">Oauth parameters.  If token is invalid a new one will be requested using the application credentials included within.</param>
        /// <returns>List of installed agents.  Empty list if no agents are installed at the provided location.</returns>
        public List<AgentDescriptor> InstalledAgentListGet(string agentServiceFqdn, OAuthParameters parameters, string sessionToken)
        {
            var baseUri = $"https://{agentServiceFqdn}/agentservice/api/v1";
            var route = @"agents/installed";

            var request = new HttpRestServiceRequest<HttpResponseMessage>
            {
                AcceptType = RequestHeaderSupportedContentType.Json,
                BaseUri = baseUri,
                Body = new HttpResponseMessage(HttpStatusCode.OK),
                ContentType = RequestHeaderSupportedContentType.Json,
                Method = SupportedHttpRequestMethod.GET,
                Route = route
            };
            request.CustomHeaders.Add(new StringPair { Key = "SessionToken", Value = sessionToken });

            HttpRestServiceResponse<HttpResponseMessage> databaseConnectionResponse;

            if (string.IsNullOrWhiteSpace(parameters.TokenKey) || string.IsNullOrWhiteSpace(parameters.TokenSecret))
            {
                databaseConnectionResponse = _serviceResource.OauthSecuredRestRequest(request, parameters.BaseUri);
            }
            else
            {
                databaseConnectionResponse = _serviceResource.OauthSecuredRestRequest(request, parameters.BaseUri, parameters);
            }

            var responseObject =
                NewtonsoftJson.Deserialize<List<AgentDescriptor>>(databaseConnectionResponse.Response.Content);

            return responseObject;
        }

        /// <summary>
        /// Get a list of all installed agents at the provided agent service url.
        /// </summary>
        /// <param name="agentServiceFqdn">The FQDN of the agent service to query.</param>
        /// <param name="sessionToken">Validated session token.</param>
        /// <param name="parameters">Oauth parameters.  If token is invalid a new one will be requested using the application credentials included within.</param>
        /// <returns>List of installed agents.  Empty list if no agents are installed at the provided location.</returns>
        public List<AgentDescriptor> UpdateAgentListGet(string agentServiceFqdn, OAuthParameters parameters, string sessionToken)
        {
            var baseUri = $"https://{agentServiceFqdn}/agentservice/api/v1";
            var route = @"agent/updates";

            var request = new HttpRestServiceRequest<HttpResponseMessage>
            {
                AcceptType = RequestHeaderSupportedContentType.Json,
                BaseUri = baseUri,
                Body = new HttpResponseMessage(HttpStatusCode.OK),
                ContentType = RequestHeaderSupportedContentType.Json,
                Method = SupportedHttpRequestMethod.GET,
                Route = route
            };
            request.CustomHeaders.Add(new StringPair { Key = "SessionToken", Value = sessionToken });

            HttpRestServiceResponse<HttpResponseMessage> databaseConnectionResponse;

            if (string.IsNullOrWhiteSpace(parameters.TokenKey) || string.IsNullOrWhiteSpace(parameters.TokenSecret))
            {
                databaseConnectionResponse = _serviceResource.OauthSecuredRestRequest(request, parameters.BaseUri);
            }
            else
            {
                databaseConnectionResponse = _serviceResource.OauthSecuredRestRequest(request, parameters.BaseUri, parameters);
            }

            var responseObject =
                NewtonsoftJson.Deserialize<List<AgentDescriptor>>(databaseConnectionResponse.Response.Content);

            return responseObject;
        }

        /// <summary>
        /// Install an agent.
        /// </summary>
        /// <param name="agent">The agent to install</param>
        /// <param name="agentServiceFqdn">The FQDN of the agent service to query.</param>
        /// <param name="sessionToken">Validated session token.</param>
        /// <param name="parameters">Oauth parameters.  If token is invalid a new one will be requested using the application credentials included within.</param>
        /// <returns>List of installed agents.  Empty list if no agents are installed at the provided location.</returns>
        public AgentActionResponse InstallAgentPost(AgentDescriptor agent, string agentServiceFqdn, OAuthParameters parameters, string sessionToken)
        {
            var baseUri = $"https://{agentServiceFqdn}/agentservice/api/v1";
            var route = @"agent/install";

            var request = new HttpRestServiceRequest<AgentActionRequest>
            {
                AcceptType = RequestHeaderSupportedContentType.Json,
                BaseUri = baseUri,
                Body = new AgentActionRequest { Descriptor = agent, RequestId = Guid.NewGuid().ToString("D")},
                ContentType = RequestHeaderSupportedContentType.Json,
                Method = SupportedHttpRequestMethod.POST,
                Route = route
            };
            request.CustomHeaders.Add(new StringPair { Key = "SessionToken", Value = sessionToken });

            HttpRestServiceResponse<AgentActionRequest> databaseConnectionResponse;

            if (string.IsNullOrWhiteSpace(parameters.TokenKey) || string.IsNullOrWhiteSpace(parameters.TokenSecret))
            {
                databaseConnectionResponse = _serviceResource.OauthSecuredRestRequest(request, parameters.BaseUri);
            }
            else
            {
                databaseConnectionResponse = _serviceResource.OauthSecuredRestRequest(request, parameters.BaseUri, parameters);
            }

            var responseObject = NewtonsoftJson.Deserialize<AgentActionResponse>(databaseConnectionResponse.Response.Content);

            return responseObject;
        }

        /// <summary>
        /// Update an agent.
        /// </summary>
        /// <param name="agent">The agent to update</param>
        /// <param name="agentServiceFqdn">The FQDN of the agent service to query.</param>
        /// <param name="sessionToken">Validated session token.</param>
        /// <param name="parameters">Oauth parameters.  If token is invalid a new one will be requested using the application credentials included within.</param>
        /// <returns></returns>
        public AgentManagerActionResponse UpdateAgentPost(AgentDescriptor agent, string agentServiceFqdn, OAuthParameters parameters, string sessionToken)
        {
            var baseUri = $"https://{agentServiceFqdn}/agentservice/api/v1";
            var route = @"agent/update";

            var request = new HttpRestServiceRequest<AgentActionRequest>
            {
                AcceptType = RequestHeaderSupportedContentType.Json,
                BaseUri = baseUri,
                Body = new AgentActionRequest { Descriptor = agent},
                ContentType = RequestHeaderSupportedContentType.Json,
                Method = SupportedHttpRequestMethod.POST,
                Route = route
            };
            request.CustomHeaders.Add(new StringPair { Key = "SessionToken", Value = sessionToken });

            HttpRestServiceResponse<AgentActionRequest> databaseConnectionResponse;

            if (string.IsNullOrWhiteSpace(parameters.TokenKey) || string.IsNullOrWhiteSpace(parameters.TokenSecret))
            {
                databaseConnectionResponse = _serviceResource.OauthSecuredRestRequest(request, parameters.BaseUri);
            }
            else
            {
                databaseConnectionResponse = _serviceResource.OauthSecuredRestRequest(request, parameters.BaseUri, parameters);
            }

            var responseObject = NewtonsoftJson.Deserialize<AgentManagerActionResponse>(databaseConnectionResponse.Response.Content);

            return responseObject;
        }

        /// <summary>
        /// Update an agent.
        /// </summary>
        /// <param name="guid">The unique identifier of the agent to remove.</param>
        /// <param name="agentServiceFqdn">The FQDN of the agent service to query.</param>
        /// <param name="sessionToken">Validated session token.</param>
        /// <param name="parameters">Oauth parameters.  If token is invalid a new one will be requested using the application credentials included within.</param>
        /// <returns></returns>
        public AgentManagerActionResponse RemoveAgentDelete(string guid, string agentServiceFqdn, OAuthParameters parameters, string sessionToken)
        {
            Guard.IsGuid(guid);

            var baseUri = $"https://{agentServiceFqdn}/agentservice/api/v1";
            var route = $"agent/{guid}";

            var request = new HttpRestServiceRequest<HttpResponseMessage>
            {
                AcceptType = RequestHeaderSupportedContentType.Json,
                BaseUri = baseUri,
                Body = new HttpResponseMessage(),
                ContentType = RequestHeaderSupportedContentType.Json,
                Method = SupportedHttpRequestMethod.DELETE,
                Route = route
            };
            request.CustomHeaders.Add(new StringPair { Key = "SessionToken", Value = sessionToken });

            HttpRestServiceResponse<HttpResponseMessage> databaseConnectionResponse;

            if (string.IsNullOrWhiteSpace(parameters.TokenKey) || string.IsNullOrWhiteSpace(parameters.TokenSecret))
            {
                databaseConnectionResponse = _serviceResource.OauthSecuredRestRequest(request, parameters.BaseUri);
            }
            else
            {
                databaseConnectionResponse = _serviceResource.OauthSecuredRestRequest(request, parameters.BaseUri, parameters);
            }

            var responseObject = NewtonsoftJson.Deserialize<AgentManagerActionResponse>(databaseConnectionResponse.Response.Content);

            return responseObject;
        }

    }
}

