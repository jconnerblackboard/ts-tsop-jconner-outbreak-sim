﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using BbTS.Core.Configuration;
using BbTS.Core.Conversion;
using BbTS.Core.Logging.Tracing;
using BbTS.Core.Logging.Tracking;
using BbTS.Core.Serialization;
using BbTS.Core.System.FileUtilities;
using BbTS.Domain.Models.Agent;
using BbTS.Domain.Models.Configuration;
using BbTS.Domain.Models.Definitions.Agent;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Definitions.Service;
using BbTS.Domain.Models.Service.Request;
using BbTS.Domain.Models.System;
using BbTS.Domain.Models.System.Logging.Tracking;
using BbTS.Resource.Database.Abstract;
using BbTS.Resource.Service.Abstract;

namespace BbTS.Domain.Agent
{
    /// <summary>
    /// Domain controller for handling management functions related to agent control.
    /// </summary>
    [Serializable]
    [Trace]
    public class AgentPluginController
    {
        private static readonly Dictionary<string, AppDomain> LoadedDomains = new Dictionary<string, AppDomain>();
        private static readonly object LockLoadedDomains = new object();
        private readonly ResourceDatabase _databaseResource;
        private readonly ResourceServiceAbstract _serviceResource;

        /// <summary>
        /// Configuration object.
        /// </summary>
        public CustomConfigurationModel Configuration { get; internal set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="databaseResource"></param>
        /// <param name="serviceResource"></param>
        [Trace(AttributeExclude = true, AttributePriority = 1)]
        public AgentPluginController(
            CustomConfigurationModel configuration,
            ResourceDatabase databaseResource,
            ResourceServiceAbstract serviceResource)
        {
            Configuration = configuration;
            _databaseResource = databaseResource;
            _serviceResource = serviceResource;


            // Create the update and plugin directories.
            string message;
            // Verify and create the directory if it doesn't exist
            var updateDirectory = CustomConfiguration.GetKeyValueAsString("AgentUpdateDirectory", Configuration, ".");
            if (!FileConfigurationManager.VerifyAndCreateDirectory(updateDirectory, out message))
            {
                throw new ApplicationException(message);
            }

            var pluginDir = CustomConfiguration.GetKeyValueAsString("AgentPluginDirectory", Configuration, ".");
            if (!FileConfigurationManager.VerifyAndCreateDirectory(pluginDir, out message))
            {
                throw new ApplicationException(message);
            }
        }

        /// <summary>
        /// Delete an agent plugin from disk.
        /// </summary>
        /// <param name="descriptor"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool DeleteAgentPlugin(AgentDescriptor descriptor, out string message)
        {
            try
            {
                lock (LockLoadedDomains)
                {
                    if (LoadedDomains.ContainsKey(descriptor.Id))
                    {
                        message = $"Unable to delete agent plugin {descriptor.Id} ({descriptor.AgentName} version {descriptor.Version}) and the domain because the plugin is still loaded.  Unload the plugin and try again.";
                        return false;
                    }
                }

                var pluginDir = CustomConfiguration.GetKeyValueAsString("AgentPluginDirectory", Configuration, ".");
                var pluginDirectory = $"{pluginDir}\\{descriptor.Id}";
                Directory.Delete(pluginDirectory, true);
                var key = $"{descriptor.Id}_PathToConfig";
                RegistryConfigurationTool.Instance.ValueDelete(key);

                var result = DeauthorizeAgent(descriptor);
                if (result.Code != AgentServiceDefinitions.AgentServiceActionResultDomainIdCode.Success)
                {
                    message = $"Removal of plugin agent '{descriptor.AgentName}' was successful.  " +
                              $"However, the agent couldn't be deauthorized because an error occured during the deauthorization process.\r\n" +
                              $"Code: {result.Code} - {result.CodeString}.\r\n" +
                              $"Additional information: {result.Message}";

                    return false;
                }

            }
            catch (Exception ex)
            {
                message = $"Failed to delete agent plugin {descriptor.Id} ({descriptor.AgentName} version {descriptor.Version}). Exception was {Formatting.FormatException(ex)}.";
                return false;
            }

            message = "Success";
            return true;
        }

        /// <summary>
        /// Download new updates from the update server.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool DownloadNewUpdates(out string message)
        {
            var updateHostname = CustomConfiguration.GetKeyValueAsString("UpdateServerHostname", Configuration);
            if (String.IsNullOrEmpty(updateHostname))
            {
                message =
                    "Failed to download updates because the UpdateServerHostname is not set in the application configuration file.";
                return false;
            }

            var baseurl = $"https://{updateHostname}/UpdateService/api/v1/";
            try
            {
                var request = new HttpRestServiceRequest<string>
                {
                    AcceptType = RequestHeaderSupportedContentType.Json,
                    BaseUri = baseurl,
                    Body = "",
                    ContentType = RequestHeaderSupportedContentType.Json,
                    Method = SupportedHttpRequestMethod.GET,
                    Route = "updates/list"
                };

                var response = _serviceResource.RestRequest(request);
                var updateDescriptors = NewtonsoftJson.Deserialize<List<AgentDescriptor>>(response.Response.Content);

                var files = new List<string>();
                if (
                    !DirectoryTools.GetFilePathsWithExtension(ref files,
                        CustomConfiguration.GetKeyValueAsString("AgentUpdateDirectory", Configuration, "."), "*.xml", out message))
                {
                    message = $"Unable to get a list of *.xml files in the AgentUpdateDirectory.  {message}";
                    return false;
                }

                // Get the downloaded descriptors (the updates waiting to be applied)
                List<AgentDescriptor> downloadedDescriptors = new List<AgentDescriptor>(files.Select(Xml.Deserialize<AgentDescriptor>));

                // Get a list of updates that are:
                // 1. Not installed
                // 2. Installed but of a different version
                var diff = updateDescriptors.Except(downloadedDescriptors, new AgentDescriptorComparer());

                foreach (var descriptor in diff)
                {
                    request = new HttpRestServiceRequest<string>
                    {
                        AcceptType = RequestHeaderSupportedContentType.Json,
                        BaseUri = baseurl,
                        Body = "",
                        ContentType = RequestHeaderSupportedContentType.Json,
                        Method = SupportedHttpRequestMethod.GET,
                        Route = $"update/download/{descriptor.Id}/{descriptor.Version}"
                    };
                    response = _serviceResource.RestRequest(request);

                    var updateRepoDirectory = $"{CustomConfiguration.GetKeyValueAsString("AgentUpdateDirectory", Configuration, ".")}";
                    var updateDirectory = $"{updateRepoDirectory}\\{descriptor.Id}";
                    var updatebase = $"{updateDirectory}\\{descriptor.AgentName}_{descriptor.Version}";

                    byte[] bytes = Encoding.ASCII.GetBytes(response.Response.Content);

                    var zipPath = FileConfigurationManager.VerifyFilePathAndDirectory(updatebase, ".zip");

                    // Save the zip file
                    File.WriteAllBytes(zipPath, bytes);

                    var xmlPath = FileConfigurationManager.VerifyFilePathAndDirectory(updatebase, ".xml");

                    // Save the xml descriptor file
                    File.WriteAllText(xmlPath, Xml.Serialize(descriptor));
                }
            }
            catch (Exception ex)
            {
                message = $"Failed to download new updates.  {Formatting.FormatException(ex)}";
                return false;
            }

            message = "Success";
            return true;
        }

        /// <summary>
        /// Install an agent plugin from the specified location.
        /// </summary>
        /// <param name="descriptor">Descriptor of the agent to install</param>
        /// <param name="message">Results message.</param>
        /// <returns>Success or Failure: Agent was installed.</returns>
        public bool InstallAgentPlugin(AgentDescriptor descriptor, out string message)
        {
            try
            {
                // Enable the plugin in the data layer
                var result = AuthorizeAgent(descriptor);

                if (result.Code != AgentServiceDefinitions.AgentServiceActionResultDomainIdCode.Success)
                {
                    message = $"Installation of plugin agent '{descriptor.AgentName}' failed.  Authorization of the plugin was unsuccesful with code the following:\r\n" +
                              $"{result.Code}: {result.CodeString}.\r\n" +
                              $"Additional information: {result.Message}.";
                    return false;
                }

                // Validate that the plugin can be installed.
                if (!ValidatePlugin(descriptor, out message)) return false;

                var updateRepoDirectory = CustomConfiguration.GetKeyValueAsString("AgentUpdateDirectory", Configuration, ".");
                var updateDirectory = $"{updateRepoDirectory}\\{descriptor.Id}";
                var updatebase = $"{updateDirectory}\\{descriptor.AgentName}_{descriptor.Version}";

                var zipPath = $"{updatebase}.zip";
                if (!File.Exists($"{zipPath}"))
                {
                    message = $"InstallAgentPlugin: {zipPath} does not exist.  Installation for {descriptor.AgentName} version {descriptor.Version} failed.";
                    return false;
                }

                var rootPluginDirectory = CustomConfiguration.GetKeyValueAsString("AgentPluginDirectory", Configuration, ".");
                var pluginDirectory = $@"{rootPluginDirectory}\{descriptor.Id}";
                var pluginDirectoryTemp = $@"Temp\{descriptor.Id}";

                if (IsLoaded(descriptor.Id))
                {
                    message = "InstallAgentPlugin: Cannot install agent while another instance is installed.  Please remove the old instance first.";
                    return false;
                }

                // If the plugin directory already exists then delete it.
                if (Directory.Exists(pluginDirectory))
                {
                    Directory.Delete(pluginDirectory, true);
                }

                // Create the plugin directory and a temporary directory.  Unpack the zip file to the temp directory then copy
                // all files to the plugin directory.  Delete the temp directory afterwards.
                string directorCreateMessage;
                if (!FileConfigurationManager.VerifyAndCreateDirectory(pluginDirectoryTemp, out directorCreateMessage))
                {
                    message = $"InstallAgentPlugin: Cannot install plugin.  {directorCreateMessage}";
                    return false;
                }

                var pluginBase = $"{pluginDirectoryTemp}\\{descriptor.AgentName}_{descriptor.Version}";
                var pluginZip = FileConfigurationManager.VerifyFilePathAndDirectory(pluginBase, ".zip");

                File.Copy(zipPath, pluginZip);

                // Unzip the zip file
                ZipFile.ExtractToDirectory(pluginZip, pluginDirectoryTemp);
                Directory.Move($"{pluginDirectoryTemp}\\{descriptor.Id}", pluginDirectory);

                // Remove the zip file
                File.Delete(pluginZip);
                Directory.Delete(pluginDirectoryTemp, true);
            }
            catch (Exception ex)
            {
                message = $"Failed to install agent plugin {descriptor.Id} ({descriptor.AgentName} version {descriptor.Version}). Exception was {Formatting.FormatException(ex)}.";
                return false;
            }

            message = "Success";
            return true;
        }

        /// <summary>
        /// Indicates whether or not an agent matching the supplied descriptor is currently loaded into the manager.
        /// </summary>
        /// <param name="id">The unique identifer of the agent.</param>
        /// <returns>Loaded or not.</returns>
        public bool IsLoaded(string id)
        {
            lock (LockLoadedDomains)
            {
                return LoadedDomains.ContainsKey(id);
            }
        }

        /// <summary>
        /// Load an agent plugin.
        /// </summary>
        /// <param name="path">Path to the agent to load</param>
        /// <param name="agentPlugin">Agent loaded or null if unsuccessful.</param>
        /// <param name="message">Result message.</param>
        /// <returns></returns>
        public bool LoadAgentPlugin(string path, out IAgentPlugin agentPlugin, out string message)
        {
            var domainName = $"Domain,{path}";
            agentPlugin = null;
            try
            {
                // create a file descriptor based on the path
                var fi = new FileInfo(path);
                var fileDir = fi.DirectoryName;

                var setup = new AppDomainSetup
                {
                    ApplicationBase = fileDir
                };

                // create an app domain based on the path.
                var domain = AppDomain.CreateDomain(domainName, AppDomain.CurrentDomain.Evidence, setup);

                var rootFileName = fi.Name.Substring(0, fi.Name.Length - fi.Extension.Length);
                var defaultConfigPath = $"{fileDir}\\{rootFileName}.dll.config";

                // Load the configuration file for the agent plugin
                Configuration configuration;
                if (File.Exists(defaultConfigPath))
                {
                    if (!CustomConfiguration.LoadCustomConfiguration(defaultConfigPath, out configuration,
                        out message))
                    {
                        EventLogTracker.Instance.Log(new EventLogTrackingObject
                        {
                            Message = $"Unable to process the configuraiton file found at {defaultConfigPath}.  {message}",
                            Severity = EventLogEntryType.Error,
                            EventCategory = LoggingDefinitions.Category.Agent,
                            EventId = LoggingDefinitions.EventId.AgentFailedToLoadAgentPlugin
                        });
                    }
                }
                else
                {
                    message = $"Unable to locate an appropriate configuration file at {fileDir}.";
                    return false;
                }

                // Pull the assembly name from the agent plugin configuration data
                var assemblyFullName = CustomConfiguration.GetKeyValueAsString("AgentAssemblyName", configuration);
                if (string.IsNullOrEmpty(assemblyFullName))
                {
                    message = "Plugin assembly name not found in configuration file.";
                    return false;
                }

                // Load the assembly for the agent plugin
                agentPlugin = domain.CreateInstanceFromAndUnwrap(path, assemblyFullName) as IAgentPlugin;

                if (agentPlugin != null)
                {
                    var descriptor = new AgentDescriptor
                    {
                        Id = agentPlugin.Id(),
                        AgentName = agentPlugin.AgentName(),
                        Description = agentPlugin.Description(),
                        DisplayName = agentPlugin.DisplayName,
                        Version = agentPlugin.Version
                    };
                    // Make sure the plugin is valid and allowed to be loaded.
                    if (!ValidatePlugin(descriptor, out message))
                    {
                        message = $"Plugin '{agentPlugin.AgentName()}' with id = '{agentPlugin.Id()}' is not authorized for installation.";
                        AppDomain.Unload(domain);
                        return false;
                    }

                    // Set the display name as the path to the agent file
                    agentPlugin.DisplayName = path;

                    agentPlugin.Version = CustomConfiguration.GetKeyValueAsString("Version", configuration, "0.0.0.0");

                    agentPlugin.Configuration = CustomConfigurationModel.GenerateCustomConfigurationModel(configuration);

                    // Store the location of the path to the configuration file in case the plugin needs it later
                    var key = $"{agentPlugin.Id()}_PathToConfig";
                    RegistryConfigurationTool.Instance.ValueSet(key, defaultConfigPath);


                    // Emit the location of the application configuration file to the tracer for debugging.
                    TraceEventSource.Instance.Trace($"{agentPlugin.AgentName()}: config path is being recorded at '{defaultConfigPath}' under the key '{agentPlugin.Id()}_PathToConfig'.");

                    // Tell the agent to load and configure itself
                    if (!agentPlugin.LoadAgent(out message)) return false;

                    lock (LockLoadedDomains)
                    {
                        // Save the app domain
                        LoadedDomains.Add(agentPlugin.Id(), domain);
                    }

                    message = "Success";
                    return true;
                }
            }
            catch (Exception ex)
            {
                message = $"Failed to load agent plugin {path}.  Exception was {Formatting.FormatException(ex)}";
                return false;
            }

            message =
                $"Failed to load agent plugin {path} and the domain {domainName} associated with it because it doesn't implement IAgentPlugin.";
            return false;
        }


        /// <summary>
        /// Get all new agents and add them to the agent list.
        /// </summary>
        /// <param name="agents">Current list of loaded agents</param>
        /// <param name="doNotLoad">List of agent ids to skip</param>
        /// <param name="message">Operation result message</param>
        /// <returns></returns>
        public bool NewAgentsGet(List<IAgentPlugin> agents, List<string> doNotLoad, out string message)
        {
            var pluginDir = CustomConfiguration.GetKeyValueAsString("AgentPluginDirectory", Configuration, ".");
            var files = new List<string>();

            if (!DirectoryTools.GetFilePathsWithExtension(ref files, pluginDir, "*.plug", out message))
            {
                message = $"Unable to get a list of *.plug files in the AgentPluginDirectory.  {message}";
                return false;
            }
            var unloadedAgents = files.Except(agents.Select(a => a.DisplayName));

            int failedCount = 0;
            var enumerable = unloadedAgents as IList<string> ?? unloadedAgents.ToList();
            foreach (var agent in enumerable)
            {
                if (doNotLoad.Contains(agent)) continue;

                IAgentPlugin agentPlugin;
                if (!LoadAgentPlugin(agent, out agentPlugin, out message))
                {
                    failedCount++;
                    EventLogTracker.Instance.Log(new EventLogTrackingObject
                    {
                        Message = $"Failed to load agent plugin {agent}.  Reason: {message}",
                        Severity = EventLogEntryType.Error,
                        EventCategory = LoggingDefinitions.Category.Agent,
                        EventId = LoggingDefinitions.EventId.AgentFailedToLoadAgentPlugin
                    });
                    continue;
                }
                // Load the plugin if it hasn't been flagged as do not load.
                if (!doNotLoad.Contains(agentPlugin.Id())) agents.Add(agentPlugin);
            }
            message = enumerable.Count == 0 ?
                "No new agents to load" :
                failedCount == 0 ?
                    "All agent plugins loaded successfully." :
                    $"Failed to load {failedCount} of {enumerable.Count} plugins.  " +
                    $"Check the application event log under event id {(short)LoggingDefinitions.Category.Agent} and category of {(int)LoggingDefinitions.EventId.AgentFailedToLoadAgentPlugin} for more details.";
            return true;
        }

        /// <summary>
        /// Load an agent plugin.
        /// </summary>
        /// <param name="id">The unique identifier for the agent.</param>
        /// <param name="message">Result message.</param>
        /// <returns></returns>
        public bool UnloadAgentPlugin(string id, out string message)
        {
            try
            {
                if (IsLoaded(id))
                {
                    lock (LockLoadedDomains)
                    {

                        AppDomain.Unload(LoadedDomains[id]);
                        LoadedDomains.Remove(id);

                        message = $"Successfully unloaded agent plugin '{id}' and the domain associated with it.";
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                message = $"Failed to unload agent plugin '{id}'. Exception was {Formatting.FormatException(ex)}.";
                return false;
            }

            message = $"Failed to unload agent plugin '{id}' because it is not currently loaded.";
            return false;
        }

        /// <summary>
        /// Get a list of updates waiting to be applied.  Return a list of agent descriptors.
        /// </summary>
        /// <param name="updates">list of agent descriptors</param>
        /// <param name="message">opeation results message</param>
        /// <returns></returns>
        public bool UpdatesGet(out List<AgentDescriptor> updates, out string message)
        {
            updates = new List<AgentDescriptor>();

            var pluginDir = CustomConfiguration.GetKeyValueAsString("AgentUpdateDirectory", Configuration, ".");
            var agentUpdateList = new List<string>();
            if (!DirectoryTools.GetFilePathsWithExtension(ref agentUpdateList, pluginDir, "*.xml", out message))
            {
                message = $"Unable to get a list of *.xml files in the AgentUpdateDirectory.  {message}";
                return false;
            }

            updates = new List<AgentDescriptor>(agentUpdateList.Select(a => Xml.Deserialize<AgentDescriptor>(File.ReadAllText(a))));
            message = "Success";
            return true;
        }

        /// <summary>
        /// Validate that a plugin/agent is allowed to be installed.
        /// </summary>
        /// <param name="descriptor">Description for the agent.</param>
        /// <param name="message">Results message.</param>
        /// <returns>Success or Failure: Agent is valid.</returns>
        public bool ValidatePlugin(AgentDescriptor descriptor, out string message)
        {
            var authorizedPlugins = _databaseResource.ParameterListGet("AuthorizedAgentPlugins");
            if (authorizedPlugins == null)
            {
                message = $"The list of authorized plugins was empty.  The plugin '{descriptor.AgentName}' with id '{descriptor.Id}' is not authorized to run.";
                return false;
            }

            if (authorizedPlugins.FirstOrDefault(p => p.ParameterValue == descriptor.Id) == null)
            {
                message = $"The plugin '{descriptor.AgentName}' with id '{descriptor.Id}' is not authorized to run.";
                return false;
            }

            message = "Success";
            return true;
        }

        /// <summary>
        /// Authorize an agent to run.
        /// </summary>
        /// <param name="agent">The agent.</param>
        /// <returns></returns>
        public AgentManagerActionResponse AuthorizeAgent(AgentDescriptor agent)
        {
            var parameterGetResponse = _databaseResource.ParameterGet("AuthorizedAgentPlugins");
            if (parameterGetResponse == null)
            {
                return new AgentManagerActionResponse(
                    agent,
                    AgentPluginManagerAction.AuthorizeAgent,
                    AgentServiceDefinitions.AgentServiceActionResultDomainIdCode.AuthorizeAgentFailedNoParentKey,
                    AgentServiceDefinitions.ResultCodeToString(AgentServiceDefinitions.AgentServiceActionResultDomainIdCode.AuthorizeAgentFailedNoParentKey));
            }

            var request = new ControlParameterPatchRequest
            {
                ParameterName = agent.AgentName,
                ParameterValue = agent.Id,
                ParameterDescription = agent.Description,
                ParameterType = "Variable",
                ParentControlParameterId = parameterGetResponse.ControlParameterId
            };

            var response = _databaseResource.ParameterSet(request);
            if (response == null)
            {
                return new AgentManagerActionResponse(
                    agent,
                    AgentPluginManagerAction.AuthorizeAgent,
                    AgentServiceDefinitions.AgentServiceActionResultDomainIdCode.AuthorizeAgentFailedPostCreateValidationCheck,
                    AgentServiceDefinitions.ResultCodeToString(AgentServiceDefinitions.AgentServiceActionResultDomainIdCode.AuthorizeAgentFailedPostCreateValidationCheck));
            }

            return new AgentManagerActionResponse(
                agent,
                AgentPluginManagerAction.AuthorizeAgent,
                AgentServiceDefinitions.AgentServiceActionResultDomainIdCode.Success,
                "Success.");
        }

        /// <summary>
        /// Authorize an agent to run.
        /// </summary>
        /// <param name="agent">The agent.</param>
        /// <returns></returns>
        public AgentManagerActionResponse DeauthorizeAgent(AgentDescriptor agent)
        {
            // Get all the authorized plugins.  If there are no authorized plugins, then the 
            // agent is not authorized to run and the operation was successful.
            var parameterGetResponse = _databaseResource.ParameterGet("AuthorizedAgentPlugins");
            if (parameterGetResponse == null)
            {
                return new AgentManagerActionResponse(
                    agent,
                    AgentPluginManagerAction.DeauthorizeAgent,
                    AgentServiceDefinitions.AgentServiceActionResultDomainIdCode.Success,
                    "Success.");
            }

            // Delete the plugin from the control parameter list.
            _databaseResource.ParameterDelete(agent.AgentName);

            // If all else, success.
            return new AgentManagerActionResponse(
                agent,
                AgentPluginManagerAction.DeauthorizeAgent,
                AgentServiceDefinitions.AgentServiceActionResultDomainIdCode.Success,
                "Success.");
        }
    }
}

