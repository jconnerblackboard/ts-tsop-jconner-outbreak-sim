﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using BbTS.Core;
using BbTS.Core.Configuration;
using BbTS.Core.Conversion;
using BbTS.Core.Logging.Tracking;
using BbTS.Core.Security.Database;
using BbTS.Domain.Models.Agent;
using BbTS.Domain.Models.Configuration;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Status;
using BbTS.Domain.Models.System.Logging.Tracking;
using BbTS.Monitoring.Logging;
using BbTS.Resource;

//using System.ServiceModel;

namespace BbTS.Domain.Agent
{
    public class AgentServiceManager
    {
        private readonly List<IAgentPlugin> _agents = new List<IAgentPlugin>();
        private static readonly object AgentLock = new object();
        private static readonly object HealthcheckLock = new object();

        private readonly List<String> _doNotLoad = new List<string>();
        private static readonly object DoNotLoadLock = new object();

        private static readonly object ConfigLock = new object();
        //private readonly AgentRestService _restService = new AgentRestService();

        private CustomConfigurationModel _configuration;

        private AgentPluginController _agentPluginController;

        /// <summary>
        /// <see cref="Initialize"/> must be explicitely called if using this constructor before agent management functions can be used.
        /// </summary>
        public AgentServiceManager()
        {
        }

        /// <summary>
        /// Internally calls <see cref="Initialize"/> with the provided configuration object.
        /// </summary>
        /// <param name="configuration">The configuration object.</param>
        public AgentServiceManager(CustomConfigurationModel configuration)
        {
            Initialize(configuration);
        }

        public static AgentServiceManager Instance = new AgentServiceManager();

        #region Initialization
        /// <summary>
        /// Initialize the attached <see cref="AgentPluginController"/> and <see cref="Configuration"/>.
        /// </summary>
        /// <param name="configuration">The custom configuration to use for this manager.</param>
        public void Initialize(CustomConfigurationModel configuration)
        {
            _configuration = configuration;
            var dataSourceString = CustomConfiguration.GetKeyValueAsString("DataSource", _configuration, DataSource.Oracle.ToString());
            Guard.IsNotNullOrWhiteSpace(dataSourceString, "DataSource From Congfig");
            var dataSource = (DataSource)Enum.Parse(typeof(DataSource), dataSourceString);
            var databaseResource = ResourceManager.Instance.Resource(dataSource);
            databaseResource.ConnectionString = DatabaseSecurityTool.CreateConnectionStringFromRegistry();

            var dataSourceServiceString = CustomConfiguration.GetKeyValueAsString("ServiceSource", _configuration, ServiceSource.Service.ToString());
            Guard.IsNotNullOrWhiteSpace(dataSourceString, "DataSource From Congfig");
            var serviceSource = (ServiceSource)Enum.Parse(typeof(ServiceSource), dataSourceServiceString);
            var serviceResource = ResourceManager.Instance.ResourceService(serviceSource);

            _agentPluginController = new AgentPluginController(_configuration, databaseResource, serviceResource);
        }
        #endregion

        #region Service Functions

        /// <summary>
        /// Enable/Disable an agent
        /// </summary>
        /// <param name="id">id of the agent to enable/disable</param>
        /// <param name="value">enable/disable</param>
        /// <param name="message">results message</param>
        /// <returns></returns>
        public bool AgentEnable(string id, bool value, out string message)
        {
            Guard.IsNotNull(_agentPluginController, "AgentPluginController");
            lock (AgentLock)
            {
                var agent = _agents.FirstOrDefault(a => a.Id() == id);
                if (agent == null)
                {
                    message = $"Operation Failed: Agent with id {id} is not currently installed.";
                    return false;
                }
                if (!agent.EnableAgent(value, out message)) return false;
            }

            message = "Success";
            return true;
        }

        /// <summary>
        /// Perform a healthcheck on the service and all attached plugins.
        /// </summary>
        /// <returns></returns>
        public List<HealthcheckItem> HealthCheck()
        {
            var items = new List<HealthcheckItem>();
            try
            {
                // Run agent healthchecks in parallel for all agents
                Parallel.ForEach(_agents, agent =>
                {
                    var agentItems = agent.Healthcheck();

                    lock (HealthcheckLock)
                    {
                        items.AddRange(agentItems);
                    }
                });
            }
            catch (Exception ex)
            {
                EventLogTracker.Instance.Log(new EventLogTrackingObject
                {
                    Message = Formatting.FormatException(ex),
                    Severity = EventLogEntryType.Error,
                    EventCategory = LoggingDefinitions.Category.Agent,
                    EventId = LoggingDefinitions.EventId.AgentManagementCycleFailed
                });
            }

            return items;
        } 


        /// <summary>
        /// Install an agent
        /// </summary>
        /// <param name="descriptor"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool InstallAgent(AgentDescriptor descriptor, out string message)
        {
            Guard.IsNotNull(_agentPluginController, "AgentPluginController");
            lock (AgentLock)
            {
                var index = _agents.FindIndex(a => a.Id() == descriptor.Id);
                if (index >= 0)
                {
                    message = $"Operation Failed: Agent with id {descriptor.Id} is already currently installed.";
                    return false;
                }
                // Install the new plugin
                if (!_agentPluginController.InstallAgentPlugin(descriptor, out message)) return false;
            }

            message = "Success";
            return true;
        }

        /// <summary>
        /// Get a list of all the agents installed in the service by Id and DisplayName
        /// </summary>
        /// <returns></returns>
        public List<AgentDescriptor> InstalledAgentsGet()
        {
            Guard.IsNotNull(_agentPluginController, "AgentPluginController");
            lock (AgentLock)
            {
                return new List<AgentDescriptor>(
                    _agents.Select(
                        a => new AgentDescriptor
                        {
                            AgentName = a.AgentName(),
                            Description = a.Description(),
                            DisplayName = a.DisplayName,
                            Id = a.Id(),
                            Version = a.Version
                        }));
            }
        }

        /// <summary>
        /// Mark all agents as loadable.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool ReloadAllAgents(out string message)
        {
            Guard.IsNotNull(_agentPluginController, "AgentPluginController");
            lock (DoNotLoadLock)
            {
                _doNotLoad.Clear();
            }

            message = "Success";
            return true;
        }

        /// <summary>
        /// Update an agent
        /// </summary>
        /// <param name="id"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool RemoveAgent(string id, out string message)
        {
            Guard.IsNotNull(_agentPluginController, "AgentPluginController");
            lock (AgentLock)
            {
                var index = _agents.FindIndex(a => a.Id() == id);
                if (index < 0)
                {
                    message = $"Operation Failed: Agent with id {id} is not currently installed.";
                    return false;
                }

                // Get the agent display name so that we can perform operations based on that
                var descriptor = new AgentDescriptor
                {
                    AgentName = _agents[index].AgentName(),
                    Description = _agents[index].Description(),
                    DisplayName = _agents[index].DisplayName,
                    Id = _agents[index].Id(),
                    Version = _agents[index].Version
                };

                // Add the agent to the do not load list so it doesn't immediately get reloaded
                lock (DoNotLoadLock) if (!_doNotLoad.Contains(descriptor.DisplayName)) _doNotLoad.Add(descriptor.DisplayName);

                // Unload the agent
                if (!_agents[index].UnloadAgent(out message)) return false;

                // Remove it from the list of agents
                _agents.RemoveAt(index);

                // Unload the assemblies associated with the agent.
                if (!_agentPluginController.UnloadAgentPlugin(descriptor.Id, out message)) return false;

                // Delete the old plugin
                if (!_agentPluginController.DeleteAgentPlugin(descriptor, out message)) return false;

                // Remove the agent from the do not load list so it can get reloaded
                lock (DoNotLoadLock) if (_doNotLoad.Contains(descriptor.DisplayName)) _doNotLoad.Remove(descriptor.DisplayName);
            }

            message = "Success";
            return true;
        }

        /// <summary>
        /// Unload an agent
        /// </summary>
        /// <param name="id">agent id</param>
        /// <param name="message">results</param>
        /// <returns></returns>
        public bool UnloadAgent(string id, out string message)
        {
            Guard.IsNotNull(_agentPluginController, "AgentPluginController");
            lock (AgentLock)
            {
                var index = _agents.FindIndex(a => a.Id() == id);
                if (index < 0)
                {
                    message = $"Operation Failed: Agent with id {id} is not currently installed.";
                    return false;
                }

                // Get the agent display name so that we can perform operations based on that
                var agentName = _agents[index].DisplayName;

                // Add the agent to the do not load list so it doesn't immediately get reloaded
                lock (DoNotLoadLock) if (!_doNotLoad.Contains(agentName)) _doNotLoad.Add(agentName);

                // Unload the agent
                if (!_agents[index].UnloadAgent(out message)) return false;

                // Remove it from the list of agents
                _agents.RemoveAt(index);

                // Finally unload the assemblies associated with the agent.
                if (!_agentPluginController.UnloadAgentPlugin(id, out message)) return false;
            }

            message = "Success";
            return true;
        }

        /// <summary>
        /// Update an agent
        /// </summary>
        /// <param name="descriptor"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool UpdateAgent(AgentDescriptor descriptor, out string message)
        {
            Guard.IsNotNull(_agentPluginController, "AgentPluginController");
            lock (AgentLock)
            {
                var index = _agents.FindIndex(a => a.Id() == descriptor.Id);
                if (index < 0)
                {
                    message = $"Operation Failed: Agent with id {descriptor.Id} is not currently installed.";
                    return false;
                }

                // Get the agent display name so that we can perform operations based on that


                // Get the agent display name so that we can perform operations based on that
                var trueDescriptor = new AgentDescriptor
                {
                    AgentName = _agents[index].AgentName(),
                    Description = _agents[index].Description(),
                    DisplayName = _agents[index].DisplayName,
                    Id = _agents[index].Id(),
                    Version = _agents[index].Version
                };

                // Add the agent to the do not load list so it doesn't immediately get reloaded
                lock (DoNotLoadLock) if (!_doNotLoad.Contains(trueDescriptor.DisplayName)) _doNotLoad.Add(trueDescriptor.DisplayName);

                // Unload the agent
                if (!_agents[index].UnloadAgent(out message)) return false;

                // Remove it from the list of agents
                _agents.RemoveAt(index);
                
                // Unload the assemblies associated with the agent.
                if (!_agentPluginController.UnloadAgentPlugin(trueDescriptor.Id, out message)) return false;

                // Delete the old plugin
                if (!_agentPluginController.DeleteAgentPlugin(trueDescriptor, out message)) return false;

                // Install the new plugin
                if (!_agentPluginController.InstallAgentPlugin(descriptor, out message)) return false;

                // Remove the agent from the do not load list so it can get reloaded
                lock(DoNotLoadLock) if (_doNotLoad.Contains(trueDescriptor.DisplayName)) _doNotLoad.Remove(trueDescriptor.DisplayName);
            }

            message = "Success";
            return true;
        }

        /// <summary>
        /// Run a single management cycle
        /// </summary>
        public void ManagementCycleRun()
        {
            // Load new agents
            lock (AgentLock)
            {
                string message;
                if (!_agentPluginController.NewAgentsGet(_agents, _doNotLoad, out message))
                {
                    EventLogTracker.Instance.Log(new EventLogTrackingObject
                    {
                        Message = message,
                        Severity = EventLogEntryType.Error,
                        EventCategory = LoggingDefinitions.Category.Agent,
                        EventId = LoggingDefinitions.EventId.AgentManagementCycleDiscoverNewAgentsFailed
                    });
                }
            }

            try
            {
                // Run agent upkeep in parallel for all agents
                Parallel.ForEach(_agents, agent => agent.RunManagementInterval());
            }
            catch (Exception ex)
            {
                EventLogTracker.Instance.Log(new EventLogTrackingObject
                {
                    Message = Formatting.FormatException(ex),
                    Severity = EventLogEntryType.Error,
                    EventCategory = LoggingDefinitions.Category.Agent,
                    EventId = LoggingDefinitions.EventId.AgentManagementCycleFailed
                });
            }
        }

        #endregion
    }
}
