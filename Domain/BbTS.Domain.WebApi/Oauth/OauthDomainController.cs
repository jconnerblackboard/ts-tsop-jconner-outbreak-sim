﻿using System;
using System.Configuration;
using BbTS.Core;
using BbTS.Core.Configuration;
using BbTS.Core.Logging.Tracing;
using BbTS.Core.Security.Database;
using BbTS.Domain.Models.System.Security.Oauth;
using BbTS.Resource.Database.Abstract;
using BbTS.Resource.Service.Abstract;

namespace BbTS.Domain.WebApi.Oauth
{
    /// <summary>
    /// Domain controller class to handle OAuth requests to BbTS WebApi.
    /// </summary>
    public class OauthDomainController
    {
        private readonly ResourceDatabase _databaseResource;
        private readonly ResourceServiceAbstract _serviceResource;
        private readonly Configuration _configuration;

        /// <summary>
        /// Parameter constructor.  Initialize internal resource to the provided instance.
        /// </summary>
        /// <param name="configuration"><see cref="CustomConfiguration"/> object containing configuration parameters for the calling app.</param>
        /// <param name="resource"><see cref="ResourceDatabase"/> object with the desired database repository.</param>
        /// <param name="serviceResource"><see cref="ResourceServiceAbstract"/> object with the desired service repository.</param>
        [Trace(AttributeExclude = true, AttributePriority = 1)]
        public OauthDomainController(
            Configuration configuration,
            ResourceDatabase resource,
            ResourceServiceAbstract serviceResource)
        {
            _configuration = configuration;
            _databaseResource = resource;
            _serviceResource = serviceResource;

            // Verify and fetch the connection string information from the configuration values.
            var connectionStringBase = CustomConfiguration.GetKeyValueAsString("OracleConnectionString",
                _configuration);
            var encryptedPassword = CustomConfiguration.GetKeyValueAsString("OraclePassword", _configuration);

            Guard.IsNotNullOrWhiteSpace(connectionStringBase, "connectionStringBase");
            Guard.IsNotNullOrWhiteSpace(encryptedPassword, "encryptedPassword");

            // Create the connection string
            _databaseResource.ConnectionString = DatabaseSecurityTool.CreateConnectionString(connectionStringBase,
                encryptedPassword);
        }

        /// <summary>
        /// Authorizes the specified token key.
        /// </summary>
        /// <param name="tokenKey">The token key.</param>
        /// <returns>The security token instance</returns>
        public OAuth1ASecurityToken Authorize(string tokenKey)
        {
            Guid consumerKey;

            if (!Guid.TryParse(tokenKey, out consumerKey))
            {
                return null;
            }

            OAuth1ASecurityToken securityToken = null;
            var response = _databaseResource.GetApplicationCredentialByConsumerKey(consumerKey);
            if (response != null)
            {
                securityToken = new OAuth1ASecurityToken
                {
                    Token = tokenKey,
                    Secret = response.ApplicationCredential.ConsumerSecret,
                    TimeToLive = response.ApplicationCredential.TimeToLive
                };
            }

            return securityToken;
        }


    }
}
