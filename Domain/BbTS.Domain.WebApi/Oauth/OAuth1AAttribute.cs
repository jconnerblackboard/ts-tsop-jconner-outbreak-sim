﻿using System;
using System.Collections.Specialized;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using BbTS.Core.Configuration;
using BbTS.Core.Security.Oauth;
using BbTS.Domain.Models.Exceptions.Security.Oauth;
using BbTS.Domain.Models.Exceptions.Service;
using BbTS.Domain.Models.System.Security.Oauth;
using BbTS.Resource;
using PostSharp.Aspects.Dependencies;

namespace BbTS.Domain.WebApi.Oauth
{
    /// <summary> 
    /// Aspect that, when applied on a method, emits a trace message before and 
    /// after the method execution. 
    /// </summary> 
    [Serializable]
    [AspectRoleDependency(AspectDependencyAction.Order, AspectDependencyPosition.After, "Trace")]
    public class Oauth1AAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// Method invoked before the execution of the method to which the current 
        /// aspect is applied. 
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var databaseSource =
                (DataSource)Enum.Parse(
                    typeof(DataSource),
                    ApiConfigurationManager.Instance.GetConfigurationItem("DataSource", DataSource.Oracle.ToString()));

            var serviceSource =
                (ServiceSource)Enum.Parse(
                    typeof(ServiceSource),
                    ApiConfigurationManager.Instance.GetConfigurationItem("ServiceSource", ServiceSource.Service.ToString()));

            // Hard return if Oauth is not enabled.
            if (!ApplicationConfiguration.GetKeyValueAsBoolean("OAuthEnabled", true)) return;

            // Get the OAuth Parameters.
            var parameters = OauthTool.ExtractOAuthParameters(actionContext.Request.Headers.Authorization);

            var queryCollection = new NameValueCollection();
            var queryParameters = actionContext.Request.GetQueryNameValuePairs();
            foreach (var param in queryParameters)
            {
                queryCollection.Add(param.Key, param.Value);
            }

            var route = actionContext.Request.GetRouteData().Route.RouteTemplate;
            route = route.StartsWith("/") ? route : $"/{route}";
            var formdata = actionContext.Request.Content.IsFormData() ? actionContext.Request.Content.ReadAsFormDataAsync().Result : null;
            var surrogateRequest = new ServiceStackRequestSurrogate
            {
                AbsoluteUri = actionContext.Request.RequestUri.AbsoluteUri,
                ContentType = actionContext.Request.Content.Headers.ContentType?.MediaType,
                FormData = formdata,
                PathInfo = route,
                QueryString = queryCollection,
                Verb = actionContext.Request.Method.Method
            };

            var oauthController = new OauthDomainController(
                ApiConfigurationManager.Instance.Configuration,
                ResourceManager.Instance.Resource(databaseSource),
                ResourceManager.Instance.ResourceService(serviceSource));

            var securityToken = oauthController.Authorize(parameters.ConsumerKey);

            var surrogateResponse = new ServiceStackResponseSurrogate();
            try
            {
                OauthTool.AuthorizeApiRequest(surrogateRequest, surrogateResponse, parameters, securityToken);
            }
            catch (WebServiceCommunicationFailedException wex)
            {
                HttpStatusCode code = (HttpStatusCode)wex.ResponseCode;
                throw new HttpResponseException(actionContext.Request.CreateErrorResponse(code, wex.Message));
            }
            catch (OauthException oex)
            {
                throw new HttpResponseException(actionContext.Request.CreateErrorResponse(HttpStatusCode.Unauthorized, oex.Message));
            }

        }
    }
}
