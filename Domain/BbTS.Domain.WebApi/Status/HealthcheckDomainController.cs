﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using BbTS.Core.Configuration;
using BbTS.Core.Conversion;
using BbTS.Core.Logging.Tracing;
using BbTS.Core.Security.Database;
using BbTS.Core.Security.Encryption;
using BbTS.Domain.Models.Configuration;
using BbTS.Domain.Models.Definitions;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Definitions.Security.Oauth;
using BbTS.Domain.Models.Definitions.Service;
using BbTS.Domain.Models.Definitions.Status;
using BbTS.Domain.Models.General;
using BbTS.Domain.Models.Service.Request;
using BbTS.Domain.Models.Status;
using BbTS.Domain.Models.System.Security.Oauth;
using BbTS.Monitoring.Logging;
using BbTS.Resource.Database.Abstract;
using BbTS.Resource.Service.Abstract;

namespace BbTS.Domain.WebApi.Status
{
    /// <summary>
    /// Controller designated to perform various status operations including verifying database and service connectivity.
    /// </summary>
    [Trace(AttributePriority = 2)]
    public class HealthcheckDomainController
    {
        private readonly ResourceDatabase _resource;
        private readonly ResourceServiceAbstract _serviceResource;
        private readonly CustomConfigurationModel _configuration;

        /// <summary>
        /// Parameter constructor.  Initialize internal resource to the provided instance.
        /// </summary>
        /// <param name="configuration"><see cref="CustomConfiguration"/> object containing configuration parameters for the calling app.</param>
        /// <param name="resource"><see cref="ResourceDatabase"/> object with the desired database repository.</param>
        /// <param name="serviceResource"><see cref="ResourceServiceAbstract"/> object with the desired service repository.</param>
        [Trace(AttributeExclude = true, AttributePriority = 1)]
        public HealthcheckDomainController(
            CustomConfigurationModel configuration,
            ResourceDatabase resource,
            ResourceServiceAbstract serviceResource)
        {
            _configuration = configuration;
            _resource = resource;
            _serviceResource = serviceResource;
            _resource.ConnectionString = DatabaseSecurityTool.CreateConnectionStringFromRegistry();
        }

        /// <summary>
        /// Execute a healthcheck and analyze the results.
        /// </summary>
        /// <param name="message">results message.</param>
        /// <returns>Successful if all items pass.  Failed if one fails.</returns>
        public bool ExecuteAndAnalyzeHealthCheck(out string message)
        {
            var items = ExecuteHealthcheck();

            var failedItems = items.Where(item => item.Result == HealthcheckItemResult.Fail.ToString()).ToList();
            if (failedItems.Count > 0)
            {
                message = failedItems.Aggregate(string.Empty, (current, item) => $"{current}  {item}");
                return false;
            }

            message = "Success.";
            return true;
        }

        /// <summary>
        /// Execute a complete healthcheck test.
        /// </summary>
        /// <returns><see cref="IEnumerable{T}"/> of <see cref="HealthcheckItem"/> tests performed</returns>
        public IEnumerable<HealthcheckItem> ExecuteHealthcheck()
        {
            var checks = new List<HealthcheckItem>
            {
                ValidateDatabaseConnection(),
                ValidateBbSpConnection(),
                ValidateOauthConnection(),
                ValidateRegistration(),
                LoopbackOauthHealthcheck()
            };

            checks.AddRange(ValidateBbSpSync());
            return checks;
        }

        /// <summary>
        /// Validate that the service can issue an OAuth token and that the issued token is valid.
        /// </summary>
        /// <returns><see cref="HealthcheckItem"/> object containing the results of the request.</returns>
        public HealthcheckItem LoopbackOauthHealthcheck()
        {
            try
            {
                var parameters = _resource.ConsumerKeyAndSecretGetFirstValue();
                if (parameters == null)
                {
                    return HealthcheckDefinitions.CreateItem(
                        HealthcheckItemCategories.Loopback,
                        HealthcheckItemResult.Warn,
                        "No valid OAuth consumer key and secret was found in the data layer to test against.  Cannot proceed with the test.",
                        (short)LoggingDefinitions.Category.WebApi,
                        (int)LoggingDefinitions.EventId.WebApiHealthcheckLoopbackNoAppCredentials);
                }
                var securityMode = CustomConfiguration.GetKeyValueAsString("ServiceSecurityMode", _configuration, "Transport");
                var preamble = securityMode == "Transport" ? @"https://" : @"http://";
                var baseUri = RegistryConfigurationTool.Instance.ValueGet("ApplicationServerHostAddress");
                parameters.BaseUri = !baseUri.StartsWith(preamble) ? preamble + baseUri : baseUri;
                parameters.BaseUri = $"{parameters.BaseUri}/transact/api";
                var token = _serviceResource.AcquireOauth10AToken(parameters);

                var request = new HttpRestServiceRequest<HttpResponseMessage>
                {
                    AcceptType = RequestHeaderSupportedContentType.Json,
                    BaseUri = parameters.BaseUri,
                    Body = new HttpResponseMessage(HttpStatusCode.OK),
                    ContentType = RequestHeaderSupportedContentType.Json,
                    Method = SupportedHttpRequestMethod.POST,
                    Route = "verify"
                };

                _serviceResource.OauthSecuredRestRequest(request, parameters.BaseUri, token);
            }
            catch (Exception ex)
            {
                var exceptionMessage =
                    $"An exception of type: {ex.GetType().Name} has occured.\r\n" +
                    $"Message: {Formatting.FormatException(ex)}\r\n" +
                    $"Stack Trace:\r\n{ex.StackTrace}\r\n";

                LoggingManager.Instance.LogException(
                    exceptionMessage, EventLogEntryType.Error,
                    (short)LoggingDefinitions.Category.Service,
                    (int)LoggingDefinitions.EventId.WebApiHealthcheckLoopbackValidation);

                var message =
                    "Please check the application event log for more details.  " +
                    "Exception was logged under " +
                    $"Category {(short)LoggingDefinitions.Category.Service} and " +
                    $"Event ID {(int)LoggingDefinitions.EventId.WebApiHealthcheckLoopbackValidation}";

                return HealthcheckDefinitions.CreateItem(
                    HealthcheckItemCategories.Loopback,
                    HealthcheckItemResult.Fail,
                    message,
                    (short)LoggingDefinitions.Category.WebApi,
                    (int)LoggingDefinitions.EventId.WebApiHealthcheckLoopbackValidation);
            }

            return HealthcheckDefinitions.CreateItem(
                HealthcheckItemCategories.Loopback,
                HealthcheckItemResult.Pass,
                "Loopback OAuth test was successful.",
                (short)LoggingDefinitions.Category.WebApi,
                (int)LoggingDefinitions.EventId.Success);
        }

        /// <summary>
        /// Validate the connection to the database using a simple ping request.
        /// </summary>
        /// <returns><see cref="HealthcheckItem"/> object containing the results of the request.</returns>
        public HealthcheckItem ValidateDatabaseConnection()
        {
            try
            {
                // Ping the database
                var resultToken = _resource.PingDatabase();
                if (!resultToken.IsResult((int) DomainValue.DomainTransactApi, (int) ConnectionTest.Success))
                {
                    return HealthcheckDefinitions.CreateItem(
                        HealthcheckItemCategories.Database,
                        HealthcheckItemResult.Fail,
                        resultToken.Message,
                        (short)LoggingDefinitions.Category.WebApi,
                        (int)LoggingDefinitions.EventId.WebApiHealthcheckValidateDatabaseConnectionFailed);
                }

            }
            catch (Exception ex)
            {
                var exceptionMessage =
                    $"An exception of type: {ex.GetType().Name} has occured.\r\n" +
                    $"Message: {Formatting.FormatException(ex)}\r\n" +
                    $"Stack Trace:\r\n{ex.StackTrace}\r\n";

                LoggingManager.Instance.LogException(
                    exceptionMessage, EventLogEntryType.Error,
                    (short)LoggingDefinitions.Category.WebApi,
                    (int)LoggingDefinitions.EventId.WebApiHealthcheckValidateDatabaseConnectionFailed);

                var message =
                    "HealthcheckDomainController.ValidateDatabaseConnection failed.  Please check the application event log for more details.  " +
                    "Exception was logged under " +
                    $"Category {(short)LoggingDefinitions.Category.Service} and " +
                    $"Event ID {(int)LoggingDefinitions.EventId.WebApiHealthcheckValidateDatabaseConnectionFailed}";

                return HealthcheckDefinitions.CreateItem(
                    HealthcheckItemCategories.Database,
                    HealthcheckItemResult.Fail,
                    message,
                    (short)LoggingDefinitions.Category.WebApi,
                    (int)LoggingDefinitions.EventId.WebApiHealthcheckValidateDatabaseConnectionFailed);
            }

            // Return a successful healthcheck item.
            return HealthcheckDefinitions.CreateItem(
                HealthcheckItemCategories.Database,
                HealthcheckItemResult.Pass,
                "Connection successfully established against the database.",
                (short)LoggingDefinitions.Category.WebApi,
                (int)LoggingDefinitions.EventId.Success);
        }

        /// <summary>
        /// Verify that a connection exists to the BbSP service.
        /// </summary>
        /// <returns><see cref="HealthcheckItem"/> object with the results of the test.</returns>
        public HealthcheckItem ValidateBbSpConnection()
        {
            // Verify and fetch the connection string information from the configuration values.
            var bbspUrl = RegistryConfigurationTool.Instance.ValueGet("ServicePointBaseURI");
            if (string.IsNullOrEmpty(bbspUrl))
            {
                return HealthcheckDefinitions.CreateItem(
                    HealthcheckItemCategories.BbSPService,
                    HealthcheckItemResult.Fail, 
                    "Failed to parse the BbSP URL from the configuration file.",
                    (short)LoggingDefinitions.Category.WebApi,
                    (int)LoggingDefinitions.EventId.WebApiHealthcheckBbSPCheckNoBbSPUrl);
            }
            
            try
            {
                var request = new HttpRestServiceRequest<string>
                {
                    AcceptType = RequestHeaderSupportedContentType.Json,
                    BaseUri = bbspUrl,
                    Body = string.Empty,
                    ContentType = RequestHeaderSupportedContentType.Json,
                    CustomHeaders = new List<StringPair>(),
                    Method = SupportedHttpRequestMethod.GET,
                    Route = "healthcheck"
                };
                _serviceResource.RestRequest(request);
            }
            catch (Exception ex)
            {
                string exceptionMessage =
                    $"An exception of type: {ex.GetType().Name} has occured.\r\n" +
                    $"Message: {Formatting.FormatException(ex)}\r\n" +
                    $"Stack Trace:\r\n{ex.StackTrace}\r\n";

                LoggingManager.Instance.LogException(
                    exceptionMessage, EventLogEntryType.Error,
                    (short)LoggingDefinitions.Category.WebApi,
                    (int)LoggingDefinitions.EventId.WebApiHealthcheckValidateBbSPConnectionFailed);

                var message =
                    "Please check the application event log for more details.  " +
                    "Exception was logged under " +
                    $"Category {(short)LoggingDefinitions.Category.WebApi} and " +
                    $"Event ID {(int)LoggingDefinitions.EventId.WebApiHealthcheckValidateBbSPConnectionFailed}";

                return HealthcheckDefinitions.CreateItem(
                    HealthcheckItemCategories.BbSPService,
                    HealthcheckItemResult.Fail,
                    message,
                    (short)LoggingDefinitions.Category.WebApi,
                    (int)LoggingDefinitions.EventId.WebApiHealthcheckValidateBbSPConnectionFailed);
            }

            // Return a successful healthcheck item.
            return HealthcheckDefinitions.CreateItem(
                HealthcheckItemCategories.BbSPService,
                HealthcheckItemResult.Pass,
                "Connection successfully established to BbSP.",
                (short)LoggingDefinitions.Category.WebApi,
                (int)LoggingDefinitions.EventId.Success);
        }

        /// <summary>
        /// Validate that the sync has been done with BbSP
        /// </summary>
        /// <returns><see cref="HealthcheckItem"/> objects containing the results of the request.</returns>
        public List<HealthcheckItem> ValidateBbSpSync()
        {
            return new[]
            {
                new { TaskId = LoggingDefinitions.EventId.WebApiBbSPApplicationCredentialSync, Name = "Application Crendentials", Item = HealthcheckItemCategories.BbSPSyncApplicationCredentials },
                new { TaskId = LoggingDefinitions.EventId.WebApiBbSPInstitutionRouteSync, Name = "Institution Routes", Item = HealthcheckItemCategories.BbSPSyncInstitutionRoutes },
                new { TaskId = LoggingDefinitions.EventId.WebApiBbSPMerchantSync, Name = "Merchants", Item = HealthcheckItemCategories.BbSPSyncMerchants }
            }.Select(task =>
            {
                try
                {
                    var latestSyncLog = _resource.EventLogLatestByTaskGet((int)task.TaskId, (short)LoggingDefinitions.Category.WebApi, LoggingManager.LogSource, Environment.MachineName);
                    return HealthcheckDefinitions.CreateItem(
                        task.Item,
                        latestSyncLog != null ? HealthcheckItemResult.Pass : HealthcheckItemResult.Warn,
                        latestSyncLog != null ? $"{task.Name} has been successfully synchronized on {latestSyncLog.CreatedDateTime.ToShortDateString()}." : $"{task.Name} has not been successfully synchronized recently.",
                        (short)LoggingDefinitions.Category.WebApi,
                        latestSyncLog != null ? (int)LoggingDefinitions.EventId.Success : (short)task.TaskId);
                }
                catch (Exception ex)
                {
                    var exceptionMessage =
                        $"An exception of type: {ex.GetType().Name} has occured.\r\n" +
                        $"Message: {Formatting.FormatException(ex)}\r\n" +
                        $"Stack Trace:\r\n{ex.StackTrace}\r\n";

                    LoggingManager.Instance.LogException(
                        exceptionMessage, EventLogEntryType.Error,
                        (short)LoggingDefinitions.Category.WebApi,
                        (int)LoggingDefinitions.EventId.WebApiHealthcheckBbSPSyncValidationFailed);

                    var message =
                        "Please check the application event log for more details.  " +
                        "Exception was logged under " +
                        $"Category {(short)LoggingDefinitions.Category.WebApi} and " +
                        $"Event ID {(int)LoggingDefinitions.EventId.WebApiHealthcheckBbSPSyncValidationFailed}";

                    return HealthcheckDefinitions.CreateItem(
                        task.Item,
                        HealthcheckItemResult.Fail,
                        message,
                        (short)LoggingDefinitions.Category.WebApi,
                        (int)LoggingDefinitions.EventId.WebApiHealthcheckBbSPSyncValidationFailed);
                }
            }).ToList();
        }

        /// <summary>
        /// Validate the OAuth connection to BbSP.
        /// </summary>
        /// <returns><see cref="HealthcheckItem"/> object with the results of the test.</returns>
        public HealthcheckItem ValidateOauthConnection()
        {
            // Verify and fetch the connection string information from the configuration values.
            var bbspUrl = RegistryConfigurationTool.Instance.ValueGet("ServicePointBaseURI");
            if (string.IsNullOrEmpty(bbspUrl))
            {
                return HealthcheckDefinitions.CreateItem(
                    HealthcheckItemCategories.OAuth,
                    HealthcheckItemResult.Fail, 
                    "Failed to validate the OAuth connection to the BbSP URL provided in the configuration file.",
                    (short)LoggingDefinitions.Category.WebApi,
                    (int)LoggingDefinitions.EventId.WebApiHealthcheckOAuthCheckNoBbSPUrl);
            }

            try
            {
                var aesProvider = new AesEncryptionProvider();
                var parameters = new OAuthParameters
                {
                    BaseUri = bbspUrl,
                    ConsumerKey = aesProvider.Decrypt(OAuthConstants.DefaultConsumerKey.Base64Decode()),
                    ConsumerSecret = aesProvider.Decrypt(OAuthConstants.DefaultConsumerSecret.Base64Decode())
                };
                
                _serviceResource.AcquireOauth10AToken(parameters);
            }
            catch (Exception ex)
            {
                var exceptionMessage = 
                    $"An exception of type: {ex.GetType().Name} has occured.\r\n" +
                    $"Message: {Formatting.FormatException(ex)}\r\n" +
                    $"Stack Trace:\r\n{ex.StackTrace}\r\n";

                LoggingManager.Instance.LogException(
                    exceptionMessage, EventLogEntryType.Error,
                    (short)LoggingDefinitions.Category.WebApi,
                    (int)LoggingDefinitions.EventId.WebApiHealthcheckValidateOauthConnection);

                var message =
                    "Please check the application event log for more details.  " +
                    "Exception was logged under " +
                    $"Category {(short)LoggingDefinitions.Category.WebApi} and " +
                    $"Event ID {(int)LoggingDefinitions.EventId.WebApiHealthcheckValidateOauthConnection}";

                return HealthcheckDefinitions.CreateItem(
                    HealthcheckItemCategories.OAuth,
                    HealthcheckItemResult.Fail,
                    message,
                    (short)LoggingDefinitions.Category.WebApi,
                    (int)LoggingDefinitions.EventId.WebApiHealthcheckValidateOauthConnection);
            }

            // Return a successful healthcheck item.
            return HealthcheckDefinitions.CreateItem(
                HealthcheckItemCategories.OAuth,
                HealthcheckItemResult.Pass,
                "OAuth negotiation successful against the provided BbSP service.",
                (short)LoggingDefinitions.Category.WebApi,
                (int)LoggingDefinitions.EventId.Success);
        }

        /// <summary>
        /// Check the current registration status and report on it with the following:
        /// PASS - Service is registered and ready to accept requests
        /// WARN - Service has registered wtih BbSP but is in an incomplete state (initial registration).
        /// FAIL - Service has no registration with BbSP at all
        /// </summary>
        /// <returns><see cref="HealthcheckItem"/> object with the results of the check.</returns>
        public HealthcheckItem ValidateRegistration()
        {
            try
            {
                var localTransactionSystem = _resource.TransactionSystemGet();
                if (localTransactionSystem == null)
                {
                    var notRegisteredMessage = "Service has not been registered nor has it been assigned to a valid insitution.";
                    return HealthcheckDefinitions.CreateItem(
                        HealthcheckItemCategories.Registration,
                        HealthcheckItemResult.Fail,
                        notRegisteredMessage,
                        (short)LoggingDefinitions.Category.WebApi,
                        (int)LoggingDefinitions.EventId.WebApiHealthcheckRegistrationSystemNotRegistered);
                }
                if (!localTransactionSystem.IsValid)
                {
                    var notRegisteredMessage = "Service has been initially registered but it has not been assigned to a valid insitution.";
                    return HealthcheckDefinitions.CreateItem(
                        HealthcheckItemCategories.Registration,
                        HealthcheckItemResult.Warn,
                        notRegisteredMessage,
                        (short)LoggingDefinitions.Category.WebApi,
                        (int)LoggingDefinitions.EventId.WebApiHealthcheckRegistrationSystemNotValid);
                }
            }
            catch (Exception ex)
            {
                var exceptionMessage =
                    $"An exception of type: {ex.GetType().Name} has occured.\r\n" +
                    $"Message: {Formatting.FormatException(ex)}\r\n" +
                    $"Stack Trace:\r\n{ex.StackTrace}\r\n";

                LoggingManager.Instance.LogException(
                    exceptionMessage, EventLogEntryType.Error,
                    (short)LoggingDefinitions.Category.WebApi,
                    (int)LoggingDefinitions.EventId.WebApiHealthcheckValidateRegistration);

                var message =
                    "Please check the application event log for more details.  " +
                    "Exception was logged under " +
                    $"Category {(short)LoggingDefinitions.Category.WebApi} and " +
                    $"Event ID {(int)LoggingDefinitions.EventId.WebApiHealthcheckValidateRegistration}";

                return HealthcheckDefinitions.CreateItem(
                    HealthcheckItemCategories.Registration,
                    HealthcheckItemResult.Fail,
                    message,
                    (short)LoggingDefinitions.Category.WebApi,
                    (int)LoggingDefinitions.EventId.WebApiHealthcheckValidateRegistration);
            }

            // Return a successful healthcheck item.
            return HealthcheckDefinitions.CreateItem(
                HealthcheckItemCategories.Registration,
                HealthcheckItemResult.Pass,
                "Service is properly registered and ready to accept transactions.",
                (short)LoggingDefinitions.Category.WebApi,
                (int)LoggingDefinitions.EventId.Success);
        }
    }
}
