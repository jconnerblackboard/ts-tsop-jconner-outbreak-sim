﻿using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Filters;
using BbTS.Core.Conversion;
using BbTS.Core.System.ExceptionHandling;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Exceptions.Agent;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.Exceptions.Security.Oauth;
using BbTS.Domain.Models.Exceptions.Service;
using BbTS.Domain.Models.Exceptions.Transaction;
using BbTS.Domain.Models.Exceptions.WebApi;
using BbTS.Domain.Models.Transaction;
using BbTS.Domain.Models.Transaction.Processing;
using BbTS.Monitoring.Logging;

namespace BbTS.Domain.WebApi.Aspects
{
    /// <summary>
    /// Aspect class for handling service level exception handling in BbTS WebAPI.
    /// </summary>
    public class WebApiExceptionHandlingAttribute : ExceptionFilterAttribute
    {
        /// <summary>
        /// OnException attribute.
        /// </summary>
        /// <param name="context"></param>
        public override void OnException(HttpActionExecutedContext context)
        {
            var exception = context.Exception as WebApiException;
            if (exception != null)
            {
                throw _createException(exception, context);
            }

            if (context.Exception is WebServiceCommunicationFailedException)
            {
                var wex = (WebServiceCommunicationFailedException)context.Exception;
                throw _createException(
                    new WebApiException("<unknown>", wex.Message, (HttpStatusCode)wex.ResponseCode),
                    context);
            }

            if (context.Exception is OauthException)
            {
                throw _createException(
                    new WebApiException("<unknown>", context.Exception.Message, HttpStatusCode.Unauthorized),
                    context);
            }

            if (context.Exception is HttpResponseException)
            {
                throw context.Exception;
            }

            var resourceLayerException = context.Exception as ResourceLayerException;
            if (resourceLayerException != null)
            {
                var processingResult = new ProcessingResult { RequestId = resourceLayerException.RequestId, ErrorCode = resourceLayerException.EventId, DeniedText = resourceLayerException.Message };
                throw _createException(new WebApiException(processingResult, HttpStatusCode.BadRequest), context);
            }

            var agentServiceBusinessException = context.Exception as AgentServiceBusinessException;
            if (agentServiceBusinessException != null)
            {
                throw _createException(
                    new WebApiException(
                        agentServiceBusinessException.ResultToken.Id,
                        agentServiceBusinessException.ResultToken.Message,
                        HttpStatusCode.BadRequest),
                    context);
            }

            var tpex = context.Exception as TransactionProcessingException;
            if (tpex != null)
            {
                throw _createException(new WebApiException(tpex.Response, HttpStatusCode.BadRequest), context);
            }

            var genex = new WebApiException(new ProcessingResult
            {
                DeniedText = $"An unexpected error occurred, please try again or contact the administrator.  Exception text: {Formatting.FormatException(context.Exception)}",
                ErrorCode = (int)LoggingDefinitions.EventId.WebApiServiceBusinessException,
                RequestId = string.Empty
            }, HttpStatusCode.InternalServerError);
        

            throw _createException(genex, context);
        }

        /// <summary>
        /// Create and log the exception.
        /// </summary>
        /// <param name="exception">exception to log</param>
        /// <param name="context">context.</param>
        /// <returns>formatted <see cref="HttpResponseException"/></returns>
        private HttpResponseException _createException(WebApiException exception, HttpActionExecutedContext context)
        {
            if (exception == null)
            {
                exception = new WebApiException("<unknown>", "An unexpected error occurred, please try again or contact the administrator.", HttpStatusCode.InternalServerError);
            }

            //Log Critical errors
            LoggingManager.Instance.LogWebApiException(
                exception,
                EventLogEntryType.Error,
                (short)LoggingDefinitions.Category.Service,
                (int)LoggingDefinitions.EventId.WebApiServiceBusinessException);

            return new HttpResponseException(
                   context.Request.CreateErrorResponse(
                       exception.HttpStatusCode,
                       ServiceExceptionTool.Create<HttpResponseException>(exception)));
        }
    }
}
