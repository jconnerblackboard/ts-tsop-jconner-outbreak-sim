﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using BbTS.Core.Configuration;
using BbTS.Core.Conversion;
using BbTS.Core.Logging.Tracing;
using BbTS.Core.Serialization;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Monitoring.Logging;

namespace BbTS.Domain.WebApi.Aspects
{
    /// <summary>
    /// Attribute for debugging information coming in and out of a WEB API endpoint.
    /// </summary>
    public class EndpointDebuggerAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// Method invoked before the execution of the method to which the current 
        /// aspect is applied. 
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            try
            {
                if (!Formatting.TfStringToBool(RegistryConfigurationTool.Instance.ValueGet("WebApiEndpointDebugEnabled", "false")))
                {
                    return;
                }

                // Get URL information
                var message = $"\r\n{actionContext.Request.Method.ToString().ToUpper()}: {actionContext.Request.RequestUri}\r\n";

                message = $"{message}\r\nHeaders:\r\n";

                // Enumerate header information
                foreach (var header in actionContext.Request.Headers)
                {
                    var valueEnumeration = string.Empty;
                    foreach (var value in header.Value)
                    {
                        valueEnumeration = $"{value},";
                    }

                    message = $"{message}\"{header.Key}\" : \"{valueEnumeration.Substring(0, valueEnumeration.Length - 1)}\",";
                }
                message = message.Substring(0, message.Length - 1) + "\r\n";

                // Get Body data if the method is of an appropriate type.
                if (actionContext.Request.Method != HttpMethod.Get &&
                    actionContext.Request.Method != HttpMethod.Delete &&
                    actionContext.Request.Method != HttpMethod.Trace &&
                    actionContext.Request.Method != HttpMethod.Options &&
                    actionContext.Request.Method != HttpMethod.Head)
                {
                    var body = string.Empty;
                    foreach (var argument in actionContext.ActionArguments)
                    {
                        // Skip if the parameter is an HttpRequestMessage (it doesn't serialize to JSON properly).
                        if (argument.Value is HttpRequestMessage) continue;

                        var json = argument.Value == null ? "" : NewtonsoftJson.Serialize(argument.Value);
                        body = $"{body}\r\n\"{argument.Key}\" : \"{json}\",";
                    }
                    if (!string.IsNullOrWhiteSpace(body))
                    {
                        message = $"{message}\r\n\r\nBody:\r\n{body.Substring(0, body.Length - 1)}\r\n";
                    }
                }

                // Emit tracing message
                EndpointDebugEventSource.Instance.Trace(message);
            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(ex, EventLogEntryType.Error, (short)LoggingDefinitions.Category.Trace);
            }
        }
    }
}
