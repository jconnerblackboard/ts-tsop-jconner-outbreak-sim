﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using BbTS.Core.Configuration;
using BbTS.Core.Logging.Tracing;
using BbTS.Core.Logging.Tracking;
using BbTS.Domain.Models.Configuration;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Definitions.Web.Api;
using BbTS.Domain.Models.System.Logging.Tracking;
using PostSharp.Aspects;

namespace BbTS.Domain.WebApi.Aspects
{
    /// <summary> 
    /// Aspect that, when applied on a method, emits a trace message before and 
    /// after the method execution. 
    /// </summary> 
    [Serializable]
    public class WebApiConfigurationAttribute : OnMethodBoundaryAspect
    {

        /// <summary> 
        /// Method invoked before the execution of the method to which the current 
        /// aspect is applied. 
        /// </summary> 
        /// <param name="args">Unused.</param> 
        public override void OnEntry(MethodExecutionArgs args)
        {
            if (ApiConfigurationManager.Instance.Configuration != null) return;

            var appConfigPath = ApplicationConfiguration.GetKeyValueAsString($"{WebApiDefinitions.WebApiPluginIdentifier}_PathToConfig");

            // Load the configuration file for the agent plugin
            TraceEventSource.Instance.Trace($"Looking for {appConfigPath}");
            if (File.Exists(appConfigPath))
            {
                string message;
                Configuration configuration;
                if (!CustomConfiguration.LoadCustomConfiguration(appConfigPath, out configuration,
                    out message))
                {
                    EventLogTracker.Instance.Log(new EventLogTrackingObject
                    {
                        Message = $"Unable to process the App.Config file found at {appConfigPath}.  {message}",
                        Severity = EventLogEntryType.Error,
                        EventCategory = LoggingDefinitions.Category.Agent,
                        EventId = LoggingDefinitions.EventId.AgentFailedToLoadAgentPlugin
                    });
                }
                ApiConfigurationManager.Instance.Configuration = CustomConfigurationModel.GenerateCustomConfigurationModel(configuration);
                TraceEventSource.Instance.Trace($"Found {ApiConfigurationManager.Instance.Configuration.Items.Count} items in {appConfigPath}");
            }
        }
    } 
}
