﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Definitions.System;
using BbTS.Domain.Models.Exceptions.WebApi;

namespace BbTS.Domain.WebApi
{
    /// <summary>
    /// Tool class for handling value checking at the Web API layer.  Exceptions are formatted into <see cref="WebApiException"/> exceptions.
    /// </summary>
    public class GuardWebApi
    {
        /// <summary>
        /// Verify that the value is not null.
        /// </summary>
        /// <param name="value">Value to check</param>
        /// <param name="name">Name of the parameter</param>
        /// <param name="requestId">The request associated with the web api call.</param>
        public static void IsNotNull(object value, string name, string requestId)
        {
            if (value == null)
            {
                throw new WebApiException(requestId, $"parameter {name} is null.", HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Verify that the given guid is in a valid guid format.
        /// </summary>
        /// <param name="guid">The value to verify.</param>
        /// <param name="requestId">The request associated with the web api call.</param>
        public static void IsGuid(string guid, string requestId)
        {
            Guid value;
            if (!Guid.TryParse(guid, out value))
            {
                throw new WebApiException(requestId, $"parameter {guid} is not a valid Guid.", HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Verify that the given guid is in a valid guid format.
        /// </summary>
        /// <param name="guid">The value to verify.</param>
        /// <param name="name">Name of the parameter</param>
        /// <param name="requestId">The request associated with the web api call.</param>
        public static void IsNotEmpty(Guid guid, string name, string requestId)
        {
            if (guid == Guid.Empty)
            {
                throw new WebApiException(requestId, $"parameter {name} is empty.", HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Verify that the given URI is in a valid URI format.
        /// </summary>
        /// <param name="uri">The value to verify.</param>
        /// <param name="requestId">The request associated with the web api call.</param>
        public static void IsValidUri(string uri, string requestId)
        {
            Uri value;
            if (!Uri.TryCreate(uri, UriKind.Absolute, out value))
            {
                throw new WebApiException(requestId, $"parameter {uri} is not a valid URI.", HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Verify that the date and time fall within expected parameters for a TSOP date time object.
        /// </summary>
        /// <param name="datetime">Nullable date and time with offset.</param>
        /// <param name="name">Name of the parameter</param>
        /// <param name="requestId">The request associated with the web api call.</param>
        /// <param name="allowNull">Set to allow null values to pass validation.</param>
        public static void IsValidDateTime(DateTimeOffset? datetime, string name, string requestId, bool allowNull = true)
        {
            if(datetime == null  && allowNull)
            {
                return;
            }

            if(datetime == null)
            {
                throw new WebApiException(requestId, $"DateTimeOffset value was null and allow null flag was set to false.", HttpStatusCode.BadRequest);
            }

            if((datetime - SystemDefinitions.TsopDateTimeMin).Value.TotalMilliseconds < 0)
            {
                throw new WebApiException(requestId, 
                    $"{name} value '{Formatting.FormatDateTimeOffset(datetime, true)}' " +
                    $"was less than the acceptable minimum value of {Formatting.FormatDateTimeOffset(SystemDefinitions.TsopDateTimeMin, true)}.",
                    HttpStatusCode.BadRequest);
            }

            if ((SystemDefinitions.TsopDateTimeMax - datetime).Value.TotalMilliseconds < 0)
            {
                throw new WebApiException(requestId,
                    $"{name} value '{Formatting.FormatDateTimeOffset(datetime, true)}' " +
                    $"was greater than the acceptable maximum value of {Formatting.FormatDateTimeOffset(SystemDefinitions.TsopDateTimeMax, true)}.",
                    HttpStatusCode.BadRequest);
            }
        }
    }
}
