﻿using System.Collections.Generic;
using BbTS.Domain.Models.Definitions;
using BbTS.Domain.Models.Definitions.Terminal;
using BbTS.Domain.Models.PaymentExpress;
using BbTS.Domain.Models.System;
using BbTS.Domain.Models.Terminal;
using BbTS.Domain.Resources;

namespace BbTS.Domain.Terminal
{
    public class TerminalController
    {
        /// <summary>
        /// Get a list of POS devices that are configured with Bb Payment Gateway processing.
        /// </summary>
        /// <returns></returns>
        public List<Pos> PosBbPaygateConfiguredGet()
        {
            return ResourceManager.Instance.Resource.PosBbPaygateConfiguredGet();
        }

        /// <summary>
        /// Get the EMV terminal settings
        /// </summary>
        /// <param name="terminalId"></param>
        /// <returns></returns>
        public EmvSettings EmvSettingsGet(int terminalId)
        {
            return ResourceManager.Instance.Resource.EmvSettingsGet(terminalId);
        }

        /// <summary>
        /// Persist EMV TxnPur request messages to the data layer
        /// </summary>
        /// <param name="terminalId">Id of the device initiating the request</param>
        /// <param name="request">Message data to persist</param>
        /// <returns></returns>
        public ActionResultToken EmvTxnPurRequestSet(int terminalId, BbTxnPurRequestLog request)
        {
            ResourceManager.Instance.Resource.EmvTxnPurRequestSet(terminalId, request);
            return DomainTerminalDefinitions.Instance.GetActionResultCode(TerminalActionResultDomainIdCode.Success);
        }
        /// <summary>
        /// Persist EMV TxnPur response messages to the data layer
        /// </summary>
        /// <param name="terminalId">Id of the device initiating the request</param>
        /// <param name="response">Message data to persist</param>
        /// <returns></returns>
        public ActionResultToken EmvTxnPurResponseSet(int terminalId, BbTxnPurResponseLog response)
        {
            ResourceManager.Instance.Resource.EmvTxnPurResponseSet(terminalId, response);
            return DomainTerminalDefinitions.Instance.GetActionResultCode(TerminalActionResultDomainIdCode.Success);
        }

        /// <summary>
        /// Persist EMV TxnRef request messages to the data layer
        /// </summary>
        /// <param name="terminalId">Id of the device initiating the request</param>
        /// <param name="request">Message data to persist</param>
        /// <returns></returns>
        public ActionResultToken EmvTxnRefRequestSet(int terminalId, BbTxnRefRequestLog request)
        {
            ResourceManager.Instance.Resource.EmvTxnRefRequestSet(terminalId, request);
            return DomainTerminalDefinitions.Instance.GetActionResultCode(TerminalActionResultDomainIdCode.Success);
        }
        /// <summary>
        /// Persist EMV TxnRef response messages to the data layer
        /// </summary>
        /// <param name="terminalId">Id of the device initiating the request</param>
        /// <param name="response">Message data to persist</param>
        /// <returns></returns>
        public ActionResultToken EmvTxnRefResponseSet(int terminalId, BbTxnRefResponseLog response)
        {
            ResourceManager.Instance.Resource.EmvTxnRefResponseSet(terminalId, response);
            return DomainTerminalDefinitions.Instance.GetActionResultCode(TerminalActionResultDomainIdCode.Success);
        }

        /// <summary>
        /// Persist EMV TxnRef request messages to the data layer
        /// </summary>
        /// <param name="terminalId">Id of the device initiating the request</param>
        /// <param name="request">Message data to persist</param>
        /// <returns></returns>
        public ActionResultToken EmvTxnSigRequestSet(int terminalId, BbTxnSigRequestLog request)
        {
            ResourceManager.Instance.Resource.EmvTxnSigRequestSet(terminalId, request);
            return DomainTerminalDefinitions.Instance.GetActionResultCode(TerminalActionResultDomainIdCode.Success);
        }
        /// <summary>
        /// Persist EMV TxnRef response messages to the data layer
        /// </summary>
        /// <param name="terminalId">Id of the device initiating the request</param>
        /// <param name="response">Message data to persist</param>
        /// <returns></returns>
        public ActionResultToken EmvTxnSigResponseSet(int terminalId, BbTxnSigResponseLog response)
        {
            ResourceManager.Instance.Resource.EmvTxnSigResponseSet(terminalId, response);
            return DomainTerminalDefinitions.Instance.GetActionResultCode(TerminalActionResultDomainIdCode.Success);
        }
        
        /// <summary>
        /// Persist EMV Get1 response messages to the data layer
        /// </summary>
        /// <param name="terminalId">Id of the device initiating the request</param>
        /// <param name="response">Message data to persist</param>
        /// <returns></returns>
        public ActionResultToken EmvTxnGet1ResponseSet(int terminalId, BbTxnGet1ResponseLog response)
        {
            ResourceManager.Instance.Resource.EmvTxnGet1ResponseSet(terminalId, response);
            return DomainTerminalDefinitions.Instance.GetActionResultCode(TerminalActionResultDomainIdCode.Success);
        }

        /// <summary>
        /// Persist EMV TxnVoid request messages to the data layer
        /// </summary>
        /// <param name="terminalId">Id of the device initiating the request</param>
        /// <param name="request">Message data to persist</param>
        /// <returns></returns>
        public ActionResultToken EmvTxnVoidRequestSet(int terminalId, BbTxnVoidRequestLog request)
        {
            ResourceManager.Instance.Resource.EmvTxnVoidRequestSet(terminalId, request);
            return DomainTerminalDefinitions.Instance.GetActionResultCode(TerminalActionResultDomainIdCode.Success);
        }
        /// <summary>
        /// Persist EMV TxnVoid response messages to the data layer
        /// </summary>
        /// <param name="terminalId">Id of the device initiating the request</param>
        /// <param name="response">Message data to persist</param>
        /// <returns></returns>
        public ActionResultToken EmvTxnVoidResponseSet(int terminalId, BbTxnVoidResponseLog response)
        {
            ResourceManager.Instance.Resource.EmvTxnVoidResponseSet(terminalId, response);
            return DomainTerminalDefinitions.Instance.GetActionResultCode(TerminalActionResultDomainIdCode.Success);
        }
    }
}
