﻿using System;
using System.Collections.Generic;
using BbTS.Domain.Models.Credential;
using BbTS.Domain.Models.Customer;
using BbTS.Domain.Models.PaymentExpress;
using BbTS.Domain.Models.System;
using BbTS.Domain.Models.Terminal;
using BbTS.Domain.Models.Transaction;

namespace BbTS.Domain.Resources.Abstract
{
    public abstract class Resource
    {
        #region Customer Functions

        public abstract TsCustomer CustomerGet(string username, string customerGuid);
        public abstract ActionResultToken CustomerSet(string username, TsCustomer customer);
        public abstract ActionResultToken CustomerDelete(string username, int customerId);
        public abstract CredentialView CustomerCredentialGet(string username, int customerId, string credentialGuid);
        public abstract CustomerCredentialResponse CustomerCredentialSet(string username, int customerId, CustomerCredentialRequest credential);
        public abstract CustomerMobileAttributes CustomerMobileAttributesGet(string username, string id);
        public abstract ActionResultToken CustomerCredentialRetire(string username, string customerId, string credentialGuid);
        public abstract Int32 CustomerIdGet(string userName, string id);
        public abstract ActionResultToken CustomerPhotoSet(int id, byte[] photo);

        #endregion

        #region System Functions

        public abstract ActionResultToken PingGet();
        public abstract TerminalTimeZoneInformation TimeZoneInformationGet();
        public abstract string TransactApiUriGet();
        public abstract DateTime DataResourceDateTimeGet();

        #endregion

        #region Transaction Functions

        public abstract List<CreditCardTransactionView> TransactionCreditCardUnprocessedGet();

        #endregion

        #region Terminal Functions

        public abstract List<Pos> PosBbPaygateConfiguredGet();
        public abstract List<PaymentExpressRequestResponseView> EmvReconciliationTransactionGet(string userName, string id);
        public abstract void EmvReconciliationTransactionSet(PaymentExpressRequestResponseView value);
        public abstract EmvSettings EmvSettingsGet(int terminalId);
        public abstract void EmvTxnPurRequestSet(int terminalId, BbTxnPurRequestLog request);
        public abstract void EmvTxnPurResponseSet(int terminalId, BbTxnPurResponseLog response);
        public abstract void EmvTxnGet1ResponseSet(int terminalId, BbTxnGet1ResponseLog response);
        public abstract void EmvTxnVoidRequestSet(int terminalId, BbTxnVoidRequestLog request);
        public abstract void EmvTxnVoidResponseSet(int terminalId, BbTxnVoidResponseLog response);
        public abstract void EmvTxnRefRequestSet(int terminalId, BbTxnRefRequestLog request);
        public abstract void EmvTxnRefResponseSet(int terminalId, BbTxnRefResponseLog response);
        public abstract void EmvTxnSigRequestSet(int terminalId, BbTxnSigRequestLog request);
        public abstract void EmvTxnSigResponseSet(int terminalId, BbTxnSigResponseLog response);

        #endregion
    }
}
