using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using BbTS.Core.Interfaces.Database.Oracle;
using BbTS.Domain.Models.Credential;
using BbTS.Domain.Models.Customer;
using BbTS.Domain.Models.Definitions;
using BbTS.Domain.Models.Definitions.Customer;
using BbTS.Domain.Models.PaymentExpress;
using BbTS.Domain.Models.System;
using BbTS.Domain.Models.System.Database;
using BbTS.Domain.Models.Terminal;
using BbTS.Domain.Models.Transaction;
using BbTS.Domain.Resources.Abstract;
using BbTS.Monitoring.Logging;
using Devart.Data.Oracle;

namespace BbTS.Domain.Resources.Concrete
{
    /// <summary>
    /// 
    /// </summary>
    public class ResourceOracle : Resource
    {
        private const string ConnectionStringName = "BbTS.Service.TransactionSystemInterface";

        #region Customer Functions

        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="customerGuid"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public override TsCustomer CustomerGet(string username, String customerGuid)
        {
            using(OracleConnection con = OracleInterface.Instance.OracleConnectionGet(ConnectionStringName))
            {
                using(OracleCommand cmd = new OracleCommand
                {
                    Connection = con,
                    CommandType = CommandType.Text,
                    CommandText = @"SELECT      CUS.*
                                    FROM        Envision.Customer CUS 
                                    INNER JOIN  Envision.External_Client_Customer ECC ON CUS.Cust_Id = ECC.Cust_Id
                                    WHERE       ECC.Customer_Client_Guid = :pCustomerGuid"
                })
                {

                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pCustomerGuid", OracleDbType.VarChar, customerGuid.Length, ParameterDirection.Input, customerGuid));

                    OracleInterface.Instance.OpenConnection(con);

                    using(var r = cmd.ExecuteReader(CommandBehavior.SingleRow))
                    {
                        while(r.Read())
                        {
                            return new TsCustomer
                            {
                                CustomerId = r.GetInt32("Cust_Id"),
                                CustomerNumber = r.GetString("CustNum"),
                                CustomerGuid = customerGuid,
                                DefaultCardNumber = r.GetString("DefaultCardNum"),
                                FirstName = r.GetString("FirstName"),
                                MiddleName = r.GetString("MiddleName"),
                                LastName = r.GetString("LastName"),
                                BirthDate = DateTime.FromOADate(r.GetFloat("BirthDate")),
                                Sex = r.GetString("Sex"),
                                PinNumber = r.GetInt32("PinNumber"),
                                IsActive = Core.Conversion.Formatting.TfStringToBool(r.GetString("Is_Active")),
                                ActiveStartDate = DateTime.FromOADate(r.GetFloat("Active_Start_Date")),
                                ActiveEndDate = DateTime.FromOADate(r.GetFloat("Active_End_Date")),
                                OpenDateTime = DateTime.FromOADate(r.GetFloat("OpenDateTime")),
                                LastModifiedDateTime = DateTime.FromOADate(r.GetFloat("LastMod_DateTime")),
                                LastModifiedUsername = r.GetString("LastMod_Username"),
                                DoorExtendedUnlockAllowed = Core.Conversion.Formatting.TfStringToBool(r.GetString("DoorExtendedUnlockAllowed"))
                            };
                        }
                    }
                }
            }
            throw new ArgumentException($@"Customer record not found for CustomerGuid {{{customerGuid}}}", "customerGuid");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="customer"></param>
        /// <returns></returns>
        public override ActionResultToken CustomerSet(string username, TsCustomer customer)
        {
            using(OracleConnection con = OracleInterface.Instance.OracleConnectionGet(ConnectionStringName))
            {
                try
                {
                    Int32 customerId;

                    // Insert/Update the CustomerPhoto table
                    using(OracleCommand cmd = new OracleCommand
                    {
                        Connection = con,
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "CustomerFunctions.CustomerSet"
                    })
                    {
                        cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pCustId", OracleDbType.Integer, 0, ParameterDirection.InputOutput, customer.CustomerId));
                        cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pCustNum", OracleDbType.VarChar, customer.CustomerNumber.Length, ParameterDirection.Input, customer.CustomerNumber));
                        cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pFirstName", OracleDbType.VarChar, customer.FirstName.Length, ParameterDirection.Input, customer.FirstName));
                        cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pMiddleName", OracleDbType.VarChar, customer.MiddleName.Length, ParameterDirection.Input, customer.MiddleName));
                        cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pLastName", OracleDbType.VarChar, customer.LastName.Length, ParameterDirection.Input, customer.LastName));
                        if (customer.BirthDate.HasValue)
                        {
                            cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pBirthdate", OracleDbType.Float, 0, ParameterDirection.Input, customer.BirthDate.Value.ToOADate()));
                        }
                        else
                        {
                            cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pBirthdate", OracleDbType.Float, 0, ParameterDirection.Input, DateTime.MinValue));
                        }
                        cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pSex", OracleDbType.VarChar, customer.Sex.Length, ParameterDirection.Input, customer.Sex));
                        cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pPinNumber", OracleDbType.VarChar, 0, ParameterDirection.Input, customer.PinNumber));
                        cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pIsActive", OracleDbType.VarChar, 1, ParameterDirection.Input, Core.Conversion.Formatting.BooleanToTf(customer.IsActive)));
                        if (customer.ActiveStartDate.HasValue)
                        {
                            cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pActiveStartDate", OracleDbType.Float, 0, ParameterDirection.Input, customer.ActiveStartDate.Value.ToOADate()));
                        }
                        else
                        {
                            cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pActiveStartDate", OracleDbType.Float, 0, ParameterDirection.Input, DateTime.MinValue));
                        }
                        if (customer.ActiveEndDate.HasValue)
                        { 
                            cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pActiveEndDate", OracleDbType.Float, 0, ParameterDirection.Input, customer.ActiveEndDate.Value.ToOADate()));
                        }
                        else
                        {
                            cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pActiveEndDate", OracleDbType.Float, 0, ParameterDirection.Input, DateTime.MinValue));
                        }
                        cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pOpenDatetime", OracleDbType.Float, 0, ParameterDirection.Input, customer.OpenDateTime.ToOADate()));
                        cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pLastmodUserName", OracleDbType.VarChar, customer.LastModifiedUsername.Length, ParameterDirection.Input, customer.LastModifiedUsername));
                        cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pDoorExtendedUnlockAllowed", OracleDbType.VarChar, 1, ParameterDirection.Input, Core.Conversion.Formatting.BooleanToTf(customer.DoorExtendedUnlockAllowed)));

                        try
                        {
                            OracleInterface.Instance.OpenConnection(con);
                            cmd.ExecuteNonQuery();

                            // Grab the CustomerId
                            customerId = Convert.ToInt32(cmd.Parameters["pCustId"].Value);                            
                        }
                        catch(Exception ex)
                        {
                            var resultToken = DomainCustomerDefinitions.GetActionResultCode(CustomerActionResultDomainIdCode.UnknownFailure);
                            resultToken.Message = ex.ToString();
                            return resultToken;
                        }
                        finally
                        {
                            cmd.Dispose();
                        }
                    }

                    // If there is a default card number, set it now
                    if (!String.IsNullOrEmpty(customer.DefaultCardNumber))
                    {
                        using (OracleCommand cmd = new OracleCommand
                        {
                            Connection = con,
                            CommandType = CommandType.StoredProcedure,
                            CommandText = "CustomerFunctions.CustomerDefaultCardNumSet"
                        })
                        {
                            cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pCustId", OracleDbType.Integer, 0, ParameterDirection.InputOutput, customerId));
                            cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pDefaultCardNum", OracleDbType.VarChar, customer.DefaultCardNumber.Length, ParameterDirection.Input, customer.DefaultCardNumber));
                            cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pLastmodUserName", OracleDbType.VarChar, username.Length, ParameterDirection.Input, username));

                            try
                            {
                                OracleInterface.Instance.OpenConnection(con);
                                cmd.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                var resultToken = DomainCustomerDefinitions.GetActionResultCode(CustomerActionResultDomainIdCode.UnknownFailure);
                                resultToken.Message = ex.ToString();
                                return resultToken;
                            }
                            finally
                            {
                                cmd.Dispose();
                            }
                        }
                    }

                    // Insert/Update the ExternalClientCustomer table
                    using(OracleCommand cmd = new OracleCommand
                    {
                        Connection = con,
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "ExternalClientFunctions.ExternalClientCustomerSet"
                    })
                    {
                        cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pUserName", OracleDbType.VarChar, username.Length, ParameterDirection.Input, username));
                        cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pCustId", OracleDbType.Integer, 0, ParameterDirection.InputOutput, customerId));
                        cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pCustomerClientGuid", OracleDbType.VarChar, customer.CustomerGuid.Length, ParameterDirection.Input, customer.CustomerGuid));

                        try
                        {
                            OracleInterface.Instance.OpenConnection(con);
                            cmd.ExecuteNonQuery();
                        }
                        catch(Exception ex)
                        {
                            var resultToken = DomainCustomerDefinitions.GetActionResultCode(CustomerActionResultDomainIdCode.UnknownFailure);
                            resultToken.Message = ex.ToString();
                            return resultToken;
                        }
                        finally
                        {
                            cmd.Dispose();
                        }
                    }

                    // Return the success message
                    return DomainCustomerDefinitions.GetActionResultCode(CustomerActionResultDomainIdCode.Success);
                }
                catch(Exception ex)
                {
                    var resultToken = DomainCustomerDefinitions.GetActionResultCode(CustomerActionResultDomainIdCode.UnknownFailure);
                    resultToken.Message = ex.ToString();
                    return resultToken;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public override ActionResultToken CustomerDelete(string username, int customerId)
        {
            using(OracleConnection con = OracleInterface.Instance.OracleConnectionGet(ConnectionStringName))
            {
                try
                {
                    // Insert/Update the CustomerPhoto table
                    using(OracleCommand cmd = new OracleCommand
                    {
                        Connection = con,
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "CustomerFunctions.CustomerDelete"
                    })
                    {
                        cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pCustId", OracleDbType.Integer, 0, ParameterDirection.Input, customerId));

                        try
                        {
                            OracleInterface.Instance.OpenConnection(con);
                            cmd.ExecuteNonQuery();
                        }
                        catch(Exception ex)
                        {
                            var resultToken = DomainCustomerDefinitions.GetActionResultCode(CustomerActionResultDomainIdCode.UnknownFailure);
                            resultToken.Message = ex.ToString();
                            return resultToken;
                        }
                        finally
                        {
                            cmd.Dispose();
                        }
                    }

                    // Return the success message
                    return DomainCustomerDefinitions.GetActionResultCode(CustomerActionResultDomainIdCode.Success);
                }
                catch(Exception ex)
                {
                    var resultToken = DomainCustomerDefinitions.GetActionResultCode(CustomerActionResultDomainIdCode.UnknownFailure);
                    resultToken.Message = ex.ToString();
                    return resultToken;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// This method returns a customer credential
        /// </summary>
        /// <param name="username">The user calling the function</param>
        /// <param name="customerId">The customer being looked for</param>
        /// <param name="credentialGuid">The credential Guid</param>
        /// <returns></returns>
        public override CredentialView CustomerCredentialGet(string username, int customerId, string credentialGuid)
        {
            using (OracleConnection con = OracleInterface.Instance.OracleConnectionGet(ConnectionStringName))
            {
                using (OracleCommand cmd = new OracleCommand
                {
                    Connection = con,
                    CommandType = CommandType.Text,
                    CommandText = @"SELECT      CUS.CustNum,
                                                CUS.LastName,
                                                CUS.Cust_Id, 
                                                LTRIM(CRD.CardNum,'0') AS CardNum, 
                                                CRD.Issue_Number, 
                                                DomainData.ValueGet('Card.Type',CRD.Card_Type) AS Card_Type,
                                                DomainData.ValueGet('Card Status',CAST(CRD.Card_Status AS VARCHAR2(1))) AS Card_Status,
                                                CRD.Card_Status_Text,
                                                CRD.Card_Status_DateTime,
                                                CRD.Lost_Flag,
                                                CRD.Card_Idm,
                                                CRD.DomainId,
                                                DomainData.ValueGet('Card.Issuer',CRD.IssuerId) AS IssuerId,
                                                CRD.CreatedDateTime,
                                                CRD.ModifiedDateTime,
                                                NVL(CUS.Active_End_Date,99999) AS ActiveEndDate
                                    FROM        Envision.Card CRD 
                                    INNER JOIN  Envision.Customer CUS ON CRD.Cust_Id = CUS.Cust_Id 
                                    WHERE       CRD.Cust_Id = :pCustomerId 
                                    AND         CRD.DomainId = :pCredentialGuid"
                })
                {

                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pCustomerId", OracleDbType.Integer, 0, ParameterDirection.Input, customerId));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pCredentialGuid", OracleDbType.VarChar, credentialGuid.Length, ParameterDirection.Input, credentialGuid));

                    OracleInterface.Instance.OpenConnection(con);

                    using (var r = cmd.ExecuteReader(CommandBehavior.SingleRow))
                    {
                        while (r.Read())
                        {
                            return new CredentialView
                            {
                                CustomerNumber = r.GetString("CustNum"),
                                LastName = r.GetString("LastName"),
                                CustomerId = r.GetInt32("Cust_Id"),
                                CredentialNumber = r.GetString("CardNum"),
                                IssueNumber = r.GetString("Issue_Number"),
                                CredentialType = r.GetString("Card_Type"),
                                CredentialStatus = r.GetString("Card_Status"),
                                CredentialStatusText = r.GetString("Card_Status_Text"),
                                CredentialStatusDateTime = DateTime.FromOADate(r.GetFloat("Card_Status_DateTime")),
                                CredentialLost = r.GetString("Lost_Flag"),
                                CredentialIdm = r.GetString("Card_Idm"),
                                CredentialId = r.GetString("DomainId"),
                                Issuer = r.GetString("IssuerId"),
                                CreatedDateTime = r.GetDateTime("CreatedDateTime"),
                                ModifieDateTime = r.GetDateTime("ModifiedDateTime"),
                                CredentialExpirationDateTime = DateTime.FromOADate(r.GetFloat("ActiveEndDate"))
                            };
                        }
                    }
                }
            }
            throw new ArgumentException($@"Customer credential not found for CredentialGuid {{{credentialGuid}}} and CustomerId {{{customerId}}}", "credentialGuid");
        }

        /// <summary>
        /// Set a customer credential
        /// </summary>
        /// <param name="username"></param>
        /// <param name="customerId"></param>
        /// <param name="credential"></param>
        /// <returns></returns>
        public override CustomerCredentialResponse CustomerCredentialSet(string username, int customerId, CustomerCredentialRequest credential)
        {
            using(OracleConnection con = OracleInterface.Instance.OracleConnectionGet(ConnectionStringName))
            {
                try
                {
                    // Use optimistic processing to claim the credential
                    try
                    {
                        CredentialSet(new CredentialView
                        {
                            CredentialNumber = credential.Credential,
                            IssueNumber = String.Empty,
                            CustomerId = customerId,
                            CredentialType = credential.CredentialType,
                            CredentialStatus = "Active",
                            CredentialStatusText = String.Empty,
                            CredentialStatusDateTime = DateTime.Now,
                            CredentialLost = "F",
                            CredentialIdm = String.Empty,
                            Issuer = credential.CredentialIssuerIdentifier,
                            CredentialId = credential.CredentialGuid,
                        }, con);

                        // If credential.MakePrimary is true, make the credential default
                        if (credential.MakePrimary)
                            CustomerDefaultCredentialSet(customerId,credential.Credential,credential.CredentialIssuerIdentifier,con);

                        // Return the success message
                        return new CustomerCredentialResponse
                        {
                            ActionResultToken = DomainCustomerDefinitions.GetActionResultCode(CustomerActionResultDomainIdCode.Success),
                            Credential = credential.Credential
                        };
                    }
                    catch(Exception ex)
                    {
                        if(ex.ToString().Contains("unique constraint (ENVISION.PK_CARD) violated"))
                        {
                            // If not, grab the next credential and return "credential in use"
                            return new CustomerCredentialResponse
                            {
                                ActionResultToken = DomainCustomerDefinitions.GetActionResultCode(CustomerActionResultDomainIdCode.CredentialInUse),
                                Credential = CardNextCredentialGet(credential.Credential, credential.CredentialType, con)
                            };
                        }

                        CustomerCredentialResponse response = new CustomerCredentialResponse
                        {
                            ActionResultToken = DomainCustomerDefinitions.GetActionResultCode(CustomerActionResultDomainIdCode.UnknownFailure),
                            Credential = CardNextCredentialGet(credential.Credential, credential.CredentialType, con)
                        };
                        response.ActionResultToken.Message += ex.ToString();

                        return response;
                    }
                }
                catch(Exception ex)
                {
                    CustomerCredentialResponse response = new CustomerCredentialResponse
                    {
                        ActionResultToken = DomainCustomerDefinitions.GetActionResultCode(CustomerActionResultDomainIdCode.UnknownFailure),
                        Credential = CardNextCredentialGet(credential.Credential, credential.CredentialType, con)
                    };
                    response.ActionResultToken.Message += ex.ToString();

                    return response;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        /// <summary>
        /// Get the mobile attributes for a customer
        /// </summary>
        /// <param name="username"></param>
        /// <param name="id">Customer guid</param>
        /// <returns></returns>
        public override CustomerMobileAttributes CustomerMobileAttributesGet(string username, string id)
        {
            try
            {
                var query = @"SELECT  * 
                              FROM  (
                                  SELECT      DEF.Title       AS Title,
                                              VAL.Field_Value AS FieldValue
                                  FROM        Envision.Customer_Def_Field_Def DEF
                                  INNER JOIN  Envision.Customer_Def_Field_Value VAL ON DEF.CUSTOMER_DEF_FIELD_DEF_ID = VAL.CUSTOMER_DEF_FIELD_DEF_ID
                                  WHERE       DEF.Title IN ('MobileIdProcurementAllowed','MobileIdCardType','MobileIdIINPool','MobileIdRefText')
                                  AND         VAL.Cust_Id = :pCustomerId
                              ) 
                              PIVOT (
                                  MAX(FieldValue)
                                  FOR Title IN (
                                  'MobileIdProcurementAllowed'  AS MobileIdProcurementAllowed,
                                  'MobileIdCardType'            AS MobileIdCardType,
                                  'MobileIdIINPool'             AS MobileIdIINPool,
                                  'MobileIdRefText'             AS MobileIdRefText
                                  )
                              )";
                using (OracleConnection con = OracleInterface.Instance.OracleConnectionGet(ConnectionStringName))
                {
                    using (OracleCommand cmd = new OracleCommand
                    {
                        Connection = con,
                        CommandType = CommandType.Text,
                        CommandText = query
                    })
                    {

                        cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pCustomerId", OracleDbType.Integer, 0, ParameterDirection.Input, CustomerIdGet(username, id)));

                        OracleInterface.Instance.OpenConnection(con);

                        using (var r = cmd.ExecuteReader(CommandBehavior.SingleRow))
                        {
                            while (r.Read())
                            {
                                return new CustomerMobileAttributes
                                {
                                    Allowed = Core.Conversion.Formatting.TfStringToBool(r.GetString("MobileIdProcurementAllowed")),
                                    CardType = r.GetString("MobileIdCardType"),
                                    IINPool = r.GetString("MobileIdIINPool"),
                                    ReferenceText = r.GetString("MobileIdRefText")
                                };
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Just log the error as a non-fatal error and return a valid empty object.
                StackTrace st = new StackTrace(new StackFrame(true));
                StackFrame sf = st.GetFrame(0);
                LoggingManager.Instance.LogException(
                    ex,
                    "",
                    "",
                    "ResourceOracle::CustomerMobileAttributesGet",
                    sf.GetFileName(),
                    sf.GetFileLineNumber().ToString(CultureInfo.InvariantCulture));
            }

            return new CustomerMobileAttributes
            {
                Allowed = false,
                CardType = String.Empty,
                IINPool = String.Empty,
                ReferenceText = String.Empty
            };
        }

        /// <summary>
        /// Retire a customer credential
        /// </summary>
        /// <param name="username">Name of the user currently logged into TS</param>
        /// <param name="customerId">customer guid</param>
        /// <param name="credentialGuid">credential guid</param>
        /// <returns></returns>
        public override ActionResultToken CustomerCredentialRetire(string username, string customerId, string credentialGuid)
        {
            using (OracleConnection con = OracleInterface.Instance.OracleConnectionGet(ConnectionStringName))
            {
                try
                {
                    using (OracleCommand cmd = new OracleCommand
                    {
                        Connection = con,
                        CommandType = CommandType.Text,
                        CommandText = @"UPDATE Envision.Card SET Card_Status = 2, Card_Status_DateTime = Envision.Udf_Functions.Now() WHERE Cust_Id = :pCustomerId AND DomainId = :pCredentialGuid"
                    })
                    {
                        cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pCustomerId", OracleDbType.Integer, 0, ParameterDirection.Input, CustomerIdGet(username, customerId)));
                        cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pCredentialGuid", OracleDbType.VarChar, credentialGuid.Length, ParameterDirection.Input, credentialGuid));

                        try
                        {
                            OracleInterface.Instance.OpenConnection(con);
                            var rowsModified = cmd.ExecuteNonQuery();

                            // Check here for how many rows were updated and set a "friendly error message" if the number of rows is 0
                            if (rowsModified.Equals(0))
                            {
                                return DomainCustomerDefinitions.GetActionResultCode(CustomerActionResultDomainIdCode.CredentialNotThisCustomer);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new ApplicationException("Fatal error updating Envision.Card.", ex);
                        }
                        finally
                        {
                            cmd.Dispose();
                        }
                    }
                }
                catch(Exception ex)
                {
                    ActionResultToken response = DomainCustomerDefinitions.GetActionResultCode(CustomerActionResultDomainIdCode.UnknownFailure);
                    response.Message += ex.ToString();

                    return response;
                }
                finally
                {
                    con.Close();
                }
            }

            // If the primary card was just retired, make another card primary if it exists.
            using(OracleConnection con = OracleInterface.Instance.OracleConnectionGet(ConnectionStringName))
            {
                try
                {
                    using(OracleCommand cmd = new OracleCommand
                    {
                        Connection = con,
                        CommandType = CommandType.StoredProcedure,
                        CommandText = @"CustomerFunctions.DefaultCardNumberSet"
                    })
                    {
                        cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pCustomerId", OracleDbType.Integer, 0, ParameterDirection.Input, CustomerIdGet(username, customerId)));

                        try
                        {
                            OracleInterface.Instance.OpenConnection(con);
                            cmd.ExecuteNonQuery();
                        }
                        catch(Exception ex)
                        {
                            throw new ApplicationException("Fatal error calling CustomerFunctions.DefaultCardNumberSet.", ex);
                        }
                        finally
                        {
                            cmd.Dispose();
                        }
                    }
                }
                catch(Exception ex)
                {
                    ActionResultToken response = DomainCustomerDefinitions.GetActionResultCode(CustomerActionResultDomainIdCode.UnknownFailure);
                    response.Message += ex.ToString();

                    return response;
                }
                finally
                {
                    con.Close();
                }
            }

            return DomainCustomerDefinitions.GetActionResultCode(CustomerActionResultDomainIdCode.Success);
        }

        /// <summary>
        /// This method returns a customer id based on the passed in External_Client_Customer.Customer_Client_Guid 
        /// </summary>
        /// <param name="userName">THe user calling this method</param>
        /// <param name="id">Customer_Client_Guid</param>
        /// <returns>Customer Id</returns>
        public override Int32 CustomerIdGet(string userName, string id)
        {
            using (OracleConnection con = OracleInterface.Instance.OracleConnectionGet(ConnectionStringName))
            {
                try
                {
                    using (OracleCommand cmd = new OracleCommand
                    {
                        Connection = con,
                        CommandType = CommandType.Text,
                        CommandText = @"SELECT      Cust_Id 
                                        FROM        Envision.External_Client_Customer   EC 
                                        INNER JOIN  Envision.Users                      USR ON EC.External_Client_Id = USR.ExternalClientId 
                                        WHERE       EC.Customer_Client_Guid = :pCustomerGuid 
                                        AND         USR.UserName            = :pUserName"
                    })
                    {
                        cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pCustomerGuid", OracleDbType.VarChar, id.Length, ParameterDirection.Input, id));
                        cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pUserName", OracleDbType.VarChar, userName.Length, ParameterDirection.Input, userName));

                        try
                        {
                            OracleInterface.Instance.OpenConnection(con);
                            OracleDataReader reader = cmd.ExecuteReader(CommandBehavior.SingleRow);

                            while (reader.Read())
                            {
                                return reader.GetInt32("Cust_Id");
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new ApplicationException("Fatal error validating CustomerGuid.", ex);
                        }
                        finally
                        {
                            cmd.Dispose();
                        }
                    }
                }
                catch
                {
                    // Do nothing in this case
                }
                finally
                {
                    con.Close();
                }
            }

            throw new ArgumentException($@"Customer id not found for CustomerGuid {{{id}}}", "id");
        }

        /// <summary>
        /// Set a customer photo
        /// </summary>
        /// <param name="id">Id of the customer</param>
        /// <param name="photo">new customer photo</param>
        /// <returns></returns>
        public override ActionResultToken CustomerPhotoSet(int id, byte[] photo)
        {
            using(OracleConnection con = OracleInterface.Instance.OracleConnectionGet(ConnectionStringName))
            {
                try
                {
                    // Insert/Update the CustomerPhoto table
                    using (OracleCommand cmd = new OracleCommand
                    {
                        Connection = con,
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "CustomerFunctions.PhotoSet"
                    })
                    {
                        cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pCustomerId", OracleDbType.Integer, 0, ParameterDirection.Input, id));
                        cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pPhoto", OracleDbType.Blob, photo.Length, ParameterDirection.Input, photo));

                        try
                        {
                            OracleInterface.Instance.OpenConnection(con);
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            var resultToken = DomainCustomerDefinitions.GetActionResultCode(CustomerActionResultDomainIdCode.UnknownFailure);
                            resultToken.Message = ex.ToString();
                            return resultToken;
                        }
                        finally
                        {
                            cmd.Dispose();
                        }

                        // Return the success message
                        return DomainCustomerDefinitions.GetActionResultCode(CustomerActionResultDomainIdCode.Success);
                    }
                }
                catch(Exception ex)
                {
                    var resultToken = DomainCustomerDefinitions.GetActionResultCode(CustomerActionResultDomainIdCode.UnknownFailure);
                    resultToken.Message = ex.ToString();
                    return resultToken;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        #region Card Functions

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <exception cref="ApplicationException"></exception>
        public static void CardDelete(String id)
        {
            // Clean up the record
            using(OracleConnection con = OracleInterface.Instance.OracleConnectionGet(ConnectionStringName))
            {
                try
                {
                    using(OracleCommand cmd = new OracleCommand
                    {
                        Connection = con,
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "Envision.CardFunctions.CardDelete"
                    })
                    {
                        cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pCardNum", OracleDbType.VarChar, 22, ParameterDirection.Input, id.PadLeft(22,'0')));

                        try
                        {
                            OracleInterface.Instance.OpenConnection(con);
                            cmd.ExecuteNonQuery();
                        }
                        catch(Exception ex)
                        {
                            throw new ApplicationException("Fatal error in CardDelete.", ex);
                        }
                        finally
                        {
                            cmd.Dispose();
                        }
                    }
                }
                finally
                {
                    con.Close();
                }
            }
        }

        private static String CardNextCredentialGet(String id, String credentialType, OracleConnection con)
        {
            using(OracleCommand cmd = new OracleCommand
            {
                Connection = con,
                CommandType = CommandType.StoredProcedure,
                CommandText = "Envision.CardFunctions.CardNextNumberGet"
            })
            {
                cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pCardNumber", OracleDbType.VarChar, id.Length, ParameterDirection.Input, id));
                cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pCardType", OracleDbType.VarChar, credentialType.Length, ParameterDirection.Input, credentialType));
                cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pNextValue", OracleDbType.VarChar, 22, ParameterDirection.Output, String.Empty));

                try
                {
                    OracleInterface.Instance.OpenConnection(con);
                    cmd.ExecuteNonQuery();

                    return cmd.Parameters["pNextValue"].Value.ToString();
                }
                catch(Exception ex)
                {
                    throw new ApplicationException("Fatal error validating CardNextCredentialGet.", ex);
                }
                finally
                {
                    cmd.Dispose();
                }
            }
        }

        private static void CredentialSet(CredentialView credential, OracleConnection con)
        {
            using(OracleCommand cmd = new OracleCommand
            {
                Connection = con,
                CommandType = CommandType.Text,
                CommandText =
                @"INSERT INTO Envision.Card ( 
                    CardNum,Issue_Number,Cust_Id,Card_Type,Card_Status,Card_Status_Text,Card_Status_DateTime,Lost_Flag,Card_Idm,DomainId,IssuerId
                ) VALUES (
                    :pCardNumber,
                    :pIssueNumber,
                    :pCustomerId,
                    DomainData.IdGet('Card.Type',:pCardType),
                    DomainData.NameFromValueGet('Card Status',:pCardStatus),
                    :pCardStatusText,
                    :pCardStatusDateTime,
                    :pLostFlag,
                    :pCardIdm,
                    :pDomainId,
                    DomainData.IdGet('Card.Issuer',:pIssuerId)
                )"
            })
            {
                cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pCardNumber", OracleDbType.VarChar, 22, ParameterDirection.Input, credential.CredentialNumber.PadLeft(22,'0')));
                cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pIssueNumber", OracleDbType.VarChar, credential.IssueNumber.Length, ParameterDirection.Input, credential.IssueNumber));
                cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pCustomerId", OracleDbType.Integer, 0, ParameterDirection.Input, credential.CustomerId));
                cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pCardType", OracleDbType.VarChar, credential.CredentialType.Length, ParameterDirection.Input, credential.CredentialType));
                cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pCardStatus", OracleDbType.VarChar, credential.CredentialStatus.Length, ParameterDirection.Input, credential.CredentialStatus));
                cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pCardStatusText", OracleDbType.VarChar, credential.CredentialStatusText.Length, ParameterDirection.Input, credential.CredentialStatusText));
                cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pCardStatusDateTime", OracleDbType.Float, 0, ParameterDirection.Input, credential.CredentialStatusDateTime.ToOADate()));
                cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pLostFlag", OracleDbType.VarChar, credential.CredentialLost.Length, ParameterDirection.Input, credential.CredentialLost));
                cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pCardIdm", OracleDbType.VarChar, credential.CredentialIdm.Length, ParameterDirection.Input, credential.CredentialIdm));
                cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pDomainId", OracleDbType.VarChar, credential.CredentialId.Length, ParameterDirection.Input, credential.CredentialId));
                cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pIssuerId", OracleDbType.VarChar, credential.Issuer.Length, ParameterDirection.Input, credential.Issuer));

                try
                {
                    OracleInterface.Instance.OpenConnection(con);
                    cmd.ExecuteNonQuery();
                }
                catch(Exception ex)
                {
                    throw new ApplicationException("Fatal error inserting into Envision.Card.", ex);
                }
                finally
                {
                    cmd.Dispose();
                }
            }
        }

        #endregion

        #region Customer Functions

        /// <summary>
        /// This method will establish the default credential for a given customer
        /// </summary>
        /// <param name="customerId">The identity of the customer</param>
        /// <param name="credential">The credential that will be the default for a customer</param>
        /// <param name="modifiedBy">The identity of the system making this call</param>
        /// <param name="con">The database connection object.</param>
        public static void CustomerDefaultCredentialSet(Int32 customerId, String credential, String modifiedBy, OracleConnection con)
        {
            using(OracleCommand cmd = new OracleCommand
            {
                Connection = con,
                CommandType = CommandType.Text,
                CommandText = "UPDATE Envision.Customer SET DefaultCardNum = :pCredential, LastMod_DateTime = Udf_Functions.Now, LastMod_UserName = :pModifiedBy WHERE Cust_Id = :pCustomerId"
            })
            {
                cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pCustomerId", OracleDbType.Integer, 0, ParameterDirection.Input, customerId));
                cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pCredential", OracleDbType.VarChar, 22, ParameterDirection.Input, credential.PadLeft(22,'0')));
                cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pModifiedBy", OracleDbType.VarChar, modifiedBy.Length, ParameterDirection.Input, modifiedBy));

                try
                {
                    OracleInterface.Instance.OpenConnection(con);
                    cmd.ExecuteNonQuery();
                }
                catch(Exception ex)
                {
                    throw new ApplicationException("Fatal error calling CustomerDefaultCardNumberSet.", ex);
                }
                finally
                {
                    cmd.Dispose();
                }
            }
        }

        #endregion

        #endregion

        #region System Functions

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override ActionResultToken PingGet()
        {
            // Start by assuming the worst
            ActionResultToken token = new ActionResultToken { ResultDomain = (int)DomainValue.DomainTransactApi, ResultDomainId = (int)ConnectionTest.Failure, Id = Guid.NewGuid().ToString(), Message = "Unable to connect." };

            using(OracleConnection con = OracleInterface.Instance.OracleConnectionGet(ConnectionStringName))
            {
                using(OracleCommand cmd = new OracleCommand 
                {
                    Connection = con, 
                    CommandType = CommandType.Text,
                    CommandText = @"SELECT 'Ping' FROM DUAL"
                })
                {
                    OracleInterface.Instance.OpenConnection(con);

                    using(var r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            token.Message = r.GetString(0);
                            token.ResultDomainId = (int)ConnectionTest.Success;
                        }
                    }
                }
            }
            return token;
        }

        /// <summary>
        /// Get the current timezone information from the Oracle layer.
        /// </summary>
        /// <returns></returns>
        public override TerminalTimeZoneInformation TimeZoneInformationGet()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get the transact api uri from the Oracle layer.
        /// </summary>
        /// <returns></returns>
        public override string TransactApiUriGet()
        {
            using (OracleConnection con = OracleInterface.Instance.OracleConnectionGet(ConnectionStringName))
            {

                using (OracleCommand cmd = new OracleCommand
                {
                    Connection = con,
                    CommandType = CommandType.Text,
                    CommandText = @"SELECT ParameterValue FROM ControlParameter WHERE ParameterName='TransactApiUri'"
                })
                {
                    OracleInterface.Instance.OpenConnection(con);

                    using (var r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            return r.GetString("ParameterValue");
                        }
                    }
                }
            }
            throw new ArgumentException("TransactApiUri parameter not found in the ControlParameter table.");
        }

        /// <summary>
        /// Fetch the date and time from the Oracle instance
        /// </summary>
        /// <returns></returns>
        public override DateTime DataResourceDateTimeGet()
        {
            using (OracleConnection con = OracleInterface.Instance.OracleConnectionGet(ConnectionStringName))
            {
                using (OracleCommand cmd = new OracleCommand
                {
                    Connection = con,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "Envision.Udf_Functions.DateTimeInfoUtcGet"
                })
                {
                    cmd.Parameters.Add(new OracleParameter { ParameterName = "pDocument", OracleDbType = OracleDbType.Clob, Direction = ParameterDirection.Output });

                    try
                    {
                        OracleInterface.Instance.OpenConnection(con);
                        cmd.ExecuteNonQuery();

                        string xml = cmd.Parameters[0].Value.ToString();

                        var dateTimeInfoUtc = Core.Serialization.Xml.Deserialize<DateTimeInfoUtc>(xml);
                        var databaseTimeUtc = new DateTime(dateTimeInfoUtc.Year, dateTimeInfoUtc.Month, dateTimeInfoUtc.Day, dateTimeInfoUtc.Hour, dateTimeInfoUtc.Minutes, (int)dateTimeInfoUtc.Seconds, DateTimeKind.Utc);

                        var appServerTime = DateTimeOffset.Now;
                        if (TimeSpan.TryParse(dateTimeInfoUtc.DatabaseTimezone, out var databaseOffset))
                        {
                            if (databaseOffset != appServerTime.Offset)
                            {
                                LoggingManager.Instance.LogMessage($"Database timezone offset ({databaseOffset}) does not match application server timezone offset ({appServerTime.Offset})");
                            }
                        }

                        var appServerTimeUtc = appServerTime.ToUniversalTime();
                        if (Math.Abs(databaseTimeUtc.Ticks - appServerTimeUtc.Ticks) > 300000000)
                        {
                            LoggingManager.Instance.LogMessage($"Database time differs from application server time by more than 30 seconds (Database = {databaseTimeUtc}, Application Server = {appServerTimeUtc})");
                        }

                        return databaseTimeUtc.ToLocalTime();
                    }
                    finally
                    {
                        cmd.Dispose();
                    }
                }
            }
        }

        #endregion

        #region Transaction Functions

        /// <summary>
        /// Get a list of all credit card transactions that are configured on the Bb Payment Gateway and
        /// are marked as unprocessed (i.e. not voided, etc.)
        /// </summary>
        /// <returns></returns>
        public override List<CreditCardTransactionView> TransactionCreditCardUnprocessedGet()
        {
            List<CreditCardTransactionView> creditCardTransactionList = new List<CreditCardTransactionView>();
            using(OracleConnection con = OracleInterface.Instance.OracleConnectionGet(ConnectionStringName))
            {
                using(OracleCommand cmd = new OracleCommand 
                {
                    Connection = con, 
                    CommandType = CommandType.Text,
                    CommandText = $@"SELECT * FROM Envision.BbPaygateTransactionView WHERE TransactionDateTime > {(DateTime.Now.ToOADate() - 7)}"
                })
                {
                    OracleInterface.Instance.OpenConnection(con);

                    using(var r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            creditCardTransactionList.Add(new CreditCardTransactionView
                            {
                                BbPaygateTransactionId = r.GetInt32(0),
                                BbPaygateOrderId = r.GetString(1),
                                TransactionDateTime = r.GetFloat(2),
                                BbPaygateTransactionTypeId = r.GetInt32(3),
                                BbPaygateTransactionTypeName = r.GetString(4),
                                BbPaygateTransctnDatasrcId = r.GetInt32(5),
                                BbPaygateTransctnDatasrcName = r.GetString(6),
                                SignatureCaptured = r.GetString(7).Equals("T"),
                                Amount = Convert.ToDouble(r.GetInt32(8)) / 1000,
                                Authcode = r.GetString(9),
                                BbPaygateTransactnCommitId = r.GetInt32(10),
                                BbPaygateTransactnCommitName = r.GetString(11),
                                BbPaygateTransactnStateId = r.GetInt32(12),
                                BbPaygateTransactnStateName = r.GetString(13),
                                VoidStateId = r.GetInt32(14),
                                VoidStateName = r.GetString(15),
                                VoidDateTime = r.GetInt32(16),
                                CreditCardIin = r.GetInt32(17),
                                CreditCardLast4 = r.GetInt32(18),
                                ExternalTransactionId = r.GetString(19),
                                BbPaygateConfigStoreId = r.GetInt32(20),
                                BbPaygateConfigStoreName = r.GetString(21),
                                BbPaygateCardholderPrsntId = r.GetInt32(22),
                                BbPaygateCardholderPrsntName = r.GetString(23),
                                BbPaygateTrnsctnTermCapId = r.GetInt32(24),
                                BbPaygateTrnsctnTermCapName = r.GetString(25),
                                BbPaygateTrnscnInputEnvId = r.GetInt32(26),
                                BbPaygateTrnscnInputEnvName = r.GetString(27),
                                PosId = r[29] != null ? r.GetInt32(29) : new int?(),
                                PosName = Convert.ToString(r[30])
                            });
                        }
                    }
                }
            }
            return creditCardTransactionList;
        }

        #endregion

        #region Terminal Functions

        /// <summary>
        /// Get a list of POS devices that are configured with Bb Payment Gateway processing.
        /// </summary>
        /// <returns></returns>
        public override List<Pos> PosBbPaygateConfiguredGet()
        {
            List<Pos> terminalList = new List<Pos>();
            using(OracleConnection con = OracleInterface.Instance.OracleConnectionGet(ConnectionStringName))
            {
                using(OracleCommand cmd = new OracleCommand
                {
                    Connection = con,
                    CommandType = CommandType.Text,
                    CommandText = @"SELECT * FROM Envision.PosCreditCardList WHERE CreditCardProcessingMethodId = 1"
                })
                {
                    OracleInterface.Instance.OpenConnection(con);

                    using(var r = cmd.ExecuteReader())
                    {
                        while(r.Read())
                        {
                            terminalList.Add(new Pos
                            {
                                PosId = r.GetInt32(0),
                                Name = r.GetString(1),
                                PosType = (PosType)Enum.Parse(typeof(PosType), r.GetInt32(2).ToString()),
                                MacAddress = r.GetString(3),
                                ProfitCenterName = r.GetString(4),
                                MerchantName = r.GetString(5),
                                DeviceGroup = r.GetString(6)
                            });
                        }
                    }
                }
            }
            return terminalList;
        }

        /// <summary>
        /// Get a list of EMV reconciliation transactions.
        /// </summary>
        /// <param name="userName">The name of the user making the call.</param>
        /// <param name="id">transaction to fetch; null for all transactions</param>
        /// <returns></returns>
        public override List<PaymentExpressRequestResponseView> EmvReconciliationTransactionGet(string userName, string id)
        {
            List<PaymentExpressRequestResponseView> unreconciledList = new List<PaymentExpressRequestResponseView>();
            using(OracleConnection con = OracleInterface.Instance.OracleConnectionGet(ConnectionStringName))
            {
                using(OracleCommand cmd = new OracleCommand
                {
                    Connection = con,
                    CommandType = CommandType.Text,
                    CommandText = $"SELECT * FROM PaymentExpressReqRespView WHERE PaymentExpress.UserInGroupAccountRoleGroup(pUserName => '{userName}', pPaymentExpressGroupAccountId => PaymentExpressGroupAccountId ) = 1 AND {(string.IsNullOrEmpty(id) ? "ReconciliationStateId = 1 AND InProcess = 0 ORDER BY PaymentExpressReqRespId" : $"PaymentExpressReqRespId = {id}")}"
                })
                {
                    OracleInterface.Instance.OpenConnection(con);

                    using(var r = cmd.ExecuteReader())
                    {
                        while(r.Read())
                        {
                            unreconciledList.Add(new PaymentExpressRequestResponseView
                            {
                                PaymentExpressReqRespId = r.GetInt64("PaymentExpressReqRespId"),
                                TransactionReference = r.GetString("TransactionReference"),
                                TerminalId = r.GetInt32("TerminalId"),
                                TerminalName = r.GetString("TerminalName"),
                                MerchantReference = r.GetString("MerchantReference"),
                                PaymentExpressGroupAccountId = r.GetInt64("PaymentExpressGroupAccountId"),
                                PaymentExpressGroupAccountName = r.GetString("PaymentExpressGroupAccountName"),
                                EmvDeviceId = r.GetString("EmvDeviceId"),
                                AmountRequested = r.GetDecimal("AmountRequested"),
                                AmountAuthorized = r.GetDecimal("AmountAuthorized"),
                                PurchaseResponseCode = r.GetString("PurchaseResponseCode"),
                                PurchaseResponseCodeValue = r.GetString("PurchaseResponseCodeValue"),
                                DpsTransactionReference = r.GetString("DpsTransactionReference"),
                                MaskedPan = r.GetString("MaskedPan"),
                                CardSuffix = r.GetString("CardSuffix"),
                                CardId = r.GetInt32("CardId"),
                                CardName = r.GetString("CardName"),
                                CommandSequence = r.GetInt32("CommandSequence"),
                                TransactionStateId = r.GetInt32("TransactionStateId"),
                                TransactionStateName = r.GetString("TransactionStateName"),
                                Stan = r.GetInt32("Stan"),
                                SettlementDate = r.GetDateTime("SettlementDate"),
                                AuthorizationCode = r.GetString("AuthorizationCode"),
                                RequestDateTime = r.GetDateTime("RequestDateTime"),
                                ResponseDateTime = r.GetDateTime("ResponseDateTime"),
                                TransactionResponseCode = r.GetString("TransactionResponseCode"),
                                TransactionResponseValue = r.GetString("TransactionResponseValue"),
                                TransactionTypeId = r.GetInt32("TransactionTypeId"),
                                TransactionTypeName = r.GetString("TransactionTypeName"),
                                RespActionResultTokenDomain = r.GetInt32("RespActionResultTokenDomain"),
                                RespActionResultTokenDomainId = r.GetInt32("RespActionResultTokenDomainId"),
                                RespActionResultTokenId = r.GetString("RespActionResultTokenId"),
                                RespActionResultTokenMessage = r.GetString("RespActionResultTokenMessage"),
                                Get1DateTime = r.GetDateTime("Get1DateTime"),
                                Get1ActionResultTokenDomain = r.GetInt32("Get1ActionResultTokenDomain"),
                                Get1ActionResultTokenDomainId = r.GetInt32("Get1ActionResultTokenDomainId"),
                                Get1ActionResultTokenId = r.GetString("Get1ActionResultTokenId"),
                                Get1ActionResultTokenMessage = r.GetString("Get1ActionResultTokenMessage"),
                                VoidResponseCode = r.GetString("VoidResponseCode"),
                                VoidResponseValue = r.GetString("VoidResponseValue"),
                                VoidRequestDateTime = r.GetDateTime("VoidRequestDateTime"),
                                VoidResponseDateTime = r.GetDateTime("VoidResponseDateTime"),
                                VoidActionResultTokenDomain = r.GetInt32("VoidActionResultTokenDomain"),
                                VoidActionResultTokenDomainId = r.GetInt32("VoidActionResultTokenDomainId"),
                                VoidActionResultTokenId = r.GetString("VoidActionResultTokenId"),
                                VoidActionResultTokenMessage = r.GetString("VoidActionResultTokenMessage"),
                                TransactionId = r.GetInt64("TransactionId"),
                                InProcess = r.GetInt32("InProcess"),
                                ReconciliationStateId = r.GetInt32("ReconciliationStateId"),
                                ReconciliationStateName = r.GetString("ReconciliationStateName")
                            });
                        }
                    }
                }
            }
            return unreconciledList;

        }

        /// <summary>
        /// Set a specific EMV reconciliation transaction
        /// </summary>
        /// <param name="value"></param>
        public override void EmvReconciliationTransactionSet(PaymentExpressRequestResponseView value)
        {
            EmvTxnVoidRequestSet(
                value.TerminalId, 
                new BbTxnVoidRequestLog 
                {   TxnRef=value.TransactionReference, 
                    TimeStamp = value.VoidRequestDateTime
                }
            );

            EmvTxnVoidResponseSet(
                value.TerminalId, 
                new BbTxnVoidResponseLog
                {
                    TxnRef = value.TransactionReference, 
                    ReCo = value.VoidResponseCode,
                    TimeStamp = value.VoidResponseDateTime,
                    Result = new ActionResultToken
                    {
                        ResultDomain = value.VoidActionResultTokenDomain,
                        ResultDomainId = value.VoidActionResultTokenDomainId,
                        Id = value.VoidActionResultTokenId,
                        Message = value.VoidActionResultTokenMessage
                    }
                }
            );
        }

        /// <summary>
        /// Get the EMV terminal settings from oracle
        /// </summary>
        /// <returns></returns>
        public override EmvSettings EmvSettingsGet(int terminalId)
        {
            EmvSettings settings = new EmvSettings();
            using (OracleConnection con = OracleInterface.Instance.OracleConnectionGet(ConnectionStringName))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "SystemFunctions.EmvGatewaySettingsGet", CommandType = CommandType.StoredProcedure })
                {
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pReturnAll", OracleDbType.VarChar, 0, ParameterDirection.Input, "0"));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pTerminalId", OracleDbType.Integer, 0, ParameterDirection.Input, terminalId));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pList", OracleDbType.Cursor, 0, ParameterDirection.Output));
                    OracleInterface.Instance.OpenConnection(con);
                    using (OracleDataReader r = cmd.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            settings.Identity = r.GetInt32("Identity");
                            settings.Name = r.GetString("Name");
                            settings.Address = r.GetString("Address");
                            settings.Type = r.GetString("Type");
                            settings.Port = r.GetString("Port");
                            settings.Priority = r.GetString("Priority");
                            settings.Enabled = r.GetInt32("Enabled");
                            settings.CurrencyCode = r.GetString("CurrencyCode");
                            settings.DeviceId = r.GetString("DeviceId");
                            settings.VendorId = r.GetString("VendorId");
                            settings.IdleDisconnectTimeout = r.GetInt32("IdleDisconnectTimeout");
                        }
                    }
                }
            }
            return settings;
        }

        /// <summary>
        /// Persist EMV TxnPur request messages to the data layer
        /// </summary>
        /// <param name="terminalId"></param>
        /// <param name="request"></param>
        public override void EmvTxnPurRequestSet(int terminalId, BbTxnPurRequestLog request)
        {
            using (OracleConnection con = OracleInterface.Instance.OracleConnectionGet(ConnectionStringName))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "PaymentExpress.PurchaseRequest", CommandType = CommandType.StoredProcedure })
                {
                    string txnref = string.IsNullOrEmpty(request.TxnRef) ? string.Empty : request.TxnRef;
                    string amountRequested = string.IsNullOrEmpty(request.Amount) ? string.Empty : request.Amount;
                    string merchantRef = string.IsNullOrEmpty(request.MerchantReference) ? string.Empty : request.MerchantReference;
                    string deviceId = string.IsNullOrEmpty(request.DeviceId) ? string.Empty : request.DeviceId;

                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pTransactionReference", OracleDbType.VarChar, txnref.Length, ParameterDirection.Input, txnref));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pAmountRequested", OracleDbType.Number, 0, ParameterDirection.Input, amountRequested));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pMerchantReference", OracleDbType.VarChar, merchantRef.Length, ParameterDirection.Input, merchantRef));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pTerminalId", OracleDbType.Integer, 0, ParameterDirection.Input, terminalId));
                    cmd.Parameters.Add(new OracleParameter("pRequestDateTime", OracleDbType.TimeStamp, 0, request.TimeStamp.ToLocalTime(), ParameterDirection.Input));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pDeviceId", OracleDbType.VarChar, deviceId.Length, ParameterDirection.Input, deviceId));
                    OracleInterface.Instance.OpenConnection(con);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Persist EMV TxnPur response messages to the data layer
        /// </summary>
        /// <param name="terminalId"></param>
        /// <param name="response"></param>
        public override void EmvTxnPurResponseSet(int terminalId, BbTxnPurResponseLog response)
        {
            using (OracleConnection con = OracleInterface.Instance.OracleConnectionGet(ConnectionStringName))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "PaymentExpress.PurchaseResponse", CommandType = CommandType.StoredProcedure })
                {
                    string txnref = string.IsNullOrEmpty(response.TxnRef) ? string.Empty : response.TxnRef;
                    string reco = string.IsNullOrEmpty(response.ReCo) ? string.Empty : response.ReCo;
                    string amountRequested = string.IsNullOrEmpty(response.Amount) ? string.Empty : response.Amount;
                    string dpsTxnRef = string.IsNullOrEmpty(response.DpsTxnRef) ? string.Empty : response.DpsTxnRef;

                    string resultId = String.IsNullOrEmpty(response.Result.Id) ? string.Empty : response.Result.Id;
                    string resultMessage = String.IsNullOrEmpty(response.Result.Message) ? string.Empty : response.Result.Message;

                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pTransactionReference", OracleDbType.VarChar, txnref.Length, ParameterDirection.Input, txnref));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pPurchaseResponseCode", OracleDbType.VarChar, reco.Length, ParameterDirection.Input, reco));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pAmountRequested", OracleDbType.Number, 0, ParameterDirection.Input, amountRequested));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pDpsTransactionReference", OracleDbType.VarChar, dpsTxnRef.Length, ParameterDirection.Input, dpsTxnRef));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pTerminalId", OracleDbType.Integer, 0, ParameterDirection.Input, terminalId));
                    cmd.Parameters.Add(new OracleParameter("pResponseDateTime", OracleDbType.TimeStamp, 0, response.TimeStamp.ToLocalTime(), ParameterDirection.Input));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pRespActionResultTokenDomain", OracleDbType.Number, 0, ParameterDirection.Input, response.Result.ResultDomain));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pRespActionResultTokenDomainId", OracleDbType.Number, 0, ParameterDirection.Input, response.Result.ResultDomainId));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pRespActionResultTokenId", OracleDbType.VarChar, resultId.Length, ParameterDirection.Input, resultId));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pRespActionResultTokenMessage", OracleDbType.VarChar, resultMessage.Length, ParameterDirection.Input, resultMessage));
                    
                    OracleInterface.Instance.OpenConnection(con);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Persist EMV Get1 response messages to the data layer
        /// </summary>
        /// <param name="terminalId"></param>
        /// <param name="response"></param>
        public override void EmvTxnGet1ResponseSet(int terminalId, BbTxnGet1ResponseLog response)
        {
            using (OracleConnection con = OracleInterface.Instance.OracleConnectionGet(ConnectionStringName))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "PaymentExpress.PurchaseComplete", CommandType = CommandType.StoredProcedure })
                {
                    DateTime settlementDateTime;
                    DateTime.TryParseExact(response.SettlementDate, "yyyyMMdd", new CultureInfo("en-US"), DateTimeStyles.AssumeUniversal, out settlementDateTime);

                    DateTime transactionDateTime;
                    DateTime.TryParseExact(response.TxnTime, "yyyyMMddHHmmss", new CultureInfo("en-US"), DateTimeStyles.AssumeUniversal, out transactionDateTime);

                    string txnref = string.IsNullOrEmpty(response.TxnRef) ? string.Empty : response.TxnRef;
                    string cmdseq = string.IsNullOrEmpty(response.CmdSeq) ? string.Empty : response.CmdSeq;
                    string reco = string.IsNullOrEmpty(response.ReCo) ? string.Empty : response.ReCo;
                    string cardSuffix = string.IsNullOrEmpty(response.CardSuffix) ? string.Empty : response.CardSuffix;
                    string cardId = string.IsNullOrEmpty(response.CardId) ? string.Empty : response.CardId;
                    string stateId = string.IsNullOrEmpty(response.TxnState) ? string.Empty : response.TxnState;
                    string stan = string.IsNullOrEmpty(response.Stan) ? string.Empty : response.Stan;
                    string amountRequested = string.IsNullOrEmpty(response.AmountRequested) ? string.Empty : response.AmountRequested;
                    string amountAuthorized = string.IsNullOrEmpty(response.AmountAuthorized) ? string.Empty : response.AmountAuthorized;
                    string authCode = string.IsNullOrEmpty(response.AuthCode) ? string.Empty : response.AuthCode;
                    string dpsTxnRef = string.IsNullOrEmpty(response.DpsTxnRef) ? string.Empty : response.DpsTxnRef;
                    string txnReCo = string.IsNullOrEmpty(response.TxnReCo) ? string.Empty : response.TxnReCo;
                    string merchantRef = string.IsNullOrEmpty(response.MerchantReference) ? string.Empty : response.MerchantReference;
                    string txnType = string.IsNullOrEmpty(response.TxnType) ? string.Empty : response.TxnType;
                    string maskedPan = string.IsNullOrEmpty(response.MaskedPan) ? string.Empty : response.MaskedPan;

                    string resultId = String.IsNullOrEmpty(response.Result.Id) ? string.Empty : response.Result.Id;
                    string resultMessage = String.IsNullOrEmpty(response.Result.Message) ? string.Empty : response.Result.Message;

                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pTransactionReference", OracleDbType.VarChar, txnref.Length, ParameterDirection.Input, txnref));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pCommandSequence", OracleDbType.VarChar, cmdseq.Length, ParameterDirection.Input, cmdseq));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pPurchaseResponseCode", OracleDbType.VarChar, reco.Length, ParameterDirection.Input, reco));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pCardSuffix", OracleDbType.VarChar, cardSuffix.Length, ParameterDirection.Input, cardSuffix));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pCardId", OracleDbType.VarChar, cardId.Length, ParameterDirection.Input, cardId));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pAmountRequested", OracleDbType.Number, 0, ParameterDirection.Input, amountRequested));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pAmountAuthorized", OracleDbType.Number, 0, ParameterDirection.Input, amountAuthorized));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pTransactionStateId", OracleDbType.VarChar, stateId.Length, ParameterDirection.Input, stateId));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pStan", OracleDbType.VarChar, stan.Length, ParameterDirection.Input, stan));

                    cmd.Parameters.Add(new OracleParameter("pSettlementDate", OracleDbType.TimeStamp, 0, settlementDateTime.ToLocalTime(), ParameterDirection.Input));

                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pAuthorizationCode", OracleDbType.VarChar, authCode.Length, ParameterDirection.Input, authCode));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pDpsTransactionReference", OracleDbType.VarChar, dpsTxnRef.Length, ParameterDirection.Input, dpsTxnRef));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pTransactionResponseCode", OracleDbType.VarChar, txnReCo.Length, ParameterDirection.Input, txnReCo));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pMerchantReference", OracleDbType.VarChar, merchantRef.Length, ParameterDirection.Input, merchantRef));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pTransactionType", OracleDbType.VarChar, txnType.Length, ParameterDirection.Input, txnType));

                    cmd.Parameters.Add(new OracleParameter("pTransactionTime", OracleDbType.TimeStamp, 0, transactionDateTime.ToLocalTime(), ParameterDirection.Input));

                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pMaskedPan", OracleDbType.VarChar, maskedPan.Length, ParameterDirection.Input, maskedPan));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pTerminalId", OracleDbType.Integer, 0, ParameterDirection.Input, terminalId));

                    cmd.Parameters.Add(new OracleParameter("pGet1DateTime", OracleDbType.TimeStamp, 0, response.TimeStamp.ToLocalTime(), ParameterDirection.Input));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pGet1ActionResultTokenDomain", OracleDbType.Number, 0, ParameterDirection.Input, response.Result.ResultDomain));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pGet1ActionResultTokenDomainId", OracleDbType.Number, 0, ParameterDirection.Input, response.Result.ResultDomainId));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pGet1ActionResultTokenId", OracleDbType.VarChar, resultId.Length, ParameterDirection.Input, resultId));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pGet1ActionResultTokenMessage", OracleDbType.VarChar, resultMessage.Length, ParameterDirection.Input, resultMessage));
                    OracleInterface.Instance.OpenConnection(con);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Persist EMV void request messages to the data layer
        /// </summary>
        /// <param name="terminalId"></param>
        /// <param name="request"></param>
        public override void EmvTxnVoidRequestSet(int terminalId, BbTxnVoidRequestLog request)
        {
            using (OracleConnection con = OracleInterface.Instance.OracleConnectionGet(ConnectionStringName))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "PaymentExpress.VoidRequest", CommandType = CommandType.StoredProcedure })
                {
                    string txnref = string.IsNullOrEmpty(request.TxnRef) ? string.Empty : request.TxnRef;

                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pTransactionReference", OracleDbType.VarChar, txnref.Length, ParameterDirection.Input, txnref));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pTerminalId", OracleDbType.Integer, 0, ParameterDirection.Input, terminalId));
                    cmd.Parameters.Add(new OracleParameter("pVoidRequestDateTime", OracleDbType.TimeStamp, 0, request.TimeStamp.ToLocalTime(), ParameterDirection.Input));
                    OracleInterface.Instance.OpenConnection(con);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Persist EMV void response messages to the data layer
        /// </summary>
        /// <param name="terminalId"></param>
        /// <param name="response"></param>
        public override void EmvTxnVoidResponseSet(int terminalId, BbTxnVoidResponseLog response)
        {
            using (OracleConnection con = OracleInterface.Instance.OracleConnectionGet(ConnectionStringName))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "PaymentExpress.VoidResponse", CommandType = CommandType.StoredProcedure })
                {

                    string txnref = string.IsNullOrEmpty(response.TxnRef) ? string.Empty : response.TxnRef;
                    string reco = string.IsNullOrEmpty(response.ReCo) ? string.Empty : response.ReCo;

                    string resultId = String.IsNullOrEmpty(response.Result.Id) ? string.Empty : response.Result.Id;
                    string resultMessage = String.IsNullOrEmpty(response.Result.Message) ? string.Empty : response.Result.Message;

                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pTransactionReference", OracleDbType.VarChar, txnref.Length, ParameterDirection.Input, txnref));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pVoidResponseCode", OracleDbType.VarChar, reco.Length, ParameterDirection.Input, reco));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pTerminalId", OracleDbType.Integer, 0, ParameterDirection.Input, terminalId));

                    cmd.Parameters.Add(new OracleParameter("pVoidResponseDateTime", OracleDbType.TimeStamp, 0, response.TimeStamp.ToLocalTime(), ParameterDirection.Input));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pVoidActionResultTokenDomain", OracleDbType.Number, 0, ParameterDirection.Input, response.Result.ResultDomain));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pVoidActionResultTokenDomainId", OracleDbType.Number, 0, ParameterDirection.Input, response.Result.ResultDomainId));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pVoidActionResultTokenId", OracleDbType.VarChar, resultId.Length, ParameterDirection.Input, resultId));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pVoidActionResultTokenMessage", OracleDbType.VarChar, resultMessage.Length, ParameterDirection.Input, resultMessage));
                    OracleInterface.Instance.OpenConnection(con);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Refund request
        /// </summary>
        /// <param name="terminalId"></param>
        /// <param name="request"></param>
        public override void EmvTxnRefRequestSet(int terminalId, BbTxnRefRequestLog request)
        {
            using (OracleConnection con = OracleInterface.Instance.OracleConnectionGet(ConnectionStringName))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "PaymentExpress.RefundRequest", CommandType = CommandType.StoredProcedure })
                {
                    string txnref = string.IsNullOrEmpty(request.TxnRef) ? string.Empty : request.TxnRef;
                    string amountRequested = string.IsNullOrEmpty(request.Amount) ? string.Empty : request.Amount;
                    string merchantRef = string.IsNullOrEmpty(request.MerchantReference) ? string.Empty : request.MerchantReference;
                    string deviceId = string.IsNullOrEmpty(request.DeviceId) ? string.Empty : request.DeviceId;

                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pTransactionReference", OracleDbType.VarChar, txnref.Length, ParameterDirection.Input, txnref));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pAmountRequested", OracleDbType.Number, 0, ParameterDirection.Input, amountRequested));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pMerchantReference", OracleDbType.VarChar, merchantRef.Length, ParameterDirection.Input, merchantRef));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pTerminalId", OracleDbType.Integer, 0, ParameterDirection.Input, terminalId));
                    cmd.Parameters.Add(new OracleParameter("pRequestDateTime", OracleDbType.TimeStamp, 0, request.TimeStamp.ToLocalTime(), ParameterDirection.Input));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pDeviceId", OracleDbType.VarChar, deviceId.Length, ParameterDirection.Input, deviceId));
                    OracleInterface.Instance.OpenConnection(con);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Refund response
        /// </summary>
        /// <param name="terminalId"></param>
        /// <param name="response"></param>
        public override void EmvTxnRefResponseSet(int terminalId, BbTxnRefResponseLog response)
        {
            using (OracleConnection con = OracleInterface.Instance.OracleConnectionGet(ConnectionStringName))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "PaymentExpress.RefundResponse", CommandType = CommandType.StoredProcedure })
                {
                    string txnref = string.IsNullOrEmpty(response.TxnRef) ? string.Empty : response.TxnRef;
                    string reco = string.IsNullOrEmpty(response.ReCo) ? string.Empty : response.ReCo;
                    string amountRequested = string.IsNullOrEmpty(response.Amount) ? string.Empty : response.Amount;
                    string dpsTxnRef = string.IsNullOrEmpty(response.DpsTxnRef) ? string.Empty : response.DpsTxnRef;

                    string resultId = String.IsNullOrEmpty(response.Result.Id) ? string.Empty : response.Result.Id;
                    string resultMessage = String.IsNullOrEmpty(response.Result.Message) ? string.Empty : response.Result.Message;

                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pTransactionReference", OracleDbType.VarChar, txnref.Length, ParameterDirection.Input, txnref));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pRefundResponseCode", OracleDbType.VarChar, reco.Length, ParameterDirection.Input, reco));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pAmountRequested", OracleDbType.Number, 0, ParameterDirection.Input, amountRequested));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pDpsTransactionReference", OracleDbType.VarChar, dpsTxnRef.Length, ParameterDirection.Input, dpsTxnRef));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pTerminalId", OracleDbType.Integer, 0, ParameterDirection.Input, terminalId));
                    cmd.Parameters.Add(new OracleParameter("pResponseDateTime", OracleDbType.TimeStamp, 0, response.TimeStamp.ToLocalTime(), ParameterDirection.Input));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pRespActionResultTokenDomain", OracleDbType.Number, 0, ParameterDirection.Input, response.Result.ResultDomain));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pRespActionResultTokenDomainId", OracleDbType.Number, 0, ParameterDirection.Input, response.Result.ResultDomainId));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pRespActionResultTokenId", OracleDbType.VarChar, resultId.Length, ParameterDirection.Input, resultId));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pRespActionResultTokenMessage", OracleDbType.VarChar, resultMessage.Length, ParameterDirection.Input, resultMessage));

                    OracleInterface.Instance.OpenConnection(con);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Process an Emv TxnSig Request
        /// </summary>
        /// <param name="terminalId"></param>
        /// <param name="request"></param>
        public override void EmvTxnSigRequestSet(int terminalId, BbTxnSigRequestLog request)
        {
            using (OracleConnection con = OracleInterface.Instance.OracleConnectionGet(ConnectionStringName))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "PaymentExpress.SignatureRequest", CommandType = CommandType.StoredProcedure })
                {
                    string txnref = string.IsNullOrEmpty(request.TxnRef) ? string.Empty : request.TxnRef;
                    string sigResponseCode = string.IsNullOrEmpty(request.SignatureResult) ? string.Empty : request.SignatureResult;

                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pTransactionReference", OracleDbType.VarChar, txnref.Length, ParameterDirection.Input, txnref));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pSignatureResponseCode", OracleDbType.VarChar, sigResponseCode.Length, ParameterDirection.Input, sigResponseCode));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pTerminalId", OracleDbType.Integer, 0, ParameterDirection.Input, terminalId));
                    cmd.Parameters.Add(new OracleParameter("pSigResponseDateTime", OracleDbType.TimeStamp, 0, request.TimeStamp.ToLocalTime(), ParameterDirection.Input));
                    OracleInterface.Instance.OpenConnection(con);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Process an Emv TxnSig Response
        /// </summary>
        /// <param name="terminalId"></param>
        /// <param name="response"></param>
        public override void EmvTxnSigResponseSet(int terminalId, BbTxnSigResponseLog response)
        {
            using (OracleConnection con = OracleInterface.Instance.OracleConnectionGet(ConnectionStringName))
            {
                using (OracleCommand cmd = new OracleCommand { Connection = con, CommandText = "PaymentExpress.SignatureResponse", CommandType = CommandType.StoredProcedure })
                {
                    string txnref = string.IsNullOrEmpty(response.TxnRef) ? string.Empty : response.TxnRef;
                    string reco = string.IsNullOrEmpty(response.ReCo) ? string.Empty : response.ReCo;

                    string resultId = String.IsNullOrEmpty(response.Result.Id) ? string.Empty : response.Result.Id;
                    string resultMessage = String.IsNullOrEmpty(response.Result.Message) ? string.Empty : response.Result.Message;

                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pTransactionReference", OracleDbType.VarChar, txnref.Length, ParameterDirection.Input, txnref));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pSignatureResponseCode", OracleDbType.VarChar, reco.Length, ParameterDirection.Input, reco));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pTerminalId", OracleDbType.Integer, 0, ParameterDirection.Input, terminalId));
                    cmd.Parameters.Add(new OracleParameter("pSigResponseDateTime", OracleDbType.TimeStamp, 0, response.TimeStamp.ToLocalTime(), ParameterDirection.Input));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pSigActionResultTokenDomain", OracleDbType.Number, 0, ParameterDirection.Input, response.Result.ResultDomain));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pSigActionResultTokenDomainId", OracleDbType.Number, 0, ParameterDirection.Input, response.Result.ResultDomainId));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pSigActionResultTokenId", OracleDbType.VarChar, resultId.Length, ParameterDirection.Input, resultId));
                    cmd.Parameters.Add(OracleInterface.Instance.OracleParameterGet("pSigActionResultTokenMessage", OracleDbType.VarChar, resultMessage.Length, ParameterDirection.Input, resultMessage));

                    OracleInterface.Instance.OpenConnection(con);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        #endregion
    }
}
