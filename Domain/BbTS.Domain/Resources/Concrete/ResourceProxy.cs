﻿using System;
using System.Collections.Generic;
using System.Drawing;
using BbTS.Domain.Models.Credential;
using BbTS.Domain.Models.Customer;
using BbTS.Domain.Models.Definitions;
using BbTS.Domain.Models.Definitions.Customer;
using BbTS.Domain.Models.PaymentExpress;
using BbTS.Domain.Models.System;
using BbTS.Domain.Models.Terminal;
using BbTS.Domain.Models.Transaction;
using BbTS.Domain.Resources.Abstract;

namespace BbTS.Domain.Resources.Concrete
{
    public class ResourceProxy : Resource
    {
        public static Resource Resource { get; set; }

        #region Customer Functions

        public override TsCustomer CustomerGet(string username, string customerGuid)
        {
            return Resource.CustomerGet(username, customerGuid);
        }

        public override ActionResultToken CustomerSet(string username, TsCustomer customer)
        {
            return Resource.CustomerSet(username, customer);
        }

        public override ActionResultToken CustomerDelete(string username, int customerId)
        {
            return Resource.CustomerDelete(username, customerId);
        }

        public override CredentialView CustomerCredentialGet(string username, int customerId, string credentialGuid)
        {
            return Resource.CustomerCredentialGet(username, customerId, credentialGuid);
        }

        public override CustomerCredentialResponse CustomerCredentialSet(string username, int customerId, CustomerCredentialRequest credential)
        {
            return Resource.CustomerCredentialSet(username, customerId, credential);
        }

        public override CustomerMobileAttributes CustomerMobileAttributesGet(string username, string id)
        {
            return Resource.CustomerMobileAttributesGet(username, id);
        }

        public override ActionResultToken CustomerCredentialRetire(string username, string customerId, string credentialGuid)
        {
            return Resource.CustomerCredentialRetire(username, customerId, credentialGuid);
        }

        public override int CustomerIdGet(String userName, string id)
        {
            return Resource.CustomerIdGet(userName, id);
        }

        public override ActionResultToken CustomerPhotoSet(int id, byte[] photo)
        {
            return Resource.CustomerPhotoSet(id, photo);
        }

        #endregion

        #region System Functions

        public override ActionResultToken PingGet()
        {
            return new ActionResultToken { ResultDomain = (int)DomainValue.DomainTransactApi, ResultDomainId = (int)ConnectionTest.Unknown, Id = Guid.NewGuid().ToString(), Message = "Resource Proxy" };
        }

        public override TerminalTimeZoneInformation TimeZoneInformationGet()
        {
            return Resource.TimeZoneInformationGet();
        }

        public override string TransactApiUriGet()
        {
            return Resource.TransactApiUriGet();
        }

        public override DateTime DataResourceDateTimeGet()
        {
            return Resource.DataResourceDateTimeGet();
        }

        #endregion

        #region Transaction Functions

        /// <summary>
        /// Get a list of all credit card transactions that are configured on the Bb Payment Gateway and
        /// are marked as unprocessed (i.e. not voided, etc.)
        /// </summary>
        /// <returns></returns>
        public override List<CreditCardTransactionView> TransactionCreditCardUnprocessedGet()
        {
            return Resource.TransactionCreditCardUnprocessedGet();
        }

        #endregion

        #region Terminal Functions

        /// <summary>
        /// Get a list of POS devices that are configured with Bb Payment Gateway processing.
        /// </summary>
        /// <returns></returns>
        public override List<Pos> PosBbPaygateConfiguredGet()
        {
            return Resource.PosBbPaygateConfiguredGet();
        }

        /// <summary>
        /// Get a list of EMV reconciliation transactions.
        /// </summary>
        /// <param name="userName">The name of the user making the call.</param>
        /// <param name="id">transaction to fetch; null for all transactions</param>
        /// <returns></returns>
        public override List<PaymentExpressRequestResponseView> EmvReconciliationTransactionGet(string userName, string id)
        {
            return Resource.EmvReconciliationTransactionGet(userName, id);
        }

        public override void EmvReconciliationTransactionSet(PaymentExpressRequestResponseView value)
        {
            Resource.EmvReconciliationTransactionSet(value);
        }

        /// <summary>
        /// Get the EMV terminal settings
        /// </summary>
        /// <returns></returns>
        public override EmvSettings EmvSettingsGet(int terminalId)
        {
            return Resource.EmvSettingsGet(terminalId);
        }

        public override void EmvTxnPurRequestSet(int terminalId, BbTxnPurRequestLog request)
        {
            Resource.EmvTxnPurRequestSet(terminalId, request);
        }

        public override void EmvTxnPurResponseSet(int terminalId, BbTxnPurResponseLog response)
        {
            Resource.EmvTxnPurResponseSet(terminalId, response);
        }

        public override void EmvTxnGet1ResponseSet(int terminalId, BbTxnGet1ResponseLog response)
        {
            Resource.EmvTxnGet1ResponseSet(terminalId, response);
        }

        public override void EmvTxnVoidRequestSet(int terminalId, BbTxnVoidRequestLog request)
        {
            Resource.EmvTxnVoidRequestSet(terminalId, request);
        }

        public override void EmvTxnVoidResponseSet(int terminalId, BbTxnVoidResponseLog response)
        {
            Resource.EmvTxnVoidResponseSet(terminalId, response);
        }

        public override void EmvTxnRefRequestSet(int terminalId, BbTxnRefRequestLog request)
        {
            Resource.EmvTxnRefRequestSet(terminalId, request);
        }

        public override void EmvTxnRefResponseSet(int terminalId, BbTxnRefResponseLog response)
        {
            Resource.EmvTxnRefResponseSet(terminalId, response);
        }

        public override void EmvTxnSigRequestSet(int terminalId, BbTxnSigRequestLog request)
        {
            Resource.EmvTxnSigRequestSet(terminalId, request);
        }

        public override void EmvTxnSigResponseSet(int terminalId, BbTxnSigResponseLog response)
        {
            Resource.EmvTxnSigResponseSet(terminalId, response);
        }

        #endregion
    }
}
