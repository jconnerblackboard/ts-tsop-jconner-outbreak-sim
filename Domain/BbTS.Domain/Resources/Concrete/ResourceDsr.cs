﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Persona.AccessControl;
using BbTS.Domain.Persona.Code;
using BbTS.Domain.Persona.Code.Aspects;
using BbTS.Domain.Persona.Code.Aspects.DsrExceptionHandlers;
using BbTS.Domain.Persona.DataAccess;
using BbTS.Domain.Persona.Management;
using BbTS.Domain.Persona.Models;
using BbTS.Domain.Persona.Resources.Abstract;
using BbTS.Monitoring.Logging;
using BbTS.Service;
using AccessPoint = BbTS.Domain.Persona.Models.AccessPoint;
using AccessPointMode = BbTS.Domain.Persona.Models.AccessPointMode;
using Authorization = BbTS.Domain.Persona.Models.Authorization;
using DayException = BbTS.Domain.Persona.Models.DayException;
using DayExceptionGroup = BbTS.Domain.Persona.Models.DayExceptionGroup;
using DayPeriod = BbTS.Domain.Persona.Models.DayPeriod;
using Schedule = BbTS.Domain.Persona.Models.Schedule;
using User = BbTS.Domain.Persona.Models.User;

namespace BbTS.Domain.Persona.Resources.Concrete
{
    public class ResourceDsr : Resource
    {

        #region AccessPoints

        [NoThrowLogExceptionPolicy]
        public override List<AccessPoint> GetAllAccessPoints()
        {
            String url = String.Format("{0}/{1}", DataConfiguration.DsrUrl, "/Management?wsdl");
            ManagementInterfaceClient client = SoapInterface.Instance.ManagementClientGet(url);

            // Fetch the authorizations from the dsr
            List<Management.AccessPoint> dsrAccessPoints = new List<Management.AccessPoint>(client.listAllAccessPoints());

            // Place the dsr authorizations into our own authorization object
            List<AccessPoint> accessPoints = new List<AccessPoint>();

            foreach (var accessPoint in dsrAccessPoints)
            {
                try
                {
                    accessPoints.Add(new AccessPoint(accessPoint));
                }
                catch (Exception ex)
                {
                    LoggingManager.Instance.LogException(ex, "For AccessPointMode Id = " + accessPoint.id, "", "ResourceDsr::GetAllAccessPoints()", "", "");
                }
            }

            return accessPoints;
        }

        [NoThrowLogExceptionPolicy]
        public override AccessPoint GetAccessPoint(string id)
        {
            String url = String.Format("{0}/{1}", DataConfiguration.DsrUrl, "/Management?wsdl");
            ManagementInterfaceClient client = SoapInterface.Instance.ManagementClientGet(url);
            Management.AccessPoint dsrAccessPoint = new List<Management.AccessPoint>(client.listAllAccessPoints()).FirstOrDefault(ap => ap.id == id);

            if (dsrAccessPoint != null)
            {
                return new AccessPoint(dsrAccessPoint);
            }
            String errorMessage = String.Format("AccessPointMode Id = {0} was not found", id);
            LoggingManager.Instance.LogMessage(errorMessage);

            return null;
        }

        [NoThrowLogExceptionPolicy]
        public override string AddAccessPoint(AccessPoint accessPoint)
        {
            Management.AccessPoint ap = TransactToDsrFormatter.AccessPointFormat(accessPoint);
            String url = String.Format("{0}/{1}", DataConfiguration.DsrUrl, "/Management?wsdl");
            ManagementInterfaceClient client = SoapInterface.Instance.ManagementClientGet(url);
            String apGuid = client.addAndConfirmAccessPoint(ap.accessPointType.id, ap.serialNumber);
            return apGuid;
        }

        [NoThrowLogExceptionPolicy]
        public override void UpdateAccessPoint(string id, AccessPoint accessPoint)
        {
            throw new NotImplementedException();
        }

        [LogExceptionPolicy]
        public override void DeleteAccessPoint(string id)
        {
            String url = String.Format("{0}/{1}", DataConfiguration.DsrUrl, "/Management?wsdl");
            ManagementInterfaceClient client = SoapInterface.Instance.ManagementClientGet(url);
            client.removeAccessPoint(id);
        }

        [NoThrowLogExceptionPolicy]
        public override void ConfirmAccessPoint(string accessPointGuid)
        {
            String url = String.Format("{0}/{1}", DataConfiguration.DsrUrl, "/Management?wsdl");
            ManagementInterfaceClient client = SoapInterface.Instance.ManagementClientGet(url);
            client.confirmAccessPoint(accessPointGuid, true);
        }

        /// <summary>
        /// Should not be implemented or called within the context of his resource
        /// </summary>
        /// <param name="oldGuid"></param>
        /// <param name="serialNumber"></param>
        /// <param name="newGuid"></param>
        [LogExceptionPolicy]
        public override void UpdateAccessPointGuid(string oldGuid, string serialNumber, string newGuid)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Should not be implemented or called within the context of his resource
        /// </summary>
        /// <param name="apGuid"></param>
        [LogExceptionPolicy]
        public override void MarkToSendDeviceSettings(string apGuid)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Should not be implemented or called within the context of his resource
        /// </summary>
        /// <returns></returns>
        [LogExceptionPolicy]
        public override List<string> GetMarkedDevicesForConfigUpdate()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Send the device specific commands to the access point
        /// </summary>
        /// <param name="apGuid"></param>
        /// <param name="settings">List of commands (Settings) to send</param>
        [LogExceptionPolicy]
        public override void SendDeviceSettings(string apGuid, List<DeviceSpecificCommand> settings)
        {
            foreach (var setting in settings)
            {
                AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");
                DeviceSpecificCommandType command = TransactToDsrFormatter.DeviceSpecificCommandFormat(setting);
                client.deviceSpecificCommand(apGuid, command);
            }
        }

        /// <summary>
        /// Should not be implemented or called within the context of his resource
        /// </summary>
        /// <param name="accessPointGuids"></param>
        [LogExceptionPolicy]
        public override void MarkAccessPointsAsUpdated(List<string> accessPointGuids)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Should not be implemented or called within the context of his resource
        /// </summary>
        /// <param name="accessPointGuid"></param>
        /// <returns></returns>
        [LogExceptionPolicy]
        public override List<DeviceSpecificCommandItem> GetDeviceSpecificCommands(string accessPointGuid)
        {
            throw new NotImplementedException();
        }

        public override void UpdateDstSettings(string apGuid, DeviceSpecificCommand dstSettings)
        {
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");
            DeviceSpecificCommandType command = TransactToDsrFormatter.DeviceSpecificCommandFormat(dstSettings);
            client.deviceSpecificCommand(apGuid, command);
        }

        public override DeviceSpecificCommand GetDstSettings(string apGuid)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region AccessPointModes
        [NoThrowLogExceptionPolicy]
        public override List<AccessPointMode> GetAllAccessPointModes()
        {
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");

            // Fetch the authorizations from the dsr
            List<AccessControl.AccessPointMode> dsrAccessPointModes =
                new List<AccessControl.AccessPointMode>(
                    client.findAccessPointModes(client.getAccessPointModeIds()));

            // Place the dsr authorizations into our own authorization object
            List<AccessPointMode> accessPointModes = new List<AccessPointMode>();

            foreach (var accessPointMode in dsrAccessPointModes)
            {
                try
                {
                    accessPointModes.Add(new AccessPointMode(accessPointMode));
                }
                catch (Exception ex)
                {
                    LoggingManager.Instance.LogException(ex, "For AccessPointMode Id = " + accessPointMode.id, "", "ResourceDsr::GetAccessPointMode()", "", "");
                }
            }

            // Return the authorizations
            return accessPointModes;
        }

        [NoThrowLogExceptionPolicy]
        public override AccessPointMode GetAccessPointMode(string id)
        {
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");

            // Fetch the authorizations from the dsr
            List<AccessControl.AccessPointMode> dsrAccessPointModes =
                new List<AccessControl.AccessPointMode>(client.findAccessPointModes(new string[] { id }));

            // Place the dsr authorizations into our own authorization object
            List<AccessPointMode> accessPointModes = new List<AccessPointMode>();

            foreach (var accessPointMode in dsrAccessPointModes)
            {
                try
                {
                    accessPointModes.Add(new AccessPointMode(accessPointMode));
                }
                catch (Exception ex)
                {
                    LoggingManager.Instance.LogException(ex, "For AccessPointMode Id = " + accessPointMode.id, "", "ResourceDsr::GetAccessPointMode()", "", "");
                }
            }

            // Return the authorizations
            return accessPointModes.Count > 0 ? accessPointModes[0] : null;
        }

        [NoThrowLogExceptionPolicy]
        public override string AddAccessPointMode(AccessPointMode accesspointmode)
        {
            AccessControl.AccessPointMode apm = TransactToDsrFormatter.AccessPointModeFormat(accesspointmode);
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");
            AccessControl.DsrUUID requestId = new AccessControl.DsrUUID { requestId = Guid.NewGuid().ToString("D") };
            client.addAccessPointMode(apm, requestId);
            return accesspointmode.Guid;
        }

        [NoThrowLogExceptionPolicy]
        public override void UpdateAccessPointMode(string id, AccessPointMode accesspointmode)
        {
            AccessControl.AccessPointMode apm = TransactToDsrFormatter.AccessPointModeFormat(accesspointmode);
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");
            AccessControl.DsrUUID requestId = new AccessControl.DsrUUID { requestId = Guid.NewGuid().ToString("D") };
            client.modifyAccessPointMode(apm, requestId);
        }

        [NoThrowLogExceptionPolicy]
        public override void DeleteAccessPointMode(string id)
        {
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");
            AccessControl.DsrUUID requestId = new AccessControl.DsrUUID { requestId = Guid.NewGuid().ToString("D") };
            client.removeAccessPointMode(id, requestId);
        }

        public override void AddAccessPointsToAccessPointMode(DataConfiguration config, List<string> apGuids, string apmGuid)
        {
        }

        public override void RemoveAccessPointsFromAccessPointMode(DataConfiguration config, List<string> apGuids, string apmGuid)
        {
        }

        #endregion

        #region Authorization
        [NoThrowLogExceptionPolicy]
        public override List<Authorization> GetAllAuthorizations()
        {
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");

            // Fetch the authorizations from the dsr
            List<AccessControl.Authorization> dsrAuthList = 
                new List<AccessControl.Authorization>(
                    client.findAuthorizations(client.getAuthorizationIds()));

            // Place the dsr authorizations into our own authorization object
            List<Authorization> authorizations = new List<Authorization>();

            foreach (var dsrAuth in dsrAuthList)
            {
                try
                {
                    authorizations.Add(new Authorization(dsrAuth));
                }
                catch (Exception ex)
                {
                    LoggingManager.Instance.LogException(ex, "For Authorization Id = " + dsrAuth.id, "", "ResourceDsr::GetAllAuthorizations()","","");
                }
            }

            // Return the authorizations
            return authorizations;
        }

        [NoThrowLogExceptionPolicy]
        public override Authorization GetAuthorization(string id)
        {
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");

            // Fetch the authorizations from the dsr
            List<AccessControl.Authorization> dsrAuthList =
                new List<AccessControl.Authorization>(client.findAuthorizations(new string[] { id }));

            // Place the dsr authorizations into our own authorization object
            List<Authorization> authorizations = new List<Authorization>();

            foreach (var dsrAuth in dsrAuthList)
            {
                try
                {
                    authorizations.Add(new Authorization(dsrAuth));
                }
                catch (Exception ex)
                {
                    LoggingManager.Instance.LogException(ex, "For Authorization Id = " + dsrAuth.id, "", "ResourceDsr::GetAllAuthorizations()", "", "");
                }
            }

            // Return the authorizations
            return authorizations.Count > 0 ? authorizations[0] : null;
        }

        [NoThrowLogExceptionPolicy]
        public override string AddAuthorization(Authorization authorization)
        {
            AccessControl.Authorization dsrAuthorization = TransactToDsrFormatter.AuthorizationFormat(authorization);
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");
            AccessControl.DsrUUID requestId = new AccessControl.DsrUUID { requestId = Guid.NewGuid().ToString("D") };
            client.addAuthorization(dsrAuthorization, requestId);
            return authorization.Guid;
        }

        [NoThrowLogExceptionPolicy]
        public override void UpdateAuthorization(string id, Authorization authorization)
        {
            AccessControl.Authorization dsrAuthorization = TransactToDsrFormatter.AuthorizationFormat(authorization);
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");
            AccessControl.DsrUUID requestId = new AccessControl.DsrUUID { requestId = Guid.NewGuid().ToString("D") };
            client.modifyAuthorization(dsrAuthorization, requestId);
        }

        [NoThrowLogExceptionPolicy]
        public override void DeleteAuthorization(string id)
        {
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");
            AccessControl.DsrUUID requestId = new AccessControl.DsrUUID { requestId = Guid.NewGuid().ToString("D") };
            client.removeAuthorization(id, requestId);
        }

        [NoThrowLogExceptionPolicy]
        public override void AddUsersToAuthorization(DataConfiguration config, List<string> userGuid, string authGuid)
        {
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");
            AccessControl.DsrUUID requestId = new AccessControl.DsrUUID { requestId = Guid.NewGuid().ToString("D") };
            client.addUsersToAuthorization(userGuid.ToArray(), authGuid, requestId);
        }

        [NoThrowLogExceptionPolicy]
        public override void RemoveUsersFromAuthorization(DataConfiguration config, List<string> userGuid, string authGuid)
        {
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");
            AccessControl.DsrUUID requestId = new AccessControl.DsrUUID { requestId = Guid.NewGuid().ToString("D") };
            client.removeUsersFromAuthorization(userGuid.ToArray(), authGuid, requestId);
        }

        [NoThrowLogExceptionPolicy]
        public override void AddAccessPointsToAuthorization(DataConfiguration config, List<string> apGuid, string authGuid)
        {
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");
            AccessControl.DsrUUID requestId = new AccessControl.DsrUUID { requestId = Guid.NewGuid().ToString("D") };
            client.addAccessPointsToAuthorization(apGuid.ToArray(), authGuid, requestId);
        }

        [NoThrowLogExceptionPolicy]
        public override void RemoveAccessPointsFromAuthorization(DataConfiguration config, List<string> apGuid, string authGuid)
        {
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");
            AccessControl.DsrUUID requestId = new AccessControl.DsrUUID { requestId = Guid.NewGuid().ToString("D") };
            client.removeAccessPointsFromAuthorizationAsync(apGuid.ToArray(), authGuid, requestId);
        }

        #endregion

        #region DayExceptionGroups

        [NoThrowLogExceptionPolicy]
        public override List<DayExceptionGroup> GetAllDayExceptionGroups()
        {
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");

            // Fetch the authorizations from the dsr
            List<AccessControl.DayExceptionGroup> dsrDayExceptionGroups =
                new List<AccessControl.DayExceptionGroup>(
                    client.findDayExceptionGroups(client.getDayExceptionGroupIds()));

            // Place the dsr authorizations into our own authorization object
            List<DayExceptionGroup> dayExceptionGroups = new List<DayExceptionGroup>();

            foreach (var dayExceptionGroup in dsrDayExceptionGroups)
            {
                try
                {
                    dayExceptionGroups.Add(new DayExceptionGroup(dayExceptionGroup));
                }
                catch (Exception ex)
                {
                    LoggingManager.Instance.LogException(ex, "For Day Exception Group Id = " + dayExceptionGroup.id, "", "ResourceDsr::GetAllDayExceptionGroups()", "", "");
                }
            }

            // Return the authorizations
            return dayExceptionGroups;
        }

        [NoThrowLogExceptionPolicy]
        public override DayExceptionGroup GetDayExceptionGroup(string id)
        {
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");

            // Fetch the authorizations from the dsr
            List<AccessControl.DayExceptionGroup> dsrDayExceptionGroups =
                new List<AccessControl.DayExceptionGroup>(client.findDayExceptionGroups(new string[] { id }));

            // Place the dsr authorizations into our own authorization object
            List<DayExceptionGroup> dayExceptionGroups = new List<DayExceptionGroup>();

            foreach (var dayExceptionGroup in dsrDayExceptionGroups)
            {
                try
                {
                    dayExceptionGroups.Add(new DayExceptionGroup(dayExceptionGroup));
                }
                catch (Exception ex)
                {
                    LoggingManager.Instance.LogException(ex, "For Day Exception Group Id = " + dayExceptionGroup.id, "", "ResourceDsr::GetAllDayExceptionGroups()", "", "");
                }
            }

            // Return the authorizations
            return dayExceptionGroups.Count > 0 ? dayExceptionGroups[0] : null;
        }

        [NoThrowLogExceptionPolicy]
        public override string AddDayExceptionGroup(DayExceptionGroup dayexceptiongroup)
        {
            AccessControl.DayExceptionGroup deGroup = TransactToDsrFormatter.DayExceptionGroupFormat(dayexceptiongroup);
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");
            AccessControl.DsrUUID requestId = new AccessControl.DsrUUID { requestId = Guid.NewGuid().ToString("D") };
            client.addDayExceptionGroup(deGroup, requestId);
            return dayexceptiongroup.Guid;
        }

        [NoThrowLogExceptionPolicy]
        public override void UpdateDayExceptionGroup(string id, DayExceptionGroup dayexceptiongroup)
        {
            AccessControl.DayExceptionGroup deGroup = TransactToDsrFormatter.DayExceptionGroupFormat(dayexceptiongroup);
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");
            AccessControl.DsrUUID requestId = new AccessControl.DsrUUID { requestId = Guid.NewGuid().ToString("D") };
            client.modifyDayExceptionGroup(deGroup, requestId);
        }

        [NoThrowLogExceptionPolicy]
        public override void DeleteDayExceptionGroup(string id)
        {
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");
            AccessControl.DsrUUID requestId = new AccessControl.DsrUUID { requestId = Guid.NewGuid().ToString("D") };
            client.removeDayExceptionGroup(id, requestId);
        }
        #endregion

        #region DayExceptions

        [NoThrowLogExceptionPolicy]
        public override List<DayException> GetAllDayExceptions()
        {
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");

            // Fetch the authorizations from the dsr
            List<AccessControl.DayException> dsrDayExceptions =
                new List<AccessControl.DayException>(
                    client.findDayExceptions(client.getDayExceptionIds()));

            // Place the dsr authorizations into our own authorization object
            List<DayException> dayExceptions = new List<DayException>();

            foreach (var dayException in dsrDayExceptions)
            {
                try
                {
                    dayExceptions.Add(new DayException(dayException));
                }
                catch (Exception ex)
                {
                    LoggingManager.Instance.LogException(ex, "For Day Exception Id = " + dayException.id, "", "ResourceDsr::GetAllDayExceptions()", "", "");
                }
            }

            // Return the authorizations
            return dayExceptions;
        }

        [NoThrowLogExceptionPolicy]
        public override DayException GetDayException(string id)
        {
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");

            // Fetch the authorizations from the dsr
            List<AccessControl.DayException> dsrDayExceptions =
                new List<AccessControl.DayException>(client.findDayExceptions(new string[] { id }));

            // Place the dsr authorizations into our own authorization object
            List<DayException> dayExceptions = new List<DayException>();

            foreach (var dayException in dsrDayExceptions)
            {
                try
                {
                    dayExceptions.Add(new DayException(dayException));
                }
                catch (Exception ex)
                {
                    LoggingManager.Instance.LogException(ex, "For Day Exception Id = " + dayException.id, "", "ResourceDsr::GetAllDayExceptions()", "", "");
                }
            }

            // Return the authorizations
            return dayExceptions.Count > 0 ? dayExceptions[0] : null;
        }

        [NoThrowLogExceptionPolicy]
        public override string AddDayException(DayException dayexception)
        {
            AccessControl.DayException dsrdayException = TransactToDsrFormatter.DayExceptionFormat(dayexception);
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");
            AccessControl.DsrUUID requestId = new AccessControl.DsrUUID { requestId = Guid.NewGuid().ToString("D") };
            client.addDayException(dsrdayException, requestId);
            return dayexception.Guid;
        }

        [NoThrowLogExceptionPolicy]
        public override void UpdateDayException(string id, DayException dayexception)
        {
            AccessControl.DayException dsrdayException = TransactToDsrFormatter.DayExceptionFormat(dayexception);
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");
            AccessControl.DsrUUID requestId = new AccessControl.DsrUUID { requestId = Guid.NewGuid().ToString("D") };
            client.modifyDayException(dsrdayException, requestId);
        }

        [NoThrowLogExceptionPolicy]
        public override void DeleteDayException(string id)
        {
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");
            AccessControl.DsrUUID requestId = new AccessControl.DsrUUID { requestId = Guid.NewGuid().ToString("D") };
            client.removeDayException(id, requestId);
        }

        #endregion
        
        #region DayPeriods

        [NoThrowLogExceptionPolicy]
        public override List<DayPeriod> GetAllDayPeriods()
        {
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");

            List<AccessControl.DayPeriod> dsrDayPeriods =
                new List<AccessControl.DayPeriod>(
                    client.findDayPeriods(client.getDayPeriodIds()));

            List<DayPeriod> dayPeriods = new List<DayPeriod>();

            foreach (var dayPeriod in dsrDayPeriods)
            {
                try
                {
                    dayPeriods.Add(new DayPeriod(dayPeriod));
                }
                catch (Exception ex)
                {
                    LoggingManager.Instance.LogException(ex, "For Day Period Id = " + dayPeriod.id, "", "ResourceDsr::GetAllDayPeriods()", "", "");
                }
            }

            return dayPeriods;
        }

        [NoThrowLogExceptionPolicy]
        public override DayPeriod GetDayPeriod(string id)
        {
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");

            List<AccessControl.DayPeriod> dsrDayPeriods =
                new List<AccessControl.DayPeriod>(client.findDayPeriods(new string[] { id }));

            List<DayPeriod> dayPeriods = new List<DayPeriod>();

            foreach (var dayPeriod in dsrDayPeriods)
            {
                try
                {
                    dayPeriods.Add(new DayPeriod(dayPeriod));
                }
                catch (Exception ex)
                {
                    LoggingManager.Instance.LogException(ex, "For Day Period Id = " + dayPeriod.id, "", "ResourceDsr::GetAllDayPeriods()", "", "");
                }
            }

            return dayPeriods.Count > 0 ? dayPeriods[0] : null;
        }

        [NoThrowLogExceptionPolicy]
        public override string AddDayPeriod(DayPeriod dayperiod)
        {
            AccessControl.DayPeriod dsrItem = TransactToDsrFormatter.DayPeriodFormat(dayperiod);
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");
            AccessControl.DsrUUID requestId = new AccessControl.DsrUUID { requestId = Guid.NewGuid().ToString("D") };
            client.addDayPeriod(dsrItem, requestId);
            return dayperiod.Guid;
        }

        [NoThrowLogExceptionPolicy]
        public override void UpdateDayPeriod(string id, DayPeriod dayperiod)
        {
            AccessControl.DayPeriod dsrItem = TransactToDsrFormatter.DayPeriodFormat(dayperiod);
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");
            AccessControl.DsrUUID requestId = new AccessControl.DsrUUID { requestId = Guid.NewGuid().ToString("D") };
            client.modifyDayPeriod(dsrItem, requestId);
        }

        [NoThrowLogExceptionPolicy]
        public override void DeleteDayPeriod(string id)
        {
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");
            AccessControl.DsrUUID requestId = new AccessControl.DsrUUID { requestId = Guid.NewGuid().ToString("D") };
            client.removeDayPeriod(id, requestId);
        }
        #endregion

        #region Schedules
        [NoThrowLogExceptionPolicy]
        public override List<Schedule> GetAllSchedules()
        {
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");

            // Fetch the authorizations from the dsr
            List<AccessControl.Schedule> dsrSchedules =
                new List<AccessControl.Schedule>(
                    client.findSchedules(client.getScheduleIds()));

            // Place the dsr authorizations into our own authorization object
            List<Schedule> schedules = new List<Schedule>();

            foreach (var dsrSchedule in dsrSchedules)
            {
                try
                {
                    schedules.Add(new Schedule(dsrSchedule));
                }
                catch (Exception ex)
                {
                    LoggingManager.Instance.LogException(ex, "For Schedule Id = " + dsrSchedule.id, "", "ResourceDsr::GetAllSchedules()", "", "");
                }
            }

            // Return the authorizations
            return schedules;
        }

        [NoThrowLogExceptionPolicy]
        public override Schedule GetSchedule(string id)
        {
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");

            List<AccessControl.Schedule> dsrSchedules =
                new List<AccessControl.Schedule>(client.findSchedules(new string[] { id }));

            List<Schedule> schedules = new List<Schedule>();

            foreach (var dsrSchedule in dsrSchedules)
            {
                try
                {
                    schedules.Add(new Schedule(dsrSchedule));
                }
                catch (Exception ex)
                {
                    LoggingManager.Instance.LogException(ex, "For Schedule Id = " + dsrSchedule.id, "", "ResourceDsr::GetAllSchedules()", "", "");
                }
            }

            return schedules.Count > 0 ? schedules[0] : null;
        }

        [NoThrowLogExceptionPolicy]
        public override string AddSchedule(Schedule schedule)
        {
            AccessControl.Schedule dsrItem = TransactToDsrFormatter.ScheduleFormat(schedule);
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");
            AccessControl.DsrUUID requestId = new AccessControl.DsrUUID { requestId = Guid.NewGuid().ToString("D") };
            client.addSchedule(dsrItem, requestId);
            return schedule.Guid;
        }

        [NoThrowLogExceptionPolicy]
        public override void UpdateSchedule(string id, Schedule schedule)
        {
            AccessControl.Schedule dsrItem = TransactToDsrFormatter.ScheduleFormat(schedule);
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");
            AccessControl.DsrUUID requestId = new AccessControl.DsrUUID { requestId = Guid.NewGuid().ToString("D") };
            client.modifySchedule(dsrItem, requestId);
        }

        [NoThrowLogExceptionPolicy]
        public override void DeleteSchedule(string id)
        {
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");
            AccessControl.DsrUUID requestId = new AccessControl.DsrUUID { requestId = Guid.NewGuid().ToString("D") };
            client.removeSchedule(id, requestId);
        }
        #endregion

        #region Users
        [NoThrowLogExceptionPolicy]
        public override List<User> GetAllUsers()
        {
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");

            // Fetch the authorizations from the dsr
            List<AccessControl.User> dsrUsers =
                new List<AccessControl.User>(
                    client.findUsers(client.getUserIds()));

            // Place the dsr authorizations into our own authorization object
            List<User> users = new List<User>();

            foreach (var dsrUser in dsrUsers)
            {
                try
                {
                    users.Add(new User(dsrUser));
                }
                catch (Exception ex)
                {
                    LoggingManager.Instance.LogException(ex, "For User Id = " + dsrUser.id, "", "ResourceDsr::GetAllUsers()", "", "");
                }
            }

            // Return the authorizations
            return users;
        }

        [NoThrowLogExceptionPolicy]
        public override User GetUser(string id)
        {
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");

            // Fetch the authorizations from the dsr
            List<AccessControl.User> dsrUsers = new List<AccessControl.User>(client.findUsers(new string[] { id }));

            // Place the dsr authorizations into our own authorization object
            List<User> users = new List<User>();

            foreach (var dsrUser in dsrUsers)
            {
                try
                {
                    users.Add(new User(dsrUser));
                }
                catch (Exception ex)
                {
                    LoggingManager.Instance.LogException(ex, "For User Id = " + dsrUser.id, "", "ResourceDsr::GetUser()", "", "");
                }
            }

            // Return the authorizations
            return users.Count > 0 ? users[0] : null;
        }

        [NoThrowLogExceptionPolicy]
        public override string AddUser(User user)
        {
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");
            AccessControl.User dsrUser = TransactToDsrFormatter.UserFormat(user);
            AccessControl.DsrUUID requestId = new AccessControl.DsrUUID { requestId = Guid.NewGuid().ToString("D") };
            client.addUser(dsrUser, requestId);
            return user.Guid;
        }

        [NoThrowLogExceptionPolicy]
        public override void UpdateUser(string id, User user)
        {
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");
            AccessControl.User dsrUser = TransactToDsrFormatter.UserFormat(user);
            AccessControl.DsrUUID requestId = new AccessControl.DsrUUID { requestId = Guid.NewGuid().ToString("D") };
            client.modifyUser(dsrUser, requestId);
        }

        [NoThrowLogExceptionPolicy]
        public override void DeleteUser(string id)
        {
            AccessControlInterfaceClient client = SoapInterface.Instance.AccessControlClient(DataConfiguration.DsrUrl + "/AccessControl?wsdl");
            AccessControl.DsrUUID requestId = new AccessControl.DsrUUID { requestId = Guid.NewGuid().ToString("D") };
            client.removeUser(id, requestId);
        }

        #endregion
    }
}
