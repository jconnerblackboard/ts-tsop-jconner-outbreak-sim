﻿using System;
using System.Collections.Generic;
using BbTS.Core.Configuration;
using BbTS.Domain.Resources.Abstract;
using BbTS.Domain.Resources.Concrete;


namespace BbTS.Domain.Resources
{
    public class ResourceManager
    {
        private static ResourceManager _instance = new ResourceManager();
        private readonly Dictionary<DataSource, Resource> _resources;

        private ResourceManager()
        {
            _resources = new Dictionary<DataSource, Resource>
                {
                    { DataSource.Oracle, new ResourceOracle() },
                    { DataSource.Proxy, new ResourceProxy() }
                };
        }

        public static ResourceManager Instance
        {
            get { return _instance; }
            internal set { _instance = value; }
        }


        /// <summary>
        /// Get the resource concrete.  Value is set in SystemConfiguration.xml under the DataSource tag.
        /// </summary>
        public Resource Resource
        {
            get
            {
                String dataSourceTag = ApplicationConfiguration.GetKeyValueAsString("DataSource", "");
                if (!String.IsNullOrEmpty(dataSourceTag))
                {
                    try
                    {
                        DataSource source = (DataSource)Enum.Parse(typeof (DataSource), dataSourceTag);
                        if (!_resources.ContainsKey(source))
                        {
                            String message = String.Format(
                            "Data source specified (\"{0}\") not a supported type.  Please contact your system administrator.",
                            dataSourceTag);
                            throw new ArgumentException(message);
                        }
                        
                        return _resources[source];
                    }
                    catch (ArgumentException)
                    {
                        String message = String.Format(
                            "Data source specified (\"{0}\") is not a valid DataSource type.  Please contact your system administrator.",
                            dataSourceTag);
                        throw new ArgumentException(message);
                    }
                }

// ReSharper disable NotResolvedInText
                throw new ArgumentException(@"Data source not specified in SystemConfiguration.xml.  Please set the DataSource tag with the proper data location.", @"DataSource");
// ReSharper restore NotResolvedInText
            }
// ReSharper disable once ValueParameterNotUsed
            internal set {  }
        }
    }

    public enum DataSource
    {
        Oracle,
        Proxy
    }
}
