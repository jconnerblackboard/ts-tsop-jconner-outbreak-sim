﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using BbTS.Domain.Monitoring.Logging.Models;
using BbTS.Security;
using BbTS.Service;
using Devart.Data.Oracle;

namespace BbTS.Domain.Resources
{

    /// <summary>
    /// This class contains database access functionality
    /// </summary>
    public class OracleResource
    {
        private static OracleResource _instance = new OracleResource();

        public static OracleResource Instance
        {
            get { return _instance; }
            internal set { _instance = value; }
        }

        #region Connection/Parameter Settings

        /// <summary>
        /// This method returns a connection object
        /// </summary>
        /// <param name="connectionName">The name of the connection string to be used</param>
        /// <returns>A <see cref="OracleConnection"/></returns>
        public OracleConnection OracleConnectionGet(String connectionName)
        {
            // To fetch from web.config instead of SystemConfiguration.xml
            // ConfigurationManager.ConnectionStrings[connectionName].ToString()
            String connectionString = ConnectionStringGet(connectionName);

            return new OracleConnection(connectionString);
        }

        /// <summary>
        /// Build an Oracle Connection string using an entry in SystemConfiguration.xml
        /// </summary>
        /// <param name="connectionStringName"></param>
        /// <returns></returns>
        public String ConnectionStringGet(string connectionStringName)
        {
            // Connect to the database
            ConnectionStringMapEntry connectionStringMapEntry =
                ConnectionStringMap.GetConnectionStringMapEntry(connectionStringName);
            string encryptedPassword = connectionStringMapEntry.EncryptedPassword;
            string connectionString = connectionStringMapEntry.ConnectionString;

            string decryptedPasswordString = EncryptionTool.DecryptOraclePassword(encryptedPassword);

            return connectionString + ";password=" + decryptedPasswordString;
        }

        /// <summary>
        /// This method creates an OracleParameter - DateTime value overload
        /// </summary>
        /// <param name="parmName">The name of the parameter</param>
        /// <param name="parmDataType">The data type of the parameter</param>
        /// <param name="parmDataSize">The size of the parameter</param>
        /// <param name="parmDirection">The direction of the parameter</param>
        /// <param name="parmValue">The value of the parameter</param>
        /// <returns>A <see cref="OracleParameter"/> object</returns>
        public OracleParameter OracleParameterGet(string parmName, OracleDbType parmDataType, int parmDataSize, ParameterDirection parmDirection, DateTime parmValue)
        {
            return OracleParameterGet(parmName, parmDataType, parmDataSize, parmDirection, parmValue.ToString(CultureInfo.InvariantCulture));
        }

        /// <summary>
        /// This method creates an OracleParameter - Int32 value overload
        /// </summary>
        /// <param name="parmName">The name of the parameter</param>
        /// <param name="parmDataType">The data type of the parameter</param>
        /// <param name="parmDataSize">The size of the parameter</param>
        /// <param name="parmDirection">The direction of the parameter</param>
        /// <param name="parmValue">The value of the parameter</param>
        /// <returns>A <see cref="OracleParameter"/> object</returns>
        public OracleParameter OracleParameterGet(String parmName, OracleDbType parmDataType, Int32 parmDataSize, ParameterDirection parmDirection, Int32? parmValue)
        {
            return OracleParameterGet(parmName, parmDataType, parmDataSize, parmDirection, parmValue.ToString());
        }

        /// <summary>
        /// This method creates an OracleParameter - No value overload
        /// </summary>
        /// <param name="parmName">The name of the parameter</param>
        /// <param name="parmDataType">The data type of the parameter</param>
        /// <param name="parmDataSize">The size of the parameter</param>
        /// <param name="parmDirection">The direction of the parameter</param>
        /// <returns>A <see cref="OracleParameter"/> object</returns>
        public OracleParameter OracleParameterGet(String parmName, OracleDbType parmDataType, Int32 parmDataSize, ParameterDirection parmDirection)
        {
            return OracleParameterGet(parmName, parmDataType, parmDataSize, parmDirection, String.Empty);
        }

        /// <summary>
        /// This method creates an OracleParameter - Base method 
        /// </summary>
        /// <param name="parmName">The name of the parameter</param>
        /// <param name="parmDataType">The data type of the parameter</param>
        /// <param name="parmDataSize">The size of the parameter</param>
        /// <param name="parmDirection">The direction of the parameter</param>
        /// <param name="parmValue">The value of the parameter</param>
        /// <returns>A <see cref="OracleParameter"/> object</returns>
        public OracleParameter OracleParameterGet(String parmName, OracleDbType parmDataType, Int32 parmDataSize, ParameterDirection parmDirection, String parmValue)
        {
            OracleParameter p = new OracleParameter { ParameterName = parmName, OracleDbType = parmDataType };

            if (!parmDataSize.Equals(0))
                p.Size = parmDataSize;

            p.Direction = parmDirection;

            if (!String.IsNullOrEmpty(parmValue))
            {

                if (!parmDataType.Equals(OracleDbType.Raw) && !parmDataType.Equals(OracleDbType.Date))
                {
                    p.Value = parmValue;
                }
                else if (parmDataType.Equals(OracleDbType.Date))
                {
                    p.Value = new OracleDate(DateTime.Parse(parmValue));
                }
                else
                {
                    byte[] buffer = new byte[p.Size];
                    for (int i = 0; i < p.Size; i++)
                    {
                        buffer[i] = Convert.ToByte(parmValue.Substring(i, 1));
                    }
                    p.Value = buffer;
                }
            }

            if (p.Value == null)
                p.Value = DBNull.Value;

            return p;
        }

        #endregion
    }
}