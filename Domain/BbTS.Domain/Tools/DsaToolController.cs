﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BbTS.Client.Persona;
using BbTS.Core.Conversion;
using BbTS.Core.Security.Network;
using BbTS.Core.UI.Progress;
using BbTS.Domain.Models.Definitions;
using BbTS.Domain.Models.Definitions.Persona;
using BbTS.Domain.Models.System;
using BbTS.Domain.Persona.Code.Aspects;
using BbTS.Domain.Persona.Models;
using BbTS.Monitoring.Logging;
using User = BbTS.Domain.Persona.Models.User;

namespace BbTS.Domain.Tools
{
    public class DsaToolController
    {

        /// <summary>
        /// Get a diff report from the sync server at {hostname}.
        /// </summary>
        /// <param name="callingAppName">Name of the application making the request</param>
        /// <param name="hostname">hostname of the DsrSyncServer.</param>
        /// <param name="token">result token for the request</param>
        /// <param name="diff">diff report</param>
        /// <param name="message">results message</param>
        /// <returns></returns>
        public bool DiffReportGet(
            string callingAppName,
            string hostname,
            out ActionResultToken token,
            out DsrSyncResults diff,
            out string message)
        {
            token = null;
            diff = null;
            try
            {
                string uri = $"https://{hostname}/DsrSyncService/v1.0";
                PersonaClient client = new PersonaClient(
                   callingAppName,
                   NetworkTools.HostName(),
                   NetworkTools.LocalIpAddressGet(),
                   "",
                   "0C014D5C-E5C6-437B-8C9A-19299871DDA7",
                   "N5RgocJQfSLTOsA5hOHZRbslUM5gMobN+8BnqHVEuVRLFZf63RVUbdaFnkpKShpT",
                   uri);

                DsrSyncResults view;
                token = client.SendDiffRequest("", out view, "dsr");
                if (token.IsResult((int)DomainValue.DomainPersona, (int)PersonaActionResultDomainIdCode.Success))
                {
                    diff = view;
                }
            }
            catch (Exception ex)
            {
                message =
                    $"DiffReportGet: failed to retrieve diff report for {hostname}.  {Formatting.FormatException(ex)}.";
                return false;
            }

            message = "Success";
            return true;
        }

        /// <summary>
        /// Get a list of access points the user has rights on.
        /// </summary>
        /// <param name="view">System view</param>
        /// <param name="guid">guid of the user</param>
        /// <param name="userAccessPointMap">Access Points the user has permissions on</param>
        /// <param name="message">results message</param>
        /// <param name="progress">optional progress tracker</param>
        /// <returns></returns>
        public bool GetUserAccessPointsByGuid(
            DsrSystemView view,
            string guid,
            out Dictionary<User, AccessPointList> userAccessPointMap,
            out string message,
            ProgressTracker progress = null)
        {
            userAccessPointMap = new Dictionary<User, AccessPointList>();
            try
            {
                var user = view.Users.FirstOrDefault(u => u.Guid.ToLower() == guid.ToLower());
                if (user == null)
                {
                    message = $"GetUserAccessPoints: failed to user {guid} in the provided view.";
                    return false;
                }
                var userAuths = view.Authorizations.FindAll(auth => auth.UserGuids.Contains(guid));
                userAccessPointMap =
                    new Dictionary<User, AccessPointList>
                    {
                        {
                            user,
                            new AccessPointList(
                                view.AccessPoints.Where(
                                    ap => userAuths.Any(auth => auth.AccessPointGuids.Contains(ap.Guid))))
                        }
                    };
            }
            catch (Exception ex)
            {
                message =
                    $"GetUserAccessPoints: failed to locate locks for user {guid}.  {Formatting.FormatException(ex)}.";
                return false;
            }

            message = "Success";
            return true;
        }

        /// <summary>
        /// Get a list of access points the user has rights on.
        /// </summary>
        /// <param name="view">System view</param>
        /// <param name="credential">(full or partial) credential of the user</param>
        /// <param name="userAccessPointMap">Dictionary map of user credential matches to access points the user has permissions on</param>
        /// <param name="message">results message</param>
        /// <param name="progress">optional progress tracker</param>
        /// <returns></returns>
        [Timing]
        public bool GetUserAccessPointsByCredential(
            DsrSystemView view,
            string credential,
            out Dictionary<User, AccessPointList> userAccessPointMap,
            out string message,
            ProgressTracker progress = null)
        {
            userAccessPointMap = new Dictionary<User, AccessPointList>();
            try
            {
                var users =
                    new UserList(
                        view.Users.Where(u => u.Credentials.Any(cred => cred.Value.StringValue == credential || cred.Value.StringValue.Contains(credential))));

                ConcurrentDictionary<User,AccessPointList> map = new ConcurrentDictionary<User, AccessPointList>();
                progress?.Reset(0, Math.Max(1, users.Count));

                CancellationTokenSource token = new CancellationTokenSource();
                if(progress != null) progress.CancellationToken = token;
                try
                {
                    ParallelOptions options = new ParallelOptions { CancellationToken = token.Token };
                    Parallel.ForEach(users, options, user => _parallelFetchUserAccessPoints(view, user, map, progress));
                }
                catch (OperationCanceledException)
                {
                    //ignore
                }
                finally
                {
                    token.Dispose();
                }

                //Parallel.ForEach(sublists, list => _parallelGroupFetchUserAccessPoints(view, list, map, progress));
                userAccessPointMap = map.ToDictionary(item => item.Key, item => item.Value);
            }
            catch (Exception ex)
            {
                message =
                    $"GetUserAccessPointsByCredential: failed to locate locks for user with credential (or partial) {credential}.  {Formatting.FormatException(ex)}";
                return false;
            }

            message = "Success";
            return true;
        }
        
        /// <summary>
        /// Fetch access points for a single user.  Called from parallel launch point.
        /// </summary>
        /// <param name="view">System view</param>
        /// <param name="user">user to fetch access points for</param>
        /// <param name="map">Dictionary map of user credential matches to access points the user has permissions on</param>
        /// <param name="progress">optional progress tracker</param>
        private void _parallelFetchUserAccessPoints(
            DsrSystemView view,
            User user,
            ConcurrentDictionary<User, AccessPointList> map,
            ProgressTracker progress = null)
        {
            try
            {
                var userAuths = view.Authorizations.Where(auth => auth.UserGuids.Contains(user.Guid));

                var accessPoints =
                    new AccessPointList(
                        view.AccessPoints.Where(ap => userAuths.Any(auth => auth.AccessPointGuids.Contains(ap.Guid))));

                map.TryAdd(user, accessPoints);
                progress?.Increment();

            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(ex, "", "", EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// Get all users on a lock by guid
        /// </summary>
        /// <param name="view">System view</param>
        /// <param name="guid">Access point guid</param>
        /// <param name="apUsersDictionary">Users with permissions on the lock</param>
        /// <param name="message">Results message</param>
        /// <param name="progress">optional progress tracker</param>
        /// <returns></returns>
        public bool GetUsersOnAccessPointByGuid(
            DsrSystemView view,
            string guid,
            out Dictionary<AccessPoint, UserList> apUsersDictionary,
            out string message,
            ProgressTracker progress = null)
        {
            apUsersDictionary = new Dictionary<AccessPoint, UserList>();
            try
            {
                var accessPoint = view.AccessPoints.FirstOrDefault(ap => ap.Guid == guid);
                if (accessPoint == null)
                {
                    message = $"GetUsersOnAccessPointByGuid: failed to find access point {guid} in the provided view.";
                    return false;
                }
                var apAuths = view.Authorizations.FindAll(auth => auth.AccessPointGuids.Contains(guid));
                var users = new UserList(view.Users.Where(user => apAuths.Any(auth => auth.UserGuids.Contains(user.Guid))));

                apUsersDictionary.Add(accessPoint, users);
            }
            catch (Exception ex)
            {
                message =
                    $"GetUsersOnAccessPointByGuid: failed to locate users for lock with guid {guid}.  {Formatting.FormatException(ex)}.";
                return false;
            }
            message = "Success";
            return true;
        }

        /// <summary>
        /// Fetch users for one or more access points.
        /// </summary>
        /// <param name="view">System view</param>
        /// <param name="serialNumber">access point to fetch users for</param>
        /// <param name="apUsersDictionary">Dictionary map of serial number matches to users they contain</param>
        /// <param name="message">Operation result message.</param>
        /// <param name="progress">optional progress tracker</param>
        /// <returns></returns>
        public bool GetUsersOnAccessPointBySerialNumber(
            DsrSystemView view,
            string serialNumber,
            out Dictionary<AccessPoint, UserList> apUsersDictionary,
            out string message,
            ProgressTracker progress = null)
        {
            apUsersDictionary = new Dictionary<AccessPoint, UserList>();
            try
            {
                var apList = new AccessPointList(
                    view.AccessPoints.Where(ap => ap.SerialNumber == serialNumber || ap.SerialNumber.Contains(serialNumber)));
               
                ConcurrentDictionary<AccessPoint, UserList> map = new ConcurrentDictionary<AccessPoint, UserList>();
                progress?.Reset(0, Math.Max(1, apList.Count));

                CancellationTokenSource token = new CancellationTokenSource();
                if (progress != null) progress.CancellationToken = token;
                try
                {
                    ParallelOptions options = new ParallelOptions { CancellationToken = token.Token };
                    Parallel.ForEach(apList, options, accessPoint => _parallelFetchAccessPointUsers(view, accessPoint, map, progress));
                }
                catch (OperationCanceledException)
                {
                    //ignore
                }
                finally
                {
                    token.Dispose();
                }
                
                apUsersDictionary = map.ToDictionary(item => item.Key, item => item.Value);
            }
            catch (Exception ex)
            {
                message =
                    $"GetUsersOnAccessPointBySerialNumber: failed to locate users for lock with serial number (or partial) {serialNumber}.  {Formatting.FormatException(ex)}";
                return false;
            }

            message = "Success";
            return true;
        }

        /// <summary>
        /// Fetch users for one or more access points.  Called from parallel launch point.
        /// </summary>
        /// <param name="view">System view</param>
        /// <param name="ap">access point to fetch users for</param>
        /// <param name="map">Dictionary map of serial number matches to users they contain</param>
        /// <param name="progress">optional progress tracker</param>
        private void _parallelFetchAccessPointUsers(
            DsrSystemView view,
            AccessPoint ap,
            ConcurrentDictionary<AccessPoint, UserList> map,
            ProgressTracker progress = null)
        {
            try
            {
                var apAuths = view.Authorizations.FindAll(auth => auth.AccessPointGuids.Contains(ap.Guid));
                var users = new UserList(view.Users.Where(user => apAuths.Any(auth => auth.UserGuids.Contains(user.Guid))));

                map.TryAdd(ap, users);
                progress?.Increment();

            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(ex, "", "", EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// Fetch users for one or more access points.
        /// </summary>
        /// <param name="view">System view</param>
        /// <param name="type">The type to filter on.</param>
        /// <param name="authorizations">List of authorizations matching the requested type.</param>
        /// <param name="message">Operation result message.</param>
        /// <param name="progress">optional progress tracker</param>
        /// <returns></returns>
        public bool AuthorizationsByTypeGet(
            DsrSystemView view,
            DsrAuthorizationType type,
            out List<Authorization> authorizations,
            out string message,
            ProgressTracker progress = null)
        {
            authorizations = new List<Authorization>();
            try
            {
                authorizations = view.Authorizations.FindAll(a => a.AuthorizationTypes.Contains(type.ToString()));

                var map = new ConcurrentDictionary<string, Authorization>();
                progress?.Reset(0, Math.Max(1, authorizations.Count));

                var token = new CancellationTokenSource();
                if (progress != null) progress.CancellationToken = token;
                try
                {
                    var options = new ParallelOptions { CancellationToken = token.Token };
                    Parallel.ForEach(authorizations, options, authorization => _parallelAuthorizationByTypeGet(authorization, type, map, progress));
                }
                catch (OperationCanceledException)
                {
                    //ignore
                }
                finally
                {
                    token.Dispose();
                }

                authorizations = map.Values.ToList();
            }
            catch (Exception ex)
            {
                message =
                    $"AuthorizationsByTypeGet: failed to apply filter for type '{type.ToString()}'.  Error reported was: {Formatting.FormatException(ex)}";
                return false;
            }

            message = "Success";
            return true;
        }

        /// <summary>
        /// Fetch users for one or more access points.  Called from parallel launch point.
        /// </summary>
        /// <param name="authorization">Authorization to check.</param>
        /// <param name="type">The type to check for.</param>
        /// <param name="map">Dictionary map of serial number matches to users they contain</param>
        /// <param name="progress">optional progress tracker</param>
        private void _parallelAuthorizationByTypeGet(
            Authorization authorization,
            DsrAuthorizationType type,
            ConcurrentDictionary<string, Authorization> map,
            ProgressTracker progress = null)
        {
            try
            {
                if (authorization.AuthorizationTypes.Contains(type.ToString()))
                {
                    if (!map.TryAdd(authorization.Guid, authorization))
                    {
                        var name = $"{authorization.Guid}_{DateTimeOffset.Now.ToUnixTimeMilliseconds().ToString()}";
                        if (map.ContainsKey(authorization.Guid))
                        {
                            LoggingManager.Instance.LogException(
                                $"Failed to add authorization '{authorization.Guid}' because it already exists. Adding authorization as {name}",
                                EventLogEntryType.Warning);

                            if (!map.TryAdd(name, authorization))
                            {
                                LoggingManager.Instance.LogException(
                                    $"Failed to add authorization '{name}' for unknown reasons.",
                                    EventLogEntryType.Error);
                            }
                        }
                    }
                }

                progress?.Increment();

            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(ex, "", "", EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// Fetch users for one or more access points.
        /// </summary>
        /// <param name="view">System view</param>
        /// <param name="scheduleGuidPartial">Full or partial schedule guid to search for.</param>
        /// <param name="authorizations">List of authorizations matching the requested type.</param>
        /// <param name="message">Operation result message.</param>
        /// <param name="progress">optional progress tracker</param>
        /// <returns></returns>
        public bool AuthorizationsByScheduleGuidGet(
            DsrSystemView view,
            string scheduleGuidPartial,
            out List<Authorization> authorizations,
            out string message,
            ProgressTracker progress = null)
        {
            authorizations = new List<Authorization>();
            try
            {
                var map = new ConcurrentDictionary<string, Authorization>();
                progress?.Reset(0, Math.Max(1, view.Authorizations.Count));

                var token = new CancellationTokenSource();
                if (progress != null) progress.CancellationToken = token;
                try
                {
                    var options = new ParallelOptions { CancellationToken = token.Token };
                    Parallel.ForEach(view.Authorizations, options, authorization => _parallelAuthorizationByScheduleGuidGet(authorization, scheduleGuidPartial, map, progress));
                }
                catch (OperationCanceledException)
                {
                    //ignore
                }
                finally
                {
                    token.Dispose();
                }

                authorizations = map.Values.ToList();
            }
            catch (Exception ex)
            {
                message =
                    $"AuthorizationsByScheduleGuidGet: failed to apply filter for schedule partial '{scheduleGuidPartial}'.  Error reported was: {Formatting.FormatException(ex)}";
                return false;
            }

            message = "Success";
            return true;
        }

        /// <summary>
        /// Fetch users for one or more access points.  Called from parallel launch point.
        /// </summary>
        /// <param name="authorization">Authorization to check.</param>
        /// <param name="scheduleGuidPartial">Full or partial schedule guid to search for.</param>
        /// <param name="map">Dictionary map of serial number matches to users they contain</param>
        /// <param name="progress">optional progress tracker</param>
        private void _parallelAuthorizationByScheduleGuidGet(
            Authorization authorization,
            string scheduleGuidPartial,
            ConcurrentDictionary<string, Authorization> map,
            ProgressTracker progress = null)
        {
            try
            {

                if (authorization.ScheduleGuid.Contains(scheduleGuidPartial) && !map.ContainsKey(authorization.Guid))
                {
                    map.TryAdd(authorization.Guid, authorization);
                }

                progress?.Increment();

            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(ex, "", "", EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// Fetch users for one or more access points.
        /// </summary>
        /// <param name="view">System view</param>
        /// <param name="guid">Full or partial guid to search for.</param>
        /// <param name="authorizations">List of authorizations matching the requested type.</param>
        /// <param name="message">Operation result message.</param>
        /// <param name="progress">optional progress tracker</param>
        /// <returns></returns>
        public bool AuthorizationsByUserGuidGet(
            DsrSystemView view,
            string guid,
            out List<Authorization> authorizations,
            out string message,
            ProgressTracker progress = null)
        {
            authorizations = new List<Authorization>();
            try
            {
                var map = new ConcurrentDictionary<string, Authorization>();
                progress?.Reset(0, Math.Max(1, view.Authorizations.Count));

                var token = new CancellationTokenSource();
                if (progress != null) progress.CancellationToken = token;
                try
                {
                    var options = new ParallelOptions { CancellationToken = token.Token };
                    Parallel.ForEach(view.Authorizations, options, authorization => _parallelAuthorizationByUserGuidGet(authorization, guid, map, progress));
                }
                catch (OperationCanceledException)
                {
                    //ignore
                }
                finally
                {
                    token.Dispose();
                }

                authorizations = map.Values.ToList();
            }
            catch (Exception ex)
            {
                message =
                    $"AuthorizationsByUserGuidGet: failed to apply filter for user partial '{guid}'.  Error reported was: {Formatting.FormatException(ex)}";
                return false;
            }

            message = "Success";
            return true;
        }

        /// <summary>
        /// Fetch users for one or more access points.  Called from parallel launch point.
        /// </summary>
        /// <param name="authorization">Authorization to check.</param>
        /// <param name="guid">Full or partial guid to search for.</param>
        /// <param name="map">Dictionary map of serial number matches to users they contain</param>
        /// <param name="progress">optional progress tracker</param>
        private void _parallelAuthorizationByUserGuidGet(
            Authorization authorization,
            string guid,
            ConcurrentDictionary<string, Authorization> map,
            ProgressTracker progress = null)
        {
            try
            {
                var userGuid = authorization.UserGuids.FirstOrDefault(a => a.Contains(guid));

                if (!string.IsNullOrWhiteSpace(userGuid) && !map.ContainsKey(authorization.Guid))
                {
                    map.TryAdd(authorization.Guid, authorization);
                }

                progress?.Increment();

            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(ex, "", "", EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// Fetch users for one or more access points.
        /// </summary>
        /// <param name="view">System view</param>
        /// <param name="guid">Full or partial guid to search for.</param>
        /// <param name="authorizations">List of authorizations matching the requested type.</param>
        /// <param name="message">Operation result message.</param>
        /// <param name="progress">optional progress tracker</param>
        /// <returns></returns>
        public bool AuthorizationsByApGuidGet(
            DsrSystemView view,
            string guid,
            out List<Authorization> authorizations,
            out string message,
            ProgressTracker progress = null)
        {
            authorizations = new List<Authorization>();
            try
            {
                var map = new ConcurrentDictionary<string, Authorization>();
                progress?.Reset(0, Math.Max(1, view.Authorizations.Count));

                var token = new CancellationTokenSource();
                if (progress != null) progress.CancellationToken = token;
                try
                {
                    var options = new ParallelOptions { CancellationToken = token.Token };
                    Parallel.ForEach(view.Authorizations, options, authorization => _parallelAuthorizationByApGuidGet(authorization, guid, map, progress));
                }
                catch (OperationCanceledException)
                {
                    //ignore
                }
                finally
                {
                    token.Dispose();
                }

                authorizations = map.Values.ToList();
            }
            catch (Exception ex)
            {
                message =
                    $"AuthorizationsByApGuidGet: failed to apply filter for access point partial '{guid}'.  Error reported was: {Formatting.FormatException(ex)}";
                return false;
            }

            message = "Success";
            return true;
        }

        /// <summary>
        /// Fetch users for one or more access points.  Called from parallel launch point.
        /// </summary>
        /// <param name="authorization">Authorization to check.</param>
        /// <param name="guid">Full or partial guid to search for.</param>
        /// <param name="map">Dictionary map of serial number matches to users they contain</param>
        /// <param name="progress">optional progress tracker</param>
        private void _parallelAuthorizationByApGuidGet(
            Authorization authorization,
            string guid,
            ConcurrentDictionary<string, Authorization> map,
            ProgressTracker progress = null)
        {
            try
            {
                var userGuid = authorization.AccessPointGuids.FirstOrDefault(a => a.Contains(guid));

                if (!string.IsNullOrWhiteSpace(userGuid) && !map.ContainsKey(authorization.Guid))
                {
                    map.TryAdd(authorization.Guid, authorization);
                }

                progress?.Increment();

            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(ex, "", "", EventLogEntryType.Error);
            }
        }
    }
}
