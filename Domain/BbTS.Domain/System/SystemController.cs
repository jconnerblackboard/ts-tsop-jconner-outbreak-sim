﻿using System;
using System.Linq;
using BbTS.Core.System.Time;
using BbTS.Domain.Models.System;
using BbTS.Domain.Resources;
using BbTS.Domain.System.Configuration;

namespace BbTS.Domain.System
{
    public class SystemController
    {
        /// <summary>
        /// Ping the resource layer.
        /// </summary>
        /// <returns></returns>
        public ActionResultToken PingGet()
        {
            return ResourceManager.Instance.Resource.PingGet();
        }

        /// <summary>
        /// Get the date and time from the current resource layer.
        /// </summary>
        /// <returns></returns>
        public DateTime DataResourceDateTimeGet()
        {
            return ResourceManager.Instance.Resource.DataResourceDateTimeGet();
        }

        /// <summary>
        /// Get the current timezone information from the resource layer.
        /// </summary>
        /// <returns></returns>
        public TerminalTimeZoneInformation TimeZoneInformationGet()
        {
            return DateTimeUtility.CreateTerminalTimeZoneInformation();
        }

        /// <summary>
        /// Get the transact api uri from the resource layer.
        /// </summary>
        /// <returns></returns>
        public string TransactApiUriGet()
        {
            ControlParameter parameter = new SystemConfigurationController().ControlParametersListGet("TransactApiUri").FirstOrDefault();
            return parameter == null ? null : parameter.ParameterValue;
        }
    }
}
