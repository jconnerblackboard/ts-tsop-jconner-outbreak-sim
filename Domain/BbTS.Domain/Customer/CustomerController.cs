﻿using System;
using BbTS.Domain.Models.Credential;
using BbTS.Domain.Models.Customer;
using BbTS.Domain.Models.Definitions.Customer;
using BbTS.Domain.Models.System;
using BbTS.Domain.Resources;

namespace BbTS.Domain.Customer
{
    public class CustomerController
    {
        /// <summary>
        /// Get customer information.
        /// </summary>
        /// <param name="username">user making the request</param>
        /// <param name="customerGuid">Customer guid</param>
        /// <returns></returns>
        public TsCustomer CustomerGet(string username, string customerGuid)
        {
            return ResourceManager.Instance.Resource.CustomerGet(username, customerGuid);
        }

        /// <summary>
        /// Set/Create a customer record
        /// </summary>
        /// <param name="username">user making the request</param>
        /// <param name="customer">Customer attributes</param>
        /// <returns></returns>
        public ActionResultToken CustomerSet(string username, TsCustomer customer)
        {
            return ResourceManager.Instance.Resource.CustomerSet(username, customer);
        }

        /// <summary>
        /// Delete a customer
        /// </summary>
        /// <param name="username">user making the request</param>
        /// <param name="customerGuid">Customer guid</param>
        /// <returns></returns>
        public ActionResultToken CustomerDelete(string username, string customerGuid)
        {
            return ResourceManager.Instance.Resource.CustomerDelete(username, CustomerIdGet(username, customerGuid));
        }

        /// <summary>
        /// Get customer credential attributes for the credential number
        /// </summary>
        /// <param name="username"></param>
        /// <param name="customerGuid"></param>
        /// <param name="credentialGuid">credential number</param>
        /// <returns></returns>
        public CredentialView CustomerCredentialGet(string username, string customerGuid, string credentialGuid)
        {
            return ResourceManager.Instance.Resource.CustomerCredentialGet(username, CustomerIdGet(username, customerGuid), credentialGuid);
        }

        /// <summary>
        /// Set a customer credential
        /// </summary>
        /// <param name="username">user that logged into transact</param>
        /// <param name="id">customer guid</param>
        /// <param name="credential">new crednetials</param>
        /// <returns>Result of the operation</returns>
        public CustomerCredentialResponse CustomerCredentialSet(string username, string id, CustomerCredentialRequest credential)
        {
            // Validate CustomerGuid
            Int32 customerId;
            try
            {
                customerId = CustomerIdGet(username, id);
            }
            catch (ArgumentException)
            {
                return new CustomerCredentialResponse
                {
                    ActionResultToken = DomainCustomerDefinitions.GetActionResultCode(CustomerActionResultDomainIdCode.CustomerNotFound),
                    Credential = String.Empty
                };
            }
            return ResourceManager.Instance.Resource.CustomerCredentialSet(username, customerId, credential);
        }

        /// <summary>
        /// Get mobile attributes for a customer
        /// </summary>
        /// <param name="username">user that logged into transact</param>
        /// <param name="id">customer guid</param>
        /// <returns></returns>
        public CustomerMobileAttributes CustomerMobileAttributesGet(string username, string id)
        {
            return ResourceManager.Instance.Resource.CustomerMobileAttributesGet(username, id);
        }

        /// <summary>
        /// Retire a customer credential
        /// </summary>
        /// <param name="username">user that logged into transact</param>
        /// <param name="customerId">customer guid</param>
        /// <param name="credentialGuid">customer credential number</param>
        /// <returns></returns>
        public ActionResultToken CustomerCredentialRetire(string username, string customerId, string credentialGuid)
        {
            return ResourceManager.Instance.Resource.CustomerCredentialRetire(username, customerId, credentialGuid);
        }

        /// <summary>
        /// Get the customer customerGuid from the customer guid
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="customerGuid">Guid of the customer</param>
        /// <returns>Id or ArgumentException if the customer is not found</returns>
        public Int32 CustomerIdGet(String userName, string customerGuid)
        {
            return ResourceManager.Instance.Resource.CustomerIdGet(userName, customerGuid);
        }

        /// <summary>
        /// Set a customer photo
        /// </summary>
        /// <param name="customerId">Id of the customer</param>
        /// <param name="photo">new customer photo</param>
        /// <returns></returns>
        public ActionResultToken CustomerPhotoSet(int customerId, byte[] photo)
        {
            return ResourceManager.Instance.Resource.CustomerPhotoSet(customerId, photo);
        }
    }
}
