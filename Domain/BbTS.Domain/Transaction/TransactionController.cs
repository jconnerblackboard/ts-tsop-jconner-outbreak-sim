﻿using System.Collections.Generic;
using BbTS.Domain.Models.PaymentExpress;
using BbTS.Domain.Models.Transaction;
using BbTS.Domain.Resources;

namespace BbTS.Domain.Transaction
{
    public class TransactionController
    {
        /// <summary>
        /// Get a list of all credit card transactions that are configured on the Bb Payment Gateway and
        /// are marked as unprocessed (i.e. not voided, etc.)
        /// </summary>
        /// <returns></returns>
        public List<CreditCardTransactionView> TransactionCreditCardUnprocessedGet()
        {
            return ResourceManager.Instance.Resource.TransactionCreditCardUnprocessedGet();
        }

        /// <summary>
        /// Get a list of EMV reconciliation transactions.
        /// </summary>
        /// <param name="userName">The name of the user making the request.</param>
        /// <param name="id">transaction to fetch; null for all transactions</param>
        /// <returns></returns>
        public List<PaymentExpressRequestResponseView> EmvReconciliationTransactionsGet(string userName, string id)
        {
            return ResourceManager.Instance.Resource.EmvReconciliationTransactionGet(userName, id);
        }

        /// <summary>
        /// Set a specific EMV reconciliation transaction
        /// </summary>
        /// <param name="value"></param>
        public void EmvReconciliationTransactionSet(PaymentExpressRequestResponseView value)
        {
            ResourceManager.Instance.Resource.EmvReconciliationTransactionSet(value);
        }
    }
}
