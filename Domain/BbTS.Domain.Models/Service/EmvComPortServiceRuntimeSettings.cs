﻿using System;
using BbTS.Domain.Models.Definitions.Terminal;

namespace BbTS.Domain.Models.Service
{
    public class EmvComPortServiceRuntimeSettings
    {
        public EmvPinPadType DeviceType { get; set; }
        public DateTime LastComPortNotFoundErrorTimestamp { get; set; }
        public DateTime FirstComPortNotFoundErrorTimestamp { get; set; }
        public EmvPinPadErrorState ErrorState { get; set; }
    }
}
