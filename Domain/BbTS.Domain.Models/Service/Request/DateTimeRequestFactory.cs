﻿using System.Collections.Specialized;

namespace BbTS.Domain.Models.Service.Request
{
    public class DateTimeRequestFactory
    {
        // ReSharper disable UnusedMember.Local
        private const string Post = "POST";
        private const string Get = "GET";
        private const string Put = "PUT";
        private const string Delete = "DELETE";
        // ReSharper restore UnusedMember.Local

        private readonly string _applicationName;
        private readonly string _osHostName;
        private readonly string _ipAddress;
        private readonly string _osUser;

        public string BaseUri { get; set; }

        public DateTimeRequestFactory(string applicationName, string osHostName, string ipAddress, string osUser, string baseUri)
        {
            _applicationName = applicationName;
            _osHostName = osHostName;
            _ipAddress = ipAddress;
            _osUser = osUser;
            BaseUri = baseUri;
        }

        /// <summary>
        /// Make the HTTP request headers
        /// </summary>
        /// <returns></returns>
        private NameValueCollection CreateHeaders()
        {
            return new NameValueCollection
            {
                { "BbTS", $"ApplicationName={_applicationName},OsHostName={_osHostName},IpAddress={_ipAddress},OsUser={_osUser}" }
            };
        }

        /// <summary>
        /// Set the parameters that are common to all the requests we make
        /// </summary>
        /// <param name="serviceApiRequest"></param>
        private void SetBaseParameters(OAuthSendDataXml serviceApiRequest)
        {
            serviceApiRequest.ContentType = "application/xml";
            serviceApiRequest.BaseUri = BaseUri;
            serviceApiRequest.UseRequestHeader = true;
            serviceApiRequest.UserAgent = _applicationName;
            serviceApiRequest.AdditionalHeaderParameters = CreateHeaders();
        }

        /// <summary>
        /// Create a new Ping request.  Tests connection to service.
        /// </summary>
        /// <returns></returns>
        public OAuthSendDataXml NewPing()
        {
            OAuthSendDataXml serviceApiRequest = new OAuthSendDataXml
            {
                Xml = string.Empty,
                Endpoint = "ping",
                Type = Get
            };

            SetBaseParameters(serviceApiRequest);
            return serviceApiRequest;
        }

        /// <summary>
        /// Create a new TerminalTimeZone request.  Retrieves time zone of the database server.
        /// </summary>
        /// <returns></returns>
        public OAuthSendDataXml NewTerminalTimeZoneRequest()
        {
            OAuthSendDataXml serviceApiRequest = new OAuthSendDataXml
            {
                Xml = string.Empty,
                Endpoint = "system/timezoneinfo",
                Type = Get
            };

            SetBaseParameters(serviceApiRequest);
            return serviceApiRequest;
        }

        /// <summary>
        /// Create a new DateTime request.  Retrieves the datetime of the database server.
        /// </summary>
        /// <returns></returns>
        public OAuthSendDataXml NewDateTimeRequest()
        {
            OAuthSendDataXml serviceApiRequest = new OAuthSendDataXml
            {
                Xml = string.Empty,
                Endpoint = "system/datetime",
                Type = Get
            };

            SetBaseParameters(serviceApiRequest);
            return serviceApiRequest;
        }
    }
}
