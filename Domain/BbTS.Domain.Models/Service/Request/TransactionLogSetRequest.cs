﻿namespace BbTS.Domain.Models.Service.Request
{
    /// <summary>
    /// Container class for a TransactionLogSet (POST) call to the Service Layer.
    /// </summary>
    public class TransactionLogSetRequest
    {
        /// <summary>
        /// The unique identifier for this request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The Transaction data associated with this request.
        /// </summary>
        public Container.Transaction Transaction { get; set; }

        /// <summary>
        /// Return a deep copy of this object.
        /// </summary>
        /// <returns><see cref="TransactionLogSetRequest"/> object that is a clone of this.</returns>
        public TransactionLogSetRequest Clone()
        {
            var clone = (TransactionLogSetRequest) MemberwiseClone();
            clone.Transaction = Transaction.Clone();
            return clone;
        }
    }
}
