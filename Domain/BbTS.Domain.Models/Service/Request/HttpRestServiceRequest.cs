﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Definitions.Service;
using BbTS.Domain.Models.General;

namespace BbTS.Domain.Models.Service.Request
{
    /// <summary>
    /// Class to facilitate making REST requests to an external service easier.
    /// </summary>
    public class HttpRestServiceRequest<T>
    {
        /// <summary>
        /// Base URI for the request. (https://hostname.com/)
        /// </summary>
        public string BaseUri { get; set; }

        /// <summary>
        /// The route for the request. (api/controller/action)
        /// </summary>
        public string Route { get; set; }

        /// <summary>
        /// <see cref="RequestHeaderSupportedContentType"/> object with the content type of the request.
        /// </summary>
        public RequestHeaderSupportedContentType ContentType { get; set; }

        /// <summary>
        /// <see cref="RequestHeaderSupportedContentType"/> object with the accept type of the request.
        /// </summary>
        public RequestHeaderSupportedContentType AcceptType { get; set; }

        /// <summary>
        /// <see cref="SupportedHttpRequestMethod"/> method to execute as.
        /// </summary>
        public SupportedHttpRequestMethod Method { get; set; }

        /// <summary>
        /// Information to be placed in the body of the request.  Disabled for GET type requests.
        /// </summary>
        public T Body { get; set; }

        /// <summary>
        /// Any custom header information to attach to the request
        /// </summary>
        public List<StringPair> CustomHeaders { get; set; } = new List<StringPair>();

        /// <summary>
        /// Sets timeout for current request
        /// </summary>
        public TimeSpan? Timeout { get; set; }
    }
}
