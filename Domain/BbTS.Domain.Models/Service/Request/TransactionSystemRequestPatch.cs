﻿using System;
using BbTS.Domain.Models.System;

namespace BbTS.Domain.Models.Service.Request
{
    /// <summary>
    /// Container class for a PATCH method request for a <see cref="TransactionSystem"/> object.
    /// </summary>
    public class TransactionSystemRequestPatch
    {

        /// <summary>
        ///     Gets or sets the TransactionSystem Id.
        /// </summary>
        /// <value>
        ///     The TransactionSystem Id.
        /// </value>
        public Guid TransactionSystemId { get; set; }

        /// <summary>
        /// The transaction system associated with this request.
        /// </summary>
        public PatchTransactionSystemRegistration TransactionSystem { get; set; } = new PatchTransactionSystemRegistration();
    }

    /// <summary>
    /// Transaction System model for PATCH operation during system registration.
    /// </summary>
    public class PatchTransactionSystemRegistration
    {
        /// <summary>
        /// Gets or sets the name of the license.
        /// </summary>
        /// <value>
        /// The name of the license.
        /// </value>
        public string LicenseName { get; set; }

        /// <summary>
        ///     Gets or sets the License Number.
        /// </summary>
        /// <value>
        ///     The License Number.
        /// </value>
        public string LicenseNumber { get; set; }

        /// <summary>
        ///     Gets or sets the Expiration Date.
        /// </summary>
        /// <value>
        ///     The expiration date.
        /// </value>
        public DateTimeOffset ExpirationDate { get; set; }
    }
}
