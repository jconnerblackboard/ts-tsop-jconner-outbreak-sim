﻿using System;
using BbTS.Domain.Models.Container;

namespace BbTS.Domain.Models.Service.Request
{
    /// <summary>
    /// Container class for a TransactionMf4100LogPost call to the Service Layer.  (Version 1)
    /// </summary>
    public class TransactionMf4100LogPostRequestV01
    {
        /// <summary>
        /// The unique identifier for this request.
        /// </summary>
        public Guid RequestId { get; set; }

        /// <summary>
        /// The Transaction data associated with this request.
        /// </summary>
        public TransactionMf4100ViewV01 Transaction { get; set; }
    }
}