﻿using System;
using BbTS.Domain.Models.System;

namespace BbTS.Domain.Models.Service.Request
{
    /// <summary>
    /// Container class for a POST method request for a <see cref="TransactionSystem"/>
    /// </summary>
    public class TransactionSystemRequestPost
    {
        /// <summary>
        /// Gets or Sets the transaction system associated with this request.
        /// </summary>
        public TransactionSystem TransactionSystem { get; set; } = new TransactionSystem();
    }
}
