﻿using System.Collections.Specialized;

namespace BbTS.Domain.Models.Service.Request
{
    public class OAuthSendDataXml
    {
        public string Xml { get; set; }
        public string Endpoint { get; set; }
        public string Type { get; set; }
        public string ContentType { get; set; }
        public string BaseUri { get; set; }
        public bool UseRequestHeader { get; set; }
        public string UserAgent { get; set; }
        public NameValueCollection AdditionalHeaderParameters { get; set; }
    }
}