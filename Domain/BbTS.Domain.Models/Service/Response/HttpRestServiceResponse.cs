﻿using BbTS.Domain.Models.Service.Request;

namespace BbTS.Domain.Models.Service.Response
{
    /// <summary>
    /// Class to facility processing REST responses from an external service easier.
    /// </summary>
    public class HttpRestServiceResponse<T>
    {
        /// <summary>
        /// The <see cref="HttpRestServiceRequest{T}"/> that spawned this response.
        /// </summary>
        public HttpRestServiceRequest<T> Request { get; internal set; } 

        /// <summary>
        /// The body content of the response as a string and the headers.
        /// </summary>
        public HttpServiceResponse Response { get; set; }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="request">The <see cref="HttpRestServiceRequest{T}"/> that spawned this response.</param>
        public HttpRestServiceResponse (HttpRestServiceRequest<T> request)
        {
            Request = request;
        }
    }
}
