﻿using System;
using BbTS.Core.Serialization;
using BbTS.Domain.Models.System;

namespace BbTS.Domain.Models.Service.Response
{
    public class DateTimeResponse : IResponseDeserializer
    {
        public DateTime ServerDateTime { get; set; }

        public ActionResultToken Result { get; set; }
        public bool ResponseContainsResult { get; set; }

        public void Deserialize(string responseOutput)
        {
            ServerDateTime = Xml.Deserialize<DateTime>(responseOutput);
            ResponseContainsResult = false;
        }
    }
}
