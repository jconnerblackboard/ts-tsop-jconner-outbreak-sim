﻿using BbTS.Domain.Models.System;

namespace BbTS.Domain.Models.Service.Response
{
    public interface IResponseDeserializer
    {
        /// <summary>
        /// The ActionResultToken that indicates the Result of the operation.
        /// </summary>
        ActionResultToken Result { get; set; }

        /// <summary>
        /// True if the deserialized responseOutput contains the ActionResultToken value and has been assigned to Result.
        /// </summary>
        bool ResponseContainsResult { get; set; }

        /// <summary>
        /// Converts the XML returned from the TransactAPI request into an object of the appropriate type.
        /// </summary>
        /// <param name="responseOutput"></param>
        void Deserialize(string responseOutput);
    }
}