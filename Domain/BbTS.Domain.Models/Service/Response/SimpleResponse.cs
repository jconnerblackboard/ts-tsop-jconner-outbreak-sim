﻿using BbTS.Core.Serialization;
using BbTS.Domain.Models.System;

namespace BbTS.Domain.Models.Service.Response
{
    /// <summary>
    /// A simple response consisting of just an ActionResultToken.
    /// </summary>
    public class SimpleResponse : IResponseDeserializer
    {
        public ActionResultToken Result { get; set; }
        public bool ResponseContainsResult { get; set; }

        public void Deserialize(string responseOutput)
        {
            Result = Xml.Deserialize<ActionResultToken>(responseOutput);
            ResponseContainsResult = true;
        }
    }
}
