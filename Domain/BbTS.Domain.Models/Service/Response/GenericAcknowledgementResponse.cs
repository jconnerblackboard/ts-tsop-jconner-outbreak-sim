﻿using System;

namespace BbTS.Domain.Models.Service.Response
{
    /// <summary>
    /// Container class for a generic acknowlegement response.
    /// </summary>
    public class GenericAcknowledgementResponse
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public Guid RequestId { get; set; }
    }

    /// <summary>
    /// View for a <see cref="GenericAcknowledgementResponse"/>.  (Version 1)
    /// </summary>
    public class GenericAcknowledgementResponseV01
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public Guid RequestId { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="GenericAcknowledgementResponse"/> conversion.
    /// </summary>
    public static class GenericAcknowlegementResponseConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="GenericAcknowledgementResponseV01"/> object based on this <see cref="GenericAcknowledgementResponse"/>.
        /// </summary>
        /// <param name="genericAcknowledgementResponse"></param>
        /// <returns></returns>
        public static GenericAcknowledgementResponseV01 ToGenericAcknowledgementResponseViewV01(this GenericAcknowledgementResponse genericAcknowledgementResponse)
        {
            if (genericAcknowledgementResponse == null) return null;

            return new GenericAcknowledgementResponseV01
            {
                RequestId = genericAcknowledgementResponse.RequestId
            };
        }

        /// <summary>
        /// Returns a <see cref="GenericAcknowledgementResponse"/> object based on this <see cref="GenericAcknowledgementResponseV01"/>.
        /// </summary>
        /// <param name="genericAcknowledgementResponseV01"></param>
        /// <returns></returns>
        public static GenericAcknowledgementResponse ToGenericAcknowledgementResponse(this GenericAcknowledgementResponseV01 genericAcknowledgementResponseV01)
        {
            if (genericAcknowledgementResponseV01 == null) return null;

            return new GenericAcknowledgementResponse
            {
                RequestId = genericAcknowledgementResponseV01.RequestId
            };
        }
        #endregion
    }
}