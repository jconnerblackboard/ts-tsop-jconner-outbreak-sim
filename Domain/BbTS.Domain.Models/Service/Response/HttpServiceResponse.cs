﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Service.Response
{
    /// <summary>
    /// Class to encapsulate base elements of an Http Response
    /// </summary>
    public class HttpServiceResponse
    {
        /// <summary>
        /// The body content of the response.
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// The headers of the response.
        /// </summary>
        public HttpHeaders Headers { get; set; }

        /// <summary>
        /// The response code.
        /// </summary>
        public HttpStatusCode Code { get; set; }
    }
}
