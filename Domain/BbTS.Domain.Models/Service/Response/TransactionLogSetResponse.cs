﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Service.Request;

namespace BbTS.Domain.Models.Service.Response
{
    /// <summary>
    /// Container class for a TransactionLogSet request response.
    /// </summary>
    public class TransactionLogSetResponse
    {
        /// <summary>
        /// The initial TransactionLogSet request.
        /// </summary>
        public TransactionLogSetRequest Request { get; set; }

        /// <summary>
        /// The Transaction object after any alterations made by the domain/resource layer.
        /// </summary>
        public Container.Transaction Transaction { get; set; }
    }
}
