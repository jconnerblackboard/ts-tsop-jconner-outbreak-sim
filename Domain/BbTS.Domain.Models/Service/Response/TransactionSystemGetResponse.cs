﻿using BbTS.Domain.Models.System;
using System.Collections.Generic;

namespace BbTS.Domain.Models.Service.Response
{
    /// <summary>
    /// Container class for a response to a request that returns a transaction system.
    /// </summary>
    public class TransactionSystemGetResponse
    {
        /// <summary>
        /// Transaction system assigned to this transaction system response.
        /// </summary>
        public List<TransactionSystem> TransactionSystems { get; set; }
    }
}
