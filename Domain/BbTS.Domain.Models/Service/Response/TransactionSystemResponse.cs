﻿using BbTS.Domain.Models.System;

namespace BbTS.Domain.Models.Service.Response
{
    /// <summary>
    /// Container class for a response to a request that returns a transaction system.
    /// </summary>
    public class TransactionSystemResponse
    {
        /// <summary>
        /// Transaction system assigned to this transaction system response.
        /// </summary>
        public TransactionSystem TransactionSystem { get; set; }
    }
}
