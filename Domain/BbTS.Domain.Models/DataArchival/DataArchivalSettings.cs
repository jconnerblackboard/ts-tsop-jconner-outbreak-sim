﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.DataArchival
{
    /// <summary>
    /// This method contains the properties necessary to control the DataArchivalSettings process
    /// </summary>
    [Serializable]
    public class DataArchivalSettings
    {
        /// <summary>
        /// The root directory for where the archive files are saved
        /// </summary>
        [XmlAttribute]
        public string RootDirectory { get; set; }

        /// <summary>
        /// The prefix given to the Xml file when data is archived
        /// </summary>
        [XmlAttribute]
        public string ZipFilePrefix { get; set; }

        /// <summary>
        /// Whether or not physical deletes are performed
        /// </summary>
        [XmlAttribute]
        public bool SimulationMode { get; set; }

        /// <summary>
        /// The list of TableArchiveGroups
        /// </summary>
        [XmlElement]
        public List<TableArchiveGroup> TableArchiveGroupList { get; set; }
       
    }

    /// <summary>
    /// A grouping of TableArchive objects
    /// </summary>
    public class TableArchiveGroup
    {
        /// <summary>
        /// The name of this table archive groups
        /// </summary>
        [XmlAttribute]
        public string Name { get; set; }
        /// <summary>
        /// The number of days data should be kept in the table
        /// </summary>
        [XmlAttribute]
        public int DaysInTable { get; set; }

        /// <summary>
        /// A list of table archive objects
        /// </summary>
        [XmlElement]
        public List<TableArchive> TableArchiveList { get; set; }
    }

    /// <summary>
    /// The attributes of a table archive object
    /// </summary>
    public class TableArchive
    {
        /// <summary>
        /// The name of the table being archived
        /// </summary>
        [XmlAttribute]
        public string Name { get; set; }
    }
}