﻿using BbTS.Domain.Models.Container;

namespace BbTS.Domain.Models.Transaction.WebApi
{
    /// <summary>
    /// Container class for the response to a transaction convert request.
    /// </summary>
    public class TransactionConvertResponse
    {
        /// <summary>
        /// Unique identifier for the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The converted product.
        /// </summary>
        public TransactionViewV01 Transaction { get; set; }
    }
}
