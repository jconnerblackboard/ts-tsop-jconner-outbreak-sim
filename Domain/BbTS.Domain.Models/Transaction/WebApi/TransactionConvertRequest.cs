﻿namespace BbTS.Domain.Models.Transaction.WebApi
{
    /// <summary>
    /// Container class for a request to convert a transaciton into another format.
    /// </summary>
    public class TransactionConvertRequest
    {
        /// <summary>
        /// The unique identifier for the transaction.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The transaction to convert.
        /// </summary>
        public Container.Transaction Transaction { get; set; }
    }
}
