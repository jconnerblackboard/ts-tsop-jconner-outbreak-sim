﻿using BbTS.Domain.Models.Definitions.Pos;

namespace BbTS.Domain.Models.Transaction.Processing
{
    /// <summary>
    /// Container class for a board transaction processing result.
    /// </summary>
    public class BoardTransactionProcessingResult : ProcessingResult
    {
        /// <summary>
        /// Regular meal grid count left
        /// </summary>
        public int RegularGridLeft { get; set; }

        /// <summary>
        /// Guest meal grid count left
        /// </summary>
        public int GuestGridLeft { get; set; }

        /// <summary>
        /// Period meal count left
        /// </summary>
        public int PeriodLeft { get; set; }

        /// <summary>
        /// Day meal count left
        /// </summary>
        public int DayLeft { get; set; }

        /// <summary>
        /// Week meal count left
        /// </summary>
        public int WeekLeft { get; set; }

        /// <summary>
        /// Month meal count left
        /// </summary>
        public int MonthLeft { get; set; }

        /// <summary>
        /// Semester/Quarter meal count left
        /// </summary>
        public int SemesterQuarterLeft { get; set; }

        /// <summary>
        /// Year meal count left
        /// </summary>
        public int YearLeft { get; set; }

        /// <summary>
        /// Guest meal total limit left
        /// </summary>
        public int GuestTotalLeft { get; set; }

        /// <summary>
        /// Extra meals left
        /// </summary>
        public int ExtraLeft { get; set; }

        /// <summary>
        /// Transfer meals left
        /// </summary>
        public int TransferLeft { get; set; }

        /// <summary>
        /// Meal Type used.
        /// 1 -- Regular, no Extra or Transfer
        /// 2 -- Guest Meal
        /// 3 -- Extra
        /// 4 -- Transfer
        /// 5 -- Extra and Regular
        /// 6 -- Transfer and Regular
        /// 7 -- Extra and Transfer
        /// 8 -- Regular, Extra, and Transfer
        /// </summary>
        public int TypeUsed { get; set; }

        /// <summary>
        /// Display meals left by
        /// </summary>
        public MealsLeftBy DisplayMealsLeftBy { get; set; }

        /// <summary>
        /// Show board count
        /// </summary>
        public bool ShowBoardCount { get; set; }

        /// <summary>
        /// Print board count
        /// </summary>
        public bool PrintBoardCount { get; set; }

        /// <summary>
        /// Parameter-less constructor.
        /// </summary>
        public BoardTransactionProcessingResult()
        {
        }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="requestId">The unique identifier for the request that preempted the processing routine.</param>
        /// <param name="errorCode">The error code associated with the response.  0 means success.</param>
        /// <param name="deniedText">Reason for failure (if there was a failure error code).</param>
        public BoardTransactionProcessingResult(string requestId, int errorCode, string deniedText)
        {
            RequestId = requestId;
            ErrorCode = errorCode;
            DeniedText = deniedText;
        }
    }

    /// <summary>
    /// View for a <see cref="BoardTransactionProcessingResult"/>.  (Version 1)
    /// </summary>
    public class BoardTransactionProcessingResultViewV01
    {
        /// <summary>
        /// The unique identifier for the request that preempted the processing routine.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The error code associated with the response.  0 means success.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Reason for failure (if there was a failure error code).
        /// </summary>
        public string DeniedText { get; set; }

        /// <summary>
        /// Regular meal grid count left
        /// </summary>
        public int RegularGridLeft { get; set; }

        /// <summary>
        /// Guest meal grid count left
        /// </summary>
        public int GuestGridLeft { get; set; }

        /// <summary>
        /// Period meal count left
        /// </summary>
        public int PeriodLeft { get; set; }

        /// <summary>
        /// Day meal count left
        /// </summary>
        public int DayLeft { get; set; }

        /// <summary>
        /// Week meal count left
        /// </summary>
        public int WeekLeft { get; set; }

        /// <summary>
        /// Month meal count left
        /// </summary>
        public int MonthLeft { get; set; }

        /// <summary>
        /// Semester/Quarter meal count left
        /// </summary>
        public int SemesterQuarterLeft { get; set; }

        /// <summary>
        /// Year meal count left
        /// </summary>
        public int YearLeft { get; set; }

        /// <summary>
        /// Guest meal total limit left
        /// </summary>
        public int GuestTotalLeft { get; set; }

        /// <summary>
        /// Extra meals left
        /// </summary>
        public int ExtraLeft { get; set; }

        /// <summary>
        /// Transfer meals left
        /// </summary>
        public int TransferLeft { get; set; }

        /// <summary>
        /// Meal Type used.
        /// 1 -- Regular, no Extra or Transfer
        /// 2 -- Guest Meal
        /// 3 -- Extra
        /// 4 -- Transfer
        /// 5 -- Extra and Regular
        /// 6 -- Transfer and Regular
        /// 7 -- Extra and Transfer
        /// 8 -- Regular, Extra, and Transfer
        /// </summary>
        public int TypeUsed { get; set; }

        /// <summary>
        /// Display meals left by
        /// </summary>
        public MealsLeftBy DisplayMealsLeftBy { get; set; }

        /// <summary>
        /// Show board count
        /// </summary>
        public bool ShowBoardCount { get; set; }

        /// <summary>
        /// Print board count
        /// </summary>
        public bool PrintBoardCount { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="BoardTransactionProcessingResult"/> conversion.
    /// </summary>
    public static class BoardTransactionProcessingResultConverter
    {
        /// <summary>
        /// Returns a <see cref="BoardTransactionProcessingResultViewV01"/> object based on this <see cref="BoardTransactionProcessingResult"/>.
        /// </summary>
        /// <param name="boardTransactionProcessingResult"></param>
        /// <returns></returns>
        public static BoardTransactionProcessingResultViewV01 ToBoardTransactionProcessingResultViewV01(this BoardTransactionProcessingResult boardTransactionProcessingResult)
        {
            if (boardTransactionProcessingResult == null) return null;

            return new BoardTransactionProcessingResultViewV01
            {
                RequestId = boardTransactionProcessingResult.RequestId,
                ErrorCode = boardTransactionProcessingResult.ErrorCode,
                DeniedText = boardTransactionProcessingResult.DeniedText,
                RegularGridLeft = boardTransactionProcessingResult.RegularGridLeft,
                GuestGridLeft = boardTransactionProcessingResult.GuestGridLeft,
                PeriodLeft = boardTransactionProcessingResult.PeriodLeft,
                DayLeft = boardTransactionProcessingResult.DayLeft,
                WeekLeft = boardTransactionProcessingResult.WeekLeft,
                MonthLeft = boardTransactionProcessingResult.MonthLeft,
                SemesterQuarterLeft = boardTransactionProcessingResult.SemesterQuarterLeft,
                YearLeft = boardTransactionProcessingResult.YearLeft,
                GuestTotalLeft = boardTransactionProcessingResult.GuestTotalLeft,
                ExtraLeft = boardTransactionProcessingResult.ExtraLeft,
                TransferLeft = boardTransactionProcessingResult.TransferLeft,
                TypeUsed = boardTransactionProcessingResult.TypeUsed,
                DisplayMealsLeftBy = boardTransactionProcessingResult.DisplayMealsLeftBy,
                ShowBoardCount = boardTransactionProcessingResult.ShowBoardCount,
                PrintBoardCount = boardTransactionProcessingResult.PrintBoardCount
            };
        }

        /// <summary>
        /// Returns a <see cref="BoardTransactionProcessingResult"/> object based on this <see cref="BoardTransactionProcessingResultViewV01"/>.
        /// </summary>
        /// <param name="boardTransactionProcessingResultViewV01"></param>
        /// <returns></returns>
        public static BoardTransactionProcessingResult ToBoardTransactionProcessingResult(this BoardTransactionProcessingResultViewV01 boardTransactionProcessingResultViewV01)
        {
            if (boardTransactionProcessingResultViewV01 == null) return null;

            return new BoardTransactionProcessingResult
            {
                RequestId = boardTransactionProcessingResultViewV01.RequestId,
                ErrorCode = boardTransactionProcessingResultViewV01.ErrorCode,
                DeniedText = boardTransactionProcessingResultViewV01.DeniedText,
                RegularGridLeft = boardTransactionProcessingResultViewV01.RegularGridLeft,
                GuestGridLeft = boardTransactionProcessingResultViewV01.GuestGridLeft,
                PeriodLeft = boardTransactionProcessingResultViewV01.PeriodLeft,
                DayLeft = boardTransactionProcessingResultViewV01.DayLeft,
                WeekLeft = boardTransactionProcessingResultViewV01.WeekLeft,
                MonthLeft = boardTransactionProcessingResultViewV01.MonthLeft,
                SemesterQuarterLeft = boardTransactionProcessingResultViewV01.SemesterQuarterLeft,
                YearLeft = boardTransactionProcessingResultViewV01.YearLeft,
                GuestTotalLeft = boardTransactionProcessingResultViewV01.GuestTotalLeft,
                ExtraLeft = boardTransactionProcessingResultViewV01.ExtraLeft,
                TransferLeft = boardTransactionProcessingResultViewV01.TransferLeft,
                TypeUsed = boardTransactionProcessingResultViewV01.TypeUsed,
                DisplayMealsLeftBy = boardTransactionProcessingResultViewV01.DisplayMealsLeftBy,
                ShowBoardCount = boardTransactionProcessingResultViewV01.ShowBoardCount,
                PrintBoardCount = boardTransactionProcessingResultViewV01.PrintBoardCount
            };
        }
    }
}
