﻿using System;

namespace BbTS.Domain.Models.Transaction.Processing
{
    /// <summary>
    /// Container class for a board transaction request.
    /// </summary>
    public class BoardTransactionProcessRequest
    {
        /// <summary>
        /// The id of the request that spawned this response.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The unique identifier for the device.
        /// </summary>
        public string DeviceGuid { get; set; }

        /// <summary>
        /// The number of the transaction.
        /// </summary>
        public int TransactionNumber { get; set; }

        /// <summary>
        /// The date and time the transaction took place
        /// </summary>
        public DateTimeOffset TransactionDateTime { get; set; }

        /// <summary>
        /// The card number used.
        /// </summary>
        public string CardNumber { get; set; }

        /// <summary>
        /// The issue number associated with the card number.
        /// </summary>
        public string IssueNumber { get; set; }

        /// <summary>
        /// Was the issue number captured?
        /// </summary>
        public bool IssueNumberCaptured { get; set; }

        /// <summary>
        /// The physical type of media used in the transaction.
        /// "0 - card (magstripe), 1 - contactless"
        /// </summary>
        public int PhysicalIdType { get; set; }

        /// <summary>
        /// The method used to capture the customer's credentials.
        /// "0 - swiped, 1 - manually entered"
        /// </summary>
        public int CustEntryMethodType { get; set; }

        /// <summary>
        /// Was a secondary auth type used?
        /// </summary>
        public bool SecondaryAuthEntered { get; set; }

        /// <summary>
        /// Was the transaction keyed offline.
        /// </summary>
        public bool KeyedOfflineFlag { get; set; }

        /// <summary>
        /// The unique identifier for the operator of the device.
        /// </summary>
        public string OperatorGuid { get; set; }

        /// <summary>
        /// MealType to use
        /// </summary>
        public int MealType { get; set; }
        /// <summary>
        /// Flag to indicate a Guest meal
        /// </summary>
        public bool GuestMealFlag { get; set; }
        /// <summary>
        /// Board count used
        /// </summary>
        public int CountUsed { get; set; }

        /// <summary>
        /// Force post.
        /// </summary>
        public bool IsForcePost { get; set; }
    }
}
