﻿using BbTS.Domain.Models.ArtsDataModel;

namespace BbTS.Domain.Models.Transaction.Processing
{
    /// <summary>
    /// Container class for a retail transaction line item tender surcharge request.
    /// </summary>
    public class RetailTransactionLineTenderSurchargeRequest : IProcessingRequest
    {
        /// <summary>
        /// The unique identifier for this request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The line item tender cash equivalence object from the transction object parent.
        /// </summary>
        public LineItemTenderSurcharge LineItemTenderSurcharge { get; set; }
    }
}