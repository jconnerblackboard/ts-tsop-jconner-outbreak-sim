﻿namespace BbTS.Domain.Models.Transaction.Processing
{
    /// <summary>
    /// Container class for a retail transaction line product tax request.
    /// </summary>
    public class RetailTransactionLineProductTaxRequest : IProcessingRequest
    {
        /// <summary>
        /// Unique identifier for the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The unique identifier for the transaction expressed as an integer.
        /// Number is created from the return value from a <see cref="RetailTransactionBeginProcessRequest"/>.
        /// </summary>
        public int TransactionId { get; set; }

        /// <summary>
        /// The order in which this line item is entered into the POS.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// The tax schedule id.
        /// </summary>
        public int TaxScheduleId { get; set; }

        /// <summary>
        /// The percentage of taxable amount.
        /// </summary>
        public decimal TaxablePercent { get; set; }

        /// <summary>
        /// The taxable amount of the transaction.
        /// </summary>
        public decimal TaxableAmount { get; set; }

        /// <summary>
        /// The  percentage of the taxable portion of the taxable amount that is being collected as tax by this LineItem.
        /// </summary>
        public decimal TaxPercent { get; set; }

        /// <summary>
        /// Total tax associated with this tender line item.  If null, information about the tax is not available.
        /// </summary>
        public decimal TaxAmount { get; set; }

        /// <summary>
        /// The type of tax exempt override.
        /// </summary>
        public int TaxExemptOverrideType { get; set; }

        /// <summary>
        /// Commit when successful?
        /// </summary>
        public bool OnSuccessCommit { get; set; }
    }
}
