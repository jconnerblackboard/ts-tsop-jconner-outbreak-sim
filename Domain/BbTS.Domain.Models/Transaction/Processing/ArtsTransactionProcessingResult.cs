﻿using System.Collections.Generic;
using System.Linq;

namespace BbTS.Domain.Models.Transaction.Processing
{
    /// <summary>
    /// Container class for the results of processing an arts transaction.
    /// </summary>
    public class ArtsTransactionProcessingResult : ProcessingResult
    {
        /// <summary>
        /// Results from processing the line items of the arts transaction.
        /// </summary>
        public List<LineItemProcessingResult> LineItemProcessingResults { get; set; }  = new List<LineItemProcessingResult>();

        /// <summary>
        /// Parameter-less constructor.
        /// </summary>
        public ArtsTransactionProcessingResult()
        {
        }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="requestId">The unique identifier for the request that preempted the processing routine.</param>
        /// <param name="errorCode">The error code associated with the response.  0 means success.</param>
        /// <param name="deniedText">Reason for failure (if there was a failure error code).</param>
        public ArtsTransactionProcessingResult(string requestId, int errorCode, string deniedText)
        {
            RequestId = requestId;
            ErrorCode = errorCode;
            DeniedText = deniedText;
        }
    }

    /// <summary>
    /// View for a <see cref="ArtsTransactionProcessingResult"/>.  (Version 1)
    /// </summary>
    public class ArtsTransactionProcessingResultViewV01
    {
        /// <summary>
        /// The unique identifier for the request that preempted the processing routine.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The error code associated with the response.  0 means success.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Reason for failure (if there was a failure error code).
        /// </summary>
        public string DeniedText { get; set; }

        /// <summary>
        /// Results from processing the line items of the arts transaction.
        /// </summary>
        public List<LineItemProcessingResultViewV01> LineItemProcessingResults { get; set; } = new List<LineItemProcessingResultViewV01>();
    }

    /// <summary>
    /// Extension methods for <see cref="ArtsTransactionProcessingResult"/> conversion.
    /// </summary>
    public static class ArtsTransactionProcessingResultConverter
    {
        /// <summary>
        /// Returns a <see cref="ArtsTransactionProcessingResultViewV01"/> object based on this <see cref="ArtsTransactionProcessingResult"/>.
        /// </summary>
        /// <param name="artsTransactionProcessingResult"></param>
        /// <returns></returns>
        public static ArtsTransactionProcessingResultViewV01 ToArtsTransactionProcessingResultViewV01(this ArtsTransactionProcessingResult artsTransactionProcessingResult)
        {
            if (artsTransactionProcessingResult == null) return null;

            return new ArtsTransactionProcessingResultViewV01
            {
                RequestId = artsTransactionProcessingResult.RequestId,
                ErrorCode = artsTransactionProcessingResult.ErrorCode,
                DeniedText = artsTransactionProcessingResult.DeniedText,
                LineItemProcessingResults = artsTransactionProcessingResult.LineItemProcessingResults.Select(lineItemProcessingResult => lineItemProcessingResult.ToLineItemProcessingResultViewV01()).ToList()
            };
        }

        /// <summary>
        /// Returns a <see cref="ArtsTransactionProcessingResult"/> object based on this <see cref="ArtsTransactionProcessingResultViewV01"/>.
        /// </summary>
        /// <param name="artsTransactionProcessingResultViewV01"></param>
        /// <returns></returns>
        public static ArtsTransactionProcessingResult ToArtsTransactionProcessingResult(this ArtsTransactionProcessingResultViewV01 artsTransactionProcessingResultViewV01)
        {
            if (artsTransactionProcessingResultViewV01 == null) return null;

            return new ArtsTransactionProcessingResult
            {
                RequestId = artsTransactionProcessingResultViewV01.RequestId,
                ErrorCode = artsTransactionProcessingResultViewV01.ErrorCode,
                DeniedText = artsTransactionProcessingResultViewV01.DeniedText,
                LineItemProcessingResults = artsTransactionProcessingResultViewV01.LineItemProcessingResults.Select(lineItemProcessingResult => lineItemProcessingResult.ToLineItemProcessingResult()).ToList()
            };
        }
    }
}
