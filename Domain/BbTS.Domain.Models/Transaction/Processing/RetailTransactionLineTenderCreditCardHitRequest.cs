﻿using System;
using BbTS.Domain.Models.ArtsDataModel;

namespace BbTS.Domain.Models.Transaction.Processing
{
    /// <summary>
    /// Container class for the request for the processing/validation of a retail transaction line item credit card.
    /// </summary>
    public class RetailTransactionLineTenderCreditCardHitRequest : IProcessingRequest
    {
        /// <summary>
        /// The unique identifier for this request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The line item tender emv associated with the request.
        /// </summary>
        public LineItemTenderEmvHit LineItemTenderEmvHit { get; set; }

        /// <summary>
        /// The numerical identifier for the POS that originated the request.
        /// </summary>
        public int TerminalId { get; set; }

        /// <summary>
        /// The date and time the transaction took place.
        /// </summary>
        public DateTime TransactionDateTime { get; set; }

        /// <summary>
        /// Commit when successful?
        /// </summary>
        public bool OnSuccessCommit { get; set; }
    }
}
