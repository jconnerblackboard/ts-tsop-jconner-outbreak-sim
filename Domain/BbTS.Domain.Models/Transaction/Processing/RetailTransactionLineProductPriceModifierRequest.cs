﻿namespace BbTS.Domain.Models.Transaction.Processing
{
    /// <summary>
    /// Container class for a retail transaction line product price modifier request.
    /// </summary>
    public class RetailTransactionLineProductPriceModifierRequest : IProcessingRequest
    {
        /// <summary>
        /// Unique identifier for the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The unique identifier for the transaction expressed as an integer.
        /// Number is created from the return value from a <see cref="RetailTransactionBeginProcessRequest"/>.
        /// </summary>
        public int TransactionId { get; set; }

        /// <summary>
        /// The order in which this line item is entered into the POS.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// The sequence number for this RetailPriceModifier allowing more than one price modification to occur on each retail transaction line item.
        /// </summary>
        public int PriceModifierSequenceNumber { get; set; }

        /// <summary>
        /// The Type of retail price modifier.
        /// 0 - Discount Key
        /// 1 - Surcharge Key
        /// 2 - Promotion
        /// 3 - Price Override
        /// 4 - Variable Price
        /// 5 - Tender Discount
        /// 6 - Tender Surcharge
        /// 7 - Quantity Discount
        /// </summary>
        public int RetailPriceModifierType { get; set; }

        /// <summary>
        /// The product promo identifier.
        /// </summary>
        public int ProductPromoId { get; set; }

        /// <summary>
        /// Indicates if there was a product promotion at the time of the transaction.
        /// </summary>
        public bool ProductPromoFlag { get; set; }

        /// <summary>
        /// The unit price that was used as the basis of the price modification.
        /// </summary>
        public decimal PreviousPrice { get; set; }

        /// <summary>
        /// The percent adjustment that was applied to the unit retail price to arrive at the modified selling price.
        /// </summary>
        public decimal Percent { get; set; }

        /// <summary>
        /// The flat amount of the price adjustment that was removed from the unit selling price to arrive at the modified selling price.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// The original amount.
        /// 
        /// 0 - Percentage
        /// 1 - Amount
        /// 2 - Manually Entered Price
        /// 3 - Promotional Price
        /// </summary>
        public int CalculationMethodType { get; set; }

        /// <summary>
        /// The unit price that was the result of the price modification.
        /// </summary>
        public decimal NewPrice { get; set; }

        /// <summary>
        /// Commit when successful?
        /// </summary>
        public bool OnSuccessCommit { get; set; }
    }
}
