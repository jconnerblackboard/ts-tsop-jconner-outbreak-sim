﻿namespace BbTS.Domain.Models.Transaction.Processing
{
    /// <summary>
    /// Container for a response to a retail transaction begin request.
    /// </summary>
    public class RetailTransactionBeginProcessResponse
    {
        /// <summary>
        /// The id of the request that spawned this response.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The error code associated with the response.  0 means success.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Reason for failure (if there was a failure error code).
        /// </summary>
        public string DeniedText { get; set; }

        /// <summary>
        /// The id of the transaction after processing.
        /// </summary>
        public int TransactionId { get; set; }

        /// <summary>
        /// Empty constructor required for serialization.
        /// </summary>
        public RetailTransactionBeginProcessResponse()
        {
        }

        /// <summary>
        /// Parameterized constructor requiring the request that spawned this response.
        /// </summary>
        /// <param name="requestId">The id of the request that spawned this response.</param>
        public RetailTransactionBeginProcessResponse(string requestId)
        {
            RequestId = requestId;
        }
    }
}
