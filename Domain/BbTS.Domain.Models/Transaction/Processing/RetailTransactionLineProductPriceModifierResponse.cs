﻿namespace BbTS.Domain.Models.Transaction.Processing
{
    /// <summary>
    /// Container class for the response to a retail transaction line product price modifier request.
    /// </summary>
    public class RetailTransactionLineProductPriceModifierResponse
    {
        /// <summary>
        /// Unique identifier for the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The error code associated with the response.  0 means success.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Reason for failure (if there was a failure error code).
        /// </summary>
        public string DeniedText { get; set; }

    }
}
