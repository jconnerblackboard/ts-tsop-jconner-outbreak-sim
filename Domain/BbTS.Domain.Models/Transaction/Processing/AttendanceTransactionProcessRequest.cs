﻿using System;
using BbTS.Domain.Models.Definitions.Container;

namespace BbTS.Domain.Models.Transaction.Processing
{
    /// <summary>
    /// Container class for an attendance transaction request.
    /// </summary>
    public class AttendanceTransactionProcessRequest
    {
        /// <summary>
        /// The id of the request that spawned this response.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The unique identifier for the device.
        /// </summary>
        public string DeviceGuid { get; set; }

        /// <summary>
        /// The number of the transaction.
        /// </summary>
        public int TransactionNumber { get; set; }

        /// <summary>
        /// The date and time the transaction took place
        /// </summary>
        public DateTimeOffset TransactionDateTime { get; set; }

        /// <summary>
        /// The card number used.
        /// </summary>
        public string CardNumber { get; set; }

        /// <summary>
        /// The issue number associated with the card number.
        /// </summary>
        public string IssueNumber { get; set; }

        /// <summary>
        /// Was the issue number captured?
        /// </summary>
        public bool IssueNumberCaptured { get; set; }

        /// <summary>
        /// The physical type of media used in the transaction.
        /// "0 - card (magstripe), 1 - contactless"
        /// </summary>
        public int PhysicalIdType { get; set; }

        /// <summary>
        /// Was a secondary auth type used?
        /// </summary>
        public bool SecondaryAuthEntered { get; set; }

        /// <summary>
        /// Was the transaction keyed offline.
        /// </summary>
        public bool KeyedOfflineFlag { get; set; }

        /// <summary>
        /// The unique identifier for the operator of the device.
        /// </summary>
        public string OperatorGuid { get; set; }

        /// <summary>
        /// Force post.
        /// </summary>
        public bool ForcePost { get; set; }

        /// <summary>
        /// Event number.
        /// </summary>
        public int EventNumber { get; set; }

        /// <summary>
        /// Customer guid.
        /// </summary>
        public string CustomerGuid { get; set; }

        /// <summary>
        /// Attended event transaction type.
        /// </summary>
        public AttendedEventTranType TranType { get; set; }
    }
}
