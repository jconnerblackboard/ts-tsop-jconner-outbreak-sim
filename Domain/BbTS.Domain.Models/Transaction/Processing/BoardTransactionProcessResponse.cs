﻿using BbTS.Domain.Models.Definitions.Pos;

namespace BbTS.Domain.Models.Transaction.Processing
{
    /// <summary>
    /// Container for a response to a retail transaction begin request.
    /// </summary>
    public class BoardTransactionProcessResponse
    {
        /// <summary>
        /// The id of the request that spawned this response.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The error code associated with the response.  0 means success.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Reason for failure (if there was a failure error code).
        /// </summary>
        public string DeniedText { get; set; }

        /// <summary>
        /// The id of the transaction after processing.
        /// </summary>
        public int TransactionId { get; set; }

        /// <summary>
        /// Regular meals grid count left
        /// </summary>
        public int RegularGridLeft { get; set; }

        /// <summary>
        /// Guest meals grid count left
        /// </summary>
        public int GuestGridLeft { get; set; }

        /// <summary>
        /// Meals left for the current period limit
        /// </summary>
        public int PeriodLeft { get; set; }

        /// <summary>
        /// Meals left for the current day limit
        /// </summary>
        public int DayLeft { get; set; }

        /// <summary>
        /// Meals left for current week limit
        /// </summary>
        public int WeekLeft { get; set; }
        /// <summary>
        /// Meals left for current month limit
        /// </summary>
        public int MonthLeft { get; set; }
        /// <summary>
        /// Meals left for current semester/quarter limit
        /// </summary>
        public int SemesterQuarterLeft { get; set; }
        /// <summary>
        /// Meals left for current year limit
        /// </summary>
        public int YearLeft { get; set; }
        /// <summary>
        /// Total guest meals left
        /// </summary>
        public int GuestTotalLeft { get; set; }
        /// <summary>
        /// Extra meals left
        /// </summary>
        public int ExtraLeft { get; set; }
        /// <summary>
        /// Transfer meals left
        /// </summary>
        public int TransferLeft { get; set; }

        /// <summary>
        /// Meal Type used.
        /// 1 -- Regular, no Extra or Transfer
        /// 2 -- Guest Meal
        /// 3 -- Extra
        /// 4 -- Transfer
        /// 5 -- Extra and Regular
        /// 6 -- Transfer and Regular
        /// 7 -- Extra and Transfer
        /// 8 -- Regular, Extra, and Transfer
        /// </summary>
        public int TypeUsed { get; set; }

        /// <summary>
        /// Display meals left by
        /// </summary>
        public MealsLeftBy DisplayMealsLeftBy { get; set; }

        /// <summary>
        /// Show board count
        /// </summary>
        public bool ShowBoardCount { get; set; }

        /// <summary>
        /// Print board count
        /// </summary>
        public bool PrintBoardCount { get; set; }

        /// <summary>
        /// Parameterized constructor requiring the request that spawned this response.
        /// </summary>
        /// <param name="requestId">The id of the request that spawned this response.</param>
        public BoardTransactionProcessResponse(string requestId)
        {
            RequestId = requestId;
        }

    }
}
