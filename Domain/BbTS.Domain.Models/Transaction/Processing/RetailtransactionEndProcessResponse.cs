﻿using System.Collections.Generic;
using BbTS.Domain.Models.Pos;
using BbTS.Domain.Models.StoredValue;

namespace BbTS.Domain.Models.Transaction.Processing
{
    /// <summary>
    /// Container class for the response to a retail transaction end process request.
    /// </summary>
    public class RetailTransactionEndProcessResponse
    {
        /// <summary>
        /// The id of the request that spawned this response.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The error code associated with the response.  0 means success.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Reason for failure (if there was a failure error code).
        /// </summary>
        public string DeniedText { get; set; }

        /// <summary>
        /// List of customer pos information.
        /// </summary>
        public List<CustomerPosInfo> CustomerPosInfoList { get; set; } = new List<CustomerPosInfo>();

        /// <summary>
        /// List of customer stored value accounts.
        /// </summary>
        public List<CustomerStoredValueAccount> CustomerStoredValueAccounts { get; set; } = new List<CustomerStoredValueAccount>();
        
        /// <summary>
        /// Empty constructor required for serialization.
        /// </summary>
        public RetailTransactionEndProcessResponse()
        {
        }

        /// <summary>
        /// Parameterized constructor requiring the request that spawned this response.
        /// </summary>
        /// <param name="requestId">The id of the request that spawned this response.</param>
        public RetailTransactionEndProcessResponse(string requestId)
        {
            RequestId = requestId;
        }
    }
}
