﻿namespace BbTS.Domain.Models.Transaction.Processing
{
    /// <summary>
    /// Container class for an attendance transaction processing result.
    /// </summary>
    public class AttendanceTransactionProcessingResult : ProcessingResult
    {
        /// <summary>
        /// Determines if attendance should be displayed after the transaction.
        /// </summary>
        public bool DisplayAttendance { get; set; }

        /// <summary>
        /// Attendance.
        /// </summary>
        public int Attendance { get; set; }

        /// <summary>
        /// Capacity.
        /// </summary>
        public int Capacity { get; set; }

        /// <summary>
        /// Parameterless constructor.
        /// </summary>
        public AttendanceTransactionProcessingResult()
        {
        }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="requestId">The unique identifier for the request that preempted the processing routine.</param>
        /// <param name="errorCode">The error code associated with the response.  0 means success.</param>
        /// <param name="deniedText">Reason for failure (if there was a failure error code).</param>
        public AttendanceTransactionProcessingResult(string requestId, int errorCode, string deniedText)
        {
            RequestId = requestId;
            ErrorCode = errorCode;
            DeniedText = deniedText;
        }
    }

    /// <summary>
    /// View for a <see cref="AttendanceTransactionProcessingResult"/>.  (Version 1)
    /// </summary>
    public class AttendanceTransactionProcessingResultViewV01
    {
        /// <summary>
        /// The unique identifier for the request that preempted the processing routine.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The error code associated with the response.  0 means success.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Reason for failure (if there was a failure error code).
        /// </summary>
        public string DeniedText { get; set; }

        /// <summary>
        /// Determines if attendance should be displayed after the transaction.
        /// </summary>
        public bool DisplayAttendance { get; set; }

        /// <summary>
        /// Attendance.
        /// </summary>
        public int Attendance { get; set; }

        /// <summary>
        /// Capacity.
        /// </summary>
        public int Capacity { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="AttendanceTransactionProcessingResult"/> conversion.
    /// </summary>
    public static class AttendanceTransactionProcessingResultConverter
    {
        /// <summary>
        /// Returns a <see cref="AttendanceTransactionProcessingResultViewV01"/> object based on this <see cref="AttendanceTransactionProcessingResult"/>.
        /// </summary>
        /// <param name="attendanceTransactionProcessingResult"></param>
        /// <returns></returns>
        public static AttendanceTransactionProcessingResultViewV01 ToAttendanceTransactionProcessingResultViewV01(this AttendanceTransactionProcessingResult attendanceTransactionProcessingResult)
        {
            if (attendanceTransactionProcessingResult == null) return null;

            return new AttendanceTransactionProcessingResultViewV01
            {
                RequestId = attendanceTransactionProcessingResult.RequestId,
                ErrorCode = attendanceTransactionProcessingResult.ErrorCode,
                DeniedText = attendanceTransactionProcessingResult.DeniedText,
                DisplayAttendance = attendanceTransactionProcessingResult.DisplayAttendance,
                Attendance = attendanceTransactionProcessingResult.Attendance,
                Capacity = attendanceTransactionProcessingResult.Capacity
            };
        }

        /// <summary>
        /// Returns a <see cref="AttendanceTransactionProcessingResult"/> object based on this <see cref="AttendanceTransactionProcessingResultViewV01"/>.
        /// </summary>
        /// <param name="attendanceTransactionProcessingResultViewV01"></param>
        /// <returns></returns>
        public static AttendanceTransactionProcessingResult ToAttendanceTransactionProcessingResult(this AttendanceTransactionProcessingResultViewV01 attendanceTransactionProcessingResultViewV01)
        {
            if (attendanceTransactionProcessingResultViewV01 == null) return null;

            return new AttendanceTransactionProcessingResult
            {
                RequestId = attendanceTransactionProcessingResultViewV01.RequestId,
                ErrorCode = attendanceTransactionProcessingResultViewV01.ErrorCode,
                DeniedText = attendanceTransactionProcessingResultViewV01.DeniedText,
                DisplayAttendance = attendanceTransactionProcessingResultViewV01.DisplayAttendance,
                Attendance = attendanceTransactionProcessingResultViewV01.Attendance,
                Capacity = attendanceTransactionProcessingResultViewV01.Capacity
            };
        }
    }
}
