﻿namespace BbTS.Domain.Models.Transaction.Processing
{
    /// <summary>
    /// Container class for the response to a transaction post request.  (Version 1)
    /// </summary>
    public class TransactionPostResponseV01
    {
        /// <summary>
        /// The unique identifier for the request that preempted the processing routine.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The error code associated with the response.  0 means success.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Reason for failure (if there was a failure error code).
        /// </summary>
        public string DeniedText { get; set; }

        /// <summary>
        /// Processing result for control transaction contained within the super set.
        /// </summary>
        public ProcessingResultViewV01 ControlTransactionProcessingResult { get; set; }

        /// <summary>
        /// Set of processing results for all arts transactions contained within the super set.
        /// </summary>
        public ArtsTransactionProcessingResultSetViewV01 ArtsTransactionProcessingResultSet { get; set; }

        /// <summary>
        /// Set of processing results for all attendance transactions contained within the super set.
        /// </summary>
        public AttendanceTransactionProcessingResultSetViewV01 AttendanceTransactionProcessingResultSet { get; set; }

        /// <summary>
        /// Set of processing results for all board transactions contained within the super set.
        /// </summary>
        public BoardTransactionProcessingResultSetViewV01 BoardTransactionProcessingResultSet { get; set; }
    }

    /// <summary>
    /// Extension methods to create transaction post responses.
    /// </summary>
    public static class TransactionPostResponseConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="TransactionPostResponseV01"/> object based on this <see cref="TransactionProcessingResult"/>.
        /// </summary>
        /// <param name="transactionProcessingResult"></param>
        /// <returns></returns>
        public static TransactionPostResponseV01 ToTransactionPostResponseV01(this TransactionProcessingResult transactionProcessingResult)
        {
            if (transactionProcessingResult == null) return null;

            var transactionProcessingResultViewV01 = transactionProcessingResult.ToTransactionProcessingResultViewV01();
            return new TransactionPostResponseV01
            {
                RequestId = transactionProcessingResultViewV01.RequestId,
                ErrorCode = transactionProcessingResultViewV01.ErrorCode,
                DeniedText = transactionProcessingResultViewV01.DeniedText,
                ControlTransactionProcessingResult = transactionProcessingResultViewV01.ControlTransactionProcessingResult,
                ArtsTransactionProcessingResultSet = transactionProcessingResultViewV01.ArtsTransactionProcessingResultSet,
                AttendanceTransactionProcessingResultSet = transactionProcessingResultViewV01.AttendanceTransactionProcessingResultSet,
                BoardTransactionProcessingResultSet = transactionProcessingResultViewV01.BoardTransactionProcessingResultSet
            };
        }

        /// <summary>
        /// Returns a <see cref="TransactionProcessingResult"/> object based on this <see cref="TransactionPostResponseV01"/>.
        /// </summary>
        /// <param name="transactionPostResponseV01"></param>
        /// <returns></returns>
        public static TransactionProcessingResult ToTransactionProcessingResult(this TransactionPostResponseV01 transactionPostResponseV01)
        {
            if (transactionPostResponseV01 == null) return null;

            var transactionProcessingResultViewV01 = new TransactionProcessingResultViewV01
            {
                RequestId = transactionPostResponseV01.RequestId,
                ErrorCode = transactionPostResponseV01.ErrorCode,
                DeniedText = transactionPostResponseV01.DeniedText,
                ControlTransactionProcessingResult = transactionPostResponseV01.ControlTransactionProcessingResult,
                ArtsTransactionProcessingResultSet = transactionPostResponseV01.ArtsTransactionProcessingResultSet,
                AttendanceTransactionProcessingResultSet = transactionPostResponseV01.AttendanceTransactionProcessingResultSet,
                BoardTransactionProcessingResultSet = transactionPostResponseV01.BoardTransactionProcessingResultSet
            };

            return transactionProcessingResultViewV01.ToTransactionProcessingResult();
        }
        #endregion
    }
}
