﻿using System.Collections.Generic;
using System.Linq;

namespace BbTS.Domain.Models.Transaction.Processing
{
    /// <summary>
    /// Container class for a set of board transaction processing results.
    /// </summary>
    public class BoardTransactionProcessingResultSet : ProcessingResult
    {
        /// <summary>
        /// List of transaction results for all arts transactions under this super set.
        /// </summary>
        public List<BoardTransactionProcessingResult> ProcessingResults { get; set; } = new List<BoardTransactionProcessingResult>();

        /// <summary>
        /// Parameter-less constructor.
        /// </summary>
        public BoardTransactionProcessingResultSet()
        {
        }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="requestId">The unique identifier for the request that preempted the processing routine.</param>
        public BoardTransactionProcessingResultSet(string requestId)
        {
            RequestId = requestId;
        }
    }

    /// <summary>
    /// View for a <see cref="BoardTransactionProcessingResultSet"/>.  (Version 1)
    /// </summary>
    public class BoardTransactionProcessingResultSetViewV01
    {
        /// <summary>
        /// The unique identifier for the request that preempted the processing routine.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The error code associated with the response.  0 means success.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Reason for failure (if there was a failure error code).
        /// </summary>
        public string DeniedText { get; set; }

        /// <summary>
        /// List of transaction results for all board transactions under this super set.
        /// </summary>
        public List<BoardTransactionProcessingResultViewV01> ProcessingResults { get; set; } = new List<BoardTransactionProcessingResultViewV01>();
    }

    /// <summary>
    /// Extension methods for <see cref="BoardTransactionProcessingResultSet"/> conversion.
    /// </summary>
    public static class BoardTransactionProcessingResultSetConverter
    {
        /// <summary>
        /// Returns a <see cref="BoardTransactionProcessingResultSetViewV01"/> object based on this <see cref="BoardTransactionProcessingResultSet"/>.
        /// </summary>
        /// <param name="boardTransactionProcessingResultSet"></param>
        /// <returns></returns>
        public static BoardTransactionProcessingResultSetViewV01 ToBoardTransactionProcessingResultSetViewV01(this BoardTransactionProcessingResultSet boardTransactionProcessingResultSet)
        {
            if (boardTransactionProcessingResultSet == null) return null;

            return new BoardTransactionProcessingResultSetViewV01
            {
                RequestId = boardTransactionProcessingResultSet.RequestId,
                ErrorCode = boardTransactionProcessingResultSet.ErrorCode,
                DeniedText = boardTransactionProcessingResultSet.DeniedText,
                ProcessingResults = boardTransactionProcessingResultSet.ProcessingResults.Select(boardTransactionProcessingResult => boardTransactionProcessingResult.ToBoardTransactionProcessingResultViewV01()).ToList()
            };
        }

        /// <summary>
        /// Returns a <see cref="BoardTransactionProcessingResultSet"/> object based on this <see cref="BoardTransactionProcessingResultSetViewV01"/>.
        /// </summary>
        /// <param name="boardTransactionProcessingResultSetViewV01"></param>
        /// <returns></returns>
        public static BoardTransactionProcessingResultSet ToBoardTransactionProcessingResultSet(this BoardTransactionProcessingResultSetViewV01 boardTransactionProcessingResultSetViewV01)
        {
            if (boardTransactionProcessingResultSetViewV01 == null) return null;

            return new BoardTransactionProcessingResultSet
            {
                RequestId = boardTransactionProcessingResultSetViewV01.RequestId,
                ErrorCode = boardTransactionProcessingResultSetViewV01.ErrorCode,
                DeniedText = boardTransactionProcessingResultSetViewV01.DeniedText,
                ProcessingResults = boardTransactionProcessingResultSetViewV01.ProcessingResults.Select(boardTransactionProcessingResultViewV01 => boardTransactionProcessingResultViewV01.ToBoardTransactionProcessingResult()).ToList()
            };
        }
    }
}
