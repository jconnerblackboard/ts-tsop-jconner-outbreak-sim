﻿using System;
using BbTS.Domain.Models.Definitions.Container;

namespace BbTS.Domain.Models.Transaction.Processing
{
    /// <summary>
    /// Container class for a retail transaction begin request.
    /// </summary>
    public class RetailTransactionBeginProcessRequest : IProcessingRequest
    {
        /// <summary>
        /// The id of the request that spawned this response.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The unique identifier for the device.
        /// </summary>
        public string DeviceGuid { get; set; }

        /// <summary>
        /// The unique identifier for the transaction
        /// </summary>
        public string TransactionGuid { get; set; }

        /// <summary>
        /// Guid that is generated at sign-on and will be linked with all transactions that take place until sign-off
        /// </summary>
        public string SessionGuid { get; set; }

        /// <summary>
        /// The number of the transaction.
        /// </summary>
        public int TransactionNumber { get; set; }

        /// <summary>
        /// The data and time the transaction took place
        /// </summary>
        public DateTime TransactionDateTime { get; set; }

        /// <summary>
        /// Was the transaction canceled.
        /// </summary>
        public bool CancelledFlag { get; set; }

        /// <summary>
        /// Was the transaction suspended.
        /// </summary>
        public bool SuspendedFlag { get; set; }

        /// <summary>
        /// The data and time the transaction was suspended.  Null if not suspended.
        /// </summary>
        public DateTime? SuspendedUnsuspendDatetime { get; set; }

        /// <summary>
        /// Is this a training exercise.
        /// </summary>
        public bool TrainingFlag { get; set; }

        /// <summary>
        /// Was the transaction keyed offline.
        /// </summary>
        public bool KeyedOfflineFlag { get; set; }

        /// <summary>
        /// The attend type (cashier available, unattended, etc.)
        /// </summary>
        public int AttendedType { get; set; }

        /// <summary>
        /// The type of retail transaction.
        /// "0 - Sale, 1 - Return"
        /// </summary>
        public RetailTranType RetailTranType { get; set; }

        /// <summary>
        /// The unique identifier for the operator of the device.
        /// </summary>
        public string OperatorGuid { get; set; }

        /// <summary>
        /// The cash drawer number used.
        /// </summary>
        public int CashdrawerNumber { get; set; }

        /// <summary>
        /// The device control total prior to the current transaction.
        /// </summary>
        public decimal? ControlTotalBeforeTransaction { get; set; }
        
        /// <summary>
        /// The device control total after the current transaction.
        /// </summary>
        public decimal? ControlTotalAfterTransaction { get; set; }

        /// <summary>
        /// The change in control total.
        /// </summary>
        public decimal? ChangeInControlTotal
        {
            get
            {
                if (!ControlTotalBeforeTransaction.HasValue || !ControlTotalAfterTransaction.HasValue) return null;

                return ControlTotalAfterTransaction - ControlTotalBeforeTransaction;
            }
        }
    }
}
