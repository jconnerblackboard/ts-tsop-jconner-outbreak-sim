﻿using BbTS.Domain.Models.Definitions.ArtsDataModel;
using BbTS.Domain.Models.Definitions.Credential;

namespace BbTS.Domain.Models.Transaction.Processing
{
    /// <summary>
    /// Container class for a retail transaction line stored value type card request.
    /// </summary>
    public class RetailTransactionLineStoredValueTypeCardRequest : IProcessingRequest
    {
        /// <summary>
        /// The id of the request that spawned this response.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The unique numerical identifier for the transaction.
        /// </summary>
        public int TransactionId { get; set; }

        /// <summary>
        /// The line item sequence number.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// Is this transaction void?
        /// </summary>
        public bool VoidFlag { get; set; }

        /// <summary>
        /// The stored value amount of the transaction.
        /// </summary>
        public decimal StoredValueAmount { get; set; }

        /// <summary>
        /// The card purchase fee amount.
        /// </summary>
        public decimal CardPurchaseFeeAmount { get; set; }

        /// <summary>
        /// The actual unit price.
        /// </summary>
        public decimal ActualUnitProce { get; set; }

        /// <summary>
        /// The unit cost price.
        /// </summary>
        public decimal UnitCostPrice { get; set; }

        /// <summary>
        /// The stored value account type id.
        /// </summary>
        public int SvAccountTypeId { get; set; }

        /// <summary>
        /// The customer card number.
        /// </summary>
        public string CardNumber { get; set; }

        /// <summary>
        /// The issue number for the card.
        /// </summary>
        public string IssueNumber { get; set; }

        /// <summary>
        /// Indicates whether or not the issue number was captured.
        /// </summary>
        public bool IssueNumberCaptured { get; set; }

        /// <summary>
        /// The physical id type.
        /// </summary>
        public CustomerTenderPhysicalIdType PhysicalIdType { get; set; }

        /// <summary>
        /// The customer entry method type.
        /// </summary>
        public CustomerTenderEntryMethodType CustomerEntryMethodType { get; set; }

        /// <summary>
        /// Indicates whether or not a secondary auth was entered.
        /// </summary>
        public bool SecondaryAuthEntered { get; set; }

        /// <summary>
        /// Indicates whether or not enrichment will be applied.
        /// </summary>
        public bool EnrichmentFlag { get; set; }

        /// <summary>
        /// Indicates the line item seuqence number where the enrichment line item was entered.
        /// </summary>
        public int EnrichmentParentLineItemSequence { get; set; }

        /// <summary>
        /// On success commit.
        /// </summary>
        public bool OnSuccessCommit { get; set; }

        /// <summary>
        /// Force post.
        /// </summary>
        public bool ForcePost { get; set; }
    }
}
