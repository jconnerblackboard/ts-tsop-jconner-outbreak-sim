﻿using BbTS.Domain.Models.Definitions.ArtsDataModel;

namespace BbTS.Domain.Models.Transaction.Processing
{
    /// <summary>
    /// Container class for a retail transaction line item product process request.
    /// </summary>
    public class RetailTransactionLineProductProcessRequest : IProcessingRequest
    {
        /// <summary>
        /// The unique identifier for this request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The unique numerical identifier for the transaction.
        /// </summary>
        public int TransactionId { get; set; }

        /// <summary>
        /// The line item sequence number.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// Is this transaction void?
        /// </summary>
        public bool VoidFlag { get; set; }

        /// <summary>
        /// The numerical identfier for the product detail.
        /// </summary>
        public int ProdDetailId { get; set; }

        /// <summary>
        /// The numerical identfier for the tax schedule.
        /// </summary>
        public int TaxScheduleId { get; set; }

        /// <summary>
        /// The numerical identfier for the tax group.
        /// </summary>
        public int? TaxGroupId { get; set; }

        /// <summary>
        /// The retail price.
        /// </summary>
        public decimal RetailPrice { get; set; }

        /// <summary>
        /// The retail price quantity.
        /// </summary>
        public int RetailPriceQty { get; set; }

        /// <summary>
        /// The actual unit price.
        /// </summary>
        public decimal ActualUnitPrice { get; set; }

        /// <summary>
        /// The actual unit price quantity.
        /// </summary>
        public int ActualUnitPriceQty { get; set; }

        /// <summary>
        /// The quantity purchased.
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// TUnits Amount. The number of units sold, when selling bulk merchandise. 
        /// Eg: A sale of 20 Gallons of Gas: Quantity=20, Units=1, UnitOfMeasure=Ga
        /// Eg: A sale of 3 cans of Beans: Quantity=3, Units=3, UnitOfMeasure=Ea
        /// </summary>
        public int Units { get; set; }

        /// <summary>
        /// The type of unit measurement.
        /// </summary>
        public UnitMeasureType UnitMeasureType { get; set; }

        /// <summary>
        /// Extended Amount of Line Item (in millidollars). The product of multiplying Quantity
        /// by the retail selling unit price derived from price lookup (and any applicable price derivation rules).
        /// This retail sale unit price excludes sales and/or value added tax.
        /// </summary>
        public decimal ExtendedAmount { get; set; }

        /// <summary>
        /// Amount of any Unit Discounts (in millidollars). The monetary total per-unit of all Discounts and RetailPriceModifiers that were applied to this Item
        /// </summary>
        public decimal UnitDiscountAmount { get; set; }

        /// <summary>
        /// Extended Amount of Discounts (in millidollars). The monetary total of all Discounts and RetailPriceModifiers that were applied to this Item
        /// </summary>
        public decimal ExtendedDiscountAmount { get; set; }

        /// <summary>
        /// The product entry method: "0 - MenuItem, 1 - Scanned(barcode, etc), 2 - Keyed(Manual Product Barcode)"
        /// </summary>
        public ProductEntryMethodType ProductEntryMethodType { get; set; }

        /// <summary>
        /// The retail price entry method type: "0 - Lookup, 1 - Manually Entered"
        /// </summary>
        public RetailPriceEntryMethodType RetailPriceEntryMethodType { get; set; }

        /// <summary>
        /// Price per Unit (in millidollars). The unit MSRP of the Item at the time of the Transaction
        /// </summary>
        public decimal UnitListPrice { get; set; }

        /// <summary>
        /// The number of units applicable to the MSRP of the Item at the time of the Transaction e.g. 3 for 99c
        /// </summary>
        public int UnitListPriceQty { get; set; }

        /// <summary>
        /// Indicates if there was a product promotion at the time of the transaction (T/F)
        /// </summary>
        public bool ProdPromoFlag { get; set; }

        /// <summary>
        /// The product promotion price.
        /// </summary>
        public decimal ProdPromoPrice { get; set; }

        /// <summary>
        /// The numerical identifier for the product promo.
        /// </summary>
        public int ProdPromoId { get; set; }

        /// <summary>
        /// Indicates there is at least one price modification on this product, which can be determined by looking at the  new RETAIL_TRAN_LINEITEM_PRODPRCMD table (T/F) 
        /// </summary>
        public bool RetailPriceModifiedFlag { get; set; }

        /// <summary>
        /// Commit if successful.
        /// </summary>
        public bool OnSuccessCommit { get; set; }

    }
}
