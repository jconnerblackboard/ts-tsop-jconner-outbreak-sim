﻿using System.Collections.Generic;
using System.Linq;

namespace BbTS.Domain.Models.Transaction.Processing
{
    /// <summary>
    /// Container class for the results of processing multiple arts transactions.
    /// </summary>
    public class ArtsTransactionProcessingResultSet : ProcessingResult
    {
        /// <summary>
        /// List of transaction results for all arts transactions under this super set.
        /// </summary>
        public List<ArtsTransactionProcessingResult> ArtsTransactionProcessingResults { get; set; } = new List<ArtsTransactionProcessingResult>();

        /// <summary>
        /// Parameter-less constructor.
        /// </summary>
        public ArtsTransactionProcessingResultSet()
        {
        }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="requestId">The unique identifier for the request that preempted the processing routine.</param>
        public ArtsTransactionProcessingResultSet(string requestId)
        {
            RequestId = requestId;
        }
    }

    /// <summary>
    /// View for a <see cref="ArtsTransactionProcessingResultSet"/>.  (Version 1)
    /// </summary>
    public class ArtsTransactionProcessingResultSetViewV01
    {
        /// <summary>
        /// The unique identifier for the request that preempted the processing routine.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The error code associated with the response.  0 means success.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Reason for failure (if there was a failure error code).
        /// </summary>
        public string DeniedText { get; set; }

        /// <summary>
        /// List of transaction results for all arts transactions under this super set.
        /// </summary>
        public List<ArtsTransactionProcessingResultViewV01> ArtsTransactionProcessingResults { get; set; } = new List<ArtsTransactionProcessingResultViewV01>();
    }

    /// <summary>
    /// Extension methods for <see cref="ArtsTransactionProcessingResultSet"/> conversion.
    /// </summary>
    public static class ArtsTransactionProcessingResultSetConverter
    {
        /// <summary>
        /// Returns a <see cref="ArtsTransactionProcessingResultSetViewV01"/> object based on this <see cref="ArtsTransactionProcessingResultSet"/>.
        /// </summary>
        /// <param name="artsTransactionProcessingResultSet"></param>
        /// <returns></returns>
        public static ArtsTransactionProcessingResultSetViewV01 ToArtsTransactionProcessingResultSetViewV01(this ArtsTransactionProcessingResultSet artsTransactionProcessingResultSet)
        {
            if (artsTransactionProcessingResultSet == null) return null;

            return new ArtsTransactionProcessingResultSetViewV01
            {
                RequestId = artsTransactionProcessingResultSet.RequestId,
                ErrorCode = artsTransactionProcessingResultSet.ErrorCode,
                DeniedText = artsTransactionProcessingResultSet.DeniedText,
                ArtsTransactionProcessingResults = artsTransactionProcessingResultSet.ArtsTransactionProcessingResults.Select(artsTransactionProcessingResult => artsTransactionProcessingResult.ToArtsTransactionProcessingResultViewV01()).ToList()
            };
        }

        /// <summary>
        /// Returns a <see cref="ArtsTransactionProcessingResultSet"/> object based on this <see cref="ArtsTransactionProcessingResultSetViewV01"/>.
        /// </summary>
        /// <param name="artsTransactionProcessingResultSetViewV01"></param>
        /// <returns></returns>
        public static ArtsTransactionProcessingResultSet ToArtsTransactionProcessingResultSet(this ArtsTransactionProcessingResultSetViewV01 artsTransactionProcessingResultSetViewV01)
        {
            if (artsTransactionProcessingResultSetViewV01 == null) return null;

            return new ArtsTransactionProcessingResultSet
            {
                RequestId = artsTransactionProcessingResultSetViewV01.RequestId,
                ErrorCode = artsTransactionProcessingResultSetViewV01.ErrorCode,
                DeniedText = artsTransactionProcessingResultSetViewV01.DeniedText,
                ArtsTransactionProcessingResults = artsTransactionProcessingResultSetViewV01.ArtsTransactionProcessingResults.Select(artsTransactionProcessingResultViewV01 => artsTransactionProcessingResultViewV01.ToArtsTransactionProcessingResult()).ToList()
            };
        }
    }
}
