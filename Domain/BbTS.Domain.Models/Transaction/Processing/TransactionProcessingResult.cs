﻿namespace BbTS.Domain.Models.Transaction.Processing
{
    /// <summary>
    /// Container class for the response to a transaction post request.
    /// </summary>
    public class TransactionProcessingResult : ProcessingResult
    {
        /// <summary>
        /// Processing result for control transaction contained within the super set.
        /// </summary>
        public ProcessingResult ControlTransactionProcessingResult { get; set; }

        /// <summary>
        /// Set of processing results for all arts transactions contained within the super set.
        /// </summary>
        public ArtsTransactionProcessingResultSet ArtsTransactionProcessingResultSet { get; set; }

        /// <summary>
        /// Set of processing results for all attendance transactions contained within the super set.
        /// </summary>
        public AttendanceTransactionProcessingResultSet AttendanceTransactionProcessingResultSet { get; set; }

        /// <summary>
        /// Set of processing results for all board transactions contained within the super set.
        /// </summary>
        public BoardTransactionProcessingResultSet BoardTransactionProcessingResultSet { get; set; }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="requestId">The unique identifier for this request.</param>
        public TransactionProcessingResult(string requestId)
        {
            RequestId = requestId;
        }
    }

    /// <summary>
    /// View for a <see cref="TransactionProcessingResult"/>.  (Version 1)
    /// </summary>
    public class TransactionProcessingResultViewV01
    {
        /// <summary>
        /// The unique identifier for the request that preempted the processing routine.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The error code associated with the response.  0 means success.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Reason for failure (if there was a failure error code).
        /// </summary>
        public string DeniedText { get; set; }

        /// <summary>
        /// Processing result for control transaction contained within the super set.
        /// </summary>
        public ProcessingResultViewV01 ControlTransactionProcessingResult { get; set; }

        /// <summary>
        /// Set of processing results for all arts transactions contained within the super set.
        /// </summary>
        public ArtsTransactionProcessingResultSetViewV01 ArtsTransactionProcessingResultSet { get; set; }

        /// <summary>
        /// Set of processing results for all attendance transactions contained within the super set.
        /// </summary>
        public AttendanceTransactionProcessingResultSetViewV01 AttendanceTransactionProcessingResultSet { get; set; }

        /// <summary>
        /// Set of processing results for all board transactions contained within the super set.
        /// </summary>
        public BoardTransactionProcessingResultSetViewV01 BoardTransactionProcessingResultSet { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="TransactionProcessingResult"/> conversion.
    /// </summary>
    public static class TransactionProcessingResultConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="TransactionProcessingResultViewV01"/> object based on this <see cref="TransactionProcessingResult"/>.
        /// </summary>
        /// <param name="transactionProcessingResult"></param>
        /// <returns></returns>
        public static TransactionProcessingResultViewV01 ToTransactionProcessingResultViewV01(this TransactionProcessingResult transactionProcessingResult)
        {
            if (transactionProcessingResult == null) return null;

            return new TransactionProcessingResultViewV01
            {
                RequestId = transactionProcessingResult.RequestId,
                ErrorCode = transactionProcessingResult.ErrorCode,
                DeniedText = transactionProcessingResult.DeniedText,
                ControlTransactionProcessingResult = transactionProcessingResult.ControlTransactionProcessingResult.ToProcessingResultViewV01(),
                ArtsTransactionProcessingResultSet = transactionProcessingResult.ArtsTransactionProcessingResultSet.ToArtsTransactionProcessingResultSetViewV01(),
                AttendanceTransactionProcessingResultSet = transactionProcessingResult.AttendanceTransactionProcessingResultSet.ToAttendanceTransactionProcessingResultSetViewV01(),
                BoardTransactionProcessingResultSet = transactionProcessingResult.BoardTransactionProcessingResultSet.ToBoardTransactionProcessingResultSetViewV01()
            };
        }

        /// <summary>
        /// Returns a <see cref="TransactionProcessingResult"/> object based on this <see cref="TransactionProcessingResultViewV01"/>.
        /// </summary>
        /// <param name="transactionProcessingResultViewV01"></param>
        /// <returns></returns>
        public static TransactionProcessingResult ToTransactionProcessingResult(this TransactionProcessingResultViewV01 transactionProcessingResultViewV01)
        {
            if (transactionProcessingResultViewV01 == null) return null;

            return new TransactionProcessingResult(transactionProcessingResultViewV01.RequestId)
            {
                RequestId = transactionProcessingResultViewV01.RequestId,
                ErrorCode = transactionProcessingResultViewV01.ErrorCode,
                DeniedText = transactionProcessingResultViewV01.DeniedText,
                ControlTransactionProcessingResult = transactionProcessingResultViewV01.ControlTransactionProcessingResult.ToProcessingResult(),
                ArtsTransactionProcessingResultSet = transactionProcessingResultViewV01.ArtsTransactionProcessingResultSet.ToArtsTransactionProcessingResultSet(),
                AttendanceTransactionProcessingResultSet = transactionProcessingResultViewV01.AttendanceTransactionProcessingResultSet.ToAttendanceTransactionProcessingResultSet(),
                BoardTransactionProcessingResultSet = transactionProcessingResultViewV01.BoardTransactionProcessingResultSet.ToBoardTransactionProcessingResultSet()
            };
        }
        #endregion
    }
}