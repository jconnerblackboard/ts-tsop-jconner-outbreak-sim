﻿
namespace BbTS.Domain.Models.Transaction.Processing
{
    /// <summary>
    /// Container class for a retail transaction line stored value enrichment request.
    /// </summary>
    public class RetailTransactionLineStoredValueEnrichmentRequest : IProcessingRequest
    {
        /// <summary>
        /// The id of the request that spawned this response.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The unique numerical identifier for the transaction.
        /// </summary>
        public int TransactionId { get; set; }

        /// <summary>
        /// The line item sequence number.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// Sv enrichment sequence
        /// </summary>
        public int StoredValueEnrichmentSequence { get; set; }

        /// <summary>
        /// Percentage (value between 0 - 1.0)
        /// </summary>
        public decimal Percentage { get; set; }

        /// <summary>
        /// Rounding amount
        /// </summary>
        public decimal RoundingAmount { get; set; }

        /// <summary>
        /// Amount
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// On success commit.
        /// </summary>
        public bool OnSuccessCommit { get; set; }
    }
}
