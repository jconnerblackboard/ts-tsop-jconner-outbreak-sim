﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Transaction.Processing
{
    /// <summary>
    /// Container class for the response to a retail transaction line tender SV Card process request.
    /// </summary>
    public class RetailTransactionLineTenderSvCardProcessResponse
    {
        /// <summary>
        /// The id of the request that spawned this response.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The error code associated with the response.  0 means success.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Reason for failure (if there was a failure error code).
        /// </summary>
        public string DeniedText { get; set; }

        /// <summary>
        /// Empty constructor required for serialization.
        /// </summary>
        public RetailTransactionLineTenderSvCardProcessResponse()
        {
        }

        /// <summary>
        /// Parameterized constructor requiring the request that spawned this response.
        /// </summary>
        /// <param name="requestId">The id of the request that spawned this response.</param>
        public RetailTransactionLineTenderSvCardProcessResponse(string requestId)
        {
            RequestId = requestId;
        }
    }
}
