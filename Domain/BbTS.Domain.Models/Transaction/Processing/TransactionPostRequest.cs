﻿using System;
using System.Collections.Generic;
using BbTS.Domain.Models.Container.Elements;
using BbTS.Domain.Models.Definitions.Container;
using BbTS.Domain.Models.General;

namespace BbTS.Domain.Models.Transaction.Processing
{
    #region Nested usings - this allows us to leave off qualifiers for the non-api transaction object names.
    // ReSharper disable RedundantNameQualifier
    // ReSharper disable RedundantUsingDirective
    using BbTS.Domain.Models.Container;
    // ReSharper restore RedundantNameQualifier
    // ReSharper restore RedundantUsingDirective
    #endregion

    /// <summary>
    /// Container class for a transaction post request.  (Version 1)
    /// </summary>
    public class TransactionPostRequestV01
    {
        /// <summary>
        /// The unique identifier for this request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The transaction.
        /// </summary>
        public TransactionViewV01 Transaction { get; set; }
    }

    /// <summary>
    /// Container class for a transaction post request.  (Version 2)
    /// </summary>
    public class TransactionPostRequestV02
    {
        /// <summary>
        /// The unique identifier for this request.
        /// </summary>
        public Guid RequestId { get; set; }

        /// <summary>
        /// Unique identifier (guid) for this transaction.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Unique identifier (guid) for the originator of this transaction.  Every originator would be assigned a guid that it would use forever.
        /// </summary>
        public Guid OriginatorId { get; set; }

        /// <summary>
        /// Overall state of the transaction.
        /// </summary>
        public Disposition Disposition { get; set; }

        /// <summary>
        /// True if this transaction has already occured (as would be in the case of an offline transaction).
        /// </summary>
        public bool ForcePost { get; set; }

        /// <summary>
        /// List of all meaningful timestamps, i.e. Start, Finish, Submit, Post, etc.
        /// </summary>
        public List<TransactionTimestampViewV02> TransactionTimestamps { get; set; } = new List<TransactionTimestampViewV02>();

        /// <summary>
        /// The device or client where the transaction originated from.
        /// </summary>
        public OriginatorElementViewV01 OriginatorElement { get; set; }

        /// <summary>
        /// The operator performing this transaction.
        /// </summary>
        public OperatorElementViewV01 OperatorElement { get; set; }

        /// <summary>
        /// The control element used for login and logout of the operator.
        /// </summary>
        public ControlElementViewV01 ControlElement { get; set; }

        /// <summary>
        /// ARTS element.
        /// </summary>
        public ArtsElementViewV02 ArtsElement { get; set; }

        /// <summary>
        /// Attendance element.
        /// </summary>
        public AttendanceElementViewV02 AttendanceElement { get; set; }

        /// <summary>
        /// Board element.
        /// </summary>
        public BoardElementViewV02 BoardElement { get; set; }

        /// <summary>
        /// Any additional metadata that is available for this transaction.  Examples are things like IP Address, Phase of the Moon, etc.
        /// </summary>
        public List<StringPairViewV01> NameValues { get; set; } = new List<StringPairViewV01>();
    }

    /// <summary>
    /// Extension methods to create transaction post requests. 
    /// </summary>
    public static class TransactionPostRequestConverter
    {
        #region Version 1
        /// <summary>
        /// Creates a <see cref="TransactionPostRequestV01"/> from a <see cref="Transaction"/> instance.
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static TransactionPostRequestV01 ToTransactionPostRequestV01(this Transaction transaction)
        {
            return new TransactionPostRequestV01
            {
                RequestId = Guid.NewGuid().ToString("D").ToLowerInvariant(),
                Transaction = transaction.ToTransactionViewV01()
            };
        }

        /// <summary>
        /// Extracts the Request ID from the request.
        /// </summary>
        /// <param name="transactionPostRequestV01"></param>
        /// <returns></returns>
        public static Guid RequestIdExtract(this TransactionPostRequestV01 transactionPostRequestV01)
        {
            if (transactionPostRequestV01 == null) return Guid.Empty;

            try
            {
                return Guid.Parse(transactionPostRequestV01.RequestId);
            }
            catch (ArgumentNullException)
            {
                return Guid.Empty;
            }
            catch (FormatException)
            {
                return Guid.Empty;
            }
        }

        /// <summary>
        /// Returns a view of the transaction contained within the request.
        /// </summary>
        /// <param name="transactionPostRequestV01"></param>
        /// <returns></returns>
        public static TransactionViewV01 TransactionViewExtract(this TransactionPostRequestV01 transactionPostRequestV01)
        {
            return transactionPostRequestV01?.Transaction;
        }
        #endregion

        #region Version 2
        /// <summary>
        /// Creates a <see cref="TransactionPostRequestV02"/> from a <see cref="Transaction"/> instance.
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static TransactionPostRequestV02 ToTransactionPostRequestV02(this Transaction transaction)
        {
            if (transaction == null) return null;

            var transactionViewV02 = transaction.ToTransactionViewV02();
            return new TransactionPostRequestV02
            {
                RequestId = Guid.NewGuid(),
                Id = transactionViewV02.Id,
                OriginatorId = transactionViewV02.OriginatorId,
                Disposition = transactionViewV02.Disposition,
                ForcePost = transactionViewV02.ForcePost,
                TransactionTimestamps = transactionViewV02.TransactionTimestamps,
                OriginatorElement = transactionViewV02.OriginatorElement,
                OperatorElement = transactionViewV02.OperatorElement,
                ControlElement = transactionViewV02.ControlElement,
                ArtsElement = transactionViewV02.ArtsElement,
                AttendanceElement = transactionViewV02.AttendanceElement,
                BoardElement = transactionViewV02.BoardElement,
                NameValues = transactionViewV02.NameValues
            };
        }

        /// <summary>
        /// Extracts the Request ID from the request.
        /// </summary>
        /// <param name="transactionPostRequestV02"></param>
        /// <returns></returns>
        public static Guid RequestIdExtract(this TransactionPostRequestV02 transactionPostRequestV02)
        {
            return transactionPostRequestV02?.RequestId ?? Guid.Empty;
        }

        /// <summary>
        /// Returns a view of the transaction contained within the request.
        /// </summary>
        /// <param name="transactionPostRequestV02"></param>
        /// <returns></returns>
        public static TransactionViewV02 TransactionViewExtract(this TransactionPostRequestV02 transactionPostRequestV02)
        {
            return new TransactionViewV02
            {
                Id = transactionPostRequestV02.Id,
                OriginatorId = transactionPostRequestV02.OriginatorId,
                Disposition = transactionPostRequestV02.Disposition,
                ForcePost = transactionPostRequestV02.ForcePost,
                TransactionTimestamps = transactionPostRequestV02.TransactionTimestamps,
                OriginatorElement = transactionPostRequestV02.OriginatorElement,
                OperatorElement = transactionPostRequestV02.OperatorElement,
                ControlElement = transactionPostRequestV02.ControlElement,
                ArtsElement = transactionPostRequestV02.ArtsElement,
                AttendanceElement = transactionPostRequestV02.AttendanceElement,
                BoardElement = transactionPostRequestV02.BoardElement,
                NameValues = transactionPostRequestV02.NameValues
            };
        }
        #endregion
    }
}
