﻿namespace BbTS.Domain.Models.Transaction.Processing
{
    /// <summary>
    /// Container class for a retail transaction line tender request.
    /// </summary>
    public class RetailTransactionLineTenderRequest : IProcessingRequest
    {
        /// <summary>
        /// The unique identifier for this request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The unique identifier for the transaction expressed as an integer.
        /// Number is created from the return value from a <see cref="RetailTransactionBeginProcessRequest"/>.
        /// </summary>
        public int TransactionId { get; set; }

        /// <summary>
        /// The order in which this line item is entered into the POS.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// Is the transaction void?
        /// </summary>
        public bool VoidFlag { get; set; }

        /// <summary>
        /// The identifier of the Tender used.
        /// </summary>
        public int TenderId { get; set; }

        /// <summary>
        /// The amount of tender used.
        /// </summary>
        public decimal TenderAmount { get; set; }

        /// <summary>
        /// The amount of tip included.
        /// </summary>
        public decimal TipAmount { get; set; }

        /// <summary>
        /// Total tax associated with this tender line item.  If null, information about the tax is not available.
        /// </summary>
        public decimal? TaxAmount { get; set; }

        /// <summary>
        /// The amount of tip included.
        /// </summary>
        public decimal RoundingAmount { get; set; }

        /// <summary>
        /// Commit when successful?
        /// </summary>
        public bool OnSuccessCommit { get; set; }
    }
}
