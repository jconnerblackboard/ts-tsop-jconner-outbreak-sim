﻿using BbTS.Domain.Models.Definitions.Transaction;

namespace BbTS.Domain.Models.Transaction.Processing
{
    /// <summary>
    /// Container class for the results of a line item processing operation.
    /// </summary>
    public class LineItemProcessingResult : ProcessingResult
    {
        /// <summary>
        /// The type of line item this processing result represents.
        /// </summary>
        public TransactionLineItemType LineItemType { get; set; }

        /// <summary>
        /// The type of line item this processing result represents.
        /// </summary>
        public string LineItemTypeText { get; set; }

        /// <summary>
        /// Parameterless constructor for (de)serialization.
        /// </summary>
        public LineItemProcessingResult()
        {
        }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="requestId">The unique identifier for the request that preempted the processing routine.</param>
        /// <param name="errorCode">The error code associated with the response.  0 means success.</param>
        /// <param name="deniedText">Reason for failure (if there was a failure error code).</param>
        /// <param name="type">The type of line item that was processed.</param>
        public LineItemProcessingResult(string requestId, int errorCode, string deniedText, TransactionLineItemType type)
        {
            RequestId = requestId;
            ErrorCode = errorCode;
            DeniedText = deniedText;
            LineItemType = type;
            LineItemTypeText = type.ToString();
        }
    }

    /// <summary>
    /// View for a <see cref="LineItemProcessingResult"/>.  (Version 1)
    /// </summary>
    public class LineItemProcessingResultViewV01
    {
        /// <summary>
        /// The unique identifier for the request that preempted the processing routine.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The error code associated with the response.  0 means success.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Reason for failure (if there was a failure error code).
        /// </summary>
        public string DeniedText { get; set; }

        /// <summary>
        /// The type of line item this processing result represents.
        /// </summary>
        public TransactionLineItemType LineItemType { get; set; }

        /// <summary>
        /// The type of line item this processing result represents.
        /// </summary>
        public string LineItemTypeText { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="LineItemProcessingResult"/> conversion.
    /// </summary>
    public static class LineItemProcessingResultConverter
    {
        /// <summary>
        /// Returns a <see cref="LineItemProcessingResultViewV01"/> object based on this <see cref="LineItemProcessingResult"/>.
        /// </summary>
        /// <param name="lineItemProcessingResult"></param>
        /// <returns></returns>
        public static LineItemProcessingResultViewV01 ToLineItemProcessingResultViewV01(this LineItemProcessingResult lineItemProcessingResult)
        {
            if (lineItemProcessingResult == null) return null;

            return new LineItemProcessingResultViewV01
            {
                RequestId = lineItemProcessingResult.RequestId,
                ErrorCode = lineItemProcessingResult.ErrorCode,
                DeniedText = lineItemProcessingResult.DeniedText,
                LineItemType = lineItemProcessingResult.LineItemType,
                LineItemTypeText = lineItemProcessingResult.LineItemTypeText
            };
        }

        /// <summary>
        /// Returns a <see cref="LineItemProcessingResult"/> object based on this <see cref="LineItemProcessingResultViewV01"/>.
        /// </summary>
        /// <param name="lineItemProcessingResult"></param>
        /// <returns></returns>
        public static LineItemProcessingResult ToLineItemProcessingResult(this LineItemProcessingResultViewV01 lineItemProcessingResult)
        {
            if (lineItemProcessingResult == null) return null;

            return new LineItemProcessingResult
            {
                RequestId = lineItemProcessingResult.RequestId,
                ErrorCode = lineItemProcessingResult.ErrorCode,
                DeniedText = lineItemProcessingResult.DeniedText,
                LineItemType = lineItemProcessingResult.LineItemType,
                LineItemTypeText = lineItemProcessingResult.LineItemTypeText
            };
        }
    }
}
