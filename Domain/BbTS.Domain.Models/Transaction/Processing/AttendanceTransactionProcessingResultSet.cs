﻿using System.Collections.Generic;
using System.Linq;

namespace BbTS.Domain.Models.Transaction.Processing
{
    /// <summary>
    /// Container class for a set of attendance transaction processing results.
    /// </summary>
    public class AttendanceTransactionProcessingResultSet : ProcessingResult
    {
        /// <summary>
        /// List of transaction results for all attendance transactions under this super set.
        /// </summary>
        public List<AttendanceTransactionProcessingResult> ProcessingResults { get; set; } = new List<AttendanceTransactionProcessingResult>();

        /// <summary>
        /// Parameter-less constructor.
        /// </summary>
        public AttendanceTransactionProcessingResultSet()
        {
        }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="requestId">The unique identifier for the request that preempted the processing routine.</param>
        public AttendanceTransactionProcessingResultSet(string requestId)
        {
            RequestId = requestId;
        }
    }

    /// <summary>
    /// View for a <see cref="AttendanceTransactionProcessingResultSet"/>.  (Version 1)
    /// </summary>
    public class AttendanceTransactionProcessingResultSetViewV01
    {
        /// <summary>
        /// The unique identifier for the request that preempted the processing routine.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The error code associated with the response.  0 means success.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Reason for failure (if there was a failure error code).
        /// </summary>
        public string DeniedText { get; set; }

        /// <summary>
        /// List of transaction results for all attendance transactions under this super set.
        /// </summary>
        public List<AttendanceTransactionProcessingResultViewV01> ProcessingResults { get; set; } = new List<AttendanceTransactionProcessingResultViewV01>();
    }

    /// <summary>
    /// Extension methods for <see cref="AttendanceTransactionProcessingResultSet"/> conversion.
    /// </summary>
    public static class AttendanceTransactionProcessingResultSetConverter
    {
        /// <summary>
        /// Returns a <see cref="AttendanceTransactionProcessingResultSetViewV01"/> object based on this <see cref="AttendanceTransactionProcessingResultSet"/>.
        /// </summary>
        /// <param name="attendanceTransactionProcessingResultSet"></param>
        /// <returns></returns>
        public static AttendanceTransactionProcessingResultSetViewV01 ToAttendanceTransactionProcessingResultSetViewV01(this AttendanceTransactionProcessingResultSet attendanceTransactionProcessingResultSet)
        {
            if (attendanceTransactionProcessingResultSet == null) return null;

            return new AttendanceTransactionProcessingResultSetViewV01
            {
                RequestId = attendanceTransactionProcessingResultSet.RequestId,
                ErrorCode = attendanceTransactionProcessingResultSet.ErrorCode,
                DeniedText = attendanceTransactionProcessingResultSet.DeniedText,
                ProcessingResults = attendanceTransactionProcessingResultSet.ProcessingResults.Select(attendanceTransactionProcessingResult => attendanceTransactionProcessingResult.ToAttendanceTransactionProcessingResultViewV01()).ToList()
            };
        }

        /// <summary>
        /// Returns a <see cref="AttendanceTransactionProcessingResultSet"/> object based on this <see cref="AttendanceTransactionProcessingResultSetViewV01"/>.
        /// </summary>
        /// <param name="attendanceTransactionProcessingResultSetViewV01"></param>
        /// <returns></returns>
        public static AttendanceTransactionProcessingResultSet ToAttendanceTransactionProcessingResultSet(this AttendanceTransactionProcessingResultSetViewV01 attendanceTransactionProcessingResultSetViewV01)
        {
            if (attendanceTransactionProcessingResultSetViewV01 == null) return null;

            return new AttendanceTransactionProcessingResultSet
            {
                RequestId = attendanceTransactionProcessingResultSetViewV01.RequestId,
                ErrorCode = attendanceTransactionProcessingResultSetViewV01.ErrorCode,
                DeniedText = attendanceTransactionProcessingResultSetViewV01.DeniedText,
                ProcessingResults = attendanceTransactionProcessingResultSetViewV01.ProcessingResults.Select(attendanceTransactionProcessingResultViewV01 => attendanceTransactionProcessingResultViewV01.ToAttendanceTransactionProcessingResult()).ToList()
            };
        }
    }
}
