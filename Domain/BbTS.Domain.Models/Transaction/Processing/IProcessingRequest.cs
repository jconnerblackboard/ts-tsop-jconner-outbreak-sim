﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Transaction.Processing
{
    /// <summary>
    /// Interface for defining a retail transaction processing request.
    /// </summary>
    public interface IProcessingRequest
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        string RequestId { get; set; }
    }
}
