﻿using System;
using BbTS.Domain.Models.Definitions.Container;

namespace BbTS.Domain.Models.Transaction.Processing
{
    /// <summary>
    /// Container class for a retail transaction end process request.
    /// </summary>
    public class RetailTransactionEndProcessRequest : IProcessingRequest
    {
        /// <summary>
        /// The id of the request that spawned this response.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The unique numerical identifier for the transaction.
        /// </summary>
        public int TransactionId { get; set; }

        /// <summary>
        /// The date and time the transaction took place.
        /// </summary>
        public DateTime TransactionDateTime { get; set; }

        /// <summary>
        /// The total amount of the transaction.
        /// </summary>
        public decimal TransactionTotal { get; set; }

        /// <summary>
        /// The total units involved in the transaction.
        /// </summary>
        public int UnitCount { get; set; }

        /// <summary>
        /// Count of line items scanned.
        /// </summary>
        public int LineItemsScannedCount { get; set; }

        /// <summary>
        /// Percent of Line Items Scanned (in millipercent)
        /// </summary>
        public decimal LineItemsScannedPercent { get; set; }

        /// <summary>
        /// Count of Line Items Keyed
        /// </summary>
        public int LineItemsKeyedCount { get; set; }

        /// <summary>
        /// Percentage of Line Items Keyed (in millipercent)
        /// </summary>
        public decimal LineItemsKeyedPercent { get; set; }

        /// <summary>
        /// Datetime of receipt
        /// </summary>
        public DateTime ReceiptDateTime { get; set; }

        /// <summary>
        /// The type of validation performed.
        /// </summary>
        public ValidationType ValidationType { get; set; }

        /// <summary>
        /// The validation code
        /// </summary>
        public int ValidationCode { get; set; }
    }
}
