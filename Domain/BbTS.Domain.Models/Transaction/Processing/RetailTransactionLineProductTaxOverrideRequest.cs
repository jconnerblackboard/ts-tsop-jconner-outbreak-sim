﻿namespace BbTS.Domain.Models.Transaction.Processing
{
    /// <summary>
    /// Container class for a retail transaction line product tax override request.
    /// </summary>
    public class RetailTransactionLineProductTaxOverrideRequest : IProcessingRequest
    {
        /// <summary>
        /// Unique identifier for the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The unique identifier for the transaction expressed as an integer.
        /// Number is created from the return value from a <see cref="RetailTransactionBeginProcessRequest"/>.
        /// </summary>
        public int TransactionId { get; set; }

        /// <summary>
        /// The order in which this line item is entered into the POS.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// The tax schedule id.
        /// </summary>
        public int TaxScheduleId { get; set; }

        /// <summary>
        /// The tax override reason type.
        /// </summary>
        public int TaxOverrideReasonType { get; set; }

        /// <summary>
        /// The taxable amount of the transaction.
        /// </summary>
        public decimal TaxableAmount { get; set; }

        /// <summary>
        /// The original taxable amount of the transaction.
        /// </summary>
        public decimal OriginalTaxableAmount { get; set; }

        /// <summary>
        /// The new taxable amount of the transaction.
        /// </summary>
        public decimal NewTaxAmount { get; set; }

        /// <summary>
        /// The original taxable percentage of the transaction.
        /// </summary>
        public decimal OriginalTaxPercent{ get; set; }

        /// <summary>
        /// The new taxable percentage of the transaction.
        /// </summary>
        public decimal NewTaxPercent{ get; set; }

        /// <summary>
        /// Commit when successful?
        /// </summary>
        public bool OnSuccessCommit { get; set; }
    }
}
