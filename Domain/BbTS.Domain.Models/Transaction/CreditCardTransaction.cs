﻿using System.ComponentModel;

namespace BbTS.Domain.Models.Transaction
{
    public class CreditCardTransaction
    {
        [DisplayName("Transaction Id")]
        public int BbPaygateTransactionId { get; set; }         //NUMBER(15,0)
        [DisplayName("Order Id")]
        public string BbPaygateOrderId { get; set; }            //VARCHAR2(36)
        [DisplayName("Date/Time")]
        public float TransactionDateTime { get; set; }          //FLOAT(126)
        [DisplayName("Type Id")]
        public int BbPaygateTransactionTypeId { get; set; }     //NUMBER(4,0)
        [DisplayName("Data Source Id")]
        public int BbPaygateTransctnDatasrcId { get; set; }     //NUMBER(4,0)
        [DisplayName("Signed")]
        public bool SignatureCaptured { get; set; }             //VARCHAR2(1)
        [DisplayName("Amount")]
        public double Amount { get; set; }                     //NUMBER(12,0)
        [DisplayName("Authorization code")]
        public string Authcode { get; set; }                    //VARCHAR2( 6 )
        [DisplayName("Commit Id")]
        public int BbPaygateTransactnCommitId { get; set; }     //NUMBER(4,0)
        [DisplayName("State Id")]
        public int BbPaygateTransactnStateId { get; set; }      //NUMBER(4,0)
        [DisplayName("Void State Id")]
        public int? VoidStateId { get; set; }                    //NUMBER(4,0)
        [DisplayName("Void Date/Time")]
        public float? VoidDateTime { get; set; }                 //FLOAT(126)
        [DisplayName("Credit Card IIN")]
        public int? CreditCardIin { get; set; }                  //NUMBER(6,0)
        [DisplayName("Credit Card Last 4 digits")]
        public int? CreditCardLast4 { get; set; }                //NUMBER(4,0)
        [DisplayName("External Id")]
        public string ExternalTransactionId { get; set; }       //VARCHAR2(36)
        [DisplayName("Store Id")]
        public int? BbPaygateConfigStoreId { get; set; }         //NUMBER(6,0)
        [DisplayName("Card holder present Id")]
        public int BbPaygateCardholderPrsntId { get; set; }     //NUMBER(4,0)
        [DisplayName("Terminal capacity Id")]
        public int BbPaygateTrnsctnTermCapId { get; set; }      //NUMBER(4,0)
        [DisplayName("Input Environment Id")]
        public int BbPaygateTrnscnInputEnvId { get; set; }      //NUMBER(4,0)
        [DisplayName("POS Id")]
        public int? PosId { get; set; }                          //NUMBER(10)
    }

    public class CreditCardTransactionView : CreditCardTransaction
    {
        [DisplayName("Type")]
        public string BbPaygateTransactionTypeName { get; set; }
        [DisplayName("Data Source")]
        public string BbPaygateTransctnDatasrcName { get; set; }
        [DisplayName("Commit")]
        public string BbPaygateTransactnCommitName { get; set; }
        [DisplayName("State")]
        public string BbPaygateTransactnStateName { get; set; }
        [DisplayName("Void")]
        public string VoidStateName { get; set; }
        [DisplayName("Store")]
        public string BbPaygateConfigStoreName { get; set; }
        [DisplayName("Card holder present")]
        public string BbPaygateCardholderPrsntName { get; set; }
        [DisplayName("Terminal capacity")]
        public string BbPaygateTrnsctnTermCapName { get; set; }
        [DisplayName("Input Environment")]
        public string BbPaygateTrnscnInputEnvName { get; set; }
        [DisplayName("POS")]
        public string PosName { get; set; }

    }
}
