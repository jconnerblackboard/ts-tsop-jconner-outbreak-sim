﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Transaction
{
    public class CustomerTransactionLocationLog
    {
        public string InstitutionId { get; set; }
        /// <summary>
        /// The customer's guid.
        /// </summary>
        public string CustomerGuid { get; set; }
        /// <summary>
        /// Custnum, --VARCHAR2(22 BYTE)
        /// </summary>
        public string CustNum { get; set; }
        /// <summary>
        /// MerchantName
        /// </summary>
        public string MerchantName { get; set; }
        /// <summary>
        /// ProfitCenterName
        /// </summary>
        public string ProfitCenterName { get; set; }
        /// <summary>
        /// PosName
        /// </summary>
        public string PosName { get; set; }
        /// <summary>
        /// The unique identifier (guid) for the pos device.
        /// </summary>
        public string PosId { get; set; }
        /// <summary>
        /// Udf_Functions.Udf_DateTimeNumberTo8601(DAT.actualdatetime) AS EntryDateTime --FLOAT
        /// </summary>
        public DateTimeOffset EntryDateTime { get; set; }
    }
}
