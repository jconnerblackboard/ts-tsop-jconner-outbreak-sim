﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.DoorAccess;

namespace BbTS.Domain.Models.Transaction
{
    public class CustomerActivityLog
    {
        public string InstitutionId { get; set; }
        /// <summary>
        /// The customer's guid.
        /// </summary>
        public string CustomerGuid { get; set; }
        /// <summary>
        /// Custnum, --VARCHAR2(22 BYTE)
        /// </summary>
        public string CustNum { get; set; }
        /// <summary>
        /// Building Name or Merchant Name
        /// </summary>
        public string LocationLevel1 { get; set; }
        /// <summary>
        /// Area Name or Profit Center Name
        /// </summary>
        public string LocationLevel2 { get; set; }
        /// <summary>
        /// The POS device name or the Door name
        /// </summary>
        public string LocationLevel3 { get; set; }
        /// <summary>
        /// The unique identifier for the L3 location
        /// </summary>
        public string LocationUniqueIdentifier { get; set; }
        /// <summary>
        /// The date and time the location was visited.
        /// </summary>
        public DateTimeOffset AccessDateTime { get; set; }

        public CustomerActivityLog()
        {
        }

        public CustomerActivityLog(CustomerTransactionLocationLog log)
        {
            InstitutionId = log.InstitutionId;
            CustomerGuid = log.CustomerGuid;
            CustNum = log.CustNum;
            LocationLevel1 = log.MerchantName;
            LocationLevel2 = log.ProfitCenterName;
            LocationLevel3 = log.PosName;
            LocationUniqueIdentifier = log.PosId;
            AccessDateTime = log.EntryDateTime;
        }

        public CustomerActivityLog(CustomerDoorAccessLog log)
        {
            InstitutionId = log.InstitutionId;
            CustomerGuid = log.CustomerGuid;
            CustNum = log.CustNum;
            LocationLevel1 = log.BuildingName;
            LocationLevel2 = log.AreaName;
            LocationLevel3 = log.DoorIdentifier;
            LocationUniqueIdentifier = log.DoorId.ToString("D");
            AccessDateTime = log.EntryDateTime;
        }
    }
}
