﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Transaction
{
    /// <summary>
    /// Transaction history get response
    /// </summary>
    public class TransactionHistoryListGetResponse
    {
        /// <summary>
        /// Original request
        /// </summary>
        public TransactionHistoryListGetRequest Request { get; set; }

        /// <summary>
        /// Total customer count
        /// </summary>
        public int TotalCount { get; set; }

        /// <summary>
        /// Transaction history
        /// </summary>
        public List<TsTransactionHistory> Transactions { get; set; }
    }
}
