﻿using BbTS.Domain.Models.Definitions.Container;
using System;

namespace BbTS.Domain.Models.Transaction
{
    /// <summary>
    /// This object represents a record in the Transact system for the customer transaction
    /// </summary>
    public class TsTransactionHistory
    {
        /// <summary>
        /// Transaction Id
        /// </summary>
        public Int64 TransactionId { get; set; }

        /// <summary>
        /// Transaction number
        /// </summary>
        public int TransactionNumber { get; set; }

        /// <summary>
        /// Is denied
        /// </summary>
        public bool IsDenied { get; set; }

        /// <summary>
        /// Transaction date time
        /// </summary>
        public DateTime TransactionDateTime { get; set; }

        /// <summary>
        /// Profit center where transaction was made
        /// </summary>
        public string ProfitCenterName { get; set; }

        /// <summary>
        /// Pos where transaction was made
        /// </summary>
        public string PosName { get; set; }

        /// <summary>
        /// Transaction type
        /// </summary>
        public RetailTranType? TransactionType { get; set; }

        /// <summary>
        /// Tenders
        /// </summary>
        public string Tenders { get; set; }

        /// <summary>
        /// Amount
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Is online
        /// </summary>
        public bool IsOnline { get; set; }

        /// <summary>
        /// Merchant Id
        /// </summary>
        public Int16? MerchantId { get; set; }
    }
}
