﻿namespace BbTS.Domain.Models.Transaction.Financial
{
    /// <summary>
    /// Links a tender ID to the specified tax rate.
    /// </summary>
    public class TenderTax
    {
        /// <summary>
        /// Tender ID.
        /// </summary>
        public int TenderId { get; set; }
        
        /// <summary>
        /// Tax rate.  (If 9.98% tax, TaxRate == 0.0998m)
        /// </summary>
        public decimal TaxRate { get; set; }
    }
}
