﻿using BbTS.Domain.Models.Definitions.Transaction;

namespace BbTS.Domain.Models.Transaction.Financial
{
    /// <summary>
    /// Defines a tender used to pay for a financial transaction.
    /// </summary>
    public class Tender
    {
        /// <summary>
        /// Tender ID as assigned by the BbTS DB.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Authorization source.
        /// </summary>
        public AuthorizationSource AuthorizationSource { get; set; }

        /// <summary>
        /// Specifies the type of tender.
        /// </summary>
        public TenderType TenderType { get; set; }

        /// <summary>
        /// Specifies currency rounding increment.  Most tenders will will have a rounding increment of 0.01.  In Canada, cash tenders have an increment of 0.05.
        /// </summary>
        public decimal RoundingIncrement { get; set; } = 0.01m;

        /// <summary>
        /// Tender name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Short version of the tender name for use in space limited scenarios.
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// Unique code name for the tender, up to 10 characters, no spaces.
        /// </summary>
        public string CodeName { get; set; }
    }
}
