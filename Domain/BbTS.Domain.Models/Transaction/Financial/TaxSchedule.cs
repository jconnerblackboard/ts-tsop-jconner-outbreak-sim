﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Transaction.Financial
{
    /// <summary>
    /// Defines the tax schedule to apply to a product.
    /// </summary>
    public class TaxSchedule
    {
        /// <summary>
        /// TaxSchedule ID as assigned by the BbTS DB.
        /// </summary>
        public int TaxScheduleId { get; set; }

        /// <summary>
        /// Name of tax schedule.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Tax account ID as assigned by the BbTS DB.
        /// </summary>
        public int TaxAccountId { get; set; }

        /// <summary>
        /// Name of the tax account corresponding to the TaxAccountId
        /// </summary>
        public string TaxAccountName { get; set; }

        /// <summary>
        /// Indicates if the quantity of a product being sold can be tax exempt.
        /// </summary>
        public bool TaxExemptQuantityEnabled { get; set; }

        /// <summary>
        /// Quantity that can be tax exempt.
        /// </summary>
        public int TaxExemptQuantity { get; set; }

        /// <summary>
        /// Indicates if the minimum amount of the sum of product being sold can be taxed.
        /// </summary>
        public bool TaxableAmountMinimumEnabled { get; set; }

        /// <summary>
        /// Minimum amount to be taxable.
        /// </summary>
        public decimal TaxableAmountMinimum { get; set; }

        /// <summary>
        /// Indicates if the maximum amount of the sum of product being sold can be taxed.
        /// </summary>
        public bool TaxableAmountMaximumEnabled { get; set; }

        /// <summary>
        /// Maximum amount to be taxable.
        /// </summary>
        public decimal TaxableAmountMaximum { get; set; }

        /// <summary>
        /// TaxSchedule Id of a tax schedule linked to this one.
        /// </summary>
        public int TaxLinkTaxScheduleId { get; set; }

        /// <summary>
        /// Tax Link should not tax any other tax.  (i.e. This will be true if this tax is taxable when calculations are peformed on the linked tax.)
        /// </summary>
        public bool TaxLinkTaxTaxes { get; set; }

        /// <summary>
        /// Lists tenders and tax rates for this tax schedule.
        /// </summary>
        public List<TenderTax> TenderTaxList { get; set; }
    }
}