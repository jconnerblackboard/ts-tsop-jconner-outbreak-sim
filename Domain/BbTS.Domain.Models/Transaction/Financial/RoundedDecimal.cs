﻿using System;

namespace BbTS.Domain.Models.Transaction.Financial
{
    /// <summary>
    /// A class that assists in both rounding and tracking error that results from rounding.
    /// </summary>
    public class RoundedDecimal
    {
        private decimal _preciseValue;

        /// <summary>
        /// Actual, unrounded value.
        /// </summary>
        public decimal PreciseValue
        {
            get { return _preciseValue; }

            set
            {
                _preciseValue = value;
                Calculate();
            }
        }

        private decimal _placesOperand;
        private byte _places;

        /// <summary>
        /// Number of places to use for the RoundedValue.
        /// </summary>
        public byte Places
        {
            get { return _places; }

            set
            {
                _places = value;
                Calculate();
            }
        }

        /// <summary>
        /// Value rounded to the defined Places value.
        /// </summary>
        public decimal RoundedValue { get; private set; }

        /// <summary>
        /// The amount of error that results from RoundedValue - PreciseValue.
        /// </summary>
        public decimal RoundingAmount { get; private set; }

        /// <summary>
        /// Creates a new instance of the RoundedDecimal class.
        /// </summary>
        public RoundedDecimal()
        {
            //Parameter-less constructor for serialization
            Calculate();
        }

        /// <summary>
        /// Creates a new instance of the RoundedDecimal class, initialized to the specified precise value and places.
        /// </summary>
        /// <param name="preciseValue"></param>
        /// <param name="places"></param>
        public RoundedDecimal(decimal preciseValue, byte places)
        {
            _preciseValue = preciseValue;
            _places = places;
            Calculate();
        }

        private void Calculate()
        {
            //Compact Framework's decimal.Round and Math.Round use "round to even".
            //Since we don't have an "away from zero" option, we'll roll our own.
            //(Assume _preciseValue = -0.045 and _places = 2 for comment example.)
            int sign = (_preciseValue < 0) ? -1 : 1;                            //-0.045 sets sign to -1
            _placesOperand = (decimal)(Math.Pow(10, _places));                  //100.0m
            decimal d = (_preciseValue * _placesOperand * 10) + (5 * sign);     //-0.045 -> -50.0        
            int i = (int)d / 10;                                                //-50.0 -> -5
            RoundedValue = i / _placesOperand;                                  //-5 -> -0.05
            RoundingAmount = RoundedValue - _preciseValue;
        }

        /// <summary>
        /// Adjusts for accumulated error.
        /// </summary>
        /// <param name="accumulatedError">The accumulated error.</param>
        public void AdjustForAccumulatedError(ref decimal accumulatedError)
        {
            decimal roundingThreshold = 5.0m / (_placesOperand * 10);
            decimal adjustmentAmount = roundingThreshold * 2;

            accumulatedError += RoundingAmount;

            //There *should* never be enough error for this loop to execute more than once, but the loop guarantees that -0.005 < accumulatedError <= 0.005.
            while ((accumulatedError <= -roundingThreshold) || (accumulatedError > roundingThreshold))
            {
                if (accumulatedError <= -roundingThreshold)
                {
                    accumulatedError += adjustmentAmount;
                    RoundedValue += adjustmentAmount;
                    RoundingAmount += adjustmentAmount;
                }
                else if (accumulatedError > roundingThreshold)
                {
                    accumulatedError -= adjustmentAmount;
                    RoundedValue -= adjustmentAmount;
                    RoundingAmount -= adjustmentAmount;
                }
            }
        }
    }
}