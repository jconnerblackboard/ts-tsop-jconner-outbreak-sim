﻿namespace BbTS.Domain.Models.Transaction
{
    /// <summary>
    /// Container class for retail transaction attributes (used for retail transaction validation).
    /// </summary>
    public class RetailTransactionAttributes
    {
        /// <summary>
        /// The Period Number.
        /// </summary>
        public int PeriodNumber { get; set; }

        /// <summary>
        /// The transaction type code.
        /// </summary>
        public int TransactionTypeCode { get; set; }

        /// <summary>
        /// The validation type.
        /// </summary>
        public int ValidationType { get; set; }

        /// <summary>
        /// The attend type.
        /// </summary>
        public int AttendType { get; set; }

        /// <summary>
        /// The retail tran type.
        /// </summary>
        public int RetailTranType { get; set; }
    }

    /// <summary>
    /// View for a <see cref="RetailTransactionAttributes"/>.  (Version 1)
    /// </summary>
    public class RetailTransactionAttributesViewV01
    {
        /// <summary>
        /// The Period Number.
        /// </summary>
        public int PeriodNumber { get; set; }

        /// <summary>
        /// The transaction type code.
        /// </summary>
        public int TransactionTypeCode { get; set; }

        /// <summary>
        /// The validation type.
        /// </summary>
        public int ValidationType { get; set; }

        /// <summary>
        /// The attend type.
        /// </summary>
        public int AttendType { get; set; }

        /// <summary>
        /// The retail tran type.
        /// </summary>
        public int RetailTranType { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="RetailTransactionAttributes"/> conversion.
    /// </summary>
    public static class RetailTransactionAttributesConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="RetailTransactionAttributesViewV01"/> object based on this <see cref="RetailTransactionAttributes"/>.
        /// </summary>
        /// <param name="retailTransactionAttributes"></param>
        /// <returns></returns>
        public static RetailTransactionAttributesViewV01 ToRetailTransactionAttributesViewV01(this RetailTransactionAttributes retailTransactionAttributes)
        {
            if (retailTransactionAttributes == null) return null;

            return new RetailTransactionAttributesViewV01
            {
                PeriodNumber = retailTransactionAttributes.PeriodNumber,
                TransactionTypeCode = retailTransactionAttributes.TransactionTypeCode,
                ValidationType = retailTransactionAttributes.ValidationType,
                AttendType = retailTransactionAttributes.AttendType,
                RetailTranType = retailTransactionAttributes.RetailTranType
            };
        }

        /// <summary>
        /// Returns a <see cref="RetailTransactionAttributes"/> object based on this <see cref="RetailTransactionAttributesViewV01"/>.
        /// </summary>
        /// <param name="retailTransactionAttributesViewV01"></param>
        /// <returns></returns>
        public static RetailTransactionAttributes ToRetailTransactionAttributes(this RetailTransactionAttributesViewV01 retailTransactionAttributesViewV01)
        {
            if (retailTransactionAttributesViewV01 == null) return null;

            return new RetailTransactionAttributes
            {
                PeriodNumber = retailTransactionAttributesViewV01.PeriodNumber,
                TransactionTypeCode = retailTransactionAttributesViewV01.TransactionTypeCode,
                ValidationType = retailTransactionAttributesViewV01.ValidationType,
                AttendType = retailTransactionAttributesViewV01.AttendType,
                RetailTranType = retailTransactionAttributesViewV01.RetailTranType
            };
        }
        #endregion
    }
}
