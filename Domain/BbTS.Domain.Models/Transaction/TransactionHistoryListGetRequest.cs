﻿using BbTS.Domain.Models.Definitions.Container;
using System;

namespace BbTS.Domain.Models.Transaction
{
    /// <summary>
    /// Transaction history get request
    /// </summary>
    public class TransactionHistoryListGetRequest
    {
        /// <summary>
        /// Customer guid
        /// </summary>
        public DateTime StartDate { get; set; } = DateTime.Now.Subtract(new TimeSpan(14, 0, 0, 0));

        /// <summary>
        /// Board plan guid
        /// </summary>
        public DateTime EndDate { get; set; } = DateTime.Now;

        /// <summary>
        /// TransactionNumber
        /// </summary>
        public int? TransactionNumber { get; set; }

        /// <summary>
        /// Is denied
        /// </summary>
        public bool? IsDenied { get; set; }

        /// <summary>
        /// Profit center where transaction was made
        /// </summary>
        public string ProfitCenterName { get; set; }

        /// <summary>
        /// Pos where transaction was made
        /// </summary>
        public string PosName { get; set; }

        /// <summary>
        /// Transaction type
        /// </summary>
        public RetailTranType? TransactionType { get; set; }

        /// <summary>
        /// Tenders
        /// </summary>
        public string Tenders { get; set; }

        /// <summary>
        /// Offset of the result
        /// </summary>
        public int Offset { get; set; }

        /// <summary>
        /// Number of items per page
        /// </summary>
        public int? PageSize { get; set; }
    }
}
