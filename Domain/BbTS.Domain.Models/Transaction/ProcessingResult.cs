﻿namespace BbTS.Domain.Models.Transaction
{
    /// <summary>
    /// Container class for a processing result.
    /// </summary>
    public class ProcessingResult
    {
        /// <summary>
        /// The unique identifier for the request that preempted the processing routine.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The error code associated with the response.  0 means success.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Reason for failure (if there was a failure error code).
        /// </summary>
        public string DeniedText { get; set; }
    }

    /// <summary>
    /// View for a <see cref="ProcessingResult"/>.  (Version 1)
    /// </summary>
    public class ProcessingResultViewV01
    {
        /// <summary>
        /// The unique identifier for the request that preempted the processing routine.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The error code associated with the response.  0 means success.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Reason for failure (if there was a failure error code).
        /// </summary>
        public string DeniedText { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="ProcessingResult"/> conversion.
    /// </summary>
    public static class ProcessingResultConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="ProcessingResultViewV01"/> object based on this <see cref="ProcessingResult"/>.
        /// </summary>
        /// <param name="processingResult"></param>
        /// <returns></returns>
        public static ProcessingResultViewV01 ToProcessingResultViewV01(this ProcessingResult processingResult)
        {
            if (processingResult == null) return null;

            return new ProcessingResultViewV01
            {
                RequestId = processingResult.RequestId,
                ErrorCode = processingResult.ErrorCode,
                DeniedText = processingResult.DeniedText
            };
        }

        /// <summary>
        /// Returns a <see cref="ProcessingResult"/> object based on this <see cref="ProcessingResultViewV01"/>.
        /// </summary>
        /// <param name="processingResultViewV01"></param>
        /// <returns></returns>
        public static ProcessingResult ToProcessingResult(this ProcessingResultViewV01 processingResultViewV01)
        {
            if (processingResultViewV01 == null) return null;

            return new ProcessingResult
            {
                RequestId = processingResultViewV01.RequestId,
                ErrorCode = processingResultViewV01.ErrorCode,
                DeniedText = processingResultViewV01.DeniedText
            };
        }
        #endregion
    }
}
