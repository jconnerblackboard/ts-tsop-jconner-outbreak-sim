﻿using BbTS.Domain.Models.Definitions.ArtsDataModel;
using System;

namespace BbTS.Domain.Models.Transaction.StoredValue
{
    /// <summary>
    /// Stored value transaction.
    /// </summary>
    public class StoredValueTransaction
    {
        public int PosId { get; set; }
        public string CardNumber { get; set; }
        public string CustomerNumber { get; set; }

        /// <summary>
        /// The datetime when the transaction was created/recorded in DB.
        /// </summary>
        public DateTime CreatedDateTime { get; set; }

        /// <summary>
        /// The debit or credit type of the stored value account.
        /// </summary>
        public DebitCreditType DebitCreditType { get; set; }

        /// <summary>
        /// The amount of the whole transaction.
        /// </summary>
        public string TransactionTotalAmount { get; set; }

        /// <summary>
        /// The amount of the this line-item of the transaction.
        /// </summary>
        public string TransactionItemAmount { get; set; }

        public long MerchantId { get; set; }
        public long TransactionId { get; set; }
        public long TransactionItemId { get; set; }

        public string MerchantName { get; set; }
        //public string ProcessingInfo { get; set; }
    }
}