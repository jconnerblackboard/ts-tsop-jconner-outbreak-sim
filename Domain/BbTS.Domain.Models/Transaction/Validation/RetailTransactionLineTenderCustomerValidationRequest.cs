﻿namespace BbTS.Domain.Models.Transaction.Validation
{
    /// <summary>
    /// Container class for a retail transaction line tender customer validation request.
    /// </summary>
    public class RetailTransactionLineTenderCustomerValidationRequest
    {
        /// <summary>
        /// The physical id type.
        /// </summary>
        public int PhysicalIdType { get; set; }

        /// <summary>
        /// The customer entry method type.
        /// </summary>
        public int CustomerEntryMethodType { get; set; }

        /// <summary>
        /// The unique identifier (Guid) assigned to the customer.
        /// </summary>
        public string CustomerGuid { get; set; }

    }
}
