﻿namespace BbTS.Domain.Models.Transaction.Validation
{
    /// <summary>
    /// Container class for the response to a retail transaction line product validation request.
    /// </summary>
    public class RetailTransactionLineProductValidationResponse
    {
        /// <summary>
        /// The request that spawned this response.
        /// </summary>
        public RetailTransactionLineProductValidationRequest Request { get; internal set; }

        /// <summary>
        /// The error code associated with the response.  0 means success.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Reason for failure (if there was a failure error code).
        /// </summary>
        public string DeniedText { get; set; }

        /// <summary>
        /// Parameterized constructor requiring the request that spawned this response.
        /// </summary>
        /// <param name="request">The request that spawned this response.</param>
        public RetailTransactionLineProductValidationResponse(RetailTransactionLineProductValidationRequest request)
        {
            Request = request;
        }
    }
}
