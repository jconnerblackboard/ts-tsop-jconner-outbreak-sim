﻿namespace BbTS.Domain.Models.Transaction.Validation
{
    /// <summary>
    /// Container class for a retail transaciton line product validation request.
    /// </summary>
    public class RetailTransactionLineProductValidationRequest
    {
        /// <summary>
        /// The product detail id.
        /// </summary>
        public int ProductDetailId { get; set; }

        /// <summary>
        /// The tax schedule id.
        /// </summary>
        public int TaxScheduleId { get; set; }

        /// <summary>
        /// The tax group id.
        /// </summary>
        public int? TaxGroupId { get; set; }

        /// <summary>
        /// The unit measure type.
        /// </summary>
        public int UnitMeasureType { get; set; }

        /// <summary>
        /// The product entry method type.
        /// </summary>
        public int ProductEntryMethodType { get; set; }

        /// <summary>
        /// The retail price entry method type.
        /// </summary>
        public int RetailPriceEntryMethodType { get; set; }
    }
}
