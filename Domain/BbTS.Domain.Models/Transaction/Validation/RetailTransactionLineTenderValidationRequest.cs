﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Definitions.Transaction;

namespace BbTS.Domain.Models.Transaction.Validation
{
    /// <summary>
    /// Container class for a retail
    /// </summary>
    public class RetailTransactionLineTenderValidationRequest
    {
        /// <summary>
        /// The unique identifier for this request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The unique (Guid) identifier assigned to the device.
        /// </summary>
        public string DeviceGuid { get; set; }

        /// <summary>
        /// The id of the tender to verify.
        /// </summary>
        public int TenderId { get; set; }

        public TenderType TenderType { get; set; }
    }
}
