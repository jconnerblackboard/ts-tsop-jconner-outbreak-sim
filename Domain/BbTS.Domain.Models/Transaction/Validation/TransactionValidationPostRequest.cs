﻿using BbTS.Domain.Models.Container;

namespace BbTS.Domain.Models.Transaction.Validation
{
    /// <summary>
    /// Container class for a transaction validation post request.  (Version 1)
    /// </summary>
    public class TransactionValidationPostRequestV01
    {
        /// <summary>
        /// The unique identifier for this request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The transaction.
        /// </summary>
        public TransactionViewV01 Transaction { get; set; }
    }
}
