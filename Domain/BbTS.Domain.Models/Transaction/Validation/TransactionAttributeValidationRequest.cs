﻿namespace BbTS.Domain.Models.Transaction.Validation
{
    /// <summary>
    /// Container class for a transaction attribute validation request.
    /// </summary>
    public class TransactionAttributeValidationRequest
    {
        /// <summary>
        /// The unique identifier for this request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The unique (Guid) identifier assigned to the device.
        /// </summary>
        public string DeviceGuid { get; set; }

        /// <summary>
        /// Retail transaction attributes.
        /// </summary>
        public RetailTransactionAttributes Attributes { get; set; }
    }
}
