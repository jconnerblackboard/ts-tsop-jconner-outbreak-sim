﻿using System.Collections.Generic;
using System.Linq;
using BbTS.Domain.Models.Definitions.Transaction;

namespace BbTS.Domain.Models.Transaction.Validation
{
    /// <summary>
    /// Container class for the response to a transaction validation post request.
    /// </summary>
    public class TransactionValidationPostResponse
    {
        /// <summary>
        /// The unique identifier for this request.
        /// </summary>
        public string RequestId { get; internal set; }

        /// <summary>
        /// The aggregrate result of the validation process based on the results in the <see cref="Results"/> collection.
        /// </summary>
        public TransactionValidationResultEnumeration AggregrateResult { get; internal set; }

        /// <summary>
        /// The aggregrate result of the validation process based on the results in the <see cref="Results"/> collection.
        /// </summary>
        public string AggregrateResultText { get; internal set; }

        /// <summary>
        /// The results of the validation operation.
        /// </summary>
        public List<TransactionValidationResult> Results { get; internal set; }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="requestId">The unique identifier for this request.</param>
        /// <param name="results">The results of the validation operation.</param>
        public TransactionValidationPostResponse(string requestId, List<TransactionValidationResult> results )
        {
            RequestId = requestId;
            Results = results;

            AggregrateResult = Results.FirstOrDefault(item =>
                item.Result == TransactionValidationResultEnumeration.Fail) != null ?
                    TransactionValidationResultEnumeration.Fail :
                    TransactionValidationResultEnumeration.Pass;

            AggregrateResultText = AggregrateResult.ToString();
        }
    }

    /// <summary>
    /// View for a <see cref="TransactionValidationPostResponse"/>.  (Version 1)
    /// </summary>
    public class TransactionValidationPostResponseV01
    {
        /// <summary>
        /// The unique identifier for this request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The aggregrate result of the validation process based on the results in the <see cref="Results"/> collection.
        /// </summary>
        public TransactionValidationResultEnumeration AggregrateResult { get; set; }

        /// <summary>
        /// The aggregrate result of the validation process based on the results in the <see cref="Results"/> collection.
        /// </summary>
        public string AggregrateResultText { get; set; }

        /// <summary>
        /// The results of the validation operation.
        /// </summary>
        public List<TransactionValidationResultViewV01> Results { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="TransactionValidationPostResponse"/> conversion.
    /// </summary>
    public static class TransactionValidationPostResponseConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="TransactionValidationPostResponseV01"/> object based on this <see cref="TransactionValidationPostResponse"/>.
        /// </summary>
        /// <param name="transactionValidationPostResponse"></param>
        /// <returns></returns>
        public static TransactionValidationPostResponseV01 ToTransactionValidationPostResponseViewV01(this TransactionValidationPostResponse transactionValidationPostResponse)
        {
            if (transactionValidationPostResponse == null) return null;

            return new TransactionValidationPostResponseV01
            {
                RequestId = transactionValidationPostResponse.RequestId,
                AggregrateResult = transactionValidationPostResponse.AggregrateResult,
                AggregrateResultText = transactionValidationPostResponse.AggregrateResultText,
                Results = transactionValidationPostResponse.Results.Select(transactionValidationResult => transactionValidationResult.ToTransactionValidationResultViewV01()).ToList()
            };
        }

        /// <summary>
        /// Returns a <see cref="TransactionValidationPostResponse"/> object based on this <see cref="TransactionValidationPostResponseV01"/>.
        /// </summary>
        /// <param name="transactionValidationPostResponseViewV01"></param>
        /// <returns></returns>
        public static TransactionValidationPostResponse ToTransactionValidationPostResponse(this TransactionValidationPostResponseV01 transactionValidationPostResponseViewV01)
        {
            if (transactionValidationPostResponseViewV01 == null) return null;

            var results = transactionValidationPostResponseViewV01.Results.Select(transactionValidationResultViewV01 => transactionValidationResultViewV01.ToTransactionValidationResult()).ToList();
            return new TransactionValidationPostResponse(transactionValidationPostResponseViewV01.RequestId, results);
        }
        #endregion
    }
}
