﻿using BbTS.Domain.Models.Definitions.Transaction;

namespace BbTS.Domain.Models.Transaction.Validation
{
    /// <summary>
    /// 
    /// </summary>
    public class TransactionValidationResult : ProcessingResult
    {
        /// <summary>
        /// The validation function that was performed.
        /// </summary>
        public TransactionFunctionDefinition Function { get; internal set; }

        /// <summary>
        /// The validation function as a string.
        /// </summary>
        public string ValidationFunctionText { get; set; }

        /// <summary>
        /// Result.
        /// </summary>
        public TransactionValidationResultEnumeration Result { get; internal set; }

        /// <summary>
        /// String representation of the <see cref="TransactionValidationResultEnumeration"/> result.
        /// </summary>
        public string ResultString { get; internal set; }

        /// <summary>
        /// The message associated with the validation results.
        /// </summary>
        public string Message { get; internal set; }

        /// <summary>
        /// Parameter-less constructor.
        /// </summary>
        public TransactionValidationResult()
        {
        }

        /// <summary>
        /// Parameterized constructor
        /// </summary>
        /// <param name="requestId">The id associated with the request that started the validation process.</param>
        /// <param name="function">The validation function that was performed.</param>
        /// <param name="message">The message associated with the validation results.</param>
        /// <param name="result">The results of the validaiton operation.</param>
        public TransactionValidationResult(
            string requestId,
            TransactionFunctionDefinition function,
            string message,
            TransactionValidationResultEnumeration result)
        {
            RequestId = requestId;
            Function = function;
            Message = message;
            Result = result;
            ValidationFunctionText = function.ToString();
            ResultString = result.ToString();
        }

        /// <summary>
        /// Create a formatted string from this validation request.
        /// </summary>
        /// <param name="includeRequestId">Include the request id in the message.</param>
        /// <returns>Formatted message.</returns>
        public string CreateString(bool includeRequestId = false)
        {
            var requestId = includeRequestId ? $"RequestId '{RequestId}': " : string.Empty;
            //message = $"{message} [{subresult.Function}] '{subresult.Message}',";
            var message = Message.Length > 0 ? $": '{Message}'" : string.Empty;
            return $"{requestId}[{Function}] - {Result}{message}";
        }
    }

    /// <summary>
    /// View for a <see cref="TransactionValidationResult"/>.  (Version 1)
    /// </summary>
    public class TransactionValidationResultViewV01
    {
        /// <summary>
        /// The unique identifier for the request that preempted the processing routine.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The error code associated with the response.  0 means success.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Reason for failure (if there was a failure error code).
        /// </summary>
        public string DeniedText { get; set; }
        
        /// <summary>
        /// The validation function that was performed.
        /// </summary>
        public TransactionFunctionDefinition Function { get; internal set; }

        /// <summary>
        /// The validation function as a string.
        /// </summary>
        public string ValidationFunctionText { get; set; }

        /// <summary>
        /// Result.
        /// </summary>
        public TransactionValidationResultEnumeration Result { get; internal set; }

        /// <summary>
        /// String representation of the <see cref="TransactionValidationResultEnumeration"/> result.
        /// </summary>
        public string ResultString { get; internal set; }

        /// <summary>
        /// The message associated with the validation results.
        /// </summary>
        public string Message { get; internal set; }
    }

    /// <summary>
    /// Extension methods for <see cref="TransactionValidationResult"/> conversion.
    /// </summary>
    public static class TransactionValidationResultConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="TransactionValidationResultViewV01"/> object based on this <see cref="TransactionValidationResult"/>.
        /// </summary>
        /// <param name="transactionValidationResult"></param>
        /// <returns></returns>
        public static TransactionValidationResultViewV01 ToTransactionValidationResultViewV01(this TransactionValidationResult transactionValidationResult)
        {
            if (transactionValidationResult == null) return null;

            return new TransactionValidationResultViewV01
            {
                RequestId = transactionValidationResult.RequestId,
                ErrorCode = transactionValidationResult.ErrorCode,
                DeniedText = transactionValidationResult.DeniedText,
                Function = transactionValidationResult.Function,
                ValidationFunctionText = transactionValidationResult.ValidationFunctionText,
                Result = transactionValidationResult.Result,
                ResultString = transactionValidationResult.ResultString,
                Message = transactionValidationResult.Message
            };
        }

        /// <summary>
        /// Returns a <see cref="TransactionValidationResult"/> object based on this <see cref="TransactionValidationResultViewV01"/>.
        /// </summary>
        /// <param name="transactionValidationResultViewV01"></param>
        /// <returns></returns>
        public static TransactionValidationResult ToTransactionValidationResult(this TransactionValidationResultViewV01 transactionValidationResultViewV01)
        {
            if (transactionValidationResultViewV01 == null) return null;

            return new TransactionValidationResult
            {
                RequestId = transactionValidationResultViewV01.RequestId,
                ErrorCode = transactionValidationResultViewV01.ErrorCode,
                DeniedText = transactionValidationResultViewV01.DeniedText,
                Function = transactionValidationResultViewV01.Function,
                ValidationFunctionText = transactionValidationResultViewV01.ValidationFunctionText,
                Result = transactionValidationResultViewV01.Result,
                ResultString = transactionValidationResultViewV01.ResultString,
                Message = transactionValidationResultViewV01.Message
            };
        }
        #endregion
    }
}
