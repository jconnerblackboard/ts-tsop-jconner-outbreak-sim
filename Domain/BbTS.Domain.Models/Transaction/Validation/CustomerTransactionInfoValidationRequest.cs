﻿using System;

namespace BbTS.Domain.Models.Transaction.Validation
{
    /// <summary>
    /// Container class for a customer transaction info validation request.
    /// </summary>
    public class CustomerTransactionInfoValidationRequest
    {
        /// <summary>
        /// The unique identifier for this request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The customer's card number.
        /// </summary>
        public string CardNumber { get; set; }

        /// <summary>
        /// The issue number for the customer's card.
        /// </summary>
        public string IssueNumber { get; set; }

        /// <summary>
        /// Indicates whether or not the issue number was captured.
        /// </summary>
        public bool IssueNumberCaptured { get; set; }

        /// <summary>
        /// The customer's PIN.  Null if not provided or not present.
        /// </summary>
        public int? Pin { get; set; }

        /// <summary>
        /// The data and time the transaction occured.
        /// </summary>
        public DateTime TransactionDateTime { get; set; }

        /// <summary>
        /// The unique identifier (Guid) assigned to the customer.
        /// </summary>
        public string CustomerGuid { get; set; }
    }
}
