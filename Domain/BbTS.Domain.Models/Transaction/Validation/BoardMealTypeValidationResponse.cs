﻿namespace BbTS.Domain.Models.Transaction.Validation
{
    /// <summary>
    /// Container class for the response to a board meal type validation request.
    /// </summary>
    public class BoardMealTypeValidationResponse
    {
        /// <summary>
        /// The request that spawned this response.
        /// </summary>
        public BoardMealTypeValidationRequest Request { get; set; }

        /// <summary>
        /// The error code associated with the response.  0 means success.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Reason for failure (if there was a failure error code).
        /// </summary>
        public string DeniedText { get; set; }
    }
}
