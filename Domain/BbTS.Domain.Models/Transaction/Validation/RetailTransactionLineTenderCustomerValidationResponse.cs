﻿namespace BbTS.Domain.Models.Transaction.Validation
{
    /// <summary>
    /// Container class for a response to a retail transaction line tender customer validation request.
    /// </summary>
    public class RetailTransactionLineTenderCustomerValidationResponse
    {
        /// <summary>
        /// The request that spawned this response.
        /// </summary>
        public RetailTransactionLineTenderCustomerValidationRequest Request { get; set; }

        /// <summary>
        /// The error code associated with the response.  0 means success.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Reason for failure (if there was a failure error code).
        /// </summary>
        public string DeniedText { get; set; }

        /// <summary>
        /// Parameterized constructor requiring the request that spawned this response.
        /// </summary>
        /// <param name="request">The request that spawned this response.</param>
        public RetailTransactionLineTenderCustomerValidationResponse(RetailTransactionLineTenderCustomerValidationRequest request)
        {
            Request = request;
        }
    }
}
