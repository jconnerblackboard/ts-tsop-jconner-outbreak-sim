﻿namespace BbTS.Domain.Models.Transaction.Validation
{
    /// <summary>
    /// Container class for a board meal type validation request.
    /// </summary>
    public class BoardMealTypeValidationRequest
    {
        /// <summary>
        /// The unique identifier for this request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The board meal type id.
        /// </summary>
        public int BoardMealTypeId { get; set; }
    }
}
