﻿namespace BbTS.Domain.Models.Transaction.Validation
{
    /// <summary>
    /// Container class for a cashier by cashier num validation request.
    /// </summary>
    public class OperatorByOperatorGuidValidationResponse
    {
        /// <summary>
        /// The unique (Guid) identification value assigned to the cashier (operator).
        /// </summary>
        public string OperatorGuid { get; set; }

        /// <summary>
        /// The unique (Guid) identifier assigned to the device.
        /// </summary>
        public string DeviceGuid { get; set; }

        /// <summary>
        /// The error code associated with the operation.  0 means success.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// The text message associated with the operation results.
        /// </summary>
        public string DeniedText { get; set; }
    }
}
