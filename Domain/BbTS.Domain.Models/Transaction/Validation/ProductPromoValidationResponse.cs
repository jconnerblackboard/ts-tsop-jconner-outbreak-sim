﻿namespace BbTS.Domain.Models.Transaction.Validation
{
    /// <summary>
    /// Container class for a response to a product promo validation request.
    /// </summary>
    public class ProductPromoValidationResponse
    {
        /// <summary>
        /// The product promo id.
        /// </summary>
        public int ProductPromoId { get; internal set; }

        /// <summary>
        /// The error code associated with the response.  0 is success.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// The denied text message (if the request failed).
        /// </summary>
        public string DeniedText { get; set; }

        /// <summary>
        /// Parameterized constructor requiring the request that initiated this response.
        /// </summary>
        /// <param name="productPromoId">The product promo id.</param>
        public ProductPromoValidationResponse(int productPromoId)
        {
            ProductPromoId = productPromoId;
        }
    }
}
