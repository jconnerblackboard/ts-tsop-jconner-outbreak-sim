﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Transaction.Validation
{
    /// <summary>
    /// Container class for a customer transaction info validation response.
    /// </summary>
    public class CustomerTransactionInfoValidationResponse
    {
        /// <summary>
        /// The request that spawned this response.
        /// </summary>
        public CustomerTransactionInfoValidationRequest Request { get; set; }

        /// <summary>
        /// The error code associated with the response.  0 means success.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Reason for failure (if there was a failure error code).
        /// </summary>
        public string DeniedText { get; set; }

        /// <summary>
        /// Parameterized constructor requiring the request that spawned this response.
        /// </summary>
        /// <param name="request">The request that spawned this response.</param>
        public CustomerTransactionInfoValidationResponse(CustomerTransactionInfoValidationRequest request)
        {
            Request = request;
        }
    }
}
