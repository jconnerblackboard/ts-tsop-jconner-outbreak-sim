﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.BbSp
{
    /// <summary>
    /// Container class for the application credential table information.
    /// </summary>
    public class BbSpApplicationCredential
    {
        /// <summary>
        /// Unique row identifier.
        /// </summary>
        public string Guid { get; set; }

        /// <summary>
        /// Consumer Key.
        /// </summary>
        public string ConsumerKey { get; set; }

        /// <summary>
        /// Consumer Secret.
        /// </summary>
        public string ConsumerSecret { get; set; }

        /// <summary>
        /// Credential enabled?
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// Unique identifier for the application.
        /// </summary>
        public string ApplicationId { get; set; }

        /// <summary>
        /// Name of the application.
        /// </summary>
        public string ApplicationName { get; set; }

        /// <summary>
        /// Number of seconds before issued auth tokens expire.
        /// </summary>
        public int AuthTokenExpirationSeconds { get; set; }

        /// <summary>
        /// Unique identifier for the associated institution.
        /// </summary>
        public string InstitutionId { get; set; }

        /// <summary>
        /// Domain Id.
        /// </summary>
        public string DomainId { get; set; }
    }
}
