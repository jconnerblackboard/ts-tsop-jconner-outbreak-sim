// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LocalizationTokenIdentifiers.cs" company="Blackboard Inc.">
//   Copyright (C) 2013 Blackboard Inc. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace BbTS.Domain.Models.BbSp
{
    /// <summary>
    ///     ErrorMessageToken database table constants.  The constant values here refer to column TokenId.
    /// </summary>
    public static class LocalizationTokenIdentifiers
    {
        /// <summary>
        /// The publisher id
        /// </summary>
        public const int PublisherId = 1000;

        /// <summary>
        /// The publisher
        /// </summary>
        public const int Publisher = 1001;

        /// <summary>
        /// The application id
        /// </summary>
        public const int ApplicationId = 1002;

        /// <summary>
        /// The application
        /// </summary>
        public const int Application = 1003;

        /// <summary>
        /// The credential id
        /// </summary>
        public const int CredentialId = 1004;

        /// <summary>
        /// The credential
        /// </summary>
        public const int Credential = 1005;

        /// <summary>
        /// The publication type
        /// </summary>
        public const int PublicationType = 1006;

        /// <summary>
        /// The user
        /// </summary>
        public const int User = 1007;
    }
}