// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ErrorMessageIdentifiers.cs" company="Blackboard Inc.">
// Copyright (C) 2013 Blackboard Inc. All rights reserved.
// </copyright>
//
// JSC: Lifted this file from the old BbTS.Service code line from 3.13.3
//
// --------------------------------------------------------------------------------------------------------------------

using System.Net;

namespace BbTS.Domain.Models.BbSp
{
    /// <summary>
    /// ErrorMessage database table constants.  The constant values here refer to column ErrorId.
    /// </summary>
    public static class ErrorMessageIdentifiers
    {
        /// <summary>
        /// The response echo level is not valid business error
        /// </summary>
        public const int ResponseEchoLevelIsNotValidBusinessError = 1002;

        /// <summary>
        /// The institution route is not found error
        /// </summary>
        public const int InstitutionRouteIsNotFoundError = 1003;

        /// <summary>
        /// The institution route is not enabled error
        /// </summary>
        public const int InstitutionRouteIsNotEnabledError = 1004;

        /// <summary>
        /// The merchant id not found business error
        /// </summary>
        public const int MerchantIdNotFoundBusinessError = 1012;

        /// <summary>
        /// The merchant key not valid business error
        /// </summary>
        public const int MerchantKeyNotValidBusinessError = 1013;

        /// <summary>
        /// The merchant key indicator not enabled business error
        /// </summary>
        public const int MerchantKeyNotEnabledBusinessError = 1014;

        /// <summary>
        /// The transaction system service state starting business error
        /// </summary>
        public const int TransactionSystemServiceStateStartingBusinessError = 1030;

        /// <summary>
        /// The transaction system service state initializing business error
        /// </summary>
        public const int TransactionSystemServiceStateInitializingBusinessError = 1031;

        /// <summary>
        /// The transaction system service state error business error
        /// </summary>
        public const int TransactionSystemServiceStateErrorBusinessError = 1032;

        /// <summary>
        /// The institution service state starting business error
        /// </summary>
        public const int InstitutionServiceStateStartingBusinessError = 1033;

        /// <summary>
        /// The institution service state initializing business error
        /// </summary>
        public const int InstitutionServiceStateInitializingBusinessError = 1034;

        /// <summary>
        /// The institution service state error business error
        /// </summary>
        public const int InstitutionServiceStateErrorBusinessError = 1035;

        /// <summary>
        /// The institution communication error business error
        /// </summary>
        public const int InstitutionServiceCommunicationErrorBusinessError = 1036;

        /// <summary>
        /// The transaction system business error.
        /// </summary>
        public const int TransactionSystemBusinessError = 1037;

        /// <summary>
        /// The transaction system empty business error.
        /// </summary>
        public const int TransactionSystemEmptyBusinessError = 1038;

        /// <summary>
        /// The institution service transaction system business error.
        /// </summary>
        public const int InstitutionServiceTransactionSystemBusinessError = 1039;

        /// <summary>
        /// The institution service transaction system unassigned business error.
        /// </summary>
        public const int InstitutionServiceTransactionSystemUnassignedBusinessError = 1040;

        /// <summary>
        /// The transaction system communication error business error
        /// </summary>
        public const int TransactionSystemCommunicationErrorBusinessError = 1041;

        /// <summary>
        /// The Terminal Is Null Business Error
        /// </summary>
        public const int TerminalIsNullBusinessError = 2011;

        /// <summary>
        /// The Terminal Identification Is Null Business Error
        /// </summary>
        public const int TerminalIdentificationIsNullBusinessError = 2012;

        /// <summary>
        /// The Terminal Identification Method Is Null Or Empty Business Error
        /// </summary>
        public const int TerminalIdentificationMethodIsNullOrEmptyBusinessError = 2013;

        /// <summary>
        /// The card number is null or Empty business error
        /// </summary>
        public const int CardMagneticStripeTrack2IsNullOrEmptyValidBusinessError = 2050;

        /// <summary>
        /// The card number is not valid business error
        /// </summary>
        public const int CardMagneticStripeTrack2ValueIsNotValidBusinessError = 2051;

        /// <summary>
        /// The card number is null or Empty business error
        /// </summary>
        public const int CardNumberIsNullOrEmptyValidBusinessError = 2052;

        /// <summary>
        /// The card number is not valid business error
        /// </summary>
        public const int CardNumberValueIsNotValidBusinessError = 2053;

        /// <summary>
        /// The institution route headers business error.
        /// </summary>
        public const int InstitutionRouteHeaderKeyBusinessError = 2054;

        /// <summary>
        /// The institution route headers value business error.
        /// </summary>
        public const int InstitutionRouteHeadersValueBusinessError = 2055;

        /// <summary>
        /// The merchant header key business error
        /// </summary>
        public const int MerchantHeaderKeyBusinessError = 2056;

        /// <summary>
        /// The merchant headers value business error
        /// </summary>
        public const int MerchantHeadersValueBusinessError = 2057;

        /// <summary>
        /// The merchant identifier not valid unique identifier business error
        /// </summary>
        public const int MerchantIdNotValidGuidBusinessError = 2058;

        /// <summary>
        /// The merchant key not valid unique identifier business error
        /// </summary>
        public const int MerchantKeyNotValidGuidBusinessError = 2059;

        /// <summary>
        ///     The CustomerVerifyIsNotValidBusinessError /Customer
        /// </summary>
        public const int CustomerRequestIsNotValidBusinessError = 2060;

        /// <summary>
        /// The Instrument type not valid business error
        /// </summary>
        public const int InstrumentTypeNotValidBusinessError = 2061;

        /// <summary>
        /// The invalid pin number business error
        /// </summary>
        public const int InvalidPinNumberBusinessError = 2062;

        /// <summary>
        /// The identification method is not valid business error
        /// </summary>
        public const int IdentificationMethodIsNotValidBusinessError = 2063;

        /// <summary>
        /// The terminal identification method not valid business error
        /// </summary>
        public const int TerminalIdentificationMethodNotValidBusinessError = 2064;

        /// <summary>
        /// The Customer Verify Request Response Options Is Null Business Error
        /// </summary>
        public const int ResponseOptionsIsNullBusinessError = 2065;

        /// <summary>
        /// The ResponseOptions image is not valid business error
        /// </summary>
        public const int ResponseOptionsImageIsNotValidBusinessError = 2066;
     
        /// <summary>
        /// The customer request instrument is null business error
        /// </summary>
        public const int CustomerRequestInstrumentIsNullBusinessError = 2067;

        /// <summary>
        /// The instrument identification is null business error 
        /// </summary>
        public const int InstrumentIdentificationIsNullBusinessError = 2068;

        /// <summary>
        /// The instrument Type is null business error 
        /// </summary>
        public const int InstrumentTypeIsNullBusinessError = 2069;

        /// <summary>
        /// The Identification Values Is Null Business Error
        /// </summary>
        public const int IdentificationValuesIsNullBusinessError = 2070;

        /// <summary>
        /// The Identification Method IsNullOrEmpty Business Error.
        /// </summary>
        public const int IdentificationMethodIsNullOrEmptyBusinessError = 2071;

        /// <summary>
        /// The Value Card Mag Stripe Track1 Empty Business Error
        /// </summary>
        public const int ValueCardMagStripeTrack1EmptyBusinessError = 2072;

        /// <summary>
        /// The Value Card Mag Stripe Track 1 Too Long Business Error
        /// </summary>
        [global::System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", Justification = "Long does not mean the data type long in this case. ", MessageId = "long")]
        [global::System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Magstripe", Justification = "Business Requirement")]
        public const int ValueCardMagstripeTrack1TooLongBusinessError = 2073;

        /// <summary>
        /// The Value Card Mag Stripe Track3 Empty Business Error
        /// </summary>
        public const int ValueCardMagStripeTrack3EmptyBusinessError = 2074;

        /// <summary>
        /// The Value Card Mag Stripe Track 3 Too Long Business Error
        /// </summary>
        [global::System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", Justification = "Long does not mean the data type long in this case. ", MessageId = "long")]
        [global::System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Magstripe", Justification = "Business Requirement")]
        public const int ValueCardMagstripeTrack3TooLongBusinessError = 2075;

        /// <summary>
        /// The ResponseOptions image is null business error
        /// </summary>
        public const int ResponseOptionsImageIsNullBusinessError = 2077;
      
        /// <summary>
        /// The card type is not valid business error
        /// </summary>
        public const int CardTypeIsNotValidBusinessError = 2078;

/// <summary>
        ///     The domain object is unique to system business error
        /// </summary>
        public const int DomainObjectIsUniqueToSystemBusinessError = 2079;

        /// <summary>
        /// The customer gender is not valid business error
        /// </summary>
        public const int CustomerGenderIsNotValidBusinessError = 2080;

        /// <summary>
        /// The temporary card type is not valid business error
        /// </summary>
        public const int TemporaryCardTypeIsNotValidBusinessError = 2081;

        /// <summary>
        /// The customer address type is not valid business error
        /// </summary>
        public const int CustomerAddressTypeIsNotValidBusinessError = 2082;

        /// <summary>
        /// The customer email address is not valid business error
        /// </summary>
        public const int CustomerEmailAddressIsNotValidBusinessError = 2083;

        /// <summary>
        /// The customer defined field identifier invalid
        /// </summary>
        public const int CustomerDefinedFieldIdInvalidError = 2084;

        /// <summary>
        /// The customer defined field type invalid
        /// </summary>
        public const int CustomerDefinedFieldTypeInvalid = 2085;

        /// <summary>
        /// The customer group defined field value invalid
        /// </summary>
        public const int CustomerGroupDefinedFieldValueInvalid = 2086;

        /// <summary>
        /// The customer email address is unique to system business error
        /// </summary>
        public const int CustomerEmailAddressIsUniqueToSystemBusinessError = 2087;

        /// <summary>
        /// The customer defined field value invalid
        /// </summary>
        public const int CustomerDefinedFieldValueInvalid = 2088;

        /// <summary>
        /// The unknown boolean defined field value detected business error
        /// </summary>
        public const int UnknownBooleanDefinedFieldValueDetectedBusinessError = 2089;

        /// <summary>
        /// The customer image not valid business error
        /// </summary>
        public const int CustomerImageNotValidBusinessError = 2090;

        /// <summary>
        /// The one primary email allowed on create customer business error
        /// </summary>
        public const int OnePrimaryEmailAllowedOnCreateCustomerBusinessError = 2091;

        /// <summary>
        /// The customer defined field not found
        /// </summary>
        public const int CustomerDefinedFieldNotFoundBusinessError = 2092;

        /// <summary>
        /// The customer with event plan override identifier not found business error
        /// </summary>
        public const int CustomerWithEventPlanOverrideIdNotFoundBusinessError = 2093;

        /// <summary>
        /// The customer with event plan not found business error
        /// </summary>
        public const int CustomerWithEventPlanNotFoundBusinessError = 2094;

        /// <summary>
        /// The customer event override accesses missing business error
        /// </summary>
        public const int CustomerEventOverrideAccessesMissingBusinessError = 2095;

        /// <summary>
        /// The customer event override both accesses zero business error
        /// </summary>
        public const int CustomerEventOverrideBothAccessesZeroBusinessError = 2096;

        /// <summary>
        /// The customer event override regular accesses not unlimited or number business error
        /// </summary>
        public const int CustomerEventOverrideRegularAccessesNotUnlimitedOrNumberBusinessError = 2097;

        /// <summary>
        /// The customer event override guest accesses not unlimited or number business error
        /// </summary>
        public const int CustomerEventOverrideGuestAccessesNotUnlimitedOrNumberBusinessError = 2098;

        /// <summary>
        /// The customer event override duration limit type invalid business error
        /// </summary>
        public const int CustomerEventOverrideDurationLimitTypeInvalidBusinessError = 2099;

        /// <summary>
        /// The customer event override event is invalid business error
        /// </summary>
        public const int CustomerEventOverrideEventIsInvalidBusinessError = 2100;

        /// <summary>
        /// The customer event override event is already in event plan
        /// </summary>
        public const int CustomerEventOverrideEventIsAlreadyInEventPlan = 2101;

        /// <summary>
        /// The customer event override already exist business error
        /// </summary>
        public const int CustomerEventOverrideAlreadyExistBusinessError = 2102;

        /// <summary>
        /// The customer event plan identifier not valid business error
        /// </summary>
        public const int CustomerEventPlanIdNotValidBusinessError = 2103;

        /// <summary>
        /// The customer event override does not exist business error
        /// </summary>
        public const int CustomerEventOverrideDoesNotExistBusinessError = 2104;

        /// <summary>
        /// The customer event plan overrides not found business error.
        /// </summary>
        public const int CustomerEventPlanOverridesNotFoundBusinessError = 2105;

        /// <summary>
        /// The customer event plan event not found business error
        /// </summary>
        public const int CustomerEventPlanEventNotFoundBusinessError = 2106;

        /// <summary>
        /// The customer event plan not found business error.
        /// </summary>
        public const int CustomerEventPlanNotFoundBusinessError = 2107;

        /// <summary>
        /// The invalid customer number business error.
        /// </summary>
        public const int InvalidCustomerNumberBusinessError = 2108;

        /// <summary>
        /// The customer number active date business error
        /// </summary>
        public const int CustomerNumberActiveDateBusinessError = 2109;

        /// <summary>
        /// The customer email address not found business error
        /// </summary>
        public const int CustomerEmailAddressNotFoundBusinessError = 2110;

        /// <summary>
        /// The customer with board plan identifier not found business error
        /// </summary>
        public const int CustomerWithBoardPlanIdNotFoundBusinessError = 2111;

        /// <summary>
        /// The board plan with identifier not found business error
        /// </summary>
        public const int BoardPlanWithIdNotFoundBusinessError = 2112;

        /// <summary>
        /// The customer board plan already assigned business error
        /// </summary>
        public const int CustomerBoardPlanAlreadyAssignedBusinessError = 2113;

        /// <summary>
        /// The customer board plan override start date is greater than stop date business error
        /// </summary>
        public const int CustomerBoardPlanOverrideStartDateIsGreaterThanStopDateBusinessError = 2114;

        /// <summary>
        /// The customer stored value account type identifier not valid business error.
        /// </summary>
        public const int CustomerStoredValueAccountTypeIdNotValidBusinessError = 2115;

        /// <summary>
        /// The stored value account association type is not valid business error
        /// </summary>
        public const int StoredValueAccountAssociationTypeIsNotValidBusinessError = 2116;

        /// <summary>
        /// The stored value account group share type is not valid business error
        /// </summary>
        public const int StoredValueAccountGroupShareTypeIsNotValidBusinessError = 2117;

        /// <summary>
        /// The stored value account is not unique business error
        /// </summary>
        public const int StoredValueAccountIsNotUniqueBusinessError = 2118;

        /// <summary>
        /// The customer stored value account by account number not found business error.
        /// </summary>
        public const int CustomerStoredValueAccountByAccountNumberNotFoundBusinessError = 2119;

        /// <summary>
        /// The customer stored value account not valid for this operation business error
        /// </summary>
        public const int CustomerStoredValueAccountNotValidForThisOperationBusinessError = 2120;

        /// <summary>
        /// The customer stored value account balance is not valid business error
        /// </summary>
        public const int CustomerStoredValueAccountBalanceIsNotValidBusinessError = 2121;

        /// <summary>
        /// The customer email already exists business error
        /// </summary>
        public const int CustomerEmailAlreadyExistsBusinessError = 2122;

        /// <summary>
        /// The customer board plan override start date is not valid business error
        /// </summary>
        public const int CustomerBoardPlanOverrideStartDateIsNotValidBusinessError = 2123;

        /// <summary>
        /// The customer board plan override stop date is not valid business error
        /// </summary>
        public const int CustomerBoardPlanOverrideStopDateIsNotValidBusinessError = 2124;

        /// <summary>
        /// The stored value account name is not valid business error
        /// </summary>
        public const int StoredValueAccountNameIsNotValidBusinessError = 2125;

        /// <summary>
        /// The customer email type identifier not valid business error
        /// </summary>
        public const int CustomerEmailTypeIdNotValidBusinessError = 2126;

        /// <summary>
        /// The customer email default override is not valid business error
        /// </summary>
        public const int CustomerEmailDefaultOverrideIsNotValidBusinessError = 2127;

        /// <summary>
        /// The customer email already exists business error
        /// </summary>
        public const int CustomerEmailDoesExistsBusinessError = 2128;

        /// <summary>
        /// The customer not found business error
        /// A business error for when a standard card already exists
        /// </summary>
        public const int CardStandardAlreadyExistsBusinessError = 2200;

        /// <summary>
        /// A business error for when a temporary card is already assigned.
        /// </summary>
        public const int CardTemporaryIsAlreadyAssignedBusinessError = 2201;

        /// <summary>
        /// A business error for when card status type is not valid for update.
        /// </summary>
        public const int CardStatusTypeInvalidForUpdateBusinessError = 2202;

        /// <summary>
        /// The card status is already retired.
        /// </summary>
        public const int CardStatusIsAlreadyRetiredBusinessError = 2203;

        /// <summary>
        /// The card status type customer deleted not valid for this operation business error
        /// </summary>
        public const int CardStatusTypeCustomerDeletedNotValidForThisOperationBusinessError = 2204;

        /// <summary>
        /// The card status type retired needs new comment text
        /// </summary>
        public const int CardStatusTypeRetiredNeedsNewCommentTextBusinessError = 2205;

        /// <summary>
        /// The card primary set to false on request is not allowed
        /// </summary>
        public const int CardPrimarySetToFalseOnRequestIsNotAllowedBusinessError = 2206;

        /// <summary>
        /// The card status type unknown is not allowed business rule
        /// </summary>
        public const int CardStatusTypeUnknownIsNotAllowedBusinessRule = 2207;

        /// <summary>
        /// The customer door access plan identifier not valid business error
        /// </summary>
        public const int CustomerDoorAccessPlanIdNotValidBusinessError = 2208;

        /// <summary>
        /// The door access plan is not unique business error
        /// </summary>
        public const int DoorAccessPlanIsNotUniqueBusinessError = 2209;

        /// <summary>
        /// The customer door access plan not found business error
        /// </summary>
        public const int CustomerDoorAccessPlanNotFoundBusinessError = 2210;

        /// <summary>
        /// The customer not found business error
        /// </summary>
        public const int CustomerNotFoundBusinessError = 3000;

        /// <summary>
        /// The pin number not found business error. 
        /// This error id should be greater than all other id's as per workflow
        /// </summary>
        public const int PinNumberNotFoundBusinessError = 3001;
    }
}