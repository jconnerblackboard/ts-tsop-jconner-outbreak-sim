﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.BbSp
{
    public static class OperationIdentifiers
    {
        public const string OperationOk = "OK";

        public const string OperationError = "ERROR";

        public const string OperationErrorInitialCap = "Error";
    }
}
