// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LocalizationMessageIdentifiers.cs" company="Blackboard Inc.">
// Copyright (C) 2013 Blackboard Inc. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System.Net;

namespace BbTS.Domain.Models.BbSp
{
    /// <summary>
    ///     ErrorMessage database table constants.  The constant values here refer to column ErrorId.
    /// </summary>
    public static class LocalizationMessageIdentifiers
    {
        /// <summary>
        ///     The nonqualified null value detected error
        /// </summary>
        public const int NonqualifiedNullValueDetectedError = 0;

        /// <summary>
        ///     The resource not found error
        /// </summary>
        public const int ResourceNotFoundError = 1;

        /// <summary>
        ///     The invalid string value detected error
        /// </summary>
        public const int InvalidStringValueDetectedError = 2;

        /// <summary>
        ///     The invalid white space detected error
        /// </summary>
        public const int InvalidWhiteSpaceDetectedError = 3;

        /// <summary>
        ///     The integer maximum exceeded error
        /// </summary>
        public const int IntegralMaximumExceededError = 4;

        /// <summary>
        ///     The integer minimum exceeded error
        /// </summary>
        public const int IntegralMinimumExceededError = 5;

        /// <summary>
        ///     The comparable maximum exceeded error
        /// </summary>
        public const int ComparableMaximumExceededError = 6;

        /// <summary>
        ///     The comparable minimum exceeded error
        /// </summary>
        public const int ComparableMinimumExceededError = 7;

        /// <summary>
        ///     The null value detected error
        /// </summary>
        public const int NullValueDetectedError = 8;

        /// <summary>
        ///     The lookup resource not found error
        /// </summary>
        public const int LookupResourceNotFoundError = 9;

        /// <summary>
        ///     The required value was null or empty error
        /// </summary>
        public const int RequiredValueNullOrEmptyError = 10;

        /// <summary>
        ///     The required value was null or empty error
        /// </summary>
        public const int InvalidUriDetectedError = 11;

        /// <summary>
        ///     The domain object was null error
        /// </summary>
        public const int DomainObjectNullError = 12;

        /// <summary>
        ///     The domain object identifier was empty error
        /// </summary>
        public const int DomainIdEmptyError = 13;

        /// <summary>
        ///     The required object was null.
        /// </summary>
        public const int RequiredObjectNullError = 14;

        /// <summary>
        ///     The invalid unique identifier detected error
        /// </summary>
        public const int InvalidGuidDetectedError = 15;

        /// <summary>
        ///     The optional value was empty
        /// </summary>
        public const int OptionalValueEmptyError = 16;

        /// <summary>
        /// The current date greater error
        /// </summary>
        public const int CurrentDateGreaterError = 17;

        /// <summary>
        /// The current date lesser error
        /// </summary>
        public const int CurrentDateLesserError = 18;

        /// <summary>
        /// The first warning
        /// </summary>
        public const int FirstWarning = 1;

        /// <summary>
        /// The mapping error - the code is derived based on BbTS error matrix but can be used anywhere provided it is in error table.
        /// </summary>
        public const int MappingError = 2250;

        /// <summary>
        /// Get the <see cref="HttpStatusCode"/> associated with the error code.
        /// </summary>
        /// <param name="code">Code listed in this file.</param>
        /// <param name="reasonPhrase">override the reason phrase.  null to not override.</param>
        /// <returns></returns>
        public static HttpStatusCode ErrorCodeToHttpStatusCode(int code, out string reasonPhrase)
        {
            reasonPhrase = null;
            switch (code)
            {
                case 1:
                    reasonPhrase = "The requested resource could not be found.";
                    return HttpStatusCode.NotFound;
                default:
                    return HttpStatusCode.OK;
            }
        }
    }
}