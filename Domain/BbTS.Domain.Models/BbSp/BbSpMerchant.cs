﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.BbSp
{
    /// <summary>
    /// Container class for the object represented by the BbSPMerchant table.
    /// </summary>
    public class BbSpMerchant
    {
        /// <summary>
        /// Unique row identifier.
        /// </summary>
        public string Guid { get; set; }

        /// <summary>
        /// Merchant name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Merchant Key.
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Merchant secret.
        /// </summary>
        public string Secret { get; set; }

        /// <summary>
        /// Merchant Key enabled?
        /// </summary>
        public bool IsKeyEnabled { get; set; }

        /// <summary>
        /// Unique identifier for the associated institution.
        /// </summary>
        public string InstitutionId { get; set; }

        /// <summary>
        /// Domain Id.
        /// </summary>
        public string DomainId { get; set; }
    }
}
