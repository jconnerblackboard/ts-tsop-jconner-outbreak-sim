﻿using System;

namespace BbTS.Domain.Models.Security.DoorApiKeys
{
    /// <summary>
    /// Door api key set request container
    /// </summary>
    public class DoorApiKeySetRequest
    {
        /// <summary>
        /// Unique Id of the request
        /// </summary>
        public Guid RequestId { get; set; }

        /// <summary>
        /// Read only key to be set
        /// </summary>
        public string ReadOnlyKey { get; set; }

        /// <summary>
        /// Read only secret to be set
        /// </summary>
        public string ReadOnlySecret { get; set; }

        /// <summary>
        /// Read/write key to be set
        /// </summary>
        public string ReadWriteKey { get; set; }

        /// <summary>
        /// Read/write secret to be set
        /// </summary>
        public string ReadWriteSecret { get; set; }
    }
}
