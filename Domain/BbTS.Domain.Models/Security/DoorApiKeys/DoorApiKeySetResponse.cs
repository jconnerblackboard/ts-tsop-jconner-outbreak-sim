﻿using System;

namespace BbTS.Domain.Models.Security.DoorApiKeys
{
    /// <summary>
    /// Door api key set response container
    /// </summary>
    public class DoorApiKeySetResponse
    {
        /// <summary>
        /// Unique Id of the request
        /// </summary>
        public Guid RequestId { get; set; }
    }
}
