﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Dsr Schedule object
    /// </summary>
    public class DsrSchedule
    {
        /// <summary>
        /// Schedule Id
        /// </summary>
        public int ScheduleId { get; set; }

        /// <summary>
        /// Schedule name
        /// </summary>
        public string ScheduleName { get; set; }

        /// <summary>
        /// Access plan active
        /// </summary>
        public bool AccessPlanActive { get; set; }

        /// <summary>
        /// Day periods
        /// </summary>
        public List<string> DayPeriods { get; set; }
    }
}
