﻿
namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// User doors
    /// </summary>
    public class UserDoorAa : DsrSchedule, IUserDoor
    {
        /// <summary>
        /// Door id
        /// </summary>
        public int DoorId { get; set; }

        /// <summary>
        /// Door identifier
        /// </summary>
        public string DoorIdentifier { get; set; }
    }
}
