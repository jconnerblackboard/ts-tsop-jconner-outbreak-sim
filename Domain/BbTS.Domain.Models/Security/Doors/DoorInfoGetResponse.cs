﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// This object is a container for Door List Get Response
    /// </summary>
    public class DoorInfoGetResponse
    {
        /// <summary>
        /// Doors
        /// </summary>
        public List<DoorInfo> Doors { get; set; }
    }
}
