﻿using System;
using BbTS.Domain.Models.Definitions.Door;

namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Door object representing both Blackboard and Assa Abloy doors
    /// </summary>
    public class DoorAccessPoint
    {
        /// <summary>
        /// Door Identifier
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Master Controller that this Door's Door Controller is connected to
        /// </summary>
        public int? MasterControllerId { get; set; }

        /// <summary>
        /// Merchant Association (from MERCHANT table)
        /// </summary>
        public Int16? MerchantId { get; set; }

        /// <summary>
        /// Identifier unique by Merchant.  This field is visible and editable by the user.
        /// </summary>
        public string DoorIdentifier { get; set; }

        /// <summary>
        /// Text description of Door
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Building Association (from BUILDING table)
        /// </summary>
        public int? BuildingId { get; set; }

        /// <summary>
        /// Building Area Association (from AREA table)
        /// </summary>
        public int? AreaId { get; set; }

        /// <summary>
        /// The Door Group that this door belongs to (NULL mean none)
        /// </summary>
        public int? DoorGroupId { get; set; }

        /// <summary>
        /// 1..255 seconds
        /// </summary>
        public byte? DoorUnlockSeconds { get; set; }

        /// <summary>
        /// 1..255 seconds
        /// </summary>
        public byte? MinSirenSeconds { get; set; }

        /// <summary>
        /// A held alarm is when the door is open longer than HeldAlarmDelayTimeSeconds
        /// </summary>
        public DoorAlarmType? HeldAlarmType { get; set; }

        /// <summary>
        /// 1..255 seconds
        /// </summary>
        public byte? HeldAlarmDelayTimeSeconds { get; set; }

        /// <summary>
        /// A force alarm is defined as a door detected as open (door contacts are separated)
        /// </summary>
        public DoorAlarmType? ForceAlarmType { get; set; }

        /// <summary>
        /// 1..255 seconds
        /// </summary>
        public byte? ForceAlarmDelayTimeSeconds { get; set; }

        /// <summary>
        /// If USE_DEFAULT_SCHEDULE != 'T' then this will be the Door State Schedule
        /// </summary>
        public int? ScheduleId { get; set; }

        /// <summary>
        /// Instructions that the Door Access Monitor is shown for any alarm at this Door
        /// </summary>
        public string AlarmInstruction { get; set; }

        /// <summary>
        /// Door type
        /// </summary>
        public DoorType DoorType { get; set; }

        /// <summary>
        /// Door Connectivity
        /// </summary>
        public DoorConnectivity? Connectivity { get; set; }

        /// <summary>
        /// Dsr service Id
        /// </summary>
        public int? DsrServiceId { get; set; }
    }
}
