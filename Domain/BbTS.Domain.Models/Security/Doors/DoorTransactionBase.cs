﻿using System;

namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Door transaction object
    /// </summary>
    public class DoorTransactionBase
    {
        /// <summary>
        /// Transaction Id
        /// </summary>
        public Int64 TransactionId { get; set; }

        /// <summary>
        /// DateTime that this data was generated according to the Device
        /// </summary>
        public DateTime ActualDateTime { get; set; }

        /// <summary>
        /// DateTime that this data was inserted into the table according to the DB's clock
        /// </summary>
        public DateTime PostDateTime { get; set; }

        /// <summary>
        /// Door Id
        /// </summary>
        public int DoorId { get; set; }

        /// <summary>
        /// Customer Id
        /// </summary>
        public int? CustomerId { get; set; }

        /// <summary>
        /// Transaction Type
        /// </summary>
        public string TransactionType { get; set; }
    }
}
