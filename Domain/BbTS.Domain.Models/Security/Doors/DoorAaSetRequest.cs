﻿using System;

namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Door set request container 
    /// </summary>
    public class DoorAaSetRequest
    {
        /// <summary>
        /// Unique identifier of the request
        /// </summary>
        public Guid RequestId { get; set; }

        /// <summary>
        /// Set door
        /// </summary>
        public DoorAa Door { get; set; }
    }
}
