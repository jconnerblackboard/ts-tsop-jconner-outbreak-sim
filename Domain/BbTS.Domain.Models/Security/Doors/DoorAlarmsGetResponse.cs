﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Door alarm get response
    /// </summary>
    public class DoorAlarmsGetResponse
    {
        /// <summary>
        /// Door alarm logs
        /// </summary>
        public List<DoorAlarmLog> DoorAlarmLogs { get; set; }
    }
}
