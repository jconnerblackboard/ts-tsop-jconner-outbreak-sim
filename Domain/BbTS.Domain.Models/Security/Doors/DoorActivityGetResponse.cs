﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Door activities get response container
    /// </summary>
    public class DoorActivityGetResponse
    {
        /// <summary>
        /// Door transactions
        /// </summary>
        public List<DoorActivity> DoorActivities { get; set; }
    }
}
