﻿using System;

namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// This object represents a record in the Transact system for the door object
    /// </summary>
    public class DoorInfo
    {
        /// <summary>
        /// The PK of the door object
        /// </summary>
        public int DoorId { get; set; }

        /// <summary>
        /// Merchant Id
        /// </summary>
        public Int16 MerchantId { get; set; }

        /// <summary>
        /// Merchant
        /// </summary>
        public string Merchant { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Building
        /// </summary>
        public string Building { get; set; }

        /// <summary>
        /// Area
        /// </summary>
        public string Area { get; set; }

        /// <summary>
        /// Door name
        /// </summary>
        public string DoorName { get; set; }

        /// <summary>
        /// Door group name
        /// </summary>
        public string DoorGroupName { get; set; }
    }
}
