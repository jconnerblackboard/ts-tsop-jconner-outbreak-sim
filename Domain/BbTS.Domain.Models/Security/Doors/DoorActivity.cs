﻿using System;
using BbTS.Domain.Models.Definitions.Door;

namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Door activity object
    /// </summary>
    public class DoorActivity
    {
        /// <summary>
        /// Transaction Type
        /// </summary>
        public string TransactionType { get; set; }

        /// <summary>
        /// Event log Id
        /// </summary>
        public Int64? EventLogId { get; set; }

        /// <summary>
        /// MC transaction sequence number
        /// </summary>
        public Int64? McTransactionSequenceNumber { get; set; }

        /// <summary>
        /// Actual date time
        /// </summary>
        public DateTime? ActualDateTime { get; set; }

        /// <summary>
        /// Door identifier
        /// </summary>
        public string DoorIdentifier { get; set; }

        /// <summary>
        /// Merchant Id
        /// </summary>
        public Int16 MerchantId { get; set; }

        /// <summary>
        /// Building name
        /// </summary>
        public string BuildingName { get; set; }

        /// <summary>
        /// Area name
        /// </summary>
        public string AreaName { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Door Id
        /// </summary>
        public int DoorId { get; set; }

        /// <summary>
        /// Door type
        /// </summary>
        public DoorType DoorType { get; set; }
    }
}
