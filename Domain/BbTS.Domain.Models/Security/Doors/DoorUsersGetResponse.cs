﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Door users get response
    /// </summary>
    public class DoorUsersGetResponse
    {
        /// <summary>
        /// Door Id
        /// </summary>
        public int DoorId { get; set; }

        /// <summary>
        /// Door users
        /// </summary>
        public List<IDoorUser> DoorUsers { get; set; }
    }
}
