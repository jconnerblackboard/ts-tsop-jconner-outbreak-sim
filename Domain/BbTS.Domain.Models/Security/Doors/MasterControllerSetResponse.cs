﻿using System;

namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Master controller set response container 
    /// </summary>
    public class MasterControllerSetResponse
    {
        /// <summary>
        /// Unique identifier of the request
        /// </summary>
        public Guid RequestId { get; set; }

        /// <summary>
        /// Set master controller
        /// </summary>
        public MasterController MasterController { get; set; }
    }
}
