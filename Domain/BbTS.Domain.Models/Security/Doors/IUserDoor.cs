﻿
namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Door user
    /// </summary>
    public interface IUserDoor
    {
        /// <summary>
        /// Door id
        /// </summary>
        int DoorId { get; set; }

        /// <summary>
        /// Door identifier
        /// </summary>
        string DoorIdentifier { get; set; }

        /// <summary>
        /// Schedule Id
        /// </summary>
        int ScheduleId { get; set; }

        /// <summary>
        /// Schedule name
        /// </summary>
        string ScheduleName { get; set; }

        /// <summary>
        /// Access plan active
        /// </summary>
        bool AccessPlanActive { get; set; }
    }
}
