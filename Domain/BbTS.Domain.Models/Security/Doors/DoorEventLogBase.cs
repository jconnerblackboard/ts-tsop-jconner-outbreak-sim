﻿
namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Door event log object
    /// </summary>
    public class DoorEventLogBase
    {
        /// <summary>
        /// Door Association (from DOOR table)
        /// </summary>
        public int DoorId { get; set; }
    }
}
