﻿using System;

namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Door transaction object
    /// </summary>
    public class DoorAaConnection
    {
        /// <summary>
        /// Door Id
        /// </summary>
        public int DoorId { get; set; }

        /// <summary>
        /// Door guid
        /// </summary>
        public Guid DoorGuid { get; set; }

        /// <summary>
        /// Dsr service computer name
        /// </summary>
        public string DsrServiceComputer { get; set; }
    }
}
