﻿using System;
using System.Collections.Generic;
using BbTS.Domain.Models.Definitions.Door;
using BbTS.Domain.Models.General;

namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Door event log object
    /// </summary>
    public class DoorAaEventLog : DoorEventLogBase
    {
        /// <summary>
        /// Dsr service Id
        /// </summary>
        public int? DsrServiceId { get; set; }

        /// <summary>
        /// Dsr access point guid
        /// </summary>
        public Guid? DsrAccessPointGuid { get; set; }

        /// <summary>
        /// Is confirmed
        /// </summary>
        public bool IsConfirmed { get; set; }

        /// <summary>
        /// Is online
        /// </summary>
        public bool IsOnline { get; set; }

        /// <summary>
        /// Access point type Id
        /// </summary>
        public Int16? DsrAccessPointTypeId { get; set; } 

        /// <summary>
        /// Synch status
        /// </summary>
        public string SynchStatus { get; set; }

        /// <summary>
        /// Last seen
        /// </summary>
        public DateTime? LastSeen { get; set; }

        /// <summary>
        /// Last comm error
        /// </summary>
        public DateTime? LastCommnunicationError { get; set; }

        /// <summary>
        /// Door Type
        /// </summary>
        public DoorType DoorType => DoorType.AssaAbloy;

        /// <summary>
        /// State attributes
        /// </summary>
        public List<StringPair> Attributes { get; set; }
    }
}
