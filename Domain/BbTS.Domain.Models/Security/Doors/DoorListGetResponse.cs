﻿
namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// This object is a container for Door List Get Response
    /// </summary>
    public class DoorListGetResponse : DoorInfoGetResponse
    {
        /// <summary>
        /// Total door count
        /// </summary>
        public int TotalDoorCount { get; set; }
    }
}
