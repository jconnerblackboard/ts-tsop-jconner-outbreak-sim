﻿using System;

namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Door set request container 
    /// </summary>
    public class DoorSetRequest
    {
        /// <summary>
        /// Unique identifier of the request
        /// </summary>
        public Guid RequestId { get; set; }

        /// <summary>
        /// Set door
        /// </summary>
        public Door Door { get; set; }
    }
}
