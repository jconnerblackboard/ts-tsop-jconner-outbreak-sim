﻿
namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Door ackowledged alarm object
    /// </summary>
    public class DoorAcknowledgeAlarmInfo
    {
        /// <summary>
        /// Username of person acknowledging alarm
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// T means the user choose to automatically acknowledge all previous open alarms
        /// </summary>
        public bool AckowledgeAllPrevious { get; set; }

        /// <summary>
        /// Text of the Acknowledgement.
        /// </summary>
        public string Note { get; set; }
    }
}
