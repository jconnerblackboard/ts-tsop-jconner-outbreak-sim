﻿using System;

namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Door set response container 
    /// </summary>
    public class DoorSetResponse
    {
        /// <summary>
        /// Unique identifier of the request
        /// </summary>
        public Guid RequestId { get; set; }

        /// <summary>
        /// Set door
        /// </summary>
        public Door Door { get; set; }
    }
}
