﻿using System;

namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Acknowledge alarm post response container
    /// </summary>
    public class AcknowledgeAlarmPostResponse
    {
        /// <summary>
        /// Request Id
        /// </summary>
        public Guid RequestId { get; set; }

        /// <summary>
        /// Ackowledge info
        /// </summary>
        public DoorAcknowledgeAlarmInfo AcknowledgeInfo { get; set; }
    }
}
