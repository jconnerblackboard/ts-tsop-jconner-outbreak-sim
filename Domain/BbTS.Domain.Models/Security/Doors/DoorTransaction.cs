﻿using System;
using BbTS.Domain.Models.Definitions.Door;

namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Door transaction object
    /// </summary>
    public class DoorTransaction : DoorTransactionBase
    {
        /// <summary>
        /// Transaction Sequence Number from the MasterController. Increments for any transaction, include State Changes (Door Event Log). Could be used to track the order that stuff happens at a door/mastercontroller
        /// </summary>
        public Int64 McTransactionSequenceNumber { get; set; }

        /// <summary>
        /// T means Online
        /// </summary>
        public bool IsOnline { get; set; }

        /// <summary>
        /// Validation type
        /// </summary>
        public DoorTransactionValidationType ValidationType { get; set; }

        /// <summary>
        /// Tender type Id
        /// </summary>
        public int TenderTypeId { get; set; }

        /// <summary>
        /// Customer identification type
        /// </summary>
        public CustomerIdentificationType CustomerIdentificationType { get; set; }

        /// <summary>
        /// Master controller Id
        /// </summary>
        public int? MasterControllerId { get; set; }

        /// <summary>
        /// Host computer Id
        /// </summary>
        public int? HostComputerId { get; set; }

        /// <summary>
        /// Door type
        /// </summary>
        public DoorType DoorType => DoorType.Blackboard;
    }
}
