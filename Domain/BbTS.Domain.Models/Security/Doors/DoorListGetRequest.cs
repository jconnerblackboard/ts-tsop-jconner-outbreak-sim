﻿using System;
using System.Collections.Generic;

namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// This object is a container for Door List Get Request
    /// </summary>
    public class DoorListGetRequest
    {
        /// <summary>
        /// The PK of the door object
        /// </summary>
        public int? DoorId { get; set; }

        /// <summary>
        /// Merchant
        /// </summary>
        public Int16? MerchantId { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Building
        /// </summary>
        public string Building { get; set; }

        /// <summary>
        /// Area
        /// </summary>
        public string Area { get; set; }

        /// <summary>
        /// Door name
        /// </summary>
        public string DoorName { get; set; }

        /// <summary>
        /// Offset of the result
        /// </summary>
        public int Offset { get; set; }

        /// <summary>
        /// Number of items per page
        /// </summary>
        public int? PageSize { get; set; }

        /// <summary>
        /// Formats properties as optional uri params
        /// </summary>
        /// <returns></returns>
        public string FormUriParams()
        {
            var list = new List<string>();

            if (DoorId.HasValue)
                list.Add($"{nameof(DoorId)}={DoorId}");
            if (MerchantId.HasValue)
                list.Add($"{nameof(MerchantId)}={MerchantId}");
            if (!string.IsNullOrEmpty(Description))
                list.Add($"{nameof(Description)}={Uri.EscapeUriString(Description)}");
            if (!string.IsNullOrEmpty(Building))
                list.Add($"{nameof(Building)}={Uri.EscapeUriString(Building)}");
            if (!string.IsNullOrEmpty(Area))
                list.Add($"{nameof(Area)}={Uri.EscapeUriString(Area)}");
            if (!string.IsNullOrEmpty(DoorName))
                list.Add($"{nameof(DoorName)}={Uri.EscapeUriString(DoorName)}");
            if (Offset > 0)
                list.Add($"{nameof(Offset)}={Offset}");
            if (PageSize.HasValue)
                list.Add($"{nameof(PageSize)}={PageSize}");

            return string.Join("&", list);
        }
    }
}
