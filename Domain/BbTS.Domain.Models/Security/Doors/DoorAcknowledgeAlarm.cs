﻿
namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Door ackowledged alarm object
    /// </summary>
    public class DoorAcknowledgeAlarm : DoorAcknowledgeAlarmInfo
    {
        /// <summary>
        /// T means this alarm record was Acknowledged 
        /// </summary>
        public bool IsAcknowledged { get; set; }
    }
}
