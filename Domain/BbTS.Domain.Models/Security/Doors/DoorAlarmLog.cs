﻿using System;
using BbTS.Domain.Models.Definitions.Door;

namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Door alarm log object
    /// </summary>
    public class DoorAlarmLog
    {
        /// <summary>
        /// Door Access Door Alarm Log Identifier
        /// </summary>
        public Int64 LogId { get; set; }

        /// <summary>
        /// Computer Association (from HOSTNAMES table)
        /// </summary>
        public int? ComputerId { get; set; }

        /// <summary>
        /// Door Association (from DOOR table)
        /// </summary>
        public int DoorId { get; set; }

        /// <summary>
        /// Door Name
        /// </summary>
        public string DoorName { get; set; }

        /// <summary>
        /// Merchant Id
        /// </summary>
        public Int16? MerchantId { get; set; }

        /// <summary>
        /// Door alarm type
        /// </summary>
        public string DoorAlarmType { get; set; }

        /// <summary>
        /// The Master Controller Transaction Sequence Number as of the Alarm Start
        /// </summary>
        public Int64 StartMcTransEqNum { get; set; }

        /// <summary>
        /// The Master Controller's DateTime as of the Alarm Start
        /// </summary>
        public DateTime StartActualDateTime { get; set; }

        /// <summary>
        /// 'T' means the MasterController was Online when this Alarm started on the MasterController
        /// </summary>
        public bool StartIsOnline { get; set; }

        /// <summary>
        /// The Database's DateTime as of when it INSERTED the Alarm record
        /// </summary>
        public DateTime StartPostDateTime { get; set; }

        /// <summary>
        /// Indicator as to whether the alarm log entry is active (T/F)
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// The Master Controller Transaction Sequence Number as of the Alarm Stop
        /// </summary>
        public Int64? StopMcTransEqNum { get; set; }

        /// <summary>
        /// The Master Controller's DateTime as of the Alarm Stop
        /// </summary>
        public DateTime? StopActualDateTime { get; set; }

        /// <summary>
        /// 'T' means the MasterController was Online when this Alarm stopped on the MasterController
        /// </summary>
        public bool? StopIsOnline { get; set; }

        /// <summary>
        /// The Database's DateTime as of when it Updated the alarm record as stopped
        /// </summary>
        public DateTime? StopPostDateTime { get; set; }

        /// <summary>
        /// Master Controller Association (from MASTERCONTROLLER table)
        /// </summary>
        public int? MasterControllerId { get; set; }

        /// <summary>
        /// T means this alarm record was Acknowledged
        /// </summary>
        public bool IsAcked { get; set; }

        /// <summary>
        /// The associated Ack Note
        /// </summary>
        public int? DoorAlarmAckInfoId { get; set; }

        /// <summary>
        /// Globally unique identity for this record.
        /// </summary>
        public Guid DomainId { get; set; }

        /// <summary>
        /// Door Type
        /// </summary>
        public DoorType DoorType { get; set; }
    }
}
