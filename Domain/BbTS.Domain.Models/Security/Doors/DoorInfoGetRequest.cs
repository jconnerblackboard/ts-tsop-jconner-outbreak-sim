﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// This object is a container for Door List Get Request
    /// </summary>
    public class DoorInfoGetRequest
    {
        /// <summary>
        /// The PK of the door object
        /// </summary>
        public int? BuildingId { get; set; }

        /// <summary>
        /// Merchant
        /// </summary>
        public int? AreaId { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        public int? DoorGroupId { get; set; }

        /// <summary>
        /// Formats properties as optional uri params
        /// </summary>
        /// <returns></returns>
        public string FormUriParams()
        {
            var list = new List<string>();

            if (BuildingId.HasValue)
                list.Add($"{nameof(BuildingId)}={BuildingId}");
            if (AreaId.HasValue)
                list.Add($"{nameof(AreaId)}={AreaId}");
            if (DoorGroupId.HasValue)
                list.Add($"{nameof(DoorGroupId)}={DoorGroupId}");

            return string.Join("&", list);
        }
    }
}
