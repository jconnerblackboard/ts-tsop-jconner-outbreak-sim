﻿
namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Master Controller object
    /// </summary>
    public class MasterController
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name of Master Controller
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Short Name of Master Controller
        /// </summary>
        public string AbbreviatedName { get; set; }

        /// <summary>
        /// T/F Field (Tells MasterController whether to attempt to communicate with the MUX boards)
        /// </summary>
        public bool MultiplexorEnabled { get; set; }

        /// <summary>
        /// T/F Field (T means SA20xxxx mastercontroller has PINPAD device attached)
        /// </summary>
        public bool PinpadEnabled { get; set; }

        /// <summary>
        /// Door access download schedule Id
        /// </summary>
        public int DoorAccessDownloadScheduleId { get; set; }

        /// <summary>
        /// Door Harware Model
        /// 0 - SA3004
        /// 1 - SA3032
        /// 2 - SA200x
        /// 3 - SA201x
        /// 4 - SA202x
        /// </summary>
        public int HardwareModel { get; set; }

        /// <summary>
        /// Door Communication Type
        /// 0 - Network
        /// 1 - IP Converter
        /// </summary>
        public int CommunicationType { get; set; }

        /// <summary>
        /// T/F Field (T reader is active in TS)
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Used for Network CommunicationType only. Formatted as '01-23-45-67-89-AB'
        /// </summary>
        public string MacAddress { get; set; }

        /// <summary>
        /// Used for Network CommunicationType only. ConnectionID used for communicaiton.
        /// </summary>
        public int? ConnectionId { get; set; }

        /// <summary>
        /// Used for IP Converter CommunicationType only. ID of IP Converter.
        /// </summary>
        public int? IpConverterId { get; set; }

        /// <summary>
        /// Used for IP Converter CommunicationType only. RS-485 address within IP Converter to use
        /// </summary>
        public int? IpConverterAddress { get; set; }

        /// <summary>
        /// Offline table download calculation method for MasterController
        /// 1 - DA Funcs database calculation
        /// 2 - Door Access Builder calculation
        /// </summary>
        public int? DownloadType { get; set; }
    }
}
