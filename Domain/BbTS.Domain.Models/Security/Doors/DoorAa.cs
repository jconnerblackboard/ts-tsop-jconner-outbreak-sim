﻿using System;

namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Door object
    /// </summary>
    public class DoorAa : DoorBase
    {
        /// <summary>
        /// Merchant Association (from MERCHANT table)
        /// </summary>
        public Int16? MerchantId { get; set; }

        /// <summary>
        /// Door Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Serial number
        /// </summary>
        public string SerialNumber { get; set; }

        /// <summary>
        /// Dsr Service Id
        /// </summary>
        public int? DsrServiceId { get; set; }

        /// <summary>
        /// Is confirmed
        /// </summary>
        public bool IsConfirmed { get; set; }

        /// <summary>
        /// Is online
        /// </summary>
        public bool IsOnline { get; set; }

        /// <summary>
        /// Synch status
        /// </summary>
        public string SynchStatus { get; set; }

        /// <summary>
        /// Last seen
        /// </summary>
        public DateTime? LastSeen { get; set; }

        /// <summary>
        /// Last comm error
        /// </summary>
        public DateTime? LastCommnunicationError { get; set; }

        /// <summary>
        /// Dsr hardware setting group Id
        /// </summary>
        public int? DsrHardwareSettingGroupId { get; set; }

        /// <summary>
        /// Crypto algorithm type Id
        /// </summary>
        public Int16 CryptoAlgorithmTypeId { get; set; }

        /// <summary>
        /// Shared secret key hex string
        /// </summary>
        public string SharedSecretKeyHexString { get; set; }

        /// <summary>
        /// Shared secret key life span in seconds
        /// </summary>
        public int? SharedSecretKeyLifeSpanSeconds { get; set; }

        /// <summary>
        /// Dsr access point type Id
        /// </summary>
        public Int16? DsrAccessPointTypeId { get; set; }

        /// <summary>
        /// Dsr access point guid
        /// </summary>
        public Guid? DsrAccessPointGuid { get; set; }

        /// <summary>
        /// Access mode Id
        /// </summary>
        public int? AccessModeId { get; set; }

        /// <summary>
        /// Is active
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Is dirty
        /// </summary>
        public bool IsDirty { get; set; }
    }
}
