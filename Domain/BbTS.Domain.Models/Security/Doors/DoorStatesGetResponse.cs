﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Door states get response container
    /// </summary>
    public class DoorStatesGetResponse
    {
        /// <summary>
        /// Door states
        /// </summary>
        public List<DoorEventLogBase> DoorStates { get; set; }
    }
}
