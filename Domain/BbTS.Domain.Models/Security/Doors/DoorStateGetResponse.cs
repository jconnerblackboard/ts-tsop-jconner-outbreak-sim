﻿
namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Door states get response container
    /// </summary>
    public class DoorStateGetResponse
    {
        /// <summary>
        /// Door states
        /// </summary>
        public DoorEventLogBase DoorState { get; set; }
    }
}
