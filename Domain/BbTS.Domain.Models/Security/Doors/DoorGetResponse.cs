﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Door set response container 
    /// </summary>
    public class DoorGetResponse
    {
        /// <summary>
        /// Set door
        /// </summary>
        public List<DoorAccessPoint> Doors { get; set; }
    }
}
