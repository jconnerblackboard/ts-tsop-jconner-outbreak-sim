﻿using System;

namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Door user
    /// </summary>
    public interface IDoorUser
    {
        /// <summary>
        /// Customer Id
        /// </summary>
        int CustomerId { get; set; }

        /// <summary>
        /// Customer number
        /// </summary>
        decimal CustomerNumber { get; set; }

        /// <summary>
        /// Customer guid
        /// </summary>
        Guid CustomerGuid { get; set; }

        /// <summary>
        /// Customer Active
        /// </summary>
        bool CustomerActive { get; set; }

        /// <summary>
        /// Schedule Id
        /// </summary>
        int ScheduleId { get; set; }

        /// <summary>
        /// Schedule name
        /// </summary>
        string ScheduleName { get; set; }

        /// <summary>
        /// Access plan active
        /// </summary>
        bool AccessPlanActive { get; set; }
    }
}
