﻿
namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Schedule object
    /// </summary>
    public class Schedule
    {
        /// <summary>
        /// Schedule Id
        /// </summary>
        public int ScheduleId { get; set; }

        /// <summary>
        /// Schedule name
        /// </summary>
        public string ScheduleName { get; set; }

        /// <summary>
        /// Holiday schedule Id
        /// </summary>
        public int? HolidayScheduleId { get; set; }

        /// <summary>
        /// Holiday schedule name
        /// </summary>
        public string HolidayScheduleName { get; set; }

        /// <summary>
        /// Schedule id for day of week 1
        /// </summary>
        public int Dow1ScheduleId { get; set; }

        /// <summary>
        /// Schedule name for day of week 1
        /// </summary>
        public string Dow1ScheduleName { get; set; }

        /// <summary>
        /// Schedule id for day of week 2
        /// </summary>
        public int Dow2ScheduleId { get; set; }

        /// <summary>
        /// Schedule name for day of week 2
        /// </summary>
        public string Dow2ScheduleName { get; set; }

        /// <summary>
        /// Schedule id for day of week 3
        /// </summary>
        public int Dow3ScheduleId { get; set; }

        /// <summary>
        /// Schedule name for day of week 3
        /// </summary>
        public string Dow3ScheduleName { get; set; }

        /// <summary>
        /// Schedule id for day of week 4
        /// </summary>
        public int Dow4ScheduleId { get; set; }

        /// <summary>
        /// Schedule name for day of week 4
        /// </summary>
        public string Dow4ScheduleName { get; set; }

        /// <summary>
        /// Schedule id for day of week 5
        /// </summary>
        public int Dow5ScheduleId { get; set; }

        /// <summary>
        /// Schedule name for day of week 5
        /// </summary>
        public string Dow5ScheduleName { get; set; }

        /// <summary>
        /// Schedule id for day of week 6
        /// </summary>
        public int Dow6ScheduleId { get; set; }

        /// <summary>
        /// Schedule name for day of week 6
        /// </summary>
        public string Dow6ScheduleName { get; set; }

        /// <summary>
        /// Schedule id for day of week 7
        /// </summary>
        public int Dow7ScheduleId { get; set; }

        /// <summary>
        /// Schedule name for day of week 7
        /// </summary>
        public string Dow7ScheduleName { get; set; }

        /// <summary>
        /// Access plan active
        /// </summary>
        public bool AccessPlanActive { get; set; }
    }
}
