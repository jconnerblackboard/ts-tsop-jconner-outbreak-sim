﻿
namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Door object
    /// </summary>
    public class DoorBase
    {
        /// <summary>
        /// Door Identifier
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Text description of Door
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Building Association (from BUILDING table)
        /// </summary>
        public int? BuildingId { get; set; }

        /// <summary>
        /// Building Area Association (from AREA table)
        /// </summary>
        public int? AreaId { get; set; }

        /// <summary>
        /// The Door Group that this door belongs to (NULL mean none)
        /// </summary>
        public int? DoorGroupId { get; set; }
    }
}
