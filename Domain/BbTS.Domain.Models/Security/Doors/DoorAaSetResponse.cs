﻿using System;

namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Door set response container 
    /// </summary>
    public class DoorAaSetResponse
    {
        /// <summary>
        /// Unique identifier of the request
        /// </summary>
        public Guid RequestId { get; set; }

        /// <summary>
        /// Set door
        /// </summary>
        public DoorAa Door { get; set; }
    }
}
