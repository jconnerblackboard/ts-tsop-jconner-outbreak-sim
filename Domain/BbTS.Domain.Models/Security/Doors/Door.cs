﻿using System;
using BbTS.Domain.Models.Definitions.Door;

namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Door object
    /// </summary>
    public class Door : DoorBase
    {
        /// <summary>
        /// Master Controller that this Door's Door Controller is connected to
        /// </summary>
        public int? MasterControllerId { get; set; }

        /// <summary>
        /// Merchant Association (from MERCHANT table)
        /// </summary>
        public Int16 MerchantId { get; set; }

        /// <summary>
        /// Identifier unique by Merchant.  This field is visible and editable by the user.
        /// </summary>
        public string DoorIdentifier { get; set; }

        /// <summary>
        /// 1..255 seconds
        /// </summary>
        public byte DoorUnlockSeconds { get; set; }

        /// <summary>
        /// 1..255 seconds
        /// </summary>
        public byte MinSirenSeconds { get; set; }

        /// <summary>
        /// A held alarm is when the door is open longer than HeldAlarmDelayTimeSeconds
        /// </summary>
        public DoorAlarmType HeldAlarmType { get; set; }

        /// <summary>
        /// 1..255 seconds
        /// </summary>
        public byte HeldAlarmDelayTimeSeconds { get; set; }

        /// <summary>
        /// A force alarm is defined as a door detected as open (door contacts are separated)
        /// </summary>
        public DoorAlarmType ForceAlarmType { get; set; }

        /// <summary>
        /// 1..255 seconds
        /// </summary>
        public byte ForceAlarmDelayTimeSeconds { get; set; }

        /// <summary>
        /// If USE_DEFAULT_SCHEDULE != 'T' then this will be the Door State Schedule
        /// </summary>
        public int? ScheduleId { get; set; }

        /// <summary>
        /// Instructions that the Door Access Monitor is shown for any alarm at this Door
        /// </summary>
        public string AlarmInstruction { get; set; }

        /// <summary>
        /// Address 0..7.  This tells the Master Controller which door it is talking to
        /// </summary>
        public int? Address { get; set; }

        /// <summary>
        /// 'T' means use the Settings from DA_DOOR_DEFAULT
        /// </summary>
        public bool UseDoorDefault { get; set; }

        /// <summary>
        /// 'T' means use the SCHEDULE_ID from DA_DOOR_DEFAULT
        /// </summary>
        public bool UseDoorDefaultSchedule { get; set; }

        /// <summary>
        /// Offline operation type
        /// </summary>
        public DoorOfflineOperation OfflineOperationType { get; set; }
    }
}
