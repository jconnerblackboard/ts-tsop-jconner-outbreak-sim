﻿using System;
using BbTS.Domain.Models.Definitions.Door;

namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Door transaction object
    /// </summary>
    public class DoorAaTransaction : DoorTransactionBase
    {
        /// <summary>
        /// Result
        /// </summary>
        public string Result { get; set; }

        /// <summary>
        /// Merchant Id
        /// </summary>
        public Int16? MerchantId { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Card number
        /// </summary>
        public string CardNumber { get; set; }

        /// <summary>
        /// Door type
        /// </summary>
        public DoorType DoorType => DoorType.AssaAbloy;
    }
}
