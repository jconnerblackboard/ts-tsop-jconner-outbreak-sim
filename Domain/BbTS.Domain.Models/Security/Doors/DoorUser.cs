﻿using System;

namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Door user
    /// </summary>
    public class DoorUser : Schedule, IDoorUser
    {
        /// <summary>
        /// Customer Id
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Customer number
        /// </summary>
        public decimal CustomerNumber { get; set; }

        /// <summary>
        /// Customer guid
        /// </summary>
        public Guid CustomerGuid { get; set; }

        /// <summary>
        /// Customer Active
        /// </summary>
        public bool CustomerActive { get; set; }
    }
}
