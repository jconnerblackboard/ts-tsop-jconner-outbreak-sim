﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// User doors get response
    /// </summary>
    public class UserDoorsGetResponse
    {
        /// <summary>
        /// Customer Id
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// User doors
        /// </summary>
        public List<IUserDoor> UserDoors { get; set; }
    }
}
