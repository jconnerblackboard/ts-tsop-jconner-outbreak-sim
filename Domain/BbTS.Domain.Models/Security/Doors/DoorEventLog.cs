﻿using System;
using BbTS.Domain.Models.Definitions.Door;

namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Door event log object
    /// </summary>
    public class DoorEventLog : DoorEventLogBase
    {
        /// <summary>
        /// Door Event Log Identifier
        /// </summary>
        public Int64 LogId { get; set; }

        /// <summary>
        /// Computer Association (from HOSTNAMES table)
        /// </summary>
        public int? ComputerId { get; set; }

        /// <summary>
        /// DateTime that this data was generated according to the Device
        /// </summary>
        public DateTime ActualDateTime { get; set; }

        /// <summary>
        /// Transaction Sequence Number from the MasterController. Increments for any transaction, include State Changes (Door Event Log). Could be used to track the order that stuff happens at a door/mastercontroller
        /// </summary>
        public Int64 McTransactionSequenceNumber { get; set; }

        /// <summary>
        /// True if StatusChange occured Online
        /// </summary>
        public bool IsOnline { get; set; }

        /// <summary>
        /// DateTime that this data was inserted into the table according to the DB's clock
        /// </summary>
        public DateTime PostDateTime { get; set; }

        /// <summary>
        /// Door control mode
        /// </summary>
        public DoorControlMode DoorControlMode { get; set; }

        /// <summary>
        /// Is exit cycle
        /// </summary>
        public bool IsExitCycle { get; set; }

        /// <summary>
        /// Is unlocked
        /// </summary>
        public bool IsUnlocked { get; set; }

        /// <summary>
        /// Is open
        /// </summary>
        public bool IsOpen { get; set; }

        /// <summary>
        /// Is held
        /// </summary>
        public bool IsHeld { get; set; }

        /// <summary>
        /// Is forced
        /// </summary>
        public bool IsForced { get; set; }

        /// <summary>
        /// Is tampered
        /// </summary>
        public bool IsTampered { get; set; }

        /// <summary>
        /// Requires pin
        /// </summary>
        public bool RequiresPin { get; set; }

        /// <summary>
        /// Door Controller Communication Lost (T means True)
        /// </summary>
        public bool DoorControlCommLost { get; set; }

        /// <summary>
        /// TB1 (Proximity Reader) Auxillary Cardswipe Tamper - T means True
        /// </summary>
        public bool Tb1AuxCsTamper { get; set; }

        /// <summary>
        /// TB2 (Proximity Reader) Auxillary Cardswipe Tamper - T means True
        /// </summary>
        public bool Tb2AuxCsTamper { get; set; }

        /// <summary>
        /// Serial Port 1 Cardswipe Communication Lost - T means True
        /// </summary>
        public bool Serial1CsCommLost { get; set; }

        /// <summary>
        /// Serial Port 2 Cardswipe Communication Lost - T means True
        /// </summary>
        public bool Serial2CsCommLost { get; set; }

        /// <summary>
        /// Door Controller AC Power Failed - T means True
        /// </summary>
        public bool DoorControlAcPowerFail { get; set; }

        /// <summary>
        /// Door Controller Battery Low - T means True
        /// </summary>
        public bool DoorControlBatteryLow { get; set; }

        /// <summary>
        /// Door Override status - relates to door control state
        /// </summary>
        public DoorOverrideStatus OverrideStatus { get; set; }

        /// <summary>
        /// Master controller id
        /// </summary>
        public int? MasterControllerId { get; set; }

        /// <summary>
        /// Door Type
        /// </summary>
        public DoorType DoorType => DoorType.Blackboard;
    }
}
