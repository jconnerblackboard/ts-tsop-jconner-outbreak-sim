﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Security.Doors
{
    /// <summary>
    /// Door transactions get response container
    /// </summary>
    public class DoorTransactionsGetResponse
    {
        /// <summary>
        /// Door transactions
        /// </summary>
        public List<DoorTransactionBase> DoorTransactions { get; set; }
    }
}
