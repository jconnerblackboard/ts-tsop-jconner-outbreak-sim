﻿using System;

namespace BbTS.Domain.Models.Institution
{
    /// <summary>
    ///     Merchant Model
    /// </summary>
    public class SpMerchant
    {
        /// <summary>
        ///     Gets or sets the Merchant domain id.
        /// </summary>
        /// <value>
        ///     The Merchant domain id.
        /// </value>
        public Guid MerchantId { get; set; }

        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        /// <value>
        ///     The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Merchant" /> is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        public bool MerchantKeyEnabled { get; set; }

        /// <summary>
        ///     Gets or sets the Merchant Key.
        /// </summary>
        /// <value>
        ///     The Merchant Key.
        /// </value>
        public Guid MerchantKey { get; set; }

        /// <summary>
        ///     Gets or sets the Merchant Secret.
        /// </summary>
        /// <value>
        ///     The Merchant Secret.
        /// </value>
        public string MerchantSecret { get; set; }
    }
}
