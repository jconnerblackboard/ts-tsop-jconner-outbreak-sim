﻿using System;
using System.Collections.Generic;

namespace BbTS.Domain.Models.Institution
{
    /// <summary>
    ///     Institution Model
    /// </summary>
    public class SpInstitution
    {
        /// <summary>
        ///     Gets or sets the Institution id.
        /// </summary>
        /// <value>
        ///     The Institution id.
        /// </value>
        public Guid InstitutionId { get; set; }

        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        /// <value>
        ///     The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        ///     Gets or sets the Institution Route.
        /// </summary>
        /// <value>
        ///     The Institution Route.
        /// </value>
        public List<SpInstitutionRoute> InstitutionRoutes { get; set; }
    }
}
