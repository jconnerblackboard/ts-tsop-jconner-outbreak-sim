﻿using System;

namespace BbTS.Domain.Models.Institution
{
    /// <summary>
    ///     Merchant Model
    /// </summary>
    public class SpMerchantEntity : SpMerchant
    {
        /// <summary>
        ///     Gets or sets the Institution Id.
        /// </summary>
        /// <value>
        ///     The institution id.
        /// </value>
        public Guid InstitutionId { get; set; }

        /// <summary>
        ///     Gets or sets the domain id.
        /// </summary>
        /// <remarks>
        ///     This is the unique domain identifier
        /// </remarks>
        /// <value>
        ///     The domain id.
        /// </value>
        public Guid DomainId { get; set; }
    }
}
