﻿
namespace BbTS.Domain.Models.Institution
{
    /// <summary>
    /// Service Point API response
    /// </summary>
    public class InstitutionGetResponse
    {
        /// <summary>
        /// Gets or sets the Institution.
        /// </summary>
        /// <value>
        /// The user.
        /// </value>
        public SpInstitution Institution { get; set; }
    }
}
