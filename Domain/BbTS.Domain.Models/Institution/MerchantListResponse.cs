﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Institution
{
    /// <summary>
    /// The Merchant list response.
    /// </summary>
    public class MerchantListResponse
    {
        /// <summary>
        /// Gets or sets the List of Merchants.
        /// </summary>
        public List<SpMerchant> Merchants { get; set; }
    }
}
