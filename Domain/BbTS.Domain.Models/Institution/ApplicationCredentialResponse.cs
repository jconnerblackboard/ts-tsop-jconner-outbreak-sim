﻿
namespace BbTS.Domain.Models.Institution
{
    /// <summary>
    ///     BbIS Application Credential Response
    /// </summary>
    public class ApplicationCredentialResponse
    {
        /// <summary>
        /// Gets or sets the application credential.
        /// </summary>
        /// <value>
        /// The application credential.
        /// </value>
        public ApplicationCredential ApplicationCredential { get; set; }
    }
}
