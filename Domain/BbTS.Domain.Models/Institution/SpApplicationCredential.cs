﻿using System;

namespace BbTS.Domain.Models.Institution
{
    /// <summary>
    /// Application Credential Entity
    /// </summary>
    public class SpApplicationCredential
    {
        /// <summary>
        /// Gets or sets the application credential identifier.
        /// </summary>
        /// <value>
        /// The application credential identifier.
        /// </value>
        public Guid ApplicationCredentialId { get; set; }

        /// <summary>
        /// Gets or sets the consumer key.
        /// </summary>
        /// <value>
        /// The consumer key.
        /// </value>
        public Guid ConsumerKey { get; set; }

        /// <summary>
        /// Gets or sets the consumer secret.
        /// </summary>
        /// <value>
        /// The consumer secret.
        /// </value>
        public string ConsumerSecret { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [is enabled].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// Gets or sets the application identifier.
        /// </summary>
        /// <value>
        /// The application identifier.
        /// </value>
        public Guid ApplicationId { get; set; }

        /// <summary>
        /// Gets or sets the name of the application.
        /// </summary>
        /// <value>
        /// The name of the application.
        /// </value>
        public string ApplicationName { get; set; }

        /// <summary>
        /// Gets or sets the authentication token expiration seconds.
        /// </summary>
        /// <value>
        /// The authentication token expiration seconds.
        /// </value>
        public int AuthTokenExpirationSeconds { get; set; }

        /// <summary>
        ///     Gets or sets the institution identifier.
        /// </summary>
        /// <value>
        ///     The institution identifier.
        /// </value>
        public Guid InstitutionId { get; set; }

        /// <summary>
        ///     Gets or sets the domain id.
        /// </summary>
        /// <remarks>
        ///     This is the unique domain identifier
        /// </remarks>
        /// <value>
        ///     The domain id.
        /// </value>
        public Guid DomainId { get; set; }
    }
}
