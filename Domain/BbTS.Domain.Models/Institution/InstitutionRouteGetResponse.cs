﻿
namespace BbTS.Domain.Models.Institution
{
    /// <summary>
    /// BbIS API response
    /// </summary>
    public class InstitutionRouteGetResponse
    {
        /// <summary>
        /// Gets or sets the Institution.
        /// </summary>
        /// <value>
        /// The user.
        /// </value>
        public InstitutionRoute InstitutionRoute { get; set; }
    }
}
