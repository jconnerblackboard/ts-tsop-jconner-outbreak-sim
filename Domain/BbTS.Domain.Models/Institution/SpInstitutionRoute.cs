﻿using System;

namespace BbTS.Domain.Models.Institution
{
    /// <summary>
    ///    The InstitutionRoute.
    /// </summary>
    public class SpInstitutionRoute
    {
        /// <summary>
        /// Gets or sets the Institution Route id.
        /// </summary>
        /// <value>
        /// The Institution Route id.
        /// </value>
        public Guid InstitutionRouteId { get; set; }

        /// <summary>
        ///     Gets or sets the InstitutionRouteSchemeTypeApiKey.
        /// </summary>
        /// <value>
        ///     The InstitutionRouteSchemeTypeApiKey.
        /// </value>
        public string InstitutionRouteSchemeTypeApiKey { get; set; }

        /// <summary>
        ///     Gets or sets the Value.
        /// </summary>
        /// <value>
        ///     The Value.
        /// </value>
        public string Value { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [enabled].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool Enabled { get; set; }
    }
}
