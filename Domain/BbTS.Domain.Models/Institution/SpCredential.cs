﻿using System;

namespace BbTS.Domain.Models.Institution
{
    /// <summary>
    ///     Credential Model
    /// </summary>
    public class SpCredential
    {
        /// <summary>
        ///     Gets or sets the application credential identifier.
        /// </summary>
        /// <value>
        ///     The application credential identifier.
        /// </value>
        public Guid ApplicationCredentialId { get; set; }

        /// <summary>
        ///     Gets or sets the consumer key.
        /// </summary>
        /// <value>
        ///     The consumer key.
        /// </value>
        public string ConsumerKey { get; set; }

        /// <summary>
        ///     Gets or sets the consumer secret.
        /// </summary>
        /// <value>
        ///     The consumer secret.
        /// </value>
        public string ConsumerSecret { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether [is enabled].
        /// </summary>
        /// <value>
        ///     <c>
        ///         true
        ///     </c>
        ///     if [is enabled]; otherwise,
        ///     <c>
        ///         false
        ///     </c>
        ///     .
        /// </value>
        public bool IsEnabled { get; set; }
    }
}
