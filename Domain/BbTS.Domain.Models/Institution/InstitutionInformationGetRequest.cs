﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Institution
{
    /// <summary>
    /// Container class for a request for institution information from the eaccounts portal.
    /// </summary>
    public class InstitutionInformationGetRequest
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public Guid RequestId { get; set; } = Guid.NewGuid();

        /// <summary>
        /// The Institution Id to search for.  Null for all institutions.
        /// </summary>
        public Guid? InstitutionId { get; set; }

        /// <summary>
        /// The consumer key to use when authenticating against the eaccounts server
        /// </summary>
        public string ConsumerKey { get; set; }

        /// <summary>
        /// The consumer secret to use when authenticating against the eaccounts server
        /// </summary>
        public string ConsumerSecret { get; set; }

        /// <summary>
        /// The route for the endpoint.
        /// </summary>
        public const string Route = "rest/transactsp/20111101/institutions";

        /// <summary>
        /// The hostname of the server.
        /// </summary>
        public string Hostname { get; set; }
    }
}
