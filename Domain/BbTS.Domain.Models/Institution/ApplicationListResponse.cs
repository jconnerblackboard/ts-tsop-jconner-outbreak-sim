﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Institution
{
    /// <summary>
    ///     Service Point Application Response
    /// </summary>
    public class ApplicationListResponse
    {
        /// <summary>
        /// Gets or sets the applications.
        /// </summary>
        /// <value>
        /// The applications.
        /// </value>
        public List<SpApplication> Applications { get; set; }
    }
}
