﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Institution
{
    /// <summary>
    /// Container class for a request for institution information from the eaccounts portal.
    /// </summary>
    public class InstitutionInformationGetResponse
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public Guid RequestId { get; set; } = Guid.NewGuid();

        /// <summary>
        /// The list of institutions received from the eaccounts server.
        /// </summary>
        public EaccountsInstitutionList InstitutionList { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("InstitutionList")]
    public class EaccountsInstitutionList
    {
        /// <summary>
        /// List of institutions.
        /// </summary>
        [XmlArray("Institutions"), XmlArrayItem("Institution")]
        public List<EaccountsInstitutionObject> Institutions = new List<EaccountsInstitutionObject>();
    }

    /// <summary>
    /// Container class for a single institution returned from eaccounts.
    /// </summary>
    [XmlType("Institution")]
    public class EaccountsInstitutionObject
    {
        /// <summary>
        /// Description of the institution.
        /// </summary>
        [DataMember(Name = "Description")]
        public string Description { get; set; }

        /// <summary>
        /// Unique identifier for the institution.
        /// </summary>
        [DataMember(Name = "InstitutionId")]
        public string InstitutionId { get; set; }

        /// <summary>
        /// Name of the institution.
        /// </summary>
        [DataMember(Name = "Name")]
        public string Name { get; set; }

        /// <summary>
        /// Short name for the institution.
        /// </summary>
        [DataMember(Name = "ShortName")]
        public string ShortName { get; set; }
    }
}
