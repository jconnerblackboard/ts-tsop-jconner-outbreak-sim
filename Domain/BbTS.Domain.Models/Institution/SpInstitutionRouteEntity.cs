﻿using System;

namespace BbTS.Domain.Models.Institution
{
    /// <summary>
    ///    The InstitutionRoute.
    /// </summary>
    public class SpInstitutionRouteEntity : SpInstitutionRoute
    {
        /// <summary>
        ///     Gets or sets the Institution Id.
        /// </summary>
        /// <value>
        ///     The institution id.
        /// </value>
        public Guid InstitutionId { get; set; }

        /// <summary>
        ///     Gets or sets the name of the institution.
        /// </summary>
        /// <value>
        ///     The name of the institution.
        /// </value>
        public string InstitutionName { get; set; }

        /// <summary>
        ///     Gets or sets the domain id.
        /// </summary>
        /// <remarks>
        ///     This is the unique domain identifier
        /// </remarks>
        /// <value>
        ///     The domain id.
        /// </value>
        public Guid DomainId { get; set; }
    }
}
