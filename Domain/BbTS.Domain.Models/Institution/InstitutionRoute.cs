﻿using System;

namespace BbTS.Domain.Models.Institution
{
    /// <summary>
    ///    The Institution Route.
    /// </summary>
    public class InstitutionRoute
    {
        /// <summary>
        /// Gets or sets the institution route scheme type API key.
        /// </summary>
        /// <value>
        /// The institution route scheme type API key.
        /// </value>
        public string InstitutionRouteSchemeTypeApiKey { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public string Value { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [enabled].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// Gets or sets the Institution.
        /// </summary>
        /// <value>
        /// The Institution.
        /// </value>
        public Guid InstitutionId { get; set; }

        /// <summary>
        /// Gets or sets the name of the institution.
        /// </summary>
        /// <value>
        /// The name of the institution.
        /// </value>
        public string InstitutionName { get; set; }
    }
}
