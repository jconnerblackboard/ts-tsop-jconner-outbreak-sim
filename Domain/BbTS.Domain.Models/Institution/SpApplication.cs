﻿using System;
using System.Collections.Generic;

namespace BbTS.Domain.Models.Institution
{
    /// <summary>
    /// Application Credentials institution model
    /// </summary>
    public class SpApplication
    {
        /// <summary>
        ///     Gets or sets the application identifier.
        /// </summary>
        /// <value>
        ///     The application identifier.
        /// </value>
        public Guid ApplicationId { get; set; }

        /// <summary>
        ///     Gets or sets the name of the application.
        /// </summary>
        /// <value>
        ///     The name of the application.
        /// </value>
        public string ApplicationName { get; set; }

        /// <summary>
        ///     Gets or sets the authentication token expiration seconds.
        /// </summary>
        /// <value>
        ///     The authentication token expiration seconds.
        /// </value>
        public int AuthTokenExpirationSeconds { get; set; }

        /// <summary>
        ///     Gets or sets the credentials.
        /// </summary>
        /// <value>
        ///     The credentials.
        /// </value>
        public List<SpCredential> Credentials { get; set; }
    }
}
