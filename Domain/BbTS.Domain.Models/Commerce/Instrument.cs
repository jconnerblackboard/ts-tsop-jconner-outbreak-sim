﻿
namespace BbTS.Domain.Models.Commerce
{
    /// <summary>
    ///     The Instrument.
    /// </summary>
    public class Instrument
    {
        /// <summary>
        ///     Gets or sets the Identification.
        /// </summary>
        /// <value>
        ///     The Identification.
        /// </value>
        public Identification Identification { get; set; }

        /// <summary>
        ///     Gets or sets the Identification.
        /// </summary>
        /// <value>
        ///     The Identification.
        /// </value>
        public string Type { get; set; }
    }
}
