﻿using System;

namespace BbTS.Domain.Models.Commerce
{
    /// <summary>
    ///     Customer definition.
    /// </summary>
    public class Customer
    {
        /// <summary>
        ///     Gets or sets the ActiveEndDate.
        /// </summary>
        /// <value>
        ///     The ActiveEndDate.
        /// </value>
        public DateTime? ActiveEndDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Customer" /> is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        public bool ActiveIndicator { get; set; }

        /// <summary>
        ///     Gets or sets the Active StartDate.
        /// </summary>
        /// <value>
        ///     The Active StartDate.
        /// </value>
        public DateTime? ActiveStartDate { get; set; }

        /// <summary>
        ///     Gets or sets the Card.
        /// </summary>
        /// <value>
        ///     The Card.
        /// </value>
        public Card Card { get; set; }

        /// <summary>
        ///     Gets or sets the Customer Number.
        /// </summary>
        /// <value>
        ///     The Customer Number.
        /// </value>
        public string CustomerNumber { get; set; }

        /// <summary>
        ///     Gets or sets the Open DateTime.
        /// </summary>
        /// <value>
        ///     The Open DateTime.
        /// </value>
        public DateTime OpenDateTime { get; set; }

        /// <summary>
        ///     Gets or sets the Person .
        /// </summary>
        /// <value>
        ///     The Person.
        /// </value>
        public Person Person { get; set; }

        /// <summary>
        ///     Gets or sets the Image .
        /// </summary>
        /// <value>
        ///     The Image.
        /// </value>
        public string Image { get; set; }

        /// <summary>
        /// Gets or sets the primary email address.
        /// </summary>
        /// <value>
        /// The primary email address.
        /// </value>
        public string PrimaryEmailAddress { get; set; }
    }
}
