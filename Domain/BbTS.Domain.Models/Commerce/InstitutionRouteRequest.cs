﻿
namespace BbTS.Domain.Models.Commerce
{
    /// <summary>
    ///    The InstitutionRoute request.
    /// </summary>
    public class InstitutionRouteRequest
    {
        /// <summary>
        ///     Gets or sets the SchemeTypeApiKey.
        /// </summary>
        /// <value>
        ///     The SchemeTypeApiKey.
        /// </value>
        public string SchemeTypeApiKey { get; set; }

        /// <summary>
        ///     Gets or sets the Value.
        /// </summary>
        /// <value>
        ///     The Value.
        /// </value>
        public string Value { get; set; }
    }
}