﻿
namespace BbTS.Domain.Models.Commerce
{
    /// <summary>
    ///     Service model that handles verification of an customer.
    /// </summary>
    public class CustomerVerifyResponse
    {
        /// <summary>
        ///     Gets or sets the CustomerVerify.
        /// </summary>
        /// <value>
        ///     The CustomerVerify.
        /// </value>
        public CustomerVerify CustomerVerify { get; set; }
    }
}
