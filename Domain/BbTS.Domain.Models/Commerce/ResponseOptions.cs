﻿
namespace BbTS.Domain.Models.Commerce
{
    /// <summary>
    ///     Service model that handles Response options.
    /// </summary>
    public class ResponseOptions
    {
        /// <summary>
        ///     Gets or sets the Image.
        /// </summary>
        /// <value>
        ///     The Image.
        /// </value>
        public string Image { get; set; }
    }
}
