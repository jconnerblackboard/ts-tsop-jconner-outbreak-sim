﻿using System;

namespace BbTS.Domain.Models.Commerce
{
    /// <summary>
    ///    The MerchantAuthorization request.
    /// </summary>
    public class MerchantAuthorizationRequest
    {
        /// <summary>
        ///     Gets or sets the MerchantId.
        /// </summary>
        /// <value>
        ///     The MerchantId.
        /// </value>
        public Guid MerchantId { get; set; }

        /// <summary>
        ///     Gets or sets the MerchantKey.
        /// </summary>
        /// <value>
        ///     The MerchantKey.
        /// </value>
        public Guid MerchantKey { get; set; }

        /// <summary>
        ///     Gets or sets the MerchantSecret.
        /// </summary>
        /// <value>
        ///     The MerchantSecret.
        /// </value>
        public string MerchantSecret { get; set; }
    }
}