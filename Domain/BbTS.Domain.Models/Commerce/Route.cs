﻿
namespace BbTS.Domain.Models.Commerce
{
    /// <summary>
    /// The Route.
    /// </summary>
    public class Route
    {
        /// <summary>
        /// Gets or sets the institution.
        /// </summary>
        /// <value>
        /// The institution.
        /// </value>
        public Institution Institution { get; set; }

        /// <summary>
        ///     Gets or sets the Merchant.
        /// </summary>
        /// <value>
        ///     The Merchant.
        /// </value>
        public Merchant Merchant { get; set; }
    }
}
