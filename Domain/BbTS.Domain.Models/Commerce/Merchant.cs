﻿using System;

namespace BbTS.Domain.Models.Commerce
{
    /// <summary>
    ///    The Merchant Authorization.
    /// </summary>
    public class Merchant
    {
        /// <summary>
        ///     Gets or sets the Id.
        /// </summary>
        /// <value>
        ///     The Id.
        /// </value>
        public Guid Id { get; set; }

        /// <summary>
        ///     Gets or sets the Name.
        /// </summary>
        /// <value>
        ///     The Name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        ///     Gets or sets the RequestValue.
        /// </summary>
        /// <value>
        ///     The RequestValue.
        /// </value>
        public string RequestValue { get; set; }
    }
}
