﻿
namespace BbTS.Domain.Models.Commerce
{
    /// <summary>
    /// The Card Information.
    /// </summary>
    public class CardInfo
    {
        /// <summary>
        ///      Gets the Id Number.
        /// </summary>
        public string IdNumber { get; set; }

        /// <summary>
        /// Gets the Issue Number.
        /// </summary>
        public string IssueNumber { get; set; }

        /// <summary>
        /// Gets the Site Code.
        /// </summary>
        public string SiteCode { get; set; }
    }
}
