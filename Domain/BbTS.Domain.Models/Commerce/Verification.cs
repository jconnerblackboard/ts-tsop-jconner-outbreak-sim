﻿
namespace BbTS.Domain.Models.Commerce
{
    /// <summary>
    ///     Service model that handles verification.
    /// </summary>
    public class Verification
    {
        /// <summary>
        ///     Gets or sets the Pin.
        /// </summary>
        /// <value>
        ///     The Pin.
        /// </value>
        public string Pin { get; set; }
    }
}
