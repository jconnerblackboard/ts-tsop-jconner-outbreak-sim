﻿
namespace BbTS.Domain.Models.Commerce
{
    /// <summary>
    ///     The Error.
    /// </summary>
    public class Error
    {
        /// <summary>
        ///     Gets or sets the Code.
        /// </summary>
        /// <value>
        ///     The Code.
        /// </value>
        public int Code { get; set; }

        /// <summary>
        ///     Gets or sets the Text.
        /// </summary>
        /// <value>
        ///     The Text.
        /// </value>
        public string Text { get; set; }
    }
}