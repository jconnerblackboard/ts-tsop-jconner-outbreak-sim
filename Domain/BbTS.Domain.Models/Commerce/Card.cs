﻿using System;

namespace BbTS.Domain.Models.Commerce
{
    /// <summary>
    /// The card.
    /// </summary>
    public class Card
    {
        /// <summary>
        ///     Gets or sets the Comment.
        /// </summary>
        /// <value>
        ///     The Comment.
        /// </value>
        public string Comment { get; set; }

        /// <summary>
        ///     Gets or sets the last modified date.
        /// </summary>
        /// <value>
        ///     The Last modified date.
        /// </value>
        public DateTime LastModifiedDateTimeUtc { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether lost indicator is enabled.
        /// </summary>
        /// <value>
        /// <c>true</c> if lost indicator is enabled; otherwise, <c>false</c>.
        /// </value>
        public bool LostIndicator { get; set; }

        /// <summary>
        ///     Gets or sets the number Last 4 digits.
        /// </summary>
        /// <value>
        ///     The Number Last 4 digits.
        /// </value>
        public string NumberLast4 { get; set; }

        /// <summary>
        ///     Gets or sets the status.
        /// </summary>
        /// <value>
        ///     The status.
        /// </value>
        public string Status { get; set; }
    }
}
