﻿using System;

namespace BbTS.Domain.Models.Commerce
{
    /// <summary>
    ///     The Operation.
    /// </summary>
    public class Operation
    {
        /// <summary>
        ///     Gets or sets the CustomerVerifyRequest.
        /// </summary>
        /// <value>
        ///     The CustomerVerifyRequest.
        /// </value>
        public CustomerVerifyRequest CustomerVerifyRequest { get; set; }

        /// <summary>
        ///     Gets or sets the Error.
        /// </summary>
        /// <value>
        ///     The Error.
        /// </value>
        public Error Error { get; set; }

        /// <summary>
        ///     Gets or sets the Request Timestamp.
        /// </summary>
        /// <value>
        ///     The Request Timestamp.
        /// </value>
        public DateTime RequestTimestampUtc { get; set; }

        /// <summary>
        ///     Gets or sets the Response Timestamp.
        /// </summary>
        /// <value>
        ///     The Response Timestamp.
        /// </value>
        public DateTime ResponseTimestampUtc { get; set; }

        /// <summary>
        ///     Gets or sets the Result.
        /// </summary>
        /// <value>
        ///     The Result.
        /// </value>
        public string Result { get; set; }
    }
}
