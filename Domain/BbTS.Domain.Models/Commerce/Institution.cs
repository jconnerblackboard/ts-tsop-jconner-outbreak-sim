﻿
namespace BbTS.Domain.Models.Commerce
{
    /// <summary>
    ///     Institution Model
    /// </summary>
    public class Institution
    {
        /// <summary>
        ///     Gets or sets the Id.
        /// </summary>
        /// <value>
        ///     The Id.
        /// </value>
        public string Id { get; set; }

        /// <summary>
        ///     Gets or sets the Name.
        /// </summary>
        /// <value>
        ///     The Name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        ///     Gets or sets the Request Scheme.
        /// </summary>
        /// <value>
        ///     The Request Scheme.
        /// </value>
        public string RequestScheme { get; set; }

        /// <summary>
        ///     Gets or sets the RequestValue.
        /// </summary>
        /// <value>
        ///     The RequestValue.
        /// </value>
        public string RequestValue { get; set; }
    }
}
