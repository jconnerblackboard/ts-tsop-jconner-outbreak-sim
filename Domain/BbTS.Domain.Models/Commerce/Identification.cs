﻿
namespace BbTS.Domain.Models.Commerce
{
    /// <summary>
    ///     Service model that handles Identification.
    /// </summary>
    public class Identification
    {
        /// <summary>
        ///     Gets or sets the values.
        /// </summary>
        /// <value>
        ///     The Values.
        /// </value>
        public Values Values { get; set; }

        /// <summary>
        ///     Gets or sets the Method.
        /// </summary>
        /// <value>
        ///     The Method.
        /// </value>
        public string Method { get; set; }

        /// <summary>
        ///     Gets or sets the Verification.
        /// </summary>
        /// <value>
        ///     The Verification.
        /// </value>
        public Verification Verification { get; set; }
    }
}
