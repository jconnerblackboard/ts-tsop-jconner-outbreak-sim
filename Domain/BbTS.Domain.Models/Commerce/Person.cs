﻿using System;

namespace BbTS.Domain.Models.Commerce
{
    /// <summary>
    /// The Person.
    /// </summary>
    public class Person
    {
        /// <summary>
        ///     Gets or sets the Birth date.
        /// </summary>
        /// <value>
        ///     The Identification.
        /// </value>
        public DateTime? BirthDate { get; set; }

        /// <summary>
        ///     Gets or sets the FirstName.
        /// </summary>
        /// <value>
        ///     The FirstName.
        /// </value>
        public string FirstName { get; set; }

        /// <summary>
        ///     Gets or sets the Gender.
        /// </summary>
        /// <value>
        ///     The Gender.
        /// </value>
        public int Gender { get; set; }

        /// <summary>
        ///     Gets or sets the Last Name.
        /// </summary>
        /// <value>
        ///     The Last Name.
        /// </value>
        public string LastName { get; set; }

        /// <summary>
        ///     Gets or sets the Middle Name.
        /// </summary>
        /// <value>
        ///     The Middle Name.
        /// </value>
        public string MiddleName { get; set; }
    }
}
