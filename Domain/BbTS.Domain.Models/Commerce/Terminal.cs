﻿
namespace BbTS.Domain.Models.Commerce
{
    /// <summary>
    ///     Service model that handles terminal identification.
    /// </summary>
    public class Terminal
    {
        /// <summary>
        ///     Gets or sets the Identification.
        /// </summary>
        /// <value>
        ///     The Terminal Identification.
        /// </value>
        public TerminalIdentification Identification { get; set; }
    }
}
