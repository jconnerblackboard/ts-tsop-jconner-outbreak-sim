﻿
namespace BbTS.Domain.Models.Commerce
{
    /// <summary>
    ///     Service model used when creating a customer abstracting customer request from Customer Response.
    /// </summary>
    public class CustomerRequest
    {
        /// <summary>
        ///     Gets or sets the Instrument.
        /// </summary>
        /// <value>
        ///     The Instrument.
        /// </value>
        public Instrument Instrument { get; set; }
    }
}
