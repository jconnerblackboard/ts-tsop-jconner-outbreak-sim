﻿
namespace BbTS.Domain.Models.Commerce
{
    /// <summary>
    ///  Service model that handles verification of an customer verify.
    /// </summary>
    public class CustomerVerify
    {
        /// <summary>
        ///     Gets or sets the customer response object.
        /// </summary>
        /// <value>
        ///     The customer.
        /// </value>
        public Customer Customer { get; set; }

        /// <summary>
        ///     Gets or sets the Operation.
        /// </summary>
        /// <value>
        ///     The Operation.
        /// </value>
        public Operation Operation { get; set; }

        /// <summary>
        ///     Gets or sets the response options.
        /// </summary>
        /// <value>
        ///     The response options.
        /// </value>
        public Route Route { get; set; }
    }
}
