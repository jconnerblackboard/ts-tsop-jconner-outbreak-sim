﻿
namespace BbTS.Domain.Models.Commerce
{
    /// <summary>
    ///     Service model that handles verification of an customer.
    /// </summary>
    public class CustomerVerifyRequest
    {
        /// <summary>
        ///     Gets or sets the customer.
        /// </summary>
        /// <value>
        ///     The customer.
        /// </value>
        public CustomerRequest Customer { get; set; }

        /// <summary>
        ///     Gets or sets the Response Echo Level.
        /// </summary>
        /// <value>
        ///     The Response Echo Level.
        /// </value>
        public string ResponseEchoLevel { get; set; }

        /// <summary>
        ///     Gets or sets the response options.
        /// </summary>
        /// <value>
        ///     The response options.
        /// </value>
        public ResponseOptions ResponseOptions { get; set; }

        /// <summary>
        ///     Gets or sets the terminal.
        /// </summary>
        /// <value>
        ///     The terminal details.
        /// </value>
        public Terminal Terminal { get; set; }
    }
}
