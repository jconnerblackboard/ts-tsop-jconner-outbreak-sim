﻿
namespace BbTS.Domain.Models.Commerce
{
    /// <summary>
    ///     Service model that handles values.
    /// </summary>
    public class Values
    {
        /// <summary>
        ///     Gets or sets the Card Magnetic stripe track1.
        /// </summary>
        /// <value>
        ///     The Card Magnetic stripe track1.
        /// </value>
        public string CardMagstripeTrack1 { get; set; }

        /// <summary>
        ///     Gets or sets the Card Magnetic stripe track2.
        /// </summary>
        /// <value>
        ///     The Card Magnetic stripe track2.
        /// </value>
        public string CardMagstripeTrack2 { get; set; }

        /// <summary>
        ///     Gets or sets the Card Magnetic stripe track3.
        /// </summary>
        /// <value>
        ///     The Card Magnetic stripe track3.
        /// </value>
        public string CardMagstripeTrack3 { get; set; }

        /// <summary>
        ///     Gets or sets the Card Number.
        /// </summary>
        /// <value>
        ///     The Card Number.
        /// </value>
        public string CardNumber { get; set; }

        /// <summary>
        ///     Gets or sets the issue number.
        /// </summary>
        /// <value>
        ///     The issue number.
        /// </value>
        public string IssueNumber { get; set; }
    }
}
