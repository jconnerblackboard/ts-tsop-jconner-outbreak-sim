﻿
namespace BbTS.Domain.Models.Commerce
{
    /// <summary>
    ///     Service model that handles Terminal Identification.
    /// </summary>
    public class TerminalIdentification
    {
        /// <summary>
        ///     Gets or sets the Method.
        /// </summary>
        /// <value>
        ///     The Method.
        /// </value>
        public string Method { get; set; }
    }
}