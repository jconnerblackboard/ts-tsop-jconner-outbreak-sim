﻿
namespace BbTS.Domain.Models.Commerce
{
    /// <summary>
    ///     Service model that handles verification of an customer.
    /// </summary>
    public class CustomerVerifyRequestObject
    {
        /// <summary>
        ///     Gets or sets the customer.
        /// </summary>
        public CustomerVerifyRequest CustomerVerifyRequest { get; set; }
    }
}
