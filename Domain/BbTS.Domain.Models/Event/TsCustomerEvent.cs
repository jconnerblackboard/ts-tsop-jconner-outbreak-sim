﻿using BbTS.Domain.Models.Definitions.Event;
using System;

namespace BbTS.Domain.Models.Event
{
    /// <summary>
    /// Customer event
    /// </summary>
    public class TsCustomerEvent : TsCustomerEventBase
    {
        /// <summary>
        /// Merchant Id
        /// </summary>
        public Int16 MerchantId { get; set; }

        /// <summary>
        /// Merchant Name
        /// </summary>
        public string MerchantName { get; set; }

        /// <summary>
        /// Event Plan Id
        /// </summary>
        public int EventPlanId { get; set; }

        /// <summary>
        /// Event number
        /// </summary>
        public int EventNumber { get; set; }

        /// <summary>
        /// Event name
        /// </summary>
        public string EventName { get; set; }

        /// <summary>
        /// Is active
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Not part of event plan or customized
        /// </summary>
        public bool IsCustom { get; set; }

        /// <summary>
        /// Event limit type
        /// </summary>
        public EventLimitType EventLimitType { get; set; }

        /// <summary>
        /// Reset date
        /// </summary>
        public DateTime? ResetDate { get; set; }
    }
}
