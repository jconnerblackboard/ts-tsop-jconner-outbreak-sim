﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Event
{
    /// <summary>
    /// This object is a container for Event Groups Get Response
    /// </summary>
    public class EventGroupsGetResponse
    {
        /// <summary>
        /// Events
        /// </summary>
        public List<TsEventGroup> EventGroups { get; set; }
    }
}
