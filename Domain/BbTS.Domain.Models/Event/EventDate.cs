﻿using System;

namespace BbTS.Domain.Models.Event
{
    /// <summary>
    /// Container class for event dates.
    /// </summary>
    public class EventDate
    {
        /// <summary>
        /// The start time of the event.
        /// </summary>
        public DateTime EventStartDateTime { get; set; }

        /// <summary>
        /// The stop time of the event.
        /// </summary>
        public DateTime EventStopDateTime { get; set; }
    }

    /// <summary>
    /// View for a <see cref="EventDate"/>.  (Version 1)
    /// </summary>
    public class EventDateViewV01
    {
        /// <summary>
        /// The start time of the event.
        /// </summary>
        public DateTime EventStartDateTime { get; set; }

        /// <summary>
        /// The stop time of the event.
        /// </summary>
        public DateTime EventStopDateTime { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="EventDate"/> conversion.
    /// </summary>
    public static class EventDateConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="EventDateViewV01"/> object based on this <see cref="EventDate"/>.
        /// </summary>
        /// <param name="eventDate"></param>
        /// <returns></returns>
        public static EventDateViewV01 ToEventDateViewV01(this EventDate eventDate)
        {
            if (eventDate == null) return null;

            return new EventDateViewV01
            {
                EventStartDateTime = eventDate.EventStartDateTime,
                EventStopDateTime = eventDate.EventStopDateTime
            };
        }

        /// <summary>
        /// Returns a <see cref="EventDate"/> object based on this <see cref="EventDateViewV01"/>.
        /// </summary>
        /// <param name="eventDateViewV01"></param>
        /// <returns></returns>
        public static EventDate ToEventDate(this EventDateViewV01 eventDateViewV01)
        {
            if (eventDateViewV01 == null) return null;

            return new EventDate
            {
                EventStartDateTime = eventDateViewV01.EventStartDateTime,
                EventStopDateTime = eventDateViewV01.EventStopDateTime
            };
        }
        #endregion
    }
}
