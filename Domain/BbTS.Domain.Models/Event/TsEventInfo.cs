﻿using System;

namespace BbTS.Domain.Models.Event
{
    /// <summary>
    /// Event info
    /// </summary>
    public class TsEventInfo
    {
        /// <summary>
        /// Event Id
        /// </summary>
        public int EventId { get; set; }

        /// <summary>
        /// Merchant Id
        /// </summary>
        public Int16 MerchantId { get; set; }

        /// <summary>
        /// Merchant Name
        /// </summary>
        public string MerchantName { get; set; }

        /// <summary>
        /// Event number
        /// </summary>
        public int EventNumber { get; set; }

        /// <summary>
        /// Event name
        /// </summary>
        public string EventName { get; set; }
    }
}
