﻿
namespace BbTS.Domain.Models.Event
{
    /// <summary>
    /// This object represents a record in the Transact system for an event plan associcated to a customer
    /// </summary>
    public class TsCustomerEventPost : TsCustomerEventBase
    {
        /// <summary>
        /// Reset date
        /// </summary>
        public double? ResetDate { get; set; }

        /// <summary>
        /// Event limit type string
        /// </summary>
        public string EventLimitType { get; set; }
    }
}
