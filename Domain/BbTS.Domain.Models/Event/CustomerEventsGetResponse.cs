﻿
namespace BbTS.Domain.Models.Event
{
    /// <summary>
    /// Customer events get response
    /// </summary>
    public class CustomerEventsGetResponse : CustomerEventsBaseGetResponse
    {
        /// <summary>
        /// Event plan Id
        /// </summary>
        public int EventPlanId { get; set; }

        /// <summary>
        /// Event plan active
        /// </summary>
        public bool EventPlanActive { get; set; }
    }
}
