﻿namespace BbTS.Domain.Models.Event
{
    /// <summary>
    /// Response type for EventAttendanceGet operation
    /// </summary>
    public class EventAttendanceGetResponse
    {
        /// <summary>
        /// EventId
        /// </summary>
        public int EventId { get; set; }

        /// <summary>
        /// Attendance.
        /// </summary>
        public int Attendance { get; set; }

        /// <summary>
        /// Capacity.
        /// </summary>
        public int Capacity { get; set; }

        /// <summary>
        /// Threshold.
        /// </summary>
        public int Threshold { get; set; }

        /// <summary>
        /// Expected Attendance.
        /// </summary>
        public int ExpectedAttendance { get; set; }
    }
}
