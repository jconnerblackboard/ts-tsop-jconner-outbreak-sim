﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Event
{
    /// <summary>
    /// Customer events get response
    /// </summary>
    public class CustomerEventsBaseGetResponse
    {
        /// <summary>
        /// Customer events
        /// </summary>
        public List<TsCustomerEvent> CustomerEvents { get; set; }
    }
}
