﻿
namespace BbTS.Domain.Models.Event
{
    /// <summary>
    /// Customer event
    /// </summary>
    public class TsCustomerEventBase
    {
        /// <summary>
        /// Event Id
        /// </summary>
        public int EventId { get; set; }

        /// <summary>
        /// Regular Allowed
        /// </summary>
        public int RegularAllowed { get; set; }

        /// <summary>
        /// Guest Allowed
        /// </summary>
        public int GuestAllowed { get; set; }
    }
}
