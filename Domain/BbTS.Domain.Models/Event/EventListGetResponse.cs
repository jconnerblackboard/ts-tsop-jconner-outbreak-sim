﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Event
{
    /// <summary>
    /// This object is a container for Event List Get Response
    /// </summary>
    public class EventListGetResponse : EventInfoGetResponse
    {
        /// <summary>
        /// Total event count
        /// </summary>
        public int TotalEventCount { get; set; }
    }
}
