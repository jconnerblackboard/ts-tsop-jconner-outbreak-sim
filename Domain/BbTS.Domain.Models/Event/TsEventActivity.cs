﻿
namespace BbTS.Domain.Models.Event
{
    /// <summary>
    /// This object represents a record in the Transact system for an event
    /// </summary>
    public class TsEventActivity
    {
        /// <summary>
        /// The identity of the event
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The merchant id for this event
        /// </summary>
        public int MerchantNumber { get; set; }

        /// <summary>
        /// The friendly event number
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// The name of the event
        /// </summary>
        public string Name { get; set; }
    }
}
