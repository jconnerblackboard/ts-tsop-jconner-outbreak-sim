﻿using BbTS.Domain.Models.Definitions.Event;
using System;

namespace BbTS.Domain.Models.Event
{
    /// <summary>
    /// Event group
    /// </summary>
    public class TsEventGroup
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Merchant Id
        /// </summary>
        public Int16 MerchantId { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Type
        /// </summary>
        public EventGroupType Type { get; set; }

        /// <summary>
        /// Is active
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Creation date
        /// </summary>
        public DateTime? CreationDate { get; set; }

    }
}
