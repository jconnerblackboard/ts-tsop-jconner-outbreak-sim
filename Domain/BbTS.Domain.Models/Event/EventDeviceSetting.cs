﻿using System;
using System.Collections.Generic;
using System.Linq;
using BbTS.Domain.Models.Definitions.Device;

namespace BbTS.Domain.Models.Event
{
    /// <summary>
    /// Container class for events belonging to a device.
    /// </summary>
    public class EventDeviceSetting
    {
        /// <summary>
        /// The identifier for the event.
        /// </summary>
        public int EventId { get; set; }

        /// <summary>
        /// The name of the event.
        /// </summary>
        public string EventName { get; set; }

        /// <summary>
        /// The number assigned to the event.
        /// </summary>
        public int EventNumber { get; set; }

        /// <summary>
        /// The schedule type (Daily, Weekly, Monthly, Custom).
        /// </summary>
        public EventDeviceSettingScheduleType ScheduleType { get; set; }

        /// <summary>
        /// The capacity of the event.
        /// </summary>
        public int Capacity { get; set; }

        /// <summary>
        /// Event dates
        /// </summary>
        public List<EventDate> Dates { get; set; }

        /// <summary>
        /// The UTC offset of the included DateTime objects.
        /// </summary>
        public string DateTimeOffset { get; set; }

        /// <summary>
        /// Allow non-customer entry.
        /// </summary>
        public bool AllowNonCustomerEntry{ get; set; }

        /// <summary>
        /// Display attendance after each entry.
        /// </summary>
        public bool DisplayAttendanceAfterEachEntry { get; set; }

        /// <summary>
        /// Expected attendance
        /// </summary>
        public int? ExpectedAttendance { get; set; }

        /// <summary>
        /// Threshold
        /// </summary>
        public int? Threshold { get; set; }

        /// <summary>
        /// Minimum age
        /// </summary>
        public int? MinimumAge { get; set; }

        /// <summary>
        /// Populate dates based on data provided
        /// </summary>
        /// <param name="startDateTime">Start date time</param>
        /// <param name="endDateTime">End date time</param>
        /// <param name="eventFrequency">Event frequency</param>
        /// <param name="endDate">Optional end date</param>
        public void PopulateDates(DateTime startDateTime, DateTime endDateTime, string eventFrequency, DateTime? endDate = null)
        {
            Dates = new List<EventDate>();
            if (endDateTime - startDateTime >= TimeSpan.FromDays(1))
                endDateTime = startDateTime.AddDays(1).AddSeconds(-1);

            switch (ScheduleType)
            {
                case EventDeviceSettingScheduleType.Daily:
                    int days;
                    if (!int.TryParse(eventFrequency, out days))
                        days = 1;

                    // Populate next 7 upcoming occurences
                    for (var i = 0; i < 7; i++)
                    {
                        var date = new EventDate()
                        {
                            EventStartDateTime = startDateTime.AddDays(i * days),
                            EventStopDateTime = endDateTime.AddDays(i * days)
                        };

                        // Event ends in less than 7 occurences
                        if (date.EventStopDateTime.Date > endDate)
                            break;

                        Dates.Add(date);
                    }

                    return;
                case EventDeviceSettingScheduleType.Weekly:
                    var allowedDays = eventFrequency?.ToCharArray().Select(x => char.ToUpper(x) == 'T').ToArray();
                    if (allowedDays?.Length != 7 || allowedDays.All(x => !x))
                        return;

                    while (Dates.Count < 7)
                    {
                        if (startDateTime > endDate)
                            break;

                        if (allowedDays[(int)startDateTime.DayOfWeek])
                        {
                            Dates.Add(new EventDate()
                            {
                                EventStartDateTime = startDateTime,
                                EventStopDateTime = endDateTime
                            });
                        }

                        startDateTime = startDateTime.AddDays(1);
                        endDateTime = endDateTime.AddDays(1);
                    }
                    return;
                case EventDeviceSettingScheduleType.Monthly:
                    var allowedMonths = eventFrequency?.ToCharArray().Select(x => char.ToUpper(x) == 'T').ToArray();
                    if (allowedMonths?.Length != 12 || allowedMonths.All(x => !x))
                        return;

                    while (Dates.Count < 7)
                    {
                        if (startDateTime > endDate)
                            break;

                        if (allowedMonths[startDateTime.Month - 1])
                        {
                            Dates.Add(new EventDate()
                            {
                                EventStartDateTime = startDateTime,
                                EventStopDateTime = endDateTime
                            });
                        }

                        startDateTime = startDateTime.AddMonths(1);
                        endDateTime = endDateTime.AddMonths(1);
                    }
                    return;
                case EventDeviceSettingScheduleType.Custom:
                    var dates = eventFrequency?.Split(';');
                    if (dates == null || !dates.Any())
                        return;

                    for (int i = 0; i < Math.Min(dates.Length, 7); i++)
                    {
                        var date = dates[i].Split(',');
                        if (date.Length != 2)
                            continue;

                        Dates.Add(new EventDate()
                        {
                            EventStartDateTime = DateTime.FromOADate(double.Parse(date[0])),
                            EventStopDateTime = DateTime.FromOADate(double.Parse(date[1]))
                        });
                    }
                    return;
                default:
                    return;
            }
        }
    }

    /// <summary>
    /// View for a <see cref="EventDeviceSetting"/>.  (Version 1)
    /// </summary>
    public class EventDeviceSettingViewV01
    {
        /// <summary>
        /// The identifier for the event.
        /// </summary>
        public int EventId { get; set; }

        /// <summary>
        /// The name of the event.
        /// </summary>
        public string EventName { get; set; }

        /// <summary>
        /// The number assigned to the event.
        /// </summary>
        public int EventNumber { get; set; }

        /// <summary>
        /// The schedule type (Daily, Weekly, Monthly, Custom).
        /// </summary>
        public EventDeviceSettingScheduleType ScheduleType { get; set; }

        /// <summary>
        /// The capacity of the event.
        /// </summary>
        public int Capacity { get; set; }

        /// <summary>
        /// Event dates
        /// </summary>
        public List<EventDateViewV01> Dates { get; set; }

        /// <summary>
        /// The UTC offset of the included DateTime objects.
        /// </summary>
        public string DateTimeOffset { get; set; }

        /// <summary>
        /// Allow non-customer entry.
        /// </summary>
        public bool AllowNonCustomerEntry { get; set; }

        /// <summary>
        /// Display attendance after each entry.
        /// </summary>
        public bool DisplayAttendanceAfterEachEntry { get; set; }

        /// <summary>
        /// Expected attendance
        /// </summary>
        public int? ExpectedAttendance { get; set; }

        /// <summary>
        /// Threshold
        /// </summary>
        public int? Threshold { get; set; }

        /// <summary>
        /// Minimum age
        /// </summary>
        public int? MinimumAge { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="EventDeviceSetting"/> conversion.
    /// </summary>
    public static class EventDeviceSettingConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="EventDeviceSettingViewV01"/> object based on this <see cref="EventDeviceSetting"/>.
        /// </summary>
        /// <param name="eventDeviceSetting"></param>
        /// <returns></returns>
        public static EventDeviceSettingViewV01 ToEventDeviceSettingViewV01(this EventDeviceSetting eventDeviceSetting)
        {
            if (eventDeviceSetting == null) return null;

            return new EventDeviceSettingViewV01
            {
                EventId = eventDeviceSetting.EventId,
                EventName = eventDeviceSetting.EventName,
                EventNumber = eventDeviceSetting.EventNumber,
                ScheduleType = eventDeviceSetting.ScheduleType,
                Capacity = eventDeviceSetting.Capacity,
                Dates = eventDeviceSetting.Dates.Select(eventDate => eventDate.ToEventDateViewV01()).ToList(),
                DateTimeOffset = eventDeviceSetting.DateTimeOffset,
                AllowNonCustomerEntry = eventDeviceSetting.AllowNonCustomerEntry,
                DisplayAttendanceAfterEachEntry = eventDeviceSetting.DisplayAttendanceAfterEachEntry,
                ExpectedAttendance = eventDeviceSetting.ExpectedAttendance,
                Threshold = eventDeviceSetting.Threshold,
                MinimumAge = eventDeviceSetting.MinimumAge
            };
        }

        /// <summary>
        /// Returns a <see cref="EventDeviceSetting"/> object based on this <see cref="EventDeviceSettingViewV01"/>.
        /// </summary>
        /// <param name="eventDeviceSettingViewV01"></param>
        /// <returns></returns>
        public static EventDeviceSetting ToEventDeviceSetting(this EventDeviceSettingViewV01 eventDeviceSettingViewV01)
        {
            if (eventDeviceSettingViewV01 == null) return null;

            return new EventDeviceSetting
            {
                EventId = eventDeviceSettingViewV01.EventId,
                EventName = eventDeviceSettingViewV01.EventName,
                EventNumber = eventDeviceSettingViewV01.EventNumber,
                ScheduleType = eventDeviceSettingViewV01.ScheduleType,
                Capacity = eventDeviceSettingViewV01.Capacity,
                Dates = eventDeviceSettingViewV01.Dates.Select(eventDateViewV01 => eventDateViewV01.ToEventDate()).ToList(),
                DateTimeOffset = eventDeviceSettingViewV01.DateTimeOffset,
                AllowNonCustomerEntry = eventDeviceSettingViewV01.AllowNonCustomerEntry,
                DisplayAttendanceAfterEachEntry = eventDeviceSettingViewV01.DisplayAttendanceAfterEachEntry,
                ExpectedAttendance = eventDeviceSettingViewV01.ExpectedAttendance,
                Threshold = eventDeviceSettingViewV01.Threshold,
                MinimumAge = eventDeviceSettingViewV01.MinimumAge
            };
        }
        #endregion
    }
}
