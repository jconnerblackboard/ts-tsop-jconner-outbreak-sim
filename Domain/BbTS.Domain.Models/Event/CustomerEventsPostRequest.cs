﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Event
{
    /// <summary>
    /// Customer event post
    /// </summary>
    [XmlRoot("CustomerEventsSet")]
    public class CustomerEventsPostRequest
    {
        /// <summary>
        /// Request Id
        /// </summary>
        [XmlIgnore]
        public Guid RequestId { get; set; }

        /// <summary>
        /// Events
        /// </summary>
        [XmlArray, XmlArrayItem("Event")]
        public List<TsCustomerEventPost> Events { get; set; }

        /// <summary>
        /// Event plan Id
        /// </summary>
        [XmlIgnore]
        public int? EventPlanId { get; set; }

        /// <summary>
        /// Event plan active
        /// </summary>
        [XmlIgnore]
        public bool EventPlanActive { get; set; }
    }
}
