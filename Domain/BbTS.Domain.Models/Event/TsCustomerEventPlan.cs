﻿
namespace BbTS.Domain.Models.Event
{
    /// <summary>
    /// This object represents a record in the Transact system for an event plan associcated to a customer
    /// </summary>
    public class TsCustomerEventPlan
    {
        /// <summary>
        /// The identity of the customer
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Whether this event plan is enabled or not
        /// </summary>
        public bool EventPlanEnabled { get; set; }

        /// <summary>
        /// The identity of the event plan associated with this customer
        /// </summary>
        public int EventPlanId { get; set; }
    }
}
