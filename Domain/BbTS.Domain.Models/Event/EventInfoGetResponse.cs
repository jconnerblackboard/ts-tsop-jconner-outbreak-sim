﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Event
{
    /// <summary>
    /// This object is a container for Event Info Get Response
    /// </summary>
    public class EventInfoGetResponse
    {
        /// <summary>
        /// Events
        /// </summary>
        public List<TsEventInfo> Events { get; set; }
    }
}
