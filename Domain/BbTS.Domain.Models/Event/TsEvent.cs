﻿using System;

namespace BbTS.Domain.Models.Event
{
    /// <summary>
    /// This object represents a record in the Transact system for an event
    /// </summary>
    public class TsEvent
    {
        /// <summary>
        /// The identity of the event
        /// </summary>
        public Int32 EventId { get; set; }
        /// <summary>
        /// The merchant id for this event
        /// </summary>
        public Int32 MerchantId { get; set; }
        /// <summary>
        /// The friendly event number
        /// </summary>
        public Int32 EventNumber { get; set; }
        /// <summary>
        /// The name of the event
        /// </summary>
        public String EventName { get; set; }
        /// <summary>
        /// The location identity of the event
        /// </summary>
        public Int32 LocationId { get; set; }
        /// <summary>
        /// The event class identity
        /// </summary>
        public Int32 EventClassId { get; set; }
        /// <summary>
        /// The event status identity
        /// </summary>
        public Int32 EventStatusId { get; set; }
        /// <summary>
        /// The minimum age of an attendee
        /// </summary>
        public Int32 MinimumAge { get; set; }
        /// <summary>
        /// Whether to check the age of the attendee or not
        /// </summary>
        public Boolean AgeCheck { get; set; }
        /// <summary>
        /// The attendance threshhold
        /// </summary>
        public Int32 Threshold { get; set; }
        /// <summary>
        /// The capacity of the event
        /// </summary>
        public Int32 Capacity { get; set; }
        /// <summary>
        /// Whether to display attendance or not
        /// </summary>
        public Boolean DisplayAttendance { get; set; }
        /// <summary>
        /// The expected attendance
        /// </summary>
        public Int32 ExpectedAttendance { get; set; }
        /// <summary>
        /// The start date of the event
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// The end date of the event
        /// </summary>
        public DateTime EndDate { get; set; }
        /// <summary>
        /// The schedule type (e.g. 1 means Daily, 2 means Weekly, 3 means Monthly, 4 means Custom)
        /// </summary>
        public Int32 ScheduleType { get; set; }
        /// <summary>
        /// Whether to allow a non-customer
        /// </summary>
        public Boolean AllowNonCustomer { get; set; }
        /// <summary>
        /// Whether to account the same customer multiple times
        /// </summary>
        public Boolean AllowSameCustomer { get; set; }
        /// <summary>
        /// Whether to track the same customer as a reentry
        /// </summary>
        public Boolean TrackSameCustomerAsReentry { get; set; }
        /// <summary>
        /// Whether to require swipe out or not
        /// </summary>
        public Boolean RequireSwipeOutForReentry { get; set; }
    }
}
