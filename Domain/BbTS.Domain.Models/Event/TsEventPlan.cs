﻿using System;

namespace BbTS.Domain.Models.Event
{
    /// <summary>
    /// This object represents a record in the Transact system for an event plan
    /// </summary>
    public class TsEventPlan
    {
        /// <summary>
        /// The identity of this event plan
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The name of the event plan
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Whether the event plan is active or not
        /// </summary>
        public bool Active { get; set; }
        /// <summary>
        /// The date this plan becomes active
        /// </summary>
        public DateTime? StartDate { get; set; }
        /// <summary>
        /// The date this plan ends
        /// </summary>
        public DateTime? StopDate { get; set; }
        /// <summary>
        /// Whether to use the holiday calendar or not
        /// </summary>
        public bool UseHolidayCalendar { get; set; }
    }
}
