﻿using System;
using System.Collections.Generic;

namespace BbTS.Domain.Models.Event
{
    /// <summary>
    /// This object is a container for Event List Get Request
    /// </summary>
    public class EventListGetRequest
    {
        /// <summary>
        /// Merchant
        /// </summary>
        public Int16? MerchantId { get; set; }

        /// <summary>
        /// Event number
        /// </summary>
        public int? EventNumber { get; set; }

        /// <summary>
        /// Event Name
        /// </summary>
        public string EventName { get; set; }

        /// <summary>
        /// Offset of the result
        /// </summary>
        public int Offset { get; set; }

        /// <summary>
        /// Number of items per page
        /// </summary>
        public int? PageSize { get; set; }

        /// <summary>
        /// Formats properties as optional uri params
        /// </summary>
        /// <returns></returns>
        public string FormUriParams()
        {
            var list = new List<string>();

            if (MerchantId.HasValue)
                list.Add($"{nameof(MerchantId)}={MerchantId}");
            if (EventNumber.HasValue)
                list.Add($"{nameof(EventNumber)}={EventNumber}");
            if (!string.IsNullOrEmpty(EventName))
                list.Add($"{nameof(EventName)}={Uri.EscapeUriString(EventName)}");
            if (Offset > 0)
                list.Add($"{nameof(Offset)}={Offset}");
            if (PageSize.HasValue)
                list.Add($"{nameof(PageSize)}={PageSize}");

            return string.Join("&", list);
        }
    }
}
