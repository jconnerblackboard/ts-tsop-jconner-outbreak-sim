﻿using System;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Cashier
{
    /// <summary>
    /// This object represents a Transact Cashier object. Cashiers
    /// </summary>
    [Serializable]
    public class TsCashier
    {
        /// <summary>
        /// Cashier ID
        /// </summary>
        [XmlAttribute]
        public int Cashier_Id { get; set; }
        /// <summary>
        /// Merchant ID for the cashier
        /// </summary>
        [XmlAttribute]
        public int Merchant_Id { get; set; }
        /// <summary>
        /// Cashier Number
        /// </summary>
        [XmlAttribute]
        public int CashierNumber { get; set; }
        /// <summary>
        /// Indicates if the cashier is active
        /// </summary>
        [XmlAttribute]
        public string Is_Active { get; set; }
        /// <summary>
        /// Cashier PIN
        /// </summary>
        [XmlAttribute]
        public int Pin { get; set; }
        /// <summary>
        /// Cashier Name
        /// </summary>
        [XmlAttribute]
        public string FullName { get; set; }
        /// <summary>
        /// T means cashier has manager override privilege
        /// </summary>
        [XmlAttribute]
        public string Manager { get; set; }
        /// <summary>
        /// Cashier Drawer Number
        /// </summary>
        [XmlAttribute]
        public int CashDrawer { get; set; }
        /// <summary>
        ///
        /// </summary>
        [XmlAttribute]
        public int Cust_Id { get; set; }
        /// <summary>
        ///
        /// </summary>
        [XmlAttribute]
        public string PinRequired { get; set; }
        /// <summary>
        ///
        /// </summary>
        [XmlAttribute]
        public string PinChangeRequired { get; set; }
    }
}