﻿using System;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Product
{
    /// <summary>
    /// This object represents a Transact DsProductDetail object. Contains product setup information by profit center.
    /// </summary>
    [Serializable]
    public class TsDsProductDetail
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int ProdDetail_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int DsProductStatus_Id { get; set; }
        /// <summary>
        /// Product ID
        /// </summary>
        [XmlAttribute]
        public int Product_Id { get; set; }
        /// <summary>
        /// Profit Center ID
        /// </summary>
        [XmlAttribute]
        public int ProfitCenter_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Location_Id { get; set; }
        /// <summary>
        /// Retail Price of product
        /// </summary>
        [XmlAttribute]
        public int RetailPrice { get; set; }
        /// <summary>
        /// Minimum Price that can be charge when using variable pricing (in MilliDollars)
        /// </summary>
        [XmlAttribute]
        public int RetailPrice_VariablePrice_Min { get; set; }
        /// <summary>
        /// Maximum Price that can be charge when using variable pricing (in MilliDollars)
        /// </summary>
        [XmlAttribute]
        public int RetailPrice_VariablePrice_Max { get; set; }
        /// <summary>
        /// Unit Cost (in MilliDollars)
        /// </summary>
        [XmlAttribute]
        public int UnitCost { get; set; }
        /// <summary>
        /// Gross Profit per unit (in MilliDollars)
        /// </summary>
        [XmlAttribute]
        public int Contribution { get; set; }
        /// <summary>
        /// T means Receipt is forced to print
        /// </summary>
        [XmlAttribute]
        public string ForceReceipt { get; set; }
        /// <summary>
        /// Indicates if the detail can be overridden (T/F)
        /// </summary>
        [XmlAttribute]
        public string AllowOverride { get; set; }
        /// <summary>
        /// Discount 1 Quantity
        /// </summary>
        [XmlAttribute]
        public int DiscountQty1 { get; set; }
        /// <summary>
        /// Discount 1 Price Amount (in MilliDollars)
        /// </summary>
        [XmlAttribute]
        public int DiscountPrice1 { get; set; }
        /// <summary>
        /// Discount 2 Quantity
        /// </summary>
        [XmlAttribute]
        public int DiscountQty2 { get; set; }
        /// <summary>
        /// Discount 2 Price Amount (in MilliDollars)
        /// </summary>
        [XmlAttribute]
        public int DiscountPrice2 { get; set; }
        /// <summary>
        /// Discount 3 Quantity
        /// </summary>
        [XmlAttribute]
        public int DiscountQty3 { get; set; }
        /// <summary>
        /// Discount 3 Price Amount (in MilliDollars)
        /// </summary>
        [XmlAttribute]
        public int DiscountPrice3 { get; set; }
        /// <summary>
        /// Last Modified DateTime
        /// </summary>
        [XmlAttribute]
        public DateTime LastModifiedDateTime { get; set; }
        /// <summary>
        /// Allow AutoDiscount Indicator (T/F)
        /// </summary>
        [XmlAttribute]
        public string AllowAutoDiscnt { get; set; }
        /// <summary>
        /// Allow AutoSurcharge Indicator (T/F)
        /// </summary>
        [XmlAttribute]
        public string AllowAutoSurchg { get; set; }
        /// <summary>
        /// Key Top Line 1 Caption
        /// </summary>
        [XmlAttribute]
        public string KeyTop1 { get; set; }
        /// <summary>
        /// Key Top Line 2 Caption
        /// </summary>
        [XmlAttribute]
        public string KeyTop2 { get; set; }
        /// <summary>
        /// Used Default Remote Printer Indicator (T/F)
        /// </summary>
        [XmlAttribute]
        public string UseDefRemotePrn { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int RemotePrinter_Id { get; set; }
    }
}