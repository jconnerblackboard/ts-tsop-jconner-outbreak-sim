﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Definitions.Transaction;

namespace BbTS.Domain.Models.Product
{
    /// <summary>
    /// Container class for defining a product on a device.
    /// </summary>
    public class ProductDeviceSetting
    {
        /// <summary>
        /// The id of the product.
        /// </summary>
        public string ProductId { get; set; }

        /// <summary>
        /// The product detail id.
        /// </summary>
        public int ProductDetailId { get; set; }

        /// <summary>
        /// The product number.
        /// </summary>
        public string ProductNumber { get; set; }

        /// <summary>
        /// The description of the product.
        /// </summary>
        public string ProductDescription { get; set; }

        /// <summary>
        /// The product type.
        /// </summary>
        public ProductType ProductType { get; set; }

        /// <summary>
        /// The id of the tax schedule associated with this product.
        /// </summary>
        public int TaxScheduleId { get; set; }

        /// <summary>
        /// The id of the tax group this product belongs to.
        /// </summary>
        public int TaxGroupId { get; set; }

        /// <summary>
        /// The retail price of the product.
        /// </summary>
        public decimal RetailPrice { get; set; }

        /// <summary>
        /// The variable minimum price.
        /// </summary>
        public decimal VariablePriceMinimum { get; set; }

        /// <summary>
        /// the variable maximum price.
        /// </summary>
        public decimal VariablePriceMaximum { get; set; }

        /// <summary>
        /// If true, a receipt must be printed if this product is part of the transaction.
        /// </summary>
        public bool ForceReceipt { get; set; }

        /// <summary>
        /// Indicates if the product price can be modified by a Discount or Surcharge operation initiated by the operator.
        /// </summary>
        public bool AllowOverride { get; set; }

        /// <summary>
        /// Threshold at which the DiscountPrice1 should be used for the product price.
        /// </summary>
        public int DiscountQuantity1 { get; set; }

        /// <summary>
        /// Price to be used for the product when the quantity of this product in the transaction >= DiscountQuantity1.
        /// </summary>
        public decimal DiscountPrice1 { get; set; }

        /// <summary>
        /// Threshold at which the DiscountPrice2 should be used for the product price.
        /// </summary>
        public int DiscountQuantity2 { get; set; }

        /// <summary>
        /// Price to be used for the product when the quantity of this product in the transaction >= DiscountQuantity2.
        /// </summary>
        public decimal DiscountPrice2 { get; set; }

        /// <summary>
        /// Threshold at which the DiscountPrice3 should be used for the product price.
        /// </summary>
        public int DiscountQuantity3 { get; set; }

        /// <summary>
        /// Price to be used for the product when the quantity of this product in the transaction >= DiscountQuantity3.
        /// </summary>
        public decimal DiscountPrice3 { get; set; }

        /// <summary>
        /// DateTime object (apiary can`t do datetime) Last modified date/time (UTC)
        /// </summary>
        public DateTime LastModified { get; set; }

        /// <summary>
        /// Indicates if the product price can be discounted by policy-based discount rule.
        /// </summary>
        public bool AllowPolicyDiscount { get; set; }

        /// <summary>
        /// Indicates if the product price can be surcharged by policy-based surcharge rule.
        /// </summary>
        public bool AllowPolicySurcharge { get; set; }

        /// <summary>
        /// The UTC offset of the included DateTime objects.
        /// </summary>
        public string DateTimeOffset { get; set; }


    }
}
