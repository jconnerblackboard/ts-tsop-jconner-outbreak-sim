﻿using System;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Product
{
    /// <summary>
    /// This object represents a Transact DsProductPromo object. Product Promotions
    /// </summary>
    [Serializable]
    public class TsDsProductPromo
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int ProdPromo_Id { get; set; }
        /// <summary>
        /// Product Promotion Name
        /// </summary>
        [XmlAttribute]
        public string ProdPromo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int ProfitCenter_Id { get; set; }
        /// <summary>
        /// Key Top Line 1 Caption
        /// </summary>
        [XmlAttribute]
        public string KeyTop1 { get; set; }
        /// <summary>
        /// Key Top Line 2 Caption
        /// </summary>
        [XmlAttribute]
        public string KeyTop2 { get; set; }
    }
}