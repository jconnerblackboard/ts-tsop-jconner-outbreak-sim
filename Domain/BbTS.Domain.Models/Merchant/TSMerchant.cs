﻿
namespace BbTS.Domain.Models.Merchant
{
    /// <summary>
    /// Merchant entity
    /// </summary>
    public class TsMerchant
    {
        /// <summary>
        /// Mechant Id
        /// </summary>
        public int MerchantId { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Mechant Number
        /// </summary>
        public string MerchantNumber { get; set; }

        /// <summary>
        /// Minimum commission
        /// </summary>
        public decimal MinimumCommission { get; set; }

        /// <summary>
        /// Payments allowed
        /// </summary>
        public bool PaymentsAllowed { get; set; }

        /// <summary>
        /// Fine dining
        /// </summary>
        public bool FineDining { get; set; }

        /// <summary>
        /// Enable discount program
        /// </summary>
        public bool EnableDiscountProgram { get; set; }

        /// <summary>
        /// Discount percent
        /// </summary>
        public decimal DiscountPercent { get; set; }

        /// <summary>
        /// Door access enabled
        /// </summary>
        public bool DoorAccessEnabled { get; set; }

        /// <summary>
        /// Domain Id
        /// </summary>
        public string DomainId { get; set; }
    }
}
