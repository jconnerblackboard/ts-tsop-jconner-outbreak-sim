﻿
namespace BbTS.Domain.Models.Merchant
{
    /// <summary>
    /// Container class for handling a response to a get merchant authorization request.
    /// </summary>
    public class MerchantAuthorizationGetResponse
    {
        /// <summary>
        /// Gets or sets the Merchant Authorization.
        /// </summary>
        /// <value>
        /// The Merchant Authorization.
        /// </value>
        public MerchantAuthorization MerchantAuthorization { get; set; }
    }
}
