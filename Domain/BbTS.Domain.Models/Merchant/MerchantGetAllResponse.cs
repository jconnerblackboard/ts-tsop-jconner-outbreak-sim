﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Merchant
{
    /// <summary>
    /// Container class for handling a response to a get all merchant request.
    /// </summary>
    public class MerchantGetAllResponse
    {
        /// <summary>
        /// Merchants
        /// </summary>
        public List<TsMerchant> Merchants { get; set; }
    }
}
