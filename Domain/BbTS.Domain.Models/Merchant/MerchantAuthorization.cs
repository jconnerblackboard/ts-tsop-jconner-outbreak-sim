﻿using System;

namespace BbTS.Domain.Models.Merchant
{
    /// <summary>
    /// Merchant entity
    /// </summary>
    public class MerchantAuthorization
    {
        /// <summary>
        ///     Gets or sets the Id.
        /// </summary>
        /// <value>
        ///     The Id.
        /// </value>
        public Guid Id { get; set; }

        /// <summary>
        ///     Gets or sets the Name.
        /// </summary>
        /// <value>
        ///     The Name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the merchant key.
        /// </summary>
        /// <value>
        /// The merchant key.
        /// </value>
        public string MerchantKey { get; set; }
    }
}
