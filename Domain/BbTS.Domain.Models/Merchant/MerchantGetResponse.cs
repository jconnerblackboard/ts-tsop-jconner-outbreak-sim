﻿using System;

namespace BbTS.Domain.Models.Merchant
{
    /// <summary>
    /// Container class for handling a response to a get merchant request.
    /// </summary>
    public class MerchantGetResponse
    {
        /// <summary>
        /// Requested guid
        /// </summary>
        public Guid MerchantGuid { get; set; }

        /// <summary>
        /// Merchant
        /// </summary>
        public TsMerchant Merchant { get; set; }
    }
}
