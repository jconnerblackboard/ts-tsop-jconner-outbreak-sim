﻿using System;

namespace BbTS.Domain.Models.Merchant
{
    /// <summary>
    /// Merchant device settings
    /// </summary>
    public class MerchantDeviceSettings
    {
        /// <summary>
        /// Requested guid
        /// </summary>
        public Guid MerchantGuid { get; set; }

        /// <summary>
        /// Merchant
        /// </summary>
        public string MerchantName { get; set; }
    }

    /// <summary>
    /// View for a <see cref="MerchantDeviceSettings"/>.  (Version 1)
    /// </summary>
    public class MerchantDeviceSettingsViewV01
    {
        /// <summary>
        /// Requested guid
        /// </summary>
        public Guid MerchantGuid { get; set; }

        /// <summary>
        /// Merchant
        /// </summary>
        public string MerchantName { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="MerchantDeviceSettings"/> conversion.
    /// </summary>
    public static class MerchantDeviceSettingsConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="MerchantDeviceSettingsViewV01"/> object based on this <see cref="MerchantDeviceSettings"/>.
        /// </summary>
        /// <param name="merchantDeviceSettings"></param>
        /// <returns></returns>
        public static MerchantDeviceSettingsViewV01 ToMerchantDeviceSettingsViewV01(this MerchantDeviceSettings merchantDeviceSettings)
        {
            if (merchantDeviceSettings == null) return null;

            return new MerchantDeviceSettingsViewV01
            {
                MerchantGuid = merchantDeviceSettings.MerchantGuid,
                MerchantName = merchantDeviceSettings.MerchantName
            };
        }

        /// <summary>
        /// Returns a <see cref="MerchantDeviceSettings"/> object based on this <see cref="MerchantDeviceSettingsViewV01"/>.
        /// </summary>
        /// <param name="merchantDeviceSettingsViewV01"></param>
        /// <returns></returns>
        public static MerchantDeviceSettings ToMerchantDeviceSettings(this MerchantDeviceSettingsViewV01 merchantDeviceSettingsViewV01)
        {
            if (merchantDeviceSettingsViewV01 == null) return null;

            return new MerchantDeviceSettings
            {
                MerchantGuid = merchantDeviceSettingsViewV01.MerchantGuid,
                MerchantName = merchantDeviceSettingsViewV01.MerchantName
            };
        }
        #endregion
    }
}
