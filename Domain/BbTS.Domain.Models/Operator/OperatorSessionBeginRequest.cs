﻿using BbTS.Domain.Models.Container;

namespace BbTS.Domain.Models.Operator
{
    /// <summary>
    /// Container class for an operator session begin request.
    /// </summary>
    public class OperatorSessionBeginRequest
    {
        /// <summary>
        /// Unique ID assigned to the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The transaction.
        /// </summary>
        public TransactionViewV01 Transaction { get; set; }
    }
}
