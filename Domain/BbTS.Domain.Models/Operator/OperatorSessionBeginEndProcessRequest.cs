﻿using System;
using BbTS.Domain.Models.Transaction.Processing;

namespace BbTS.Domain.Models.Operator
{
    /// <summary>
    /// Container class for a resource layer request to process an operator session begin or end.
    /// </summary>
    public class OperatorSessionBeginEndProcessRequest : IProcessingRequest
    {
        /// <summary>
        /// The id of the request that spawned this response.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The unique identifier for the device where the transaction was performed.
        /// </summary>
        public string DeviceGuid { get; set; }

        /// <summary>
        /// The unique identifier for the operator of the device that initiated the transaction.
        /// </summary>
        public string OperatorGuid { get; set; }

        /// <summary>
        /// The cash drawer number the operator is assigned to.
        /// </summary>
        public int CashDrawerNumber { get; set; }

        /// <summary>
        /// The numerical identifier for the transaciton.
        /// </summary>
        public int TransactionNumber { get; set; }

        /// <summary>
        /// The date and time the transaction was performed.
        /// </summary>
        public DateTime TransactionDateTime { get; set; }

        /// <summary>
        /// Indicated whether or not the transaction was performed offline.
        /// </summary>
        public bool KeyedOfflineFlag { get; set; }

        /// <summary>
        /// The unique identifier for the operator session.
        /// </summary>
        public string SessionGuid { get; set; }
    }
}
