﻿using BbTS.Domain.Models.Container;

namespace BbTS.Domain.Models.Operator
{
    /// <summary>
    /// Container class for a operator session end request.
    /// </summary>
    public class OperatorSessionEndRequest
    {
        /// <summary>
        /// The id of the request that spawned this response.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The transaction containing the information needed to process the request.
        /// </summary>
        public TransactionViewV01 Transaction { get; set; }
    }
}
