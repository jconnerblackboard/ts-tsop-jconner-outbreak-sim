﻿using System;
using System.Collections.Generic;
using BbTS.Domain.Models.Definitions.Container;
using BbTS.Domain.Models.Definitions.Operator;

namespace BbTS.Domain.Models.Operator
{
    /// <summary>
    /// This object represents a Transact Cashier object. Cashiers
    /// </summary>
    public class TsOperator
    {
        /// <summary>
        /// Operator Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Operator Guid
        /// </summary>
        public Guid Guid { get; set; }

        /// <summary>
        /// Merchant ID for the cashier
        /// </summary>
        public Int16 MerchantId { get; set; }

        /// <summary>
        /// Cashier Number
        /// </summary>
        public int OperatorNumber { get; set; }

        /// <summary>
        /// Indicates if the cashier is active
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Pin number
        /// </summary>
        public Int16? Pin { get; set; }

        /// <summary>
        /// Cashier Name
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// T means cashier has manager override privilege
        /// </summary>
        public bool Manager { get; set; }

        /// <summary>
        /// Cashier Drawer Number
        /// </summary>
        public CashDrawerNumber CashDrawer { get; set; }

        /// <summary>
        /// Customer Id
        /// </summary>
        public int? CustomerId { get; set; }

        /// <summary>
        /// Pin required
        /// </summary>
        public bool PinRequired { get; set; }

        /// <summary>
        /// Pin change required
        /// </summary>
        public bool PinChangeRequired { get; set; }

        /// <summary>
        /// Operator rights
        /// </summary>
        public List<OperatorRight> Rights { get; set; }
    }
}
