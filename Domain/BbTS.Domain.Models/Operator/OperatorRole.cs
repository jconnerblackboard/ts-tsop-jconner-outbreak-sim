﻿using System.Collections.Generic;
using BbTS.Domain.Models.Definitions.Operator;

namespace BbTS.Domain.Models.Operator
{
    /// <summary>
    /// Container class defining an operator role.
    /// </summary>
    public class OperatorRole
    {
        /// <summary>
        /// Unique identifier for the role.
        /// </summary>
        public string RoleId { get; set; }

        /// <summary>
        /// List of rights assigned to the role.
        /// </summary>
        public List<OperatorRight> Rights { get; set; } = new List<OperatorRight>();
    }

    /// <summary>
    /// View for a <see cref="OperatorRole"/>.  (Version 1)
    /// </summary>
    public class OperatorRoleViewV01
    {
        /// <summary>
        /// Unique identifier for the role.
        /// </summary>
        public string RoleId { get; set; }

        /// <summary>
        /// List of rights assigned to the role.
        /// </summary>
        public List<OperatorRight> Rights { get; set; } = new List<OperatorRight>();
    }

    /// <summary>
    /// Extension methods for <see cref="OperatorRole"/> conversion.
    /// </summary>
    public static class OperatorRoleConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="OperatorRoleViewV01"/> object based on this <see cref="OperatorRole"/>.
        /// </summary>
        /// <param name="operatorRole"></param>
        /// <returns></returns>
        public static OperatorRoleViewV01 ToOperatorRoleViewV01(this OperatorRole operatorRole)
        {
            if (operatorRole == null) return null;

            return new OperatorRoleViewV01
            {
                RoleId = operatorRole.RoleId,
                Rights = operatorRole.Rights
            };
        }

        /// <summary>
        /// Returns a <see cref="OperatorRole"/> object based on this <see cref="OperatorRoleViewV01"/>.
        /// </summary>
        /// <param name="operatorRoleViewV01"></param>
        /// <returns></returns>
        public static OperatorRole ToOperatorRole(this OperatorRoleViewV01 operatorRoleViewV01)
        {
            if (operatorRoleViewV01 == null) return null;

            return new OperatorRole
            {
                RoleId = operatorRoleViewV01.RoleId,
                Rights = operatorRoleViewV01.Rights
            };
        }
        #endregion
    }
}
