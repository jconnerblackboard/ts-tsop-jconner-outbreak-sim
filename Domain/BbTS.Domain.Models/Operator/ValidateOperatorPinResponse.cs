﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Operator
{
    /// <summary>
    /// Container class to hold a operator pin validation response.
    /// </summary>
    public class ValidateOperatorPinResponse
    {
        /// <summary>
        /// Unique ID assigned to the request.
        /// </summary>
        public string RequestId { get; internal set; }

        /// <summary>
        /// Result of the validation operation.
        /// </summary>
        public bool IsValid { get; set; }

        /// <summary>
        /// Indicates whether or not a PIN change is required.
        /// </summary>
        public bool PinChangeRequired { get; set; }

        /// <summary>
        /// Human readable message stating the reason for an IsValid = false flag.
        /// </summary>
        public string Reason { get; set; }

        /// <summary>
        /// Parameterized constructor requiring the request id.
        /// </summary>
        /// <param name="requestId">Unique ID assigned to the request.</param>
        public ValidateOperatorPinResponse(string requestId)
        {
            RequestId = requestId;
        }
    }
}
