﻿using BbTS.Domain.Models.Transaction;

namespace BbTS.Domain.Models.Operator
{
    /// <summary>
    /// Container class for the response to a resource layer request to process an operator session begin or end.
    /// </summary>
    public class OperatorSessionBeginEndProcessResponse : ProcessingResult
    {
    }
}
