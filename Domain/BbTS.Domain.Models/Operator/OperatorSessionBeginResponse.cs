﻿using BbTS.Domain.Models.Transaction;

namespace BbTS.Domain.Models.Operator
{
    /// <summary>
    /// Container class for the response to an operator session begin request.
    /// </summary>
    public class OperatorSessionBeginResponse : ProcessingResult
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        public OperatorSessionBeginResponse()
        {
        }

        /// <summary>
        /// Parameterized constructor taking a processing result.
        /// </summary>
        /// <param name="result"><see cref="ProcessingResult"/></param>
        public OperatorSessionBeginResponse(ProcessingResult result)
        {
            RequestId = result.RequestId;
            ErrorCode = result.ErrorCode;
            DeniedText = result.DeniedText;
        }
    }
}
