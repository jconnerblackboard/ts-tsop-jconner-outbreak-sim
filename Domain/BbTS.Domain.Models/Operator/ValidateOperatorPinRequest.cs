﻿namespace BbTS.Domain.Models.Operator
{
    /// <summary>
    /// Container class to hold information required for a validate operator PIN operation.
    /// </summary>
    public class ValidateOperatorPinRequest
    {
        /// <summary>
        /// Unique ID assigned to the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The unique identifier for the operator (Guid).
        /// </summary>
        public string OperatorId { get; set; }

        /// <summary>
        /// The Operator's PIN.
        /// </summary>
        public string Pin { get; set; }
    }
}
