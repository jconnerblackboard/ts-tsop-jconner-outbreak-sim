﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Operator
{
    /// <summary>
    /// Operator get response
    /// </summary>
    public class OperatorGetResponse
    {
        /// <summary>
        /// Operators
        /// </summary>
        public List<TsOperator> Operators { get; set; }
    }
}
