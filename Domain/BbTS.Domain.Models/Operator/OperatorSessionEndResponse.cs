﻿using BbTS.Domain.Models.Transaction;

namespace BbTS.Domain.Models.Operator
{
    /// <summary>
    /// Container class for
    /// </summary>
    public class OperatorSessionEndResponse : ProcessingResult
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        public OperatorSessionEndResponse()
        {
        }

        /// <summary>
        /// Parameterized constructor taking a processing result.
        /// </summary>
        /// <param name="result"><see cref="ProcessingResult"/></param>
        public OperatorSessionEndResponse(ProcessingResult result)
        {
            RequestId = result.RequestId;
            ErrorCode = result.ErrorCode;
            DeniedText = result.DeniedText;
        }
    }
}
