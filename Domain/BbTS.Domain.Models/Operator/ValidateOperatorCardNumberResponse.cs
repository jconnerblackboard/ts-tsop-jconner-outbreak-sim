﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Operator
{
    /// <summary>
    /// Container class for handling the response to a validate operator card number request.
    /// </summary>
    public class ValidateOperatorCardNumberResponse
    {
        /// <summary>
        /// Unique ID assigned to the request.
        /// </summary>
        public string RequestId { get; internal set; }

        /// <summary>
        /// Result of the validation operation.
        /// </summary>
        public bool IsValid { get; set; }

        /// <summary>
        /// Parameterized constructor requiring the request id.
        /// </summary>
        /// <param name="requestId">Unique ID assigned to the request.</param>
        public ValidateOperatorCardNumberResponse(string requestId)
        {
            RequestId = requestId;
        }
    }
}
