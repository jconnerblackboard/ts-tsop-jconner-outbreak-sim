﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Operator
{
    /// <summary>
    /// Container class for handling a validate operator card number request.
    /// </summary>
    public class ValidateOperatorCardNumberRequest
    {
        /// <summary>
        /// Unique ID assigned to the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The card number to validate.
        /// </summary>
        public string CardNumber { get; set; }

        /// <summary>
        /// The unique identifier for the operator (Guid).
        /// </summary>
        public string OperatorId { get; set; }
    }
}
