﻿using BbTS.Domain.Models.Definitions.Card;
using System;

namespace BbTS.Domain.Models.Operator
{
    /// <summary>
    /// Container class containing information about an operator of a device.
    /// </summary>
    public class Operator
    {
        /// <summary>
        /// The unique identifier for the operator (GUID).
        /// </summary>
        public string OperatorId { get; set; }

        /// <summary>
        /// The number for the operator (used for login purposes).
        /// </summary>
        public int OperatorNumber { get; set; }

        /// <summary>
        /// The full name of the operator.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// The PIN for the operator.
        /// </summary>
        public string Pin { get; set; }

        /// <summary>
        /// True means the operator has manager override privilege.
        /// </summary>
        public bool IsManager { get; set; }

        /// <summary>
        /// The operator`s drawer number.
        /// </summary>
        public int CashDrawer { get; set; }

        /// <summary>
        /// Is the operator`s pin required for login.
        /// </summary>
        public bool PinRequired { get; set; }

        /// <summary>
        /// Indicates whether or not the operator has to change their pin.
        /// </summary>
        public bool PinChangeRequired { get; set; }

        /// <summary>
        /// /The operator`s card number.
        /// </summary>
        public string CardNumber { get; set; }

        /// <summary>
        /// The operator`s card issue number.
        /// </summary>
        public string IssueNumber { get; set; }

        /// <summary>
        /// The role group the cashier belongs to.
        /// </summary>
        public string RoleGroup { get; set; }

        /// <summary>
        /// Card status
        /// </summary>
        public CardStatusType? CardStatus { get; set; }

        /// <summary>
        /// Card active start date
        /// </summary>
        public DateTime? CardActiveStartDate { get; set; }

        /// <summary>
        /// Card active end date
        /// </summary>
        public DateTime? CardActiveEndDate { get; set; }

        /// <summary>
        /// Card lost flag
        /// </summary>
        public bool? CardLost { get; set; }
    }

    /// <summary>
    /// View for a <see cref="Operator"/>.  (Version 1)
    /// </summary>
    public class OperatorViewV01
    {
        /// <summary>
        /// The unique identifier for the operator (GUID).
        /// </summary>
        public string OperatorId { get; set; }

        /// <summary>
        /// The number for the operator (used for login purposes).
        /// </summary>
        public int OperatorNumber { get; set; }

        /// <summary>
        /// The full name of the operator.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// The PIN for the operator.
        /// </summary>
        public string Pin { get; set; }

        /// <summary>
        /// True means the operator has manager override privilege.
        /// </summary>
        public bool IsManager { get; set; }

        /// <summary>
        /// The operator`s drawer number.
        /// </summary>
        public int CashDrawer { get; set; }

        /// <summary>
        /// Is the operator`s pin required for login.
        /// </summary>
        public bool PinRequired { get; set; }

        /// <summary>
        /// Indicates whether or not the operator has to change their pin.
        /// </summary>
        public bool PinChangeRequired { get; set; }

        /// <summary>
        /// /The operator`s card number.
        /// </summary>
        public string CardNumber { get; set; }

        /// <summary>
        /// The operator`s card issue number.
        /// </summary>
        public string IssueNumber { get; set; }

        /// <summary>
        /// The role group the cashier belongs to.
        /// </summary>
        public string RoleGroup { get; set; }

        /// <summary>
        /// Card status
        /// </summary>
        public CardStatusType? CardStatus { get; set; }

        /// <summary>
        /// Card active start date
        /// </summary>
        public DateTime? CardActiveStartDate { get; set; }

        /// <summary>
        /// Card active end date
        /// </summary>
        public DateTime? CardActiveEndDate { get; set; }

        /// <summary>
        /// Card lost flag
        /// </summary>
        public bool? CardLost { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="Operator"/> conversion.
    /// </summary>
    public static class OperatorConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="OperatorViewV01"/> object based on this <see cref="Operator"/>.
        /// </summary>
        /// <param name="deviceOperator"></param>
        /// <returns></returns>
        public static OperatorViewV01 ToOperatorViewV01(this Operator deviceOperator)
        {
            if (deviceOperator == null) return null;

            return new OperatorViewV01
            {
                OperatorId = deviceOperator.OperatorId,
                OperatorNumber = deviceOperator.OperatorNumber,
                FullName = deviceOperator.FullName,
                Pin = deviceOperator.Pin,
                IsManager = deviceOperator.IsManager,
                CashDrawer = deviceOperator.CashDrawer,
                PinRequired = deviceOperator.PinRequired,
                PinChangeRequired = deviceOperator.PinChangeRequired,
                CardNumber = deviceOperator.CardNumber,
                IssueNumber = deviceOperator.IssueNumber,
                RoleGroup = deviceOperator.RoleGroup,
                CardStatus = deviceOperator.CardStatus,
                CardActiveStartDate = deviceOperator.CardActiveStartDate,
                CardActiveEndDate = deviceOperator.CardActiveEndDate,
                CardLost = deviceOperator.CardLost
            };
        }

        /// <summary>
        /// Returns a <see cref="Operator"/> object based on this <see cref="OperatorViewV01"/>.
        /// </summary>
        /// <param name="deviceOperatorViewV01"></param>
        /// <returns></returns>
        public static Operator ToOperator(this OperatorViewV01 deviceOperatorViewV01)
        {
            if (deviceOperatorViewV01 == null) return null;

            return new Operator
            {
                OperatorId = deviceOperatorViewV01.OperatorId,
                OperatorNumber = deviceOperatorViewV01.OperatorNumber,
                FullName = deviceOperatorViewV01.FullName,
                Pin = deviceOperatorViewV01.Pin,
                IsManager = deviceOperatorViewV01.IsManager,
                CashDrawer = deviceOperatorViewV01.CashDrawer,
                PinRequired = deviceOperatorViewV01.PinRequired,
                PinChangeRequired = deviceOperatorViewV01.PinChangeRequired,
                CardNumber = deviceOperatorViewV01.CardNumber,
                IssueNumber = deviceOperatorViewV01.IssueNumber,
                RoleGroup = deviceOperatorViewV01.RoleGroup,
                CardStatus = deviceOperatorViewV01.CardStatus,
                CardActiveStartDate = deviceOperatorViewV01.CardActiveStartDate,
                CardActiveEndDate = deviceOperatorViewV01.CardActiveEndDate,
                CardLost = deviceOperatorViewV01.CardLost
            };
        }
        #endregion
    }
}
