﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Operator
{
    /// <summary>
    /// Container class to handle the response from an operator change pin request.
    /// </summary>
    public class OperatorChangePinResponse
    {
        /// <summary>
        /// Unique ID assigned to the request.
        /// </summary>
        public string RequestId { get; internal set; }

        /// <summary>
        /// Parameterized constructor requiring the request id.
        /// </summary>
        /// <param name="requestId">Unique ID assigned to the request.</param>
        public OperatorChangePinResponse(string requestId)
        {
            RequestId = requestId;
        }
    }
}
