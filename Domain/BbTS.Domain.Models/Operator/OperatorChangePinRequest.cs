﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Operator
{
    /// <summary>
    /// Container class to handle operator change pin requests.
    /// </summary>
    public class OperatorChangePinRequest
    {
        /// <summary>
        /// Unique ID assigned to the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The unique identifier for the operator (Guid).
        /// </summary>
        public string OperatorId { get; set; }

        /// <summary>
        /// The Operator's Current (old) PIN.
        /// </summary>
        public string OldPin { get; set; }

        /// <summary>
        /// The Operator's new PIN.
        /// </summary>
        public string NewPin { get; set; }
    }
}
