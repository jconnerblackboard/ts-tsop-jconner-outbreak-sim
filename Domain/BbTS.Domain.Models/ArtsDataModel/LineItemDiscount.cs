﻿using BbTS.Domain.Models.Definitions.ArtsDataModel;

namespace BbTS.Domain.Models.ArtsDataModel
{
    /// <summary>
    /// Line item discount.
    /// </summary>
    public class LineItemDiscount : LineItem
    {
        /// <summary>
        /// Key number.
        /// </summary>
        public int KeyNumber { get; set; }

        /// <summary>
        /// Discount key name.
        /// </summary>
        public string DiscountKeyName { get; set; }

        /// <summary>
        /// Percentage.
        /// </summary>
        public decimal Percentage { get; set; }

        /// <summary>
        /// Rounding amount.
        /// </summary>
        public decimal RoundingAmount { get; set; }

        /// <summary>
        /// Amount.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Prorated flag.
        /// </summary>
        public bool ProratedFlag { get; set; }

        /// <summary>
        /// Calculation type.
        /// </summary>
        public CalculationType CalculationType { get; set; }

        /// <inheritdoc />
        public override LineItem Clone()
        {
            return (LineItemDiscount)MemberwiseClone();
        }
    }

    /// <summary>
    /// View for a <see cref="LineItemDiscount"/>.  (Version 1)
    /// </summary>
    public class LineItemDiscountViewV01
    {
        /// <summary>
        /// Indicates the order that this line item was added to the list and serves as a unique identifier for the line item.  Consider this a corrected LineItemSequenceNumber implementation.  Starts at 1.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Legacy sequence implementation.  Some incorrect implementation details prevent this from being used as a true sequence number, as is intended by the ARTS model.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// True if this line item has been voided.
        /// </summary>
        public bool VoidFlag { get; set; }

        /// <summary>
        /// Key number.
        /// </summary>
        public int KeyNumber { get; set; }

        /// <summary>
        /// Discount key name.
        /// </summary>
        public string DiscountKeyName { get; set; }

        /// <summary>
        /// Percentage.
        /// </summary>
        public decimal Percentage { get; set; }

        /// <summary>
        /// Rounding amount.
        /// </summary>
        public decimal RoundingAmount { get; set; }

        /// <summary>
        /// Amount.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Prorated flag.
        /// </summary>
        public bool ProratedFlag { get; set; }

        /// <summary>
        /// Calculation type.
        /// </summary>
        public CalculationType CalculationType { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="LineItemDiscount"/> conversion.
    /// </summary>
    public static class LineItemDiscountConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="LineItemDiscountViewV01"/> object based on this <see cref="LineItemDiscount"/>.
        /// </summary>
        /// <param name="lineItemDiscount"></param>
        /// <returns></returns>
        public static LineItemDiscountViewV01 ToLineItemDiscountViewV01(this LineItemDiscount lineItemDiscount)
        {
            if (lineItemDiscount == null) return null;

            return new LineItemDiscountViewV01
            {
                Sequence = lineItemDiscount.Sequence,
                LineItemSequenceNumber = lineItemDiscount.LineItemSequenceNumber,
                VoidFlag = lineItemDiscount.VoidFlag,
                KeyNumber = lineItemDiscount.KeyNumber,
                DiscountKeyName = lineItemDiscount.DiscountKeyName,
                Percentage = lineItemDiscount.Percentage,
                RoundingAmount = lineItemDiscount.RoundingAmount,
                Amount = lineItemDiscount.Amount,
                ProratedFlag = lineItemDiscount.ProratedFlag,
                CalculationType = lineItemDiscount.CalculationType
            };
        }

        /// <summary>
        /// Returns a <see cref="LineItemDiscount"/> object based on this <see cref="LineItemDiscountViewV01"/>.
        /// </summary>
        /// <param name="lineItemDiscountViewV01"></param>
        /// <returns></returns>
        public static LineItemDiscount ToLineItemDiscount(this LineItemDiscountViewV01 lineItemDiscountViewV01)
        {
            if (lineItemDiscountViewV01 == null) return null;

            return new LineItemDiscount
            {
                Sequence = lineItemDiscountViewV01.Sequence,
                LineItemSequenceNumber = lineItemDiscountViewV01.LineItemSequenceNumber,
                VoidFlag = lineItemDiscountViewV01.VoidFlag,
                KeyNumber = lineItemDiscountViewV01.KeyNumber,
                DiscountKeyName = lineItemDiscountViewV01.DiscountKeyName,
                Percentage = lineItemDiscountViewV01.Percentage,
                RoundingAmount = lineItemDiscountViewV01.RoundingAmount,
                Amount = lineItemDiscountViewV01.Amount,
                ProratedFlag = lineItemDiscountViewV01.ProratedFlag,
                CalculationType = lineItemDiscountViewV01.CalculationType
            };
        }
        #endregion
    }
}
