﻿using BbTS.Domain.Models.Definitions.ArtsDataModel;

namespace BbTS.Domain.Models.ArtsDataModel
{
    /// <summary>
    /// Arts line item product.
    /// </summary>
    public class LineItemProduct : LineItem
    {
        /// <summary>
        /// Product detail ID.
        /// </summary>
        public int ProductDetailId { get; set; }

        /// <summary>
        /// Product description.
        /// </summary>
        public string ProductDescription { get; set; }

        /// <summary>
        /// Tax schedule ID.
        /// </summary>
        public int TaxScheduleId { get; set; }

        /// <summary>
        /// Tax account name.
        /// </summary>
        public string TaxAccountName { get; set; }

        /// <summary>
        /// Tax group ID.
        /// </summary>
        public int? TaxGroupId { get; set; }

        /// <summary>
        /// Retail Price of Product.  The regular or lookup per-unit price for the item before any discounts, etc have been applied.
        /// </summary>
        public decimal RetailPrice { get; set; }

        /// <summary>
        /// Retail price quantity.
        /// </summary>
        public int RetailPriceQuantity { get; set; }

        /// <summary>
        /// Actual Unit Price.  The actual per-unit price paid by the customer for this particular sale. It is obtained by applying applicable price derivation rules to the regular unit price (retailprice).
        /// </summary>
        public decimal ActualUnitPrice { get; set; }

        /// <summary>
        /// Actual unit price quantity.
        /// </summary>
        public int ActualUnitPriceQuantity { get; set; }

        /// <summary>
        /// Quantity.
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Units Amount.  The number of units sold, when selling bulk merchandise.  Eg: A sale of 20 Gallons of Gas: Quantity=20, Units=1, UnitOfMeasure=Ga Eg: A sale of 3 cans of Beans: Quantity=3, Units=3, UnitOfMeasure=Ea
        /// </summary>
        public int Units { get; set; }

        /// <summary>
        /// 0 - Count, 1 - Weight(Pounds)
        /// </summary>
        public UnitMeasureType UnitMeasureType { get; set; }

        /// <summary>
        /// Extended Amount of Line Item.  The product of multiplying Quantity by the retail selling unit price derived from price lookup (and any applicable price derivation rules). This retail sale unit price excludes sales and/or value added tax.
        /// </summary>
        public decimal ExtendedAmount { get; set; }

        /// <summary>
        /// Unit discount amount.
        /// </summary>
        public decimal UnitDiscountAmount { get; set; }

        /// <summary>
        /// Extended discount amount.
        /// </summary>
        public decimal ExtendedDiscountAmount { get; set; }

        /// <summary>
        /// Product entry method type.
        /// </summary>
        public ProductEntryMethodType ProductEntryMethodType { get; set; }

        /// <summary>
        /// Retail price entry method type.
        /// </summary>
        public RetailPriceEntryMethodType RetailPriceEntryMethodType { get; set; }

        /// <summary>
        /// Unit list price.
        /// </summary>
        public decimal UnitListPrice { get; set; }

        /// <summary>
        /// Unit list price quantity.
        /// </summary>
        public int UnitListPriceQuantity { get; set; }

        /// <summary>
        /// Product promotion flag.
        /// </summary>
        public bool ProductPromotionFlag { get; set; }

        /// <summary>
        /// Product promotion price.
        /// </summary>
        public decimal ProductPromotionPrice { get; set; }

        /// <summary>
        /// Product promotion ID.
        /// </summary>
        public int ProductPromotionId { get; set; }

        /// <summary>
        /// Retail price modified flag.  This should always be set to true for <see cref="Definitions.ArtsDataModel.RetailPriceEntryMethodType.ManuallyEntered"/> products.
        /// </summary>
        public bool RetailPriceModifiedFlag { get; set; }

        //I believe that everything below here is not used
        //public bool RetailPriceModifiedButOnlyBecauseOfPromptForPrice { get; set; }
        //public List<LineItemProductTax> ProductTaxList { get; set; }
        //public List<LineItemProductPriceModifier> ProductPriceModifierList { get; set; }
        //public bool ForcePrint { get; set; }
        //public List<ProductDiscountInfo> ProductDiscountInfoList { get; set; }
        //public bool AllowPolicyDiscount { get; set; }
        //public bool AllowPolicySurcharge { get; set; }
        //public bool AllowDiscountSurchargeKeys { get; set; }
        //public List<int> DisallowedTenderIdList { get; set; } //Evaluate this when new C# business logic is written.  We might not need this attached to the product here.

        /// <summary>
        /// Creates a memberwise clone of this instance.
        /// </summary>
        /// <returns></returns>
        public override LineItem Clone()
        {
            return (LineItemProduct)MemberwiseClone();
        }
    }

    /// <summary>
    /// View for a <see cref="LineItemProduct"/>.  (Version 1)
    /// </summary>
    public class LineItemProductViewV01
    {
        /// <summary>
        /// Indicates the order that this line item was added to the list and serves as a unique identifier for the line item.  Consider this a corrected LineItemSequenceNumber implementation.  Starts at 1.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Legacy sequence implementation.  Some incorrect implementation details prevent this from being used as a true sequence number, as is intended by the ARTS model.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// True if this line item has been voided.
        /// </summary>
        public bool VoidFlag { get; set; }

        /// <summary>
        /// Product detail ID.
        /// </summary>
        public int ProductDetailId { get; set; }

        /// <summary>
        /// Product description.
        /// </summary>
        public string ProductDescription { get; set; }

        /// <summary>
        /// Tax schedule ID.
        /// </summary>
        public int TaxScheduleId { get; set; }

        /// <summary>
        /// Tax account name.
        /// </summary>
        public string TaxAccountName { get; set; }

        /// <summary>
        /// Tax group ID.
        /// </summary>
        public int? TaxGroupId { get; set; }

        /// <summary>
        /// Retail Price of Product.  The regular or lookup per-unit price for the item before any discounts, etc have been applied.
        /// </summary>
        public decimal RetailPrice { get; set; }

        /// <summary>
        /// Retail price quantity.
        /// </summary>
        public int RetailPriceQuantity { get; set; }

        /// <summary>
        /// Actual Unit Price.  The actual per-unit price paid by the customer for this particular sale. It is obtained by applying applicable price derivation rules to the regular unit price (retailprice).
        /// </summary>
        public decimal ActualUnitPrice { get; set; }

        /// <summary>
        /// Actual unit price quantity.
        /// </summary>
        public int ActualUnitPriceQuantity { get; set; }

        /// <summary>
        /// Quantity.
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Units Amount.  The number of units sold, when selling bulk merchandise.  Eg: A sale of 20 Gallons of Gas: Quantity=20, Units=1, UnitOfMeasure=Ga Eg: A sale of 3 cans of Beans: Quantity=3, Units=3, UnitOfMeasure=Ea
        /// </summary>
        public int Units { get; set; }

        /// <summary>
        /// 0 - Count, 1 - Weight(Pounds)
        /// </summary>
        public UnitMeasureType UnitMeasureType { get; set; }

        /// <summary>
        /// Extended Amount of Line Item.  The product of multiplying Quantity by the retail selling unit price derived from price lookup (and any applicable price derivation rules). This retail sale unit price excludes sales and/or value added tax.
        /// </summary>
        public decimal ExtendedAmount { get; set; }

        /// <summary>
        /// Unit discount amount.
        /// </summary>
        public decimal UnitDiscountAmount { get; set; }

        /// <summary>
        /// Extended discount amount.
        /// </summary>
        public decimal ExtendedDiscountAmount { get; set; }

        /// <summary>
        /// Product entry method type.
        /// </summary>
        public ProductEntryMethodType ProductEntryMethodType { get; set; }

        /// <summary>
        /// Retail price entry method type.
        /// </summary>
        public RetailPriceEntryMethodType RetailPriceEntryMethodType { get; set; }

        /// <summary>
        /// Unit list price.
        /// </summary>
        public decimal UnitListPrice { get; set; }

        /// <summary>
        /// Unit list price quantity.
        /// </summary>
        public int UnitListPriceQuantity { get; set; }

        /// <summary>
        /// Product promotion flag.
        /// </summary>
        public bool ProductPromotionFlag { get; set; }

        /// <summary>
        /// Product promotion price.
        /// </summary>
        public decimal ProductPromotionPrice { get; set; }

        /// <summary>
        /// Product promotion ID.
        /// </summary>
        public int ProductPromotionId { get; set; }

        /// <summary>
        /// Retail price modified flag.  This should always be set to true for <see cref="Definitions.ArtsDataModel.RetailPriceEntryMethodType.ManuallyEntered"/> products.
        /// </summary>
        public bool RetailPriceModifiedFlag { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="LineItemProduct"/> conversion.
    /// </summary>
    public static class LineItemProductConverter
    {
        /// <summary>
        /// Returns a <see cref="LineItemProductViewV01"/> object based on this <see cref="LineItemProduct"/>.
        /// </summary>
        /// <param name="lineItemProduct"></param>
        /// <returns></returns>
        public static LineItemProductViewV01 ToLineItemProductViewV01(this LineItemProduct lineItemProduct)
        {
            if (lineItemProduct == null) return null;

            return new LineItemProductViewV01
            {
                Sequence = lineItemProduct.Sequence,
                LineItemSequenceNumber = lineItemProduct.LineItemSequenceNumber,
                VoidFlag = lineItemProduct.VoidFlag,
                ProductDetailId = lineItemProduct.ProductDetailId,
                ProductDescription = lineItemProduct.ProductDescription,
                TaxScheduleId = lineItemProduct.TaxScheduleId,
                TaxAccountName = lineItemProduct.TaxAccountName,
                TaxGroupId = lineItemProduct.TaxGroupId,
                RetailPrice = lineItemProduct.RetailPrice,
                RetailPriceQuantity = lineItemProduct.RetailPriceQuantity,
                ActualUnitPrice = lineItemProduct.ActualUnitPrice,
                ActualUnitPriceQuantity = lineItemProduct.ActualUnitPriceQuantity,
                Quantity = lineItemProduct.Quantity,
                Units = lineItemProduct.Units,
                UnitMeasureType = lineItemProduct.UnitMeasureType,
                ExtendedAmount = lineItemProduct.ExtendedAmount,
                UnitDiscountAmount = lineItemProduct.UnitDiscountAmount,
                ExtendedDiscountAmount = lineItemProduct.ExtendedDiscountAmount,
                ProductEntryMethodType = lineItemProduct.ProductEntryMethodType,
                RetailPriceEntryMethodType = lineItemProduct.RetailPriceEntryMethodType,
                UnitListPrice = lineItemProduct.UnitListPrice,
                UnitListPriceQuantity = lineItemProduct.UnitListPriceQuantity,
                ProductPromotionFlag = lineItemProduct.ProductPromotionFlag,
                ProductPromotionPrice = lineItemProduct.ProductPromotionPrice,
                ProductPromotionId = lineItemProduct.ProductPromotionId,
                RetailPriceModifiedFlag = lineItemProduct.RetailPriceModifiedFlag
            };
        }

        /// <summary>
        /// Returns a <see cref="LineItemProduct"/> object based on this <see cref="LineItemProductViewV01"/>.
        /// </summary>
        /// <param name="lineItemProductViewV01"></param>
        /// <returns></returns>
        public static LineItemProduct ToLineItemProduct(this LineItemProductViewV01 lineItemProductViewV01)
        {
            if (lineItemProductViewV01 == null) return null;

            return new LineItemProduct
            {
                Sequence = lineItemProductViewV01.Sequence,
                LineItemSequenceNumber = lineItemProductViewV01.LineItemSequenceNumber,
                VoidFlag = lineItemProductViewV01.VoidFlag,
                ProductDetailId = lineItemProductViewV01.ProductDetailId,
                ProductDescription = lineItemProductViewV01.ProductDescription,
                TaxScheduleId = lineItemProductViewV01.TaxScheduleId,
                TaxAccountName = lineItemProductViewV01.TaxAccountName,
                TaxGroupId = lineItemProductViewV01.TaxGroupId,
                RetailPrice = lineItemProductViewV01.RetailPrice,
                RetailPriceQuantity = lineItemProductViewV01.RetailPriceQuantity,
                ActualUnitPrice = lineItemProductViewV01.ActualUnitPrice,
                ActualUnitPriceQuantity = lineItemProductViewV01.ActualUnitPriceQuantity,
                Quantity = lineItemProductViewV01.Quantity,
                Units = lineItemProductViewV01.Units,
                UnitMeasureType = lineItemProductViewV01.UnitMeasureType,
                ExtendedAmount = lineItemProductViewV01.ExtendedAmount,
                UnitDiscountAmount = lineItemProductViewV01.UnitDiscountAmount,
                ExtendedDiscountAmount = lineItemProductViewV01.ExtendedDiscountAmount,
                ProductEntryMethodType = lineItemProductViewV01.ProductEntryMethodType,
                RetailPriceEntryMethodType = lineItemProductViewV01.RetailPriceEntryMethodType,
                UnitListPrice = lineItemProductViewV01.UnitListPrice,
                UnitListPriceQuantity = lineItemProductViewV01.UnitListPriceQuantity,
                ProductPromotionFlag = lineItemProductViewV01.ProductPromotionFlag,
                ProductPromotionPrice = lineItemProductViewV01.ProductPromotionPrice,
                ProductPromotionId = lineItemProductViewV01.ProductPromotionId,
                RetailPriceModifiedFlag = lineItemProductViewV01.RetailPriceModifiedFlag
            };
        }
    }
}
