﻿using BbTS.Domain.Models.Definitions.ArtsDataModel;

namespace BbTS.Domain.Models.ArtsDataModel
{
    /// <summary>
    /// Line item surcharge.
    /// </summary>
    public class LineItemSurcharge : LineItem
    {
        /// <summary>
        /// Key number.
        /// </summary>
        public int KeyNumber { get; set; }

        /// <summary>
        /// Surcharge key name.
        /// </summary>
        public string SurchargeKeyName { get; set; }

        /// <summary>
        /// Percentage.
        /// </summary>
        public decimal Percentage { get; set; }

        /// <summary>
        /// Rounding amount.
        /// </summary>
        public decimal RoundingAmount { get; set; }

        /// <summary>
        /// Amount.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Prorated flag.
        /// </summary>
        public bool ProratedFlag { get; set; }

        /// <summary>
        /// Calculation type.
        /// </summary>
        public CalculationType CalculationType { get; set; }

        /// <inheritdoc />
        public override LineItem Clone()
        {
            return (LineItemSurcharge)MemberwiseClone();
        }
    }

    /// <summary>
    /// View for a <see cref="LineItemSurcharge"/>.  (Version 1)
    /// </summary>
    public class LineItemSurchargeViewV01
    {
        /// <summary>
        /// Indicates the order that this line item was added to the list and serves as a unique identifier for the line item.  Consider this a corrected LineItemSequenceNumber implementation.  Starts at 1.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Legacy sequence implementation.  Some incorrect implementation details prevent this from being used as a true sequence number, as is intended by the ARTS model.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// True if this line item has been voided.
        /// </summary>
        public bool VoidFlag { get; set; }

        /// <summary>
        /// Key number.
        /// </summary>
        public int KeyNumber { get; set; }

        /// <summary>
        /// Surcharge key name.
        /// </summary>
        public string SurchargeKeyName { get; set; }

        /// <summary>
        /// Percentage.
        /// </summary>
        public decimal Percentage { get; set; }

        /// <summary>
        /// Rounding amount.
        /// </summary>
        public decimal RoundingAmount { get; set; }

        /// <summary>
        /// Amount.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Prorated flag.
        /// </summary>
        public bool ProratedFlag { get; set; }

        /// <summary>
        /// Calculation type.
        /// </summary>
        public CalculationType CalculationType { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="LineItemSurcharge"/> conversion.
    /// </summary>
    public static class LineItemSurchargeConverter
    {
        /// <summary>
        /// Returns a <see cref="LineItemSurchargeViewV01"/> object based on this <see cref="LineItemSurcharge"/>.
        /// </summary>
        /// <param name="lineItemSurcharge"></param>
        /// <returns></returns>
        public static LineItemSurchargeViewV01 ToLineItemSurchargeViewV01(this LineItemSurcharge lineItemSurcharge)
        {
            if (lineItemSurcharge == null) return null;

            return new LineItemSurchargeViewV01
            {
                Sequence = lineItemSurcharge.Sequence,
                LineItemSequenceNumber = lineItemSurcharge.LineItemSequenceNumber,
                VoidFlag = lineItemSurcharge.VoidFlag,
                KeyNumber = lineItemSurcharge.KeyNumber,
                SurchargeKeyName = lineItemSurcharge.SurchargeKeyName,
                Percentage = lineItemSurcharge.Percentage,
                RoundingAmount = lineItemSurcharge.RoundingAmount,
                Amount = lineItemSurcharge.Amount,
                ProratedFlag = lineItemSurcharge.ProratedFlag,
                CalculationType = lineItemSurcharge.CalculationType
            };
        }

        /// <summary>
        /// Returns a <see cref="LineItemSurcharge"/> object based on this <see cref="LineItemSurchargeViewV01"/>.
        /// </summary>
        /// <param name="lineItemSurchargeViewV01"></param>
        /// <returns></returns>
        public static LineItemSurcharge ToLineItemSurcharge(this LineItemSurchargeViewV01 lineItemSurchargeViewV01)
        {
            if (lineItemSurchargeViewV01 == null) return null;

            return new LineItemSurcharge
            {
                Sequence = lineItemSurchargeViewV01.Sequence,
                LineItemSequenceNumber = lineItemSurchargeViewV01.LineItemSequenceNumber,
                VoidFlag = lineItemSurchargeViewV01.VoidFlag,
                KeyNumber = lineItemSurchargeViewV01.KeyNumber,
                SurchargeKeyName = lineItemSurchargeViewV01.SurchargeKeyName,
                Percentage = lineItemSurchargeViewV01.Percentage,
                RoundingAmount = lineItemSurchargeViewV01.RoundingAmount,
                Amount = lineItemSurchargeViewV01.Amount,
                ProratedFlag = lineItemSurchargeViewV01.ProratedFlag,
                CalculationType = lineItemSurchargeViewV01.CalculationType
            };
        }
    }
}
