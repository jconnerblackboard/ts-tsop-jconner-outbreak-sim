﻿using BbTS.Domain.Models.Definitions.ArtsDataModel;

namespace BbTS.Domain.Models.ArtsDataModel
{
    /// <summary>
    /// Line item product price modifier.
    /// </summary>
    public class LineItemProductPriceModifier : LineItem
    {
        //public int ProductSequenceNumber { get; set; }  // LineItemSequenceNumber of the product that this modifier belongs to

        /// <summary>
        /// Seqence number with respect to siblings
        /// </summary>
        public int PriceModifierSequenceNumber { get; set; }

        /// <summary>
        /// Product price modifier type.
        /// </summary>
        public ProductPriceModifierType Type { get; set; }

        /// <summary>
        /// Indicates if there was a product promotion at the time of the transaction.
        /// </summary>
        public bool ProductPromoFlag { get; set; }

        /// <summary>
        /// Product promotion ID.
        /// </summary>
        public int ProductPromotionId { get; set; } //Always 0 unless Type == ProductPriceModifierType.Promotion

        /// <summary>
        /// Previous unit price.
        /// </summary>
        public decimal PreviousUnitPrice { get; set; }

        /// <summary>
        /// Percent.
        /// </summary>
        public decimal Percent { get; set; }

        /// <summary>
        /// Amount.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Calculation method.
        /// </summary>
        public ProductPriceModifierCalculationMethod CalculationMethod { get; set; }

        /// <summary>
        /// New unit price.
        /// </summary>
        public decimal NewUnitPrice { get; set; }

        /// <inheritdoc />
        public override LineItem Clone()
        {
            return (LineItemProductPriceModifier)MemberwiseClone();
        }
    }

    /// <summary>
    /// View for a <see cref="LineItemProductPriceModifier"/>.  (Version 1)
    /// </summary>
    public class LineItemProductPriceModifierViewV01
    {
        /// <summary>
        /// Indicates the order that this line item was added to the list and serves as a unique identifier for the line item.  Consider this a corrected LineItemSequenceNumber implementation.  Starts at 1.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Legacy sequence implementation.  Some incorrect implementation details prevent this from being used as a true sequence number, as is intended by the ARTS model.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// True if this line item has been voided.
        /// </summary>
        public bool VoidFlag { get; set; }

        /// <summary>
        /// Seqence number with respect to siblings
        /// </summary>
        public int PriceModifierSequenceNumber { get; set; }

        /// <summary>
        /// Product price modifier type.
        /// </summary>
        public ProductPriceModifierType Type { get; set; }

        /// <summary>
        /// Indicates if there was a product promotion at the time of the transaction.
        /// </summary>
        public bool ProductPromoFlag { get; set; }

        /// <summary>
        /// Product promotion ID.
        /// </summary>
        public int ProductPromotionId { get; set; } //Always 0 unless Type == ProductPriceModifierType.Promotion

        /// <summary>
        /// Previous unit price.
        /// </summary>
        public decimal PreviousUnitPrice { get; set; }

        /// <summary>
        /// Percent.
        /// </summary>
        public decimal Percent { get; set; }

        /// <summary>
        /// Amount.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Calculation method.
        /// </summary>
        public ProductPriceModifierCalculationMethod CalculationMethod { get; set; }

        /// <summary>
        /// New unit price.
        /// </summary>
        public decimal NewUnitPrice { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="LineItemProductPriceModifier"/> conversion.
    /// </summary>
    public static class LineItemProductPriceModifierConverter
    {
        /// <summary>
        /// Returns a <see cref="LineItemProductPriceModifierViewV01"/> object based on this <see cref="LineItemProductPriceModifier"/>.
        /// </summary>
        /// <param name="lineItemProductPriceModifier"></param>
        /// <returns></returns>
        public static LineItemProductPriceModifierViewV01 ToLineItemProductPriceModifierViewV01(this LineItemProductPriceModifier lineItemProductPriceModifier)
        {
            if (lineItemProductPriceModifier == null) return null;

            return new LineItemProductPriceModifierViewV01
            {
                Sequence = lineItemProductPriceModifier.Sequence,
                LineItemSequenceNumber = lineItemProductPriceModifier.LineItemSequenceNumber,
                VoidFlag = lineItemProductPriceModifier.VoidFlag,
                PriceModifierSequenceNumber = lineItemProductPriceModifier.PriceModifierSequenceNumber,
                Type = lineItemProductPriceModifier.Type,
                ProductPromoFlag = lineItemProductPriceModifier.ProductPromoFlag,
                ProductPromotionId = lineItemProductPriceModifier.ProductPromotionId,
                PreviousUnitPrice = lineItemProductPriceModifier.PreviousUnitPrice,
                Percent = lineItemProductPriceModifier.Percent,
                Amount = lineItemProductPriceModifier.Amount,
                CalculationMethod = lineItemProductPriceModifier.CalculationMethod,
                NewUnitPrice = lineItemProductPriceModifier.NewUnitPrice
            };
        }

        /// <summary>
        /// Returns a <see cref="LineItemProductPriceModifier"/> object based on this <see cref="LineItemProductPriceModifierViewV01"/>.
        /// </summary>
        /// <param name="lineItemProductPriceModifierViewV01"></param>
        /// <returns></returns>
        public static LineItemProductPriceModifier ToLineItemProductPriceModifier(this LineItemProductPriceModifierViewV01 lineItemProductPriceModifierViewV01)
        {
            if (lineItemProductPriceModifierViewV01 == null) return null;

            return new LineItemProductPriceModifier
            {
                Sequence = lineItemProductPriceModifierViewV01.Sequence,
                LineItemSequenceNumber = lineItemProductPriceModifierViewV01.LineItemSequenceNumber,
                VoidFlag = lineItemProductPriceModifierViewV01.VoidFlag,
                PriceModifierSequenceNumber = lineItemProductPriceModifierViewV01.PriceModifierSequenceNumber,
                Type = lineItemProductPriceModifierViewV01.Type,
                ProductPromoFlag = lineItemProductPriceModifierViewV01.ProductPromoFlag,
                ProductPromotionId = lineItemProductPriceModifierViewV01.ProductPromotionId,
                PreviousUnitPrice = lineItemProductPriceModifierViewV01.PreviousUnitPrice,
                Percent = lineItemProductPriceModifierViewV01.Percent,
                Amount = lineItemProductPriceModifierViewV01.Amount,
                CalculationMethod = lineItemProductPriceModifierViewV01.CalculationMethod,
                NewUnitPrice = lineItemProductPriceModifierViewV01.NewUnitPrice
            };
        }
    }
}
