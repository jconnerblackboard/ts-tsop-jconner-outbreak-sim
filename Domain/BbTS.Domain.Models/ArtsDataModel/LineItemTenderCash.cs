﻿using BbTS.Domain.Models.Definitions.Transaction;

namespace BbTS.Domain.Models.ArtsDataModel
{
    /// <summary>
    /// Container class for a line item tender cash.
    /// </summary>
    public class LineItemTenderCash : LineItemTender
    {
        /// <summary>
        /// Deep Copy.
        /// </summary>
        /// <returns></returns>
        public override LineItem Clone()
        {
            return (LineItemTenderCash)MemberwiseClone();
        }
    }

    /// <summary>
    /// View for a <see cref="LineItemTenderCash"/>.  (Version 1)
    /// </summary>
    public class LineItemTenderCashViewV01
    {
        /// <summary>
        /// Indicates the order that this line item was added to the list and serves as a unique identifier for the line item.  Consider this a corrected LineItemSequenceNumber implementation.  Starts at 1.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Legacy sequence implementation.  Some incorrect implementation details prevent this from being used as a true sequence number, as is intended by the ARTS model.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// True if this line item has been voided.
        /// </summary>
        public bool VoidFlag { get; set; }

        /// <summary>
        /// The id of the tender.
        /// </summary>
        public int TenderId { get; set; }

        /// <summary>
        /// The tender amount.
        /// </summary>
        public decimal TenderAmount { get; set; }

        /// <summary>
        /// The rounding amount.
        /// </summary>
        public decimal RoundingAmount { get; set; }

        /// <summary>
        /// The type of tender this line item associates with.
        /// </summary>
        public TenderType TenderType { get; set; } = TenderType.Cash;

        /// <summary>
        /// Total tax associated with this tender line item.  If null, information about the tax is not available.
        /// </summary>
        public decimal? TaxAmount { get; set; }

        /// <summary>
        /// The amount of tip provided.
        /// </summary>
        public decimal TipAmount { get; set; }
    }

    /// <summary>
    /// View for a <see cref="LineItemTenderCash"/>.  (Version 2)
    /// </summary>
    public class LineItemTenderCashViewV02
    {
        /// <summary>
        /// Indicates the order that this line item was added to the list and serves as a unique identifier for the line item.  Consider this a corrected LineItemSequenceNumber implementation.  Starts at 1.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Legacy sequence implementation.  Some incorrect implementation details prevent this from being used as a true sequence number, as is intended by the ARTS model.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// True if this line item has been voided.
        /// </summary>
        public bool VoidFlag { get; set; }

        /// <summary>
        /// The id of the tender.
        /// </summary>
        public int TenderId { get; set; }

        /// <summary>
        /// The tender amount.
        /// </summary>
        public decimal TenderAmount { get; set; }

        /// <summary>
        /// The rounding amount.
        /// </summary>
        public decimal RoundingAmount { get; set; }

        /// <summary>
        /// Total tax associated with this tender line item.  If null, information about the tax is not available.
        /// </summary>
        public decimal? TaxAmount { get; set; }

        /// <summary>
        /// The amount of tip provided.
        /// </summary>
        public decimal TipAmount { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="LineItemTenderCash"/> conversion.
    /// </summary>
    public static class LineItemTenderCashConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="LineItemTenderCashViewV01"/> object based on this <see cref="LineItemTenderCash"/>.
        /// </summary>
        /// <param name="lineItemTenderCash"></param>
        /// <returns></returns>
        public static LineItemTenderCashViewV01 ToLineItemTenderCashViewV01(this LineItemTenderCash lineItemTenderCash)
        {
            if (lineItemTenderCash == null) return null;

            return new LineItemTenderCashViewV01
            {
                Sequence = lineItemTenderCash.Sequence,
                LineItemSequenceNumber = lineItemTenderCash.LineItemSequenceNumber,
                VoidFlag = lineItemTenderCash.VoidFlag,
                TenderId = lineItemTenderCash.TenderId,
                TenderAmount = lineItemTenderCash.TenderAmount,
                RoundingAmount = lineItemTenderCash.RoundingAmount,
                TenderType = TenderType.Cash,
                TaxAmount = lineItemTenderCash.TaxAmount,
                TipAmount = lineItemTenderCash.TipAmount,
            };
        }

        /// <summary>
        /// Returns a <see cref="LineItemTenderCash"/> object based on this <see cref="LineItemTenderCashViewV01"/>.
        /// </summary>
        /// <param name="lineItemTenderCashViewV01"></param>
        /// <returns></returns>
        public static LineItemTenderCash ToLineItemTenderCash(this LineItemTenderCashViewV01 lineItemTenderCashViewV01)
        {
            if (lineItemTenderCashViewV01 == null) return null;

            return new LineItemTenderCash
            {
                Sequence = lineItemTenderCashViewV01.Sequence,
                LineItemSequenceNumber = lineItemTenderCashViewV01.LineItemSequenceNumber,
                VoidFlag = lineItemTenderCashViewV01.VoidFlag,
                TenderId = lineItemTenderCashViewV01.TenderId,
                TenderAmount = lineItemTenderCashViewV01.TenderAmount,
                RoundingAmount = lineItemTenderCashViewV01.RoundingAmount,
                TaxAmount = lineItemTenderCashViewV01.TaxAmount,
                TipAmount = lineItemTenderCashViewV01.TipAmount,
            };
        }
        #endregion

        #region Version 2
        /// <summary>
        /// Returns a <see cref="LineItemTenderCashViewV02"/> object based on this <see cref="LineItemTenderCash"/>.
        /// </summary>
        /// <param name="lineItemTenderCash"></param>
        /// <returns></returns>
        public static LineItemTenderCashViewV02 ToLineItemTenderCashViewV02(this LineItemTenderCash lineItemTenderCash)
        {
            if (lineItemTenderCash == null) return null;

            return new LineItemTenderCashViewV02
            {
                Sequence = lineItemTenderCash.Sequence,
                LineItemSequenceNumber = lineItemTenderCash.LineItemSequenceNumber,
                VoidFlag = lineItemTenderCash.VoidFlag,
                TenderId = lineItemTenderCash.TenderId,
                TenderAmount = lineItemTenderCash.TenderAmount,
                RoundingAmount = lineItemTenderCash.RoundingAmount,
                TaxAmount = lineItemTenderCash.TaxAmount,
                TipAmount = lineItemTenderCash.TipAmount,
            };
        }

        /// <summary>
        /// Returns a <see cref="LineItemTenderCash"/> object based on this <see cref="LineItemTenderCashViewV02"/>.
        /// </summary>
        /// <param name="lineItemTenderCashViewV02"></param>
        /// <returns></returns>
        public static LineItemTenderCash ToLineItemTenderCash(this LineItemTenderCashViewV02 lineItemTenderCashViewV02)
        {
            if (lineItemTenderCashViewV02 == null) return null;

            return new LineItemTenderCash
            {
                Sequence = lineItemTenderCashViewV02.Sequence,
                LineItemSequenceNumber = lineItemTenderCashViewV02.LineItemSequenceNumber,
                VoidFlag = lineItemTenderCashViewV02.VoidFlag,
                TenderId = lineItemTenderCashViewV02.TenderId,
                TenderAmount = lineItemTenderCashViewV02.TenderAmount,
                RoundingAmount = lineItemTenderCashViewV02.RoundingAmount,
                TaxAmount = lineItemTenderCashViewV02.TaxAmount,
                TipAmount = lineItemTenderCashViewV02.TipAmount,
            };
        }
        #endregion
    }
}
