﻿using BbTS.Domain.Models.Credential;
using BbTS.Domain.Models.Definitions.Transaction;

namespace BbTS.Domain.Models.ArtsDataModel
{
    /// <summary>
    /// Container class for a line item tender with customer (stored value)
    /// </summary>
    public class LineItemTenderWithCustomer : LineItemTender
    {
        /// <summary>
        /// Customer credentials.
        /// </summary>
        public CustomerCredential CustomerCredential { get; set; }

        /// <summary>
        /// Deep copy.
        /// </summary>
        /// <returns></returns>
        public override LineItem Clone()
        {
            var lineItemTenderWithCustomer = (LineItemTenderWithCustomer)MemberwiseClone();
            lineItemTenderWithCustomer.CustomerCredential = CustomerCredential.Clone();
            return lineItemTenderWithCustomer;
        }
    }

    /// <summary>
    /// View for a <see cref="LineItemTenderWithCustomer"/>.  (Version 1)
    /// </summary>
    public class LineItemTenderStoredValueViewV01
    {
        /// <summary>
        /// Indicates the order that this line item was added to the list and serves as a unique identifier for the line item.  Consider this a corrected LineItemSequenceNumber implementation.  Starts at 1.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Legacy sequence implementation.  Some incorrect implementation details prevent this from being used as a true sequence number, as is intended by the ARTS model.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// True if this line item has been voided.
        /// </summary>
        public bool VoidFlag { get; set; }

        /// <summary>
        /// The id of the tender.
        /// </summary>
        public int TenderId { get; set; }

        /// <summary>
        /// The tender amount.
        /// </summary>
        public decimal TenderAmount { get; set; }

        /// <summary>
        /// The rounding amount.
        /// </summary>
        public decimal RoundingAmount { get; set; }

        /// <summary>
        /// The type of tender this line item associates with.
        /// </summary>
        public TenderType TenderType { get; set; } = TenderType.StoredValue;

        /// <summary>
        /// Total tax associated with this tender line item.  If null, information about the tax is not available.
        /// </summary>
        public decimal? TaxAmount { get; set; }

        /// <summary>
        /// The amount of tip provided.
        /// </summary>
        public decimal TipAmount { get; set; }

        /// <summary>
        /// Customer credentials.
        /// </summary>
        public CustomerCredentialViewV01 CustomerCredential { get; set; }
    }

    /// <summary>
    /// View for a <see cref="LineItemTenderWithCustomer"/>.  (Version 2)
    /// </summary>
    public class LineItemTenderStoredValueViewV02
    {
        /// <summary>
        /// Indicates the order that this line item was added to the list and serves as a unique identifier for the line item.  Consider this a corrected LineItemSequenceNumber implementation.  Starts at 1.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Legacy sequence implementation.  Some incorrect implementation details prevent this from being used as a true sequence number, as is intended by the ARTS model.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// True if this line item has been voided.
        /// </summary>
        public bool VoidFlag { get; set; }

        /// <summary>
        /// The id of the tender.
        /// </summary>
        public int TenderId { get; set; }

        /// <summary>
        /// The tender amount.
        /// </summary>
        public decimal TenderAmount { get; set; }

        /// <summary>
        /// The rounding amount.
        /// </summary>
        public decimal RoundingAmount { get; set; }

        /// <summary>
        /// Total tax associated with this tender line item.  If null, information about the tax is not available.
        /// </summary>
        public decimal? TaxAmount { get; set; }

        /// <summary>
        /// The amount of tip provided.
        /// </summary>
        public decimal TipAmount { get; set; }

        /// <summary>
        /// Customer credentials.
        /// </summary>
        public CustomerCredentialViewV02 CustomerCredential { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="LineItemTenderWithCustomer"/> conversion.
    /// </summary>
    public static class LineItemTenderStoredValueConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="LineItemTenderStoredValueViewV01"/> object based on this <see cref="LineItemTenderWithCustomer"/>.
        /// </summary>
        /// <param name="lineItemTenderStoredValue"></param>
        /// <returns></returns>
        public static LineItemTenderStoredValueViewV01 ToLineItemTenderStoredValueViewV01(this LineItemTenderWithCustomer lineItemTenderStoredValue)
        {
            if (lineItemTenderStoredValue == null) return null;

            return new LineItemTenderStoredValueViewV01
            {
                Sequence = lineItemTenderStoredValue.Sequence,
                LineItemSequenceNumber = lineItemTenderStoredValue.LineItemSequenceNumber,
                VoidFlag = lineItemTenderStoredValue.VoidFlag,
                TenderId = lineItemTenderStoredValue.TenderId,
                TenderAmount = lineItemTenderStoredValue.TenderAmount,
                RoundingAmount = lineItemTenderStoredValue.RoundingAmount,
                TenderType = TenderType.StoredValue,
                TaxAmount = lineItemTenderStoredValue.TaxAmount,
                TipAmount = lineItemTenderStoredValue.TipAmount,
                CustomerCredential = lineItemTenderStoredValue.CustomerCredential.ToCustomerCredentialViewV01()
            };
        }

        /// <summary>
        /// Returns a <see cref="LineItemTenderWithCustomer"/> object based on this <see cref="LineItemTenderStoredValueViewV01"/>.
        /// </summary>
        /// <param name="lineItemTenderStoredValueViewV01"></param>
        /// <returns></returns>
        public static LineItemTenderWithCustomer ToLineItemTenderStoredValue(this LineItemTenderStoredValueViewV01 lineItemTenderStoredValueViewV01)
        {
            if (lineItemTenderStoredValueViewV01 == null) return null;

            return new LineItemTenderWithCustomer
            {
                Sequence = lineItemTenderStoredValueViewV01.Sequence,
                LineItemSequenceNumber = lineItemTenderStoredValueViewV01.LineItemSequenceNumber,
                VoidFlag = lineItemTenderStoredValueViewV01.VoidFlag,
                TenderId = lineItemTenderStoredValueViewV01.TenderId,
                TenderAmount = lineItemTenderStoredValueViewV01.TenderAmount,
                RoundingAmount = lineItemTenderStoredValueViewV01.RoundingAmount,
                TaxAmount = lineItemTenderStoredValueViewV01.TaxAmount,
                TipAmount = lineItemTenderStoredValueViewV01.TipAmount,
                CustomerCredential = lineItemTenderStoredValueViewV01.CustomerCredential.ToCustomerCredential()
            };
        }
        #endregion

        #region Version 2
        /// <summary>
        /// Returns a <see cref="LineItemTenderStoredValueViewV02"/> object based on this <see cref="LineItemTenderWithCustomer"/>.
        /// </summary>
        /// <param name="lineItemTenderStoredValue"></param>
        /// <returns></returns>
        public static LineItemTenderStoredValueViewV02 ToLineItemTenderStoredValueViewV02(this LineItemTenderWithCustomer lineItemTenderStoredValue)
        {
            if (lineItemTenderStoredValue == null) return null;

            return new LineItemTenderStoredValueViewV02
            {
                Sequence = lineItemTenderStoredValue.Sequence,
                LineItemSequenceNumber = lineItemTenderStoredValue.LineItemSequenceNumber,
                VoidFlag = lineItemTenderStoredValue.VoidFlag,
                TenderId = lineItemTenderStoredValue.TenderId,
                TenderAmount = lineItemTenderStoredValue.TenderAmount,
                RoundingAmount = lineItemTenderStoredValue.RoundingAmount,
                TaxAmount = lineItemTenderStoredValue.TaxAmount,
                TipAmount = lineItemTenderStoredValue.TipAmount,
                CustomerCredential = lineItemTenderStoredValue.CustomerCredential.ToCustomerCredentialViewV02()
            };
        }

        /// <summary>
        /// Returns a <see cref="LineItemTenderWithCustomer"/> object based on this <see cref="LineItemTenderStoredValueViewV02"/>.
        /// </summary>
        /// <param name="lineItemTenderStoredValueViewV02"></param>
        /// <returns></returns>
        public static LineItemTenderWithCustomer ToLineItemTenderStoredValue(this LineItemTenderStoredValueViewV02 lineItemTenderStoredValueViewV02)
        {
            if (lineItemTenderStoredValueViewV02 == null) return null;

            return new LineItemTenderWithCustomer
            {
                Sequence = lineItemTenderStoredValueViewV02.Sequence,
                LineItemSequenceNumber = lineItemTenderStoredValueViewV02.LineItemSequenceNumber,
                VoidFlag = lineItemTenderStoredValueViewV02.VoidFlag,
                TenderId = lineItemTenderStoredValueViewV02.TenderId,
                TenderAmount = lineItemTenderStoredValueViewV02.TenderAmount,
                RoundingAmount = lineItemTenderStoredValueViewV02.RoundingAmount,
                TaxAmount = lineItemTenderStoredValueViewV02.TaxAmount,
                TipAmount = lineItemTenderStoredValueViewV02.TipAmount,
                CustomerCredential = lineItemTenderStoredValueViewV02.CustomerCredential.ToCustomerCredential()
            };
        }
        #endregion
    }
}
