﻿using System.Linq;
using BbTS.Domain.Models.Container.Elements;
using BbTS.Domain.Models.Definitions.ArtsDataModel;

namespace BbTS.Domain.Models.ArtsDataModel
{
    /// <summary>
    /// Line item tender discount.
    /// </summary>
    public class LineItemTenderDiscount : LineItem
    {
        /// <summary>
        /// Sequence of the Line Item Tender that this discount is associated with.
        /// </summary>
        public int TenderSequence { get; set; }

        /// <summary>
        /// (Legacy) (Legacy) Indicates the order that this line item discount was applied.
        /// </summary>
        public int TenderLineItemSequenceNumber { get; set; }

        /// <summary>
        /// Discount surcharge reason.  The reason this discount was applied.
        /// </summary>
        public DiscountSurchargeReason DiscountSurchargeReason { get; set; }

        /// <summary>
        /// Discount rate.  25% discount is denoted as 0.25
        /// </summary>
        public decimal Rate { get; set; }

        /// <summary>
        /// Rounding amount.  Example: A discount rate of 0.25 applied to a $0.19 total = $0.0475, which rounds to $0.05, so rounding amount will be 0.0025
        /// </summary>
        public decimal RoundingAmount { get; set; }

        /// <summary>
        /// Discount amount.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Prorated flag.
        /// </summary>
        public bool ProratedFlag { get; set; }

        /// <summary>
        /// The type of calculation.  Always Percent for this type of discount.
        /// </summary>
        public CalculationType CalculationType { get; set; }

        /// <inheritdoc />
        public override LineItem Clone()
        {
            return (LineItemTenderDiscount)MemberwiseClone();
        }
    }

    /// <summary>
    /// View for a <see cref="LineItemTenderDiscount"/>.  (Version 1)
    /// </summary>
    public class LineItemTenderDiscountViewV01
    {
        /// <summary>
        /// Indicates the order that this line item was added to the list and serves as a unique identifier for the line item.  Consider this a corrected LineItemSequenceNumber implementation.  Starts at 1.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Legacy sequence implementation.  Some incorrect implementation details prevent this from being used as a true sequence number, as is intended by the ARTS model.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// True if this line item has been voided.
        /// </summary>
        public bool VoidFlag { get; set; }

        /// <summary>
        /// Sequence of the Line Item Tender that this discount is associated with.
        /// </summary>
        public int TenderSequence { get; set; }

        /// <summary>
        /// Discount surcharge reason.  The reason this discount was applied.
        /// </summary>
        public DiscountSurchargeReason DiscountSurchargeReason { get; set; }

        /// <summary>
        /// Discount rate.  25% discount is denoted as 0.25
        /// </summary>
        public decimal Rate { get; set; }

        /// <summary>
        /// Rounding amount.  Example: A discount rate of 0.25 applied to a $0.19 total = $0.0475, which rounds to $0.05, so rounding amount will be 0.0025
        /// </summary>
        public decimal RoundingAmount { get; set; }

        /// <summary>
        /// Discount amount.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Prorated flag.
        /// </summary>
        public bool ProratedFlag { get; set; }

        /// <summary>
        /// The type of calculation.  Always Percent for this type of discount.
        /// </summary>
        public CalculationType CalculationType { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="LineItemTenderDiscount"/> conversion.
    /// </summary>
    public static class LineItemTenderDiscountConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="LineItemTenderDiscountViewV01"/> object based on this <see cref="LineItemTenderDiscount"/>.
        /// </summary>
        /// <param name="lineItemTenderDiscount"></param>
        /// <returns></returns>
        public static LineItemTenderDiscountViewV01 ToLineItemTenderDiscountViewV01(this LineItemTenderDiscount lineItemTenderDiscount)
        {
            if (lineItemTenderDiscount == null) return null;

            return new LineItemTenderDiscountViewV01
            {
                Sequence = lineItemTenderDiscount.Sequence,
                LineItemSequenceNumber = lineItemTenderDiscount.LineItemSequenceNumber,
                VoidFlag = lineItemTenderDiscount.VoidFlag,
                TenderSequence = lineItemTenderDiscount.TenderSequence,
                DiscountSurchargeReason = lineItemTenderDiscount.DiscountSurchargeReason,
                Rate = lineItemTenderDiscount.Rate,
                RoundingAmount = lineItemTenderDiscount.RoundingAmount,
                Amount = lineItemTenderDiscount.Amount,
                ProratedFlag = lineItemTenderDiscount.ProratedFlag,
                CalculationType = lineItemTenderDiscount.CalculationType
            };
        }

        /// <summary>
        /// Returns a <see cref="LineItemTenderDiscount"/> object based on this <see cref="LineItemTenderDiscountViewV01"/>.
        /// </summary>
        /// <param name="lineItemTenderDiscountViewV01"></param>
        /// <returns></returns>
        public static LineItemTenderDiscount ToLineItemTenderDiscount(this LineItemTenderDiscountViewV01 lineItemTenderDiscountViewV01)
        {
            if (lineItemTenderDiscountViewV01 == null) return null;

            return new LineItemTenderDiscount
            {
                Sequence = lineItemTenderDiscountViewV01.Sequence,
                LineItemSequenceNumber = lineItemTenderDiscountViewV01.LineItemSequenceNumber,
                VoidFlag = lineItemTenderDiscountViewV01.VoidFlag,
                TenderSequence = lineItemTenderDiscountViewV01.TenderSequence,
                DiscountSurchargeReason = lineItemTenderDiscountViewV01.DiscountSurchargeReason,
                Rate = lineItemTenderDiscountViewV01.Rate,
                RoundingAmount = lineItemTenderDiscountViewV01.RoundingAmount,
                Amount = lineItemTenderDiscountViewV01.Amount,
                ProratedFlag = lineItemTenderDiscountViewV01.ProratedFlag,
                CalculationType = lineItemTenderDiscountViewV01.CalculationType
            };
        }
        #endregion
    }
}
