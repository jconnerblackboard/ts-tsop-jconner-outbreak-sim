﻿using System;
using BbTS.Domain.Models.Definitions.Transaction;
using BbTS.Domain.Models.System;

namespace BbTS.Domain.Models.ArtsDataModel
{
    /// <summary>
    /// Container class for a line item tender emv processed via HIT api (credit card).
    /// </summary>
    public class LineItemTenderEmvHit : LineItemTender
    {
        /// <summary>
        /// Amount requested.
        /// </summary>
        public decimal AmountRequested { get; set; }

        /// <summary>
        /// Amount requested.
        /// </summary>
        public decimal AmountAuthorized { get; set; }

        /// <summary>
        /// The authorization code.
        /// </summary>
        public string AuthorizationCode { get; set; }

        /// <summary>
        /// The card id.
        /// </summary>
        public EmvCreditCardId CardId { get; set; }

        /// <summary>
        /// Transaction reference given by DPS (DpsTxnRef).
        /// </summary>
        public string DpsTransactionReference { get; set; }

        /// <summary>
        /// The identifier assigned to the emv terminal.
        /// </summary>
        public string EmvDeviceId { get; set; }

        /// <summary>
        /// The masked pan value.
        /// </summary>
        public string MaskedPan { get; set; }

        /// <summary>
        /// PosID and Transaction Number.
        /// </summary>
        public string MerchantReference { get; set; }

        /// <summary>
        /// The date and time of the payment express request.
        /// </summary>
        public DateTimeOffset PaymentExpressRequestDateTime { get; set; }

        /// <summary>
        /// The date and time of the payment express response.
        /// </summary>
        public DateTimeOffset PaymentExpressResponseDateTime { get; set; }

        /// <summary>
        /// The purchase response code (ReCo).
        /// </summary>
        public string PurchaseResponseCode { get; set; }

        /// <summary>
        /// The result token provided from the txnpur response.
        /// </summary>
        public ActionResultToken Result { get; set; }

        /// <summary>
        /// The settlement date and time.
        /// </summary>
        public DateTimeOffset SettlementDateTime { get; set; }

        /// <summary>
        /// The system trace audit number.
        /// </summary>
        public int SystemTraceAuditNumber { get; set; }

        /// <summary>
        /// The transaction reference id (TxnRef).
        /// </summary>
        public string TransactionReference { get; set; }

        /// <summary>
        /// Deep Copy.
        /// </summary>
        /// <returns></returns>
        public override LineItem Clone()
        {
            return (LineItemTenderEmvHit)MemberwiseClone();
        }
    }

    ///LineItemTenderEmvHitViewV01 is not in use and has been deleted.

    /// <summary>
    /// View for a <see cref="LineItemTenderEmvHit"/>.
    /// </summary>
    public class LineItemTenderEmvHitViewV02
    {
        /// <summary>
        /// Indicates the order that this line item was added to the list and serves as a unique identifier for the line item.  Consider this a corrected LineItemSequenceNumber implementation.  Starts at 1.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Legacy sequence implementation.  Some incorrect implementation details prevent this from being used as a true sequence number, as is intended by the ARTS model.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// True if this line item has been voided.
        /// </summary>
        public bool VoidFlag { get; set; }

        /// <summary>
        /// The id of the tender.
        /// </summary>
        public int TenderId { get; set; }

        /// <summary>
        /// The tender amount.
        /// </summary>
        public decimal TenderAmount { get; set; }

        /// <summary>
        /// The rounding amount.
        /// </summary>
        public decimal RoundingAmount { get; set; }

        /// <summary>
        /// Total tax associated with this tender line item.  If null, information about the tax is not available.
        /// </summary>
        public decimal? TaxAmount { get; set; }

        /// <summary>
        /// The amount of tip provided.
        /// </summary>
        public decimal TipAmount { get; set; }

        /// <summary>
        /// Amount requested.
        /// </summary>
        public decimal AmountRequested { get; set; }

        /// <summary>
        /// Amount requested.
        /// </summary>
        public decimal AmountAuthorized { get; set; }

        /// <summary>
        /// The authorization code.
        /// </summary>
        public string AuthorizationCode { get; set; }

        /// <summary>
        /// The card id.
        /// </summary>
        public EmvCreditCardId CardId { get; set; }

        /// <summary>
        /// Transaction reference given by DPS (DpsTxnRef).
        /// </summary>
        public string DpsTransactionReference { get; set; }

        /// <summary>
        /// The identifier assigned to the emv terminal.
        /// </summary>
        public string EmvDeviceId { get; set; }

        /// <summary>
        /// The masked pan value.
        /// </summary>
        public string MaskedPan { get; set; }

        /// <summary>
        /// PosID and Transaction Number.
        /// </summary>
        public string MerchantReference { get; set; }

        /// <summary>
        /// The date and time of the payment express request.
        /// </summary>
        public DateTimeOffset PaymentExpressRequestDateTime { get; set; }

        /// <summary>
        /// The date and time of the payment express response.
        /// </summary>
        public DateTimeOffset PaymentExpressResponseDateTime { get; set; }

        /// <summary>
        /// The purchase response code (ReCo).
        /// </summary>
        public string PurchaseResponseCode { get; set; }

        /// <summary>
        /// The result token provided from the txnpur response.
        /// </summary>
        public ActionResultTokenViewV01 Result { get; set; }

        /// <summary>
        /// The settlement date and time.
        /// </summary>
        public DateTimeOffset SettlementDateTime { get; set; }

        /// <summary>
        /// The system trace audit number.
        /// </summary>
        public int SystemTraceAuditNumber { get; set; }

        /// <summary>
        /// The transaction reference id (TxnRef).
        /// </summary>
        public string TransactionReference { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="LineItemTenderEmvHit"/> conversion.
    /// </summary>
    public static class LineItemTenderEmvHitConverter
    {
        #region Version 1
        //LineItemTenderEmvHitViewV01 is not in use and has been deleted.
        #endregion

        #region Version 2
        /// <summary>
        /// Returns a <see cref="LineItemTenderEmvHitViewV02"/> object based on this <see cref="LineItemTenderEmvHit"/>.
        /// </summary>
        /// <param name="lineItemTenderEmv"></param>
        /// <returns></returns>
        public static LineItemTenderEmvHitViewV02 ToLineItemTenderEmvHitViewV02(this LineItemTenderEmvHit lineItemTenderEmv)
        {
            if (lineItemTenderEmv == null) return null;

            return new LineItemTenderEmvHitViewV02
            {
                Sequence = lineItemTenderEmv.Sequence,
                LineItemSequenceNumber = lineItemTenderEmv.LineItemSequenceNumber,
                VoidFlag = lineItemTenderEmv.VoidFlag,
                TenderId = lineItemTenderEmv.TenderId,
                TenderAmount = lineItemTenderEmv.TenderAmount,
                RoundingAmount = lineItemTenderEmv.RoundingAmount,
                TaxAmount = lineItemTenderEmv.TaxAmount,
                TipAmount = lineItemTenderEmv.TipAmount,
                AmountRequested = lineItemTenderEmv.AmountRequested,
                AmountAuthorized = lineItemTenderEmv.AmountAuthorized,
                AuthorizationCode = lineItemTenderEmv.AuthorizationCode,
                CardId = lineItemTenderEmv.CardId,
                DpsTransactionReference = lineItemTenderEmv.DpsTransactionReference,
                EmvDeviceId = lineItemTenderEmv.EmvDeviceId,
                MaskedPan = lineItemTenderEmv.MaskedPan,
                MerchantReference = lineItemTenderEmv.MerchantReference,
                PaymentExpressRequestDateTime = lineItemTenderEmv.PaymentExpressRequestDateTime,
                PaymentExpressResponseDateTime = lineItemTenderEmv.PaymentExpressResponseDateTime,
                PurchaseResponseCode = lineItemTenderEmv.PurchaseResponseCode,
                Result = lineItemTenderEmv.Result.ToActionResultTokenViewV01(),
                SettlementDateTime = lineItemTenderEmv.SettlementDateTime,
                SystemTraceAuditNumber = lineItemTenderEmv.SystemTraceAuditNumber,
                TransactionReference = lineItemTenderEmv.TransactionReference
            };
        }

        /// <summary>
        /// Returns a <see cref="LineItemTenderEmvHit"/> object based on this <see cref="LineItemTenderEmvHitViewV02"/>.
        /// </summary>
        /// <param name="lineItemTenderEmvHitViewV02"></param>
        /// <returns></returns>
        public static LineItemTenderEmvHit ToLineItemTenderEmvHit(this LineItemTenderEmvHitViewV02 lineItemTenderEmvHitViewV02)
        {
            if (lineItemTenderEmvHitViewV02 == null) return null;

            return new LineItemTenderEmvHit
            {
                Sequence = lineItemTenderEmvHitViewV02.Sequence,
                LineItemSequenceNumber = lineItemTenderEmvHitViewV02.LineItemSequenceNumber,
                VoidFlag = lineItemTenderEmvHitViewV02.VoidFlag,
                TenderId = lineItemTenderEmvHitViewV02.TenderId,
                TenderAmount = lineItemTenderEmvHitViewV02.TenderAmount,
                RoundingAmount = lineItemTenderEmvHitViewV02.RoundingAmount,
                TaxAmount = lineItemTenderEmvHitViewV02.TaxAmount,
                TipAmount = lineItemTenderEmvHitViewV02.TipAmount,
                AmountRequested = lineItemTenderEmvHitViewV02.AmountRequested,
                AmountAuthorized = lineItemTenderEmvHitViewV02.AmountAuthorized,
                AuthorizationCode = lineItemTenderEmvHitViewV02.AuthorizationCode,
                CardId = lineItemTenderEmvHitViewV02.CardId,
                DpsTransactionReference = lineItemTenderEmvHitViewV02.DpsTransactionReference,
                EmvDeviceId = lineItemTenderEmvHitViewV02.EmvDeviceId,
                MaskedPan = lineItemTenderEmvHitViewV02.MaskedPan,
                MerchantReference = lineItemTenderEmvHitViewV02.MerchantReference,
                PaymentExpressRequestDateTime = lineItemTenderEmvHitViewV02.PaymentExpressRequestDateTime,
                PaymentExpressResponseDateTime = lineItemTenderEmvHitViewV02.PaymentExpressResponseDateTime,
                PurchaseResponseCode = lineItemTenderEmvHitViewV02.PurchaseResponseCode,
                Result = lineItemTenderEmvHitViewV02.Result.ToActionResultToken(),
                SettlementDateTime = lineItemTenderEmvHitViewV02.SettlementDateTime,
                SystemTraceAuditNumber = lineItemTenderEmvHitViewV02.SystemTraceAuditNumber,
                TransactionReference = lineItemTenderEmvHitViewV02.TransactionReference
            };
        }
        #endregion
    }
}
