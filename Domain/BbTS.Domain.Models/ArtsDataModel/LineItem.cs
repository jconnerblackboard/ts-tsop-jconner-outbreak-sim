﻿using BbTS.Domain.Models.Container.Elements;

namespace BbTS.Domain.Models.ArtsDataModel
{
    /// <summary>
    /// Base class for ARTS model line items.
    /// </summary>
    public abstract class LineItem
    {
        /// <summary>
        /// Indicates the order that this line item was added to the list and serves as a unique identifier (within an <see cref="ArtsTransactionElement"/> instance) for the line item.  Consider this a corrected LineItemSequenceNumber implementation.  Starts at 1.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Legacy sequence implementation.  Some incorrect implementation details prevent this from being used as a true sequence number, as is intended by the ARTS model.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// True if this line item has been voided.
        /// </summary>
        public bool VoidFlag { get; set; }

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public abstract LineItem Clone();
    }
}
