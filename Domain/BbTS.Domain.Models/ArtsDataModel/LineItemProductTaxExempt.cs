﻿using BbTS.Domain.Models.Definitions.ArtsDataModel;

namespace BbTS.Domain.Models.ArtsDataModel
{
    /// <summary>
    /// Line item product tax exempt.
    /// </summary>
    public class LineItemProductTaxExempt : LineItem
    {
        //public int ProductSequenceNumber { get; set; }  // LineItemSequenceNumber of the product that this product tax exempt data belongs to

        /// <summary>
        /// Tax exempt reason.
        /// </summary>
        public TaxExemptReason TaxExemptReason { get; set; }

        /// <summary>
        /// Exempt taxable amount.
        /// </summary>
        public decimal ExemptTaxableAmount { get; set; }

        /// <summary>
        /// Exempt tax amount.
        /// </summary>
        public decimal ExemptTaxAmount { get; set; }

        /// <inheritdoc />
        public override LineItem Clone()
        {
            return (LineItemProductTaxExempt)MemberwiseClone();
        }
    }

    /// <summary>
    /// Line item product tax exempt.
    /// </summary>
    public class LineItemProductTaxExemptViewV01
    {
        /// <summary>
        /// Indicates the order that this line item was added to the list and serves as a unique identifier for the line item.  Consider this a corrected LineItemSequenceNumber implementation.  Starts at 1.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Legacy sequence implementation.  Some incorrect implementation details prevent this from being used as a true sequence number, as is intended by the ARTS model.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// True if this line item has been voided.
        /// </summary>
        public bool VoidFlag { get; set; }

        /// <summary>
        /// Tax exempt reason.
        /// </summary>
        public TaxExemptReason TaxExemptReason { get; set; }

        /// <summary>
        /// Exempt taxable amount.
        /// </summary>
        public decimal ExemptTaxableAmount { get; set; }

        /// <summary>
        /// Exempt tax amount.
        /// </summary>
        public decimal ExemptTaxAmount { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="LineItemProductTaxExempt"/> conversion.
    /// </summary>
    public static class LineItemProductTaxExemptConverter
    {
        /// <summary>
        /// Returns a <see cref="LineItemProductTaxExemptViewV01"/> object based on this <see cref="LineItemProductTaxExempt"/>.
        /// </summary>
        /// <param name="lineItemProductTaxExempt"></param>
        /// <returns></returns>
        public static LineItemProductTaxExemptViewV01 ToLineItemProductTaxExemptViewV01(this LineItemProductTaxExempt lineItemProductTaxExempt)
        {
            if (lineItemProductTaxExempt == null) return null;

            return new LineItemProductTaxExemptViewV01
            {
                Sequence = lineItemProductTaxExempt.Sequence,
                LineItemSequenceNumber = lineItemProductTaxExempt.LineItemSequenceNumber,
                VoidFlag = lineItemProductTaxExempt.VoidFlag,
                TaxExemptReason = lineItemProductTaxExempt.TaxExemptReason,
                ExemptTaxableAmount = lineItemProductTaxExempt.ExemptTaxableAmount,
                ExemptTaxAmount = lineItemProductTaxExempt.ExemptTaxAmount
            };
        }

        /// <summary>
        /// Returns a <see cref="LineItemProductTaxExempt"/> object based on this <see cref="LineItemProductTaxExemptViewV01"/>.
        /// </summary>
        /// <param name="lineItemProductTaxExemptViewV01"></param>
        /// <returns></returns>
        public static LineItemProductTaxExempt ToLineItemProductTaxExempt(this LineItemProductTaxExemptViewV01 lineItemProductTaxExemptViewV01)
        {
            if (lineItemProductTaxExemptViewV01 == null) return null;

            return new LineItemProductTaxExempt
            {
                Sequence = lineItemProductTaxExemptViewV01.Sequence,
                LineItemSequenceNumber = lineItemProductTaxExemptViewV01.LineItemSequenceNumber,
                VoidFlag = lineItemProductTaxExemptViewV01.VoidFlag,
                TaxExemptReason = lineItemProductTaxExemptViewV01.TaxExemptReason,
                ExemptTaxableAmount = lineItemProductTaxExemptViewV01.ExemptTaxableAmount,
                ExemptTaxAmount = lineItemProductTaxExemptViewV01.ExemptTaxAmount
            };
        }
    }
}
