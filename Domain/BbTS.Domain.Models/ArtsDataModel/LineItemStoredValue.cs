﻿using System;
using BbTS.Domain.Models.Credential;
using BbTS.Domain.Models.Definitions.ArtsDataModel;

namespace BbTS.Domain.Models.ArtsDataModel
{
    /// <summary>
    /// Line item deposit.  (Poorly named.  Rename to LineItemDeposit when opportunity arises.)
    /// </summary>
    public class LineItemStoredValue : LineItem
    {
        /// <summary>
        /// Debit credit type.
        /// </summary>
        public DebitCreditType DebitCreditType { get; set; }

        //Example:  $100 with $20 enrich would be
        //  StoredValue_Amount = $120
        //  ActualUnit_Price = $100
        //  UnitCost_Price := $120
        //  Therefore, Retailer takes a $20 hit

        /// <summary>
        /// Amount to Deposit into Stored Value Account
        /// </summary>
        public decimal StoredValueAmount { get; set; }

        /// <summary>
        /// Used by AMC when dispensing cards. Normally just leave it $0.00
        /// </summary>
        public decimal CardPurchaseFeeAmount { get; set; }

        /// <summary>
        /// Amount Charged to Customer for Deposit
        /// </summary>
        public decimal ActualUnitPrice { get; set; }

        /// <summary>
        /// Cost to the Retailer
        /// </summary>
        public decimal UnitCostPrice { get; set; }

        /// <summary>
        /// Stored value account type ID.
        /// </summary>
        public int StoredValueAccountTypeId { get; set; }

        /// <summary>
        /// Stored value account type name.
        /// </summary>
        [Obsolete("Originator does not have this information, nor does the middle tier require it for processing.")]
        public string StoredValueAccountTypeName { get; set; }

        /// <summary>
        /// Enrichment flag.
        /// </summary>
        public bool EnrichmentFlag { get; set; }

        /// <summary>
        /// Enrichment parent line item sequence.
        /// </summary>
        public int EnrichmentParentLineItemSequence { get; set; }

        /// <summary>
        /// Customer credential.
        /// </summary>
        public CustomerCredential CustomerCredential { get; set; }

        /// <inheritdoc />
        public override LineItem Clone()
        {
            var lineItemStoredValue = (LineItemStoredValue)MemberwiseClone();
            lineItemStoredValue.CustomerCredential = CustomerCredential.Clone();
            return lineItemStoredValue;
        }
    }

    /// <summary>
    /// View for a <see cref="LineItemStoredValue"/>.  (Version 1)
    /// </summary>
    public class LineItemDepositViewV01
    {
        /// <summary>
        /// Indicates the order that this line item was added to the list and serves as a unique identifier for the line item.  Consider this a corrected LineItemSequenceNumber implementation.  Starts at 1.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Legacy sequence implementation.  Some incorrect implementation details prevent this from being used as a true sequence number, as is intended by the ARTS model.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// True if this line item has been voided.
        /// </summary>
        public bool VoidFlag { get; set; }

        /// <summary>
        /// Debit credit type.
        /// </summary>
        public DebitCreditType DebitCreditType { get; set; }

        /// <summary>
        /// Amount to Deposit into Stored Value Account
        /// </summary>
        public decimal StoredValueAmount { get; set; }

        /// <summary>
        /// Used by AMC when dispensing cards. Normally just leave it $0.00
        /// </summary>
        public decimal CardPurchaseFeeAmount { get; set; }

        /// <summary>
        /// Amount Charged to Customer for Deposit
        /// </summary>
        public decimal ActualUnitPrice { get; set; }

        /// <summary>
        /// Cost to the Retailer
        /// </summary>
        public decimal UnitCostPrice { get; set; }

        /// <summary>
        /// Stored value account type ID.
        /// </summary>
        public int StoredValueAccountTypeId { get; set; }

        /// <summary>
        /// Stored value account type name.
        /// </summary>
        public string StoredValueAccountTypeName { get; set; }

        /// <summary>
        /// Enrichment flag.
        /// </summary>
        public bool EnrichmentFlag { get; set; }

        /// <summary>
        /// Enrichemnt parent line item sequence.
        /// </summary>
        public int EnrichmentParentLineItemSequence { get; set; }

        /// <summary>
        /// Customer credential.
        /// </summary>
        public CustomerCredentialViewV01 CustomerCredential { get; set; }
    }

    /// <summary>
    /// View for a <see cref="LineItemStoredValue"/>.  (Version 2)
    /// </summary>
    public class LineItemDepositViewV02
    {
        /// <summary>
        /// Indicates the order that this line item was added to the list and serves as a unique identifier for the line item.  Consider this a corrected LineItemSequenceNumber implementation.  Starts at 1.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Legacy sequence implementation.  Some incorrect implementation details prevent this from being used as a true sequence number, as is intended by the ARTS model.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// True if this line item has been voided.
        /// </summary>
        public bool VoidFlag { get; set; }

        /// <summary>
        /// Amount to Deposit into Stored Value Account
        /// </summary>
        public decimal StoredValueAmount { get; set; }

        /// <summary>
        /// Used by AMC when dispensing cards. Normally just leave it $0.00
        /// </summary>
        public decimal CardPurchaseFeeAmount { get; set; }

        /// <summary>
        /// Amount Charged to Customer for Deposit
        /// </summary>
        public decimal ActualUnitPrice { get; set; }

        /// <summary>
        /// Cost to the Retailer
        /// </summary>
        public decimal UnitCostPrice { get; set; }

        /// <summary>
        /// Stored value account type ID.
        /// </summary>
        public int StoredValueAccountTypeId { get; set; }

        /// <summary>
        /// Enrichment flag.
        /// </summary>
        public bool EnrichmentFlag { get; set; }

        /// <summary>
        /// Enrichemnt parent line item sequence.
        /// </summary>
        public int EnrichmentParentLineItemSequence { get; set; }

        /// <summary>
        /// Customer credential.
        /// </summary>
        public CustomerCredentialViewV02 CustomerCredential { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="LineItemStoredValue"/> conversion.
    /// </summary>
    public static class LineItemDepositConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="LineItemDepositViewV01"/> object based on this <see cref="LineItemStoredValue"/>.
        /// </summary>
        /// <param name="lineItemDeposit"></param>
        /// <returns></returns>
        public static LineItemDepositViewV01 ToLineItemDepositViewV01(this LineItemStoredValue lineItemDeposit)
        {
            if (lineItemDeposit == null) return null;

            return new LineItemDepositViewV01
            {
                Sequence = lineItemDeposit.Sequence,
                LineItemSequenceNumber = lineItemDeposit.LineItemSequenceNumber,
                VoidFlag = lineItemDeposit.VoidFlag,
                DebitCreditType = lineItemDeposit.DebitCreditType,
                StoredValueAmount = lineItemDeposit.StoredValueAmount,
                CardPurchaseFeeAmount = lineItemDeposit.CardPurchaseFeeAmount,
                ActualUnitPrice = lineItemDeposit.ActualUnitPrice,
                UnitCostPrice = lineItemDeposit.UnitCostPrice,
                StoredValueAccountTypeId = lineItemDeposit.StoredValueAccountTypeId,
                StoredValueAccountTypeName = lineItemDeposit.StoredValueAccountTypeName,
                EnrichmentFlag = lineItemDeposit.EnrichmentFlag,
                EnrichmentParentLineItemSequence = lineItemDeposit.EnrichmentParentLineItemSequence,
                CustomerCredential = lineItemDeposit.CustomerCredential.ToCustomerCredentialViewV01()
            };
        }

        /// <summary>
        /// Returns a <see cref="LineItemStoredValue"/> object based on this <see cref="LineItemDepositViewV01"/>.
        /// </summary>
        /// <param name="lineItemDepositViewV01"></param>
        /// <returns></returns>
        public static LineItemStoredValue ToLineItemDeposit(this LineItemDepositViewV01 lineItemDepositViewV01)
        {
            if (lineItemDepositViewV01 == null) return null;

            return new LineItemStoredValue
            {
                Sequence = lineItemDepositViewV01.Sequence,
                LineItemSequenceNumber = lineItemDepositViewV01.LineItemSequenceNumber,
                VoidFlag = lineItemDepositViewV01.VoidFlag,
                DebitCreditType = lineItemDepositViewV01.DebitCreditType,
                StoredValueAmount = lineItemDepositViewV01.StoredValueAmount,
                CardPurchaseFeeAmount = lineItemDepositViewV01.CardPurchaseFeeAmount,
                ActualUnitPrice = lineItemDepositViewV01.ActualUnitPrice,
                UnitCostPrice = lineItemDepositViewV01.UnitCostPrice,
                StoredValueAccountTypeId = lineItemDepositViewV01.StoredValueAccountTypeId,
                StoredValueAccountTypeName = lineItemDepositViewV01.StoredValueAccountTypeName,
                EnrichmentFlag = lineItemDepositViewV01.EnrichmentFlag,
                EnrichmentParentLineItemSequence = lineItemDepositViewV01.EnrichmentParentLineItemSequence,
                CustomerCredential = lineItemDepositViewV01.CustomerCredential.ToCustomerCredential()
            };
        }
        #endregion

        #region Version 2
        /// <summary>
        /// Returns a <see cref="LineItemDepositViewV02"/> object based on this <see cref="LineItemStoredValue"/>.
        /// </summary>
        /// <param name="lineItemDeposit"></param>
        /// <returns></returns>
        public static LineItemDepositViewV02 ToLineItemDepositViewV02(this LineItemStoredValue lineItemDeposit)
        {
            if (lineItemDeposit == null) return null;

            return new LineItemDepositViewV02
            {
                Sequence = lineItemDeposit.Sequence,
                LineItemSequenceNumber = lineItemDeposit.LineItemSequenceNumber,
                VoidFlag = lineItemDeposit.VoidFlag,
                StoredValueAmount = lineItemDeposit.StoredValueAmount,
                CardPurchaseFeeAmount = lineItemDeposit.CardPurchaseFeeAmount,
                ActualUnitPrice = lineItemDeposit.ActualUnitPrice,
                UnitCostPrice = lineItemDeposit.UnitCostPrice,
                StoredValueAccountTypeId = lineItemDeposit.StoredValueAccountTypeId,
                EnrichmentFlag = lineItemDeposit.EnrichmentFlag,
                EnrichmentParentLineItemSequence = lineItemDeposit.EnrichmentParentLineItemSequence,
                CustomerCredential = lineItemDeposit.CustomerCredential.ToCustomerCredentialViewV02()
            };
        }

        /// <summary>
        /// Returns a <see cref="LineItemStoredValue"/> object based on this <see cref="LineItemDepositViewV02"/>.
        /// </summary>
        /// <param name="lineItemDepositViewV02"></param>
        /// <returns></returns>
        public static LineItemStoredValue ToLineItemDeposit(this LineItemDepositViewV02 lineItemDepositViewV02)
        {
            if (lineItemDepositViewV02 == null) return null;

            return new LineItemStoredValue
            {
                Sequence = lineItemDepositViewV02.Sequence,
                LineItemSequenceNumber = lineItemDepositViewV02.LineItemSequenceNumber,
                VoidFlag = lineItemDepositViewV02.VoidFlag,
                DebitCreditType = DebitCreditType.Credit,
                StoredValueAmount = lineItemDepositViewV02.StoredValueAmount,
                CardPurchaseFeeAmount = lineItemDepositViewV02.CardPurchaseFeeAmount,
                ActualUnitPrice = lineItemDepositViewV02.ActualUnitPrice,
                UnitCostPrice = lineItemDepositViewV02.UnitCostPrice,
                StoredValueAccountTypeId = lineItemDepositViewV02.StoredValueAccountTypeId,
                EnrichmentFlag = lineItemDepositViewV02.EnrichmentFlag,
                EnrichmentParentLineItemSequence = lineItemDepositViewV02.EnrichmentParentLineItemSequence,
                CustomerCredential = lineItemDepositViewV02.CustomerCredential.ToCustomerCredential()
            };
        }
        #endregion
    }
}
