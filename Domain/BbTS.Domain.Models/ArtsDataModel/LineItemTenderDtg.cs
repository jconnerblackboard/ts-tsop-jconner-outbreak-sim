﻿using System;

namespace BbTS.Domain.Models.ArtsDataModel
{
    /// <summary>
    /// Line item tender direct to gateway.
    /// </summary>
    [Obsolete("Direct to gateway not supported by BbTS Web API")]
    public class LineItemTenderDtg : LineItemTender
    {
        /// <summary>
        /// Order ID.
        /// </summary>
        public string OrderId { get; set; }

        /// <inheritdoc />
        public override LineItem Clone()
        {
            return (LineItemTenderDtg)MemberwiseClone();
        }
    }

    /// <summary>
    /// View for a <see cref="LineItemTenderDtg"/>.  (Version 1)
    /// </summary>
    public class LineItemTenderDtgViewV01
    {
        /// <summary>
        /// Indicates the order that this line item was added to the list and serves as a unique identifier for the line item.  Consider this a corrected LineItemSequenceNumber implementation.  Starts at 1.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Legacy sequence implementation.  Some incorrect implementation details prevent this from being used as a true sequence number, as is intended by the ARTS model.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// True if this line item has been voided.
        /// </summary>
        public bool VoidFlag { get; set; }

        /// <summary>
        /// The id of the tender.
        /// </summary>
        public int TenderId { get; set; }

        /// <summary>
        /// The tender amount.
        /// </summary>
        public decimal TenderAmount { get; set; }

        /// <summary>
        /// The rounding amount.
        /// </summary>
        public decimal RoundingAmount { get; set; }

        /// <summary>
        /// Total tax associated with this tender line item.  If null, information about the tax is not available.
        /// </summary>
        public decimal? TaxAmount { get; set; }

        /// <summary>
        /// The amount of tip provided.
        /// </summary>
        public decimal TipAmount { get; set; }

        /// <summary>
        /// Order ID.
        /// </summary>
        public string OrderId { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="LineItemTenderDtg"/> conversion.
    /// </summary>
    public static class LineItemTenderDtgConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="LineItemTenderDtgViewV01"/> object based on this <see cref="LineItemTenderDtg"/>.
        /// </summary>
        /// <param name="lineItemTenderDtg"></param>
        /// <returns></returns>
        public static LineItemTenderDtgViewV01 ToLineItemTenderDtgViewV01(this LineItemTenderDtg lineItemTenderDtg)
        {
            if (lineItemTenderDtg == null) return null;

            return new LineItemTenderDtgViewV01
            {
                Sequence = lineItemTenderDtg.Sequence,
                LineItemSequenceNumber = lineItemTenderDtg.LineItemSequenceNumber,
                VoidFlag = lineItemTenderDtg.VoidFlag,
                TenderId = lineItemTenderDtg.TenderId,
                TenderAmount = lineItemTenderDtg.TenderAmount,
                RoundingAmount = lineItemTenderDtg.RoundingAmount,
                TaxAmount = lineItemTenderDtg.TaxAmount,
                TipAmount = lineItemTenderDtg.TipAmount,
                OrderId = lineItemTenderDtg.OrderId
            };
        }

        /// <summary>
        /// Returns a <see cref="LineItemTenderDtg"/> object based on this <see cref="LineItemTenderDtgViewV01"/>.
        /// </summary>
        /// <param name="lineItemTenderDtgViewV01"></param>
        /// <returns></returns>
        public static LineItemTenderDtg ToLineItemTenderDtg(this LineItemTenderDtgViewV01 lineItemTenderDtgViewV01)
        {
            if (lineItemTenderDtgViewV01 == null) return null;

            return new LineItemTenderDtg
            {
                Sequence = lineItemTenderDtgViewV01.Sequence,
                LineItemSequenceNumber = lineItemTenderDtgViewV01.LineItemSequenceNumber,
                VoidFlag = lineItemTenderDtgViewV01.VoidFlag,
                TenderId = lineItemTenderDtgViewV01.TenderId,
                TenderAmount = lineItemTenderDtgViewV01.TenderAmount,
                RoundingAmount = lineItemTenderDtgViewV01.RoundingAmount,
                TaxAmount = lineItemTenderDtgViewV01.TaxAmount,
                TipAmount = lineItemTenderDtgViewV01.TipAmount,
                OrderId = lineItemTenderDtgViewV01.OrderId
            };
        }
        #endregion
    }
}
