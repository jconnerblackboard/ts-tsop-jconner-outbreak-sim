﻿using System;
using BbTS.Domain.Models.Definitions.Transaction;
using BbTS.Domain.Models.System;

namespace BbTS.Domain.Models.ArtsDataModel
{
    /// <summary>
    /// Container class for a line item tender emv (credit card).
    /// </summary>
    public class LineItemTenderEmv : LineItemTender
    {
        /// <summary>
        /// Amount requested.
        /// </summary>
        public decimal AmountRequested { get; set; }

        /// <summary>
        /// Amount requested.
        /// </summary>
        public decimal AmountAuthorized { get; set; }

        /// <summary>
        /// The authorization code.
        /// </summary>
        public string AuthorizationCode { get; set; }

        /// <summary>
        /// The card id.
        /// </summary>
        public int CardId { get; set; }

        /// <summary>
        /// The card suffix.
        /// </summary>
        public string CardSuffix { get; set; }

        /// <summary>
        /// The command sequence.
        /// </summary>
        public int CommandSequence { get; set; }

        /// <summary>
        /// Transaction reference given by DPS (DpsTxnRef).
        /// </summary>
        public string DpsTransactionReference { get; set; }

        /// <summary>
        /// The identifier assigned to the emv terminal.
        /// </summary>
        public string EmvDeviceId { get; set; }

        /// <summary>
        /// The date and tiem the get 1 was performed at.
        /// </summary>
        public DateTime Get1DateTime { get; set; }

        /// <summary>
        /// The result token provided from the txnpur response.
        /// </summary>
        public ActionResultToken Get1Result { get; set; }

        /// <summary>
        /// The masked pan value.
        /// </summary>
        public string MaskedPan { get; set; }

        /// <summary>
        /// PosID and Transaction Number.
        /// </summary>
        public string MerchantReference { get; set; }

        /// <summary>
        /// The date and time of the payment express request.
        /// </summary>
        public DateTime PaymentExpressRequestDateTime { get; set; }

        /// <summary>
        /// The date and time of the payment express response.
        /// </summary>
        public DateTime PaymentExpressResponseDateTime { get; set; }

        /// <summary>
        /// The purchase response code (ReCo).
        /// </summary>
        public string PurchaseResponseCode { get; set; }

        /// <summary>
        /// The result token provided from the txnpur response.
        /// </summary>
        public ActionResultToken Result { get; set; }

        /// <summary>
        /// The settlement date and time.
        /// </summary>
        public DateTime SettlementDateTime { get; set; }

        /// <summary>
        /// The stan.
        /// </summary>
        public int Stan { get; set; }

        /// <summary>
        /// The transaction reference id (TxnRef).
        /// </summary>
        public string TransactionReference { get; set; }

        /// <summary>
        /// Deep Copy.
        /// </summary>
        /// <returns></returns>
        public override LineItem Clone()
        {
            return (LineItemTenderEmv)MemberwiseClone();
        }
    }

    /// <summary>
    /// View for a <see cref="LineItemTenderEmv"/>.
    /// </summary>
    public class LineItemTenderEmvViewV01
    {
        /// <summary>
        /// Indicates the order that this line item was added to the list and serves as a unique identifier for the line item.  Consider this a corrected LineItemSequenceNumber implementation.  Starts at 1.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Legacy sequence implementation.  Some incorrect implementation details prevent this from being used as a true sequence number, as is intended by the ARTS model.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// True if this line item has been voided.
        /// </summary>
        public bool VoidFlag { get; set; }

        /// <summary>
        /// The id of the tender.
        /// </summary>
        public int TenderId { get; set; }

        /// <summary>
        /// The tender amount.
        /// </summary>
        public decimal TenderAmount { get; set; }

        /// <summary>
        /// The rounding amount.
        /// </summary>
        public decimal RoundingAmount { get; set; }

        /// <summary>
        /// The type of tender this line item associates with.
        /// </summary>
        public TenderType TenderType { get; set; } = TenderType.CreditCard;

        /// <summary>
        /// Total tax associated with this tender line item.  If null, information about the tax is not available.
        /// </summary>
        public decimal? TaxAmount { get; set; }

        /// <summary>
        /// The amount of tip provided.
        /// </summary>
        public decimal TipAmount { get; set; }

        /// <summary>
        /// Amount requested.
        /// </summary>
        public decimal AmountRequested { get; set; }

        /// <summary>
        /// Amount requested.
        /// </summary>
        public decimal AmountAuthorized { get; set; }

        /// <summary>
        /// The authorization code.
        /// </summary>
        public string AuthorizationCode { get; set; }

        /// <summary>
        /// The card id.
        /// </summary>
        public int CardId { get; set; }

        /// <summary>
        /// The card suffix.
        /// </summary>
        public string CardSuffix { get; set; }

        /// <summary>
        /// The command sequence.
        /// </summary>
        public int CommandSequence { get; set; }

        /// <summary>
        /// Transaction reference given by DPS (DpsTxnRef).
        /// </summary>
        public string DpsTransactionReference { get; set; }

        /// <summary>
        /// The identifier assigned to the emv terminal.
        /// </summary>
        public string EmvDeviceId { get; set; }

        /// <summary>
        /// The date and tiem the get 1 was performed at.
        /// </summary>
        public DateTime Get1DateTime { get; set; }

        /// <summary>
        /// The result token provided from the txnpur response.
        /// </summary>
        public ActionResultTokenViewV01 Get1Result { get; set; }

        /// <summary>
        /// The masked pan value.
        /// </summary>
        public string MaskedPan { get; set; }

        /// <summary>
        /// PosID and Transaction Number.
        /// </summary>
        public string MerchantReference { get; set; }

        /// <summary>
        /// The date and time of the payment express request.
        /// </summary>
        public DateTime PaymentExpressRequestDateTime { get; set; }

        /// <summary>
        /// The date and time of the payment express response.
        /// </summary>
        public DateTime PaymentExpressResponseDateTime { get; set; }

        /// <summary>
        /// The purchase response code (ReCo).
        /// </summary>
        public string PurchaseResponseCode { get; set; }

        /// <summary>
        /// The result token provided from the txnpur response.
        /// </summary>
        public ActionResultTokenViewV01 Result { get; set; }

        /// <summary>
        /// The settlement date and time.
        /// </summary>
        public DateTime SettlementDateTime { get; set; }

        /// <summary>
        /// The stan.
        /// </summary>
        public int Stan { get; set; }

        /// <summary>
        /// The transaction reference id (TxnRef).
        /// </summary>
        public string TransactionReference { get; set; }
    }

    /// <summary>
    /// View for a <see cref="LineItemTenderEmv"/>.
    /// </summary>
    public class LineItemTenderEmvViewV02
    {
        /// <summary>
        /// Indicates the order that this line item was added to the list and serves as a unique identifier for the line item.  Consider this a corrected LineItemSequenceNumber implementation.  Starts at 1.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Legacy sequence implementation.  Some incorrect implementation details prevent this from being used as a true sequence number, as is intended by the ARTS model.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// True if this line item has been voided.
        /// </summary>
        public bool VoidFlag { get; set; }

        /// <summary>
        /// The id of the tender.
        /// </summary>
        public int TenderId { get; set; }

        /// <summary>
        /// The tender amount.
        /// </summary>
        public decimal TenderAmount { get; set; }

        /// <summary>
        /// The rounding amount.
        /// </summary>
        public decimal RoundingAmount { get; set; }

        /// <summary>
        /// Total tax associated with this tender line item.  If null, information about the tax is not available.
        /// </summary>
        public decimal? TaxAmount { get; set; }

        /// <summary>
        /// The amount of tip provided.
        /// </summary>
        public decimal TipAmount { get; set; }

        /// <summary>
        /// Amount requested.
        /// </summary>
        public decimal AmountRequested { get; set; }

        /// <summary>
        /// Amount requested.
        /// </summary>
        public decimal AmountAuthorized { get; set; }

        /// <summary>
        /// The authorization code.
        /// </summary>
        public string AuthorizationCode { get; set; }

        /// <summary>
        /// The card id.
        /// </summary>
        public int CardId { get; set; }

        /// <summary>
        /// The card suffix.
        /// </summary>
        public string CardSuffix { get; set; }

        /// <summary>
        /// The command sequence.
        /// </summary>
        public int CommandSequence { get; set; }

        /// <summary>
        /// Transaction reference given by DPS (DpsTxnRef).
        /// </summary>
        public string DpsTransactionReference { get; set; }

        /// <summary>
        /// The identifier assigned to the emv terminal.
        /// </summary>
        public string EmvDeviceId { get; set; }

        /// <summary>
        /// The date and tiem the get 1 was performed at.
        /// </summary>
        public DateTime Get1DateTime { get; set; }

        /// <summary>
        /// The result token provided from the txnpur response.
        /// </summary>
        public ActionResultTokenViewV01 Get1Result { get; set; }

        /// <summary>
        /// The masked pan value.
        /// </summary>
        public string MaskedPan { get; set; }

        /// <summary>
        /// PosID and Transaction Number.
        /// </summary>
        public string MerchantReference { get; set; }

        /// <summary>
        /// The date and time of the payment express request.
        /// </summary>
        public DateTime PaymentExpressRequestDateTime { get; set; }

        /// <summary>
        /// The date and time of the payment express response.
        /// </summary>
        public DateTime PaymentExpressResponseDateTime { get; set; }

        /// <summary>
        /// The purchase response code (ReCo).
        /// </summary>
        public string PurchaseResponseCode { get; set; }

        /// <summary>
        /// The result token provided from the txnpur response.
        /// </summary>
        public ActionResultTokenViewV01 Result { get; set; }

        /// <summary>
        /// The settlement date and time.
        /// </summary>
        public DateTime SettlementDateTime { get; set; }

        /// <summary>
        /// The stan.
        /// </summary>
        public int Stan { get; set; }

        /// <summary>
        /// The transaction reference id (TxnRef).
        /// </summary>
        public string TransactionReference { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="LineItemTenderEmv"/> conversion.
    /// </summary>
    public static class LineItemTenderEmvConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="LineItemTenderEmvViewV01"/> object based on this <see cref="LineItemTenderEmv"/>.
        /// </summary>
        /// <param name="lineItemTenderEmv"></param>
        /// <returns></returns>
        public static LineItemTenderEmvViewV01 ToLineItemTenderEmvViewV01(this LineItemTenderEmv lineItemTenderEmv)
        {
            if (lineItemTenderEmv == null) return null;

            return new LineItemTenderEmvViewV01
            {
                Sequence = lineItemTenderEmv.Sequence,
                LineItemSequenceNumber = lineItemTenderEmv.LineItemSequenceNumber,
                VoidFlag = lineItemTenderEmv.VoidFlag,
                TenderId = lineItemTenderEmv.TenderId,
                TenderAmount = lineItemTenderEmv.TenderAmount,
                RoundingAmount = lineItemTenderEmv.RoundingAmount,
                TenderType = TenderType.CreditCard,
                TaxAmount = lineItemTenderEmv.TaxAmount,
                TipAmount = lineItemTenderEmv.TipAmount,
                AmountRequested = lineItemTenderEmv.AmountRequested,
                AmountAuthorized = lineItemTenderEmv.AmountAuthorized,
                AuthorizationCode = lineItemTenderEmv.AuthorizationCode,
                CardId = lineItemTenderEmv.CardId,
                CardSuffix = lineItemTenderEmv.CardSuffix,
                CommandSequence = lineItemTenderEmv.CommandSequence,
                DpsTransactionReference = lineItemTenderEmv.DpsTransactionReference,
                EmvDeviceId = lineItemTenderEmv.EmvDeviceId,
                Get1DateTime = lineItemTenderEmv.Get1DateTime,
                Get1Result = lineItemTenderEmv.Get1Result.ToActionResultTokenViewV01(),
                MaskedPan = lineItemTenderEmv.MaskedPan,
                MerchantReference = lineItemTenderEmv.MerchantReference,
                PaymentExpressRequestDateTime = lineItemTenderEmv.PaymentExpressRequestDateTime,
                PaymentExpressResponseDateTime = lineItemTenderEmv.PaymentExpressResponseDateTime,
                PurchaseResponseCode = lineItemTenderEmv.PurchaseResponseCode,
                Result = lineItemTenderEmv.Result.ToActionResultTokenViewV01(),
                SettlementDateTime = lineItemTenderEmv.SettlementDateTime,
                Stan = lineItemTenderEmv.Stan,
                TransactionReference = lineItemTenderEmv.TransactionReference
            };
        }

        /// <summary>
        /// Returns a <see cref="LineItemTenderEmv"/> object based on this <see cref="LineItemTenderEmvViewV01"/>.
        /// </summary>
        /// <param name="lineItemTenderEmvViewV01"></param>
        /// <returns></returns>
        public static LineItemTenderEmv ToLineItemTenderEmv(this LineItemTenderEmvViewV01 lineItemTenderEmvViewV01)
        {
            if (lineItemTenderEmvViewV01 == null) return null;

            return new LineItemTenderEmv
            {
                Sequence = lineItemTenderEmvViewV01.Sequence,
                LineItemSequenceNumber = lineItemTenderEmvViewV01.LineItemSequenceNumber,
                VoidFlag = lineItemTenderEmvViewV01.VoidFlag,
                TenderId = lineItemTenderEmvViewV01.TenderId,
                TenderAmount = lineItemTenderEmvViewV01.TenderAmount,
                RoundingAmount = lineItemTenderEmvViewV01.RoundingAmount,
                TaxAmount = lineItemTenderEmvViewV01.TaxAmount,
                TipAmount = lineItemTenderEmvViewV01.TipAmount,
                AmountRequested = lineItemTenderEmvViewV01.AmountRequested,
                AmountAuthorized = lineItemTenderEmvViewV01.AmountAuthorized,
                AuthorizationCode = lineItemTenderEmvViewV01.AuthorizationCode,
                CardId = lineItemTenderEmvViewV01.CardId,
                CardSuffix = lineItemTenderEmvViewV01.CardSuffix,
                CommandSequence = lineItemTenderEmvViewV01.CommandSequence,
                DpsTransactionReference = lineItemTenderEmvViewV01.DpsTransactionReference,
                EmvDeviceId = lineItemTenderEmvViewV01.EmvDeviceId,
                Get1DateTime = lineItemTenderEmvViewV01.Get1DateTime,
                Get1Result = lineItemTenderEmvViewV01.Get1Result.ToActionResultToken(),
                MaskedPan = lineItemTenderEmvViewV01.MaskedPan,
                MerchantReference = lineItemTenderEmvViewV01.MerchantReference,
                PaymentExpressRequestDateTime = lineItemTenderEmvViewV01.PaymentExpressRequestDateTime,
                PaymentExpressResponseDateTime = lineItemTenderEmvViewV01.PaymentExpressResponseDateTime,
                PurchaseResponseCode = lineItemTenderEmvViewV01.PurchaseResponseCode,
                Result = lineItemTenderEmvViewV01.Result.ToActionResultToken(),
                SettlementDateTime = lineItemTenderEmvViewV01.SettlementDateTime,
                Stan = lineItemTenderEmvViewV01.Stan,
                TransactionReference = lineItemTenderEmvViewV01.TransactionReference
            };
        }
        #endregion

        #region Version 2
        /// <summary>
        /// Returns a <see cref="LineItemTenderEmvViewV02"/> object based on this <see cref="LineItemTenderEmv"/>.
        /// </summary>
        /// <param name="lineItemTenderEmv"></param>
        /// <returns></returns>
        public static LineItemTenderEmvViewV02 ToLineItemTenderEmvViewV02(this LineItemTenderEmv lineItemTenderEmv)
        {
            if (lineItemTenderEmv == null) return null;

            return new LineItemTenderEmvViewV02
            {
                Sequence = lineItemTenderEmv.Sequence,
                LineItemSequenceNumber = lineItemTenderEmv.LineItemSequenceNumber,
                VoidFlag = lineItemTenderEmv.VoidFlag,
                TenderId = lineItemTenderEmv.TenderId,
                TenderAmount = lineItemTenderEmv.TenderAmount,
                RoundingAmount = lineItemTenderEmv.RoundingAmount,
                TaxAmount = lineItemTenderEmv.TaxAmount,
                TipAmount = lineItemTenderEmv.TipAmount,
                AmountRequested = lineItemTenderEmv.AmountRequested,
                AmountAuthorized = lineItemTenderEmv.AmountAuthorized,
                AuthorizationCode = lineItemTenderEmv.AuthorizationCode,
                CardId = lineItemTenderEmv.CardId,
                CardSuffix = lineItemTenderEmv.CardSuffix,
                CommandSequence = lineItemTenderEmv.CommandSequence,
                DpsTransactionReference = lineItemTenderEmv.DpsTransactionReference,
                EmvDeviceId = lineItemTenderEmv.EmvDeviceId,
                Get1DateTime = lineItemTenderEmv.Get1DateTime,
                Get1Result = lineItemTenderEmv.Get1Result.ToActionResultTokenViewV01(),
                MaskedPan = lineItemTenderEmv.MaskedPan,
                MerchantReference = lineItemTenderEmv.MerchantReference,
                PaymentExpressRequestDateTime = lineItemTenderEmv.PaymentExpressRequestDateTime,
                PaymentExpressResponseDateTime = lineItemTenderEmv.PaymentExpressResponseDateTime,
                PurchaseResponseCode = lineItemTenderEmv.PurchaseResponseCode,
                Result = lineItemTenderEmv.Result.ToActionResultTokenViewV01(),
                SettlementDateTime = lineItemTenderEmv.SettlementDateTime,
                Stan = lineItemTenderEmv.Stan,
                TransactionReference = lineItemTenderEmv.TransactionReference
            };
        }

        /// <summary>
        /// Returns a <see cref="LineItemTenderEmv"/> object based on this <see cref="LineItemTenderEmvViewV02"/>.
        /// </summary>
        /// <param name="lineItemTenderEmvViewV02"></param>
        /// <returns></returns>
        public static LineItemTenderEmv ToLineItemTenderEmv(this LineItemTenderEmvViewV02 lineItemTenderEmvViewV02)
        {
            if (lineItemTenderEmvViewV02 == null) return null;

            return new LineItemTenderEmv
            {
                Sequence = lineItemTenderEmvViewV02.Sequence,
                LineItemSequenceNumber = lineItemTenderEmvViewV02.LineItemSequenceNumber,
                VoidFlag = lineItemTenderEmvViewV02.VoidFlag,
                TenderId = lineItemTenderEmvViewV02.TenderId,
                TenderAmount = lineItemTenderEmvViewV02.TenderAmount,
                RoundingAmount = lineItemTenderEmvViewV02.RoundingAmount,
                TaxAmount = lineItemTenderEmvViewV02.TaxAmount,
                TipAmount = lineItemTenderEmvViewV02.TipAmount,
                AmountRequested = lineItemTenderEmvViewV02.AmountRequested,
                AmountAuthorized = lineItemTenderEmvViewV02.AmountAuthorized,
                AuthorizationCode = lineItemTenderEmvViewV02.AuthorizationCode,
                CardId = lineItemTenderEmvViewV02.CardId,
                CardSuffix = lineItemTenderEmvViewV02.CardSuffix,
                CommandSequence = lineItemTenderEmvViewV02.CommandSequence,
                DpsTransactionReference = lineItemTenderEmvViewV02.DpsTransactionReference,
                EmvDeviceId = lineItemTenderEmvViewV02.EmvDeviceId,
                Get1DateTime = lineItemTenderEmvViewV02.Get1DateTime,
                Get1Result = lineItemTenderEmvViewV02.Get1Result.ToActionResultToken(),
                MaskedPan = lineItemTenderEmvViewV02.MaskedPan,
                MerchantReference = lineItemTenderEmvViewV02.MerchantReference,
                PaymentExpressRequestDateTime = lineItemTenderEmvViewV02.PaymentExpressRequestDateTime,
                PaymentExpressResponseDateTime = lineItemTenderEmvViewV02.PaymentExpressResponseDateTime,
                PurchaseResponseCode = lineItemTenderEmvViewV02.PurchaseResponseCode,
                Result = lineItemTenderEmvViewV02.Result.ToActionResultToken(),
                SettlementDateTime = lineItemTenderEmvViewV02.SettlementDateTime,
                Stan = lineItemTenderEmvViewV02.Stan,
                TransactionReference = lineItemTenderEmvViewV02.TransactionReference
            };
        }
        #endregion
    }
}
