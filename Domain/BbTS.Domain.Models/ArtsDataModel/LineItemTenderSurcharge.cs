﻿using System.Linq;
using BbTS.Domain.Models.Container.Elements;
using BbTS.Domain.Models.Definitions.ArtsDataModel;

namespace BbTS.Domain.Models.ArtsDataModel
{
    /// <summary>
    /// Line item tender surcharge.
    /// </summary>
    public class LineItemTenderSurcharge : LineItem
    {
        /// <summary>
        /// Sequence of the Line Item Tender that this surcharge is associated with.
        /// </summary>
        public int TenderSequence { get; set; }

        /// <summary>
        /// (Legacy) Indicates the order that this line item surcharge was applied.
        /// </summary>
        public int TenderLineItemSequenceNumber { get; set; }

        /// <summary>
        /// Discount surcharge reason.  The reason this surcharge was applied.
        /// </summary>
        public DiscountSurchargeReason DiscountSurchargeReason { get; set; }

        /// <summary>
        /// Surcharge rate.  25% surcharge is denoted as 0.25
        /// </summary>
        public decimal Rate { get; set; }

        /// <summary>
        /// Rounding amount.  Example:  A surcharge rate of 0.25 applied to a $0.19 total = $0.0475, which rounds to $0.05, so rounding amount will be 0.0025
        /// </summary>
        public decimal RoundingAmount { get; set; }

        /// <summary>
        /// Surcharge amount.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Prorated flag.
        /// </summary>
        public bool ProratedFlag { get; set; }

        /// <summary>
        /// The type of calculation.  Always Percent for this type of surcharge.
        /// </summary>
        public CalculationType CalculationType { get; set; }

        /// <inheritdoc />
        public override LineItem Clone()
        {
            return (LineItemTenderSurcharge)MemberwiseClone();
        }
    }

    /// <summary>
    /// View for a <see cref="LineItemTenderSurcharge"/>.  (Version 1)
    /// </summary>
    public class LineItemTenderSurchargeViewV01
    {
        /// <summary>
        /// Indicates the order that this line item was added to the list and serves as a unique identifier for the line item.  Consider this a corrected LineItemSequenceNumber implementation.  Starts at 1.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Legacy sequence implementation.  Some incorrect implementation details prevent this from being used as a true sequence number, as is intended by the ARTS model.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// True if this line item has been voided.
        /// </summary>
        public bool VoidFlag { get; set; }

        /// <summary>
        /// Sequence of the Line Item Tender that this discount is associated with.
        /// </summary>
        public int TenderSequence { get; set; }

        /// <summary>
        /// Discount surcharge reason.  The reason this discount was applied.
        /// </summary>
        public DiscountSurchargeReason DiscountSurchargeReason { get; set; }

        /// <summary>
        /// Surcharge rate.  25% discount is denoted as 0.25
        /// </summary>
        public decimal Rate { get; set; }

        /// <summary>
        /// Rounding amount.  Example: A discount rate of 0.25 applied to a $0.19 total = $0.0475, which rounds to $0.05, so rounding amount will be 0.0025
        /// </summary>
        public decimal RoundingAmount { get; set; }

        /// <summary>
        /// Surcharge amount.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Prorated flag.
        /// </summary>
        public bool ProratedFlag { get; set; }

        /// <summary>
        /// The type of calculation.  Always Percent for this type of discount.
        /// </summary>
        public CalculationType CalculationType { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="LineItemTenderSurcharge"/> conversion.
    /// </summary>
    public static class LineItemTenderSurchargeConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="LineItemTenderSurchargeViewV01"/> object based on this <see cref="LineItemTenderSurcharge"/>.
        /// </summary>
        /// <param name="lineItemTenderSurcharge"></param>
        /// <returns></returns>
        public static LineItemTenderSurchargeViewV01 ToLineItemTenderSurchargeViewV01(this LineItemTenderSurcharge lineItemTenderSurcharge)
        {
            if (lineItemTenderSurcharge == null) return null;

            return new LineItemTenderSurchargeViewV01
            {
                Sequence = lineItemTenderSurcharge.Sequence,
                LineItemSequenceNumber = lineItemTenderSurcharge.LineItemSequenceNumber,
                VoidFlag = lineItemTenderSurcharge.VoidFlag,
                TenderSequence = lineItemTenderSurcharge.TenderSequence,
                DiscountSurchargeReason = lineItemTenderSurcharge.DiscountSurchargeReason,
                Rate = lineItemTenderSurcharge.Rate,
                RoundingAmount = lineItemTenderSurcharge.RoundingAmount,
                Amount = lineItemTenderSurcharge.Amount,
                ProratedFlag = lineItemTenderSurcharge.ProratedFlag,
                CalculationType = lineItemTenderSurcharge.CalculationType
            };
        }

        /// <summary>
        /// Returns a <see cref="LineItemTenderSurcharge"/> object based on this <see cref="LineItemTenderSurchargeViewV01"/>.
        /// </summary>
        /// <param name="lineItemTenderSurchargeViewV01"></param>
        /// <returns></returns>
        public static LineItemTenderSurcharge ToLineItemTenderSurcharge(this LineItemTenderSurchargeViewV01 lineItemTenderSurchargeViewV01)
        {
            if (lineItemTenderSurchargeViewV01 == null) return null;

            return new LineItemTenderSurcharge
            {
                Sequence = lineItemTenderSurchargeViewV01.Sequence,
                LineItemSequenceNumber = lineItemTenderSurchargeViewV01.LineItemSequenceNumber,
                VoidFlag = lineItemTenderSurchargeViewV01.VoidFlag,
                TenderSequence = lineItemTenderSurchargeViewV01.TenderSequence,
                DiscountSurchargeReason = lineItemTenderSurchargeViewV01.DiscountSurchargeReason,
                Rate = lineItemTenderSurchargeViewV01.Rate,
                RoundingAmount = lineItemTenderSurchargeViewV01.RoundingAmount,
                Amount = lineItemTenderSurchargeViewV01.Amount,
                ProratedFlag = lineItemTenderSurchargeViewV01.ProratedFlag,
                CalculationType = lineItemTenderSurchargeViewV01.CalculationType
            };
        }
        #endregion
    }
}
