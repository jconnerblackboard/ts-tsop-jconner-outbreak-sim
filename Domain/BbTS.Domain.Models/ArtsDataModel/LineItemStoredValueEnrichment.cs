﻿namespace BbTS.Domain.Models.ArtsDataModel
{
    /// <summary>
    /// Line item enrichment.  (Poorly named.  Rename to LineItemEnrichment when opportunity arises.)
    /// </summary>
    public class LineItemStoredValueEnrichment : LineItem
    {
        /// <summary>
        /// Percentage.
        /// </summary>
        public decimal Percentage { get; set; }

        /// <summary>
        /// Rounding amount.
        /// </summary>
        public decimal RoundingAmount { get; set; }

        /// <summary>
        /// Amount.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Stored value enrichment sequence.
        /// </summary>
        public int StoredValueEnrichmentSequence { get; set; }

        /// <inheritdoc />
        public override LineItem Clone()
        {
            return (LineItemStoredValueEnrichment)MemberwiseClone();
        }
    }

    /// <summary>
    /// View for a <see cref="LineItemStoredValueEnrichment"/>.  (Version 1)
    /// </summary>
    public class LineItemEnrichmentViewV01
    {
        /// <summary>
        /// Indicates the order that this line item was added to the list and serves as a unique identifier for the line item.  Consider this a corrected LineItemSequenceNumber implementation.  Starts at 1.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Legacy sequence implementation.  Some incorrect implementation details prevent this from being used as a true sequence number, as is intended by the ARTS model.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// True if this line item has been voided.
        /// </summary>
        public bool VoidFlag { get; set; }

        /// <summary>
        /// Percentage.
        /// </summary>
        public decimal Percentage { get; set; }

        /// <summary>
        /// Rounding amount.
        /// </summary>
        public decimal RoundingAmount { get; set; }

        /// <summary>
        /// Amount.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Stored value enrichment sequence.
        /// </summary>
        public int StoredValueEnrichmentSequence { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="LineItemStoredValueEnrichment"/> conversion.
    /// </summary>
    public static class LineItemEnrichmentConverter
    {
        /// <summary>
        /// Returns a <see cref="LineItemEnrichmentViewV01"/> object based on this <see cref="LineItemStoredValueEnrichment"/>.
        /// </summary>
        /// <param name="lineItemEnrichment"></param>
        /// <returns></returns>
        public static LineItemEnrichmentViewV01 ToLineItemEnrichmentViewV01(this LineItemStoredValueEnrichment lineItemEnrichment)
        {
            if (lineItemEnrichment == null) return null;

            return new LineItemEnrichmentViewV01
            {
                Sequence = lineItemEnrichment.Sequence,
                LineItemSequenceNumber = lineItemEnrichment.LineItemSequenceNumber,
                VoidFlag = lineItemEnrichment.VoidFlag,
                Percentage = lineItemEnrichment.Percentage,
                RoundingAmount = lineItemEnrichment.RoundingAmount,
                Amount = lineItemEnrichment.Amount,
                StoredValueEnrichmentSequence = lineItemEnrichment.StoredValueEnrichmentSequence
            };
        }

        /// <summary>
        /// Returns a <see cref="LineItemStoredValueEnrichment"/> object based on this <see cref="LineItemEnrichmentViewV01"/>.
        /// </summary>
        /// <param name="lineItemEnrichmentViewV01"></param>
        /// <returns></returns>
        public static LineItemStoredValueEnrichment ToLineItemEnrichment(this LineItemEnrichmentViewV01 lineItemEnrichmentViewV01)
        {
            if (lineItemEnrichmentViewV01 == null) return null;

            return new LineItemStoredValueEnrichment
            {
                Sequence = lineItemEnrichmentViewV01.Sequence,
                LineItemSequenceNumber = lineItemEnrichmentViewV01.LineItemSequenceNumber,
                VoidFlag = lineItemEnrichmentViewV01.VoidFlag,
                Percentage = lineItemEnrichmentViewV01.Percentage,
                RoundingAmount = lineItemEnrichmentViewV01.RoundingAmount,
                Amount = lineItemEnrichmentViewV01.Amount,
                StoredValueEnrichmentSequence = lineItemEnrichmentViewV01.StoredValueEnrichmentSequence
            };
        }
    }
}
