﻿using BbTS.Domain.Models.Credential;
using BbTS.Domain.Models.Definitions.Transaction;

namespace BbTS.Domain.Models.ArtsDataModel
{
    /// <summary>
    /// Container class for a line item cash equivalence.
    /// </summary>
    public class LineItemTenderCashEquivalence : LineItemTender
    {
        /// <summary>
        /// Customer credentials.
        /// </summary>
        public CustomerCredential CustomerCredential { get; set; }

        /// <summary>
        /// Amount limit.
        /// </summary>
        public decimal CashEquivalenceAmountLimit { get; set; }

        /// <summary>
        /// Board meal type id.
        /// </summary>
        public int BoardMealTypeId { get; set; }

        /// <summary>
        /// Is this a guest meal.
        /// </summary>
        public bool IsGuestMeal { get; set; }

        /// <summary>
        /// Deep Copy.
        /// </summary>
        /// <returns></returns>
        public override LineItem Clone()
        {
            var lineItemTenderCashEquivalence = (LineItemTenderCashEquivalence)MemberwiseClone();
            lineItemTenderCashEquivalence.CustomerCredential = CustomerCredential.Clone();
            return lineItemTenderCashEquivalence;
        }
    }

    /// <summary>
    /// View for a <see cref="LineItemTenderCashEquivalence"/>.  (Version 1)
    /// </summary>
    public class LineItemTenderCashEquivalenceViewV01
    {
        /// <summary>
        /// Indicates the order that this line item was added to the list and serves as a unique identifier for the line item.  Consider this a corrected LineItemSequenceNumber implementation.  Starts at 1.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Legacy sequence implementation.  Some incorrect implementation details prevent this from being used as a true sequence number, as is intended by the ARTS model.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// True if this line item has been voided.
        /// </summary>
        public bool VoidFlag { get; set; }

        /// <summary>
        /// The id of the tender.
        /// </summary>
        public int TenderId { get; set; }

        /// <summary>
        /// The tender amount.
        /// </summary>
        public decimal TenderAmount { get; set; }

        /// <summary>
        /// The rounding amount.
        /// </summary>
        public decimal RoundingAmount { get; set; }

        /// <summary>
        /// The type of tender this line item associates with.
        /// </summary>
        public TenderType TenderType { get; set; } = TenderType.BoardCashEquiv;

        /// <summary>
        /// Total tax associated with this tender line item.  If null, information about the tax is not available.
        /// </summary>
        public decimal? TaxAmount { get; set; }

        /// <summary>
        /// The amount of tip provided.
        /// </summary>
        public decimal TipAmount { get; set; }

        /// <summary>
        /// Amount limit.
        /// </summary>
        public decimal CashEquivalenceAmountLimit { get; set; }

        /// <summary>
        /// Board meal type id.
        /// </summary>
        public int BoardMealTypeId { get; set; }

        /// <summary>
        /// Is this a guest meal.
        /// </summary>
        public bool IsGuestMeal { get; set; }

        /// <summary>
        /// Customer credentials.
        /// </summary>
        public CustomerCredentialViewV01 CustomerCredential { get; set; }
    }

    /// <summary>
    /// View for a <see cref="LineItemTenderCashEquivalence"/>.  (Version 2)
    /// </summary>
    public class LineItemTenderCashEquivalenceViewV02
    {
        /// <summary>
        /// Indicates the order that this line item was added to the list and serves as a unique identifier for the line item.  Consider this a corrected LineItemSequenceNumber implementation.  Starts at 1.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Legacy sequence implementation.  Some incorrect implementation details prevent this from being used as a true sequence number, as is intended by the ARTS model.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// True if this line item has been voided.
        /// </summary>
        public bool VoidFlag { get; set; }

        /// <summary>
        /// The id of the tender.
        /// </summary>
        public int TenderId { get; set; }

        /// <summary>
        /// The tender amount.
        /// </summary>
        public decimal TenderAmount { get; set; }

        /// <summary>
        /// The rounding amount.
        /// </summary>
        public decimal RoundingAmount { get; set; }

        /// <summary>
        /// Total tax associated with this tender line item.  If null, information about the tax is not available.
        /// </summary>
        public decimal? TaxAmount { get; set; }

        /// <summary>
        /// The amount of tip provided.
        /// </summary>
        public decimal TipAmount { get; set; }

        /// <summary>
        /// Amount limit.
        /// </summary>
        public decimal CashEquivalenceAmountLimit { get; set; }

        /// <summary>
        /// Board meal type id.
        /// </summary>
        public int BoardMealTypeId { get; set; }

        /// <summary>
        /// Is this a guest meal.
        /// </summary>
        public bool IsGuestMeal { get; set; }

        /// <summary>
        /// Customer credentials.
        /// </summary>
        public CustomerCredentialViewV02 CustomerCredential { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="LineItemTenderCashEquivalence"/> conversion.
    /// </summary>
    public static class LineItemTenderCashEquivalenceConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="LineItemTenderCashEquivalenceViewV01"/> object based on this <see cref="LineItemTenderCashEquivalence"/>.
        /// </summary>
        /// <param name="lineItemTenderCashEquivalence"></param>
        /// <returns></returns>
        public static LineItemTenderCashEquivalenceViewV01 ToLineItemTenderCashEquivalenceViewV01(this LineItemTenderCashEquivalence lineItemTenderCashEquivalence)
        {
            if (lineItemTenderCashEquivalence == null) return null;

            return new LineItemTenderCashEquivalenceViewV01
            {
                Sequence = lineItemTenderCashEquivalence.Sequence,
                LineItemSequenceNumber = lineItemTenderCashEquivalence.LineItemSequenceNumber,
                VoidFlag = lineItemTenderCashEquivalence.VoidFlag,
                TenderId = lineItemTenderCashEquivalence.TenderId,
                TenderAmount = lineItemTenderCashEquivalence.TenderAmount,
                RoundingAmount = lineItemTenderCashEquivalence.RoundingAmount,
                TenderType = TenderType.BoardCashEquiv,
                TaxAmount = lineItemTenderCashEquivalence.TaxAmount,
                TipAmount = lineItemTenderCashEquivalence.TipAmount,
                CashEquivalenceAmountLimit = lineItemTenderCashEquivalence.CashEquivalenceAmountLimit,
                BoardMealTypeId = lineItemTenderCashEquivalence.BoardMealTypeId,
                IsGuestMeal = lineItemTenderCashEquivalence.IsGuestMeal,
                CustomerCredential = lineItemTenderCashEquivalence.CustomerCredential.ToCustomerCredentialViewV01()
            };
        }

        /// <summary>
        /// Returns a <see cref="LineItemTenderCashEquivalence"/> object based on this <see cref="LineItemTenderCashEquivalenceViewV01"/>.
        /// </summary>
        /// <param name="lineItemTenderCashEquivalenceViewV01"></param>
        /// <returns></returns>
        public static LineItemTenderCashEquivalence ToLineItemTenderCashEquivalence(this LineItemTenderCashEquivalenceViewV01 lineItemTenderCashEquivalenceViewV01)
        {
            if (lineItemTenderCashEquivalenceViewV01 == null) return null;

            return new LineItemTenderCashEquivalence
            {
                Sequence = lineItemTenderCashEquivalenceViewV01.Sequence,
                LineItemSequenceNumber = lineItemTenderCashEquivalenceViewV01.LineItemSequenceNumber,
                VoidFlag = lineItemTenderCashEquivalenceViewV01.VoidFlag,
                TenderId = lineItemTenderCashEquivalenceViewV01.TenderId,
                TenderAmount = lineItemTenderCashEquivalenceViewV01.TenderAmount,
                RoundingAmount = lineItemTenderCashEquivalenceViewV01.RoundingAmount,
                TaxAmount = lineItemTenderCashEquivalenceViewV01.TaxAmount,
                TipAmount = lineItemTenderCashEquivalenceViewV01.TipAmount,
                CashEquivalenceAmountLimit = lineItemTenderCashEquivalenceViewV01.CashEquivalenceAmountLimit,
                BoardMealTypeId = lineItemTenderCashEquivalenceViewV01.BoardMealTypeId,
                IsGuestMeal = lineItemTenderCashEquivalenceViewV01.IsGuestMeal,
                CustomerCredential = lineItemTenderCashEquivalenceViewV01.CustomerCredential.ToCustomerCredential()
            };
        }
        #endregion

        #region Version 2
        /// <summary>
        /// Returns a <see cref="LineItemTenderCashEquivalenceViewV02"/> object based on this <see cref="LineItemTenderCashEquivalence"/>.
        /// </summary>
        /// <param name="lineItemTenderCashEquivalence"></param>
        /// <returns></returns>
        public static LineItemTenderCashEquivalenceViewV02 ToLineItemTenderCashEquivalenceViewV02(this LineItemTenderCashEquivalence lineItemTenderCashEquivalence)
        {
            if (lineItemTenderCashEquivalence == null) return null;

            return new LineItemTenderCashEquivalenceViewV02
            {
                Sequence = lineItemTenderCashEquivalence.Sequence,
                LineItemSequenceNumber = lineItemTenderCashEquivalence.LineItemSequenceNumber,
                VoidFlag = lineItemTenderCashEquivalence.VoidFlag,
                TenderId = lineItemTenderCashEquivalence.TenderId,
                TenderAmount = lineItemTenderCashEquivalence.TenderAmount,
                RoundingAmount = lineItemTenderCashEquivalence.RoundingAmount,
                TaxAmount = lineItemTenderCashEquivalence.TaxAmount,
                TipAmount = lineItemTenderCashEquivalence.TipAmount,
                CashEquivalenceAmountLimit = lineItemTenderCashEquivalence.CashEquivalenceAmountLimit,
                BoardMealTypeId = lineItemTenderCashEquivalence.BoardMealTypeId,
                IsGuestMeal = lineItemTenderCashEquivalence.IsGuestMeal,
                CustomerCredential = lineItemTenderCashEquivalence.CustomerCredential.ToCustomerCredentialViewV02()
            };
        }

        /// <summary>
        /// Returns a <see cref="LineItemTenderCashEquivalence"/> object based on this <see cref="LineItemTenderCashEquivalenceViewV02"/>.
        /// </summary>
        /// <param name="lineItemTenderCashEquivalenceViewV02"></param>
        /// <returns></returns>
        public static LineItemTenderCashEquivalence ToLineItemTenderCashEquivalence(this LineItemTenderCashEquivalenceViewV02 lineItemTenderCashEquivalenceViewV02)
        {
            if (lineItemTenderCashEquivalenceViewV02 == null) return null;

            return new LineItemTenderCashEquivalence
            {
                Sequence = lineItemTenderCashEquivalenceViewV02.Sequence,
                LineItemSequenceNumber = lineItemTenderCashEquivalenceViewV02.LineItemSequenceNumber,
                VoidFlag = lineItemTenderCashEquivalenceViewV02.VoidFlag,
                TenderId = lineItemTenderCashEquivalenceViewV02.TenderId,
                TenderAmount = lineItemTenderCashEquivalenceViewV02.TenderAmount,
                RoundingAmount = lineItemTenderCashEquivalenceViewV02.RoundingAmount,
                TaxAmount = lineItemTenderCashEquivalenceViewV02.TaxAmount,
                TipAmount = lineItemTenderCashEquivalenceViewV02.TipAmount,
                CashEquivalenceAmountLimit = lineItemTenderCashEquivalenceViewV02.CashEquivalenceAmountLimit,
                BoardMealTypeId = lineItemTenderCashEquivalenceViewV02.BoardMealTypeId,
                IsGuestMeal = lineItemTenderCashEquivalenceViewV02.IsGuestMeal,
                CustomerCredential = lineItemTenderCashEquivalenceViewV02.CustomerCredential.ToCustomerCredential()
            };
        }
        #endregion
    }
}
