﻿namespace BbTS.Domain.Models.ArtsDataModel
{
    /// <summary>
    /// ARTS line item comment.
    /// </summary>
    public class LineItemComment : LineItem
    {
        /// <summary>
        /// Comment text.
        /// </summary>
        public string CommentText { get; set; }

        /// <inheritdoc />
        public override LineItem Clone()
        {
            return (LineItemComment)MemberwiseClone();
        }
    }

    /// <summary>
    /// View for a <see cref="LineItemComment"/>.  (Version 1)
    /// </summary>
    public class LineItemCommentViewV01
    {
        /// <summary>
        /// Indicates the order that this line item was added to the list and serves as a unique identifier for the line item.  Consider this a corrected LineItemSequenceNumber implementation.  Starts at 1.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Legacy sequence implementation.  Some incorrect implementation details prevent this from being used as a true sequence number, as is intended by the ARTS model.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// True if this line item has been voided.
        /// </summary>
        public bool VoidFlag { get; set; }

        /// <summary>
        /// Comment text.
        /// </summary>
        public string CommentText { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="LineItemComment"/> conversion.
    /// </summary>
    public static class LineItemCommentConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="LineItemCommentViewV01"/> object based on this <see cref="LineItemComment"/>.
        /// </summary>
        /// <param name="lineItemComment"></param>
        /// <returns></returns>
        public static LineItemCommentViewV01 ToLineItemCommentViewV01(this LineItemComment lineItemComment)
        {
            if (lineItemComment == null) return null;

            return new LineItemCommentViewV01
            {
                Sequence = lineItemComment.Sequence,
                LineItemSequenceNumber = lineItemComment.LineItemSequenceNumber,
                VoidFlag = lineItemComment.VoidFlag,
                CommentText = lineItemComment.CommentText
            };
        }

        /// <summary>
        /// Returns a <see cref="LineItemComment"/> object based on this <see cref="LineItemCommentViewV01"/>.
        /// </summary>
        /// <param name="lineItemCommentViewV01"></param>
        /// <returns></returns>
        public static LineItemComment ToLineItemComment(this LineItemCommentViewV01 lineItemCommentViewV01)
        {
            if (lineItemCommentViewV01 == null) return null;

            return new LineItemComment
            {
                Sequence = lineItemCommentViewV01.Sequence,
                LineItemSequenceNumber = lineItemCommentViewV01.LineItemSequenceNumber,
                VoidFlag = lineItemCommentViewV01.VoidFlag,
                CommentText = lineItemCommentViewV01.CommentText
            };
        }
        #endregion
    }
}
