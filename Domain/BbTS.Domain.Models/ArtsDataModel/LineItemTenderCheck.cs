﻿using BbTS.Domain.Models.Definitions.Transaction;

namespace BbTS.Domain.Models.ArtsDataModel
{
    /// <summary>
    /// Container class for a line item tender check.
    /// </summary>
    public class LineItemTenderCheck : LineItemTender
    {
        /// <summary>
        /// Deep Copy.
        /// </summary>
        /// <returns></returns>
        public override LineItem Clone()
        {
            return (LineItemTenderCheck)MemberwiseClone();
        }
    }

    /// <summary>
    /// View for a <see cref="LineItemTenderCheck"/>.  (Version 1)
    /// </summary>
    public class LineItemTenderCheckViewV01
    {
        /// <summary>
        /// Indicates the order that this line item was added to the list and serves as a unique identifier for the line item.  Consider this a corrected LineItemSequenceNumber implementation.  Starts at 1.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Legacy sequence implementation.  Some incorrect implementation details prevent this from being used as a true sequence number, as is intended by the ARTS model.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// True if this line item has been voided.
        /// </summary>
        public bool VoidFlag { get; set; }

        /// <summary>
        /// The id of the tender.
        /// </summary>
        public int TenderId { get; set; }

        /// <summary>
        /// The tender amount.
        /// </summary>
        public decimal TenderAmount { get; set; }

        /// <summary>
        /// The rounding amount.
        /// </summary>
        public decimal RoundingAmount { get; set; }

        /// <summary>
        /// The type of tender this line item associates with.
        /// </summary>
        public TenderType TenderType { get; set; } = TenderType.Check;

        /// <summary>
        /// Total tax associated with this tender line item.  If null, information about the tax is not available.
        /// </summary>
        public decimal? TaxAmount { get; set; }

        /// <summary>
        /// The amount of tip provided.
        /// </summary>
        public decimal TipAmount { get; set; }
    }

    /// <summary>
    /// View for a <see cref="LineItemTenderCheck"/>.  (Version 2)
    /// </summary>
    public class LineItemTenderCheckViewV02
    {
        /// <summary>
        /// Indicates the order that this line item was added to the list and serves as a unique identifier for the line item.  Consider this a corrected LineItemSequenceNumber implementation.  Starts at 1.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Legacy sequence implementation.  Some incorrect implementation details prevent this from being used as a true sequence number, as is intended by the ARTS model.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// True if this line item has been voided.
        /// </summary>
        public bool VoidFlag { get; set; }

        /// <summary>
        /// The id of the tender.
        /// </summary>
        public int TenderId { get; set; }

        /// <summary>
        /// The tender amount.
        /// </summary>
        public decimal TenderAmount { get; set; }

        /// <summary>
        /// The rounding amount.
        /// </summary>
        public decimal RoundingAmount { get; set; }

        /// <summary>
        /// Total tax associated with this tender line item.  If null, information about the tax is not available.
        /// </summary>
        public decimal? TaxAmount { get; set; }

        /// <summary>
        /// The amount of tip provided.
        /// </summary>
        public decimal TipAmount { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="LineItemTenderCheck"/> conversion.
    /// </summary>
    public static class LineItemTenderCheckConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="LineItemTenderCheckViewV01"/> object based on this <see cref="LineItemTenderCheck"/>.
        /// </summary>
        /// <param name="lineItemTenderCheck"></param>
        /// <returns></returns>
        public static LineItemTenderCheckViewV01 ToLineItemTenderCheckViewV01(this LineItemTenderCheck lineItemTenderCheck)
        {
            if (lineItemTenderCheck == null) return null;

            return new LineItemTenderCheckViewV01
            {
                Sequence = lineItemTenderCheck.Sequence,
                LineItemSequenceNumber = lineItemTenderCheck.LineItemSequenceNumber,
                VoidFlag = lineItemTenderCheck.VoidFlag,
                TenderId = lineItemTenderCheck.TenderId,
                TenderAmount = lineItemTenderCheck.TenderAmount,
                RoundingAmount = lineItemTenderCheck.RoundingAmount,
                TenderType = TenderType.Check,
                TaxAmount = lineItemTenderCheck.TaxAmount,
                TipAmount = lineItemTenderCheck.TipAmount,
            };
        }

        /// <summary>
        /// Returns a <see cref="LineItemTenderCheck"/> object based on this <see cref="LineItemTenderCheckViewV01"/>.
        /// </summary>
        /// <param name="lineItemTenderCheckViewV01"></param>
        /// <returns></returns>
        public static LineItemTenderCheck ToLineItemTenderCheck(this LineItemTenderCheckViewV01 lineItemTenderCheckViewV01)
        {
            if (lineItemTenderCheckViewV01 == null) return null;

            return new LineItemTenderCheck
            {
                Sequence = lineItemTenderCheckViewV01.Sequence,
                LineItemSequenceNumber = lineItemTenderCheckViewV01.LineItemSequenceNumber,
                VoidFlag = lineItemTenderCheckViewV01.VoidFlag,
                TenderId = lineItemTenderCheckViewV01.TenderId,
                TenderAmount = lineItemTenderCheckViewV01.TenderAmount,
                RoundingAmount = lineItemTenderCheckViewV01.RoundingAmount,
                TaxAmount = lineItemTenderCheckViewV01.TaxAmount,
                TipAmount = lineItemTenderCheckViewV01.TipAmount,
            };
        }
        #endregion

        #region Version 2
        /// <summary>
        /// Returns a <see cref="LineItemTenderCheckViewV02"/> object based on this <see cref="LineItemTenderCheck"/>.
        /// </summary>
        /// <param name="lineItemTenderCheck"></param>
        /// <returns></returns>
        public static LineItemTenderCheckViewV02 ToLineItemTenderCheckViewV02(this LineItemTenderCheck lineItemTenderCheck)
        {
            if (lineItemTenderCheck == null) return null;

            return new LineItemTenderCheckViewV02
            {
                Sequence = lineItemTenderCheck.Sequence,
                LineItemSequenceNumber = lineItemTenderCheck.LineItemSequenceNumber,
                VoidFlag = lineItemTenderCheck.VoidFlag,
                TenderId = lineItemTenderCheck.TenderId,
                TenderAmount = lineItemTenderCheck.TenderAmount,
                RoundingAmount = lineItemTenderCheck.RoundingAmount,
                TaxAmount = lineItemTenderCheck.TaxAmount,
                TipAmount = lineItemTenderCheck.TipAmount,
            };
        }

        /// <summary>
        /// Returns a <see cref="LineItemTenderCheck"/> object based on this <see cref="LineItemTenderCheckViewV02"/>.
        /// </summary>
        /// <param name="lineItemTenderCheckViewV02"></param>
        /// <returns></returns>
        public static LineItemTenderCheck ToLineItemTenderCheck(this LineItemTenderCheckViewV02 lineItemTenderCheckViewV02)
        {
            if (lineItemTenderCheckViewV02 == null) return null;

            return new LineItemTenderCheck
            {
                Sequence = lineItemTenderCheckViewV02.Sequence,
                LineItemSequenceNumber = lineItemTenderCheckViewV02.LineItemSequenceNumber,
                VoidFlag = lineItemTenderCheckViewV02.VoidFlag,
                TenderId = lineItemTenderCheckViewV02.TenderId,
                TenderAmount = lineItemTenderCheckViewV02.TenderAmount,
                RoundingAmount = lineItemTenderCheckViewV02.RoundingAmount,
                TaxAmount = lineItemTenderCheckViewV02.TaxAmount,
                TipAmount = lineItemTenderCheckViewV02.TipAmount,
            };
        }
        #endregion
    }
}
