﻿using BbTS.Domain.Models.Definitions.ArtsDataModel;

namespace BbTS.Domain.Models.ArtsDataModel
{
    /// <summary>
    /// Line item product tax.
    /// </summary>
    public class LineItemProductTax : LineItem
    {
        //public int ProductSequenceNumber { get; set; }  // LineItemSequenceNumber of the product that this product tax belongs to
        
        /// <summary>
        /// Tax schedule ID.
        /// </summary>
        public int TaxScheduleId { get; set; }

        /// <summary>
        /// Tax account name.
        /// </summary>
        public string TaxAccountName { get; set; }

        /// <summary>
        /// Taxable percent.
        /// </summary>
        public decimal TaxablePercent { get; set; }

        /// <summary>
        /// Taxable amount.
        /// </summary>
        public decimal TaxableAmount { get; set; }

        /// <summary>
        /// Tax percent.
        /// </summary>
        public decimal TaxPercent { get; set; }

        /// <summary>
        /// Tax amount.
        /// </summary>
        public decimal TaxAmount { get; set; }

        /// <summary>
        /// Tax exempt override type.
        /// </summary>
        public TaxExemptOverrideType TaxExemptOverrideType { get; set; }

        /// <inheritdoc />
        public override LineItem Clone()
        {
            return (LineItemProductTax)MemberwiseClone();
        }
    }

    /// <summary>
    /// View for a <see cref="LineItemProductTax"/>.  (Version 1)
    /// </summary>
    public class LineItemProductTaxViewV01
    {
        /// <summary>
        /// Indicates the order that this line item was added to the list and serves as a unique identifier for the line item.  Consider this a corrected LineItemSequenceNumber implementation.  Starts at 1.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Legacy sequence implementation.  Some incorrect implementation details prevent this from being used as a true sequence number, as is intended by the ARTS model.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// True if this line item has been voided.
        /// </summary>
        public bool VoidFlag { get; set; }

        /// <summary>
        /// Tax schedule ID.
        /// </summary>
        public int TaxScheduleId { get; set; }

        /// <summary>
        /// Tax account name.
        /// </summary>
        public string TaxAccountName { get; set; }

        /// <summary>
        /// Taxable percent.
        /// </summary>
        public decimal TaxablePercent { get; set; }

        /// <summary>
        /// Taxable amount.
        /// </summary>
        public decimal TaxableAmount { get; set; }

        /// <summary>
        /// Tax percent.
        /// </summary>
        public decimal TaxPercent { get; set; }

        /// <summary>
        /// Tax amount.
        /// </summary>
        public decimal TaxAmount { get; set; }

        /// <summary>
        /// Tax exempt override type.
        /// </summary>
        public TaxExemptOverrideType TaxExemptOverrideType { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="LineItemProductTax"/> conversion.
    /// </summary>
    public static class LineItemProductTaxConverter
    {
        /// <summary>
        /// Returns a <see cref="LineItemProductTaxViewV01"/> object based on this <see cref="LineItemProductTax"/>.
        /// </summary>
        /// <param name="lineItemProductTax"></param>
        /// <returns></returns>
        public static LineItemProductTaxViewV01 ToLineItemProductTaxViewV01(this LineItemProductTax lineItemProductTax)
        {
            if (lineItemProductTax == null) return null;

            return new LineItemProductTaxViewV01
            {
                Sequence = lineItemProductTax.Sequence,
                LineItemSequenceNumber = lineItemProductTax.LineItemSequenceNumber,
                VoidFlag = lineItemProductTax.VoidFlag,
                TaxScheduleId = lineItemProductTax.TaxScheduleId,
                TaxAccountName = lineItemProductTax.TaxAccountName,
                TaxablePercent = lineItemProductTax.TaxablePercent,
                TaxableAmount = lineItemProductTax.TaxableAmount,
                TaxPercent = lineItemProductTax.TaxPercent,
                TaxAmount = lineItemProductTax.TaxAmount,
                TaxExemptOverrideType = lineItemProductTax.TaxExemptOverrideType
            };
        }

        /// <summary>
        /// Returns a <see cref="LineItemProductTax"/> object based on this <see cref="LineItemProductTaxViewV01"/>.
        /// </summary>
        /// <param name="lineItemProductTaxViewV01"></param>
        /// <returns></returns>
        public static LineItemProductTax ToLineItemProductTax(this LineItemProductTaxViewV01 lineItemProductTaxViewV01)
        {
            if (lineItemProductTaxViewV01 == null) return null;

            return new LineItemProductTax
            {
                Sequence = lineItemProductTaxViewV01.Sequence,
                LineItemSequenceNumber = lineItemProductTaxViewV01.LineItemSequenceNumber,
                VoidFlag = lineItemProductTaxViewV01.VoidFlag,
                TaxScheduleId = lineItemProductTaxViewV01.TaxScheduleId,
                TaxAccountName = lineItemProductTaxViewV01.TaxAccountName,
                TaxablePercent = lineItemProductTaxViewV01.TaxablePercent,
                TaxableAmount = lineItemProductTaxViewV01.TaxableAmount,
                TaxPercent = lineItemProductTaxViewV01.TaxPercent,
                TaxAmount = lineItemProductTaxViewV01.TaxAmount,
                TaxExemptOverrideType = lineItemProductTaxViewV01.TaxExemptOverrideType
            };
        }
    }
}
