﻿namespace BbTS.Domain.Models.ArtsDataModel
{
    /// <summary>
    /// Line item product promotion.
    /// </summary>
    public class LineItemProductPromotion : LineItem
    {
        /// <summary>
        /// Product promotion ID.
        /// </summary>
        public int ProductPromotionId { get; set; }

        /// <inheritdoc />
        public override LineItem Clone()
        {
            return (LineItemProductPromotion)MemberwiseClone();
        }
    }

    /// <summary>
    /// View for a <see cref="LineItemProductPromotion"/>.  (Version 1)
    /// </summary>
    public class LineItemProductPromotionViewV01
    {
        /// <summary>
        /// Indicates the order that this line item was added to the list and serves as a unique identifier for the line item.  Consider this a corrected LineItemSequenceNumber implementation.  Starts at 1.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Legacy sequence implementation.  Some incorrect implementation details prevent this from being used as a true sequence number, as is intended by the ARTS model.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// True if this line item has been voided.
        /// </summary>
        public bool VoidFlag { get; set; }

        /// <summary>
        /// Product promotion ID.
        /// </summary>
        public int ProductPromotionId { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="LineItemProductPromotion"/> conversion.
    /// </summary>
    public static class LineItemProductPromotionConverter
    {
        /// <summary>
        /// Returns a <see cref="LineItemProductPromotionViewV01"/> object based on this <see cref="LineItemProductPromotion"/>.
        /// </summary>
        /// <param name="lineItemProductPromotion"></param>
        /// <returns></returns>
        public static LineItemProductPromotionViewV01 ToLineItemProductPromotionViewV01(this LineItemProductPromotion lineItemProductPromotion)
        {
            if (lineItemProductPromotion == null) return null;

            return new LineItemProductPromotionViewV01
            {
                Sequence = lineItemProductPromotion.Sequence,
                LineItemSequenceNumber = lineItemProductPromotion.LineItemSequenceNumber,
                VoidFlag = lineItemProductPromotion.VoidFlag,
                ProductPromotionId = lineItemProductPromotion.ProductPromotionId
            };
        }

        /// <summary>
        /// Returns a <see cref="LineItemProductPromotion"/> object based on this <see cref="LineItemProductPromotionViewV01"/>.
        /// </summary>
        /// <param name="lineItemProductPromotionViewV01"></param>
        /// <returns></returns>
        public static LineItemProductPromotion ToLineItemProductPromotion(this LineItemProductPromotionViewV01 lineItemProductPromotionViewV01)
        {
            if (lineItemProductPromotionViewV01 == null) return null;

            return new LineItemProductPromotion
            {
                Sequence = lineItemProductPromotionViewV01.Sequence,
                LineItemSequenceNumber = lineItemProductPromotionViewV01.LineItemSequenceNumber,
                VoidFlag = lineItemProductPromotionViewV01.VoidFlag,
                ProductPromotionId = lineItemProductPromotionViewV01.ProductPromotionId
            };
        }
    }
}
