﻿using BbTS.Domain.Models.Definitions.ArtsDataModel;

namespace BbTS.Domain.Models.ArtsDataModel
{
    /// <summary>
    /// Line item product tax override.
    /// </summary>
    public class LineItemProductTaxOverride : LineItem
    {
        //public int ProductSequenceNumber { get; set; }  // LineItemSequenceNumber of the product that this product tax override data belongs to

        /// <summary>
        /// Tax override reason.
        /// </summary>
        public TaxOverrideReason TaxOverrideReason { get; set; }

        /// <summary>
        /// Taxable amount.
        /// </summary>
        public decimal TaxableAmount { get; set; }

        /// <summary>
        /// Original tax amount.
        /// </summary>
        public decimal OriginalTaxAmount { get; set; }

        /// <summary>
        /// New tax amount.
        /// </summary>
        public decimal NewTaxAmount { get; set; }

        /// <summary>
        /// Original tax percent.
        /// </summary>
        public decimal OriginalTaxPercent { get; set; }

        /// <summary>
        /// New tax percent.
        /// </summary>
        public decimal NewTaxPercent { get; set; }

        /// <inheritdoc />
        public override LineItem Clone()
        {
            return (LineItemProductTaxOverride)MemberwiseClone();
        }
    }

    /// <summary>
    /// View for a <see cref="LineItemProductTaxOverride"/>.  (Version 1)
    /// </summary>
    public class LineItemProductTaxOverrideViewV01
    {
        /// <summary>
        /// Indicates the order that this line item was added to the list and serves as a unique identifier for the line item.  Consider this a corrected LineItemSequenceNumber implementation.  Starts at 1.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Legacy sequence implementation.  Some incorrect implementation details prevent this from being used as a true sequence number, as is intended by the ARTS model.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// True if this line item has been voided.
        /// </summary>
        public bool VoidFlag { get; set; }

        /// <summary>
        /// Tax override reason.
        /// </summary>
        public TaxOverrideReason TaxOverrideReason { get; set; }

        /// <summary>
        /// Taxable amount.
        /// </summary>
        public decimal TaxableAmount { get; set; }

        /// <summary>
        /// Original tax amount.
        /// </summary>
        public decimal OriginalTaxAmount { get; set; }

        /// <summary>
        /// New tax amount.
        /// </summary>
        public decimal NewTaxAmount { get; set; }

        /// <summary>
        /// Original tax percent.
        /// </summary>
        public decimal OriginalTaxPercent { get; set; }

        /// <summary>
        /// New tax percent.
        /// </summary>
        public decimal NewTaxPercent { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="LineItemProductTaxOverride"/> conversion.
    /// </summary>
    public static class LineItemProductTaxOverrideConverter
    {
        /// <summary>
        /// Returns a <see cref="LineItemProductTaxOverrideViewV01"/> object based on this <see cref="LineItemProductTaxOverride"/>.
        /// </summary>
        /// <param name="lineItemProductTaxOverride"></param>
        /// <returns></returns>
        public static LineItemProductTaxOverrideViewV01 ToLineItemProductTaxOverrideViewV01(this LineItemProductTaxOverride lineItemProductTaxOverride)
        {
            if (lineItemProductTaxOverride == null) return null;

            return new LineItemProductTaxOverrideViewV01
            {
                Sequence = lineItemProductTaxOverride.Sequence,
                LineItemSequenceNumber = lineItemProductTaxOverride.LineItemSequenceNumber,
                VoidFlag = lineItemProductTaxOverride.VoidFlag,
                TaxOverrideReason = lineItemProductTaxOverride.TaxOverrideReason,
                TaxableAmount = lineItemProductTaxOverride.TaxableAmount,
                OriginalTaxAmount = lineItemProductTaxOverride.OriginalTaxAmount,
                NewTaxAmount = lineItemProductTaxOverride.NewTaxAmount,
                OriginalTaxPercent = lineItemProductTaxOverride.OriginalTaxPercent,
                NewTaxPercent = lineItemProductTaxOverride.NewTaxPercent
            };
        }

        /// <summary>
        /// Returns a <see cref="LineItemProductTaxOverride"/> object based on this <see cref="LineItemProductTaxOverrideViewV01"/>.
        /// </summary>
        /// <param name="lineItemProductTaxOverrideViewV01"></param>
        /// <returns></returns>
        public static LineItemProductTaxOverride ToLineItemProductTaxOverride(this LineItemProductTaxOverrideViewV01 lineItemProductTaxOverrideViewV01)
        {
            if (lineItemProductTaxOverrideViewV01 == null) return null;

            return new LineItemProductTaxOverride
            {
                Sequence = lineItemProductTaxOverrideViewV01.Sequence,
                LineItemSequenceNumber = lineItemProductTaxOverrideViewV01.LineItemSequenceNumber,
                VoidFlag = lineItemProductTaxOverrideViewV01.VoidFlag,
                TaxOverrideReason = lineItemProductTaxOverrideViewV01.TaxOverrideReason,
                TaxableAmount = lineItemProductTaxOverrideViewV01.TaxableAmount,
                OriginalTaxAmount = lineItemProductTaxOverrideViewV01.OriginalTaxAmount,
                NewTaxAmount = lineItemProductTaxOverrideViewV01.NewTaxAmount,
                OriginalTaxPercent = lineItemProductTaxOverrideViewV01.OriginalTaxPercent,
                NewTaxPercent = lineItemProductTaxOverrideViewV01.NewTaxPercent
            };
        }
    }
}
