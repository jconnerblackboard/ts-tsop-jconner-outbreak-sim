﻿using System;
using BbTS.Domain.Models.Definitions.Transaction;

namespace BbTS.Domain.Models.ArtsDataModel
{
    /// <summary>
    /// Container class for a line item tender.
    /// </summary>
    public abstract class LineItemTender : LineItem
    {
        /// <summary>
        /// The id of the tender.
        /// </summary>
        public int TenderId { get; set; }

        /// <summary>
        /// The tender amount.
        /// </summary>
        public decimal TenderAmount { get; set; }

        /// <summary>
        /// The rounding amount.
        /// </summary>
        public decimal RoundingAmount { get; set; }

        /// <summary>
        /// Total tax associated with this tender line item.  If null, information about the tax is not available.
        /// </summary>
        public decimal? TaxAmount { get; set; }

        /// <summary>
        /// The amount of tip provided.
        /// </summary>
        public decimal TipAmount { get; set; }
    }

    /// <summary>
    /// View for a <see cref="LineItemTender"/>.  (Version 1)
    /// </summary>
    public class LineItemTenderViewV01
    {
        /// <summary>
        /// Indicates the order that this line item was added to the list and serves as a unique identifier for the line item.  Consider this a corrected LineItemSequenceNumber implementation.  Starts at 1.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Legacy sequence implementation.  Some incorrect implementation details prevent this from being used as a true sequence number, as is intended by the ARTS model.
        /// </summary>
        public int LineItemSequenceNumber { get; set; }

        /// <summary>
        /// True if this line item has been voided.
        /// </summary>
        public bool VoidFlag { get; set; }

        /// <summary>
        /// The id of the tender.
        /// </summary>
        public int TenderId { get; set; }

        /// <summary>
        /// The tender amount.
        /// </summary>
        public decimal TenderAmount { get; set; }

        /// <summary>
        /// The rounding amount.
        /// </summary>
        public decimal RoundingAmount { get; set; }

        /// <summary>
        /// The type of tender this line item associates with.
        /// </summary>
        public TenderType TenderType { get; set; }

        /// <summary>
        /// Total tax associated with this tender line item.  If null, information about the tax is not available.
        /// </summary>
        public decimal? TaxAmount { get; set; }

        /// <summary>
        /// The amount of tip provided.
        /// </summary>
        public decimal TipAmount { get; set; }

        /// <summary>
        /// The cash equivalence specific information if this transaction is a cash equivalence transaction.
        /// </summary>
        public LineItemTenderCashEquivalenceViewV01 LineItemTenderCashEquivalence { get; set; }

        /// <summary>
        /// The stored value specific information if this transaction is a stored value transaction.
        /// </summary>
        public LineItemTenderStoredValueViewV01 LineItemTenderWithCustomer { get; set; }

        /// <summary>
        /// The emv specific information if this transaction is an emv transaction.
        /// </summary>
        public LineItemTenderEmvViewV01 LineItemTenderEmv { get; set; }

        /// <summary>
        /// Get this line item as a line item tender cash.
        /// </summary>
        public LineItemTenderCashViewV01 LineItemTenderCash => TenderType != TenderType.Cash ? null : new LineItemTenderCashViewV01
        {
            Sequence = Sequence,
            LineItemSequenceNumber = LineItemSequenceNumber,
            VoidFlag = VoidFlag,
            TenderId = TenderId,
            TenderAmount = TenderAmount,
            RoundingAmount = RoundingAmount,
            TenderType = TenderType.Cash,
            TaxAmount = TaxAmount,
            TipAmount = TipAmount
        };

        /// <summary>
        /// Get this line item as a line item tender check.
        /// </summary>
        public LineItemTenderCheckViewV01 LineItemTenderCheck => TenderType != TenderType.Check ? null : new LineItemTenderCheckViewV01
        {
            Sequence = Sequence,
            LineItemSequenceNumber = LineItemSequenceNumber,
            VoidFlag = VoidFlag,
            TenderId = TenderId,
            TenderAmount = TenderAmount,
            RoundingAmount = RoundingAmount,
            TenderType = TenderType.Check,
            TaxAmount = TaxAmount,
            TipAmount = TipAmount
        };
    }

    /// <summary>
    /// Extension methods for <see cref="LineItemTender"/> conversion.
    /// </summary>
    public static class LineItemTenderConverter
    {
        /// <summary>
        /// Returns the tender type based on <see cref="LineItemTender"/> descendant type.
        /// </summary>
        /// <param name="lineItemTender"></param>
        /// <returns></returns>
        public static TenderType TenderTypeGet(this LineItemTender lineItemTender)
        {
            var lineItemTenderCash = lineItemTender as LineItemTenderCash;
            if (lineItemTenderCash != null) return TenderType.Cash;

            var lineItemTenderCheck = lineItemTender as LineItemTenderCheck;
            if (lineItemTenderCheck != null) return TenderType.Check;

            var lineItemTenderStoredValue = lineItemTender as LineItemTenderWithCustomer;
            if (lineItemTenderStoredValue != null) return TenderType.StoredValue;

            var lineItemTenderCashEquivalence = lineItemTender as LineItemTenderCashEquivalence;
            if (lineItemTenderCashEquivalence != null) return TenderType.BoardCashEquiv;

            var lineItemTenderEmv = lineItemTender as LineItemTenderEmv;
            if (lineItemTenderEmv != null) return TenderType.CreditCard;

            var lineItemTenderEmvHit = lineItemTender as LineItemTenderEmvHit;
            if (lineItemTenderEmvHit != null) return TenderType.CreditCard;

            return TenderType.Unknown;
        }

        #region Version 1
        /// <summary>
        /// Returns a <see cref="LineItemTenderViewV01"/> object based on this <see cref="LineItemTender"/>.
        /// </summary>
        /// <param name="lineItemTender"></param>
        /// <returns></returns>
        public static LineItemTenderViewV01 ToLineItemTenderViewV01(this LineItemTender lineItemTender)
        {
            if (lineItemTender == null) return null;

            //LineItemTenderCash lineItemTenderCash = null;
            //LineItemTenderCheck lineItemTenderCheck = null;
            LineItemTenderWithCustomer lineItemTenderStoredValue = null;
            LineItemTenderCashEquivalence lineItemTenderCashEquivalence = null;
            LineItemTenderEmv lineItemTenderEmv = null;

            var tenderType = lineItemTender.TenderTypeGet();
            switch (tenderType)
            {
                case TenderType.Unknown:
                    return null;
                case TenderType.Cash:
                    //lineItemTenderCash = lineItemTender as LineItemTenderCash;
                    break;
                case TenderType.Check:
                    //lineItemTenderCheck = lineItemTender as LineItemTenderCheck;
                    break;
                case TenderType.StoredValue:
                    lineItemTenderStoredValue = lineItemTender as LineItemTenderWithCustomer;
                    break;
                case TenderType.CreditCard:
                    lineItemTenderEmv = lineItemTender as LineItemTenderEmv;
                    break;
                case TenderType.BoardCashEquiv:
                    lineItemTenderCashEquivalence = lineItemTender as LineItemTenderCashEquivalence;
                    break;
                default:
                    return null;
            }

            return new LineItemTenderViewV01
            {
                Sequence = lineItemTender.Sequence,
                LineItemSequenceNumber = lineItemTender.LineItemSequenceNumber,
                VoidFlag = lineItemTender.VoidFlag,
                TenderId = lineItemTender.TenderId,
                TenderAmount = lineItemTender.TenderAmount,
                RoundingAmount = lineItemTender.RoundingAmount,
                TenderType = tenderType,
                TaxAmount = lineItemTender.TaxAmount,
                TipAmount = lineItemTender.TipAmount,
                LineItemTenderCashEquivalence = lineItemTenderCashEquivalence.ToLineItemTenderCashEquivalenceViewV01(),
                LineItemTenderWithCustomer = lineItemTenderStoredValue.ToLineItemTenderStoredValueViewV01(),
                LineItemTenderEmv = lineItemTenderEmv.ToLineItemTenderEmvViewV01(),
                //LineItemTenderCash = lineItemTenderCash.ToLineItemTenderCashViewV01(),
                //LineItemTenderCheck = lineItemTenderCheck.ToLineItemTenderCheckViewV01()
            };
        }

        /// <summary>
        /// Returns a <see cref="LineItemTender"/> object based on this <see cref="LineItemTenderViewV01"/>.
        /// </summary>
        /// <param name="lineItemTenderViewV01"></param>
        /// <returns></returns>
        public static LineItemTender ToLineItemTender(this LineItemTenderViewV01 lineItemTenderViewV01)
        {
            if (lineItemTenderViewV01 == null) return null;

            var tenderType = lineItemTenderViewV01.TenderType;
            switch (tenderType)
            {
                case TenderType.Unknown:
                    return null;
                case TenderType.Cash:
                    return lineItemTenderViewV01.LineItemTenderCash.ToLineItemTenderCash();
                case TenderType.Check:
                    return lineItemTenderViewV01.LineItemTenderCheck.ToLineItemTenderCheck();
                case TenderType.StoredValue:
                    return lineItemTenderViewV01.LineItemTenderWithCustomer.ToLineItemTenderStoredValue();
                case TenderType.CreditCard:
                    return lineItemTenderViewV01.LineItemTenderEmv.ToLineItemTenderEmv();
                case TenderType.BoardCashEquiv:
                    return lineItemTenderViewV01.LineItemTenderCashEquivalence.ToLineItemTenderCashEquivalence();
                default:
                    return null;
            }
        }
        #endregion
    }
}
