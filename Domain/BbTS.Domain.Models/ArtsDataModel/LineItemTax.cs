﻿using BbTS.Domain.Models.Definitions.ArtsDataModel;

namespace BbTS.Domain.Models.ArtsDataModel
{
    public class LineItemTax : LineItem
    {
        public int TaxKeyNumber { get; set; }
        public string TaxKeyName { get; set; }
        public CalculationType TaxType { get; set; }
        public decimal TaxablePercent { get; set; }
        public decimal TaxableAmount { get; set; }
        public decimal TaxPercent { get; set; }
        public decimal TaxAmount { get; set; }

        public override LineItem Clone()
        {
            return (LineItemTax)MemberwiseClone();
        }
    }
}
