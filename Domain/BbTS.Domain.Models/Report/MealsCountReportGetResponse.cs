﻿
namespace BbTS.Domain.Models.Report
{
    /// <summary>
    /// Meals count report get response
    /// </summary>
    public class MealsCountReportGetResponse
    {
        /// <summary>
        /// Meals count report
        /// </summary>
        public MealsCountReport MealsCountReport { get; set; }
    }
}
