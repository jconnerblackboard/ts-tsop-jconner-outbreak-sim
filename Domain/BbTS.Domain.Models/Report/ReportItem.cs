﻿namespace BbTS.Domain.Models.Report
{
    /// <summary>
    /// Report item - typically represents a single line item on the report.
    /// </summary>
    public class ReportItem
    {
        /// <summary>
        /// Text to accompany the value.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Count.
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Determines if count should be shown.
        /// </summary>
        public bool ShowCount { get; set; }

        /// <summary>
        /// Monetary amount to show.
        /// </summary>
        public decimal Value { get; set; }

        /// <summary>
        /// Determines if value should be shown.
        /// </summary>
        public bool ShowValue { get; set; }
    }
}