﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Report
{
    /// <summary>
    /// Pos audit report class.
    /// </summary>
    public class PosAuditReport : DailyReport
    {
        /// <summary>
        /// Control total breakdown.  Starting with the provided control total value, subtract all other values.
        /// This result, the "previous control total", should be compared by a manager to the control total that was printed on a prior report.
        /// Change in control total (which is equal to the control total - previous control total) can also be shown on the report.
        /// </summary>
        public ControlTotalBreakdown ControlTotalBreakdown { get; set; }
    }
}