﻿
namespace BbTS.Domain.Models.Report
{
    /// <summary>
    /// Drawer audit get response
    /// </summary>
    public class DrawerAuditGetResponse
    {
        /// <summary>
        /// The unique identifier associated with this session.
        /// </summary>
        public string SessionGuid { get; set; }

        /// <summary>
        /// Drawer report
        /// </summary>
        public TsDrawerReport DrawerReport { get; set; }
    }
}
