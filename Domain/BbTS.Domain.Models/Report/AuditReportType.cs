﻿namespace BbTS.Domain.Models.Report
{
    /// <summary>
    /// Type of audit report.
    /// </summary>
    public enum AuditReportType
    {
        /// <summary>
        /// Drawer audit report.
        /// </summary>
        Drawer,

        /// <summary>
        /// Originator (POS) audit report.
        /// </summary>
        Pos,

        /// <summary>
        /// Profit center audit report.
        /// </summary>
        ProfitCenter
    }
}