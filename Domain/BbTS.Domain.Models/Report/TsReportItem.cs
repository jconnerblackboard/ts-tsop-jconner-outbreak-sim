﻿using System.Xml.Serialization;

namespace BbTS.Domain.Models.Report
{
    /// <summary>
    /// Represents Item in report
    /// </summary>
    public class TsReportItem
    {
        /// <summary>
        /// Type
        /// </summary>
        [XmlAttribute]
        public string Type { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [XmlAttribute]
        public string Name { get; set; }

        /// <summary>
        /// Value
        /// </summary>
        [XmlAttribute]
        public decimal Value { get; set; }

        /// <summary>
        /// Count
        /// </summary>
        [XmlAttribute]
        public int Count { get; set; }

        /// <summary>
        /// Type of value
        /// </summary>
        [XmlAttribute]
        public string ValueType { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        [XmlAttribute]
        public string Description { get; set; }

        /// <summary>
        /// Display Order
        /// </summary>
        [XmlAttribute]
        public int DisplayOrder { get; set; }
    }
}
