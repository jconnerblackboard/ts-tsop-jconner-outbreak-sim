﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Report
{
    /// <summary>
    /// Enhanced reporting domain data get response container
    /// </summary>
    public class EnhancedReportingListGetResponse
    {
        /// <summary>
        /// Sv payments
        /// </summary>
        public List<DeviceReader> ReportingList { get; set; }

    }
}
