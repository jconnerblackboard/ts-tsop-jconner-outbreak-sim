﻿using System.Xml.Serialization;

namespace BbTS.Domain.Models.Report
{
    /// <summary>
    /// Drawer report
    /// </summary>
    public class TsDrawerReport : TsReport
    {
        /// <summary>
        /// Cashier name
        /// </summary>
        [XmlElement]
        public string CashierName { get; set; }

        /// <summary>
        /// Cash drawer number
        /// </summary>
        [XmlElement]
        public int CashDrawerNumber { get; set; }

        /// <summary>
        /// Session Start
        /// </summary>
        [XmlElement]
        public string SessionStart { get; set; }

        /// <summary>
        /// Session End
        /// </summary>
        [XmlElement]
        public string SessionEnd { get; set; }
    }
}
