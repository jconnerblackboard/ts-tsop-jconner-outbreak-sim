﻿using System.Xml.Serialization;

namespace BbTS.Domain.Models.Report
{
    /// <summary>
    /// Day report
    /// </summary>
    public class TsDailyReport : TsReport
    {
        /// <summary>
        /// For Date
        /// </summary>
        [XmlElement]
        public string ForDate { get; set; }

        /// <summary>
        /// Current Date
        /// </summary>
        [XmlElement]
        public string CurrentDate { get; set; }

        /// <summary>
        /// Control total information needed for POS Audit Report.  Leave null for Profit Center Audit Report.
        /// </summary>
        [XmlElement]
        public ControlTotalBreakdown ControlTotalBreakdown { get; set; }
    }
}
