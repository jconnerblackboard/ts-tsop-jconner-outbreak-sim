﻿using System;

namespace BbTS.Domain.Models.Report
{
    /// <summary>
    /// Report for meals served
    /// </summary>
    public class MealsCountReport
    {
        /// <summary>
        /// Board Period
        /// </summary>
        public Int16 BoardPeriod { get; set; }

        /// <summary>
        /// Board Period Name
        /// </summary>
        public string BoardPeriodName { get; set; }

        /// <summary>
        /// POS/PC Name
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// Meals served for current day
        /// </summary>
        public int MealsCountDay { get; set; }

        /// <summary>
        /// Meals served for current period
        /// </summary>
        public int MealsCountPeriod { get; set; }
    }
}
