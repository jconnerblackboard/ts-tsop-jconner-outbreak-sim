﻿using BbTS.Domain.Models.System;
using System.Collections.Generic;

namespace BbTS.Domain.Models.Report
{
    /// <summary>
    /// Enhanced reporting domain data get response container
    /// </summary>
    public class EnhancedReportingDomainDataGetResponse
    {
        /// <summary>
        /// Sv payments
        /// </summary>
        public List<DomainView> SvPayments { get; set; }

        /// <summary>
        /// Sv payments
        /// </summary>
        public List<DomainView> PrivEvents { get; set; }

        /// <summary>
        /// Sv payments
        /// </summary>
        public List<DomainView> DoorAccesses { get; set; }
    }
}
