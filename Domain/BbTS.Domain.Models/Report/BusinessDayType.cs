﻿namespace BbTS.Domain.Models.Report
{
    /// <summary>
    /// Business day type used for POS Audit and Profit Center Audit reports.
    /// </summary>
    public enum BusinessDayType
    {
        /// <summary>
        /// Today.
        /// </summary>
        Today = 1,

        /// <summary>
        /// Yesterday.
        /// </summary>
        Yesterday = 2
    }
}
