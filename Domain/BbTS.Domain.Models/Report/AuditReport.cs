﻿namespace BbTS.Domain.Models.Report
{
    /// <summary>
    /// Audit report.
    /// </summary>
    public abstract class AuditReport
    {
        /// <summary>
        /// Merchant name.
        /// </summary>
        public string MerchantName { get; set; }

        /// <summary>
        /// Profit center name.
        /// </summary>
        public string ProfitCenterName { get; set; }

        /// <summary>
        /// Originator (POS) name.
        /// </summary>
        public string OriginatorName { get; set; }

        /// <summary>
        /// Sales item group.
        /// </summary>
        public ReportItemGroup Sales { get; set; }

        /// <summary>
        /// Deposits item group.
        /// </summary>
        public ReportItemGroup Deposits { get; set; }

        /// <summary>
        /// Total revenue (sales and deposits).
        /// </summary>
        public ReportItem TotalRevenue { get; set; }

        /// <summary>
        /// Taxes item group.
        /// </summary>
        public ReportItemGroup Taxes { get; set; }

        /// <summary>
        /// Discounts item group.
        /// </summary>
        public ReportItemGroup Discounts { get; set; }

        /// <summary>
        /// Surcharges item group.
        /// </summary>
        public ReportItemGroup Surcharges { get; set; }

        /// <summary>
        /// Promotions item group.
        /// </summary>
        public ReportItemGroup Promotions { get; set; }

        /// <summary>
        /// Tenders item group.
        /// </summary>
        public ReportItemGroup Tenders { get; set; }

        /// <summary>
        /// Other item group.
        /// </summary>
        public ReportItemGroup Other { get; set; }
    }
}