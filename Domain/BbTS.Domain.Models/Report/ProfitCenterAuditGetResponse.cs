﻿
namespace BbTS.Domain.Models.Report
{
    /// <summary>
    /// Profit Center audit get response
    /// </summary>
    public class ProfitCenterAuditGetResponse
    {
        /// <summary>
        /// The unique (Guid) identifier for the profit center.
        /// </summary>
        public string ProfitCenterGuid { get; set; }

        /// <summary>
        /// Report
        /// </summary>
        public TsDailyReport ProfitCenterReport { get; set; }
    }
}
