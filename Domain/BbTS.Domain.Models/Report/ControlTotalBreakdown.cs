﻿using System.Xml.Serialization;

namespace BbTS.Domain.Models.Report
{
    /// <summary>
    /// Control total breakdown.  Used as part of the POS Audit Report as a fraud detection tool.
    /// </summary>
    public class ControlTotalBreakdown
    {
        /// <summary>
        /// Equal to the ControlTotalAfterTransaction value of the most recent ARTS transaction included in the report.
        /// </summary>
        [XmlElement]
        public decimal ControlTotal { get; set; }

        /// <summary>
        /// Purchases.
        /// </summary>
        [XmlElement]
        public decimal Purchases { get; set; }

        /// <summary>
        /// Returns.
        /// </summary>
        [XmlElement]
        public decimal Returns { get; set; }

        /// <summary>
        /// Deposits.
        /// </summary>
        [XmlElement]
        public decimal Deposits { get; set; }

        /// <summary>
        /// Deposit returns.
        /// </summary>
        [XmlElement]
        public decimal DepositReturns { get; set; }

        /// <summary>
        /// Stored value enrichments.
        /// </summary>
        [XmlElement]
        public decimal StoredValueEnrichments { get; set; }

        /// <summary>
        /// Discounts.
        /// </summary>
        [XmlElement]
        public decimal Discounts { get; set; }

        /// <summary>
        /// Surcharges.
        /// </summary>
        [XmlElement]
        public decimal Surcharges { get; set; }

        /// <summary>
        /// Promotions.
        /// </summary>
        [XmlElement]
        public decimal Promotions { get; set; }

        /// <summary>
        /// Cancelled transactions.
        /// </summary>
        [XmlElement]
        public decimal CancelledTransactions { get; set; }

        /// <summary>
        /// Tax collected.
        /// </summary>
        [XmlElement]
        public decimal TaxCollected { get; set; }

        /// <summary>
        /// Tax returned.
        /// </summary>
        [XmlElement]
        public decimal TaxReturned { get; set; }

        /// <summary>
        /// Rejected.
        /// </summary>
        [XmlElement]
        public decimal Rejected { get; set; }

        /// <summary>
        /// Tender rounding.
        /// </summary>
        [XmlElement]
        public decimal TenderRounding { get; set; }
    }
}