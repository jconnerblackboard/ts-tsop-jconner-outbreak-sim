﻿using System;

namespace BbTS.Domain.Models.Report
{
    /// <summary>
    /// Drawer audit report class.
    /// </summary>
    public class DrawerAuditReport : AuditReport
    {
        /// <summary>
        /// Operator name.
        /// </summary>
        public string OperatorName { get; set; }

        /// <summary>
        /// Cash drawer number.
        /// </summary>
        public int CashDrawerNumber { get; set; }

        /// <summary>
        /// Session start date time.
        /// </summary>
        public DateTimeOffset SessionStart { get; set; }

        /// <summary>
        /// Session end date time.
        /// </summary>
        public DateTimeOffset SessionEnd { get; set; }

        /// <summary>
        /// True if the most recent transaction in this session is an operator sign-off transaction.
        /// </summary>
        public bool IsSessionFinished { get; set; }

        /// <summary>
        /// Session guid.
        /// </summary>
        public string SessionGuid { get; set; }
    }
}
