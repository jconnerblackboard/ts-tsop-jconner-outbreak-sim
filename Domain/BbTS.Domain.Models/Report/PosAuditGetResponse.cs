﻿
namespace BbTS.Domain.Models.Report
{
    /// <summary>
    /// Pos audit get response
    /// </summary>
    public class PosAuditGetResponse
    {
        /// <summary>
        /// The unique identifier for the device.
        /// </summary>
        public string OriginatorGuid { get; set; }

        /// <summary>
        /// Report
        /// </summary>
        public TsDailyReport PosReport { get; set; }
    }
}
