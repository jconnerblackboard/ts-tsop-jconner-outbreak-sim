﻿using System;

namespace BbTS.Domain.Models.Report
{
    /// <summary>
    /// Provides an overview of a session.
    /// </summary>
    public class SessionSummary
    {
        /// <summary>
        /// Session guid.
        /// </summary>
        public string SessionId { get; set; }

        /// <summary>
        /// Operator guid.
        /// </summary>
        public string OperatorId { get; set; }

        /// <summary>
        /// Start date/time of the oldest transaction in this session.
        /// </summary>
        public DateTimeOffset? OldestTransactionStartDateTime { get; set; }

        /// <summary>
        /// Finish date/time of the newest transaction in this session.
        /// </summary>
        public DateTimeOffset? NewestTransactionFinishDateTime { get; set; }

        /// <summary>
        /// Total transactions included in this session, including sign-on and sign-off transactions.
        /// </summary>
        public int TransactionCount { get; set; }

        /// <summary>
        /// True if the most recent transaction in this session is an operator sign-off transaction.
        /// </summary>
        public bool IsSessionFinished { get; set; }
    }
}
