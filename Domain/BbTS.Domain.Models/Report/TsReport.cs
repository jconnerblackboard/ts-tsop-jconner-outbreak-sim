﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Report
{
    /// <summary>
    /// Report
    /// </summary>
    public class TsReport
    {
        /// <summary>
        /// Report type
        /// </summary>
        [XmlElement]
        public virtual string ReportType { get; set; }

        /// <summary>
        /// POS Name
        /// </summary>
        [XmlElement]
        public string PosName { get; set; }

        /// <summary>
        /// Profit Center Name
        /// </summary>
        [XmlElement]
        public string ProfitCenterName { get; set; }

        /// <summary>
        /// Tenders
        /// </summary>
        [XmlArray, XmlArrayItem("Item", typeof(TsReportItem))]
        public List<TsReportItem> Items { get; set; }
    }
}
