﻿using BbTS.Domain.Models.Definitions.Report;
using System;

namespace BbTS.Domain.Models.Report
{
    /// <summary>
    /// Represents DeviceReaderObjectDefinedView element from db
    /// </summary>
    public class DeviceReader
    {
        /// <summary>
        /// Object Id
        /// </summary>
        public int ObjectId { get; set; }

        /// <summary>
        /// Object name
        /// </summary>
        public string ObjectName { get; set; }

        /// <summary>
        /// Object type id
        /// </summary>
        public ObjectTypes ObjectType { get; set; }

        /// <summary>
        /// Object type name
        /// </summary>
        public string ObjectTypeName { get; set; }

        /// <summary>
        /// Merchant Id
        /// </summary>
        public Int16 MerchantId { get; set; }

        /// <summary>
        /// MerchantName
        /// </summary>
        public string MerchantName { get; set; }

        /// <summary>
        /// Profit Center Id
        /// </summary>
        public int? ProfitCenterId { get; set; }

        /// <summary>
        /// Profit Center Name
        /// </summary>
        public string ProfitCenterName { get; set; }

        /// <summary>
        /// Building Id
        /// </summary>
        public int? BuildingId { get; set; }

        /// <summary>
        /// Building name
        /// </summary>
        public string BuildingName { get; set; }

        /// <summary>
        /// Area Id
        /// </summary>
        public int? AreaId { get; set; }

        /// <summary>
        /// Area name
        /// </summary>
        public string AreaName { get; set; }

        /// <summary>
        /// Sv Payments Id
        /// </summary>
        public int? SvPaymentId { get; set; }

        /// <summary>
        /// Sv Payment Code
        /// </summary>
        public string SvPaymentCode { get; set; }

        /// <summary>
        /// Sv Payment Name
        /// </summary>
        public string SvPaymentName { get; set; }

        /// <summary>
        /// Sv Payment object defined field Id
        /// </summary>
        public int SvPaymentObjDefFieldId { get; set; }

        /// <summary>
        /// Priv Event Id
        /// </summary>
        public int? PrivEventId { get; set; }

        /// <summary>
        /// Priv Event Code
        /// </summary>
        public string PrivEventCode { get; set; }

        /// <summary>
        /// Priv Event Name
        /// </summary>
        public string PrivEventName { get; set; }

        /// <summary>
        /// Priv Event object defined field Id
        /// </summary>
        public int PrivEventObjDefFieldId { get; set; }

        /// <summary>
        /// Door Access Id
        /// </summary>
        public int? DoorAccessId { get; set; }

        /// <summary>
        /// Door Access Code
        /// </summary>
        public string DoorAccessCode { get; set; }

        /// <summary>
        /// Door Access Name
        /// </summary>
        public string DoorAccessName { get; set; }

        /// <summary>
        /// Door Access object defined field Id
        /// </summary>
        public int DoorAccessObjDefFieldId { get; set; }
    }
}
