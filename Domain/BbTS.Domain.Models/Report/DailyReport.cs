﻿using System;

namespace BbTS.Domain.Models.Report
{
    /// <summary>
    /// Base class for PosAuditReport and ProfitCenterAuditReport
    /// </summary>
    public abstract class DailyReport : AuditReport
    {
        /// <summary>
        /// Specifies the date that this report is relevant for.
        /// </summary>
        public DateTimeOffset ForDate { get; set; }

        /// <summary>
        /// Specifies the date/time of report generation.
        /// </summary>
        public DateTimeOffset CurrentDateTime { get; set; }

        /// <summary>
        /// Board item group.
        /// </summary>
        public ReportItemGroup Board { get; set; }
        //
    }
}