﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Report
{
    /// <summary>
    /// Operator session get response container
    /// </summary>
    public class OperatorSessionGetResponse
    {
        /// <summary>
        /// List of operator sessions for Pos
        /// </summary>
        public List<OperatorSession> OperatorSessions { get; set; }
    }
}
