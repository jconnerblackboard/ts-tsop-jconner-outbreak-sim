﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Report
{
    /// <summary>
    /// Group of report items.
    /// </summary>
    public class ReportItemGroup
    {
        /// <summary>
        /// Report item group description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Report items.
        /// </summary>
        public List<ReportItem> Items { get; set; } = new List<ReportItem>();

        /// <summary>
        /// A value the represents the total amount of the report items.  Can be null if no total is required/desired.
        /// </summary>
        public ReportItem Total { get; set; }
    }
}