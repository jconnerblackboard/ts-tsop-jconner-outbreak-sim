﻿using System;

namespace BbTS.Domain.Models.Report
{
    /// <summary>
    /// Operator session
    /// </summary>
    public class OperatorSession
    {
        /// <summary>
        /// Session Id
        /// </summary>
        public Guid SessionId { get; set; }

        /// <summary>
        /// Start date time of session
        /// </summary>
        public DateTime? StartDateTime { get; set; }

        /// <summary>
        /// End date time of session
        /// </summary>
        public DateTime? EndDateTime { get; set; }

        /// <summary>
        /// Operator/Cashier name
        /// </summary>
        public string OperatorName { get; set; }

        /// <summary>
        /// Cashier number
        /// </summary>
        public int CashierNumber { get; set; }

        /// <summary>
        /// Operator guid
        /// </summary>
        public Guid OperatorGuid { get; set; }
    }
}
