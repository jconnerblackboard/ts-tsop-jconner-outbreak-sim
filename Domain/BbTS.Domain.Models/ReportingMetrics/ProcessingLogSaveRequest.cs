﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.ReportingMetrics
{
    /// <summary>
    /// Container class for a processing log save request.
    /// </summary>
    public class ProcessingLogSaveRequest
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public Guid RequestId { get; set; } = Guid.NewGuid();

        /// <summary>
        /// The log to save.
        /// </summary>
        public TableEntityProcessingLog Log { get; set; }
    }
}
