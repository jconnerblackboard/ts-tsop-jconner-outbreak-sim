﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Definitions.Notification;
using BbTS.Domain.Models.Exceptions.ReportingMetrics;

namespace BbTS.Domain.Models.ReportingMetrics
{
    /// <summary>
    /// Container class for a reporting metrics SFTP upload request.
    /// </summary>
    public class ReportingMetricsSftpUploadRequest
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// Tab separated metrics report to upload.
        /// </summary>
        public string TsvMetrics { get; set; }

        /// <summary>
        /// The frequency portion of the report.
        /// </summary>
        public ReportingMetricsFrequency Frequency { get; set; }

        /// <summary>
        /// Date of the report.
        /// </summary>
        public DateTimeOffset ReportDate { get; set; }

        /// <summary>
        /// The environment that the metrics were pulled from.
        /// </summary>
        public AzureTableStorageConnectionLocation Environment { get; set; }

        /// <summary>
        /// File name for this report.
        /// </summary>
        public string FileName
        {
            get
            {
                var envtag = Environment == AzureTableStorageConnectionLocation.Production || Environment == AzureTableStorageConnectionLocation.ProductionAus ? "" : $"_{Environment.ToString()}";
                return $"CL-PartnerMetrics-{Frequency}-{ReportDate:yyyyMMdd}00{envtag}";
            }
        }

        /// <summary>
        /// Get the upload path based on Frequency.
        /// </summary>
        public string Path
        {
            get
            {
                string baseDir;
                switch (Frequency)
                {
                    case ReportingMetricsFrequency.D:
                        baseDir = Environment == AzureTableStorageConnectionLocation.Production || Environment == AzureTableStorageConnectionLocation.ProductionAus ?
                                "/CamdenAggregatedMetrics/daily/" : "BlackboardInternalMetrics/daily/";
                        break;
                    case ReportingMetricsFrequency.M:
                        baseDir =  Environment == AzureTableStorageConnectionLocation.Production || Environment == AzureTableStorageConnectionLocation.ProductionAus ? 
                        "/CamdenAggregatedMetrics/monthly/" : "BlackboardInternalMetrics/monthly/";
                        break;
                    case ReportingMetricsFrequency.W:
                        baseDir =  Environment == AzureTableStorageConnectionLocation.Production || Environment == AzureTableStorageConnectionLocation.ProductionAus ? 
                            "/CamdenAggregatedMetrics/weekly/" : "BlackboardInternalMetrics/weekly/";
                        break;
                    default:    
                        throw new ArgumentException($"Unsupported frequency type {Frequency}.");
                }

                var remotePath = $"{ReportDate:yyyyMM}/{ReportDate:dd}/00/";
                return $"{baseDir}{remotePath}";
            }
        }
    }
}
