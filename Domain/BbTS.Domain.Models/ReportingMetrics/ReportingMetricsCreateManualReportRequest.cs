﻿using System;
using BbTS.Domain.Models.Definitions.Notification;

namespace BbTS.Domain.Models.ReportingMetrics
{
    /// <summary>
    /// Container class for a reporting metrics create manual report request.
    /// </summary>
    public class ReportingMetricsCreateManualReportRequest
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The unique identifier to generate a request for.  Leave blank or null for all institutions.
        /// </summary>
        public string InstitutionId { get; set; }

        /// <summary>
        /// The start date for the report.
        /// </summary>
        public DateTimeOffset? StartDate { get; set; }

        /// <summary>
        /// The end date for the report.
        /// </summary>
        public DateTimeOffset? EndDate { get; set; }

        /// <summary>
        /// The frequency for the report.  Set to <see cref="ReportingMetricsFrequency.Unknown"/> to use
        /// <see cref="StartDate"/> and <see cref="EndDate"/> for the reporting period.
        /// </summary>
        public ReportingMetricsFrequency Frequency { get; set; }

        /// <summary>
        /// Time slice to embed in the report.
        /// </summary>
        public ReportingMetricsTimeSlice TimeSlice { get; set; }

        /// <summary>
        /// Environment to embed in the report.
        /// </summary>
        public AzureTableStorageConnectionLocation Environment { get; set; }

        /// <summary>
        /// Ignore report validation for this manual run.
        /// </summary>
        public bool IgnoreValidation { get; set; }

        /// <summary>
        /// Set to true to ignore user count threshold validation when generating a report.
        /// </summary>
        public bool IgnoreUserCountThresholdValidation { get; set; } = false;

    }
}
