﻿using System;
using System.Collections.Generic;
using BbTS.Domain.Models.Definitions.Notification;

namespace BbTS.Domain.Models.ReportingMetrics
{
    /// <summary>
    /// Container class for the response to a processing log get request.
    /// </summary>
    public class ProcessingLogGetResponse
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public Guid RequestId { get; set; } = Guid.NewGuid();

        /// <summary>
        /// The type of processing log to fetch.
        /// </summary>
        public ReportingMetricsFrequency FrequencyType { get; set; }

        /// <summary>
        /// The date of the report.
        /// </summary>
        public DateTimeOffset ReportDate { get; set; }

        /// <summary>
        /// The retrieved processing log.  Empty list if it was not found.
        /// </summary>
        public List<TableEntityProcessingLog> ProcessingLog { get; set; } = new List<TableEntityProcessingLog>();
    }
}
