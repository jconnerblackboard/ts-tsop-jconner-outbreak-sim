﻿using System;
using BbTS.Domain.Models.Communication;
using Microsoft.WindowsAzure.Storage.Table;

namespace BbTS.Domain.Models.ReportingMetrics
{
    /// <summary>
    /// Table Entity class for the MetricsProcessingStartTime table in azure cloud storage.
    /// </summary>
    public class TableEntityMetricsProcessingStartTime : TableEntity, IAzureTableStorage
    {
        /// <summary>
        /// Start time for metrics processing agent.
        /// </summary>
        public string StartTime { get; set; }

        /// <summary>
        /// The offset for the time value.
        /// </summary>
        public string Offset { get; set; }

        /// <summary>
        /// Set the partition key.
        /// </summary>
        /// <param name="key"></param>
        public void SetPartitionKey(string key)
        {
            PartitionKey = key ?? throw new ArgumentNullException(nameof(key));
        }

        /// <inheritdoc />
        public void SetRowKey(string key)
        {
            RowKey = key ?? throw new ArgumentNullException(nameof(key));
        }
    }
}
