﻿using BbTS.Domain.Models.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.ReportingMetrics
{
    /// <summary>
    /// Container class for the response to a reporting metrics validation result get request.
    /// </summary>
    public class ReportingMetricsValidatinResultGetResponse : ProcessingResult
    {
        /// <summary>
        /// The requested report.
        /// </summary>
        public ReportingMetricsValidationReport Report { get; set; }

        /// <summary>
        /// Empty constructor.
        /// </summary>
        public ReportingMetricsValidatinResultGetResponse()
        {
            DeniedText = "Success.";
            ErrorCode = 0;
            RequestId = Guid.NewGuid().ToString("D");
        }

    }
}
