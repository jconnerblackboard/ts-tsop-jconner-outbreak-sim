﻿namespace BbTS.Domain.Models.ReportingMetrics
{
    /// <summary>
    /// Container class for the response to a reporting metrics create manual report request.
    /// </summary>
    public class ReportingMetricsCreateManualReportResponse
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The requested report in TSV file format.
        /// </summary>
        public string TsvReportString { get; set; }
    }
}
