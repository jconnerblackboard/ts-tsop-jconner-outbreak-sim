﻿using System;
using BbTS.Domain.Models.Communication;
using BbTS.Domain.Models.Definitions.Notification;
using Microsoft.WindowsAzure.Storage.Table;

namespace BbTS.Domain.Models.ReportingMetrics
{
    /// <summary>
    /// Container class for a credential metrics azure table storage entity.
    /// </summary>
    public class TableEntityReportingMetrics : TableEntity, IAzureTableStorage
    {
        /// <summary>
        /// Parameterless constructor.
        /// </summary>
        public TableEntityReportingMetrics()
        {
            
        }

        /// <summary>
        /// Parameterized constructor
        /// </summary>
        /// <param name="partitionKey"></param>
        /// <param name="rowKey"></param>
        public TableEntityReportingMetrics(string partitionKey, string rowKey)
        {
            PartitionKey = partitionKey;
            RowKey = rowKey;
        }

        /// <summary>
        /// Date the related object of the metric occured
        /// </summary>
        public DateTimeOffset Date { get; set; }

        /// <summary>
        /// Unique identification enumeration for the metric.
        /// </summary>
        public string DataKey { get; set; }

        /// <summary>
        /// The currency type of the transaction.
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// The count of transactions that occured of this metric type.
        /// </summary>
        public int TransactionCount { get; set; }

        /// <summary>
        /// The value of the transaction.
        /// </summary>
        public string TransactionValue { get; set; } = "0.00";

        /// <summary>
        /// The user count (if this metric is related to users).
        /// </summary>
        public int UserCount { get; set; }

        /// <summary>
        /// The DPAN count.
        /// </summary>
        public int DpanCount { get; set; }

        /// <summary>
        /// The device count (if this metric is related to devices).
        /// </summary>
        public int DeviceCount { get; set; }

        /// <summary>
        /// The frequency of this reporting metrics row.
        /// </summary>
        public string Frequency { get; set; }

        /// <summary>
        /// Set the partition key.
        /// </summary>
        /// <param name="key"></param>
        public void SetPartitionKey(string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            PartitionKey = key;
        }

        /// <summary>
        /// Set the row key.
        /// </summary>
        /// <param name="key"></param>
        public void SetRowKey(string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            RowKey = key;
        }

        /// <summary>
        /// Update the row key based on the <see cref="Date"/> and <see cref="DataKey"/>
        /// </summary>
        public void UpdateRowKey()
        {
            RowKey = $"{Date:yyyy-MM-dd}_{DataKey}_{Frequency}";
        }

        /// <summary>
        /// Clone this object.
        /// </summary>
        /// <returns>Memberwise Clone</returns>
        public TableEntityReportingMetrics Clone()
        {
            return (TableEntityReportingMetrics)MemberwiseClone();

        }
    }
}
