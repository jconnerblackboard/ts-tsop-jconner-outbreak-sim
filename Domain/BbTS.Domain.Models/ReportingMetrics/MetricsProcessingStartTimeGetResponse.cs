﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.ReportingMetrics
{
    /// <summary>
    /// The container class for a metrics processing start time get request.
    /// </summary>
    public class MetricsProcessingStartTimeGetResponse
    {
        public static DateTimeOffset DefaultStartTime = new DateTimeOffset(1899, 1, 1, 0, 0, 0, new TimeSpan(-7, 0, 0));

        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public string RequestId { get; set; } = Guid.NewGuid().ToString("D");
        
        /// <summary>
        /// Raw result from the query
        /// </summary>
        public TableEntityMetricsProcessingStartTime TableEntityMetricsProcessingStartTime { get; set; }

        /// <summary>
        /// The interpreted start time
        /// </summary>
        public DateTimeOffset StartTime
        {
            get
            {
                try
                {
                    var timesplit = TableEntityMetricsProcessingStartTime.StartTime.Split(':');
                    if (timesplit.Length != 2)
                    {
                        return DefaultStartTime;
                    }

                    var hour = int.Parse(timesplit[0]);
                    var minute = int.Parse(timesplit[1]);
                    return !TimeSpan.TryParse(TableEntityMetricsProcessingStartTime.Offset, CultureInfo.InvariantCulture, out var offset) ?
                        new DateTimeOffset(1899, 1, 1, hour, minute, 0, new TimeSpan(-7, 0, 0)) :
                        new DateTimeOffset(1899, 1, 1, hour, minute, 0, offset);
                }
                catch (Exception)
                {
                    return DefaultStartTime;
                }
            }
        }
    }
}
