﻿using System;
using System.Collections.Generic;

namespace BbTS.Domain.Models.ReportingMetrics
{
    /// <summary>
    /// Service level a request to process a range of reporting days in reporting metrics. 
    /// </summary>
    public class RmReportRangeProcessRequest : ReportingMetricsManuallyProcessingRequest
    {
        /// <summary>
        /// Starting date and time of the range request.
        /// </summary>
        public DateTimeOffset StartDate { get; set; }

        /// <summary>
        /// Ending date and time of the range request.
        /// </summary>
        public DateTimeOffset EndDate { get; set; }

        /// <summary>
        /// List of institutions to run the report for.
        /// </summary>
        public List<string> InstitutionIds { get; set; }
    }
}
