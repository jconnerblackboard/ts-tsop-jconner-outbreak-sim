﻿using BbTS.Domain.Models.Definitions.Notification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.ReportingMetrics
{
    public class ReportingMetricsValidationResultGetRequest
    {
        public string RequestId { get; set; } = Guid.NewGuid().ToString("D");

        public string InstitutionId { get; set; }

        public ReportingMetricsFrequency Frequency { get; set; }

        public DateTimeOffset ReportDate { get; set; }
    }
}
