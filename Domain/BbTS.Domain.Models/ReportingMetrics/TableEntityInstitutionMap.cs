﻿using System;
using BbTS.Domain.Models.Communication;
using Microsoft.WindowsAzure.Storage.Table;

namespace BbTS.Domain.Models.ReportingMetrics
{
    /// <summary>
    /// Container class for a map of institution id to institution name.
    /// </summary>
    public class TableEntityInstitutionMap : TableEntity, IAzureTableStorage
    {
        /// <summary>
        /// The type of environment the institution belongs to.
        /// </summary>
        public string Environment { get; set; }

        /// <summary>
        /// The name of the institution.
        /// </summary>
        public string InstitutionName { get; set; }

        /// <summary>
        /// The description for the institution.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Set the partition key.
        /// </summary>
        /// <param name="key"></param>
        public void SetPartitionKey(string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            PartitionKey = key;
        }

        /// <summary>
        /// Set the row key.
        /// </summary>
        /// <param name="key"></param>
        public void SetRowKey(string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            RowKey = key;
        }

        /// <summary>
        /// Clone this object.
        /// </summary>
        /// <returns>Memberwise Clone</returns>
        public TableEntityInstitutionMap Clone()
        {
            return (TableEntityInstitutionMap)MemberwiseClone();

        }
    }
}
