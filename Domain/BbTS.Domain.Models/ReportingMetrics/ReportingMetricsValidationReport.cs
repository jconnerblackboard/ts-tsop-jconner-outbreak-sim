﻿using BbTS.Domain.Models.Definitions.Notification;
using BbTS.Domain.Models.Definitions.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.ReportingMetrics
{
    /// <summary>
    /// Container class for a reporting metrics validation report.
    /// </summary>
    public class ReportingMetricsValidationReport
    {
        /// <summary>
        /// Time that the report validation routine was started.
        /// </summary>
        public DateTimeOffset? StartTime { get; set; } = DateTimeOffset.Now;

        /// <summary>
        /// Time that the report validation completed.
        /// </summary>
        public DateTimeOffset? EndTime { get; set; }

        /// <summary>
        /// The report date.
        /// </summary>
        public DateTimeOffset? ReportDate { get; set; }

        /// <summary>
        /// Identification of the Institution that owns the metrics being validated.
        /// </summary>
        public string InstitutionId { get; set; }

        /// <summary>
        /// Results of each individual metric validation operations.
        /// </summary>
        public List<MetricValidationResult> MetricValidationResults { get; set; } = new List<MetricValidationResult>();
    }

    /// <summary>
    /// Container class for an individual metrics validation result.
    /// </summary>
    public class MetricValidationResult
    {
        /// <summary>
        /// Applicable metric rule.
        /// </summary>
        public ReportingMetricsRule Rule { get; set; }

        /// <summary>
        /// The frequency to which the rule was applied.
        /// </summary>
        public ReportingMetricsFrequency AppliedFrequency { get; set; }

        /// <summary>
        /// The results of each rule and applied count.
        /// </summary>
        public List<MetricValidationCountResult> CountResults { get; set; } = new List<MetricValidationCountResult>();

        /// <summary>
        /// Was the metric validation result successful.
        /// </summary>
        public bool IsSuccessful
        {
            get
            {
                var failed = CountResults.FirstOrDefault(c => !c.IsSuccessful);
                return failed == null;
            }
        }

    }

    /// <summary>
    /// COntainer class for a single count validation.
    /// </summary>
    public class MetricValidationCountResult
    {
        /// <summary>
        /// The type of count for which this result is for.
        /// </summary>
        public MetricRulesCountCategories CountLabel { get; set; }

        /// <summary>
        /// The left value.
        /// </summary>
        public decimal LeftValue { get; set; }

        /// <summary>
        /// The right value.
        /// </summary>
        public decimal RightValue { get; set; }

        /// <summary>
        /// The operator applied.
        /// </summary>
        public ValidateValueOperator Operator { get; set; }

        /// <summary>
        /// Was the count validation successful?
        /// </summary>
        public bool IsSuccessful { get; set; }
    }
}
