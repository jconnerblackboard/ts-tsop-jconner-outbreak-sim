﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Communication;
using BbTS.Domain.Models.Definitions.Notification;
using Microsoft.WindowsAzure.Storage.Table;

namespace BbTS.Domain.Models.ReportingMetrics
{
    /// <summary>
    /// Container class for a processing log azure table storage entity.
    /// </summary>
    public class TableEntityProcessingLog : TableEntity, IAzureTableStorage
    {
        /// <summary>
        /// The date the report was last processed.
        /// </summary>
        public DateTimeOffset ProcessingDate { get; set; }

        /// <summary>
        /// The date of the report.
        /// </summary>
        public DateTimeOffset ReportDate { get; set; }

        /// <summary>
        /// The type of report.
        /// </summary>
        public string Frequency { get; set; }

        /// <summary>
        /// The environment against which the report was generated.
        /// </summary>
        public string Environment { get; set; }

        /// <summary>
        /// Was the report generation, validation, and upload successful.
        /// </summary>
        public bool IsSuccessful { get; set; }

        /// <summary>
        /// The last success or failure message associated with the report.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Set the partition key.
        /// </summary>
        /// <param name="key"></param>
        public void SetPartitionKey(string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            PartitionKey = key;
        }

        /// <summary>
        /// Set the row key.
        /// </summary>
        /// <param name="key"></param>
        public void SetRowKey(string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            RowKey = key;
        }

        /// <summary>
        /// Clone this object.
        /// </summary>
        /// <returns>Memberwise Clone</returns>
        public TableEntityProcessingLog Clone()
        {
            return (TableEntityProcessingLog)MemberwiseClone();
        }

        /// <summary>
        /// Set the Partition Key and Row Key for a <see cref="TableEntityProcessingLog"/>.
        /// </summary>
        /// <param name="log">Log to set.</param>
        public static void SetPartitionAndRowKey(TableEntityProcessingLog log)
        {
            log.PartitionKey = log.Frequency.ToString();
            log.RowKey = $"{log.ReportDate:yyyyMMdd}";
        }
    }
}
