﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Definitions.Notification;

namespace BbTS.Domain.Models.ReportingMetrics
{
    /// <summary>
    /// The container class for a metrics processing start time get request.
    /// </summary>
    public class MetricsProcessingStartTimeGetRequest
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public string RequestId { get; set; } = Guid.NewGuid().ToString("D");


        /// <summary>
        /// The Environment.
        /// </summary>
        public AzureTableStorageConnectionLocation Environment { get; set; }

        /// <summary>
        /// The unique identifier for the institution.
        /// </summary>
        public Guid InstitutionGuid { get; set; }
    }
}
