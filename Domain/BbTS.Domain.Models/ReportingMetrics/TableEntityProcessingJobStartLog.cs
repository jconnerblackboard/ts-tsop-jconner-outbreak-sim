﻿using System;
using BbTS.Domain.Models.Communication;
using Microsoft.WindowsAzure.Storage.Table;

namespace BbTS.Domain.Models.ReportingMetrics
{
    /// <summary>
    /// Table Entity class for the ProcessingJobStartLog table in azure cloud storage.
    /// </summary>
    public class TableEntityProcessingJobStartLog : TableEntity, IAzureTableStorage
    {
        /// <summary>
        /// The date the report was started.
        /// </summary>
        public DateTimeOffset? StartDateTime { get; set; }

        /// <summary>
        /// The date the report completed.
        /// </summary>
        public DateTimeOffset? EndDateTime { get; set; }

        /// <summary>
        /// The date of the report.
        /// </summary>
        public DateTimeOffset ReportDate { get; set; }

        /// <summary>
        /// The environment against which the report was generated.
        /// </summary>
        public string Environment { get; set; }

        /// <summary>
        /// Status of the log.
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// The message associated with the status.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Set the partition key.
        /// </summary>
        /// <param name="key"></param>
        public void SetPartitionKey(string key)
        {
            PartitionKey = key ?? throw new ArgumentNullException(nameof(key));
        }

        /// <summary>
        /// Set the row key.
        /// </summary>
        /// <param name="key"></param>
        public void SetRowKey(string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }

            RowKey = key;
        }

        /// <summary>
        /// Clone this object.
        /// </summary>
        /// <returns>Memberwise Clone</returns>
        public TableEntityProcessingJobStartLog Clone()
        {
            return (TableEntityProcessingJobStartLog)MemberwiseClone();
        }

        /// <summary>
        /// Set the Partition Key and Row Key for a <see cref="TableEntityProcessingLog"/>.
        /// </summary>
        /// <param name="log">Log to set.</param>
        public static void SetPartitionAndRowKey(TableEntityProcessingJobStartLog log)
        {
            log.RowKey = $"{log.ReportDate:yyyyMMdd}";
        }
    }
}
