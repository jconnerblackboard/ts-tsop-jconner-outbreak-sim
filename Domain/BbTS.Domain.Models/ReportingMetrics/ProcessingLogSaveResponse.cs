﻿using BbTS.Domain.Models.Notification;
using BbTS.Domain.Models.Transaction;

namespace BbTS.Domain.Models.ReportingMetrics
{
    /// <summary>
    /// Container class for the response to a processing log save request.
    /// </summary>
    public class ProcessingLogSaveResponse : ProcessingResult
    {
    }
}
