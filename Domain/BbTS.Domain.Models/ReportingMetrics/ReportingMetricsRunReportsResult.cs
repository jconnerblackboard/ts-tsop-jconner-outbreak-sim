﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Definitions.Notification;

namespace BbTS.Domain.Models.ReportingMetrics
{
    /// <summary>
    /// Container class for the results of a reporting metrics "run {frequency} reports" process.
    /// </summary>
    public class ReportingMetricsRunReportsResult
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public Guid RequestId { get; set; }

        /// <summary>
        /// Flag to denote whether or not failed requests were reprocessed during the initial request.
        /// </summary>
        public bool ReprocessFailedRequests { get; set; } = true;

        /// <summary>
        /// The date and time the reports were processed.
        /// </summary>
        public DateTimeOffset ProcessingDate { get; set; }

        /// <summary>
        /// The frequency value of the reports.
        /// </summary>
        public ReportingMetricsFrequency Frequency { get; set; }

        /// <summary>
        /// The environment where the data is stored.
        /// </summary>
        public AzureTableStorageConnectionLocation Environment { get; set; }

        /// <summary>
        /// The processing results of each report.
        /// </summary>
        public List<ReportingMetricsProcessResponse> Results { get; set; } = new List<ReportingMetricsProcessResponse>();

        /// <summary>
        /// The state of the processed logs before the current processing run.
        /// </summary>
        public List<ProcessingLogGetResponse> FailedLogsBeforeRun { get; set; } = new List<ProcessingLogGetResponse>();

        /// <summary>
        /// The state of the processed logs after the current processing run.
        /// </summary>
        public List<ProcessingLogGetResponse> FailedLogsAfterRun { get; set; } = new List<ProcessingLogGetResponse>();

        /// <summary>
        /// Get the success count.
        /// </summary>
        public int SuccessCount
        {
            get
            {
                var count = 0;
                Results.ForEach(x =>
                {
                    if (x.ProcessingResult == ReportingMetricsProcessingResult.Success)
                        count++;
                });
                return count;
            }
        }

        /// <summary>
        /// Get the fail count.
        /// </summary>
        public int FailCount
        {
            get
            {
                var count = 0;
                Results.ForEach(x =>
                {
                    if (x.ProcessingResult == ReportingMetricsProcessingResult.FailValidation ||
                        x.ProcessingResult == ReportingMetricsProcessingResult.FailSftpTransfer ||
                        x.ProcessingResult == ReportingMetricsProcessingResult.FailUnknown ||
                        x.ProcessingResult == ReportingMetricsProcessingResult.Error)
                        count++;
                });
                return count;
            }
        }

        /// <summary>
        /// Convert to a human readable informational string.
        /// </summary>
        /// <returns></returns>
        public string ToInformationString()
        {
            return $"Report processing was run on {ProcessingDate} for the '{Environment}' environment and '{NotificationDefinitions.ReportingMetricsFrequencyParse(Frequency)}' frequency.  " +
                   $"{Results.Count} records were processed.  {SuccessCount} records succeeded and {FailCount} records failed.  " +
                   $"Before the processing routine was run, {FailedLogsBeforeRun.Count} records were in a failed state.  " +
                   $"After the processing routine had completed, {FailedLogsAfterRun.Count} records were still in failed stated.  " +
                   $"The flag to reprocess failed records was set to {ReprocessFailedRequests}.";
        }
    }
}
