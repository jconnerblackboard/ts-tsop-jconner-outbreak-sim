﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Definitions.Notification;
using BbTS.Domain.Models.Definitions.System;

namespace BbTS.Domain.Models.ReportingMetrics
{
    /// <summary>
    /// Container class for a single reporting metrics ruleset.
    /// </summary>
    public class ReportingMetricsRule
    {
        /// <summary>
        /// Left side metrics.
        /// </summary>
        public List<string> Left { get; set; } = new List<string>();

        /// <summary>
        /// Right side metrics.
        /// </summary>
        public List<string> Right { get; set; } = new List<string>();

        /// <summary>
        /// Counts that will be checked for validation.
        /// </summary>
        public List<MetricRulesCountCategories> IncludedCounts { get; set; } = new List<MetricRulesCountCategories>();

        /// <summary>
        /// The frequencies that this metric applies to.
        /// </summary>
        public List<ReportingMetricsFrequency> IncludedFrequencies { get; set; } = new List<ReportingMetricsFrequency>();

        /// <summary>
        /// The operator that should be applied to the metric calculation.
        /// </summary>
        public ValidateValueOperator Operator { get; set; }

        /// <summary>
        /// Empty Constructor (placeholder for serialization)
        /// </summary>
        public ReportingMetricsRule()
        {
        }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="rules">Ruleset retrieved from Azure Table Storage.</param>
        public ReportingMetricsRule(TableEntityMetricsRules rules)
        {
            Left = new List<string> (rules.Left.Split(','));
            Right = new List<string>(rules.Right.Split(','));

            var included = new List<string>(rules.CountsIncluded.Split(','));
            IncludedCounts.AddRange(included.Select(r => (MetricRulesCountCategories)int.Parse(r)));

            var frequencies = new List<string>(rules.FrequenciesIncluded.Split(','));
            IncludedFrequencies.AddRange(frequencies.Select(f => (ReportingMetricsFrequency)Enum.Parse(typeof(ReportingMetricsFrequency), f)));

            switch (rules.Operator.ToLower())
            {
                case "e":
                    Operator = ValidateValueOperator.Equal;
                    break;
                case "ne":
                    Operator = ValidateValueOperator.NotEqual;
                    break;
                case "lt":
                    Operator = ValidateValueOperator.LessThan;
                    break;
                case "lte":
                    Operator = ValidateValueOperator.LessThanOrEqual;
                    break;
                case "gt":
                    Operator = ValidateValueOperator.GreaterThan;
                    break;
                case "gte":
                    Operator = ValidateValueOperator.GreatherThanOrEqual;
                    break;
                default:
                    Operator = ValidateValueOperator.Unknown;
                    break;
            }
            
        }
    }
}
