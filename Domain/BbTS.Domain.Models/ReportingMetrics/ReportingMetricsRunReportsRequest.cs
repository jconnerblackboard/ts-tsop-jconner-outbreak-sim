﻿using System;
using BbTS.Domain.Models.Definitions.Notification;

namespace BbTS.Domain.Models.ReportingMetrics
{
    /// <summary>
    /// Container class for the request to run a reporting metrics reports.
    /// </summary>
    public class ReportingMetricsRunReportsRequest
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public Guid RequestId { get; set; } = Guid.NewGuid();

        /// <summary>
        /// Ignore validation of institutions that are present in the master but not in the report
        /// </summary>
        public bool IgnoreMasterNotInReport { get; set; }

        /// <summary>
        /// Ignore validation of institutions that are present in the report but not in the master
        /// </summary>
        public bool IgnoreReportNotInMaster { get; set; }

        /// <summary>
        /// Ignore validation of user counts greater than a minimum threshold.
        /// </summary>
        public bool IgnoreUserCount { get; set; }

        /// <summary>
        /// Ignore left/right metrics count validation for the report frequency and date.
        /// </summary>
        public bool IgnoreMetricsCountValidation { get; set; }

        /// <summary>
        /// The environment where the data is stored.
        /// </summary>
        public AzureTableStorageConnectionLocation Environment { get; set; }

        /// <summary>
        /// The frequency value of the reports.
        /// </summary>
        public ReportingMetricsFrequency Frequency { get; set; }

        /// <summary>
        /// Optional DateTime to run a report outside of the current day.
        /// </summary>
        public DateTimeOffset? ReportDate { get; set; }

        /// <summary>
        /// Set flag to reprocess any requests that have previously failed.
        /// </summary>
        public bool ReprocessFailedRequests { get; set; } = true;

        /// <summary>
        /// Set to force processing of the log even if it has already been processed for the given date and frequency.
        /// </summary>
        public bool ForceProcess { get; set; }
    }
}
