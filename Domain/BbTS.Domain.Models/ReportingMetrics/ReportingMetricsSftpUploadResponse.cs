﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Definitions.Notification;

namespace BbTS.Domain.Models.ReportingMetrics
{
    /// <summary>
    /// Container class for the response to a reporting metrics SFTP upload request.
    /// </summary>
    public class ReportingMetricsSftpUploadResponse
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The result of the upload operation.
        /// </summary>
        public ReportingMetricsSftpUploadResult Result { get; set; }

        /// <summary>
        /// The message associated with the resulting operation.
        /// </summary>
        public string Message { get; set; }
    }
}
