﻿using System;

namespace BbTS.Domain.Models.ReportingMetrics
{
    /// <summary>
    /// Container class for a processing job start log save request.
    /// </summary>
    public class ProcessingJobStartLogSaveRequest
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public Guid RequestId { get; set; } = Guid.NewGuid();

        /// <summary>
        /// The log to save.
        /// </summary>
        public TableEntityProcessingJobStartLog Log { get; set; }
    }
}
