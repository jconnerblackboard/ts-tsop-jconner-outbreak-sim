﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Definitions.Notification;

namespace BbTS.Domain.Models.ReportingMetrics
{
    /// <summary>
    /// Container class for a processing log get request.
    /// </summary>
    public class ProcessingLogGetRequest
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public Guid RequestId { get; set; } = Guid.NewGuid();

        /// <summary>
        /// The type of processing log to fetch.
        /// </summary>
        public ReportingMetricsFrequency FrequencyType { get; set; }

        /// <summary>
        /// The environment against which the report was generated.
        /// </summary>
        public AzureTableStorageConnectionLocation Environment { get; set; }

        /// <summary>
        /// The date of the report.
        /// </summary>
        public DateTimeOffset ReportDate { get; set; }

    }
}
