﻿using System;
using BbTS.Domain.Models.Definitions.Notification;

namespace BbTS.Domain.Models.ReportingMetrics
{
    /// <summary>
    /// Kick off the reporting metrics processing manually for a given frequency and date.
    /// </summary>
    public class ReportingMetricsManuallyProcessingRequest
    {
        /// <summary>
        /// Unique identifier for the request.
        /// </summary>
        public Guid RequestId { get; set; } = Guid.NewGuid();

        /// <summary>
        /// Environment to run the report in.
        /// </summary>
        public AzureTableStorageConnectionLocation Environment { get; set; }

        /// <summary>
        /// The frequency value for the report.
        /// </summary>
        public ReportingMetricsFrequency Frequency { get; set; }

        /// <summary>
        /// The date of the report.
        /// </summary>
        public DateTime ReportDate { get; set; }

        /// <summary>
        /// Disable the check that each member of the master list is present in the report.
        /// </summary>
        public bool IgnoreMasterListToReportValidation { get; set; }

        /// <summary>
        /// Disable the check that an institution in a given report is in the master list.
        /// </summary>
        public bool IgnoreReportToMasterListValidation { get; set; }

        /// <summary>
        /// Disable the check that an institution in a given report is in the master list.
        /// </summary>
        public bool IgnoreUserCountValidation { get; set; }

        /// <summary>
        /// Disable the check for metrics count validation. 
        /// </summary>
        public bool IgnoreMetricsCountValidation { get; set; }
    }
}
