﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Communication;
using Microsoft.WindowsAzure.Storage.Table;

namespace BbTS.Domain.Models.ReportingMetrics
{
    /// <summary>
    /// Container class for a representation of the MetricsRules table in Azure Table Storage.
    /// </summary>
    public class TableEntityMetricsRules : TableEntity, IAzureTableStorage
    {
        /// <summary>
        /// Left side metrics.
        /// </summary>
        public string Left { get; set; }

        /// <summary>
        /// Right side metrics.
        /// </summary>
        public string Right { get; set; }

        /// <summary>
        /// Counts that will be included in validation. 
        /// </summary>
        public string CountsIncluded { get; set; }

        /// <summary>
        /// The operator with which to apply to the calculation.  
        /// Valid values are:
        ///     "e, ne, lt, gt, lte, gte" 
        /// </summary>
        public string Operator { get; set; }

        /// <summary>
        /// Frequencies in which to apply the metric calculation.
        /// Valid values are:
        ///     "D, W, M, SL"
        /// </summary>
        public string FrequenciesIncluded { get; set; }

        /// <summary>
        /// Set the partition key.
        /// </summary>
        /// <param name="key"></param>
        public void SetPartitionKey(string key)
        {
            PartitionKey = key;
        }

        /// <summary>
        /// Set the row key.
        /// </summary>
        /// <param name="key"></param>
        public void SetRowKey(string key)
        {
            RowKey = key;
        }
    }
}
