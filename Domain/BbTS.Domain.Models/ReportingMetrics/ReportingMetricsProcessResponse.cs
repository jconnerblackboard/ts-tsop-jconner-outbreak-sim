﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Definitions.Notification;

namespace BbTS.Domain.Models.ReportingMetrics
{
    /// <summary>
    /// Container class for the response to a reporting metrics processing call.
    /// </summary>
    public class ReportingMetricsProcessResponse
    {
        /// <summary>
        /// The date and time processing was started.
        /// </summary>
        public DateTimeOffset StartTime { get; set; }

        /// <summary>
        /// The date and time processing was completed.
        /// </summary>
        public DateTimeOffset EndTime { get; set; }

        /// <summary>
        /// The type of report that was processed.
        /// </summary>
        public ReportingMetricsFrequency Frequency { get; set; }

        /// <summary>
        /// The reporting environment.
        /// </summary>
        public AzureTableStorageConnectionLocation Environment { get; set; }

        /// <summary>
        /// The processing result.
        /// </summary>
        public ReportingMetricsProcessingResult ProcessingResult { get; set; }

        /// <summary>
        /// The validation result.
        /// </summary>
        public ReportingMetricsValidationResult ValidationResult { get; set; }

        /// <summary>
        /// The SFTP upload result.
        /// </summary>
        public ReportingMetricsSftpUploadResult SftpUploadResult { get; set; }

        /// <summary>
        /// The generated TSV string that was uploaded to SFTP.
        /// </summary>
        public string TsvString { get; set; }

        /// <summary>
        /// The processing log that was saved to table storage.
        /// </summary>
        public TableEntityProcessingLog ProcessingLog { get; set; }

        /// <summary>
        /// The result message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// List of days included in this processing report.
        /// </summary>
        public List<TableEntityProcessingLog> DaysIncludedInReport { get; set; } = new List<TableEntityProcessingLog>();
    }
}
