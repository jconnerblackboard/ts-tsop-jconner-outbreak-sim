﻿using System.Xml.Serialization;

namespace BbTS.Domain.Models.Configuration
{
    /// <summary>
    /// This class contains the payment express groups in the system
    /// </summary>
    public class PaymentExpressGroupAccount
    {
        /// <summary>
        /// Integer key for this table.
        /// </summary>
        [XmlAttribute]
        public int PaymentExpressGroupAccountId { get; set; }
        /// <summary>
        /// The name of the group account
        /// </summary>
        [XmlAttribute]
        public string Name { get; set; }
        /// <summary>
        /// The prefix used for all devices.
        /// </summary>
        [XmlAttribute]
        public string DevicePrefix { get; set; }
    }
}
