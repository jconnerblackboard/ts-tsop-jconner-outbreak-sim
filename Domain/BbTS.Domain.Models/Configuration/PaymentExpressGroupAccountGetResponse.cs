﻿using System.Collections.Generic;



namespace BbTS.Domain.Models.Configuration
{
    /// <summary>
    /// Container for the response to a pos group get request.
    /// </summary>
    public class PaymentExpressGroupAccountGetResponse
    {
        /// <summary>
        /// Parameterless constructor for (de)serialization.
        /// </summary>
        public PaymentExpressGroupAccountGetResponse()
        {
        }

        /// <summary>
        /// The unique identifier for this post request and response.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The requested list of Group.  When PosGuid is specified in the request, the list will have 1 entry.
        /// </summary>
        public List<PaymentExpressGroupAccount> Properties { get; set; } = new List<PaymentExpressGroupAccount>();

        /// <summary>
        /// The original request for this reponse
        /// </summary>
        public PaymentExpressGroupAccountGetRequest Request { get; set; }

        /// <summary>
        /// Parameterized constructor that requires the request id and the request.
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="request"></param>
        /// <param name="properties"></param>
        public PaymentExpressGroupAccountGetResponse(string requestId, PaymentExpressGroupAccountGetRequest request, List<PaymentExpressGroupAccount> properties)
        {
            RequestId = requestId;
            Request = request;
            Properties = properties;
        }
    }
}
