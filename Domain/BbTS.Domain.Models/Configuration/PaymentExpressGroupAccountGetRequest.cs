﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Configuration
{
    /// <summary>
    /// Container for the response to a pos properties get request.
    /// </summary>
    public class PaymentExpressGroupAccountGetRequest
    {
        /// <summary>
        /// Parameterless constructor for (de)serialization.
        /// </summary>
        public PaymentExpressGroupAccountGetRequest()
        {
        }

        /// <summary>
        /// The unique identifier for this post request and response.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The unique identifier for the PaymentExpressGroupAccount (null if all items were requested.)
        /// </summary>
        public string PaymentExpressGroupAccountName { get; internal set; }

        /// <summary>
        /// The requested list of properties.  When PaymentExpressGroupAccountId is specified in the request, the list will have 1 entry.
        /// </summary>
        public List<PaymentExpressGroupAccountGetRequest> Properties { get; set; } = new List<PaymentExpressGroupAccountGetRequest>();

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="paymentExpressGroupAccountName">The unique identifier for the pos (null if all pos properties were requested.)</param>
        public PaymentExpressGroupAccountGetRequest(string requestId, string paymentExpressGroupAccountName)
        {
            RequestId = requestId;
            PaymentExpressGroupAccountName = paymentExpressGroupAccountName;
        }
    }
}
