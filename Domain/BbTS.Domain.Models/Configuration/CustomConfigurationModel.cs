﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using BbTS.Domain.Models.General;

namespace BbTS.Domain.Models.Configuration
{
    /// <summary>
    /// Class to hold a custom configuration.  This class is needed because Configuration isn't serializable and needs to be in order for a 
    /// plugin to unpack properly.
    /// </summary>
    [Serializable]
    public class CustomConfigurationModel
    {
        private static readonly object Lock = new object();

        /// <summary>
        /// Key/Value pairs parsed from the Configuration file.
        /// </summary>
        public List<StringPair> Items { get; set; } = new List<StringPair>();

        /// <summary>
        /// Get a Specific value by its key.
        /// </summary>
        /// <param name="key">The key the value was stored under.</param>
        /// <returns>Value or null</returns>
        public string ValueGet(string key)
        {
            lock (Lock)
            {
                var item = Items.FirstOrDefault(i => i.Key == key);
                return item?.Value;
            }
        }

        /// <summary>
        /// Clone this.
        /// </summary>
        /// <returns></returns>
        public CustomConfigurationModel Clone()
        {
            return new CustomConfigurationModel
            {
                Items = new List<StringPair>(Items)
            };
        }

        /// <summary>
        /// Parse a Configuration file and generate a CustomConfigurationModel object.
        /// </summary>
        /// <param name="configuration"><see cref="Configuration"/> object to parse.</param>
        /// <returns>Generated CustomConfigurationModel</returns>
        public static CustomConfigurationModel GenerateCustomConfigurationModel(global::System.Configuration.Configuration configuration)
        {
            var keys = new List<string>(configuration.AppSettings.Settings.AllKeys);
            CustomConfigurationModel model = new CustomConfigurationModel();
            foreach (var key in keys)
            {
                model.Items.Add(new StringPair { Key = key, Value = configuration.AppSettings.Settings[key].Value});
            }
            return model;
        }

        /// <summary>
        /// Generate a custom configuration model from the default ConfigurationManager.AppSettings.
        /// </summary>
        /// <returns></returns>
        public static CustomConfigurationModel GenerateCustomConfigurationModel()
        {
            var settings = ConfigurationManager.AppSettings;

            var keys = new List<string>(settings.AllKeys);
            var model = new CustomConfigurationModel();
            foreach (var key in keys)
            {
                model.Items.Add(new StringPair {Key = key, Value = settings[key]});
            }
            return model;
        }
    }
}
