﻿namespace BbTS.Domain.Models.Configuration
{
    /// <summary>
    /// Container class for the response to a request for API Version information.
    /// </summary>
    public class VersionGetResponse
    {
        /// <summary>
        /// The version of the API.
        /// </summary>
        public string FileVersion { get; set; }

        /// <summary>
        /// The Version of the Transact Instance.
        /// </summary>
        public string TransactProductVersion { get; set; }
    }
}
