﻿namespace BbTS.Domain.Models.Configuration
{
    /// <summary>
    /// Container class for the parameters of the BbTS Header.
    /// </summary>
    public class BbtsHeader
    {
        /// <summary>
        /// Name of the application initiating a request.
        /// </summary>
        public string ApplicationName { get; set; }

        /// <summary>
        /// The hostname on the box.
        /// </summary>
        public string OsHostName { get; set; }

        /// <summary>
        /// The ip address of the client.
        /// </summary>
        public string IpAddress { get; set; }

        /// <summary>
        /// The OS User account *or* logged in user if from a web environment
        /// </summary>
        public string OsUser { get; set; }

        /// <summary>
        /// Format as a string like the following:
        /// $"ApplicationName={ApplicationName},OsHostName={OsHostName},IpAddress={IpAddress},OsUser={OsUser}"
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"ApplicationName={ApplicationName},OsHostName={OsHostName},IpAddress={IpAddress},OsUser={OsUser}";
        }
    }
}
