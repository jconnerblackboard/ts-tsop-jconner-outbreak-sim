﻿using System;

namespace BbTS.Domain.Models.ProfitCenter
{
    /// <summary>
    /// Profit center device settings
    /// </summary>
    public class ProfitCenterDeviceSettings
    {
        /// <summary>
        /// Profit center guid
        /// </summary>
        public Guid ProfitCenterGuid { get; set; }

        /// <summary>
        /// Profit center name
        /// </summary>
        public string ProfitCenterName { get; set; }
    }

    /// <summary>
    /// View for a <see cref="ProfitCenterDeviceSettings"/>.  (Version 1)
    /// </summary>
    public class ProfitCenterDeviceSettingsViewV01
    {
        /// <summary>
        /// Profit center guid
        /// </summary>
        public Guid ProfitCenterGuid { get; set; }

        /// <summary>
        /// Profit center name
        /// </summary>
        public string ProfitCenterName { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="ProfitCenterDeviceSettings"/> conversion.
    /// </summary>
    public static class ProfitCenterDeviceSettingsConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="ProfitCenterDeviceSettingsViewV01"/> object based on this <see cref="ProfitCenterDeviceSettings"/>.
        /// </summary>
        /// <param name="profitCenterDeviceSettings"></param>
        /// <returns></returns>
        public static ProfitCenterDeviceSettingsViewV01 ToProfitCenterDeviceSettingsViewV01(this ProfitCenterDeviceSettings profitCenterDeviceSettings)
        {
            if (profitCenterDeviceSettings == null) return null;

            return new ProfitCenterDeviceSettingsViewV01
            {
                ProfitCenterGuid = profitCenterDeviceSettings.ProfitCenterGuid,
                ProfitCenterName = profitCenterDeviceSettings.ProfitCenterName
            };
        }

        /// <summary>
        /// Returns a <see cref="ProfitCenterDeviceSettings"/> object based on this <see cref="ProfitCenterDeviceSettingsViewV01"/>.
        /// </summary>
        /// <param name="profitCenterDeviceSettingsViewV01"></param>
        /// <returns></returns>
        public static ProfitCenterDeviceSettings ToProfitCenterDeviceSettings(this ProfitCenterDeviceSettingsViewV01 profitCenterDeviceSettingsViewV01)
        {
            if (profitCenterDeviceSettingsViewV01 == null) return null;

            return new ProfitCenterDeviceSettings
            {
                ProfitCenterGuid = profitCenterDeviceSettingsViewV01.ProfitCenterGuid,
                ProfitCenterName = profitCenterDeviceSettingsViewV01.ProfitCenterName
            };
        }
        #endregion
    }
}
