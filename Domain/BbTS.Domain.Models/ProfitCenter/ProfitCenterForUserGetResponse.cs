﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BbTS.Domain.Models.ProfitCenter
{
    /// <summary>
    /// Container for the response to a pos properties get request.
    /// </summary>
    [DataContract]
    public class ProfitCenterForUserGetResponse
    {
        /// <summary>
        /// Parameterless constructor for (de)serialization.
        /// </summary>
        public ProfitCenterForUserGetResponse()
        {
        }

        /// <summary>
        /// The unique identifier for the pos (null if all pos properties were requested.)
        /// </summary>
        [DataMember]
        public int? ProfitCenterId { get; set; }


        /// <summary>
        /// The unique identifier for the user.
        /// </summary>
        [DataMember]
        public string UserId { get; set; }

        /// <summary>
        /// The requested list of properties.  When PosGuid is specified in the request, the list will have 1 entry.
        /// </summary>
        [DataMember]
        public List<TsProfitCenter> Properties { get; set; } = new List<TsProfitCenter>();

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="userId">The identity of the user this request is for</param>
        /// <param name="id">The unique identifier for the profit center (null if all profit centers were requested.)</param>
        public ProfitCenterForUserGetResponse(string userId, int? id = null)
        {
            UserId = userId;
            ProfitCenterId = id;
        }
    }
}
