﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.ProfitCenter
{
    /// <summary>
    /// Container for the response to a pos properties get request.
    /// </summary>
    public class ProfitCenterGetResponse
    {
        /// <summary>
        /// Parameterless constructor for (de)serialization.
        /// </summary>
        public ProfitCenterGetResponse()
        {
        }

        /// <summary>
        /// The unique identifier for the pos (null if all pos properties were requested.)
        /// </summary>
        public int ProfitCenterId { get; internal set; }

        /// <summary>
        /// The requested list of properties.  When PosGuid is specified in the request, the list will have 1 entry.
        /// </summary>
        public List<TsProfitCenter> Properties { get; set; } = new List<TsProfitCenter>();

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="id">The unique identifier for the profit center (null if all profit centers were requested.)</param>
        public ProfitCenterGetResponse(int id)
        {
            ProfitCenterId = id;
        }
    }
}
