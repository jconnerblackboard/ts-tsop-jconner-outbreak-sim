﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.ProfitCenter
{
    /// <summary>
    /// This object represents a Transact ProfitCenter object. Main Table for Profit Center setup information
    /// </summary>
    [Serializable]
    [DataContract]
    public class TsProfitCenter
    {
        /// <summary>
        /// Profit Center ID
        /// </summary>
        [XmlAttribute]
        [DataMember]
        public int ProfitCenter_Id { get; set; }
        /// <summary>
        /// Merchant that the Profit Center belongs to
        /// </summary>
        [XmlAttribute]
        [DataMember]
        public int Merchant_Id { get; set; }
        /// <summary>
        /// Name of profit center
        /// </summary>
        [XmlAttribute]
        [DataMember]
        public string Name { get; set; }
        /// <summary>
        /// Code name of profit center
        /// </summary>
        [XmlAttribute]
        [DataMember]
        public string Code_Name { get; set; }
        /// <summary>
        /// Profit center guid
        /// </summary>
        [XmlAttribute]
        [DataMember]
        public string ProfitCenterGuid { get; set; }
    }
}
