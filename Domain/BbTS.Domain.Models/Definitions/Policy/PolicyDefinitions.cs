﻿namespace BbTS.Domain.Models.Definitions.Policy
{
    /// <summary>
    /// The evaluation method used for policies contained in the PolicySet.
    /// </summary>
    public enum PolicyDecisionStrategy
    {
        /// <summary>
        /// Enforces the actions of the first rule that evaluates to TRUE.
        /// </summary>
        FirstMatching = 1,

        /// <summary>
        /// Enforces the actions of all rules that evaluate to TRUE.
        /// </summary>
        AllMatching = 2
    }

    /// <summary>
    /// An enumeration indicating whether a policy rule is administratively enabled, administratively disabled, or enabled for debug mode.
    /// </summary>
    public enum EnabledStatus
    {
        /// <summary>
        /// Administratively enabled.
        /// </summary>
        Enabled = 1,

        /// <summary>
        /// Administratively disabled.
        /// </summary>
        Disabled = 2,

        /// <summary>
        /// The entity evaluating the PolicyConditions is instructed to evaluate the conditions for the Rule, but not
        /// to perform the actions if the PolicyConditions evaluate to TRUE.  This serves as a debug vehicle when
        /// attempting to determine what policies would execute in a particular scenario, without taking any actions
        /// to change state during the debugging.
        /// </summary>
        EnabledForDebug = 3
    }

    /// <summary>
    /// Indicates whether the list of policy conditions associated with this policy rule is in disjunctive normal form (DNF) or conjunctive normal form (CNF).
    /// </summary>
    public enum ConditionListType
    {
        /// <summary>
        /// Disjunctive normal form (OR'ed)
        /// </summary>
        Disjunctive = 1,

        /// <summary>
        /// Conjunctive normal form (AND'ed)
        /// </summary>
        Conjunctive = 2
    }

    /// <summary>
    /// An enumeration indicating how to interpret the action ordering indicated via the PolicyActionInPolicyRule aggregation.
    /// </summary>
    public enum SequencedActions
    {
        /// <summary>
        /// Do the actions in the indicated order, or don't do them at all.
        /// </summary>
        Mandatory = 1,

        /// <summary>
        /// Do the actions in the indicated order if you can, but if you can't do them in this order, do them in another order if you can.
        /// </summary>
        Recommended = 2,

        /// <summary>
        /// Do them -- I don't care about the order.
        /// </summary>
        DontCare = 3
    }

    /// <summary>
    /// An enumeration indicating how to interpret the action ordering for the actions aggregated by this PolicyRule.
    /// </summary>
    public enum ExecutionStrategyType
    {
        /// <summary>
        /// Execute actions according to predefined order, until successful execution of a single action.
        /// </summary>
        DoUntilSuccess = 1,

        /// <summary>
        /// Execute ALL actions which are part of the modeled set, according to their predefined order.  Continue doing this, even if one or more of the actions fails.
        /// </summary>
        DoAll = 2,

        /// <summary>
        /// Execute actions according to predefined order, until the first failure in execution of a single sub-action.
        /// </summary>
        DoUntilFailure = 3
    }

    /// <summary>
    /// Policy condition type.
    /// </summary>
    public enum PolicyConditionType
    {
        /// <summary>
        /// Container condition that holds other conditions.
        /// </summary>
        Compound = 0,

        /// <summary>
        /// Condition that evaluates to true.
        /// </summary>
        True = 1,

        /// <summary>
        /// Condition that evaluates to false.
        /// </summary>
        False = 2,

        /// <summary>
        /// Condition that compares two values.  (This functionality is not implemented.)
        /// </summary>
        Match = 3,

        /// <summary>
        /// Condition that evaluates to true if the pos belongs to the specified pos group (or parent of the pos group).
        /// </summary>
        ReaderInGroup = 4,

        /// <summary>
        /// Condition that evaluates to true if the customer has the specified CDF value.
        /// </summary>
        CustomerInGroup = 5,

        /// <summary>
        /// Condition that evaluates to true if the tender used matches the specified tender.
        /// </summary>
        Tender = 6,

        /// <summary>
        /// Condition that evaluates to true if the evaluation time satisfies the defined time restrictions.
        /// </summary>
        TimePeriod = 7
    }

    /// <summary>
    /// Policy action type.
    /// </summary>
    public enum PolicyActionType
    {
        /// <summary>
        /// Action that results in a denial.
        /// </summary>
        DenyTransaction = 0,

        /// <summary>
        /// Action that results in an approval.
        /// </summary>
        AllowTransaction = 1,

        /// <summary>
        /// Action that results in a discount.
        /// </summary>
        ApplyDiscount = 2,

        /// <summary>
        /// Action that results in a surcharge.
        /// </summary>
        ApplySurcharge = 3,

        /// <summary>
        /// Action that specifies what stored value account(s) to use for the transaction.
        /// </summary>
        StoredValueAccountType = 4,

        /// <summary>
        /// Action that results in a stored value transaction limit being applied to the transaction.
        /// </summary>
        ApplyStoredValueTranLimit = 5
    }
}
