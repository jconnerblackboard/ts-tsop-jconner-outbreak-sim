﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Definitions.Device
{
    /// <summary>
    /// Enumeration for HardwareModel from the V_IP_READER view.
    /// </summary>
    public enum HardwareModel
    {
        /// <summary>
        /// -- 0 - MasterController:SA3004 (Host Value: 7)
        /// </summary>
        MasterControllerSA3004 = 0,
        /// <summary>
        /// -- 1 - MasterController:SA3032 (Host Value: 8)
        /// </summary>
        MasterControllerSA3032 = 1,
        /// <summary>
        /// -- 2 - MasterController:SA200x (Host Value: 4)
        /// </summary>
        MasterControllerSA200x = 2,
        /// <summary>
        /// -- 3 - MasterController:SA201x (Host Value: 5)
        /// </summary>
        MasterControllerSA201x = 3,
        /// <summary>
        /// -- 4 - MasterController:SA202x (Host Value: 6)
        /// </summary>
        MasterControllerSA202x = 4,
        /// <summary>
        /// -- 5 - BBCopier:CR1120 (Host Value: 11)
        /// </summary>
        BBCopierCR1120 = 5,
        /// <summary>
        /// -- 6 - BBCopier:CR1122 (Host Value: 11)
        /// </summary>
        BBCopierCR1122 = 6,
        /// <summary>
        /// -- 7 - BBCopier:CR2000 (Host Value: 12)
        /// </summary>
        BBCopierCR2000 = 7,
        /// <summary>
        /// -- 8 - BBCopier:CR2002 (Host Value: 12)
        /// </summary>
        BBCopierCR2002 = 8,
        /// <summary>
        /// -- 9 - BBCopier:CR3000 (Host Value: 2)
        /// </summary>
        BBCopierCR3000 = 9,
        /// <summary>
        /// -- 10 - BBVendingReader: VR/MDB  (Host Value: 10)
        /// </summary>
        VRMDB = 10,
        /// <summary>
        /// -- 11 - BBVendingReader: VR/MDB2 (Host Value: 1)
        /// </summary>
        VRMDB2 = 11,
        /// <summary>
        /// -- 12 - BBVendingReader: VR/MM (Host Value: 10)
        /// </summary>
        VRMM = 12,
        /// <summary>
        /// -- 13 - BBVendingReader: VR/SP (Host Value: 10)
        /// </summary>
        VRSP = 13,
        /// <summary>
        /// -- 14 - BBVendingReader: VR/MDBMP (Host Value: 1)
        /// </summary>
        VRMDBMP = 14,
        /// <summary>
        /// -- 15 - BBLaundry: MW9010 (Host Value: 13)
        /// </summary>
        MW9010 = 15,
        /// <summary>
        /// -- 16 - BBLaundry: MW9012 (Host Value: 14)
        /// </summary>
        MW9012 = 16,
        /// <summary>
        /// -- 17 - BBLaundry:  LC3000 (Host Value: 3)
        /// </summary>
        LC3000 = 17,
        /// <summary>
        /// -- 18 - AMC: AC3000 (Host Value: 9)
        /// </summary>
        AC3000 = 18,
        /// <summary>
        /// -- 19 - VTS (Host Value: 15)
        /// </summary>
        VTS = 19,
        /// <summary>
        /// -- 20 - BBVendingReader: VR4100 (Host Value: 16)
        /// </summary>
        VR4100 = 20,
        /// <summary>
        /// -- 21 - BBCopier: CR4100 (Host Value: 17)
        /// </summary>
        CR4100 = 21,
        /// <summary>
        /// -- 22 - BBLaundry: LC4100 (Host Value: 18)
        /// </summary>
        LC4100 = 22
    }

    /// <summary>
    /// Device types used in the registration process for a device.
    /// </summary>
    public enum DeviceRegistrationDeviceType
    {
        /// <summary>
        /// Numerical value = 1. AT3000 style transaction terminal.
        /// </summary>
        AT3000 = 1,
        /// <summary>
        /// Numerical value = 2. MF4100 style transaction terminal.
        /// </summary>
        MF4100 = 2,
        /// <summary>
        /// Numerical value = 3. PR5000/Slate style transaciton terminal.
        /// </summary>
        PR5000 = 3,
        /// <summary>
        /// Numerical value = 4. Workstation point of sale.
        /// </summary>
        Workstation = 4
    }

    /// <summary>
    /// Enumeration for all actions the device can take to update itself
    /// </summary>
    public enum DeviceUpdateActions
    {
        /// <summary>
        /// Fetch card formats again.
        /// </summary>
        CardFormat,
        /// <summary>
        /// Sync date and time with the NTP server
        /// </summary>
        DateTimeSync,
        /// <summary>
        /// Fetch cashier information again.
        /// </summary>
        Cashiers,
        /// <summary>
        /// Fetch product information again.
        /// </summary>
        Products,
        /// <summary>
        ///  Fetch tax schedules again.
        /// </summary>
        TaxSchedules,
        /// <summary>
        ///  Fetch tenders again.
        /// </summary>
        Tenders,
        /// <summary>
        /// Download and update the device software.
        /// </summary>
        Software
    }

    /// <summary>
    /// Enumeration of schedule types for an event on a device.
    /// </summary>
    public enum EventDeviceSettingScheduleType
    {
        /// <summary>
        /// Daily
        /// </summary>
        Daily = 1,
        /// <summary>
        /// Weekly
        /// </summary>
        Weekly = 2,
        /// <summary>
        /// Monthly
        /// </summary>
        Monthly = 3,
        /// <summary>
        /// Custom
        /// </summary>
        Custom = 4
    }
}
