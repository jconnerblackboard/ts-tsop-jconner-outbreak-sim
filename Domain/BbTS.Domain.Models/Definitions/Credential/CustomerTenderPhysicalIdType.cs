﻿namespace BbTS.Domain.Models.Definitions.Credential
{
    /// <summary>
    /// Enumeration of physical id types.
    /// </summary>
    public enum CustomerTenderPhysicalIdType
    {
        /// <summary>
        /// Card
        /// </summary>
        Card = 0 ,

        /// <summary>
        /// Contactless
        /// </summary>
        ContactLess = 1
    }
}