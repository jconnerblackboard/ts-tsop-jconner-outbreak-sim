﻿namespace BbTS.Domain.Models.Definitions.Credential
{
    /// <summary>
    /// Enumeration of possible credential values.
    /// </summary>
    public enum CredentialType
    {
        /// <summary>
        /// Credential origin is from a card swipe of Track2 data.
        /// </summary>
        Track2 = 0,
        /// <summary>
        /// Credential origin was manually keyed.
        /// </summary>
        ManualEntry = 1,
        /// <summary>
        /// Credential is an email address.
        /// </summary>
        Email = 2,
        /// <summary>
        /// Credential is a PIN.
        /// </summary>
        Pin = 3,
        /// <summary>
        /// Credential is a unique identifier for a user (Guid).
        /// </summary>
        UserGuid = 4,
        /// <summary>
        /// Credential is a card number.
        /// </summary>
        CardNumber = 5,
        /// <summary>
        /// Credential is an issue number.
        /// </summary>
        IssueNumber = 6,
        /// <summary>
        /// The unique identifier assigned to an agent in the system (i.e. Customer, Operator, etc.).
        /// </summary>
        IdNumber = 7
    }

    /// <summary>
    /// Specifies where to begin counting for the Location index.
    /// </summary>
    public enum ParseAnchor
    {
        /// <summary>
        /// Location index should be counted from the very beginning of the raw track data.
        /// </summary>
        TrackBeginning,

        /// <summary>
        /// Location index should be counted from immediately after the first field separator in the raw track data.
        /// </summary>
        FirstSeparator
    }
}
