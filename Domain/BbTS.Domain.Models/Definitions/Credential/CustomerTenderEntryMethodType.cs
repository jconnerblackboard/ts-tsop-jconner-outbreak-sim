﻿namespace BbTS.Domain.Models.Definitions.Credential
{
    /// <summary>
    /// Enumeration of credential entry methods.
    /// </summary>
    public enum CustomerTenderEntryMethodType
    {
        /// <summary>
        /// Swiped
        /// </summary>
        Swiped = 0,

        /// <summary>
        /// ManuallyEntered
        /// </summary>
        ManuallyEntered = 1
    }
}