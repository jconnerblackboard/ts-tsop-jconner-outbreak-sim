﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BbTS.Domain.Models.Definitions.Logging
{
    public enum ProcessTrackingObjectState
    {
        Unknown,
        NotStarted,
        InProgress,
        Completed
    }
}
