﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BbTS.Domain.Models.Definitions.Logging
{
    public enum EventingSources
    {
        Unspecified,
        DsrSyncTracing,
        GeneralTracing,
        HostMonitor,
        EndpointDebug
    }
    public class EventingDefinitions
    {
        private static readonly Dictionary<EventingSources, string> EventingSourceNames = new Dictionary<EventingSources, string>
        {
            { EventingSources.DsrSyncTracing, "Blackboard DsrSync Eventing" },
            { EventingSources.GeneralTracing, "Blackboard Transact Tracing Events" },
            { EventingSources.HostMonitor, "Blackboard Transact Host Monitor Events" },
            { EventingSources.EndpointDebug, "Blackboard Endpoint Debugging Events" }
        };

        /// <summary>
        /// Get the Name associated with the event source.
        /// </summary>
        /// <param name="source">EventingSources Source name</param>
        /// <returns></returns>
        public static string EventSourceProviderStringGet(EventingSources source)
        {
            if (!EventingSourceNames.ContainsKey(source)) throw new ArgumentException(String.Format("EventingDefinitions::EventSourceProviderStringGet {0} key not found in name catalog.", source));
            return EventingSourceNames[source];
        }

        /// <summary>
        /// Get the list of event source names as a list of strings.
        /// </summary>
        /// <returns></returns>
        public static List<String> EventSourceTypesGetAll()
        {
            return new List<string>(EventingSourceNames.Keys.Select(key => key.ToString()));
        }
    }
}
