﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Resources;
using BbTS.Domain.Models.Transaction;

namespace BbTS.Domain.Models.Definitions.Logging
{
    /// <summary>
    /// Utility class to map errors to error text.
    /// </summary>
    public static class BbtsErrorDefinitions
    {
        private static readonly Dictionary<LoggingDefinitions.EventId, string> BbtsErrors = new Dictionary<LoggingDefinitions.EventId, string>
        {
            #region Dictionary values
            { LoggingDefinitions.EventId.Unknown, BbtsErrorResource.Unknown },
            { LoggingDefinitions.EventId.Success, BbtsErrorResource.Success },

            { LoggingDefinitions.EventId.CredentialVerifyNoCustomerForEmail, BbtsErrorResource.CredentialVerifyNoCustomerForEmail },
            { LoggingDefinitions.EventId.CredentialVerifyNoCustomerForTrack2, BbtsErrorResource.CredentialVerifyNoCustomerForTrack2 },
            { LoggingDefinitions.EventId.CredentialVerifyNoCustomerForManualEntry, BbtsErrorResource.CredentialVerifyNoCustomerForManualEntry },
            { LoggingDefinitions.EventId.CredentialVerifyNoCustomerForCardNumberAndIssueCode, BbtsErrorResource.CredentialVerifyNoCustomerForCardNumberAndIssueCode },
            { LoggingDefinitions.EventId.CredentialVerifyPinRequiresOtherCredential, BbtsErrorResource.CredentialVerifyPinRequiresOtherCredential },
            { LoggingDefinitions.EventId.CredentialVerifyPinValidation, BbtsErrorResource.CredentialVerifyPinValidation },

            { LoggingDefinitions.EventId.PosNotFound, BbtsErrorResource.PosNotFound },
            { LoggingDefinitions.EventId.InvalidTenderNumber, BbtsErrorResource.InvalidTenderNumber },
            { LoggingDefinitions.EventId.OutsideValidPeriodTime, BbtsErrorResource.OutsideValidPeriodTime },
            { LoggingDefinitions.EventId.CustomerNotFoundInSystem, BbtsErrorResource.CustomerNotFoundInSystem },
            { LoggingDefinitions.EventId.InvalidTransactionId, BbtsErrorResource.InvalidTransactionId },
            { LoggingDefinitions.EventId.InvalidControlTransactionType, BbtsErrorResource.InvalidControlTransactionType },
            { LoggingDefinitions.EventId.ValidationTypeInvalid, BbtsErrorResource.ValidationTypeInvalid },
            { LoggingDefinitions.EventId.AttendedTypeInvalid, BbtsErrorResource.AttendedTypeInvalid },
            { LoggingDefinitions.EventId.RetailTransactionTypeInvalid, BbtsErrorResource.RetailTransactionTypeInvalid },
            { LoggingDefinitions.EventId.InvalidTransactionNumber, BbtsErrorResource.InvalidTransactionNumber },
            { LoggingDefinitions.EventId.NoTransactionTender, BbtsErrorResource.NoTransactionTender },

            { LoggingDefinitions.EventId.CardNotInSystem, BbtsErrorResource.CardNotInSystem },
            { LoggingDefinitions.EventId.CardNotFound, BbtsErrorResource.CardNotFound },
            { LoggingDefinitions.EventId.InvalidCustomerPin, BbtsErrorResource.InvalidCustomerPin },
            { LoggingDefinitions.EventId.InvalidIssueNumber, BbtsErrorResource.InvalidIssueNumber },
            { LoggingDefinitions.EventId.CardMarkedLost, BbtsErrorResource.CardMarkedLost },
            { LoggingDefinitions.EventId.CardStatusUnknown, BbtsErrorResource.CardStatusUnknown },
            { LoggingDefinitions.EventId.CardStatusRetired, BbtsErrorResource.CardStatusRetired },
            { LoggingDefinitions.EventId.CardStatusFrozen, BbtsErrorResource.CardStatusFrozen },
            { LoggingDefinitions.EventId.CardStatusCustomerDeleted, BbtsErrorResource.CardStatusCustomerDeleted },
            { LoggingDefinitions.EventId.CardStatusInvalid, BbtsErrorResource.CardStatusInvalid },
            { LoggingDefinitions.EventId.CardOutsideActiveStartEndDates, BbtsErrorResource.CardOutsideActiveStartEndDates },
            { LoggingDefinitions.EventId.CustomerNotActive, BbtsErrorResource.CustomerNotActive },
            { LoggingDefinitions.EventId.CustomerOutsideActiveStartEndDates, BbtsErrorResource.CustomerOutsideActiveStartEndDates },

            { LoggingDefinitions.EventId.InvalidMerchantTender, BbtsErrorResource.InvalidMerchantTender },
            { LoggingDefinitions.EventId.PolicyTransactionDenied, BbtsErrorResource.PolicyTransactionDenied },
            { LoggingDefinitions.EventId.PolicyTransactionStatus, BbtsErrorResource.PolicyTransactionStatus },
            { LoggingDefinitions.EventId.InvalidTenderMap, BbtsErrorResource.InvalidTenderMap },
            { LoggingDefinitions.EventId.NoCustomerStoredValueAccounts, BbtsErrorResource.NoCustomerStoredValueAccounts },
            { LoggingDefinitions.EventId.TempCardUnassigned, BbtsErrorResource.TempCardUnassigned },

            { LoggingDefinitions.EventId.TransactionClassificationUnknown, BbtsErrorResource.TransactionClassificationUnknown },
            { LoggingDefinitions.EventId.TransactionClassificationInvalid, BbtsErrorResource.TransactionClassificationInvalid },
            { LoggingDefinitions.EventId.DispositionInvalid, BbtsErrorResource.DispositionInvalid },
            { LoggingDefinitions.EventId.TimestampHistoryIncomplete, BbtsErrorResource.TimestampHistoryIncomplete },
            { LoggingDefinitions.EventId.TimestampHistoryInvalid, BbtsErrorResource.TimestampHistoryInvalid },
            { LoggingDefinitions.EventId.CustomerCardInformationNotPresent, BbtsErrorResource.CustomerCardInformationNotPresent },
            { LoggingDefinitions.EventId.CustomerCredentialDiscrepancy, BbtsErrorResource.CustomerCredentialDiscrepancy },

            { LoggingDefinitions.EventId.ElementProcessingFailed, BbtsErrorResource.ElementProcessingFailed},
            { LoggingDefinitions.EventId.ElementMissing, BbtsErrorResource.ElementMissing },
            { LoggingDefinitions.EventId.ArtsElementMissing, BbtsErrorResource.ArtsElementMissing },
            { LoggingDefinitions.EventId.AttendanceElementMissing, BbtsErrorResource.AttendanceElementMissing },
            { LoggingDefinitions.EventId.BoardElementMissing, BbtsErrorResource.BoardElementMissing },
            { LoggingDefinitions.EventId.ControlElementMissing, BbtsErrorResource.ControlElementMissing },
            { LoggingDefinitions.EventId.OriginatorElementMissing, BbtsErrorResource.OriginatorElementMissing },
            { LoggingDefinitions.EventId.OperatorElementMissing, BbtsErrorResource.OperatorElementMissing },

            { LoggingDefinitions.EventId.ControlTotalMissingOrInvalid, BbtsErrorResource.ControlTotalMissingOrInvalid },
            { LoggingDefinitions.EventId.LineItemProcessingFailed, BbtsErrorResource.LineItemProcessingFailed },
            { LoggingDefinitions.EventId.LineItemTypeInvalid, BbtsErrorResource.LineItemTypeInvalid },

            { LoggingDefinitions.EventId.DeviceNotFound, BbtsErrorResource.DeviceNotFound },
            { LoggingDefinitions.EventId.DeviceNotRegistered, BbtsErrorResource.DeviceNotRegistered },
            #endregion
        };

        /// <summary>
        /// Get a single error message from the error name.
        /// </summary>
        /// <param name="errorCode">Code as it relates to <see cref="LoggingDefinitions.EventId"/></param>
        /// <returns></returns>
        public static string ErrorMessageGet(int errorCode)
        {
            var eventId = (LoggingDefinitions.EventId)errorCode;
            var resource = new ResourceManager(typeof(BbtsErrorResource));
            var set = resource.GetResourceSet(CultureInfo.CurrentCulture, true, true);
            return set.GetString(eventId.ToString(), true);
        }

        /// <summary>
        /// Get a single error message from the error name.
        /// </summary>
        /// <param name="eventId">Code as it relates to <see cref="LoggingDefinitions.EventId"/></param>
        /// <returns></returns>
        public static string ErrorMessageGet(LoggingDefinitions.EventId eventId)
        {
            var resource = new ResourceManager(typeof(BbtsErrorResource));
            var set = resource.GetResourceSet(CultureInfo.CurrentCulture, true, true);
            return set.GetString(eventId.ToString(), true);
        }

        /// <summary>
        /// Checks if a dictionary entry exists for the specified error.
        /// </summary>
        /// <param name="error"></param>
        /// <returns></returns>
        public static bool ErrorExistsInDictionary(LoggingDefinitions.EventId error)
        {
            string message;
            return BbtsErrors.TryGetValue(error, out message);
        }

        /// <summary>
        /// Gets error text (concatenated with the supplied additional information) for the specified error.
        /// </summary>
        /// <param name="error"></param>
        /// <param name="additionalInformation"></param>
        /// <param name="requestId"></param>
        /// <returns></returns>
        public static ProcessingResult ErrorWithAdditionalInfoGet(LoggingDefinitions.EventId error, string additionalInformation, string requestId = null)
        {
            string message;
            if (!BbtsErrors.TryGetValue(error, out message))
            {
                message = $"{BbtsErrors[LoggingDefinitions.EventId.Unknown]}:  {error}";
            }

            return new ProcessingResult
            {
                RequestId = requestId,
                ErrorCode = (int)error,
                DeniedText = string.IsNullOrEmpty(additionalInformation) ? message : message + " (" + additionalInformation + ")"
            };
        }

        /// <summary>
        /// Attempts to match the supplied error code with a <see cref="LoggingDefinitions.EventId"/> value and return error text for the specified error.
        /// Error text for the specified error is concatenated with the supplied additional information.
        /// </summary>
        /// <param name="errorCode"></param>
        /// <param name="additionalInformation"></param>
        /// <param name="requestId"></param>
        /// <returns></returns>
        public static ProcessingResult ErrorWithAdditionalInfoGet(int errorCode, string additionalInformation, string requestId = null)
        {
            if (!Enum.IsDefined(typeof(LoggingDefinitions.EventId), errorCode))
            {
                string additionalInfo = string.Empty;
                if (!string.IsNullOrEmpty(additionalInformation))
                {
                    additionalInfo = $"  {additionalInformation}";
                }

                return ErrorWithAdditionalInfoGet(LoggingDefinitions.EventId.Unknown, $"Error Code {errorCode} does not correspond to any {nameof(LoggingDefinitions.EventId)} value.{additionalInfo}", requestId);
            }

            var error = (LoggingDefinitions.EventId)errorCode;
            return ErrorWithAdditionalInfoGet(error, additionalInformation, requestId);
        }

        /// <summary>
        /// Attempts to match the supplied error code with a <see cref="LoggingDefinitions.EventId"/> value and return error text for the specified error.
        /// </summary>
        /// <param name="errorCode"></param>
        /// <param name="requestId"></param>
        /// <returns></returns>
        public static ProcessingResult ErrorGet(int errorCode, string requestId = null)
        {
            return ErrorWithAdditionalInfoGet(errorCode, null, requestId);
        }

        /// <summary>
        /// Gets error text for the specified error.
        /// </summary>
        /// <param name="error"></param>
        /// <param name="requestId"></param>
        /// <returns></returns>
        public static ProcessingResult ErrorGet(LoggingDefinitions.EventId error, string requestId = null)
        {
            return ErrorWithAdditionalInfoGet(error, null, requestId);
        }

        /// <summary>
        /// Gets error text for the specified error.
        /// </summary>
        /// <param name="error"></param>
        /// <param name="requestId"></param>
        /// <returns></returns>
        public static ProcessingResult ErrorGet(LoggingDefinitions.EventId error, Guid requestId)
        {
            return ErrorWithAdditionalInfoGet(error, null, requestId.ToString("D").ToLowerInvariant());
        }

        /// <summary>
        /// Sets ErrorCode and DeniedText for this <see cref="ProcessingResult"/> instance based on the supplied <see cref="LoggingDefinitions.EventId"/>.
        /// </summary>
        /// <param name="processingResult"></param>
        /// <param name="error"></param>
        public static void ErrorSet(this ProcessingResult processingResult, LoggingDefinitions.EventId error)
        {
            var result = ErrorGet(error);
            processingResult.ErrorCode = result.ErrorCode;
            processingResult.DeniedText = result.DeniedText;
        }
    }
}
