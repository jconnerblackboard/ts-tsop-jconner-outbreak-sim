﻿using System;

namespace BbTS.Domain.Models.Definitions.Logging
{
    /// <summary>
    /// Logging definitions.
    /// </summary>
    public class LoggingDefinitions
    {
        /// <summary>
        /// Category.
        /// </summary>
        public enum Category : short
        {
            /// <summary>
            /// General.
            /// </summary>
            General = 10,

            /// <summary>
            /// Service.
            /// </summary>
            Service = 11,

            /// <summary>
            /// Resource.
            /// </summary>
            Resource = 12,

            /// <summary>
            /// Trace.
            /// </summary>
            Trace = 13,

            /// <summary>
            /// Tracking.
            /// </summary>
            Tracking = 14,

            /// <summary>
            /// Agent.
            /// </summary>
            Agent = 15,

            /// <summary>
            /// Database.
            /// </summary>
            Database = 16,

            /// <summary>
            /// Web API.
            /// </summary>
            WebApi = 17,

            /// <summary>
            /// Tool.
            /// </summary>
            Tool = 18,

            /// <summary>
            /// Transact API.
            /// </summary>
            TransactApi = 19,

            /// <summary>
            /// Host monitor.
            /// </summary>
            HostMonitor = 20,

            /// <summary>
            /// Subscription service.
            /// </summary>
            SubscriptionService = 21,

            /// <summary>
            /// Security monitor.
            /// </summary>
            SecurityMonitor = 22,

            /// <summary>
            /// Communication utility.
            /// </summary>
            CommunicationUtility = 23,

            /// <summary>
            /// Host packets utility.
            /// </summary>
            HostPacketsUtility = 24,

            /// <summary>
            /// WCF exception handling.
            /// </summary>
            WcfExceptionHandling = 25,

            /// <summary>
            /// Event hub agent.
            /// </summary>
            EventHubAgent = 26,

            /// <summary>
            /// Azure table storage.
            /// </summary>
            AzureTableStorage = 27,

            /// <summary>
            /// General middle tier.
            /// </summary>
            MiddleTier = 28,

            /// <summary>
            /// Oauth related stuff.
            /// </summary>
            Oauth = 29,

            /// <summary>
            /// Reporting metrics for mobile credential.
            /// </summary>
            ReportingMetrics = 30,

            /// <summary>
            /// External client functions integrated into Web Api
            /// </summary>
            ExternalClientOnWebApi = 31,
            
            /// <summary>
            /// Agent integration monitor.
            /// </summary>
            AgentIntegrationMonitor = 500
        }

        /// <summary>
        /// Value added to EventId values that are based on the DENIED_MESSAGE table.
        /// </summary>
        public const int DatabaseErrorCodeOffset = 100000;

        /// <summary>
        /// Maximum value for a value based on the DENIED_MESSAGE table (after offset).
        /// </summary>
        public const int DatabaseErrorCodeMaxValue = DatabaseErrorCodeOffset + 99999;

        /// <summary>
        /// Event ID.
        /// </summary>
        public enum EventId
        {
#pragma warning disable 1591  //Disables warnings for no XML documentation (ReSharper name suggestions get lost in the noise)

            /// <summary>
            /// Unknown.
            /// </summary>
            Unknown = -1,

            /// <summary>
            /// Success.
            /// </summary>
            Success = 0,
            General = 1,
            MethodIgnored = 3,
            UnknownFatalError = 5,
            GeneralDebuggingMessage = 6,
            ValidationError = 7,
            DisplayActionResult = 50,
            ThreadInterrupted = 100,
            ThreadStopped = 101,
            ThreadStarted = 102,

            TransactionSystemNotRegistered = 150,

            CommunicationUtilityCouldNotConnect = 400,

            DatabaseConnectionFailed = 500,

            RegistryConfigurationValueMissing = 600,

            HostMonitorMsmqCallbackError = 700,
            HostMonitorParsingError = 701,

            SecurityMonitorMsmqCallbackError = 720,
            SecurityMonitorParsingError = 721,

            SubscriptionServiceCallbackError = 751,

            MiddleTierBusinessLogError = 800,

            SerivceStopped = 900,
            ServiceStarted = 901,
            ServiceStartException = 902,
            ServiceStopException = 903,
            ServiceInstalled = 904,
            SerivceInstallException = 905,
            ServiceUninstalled = 906,
            ServiceUninstallException = 907,
            ServiceUsermodeFailedToStart = 908,
            ServiceCommandLineExecuteMode = 909,
            ServiceUsermodeException = 910,
            ServiceStarting = 911,
            ServiceStopping = 912,

            ServiceDsrSyncServiceRunningInManualWarning = 1000,
            ServiceDsrSyncServiceRunningProxyResource = 1001,
            ServiceDsrSyncServiceStartupException = 1002,
            ServiceDsrSyncServiceShutdownException = 1003,
            ServiceDsrSyncServiceSyncLoopException = 1004,
            ServiceDsrSyncServiceStartMessage = 1005,
            ServiceDsrSyncServiceStopMessage = 1006,
            ServiceDsrSyncServiceSyncOperationStarted = 1007,
            ServiceDsrSyncServiceSyncOperationComplete = 1008,
            ServiceDsrSyncServiceClearDsrListenerStarted = 1009,
            ServiceDsrSyncServiceClearDsrOperationStarted = 1010,
            ServiceDsrSyncServiceClearDsrOperationCompleted = 1011,
            ServiceDsrSyncServiceClearDsrListenerException = 1012,
            ServiceDsrSyncServiceClearDsrListenerStopped = 1013,
            ServiceDsrSyncServiceLogLoopStarted = 1014,
            ServiceDsrSyncServiceLogLoopOperationStarted = 1015,
            ServiceDsrSyncServiceLogLoopOperationCompleted = 1016,
            ServiceDsrSyncServiceLogLoopException = 1017,
            ServiceDsrSyncServiceLogLoopStopped = 1018,
            ServiceDsrSyncServiceSyncOperationException = 1019,
            ServiceDsrSyncServiceSyncOperationFailed = 1020,
            ServiceDsrSyncServiceClearDsrOperationException = 1021,

            ServiceBbTsWebApiRunLoopException = 1200,

            ServiceRestExceptionAttribute = 1500,

            ServiceHostConnectionException = 1550,
            ServiceHostSslConnectionError = 1551,

            DomainPersonaRestAttributeException = 2000,
            DomainPersonaTimingAttributeMessage = 2001,
            DomainPersonaDsrCallbackNotifyUpdate = 2002,
            DomainPersonaDsrCallbackNewEvent = 2003,
            DomainPersonaAccessPointDeleteException = 2004,
            DomainPersonaAccessPointUpdateException = 2005,
            DomainPersonaAccessPointModeAddException = 2006,
            DomainPersonaAccessPointModeDeleteException = 2007,
            DomainPersonaAccessPointModeUpdateException = 2008,
            DomainPersonaAccessPointModeValidateException = 2009,
            DomainPersonaAuthorizationAddException = 2010,
            DomainPersonaAuthorizationDeleteException = 2011,
            DomainPersonaAuthorizationUpdateException = 2012,
            DomainPersonaAuthorizationValidateException = 2013,
            DomainPersonaDayExceptionAddException = 2014,
            DomainPersonaDayExceptionDeleteException = 2015,
            DomainPersonaDayExceptionUpdateException = 2016,
            DomainPersonaDayExceptionGroupAddException = 2017,
            DomainPersonaDayExceptionGroupDeleteException = 2018,
            DomainPersonaDayExceptionGroupUpdateException = 2019,
            DomainPersonaDayPeriodAddException = 2020,
            DomainPersonaDayPeriodDeleteException = 2021,
            DomainPersonaDayPeriodUpdateException = 2022,
            DomainPersonaScheduleAddException = 2023,
            DomainPersonaScheduleDeleteException = 2024,
            DomainPersonaScheduleUpdateException = 2025,
            DomainPersonaUserAddException = 2026,
            DomainPersonaUserDeleteException = 2027,
            DomainPersonaUserUpdateException = 2028,
            DomainPersonaReloadOutOfSyncWarning = 2029,
            DomainPersonaReloadOutOfSyncFailed = 2030,
            DomainPersonaManualSyncRequested = 2031,
            DomainPersonaClearDsrRequested = 2032,
            DomainPersonaClearDsrResultReceived = 2033,

            ResourceDsrSyncResourceDsr = 3000,
            ResourceDsrSyncResourceOracle = 3001,
            ResourceInvalidEnumerationParse = 3002,

            AzureTableStorageConnectionStringGetSetFailure = 3500,

            TrackerFinalizeError = 4000,

            InstitutionServiceCommunicationFailed = 4500,

            AgentFailedToLoadAgentPlugin = 5000,
            AgentFailedToUnloadAgentPlugin = 5001,
            AgentManagementCycleFailed = 5002,
            AgentManagementCycleDiscoverNewAgentsFailed = 5003,
            AgentManagementCycleUnloadOldAgentsFailed = 5004,
            AgentUpdateFailedNoConfigurationFile = 5005,
            AgentUpdateFailedNoAssemblyNameInConfigFile = 5006,
            AgentUpdateFailedUnknownError = 5007,
            AgentUpdateFailedToLoadAssembly = 5008,
            AgentUpdateFailedUpdateNotFound = 5009,
            AgentUpdateServiceAgentDownloadFailed = 5010,
            AgentUpdateServiceAgentUploadFailed = 5011,
            AgentUpdateServiceAgentListFailed = 5012,
            AgentDescriptorFailedToRetrieve = 5013,
            AgentServiceApiException = 5014,
            AgentServiceBusinessException = 5015,
            AgentManagementCycleStarted = 5016,
            AgentManagementCycleCompleted = 5017,

            AgentIntegrationMonitorManagementIntervalFailure = 5500,
            AgentIntegrationMonitorFailedUploadedDevicesFound = 5501,
            AgentIntegrationMonitorSuccessfullyUploadedDevicesFound = 5502,
            AgentIntegrationMonitorCountOfDevicesFound = 5503,
            AgentIntegrationMonitorCountOfResultsFound = 5504,

            AgentMetricsProcessingStartTimeHasNotBeenSet = 5600,

            AgentReportingServiceError = 5700,
            AgentReportingServiceDailyProcessing = 5701,
            AgentReportingServiceWeeklyProcessing = 5702,
            AgentReportingServiceMonthlyProcessing = 5703,

            DateTimeServiceTimeAdjustmentNotification = 6000,
            DateTimeServiceMissingTransactApiBaseUri = 6001,

            WebApiServiceApiException = 6997,
            WebApiServiceBusinessException = 6998,
            WebApiHealthcheckGeneralFailure = 6999,
            WebApiHealthcheckValidateDatabaseConnectionFailed = 7000,
            WebApiHealthcheckValidateBbSPConnectionFailed = 7001,
            WebApiHealthcheckValidateOauthConnection = 7002,
            WebApiHealthcheckValidateRegistration = 7003,
            WebApiHealthcheckLoopbackValidation = 7004,
            WebApiHealthcheckLoopbackNoAppCredentials = 7005,
            WebApiHealthcheckBbSPCheckNoBbSPUrl = 7006,
            WebApiHealthcheckOAuthCheckNoBbSPUrl = 7007,
            WebApiHealthcheckRegistrationSystemNotRegistered = 7008,
            WebApiHealthcheckRegistrationSystemNotValid = 7009,
            WebApiBbSPApplicationCredentialSync = 7010,
            WebApiBbSPInstitutionRouteSync = 7011,
            WebApiBbSPMerchantSync = 7012,
            WebApiHealthcheckBbSPSyncValidationFailed = 7013,
            WebApiTransactionProcessing = 7014,
            WebApiInvalidDataValueAtEndpoint = 7015,

            TransactApiHealthcheckDatabaseConnectivity = 7100,

            OAuthValidationFailed = 7500,

            MsmqAttributeException = 8000,

            EventHubAgentNotificationsProcessFailure = 8500,
            EventHubAgentNotificationsInformationalMessage = 8501,
            EventHubAgentNotificationsHubSendFailure = 8502,
            EventHubAgentNotificationsHubSendNotificationFailure = 8503,
            EventHubAgentNotificationsHubSendImageFailure = 8504,
            EventHubAgentNotificationsHubSendNotificationTimeout = 8505,
            EventHubAgentDbmsAlertListenerFailed = 8506,
            EventHubAgentDbmsAlertRegistrationFailed = 8507,

            ReportingMetricsValidationError = 8550,
            ReportingMetricsGeneralException = 8551,
            ReportingMetricsDailyMetricsGatheringProcCallFail = 8552,
            ReportingMetricsValidationReportNotFound = 8553,
            ReportingMetricsDailyMetricsStartDateFetchFail = 8554,

            ResourceLayerExceptionBoardInformationGetReturnedNoResults = 9000,
            ResourceLayerExceptionOracleDependencyCallbackRegistrationFailed = 9001,
            ResourceLayerExceptionDbmsWaitForEventFailed = 9002,
            ResourceLayerExceptionFailedResultParse = 9003,

            CustomerVerifyBbSPNotSynced = 10000,
            CustomerVerifyInvalidCredentials = 10001,
            CustomerVerifyCardInfoNotFound = 10002,
            CustomerVerifyCustomerNotFound = 10003,
            CustomerVerifyCardNumberNotSpecified = 10004,
            CustomerVerifyMissingArgument = 10005,
            CustomerVerifyInvalidInstitution = 10006,

            CredentialVerifyNoCustomerForEmail = 10007,
            CredentialVerifyNoCustomerForTrack2 = 10008,
            CredentialVerifyNoCustomerForManualEntry = 10009,
            CredentialVerifyNoCustomerForCardNumberAndIssueCode = 10010,
            CredentialVerifyPinRequiresOtherCredential = 10011,
            CredentialVerifyPinValidation = 10012,

            /// <summary>
            /// The card status is unknown.
            /// </summary>
            CardStatusUnknown = 10013,

            /// <summary>
            /// The card status is customer deleted.
            /// </summary>
            CardStatusCustomerDeleted = 10014,
            CardUpdateFailedCardRetired = 10015,
            CardUpdateFailedCardNotExist = 10016,
            CardUpdateFailedCardBelongsToAnotherCustomer = 10017,

            /// <summary>
            /// The card status is invalid.
            /// </summary>
            CardStatusInvalid = 10018,

            // ExceptionSeverityDefinitions moved from the database to here.
            ApplicationUnauthorized = 20500,
            AuthenticationFailurePhase1 = 20501,
            AuthenticationFailurePhase2 = 20502,
            AuthenticationFailurePhase3 = 20503,
            AuthenticationFailurePhase4 = 20504,
            AuthenticationFailurePhase6 = 20505,
            AuthenticationFailurePhase7 = 20506,
            AuthenticationFailureTokenMissingOrExpired = 20507,
            AuthenticationFailureInvalidSecret = 20508,
            AuthenticationFailureInvalidToken = 20509,
            UnableToProperlyLocateAuthenticationRecord = 20510,
            UnableToProperlyInsertAuthenticationApiPermissionRecord = 20511,
            AuthenticationIdIsRequiredParameter = 20512,
            UnableToProperlyInsertAuthenticationRecord = 20513,
            UnableToProperlyUpdateAuthenticationRecord = 20514,
            PerformHashCompareCheckFailed = 30000,
            TokenCheckFailed = 30001,
            TokenTimeoutCheckFailed = 30002,
            EmptyOAuthParameters = 30003,
            UnsupportedOAuthSignatureMethod = 30004,
            InvalidOrMissingConsumerKey = 30005,
            InvalidOrUsedNonceProvided = 30006,

            #region 100000 to 199999 - Reserved for errors that also exist in the DENIED_MESSAGE table.  Enum values in this section should equal DENIED_MESSAGE_ID + 100000 (DatabaseErrorCodeOffset). 
            //                                                                         Denied_Message_Id    Denied_Message_Name
            //                                                                                     -----    ------------------------------
            /// <summary>
            /// This is more trouble than it is worth to use.  Use <see cref="Success"/> instead.  Included here for completeness.
            /// </summary>
            [Obsolete]
            Accepted = 100000,                                                      //                 0    ACCEPTED
            
            /// <summary>
            /// POS not found.
            /// </summary>
            PosNotFound = 100001,                                                   //                 1    POS_NOT_FOUND
            CashierNotFound = 100002,                                               //                 2    CASHIER_NOT_FOUND
            NoBasePosSetupsFound = 100003,                                          //                 3    NO_BASE_POS_SETUPS_FOUND
            NoPosOptionsFound = 100004,                                             //                 4    NO_POS_OPTIONS_FOUND
            InvalidTransactionType = 100005,                                        //                 5    INVALID_TRANSACTION_TYPE

            /// <summary>
            /// Invalid tender number.
            /// </summary>
            InvalidTenderNumber = 100006,                                           //                 6    INVALID_TENDER_NUMBER

            /// <summary>
            /// Outside valid period time.
            /// </summary>
            OutsideValidPeriodTime = 100007,                                        //                 7    OUTSIDE_VALID_PERIOD_TIME
            ProductPromoNotFound = 100008,                                          //                 8    PRODUCT_PROMO_NOT_FOUND

            /// <summary>
            /// Customer not found.
            /// </summary>
            CustomerNotFoundInSystem = 100009,                                      //                 9    CUSTOMER_NOT_FOUND_IN_SY
            CustomerNotFoundInModule = 100010,                                      //                10    CUSTOMER_NOT_FOUND_IN_MODULE

            /// <summary>
            /// The card status is retired.
            /// </summary>
            CardStatusRetired = 100011,                                             //                11    CARD_RETIRED
            CustomerStatusDenied = 100012,                                          //                12    CUSTOMER_STATUS_DENIED
            CustomerFunctionStatusDenied = 100013,                                  //                13    CUSTOMER_FUNC_STATUS_DENIED

            /// <summary>
            /// Customer outside active start/end dates.
            /// </summary>
            CustomerOutsideActiveStartEndDates = 100015,                            //                15    CUSTOMER_OUTSIDE_ACTIVE_CNCL
            CustomerOutsideFunctionActivationCancellationDate = 100016,             //                16    CUSTOMER_OUTSIDE_FUNC_ACT_CNCL
            CustomerIdValidated = 100017,                                           //                17    CUSTOMER_ID_VALIDATED
            CustomerOverSystemSwipesPin = 100018,                                   //                18    CUSTOMER_OVER_SWIPES_PIN
            CustomerOverSystemSwipesInvalidPin = 100019,                            //                19    CUSTOMER_OVER_SWIPES_INVAL_PIN
            CustomerOverModuleSwipesPin = 100020,                                   //                20    CUST_OVER_SWIPES_PIN
            CustomerOverModuleSwipesInvalidPin = 100021,                            //                21    CUST_OVER_SWIPES_INVALID_PIN
            CustomerNoBoardPlan = 100022,                                           //                22    CUST_NO_BOARD_PLAN
            BoardOutsideDateRange = 100023,                                         //                23    BOARD_OUTSIDE_DATE_RANGE
            MasterBoardInactive = 100024,                                           //                24    MASTER_BOARD_INACTIVE
            CustomerBoardPlanInactive = 100025,                                     //                25    CUST_BOARD_PLAN_INACTIVE
            InvalidBoardMealType = 100026,                                          //                26    INVALID_BOARD_MEAL_TYPE
            MealTypeNotAllowedForCustomer = 100027,                                 //                27    MEAL_TYPE_NOT_ALLOWED_CUST
            ProfitCenterNotAllowedForCustomer = 100028,                             //                28    PC_NOT_ALLOWED_CUST
            ExceedMaxMealsForPeriod = 100029,                                       //                29    EXCEED_MAX_MEALS_FOR_PD
            NoGuestMealsForCashEquiv = 100030,                                      //                30    NO_GUEST_MEAILS_FOR_CE
            ExceedMaxGuestMealsForPeriodWeek = 100031,                              //                31    EXCEED_MAX_GUEST_ML_PD
            UseRegularBeforeGuest = 100032,                                         //                32    USE_REGULAR_BEFORE_GUEST
            ExceedMaxGuestMealTotal = 100033,                                       //                33    EXCEED_MAX_GUEST_MEAL_TTL
            ExceedMaxMealsForPeriodWeek = 100034,                                   //                34    EXCEED_MAX_MEALS_FOR_PDWEEK
            ExceedMaxMealsForDay = 100035,                                          //                35    EXCEED_MAX_MEALS_FOR_DAY
            ExceedMaxMealsForWeek = 100036,                                         //                36    EXCEED_MAX_MEALS_FOR_WEEK
            ExceedMaxMealsForMonth = 100037,                                        //                37    EXCEED_MAX_MEALS_FOR_MNTH
            ExceedMaxMealsForQuarter = 100038,                                      //                38    EXCEED_MAX_MEALS_QTR
            ExceedMaxMealsForYear = 100039,                                         //                39    EXCEED_MAX_MEALS_YEAR
            TransactionExceedsCashEquivValue = 100040,                              //                40    TRAN_EXCEED_CE_VALUE
            NoBoardPlan = 100041,                                                   //                41    NO_BOARD_PLAN
            CustomerNoStoredValuePlan = 100042,                                     //                42    CUST_NO_SV_PLAN
            CustomerStoredValuePlanInactive = 100043,                               //                43    CUST_SV_PLAN_INACTIVE
            MasterStoredValuePlanInactive = 100044,                                 //                44    MASTER_SV_PLAN_INACTIVE
            OverMaxSalesDayPin = 100045,                                            //                45    OVER_MAX_SALES_DAY_PIN
            OverMaxSalesDayInvalidPin = 100046,                                     //                46    OVER_MAX_SALES_DAY_INVALIDPIN
            OverMaxTransactionAmountPin = 100049,                                   //                49    OVER_MAX_TRAN_AMT_PIN
            OverMaxTransactionAmountInvalidPin = 100050,                            //                50    OVER_MAX_TRAN_AMT_INVALIDPIN
            OverMaxSwipesDayNoPin = 100051,                                         //                51    OVER_MAX_SWIPES_DAY_NOPIN
            OverMaxSalesDayNoPin = 100053,                                          //                53    OVER_MAX_SALES_DAY_NOPIN
            OverMaxTransactionAmountNoPin = 100055,                                 //                55    OVER_MAX_TRAN_AMT_NOPIN
            NoCustomerFundsAvailable = 100056,                                      //                56    NO_CUSTOMER_FUNDS_AVAIL
            InactiveFundNumberForCustomer = 100057,                                 //                57    INACTIVE_FUNDNUM_FORCUST
            FundNotAllowedForCustomer = 100058,                                     //                58    FUND_NOT_ALLOWED_CUST
            InsufficientFunds = 100059,                                             //                59    INSUFFICIENT_FUNDS
            InvalidAutoFundCountsForCustomer = 100060,                              //                60    INVALID_AUTO_FUND_CNT_CUST
            ZeroIsInvalidAmount = 100061,                                           //                61    ZERO_INVALID_AMOUNT
            SubTaxNotDefinedForFund = 100062,                                       //                62    SUB_TAX_NOT_DEFINED_FUND
            FundNotActive = 100063,                                                 //                63    FUND_NOT_ACTIVE
            NoTransferMeals = 100066,                                               //                66    NO_TRANSFER_MEALS
            VendingAutoFundNotDefined = 100067,                                     //                67    VENDING_AUTOFUND_NOT_DEFINED
            InternalErrorDuplicateFundNumbers = 100070,                             //                70    INTERNAL_ERROR_DUP_FUNDNUMS
            ExceedMaxRegularAccessForEvent = 100071,                                //                71    EXCEED_MAX_REG_ACCESS_EVT
            ExceedMaxGuestAccessForEvent = 100072,                                  //                72    EXCEED_MAX_GST_ACCESS_EVT
            EventNotFound = 100073,                                                 //                73    EVENT_NOT_FOUND
            NoEventReentry = 100074,                                                //                74    NO_EVENT_REENTRY
            ProfitCenterNotAllowedForEvent = 100075,                                //                75    PC_NOT_ALLOWED_EVT
            EventNotActive = 100076,                                                //                76    EVENT_NOT_ACTIVE
            NoEventPlanForCustomer = 100077,                                        //                77    NO_EVT_PLAN_CUST
            InactiveEvent = 100078,                                                 //                78    INACTIVE_EVENT
            CustomerUnderage = 100079,                                              //                79    CUST_UNDERAGE
            CustomerUnknown = 100080,                                               //                80    CUST_UNKNOWN
            EventNotAllowedForCustomer = 100081,                                    //                81    EVT_NOT_ALLOWED_CUST
            EventNotAllowedGuest = 100082,                                          //                82    EVT_NOT_ALLOWED_GST
            InactiveEventPlan = 100083,                                             //                83    INACTIVE_EVT_PLAN
            EventPlanOutsideActivationCancellationDate = 100084,                    //                84    EVT_PLAN_OUTSIDE_ACT_CANCL
            SwipeOutRequired = 100085,                                              //                85    SWIPE_OUT_REQUIRED
            InactiveCustomerEventPlan = 100086,                                     //                86    INACTIVE_CUST_EVT_PLAN
            NonCustomerEntryDisallow = 100089,                                      //                89    NON_CUSTOMER_ENTRY_DISALLOW
            InvalidManagerCard = 100090,                                            //                90    INVALID_MANAGER_CARD
            InvalidCreditCardTypeForMerchant = 100091,                              //                91    INVALID_CC_TYPE_FOR_MERCH
            PendingEventTransactionInvalid = 100092,                                //                92    PENDING_EVENT_TRAN_INVALID
            EventEntryNotFound = 100093,                                            //                93    EVENT_ENTRY_NOT_FOUND
            CopierTenderNotFound = 100094,                                          //                94    COPIER_TENDER_NOT_FOUND
            NoPosOptionTenderIdFound = 100095,                                      //                95    NO_POSOPTIONTENDERID_FOUND
            NoPosOptionStordValueTenderFound = 100096,                              //                96    NO_POSOPTIONSTORDVALTNDR_FOUND
            NoCustomerPosStoredValueAccountsFound = 100097,                         //                97    NO_CUSTPOSSVACCTS_FOUND
            BadCashierPinUpdate = 100098,                                           //                98    BADCASHIERPINUPDATE
            BadOldCashierPin = 100099,                                              //                99    BADOLDCASHIERPIN
            CashNotAllowedForPurchase = 100100,                                     //               100    CASH_NOT_ALLOWED_PURCHASE
            CheckNotAllowedForPurchase = 100101,                                    //               101    CHECK_NOT_ALLOWED_PURCHASE
            CreditCardNotAllowedForPurchase = 100102,                               //               102    CC_NOT_ALLOWED_PURCHASE
            FundNotAllowedForPurchase = 100103,                                     //               103    FUND_NOT_ALLOWED_PURCHASE
            OverPaymentLimit = 100104,                                              //               104    OVER_PAYMENT_LIMIT
            GuestBoardInactiveForPeriodDay = 100105,                                //               105    GST_BOARD_INACTIVE_PDDAY
            BoardAccessInactiveForPeriodDay = 100106,                               //               106    BOARD_ACCESS_INACTIVE_PDDAY
            CustomerOverMaxSwipesPerDay = 100110,                                   //               110    MAX_SWIPES_DAY
            CustomerOverMaxSalesPerDay = 100112,                                    //               112    MAX_SALES_DAY
            MaxTransactionAmount = 100114,                                          //               114    MAX_TRANSACTION_ACCOUNT
            DeviceReinitializationNeeded = 100115,                                  //               115    DEVICE_REINITIALIZATION_NEEDED
            ControlTotalRequired = 100116,                                          //               116    CONTROL_TOTAL_REQUIRED

            /// <summary>
            /// Card outside active start/end dates.
            /// </summary>
            CardOutsideActiveStartEndDates = 100117,                                //               117    CARD_OUTSIDE_ACTIVE_CNCL
            CashierNotFound2 = 100200,                                              //               200    CASHIER_NOT_FOUND_2
            GatewayNotFound = 100201,                                               //               201    GATEWAY_NOT_FOUND
            RemotePrinterNotfound = 100202,                                         //               202    REMOTE_PRINTER_NOTFOUND
            HostFetchVerInfoFailed = 100250,                                        //               250    HOST_FETCH_VER_INFO_FAILED
            HostOpenFileFailed = 100251,                                            //               251    HOST_OPEN_FILE_FAILED
            HostFileDateTimeFailed = 100252,                                        //               252    HOST_FILE_DATE_TIME_FAILED
            HostReadFileFailed = 100253,                                            //               253    HOST_READ_FILE_FAILED
            DuplicateTransactionNumber = 100300,                                    //               300    DUPLICATE_TRAN_NUM
            DuplicateProduct = 100301,                                              //               301    DUPLICATE_PRODUCT
            InvalidPort = 100400,                                                   //               400    INVALID_PORT
            MerchantSetupNotFound = 100501,                                         //               501    MERCHANT_SETUP_NOT_FOUND
            CreditCardLogNotFound = 100502,                                         //               502    CCLOG_NOT_FOUND
            CreditCardSetupinfoNotFound = 100503,                                   //               503    CCSETUPINFO_NOT_FOUND
            CreditCardBatchNotFound = 100504,                                       //               504    CCBATCH_NOT_FOUND
            TransactionExistsInBatch = 100505,                                      //               505    TRANSACTION_EXISTS_IN_BATCH
            NoCreditCardHostConfigured = 100506,                                    //               506    NO_CC_HOST_CONFIGURED
            CreditCardTrackDataInvalid = 100507,                                    //               507    CC_TRACK_DATA_INVALID
            TiaSetupsNotFound = 100601,                                             //               601    TIA_SETUPS_NOT_FOUND
            InvalidTiaVendor = 100602,                                              //               602    INVALID_TIA_VENDOR
            TiaVendorInactive = 100603,                                             //               603    TIA_VENDOR_INACTIVE
            TiaTransactionTypeNotSupported = 100604,                                //               604    TIA_TRANS_TYPE_NOT_SUPPORTED
            TiaInvalidPin = 100608,                                                 //               608    TIA_INVALID_PIN

            /// <summary>
            /// The card is not entered in the System.
            /// </summary>
            CardNotInSystem = 100650,                                               //               650    CARD NOT IN SYSTEM
            CardInvalidIssueCode = 100651,                                          //               651    CARD_INVALID_ISSUE_CODE
            CardInvalidLost = 100652,                                               //               652    CARD_INVALID_LOST
            CardExpired = 100653,                                                   //               653    CARD_EXPIRED
            CardDeleted = 100654,                                                   //               654    CARD_DELETED
            CardSuspended = 100655,                                                 //               655    CARD_SUSPENDED
            CardOnHold = 100656,                                                    //               656    CARD_ON_HOLD
            TiaInsufficientFunds = 100657,                                          //               657    TIA_INSUFFICIENT_FUNDS
            OverTransactionLimit = 100658,                                          //               658    OVER_TRANS_LIMIT
            OverDailyLimit = 100659,                                                //               659    OVER_DAILY_LIMIT
            OverCreditLimit = 100660,                                               //               660    OVER_CREDIT_LIMIT
            OverCreditLimit2 = 100661,                                              //               661    OVER_CREDIT_LIMIT2
            PrivilegeUnassigned = 100662,                                           //               662    PRIVILEGE_UNASSIGNED
            PrivilegeSuspended = 100663,                                            //               663    PRIVILEGE_SUSPENDED
            PrivilegeExpired = 100664,                                              //               664    PRIVILEGE_EXPIRED
            PeriodExcluded = 100665,                                                //               665    PERIOD_EXCLUDED
            PeriodNotAllowed = 100666,                                              //               666    PERIOD_NOT_ALLOWED
            PeriodUsed = 100667,                                                    //               667    PERIOD_USED
            PlanExpired = 100668,                                                   //               668    PLAN_EXPIRED
            TiaInvalidPin2 = 100669,                                                //               669    TIA_INVALID_PIN2
            InvalidLocation = 100670,                                               //               670    INVALID_LOCATION
            InvalidTime = 100671,                                                   //               671    INVALID_TIME
            NotWithinHoliday = 100672,                                              //               672    NOT_WITHIN_HOLIDAY
            InvalidOnHoliday = 100673,                                              //               673    INVALID_ON_HOLIDAY
            UndefinedError = 100674,                                                //               674    UNDEFINED_ERROR
            DbAccessError = 100800,                                                 //               800    DB_ACCESS_ERROR
            DbRecordConflict = 100801,                                              //               801    DB_RECORD_CONFLICT
            ProcessNotSupported = 101000,                                           //              1000    PROCESS_NOT_SUPPORTED
            InvalidPacketSize = 101001,                                             //              1001    INVALID_PACKET_SIZE
            PosNumberRangeError = 101002,                                           //              1002    POS_NBR_RANGE_ERROR
            DuplicatePos = 101003,                                                  //              1003    DUPLICATE_POS
            InvalidHostDestination = 101004,                                        //              1004    INVALID_HOST_DESTINATION
            BadDataInStoredValuePost = 101005,                                      //              1005    BAD_DATA_SV_POST
            InvalidProfitCenter = 101006,                                           //              1006    INVALID_PROFITCENTER
            ExceedItemCost = 101007,                                                //              1007    EXCEED_ITEM_COST
            DoorNotFound = 101501,                                                  //              1501    DOOR_NOT_FOUND

            /// <summary>
            /// Customer not found.  Seems to be redundant with <see cref="CustomerNotFoundInSystem"/>, which is more widely referenced.
            /// </summary>
            CustomerNotFound = 101502,                                              //              1502    CUSTOMER_NOT_FOUND
            PermissionScheduleNotFound = 101503,                                    //              1503    PERMSCHEDULE_NOT_FOUND
            PermissionDayScheduleNotFound = 101504,                                 //              1504    PERMDAYSCHEDULE_NOT_FOUND
            DoorAccessDenied = 101505,                                              //              1505    DA_ACCESS_DENIED
            ControlDayScheduleNotFound = 101506,                                    //              1506    CTRLDAYSCHEDULE_NOT_FOUND
            ControlScheduleNotFound = 101507,                                       //              1507    CTRLSCHEDULE_NOT_FOUND
            DoorLocked = 101508,                                                    //              1508    DOOR_LOCKED
            DoorUnlocked = 101509,                                                  //              1509    DOOR_UNLOCKED
            DoorInvalidPin = 101510,                                                //              1510    DOOR_INVALIDPIN
            DoorNoPin = 101511,                                                     //              1511    DOOR_NOPIN
            DoorPinRequired = 101512,                                               //              1512    DOOR_PINREQUIRED
            DoorAccessDownloadSizeExceeded = 101513,                                //              1513    DA_DOWNLOADSIZEEXCEEDED
            MasterControllerNotFound = 101514,                                      //              1514    MASTERCONTROLLER_NOT_FOUND
            SystemFeaturesNotFound = 101515,                                        //              1515    SYSTEMFEATURES_NOT_FOUND
            ComputerNotFound = 101516,                                              //              1516    COMPUTER_NOT_FOUND
            UnexpectedMasterControllerReaderType = 101517,                          //              1517    UNEXPECTED_MCREADER_TYPE
            OfflineAccessDenied = 101518,                                           //              1518    OFFLINE_ACCESS_DENIED
            NoOfflineDatabase = 101519,                                             //              1519    NO_OFFLINE_DATABASE
            OfflineCardNotFound = 101520,                                           //              1520    OFFLINE_CARD_NOT_FOUND
            OfflineInvalidPin = 101521,                                             //              1521    OFFLINE_INVALID_PIN
            OfflinePlanOutsideActivation = 101522,                                  //              1522    OFFLINE_PLAN_OUTSIDE_ACTIV
            OfflinePlanOutsideCancellation = 101523,                                //              1523    OFFLINE_PLAN_OUTSIDE_CANCEL
            OfflineInvalidDoor = 101524,                                            //              1524    OFFLINE_INVALID_DOOR
            OfflineOutsideTimeSchedule = 101525,                                    //              1525    OFFLINE_OUTSIDE_TIME_SCHED
            OfflineInvalidPlan = 101526,                                            //              1526    OFFLINE_INVALID_PLAN
            OfflineInvalidPermissionList = 101527,                                  //              1527    OFFLINE_INVALID_PERM_LIST
            OfflineInvalidPermissionSchedule = 101528,                              //              1528    OFFLINE_INVALID_PERMN_SCHED
            OfflineReuseViolation = 101529,                                         //              1529    OFFLINE_REUSE_VIOLATION
            OfflineStorageFull = 101530,                                            //              1530    OFFLINE_STORAGE_FULL
            OfflineUnknownError = 101531,                                           //              1531    OFFLINE_UNKNOWN_ERROR
            DoorAccessOverrideIdNotFound = 101532,                                  //              1532    DA_OVERRIDE_ID_NOTFOUND
            IpConverterNotFound = 101533,                                           //              1533    IPCONVERTER_NOT_FOUND
            PointNumberNotFound = 101534,                                           //              1534    POINTNUM_NOTFOUND
            DoorAccessPointsConfigSizeExceeded = 101535,                            //              1535    DA_POINTSCONFIGSIZEEXCEEDED
            ExcessiveDoorUsage = 101536,                                            //              1536    EXCESSIVE_DOOR_USAGE
            ExcessiveAccessPlanUsage = 101537,                                      //              1537    EXCESSIVE_ACCESSPLAN_USAGE
            OfflineInvalidCustomerRecords = 101538,                                 //              1538    OFFLINE_INVALID_CUST_RECORDS
            OfflineInvalidIssueCode = 101539,                                       //              1539    OFFLINE_INVALID_ISSUE_CODE
            LaundryMachineNotFound = 101601,                                        //              1601    LAUNDRY_MACHINE_NOT_FOUND
            LaundryControllerNotFound = 101602,                                     //              1602    LAUNDRY_CONTROLLER_NOT_FOUND
            InvalidProductCount = 102000,                                           //              2000    INVALID_PRODUCT_COUNT
            ProductQueueFull = 102001,                                              //              2001    PRODUCT_QUEUE_FULL
            DetailTransactionMismatch = 102002,                                     //              2002    DTL_TRANS_MISMATCH
            DetailTransactionInvalidPosition = 102003,                              //              2003    DTL_TRANS_INVALID_POSITION
            DetailTransactionIoError = 102004,                                      //              2004    DTL_TRANS_IO_ERROR
            DetailTransactionOpenError = 102005,                                    //              2005    DTL_TRAN_OPEN_ERROR
            DetailTransactionRenameError = 102006,                                  //              2006    DTL_TRAN_RENAME_ERROR
            UnexpectedDeviceType = 102401,                                          //              2401    UNEXPECTED_DEVICETYPE
            InvalidControllerLoopInfo = 103000,                                     //              3000    INVALID_CTLLR_LOOP_INFO
            ControllerNotFound = 103001,                                            //              3001    CONTROLLER_NOT_FOUND
            ControllerLoopInactive = 103002,                                        //              3002    CTLR_LOOP_INACTIVE
            InvalidCreditCardTrackData = 104000,                                    //              4000    INVALID_CC_TRACK_DATA
            InvalidCreditCardTrackSpecified = 104001,                               //              4001    INVALID_CC_TRACK_SPEC
            InvalidTrackOrCreditCardNumber = 104002,                                //              4002    INVALID_TRACK_OR_CC_NBR
            InvalidCreditCardNumber = 104003,                                       //              4003    INVALID_CC_NBR
            ExpiredCreditCard = 104004,                                             //              4004    EXPIRED_CREDIT_CARD
            HostDbAccessError = 105000,                                             //              5000    HOST_DB_ACCESS_ERROR
            HostDbRecordConflict = 105001,                                          //              5001    HOST_DB_RECORD_CONFLICT
            HostCcUnavailable = 105002,                                             //              5002    HOST_CC_UNAVAILABLE
            HostLowResources = 106000,                                              //              6000    HOST_LOW_RESOURCES
            HostRequestLimitError = 106001,                                         //              6001    HOST_REQUEST_LIMIT_ERROR
            PosRequestQueueFull = 106002,                                           //              6002    POS_REQUEST_QUEUE_FULL
            InvalidPendingIdData = 106003,                                          //              6003    INVALID_PENDING_ID_DATA
            PosRequestStillPending = 106100,                                        //              6100    POS_REQUEST_STILL_PENDING
            CustomerMessageLimitExceeded = 110000,                                  //             10000    CUSTOMER_MSG_LIMIT_EXCEEDED
            PendingBoardTransactionInvalid = 111000,                                //             11000    PENDING_BOARD_TRAN_INVALID
            InvalidBoardTransactionType = 111001,                                   //             11001    INVALID_BOARD_TRAN_TYPE
            ExcessiveBoardUsage = 111002,                                           //             11002    EXCESSIVE_BOARD_USAGE
            BoardExclusion = 111003,                                                //             11003    BOARD_EXCLUSION
            CashierNotActive = 112000,                                              //             12000    CASHIER_NOT_ACTIVE
            
            /// <summary>
            /// Customer PIN is invalid.
            /// </summary>
            InvalidCustomerPin = 112001,                                            //             12001    INVALID_CUST_PIN

            /// <summary>
            /// Customer issue number is invalid.
            /// </summary>
            InvalidIssueNumber = 112002,                                            //             12002    INVALID_ISSUE_NBR
            TenderCreditCardInvalid = 112003,                                       //             12003    TENDER_CC_INVALID
            TenderTypeInvalid = 112004,                                             //             12004    TENDER_TYPE_INVALID
            CustomerStoredValueAccountInvalid = 112005,                             //             12005    CUSTOMER_SV_ACCOUNT_INVALID
            NoMenuPages = 112006,                                                   //             12006    NO_MENU_PAGES

            /// <summary>
            /// The card status is frozen.
            /// </summary>
            CardStatusFrozen = 112007,                                              //             12007    CARD_STATUS_FROZEN

            /// <summary>
            /// The customer is set to inactive.
            /// </summary>
            CustomerNotActive = 112008,                                             //             12008    CUSTOMER_NOT_ACTIVE
            EntityDictionaryNotFound = 112009,                                      //             12009    ENTITY_DICT_NOT_FOUND
            WorkstationNotFound = 112010,                                           //             12010    WORKSTATION_NOT_FOUND

            /// <summary>
            /// The card has been marked lost.
            /// </summary>
            CardMarkedLost = 112011,                                                //             12011    CARD_MARKED_LOST
            DefaultCardnumNotFound = 112012,                                        //             12012    DEFAULT_CARDNUM_NOT_FOUND

            /// <summary>
            /// The card number does not exist.
            /// </summary>
            CardNotFound = 112013,                                                  //             12013    CARD_NOT_FOUND
            CardNotMarkedLost = 112014,                                             //             12014    CARD_NOT_MARKED_LOST
            InvalidCardNumber = 112015,                                             //             12015    INVALID_CARDNUMBER
            InvalidCustNumber = 112016,                                             //             12016    INVALID_CUSTNUMBER
            TiaDuplicateTransactionForPos = 113001,                                 //             13001    TIA_DUPE_TRAN_FOR_POS
            TiaDuplicateTransactionForOtherPos = 113002,                            //             13002    TIA_DUPE_TRAN_FOR_OTH_POS
            TiaDuplicateTransactionForPosDenied = 113003,                           //             13003    TIA_DUPE_TRAN_FOR_POS_DEN

            /// <summary>
            /// Transaction ID is null or otherwise invalid.
            /// </summary>
            InvalidTransactionId = 113004,                                          //             13004    INVALID_TRANSACTION_ID

            /// <summary>
            /// Invalid control transaction type.
            /// </summary>
            InvalidControlTransactionType = 114000,                                 //             14000    INVALID_CTL_TRAN_TYPE
            TransactionTypeInvalid = 114001,                                        //             14001    TRANTYPE_INVALID

            /// <summary>
            /// Invalid transaction validation type.
            /// </summary>
            ValidationTypeInvalid = 114002,                                         //             14002    VALIDATIONTYPE_INVALID
            
            /// <summary>
            /// Invalid transaction attended type.
            /// </summary>
            AttendedTypeInvalid = 114003,                                           //             14003    ATTENDEDTYPE_INVALID

            /// <summary>
            /// Invalid retail transaction type.
            /// </summary>
            RetailTransactionTypeInvalid = 114004,                                  //             14004    RETAILTRANTYPE_INVALID
            ProductDetailNotFound = 114005,                                         //             14005    PRODDTL_NOT_FOUND
            TaxScheduleNotFound = 114006,                                           //             14006    TAXSCHED_NOT_FOUND
            TaxGroupNotFound = 114007,                                              //             14007    TAXGROUP_NOT_FOUND
            UnitMeasureNotFound = 114008,                                           //             14008    UNIT_MEASURE_NOT_FOUND
            InvalidProductEntryType = 114009,                                       //             14009    INVALID_PRODUCT_ENTRY_TYPE
            InvalidRetailPriceEntry = 114010,                                       //             14010    INVALID_RETAIL_PRICE_ENTRY
            InvalidTaxExemptReason = 114011,                                        //             14011    INVALID_TAXEXEMPT_REASON
            InvalidTaxOverrideReason = 114012,                                      //             14012    INVALID_TAXOVERRIDE_REASON
            InvalidPriceModifierType = 114013,                                      //             14013    INVALID_PRICE_MODIFIER_TYPE
            InvalidModifierCalculationMethod = 114014,                              //             14014    INVALID_MOD_CALC_METHOD
            StoredValueAccountNotFound = 114015,                                    //             14015    SV_ACCOUNT_NOT_FOUND
            InvalidMediaKeyType = 114017,                                           //             14017    INVALID_MEDIA_KEY_TYPE
            InvalidPhysicalIdType = 114018,                                         //             14018    INVALID_PHYSICAL_ID_TYPE
            InvalidCustomerEntryType = 114019,                                      //             14019    INVALID_CUSTOMER_ENTRY_TYPE
            DiscountSurchargeReasonType = 114020,                                   //             14020    DISC_SURCH_REASON_TYPE
            InvalidTaxExemptOverrideType = 114021,                                  //             14021    INVALID_TAX_EXMPT_OVR_TYPE

            /// <summary>
            /// Transaction number is null or otherwise invalid.
            /// </summary>
            InvalidTransactionNumber = 114022,                                      //             14022    INVALID_TRANSACTION_NUMBER
            InvalidPos = 114023,                                                    //             14023    INVALID_POS
            InvalidDebitCreditType = 114024,                                        //             14024    INVALID_DEBITCREDIT_TYPE
            NoEnrichmentParent = 114025,                                            //             14025    NO_ENRICHMENT_PARENT
            InvalidCustEnrichment = 114026,                                         //             14026    INVALID_CUST_ENRICHMENT
            DuplicateLineItem = 114027,                                             //             14027    DUPLICATE_LINEITEM
            DuplicateTransaction = 114028,                                          //             14028    DUPLICATE_TRAN
            TransactionNotFound = 114029,                                           //             14029    TRANSACTION_NOT_FOUND
            RetailTransactionNotFound = 114030,                                     //             14030    RETAIL_TRAN_NOT_FOUND
            TransactionLinesMissing = 114031,                                       //             14031    TRAN_LINES_MISSING
            TransactionVoidFailed = 114032,                                         //             14032    TRAN_VOID_FAILED

            /// <summary>
            /// The tender is not allowed for the merchant.
            /// </summary>
            InvalidMerchantTender = 114033,                                         //             14033    INVALID_MERCHANT_TENDER

            /// <summary>
            /// There is no defined tender for this transaction.
            /// </summary>
            NoTransactionTender = 114034,                                           //             14034    NO_TRAN_TENDER
            NoTenderCustomer = 114035,                                              //             14035    NO_TENDER_CUST
            NoTenderCard = 114036,                                                  //             14036    NO_TENDER_CARD
            NoTenderCashEquiv = 114037,                                             //             14037    NO_TENDER_CE

            /// <summary>
            /// The customer has no authorized stored value accounts.
            /// </summary>
            NoCustomerStoredValueAccounts = 114038,                                 //             14038    NO_CUST_SV_ACCTS
            MissingCashierSession = 114039,                                         //             14039    MISSING_CASHIER_SESSION
            DuplicateCashierSignon = 114040,                                        //             14040    DUPLICATE_CASHIER_SIGNON

            /// <summary>
            /// The temporary card was unassigned at the time of the transaction.
            /// </summary>
            TempCardUnassigned = 114041,                                            //             14041    TEMP_CARD_UNASSIGNED
            MerchantDepositDisabled = 114042,                                       //             14042    MERCHANT_DEPOSIT_DISABLED
            InvalidTransactionAmount = 114043,                                      //             14043    INVALID_TRANSACTION_AMOUNT
            OverTransactionLimitForPeriod = 114044,                                 //             14044    OVER_TRANSACTION_LIMIT
            StoredValueAdjustmentFailure = 114045,                                  //             14045    SV_ADJUSTMENT_FAILURE
            OrderIdNotFound = 114046,                                               //             14046    ORDERID_NOT_FOUND

            /// <summary>
            /// No allow or deny status in policy.
            /// </summary>
            PolicyTransactionStatus = 115000,                                       //             15000    POLICY_TRAN_STATUS

            /// <summary>
            /// The system policy explicitly denied the transaction.
            /// </summary>
            PolicyTransactionDenied = 115001,                                       //             15001    POLICY_TRAN_DENIED

            /// <summary>
            /// The system policy could not map tender to SV account type.
            /// </summary>
            InvalidTenderMap = 115002,                                              //             15002    INVALID_TENDER_MAP
            UserNotFound = 116000,                                                  //             16000    USER_NOT_FOUND
            UserDisabled = 116001,                                                  //             16001    USER_DISABLED
            UserInactive = 116002,                                                  //             16002    USER_INACTIVE
            UserExpired = 116003,                                                   //             16003    USER_EXPIRED
            UserEmailAddressNotFound = 116004,                                      //             16004    USER_EMAIL_ADDY_NOT_FOUND
            GroupNotFound = 116005,                                                 //             16005    GROUP_NOT_FOUND
            EventTypeNotFound = 116006,                                             //             16006    EVENT_TYPE_NOT_FOUND
            PasswordChangeRequired = 116007,                                        //             16007    PASSWORD_CHANGE_REQUIRED
            UserInactiveDays = 116008,                                              //             16008    USER_INACTIVE_DAYS
            UserPasswordExpired = 116009,                                           //             16009    USER_PASSWORD_EXPIRED
            PasswordAttemptsExceeded = 116010,                                      //             16010    PASSWORD_ATTEMPTS_EXCEEDED
            ReportTemplateNotFound = 117000,                                        //             17000    REPORT_TEMPLATE_NOT_FOUND
            ReportInstanceNotFound = 117001,                                        //             17001    REPORT_INSTANCE_NOT_FOUND
            ReportStatusNotFound = 117002,                                          //             17002    REPORT_STATUS_NOT_FOUND
            ReportSourceNotFound = 117003,                                          //             17003    REPORT_SOURCE_NOT_FOUND
            ReportClassNotFound = 117004,                                           //             17004    REPORT_CLASS_NOT_FOUND
            ReportScheduleNotFound = 117005,                                        //             17005    REPORT_SCHEDULE_NOT_FOUND
            ReportFormatNotFound = 117006,                                          //             17006    REPORT_FORMAT_NOT_FOUND
            PosReportInvalid = 117007,                                              //             17007    POS_REPORT_INVALID
            PosReportBusinessDayInvalid = 117008,                                   //             17008    POS_REPORT_BIZ_DAY_INVALID
            PosReportInstanceNotFound = 117009,                                     //             17009    POS_REPORT_INSTANCE_NOT_FOUND
            ReportInstanceScheduleNotFound = 117010,                                //             17010    RPT_INSTANCE_SCHED_NOT_FOUND
            ReportDeliveryProfileNotFound = 117011,                                 //             17011    RPT_DELIVERY_PROFILE_NOT_FOUND
            ReportXsltNotFound = 117012,                                            //             17012    REPORT_XSLT_NOT_FOUND
            DeliveryProfileExistInInstanceSchedule = 117013,                        //             17013    DELPROF_EXIST_IN_INSTSCHED
            ScheduleExistInInstanceSchedule = 117014,                               //             17014    SCHED_EXIST_IN_INSTSCHED
            CardholderPresentCodeNotFound = 118000,                                 //             18000    CARDHLD_PRESENT_CODE_NOT_FOUND
            RequestResponesCodeNotFound = 118001,                                   //             18001    REQ_RESP_CODE_NOT_FOUND
            PipelineNotFound = 118002,                                              //             18002    PIPELINE_NOT_FOUND
            CommitFound = 118003,                                                   //             18003    COMMIT_FOUND
            DatasourceNotFound = 118004,                                            //             18004    DATASOURCE_NOT_FOUND
            TransactionStateNotFound = 118005,                                      //             18005    TRANSACTION_STATE_NOT_FOUND
            TransactionTypeNotFound = 118006,                                       //             18006    TRANSACTION_TYPE_NOT_FOUND
            CreditCardReconcileStateNotFound = 118007,                              //             18007    CC_RECONCILE_STATE_NOT_FOUND
            DataEncryptionNotFound = 118008,                                        //             18008    DATA_ENCRYPTION_NOT_FOUND
            CreditCardTypeNotFound = 118009,                                        //             18009    CREDIT_CARD_TYPE_NOT_FOUND
            BbPaygateOrderIdNotFound = 118010,                                      //             18010    BB_PAYGATE_ORDER_ID_NOT_FOUND
            BbPaygateTransactionInUse = 118011,                                     //             18011    BB_PAYGATE_TRANSACTION_IN_USE
            BbPaygateStoreConfigurationInvalid = 118012,                            //             18012    BB_PAYGATE_STORE_CFG_INVALID
            BbPaygateHostConfigurationNotFound = 118013,                            //             18013    BB_PAYGATE_HOST_CFG_NOT_FOUND
            IinLast4NotFound = 118014,                                              //             18014    IIN_LAST4_NOT_FOUND
            BbPaygateTransactionNotFound = 118015,                                  //             18015    BB_PAYGATE_TRAN_NOT_FOUND
            BbPaygateRequestResponseIdNotfound = 118016,                            //             18016    BB_PAYGATE_RR_NOTFOUND
            KekNotFound = 118017,                                                   //             18017    KEK_NOT_FOUND
            BbPaygateErrorNotFound = 118018,                                        //             18018    BB_PAYGATE_ERROR_NOT_FOUND
            ExternalClientNotFound = 119000,                                        //             19000    EXTERNAL_CLIENT_NOT_FOUND
            ExternalClientInactive = 119001,                                        //             19001    EXTERNAL_CLIENT_INACTIVE
            CustomerRegistrationCustomerNumberFailed = 119002,                      //             19002    CUST_REG_CUSTNUM_FAILED
            CustomerRegistrationCardNumberFailed = 119003,                          //             19003    CUST_REG_CARDNUM_FAILED
            CustomerRegistrationEmailFailed = 119004,                               //             19004    CUST_REG_EMAIL_FAILED
            CustomerRegistrationFirstNameFailed = 119005,                           //             19005    CUST_REG_FIRSTNAME_FAILED
            CustomerRegistrationMiddleNameFailed = 119006,                          //             19006    CUST_REG_MIDDLENAME_FAILED
            CustomerRegistrationLastNameFailed = 119007,                            //             19007    CUST_REG_LASTNAME_FAILED
            CustomerRegistrationFailed = 119008,                                    //             19008    CUST_REGISTRATION_FAILED
            ClientCustomerNotFound = 119009,                                        //             19009    CLIENT_CUST_NOT_FOUND
            ClientPrivilegeNotAllowed = 119012,                                     //             19012    CLIENT_PRIVILEGE_NOT_ALLOWED
            XmlParsingFailed = 119013,                                              //             19013    XML_PARSING_FAILED
            StatementStartDateInvalid = 119014,                                     //             19014    STATEMENT_STARTDATE_INVALID
            CustomerAnonymousCustomerNumberFailed = 119015,                         //             19015    CUST_ANON_CUSTNUM_FAILED
            CustomerAnonymousCardNumberFailed = 119016,                             //             19016    CUST_ANON_CARDNUM_FAILED
            CustomerAnonymousEmailFailed = 119017,                                  //             19017    CUST_ANON_EMAIL_FAILED
            CustomerAnonymousFirstNameFailed = 119018,                              //             19018    CUST_ANON_FIRSTNAME_FAILED
            CustomerAnonymousMiddleNameFailed = 119019,                             //             19019    CUST_ANON_MIDDLENAME_FAILED
            CustomerAnonymousLastNameFailed = 119020,                               //             19020    CUST_ANON_LASTNAME_FAILED
            CustomerAnonymousFailed = 119021,                                       //             19021    CUST_ANONYMOUS_FAILED
            InvalidPinValue = 119022,                                               //             19022    INVALID_PIN_VALUE
            OverMaxDeposit = 119023,                                                //             19023    OVER_MAX_DEPOSIT
            UnderMinDeposit = 119024,                                               //             19024    UNDER_MIN_DEPOSIT
            InvalidStoredValueAccountDeposit = 119025,                              //             19025    INVALID_SVACCOUNT_DEPOSIT
            InvalidResourceName = 120001,                                           //             20001    INVALID_RES_NAME
            InvalidPermmissionName = 120002,                                        //             20002    INVALID_PERM_NAME
            RoleNotShareable = 120003,                                              //             20003    ROLE_NOT_SHAREABLE
            InvalidNumbersSet = 120004,                                             //             20004    INVALID_NUMBERS_SET
            InvalidDeleteOperation = 120005,                                        //             20005    INVALID_DELETE_OPERATION
            InvalidInsertOperation = 120006,                                        //             20006    INVALID_INSERT_OPERATION
            InvalidInsertRecordExists = 120007,                                     //             20007    INVALID_INSERT_RECORD_EXISTS
            ExcessivePosUsage = 120008,                                             //             20008    EXCESSIVE_POS_USAGE
            PosTypeMismatch = 120009,                                               //             20009    POS_TYPE_MISMATCH
            TransactionIncorrectSize = 120011,                                      //             20011    TRAN_INCORRECT_SIZE
            AmountRequestedIsInvalid = 120012,                                      //             20012    AMT_REQ_INVALID
            MerchantInvalidReference = 120013,                                      //             20013    MERCH_INVALID_REF
            PaymentExpressRequestResponseInsert = 120014,                           //             20014    PAYEXPRESSREQRES_INS
            TransactionReferenceNotFound = 120015,                                  //             20015    TRANREF_NOT_FOUND
            AmounttAuthorizedIsInvalid = 120016,                                    //             20016    AMT_AUTH_INVALID
            PurchaseInvalidResponseCode = 120017,                                   //             20017    PURCHASE_INV_RESCODE
            DpsRefInvalid = 120018,                                                 //             20018    DPS_REF_INVALID
            PaymentExpressRequestResponseUpdate = 120019,                           //             20019    PAYEXPRESSREQRES_UPD
            PaymentExpressRequestVoidFail = 120020,                                 //             20020    PAYEXPRESSREQVOIDFAIL
            PaymentExpressResponseVoidFail = 120021,                                //             20021    PAYEXPRESSRESVOIDFAIL
            PaymentExpressInvalidDeviceId = 120022,                                 //             20022    PAYEXPINVALIDDEVICEID
            PaymentExpressPaygateTransactionNotFound = 120023,                      //             20023    PE_PAYGATE_TRAN_NOT_FOUND
            InvalidTransactionDate = 120050,                                        //             20050    INVALID_TRANSACTION_DATE
            InvalidForcePost = 120051,                                              //             20051    INVALID_FORCE_POST
            MiscellaneousError = 120999,                                            //             20999    MISC_ERROR
            SimulatedFailure = 132767,                                              //             32767    SIMULATED_FAILURE
            SystemError = 132768,                                                   //             32768    SYSTEM_ERROR

            #endregion 100000 to 199999

#pragma warning restore 1591  //Restores warnings for no XML documentation.

            /// <summary>
            /// Transaction classification unknown or not determinable.
            /// </summary>
            TransactionClassificationUnknown = 200000,

            /// <summary>
            /// Transaction classification is not valid for the attempted operation.
            /// </summary>
            TransactionClassificationInvalid = 200001,

            /// <summary>
            /// Transaction disposition is not valid for the current operation.
            /// </summary>
            DispositionInvalid = 200002,

            /// <summary>
            /// Timestamp history is missing required timestamps.
            /// </summary>
            TimestampHistoryIncomplete = 200003,

            /// <summary>
            /// Timestamp history is invalid.  (Finish must be more recent than Start, etc.)
            /// </summary>
            TimestampHistoryInvalid = 200004,

            /// <summary>
            /// Customer card capture information not present in transaction.
            /// </summary>
            CustomerCardInformationNotPresent = 200005,

            /// <summary>
            /// Customer identifier included in the credential does not match the lookup value.
            /// </summary>
            CustomerCredentialDiscrepancy = 200006,

            /// <summary>
            /// The element could not be processed.
            /// </summary>
            ElementProcessingFailed = 200010,

            /// <summary>
            /// A required element is not present in the transaction.
            /// </summary>
            ElementMissing = 200011,

            /// <summary>
            /// ARTS element is not present in the transaction.
            /// </summary>
            ArtsElementMissing = 200012,

            /// <summary>
            /// Attendance element is not present in the transaction.
            /// </summary>
            AttendanceElementMissing = 200013,

            /// <summary>
            /// Board element is not present in the transaction.
            /// </summary>
            BoardElementMissing = 200014,

            /// <summary>
            /// Control element is not present in the transaction.
            /// </summary>
            ControlElementMissing = 200015,

            /// <summary>
            /// Originator element is not present in the transaction.
            /// </summary>
            OriginatorElementMissing = 200016,

            /// <summary>
            /// Operator element is not present in the transaction.
            /// </summary>
            OperatorElementMissing = 200017,

            /// <summary>
            /// Control total information is missing or invalid in the transaction.
            /// </summary>
            ControlTotalMissingOrInvalid = 200100,

            /// <summary>
            /// An ARTS line item could not be processed.
            /// </summary>
            LineItemProcessingFailed = 200101,

            /// <summary>
            /// Line item type is invalid.
            /// </summary>
            LineItemTypeInvalid = 200102,

            /// <summary>
            /// The requested device could not be found.
            /// </summary>
            DeviceNotFound = 200200,

            /// <summary>
            /// The registration for the requested device has not been completed.
            /// </summary>
            DeviceNotRegistered = 200201,
        }

    }
}
