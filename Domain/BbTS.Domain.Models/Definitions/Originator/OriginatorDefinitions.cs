﻿namespace BbTS.Domain.Models.Definitions.Originator
{
    /// <summary>
    /// Enumeration of the types of originators.
    /// </summary>
    public enum OriginatorType
    {
        /// <summary>
        /// Numerical value = 1. AT3000 style transaction terminal.
        /// </summary>
        AT3000 = 1,
        /// <summary>
        /// Numerical value = 2. MF4100 style transaction terminal.
        /// </summary>
        MF4100 = 2,
        /// <summary>
        /// Numerical value = 3. PR5000/Slate style transaciton terminal.
        /// </summary>
        PR5000 = 3,
        /// <summary>
        /// Numerical value = 4.  Workstation point of sale.
        /// </summary>
        Workstation = 4
    }

    /// <summary>
    /// The type of operation mode the point of service is in.
    /// </summary>
    public enum AttendType
    {
        /// <summary>
        /// No operator present at the point of service.
        /// </summary>
        Unattended = 0,
        /// <summary>
        /// Operator present at the point of service.
        /// </summary>
        Attended = 1
    }
}
