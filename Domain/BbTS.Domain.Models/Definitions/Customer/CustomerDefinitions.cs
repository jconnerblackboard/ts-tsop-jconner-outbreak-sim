﻿
namespace BbTS.Domain.Models.Definitions.Customer
{
    /// <summary>
    ///     Enumeration of Customer address Type
    /// </summary>
    public enum CustomerAddressType
    {
        /// <summary>
        /// The unknown
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// The local
        /// </summary>
        Local = 1,

        /// <summary>
        /// The permanent
        /// </summary>
        Permanent = 2,

        /// <summary>
        /// The emergency1
        /// </summary>
        Emergency1 = 3,

        /// <summary>
        /// The emergency2
        /// </summary>
        Emergency2 = 4
    }

    /// <summary>
    /// Enumeration of customer gender
    /// </summary>
    public enum CustomerGender
    {
        Unspecified = 0,
        Male = 1,
        Female = 2
    }

    /// <summary>
    /// Customer display rule types
    /// </summary>
    public enum CustomerDisplayRuleType
    {
        DisplaySvBalanceAttended = 0,
        DisplaySvBalanceUnattended = 1,
        PrintSvBalanceAttended = 2,
        PrintSvBalanceUnattended = 3,
        DisplayCustomerName = 4,
        DisplayCustomerBirthday = 5,
        PrintCustomerName = 6,
        PrintCardNumber = 7,
        PrintCustomerNumber = 8
    }
}
