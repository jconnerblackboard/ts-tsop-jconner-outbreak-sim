﻿using System.Collections.Generic;
using BbTS.Domain.Models.Definitions.Terminal;
using BbTS.Domain.Models.System;

namespace BbTS.Domain.Models.Definitions.Customer
{
    public enum CustomerActionResultDomainIdCode
    {
        Success = 0,
        CustomerNotFound = 1,
        InvalidCredentials = 2,
        CredentialInUse = 3,
        UnknownFailure = 4,
        CredentialNotThisCustomer = 5
    }

    /// <summary>
    /// Identification requirements for a customer using the device.
    /// </summary>
    public enum CustomerIdentificationRequirement
    {
        /// <summary>
        /// Use a customer number to validate a customer.
        /// </summary>
        CustomerNumber,
        /// <summary>
        /// Use a card number manually or swiped to validate a customer.
        /// </summary>
        CardNumberAndSwipe,
        /// <summary>
        /// Any other form of idenfication (provided in this enumeration) are considered valid.
        /// </summary>
        Any
    }

    public class DomainCustomerDefinitions
    {
        /// <summary>
        /// Dictionary containing a mapping for DomainTerminal result domain to internal ActionResult codes
        /// </summary>
        private static readonly Dictionary<CustomerActionResultDomainIdCode, ActionResultToken> ActionResultCodeMessages =
            new Dictionary<CustomerActionResultDomainIdCode, ActionResultToken>
            {
                {
                    CustomerActionResultDomainIdCode.Success, new ActionResultToken
                    {
                        ResultDomain = (int)DomainValue.DomainClientTerminal,
                        ResultDomainId = (int)TerminalActionResultDomainIdCode.Success,
                        Message = "Success"
                    }
                },
                {
                    CustomerActionResultDomainIdCode.CustomerNotFound, new ActionResultToken
                    {
                        ResultDomain = (int)DomainValue.DomainClientTerminal,
                        ResultDomainId = (int)TerminalActionResultDomainIdCode.BusinessLogicError,
                        Message = "Customer was not found."
                    }
                },
                {
                    CustomerActionResultDomainIdCode.InvalidCredentials, new ActionResultToken
                    {
                        ResultDomain = (int)DomainValue.DomainClientTerminal,
                        ResultDomainId = (int)TerminalActionResultDomainIdCode.BusinessLogicError,
                        Message = "Invalid credential or credential format."
                    }
                },
                {
                    CustomerActionResultDomainIdCode.CredentialInUse, new ActionResultToken
                    {
                        ResultDomain = (int)DomainValue.DomainClientTerminal,
                        ResultDomainId = (int)TerminalActionResultDomainIdCode.BusinessLogicError,
                        Message = "Passed credential already in use. Next available number returned."
                    }
                },
                {
                    CustomerActionResultDomainIdCode.CredentialNotThisCustomer, new ActionResultToken
                    {
                        ResultDomain = (int)DomainValue.DomainClientTerminal,
                        ResultDomainId = (int)TerminalActionResultDomainIdCode.BusinessLogicError,
                        Message = "Credential is not assigned to the requested Customer."
                    }
                },
                {
                    CustomerActionResultDomainIdCode.UnknownFailure, new ActionResultToken
                    {
                        ResultDomain = (int)DomainValue.DomainClientTerminal,
                        ResultDomainId = (int)TerminalActionResultDomainIdCode.BusinessLogicError,
                        Message = "Unknown failure. "
                    }
                }
            };

        /// <summary>
        /// Get the ActionResultToken for the Terminal DomainId code
        /// </summary>
        /// <param name="code">TerminalActionResultDomainIdCode code</param>
        /// <returns></returns>
        public static ActionResultToken GetActionResultCode(CustomerActionResultDomainIdCode code)
        {
            if (!ActionResultCodeMessages.ContainsKey(code)) return null;
            return ActionResultCodeMessages[code];
        }
    }
}
