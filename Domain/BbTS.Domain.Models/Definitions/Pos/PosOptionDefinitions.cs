﻿
namespace BbTS.Domain.Models.Definitions.Pos
{
    /// <summary>
    /// Pos option Meals Left By Type
    /// </summary>
    public enum MealsLeftBy
    {
        Auto = 0,
        Day,
        Week,
        Month,
        Semester,
        Year
    }

    /// <summary>
    /// Discount/Surcharge/Tax type
    /// </summary>
    public enum PaymentType
    {
        None = 0,
        Amount,
        Open,
        Rate
    }

    /// <summary>
    /// Purchase Entry Type
    /// </summary>
    public enum PurchaseEntryType
    {
        Amount = 0,
        Itemize
    }
}
