﻿using System.Collections.Generic;
using BbTS.Domain.Models.System;

namespace BbTS.Domain.Models.Definitions.Terminal
{
    public enum TerminalActionResultDomainIdCode
    {
        Success = 0,
        NoTerminalSettings = 1,
        CommunicationError = 2,
        ServiceControllerStatusError = 3,
        BusinessLogicError = 4
    }

    public enum EmvPinPadType
    {
        Serial,
        Usb
    }

    public enum EmvPinPadErrorState
    {
        None,
        ComPortNotDetected
    }

    public class DomainTerminalDefinitions
    {
        private static DomainTerminalDefinitions _instance = new DomainTerminalDefinitions();

        private DomainTerminalDefinitions()
        {}

        public static DomainTerminalDefinitions Instance
        {
            get { return _instance; }
            internal set { _instance = value; }
        }

        /// <summary>
        /// Dictionary containing a mapping for DomainTerminal result domain to internal ActionResult codes
        /// </summary>
        private readonly Dictionary<TerminalActionResultDomainIdCode, ActionResultToken> _terminalActionResultCodeMessages =
            new Dictionary<TerminalActionResultDomainIdCode, ActionResultToken>
            {
                {
                    TerminalActionResultDomainIdCode.Success, new ActionResultToken
                    {
                        ResultDomain = (int)DomainValue.DomainClientTerminal,
                        ResultDomainId = (int)TerminalActionResultDomainIdCode.Success,
                        Message = "Success"
                    }
                },
                {
                    TerminalActionResultDomainIdCode.NoTerminalSettings, new ActionResultToken
                    {
                        ResultDomain = (int)DomainValue.DomainClientTerminal,
                        ResultDomainId = (int)TerminalActionResultDomainIdCode.NoTerminalSettings,
                        Message = "No credit card processing settings were found for the requested terminal."
                    }
                },
                {
                    TerminalActionResultDomainIdCode.CommunicationError, new ActionResultToken
                    {
                        ResultDomain = (int)DomainValue.DomainClientTerminal,
                        ResultDomainId = (int)TerminalActionResultDomainIdCode.CommunicationError,
                        Message = "Communication error with Transact API."
                    }
                },
                {
                    TerminalActionResultDomainIdCode.ServiceControllerStatusError, new ActionResultToken
                    {
                        ResultDomain = (int)DomainValue.DomainClientTerminal,
                        ResultDomainId = (int)TerminalActionResultDomainIdCode.ServiceControllerStatusError,
                        Message = "Unable to adjust the service status of the requested service. "
                    }
                }
            };

        /// <summary>
        /// Get the ActionResultToken for the Terminal DomainId code
        /// </summary>
        /// <param name="code">TerminalActionResultDomainIdCode code</param>
        /// <returns></returns>
        public ActionResultToken GetActionResultCode(TerminalActionResultDomainIdCode code)
        {
            if (!_terminalActionResultCodeMessages.ContainsKey(code)) return null;
            return _terminalActionResultCodeMessages[code];
        }

    }

}
