﻿using System.Collections.Generic;
using BbTS.Domain.Models.System;

namespace BbTS.Domain.Models.Definitions.Terminal
{
    public enum ClientTerminalHydraDomainId
    {
        Unknown = 0,
        Success = 1,
        SetConfigurationNotCalled = 2
    }
    
    public static class ClientTerminalHydraDictionary
    {
        private const int DomainClientTerminalHydra = (int)DomainValue.DomainClientTerminalHydra;

        private static readonly Dictionary<ClientTerminalHydraDomainId, string> ClientTerminalHydraCodes = new Dictionary<ClientTerminalHydraDomainId, string>
        {
            { ClientTerminalHydraDomainId.Unknown, "Unknown Error" },
            { ClientTerminalHydraDomainId.Success, "Success" },
            { ClientTerminalHydraDomainId.SetConfigurationNotCalled, "SetConfiguration has not been called.  Call SetConfiguration once before any other calls." }
        };

        public static ActionResultToken GetActionResultToken(ClientTerminalHydraDomainId domainId, string additionalInformation)
        {
            string message;
            if (!ClientTerminalHydraCodes.TryGetValue(domainId, out message))
            {
                message = ClientTerminalHydraCodes[ClientTerminalHydraDomainId.Unknown] + ":  " + domainId;
            }

            return new ActionResultToken
            {
                ResultDomain = DomainClientTerminalHydra,
                ResultDomainId = (int)domainId,
                Message = string.IsNullOrEmpty(additionalInformation) ? message : message + " (" + additionalInformation + ")"
            };
        }

        public static ActionResultToken GetActionResultToken(ClientTerminalHydraDomainId domainId)
        {
            return GetActionResultToken(domainId, null);
        }
    }
}