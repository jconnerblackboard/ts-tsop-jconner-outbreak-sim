﻿using System.Collections.Generic;
using BbTS.Domain.Models.System;

namespace BbTS.Domain.Models.Definitions.Terminal
{
    public enum TerminalLogicDomainId
    {
        Unknown = 0,
        Success = 1,
        OperationNotAllowedOffline = 2,
        TimeoutWaitingForResponse = 3,
        SaveFailed = 4
    }
    
    public static class DomainTerminalLogicDefinitions
    {
        private static readonly Dictionary<TerminalLogicDomainId, string> TerminalLogicCodes = new Dictionary<TerminalLogicDomainId, string>
        {
            { TerminalLogicDomainId.Unknown, "Unknown Error" },
            { TerminalLogicDomainId.Success, "Success" },
            { TerminalLogicDomainId.OperationNotAllowedOffline, "Operation not allowed offline" },
            { TerminalLogicDomainId.TimeoutWaitingForResponse, "Timeout waiting for response" },
            { TerminalLogicDomainId.SaveFailed, "Save operation failed" }
        };

        public static ActionResultToken GetActionResultToken(TerminalLogicDomainId domainId, string additionalInformation)
        {
            string message;
            if (!TerminalLogicCodes.TryGetValue(domainId, out message))
            {
                message = TerminalLogicCodes[TerminalLogicDomainId.Unknown] + ":  " + domainId;
            }

            return new ActionResultToken
            {
                ResultDomain = (int)DomainValue.DomainTerminalLogic,
                ResultDomainId = (int)domainId,
                Message = string.IsNullOrEmpty(additionalInformation) ? message : message + " (" + additionalInformation + ")"
            };
        }

        public static ActionResultToken GetActionResultToken(TerminalLogicDomainId domainId)
        {
            return GetActionResultToken(domainId, null);
        }
    }
}
