﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Definitions.Web.Api
{
    /// <summary>
    /// Set of definitions for the Web Api Service and plugin module.
    /// </summary>
    public static class WebApiDefinitions
    {
        /// <summary>
        /// The unique identifier for the web api plugin
        /// </summary>
        public const string WebApiPluginIdentifier = "AFFC7122-AC38-468C-A11A-5BE7F802D13C";

    }

    /// <summary>
    /// Enumeration for the response echo level requested by the client in an API request call.
    /// </summary>
    public enum ResponseEchoLevel
    {
        /// <summary>
        /// The following element are removed from the echoed request element in the response
        ///  Payer | Account | Identification | Values
        ///  Payer | Instrument | Identification | Values
        ///  Payer | Instrument | Identification | Verification
        /// </summary>
        Normal,
        /// <summary>
        /// The following element are removed from the echoed request element in the response
        ///  Payer | Instrument | Identification | Verification
        /// </summary>
        Verbose
    }

    /// <summary>
    /// Enumeration definining the types of credential capture methods.
    /// </summary>
    public enum ServiceCredentialType
    {
        /// <summary>
        /// Credential was manually keyed.
        /// </summary>
        ManualEntry,
        /// <summary>
        /// Credential was captured with a card swipe (or card insert).
        /// </summary>
        Swipe
    }
}
