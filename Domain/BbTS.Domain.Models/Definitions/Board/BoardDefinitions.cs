﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Definitions.Board
{
    /// <summary>
    /// Enumeration of schedule types for board plan reset.
    /// </summary>
    public enum ResetFrequency
    {
        /// <summary>
        /// Undefined
        /// </summary>
        Undefined = 0,
        /// <summary>
        /// Daily
        /// </summary>
        Day = 1,
        /// <summary>
        /// Weekly
        /// </summary>
        Week = 2,
        /// <summary>
        /// Monthly
        /// </summary>
        Month = 3,
        /// <summary>
        /// Yearly
        /// </summary>
        Year = 4,
        /// <summary>
        /// Semestral
        /// </summary>
        Semester = 5
    }
}
