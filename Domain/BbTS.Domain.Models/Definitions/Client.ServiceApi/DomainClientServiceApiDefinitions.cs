﻿using System.Collections.Generic;
using BbTS.Domain.Models.System;

namespace BbTS.Domain.Models.Definitions.Client.ServiceApi
{
    public enum ClientServiceApiDomainId
    {
        Unknown = 0,
        Success = 1,
        CommunicationError = 2,
        GeneralException = 3
    }

    public static class DomainClientServiceApiDefinitions
    {
        private static readonly Dictionary<ClientServiceApiDomainId, string> ClientServiceApiCodes = new Dictionary<ClientServiceApiDomainId, string>
        {
            { ClientServiceApiDomainId.Unknown, "Unknown Error" },
            { ClientServiceApiDomainId.Success, "Success" },
            { ClientServiceApiDomainId.CommunicationError, "Communication Error" },
            { ClientServiceApiDomainId.GeneralException, "General Exception"}
        };

        public static ActionResultToken GetActionResultToken(ClientServiceApiDomainId domainId, string additionalInformation)
        {
            string message;
            if (!ClientServiceApiCodes.TryGetValue(domainId, out message))
            {
                message = $"{ClientServiceApiCodes[ClientServiceApiDomainId.Unknown]}:  {domainId}";
            }

            return new ActionResultToken
            {
                ResultDomain = (int)DomainValue.DomainClientServiceApi,  // DomainClientServiceApi = 250
                ResultDomainId = (int)domainId,
                Message = string.IsNullOrEmpty(additionalInformation) ? message : $"{message} ({additionalInformation})"
            };
        }

        public static ActionResultToken GetActionResultToken(ClientServiceApiDomainId domainId)
        {
            return GetActionResultToken(domainId, null);
        }
    }
}
