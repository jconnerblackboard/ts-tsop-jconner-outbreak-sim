﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Definitions.Licensing
{
    /// <summary>
    /// Licensing server device types.
    /// </summary>
    public enum LicenseServerDeviceType
    {
        /// <summary>
        /// Unknown device type (also the default option if nothing is specified)
        /// </summary>
        Unknown,
        /// <summary>
        /// Door lock.
        /// </summary>
        Lock,
        /// <summary>
        /// Door communication.
        /// </summary>
        DoorComm,
        /// <summary>
        /// Point of sale.
        /// </summary>
        Pos
    }

    /// <summary>
    /// Licensing server device subtypes.
    /// </summary>
    public enum LicenseServerDeviceSubType
    {
        /// <summary>
        /// Unknown device subtype (also the default option if nothing is specified)
        /// </summary>
        Unknown,
        /// <summary>
        /// Allegion AD300
        /// </summary>
        AD300,
        /// <summary>
        /// Allegion AD400
        /// </summary>
        AD400,
        /// <summary>
        /// Allegion NDE
        /// </summary>
        NDE,
        /// <summary>
        /// Allegion LE
        /// </summary>
        LE,
        /// <summary>
        /// Allegion PIM
        /// </summary>
        PIM,
        /// <summary>
        /// Allegion Gateway
        /// </summary>
        Gateway,
        /// <summary>
        /// Assa Abloy Wired
        /// </summary>
        AssaAbloyWired,
        /// <summary>
        /// Assa Abloy Wireless
        /// </summary>
        AssaAbloyWireless,
        /// <summary>
        /// Bb Master Controller SA3032
        /// </summary>
        SA3032,
        /// <summary>
        /// Bb Master Controller SA3004
        /// </summary>
        SA3004,
        /// <summary>
        /// Bb MF4100 Laundry
        /// </summary>
        MF4100L,
        /// <summary>
        /// Bb MF4100 Copier
        /// </summary>
        MF4100C,
        /// <summary>
        /// Bb MF4100 POS
        /// </summary>
        MF4100POS,
        /// <summary>
        /// Bb VR4100
        /// </summary>
        VR4100
    }

    /// <summary>
    /// Definition class for handling license definitions
    /// </summary>
    public static class LicenseDefinitions
    {
        /// <summary>
        /// Get the device type from its subtype.
        /// </summary>
        /// <param name="subtype">The subtype of the device</param>
        /// <returns></returns>
        public static LicenseServerDeviceType DeviceTypeFromSubType(LicenseServerDeviceSubType subtype)
        {
            switch (subtype)
            {
                case LicenseServerDeviceSubType.AD300:
                case LicenseServerDeviceSubType.AD400:
                case LicenseServerDeviceSubType.NDE:
                case LicenseServerDeviceSubType.LE:
                case LicenseServerDeviceSubType.AssaAbloyWired:
                case LicenseServerDeviceSubType.AssaAbloyWireless:
                    return LicenseServerDeviceType.Lock;

                case LicenseServerDeviceSubType.PIM:
                case LicenseServerDeviceSubType.Gateway:
                case LicenseServerDeviceSubType.SA3004:
                case LicenseServerDeviceSubType.SA3032:
                    return LicenseServerDeviceType.DoorComm;

                case LicenseServerDeviceSubType.MF4100C:
                case LicenseServerDeviceSubType.MF4100L:
                case LicenseServerDeviceSubType.MF4100POS:
                case LicenseServerDeviceSubType.VR4100:
                    return LicenseServerDeviceType.Pos;

                default:
                    return LicenseServerDeviceType.Unknown;
            }
        }

        /// <summary>
        /// Get the device type from its subtype.
        /// </summary>
        /// <param name="subtypeString">The subtype of the device (as a string)</param>
        /// <returns></returns>
        public static LicenseServerDeviceType DeviceTypeFromSubType(string subtypeString)
        {
            var subtype = (LicenseServerDeviceSubType)Enum.Parse(typeof (LicenseServerDeviceSubType), subtypeString);
            return DeviceTypeFromSubType(subtype);
        }
    }
}
