﻿namespace BbTS.Domain.Models.Definitions.Monitoring
{
    /// <summary>
    /// Enumeration of subscription actions for the host monitor service.
    /// </summary>
    public enum SubscriptionAction
    {
        /// <summary>
        /// No subscription related action to be taken.
        /// </summary>
        None = 0,
        /// <summary>
        /// Register client to receive messages.
        /// </summary>
        Register = 1,
        /// <summary>
        /// Remove registration.  Client will no longer receive messages.
        /// </summary>
        Unregister = 2
    }

    /// <summary>
    /// Enumeration of the different types of subscription services available for client registration.
    /// </summary>
    public enum SubscriptionServiceType
    {
        /// <summary>
        /// No subscription service specified.  Used ilo "null".
        /// </summary>
        None = 0,
        /// <summary>
        /// The host monitor subscription service.
        /// </summary>
        HostMonitor = 1,
        /// <summary>
        /// The security monitor subscription service.
        /// </summary>
        SecurityMonitor = 2
    }

    /// <summary>
    /// Enumeration of host monitor subscription service functions.
    /// </summary>
    public enum HostMonitorSubscriptionServiceFunctions
    {
        Unspecified,
        TransactionPost,
        DeviceSettingsGet,
        DeviceRegistrationPost,
        DeviceRegistrationPatch,
        DeviceLogin,
        DeviceLogout,
        DeviceOperatorsGet,
        DevicePropertiesGet,
        BoardInformationGet,
        CustomerBoardBalanceGet,
        CustomerStoredValueBalanceGet,
        CustomerCredentialVerifyRequest,
        CustomerCredentialVerifyResponse,
        CustomerInfoGet,
        DeviceHeartbeat,
        EventLogGet,
        EventLogGetSummary,
        EventLogSet,
        OperatorSessionBegin,
        OperatorSessionEnd,
        OperatorPinChange,
        OperatorValidate,
        DrawerAuditGet,
        PosAuditGet,
        ProfitCenterAuditGet,
        ServiceHealthcheck
    }
}
