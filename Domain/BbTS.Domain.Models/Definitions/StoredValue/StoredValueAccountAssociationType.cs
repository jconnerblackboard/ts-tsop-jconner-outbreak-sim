﻿
namespace BbTS.Domain.Models.Definitions.StoredValue
{
    /// <summary>
    ///     Enumeration of Stored Value Account Association Type
    /// </summary>
    public enum StoredValueAccountAssociationType
    {
        /// <summary>
        /// The unknown
        /// </summary>
        Unknown = -1,

        /// <summary>
        /// The individual
        /// </summary>
        Individual = 0,

        /// <summary>
        /// The joint
        /// </summary>
        Joint = 1
    }
}
