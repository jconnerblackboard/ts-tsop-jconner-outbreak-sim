﻿namespace BbTS.Domain.Models.Definitions.ArtsDataModel
{
    public enum TaxExemptReason
    {
        StoredValueAccountType = 0,
        TaxGroupQuantity = 1,
        TransactionSubtotal = 2
    }
}