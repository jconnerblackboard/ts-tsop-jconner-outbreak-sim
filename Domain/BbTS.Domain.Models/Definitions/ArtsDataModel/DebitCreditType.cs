﻿namespace BbTS.Domain.Models.Definitions.ArtsDataModel
{
    public enum DebitCreditType
    {
        /// <summary>
        /// A debit is a void deposit
        /// </summary>
        Debit,

        /// <summary>
        /// A credit is a deposit
        /// </summary>
        Credit
    }
}