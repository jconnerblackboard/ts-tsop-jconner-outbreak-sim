﻿namespace BbTS.Domain.Models.Definitions.ArtsDataModel
{
    /// <summary>
    /// Product entry method types.
    /// </summary>
    public enum ProductEntryMethodType
    {
        /// <summary>
        /// 0 - MenuItem
        /// </summary>
        MenuItem = 0,
        /// <summary>
        /// 1 - Scanned (barcode, etc)
        /// </summary>
        Scanned = 1,
        /// <summary>
        /// 2 - Keyed (Manual Product Barcode)
        /// </summary>
        Keyed = 2
    }
}