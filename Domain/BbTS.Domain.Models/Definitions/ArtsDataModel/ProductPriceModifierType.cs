﻿namespace BbTS.Domain.Models.Definitions.ArtsDataModel
{
    public enum ProductPriceModifierType
    {
        DiscountKey = 0,
        SurchargeKey = 1,
        Promotion = 2,
        PriceOverride = 3,
        VariablePrice = 4,
        TenderDiscount = 5,
        TenderSurcharge = 6,
        QuantityDiscount = 7
    }
}