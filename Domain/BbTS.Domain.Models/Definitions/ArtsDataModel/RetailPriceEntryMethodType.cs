﻿namespace BbTS.Domain.Models.Definitions.ArtsDataModel
{
    public enum RetailPriceEntryMethodType
    {
        Lookup = 0,
        ManuallyEntered = 1
    }
}