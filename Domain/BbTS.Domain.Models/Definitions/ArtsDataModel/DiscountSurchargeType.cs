﻿namespace BbTS.Domain.Models.Definitions.ArtsDataModel
{
    public enum DiscountSurchargeType
    {
        Discount = 0,
        Surcharge = 1
    }
}