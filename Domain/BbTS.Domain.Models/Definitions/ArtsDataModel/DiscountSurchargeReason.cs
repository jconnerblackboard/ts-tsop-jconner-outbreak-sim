﻿namespace BbTS.Domain.Models.Definitions.ArtsDataModel
{
    public enum DiscountSurchargeReason
    {
        ProfitCenterPeriodDiscountSurcharge,
        PolicyDiscountSurcharge
    }
}