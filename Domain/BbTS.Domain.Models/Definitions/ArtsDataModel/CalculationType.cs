﻿namespace BbTS.Domain.Models.Definitions.ArtsDataModel
{
    public enum CalculationType
    {
        None = 0,
        Amount = 1,
        Open = 2,
        Percent = 3
    }
}