﻿namespace BbTS.Domain.Models.Definitions.ArtsDataModel
{
    public enum TaxExemptOverrideType
    {
        Normal = 0,
        Exempt = 1,
        Override = 2
    }
}