﻿namespace BbTS.Domain.Models.Definitions.ArtsDataModel
{
    public enum ProductPriceModifierCalculationMethod
    {
        Percentage = 0,
        Amount = 1,
        ManuallyEnteredPrice = 2,
        PromotionalPrice = 3
    }
}