﻿namespace BbTS.Domain.Models.Definitions.ArtsDataModel
{
    /// <summary>
    /// Unit measurement types.
    /// </summary>
    public enum UnitMeasureType
    {
        /// <summary>
        /// 0 - Count
        /// </summary>
        Count = 0,
        /// <summary>
        /// 1 - Weight (Pounds)
        /// </summary>
        Weight = 1
    }
}
