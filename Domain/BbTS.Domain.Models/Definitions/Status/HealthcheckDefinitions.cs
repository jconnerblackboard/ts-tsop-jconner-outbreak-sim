﻿using System.Collections.Generic;
using BbTS.Domain.Models.Exceptions.Status;
using BbTS.Domain.Models.Status;

namespace BbTS.Domain.Models.Definitions.Status
{
    /// <summary>
    /// Enumeration of various healthcheck items.
    /// </summary>
    public enum HealthcheckItemCategories
    {
        Database,
        BbSPService,
        Certificate,
        Service,
        OAuth,
        Registration,
        Loopback,
        BbSPSyncApplicationCredentials,
        BbSPSyncInstitutionRoutes,
        BbSPSyncMerchants
    }

    /// <summary>
    /// Health check results enumeration.
    /// </summary>
    public enum HealthcheckItemResult
    {
        /// <summary>
        /// Healthcheck was a success.
        /// </summary>
        Pass,
        /// <summary>
        /// Healthcheck failed.
        /// </summary>
        Fail,
        /// <summary>
        /// Healthcheck passed with warnings.
        /// </summary>
        Warn
    }

    /// <summary>
    /// Class containing definitions for healthcheck items.
    /// </summary>
    public static class HealthcheckDefinitions
    {
        private static readonly Dictionary<HealthcheckItemCategories, HealthcheckItem> HealthcheckItems = new Dictionary
            <HealthcheckItemCategories, HealthcheckItem>
        {
            {HealthcheckItemCategories.Database, new HealthcheckItem {Name = "Database Check"}},
            {HealthcheckItemCategories.BbSPService, new HealthcheckItem {Name = "BbSP Service Check"}},
            {HealthcheckItemCategories.Certificate, new HealthcheckItem {Name = "Certificate Check"}},
            {HealthcheckItemCategories.Service, new HealthcheckItem {Name = "Service Check"}},
            {HealthcheckItemCategories.OAuth, new HealthcheckItem {Name = "OAuth Check"}},
            {HealthcheckItemCategories.Registration, new HealthcheckItem {Name = "Registration Check"}},
            {HealthcheckItemCategories.Loopback, new HealthcheckItem {Name = "Loopback OAuth Check"}},
            {HealthcheckItemCategories.BbSPSyncApplicationCredentials, new HealthcheckItem {Name = "App Credentials Check"}},
            {HealthcheckItemCategories.BbSPSyncInstitutionRoutes, new HealthcheckItem {Name = "Institution Routes Check"}},
            {HealthcheckItemCategories.BbSPSyncMerchants, new HealthcheckItem {Name = "Merchants Check"}}
        };

        /// <summary>
        /// Create a healthcheck item from among the registered healthcheck items.
        /// </summary>
        /// <param name="category">Category of healtcheck item to create from among <see cref="HealthcheckItemCategories"/>.</param>
        /// <param name="result">Result of the healthcheck test to register to the item.</param>
        /// <param name="message">Message to attach to the healthcheck item upon creation.</param>
        /// <param name="eventCategory">Event Category the message was logged under.</param>
        /// <param name="eventId">Event Id the message was logged under.</param>
        /// <returns></returns>
        public static HealthcheckItem CreateItem(
            HealthcheckItemCategories category,
            HealthcheckItemResult result,
            string message,
            short eventCategory,
            int eventId)
        {
            var item = HealthcheckItems.ContainsKey(category) ? HealthcheckItems[category] : null;

            if (item == null)
            {
                var exceptionMessage = $"Failed to create a healthcheck item for {category} because it doesn't exist in the registered set of HealthcheckItems.";
                throw new HealthcheckCreationException(exceptionMessage);
            }

            item.Result = result.ToString();
            item.Message = message;
            item.EventCategory = eventCategory;
            item.EventId = eventId;
            return item;
        }
    }
}
