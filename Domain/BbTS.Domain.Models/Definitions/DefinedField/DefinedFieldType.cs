﻿
namespace BbTS.Domain.Models.Definitions.DefinedField
{
    /// <summary>
    ///     The defined field type
    /// </summary>
    public enum DefinedFieldType
    {
        /// <summary>
        /// The none
        /// </summary>
        None = -1,

        /// <summary>
        /// The string
        /// </summary>
        String = 0,

        /// <summary>
        /// The numeric
        /// </summary>
        Numeric = 1,

        /// <summary>
        /// The dollar
        /// </summary>
        Dollar = 2,

        /// <summary>
        /// The text
        /// </summary>
        Text = 3,

        /// <summary>
        /// The group
        /// </summary>
        Group = 4,

        /// <summary>
        /// The boolean
        /// </summary>
        Boolean = 5
    }
}
