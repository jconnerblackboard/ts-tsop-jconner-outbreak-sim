﻿
namespace BbTS.Domain.Models.Definitions.Report
{
    public enum ObjectTypes
    {
        Undefined = 0,
        Door = 1,
        Pos = 2
    }
}
