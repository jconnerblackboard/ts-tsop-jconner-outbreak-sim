﻿using System.Collections.Generic;
using System.Net.Http;
using BbTS.Domain.Models.Exceptions.Definitions;

namespace BbTS.Domain.Models.Definitions.Service
{
    /// <summary>
    /// Enumeration of supported ContentType(s)
    /// </summary>
    public enum RequestHeaderSupportedContentType
    {
        Json,
        Xml,
        FormEncoded
    }

    /// <summary>
    /// Enumeration of supported http method types.
    /// </summary>
    public enum SupportedHttpRequestMethod
    {
        // ReSharper disable once InconsistentNaming
        GET,
        // ReSharper disable once InconsistentNaming
        PUT,
        // ReSharper disable once InconsistentNaming
        POST,
        // ReSharper disable once InconsistentNaming
        PATCH,
        // ReSharper disable once InconsistentNaming
        DELETE
    }


    /// <summary>
    /// simple serive status
    /// </summary>
    public enum ServiceStatus
    {
        Uninitialized,
        Ok,
        Starting,
        Started,
        Stopping,
        Stopped,
        Error
    }

    /// <summary>
    /// External Client Interface Response Codes.
    /// </summary>
    public enum ExternalClientInterfaceResponseCode
    {
        /// <summary>
        /// Success = 0
        /// </summary>
        Success = 0,

        /// <summary>
        /// InvalidInterfaceProtocolVersion = 1
        /// </summary>
        InvalidInterfaceProtocolVersion = 1,

        /// <summary>
        /// InvalidTsPosLogin = 2
        /// </summary>
        InvalidTsPosLogin = 2,

        /// <summary>
        /// InvalidTsClientLogin = 3
        /// </summary>
        InvalidTsClientLogin = 3,

        /// <summary>
        /// ActionNotAllowed = 4
        /// </summary>
        ActionNotAllowed = 4,

        /// <summary>
        /// TsClientIdNotFound = 5
        /// </summary>
        TsClientIdNotFound = 5,

        /// <summary>
        /// TsClientIdNotActive = 6
        /// </summary>
        TsClientIdNotActive = 6,

        /// <summary>
        /// TsTerminalNumberNotFound = 7
        /// </summary>
        TsTerminalNumberNotFound = 7
    }

    /// <summary>
    /// Class that provides definitions related to External Client functions.
    /// </summary>
    public static class ExternalClientDefinitions
    {
        /// <summary>
        /// Supported External Client Protocol Versions.
        /// </summary>
        public static readonly string[] ExternalClientSupportedProtocolVersions = { "1.0.0.0", "1.1.0.0", "1.2.0.0" };
    }


    /// <summary>
    /// Class that proivides definitions related to web service related functions.
    /// </summary>
    public static class WebServiceDefinitions
    {
        private static readonly Dictionary<RequestHeaderSupportedContentType, string> SupportedContentTypeDefinitions = new Dictionary
            <RequestHeaderSupportedContentType, string>
        {
            {RequestHeaderSupportedContentType.Json, "application/json"},
            {RequestHeaderSupportedContentType.Xml, "application/xml"},
            {RequestHeaderSupportedContentType.FormEncoded, "application/x-www-form-urlencoded" }
        };

        /// <summary>
        /// Get the content type string from the enumeration type.
        /// </summary>
        /// <param name="type"><see cref="RequestHeaderSupportedContentType"/> enumeration type.</param>
        /// <returns></returns>
        public static string ContentTypeAsString(RequestHeaderSupportedContentType type)
        {
            if (!SupportedContentTypeDefinitions.ContainsKey(type))
            {
                throw new UndefinedDefinitionException(
                    "The type was not found amoung the supported types",
                    "BbTS.Domain.Models.Definitions.Service.WebServiceDefinitions",
                    type.ToString());
            }
            return SupportedContentTypeDefinitions[type];
        }

        /// <summary>
        /// Test whether or not the supported http method has a body content component.
        /// </summary>
        /// <param name="method"><see cref="SupportedHttpRequestMethod"/> type </param>
        /// <returns>Whether or not the type has body content.</returns>
        public static bool HasBodyContent(SupportedHttpRequestMethod method)
        {
            return
                method == SupportedHttpRequestMethod.PATCH ||
                method == SupportedHttpRequestMethod.POST ||
                method == SupportedHttpRequestMethod.PUT;
        }

        /// <summary>
        /// Get the corresponding <see cref="HttpMethod"/> for the given <see cref="SupportedHttpRequestMethod"/>
        /// </summary>
        /// <param name="method"><see cref="SupportedHttpRequestMethod"/> method to translate.</param>
        /// <returns><see cref="HttpMethod"/> corresponding to the given <see cref="SupportedHttpRequestMethod"/></returns>
        public static HttpMethod GetHttpMethod(SupportedHttpRequestMethod method)
        {
            switch (method)
            {
                case SupportedHttpRequestMethod.PUT: return HttpMethod.Put;
                case SupportedHttpRequestMethod.DELETE: return HttpMethod.Delete;
                case SupportedHttpRequestMethod.GET: return HttpMethod.Get;
                case SupportedHttpRequestMethod.POST: return HttpMethod.Post;
                case SupportedHttpRequestMethod.PATCH: return new HttpMethod("PATCH");
            }

            throw new UndefinedDefinitionException("Unsupported type.", "GetHttpMethod", method.ToString());
        }
    }
}
