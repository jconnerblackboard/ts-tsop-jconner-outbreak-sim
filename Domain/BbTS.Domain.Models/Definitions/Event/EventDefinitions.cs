﻿
namespace BbTS.Domain.Models.Definitions.Event
{
    /// <summary>
    /// Event Limit Type
    /// </summary>
    public enum EventLimitType
    {
        /// <summary>
        /// The none
        /// </summary>
        None = 0,

        /// <summary>
        /// The day
        /// </summary>
        Day = 1,

        /// <summary>
        /// The week
        /// </summary>
        Week = 2,

        /// <summary>
        /// The month
        /// </summary>
        Month = 3,

        /// <summary>
        /// The semester
        /// </summary>
        Semester = 4,

        /// <summary>
        /// The year
        /// </summary>
        Year = 5
    }

    /// <summary>
    /// Event group type
    /// </summary>
    public enum EventGroupType
    {
        /// <summary>
        /// Static
        /// </summary>
        Static = 0,

        /// <summary>
        /// Dynamic
        /// </summary>
        Dynamic = 1
    }
}
