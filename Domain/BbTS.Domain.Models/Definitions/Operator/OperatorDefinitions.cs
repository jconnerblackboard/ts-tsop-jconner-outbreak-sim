﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Definitions.Operator
{
    /// <summary>
    /// Login requirements for an operator of the device.
    /// </summary>
    public enum OperatorLoginRequirements
    {
        /// <summary>
        /// Operator can login by selecting their name from a list or provide their operator number and a PIN.
        /// </summary>
        PickListAndOperatorNumber,
        /// <summary>
        /// Operator can provide their card number manually or via a swipe.
        /// </summary>
        OperatorCardSwipeOrManualEntry,
        /// <summary>
        /// Any other form of idenfication (provided in this enumeration) are considered valid.
        /// </summary>
        Any
    }

    /// <summary>
    /// Enumeration of device operator rights
    /// </summary>
    public enum OperatorRight
    {
        Undefined = 0,
        BoardCashEquiv_Return = 1,
        BoardCashEquiv_Sale = 2,
        Board_Apply = 3,
        Board_Reverse = 4,
        CardUtility_Encode = 5,
        CardUtility_View = 6,
        Cash_Return = 7,
        Cash_Sale = 8,
        Check_Return = 9,
        Check_Sale = 10,
        CreditCard_Return = 11,
        CreditCard_Sale = 12,
        PosPriv = 13,
        PosPriv_AgeVerify = 14,
        PosPriv_AutoBoard = 15,
        PosPriv_AutoEvent = 16,
        PosPriv_AutonomousBoard = 17,
        PosPriv_AutonomousEvent = 18,
        PosPriv_BarcodeNumber = 19,
        PosPriv_Board = 20,
        PosPriv_BoardBalanceCheck = 21,
        PosPriv_BoardCashEquiv = 22,
        PosPriv_BoardCount = 23,
        PosPriv_CancelTransaction = 24,
        PosPriv_CardUtility = 25,
        PosPriv_Cash = 26,
        PosPriv_Check = 27,
        PosPriv_CheckAttendance = 28,
        PosPriv_CreditCard = 29,
        PosPriv_DepartmentCard = 30,
        PosPriv_Discount1 = 31,
        PosPriv_Discount2 = 32,
        PosPriv_Discount3 = 33,
        PosPriv_Discount4 = 34,
        PosPriv_EventAccess = 35,
        PosPriv_EventReverse = 36,
        PosPriv_EventStatus = 37,
        PosPriv_EventSwipeOut = 38,
        PosPriv_FreezeCard = 39,
        PosPriv_KeypadEntry = 40,
        PosPriv_LastTransaction = 41,
        PosPriv_Logout = 42,
        PosPriv_MenuPage1 = 43,
        PosPriv_MenuPage2 = 44,
        PosPriv_MenuPage3 = 45,
        PosPriv_MenuPage4 = 46,
        PosPriv_NoSale = 47,
        PosPriv_PosMessage = 48,
        PosPriv_PriceCheck = 49,
        PosPriv_PrintReport = 50,
        PosPriv_Return = 51,
        PosPriv_SelectMenu = 52,
        PosPriv_SlateSettings = 53,
        PosPriv_StoredValAcctBalCheck = 54,
        PosPriv_StoredValueCharge = 55,
        PosPriv_StoredValueDeposit = 56,
        PosPriv_Subtotal = 57,
        PosPriv_Surcharge1 = 58,
        PosPriv_Surcharge2 = 59,
        PosPriv_Tax1 = 60,
        PosPriv_Tax2 = 61,
        PosPriv_TimesX = 62,
        PosPriv_TogglePrinter = 63,
        PosPriv_TransactionComment = 64,
        PosPriv_ViewJournalTape = 65,
        PosPriv_VoidItem = 66,
        PosPriv_WipeScreen = 67,
        PrintReport_PosDailyAudit = 68,
        PrintReport_ProftCntrDailyAudt = 69,
        PrintReport_SessionDrawerAudit = 70,
        SDeviceSettings_Edit = 71,
        SDeviceSettings_View = 72,
        SlateSettings_ExportOfflineTr = 73,
        SlateSettings_RestoreDefaults = 74,
        SlateSettings_SDeviceSettings = 75,
        SlateSettings_SLogSettings = 76,
        SlateSettings_SNetworkSettings = 77,
        SLogSettings_Export = 78,
        SLogSettings_View = 79,
        SNetworkSettings_Edit = 80,
        SNetworkSettings_View = 81,
        StoredValue_Deposit = 82,
        StoredValue_Return = 83,
        StoredValue_Sale = 84,
        StoredValue_Transfer = 85,
        WsPriv = 86,
        WsPriv_Board = 87,
        WsPriv_BoardCashEquiv = 88,
        WsPriv_Cash = 89,
        WsPriv_Check = 90,
        WsPriv_CreditCard = 91,
        WsPriv_StoredValue = 92,
        PR5000Settings_SNetworkSetting = 93,
        PR5000Settings_SLogSettings = 94,
        PR5000Settings_SDeviceSettings = 95,
        PR5000Settings_RestoreDefaults = 96,
        PR5000Settings_PR5000DSUpdates = 97,
        PR5000Settings_ExportOfflineTr = 98,
        PR5000DSUpdates_View = 99,
        PR5000DSUpdates_Apply = 100,
        PosPriv_PR5000Settings = 101
    }
}