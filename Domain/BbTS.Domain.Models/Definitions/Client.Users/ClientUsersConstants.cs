﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Definitions.Client.Users
{
    public class ClientUsersConstants
    {
        public const string FriendlyMessage = "Communication error with Transact API.\n";
    }
}
