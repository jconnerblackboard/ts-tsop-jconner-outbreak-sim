﻿using System;

namespace BbTS.Domain.Models.Definitions.Institution
{
    /// <summary>
    /// Institution definitions
    /// </summary>
    public static class InstitutionDefinitions
    {
        /// <summary>
        /// Unknown institution guid
        /// </summary>
        public static Guid UnknownInstitutionGuid { get; } = new Guid("12E7AE52-5AC9-419C-8BE5-27B290AEF303");
    }
}
