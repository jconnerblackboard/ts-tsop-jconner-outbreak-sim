﻿using System;

namespace BbTS.Domain.Models.Definitions.Container
{
    public enum CashierMode
    {
        /// <summary>
        /// This cashier was operating under their normal cashier rights.
        /// </summary>
        Standard,

        /// <summary>
        /// This cashier was operating as a manager and used manager override during this transaction.
        /// </summary>
        Manager
    }

    /// <summary>
    /// Enumeration of cashier drawer numbers
    /// </summary>
    public enum CashDrawerNumber
    {
        /// <summary>
        /// There is no cash drawer present or on the device or no drawer number was specified.
        /// </summary>
        NotSpecified = 0,
        /// <summary>
        /// Operator in the cashier role is using drawer 1.
        /// </summary>
        CashDrawer1 = 1,
        /// <summary>
        /// Operator in the cashier role is using drawer 2.
        /// </summary>
        CashDrawer2 = 2
    }

    public enum ValidationType
    {
        AcceptedOnline,
        AcceptedOffline,
        DeniedOnline,
        DeniedOffline
    }

    public enum CreditCardMethodType
    {
        None,
        BbHost,
        Direct,
        PaymentExpress
    }

    public enum TransactionTypeCode
    {
        RetailTran,
        ControlTransaction
    }

    public enum AttendedType
    {
        NoCashier,
        CashierPresent
    }

    /// <summary>
    /// Type of the retail transaction
    /// </summary>
    public enum RetailTranType
    {
        /// <summary>
        /// Sale - 0
        /// </summary>
        Sale = 0,
        /// <summary>
        /// Return - 1
        /// </summary>
        Return = 1
    }

    public enum AttendedEventTranType
    {
        RegularEntry = 101,
        RegularReEntry = 102,
        RegularSwipeOut = 103,
        RegularReverse = 104,
        GuestEntry = 201,
        GuestReEntry = 202,
        GuestSwipeOut = 203,
        GuestReverse = 204,
        NonCustomerEntry = 301,
        NonCustomerSwipeOut = 303,
        NonCustomerReverse = 304
    }

    public enum ControlType
    {
        SignOn = 1,
        SignOff = 2
    }

    public enum RequestResponseCode
    {
        Approved = 1,
        Declined = 2,
        BadFormat = 3,
        Error = 4
    }

    //ToDo - maybe add these to DomainValue in namespace BbTS.Domain.Models.Definitions?
    public enum BbtsCreditCardDomain
    {
        Config = 203,           //Library configuration issues
        Web = 204,              //WebExceptionStatus exceptions
        General = 205,          //General exception
        ClearCommerce = 206     //ClearCommerce return values
    }

    public enum RequestType
    {
        Authorization,
        CreditPrimary,
        CreditSecondary,
        Void,
        ForceInsertAuth,
        PreAuth,
        PostAuth
    }

    public enum TransactionType
    {
        Control = 0,
        AttendedEvent = 1,
        Board = 2,
        CashCheck = 3,
        CashEquiv = 4,
        CreditCard = 5,
        StoredValue = 6,
        Deposit = 7,
        CreditCardCancelAuth = 8,
        CreditCardComplete = 9,
        PaymentGatewayCommLog = 10,
        Emv = 11,
        EmvTxnGet1ResponseLog = 12,
        EmvTxnVoidRequestLog = 13,
        EmvTxnVoidResponseLog = 14,

        Unknown = 999 //Ordinarily, I'd prefer Unknown to be at the top (and be the default, 0), but this enum did not always have explicitly defined int values.
    }

    /// <summary>
    /// Transaction classification.
    /// </summary>
    public enum TransactionClassification
    {
        /// <summary>
        /// Unknown or not determinable.
        /// </summary>
        Unknown,

        /// <summary>
        /// Arts transaction.
        /// </summary>
        Arts,

        /// <summary>
        /// Attendance transaction.
        /// </summary>
        Attendance,

        /// <summary>
        /// Board transaction.
        /// </summary>
        Board,

        /// <summary>
        /// Control transaction.
        /// </summary>
        Control
    }

    /// <summary>
    /// Tender flags.
    /// </summary>
    [Flags]
    public enum TenderFlags
    {
        /// <summary>
        /// None.
        /// </summary>
        None = 0,

        /// <summary>
        /// Cash.
        /// </summary>
        Cash = 1,

        /// <summary>
        /// Check.
        /// </summary>
        Check = 2,

        /// <summary>
        /// Stored value.
        /// </summary>
        StoredValue = 4,

        /// <summary>
        /// Credit card.
        /// </summary>
        CreditCard = 8,

        /// <summary>
        /// Cash equivalence.
        /// </summary>
        CashEquivalence = 16
    }
}
