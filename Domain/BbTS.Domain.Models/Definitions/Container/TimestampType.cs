﻿namespace BbTS.Domain.Models.Definitions.Container
{
    /// <summary>
    /// Used to label timestamps relevant to the transaction.
    /// </summary>
    public enum TimestampType
    {
        /// <summary>
        /// When the transaction was started at the Originator, typically set by Originator.
        /// </summary>
        Start,

        /// <summary>
        /// When the transaction was completed at the Originator, typically set by Originator.
        /// </summary>
        Finish,

        /// <summary>
        /// When the transaction was sent from the Originator, typically set by Originator.
        /// </summary>
        Submit,

        /// <summary>
        /// In cases where the transaction could not be sent, this is when the transaction was put into the offline storage, typically set by storage agent.
        /// </summary>
        InQueue,

        /// <summary>
        /// When the transaction was posted to the BbTS database, typically set by middle tier.
        /// </summary>
        Post
    }
}
