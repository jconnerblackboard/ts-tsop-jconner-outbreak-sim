﻿namespace BbTS.Domain.Models.Definitions.Container
{
    /// <summary>
    /// Overall state of the transaction.
    /// </summary>
    public enum Disposition
    {
        /// <summary>
        /// Default status
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Transaction is in process at the originator
        /// </summary>
        InProcess = 1,

        /// <summary>
        /// Finished at originator
        /// </summary>
        Complete = 2,

        /// <summary>
        /// Finished at originator, and accepted by host/api/db
        /// </summary>
        Accepted = 3,

        /// <summary>
        /// Finished at originator, but rejected by host/api/db
        /// </summary>
        Rejected = 4,

        /// <summary>
        /// Cancelled at originator
        /// </summary>
        Cancelled = 5
    }

}
