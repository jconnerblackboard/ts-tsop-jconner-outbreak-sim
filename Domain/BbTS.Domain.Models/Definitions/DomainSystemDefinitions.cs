﻿namespace BbTS.Domain.Models.Definitions
{
    /// <summary>
    /// Audit log event type
    /// </summary>
    public enum AuditLogEventType
    {
        /// <summary>
        /// LogIn = 0
        /// </summary>
        LogIn = 0,
        /// <summary>
        /// LogOut = 1
        /// </summary>
        LogOut = 1,
        /// <summary>
        /// Add = 2
        /// </summary>
        Add = 2,
        /// <summary>
        /// Edit = 3
        /// </summary>
        Edit = 3,
        /// <summary>
        /// Remove = 4
        /// </summary>
        Remove = 4,
        /// <summary>
        /// Rename = 5
        /// </summary>
        Rename = 5,
        /// <summary>
        /// View = 6
        /// </summary>
        View = 6,
        /// <summary>
        /// QueryForOrList = 7
        /// </summary>
        QueryForOrList = 7,
        /// <summary>
        /// UserLockBySystem = 8
        /// </summary>
        UserLockBySystem = 8,
        /// <summary>
        /// UserUnlock = 9
        /// </summary>
        UserUnlock = 9,
        /// <summary>
        /// UserPasswordChange = 10
        /// </summary>
        UserPasswordChange = 10
    }

    /// <summary>
    /// Audit log entry entity names.
    /// </summary>
    public enum AuditLogEntityName
    {
        /// <summary>
        /// User
        /// </summary>
        User = 50
    }

    /// <summary>
    /// Generic enumerations for low, medium and high.
    /// </summary>
    public enum LowMediumHigh
    {
        /// <summary>
        /// Low = 1
        /// </summary>
        Low = 1,
        /// <summary>
        /// Medium = 2
        /// </summary>
        Medium = 2,
        /// <summary>
        /// High = 3
        /// </summary>
        High = 3
    }

    /// <summary>
    /// Connection test result enumeration
    /// </summary>
    public enum ConnectionTest
    {
        /// <summary>
        /// Unknown
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// Success
        /// </summary>
        Success = 1,
        /// <summary>
        /// Failure
        /// </summary>
        Failure = 2
    }

    /// <summary>
    /// Activity log origin entity enumeration
    /// </summary>
    public enum ActivityLogOriginEntity
    {
        /// <summary>
        /// Integration monitor on premises.
        /// </summary>
        IntegrationMonitorOnPremises = 0
    }

    /// <summary>
    /// Connection string types for generate a database connection string.
    /// </summary>
    public enum DatabaseConnectionStringType
    {
        /// <summary>
        /// Use the Devart library with Direct=True
        /// Sample:
        /// 
        /// User Id={userId}; Data Source={server}; Port={port};Password={password};SID={serviceName};Direct=True
        /// </summary>
        DevartDirectConnect,
        /// <summary>
        /// Use the devart library with no direct connect and use a SID.
        /// Sample:
        /// 
        /// User Id={userId}; Data Source={server}; Port={port};Password={password};SID={serviceName};Direct=False
        /// </summary>
        DevartSidNoDirect,
        /// <summary>
        /// Use the Devart library with a TNS formatted connection string.  Direct is disabled in this method.
        /// Sample:
        /// 
        /// Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST={server})(PORT={port}))(CONNECT_DATA=(SERVICE_NAME={serviceName})));User Id={userId};Password={password};
        /// </summary>
        TnsName,
        /// <summary>
        /// Using the oracle managed dll connection string type.
        /// Sample:
        /// 
        /// User Id = {userId}; Password = {password}; Data Source = {server}:{port}/{serviceName}
        /// </summary>
        OracleManaged
    }
}
