﻿
namespace BbTS.Domain.Models.Definitions.Door
{
    /// <summary>
    /// Door control mode
    /// </summary>
    public enum DoorControlMode
    {
        Controlled = 0,
        Unlocked = 1,
        Locked = 2,
        MomentaryUnlocked = 3
    }

    /// <summary>
    /// Door override status
    /// </summary>
    public enum DoorOverrideStatus
    {
        AsScheduled = 0,
        IndefinateOverride = 1,
        SpanOverride = 2
    }

    /// <summary>
    /// Door alarm type
    /// </summary>
    public enum DoorAlarmLogType
    {
        HeldOpen = 0,
        ForcedOpen = 1,
        Tampered = 2,
        CommLost = 3,
        Tb1AuxCsTamper = 4,
        Tb2AuxCsTamper = 5,
        Serial1CsCommLost = 6,
        Serial2CsCommLost = 7,
        AcPowerFail = 8,
        BatteryLow = 9
    }

    /// <summary>
    /// Door transaction validation type
    /// </summary>
    public enum DoorTransactionValidationType
    {
        AcceptedOnline = 0,
        AcceptedOffline = 1
    }

    /// <summary>
    /// Customer identification type
    /// </summary>
    public enum CustomerIdentificationType
    {
        CardNumber = 0,
        FutureFeature = 1
    }

    /// <summary>
    /// Enumeration of various door states.
    /// </summary>
    public enum DoorStateValue
    {
        /// <summary>
        /// Door is forced and open
        /// </summary>
        ForcedAndOpen = 0,
        /// <summary>
        /// Door is tampered and open
        /// </summary>
        TamperedAndOpen = 1,
        /// <summary>
        /// Door is forced, but not open
        /// </summary>
        Forced = 2,
        /// <summary>
        /// Door is tampered, but not open
        /// </summary>
        Tampered = 3,
        /// <summary>
        /// Door is disabled.
        /// </summary>
        Disabled = 4,
        /// <summary>
        /// Door is held open
        /// </summary>
        Held = 5,
        /// <summary>
        /// Door is offline
        /// </summary>
        Offline = 6,
        /// <summary>
        /// Door is open
        /// </summary>
        Open = 7,
        /// <summary>
        /// Door is unlocked and open
        /// </summary>
        UnlockedAndOpen = 8,
        /// <summary>
        /// Door is unlocked
        /// </summary>
        Unlocked = 9,
        /// <summary>
        /// Door is in a requires pin mode and is open.
        /// </summary>
        RequiresPinAndOpen = 10,
        /// <summary>
        /// Door is in requires pin mode.
        /// </summary>
        RequiresPin = 11,
        /// <summary>
        /// Door is in a controlled mode and open.
        /// </summary>
        ControlledAndOpen = 12,
        /// <summary>
        /// Door is in a controlled mode but closed.
        /// </summary>
        Controlled = 13,
        /// <summary>
        /// Door is in a locked mode and open.
        /// </summary>
        LockedAndOpen = 14,
        /// <summary>
        /// Door is in locked mode but closed.
        /// </summary>
        Locked = 15,
        /// <summary>
        /// Door is in normal mode.
        /// </summary>
        Normal = 16,
        /// <summary>
        /// Door configuration needed.
        /// </summary>
        ConfigNeeded = 17,
        /// <summary>
        /// Door state is unknown.
        /// </summary>
        Unknown = 18,
        /// <summary>
        /// Door is initializing.
        /// </summary>
        Initializing = 19
    }

    /// <summary>
    /// Door alarm type
    /// </summary>
    public enum DoorAlarmType
    {
        /// <summary>
        /// No alarm is sent to the DA Host and no siren sounds.
        /// </summary>
        Disabled = 0,
        /// <summary>
        /// A alarm is sent to the DA Host after the held alarm delay time has expired.
        /// </summary>
        Silent = 1,
        /// <summary>
        /// Siren sounds after the held alarm delay time has expired.
        /// </summary>
        Siren = 2
    }

    /// <summary>
    /// Offline operation
    /// </summary>
    public enum DoorOfflineOperation
    {
        SiteCodeCheckOnly = 0,
        OnlineEnumeration = 1,
        NoAccess = 2
    }

    /// <summary>
    /// Enumeration of door status monitor packet types.
    /// </summary>
    public enum DoorStatusMonitorPacketTypes
    {
        /// <summary>
        /// Subscribe to a stats service.
        /// </summary>
        StatsSubscription = 201
    }

    /// <summary>
    /// Enumeration of values used in a door state override packet.
    /// </summary>
    public enum DoorStateChangeStateOverrideValues
    {
        /// <summary>
        /// Not defined.
        /// </summary>
        Undefined = 0,
        /// <summary>
        /// Unlocked.
        /// </summary>
        Unlocked = 5,
        /// <summary>
        /// Locked.
        /// </summary>
        Locked = 6,
        /// <summary>
        /// Controlled.
        /// </summary>
        Controlled = 3,
        /// <summary>
        /// Require Pin.
        /// </summary>
        RequirePin = 4
    }

    /// <summary>
    /// Enumeration of values used in assa abloy door state override.
    /// </summary>
    public enum DoorAaStateChangeStateOverrideValues
    {
        /// <summary>
        /// Not defined.
        /// </summary>
        Undefined = 0,
        /// <summary>
        /// Unlocked.
        /// </summary>
        Unlock = 1,
        /// <summary>
        /// Locked.
        /// </summary>
        Lock = 2,
        /// <summary>
        /// Panic off.
        /// </summary>
        PanicOff = 3,
        /// <summary>
        /// Panic on.
        /// </summary>
        PanicOn = 4,
        /// <summary>
        /// Pulse.
        /// </summary>
        Pulse = 5
    }

    /// <summary>
    /// Enumeration of override duration values for a door state change request.
    /// </summary>
    public enum DoorStateChangeDurationOverrideValue
    {
        /// <summary>
        /// Change until the next schedule change.
        /// </summary>
        Schedule = 1,
        /// <summary>
        /// Change state until canceled.
        /// </summary>
        Cancelled = 2
    }

    /// <summary>
    /// Enumeration of host communication packet types.
    /// </summary>
    public enum HostPacketTypes
    {
        /// <summary>
        /// Door State change.
        /// </summary>
        DoorStateChange = 59
    }

    /// <summary>
    /// Door type
    /// </summary>
    public enum DoorType
    {
        /// <summary>
        /// Door table
        /// </summary>
        Blackboard = 0,
        /// <summary>
        /// DSRAccessPoint table
        /// </summary>
        AssaAbloy = 1
    }

    /// <summary>
    /// Door connectivity
    /// </summary>
    public enum DoorConnectivity
    {
        Wireless = 0,
        Wired = 1
    }

    /// <summary>
    /// Door override type for customer door overrides
    /// </summary>
    public enum DoorOverrideType
    {
        AddToPlan = 0,
        OverridePlan = 1
    }
}
