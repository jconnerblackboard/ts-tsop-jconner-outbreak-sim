﻿using System.Collections.Generic;
using BbTS.Domain.Models.System;

namespace BbTS.Domain.Models.Definitions.PaymentExpress
{
    public enum ClientPaymentExpressDomainId
    {
        Unknown = 0,
        Success = 1,
        ConfigDirectoryDoesNotExist = 2,
        BackupDirectoryDoesNotExist = 3,
        CouldNotCreateBackupDirectory = 4,
        MissingHostInterface = 5,
        CouldNotSaveConfiguration = 6,
        GeneralException = 7,
        NoScrConnection = 8,
        ScrConnectionSendException = 9,
        RequestTimedOut = 10,
        RegistryValueNotFound = 11,
        ServiceNotFound = 12,
        ServiceStopFailed = 13,
        ServiceStartFailed = 14,
        ResponseTimedOut = 15
    }
    
    public static class DomainPaymentExpressDefinitions
    {
        public const string ScrControllerServiceName = "Payment Express SCR200 Controller";
        public const string EmvComPortServiceName = "Blackboard EMV COM Port Sync Service";
        public const string EmvConfigFileRegistryKey = "HKEY_LOCAL_MACHINE\\SOFTWARE\\Blackboard\\Transact\\PxScrController";
        public const string EmvConfigFileRegistryValueName = "ConfigFilePath";
        public const string TwDrvServiceRegistryKey = "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\TWTouch\\Parameters";
        public const string TwDrvServiceRegistryValue = "SerialPortNoSearch";
        public const string TsDrvServiceName = "MT7 Serial Search Service";

        private const int BbTsClientPaymentExpress = (int)DomainValue.DomainClientPaymentExpress;

        private static readonly Dictionary<ClientPaymentExpressDomainId, string> ClientPaymentExpressCodes = new Dictionary<ClientPaymentExpressDomainId, string>
        {
            { ClientPaymentExpressDomainId.Unknown, "Unknown Error" },
            { ClientPaymentExpressDomainId.Success, "Success" },
            { ClientPaymentExpressDomainId.ConfigDirectoryDoesNotExist, "Configuration directory does not exist" },
            { ClientPaymentExpressDomainId.BackupDirectoryDoesNotExist, "Backup directory does not exist and could not be created" },
            { ClientPaymentExpressDomainId.CouldNotCreateBackupDirectory, "An exception occured while trying to create the backup directory" },
            { ClientPaymentExpressDomainId.MissingHostInterface, "PxScrController requires at least one HostInterface" },
            { ClientPaymentExpressDomainId.CouldNotSaveConfiguration, "Could not save PxScrController configuration" },
            { ClientPaymentExpressDomainId.GeneralException, "A general exception occured" },
            { ClientPaymentExpressDomainId.NoScrConnection, "Connection to PxScrController not available" },
            { ClientPaymentExpressDomainId.ScrConnectionSendException, "An exception occured while sending the request" },
            { ClientPaymentExpressDomainId.RequestTimedOut, "Request timed out" },
            { ClientPaymentExpressDomainId.RegistryValueNotFound, "Service location registry value not found" },
            { ClientPaymentExpressDomainId.ServiceNotFound, "Service not found" },
            { ClientPaymentExpressDomainId.ServiceStopFailed, "Service stop command failed" },
            { ClientPaymentExpressDomainId.ServiceStartFailed, "Service start command failed" },
            { ClientPaymentExpressDomainId.ResponseTimedOut, "Response timed out" }
        };

        public static ActionResultToken GetActionResultToken(ClientPaymentExpressDomainId domainId, string additionalInformation)
        {
            string message;
            if (!ClientPaymentExpressCodes.TryGetValue(domainId, out message))
            {
                message = ClientPaymentExpressCodes[ClientPaymentExpressDomainId.Unknown] + ":  " + domainId;
            }

            return new ActionResultToken
            {
                ResultDomain = BbTsClientPaymentExpress,
                ResultDomainId = (int)domainId,
                Message = string.IsNullOrEmpty(additionalInformation) ? message : message + " (" + additionalInformation + ")"
            };
        }

        public static ActionResultToken GetActionResultToken(ClientPaymentExpressDomainId domainId)
        {
            return GetActionResultToken(domainId, null);
        }
    }
}
