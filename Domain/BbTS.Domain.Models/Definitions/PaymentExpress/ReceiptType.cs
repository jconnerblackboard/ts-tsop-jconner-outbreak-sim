﻿namespace BbTS.Domain.Models.Definitions.PaymentExpress
{
    public enum ReceiptType
    {
        /// <summary>
        /// For a customer receipt that requires a signature.
        /// </summary>
        CustomerWithSignature = 1,
        /// <summary>
        /// For a customer receipt without signature.
        /// </summary>
        CustomerWithoutSignature = 2,
        /// <summary>
        /// For a merchant receipt.
        /// </summary>
        Merchant = 3,
        /// <summary>
        /// For a Logon receipt, indicating the result of the last manual logon attempt.
        /// </summary>
        Logon = 4
    }
}
