﻿using System.Collections.Generic;
using BbTS.Domain.Models.System;

namespace BbTS.Domain.Models.Definitions
{
    /// <summary>
    /// For ResultDomainId values originating from...     
    /// </summary>
    public enum DomainValue
    {
        /// <summary>
        /// unknown or generic source
        /// </summary>
        DomainGeneral = 222,
        /// <summary>
        /// BbTS.Client.Users
        /// </summary>
        DomainUserValidationTools = 223,
        /// <summary>
        /// TransactApi
        /// </summary>
        DomainTransactApi = 224,
        /// <summary>
        /// BbTS.Client.Terminal
        /// </summary>
        DomainClientTerminal = 232,
        /// <summary>
        /// PxScrController (Payment Express)
        /// </summary>
        DomainPxScrResponseCode = 236,
        /// <summary>
        /// BbTS.Client.PaymentExpress
        /// </summary>
        DomainClientPaymentExpress = 237,
        /// <summary>
        /// BbTS.Client.Terminal.Hydra
        /// </summary>
        DomainClientTerminalHydra = 241,
        /// <summary>
        /// NCR Register Code, MF4100 Code, etc.  (not from libraries)
        /// </summary>
        DomainTerminalLogic = 244,
        /// <summary>
        /// Persona Sync Service Domain
        /// </summary>
        DomainPersona = 248,
        /// <summary>
        /// BbTS.Client.TransactApi
        /// </summary>
        DomainClientTransactApi = 249,
        /// <summary>
        /// BbTS.Client.ServiceApi
        /// </summary>
        DomainClientServiceApi = 250,
        /// <summary>
        /// BbTS.Domain.Agent
        /// </summary>
        DomainAgentPluginService = 499,
        /// <summary>
        /// BbTS.Domain.Agent.UpdateAgent
        /// </summary>
        DomainAgentUpdateService = 500,
        /// <summary>
        /// The security monitor plugin.
        /// </summary>
        WcfExceptionHandler = 501
    }

    /// <summary>
    /// General Domain id codes.
    /// </summary>
    public enum DomainGeneralDomainIdCode
    {
        /// <summary>
        /// Success
        /// </summary>
        Success = 0,
        /// <summary>
        /// General
        /// </summary>
        GeneralException = 1
    }

    /// <summary>
    /// Definitions for "Domain" related functions.
    /// </summary>
    public class DomainDefinitions
    {
        /// <summary>
        /// Server response codes for the TransactApi Client for Users.
        /// </summary>
        public const string TransactApiClientUsersServerResponseCodes = @"BbTs.Client.Users.TransactApi";

        /// <summary>
        /// Password complexity domain (used for DomainListGet).
        /// </summary>
        public const string PasswordComplexityDomain = @"BbTS.Domain.System.Rules.PasswordComplexity";

        /// <summary>
        /// Empty contstructor.
        /// </summary>
        private DomainDefinitions()
        {
        }

        /// <summary>
        /// Singleton instance
        /// </summary>
        public static DomainDefinitions Instance { get; internal set; } = new DomainDefinitions();

        /// <summary>
        /// Dictionary containing a mapping for DomainTerminal result domain to internal ActionResult codes
        /// </summary>
        private readonly Dictionary<DomainGeneralDomainIdCode, ActionResultToken> _domainGeneralResultIdCodes =
            new Dictionary<DomainGeneralDomainIdCode, ActionResultToken>
            {
                {
                    DomainGeneralDomainIdCode.Success, new ActionResultToken
                    {
                        ResultDomain = (int)DomainValue.DomainGeneral,
                        ResultDomainId = (int)DomainGeneralDomainIdCode.Success,
                        Message = "Success."
                    }
                },
                {
                    DomainGeneralDomainIdCode.GeneralException, new ActionResultToken
                    {
                        ResultDomain = (int)DomainValue.DomainGeneral,
                        ResultDomainId = (int)DomainGeneralDomainIdCode.GeneralException,
                        Message = "General exception."
                    }
                }
            };

        /// <summary>
        /// Get the ActionResultToken for the DomainGeneral DomainId code
        /// </summary>
        /// <param name="code">DomainGeneral DomainId code</param>
        /// <returns></returns>
        public ActionResultToken GetGeneralDomainActionResultToken(DomainGeneralDomainIdCode code)
        {
            if (!_domainGeneralResultIdCodes.ContainsKey(code)) return null;
            return _domainGeneralResultIdCodes[code];
        }

    }


}
