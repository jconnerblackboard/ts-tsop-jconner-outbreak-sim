﻿namespace BbTS.Domain.Models.Definitions.Cache
{
    /// <summary>
    /// Enumeration to describe the different caching methods.
    /// </summary>
    public enum CacheType
    {
        /// <summary>
        /// Simple caching implementation
        /// </summary>
        Simple,
        /// <summary>
        /// In-Memory cache implementation
        /// </summary>
        Memory,
        /// <summary>
        /// Database cache implementation
        /// </summary>
        Database
    }
}
