﻿namespace BbTS.Domain.Models.Definitions.System.Caching
{
    /// <summary>
    /// MemoryCache Setting Constants
    /// </summary>
    public static class MemoryCacheSettingConstants
    {
        /// <summary>
        /// The physical memory limit percentage
        /// </summary>
        public const string PhysicalMemoryLimitPercentage = "PhysicalMemoryLimitPercentage";

        /// <summary>
        /// The cache memory limit megabytes
        /// </summary>
        public const string CacheMemoryLimitMegabytes = "CacheMemoryLimitMegabytes";

        /// <summary>
        /// The polling interval
        /// </summary>
        public const string PollingInterval = "PollingInterval";
    }
}
