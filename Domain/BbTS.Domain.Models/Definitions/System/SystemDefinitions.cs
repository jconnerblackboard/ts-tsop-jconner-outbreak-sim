﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Definitions.System
{
    /// <summary>
    /// Enumeration of validation value operators for resource layer validation functions.
    /// </summary>
    public enum ValidateValueOperator
    {
        /// <summary>
        /// Unknown operator.
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// Not equal.
        /// </summary>
        NotEqual = 1,
        /// <summary>
        /// Equal
        /// </summary>
        Equal = 2,
        /// <summary>
        /// Less than.
        /// </summary>
        LessThan = 3,
        /// <summary>
        /// Greater Than
        /// </summary>
        GreaterThan = 4,
        /// <summary>
        /// Less Than Or Equal
        /// </summary>
        LessThanOrEqual = 5,
        /// <summary>
        /// Greather Than Or Equal
        /// </summary>
        GreatherThanOrEqual = 6
    }

    /// <summary>
    /// Consts and functions related to "System"
    /// </summary>
    public static class SystemDefinitions
    {
        /// <summary>
        /// The minimum date time value for TSOP will be 1/1/1900 00:00:00.00000 +0:00
        /// </summary>
        public static DateTimeOffset TsopDateTimeMin = new DateTimeOffset(1900, 1, 1, 0, 0, 0, 0, new TimeSpan(0, 0, 0));

        /// <summary>
        /// The maximum date time value for TSOP will be 12/31/3000 00:00:00.00000 +0:00
        /// </summary>
        public static DateTimeOffset TsopDateTimeMax = new DateTimeOffset(3000, 12, 31, 23, 59, 59, 999, new TimeSpan(0, 0, 0));
    }
}
