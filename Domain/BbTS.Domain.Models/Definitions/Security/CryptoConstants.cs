﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Definitions.Security
{
    /// <summary>
    ///     Defines constant values used in cryptography
    /// </summary>
    public static class CryptoConstants
    {
        /// <summary>
        ///     The HMAC-SHA1 constant
        /// </summary>
        public const string HmacSha1 = "HMAC-SHA1";

        /// <summary>
        ///     The RSA-SHA1 constant
        /// </summary>
        public const string RsaSha1 = "RSA-SHA1";

        /// <summary>
        ///     The PLAINTEXT constant
        /// </summary>
        public const string Plaintext = "PLAINTEXT";
    }
}
