﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Definitions.Security.Oauth
{
    /// <summary>
    /// Exception codes for OAuth
    /// </summary>
    public enum OauthExceptionCode
    {
        /// <summary>
        /// General OAuth exception
        /// </summary>
        General = 0,
        /// <summary>
        /// "No data was returned from an upstream system unexpectedly. It is unknown whether the request would succeed if resubmitted."
        /// </summary>
        ProcessingException,
        /// <summary>
        /// "The value provided for one of the request parameters is invalid."
        /// </summary>
        InvalidParameters,
        /// <summary>
        /// "The application authentication data is invalid."
        /// </summary>
        InvalidApplication,
        /// <summary>
        /// "The user credential provided is invalid."
        /// </summary>
        InvalidCredentials,
        /// <summary>
        /// The requested system is not supported on the server."
        /// </summary>
        InvalidSystem,
        /// <summary>
        /// "Invalid Interface Protocol Version"
        /// </summary>
        InvalidInterfaceProtocolVersion,
        /// <summary>
        /// "ClientID Not Found"
        /// </summary>
        TsClientIdNotFound,
        /// <summary>
        /// "ClientID Not Active"
        /// </summary>
        TsClientIdNotActive,
        /// <summary>
        /// "Invalid Client Login"
        /// </summary>
        InvalidTsClientLogin,
        /// <summary>
        /// "Terminal Number Not Found"
        /// </summary>
        TsTerminalNumberNotFound,
        /// <summary>
        /// "Invalid Client Login for Terminal"
        /// </summary>
        InvalidTsPosLogin,
        /// <summary>
        /// "Unsupported Parameter"
        /// </summary>
        OauthUnsupportedParameter,
        /// <summary>
        /// "Unsupported Signature Method"
        /// </summary>
        OauthUnsupportedSignatureMethod,
        /// <summary>
        /// "Missing required parameter"
        /// </summary>
        OauthMissingParameter,
        /// <summary>
        /// "Duplicate OAuth Protocol Parameter"
        /// </summary>
        OauthDuplicateParameter,
        /// <summary>
        /// "Invalid Consumer Key"
        /// </summary>
        OauthInvalidConsumerKey,
        /// <summary>
        /// "Invalid or expired Token"
        /// </summary>
        OauthInvalidExpiredToken,
        /// <summary>
        /// "Invalid signature"
        /// </summary>
        OauthInvalidSignature,
        /// <summary>
        /// "Invalid or duplicate nonce"
        /// </summary>
        OauthInvalidNonce,
        /// <summary>
        /// "Invalid timestamp value"
        /// </summary>
        OauthInvalidTimestamp,
        /// <summary>
        /// "OAuth Parameter parsing failed because Authorization Header not found or the header has no data"
        /// </summary>
        AuthorizationHeaderNotFound,
        /// <summary>
        /// "OAuth Parameter parsing failed because Authorization scheme not found"
        /// </summary>
        AuthorizationSchemaNotFound,
        /// <summary>
        /// The token validation failed error message
        /// </summary>
        TokenValidationFailed
    }

    /// <summary>
    /// Definitions class to match an enumeration with a message.
    /// </summary>
    public class OauthDefinitions
    {
        private static readonly Dictionary<OauthExceptionCode, string> ExceptionCodeMessages = new Dictionary
            <OauthExceptionCode, string>
        {
            { OauthExceptionCode.ProcessingException, "No data was returned from an upstream system unexpectedly. It is unknown whether the request would succeed if resubmitted." },
            { OauthExceptionCode.InvalidParameters, "The value provided for one of the request parameters is invalid." },
            { OauthExceptionCode.InvalidApplication, "The application authentication data is invalid." },
            { OauthExceptionCode.InvalidCredentials, "The user credential provided is invalid." },
            { OauthExceptionCode.InvalidSystem, "The requested system is not supported on the server." },
            { OauthExceptionCode.InvalidInterfaceProtocolVersion, "Invalid Interface Protocol Version" },
            { OauthExceptionCode.TsClientIdNotFound, "ClientID Not Found" },
            { OauthExceptionCode.TsClientIdNotActive, "ClientID Not Active" },
            { OauthExceptionCode.InvalidTsClientLogin, "Invalid Client Login" },
            { OauthExceptionCode.TsTerminalNumberNotFound, "Terminal Number Not Found" },
            { OauthExceptionCode.InvalidTsPosLogin, "Invalid Client Login for Terminal" },
            { OauthExceptionCode.OauthUnsupportedParameter, "Unsupported Parameter" },
            { OauthExceptionCode.OauthMissingParameter, "Missing required parameter" },
            { OauthExceptionCode.OauthDuplicateParameter, "Duplicate OAuth Protocol Parameter" },
            { OauthExceptionCode.OauthInvalidConsumerKey, "Invalid Consumer Key" },
            { OauthExceptionCode.OauthInvalidExpiredToken, "Invalid or expired Token" },
            { OauthExceptionCode.OauthInvalidSignature, "Invalid signature" },
            { OauthExceptionCode.OauthInvalidNonce, "Invalid or duplicate nonce" },
            { OauthExceptionCode.OauthInvalidTimestamp, "Invalid timestamp value" },
            { OauthExceptionCode.AuthorizationHeaderNotFound, "OAuth Parameter parsing failed because Authorization Header not found or the header has no data" },
            {OauthExceptionCode.AuthorizationSchemaNotFound, "OAuth Parameter parsing failed because Authorization scheme not found" },
            { OauthExceptionCode.TokenValidationFailed, "OAuth validation failed. Verify that all required parameters are being sent in the request."}
        };

        /// <summary>
        /// Create a message from the OAuth code.
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static string Message(OauthExceptionCode code)
        {
            return ExceptionCodeMessages.ContainsKey(code)
                ? ExceptionCodeMessages[code]
                : $"Invalid exception code: {code}";
        }
    }
}
