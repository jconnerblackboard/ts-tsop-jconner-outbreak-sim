﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Core;
using BbTS.Domain.Models.System.Security.Oauth;

namespace BbTS.Domain.Models.Definitions.Security.Oauth
{
    /// <summary>
    ///     The inputs that will be used to create an OAuth signature.
    /// </summary>
    public class OAuthSignatureParameters
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OAuthSignatureParameters" /> class.
        /// </summary>
        /// <param name="httpMethod">The HTTP method.</param>
        /// <param name="serverUri">The server URI.</param>
        /// <param name="contentType">The request content type.</param>
        /// <param name="oauthParameters">The authentication parameters.</param>
        /// <param name="clientSecret">The client shared secret.</param>
        /// <param name="tokenSecret">The token shared secret.</param>
        /// <param name="queryString">The query string.</param>
        /// <param name="formData">The form data.</param>
        public OAuthSignatureParameters(
            string httpMethod,
            Uri serverUri,
            string contentType, // can be null, an example is a GET with no request data
            OAuthParameters oauthParameters,
            string clientSecret,
            string tokenSecret,
            NameValueCollection queryString,
            NameValueCollection formData)
        {
            Guard.IsNotNullOrEmpty(httpMethod, "httpMethod");
            Guard.IsNotNull(serverUri, "serverUri");
            Guard.IsNotNull(oauthParameters, "oAuthParameters");
            Guard.IsNotNullOrEmpty(clientSecret, "clientSecret");
            Guard.IsNotNull(tokenSecret, "tokenSecret"); // It is okay for this to be an empty string
            Guard.IsNotNull(queryString, "queryString");
            Guard.IsNotNull(formData, "formData");

            HttpMethod = httpMethod;
            ServerUri = serverUri;
            ContentType = contentType;
            OauthParameters = oauthParameters;
            ClientSecret = clientSecret;
            TokenSecret = tokenSecret;
            QueryString = queryString;
            FormData = formData;
        }

        /// <summary>
        /// Gets the authentication parameters.
        /// </summary>
        /// <value>
        /// The authentication parameters.
        /// </value>
        public OAuthParameters OauthParameters { get; private set; }

        /// <summary>
        /// Gets the client shared secret.
        /// </summary>
        /// <value>
        /// The client shared secret.
        /// </value>
        public string ClientSecret { get; private set; }

        /// <summary>
        /// Gets the token secret.
        /// </summary>
        /// <value>
        /// The token secret.
        /// </value>
        public string TokenSecret { get; private set; }

        /// <summary>
        /// Gets the HTTP method.
        /// </summary>
        /// <value>
        /// The HTTP method.
        /// </value>
        public string HttpMethod { get; private set; }

        /// <summary>
        /// Gets the server URI.
        /// </summary>
        /// <value>
        /// The server URI.
        /// </value>
        public Uri ServerUri { get; private set; }

        /// <summary>
        /// Gets the request content type.
        /// </summary>
        /// <value>
        /// The request content type.
        /// </value>
        public string ContentType { get; private set; }

        /// <summary>
        /// Gets the query string.
        /// </summary>
        /// <value>
        /// The query string.
        /// </value>
        public NameValueCollection QueryString { get; private set; }

        /// <summary>
        /// Gets the form data.
        /// </summary>
        /// <value>
        /// The form data.
        /// </value>
        public NameValueCollection FormData { get; private set; }
    }
}
