﻿
namespace BbTS.Domain.Models.Definitions.Security.Oauth
{
    /// <summary>
    ///     Defines the constants used by OAuth providers
    /// </summary>
    public static class OAuthConstants
    {
        /// <summary>
        ///     The authorization header
        /// </summary>
        public const string AuthorizationHeader = "Authorization";

        /// <summary>
        ///     The authorization realm
        /// </summary>
        public const string AuthorizationRealm = "realm";

        /// <summary>
        /// The client Consumer Key.
        /// </summary>
        public const string DefaultConsumerKey =
            "FwBgADUAFQBoANcAtQBhAfUAIQBvAFIAfQEYIAIABwAqAH4BYAEKAPsAbABeAPcASQCrAGsAEQBwAGEAJwDjALsAIiHdAKAAFgApAE4AMAAcIDgAQABtALEAUAB9ASwA";

        /// <summary>
        /// The client Consumer Secret.
        /// </summary>
        public const string DefaultConsumerSecret = "4gBoABUAGSDvAHAArQD/ALUAIgDKACsAVACnANkARwBkAFMAdwBbAH4AqwClACIh3gBfAGcAZADHAFQA1gA+AA==";

        /// <summary>
        ///     The identifier for the OAuth AuthorizationScheme
        /// </summary>
        public const string OAuthAuthorizationScheme = "OAuth";

        /// <summary>
        ///     The OAuth temporary token path
        /// </summary>
        public const string OAuthTemporaryTokenPath = "/initiate";

        /// <summary>
        ///     The OAuth token path
        /// </summary>
        public const string OAuthTokenPath = "/token";

        /// <summary>
        ///     The prefix used for all OAuth parameters
        /// </summary>
        public const string OAuthPrefix = "oauth_";

        /// <summary>
        ///     The oauth token constant
        /// </summary>
        public const string OAuthToken = "oauth_token";

        /// <summary>
        ///     The oauth token secret constant
        /// </summary>
        public const string OAuthTokenSecret = "oauth_token_secret";

        /// <summary>
        ///     The oauth callback constant
        /// </summary>
        public const string OAuthCallback = "oauth_callback";

        /// <summary>
        ///     The OAuth callback confirmed constant
        /// </summary>
        public const string OAuthCallbackConfirmed = "oauth_callback_confirmed";

        /// <summary>
        ///     The oauth consumer key constant
        /// </summary>
        public const string OAuthConsumerKey = "oauth_consumer_key";

        /// <summary>
        ///     The oauth signature method constant
        /// </summary>
        public const string OAuthSignatureMethod = "oauth_signature_method";

        /// <summary>
        ///     The oauth signature constant
        /// </summary>
        public const string OAuthSignature = "oauth_signature";

        /// <summary>
        ///     The oauth timestamp constant
        /// </summary>
        public const string OAuthTimestamp = "oauth_timestamp";

        /// <summary>
        ///     The oauth nonce constant
        /// </summary>
        public const string OAuthNonce = "oauth_nonce";

        /// <summary>
        ///     The oauth version constant
        /// </summary>
        public const string OAuthVersion = "oauth_version";

        /// <summary>
        /// Out-of-band indicates that a URI callback to allow the resource owner
        /// to authorize access will not be provided (essentially making the process
        /// two-legged instead of three-legged).
        /// </summary>
        public const string OutOfBand = "oob";
    }
}
