﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Definitions.Transaction
{
    public enum AuthorizationSource
    {
        None = 0,
        Internal = 1,
        CreditCard = 2
    }

    /// <summary>
    /// Product Type enumeration.
    /// </summary>
    public enum ProductType
    {
        /// <summary>
        /// Standard product.
        /// </summary>
        Standard = 0,

        /// <summary>
        /// Product must be weighed.
        /// </summary>
        Weighed = 1,

        /// <summary>
        /// Product must be weighed (zero tare).
        /// </summary>
        WeighedZeroTare = 2,

        /// <summary>
        /// The quantity of this product must be specified at the time of purchase.
        /// </summary>
        PromptForCount = 3,

        /// <summary>
        /// The price of this product must be entered at the time of purchase.
        /// </summary>
        PromptForPrice = 4
    }

    public enum TaxCalculationMode
    {
        NotTaxed,
        Normal,
        Override,
        Exempt
    }

    /// <summary>
    /// Tender type.
    /// </summary>
    public enum TenderType
    {
        /// <summary>
        /// Unknown or not determinable.
        /// </summary>
        Unknown = -1,

        /// <summary>
        /// Cash.
        /// </summary>
        Cash = 0,

        /// <summary>
        /// Check.
        /// </summary>
        Check = 1,

        /// <summary>
        /// Stored value.
        /// </summary>
        StoredValue = 2,

        /// <summary>
        /// Credit card.
        /// </summary>
        CreditCard = 3,

        /// <summary>
        /// Cash equivalence.
        /// </summary>
        BoardCashEquiv = 4
    }

    /// <summary>
    /// Credit card processing method.
    /// </summary>
    public enum CreditCardProcessingMethod
    {
        /// <summary>
        /// None.
        /// </summary>
        None = 0,

        /// <summary>
        /// Payment Gateway Host.
        /// </summary>
        PaymentGatewayHost = 1,

        /// <summary>
        /// Direct to Gateway.
        /// </summary>
        DirectToPaymentGateway = 2,

        /// <summary>
        /// Payment Express (EMV).
        /// </summary>
        PaymentExpress = 3
    }

    /// <summary>
    /// Enumeration for the validation tests that are done on a transaction.
    /// Used in the result of a validation process to denote what function was executed.
    /// </summary>
    public enum TransactionFunctionDefinition
    {
        /// <summary>
        /// Validate the board meal type id.
        /// </summary>
        BoardMealTypeIdValidate,
        /// <summary>
        /// Validate customer information in a transaction.
        /// </summary>
        CustomerTransactionInfoValidate,
        /// <summary>
        /// Validate the debit or credit type.
        /// </summary>
        DebitCreditTypeValidationValidate,
        /// <summary>
        /// Validate the operator using their unique identifier.
        /// </summary>
        OperatorByOperatorGuidValidate,
        /// <summary>
        /// Validate product promo.
        /// </summary>
        ProductPromoValidate,
        /// <summary>
        /// Validate a retail transaction line product.
        /// </summary>
        RetailTransactionLineProductValidate,
        /// <summary>
        /// Validate a retail transaction line product price modifier.
        /// </summary>
        RetailTransactionLineProductPriceModifierValidate,
        /// <summary>
        /// Validate a retail transaction line product tax.
        /// </summary>
        RetailTransactionLineProductTaxValidate,
        /// <summary>
        /// Validate a retail transaction line product tax override.
        /// </summary>
        RetailTransactionLineProductTaxOverrideValidate,
        /// <summary>
        /// Validate a retail transaction line tender.
        /// </summary>
        RetailTransactionLineTenderValidate,
        /// <summary>
        /// Validate a retail transaction line tender cash equivalency.
        /// </summary>
        RetailTransactionLineTenderCashEquivalencyValidate,
        /// <summary>
        /// Validate a retail transaction line tender customer.
        /// </summary>
        RetailTransactionLineTenderCustomerValidate,
        /// <summary>
        /// Validate transaction attributes.
        /// </summary>
        TransactionAttributeValidate,
        /// <summary>
        /// Validate the originator of the transaction.
        /// </summary>
        OriginatorValidate,
        /// <summary>
        /// Process an arts transaction.
        /// </summary>
        ArtsTransactionProcess,
        /// <summary>
        /// Extracting the tender type used to complete the transaction.
        /// </summary>
        TenderTypeExtraction,
        /// <summary>
        /// Process a board transaction.
        /// </summary>
        BoardTransactionProcess,
        /// <summary>
        /// Process an operator session begin request.
        /// </summary>
        OperatorSessionBegin,
        /// <summary>
        /// Process an operator session end request.
        /// </summary>
        OperatorSessionEnd,
        /// <summary>
        /// Process an attendance transaction.
        /// </summary>
        AttendanceTransactionProcess,
        /// <summary>
        /// Process a control transaction.
        /// </summary>
        ControlTransactionProcess
    }

    /// <summary>
    /// Enumeration of the results of the validaiton operation.
    /// </summary>
    public enum TransactionValidationResultEnumeration
    {
        /// <summary>
        /// Validation was a success.
        /// </summary>
        Pass,
        /// <summary>
        /// Validation failed.
        /// </summary>
        Fail,
        /// <summary>
        /// Validation passed with warnings.
        /// </summary>
        Warn,
        /// <summary>
        /// Validation test was inconclusive.
        /// </summary>
        Inconclusive,
        /// <summary>
        /// Validation test was skipped.
        /// </summary>
        Skipped
    }

    /// <summary>
    /// Enumeration of line item types.
    /// </summary>
    public enum TransactionLineItemType
    {
        /// <summary>
        /// An undefined line item type.
        /// </summary>
        LineItemUndefined,
        /// <summary>
        /// Begin line items reporting.
        /// </summary>
        LineItemRetailTransactionBegin,
        /// <summary>
        /// LineItemComment
        /// </summary>
        LineItemComment,
        /// <summary>
        /// LineItemDiscount
        /// </summary>
        LineItemDiscount,
        /// <summary>
        /// LineItemProduct
        /// </summary>
        LineItemProduct,
        /// <summary>
        /// LineItemProductPriceModifier
        /// </summary>
        LineItemProductPriceModifier,
        /// <summary>
        /// LineItemProductTax
        /// </summary>
        LineItemProductTax,
        /// <summary>
        /// LineItemProductTaxExempt
        /// </summary>
        LineItemProductTaxExempt,
        /// <summary>
        /// LineItemProductTaxOverride
        /// </summary>
        LineItemProductTaxOverride,
        /// <summary>
        /// LineItemStoredValue
        /// </summary>
        LineItemStoredValue,
        /// <summary>
        /// LineItemStoredValueEnrichment
        /// </summary>
        LineItemStoredValueEnrichment,
        /// <summary>
        /// LineItemSurcharge
        /// </summary>
        LineItemSurcharge,
        /// <summary>
        /// LineItemTax
        /// </summary>
        LineItemTax,
        /// <summary>
        /// LineItemTender
        /// </summary>
        LineItemTender,
        /// <summary>
        /// LineItemTenderWithCustomer
        /// </summary>
        LineItemTenderWithCustomer,
        /// <summary>
        /// LineItemTenderCashEquivalence
        /// </summary>
        LineItemTenderCashEquivalence,
        /// <summary>
        /// LineItemTenderDtg
        /// </summary>
        LineItemTenderDtg,
        /// <summary>
        /// LineItemTenderEmv
        /// </summary>
        LineItemTenderEmv,
        /// <summary>
        /// Processing an EMV transaction request (TxnReq).
        /// </summary>
        LineItemTenderEmvTxnReq,
        /// <summary>
        /// Processing an EMV transaction response (TxnResp).
        /// </summary>
        LineItemTenderEmvTxnResp,
        /// <summary>
        /// Processing an EMV Get1 response (Get1).
        /// </summary>
        LineItemTenderEmvGet1Resp,
        /// <summary>
        /// End list of line items.
        /// </summary>
        LineItemRetailTransactionEnd,
        /// <summary>
        /// LineItemTenderDiscount
        /// </summary>
        LineItemTenderDiscount,
        /// <summary>
        /// LineItemTenderSurcharge
        /// </summary>
        LineItemTenderSurcharge,
        /// <summary>
        /// LineItemTenderEmvHit
        /// </summary>
        LineItemTenderEmvHit,
        /// <summary>
        /// LineItemTenderEmvHitRecord
        /// </summary>
        LineItemTenderEmvHitRecord
    }

    /// <summary>
    ///     Enumeration of Stored Value Account Share Type
    /// </summary>
    public enum StoredValueAccountShareType
    {
        /// <summary>
        /// The standard
        /// </summary>
        Standard = 0,

        /// <summary>
        /// The group
        /// </summary>
        Group = 1,

        /// <summary>
        /// The unknown
        /// </summary>
        Unknown = 2,
    }

    /// <summary>
    /// Transaction object type enumeration.  Indicates the type of the object that has been serialized and saved to the TransactionObjectLog table.
    /// </summary>
    public enum TransactionObjectType
    {
        /// <summary>
        /// Unknown.
        /// </summary>
        Unknown,

        /// <summary>
        /// Transaction as it is/was defined in the MF4100 code.  Used for legacy purposes only.  Because this object type is not versioned, serialized objects of this type may not deserialize properly.
        /// </summary>
        TransactionMf4100,

        /// <summary>
        /// View object for an MF4100 transaction, version 1.
        /// </summary>
        TransactionMf4100ViewV01,

        /// <summary>
        /// Transaction.  Used for legacy purposes only.  Because this object type is not versioned, serialized objects of this type may not deserialize properly.
        /// </summary>
        Transaction,

        /// <summary>
        /// View object for a transaction, version 1.
        /// </summary>
        TransactionViewV01,

        /// <summary>
        /// View object for a transaction, version 2.
        /// </summary>
        TransactionViewV02
    }

    /// <summary>
    /// Emv Credit Card CardId enumeration. Corresponds to database domain 233 values.
    /// </summary>
    public enum EmvCreditCardId
    {
        /// <summary>
        /// Unknown card type
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// MasterCard card type
        /// </summary>
        MasterCard = 1,
        /// <summary>
        /// Visa card type
        /// </summary>
        Visa = 2,
        /// <summary>
        /// JCB card type
        /// </summary>
        // ReSharper disable once InconsistentNaming
        JCB = 3,
        /// <summary>
        /// Amex (American Express) card type
        /// </summary>
        Amex = 6,
        /// <summary>
        /// Diners card type
        /// </summary>
        Diners = 7
    }

    /// <summary>
    /// Utility class to map transaction object type to text.
    /// </summary>
    public static class TransactionObjectDefinitions
    {
        /// <summary>
        /// Transaction object types dictionary.
        /// </summary>
        public static readonly Dictionary<TransactionObjectType, string> Dictionary = new Dictionary<TransactionObjectType, string>
        {
            #region Dictionary values
            { TransactionObjectType.Unknown, "Unknown" },
            { TransactionObjectType.TransactionMf4100, "TransactionMf4100" },
            { TransactionObjectType.TransactionMf4100ViewV01, "TransactionMf4100ViewV01" },
            { TransactionObjectType.Transaction, "Transaction" },
            { TransactionObjectType.TransactionViewV01, "TransactionViewV01" },
            { TransactionObjectType.TransactionViewV02, "TransactionViewV02" },
            #endregion
        };

        /// <summary>
        /// Gets object type text.
        /// </summary>
        /// <param name="objectType"></param>
        /// <returns></returns>
        public static string TransactionObjectTypeGet(TransactionObjectType objectType)
        {
            string objectTypeText;
            if (!Dictionary.TryGetValue(objectType, out objectTypeText))
            {
                objectTypeText = $"{Dictionary[TransactionObjectType.Unknown]}";
            }

            return objectTypeText;
        }
    }
}
