﻿using BbTS.Domain.Models.System;

namespace BbTS.Domain.Models.Definitions.Agent
{
    /// <summary>
    /// Enumeration of actions that the Agent Plugin Service supports.
    /// </summary>
    public enum AgentPluginManagerAction
    {
        /// <summary>
        /// Action is unknown.
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// List all installed agents.
        /// </summary>
        ListAllInstalled = 1,
        /// <summary>
        /// List all the agents that are available for update/installation.
        /// </summary>
        ListAllAvailableUpdates = 2,
        /// <summary>
        /// Install an agent.
        /// </summary>
        InstallAgent = 3,
        /// <summary>
        /// Reload (into their respective AppDomains) all (unloaded) agents.
        /// </summary>
        ReloadAllAgents = 4,
        /// <summary>
        /// Fetch updates from the configured update server.
        /// </summary>
        FetchUpdates = 5,
        /// <summary>
        /// Enable an agent.  Calls the startup routine for the agent.
        /// </summary>
        EnableAgent = 6,
        /// <summary>
        /// Disable an agent.  Calls teh shutdown routing for the agent.  Agent will remain loaded into the AppDomain it was assigned.
        /// </summary>
        DisableAgent = 7,
        /// <summary>
        /// Unload an agent.  Removed the agent from the AppDomain and will ignore future parses of installed agent get requests.
        /// </summary>
        UnloadAgent = 8,
        /// <summary>
        /// Update an agent.
        /// </summary>
        UpdateAgent = 9,
        /// <summary>
        /// Remove an agent.  Unloads the agent from its respective AppDomain and deletes the plugin from the Plugins directory.
        /// </summary>
        RemoveAgent = 10,
        /// <summary>
        /// Authorize an agent to run.
        /// </summary>
        AuthorizeAgent = 11,
        /// <summary>
        /// Deauthorize an agent to run.
        /// </summary>
        DeauthorizeAgent = 12
    }

    /// <summary>
    /// Definitions for the agent service.
    /// </summary>
    public class AgentServiceDefinitions
    {
        /// <summary>
        /// Agent status enumerations.
        /// </summary>
        public enum AgentStatus
        {
            /// <summary>
            /// Agent is in an unknown state.
            /// </summary>
            Unknown,
            /// <summary>
            /// Agent is online.
            /// </summary>
            Online,
            /// <summary>
            /// Agent is offline.
            /// </summary>
            Offline,
            /// <summary>
            /// Agent is in an error state.
            /// </summary>
            Error
        }

        /// <summary>
        /// Agent service action result domain id codes.
        /// </summary>
        public enum AgentServiceActionResultDomainIdCode
        {
            /// <summary>
            /// Unknown failure.
            /// </summary>
            UnknownFailure = 99999,
            /// <summary>
            /// Success.
            /// </summary>
            Success = 0,
            /// <summary>
            /// Operation failed during an installed agents get request.
            /// </summary>
            InstalledAgentsGetFailed = 1,
            /// <summary>
            /// Operation failed during a reload all agents request.
            /// </summary>
            ReloadAllAgentsFailed = 2,
            /// <summary>
            /// Operation failed during an install agent request.
            /// </summary>
            InstallAgentFailed = 3,
            /// <summary>
            /// Operation failed during an update agent request.
            /// </summary>
            UpdateAgentFailed = 4,
            /// <summary>
            /// Operation failed during an enable/disable agent request.
            /// </summary>
            EnableAgentFailed = 5,
            /// <summary>
            /// Operation failed during an agent updates fetch request.
            /// </summary>
            AgentUpdatesFetchFailed = 6,
            /// <summary>
            /// Operation failed during an agent updates list request.
            /// </summary>
            AgentUpdatesListFailed = 7,
            /// <summary>
            /// Operation failed during an unload agent request.
            /// </summary>
            UnloadAgentFailed = 8,
            /// <summary>
            /// Operation failed during a remove agent request.
            /// </summary>
            RemoveAgentFailed = 9,
            /// <summary>
            /// Operation failed during an agent details get request.
            /// </summary>
            AgentDetailsGetFailed = 10,
            /// <summary>
            /// The authorization operation failed because the AuthorizedAgentPlugins control parameter group wasn't present in the data layer.
            /// </summary>
            AuthorizeAgentFailedNoParentKey = 11,
            /// <summary>
            /// The authorization operation failed because the return parameter after creating the authorization was null.
            /// </summary>
            AuthorizeAgentFailedPostCreateValidationCheck = 12,
            /// <summary>
            /// The deauthorization operation failed because the return parameter still has the same id as the agent.
            /// </summary>
            DeauthorizeAgentFailedPostCreateValidationCheck = 13
        }

        /// <summary>
        /// Generate and action result token.
        /// </summary>
        /// <param name="code"><see cref="AgentServiceActionResultDomainIdCode"/> associated with the token.</param>
        /// <param name="message">User friendly message.</param>
        /// <param name="pluginId">Unique identifier for the plugin that the result was generated for.</param>
        /// <returns>Result token.</returns>
        public static ActionResultToken GenerateActionResultToken(
            AgentServiceActionResultDomainIdCode code,
            string message,
            string pluginId = null)
        {
            return new ActionResultToken
            {
                Id = pluginId,
                ResultDomain = (int) DomainValue.DomainAgentPluginService,
                ResultDomainId = (int) code,
                Message = message
            };
        }

        /// <summary>
        /// Turn a result code into a human readable message.
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static string ResultCodeToString(AgentServiceActionResultDomainIdCode code)
        {
            switch (code)
            {
                case AgentServiceActionResultDomainIdCode.AgentUpdatesFetchFailed:
                    return "failed to retrieve updates from the update server.";
                case AgentServiceActionResultDomainIdCode.AgentUpdatesListFailed:
                    return "failed to enumerate updates from the local repository.";
                case AgentServiceActionResultDomainIdCode.EnableAgentFailed:
                    return "failed to enable the agent.";
                case AgentServiceActionResultDomainIdCode.InstallAgentFailed:
                    return "failed to install the agent.";
                case AgentServiceActionResultDomainIdCode.InstalledAgentsGetFailed:
                    return "failed to enumerate a list of installed agents from the local repository.";
                case AgentServiceActionResultDomainIdCode.ReloadAllAgentsFailed:
                    return "failed to reload all agents into their respective AppDomains.";
                case AgentServiceActionResultDomainIdCode.RemoveAgentFailed:
                    return "failed to remove the agent.";
                case AgentServiceActionResultDomainIdCode.Success:
                    return "the operation was completed successfully.";
                case AgentServiceActionResultDomainIdCode.UnloadAgentFailed:
                    return "failed to unload the agent.";
                case AgentServiceActionResultDomainIdCode.UpdateAgentFailed:
                    return "failed to update the agent.";
                case AgentServiceActionResultDomainIdCode.UnknownFailure:
                    return "an unknown failure as occured.  Check event logs on the server for more information.";
                case AgentServiceActionResultDomainIdCode.AgentDetailsGetFailed:
                    return "failed to fetch details for the agent.";
                case AgentServiceActionResultDomainIdCode.AuthorizeAgentFailedNoParentKey:
                    return "The authorization operation failed because the AuthorizedAgentPlugins control parameter group wasn't present in the data layer.";
                default:
                    return "an unknown response code whas thrown.";

            }
        }
    }
}
