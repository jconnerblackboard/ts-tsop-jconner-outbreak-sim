﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BbTS.Domain.Models.Definitions.Customer;
using BbTS.Domain.Models.Definitions.Terminal;
using BbTS.Domain.Models.System;

namespace BbTS.Domain.Models.Definitions.Agent
{
    public class UpdateDefinitions
    {
        public enum UpdateServiceResultCodes
        {
            Success = 0,
            DownloadError = 1,
            ListError = 2,
            UploadError = 3,
            UploadErrorUnsupportedMediaType = 4,
            UploadFileStoreError = 5
        }

        /// <summary>
        /// Dictionary containing a mapping for DomainTerminal result domain to internal ActionResult codes
        /// </summary>
        private static readonly Dictionary<UpdateServiceResultCodes, ActionResultToken> ActionResultCodeMessages =
            new Dictionary<UpdateServiceResultCodes, ActionResultToken>
            {
                {
                    UpdateServiceResultCodes.Success, new ActionResultToken
                    {
                        ResultDomain = (int) DomainValue.DomainAgentUpdateService,
                        ResultDomainId = (int) UpdateServiceResultCodes.Success,
                        Message = "Success."
                    }
                },
                {
                    UpdateServiceResultCodes.DownloadError, new ActionResultToken
                    {
                        ResultDomain = (int) DomainValue.DomainAgentUpdateService,
                        ResultDomainId = (int) UpdateServiceResultCodes.DownloadError,
                        Message = "Download update request failed."
                    }
                },
                {
                    UpdateServiceResultCodes.ListError, new ActionResultToken
                    {
                        ResultDomain = (int) DomainValue.DomainAgentUpdateService,
                        ResultDomainId = (int) UpdateServiceResultCodes.ListError,
                        Message = "List updates request failed."
                    }
                },
                {
                    UpdateServiceResultCodes.UploadError, new ActionResultToken
                    {
                        ResultDomain = (int) DomainValue.DomainAgentUpdateService,
                        ResultDomainId = (int) UpdateServiceResultCodes.UploadError,
                        Message = "Upload upload request failed."
                    }
                },
                {
                    UpdateServiceResultCodes.UploadErrorUnsupportedMediaType, new ActionResultToken
                    {
                        ResultDomain = (int) DomainValue.DomainAgentUpdateService,
                        ResultDomainId = (int) UpdateServiceResultCodes.UploadErrorUnsupportedMediaType,
                        Message = "Upload upload request failed because the caller passed an unsupported media type (HTTP Code 415)."
                    }
                },
                {
                    UpdateServiceResultCodes.UploadFileStoreError, new ActionResultToken
                    {
                        ResultDomain = (int) DomainValue.DomainAgentUpdateService,
                        ResultDomainId = (int) UpdateServiceResultCodes.UploadFileStoreError,
                        Message = "Upload failed to store the file."
                    }
                }
            };

        /// <summary>
        /// Get the ActionResultToken for the UpdateService DomainId code
        /// </summary>
        /// <param name="code">UpdateServiceResultCodes code</param>
        /// <param name="additionalInformation">additional message information</param>
        /// <returns></returns>
        public static ActionResultToken GenerateActionResultToken(
            UpdateServiceResultCodes code,
            string additionalInformation = null)
        {
            if (!ActionResultCodeMessages.ContainsKey(code)) return null;
            var token = ActionResultCodeMessages[code];
            if (!String.IsNullOrEmpty(additionalInformation))
            {
                token.Message = $"{token.Message}  {additionalInformation}";
            }
            return ActionResultCodeMessages[code];
        }
    }
}
