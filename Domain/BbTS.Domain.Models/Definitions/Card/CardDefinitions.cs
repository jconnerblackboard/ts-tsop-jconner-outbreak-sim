﻿
namespace BbTS.Domain.Models.Definitions.Card
{
    /// <summary>
    ///     Enumeration of Card Status Type
    /// </summary>
    public enum CardStatusType
    {
        /// <summary>
        /// The Unknown
        /// </summary>
        Unknown = -1,

        /// <summary>
        /// The active
        /// </summary>
        Active = 0,

        /// <summary>
        /// The frozen
        /// </summary>
        Frozen = 1,

        /// <summary>
        /// The retired
        /// </summary>
        Retired = 2,

        /// <summary>
        /// The customer deleted
        /// </summary>
        CustomerDeleted = 3,

        /// <summary>
        /// The customer deleted
        /// </summary>
        // ReSharper disable once InconsistentNaming
        //Management API expects and accepts this value.
        Customer_Deleted = CustomerDeleted
    }

    /// <summary>
    ///     Enumeration of Card Type
    /// </summary>
    public enum CardType
    {
        /// <summary>
        /// The unknown
        /// </summary>
        Unknown = -1,

        /// <summary>
        /// The standard
        /// </summary>
        Standard = 0,

        /// <summary>
        /// The temporary
        /// </summary>
        Temporary = 1,

        /// <summary>
        /// The mobile
        /// </summary>
        Mobile = 2,

        /// <summary>
        /// Assa Abloy - Wake up
        /// </summary>
        WakeUp = 3
    }

    /// <summary>
    /// Card format start type
    /// </summary>
    public enum CardFormatStartType
    {
        /// <summary>
        /// Absolute position from the first digit on track 2
        /// </summary>
        AbsoluteFromFirstDigit = 0,

        /// <summary>
        /// Relative positioning from the first field separator on track 2
        /// </summary>
        RelativeFromFirstSeparator = 1
    }
}
