﻿using BbTS.Domain.Models.Exceptions.Notification;
using System;
using System.Collections.Generic;

namespace BbTS.Domain.Models.Definitions.Notification
{
    /// <summary>
    /// Enumeration for the types of notification states.
    /// </summary>
    public enum NotificationStatusType
    {
        /// <summary>
        /// Notification is still pending.
        /// </summary>
        Pending = 0,
        /// <summary>
        /// The notification was successfully processed.
        /// </summary>
        Complete = 1,
        /// <summary>
        /// An error occured during notificaiton processing.
        /// </summary>
        Error = 2
    }

    /// <summary>
    /// Enumeration of notification event types.
    /// </summary>
    public enum NotificationEventType
    {
        /// <summary>
        /// Object was created.
        /// </summary>
        Created = 0,
        /// <summary>
        /// Object was updated.
        /// </summary>
        Updated = 1,
        /// <summary>
        /// Object was deleted.
        /// </summary>
        Deleted = 2,
        /// <summary>
        /// Door access was assigned.
        /// </summary>
        Assigned = 3,
        /// <summary>
        /// Door access was revoked.
        /// </summary>
        Revoked = 4
    }

    /// <summary>
    /// Enumeration of notification event types.
    /// </summary>
    public enum NotificationObjectType
    {
        /// <summary>
        /// Not applicable to this object.
        /// </summary>
        NotApplicable = -1,
        /// <summary>
        /// Event is a heartbeat operation.
        /// </summary>
        Heartbeat = 0,
        /// <summary>
        /// Event is related to a customer object.
        /// </summary>
        Customer = 1,
        /// <summary>
        /// Event is related to a card object.
        /// </summary>
        Card = 2,
        /// <summary>
        /// Event is related to a pin object.
        /// </summary>
        CustomerPinHash = 3,
        /// <summary>
        /// Event is related to a photo object.
        /// </summary>
        CustomerImage = 4,
        /// <summary>
        /// Event is related to a stored value object.
        /// </summary>
        StoredValueTransaction = 5,
        /// <summary>
        /// Event is related to an access object.
        /// </summary>
        Access = 6,
        /// <summary>
        /// Event is related to a customer extension object.
        /// </summary>
        CustomerExtensions = 7,
        /// <summary>
        /// Event is related to a credential metric object.
        /// </summary>
        ReportingMetrics = 51

    }

    /// <summary>
    /// Enumeration of notificaiton processing states.
    /// </summary>
    public enum NotificationProcessingState
    {
        /// <summary>
        /// No processing is currently running or has been recently run.
        /// </summary>
        Idle = 0,
        /// <summary>
        /// Processing is currently running.
        /// </summary>
        InProcess = 1,
        /// <summary>
        /// Processing has recently completed and was successful.
        /// </summary>
        Complete = 2,
        /// <summary>
        /// Processing has been recently run, but resulted in an error state.
        /// </summary>
        Error = 3
    }

    /// <summary>
    /// Enumeration of possible results from uploading a notification to the event hub.
    /// </summary>
    public enum EventHubNotificationUploadResult
    {
        /// <summary>
        /// The result is unknown.
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// The operation was successful.
        /// </summary>
        Success = 1,
        /// <summary>
        /// The operation was a failure.
        /// </summary>
        Failure = 2,
        /// <summary>
        /// Image processing was aborted.
        /// </summary>
        Aborted = 3
    }

    /// <summary>
    /// Enumeration of processing functions.
    /// </summary>
    public enum NotificationProcessingFunction
    {
        /// <summary>
        /// Processing set is the root processing set.
        /// </summary>
        Root,
        /// <summary>
        /// Processing set is a heartbeat operation.
        /// </summary>
        Heartbeat,
        /// <summary>
        /// Processing set is related to a customer object.
        /// </summary>
        Customer,
        /// <summary>
        /// Processing set is related to a card object.
        /// </summary>
        Card,
        /// <summary>
        /// Processing set is related to a pin object.
        /// </summary>
        CustomerPinHash,
        /// <summary>
        /// Processing set is related to a photo object.
        /// </summary>
        CustomerImage,
        /// <summary>
        /// Processing set is related to a stored value object.
        /// </summary>
        StoredValueTransaction,
        /// <summary>
        /// Processing set is related to a set of notification metrics.
        /// </summary>
        ReportingMetrics,
        /// <summary>
        /// Processing set is related to an access object.
        /// </summary>
        Access,
        /// <summary>
        /// Processing set is related to a customer extension object.
        /// </summary>
        CustomerExtension

    }

    /// <summary>
    /// Enumeration of hashing algorithms used to create a PIN hash for an event hub notification.
    /// </summary>
    public enum NotificationPinAlgorithmIdentifiers
    {
        SHA256 = 0
    }

    /// <summary>
    /// Image types for the event hub.
    /// </summary>
    public enum EventHubImageType
    {
        Photo,
        Thumbnail
    }

    /// <summary>
    ///     Enumeration of Card Status Type as they relate to the event notificaiton system.
    /// </summary>
    public enum NotificationCardStatus
    {
        /// <summary>
        /// Unknown card status.
        /// </summary>
        Unknown = -99999,

        /// <summary>
        /// The status of the card has no issues
        /// </summary>
        Active = 0,

        /// <summary>
        /// The status of the card is suspended (e.g. lost, frozen or outside of effectivity dates).
        /// </summary>
        Suspended = 1,

        /// <summary>
        /// The card has been retired.  This is a terminal state.
        /// </summary>
        Retired = 2
    }

    /// <summary>
    ///     Enumeration of Card Types as they relate to the event notificaiton system.
    /// </summary>
    public enum NotificationCardType
    {
        /// <summary>
        /// Unknown card type.
        /// </summary>
        Unknown = -99999,

        /// <summary>
        /// The standard
        /// </summary>
        Standard = 0,

        /// <summary>
        /// The temporary
        /// </summary>
        Temporary = 1,

        /// <summary>
        /// The mobile
        /// </summary>
        Mobile = 2,

        /// <summary>
        /// A persona wake-up card.
        /// </summary>
        WakeUp = 3
    }

    /// <summary>
    /// Types of stored value transaction for notification messages.
    /// </summary>
    public enum NotificationSvTransactionType
    {
        /// <summary>
        /// Purchase
        /// </summary>
        Purchase = 0,
        /// <summary>
        /// Refund for a purchase
        /// </summary>
        PurchaseRefund = 1,
        /// <summary>
        /// Deposit.
        /// </summary>
        Deposit = 2,
        /// <summary>
        /// Refund on a deposit.
        /// </summary>
        DepositRefund = 3,
        /// <summary>
        /// Enrichment.
        /// </summary>
        Enrichment = 4
    }

    /// <summary>
    /// Enumeration of stored value transaction status for notification objects.
    /// </summary>
    public enum NotificationSvTransactionStatus
    {
        /// <summary>
        /// Transaction was approved.
        /// </summary>
        Approved = 0,
        /// <summary>
        /// Transaction was declined.
        /// </summary>
        Declined = 1
    }

    /// <summary>
    /// Enumeration of currency codes used by the notification system.
    /// </summary>
    public enum NotificationCurrencyCodes
    {
        /// <summary>
        /// Canadian dollar.
        /// </summary>
        CAD,
        /// <summary>
        /// US Dollar.
        /// </summary>
        USD,
        /// <summary>
        /// Australian Dollar.
        /// </summary>
        AUD,
        /// <summary>
        /// New Zealand Dollar.
        /// </summary>
        NZD
    }

    /// <summary>
    /// Enumeration of connection string locations.
    /// </summary>
    public enum AzureTableStorageConnectionLocation
    {
        /// <summary>
        /// Unknown or undefined.
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// Use the developer connection string.
        /// </summary>
        Development = 1,
        /// <summary>
        /// Use the production connection string.
        /// </summary>
        Qa = 2,
        /// <summary>
        /// Use the production connection string.
        /// </summary>
        Production = 3,
        /// <summary>
        /// Use the UAT connection string.
        /// </summary>
        Uat = 4,
        /// <summary>
        /// The australian production connection string.
        /// </summary>
        ProductionAus = 5,
        /// <summary>
        /// The unit test environment.
        /// </summary>
        UnitTest = 999999
    }

    /// <summary>
    /// Enumeration of reporting metrics report generation frequency.
    /// </summary>
    public enum ReportingMetricsFrequency
    {
        /// <summary>
        /// Unknown
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Daily
        /// </summary>
        D = 1,

        /// <summary>
        /// Weekly
        /// </summary>
        W = 2,

        /// <summary>
        /// Monthly
        /// </summary>
        M = 3,

        /// <summary>
        /// Since Launch.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        SL = 4
    }

    /// <summary>
    /// Enumeration of reporting metrics time slice values.
    /// </summary>
    public enum ReportingMetricsTimeSlice
    {
        /// <summary>
        /// Unknown
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// For the corresponding Frequency period
        /// </summary>
        F = 1,
        /// <summary>
        /// Since Launch till end of the Day/Week/Month
        /// </summary>
        SL = 2
    }

    /// <summary>
    /// Enumeration of reporting metrics environments.
    /// </summary>
    public enum ReportingMetricsEnvironment
    {
        /// <summary>
        /// Unknown
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// QA
        /// </summary>
        Q = 1,
        /// <summary>
        /// Stage
        /// </summary>
        S = 2,
        /// <summary>
        /// Period
        /// </summary>
        P = 3,
        /// <summary>
        /// Developer.
        /// </summary>
        D = 4
    }

    /// <summary>
    /// Enumeration of reporting metrics validation results.
    /// </summary>
    public enum ReportingMetricsValidationResult
    {
        /// <summary>
        /// Result is unknown.
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// Validation was successful.
        /// </summary>
        Success = 1,
        /// <summary>
        /// The Id was included in the report metrics, but wasn't present in the master list.  The ID should be ignored and not included in the final report.
        /// </summary>
        IgnoredNotInMaster = 2,
        /// <summary>
        /// Validation failed because there were ids in the master that were not the metrics report.  Institution failed to report metrics.
        /// </summary>
        FailNotInMetrics = 3,
        /// <summary>
        /// Validation failed because there are no registered institutions.
        /// </summary>
        FailNoRegisteredInstitutions = 4,
        /// <summary>
        /// Validation failed because total counts for various metrics don't match the sum of individual metrics.
        /// </summary>
        FailMetricsCountMismatch = 5,
        /// <summary>
        /// An error occured during the operation.
        /// </summary>
        Error = 999999
    }

    /// <summary>
    /// Enumeration of reporting metrics processing results.
    /// </summary>
    public enum ReportingMetricsProcessingResult
    {
        /// <summary>
        /// Result is unknown.
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// Processing was successful.
        /// </summary>
        Success = 1,
        /// <summary>
        /// Processing failed for an unknown reason.
        /// </summary>
        FailUnknown = 2,
        /// <summary>
        /// Processing failed because institution validation failed.  Additional codes available.
        /// </summary>
        FailValidation = 3,
        /// <summary>
        /// Processing failed becasue the report couldn't not be transfered to the SFTP server.
        /// </summary>
        FailSftpTransfer = 4,
        /// <summary>
        /// Process was skipped because the operation is outside the processing window.
        /// </summary>
        SkippedOutsideProcessingWindow = 5,
        /// <summary>
        /// An error occured during the operation.
        /// </summary>
        Error = 999999
    }
    
    /// <summary>
    /// Enumeration of the results of a reporting metrics SFTP upload operation.
    /// </summary>
    public enum ReportingMetricsSftpUploadResult
    {
        /// <summary>
        /// Result is unknown.
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// Processing was successful.
        /// </summary>
        Success = 1,
        /// <summary>
        /// An error occured during the operation.
        /// </summary>
        Error = 999999
    }

    /// <summary>
    /// Enumeration of azure table storage table names
    /// </summary>
    public enum AzureTableStorageTableNames
    {
        /// <summary>
        /// The unknown table (default state)
        /// </summary>
        Unknown,
        /// <summary>
        /// The name of the table in azure table storage where reporting metrics are kept.
        /// </summary>
        ReportingMetrics,
        /// <summary>
        /// The name of the table in azure table storage where institution id/name mappings are kept.
        /// </summary>
        InstitutionName,
        /// <summary>
        /// The name of the table in azure table storage where an account of the results of processing logs are kept.
        /// </summary>
        ProcessingLog,
        /// <summary>
        /// Unit test version of the reporting metrics table
        /// </summary>
        ReportingMetricsTest,
        /// <summary>
        /// Table containing information related to the metrics processing agent about when (and in what environment) it kicks off a job.
        /// </summary>
        ProcessingJobStartLog,
        /// <summary>
        /// Table containing the ruleset used to validate reporting metric counts.
        /// </summary>
        MetricsRules,
        /// <summary>
        /// The table containing the metrics processing start time for an institution.
        /// </summary>
        MetricsProcessingStartTime
    }

    /// <summary>
    /// Enumeration of possible results from uploading a notification to the event hub.
    /// </summary>
    public enum NotificationStatusSetResult
    {
        /// <summary>
        /// The result is unknown.
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// The operation was successful.
        /// </summary>
        Success = 1,
        /// <summary>
        /// The operation was a failure.
        /// </summary>
        Failure = 2
    }

    /// <summary>
    /// Switch for which email notification group to use.
    /// </summary>
    public enum EmailReportingGroup
    {
        /// <summary>
        /// Not known.
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// Email addresses listed in registry key 'ReportingMetricsEmailGroupA'.
        /// VIPs and "need-to-knows".
        /// Example(s): Apple
        /// </summary>
        ReportingMetricsEmailGroupA = 1,
        /// <summary>
        /// Email addresses listed in registry key 'ReportingMetricsEmailGroupB'.
        /// Interested or vested parties.
        /// Example(s): Blackboard support personel, Blackboard developers, Blackboard PM
        /// </summary>
        ReportingMetricsEmailGroupB = 2,
        /// <summary>
        /// Email addresses listed in registry key 'ReportingMetricsEmailGroupC'.
        /// Selected developers (largely used for debugging).
        /// Example(s): Developer(s) actively working on code changes, maintenance, or out of band report generation.
        /// </summary>
        ReportingMetricsEmailGroupC = 3
    }

    /// <summary>
    /// Enumeration of processing job start log status values.
    /// </summary>
    public enum ProcessingJobStartLogStatus
    {
        /// <summary>
        /// Unknown.
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// Processing in in progress.
        /// </summary>
        InProgress = 1,
        /// <summary>
        /// Processing completed and was successful.
        /// </summary>
        Success = 2,
        /// <summary>
        /// Processing failed.a
        /// </summary>
        Failed = 3
    }

    /// <summary>
    /// 
    /// </summary>
    public enum MetricRulesCountCategories
    {
        /// <summary>
        /// Unknown
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// TransactionCount
        /// </summary>
        TransactionCount = 1,
        /// <summary>
        /// TransactionValue
        /// </summary>
        TransactionValue = 2,
        /// <summary>
        /// UserCount
        /// </summary>
        UserCount = 3,
        /// <summary>
        /// DpanCount
        /// </summary>
        DpanCount = 4,
        /// <summary>
        /// DeviceCount
        /// </summary>
        DeviceCount = 5
    }

    /// <summary>
    /// 
    /// </summary>
    public enum CustomerExtensionValueTypes
    {
        String = 0,
        Numeric = 1,
        Decimal = 2,
        Boolean = 5
    }

    /// <summary>
    /// Tool class for manageing notification definitions
    /// </summary>
    public class NotificationDefinitions
    {
        /// <summary>
        /// Mapping of <see cref="EventHubImageType"/> to their string appendix value.
        /// </summary>
        public static Dictionary<EventHubImageType, string> EventHubImageTypeToString { get; } = new Dictionary<EventHubImageType, string>
        {
            {EventHubImageType.Photo, "P" },
            {EventHubImageType.Thumbnail, "T" }
        };

        /// <summary>
        /// Get the azure table storage connection string location based upon the BbSP Host configured in the registry.
        /// </summary>
        /// <param name="bbSpHost">The host string.</param>
        /// <returns></returns>
        public static AzureTableStorageConnectionLocation AzureTableStorageConnectionLocationGet(string bbSpHost)
        {
            if (bbSpHost.Contains("BbSPcloudserviceeast.blackboard.com"))
            {
                return AzureTableStorageConnectionLocation.Production;
            }

            if (bbSpHost.Contains("prodausbbspservice.blackboard.com"))
            {
                return AzureTableStorageConnectionLocation.ProductionAus;
            }

            if (bbSpHost.Contains("OpsBbSPService.transactsp.net"))
            {
                return AzureTableStorageConnectionLocation.Uat;
            }

            if (bbSpHost.Contains("bbsp-qa1.campuscloud.io"))
            {
                return AzureTableStorageConnectionLocation.Qa;
            }

            if (bbSpHost.ToLower() == "unittest")
            {
                return AzureTableStorageConnectionLocation.UnitTest;
            }

            return AzureTableStorageConnectionLocation.Development;
        }

        /// <summary>
        /// Get the reporting metrics table name based on the environment selected.
        /// </summary>
        /// <param name="bbSpHost"></param>
        /// <returns></returns>
        public static AzureTableStorageTableNames ReportingMetricsTableNameGet(string bbSpHost)
        {
            return AzureTableStorageConnectionLocationGet(bbSpHost) == AzureTableStorageConnectionLocation.UnitTest ?
                AzureTableStorageTableNames.ReportingMetricsTest :
                AzureTableStorageTableNames.ReportingMetrics;
        }

        /// <summary>
        /// Get the reporting metrics environment.
        /// </summary>
        /// <param name="bbSpHost">The host environment string</param>
        /// <returns></returns>
        public static ReportingMetricsEnvironment ReportingMetricsEnvironmentGet(string bbSpHost)
        {
            var value = AzureTableStorageConnectionLocationGet(bbSpHost);
            switch (value)
            {
                case AzureTableStorageConnectionLocation.Production:
                case AzureTableStorageConnectionLocation.ProductionAus:
                    return ReportingMetricsEnvironment.P;
                case AzureTableStorageConnectionLocation.Uat:
                    return ReportingMetricsEnvironment.S;
                case AzureTableStorageConnectionLocation.Development:
                    return ReportingMetricsEnvironment.D; 
                case AzureTableStorageConnectionLocation.Qa:
                    return ReportingMetricsEnvironment.Q;
                case AzureTableStorageConnectionLocation.UnitTest:
                    return ReportingMetricsEnvironment.D;
                default:
                    throw new NotSupportedException($"Enumeration value '{value}' is not a supported environment value.");

            }
        }

        /// <summary>
        /// Get the reporting metrics environment.
        /// </summary>
        /// <param name="environment">The host environment</param>
        /// <returns></returns>
        public static ReportingMetricsEnvironment ReportingMetricsEnvironmentGet(AzureTableStorageConnectionLocation environment)
        {
            switch (environment)
            {
                case AzureTableStorageConnectionLocation.Production:
                case AzureTableStorageConnectionLocation.ProductionAus:
                    return ReportingMetricsEnvironment.P;
                case AzureTableStorageConnectionLocation.Uat:
                    return ReportingMetricsEnvironment.S;
                case AzureTableStorageConnectionLocation.Development:
                case AzureTableStorageConnectionLocation.UnitTest:
                    return ReportingMetricsEnvironment.D;
                case AzureTableStorageConnectionLocation.Qa:
                    return ReportingMetricsEnvironment.Q;
                default:
                    throw new NotSupportedException($"Enumeration value '{environment}' is not a supported environment value.");
            }
        }

        /// <summary>
        /// Convert the frequency value to a human readable string.
        /// </summary>
        /// <param name="frequency">The value.</param>
        /// <returns>The string.</returns>
        public static string ReportingMetricsFrequencyParse(ReportingMetricsFrequency frequency)
        {
            switch (frequency)
            {
                case ReportingMetricsFrequency.D: return "Daily";
                case ReportingMetricsFrequency.M: return "Monthly";
                case ReportingMetricsFrequency.W: return "Weekly";
                case ReportingMetricsFrequency.SL: return "SinceLaunch";
                default: return "Unknown";
            }
        }

        /// <summary>
        /// Utility method to get the current version of the notification object type.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string NotificationObjectTypeVersionGet(NotificationObjectType type)
        {
            switch (type)
            {
                case NotificationObjectType.Access: return "v1.0";
                case NotificationObjectType.Card: return "v1.1";
                case NotificationObjectType.Customer: return "v1.0";
                case NotificationObjectType.CustomerExtensions: return "v1.0";
                case NotificationObjectType.CustomerImage: return "v1.0";
                case NotificationObjectType.CustomerPinHash: return "v1.0";
                case NotificationObjectType.Heartbeat: return "v1.0";
                case NotificationObjectType.ReportingMetrics: return "v1.0";
                case NotificationObjectType.StoredValueTransaction: return "v1.0";
                case NotificationObjectType.NotApplicable: return "v1.0";
                default:
                    throw new NotificationObjectTypeVersionNotFoundException($"'{type.ToString()}' NotificationObjectType version not supported.");
            }

        }
    }
}
