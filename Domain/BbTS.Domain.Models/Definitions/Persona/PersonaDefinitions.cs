﻿using System;
using System.Collections.Generic;
using BbTS.Domain.Models.System;

namespace BbTS.Domain.Models.Definitions.Persona
{
    #region Enums

    // ReSharper disable InconsistentNaming

    public enum DsrAccessPointType
    {
        SxPx_Batteries = 0,
        SxPx_Powered = 1,
        Undefined = -1
    }

    public enum DsrAccessPointModeType
    {
        FIRST_PERSON_THROUGH = 1,
        UNLOCK = 2,
        ACCESSMODE_PRIMARY = 3,
        ACCESSMODE_PRIMARY_THEN_SECONDARY = 4,
        ACCESSMODE_PRIMARY_OR_SECONDARY = 5
    }

    public enum DsrAuthorizationType
    {
        ACCESS = 1,
        WAKEUP = 2,
        ACCESS_OVERRIDE_DEADBOLT = 3,
        UAPM = 4
    }

    public enum DsrUserFlags
    {
        EXTENDED,
        SUSPENDED
    }

    public enum DsrSyncServiceLockControlRequestCode
    {
        Lock,
        PanicOn,
        PanicOff,
        Pulse,
        Unlock
    }

    public enum IntervalOperationRunState
    {
        Stopped,
        Starting,
        Stopping,
        Sleeping,
        Running
    }

    public enum PersonaActionResultDomainIdCode
    {
        Success = 0,
        CommunicationError = 1,
        BusinessLogicError = 2,
        GeneralException = 3,
        SyncRequestFailed = 4,
        ClearDsrRequestFailed = 5,
        SessionTokenExpired = 10
    }

    public enum PersonaWeekday
    {
        SUNDAY = 1,
        MONDAY = 2,
        TUESDAY = 3,
        WEDNESDAY = 4,
        THURSDAY = 5,
        FRIDAY = 6,
        SATURDAY = 7
    }

    public enum DsrHardwareSettingDataType
    {
        STRING = 1,
        BOOL = 2,
        U8 = 3,
        U16 = 4,
        U32 = 5
    }

    public enum DsrAlarmConfigValue
    {
        ValidIn = 1,
        DeniedAccess = 2,
        Secured = 3,
        DoorForced = 4,
        KeyOverride = 5,
        InvalidEntry = 6,
        DoorAjar = 7,
        LowBattery = 8,
        RxHeld = 9
    }

    // ReSharper restore InconsistentNaming

    #endregion

    /// <summary>
    /// Class
    /// </summary>
    public static class PersonaDefinitions
    {

        /// <summary>
        /// Dictionary containing a mapping for DomainTerminal result domain to internal ActionResult codes
        /// </summary>
        private static readonly Dictionary<PersonaActionResultDomainIdCode, ActionResultToken> PersonaActionResultCodeMessages =
            new Dictionary<PersonaActionResultDomainIdCode, ActionResultToken>
            {
                {
                    PersonaActionResultDomainIdCode.Success, new ActionResultToken
                    {
                        ResultDomain = (int)DomainValue.DomainPersona,
                        ResultDomainId = (int)PersonaActionResultDomainIdCode.Success,
                        Message = "Success."
                    }
                },
                {
                    PersonaActionResultDomainIdCode.CommunicationError, new ActionResultToken
                    {
                        ResultDomain = (int)DomainValue.DomainPersona,
                        ResultDomainId = (int)PersonaActionResultDomainIdCode.CommunicationError,
                        Message = "Communication error with the DSR Sync Service."
                    }
                },
                {
                    PersonaActionResultDomainIdCode.BusinessLogicError, new ActionResultToken
                    {
                        ResultDomain = (int)DomainValue.DomainPersona,
                        ResultDomainId = (int)PersonaActionResultDomainIdCode.BusinessLogicError,
                        Message = "Business Log error occured from within the DSR Sync Service."
                    }
                },
                {
                    PersonaActionResultDomainIdCode.GeneralException, new ActionResultToken
                    {
                        ResultDomain = (int)DomainValue.DomainPersona,
                        ResultDomainId = (int)PersonaActionResultDomainIdCode.GeneralException,
                        Message = "A general exception occured while trying to contact the DSR Sync Service."
                    }
                },
                {
                    PersonaActionResultDomainIdCode.SyncRequestFailed, new ActionResultToken
                    {
                        ResultDomain = (int)DomainValue.DomainPersona,
                        ResultDomainId = (int)PersonaActionResultDomainIdCode.SyncRequestFailed,
                        Message = "The sync request has failed."
                    }
                },
                {
                    PersonaActionResultDomainIdCode.ClearDsrRequestFailed, new ActionResultToken
                    {
                        ResultDomain = (int)DomainValue.DomainPersona,
                        ResultDomainId = (int)PersonaActionResultDomainIdCode.SyncRequestFailed,
                        Message = "The clear dsr request has failed."
                    }
                },
                {
                    PersonaActionResultDomainIdCode.SessionTokenExpired, new ActionResultToken
                    {
                        ResultDomain = (int)DomainValue.DomainPersona,
                        ResultDomainId = (int)PersonaActionResultDomainIdCode.SessionTokenExpired,
                        Message = "Session token has expired.  Please log in again."
                    }
                }
            };

        /// <summary>
        /// Get the ActionResultToken for the Terminal DomainId code
        /// </summary>
        /// <param name="code">TerminalActionResultDomainIdCode code</param>
        /// <param name="message">Additional message to tack on to the end of the code message.</param>
        /// <returns></returns>
        public static ActionResultToken GetActionResultCode(PersonaActionResultDomainIdCode code, string message = "")
        {
            if (!PersonaActionResultCodeMessages.ContainsKey(code)) return null;
            var token = PersonaActionResultCodeMessages[code].Clone();
            token.Message = String.Format("{0}{1}", token.Message, String.IsNullOrEmpty(message) ? "" : String.Format(". {0}", message));
            return token;
        }

        /// <summary>
        /// Check whether or not a give IntervalOperationRunState object is "started"
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        public static bool IntervalOperationRunStateIsStarted(IntervalOperationRunState state)
        {
            return
                state == IntervalOperationRunState.Running ||
                state == IntervalOperationRunState.Sleeping;
        }
    }
}
