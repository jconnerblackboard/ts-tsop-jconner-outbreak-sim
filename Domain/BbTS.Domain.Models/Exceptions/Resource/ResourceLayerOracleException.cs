﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Exceptions.Resource
{
    /// <summary>
    /// Exception class for an oracle exception thrown in the resource layer.  This class was created to avoid a need for another project to have to reference Oracle libraries.
    /// </summary>
    public class ResourceLayerOracleException : Exception
    {
        /// <summary>
        /// The ORA error thrown by oracle.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// The inner exception ora error code.
        /// </summary>
        public int? InnerExceptionErrorCode { get; set; }

        /// <summary>
        /// Parameterized constructor
        /// </summary>
        /// <param name="message">The exception text.</param>
        /// <param name="errorCode">The ORA error thrown by oracle.</param>
        /// <param name="innerErrorCode">The inner exception ora error code.</param>
        public ResourceLayerOracleException(string message, int errorCode, int? innerErrorCode = null) : base(message)
        {
            ErrorCode = errorCode;
            InnerExceptionErrorCode = innerErrorCode;
        }
    }
}
