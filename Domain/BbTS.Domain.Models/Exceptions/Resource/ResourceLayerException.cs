﻿using System;
using BbTS.Domain.Models.Definitions.Logging;

namespace BbTS.Domain.Models.Exceptions.Resource
{
    /// <summary>
    /// Container class for an exception triggered in the resource layer.
    /// </summary>
    public class ResourceLayerException : Exception
    {
        /// <summary>
        /// Request Id that triggered the request.
        /// </summary>
        public string RequestId { get; internal set; }

        /// <summary>
        /// Unique identifier for this exception.
        /// </summary>
        public string ReferenceId { get; internal set; }

        /// <summary>
        /// The event category to associate with this exception.
        /// </summary>
        public short EventCategory { get; internal set; }

        /// <summary>
        /// The event id to associate with this exception.
        /// </summary>
        public int EventId { get; internal set; }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="requestId">Request Id that triggered the request.</param>
        /// <param name="message">Exception message</param>
        /// <param name="eventCategory">The event category to associate with this exception.</param>
        /// <param name="eventId">The event Id to associate with this exception.</param>
        public ResourceLayerException(
            string requestId,
            string message,
            LoggingDefinitions.Category eventCategory = LoggingDefinitions.Category.Resource,
            LoggingDefinitions.EventId eventId = LoggingDefinitions.EventId.General) : base(message)
        {
            ReferenceId  = Guid.NewGuid().ToString("D");
            RequestId = requestId;
            EventCategory = (short)eventCategory;
            EventId = (int)eventId;
        }
    }
}
