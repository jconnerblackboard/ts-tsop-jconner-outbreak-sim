﻿using System;

namespace BbTS.Domain.Models.Exceptions.EventHub
{
    /// <summary>
    /// Exception class for the situation where an image waiting to be sent to the event hub failed because the size was determined to be too large.
    /// </summary>
    public class EventHubImageSizeTooLargeException : Exception
    {
        /// <summary>
        /// The size of the image in bytes.
        /// </summary>
        public long ImageSize { get; set; }

        /// <summary>
        /// The maximum size of an image in bytes.
        /// </summary>
        public long MaxSize { get; set; }

        /// <summary>
        /// The unique identifier associated with the customer.
        /// </summary>
        public Guid CustomerGuid { get; set; }


        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="imageSize">The size of the image in bytes.</param>
        /// <param name="maxSize">The maximum size of an image in bytes.</param>
        /// <param name="customerGuid">The unique identifier associated with the customer.</param>
        /// <param name="message"></param>
        public EventHubImageSizeTooLargeException(long imageSize, long maxSize, Guid customerGuid, string message = "") : base(message)
        {
            ImageSize = imageSize;
            MaxSize = maxSize;
            CustomerGuid = customerGuid;
        }
    }
}
