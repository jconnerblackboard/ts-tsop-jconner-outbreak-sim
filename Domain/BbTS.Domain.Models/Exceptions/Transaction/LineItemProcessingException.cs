﻿using System;
using BbTS.Domain.Models.Transaction.Processing;

namespace BbTS.Domain.Models.Exceptions.Transaction
{
    /// <summary>
    /// Container class for an exception while processing a transaction line item.
    /// </summary>
    public class LineItemProcessingException : Exception
    {
        /// <summary>
        /// The validation result.
        /// </summary>
        public LineItemProcessingResult LineItemProcessingResult { get; internal set; }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="result">The transaction</param>
        /// <param name="message">The message</param>
        public LineItemProcessingException(LineItemProcessingResult result, string message)
            :base(message)
        {
            LineItemProcessingResult = result;
        }
    }
}
