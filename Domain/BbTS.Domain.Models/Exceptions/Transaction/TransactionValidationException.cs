﻿using System;
using BbTS.Domain.Models.Transaction.Validation;

namespace BbTS.Domain.Models.Exceptions.Transaction
{
    /// <summary>
    /// Container class for an exception while processing a transaction validation operation.
    /// </summary>
    public class TransactionValidationException : Exception
    {
        /// <summary>
        /// The validation result.
        /// </summary>
        public TransactionValidationResult ValidationResult { get; internal set; }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="result">The validation result.</param>
        /// <param name="message">The message.</param>
        public TransactionValidationException(TransactionValidationResult result, string message)
            :base(message)
        {
            ValidationResult = result;
        }
    }
}
