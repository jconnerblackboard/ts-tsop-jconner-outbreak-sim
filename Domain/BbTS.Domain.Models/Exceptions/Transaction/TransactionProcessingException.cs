﻿using System;
using System.Net;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Transaction.Processing;

namespace BbTS.Domain.Models.Exceptions.Transaction
{
    /// <summary>
    /// Container class for an exception while processing a transaction.
    /// </summary>
    public class TransactionProcessingException : Exception
    {
        /// <summary>
        /// The error code associated with the response.  0 means success.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Reason for failure (if there was a failure error code).
        /// </summary>
        public string DeniedText { get; set; }

        /// <summary>
        /// The response to be imbedded in the exception body.
        /// </summary>
        public TransactionProcessingResult Response { get; internal set; }

        /// <summary>
        /// The status code associated with this exception.  Defaults to <see cref="HttpStatusCode.BadRequest"/>
        /// </summary>
        public HttpStatusCode StatusCode { get; set; } = HttpStatusCode.BadRequest;

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="response">The response to be imbedded in the exception body.</param>
        /// <param name="message">The message.</param>
        public TransactionProcessingException(TransactionProcessingResult response, string message = "An error occured while processing the transaction.")
            :base(message)
        {
            Response = response;
            ErrorCode = response.ErrorCode;
            DeniedText = response.DeniedText;
        }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="response">The response to be imbedded in the exception body.</param>
        /// <param name="eventId">The error.</param>
        /// <param name="message">The message.</param>
        public TransactionProcessingException(TransactionProcessingResult response, LoggingDefinitions.EventId eventId, string message = "An error occured while processing the transaction.")
            : base(message)
        {
            response.ErrorSet(eventId);
            Response = response;
            ErrorCode = response.ErrorCode;
            DeniedText = response.DeniedText;
        }
    }
}
