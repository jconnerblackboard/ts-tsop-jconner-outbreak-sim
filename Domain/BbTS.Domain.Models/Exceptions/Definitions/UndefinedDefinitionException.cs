﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Exceptions.Definitions
{
    /// <summary>
    /// Class for throwing custom exception in a definitions class
    /// </summary>
    public class UndefinedDefinitionException : Exception
    {
        /// <summary>
        /// The definition class that is throwing the exception.
        /// </summary>
        [XmlIgnore]
        public string CallingClass { get; internal set; }

        /// <summary>
        /// The type that caused the exception.
        /// </summary>
        [XmlIgnore]
        public string UnsupportedType { get; internal set; }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="message">Exception message</param>
        /// <param name="callingClass">The definition class that is throwing the exception.</param>
        /// <param name="offendingType">The type that caused the exception</param>
        public UndefinedDefinitionException(string message, string callingClass, string offendingType)
            :base(message)
        {
            CallingClass = callingClass;
            UnsupportedType = offendingType;
        }
    }
}
