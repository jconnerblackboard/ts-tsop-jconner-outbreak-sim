﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BbTS.Domain.Models.System.Logging.Tracing;

namespace BbTS.Domain.Models.Exceptions.Tracing
{
    public class TraceParsingException : Exception
    {
        public TraceNode ExceptionNode { get; set; }

        public TraceParsingException(string message) : base(message)
        {
        }
    }
}
