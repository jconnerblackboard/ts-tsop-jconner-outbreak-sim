﻿using System;

namespace BbTS.Domain.Models.Exceptions.Client
{
    [Serializable]
    public class TransactApiBaseUriException : Exception
    {
        public TransactApiBaseUriException(string message) : base(message)
        {
        }
    }
}