﻿using System;

namespace BbTS.Domain.Models.Exceptions.Emv
{
    public class EmvComPortNotFoundException : Exception
    {
        public EmvComPortNotFoundException(string message) : base(message)
        {
        }
    }
}
