﻿namespace BbTS.Domain.Models.Exceptions
{
    /// <summary>
    /// Container class for an exception response to be passed back to a client in a web request error situation.
    /// </summary>
    public class ExceptionResponseToken
    {
        /// <summary>
        /// The reference identifier for this exception.
        /// </summary>
        public string ReferenceId { get; set; }

        /// <summary>
        /// RequestId, GUID, echoed back from corresponding Request.
        /// </summary>
        public string Id;

        /// <summary>
        /// The domain the result originated from.
        /// </summary>
        public int ResultDomain;

        /// <summary>
        /// The sub-domain the result originated from.
        /// </summary>
        public int ResultDomainId;

        /// <summary>
        /// A user friendly message.
        /// </summary>
        public string Message;
    }
}
