﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Exceptions.EAccount
{
    /// <summary>
    /// Exception class related to the customer client (external client id) guid.
    /// </summary>
    public class CustomerRegistrationException : Exception
    {
        /// <summary>
        /// Error code associated with the exception.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Event id associated with the exception.
        /// </summary>
        public int EventId { get; set; }

        /// <summary>
        /// Event Category associated with the exception.
        /// </summary>
        public short EventCategory { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="message">Exception message.</param>
        /// <param name="errorCode">Error code.</param>
        public CustomerRegistrationException(string message, int errorCode) : base(message)
        {
            ErrorCode = errorCode;
        }
    }
}
