﻿using System;

namespace BbTS.Domain.Models.Exceptions.Notification
{
    /// <summary>
    /// Exception class for exposing value parsing errors for customer extensions.
    /// </summary>
    public class CustomerExtensionValueParseFailException : Exception
    {
        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="message"></param>
        public CustomerExtensionValueParseFailException(string message) : base(message)
        {
        }
    }
}
