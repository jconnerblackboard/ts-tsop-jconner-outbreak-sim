﻿using System;

namespace BbTS.Domain.Models.Exceptions.Notification
{
    /// <summary>
    /// Container class for a notificaiton object type version not found exception.
    /// </summary>
    public class NotificationObjectTypeVersionNotFoundException: Exception
    {
        /// <summary>
        /// constructor.
        /// </summary>
        /// <param name="message"></param>
        public NotificationObjectTypeVersionNotFoundException(string message) : base(message)
        {

        }
    }
}
