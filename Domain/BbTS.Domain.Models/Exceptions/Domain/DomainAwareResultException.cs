﻿using System;

namespace BbTS.Domain.Models.Exceptions.Domain
{
    /// <summary>
    /// Exception class for handling errors dealing with domain aware results.
    /// </summary>
    public class DomainAwareResultException : Exception
    {
        /// <summary>
        /// The domain.
        /// </summary>
        public int Domain { get; set; }

        /// <summary>
        /// The domain id.
        /// </summary>
        public int DomainId { get; set; }

        /// <summary>
        /// The domain message
        /// </summary>
        public string DomainMessage { get; set; }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="domain">The domain.</param>
        /// <param name="domainId">The domain id.</param>
        /// <param name="domainMessage">The domain message</param>
        /// <param name="message">Exception message passed to the base.</param>
        public DomainAwareResultException(int domain, int domainId, string domainMessage, string message) : base(message)
        {
            Domain = domain;
            DomainId = domainId;
            DomainMessage = domainMessage;
        }
    }
}
