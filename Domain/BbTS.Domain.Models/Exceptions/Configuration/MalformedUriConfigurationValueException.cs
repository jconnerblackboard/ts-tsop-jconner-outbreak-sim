﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Exceptions.Configuration
{
    /// <summary>
    /// Exception class to be thrown when a URI configuration parameter is incorrect.
    /// </summary>
    public class MalformedUriConfigurationValueException : Exception
    {
        /// <summary>
        /// Parameterized constructor to pass the message to the base class.
        /// </summary>
        /// <param name="message">Exception reason.</param>
        public MalformedUriConfigurationValueException(string message) : base(message)
        {
            
        }
    }
}
