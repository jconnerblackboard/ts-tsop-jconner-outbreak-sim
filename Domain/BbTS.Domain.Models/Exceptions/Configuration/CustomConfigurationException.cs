﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Exceptions.Configuration
{
    /// <summary>
    /// Class for throwing exceptions related to CustomConfiguration operations.
    /// </summary>
    public class CustomConfigurationException : Exception
    {
        /// <summary>
        /// Empty constructor.
        /// </summary>
        public CustomConfigurationException()
        {
        }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="message">message passed to the base.</param>
        public CustomConfigurationException(string message) : base(message)
        {
        }
    }
}
