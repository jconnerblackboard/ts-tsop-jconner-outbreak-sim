﻿using System;

namespace BbTS.Domain.Models.Exceptions
{
    /// <summary>
    /// Exception class for when a board plan verification operation failed to pull sufficient information from the resource layer.
    /// </summary>
    public class BoardPlanVerifyFailedException: Exception
    {
        public BoardPlanVerifyFailedException(string message) : base(message)
        { }
    }
}
