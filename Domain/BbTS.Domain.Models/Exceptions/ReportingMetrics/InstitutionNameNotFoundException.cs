﻿using System;

namespace BbTS.Domain.Models.Exceptions.ReportingMetrics
{
    /// <summary>
    /// Exception class for an error matching an institution id to an institution name
    /// </summary>
    public class InstitutionNameNotFoundException : Exception
    {
        /// <summary>
        /// The unique identifier for the institution.
        /// </summary>
        public Guid InstitutionId { get; set; }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="id">The unique identifier for the institution.</param>
        /// <param name="message">Exception message.</param>
        public InstitutionNameNotFoundException(Guid id, string message) : base(message)
        {
            InstitutionId = id;
        }
    }
}
