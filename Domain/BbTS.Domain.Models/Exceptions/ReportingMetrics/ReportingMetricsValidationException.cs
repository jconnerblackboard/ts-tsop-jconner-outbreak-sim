﻿using System;
using BbTS.Domain.Models.Definitions.Notification;
using BbTS.Domain.Models.Notification;

namespace BbTS.Domain.Models.Exceptions.ReportingMetrics
{
    /// <summary>
    /// Exception class for errors discovered from the result of a reporting metrics validation attempt.
    /// </summary>
    public class ReportingMetricsValidationException : Exception
    {
        /// <summary>
        /// The validation result that spawned this exception.
        /// </summary>
        public NotificationReportingMetricsValidationResponse Response { get; internal set; }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="response">The validation response.</param>
        public ReportingMetricsValidationException(NotificationReportingMetricsValidationResponse response)
        {
            Response = response;
        }
    }
}
