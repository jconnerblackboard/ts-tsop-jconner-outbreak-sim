﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Exceptions.WebApi
{
    public class WebApiExceptionAttributes
    {
        /// <summary>
        /// The ID of the request that caused this error.
        /// </summary>
        public string RequestId { get; internal set; }

        /// <summary>
        /// Unique identifier assigned to the error message.
        /// </summary>
        public string ReferenceId { get; internal set; }

        /// <summary>
        /// The error message text.
        /// </summary>
        public string Message { get; internal set; }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="requestId">The ID of the request that caused this error.</param>
        /// <param name="referenceId">Unique identifier assigned to the error message.</param>
        /// <param name="message">The error message text.</param>
        public WebApiExceptionAttributes(string requestId, string referenceId, string message)
        {
            RequestId = requestId;
            ReferenceId = referenceId;
            Message = message;
        }
    }
}
