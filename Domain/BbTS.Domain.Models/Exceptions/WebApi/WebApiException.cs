﻿using System;
using System.Net;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Transaction;

namespace BbTS.Domain.Models.Exceptions.WebApi
{
    /// <summary>
    /// Exception class thrown when there is a processing error in the BbTS Web API service layer.
    /// </summary>
    public class WebApiException : Exception
    {
        /// <summary>
        /// The ID of the request that caused this error.
        /// </summary>
        public string RequestId { get; internal set; }

        /// <summary>
        /// Unique identifier assigned to the error message.
        /// </summary>
        public string ReferenceId { get; internal set; }

        /// <summary>
        /// The <see cref="HttpStatusCode"/> associated with this exception.
        /// </summary>
        public HttpStatusCode HttpStatusCode { get; internal set; }

        /// <summary>
        /// The error code derived from <see cref="LoggingDefinitions.EventId"/>
        /// </summary>
        public int ErrorCode { get; internal set; }

        /// <summary>
        /// The error message text.
        /// </summary>
        public new string Message { get; internal set; }

        /// <summary>
        /// Parameterized constructor requiring message and request id.
        /// </summary>
        /// <param name="requestId">The ID of the request that caused this error.</param>
        /// <param name="message">The error message text.</param>
        /// <param name="code">The <see cref="HttpStatusCode"/> associated with this exception.</param>
        [Obsolete("Use the overload that requires a ProcessingResult whenever possible in order to preserve ErrorCode information.")]
        public WebApiException(string requestId, string message, HttpStatusCode code) : base(message)
        {
            RequestId = requestId;
            ReferenceId = Guid.NewGuid().ToString("D");
            HttpStatusCode = code;
            ErrorCode = (int)LoggingDefinitions.EventId.Unknown;
            Message = message;
        }

        /// <summary>
        /// Parameterized constructor requiring a processing result.
        /// </summary>
        /// <param name="processingResult"></param>
        /// <param name="code"></param>
        public WebApiException(ProcessingResult processingResult, HttpStatusCode code) : base(processingResult.DeniedText)
        {
            RequestId = processingResult.RequestId;
            ReferenceId = Guid.NewGuid().ToString("D");
            HttpStatusCode = code;
            ErrorCode = processingResult.ErrorCode;
            Message = processingResult.DeniedText;
        }

        /// <summary>
        /// Parameterized constructor requiring a processing result and reference id.
        /// </summary>
        /// <param name="processingResult"></param>
        /// <param name="referenceId"></param>
        /// <param name="code"></param>
        public WebApiException(ProcessingResult processingResult, Guid referenceId, HttpStatusCode code) : base(processingResult.DeniedText)
        {
            RequestId = processingResult.RequestId;
            ReferenceId = referenceId.ToString("D").ToLowerInvariant();
            HttpStatusCode = code;
            ErrorCode = processingResult.ErrorCode;
            Message = processingResult.DeniedText;
        }
    }

    /// <summary>
    /// View for a <see cref="WebApiException"/>.  (Version 1)
    /// </summary>
    public class WebApiExceptionViewV01
    {
        /// <summary>
        /// The ID of the request that caused this error.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// Unique identifier assigned to the error message.
        /// </summary>
        public string ReferenceId { get; set; }

        /// <summary>
        /// The <see cref="HttpStatusCode"/> associated with this exception.
        /// </summary>
        public HttpStatusCode HttpStatusCode { get; set; }

        /// <summary>
        /// The error code derived from <see cref="LoggingDefinitions.EventId"/>
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// The error message text.
        /// </summary>
        public string Message { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="WebApiException"/> conversion.
    /// </summary>
    public static class WebApiExceptionConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="WebApiExceptionViewV01"/> object based on this <see cref="WebApiException"/>.
        /// </summary>
        /// <param name="webApiException"></param>
        /// <returns></returns>
        public static WebApiExceptionViewV01 ToWebApiExceptionViewV01(this WebApiException webApiException)
        {
            if (webApiException == null) return null;

            return new WebApiExceptionViewV01
            {
                RequestId = webApiException.RequestId,
                ReferenceId = webApiException.ReferenceId,
                HttpStatusCode = webApiException.HttpStatusCode,
                ErrorCode = webApiException.ErrorCode,
                Message = webApiException.Message
            };
        }
        
        /// <summary>
        /// Returns a <see cref="WebApiException"/> object based on this <see cref="WebApiExceptionViewV01"/>.
        /// </summary>
        /// <param name="webApiExceptionViewV01"></param>
        /// <returns></returns>
        public static WebApiException ToWebApiException(this WebApiExceptionViewV01 webApiExceptionViewV01)
        {
            if (webApiExceptionViewV01 == null) return null;

            var error = new ProcessingResult
            {
                RequestId = webApiExceptionViewV01.RequestId,
                ErrorCode = webApiExceptionViewV01.ErrorCode,
                DeniedText = webApiExceptionViewV01.Message
            };
            return new WebApiException(error, Guid.Parse(webApiExceptionViewV01.ReferenceId), webApiExceptionViewV01.HttpStatusCode);
        }
        #endregion
    }
}
