﻿using System;
using System.Net;

namespace BbTS.Domain.Models.Exceptions.Service
{
    /// <summary>
    /// Exception class for surfacing failed communication attempts to an external web service.
    /// </summary>
    public class WebServiceCommunicationFailedException : Exception
    {
        /// <summary>
        /// The response code returned by the server (or underlying api).
        /// </summary>
        public int ResponseCode { get; set; }

        /// <summary>
        /// The contents of the response body of the exception.
        /// </summary>
        public string ResponseBody { get; set; }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="message">Exception message.</param>
        /// <param name="code">The response code returned by the server (or underlying api).</param>
        /// <param name="responseBody">The contents of the response body of the exception.</param>
        public WebServiceCommunicationFailedException(string message, HttpStatusCode code, string responseBody) : base(message)
        {
            ResponseCode = (int)code;
            ResponseBody = responseBody;
        }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="message">Exception message.</param>
        /// <param name="code">The response code returned by the server (or underlying api).</param>
        public WebServiceCommunicationFailedException(string message, int code) : base(message)
        {
            ResponseCode = code;
        }
    }
}
