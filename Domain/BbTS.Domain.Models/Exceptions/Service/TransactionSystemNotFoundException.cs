﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Exceptions.Service
{
    /// <summary>
    /// Exception class for instances where the transaction system cannot be retrieved from an external source.
    /// </summary>
    public class TransactionSystemNotFoundException : Exception
    {
        /// <summary>
        /// The external source whereby the transaction system cannot be retrieved.
        /// </summary>
        public string TransactionSystemSource { get; set; }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="source">A description of the external source whereby the transaction system cannot be retrieved. </param>
        /// <param name="message">Exception message.</param>
        public TransactionSystemNotFoundException(string source, string message) : base(message)
        {
            TransactionSystemSource = source;
        }
    }
}
