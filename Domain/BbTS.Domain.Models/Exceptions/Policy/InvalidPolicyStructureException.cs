﻿using System;

namespace BbTS.Domain.Models.Exceptions.Policy
{
    /// <summary>
    /// Thrown when the attempted policy structure would violate policy tree structure rules.
    /// </summary>
    public class InvalidPolicyStructureException : Exception
    {
        /// <summary>
        /// Parameter-less constructor.
        /// </summary>
        public InvalidPolicyStructureException()
        {
        }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="message"></param>
        public InvalidPolicyStructureException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Parameterized constructor with inner exception.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="inner"></param>
        public InvalidPolicyStructureException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}