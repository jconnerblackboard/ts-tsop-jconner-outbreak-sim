﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Exceptions.Status
{
    /// <summary>
    /// Exception thrown when a failure to create a healthcheck item is detected.
    /// </summary>
    public class HealthcheckCreationException : Exception
    {
        /// <summary>
        /// Empty constructor.
        /// </summary>
        public HealthcheckCreationException()
        {
        }

        /// <summary>
        /// Pass the message to the base.
        /// </summary>
        /// <param name="message">message to insert into the exception</param>
        public HealthcheckCreationException(string message) : base(message)
        {
        }
    }
}
