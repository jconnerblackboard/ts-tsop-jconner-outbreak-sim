﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Exceptions.Security.Oauth
{
    public class OauthException : Exception
    {
        public OauthException(string message) : base(message)
        {
            
        }
    }
}
