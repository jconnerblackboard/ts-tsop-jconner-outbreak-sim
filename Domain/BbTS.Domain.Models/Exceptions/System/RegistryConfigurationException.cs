﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Definitions.Logging;

namespace BbTS.Domain.Models.Exceptions.System
{
    /// <summary>
    /// Exception class thrown when an error occurs dealing with the Windows System Registry.
    /// </summary>
    public class RegistryConfigurationException : Exception
    {
        /// <summary>
        /// The event category to log the exception under.  Defaults to 18 'Tool'.
        /// </summary>
        public LoggingDefinitions.Category EventCategory { get; set; } = LoggingDefinitions.Category.Tool;

        /// <summary>
        /// The event category to log the exception under.  Defaults to 600 'RegistryConfigurationValueMissing'.
        /// </summary>
        public LoggingDefinitions.EventId EventId { get; set; } = LoggingDefinitions.EventId.RegistryConfigurationValueMissing;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="message">Exception message.</param>
        public RegistryConfigurationException(string message) : base(message)
        {
            
        }
    }
}
