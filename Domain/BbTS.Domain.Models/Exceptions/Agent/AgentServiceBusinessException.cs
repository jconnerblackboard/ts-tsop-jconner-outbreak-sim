﻿using System;
using BbTS.Domain.Models.Definitions.Agent;
using BbTS.Domain.Models.System;

namespace BbTS.Domain.Models.Exceptions.Agent
{
    /// <summary>
    /// Container class for an agent service business logic exception.
    /// </summary>
    public class AgentServiceBusinessException : Exception
    {
        /// <summary>
        /// The result token associated with the agent. 
        /// </summary>
        public ActionResultToken ResultToken { get; internal set; }

        /// <summary>
        /// Generate a new AgentServiceBusinessException type exception.
        /// </summary>
        /// <param name="token"></param>
        public AgentServiceBusinessException(ActionResultToken token)
        {
            ResultToken = token ?? 
                AgentServiceDefinitions.GenerateActionResultToken(
                    AgentServiceDefinitions.AgentServiceActionResultDomainIdCode.UnknownFailure,
                    "An unknown failure has occured.");
        }
    }
}
