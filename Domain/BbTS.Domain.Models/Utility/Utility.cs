﻿using System;
using System.Linq;
using System.Web.Mvc;

namespace BbTS.Domain.Models.Utility
{
    /// <summary>
    /// This classs will allow me to exclude properties for validation
    /// </summary>
    public class BindExclude : ActionFilterAttribute
    {
        /// <summary>
        /// A comma separate list of values to exclude
        /// </summary>
        public string Exclude { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Exclude.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList().ForEach(x => filterContext.Controller.ViewData.ModelState.Remove(x.Trim()));
            base.OnActionExecuting(filterContext);
        }
    }
}
