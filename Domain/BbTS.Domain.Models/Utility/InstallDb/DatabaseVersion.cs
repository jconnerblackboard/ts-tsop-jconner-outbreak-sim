﻿using System;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Utility.InstallDb
{
    public class DatabaseVersion
    {
        [XmlAttribute]
        public String Database { get; set; }
        [XmlAttribute]
        public String MinimumVersion { get; set; }
        [XmlAttribute]
        public String MaximumVersion { get; set; }
    }
}
