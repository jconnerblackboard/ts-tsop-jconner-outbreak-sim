﻿namespace BbTS.Domain.Models.Utility.InstallDb
{
    public enum EventType
    {
        Connect,
        Disconnect,
        ProcessStart,
        ProcessEnd,
        Message
    }
}
