﻿namespace BbTS.Domain.Models.Utility.InstallDb
{
    public enum EntryType
    {
        Debug = 0,
        Information = 1,
        Success = 2,
        Warning = 3,
        Failed = 4,
        None = 5
    }
}
