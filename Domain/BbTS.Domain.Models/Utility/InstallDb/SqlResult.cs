﻿using System;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Utility.InstallDb
{
    public class SqlResult
    {
        [XmlAttribute("Path")]
        public String Path { get; set; }
        [XmlAttribute("Name")]
        public String Name { get; set; }
        [XmlAttribute("DateTime")]
        public DateTime DateTime { get; set; }
        [XmlAttribute("Succeeded")]
        public Boolean Succeeded { get; set; }
        [XmlAttribute("Child")]
        public Int32 Child { get; set; }
        [XmlElement("Exception")]
        public String Exception { get; set; }
    }
}
