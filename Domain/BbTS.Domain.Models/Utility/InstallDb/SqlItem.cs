﻿using System;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Utility.InstallDb {
    public class SqlItem {
        /// <summary>
        /// The name of the item
        /// </summary>
        [XmlAttribute]
        public string Name { get; set; }
        /// <summary>
        /// Whether errors are fatal or not
        /// </summary>
        [XmlAttribute]
        public bool ErrorsAreFatal { get; set; }
        /// <summary>
        /// If a path qualifier is needed.
        /// </summary>
        [XmlAttribute]
        public string PathQualifier { get; set; }
        /// <summary>
        /// The group of items run together
        /// </summary>
        [XmlAttribute]
        public int Group { get; set; }
        /// <summary>
        /// The names of the items that cause a group incrementation event
        /// </summary>
        [XmlAttribute]
        public string GroupIncrementer { get; set; }
        /// <summary>
        /// Any messages that get displayed on the UI
        /// </summary>
        [XmlAttribute]
        public string Message { get; set; }
        /// <summary>
        /// The type of object
        /// </summary>
        [XmlAttribute]
        public string ObjectType { get; set; }
        /// <summary>
        /// Flag whether the file is to be only copied to the Install destination or also included in the Install XML file
        /// </summary>
        [XmlAttribute]
        public bool FileCopyOnly { get; set; }
    }
}