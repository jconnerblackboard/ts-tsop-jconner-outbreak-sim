﻿using System;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Utility.InstallDb
{
    public class DbActivityItem
    {
        [XmlAttribute]
        public String Name { get; set; }
        [XmlAttribute]
        public String SqlItemTrigger { get; set; }
        [XmlAttribute]
        public String Arguments { get; set; }
    }
}
