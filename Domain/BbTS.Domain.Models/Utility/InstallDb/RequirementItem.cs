﻿using System;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Utility.InstallDb
{
    public class RequirementItem
    {
        [XmlAttribute]
        public String Name { get; set; }
        [XmlAttribute]
        public String Value { get; set; }
        [XmlAttribute]
        public Int32 Severity { get; set; }
    }
}
