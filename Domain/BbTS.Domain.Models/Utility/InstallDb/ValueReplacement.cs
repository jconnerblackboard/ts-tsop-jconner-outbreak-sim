﻿using System;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Utility.InstallDb
{
    public class ValueReplacement
    {
        [XmlElement]
        public String Database { get; set; }
        [XmlElement]
        public String Query { get; set; }
        [XmlElement]
        public String QueryResult { get; set; }
        [XmlElement]
        public String DialogText { get; set; }
        [XmlElement]
        public String SearchTerm { get; set; }
        [XmlElement]
        public Boolean ResponseIsPassword { get; set; }
        [XmlElement]
        public Boolean IsRequired { get; set; }
        [XmlElement]
        public String Caption { get; set; }
        [XmlElement]
        public Boolean PromptUser { get; set; }
    }
}
