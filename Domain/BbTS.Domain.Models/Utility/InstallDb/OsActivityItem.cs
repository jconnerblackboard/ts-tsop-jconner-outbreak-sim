﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Utility.InstallDb
{
    public class OsActivityItem
    {
        [XmlAttribute]
        public String Name { get; set; }
        [XmlAttribute]
        public OsActivityItemType Type { get; set; }
        [XmlAttribute]
        public String SqlItemTriggerItem { get; set; }
        [XmlAttribute]
        public String Arguments { get; set; }
    }
}
