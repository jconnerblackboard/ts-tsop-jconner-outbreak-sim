﻿using System;

namespace BbTS.Domain.Models.Utility.InstallDb
{
    public class InstallItem
    {
        public String FileName { get; set; }
        public String UserId { get; set; }
        public String Password { get; set; }
        public Boolean Checked { get; set; }
        public String ServiceName { get; set; }
        public Int32 InstallationOrder { get; set; }

        public override String ToString()
        {
            return FileName;
        }
    }
}
