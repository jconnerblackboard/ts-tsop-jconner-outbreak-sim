﻿using System;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Utility.InstallDb
{
    public class ApplicationEvent
    {
        [XmlAttribute("EventType")]
        public EventType EventType { get; set; }
        [XmlText]
        public String Message { get; set; }
        [XmlAttribute("EntryType")]
        public EntryType EntryType { get; set; }
    }
}
