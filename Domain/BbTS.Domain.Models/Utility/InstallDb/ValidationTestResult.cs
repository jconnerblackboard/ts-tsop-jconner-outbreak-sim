﻿using System;
using System.Drawing;

namespace BbTS.Domain.Models.Utility.InstallDb
{
    public class ValidationTestResult
    {
        public Boolean Passed { get; set; }
        public String Result { get; set; }
        public Bitmap Image { get; set; }
    }
}
