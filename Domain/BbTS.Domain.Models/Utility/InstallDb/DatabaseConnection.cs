﻿using System;

namespace BbTS.Domain.Models.Utility.InstallDb
{
    /// <summary>
    /// This object represents a connection to an Oracle database
    /// </summary>
    public class DatabaseConnection
    {
        /// <summary>
        /// The Id of the user connecting to the database
        /// </summary>
        public String UserId { get; set; }
        /// <summary>
        /// The password of the user connecting to the database
        /// </summary>
        public String Password { get; set; }
        /// <summary>
        /// The name of the database server
        /// </summary>
        public String ServerName { get; set; }
        /// <summary>
        /// The port of the database server
        /// </summary>
        public Int32 Port { get; set; }
        /// <summary>
        /// The service name of the database
        /// </summary>
        public String ServiceName { get; set; }
    }
}
