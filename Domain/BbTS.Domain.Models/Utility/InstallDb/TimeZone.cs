﻿using System;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Utility.InstallDb
{
    public class TimeZone
    {
        [XmlAttribute]
        public String Name { get; set; }

        [XmlAttribute]
        public String Value { get; set; }

        [XmlAttribute]
        public String CountryCode { get; set; }
    }
}
