﻿using System;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Utility.InstallDb {
    [XmlRoot("InstallList"), Serializable]
    public class InstallList : Manifest {
        [XmlAttribute]
        public String RtmReleaseDate { get; set; }
        [XmlAttribute]
        public String PackageId { get; set; }
        [XmlAttribute]
        public String PackageNumber { get; set; }
        [XmlAttribute]
        public Int32 PackageVersion { get; set; }
    }
}
