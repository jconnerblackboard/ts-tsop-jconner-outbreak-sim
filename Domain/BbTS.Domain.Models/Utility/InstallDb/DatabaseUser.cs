﻿using System.Xml.Serialization;

namespace BbTS.Domain.Models.Utility.InstallDb
{
    public class DatabaseUser
    {
        /// <summary>
        /// The name of the database this user is being created in
        /// </summary>
        [XmlAttribute]
        public string Database { get; set; }
        /// <summary>
        /// The name of the database user
        /// </summary>
        [XmlAttribute]
        public string Name { get; set; }
        /// <summary>
        /// The default password of the user
        /// </summary>
        [XmlAttribute]
        public string Password { get; set; }
        /// <summary>
        /// The replacement term that'll be used in search/replace scripts for user creation
        /// </summary>
        [XmlAttribute]
        public string ReplacementTerm { get; set; }
        /// <summary>
        /// Whether this user exists in the system
        /// </summary>
        public bool Exists { get; set; }
    }
}
