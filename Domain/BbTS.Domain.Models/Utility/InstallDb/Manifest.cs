﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Utility.InstallDb {
    [XmlRoot("Manifest"), Serializable]
    public class Manifest {
        [XmlAttribute]
        public String PackageName { get; set; }
        [XmlElement("Database")]
        public List<Database> Databases { get; set; }
    }
}
