﻿using System;

namespace BbTS.Domain.Models.Utility.InstallDb
{
    public class SqlObject
    {
        public String Name { get; set; }
        public String Text { get; set; }
    }
}
