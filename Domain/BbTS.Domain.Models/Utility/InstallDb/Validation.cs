﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Utility.InstallDb
{
  /// <summary>
  /// This class represents a Validation object
  /// </summary>
  public class Validation
  {
    public String Name { get; set; }
    public String Description { get; set; }
    public String Database { get; set; }
    public String Query { get; set; }
    public List<Result> Results { get; set; }
    public List<ColumnObject> DatabaseObjects { get; set; }
  }

  /// <summary>
  /// This class represents a result object
  /// </summary>
  public class Result
  {
    [XmlAttribute("Value")]
    public String Value { get; set; }
    [XmlAttribute("Action")]
    public ResultAction Action { get; set; }
    [XmlAttribute("Message")]
    public String Message { get; set; }
  }

  /// <summary>
  /// This class represents a database object
  /// </summary>
  public class ColumnObject
  {
    [XmlAttribute("Table")]
    public String Table { get; set; }
    [XmlAttribute("Column")]
    public String Column { get; set; }
    [XmlAttribute("Action")]
    public ResultAction Action { get; set; }
  }

  /// <summary>
  /// The accepted result action
  /// </summary>
  public enum ResultAction
  {
    Abort,
    Continue,
    Warning
  }
}
