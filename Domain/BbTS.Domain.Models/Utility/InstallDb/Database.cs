﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Utility.InstallDb
{
    public class Database
    {
        [XmlAttribute]
        public String Name { get; set; }
        [XmlAttribute]
        public String DefaultUserId { get; set; }
        [XmlAttribute]
        public String DefaultServiceName { get; set; }
        [XmlAttribute]
        public Boolean InstallByDefault { get; set; }
        [XmlAttribute]
        public Int32 InstallationOrder { get; set; }
        [XmlAttribute]
        public String PathQualifier { get; set; }
        [XmlElement("SqlItem")]
        public List<SqlItem> SqlItems { get; set; }
        [XmlElement("RequirementItem")]
        public List<RequirementItem> RequirementItems { get; set; }
        [XmlElement("DbActivityItem")]
        public List<DbActivityItem> DbActivityItems { get; set; }
    }
}