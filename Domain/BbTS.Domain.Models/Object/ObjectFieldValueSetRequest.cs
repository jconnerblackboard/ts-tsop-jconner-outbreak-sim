﻿using System;
using System.Collections.Generic;

namespace BbTS.Domain.Models.Object
{
    /// <summary>
    /// Container for Object field value set response
    /// </summary>
    public class ObjectFieldValueSetRequest
    {
        /// <summary>
        /// Request Id
        /// </summary>
        public Guid? RequestId { get; set; }

        /// <summary>
        /// List of object def field value
        /// </summary>
        public List<ObjectDefFieldValue> ObjectDefinedFieldValues { get; set; } = new List<ObjectDefFieldValue>();
    }
}
