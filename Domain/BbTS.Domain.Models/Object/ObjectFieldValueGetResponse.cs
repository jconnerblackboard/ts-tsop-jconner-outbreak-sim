﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Object
{
    /// <summary>
    /// Container for Object field value get response
    /// </summary>
    public class ObjectFieldValueGetResponse
    {
        /// <summary>
        /// Object def field values
        /// </summary>
        public List<ObjectDefFieldValue> ObjectDefinedFieldValues { get; set; }
    }
}
