﻿using System;
using System.Collections.Generic;

namespace BbTS.Domain.Models.Object
{
    /// <summary>
    /// Container for Object field value set response
    /// </summary>
    public class ObjectFieldValueSetResponse
    {
        /// <summary>
        /// Request Id
        /// </summary>
        public Guid RequestId { get; set; }

        /// <summary>
        /// Object def field value
        /// </summary>
        public List<ObjectDefFieldValue> ObjectDefinedFieldValues { get; set; }
    }
}
