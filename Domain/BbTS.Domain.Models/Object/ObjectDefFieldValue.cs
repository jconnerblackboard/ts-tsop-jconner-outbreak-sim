﻿
namespace BbTS.Domain.Models.Object
{
    /// <summary>
    /// Object defined field
    /// </summary>
    public class ObjectDefFieldValue
    {
        /// <summary>
        /// Object Id
        /// </summary>
        public int ObjectId { get; set; }

        /// <summary>
        /// Object defined field Id
        /// </summary>
        public int DefinedFieldId { get; set; }

        /// <summary>
        /// Value
        /// </summary>
        public string Value { get; set; }
    }
}
