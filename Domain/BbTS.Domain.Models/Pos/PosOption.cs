﻿using System;
using System.Runtime.Serialization;
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Pos
{
    /// <summary>
    /// This object represents a Transact pos_option object. Merchant Point of Sales Option Policy Table (list of options)
    /// </summary>
    [Serializable]
    [DataContract]
    public class PosOption
    {
        /// <summary>
        /// Point of Sale Option Identifier
        /// </summary>

        [DataMember]
        public int Pos_Option_Id { get; set; }
        /// <summary>
        /// Merchant Association (from MERCHANT table)
        /// </summary>

        [DataMember]
        public int Merchant_Id { get; set; }
        /// <summary>
        /// Name of Options
        /// </summary>

        [DataMember]
        public string Name { get; set; }
        /// <summary>
        /// Indicates if the Receipt Header Is Active (T/F)
        /// </summary>

        [DataMember]
        public string Receipt_Header_Active { get; set; }
        /// <summary>
        /// Receipt Header Line 1
        /// </summary>

        [DataMember]
        public string Receipt_Header1 { get; set; }
        /// <summary>
        /// Receipt Header Line 2
        /// </summary>

        [DataMember]
        public string Receipt_Header2 { get; set; }
        /// <summary>
        /// Receipt Header Line 3
        /// </summary>

        [DataMember]
        public string Receipt_Header3 { get; set; }
        /// <summary>
        /// Receipt Header Line 4
        /// </summary>

        [DataMember]
        public string Receipt_Header4 { get; set; }
        /// <summary>
        /// Indicates if the Receipt Coupon Lines are Active (T/F)
        /// </summary>

        [DataMember]
        public string Receipt_Coupon_Active { get; set; }
        /// <summary>
        /// Receipt Coupon Line 1
        /// </summary>

        [DataMember]
        public string Receipt_Coupon1 { get; set; }
        /// <summary>
        /// Receipt Coupon Line 2
        /// </summary>

        [DataMember]
        public string Receipt_Coupon2 { get; set; }
        /// <summary>
        /// Receipt Coupon Line 3
        /// </summary>

        [DataMember]
        public string Receipt_Coupon3 { get; set; }
        /// <summary>
        /// Receipt Coupon Line 4
        /// </summary>

        [DataMember]
        public string Receipt_Coupon4 { get; set; }
        /// <summary>
        /// Indicates if the Receipt Footer Lines are Active (T/F)
        /// </summary>

        [DataMember]
        public string Receipt_Footer_Active { get; set; }
        /// <summary>
        /// Receipt Footer Line 1
        /// </summary>

        [DataMember]
        public string Receipt_Footer1 { get; set; }
        /// <summary>
        /// Receipt Footer Line 2
        /// </summary>

        [DataMember]
        public string Receipt_Footer2 { get; set; }
        /// <summary>
        /// Receipt Footer Line 3
        /// </summary>

        [DataMember]
        public string Receipt_Footer3 { get; set; }
        /// <summary>
        /// Receipt Footer Line 4
        /// </summary>

        [DataMember]
        public string Receipt_Footer4 { get; set; }
        /// <summary>
        /// Purchase Entry Type (0) Amount - summarize all lines (1) Itemize - record each line for Transaction Terminals
        /// </summary>

        [DataMember]
        public int Stored_Value_Cash_Entry_Type { get; set; }
        /// <summary>
        /// Indicator for Forcing the Cash Drawer Closed (T/F)
        /// </summary>

        [DataMember]
        public string Force_Close_Cash_Drawer { get; set; }
        /// <summary>
        /// Indicator to Allow Subtotal on Exit (T/F)
        /// </summary>

        [DataMember]
        public string Allow_Subtotal_Exit { get; set; }
        /// <summary>
        /// Indicator to Auto Continue Board Transactions (T/F)
        /// </summary>

        [DataMember]
        public string Board_Auto_Continue { get; set; }
        /// <summary>
        /// Indicator allowing the card number to be manually entered (T/F)
        /// </summary>

        [DataMember]
        public string Cardnum_Manual_Entry_Enabled { get; set; }
        /// <summary>
        /// Indicator to limit transactions (T/F)
        /// </summary>

        [DataMember]
        public string Limit_Transactions { get; set; }
        /// <summary>
        /// Limit Transactions Amount
        /// </summary>

        [DataMember]
        public int Limit_Transactions_Amount { get; set; }
        /// <summary>
        /// Indicator to limit cash (T/F)
        /// </summary>

        [DataMember]
        public string Limit_Cash { get; set; }
        /// <summary>
        /// Limit Cash Amount
        /// </summary>

        [DataMember]
        public int Limit_Cash_Amount { get; set; }
        /// <summary>
        /// Indicator to automatically logout (T/F)
        /// </summary>

        [DataMember]
        public string Auto_Logout { get; set; }
        /// <summary>
        /// Indicator to allow offline transactions (T/F)
        /// </summary>

        [DataMember]
        public string Allow_Offline_Transactions { get; set; }
        /// <summary>
        /// Indicator to allow offline payments (T/F)
        /// </summary>

        [DataMember]
        public string Allow_Offline_Payments { get; set; }
        /// <summary>
        /// Indicator to allow payments (T/F)
        /// </summary>

        [DataMember]
        public string Allow_Payments { get; set; }
        /// <summary>
        /// Freeze Customer Account Indicator (when?)
        /// </summary>

        [DataMember]
        public string Freeze_Customer_Account { get; set; }
        /// <summary>
        /// Indicator to balance check(?)
        /// </summary>

        [DataMember]
        public string Balance_Check { get; set; }
        /// <summary>
        /// Indicator to allow splitting of tender types (T/F)
        /// </summary>

        [DataMember]
        public string Split_Tender { get; set; }
        /// <summary>
        /// Indicator to display the meal served (T/F)
        /// </summary>

        [DataMember]
        public string Display_Meals_Served { get; set; }
        /// <summary>
        /// Allow event check attendance (T/F)
        /// </summary>

        [DataMember]
        public string Allow_Check_Attendance { get; set; }
        /// <summary>
        /// Indicator allowing event transaction reversals (T/F)
        /// </summary>

        [DataMember]
        public string Allow_Reverse { get; set; }
        /// <summary>
        /// Indicates if the POS can process event transactions (T/F)
        /// </summary>

        [DataMember]
        public string Allow_Event_Transaction { get; set; }
        /// <summary>
        /// Indicator to Print "I Agree to Pay..." for Credit Card on the receipt (T/F)
        /// </summary>

        [DataMember]
        public string Rcpt_Prn_Agree_Credit_Card { get; set; }
        /// <summary>
        /// Indicator to Print Audit on Cashier Logoff (T/F)
        /// </summary>

        [DataMember]
        public string Receipt_Print_Audit { get; set; }
        /// <summary>
        /// Indicator to print customer name on receipt (T/F)
        /// </summary>

        [DataMember]
        public string Receipt_Print_Customer_Name { get; set; }
        /// <summary>
        /// Indicator to print customer number on receipt (T/F)
        /// </summary>

        [DataMember]
        public string Receipt_Print_Customer_Number { get; set; }
        /// <summary>
        /// Indicator to print customer balance on receipt (T/F)
        /// </summary>

        [DataMember]
        public string Receipt_Print_Customer_Balance { get; set; }
        /// <summary>
        /// Indicator to print card number on receipt (T/F)
        /// </summary>

        [DataMember]
        public string Receipt_Print_Card_Number { get; set; }
        /// <summary>
        /// Indicator to Show Amount Deducted for Stored Value Tender Types (T/F)
        /// </summary>

        [DataMember]
        public string Receipt_Print_Dollars { get; set; }
        /// <summary>
        /// Indicator to print any transaction comments on the receipt (T/F)
        /// </summary>

        [DataMember]
        public string Receipt_Print_Trans_Comment { get; set; }
        /// <summary>
        /// Indicator to print the balance below the limit on the receipt (T/F)
        /// </summary>

        [DataMember]
        public string Receipt_Print_Balance_Below { get; set; }
        /// <summary>
        /// Print on Receipt - Balance Below Threshold Amount
        /// </summary>

        [DataMember]
        public int Receipt_Balance_Below_Amount { get; set; }
        /// <summary>
        /// Indicator forcing the printing of cash transactions on the receipt (T/F)
        /// </summary>

        [DataMember]
        public string Force_Print_Cash { get; set; }
        /// <summary>
        /// Indicator to force the printing check transactions on the receipt (T/F)
        /// </summary>

        [DataMember]
        public string Force_Print_Check { get; set; }
        /// <summary>
        /// Indicator to print stored value transactions on the receipt (T/F)
        /// </summary>

        [DataMember]
        public string Force_Prnt_Stored_Value_Trans { get; set; }
        /// <summary>
        /// Indicator to print Cash Equivalancy transactions on the receipt (T/F)
        /// </summary>

        [DataMember]
        public string Force_Print_Cash_Equivalent { get; set; }
        /// <summary>
        /// Indicator to print Paid Outs on the receipt (T/F)
        /// </summary>

        [DataMember]
        public string Force_Print_Paid_Out { get; set; }
        /// <summary>
        /// Indicator to for the printing of Freezing Accounts on the receipt (T/F)
        /// </summary>

        [DataMember]
        public string Force_Print_Freeze_Account { get; set; }
        /// <summary>
        /// Indicator for the forcing of printing overrings on the receipt (T/F)
        /// </summary>

        [DataMember]
        public string Force_Print_Overrings { get; set; }
        /// <summary>
        /// Indicator for the forcing of printing stored value payments on the receipt (T/F)
        /// </summary>

        [DataMember]
        public string Force_Prnt_Stored_Val_Pymnt { get; set; }
        /// <summary>
        /// Indicates that a board transaction will be forced to print (T/F)
        /// </summary>

        [DataMember]
        public string Force_Print_Board { get; set; }
        /// <summary>
        /// Indicates if an event transaction will be forced to print on the receipt (T/F)
        /// </summary>

        [DataMember]
        public string Force_Print_Event { get; set; }
        /// <summary>
        /// Indicator to set the printer on by default (T/F)
        /// </summary>

        [DataMember]
        public string Printer_On_By_Default { get; set; }
        /// <summary>
        /// Message Delay in milliseconds
        /// </summary>

        [DataMember]
        public int Message_Delay { get; set; }
        /// <summary>
        /// Indicator to enable confirmation beeps (T/F)
        /// </summary>

        [DataMember]
        public string Confirmation_Beep { get; set; }
        /// <summary>
        /// Indicator allowing credit card call center authorizations (T/F)
        /// </summary>

        [DataMember]
        public string Crdt_Crd_Offln_Call_Cntr_Auth { get; set; }
        /// <summary>
        /// Display meals left by : (0) Auto, (1) Days, (2) Weeks, (3) Months, (4) Semesters, (5) Year
        /// </summary>

        [DataMember]
        public int Meals_Left_By_Type { get; set; }
        /// <summary>
        /// (0) None, (1) Amount (Dollar Fmt), (2) Open, (3) Percent (Percent Fmt)  Amount and Percent Require the XXX_AMT field filled in (Gui enforced)
        /// </summary>

        [DataMember]
        public int Discount_Key_Type1 { get; set; }
        /// <summary>
        /// Discount Amount Key 1
        /// </summary>

        [DataMember]
        public int Discount_Key_Amount1 { get; set; }
        /// <summary>
        /// (0) None, (1) Amount (Dollar Fmt), (2) Open, (3) Percent (Percent Fmt)  Amount and Percent Require the XXX_AMT field filled in (Gui enforced)
        /// </summary>

        [DataMember]
        public int Discount_Key_Type2 { get; set; }
        /// <summary>
        /// Discount Amount Key 2
        /// </summary>

        [DataMember]
        public int Discount_Key_Amount2 { get; set; }
        /// <summary>
        /// (0) None, (1) Amount (Dollar Fmt), (2) Open, (3) Percent (Percent Fmt)  Amount and Percent Require the XXX_AMT field filled in (Gui enforced)
        /// </summary>

        [DataMember]
        public int Discount_Key_Type3 { get; set; }
        /// <summary>
        /// Discount Amount Key 3
        /// </summary>

        [DataMember]
        public int Discount_Key_Amount3 { get; set; }
        /// <summary>
        /// (0) None, (1) Amount (Dollar Fmt), (2) Open, (3) Percent (Percent Fmt)  Amount and Percent Require the XXX_AMT field filled in (Gui enforced)
        /// </summary>

        [DataMember]
        public int Discount_Key_Type4 { get; set; }
        /// <summary>
        /// Discount Amount Key 4
        /// </summary>

        [DataMember]
        public int Discount_Key_Amount4 { get; set; }
        /// <summary>
        /// (0) None, (1) Amount (Dollar Fmt), (2) Open, (3) Percent (Percent Fmt)  Amount and Percent Require the XXX_AMT field filled in (Gui enforced)
        /// </summary>

        [DataMember]
        public int Surcharge_Type1 { get; set; }
        /// <summary>
        /// Surcharge Amount Key 1
        /// </summary>

        [DataMember]
        public int Surcharge_Amount1 { get; set; }
        /// <summary>
        /// (0) None, (1) Amount (Dollar Fmt), (2) Open, (3) Percent (Percent Fmt)  Amount and Percent Require the XXX_AMT field filled in (Gui enforced)
        /// </summary>

        [DataMember]
        public int Surcharge_Type2 { get; set; }
        /// <summary>
        /// Surcharge Amount Key 2
        /// </summary>

        [DataMember]
        public int Surcharge_Amount2 { get; set; }
        /// <summary>
        /// (0) None, (1) Amount (Dollar Fmt), (2) Open, (3) Percent (Percent Fmt)  Amount and Percent Require the XXX_AMT field filled in (Gui enforced)
        /// </summary>

        [DataMember]
        public int Tax_Type1 { get; set; }
        /// <summary>
        /// Tax Amount Key 1
        /// </summary>

        [DataMember]
        public int Tax_Amount1 { get; set; }
        /// <summary>
        /// (0) None, (1) Amount (Dollar Fmt), (2) Open, (3) Percent (Percent Fmt)  Amount and Percent Require the XXX_AMT field filled in (Gui enforced)
        /// </summary>

        [DataMember]
        public int Tax_Type2 { get; set; }
        /// <summary>
        /// Tax Amount Key 2
        /// </summary>

        [DataMember]
        public int Tax_Amount2 { get; set; }
        /// <summary>
        /// Product Number Format Mask
        /// </summary>

        [DataMember]
        public string Product_Mask { get; set; }
        /// <summary>
        /// Indicator allowing screen saver (T/F)
        /// </summary>

        [DataMember]
        public string Screen_Saver { get; set; }
        /// <summary>
        /// Screen Saver Timeout (in minutes)
        /// </summary>

        [DataMember]
        public int Screen_Saver_Timeout { get; set; }
        /// <summary>
        /// Indicator enabling pole screen saver (T/F)
        /// </summary>

        [DataMember]
        public string Pole_Screen_Saver { get; set; }
        /// <summary>
        /// Pole Display Timeout (in minutes)
        /// </summary>

        [DataMember]
        public int Pole_Display_Timeout { get; set; }
        /// <summary>
        /// Default Board Plan Meal Type Association (from BOARDMEALTYPES)
        /// </summary>

        [DataMember]
        public int Default_Board_Meal_Type_Id { get; set; }
        /// <summary>
        /// Default Cash Equivalancy Transaction Board Meal Type Association (from BOARDMEALTYPES table)
        /// </summary>

        [DataMember]
        public int Dflt_Equiv_Board_Meal_Type_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>

        [DataMember]
        public int Default_Boardplan_Id { get; set; }
        /// <summary>
        /// Register should combine identical product line items on receipt and remote printer (T/F)
        /// </summary>

        [DataMember]
        public string Receipt_Group_Product { get; set; }
        /// <summary>
        /// 
        /// </summary>

        [DataMember]
        public int Product_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>

        [DataMember]
        public string Allow_No_Tax { get; set; }
        /// <summary>
        /// 
        /// </summary>

        [DataMember]
        public string Allow_Count_Display { get; set; }
        /// <summary>
        /// 
        /// </summary>

        [DataMember]
        public int CashierLoginType { get; set; }
        /// <summary>
        /// 
        /// </summary>

        [DataMember]
        public int TtDiscountType1 { get; set; }
        /// <summary>
        /// 
        /// </summary>

        [DataMember]
        public int TtDiscountType2 { get; set; }
        /// <summary>
        /// 
        /// </summary>

        [DataMember]
        public int TtDiscountAmount1 { get; set; }
        /// <summary>
        /// 
        /// </summary>

        [DataMember]
        public int TtDiscountAmount2 { get; set; }
        /// <summary>
        /// 
        /// </summary>

        [DataMember]
        public int TtTaxType1 { get; set; }
        /// <summary>
        /// 
        /// </summary>

        [DataMember]
        public int TtTaxType2 { get; set; }
        /// <summary>
        /// 
        /// </summary>

        [DataMember]
        public int TtTaxAmount1 { get; set; }
        /// <summary>
        /// 
        /// </summary>

        [DataMember]
        public int TtTaxAmount2 { get; set; }
        /// <summary>
        /// 
        /// </summary>

        [DataMember]
        public int TtSurchargeType1 { get; set; }
        /// <summary>
        /// 
        /// </summary>

        [DataMember]
        public int TtSurchargeType2 { get; set; }
        /// <summary>
        /// 
        /// </summary>

        [DataMember]
        public int TtSurchargeAmount1 { get; set; }
        /// <summary>
        /// 
        /// </summary>

        [DataMember]
        public int TtSurchargeAmount2 { get; set; }
        /// <summary>
        /// Flag denoting whether the Print Board Cash Value Limit should be printed on receipts. 
        /// </summary>

        [DataMember]
        public string Print_Board_Cash_Value_Limit { get; set; }
    }
}