﻿using System;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Pos
{
    /// <summary>
    /// This object represents a Transact Pos_Laundry_Machine object. Point of Sale - Laundry Machines
    /// </summary>
    [Serializable]
    public class TsPos_Laundry_Machine
    {
        /// <summary>
        /// Machine Number
        /// </summary>
        [XmlAttribute]
        public int MachineNum { get; set; }
        /// <summary>
        ///
        /// </summary>
        [XmlAttribute]
        public int Pos_Id { get; set; }
        /// <summary>
        /// Indicates if the machine is stacked with other machines (T/F)
        /// </summary>
        [XmlAttribute]
        public string Stacked_With_Enabled { get; set; }
        /// <summary>
        ///
        /// </summary>
        [XmlAttribute]
        public int Stacked_With_MachineNum { get; set; }
        /// <summary>
        /// Indicates if the machine is enabled (T/F)
        /// </summary>
        [XmlAttribute]
        public string Machine_Enabled { get; set; }
        /// <summary>
        ///
        /// </summary>
        [XmlAttribute]
        public int Laundry_Mach_Typ_Definition_Id { get; set; }
    }
}