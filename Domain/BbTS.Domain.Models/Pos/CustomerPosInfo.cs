﻿using System;

namespace BbTS.Domain.Models.Pos
{
    /// <summary>
    /// Container class for customer pos info.
    /// </summary>
    public class CustomerPosInfo
    {
        /// <summary>
        /// Customer's last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Customer's numberical identifier
        /// </summary>
        public string CustomerNumber { get; set; }

        /// <summary>
        /// Customer's card number
        /// </summary>
        public string CardNumber { get; set; }

        /// <summary>
        /// Is today the customer's birthday?
        /// </summary>
        public bool IsBirthday { get; set; }

        /// <summary>
        /// Display the customer's stored value balance?
        /// </summary>
        public decimal? DisplaySvBalance { get; set; }

        /// <summary>
        /// Print the customer's stored value balance?
        /// </summary>
        public decimal? PrintSvBalance { get; set; }

        /// <summary>
        /// Display the customer's name?
        /// </summary>
        public string DisplayCustomerName { get; set; }

        /// <summary>
        /// Display the customer's birthday?
        /// </summary>
        public DateTime? DisplayBirthday { get; set; }

        /// <summary>
        /// Print the customer's name?
        /// </summary>
        public string PrintCustomerName { get; set; }

        /// <summary>
        /// Print the customer's card number?
        /// </summary>
        public string PrintCustomerCardNumber { get; set; }

        /// <summary>
        /// PRint the customer's number?
        /// </summary>
        public string PrintCustomerNumber { get; set; }

        /// <summary>
        /// Does the customer have a message waiting?
        /// </summary>
        public bool MessageWaiting { get; set; }

        /// <summary>
        /// The customer's numerical identifier.
        /// </summary>
        public int CustomerId { get; set; }
    }
}
