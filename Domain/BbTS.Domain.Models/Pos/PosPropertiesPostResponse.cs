﻿namespace BbTS.Domain.Models.Pos
{
    /// <summary>
    /// Container for a response to a request for a pos properties post.
    /// </summary>
    public class PosPropertiesPostResponse
    {

        /// <summary>
        /// Parameterless constructor for (de)serialization.
        /// </summary>
        public PosPropertiesPostResponse()
        {
        }

        /// <summary>
        /// The unique identifier for this post request and response.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The request that spawned this response.
        /// </summary>
        public PosPropertiesPostRequest Request { get; set; }

        /// <summary>
        /// The properties that were created as part of this operation.
        /// </summary>
        public PosProperties Properties { get; set; }

        /// <summary>
        /// Parameterized constructor that requires the request id and the request.
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="request"></param>
        /// <param name="properties"></param>
        public PosPropertiesPostResponse(string requestId, PosPropertiesPostRequest request, PosProperties properties)
        {
            Request = request;
            RequestId = requestId;
            Properties = properties;
        }
    }
}
