﻿using System.ComponentModel;

namespace BbTS.Domain.Models.Pos
{
    /// <summary>
    /// Pos Credentials
    /// </summary>
    public class PosCredentialSettings
    {
        /// <summary>
        /// Credential type "Magstripe" enabled
        /// </summary>
        [DisplayName("Magstripe Enabled")]
        public bool MagstripeEnabled { get; set; }

        /// <summary>
        /// Credential type "FeliCa" enabled
        /// </summary>
        [DisplayName("FeliCa Enabled")]
        public bool FeliCaEnabled { get; set; }

        /// <summary>
        /// Credential type "Mifare Classic" enabled
        /// </summary>
        [DisplayName("Mifare Classic Enabled")]
        public bool MifareClassicEnabled { get; set; }

        /// <summary>
        /// Credential type "Mifare DESFire" enabled
        /// </summary>
        [DisplayName("DesFire Enabled")]
        public bool MifareDesFireEnabled { get; set; }

        /// <summary>
        /// Credential type "Mobile" enabled
        /// </summary>
        [DisplayName("Mobile Enabled")]
        public bool MobileEnabled { get; set; }

        /// <summary>
        /// Credential type "Biometric" enabled
        /// </summary>
        [DisplayName("Biometric Enabled")]
        public bool BiometricEnabled { get; set; }
    }

    /// <summary>
    /// View for a <see cref="PosCredentialSettings"/>.  (Version 1)
    /// </summary>
    public class PosCredentialSettingsViewV01
    {
        /// <summary>
        /// Credential type "Magstripe" enabled
        /// </summary>
        public bool MagstripeEnabled { get; set; }

        /// <summary>
        /// Credential type "FeliCa" enabled
        /// </summary>
        public bool FeliCaEnabled { get; set; }

        /// <summary>
        /// Credential type "Mifare Classic" enabled
        /// </summary>
        public bool MifareClassicEnabled { get; set; }

        /// <summary>
        /// Credential type "Mifare DESFire" enabled
        /// </summary>
        public bool MifareDesFireEnabled { get; set; }

        /// <summary>
        /// Credential type "Mobile" enabled
        /// </summary>
        public bool MobileEnabled { get; set; }

        /// <summary>
        /// Credential type "Biometric" enabled
        /// </summary>
        public bool BiometricEnabled { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="PosCredentialSettings"/> conversion.
    /// </summary>
    public static class PosCredentialSettingsConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="PosCredentialSettingsViewV01"/> object based on this <see cref="PosCredentialSettings"/>.
        /// </summary>
        /// <param name="posCredentialSettings"></param>
        /// <returns></returns>
        public static PosCredentialSettingsViewV01 ToPosCredentialSettingsViewV01(this PosCredentialSettings posCredentialSettings)
        {
            if (posCredentialSettings == null) return null;

            return new PosCredentialSettingsViewV01
            {
                MagstripeEnabled = posCredentialSettings.MagstripeEnabled,
                FeliCaEnabled = posCredentialSettings.FeliCaEnabled,
                MifareClassicEnabled = posCredentialSettings.MifareClassicEnabled,
                MifareDesFireEnabled = posCredentialSettings.MifareDesFireEnabled,
                MobileEnabled = posCredentialSettings.MobileEnabled,
                BiometricEnabled = posCredentialSettings.BiometricEnabled
            };
        }

        /// <summary>
        /// Returns a <see cref="PosCredentialSettings"/> object based on this <see cref="PosCredentialSettingsViewV01"/>.
        /// </summary>
        /// <param name="posCredentialSettingsViewV01"></param>
        /// <returns></returns>
        public static PosCredentialSettings ToPosCredentialSettings(this PosCredentialSettingsViewV01 posCredentialSettingsViewV01)
        {
            if (posCredentialSettingsViewV01 == null) return null;

            return new PosCredentialSettings
            {
                MagstripeEnabled = posCredentialSettingsViewV01.MagstripeEnabled,
                FeliCaEnabled = posCredentialSettingsViewV01.FeliCaEnabled,
                MifareClassicEnabled = posCredentialSettingsViewV01.MifareClassicEnabled,
                MifareDesFireEnabled = posCredentialSettingsViewV01.MifareDesFireEnabled,
                MobileEnabled = posCredentialSettingsViewV01.MobileEnabled,
                BiometricEnabled = posCredentialSettingsViewV01.BiometricEnabled
            };
        }
        #endregion
    }
}
