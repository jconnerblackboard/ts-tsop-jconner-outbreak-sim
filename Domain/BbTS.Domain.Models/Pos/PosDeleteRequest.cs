﻿namespace BbTS.Domain.Models.Pos
{
    /// <summary>
    /// The request to delete a POS record    
    /// </summary>
    public class PosDeleteRequest
    {
        /// <summary>
        /// The unique identifier for this request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The unique identifier of the pos to delete.
        /// </summary>
        public int PosId { get; set; }
    }
}