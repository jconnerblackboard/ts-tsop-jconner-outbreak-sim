﻿
namespace BbTS.Domain.Models.Pos.PosOptions
{
    /// <summary>
    /// Pos Option Cash Drawer Properties
    /// </summary>
    public class PosOptionCashDrawerProperties
    {
        /// <summary>
        /// Use Cash Drawer
        /// </summary>
        public bool UseCashDrawer { get; set; }

        /// <summary>
        /// Indicator for Forcing the Cash Drawer Closed (T/F)
        /// </summary>        
        public bool ForceCloseCashDrawer { get; set; }
    }

    /// <summary>
    /// View for a <see cref="PosOptionCashDrawerProperties"/>.  (Version 1)
    /// </summary>
    public class PosOptionCashDrawerPropertiesViewV01
    {
        /// <summary>
        /// Use Cash Drawer
        /// </summary>
        public bool UseCashDrawer { get; set; }

        /// <summary>
        /// Indicator for Forcing the Cash Drawer Closed (T/F)
        /// </summary>        
        public bool ForceCloseCashDrawer { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="PosOptionCashDrawerProperties"/> conversion.
    /// </summary>
    public static class PosOptionCashDrawerPropertiesConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="PosOptionCashDrawerPropertiesViewV01"/> object based on this <see cref="PosOptionCashDrawerProperties"/>.
        /// </summary>
        /// <param name="posOptionCashDrawerProperties"></param>
        /// <returns></returns>
        public static PosOptionCashDrawerPropertiesViewV01 ToPosOptionCashDrawerPropertiesViewV01(this PosOptionCashDrawerProperties posOptionCashDrawerProperties)
        {
            if (posOptionCashDrawerProperties == null) return null;

            return new PosOptionCashDrawerPropertiesViewV01
            {
                UseCashDrawer = posOptionCashDrawerProperties.UseCashDrawer,
                ForceCloseCashDrawer = posOptionCashDrawerProperties.ForceCloseCashDrawer
            };
        }

        /// <summary>
        /// Returns a <see cref="PosOptionCashDrawerProperties"/> object based on this <see cref="PosOptionCashDrawerPropertiesViewV01"/>.
        /// </summary>
        /// <param name="posOptionCashDrawerPropertiesViewV01"></param>
        /// <returns></returns>
        public static PosOptionCashDrawerProperties ToPosOptionCashDrawerProperties(this PosOptionCashDrawerPropertiesViewV01 posOptionCashDrawerPropertiesViewV01)
        {
            if (posOptionCashDrawerPropertiesViewV01 == null) return null;

            return new PosOptionCashDrawerProperties
            {
                UseCashDrawer = posOptionCashDrawerPropertiesViewV01.UseCashDrawer,
                ForceCloseCashDrawer = posOptionCashDrawerPropertiesViewV01.ForceCloseCashDrawer
            };
        }
        #endregion
    }
}
