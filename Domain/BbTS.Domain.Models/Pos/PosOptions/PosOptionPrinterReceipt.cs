﻿
namespace BbTS.Domain.Models.Pos.PosOptions
{
    /// <summary>
    /// Pos Option Printer Receipt
    /// </summary>
    public class PosOptionPrinterReceipt
    {
        /// <summary>
        /// Active
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Line 1
        /// </summary>        
        public string Line1 { get; set; }

        /// <summary>
        /// Line 2
        /// </summary>        
        public string Line2 { get; set; }

        /// <summary>
        /// Line 3
        /// </summary>        
        public string Line3 { get; set; }

        /// <summary>
        /// Line 4
        /// </summary>        
        public string Line4 { get; set; }
    }

    /// <summary>
    /// View for a <see cref="PosOptionPrinterReceipt"/>.  (Version 1)
    /// </summary>
    public class PosOptionPrinterReceiptViewV01
    {
        /// <summary>
        /// Active
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Line 1
        /// </summary>        
        public string Line1 { get; set; }

        /// <summary>
        /// Line 2
        /// </summary>        
        public string Line2 { get; set; }

        /// <summary>
        /// Line 3
        /// </summary>        
        public string Line3 { get; set; }

        /// <summary>
        /// Line 4
        /// </summary>        
        public string Line4 { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="PosOptionPrinterReceipt"/> conversion.
    /// </summary>
    public static class PosOptionPrinterReceiptConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="PosOptionPrinterReceiptViewV01"/> object based on this <see cref="PosOptionPrinterReceipt"/>.
        /// </summary>
        /// <param name="posOptionPrinterReceipt"></param>
        /// <returns></returns>
        public static PosOptionPrinterReceiptViewV01 ToPosOptionPrinterReceiptViewV01(this PosOptionPrinterReceipt posOptionPrinterReceipt)
        {
            if (posOptionPrinterReceipt == null) return null;

            return new PosOptionPrinterReceiptViewV01
            {
                Active = posOptionPrinterReceipt.Active,
                Line1 = posOptionPrinterReceipt.Line1,
                Line2 = posOptionPrinterReceipt.Line2,
                Line3 = posOptionPrinterReceipt.Line3,
                Line4 = posOptionPrinterReceipt.Line4
            };
        }

        /// <summary>
        /// Returns a <see cref="PosOptionPrinterReceipt"/> object based on this <see cref="PosOptionPrinterReceiptViewV01"/>.
        /// </summary>
        /// <param name="posOptionPrinterReceiptViewV01"></param>
        /// <returns></returns>
        public static PosOptionPrinterReceipt ToPosOptionPrinterReceipt(this PosOptionPrinterReceiptViewV01 posOptionPrinterReceiptViewV01)
        {
            if (posOptionPrinterReceiptViewV01 == null) return null;

            return new PosOptionPrinterReceipt
            {
                Active = posOptionPrinterReceiptViewV01.Active,
                Line1 = posOptionPrinterReceiptViewV01.Line1,
                Line2 = posOptionPrinterReceiptViewV01.Line2,
                Line3 = posOptionPrinterReceiptViewV01.Line3,
                Line4 = posOptionPrinterReceiptViewV01.Line4
            };
        }
        #endregion
    }
}
