﻿
namespace BbTS.Domain.Models.Pos.PosOptions
{
    /// <summary>
    /// This object represents a Transact posoption object. Merchant Point of Sales Option Policy Table (list of options)
    /// </summary>
    public class PosOptionDeviceSettings
    {
        /// <summary>
        /// Pos Option Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Indicator allowing the card number to be manually entered (T/F)
        /// </summary>        
        public bool CardnumManualEntryEnabled { get; set; }

        /// <summary>
        /// Default Values
        /// </summary>
        public PosOptionDefaultValues DefaultValues { get; set; }

        /// <summary>
        /// Constraints
        /// </summary>
        public PosOptionConstraints Constraints { get; set; }

        /// <summary>
        /// Feature availability
        /// </summary>
        public PosOptionFeatureAvailability FeatureAvailability { get; set; }

        /// <summary>
        /// Cash drawer properties
        /// </summary>
        public PosOptionCashDrawerProperties CashDrawerProperties { get; set; }

        /// <summary>
        /// Printer properties
        /// </summary>
        public PosOptionPrinterProperties PrinterProperties { get; set; }

        /// <summary>
        /// Override keys
        /// </summary>
        public PosOptionOverrideKeys OverrideKeys { get; set; }
    }
}