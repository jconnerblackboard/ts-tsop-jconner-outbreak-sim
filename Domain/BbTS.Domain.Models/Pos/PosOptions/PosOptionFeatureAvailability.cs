﻿
namespace BbTS.Domain.Models.Pos.PosOptions
{
    /// <summary>
    /// Pos Option Feature Availability
    /// </summary>
    public class PosOptionFeatureAvailability
    {
        /// <summary>
        /// Allow no tax
        /// </summary>        
        public bool AllowNoTax { get; set; }

        /// <summary>
        /// Allow count display
        /// </summary>        
        public bool AllowCountDisplay { get; set; }

        /// <summary>
        /// Indicator to Auto Continue Board Transactions (T/F)
        /// </summary>        
        public bool BoardAutoContinue { get; set; }

        /// <summary>
        /// Turnstilerelay Enabled
        /// </summary>
        public bool TurnstilerelayEnabled { get; set; }

        /// <summary>
        /// Card Utility Enabled
        /// </summary>
        public bool CardUtilityEnabled { get; set; }

        /// <summary>
        /// Indicator to automatically logout (T/F)
        /// </summary>        
        public bool AutoLogout { get; set; }

        /// <summary>
        /// Indicator to allow offline transactions (T/F)
        /// </summary>        
        public bool AllowOfflineTransactions { get; set; }

        /// <summary>
        /// Indicator to allow offline payments (T/F)
        /// </summary>        
        public bool AllowOfflinePayments { get; set; }

        /// <summary>
        /// Indicator to allow payments (T/F)
        /// </summary>        
        public bool AllowPayments { get; set; }

        /// <summary>
        /// Freeze Customer Account Indicator (when?)
        /// </summary>        
        public bool FreezeCustomerAccount { get; set; }

        /// <summary>
        /// Indicator to balance check(?)
        /// </summary>        
        public bool BalanceCheck { get; set; }

        /// <summary>
        /// Indicator to display the meal served (T/F)
        /// </summary>        
        public bool DisplayMealsServed { get; set; }

        /// <summary>
        /// Allow event check attendance (T/F)
        /// </summary>        
        public bool AllowCheckAttendance { get; set; }

        /// <summary>
        /// Indicator allowing event transaction reversals (T/F)
        /// </summary>        
        public bool AllowReverse { get; set; }

        /// <summary>
        /// Indicates if the POS can process event transactions (T/F)
        /// </summary>        
        public bool AllowEventTransaction { get; set; }

        /// <summary>
        /// Indicator allowing credit card call center authorizations (T/F)
        /// </summary>        
        public bool CreditCardOfflineCallCenterAuth { get; set; }

        /// <summary>
        /// Indicator allowing screen saver (T/F)
        /// </summary>        
        public bool ScreenSaver { get; set; }

        /// <summary>
        /// Screen Saver Timeout (in minutes)
        /// </summary>        
        public int ScreenSaverTimeout { get; set; }

        /// <summary>
        /// Indicator enabling pole screen saver (T/F)
        /// </summary>        
        public bool PoleScreenSaver { get; set; }

        /// <summary>
        /// Pole Display Timeout (in minutes)
        /// </summary>        
        public int PoleDisplayTimeout { get; set; }
    }

    /// <summary>
    /// View for a <see cref="PosOptionFeatureAvailability"/>.  (Version 1)
    /// </summary>
    public class PosOptionFeatureAvailabilityViewV01
    {
        /// <summary>
        /// Allow no tax
        /// </summary>        
        public bool AllowNoTax { get; set; }

        /// <summary>
        /// Allow count display
        /// </summary>        
        public bool AllowCountDisplay { get; set; }

        /// <summary>
        /// Indicator to Auto Continue Board Transactions (T/F)
        /// </summary>        
        public bool BoardAutoContinue { get; set; }

        /// <summary>
        /// Turnstilerelay Enabled
        /// </summary>
        public bool TurnstilerelayEnabled { get; set; }

        /// <summary>
        /// Card Utility Enabled
        /// </summary>
        public bool CardUtilityEnabled { get; set; }

        /// <summary>
        /// Indicator to automatically logout (T/F)
        /// </summary>        
        public bool AutoLogout { get; set; }

        /// <summary>
        /// Indicator to allow offline transactions (T/F)
        /// </summary>        
        public bool AllowOfflineTransactions { get; set; }

        /// <summary>
        /// Indicator to allow offline payments (T/F)
        /// </summary>        
        public bool AllowOfflinePayments { get; set; }

        /// <summary>
        /// Indicator to allow payments (T/F)
        /// </summary>        
        public bool AllowPayments { get; set; }

        /// <summary>
        /// Freeze Customer Account Indicator (when?)
        /// </summary>        
        public bool FreezeCustomerAccount { get; set; }

        /// <summary>
        /// Indicator to balance check(?)
        /// </summary>        
        public bool BalanceCheck { get; set; }

        /// <summary>
        /// Indicator to display the meal served (T/F)
        /// </summary>        
        public bool DisplayMealsServed { get; set; }

        /// <summary>
        /// Allow event check attendance (T/F)
        /// </summary>        
        public bool AllowCheckAttendance { get; set; }

        /// <summary>
        /// Indicator allowing event transaction reversals (T/F)
        /// </summary>        
        public bool AllowReverse { get; set; }

        /// <summary>
        /// Indicates if the POS can process event transactions (T/F)
        /// </summary>        
        public bool AllowEventTransaction { get; set; }

        /// <summary>
        /// Indicator allowing credit card call center authorizations (T/F)
        /// </summary>        
        public bool CreditCardOfflineCallCenterAuth { get; set; }

        /// <summary>
        /// Indicator allowing screen saver (T/F)
        /// </summary>        
        public bool ScreenSaver { get; set; }

        /// <summary>
        /// Screen Saver Timeout (in minutes)
        /// </summary>        
        public int ScreenSaverTimeout { get; set; }

        /// <summary>
        /// Indicator enabling pole screen saver (T/F)
        /// </summary>        
        public bool PoleScreenSaver { get; set; }

        /// <summary>
        /// Pole Display Timeout (in minutes)
        /// </summary>        
        public int PoleDisplayTimeout { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="PosOptionFeatureAvailability"/> conversion.
    /// </summary>
    public static class PosOptionFeatureAvailabilityConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="PosOptionFeatureAvailabilityViewV01"/> object based on this <see cref="PosOptionFeatureAvailability"/>.
        /// </summary>
        /// <param name="posOptionFeatureAvailability"></param>
        /// <returns></returns>
        public static PosOptionFeatureAvailabilityViewV01 ToPosOptionFeatureAvailabilityViewV01(this PosOptionFeatureAvailability posOptionFeatureAvailability)
        {
            if (posOptionFeatureAvailability == null) return null;

            return new PosOptionFeatureAvailabilityViewV01
            {
                AllowNoTax = posOptionFeatureAvailability.AllowNoTax,
                AllowCountDisplay = posOptionFeatureAvailability.AllowCountDisplay,
                BoardAutoContinue = posOptionFeatureAvailability.BoardAutoContinue,
                TurnstilerelayEnabled = posOptionFeatureAvailability.TurnstilerelayEnabled,
                CardUtilityEnabled = posOptionFeatureAvailability.CardUtilityEnabled,
                AutoLogout = posOptionFeatureAvailability.AutoLogout,
                AllowOfflineTransactions = posOptionFeatureAvailability.AllowOfflineTransactions,
                AllowOfflinePayments = posOptionFeatureAvailability.AllowOfflinePayments,
                AllowPayments = posOptionFeatureAvailability.AllowPayments,
                FreezeCustomerAccount = posOptionFeatureAvailability.FreezeCustomerAccount,
                BalanceCheck = posOptionFeatureAvailability.BalanceCheck,
                DisplayMealsServed = posOptionFeatureAvailability.DisplayMealsServed,
                AllowCheckAttendance = posOptionFeatureAvailability.AllowCheckAttendance,
                AllowReverse = posOptionFeatureAvailability.AllowReverse,
                AllowEventTransaction = posOptionFeatureAvailability.AllowEventTransaction,
                CreditCardOfflineCallCenterAuth = posOptionFeatureAvailability.CreditCardOfflineCallCenterAuth,
                ScreenSaver = posOptionFeatureAvailability.ScreenSaver,
                ScreenSaverTimeout = posOptionFeatureAvailability.ScreenSaverTimeout,
                PoleScreenSaver = posOptionFeatureAvailability.PoleScreenSaver,
                PoleDisplayTimeout = posOptionFeatureAvailability.PoleDisplayTimeout
            };
        }

        /// <summary>
        /// Returns a <see cref="PosOptionFeatureAvailability"/> object based on this <see cref="PosOptionFeatureAvailabilityViewV01"/>.
        /// </summary>
        /// <param name="posOptionFeatureAvailabilityViewV01"></param>
        /// <returns></returns>
        public static PosOptionFeatureAvailability ToPosOptionFeatureAvailability(this PosOptionFeatureAvailabilityViewV01 posOptionFeatureAvailabilityViewV01)
        {
            if (posOptionFeatureAvailabilityViewV01 == null) return null;

            return new PosOptionFeatureAvailability
            {
                AllowNoTax = posOptionFeatureAvailabilityViewV01.AllowNoTax,
                AllowCountDisplay = posOptionFeatureAvailabilityViewV01.AllowCountDisplay,
                BoardAutoContinue = posOptionFeatureAvailabilityViewV01.BoardAutoContinue,
                TurnstilerelayEnabled = posOptionFeatureAvailabilityViewV01.TurnstilerelayEnabled,
                CardUtilityEnabled = posOptionFeatureAvailabilityViewV01.CardUtilityEnabled,
                AutoLogout = posOptionFeatureAvailabilityViewV01.AutoLogout,
                AllowOfflineTransactions = posOptionFeatureAvailabilityViewV01.AllowOfflineTransactions,
                AllowOfflinePayments = posOptionFeatureAvailabilityViewV01.AllowOfflinePayments,
                AllowPayments = posOptionFeatureAvailabilityViewV01.AllowPayments,
                FreezeCustomerAccount = posOptionFeatureAvailabilityViewV01.FreezeCustomerAccount,
                BalanceCheck = posOptionFeatureAvailabilityViewV01.BalanceCheck,
                DisplayMealsServed = posOptionFeatureAvailabilityViewV01.DisplayMealsServed,
                AllowCheckAttendance = posOptionFeatureAvailabilityViewV01.AllowCheckAttendance,
                AllowReverse = posOptionFeatureAvailabilityViewV01.AllowReverse,
                AllowEventTransaction = posOptionFeatureAvailabilityViewV01.AllowEventTransaction,
                CreditCardOfflineCallCenterAuth = posOptionFeatureAvailabilityViewV01.CreditCardOfflineCallCenterAuth,
                ScreenSaver = posOptionFeatureAvailabilityViewV01.ScreenSaver,
                ScreenSaverTimeout = posOptionFeatureAvailabilityViewV01.ScreenSaverTimeout,
                PoleScreenSaver = posOptionFeatureAvailabilityViewV01.PoleScreenSaver,
                PoleDisplayTimeout = posOptionFeatureAvailabilityViewV01.PoleDisplayTimeout
            };
        }
        #endregion
    }
}
