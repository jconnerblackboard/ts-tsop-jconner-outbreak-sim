﻿using BbTS.Domain.Models.Definitions.Pos;

namespace BbTS.Domain.Models.Pos.PosOptions
{
    /// <summary>
    /// Pos Option Constraints
    /// </summary>
    public class PosOptionConstraints
    {
        /// <summary>
        /// Indicator to Allow Subtotal on Exit (T/F)
        /// </summary>        
        public bool AllowSubtotalExit { get; set; }

        /// <summary>
        /// Indicator to limit transactions (T/F)
        /// </summary>        
        public bool LimitTransactions { get; set; }

        /// <summary>
        /// Limit Transactions Amount
        /// </summary>        
        public decimal? LimitTransactionsAmount { get; set; }

        /// <summary>
        /// Indicator to limit cash (T/F)
        /// </summary>        
        public bool LimitCash { get; set; }

        /// <summary>
        /// Limit Cash Amount
        /// </summary>        
        public decimal? LimitCashAmount { get; set; }

        /// <summary>
        /// Message Delay in milliseconds
        /// </summary>        
        public int MessageDelay { get; set; }

        /// <summary>
        /// Indicator to enable confirmation beeps (T/F)
        /// </summary>        
        public bool ConfirmationBeep { get; set; }

        /// <summary>
        /// Product Number Format Mask
        /// </summary>        
        public string ProductMask { get; set; }

        /// <summary>
        /// Purchase Entry Type (0) Amount - summarize all lines (1) Itemize - record each line for Transaction Terminals
        /// </summary>        
        public PurchaseEntryType StoredValueCashEntryType { get; set; }

        /// <summary>
        /// Card Display Mask Right Offset
        /// </summary>
        public int CardDisplayMaskRightOffset { get; set; }

        /// <summary>
        /// Card Display Mask Length
        /// </summary>
        public int CardDisplayMaskLength { get; set; }

        /// <summary>
        /// Customer number display format
        /// </summary>
        public string CustomerNumberDisplayFormat { get; set; }
    }

    /// <summary>
    /// View for a <see cref="PosOptionConstraints"/>.  (Version 1)
    /// </summary>
    public class PosOptionConstraintsViewV01
    {
        /// <summary>
        /// Indicator to Allow Subtotal on Exit (T/F)
        /// </summary>        
        public bool AllowSubtotalExit { get; set; }

        /// <summary>
        /// Indicator to limit transactions (T/F)
        /// </summary>        
        public bool LimitTransactions { get; set; }

        /// <summary>
        /// Limit Transactions Amount
        /// </summary>        
        public decimal? LimitTransactionsAmount { get; set; }

        /// <summary>
        /// Indicator to limit cash (T/F)
        /// </summary>        
        public bool LimitCash { get; set; }

        /// <summary>
        /// Limit Cash Amount
        /// </summary>        
        public decimal? LimitCashAmount { get; set; }

        /// <summary>
        /// Message Delay in milliseconds
        /// </summary>        
        public int MessageDelay { get; set; }

        /// <summary>
        /// Indicator to enable confirmation beeps (T/F)
        /// </summary>        
        public bool ConfirmationBeep { get; set; }

        /// <summary>
        /// Product Number Format Mask
        /// </summary>        
        public string ProductMask { get; set; }

        /// <summary>
        /// Purchase Entry Type (0) Amount - summarize all lines (1) Itemize - record each line for Transaction Terminals
        /// </summary>        
        public PurchaseEntryType StoredValueCashEntryType { get; set; }

        /// <summary>
        /// Card Display Mask Right Offset
        /// </summary>
        public int CardDisplayMaskRightOffset { get; set; }

        /// <summary>
        /// Card Display Mask Length
        /// </summary>
        public int CardDisplayMaskLength { get; set; }

        /// <summary>
        /// Customer number display format
        /// </summary>
        public string CustomerNumberDisplayFormat { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="PosOptionConstraints"/> conversion.
    /// </summary>
    public static class PosOptionConstraintsConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="PosOptionConstraintsViewV01"/> object based on this <see cref="PosOptionConstraints"/>.
        /// </summary>
        /// <param name="posOptionConstraints"></param>
        /// <returns></returns>
        public static PosOptionConstraintsViewV01 ToPosOptionConstraintsViewV01(this PosOptionConstraints posOptionConstraints)
        {
            if (posOptionConstraints == null) return null;

            return new PosOptionConstraintsViewV01
            {
                AllowSubtotalExit = posOptionConstraints.AllowSubtotalExit,
                LimitTransactions = posOptionConstraints.LimitTransactions,
                LimitTransactionsAmount = posOptionConstraints.LimitTransactionsAmount,
                LimitCash = posOptionConstraints.LimitCash,
                LimitCashAmount = posOptionConstraints.LimitCashAmount,
                MessageDelay = posOptionConstraints.MessageDelay,
                ConfirmationBeep = posOptionConstraints.ConfirmationBeep,
                ProductMask = posOptionConstraints.ProductMask,
                StoredValueCashEntryType = posOptionConstraints.StoredValueCashEntryType,
                CardDisplayMaskRightOffset = posOptionConstraints.CardDisplayMaskRightOffset,
                CardDisplayMaskLength = posOptionConstraints.CardDisplayMaskLength,
                CustomerNumberDisplayFormat = posOptionConstraints.CustomerNumberDisplayFormat
            };
        }

        /// <summary>
        /// Returns a <see cref="PosOptionConstraints"/> object based on this <see cref="PosOptionConstraintsViewV01"/>.
        /// </summary>
        /// <param name="posOptionConstraintsViewV01"></param>
        /// <returns></returns>
        public static PosOptionConstraints ToPosOptionConstraints(this PosOptionConstraintsViewV01 posOptionConstraintsViewV01)
        {
            if (posOptionConstraintsViewV01 == null) return null;

            return new PosOptionConstraints
            {
                AllowSubtotalExit = posOptionConstraintsViewV01.AllowSubtotalExit,
                LimitTransactions = posOptionConstraintsViewV01.LimitTransactions,
                LimitTransactionsAmount = posOptionConstraintsViewV01.LimitTransactionsAmount,
                LimitCash = posOptionConstraintsViewV01.LimitCash,
                LimitCashAmount = posOptionConstraintsViewV01.LimitCashAmount,
                MessageDelay = posOptionConstraintsViewV01.MessageDelay,
                ConfirmationBeep = posOptionConstraintsViewV01.ConfirmationBeep,
                ProductMask = posOptionConstraintsViewV01.ProductMask,
                StoredValueCashEntryType = posOptionConstraintsViewV01.StoredValueCashEntryType,
                CardDisplayMaskRightOffset = posOptionConstraintsViewV01.CardDisplayMaskRightOffset,
                CardDisplayMaskLength = posOptionConstraintsViewV01.CardDisplayMaskLength,
                CustomerNumberDisplayFormat = posOptionConstraintsViewV01.CustomerNumberDisplayFormat
            };
        }
        #endregion
    }
}
