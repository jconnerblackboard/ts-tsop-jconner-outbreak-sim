﻿
namespace BbTS.Domain.Models.Pos.PosOptions
{
    /// <summary>
    /// Pos Option Receipt Print Flags
    /// </summary>
    public class PosOptionReceiptPrintFlags
    {
        /// <summary>
        /// Indicator to Print "I Agree to Pay..." for Credit Card on the receipt (T/F)
        /// </summary>        
        public bool ReceiptPrintAgreeCreditCard { get; set; }

        /// <summary>
        /// Indicator to Print Audit on Cashier Logoff (T/F)
        /// </summary>        
        public bool ReceiptPrintAudit { get; set; }

        /// <summary>
        /// Indicator to print customer name on receipt (T/F)
        /// </summary>        
        public bool ReceiptPrintCustomerName { get; set; }

        /// <summary>
        /// Indicator to print customer number on receipt (T/F)
        /// </summary>        
        public bool ReceiptPrintCustomerNumber { get; set; }

        /// <summary>
        /// Indicator to print customer balance on receipt (T/F)
        /// </summary>        
        public bool ReceiptPrintCustomerBalance { get; set; }

        /// <summary>
        /// Indicator to print card number on receipt (T/F)
        /// </summary>        
        public bool ReceiptPrintCardNumber { get; set; }

        /// <summary>
        /// Indicator to Show Amount Deducted for Stored Value Tender Types (T/F)
        /// </summary>        
        public bool ReceiptPrintDollars { get; set; }

        /// <summary>
        /// Indicator to print any transaction comments on the receipt (T/F)
        /// </summary>        
        public bool ReceiptPrintTransComment { get; set; }

        /// <summary>
        /// Indicator to print the balance below the limit on the receipt (T/F)
        /// </summary>        
        public bool ReceiptPrintBalanceBelow { get; set; }

        /// <summary>
        /// Print on Receipt - Balance Below Threshold Amount
        /// </summary>        
        public decimal? ReceiptBalanceBelowAmount { get; set; }

        /// <summary>
        /// Flag denoting whether the Print Board Cash Value Limit should be printed on receipts. 
        /// </summary>        
        public bool PrintBoardCashValueLimit { get; set; }
    }

    /// <summary>
    /// View for a <see cref="PosOptionReceiptPrintFlags"/>.  (Version 1)
    /// </summary>
    public class PosOptionReceiptPrintFlagsViewV01
    {
        /// <summary>
        /// Indicator to Print "I Agree to Pay..." for Credit Card on the receipt (T/F)
        /// </summary>        
        public bool ReceiptPrintAgreeCreditCard { get; set; }

        /// <summary>
        /// Indicator to Print Audit on Cashier Logoff (T/F)
        /// </summary>        
        public bool ReceiptPrintAudit { get; set; }

        /// <summary>
        /// Indicator to print customer name on receipt (T/F)
        /// </summary>        
        public bool ReceiptPrintCustomerName { get; set; }

        /// <summary>
        /// Indicator to print customer number on receipt (T/F)
        /// </summary>        
        public bool ReceiptPrintCustomerNumber { get; set; }

        /// <summary>
        /// Indicator to print customer balance on receipt (T/F)
        /// </summary>        
        public bool ReceiptPrintCustomerBalance { get; set; }

        /// <summary>
        /// Indicator to print card number on receipt (T/F)
        /// </summary>        
        public bool ReceiptPrintCardNumber { get; set; }

        /// <summary>
        /// Indicator to Show Amount Deducted for Stored Value Tender Types (T/F)
        /// </summary>        
        public bool ReceiptPrintDollars { get; set; }

        /// <summary>
        /// Indicator to print any transaction comments on the receipt (T/F)
        /// </summary>        
        public bool ReceiptPrintTransComment { get; set; }

        /// <summary>
        /// Indicator to print the balance below the limit on the receipt (T/F)
        /// </summary>        
        public bool ReceiptPrintBalanceBelow { get; set; }

        /// <summary>
        /// Print on Receipt - Balance Below Threshold Amount
        /// </summary>        
        public decimal? ReceiptBalanceBelowAmount { get; set; }

        /// <summary>
        /// Flag denoting whether the Print Board Cash Value Limit should be printed on receipts. 
        /// </summary>        
        public bool PrintBoardCashValueLimit { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="PosOptionReceiptPrintFlags"/> conversion.
    /// </summary>
    public static class PosOptionReceiptPrintFlagsConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="PosOptionReceiptPrintFlagsViewV01"/> object based on this <see cref="PosOptionReceiptPrintFlags"/>.
        /// </summary>
        /// <param name="posOptionReceiptPrintFlags"></param>
        /// <returns></returns>
        public static PosOptionReceiptPrintFlagsViewV01 ToPosOptionReceiptPrintFlagsViewV01(this PosOptionReceiptPrintFlags posOptionReceiptPrintFlags)
        {
            if (posOptionReceiptPrintFlags == null) return null;

            return new PosOptionReceiptPrintFlagsViewV01
            {
                ReceiptPrintAgreeCreditCard = posOptionReceiptPrintFlags.ReceiptPrintAgreeCreditCard,
                ReceiptPrintAudit = posOptionReceiptPrintFlags.ReceiptPrintAudit,
                ReceiptPrintCustomerName = posOptionReceiptPrintFlags.ReceiptPrintCustomerName,
                ReceiptPrintCustomerNumber = posOptionReceiptPrintFlags.ReceiptPrintCustomerNumber,
                ReceiptPrintCustomerBalance = posOptionReceiptPrintFlags.ReceiptPrintCustomerBalance,
                ReceiptPrintCardNumber = posOptionReceiptPrintFlags.ReceiptPrintCardNumber,
                ReceiptPrintDollars = posOptionReceiptPrintFlags.ReceiptPrintDollars,
                ReceiptPrintTransComment = posOptionReceiptPrintFlags.ReceiptPrintTransComment,
                ReceiptPrintBalanceBelow = posOptionReceiptPrintFlags.ReceiptPrintBalanceBelow,
                ReceiptBalanceBelowAmount = posOptionReceiptPrintFlags.ReceiptBalanceBelowAmount,
                PrintBoardCashValueLimit = posOptionReceiptPrintFlags.PrintBoardCashValueLimit
            };
        }

        /// <summary>
        /// Returns a <see cref="PosOptionReceiptPrintFlags"/> object based on this <see cref="PosOptionReceiptPrintFlagsViewV01"/>.
        /// </summary>
        /// <param name="posOptionReceiptPrintFlagsViewV01"></param>
        /// <returns></returns>
        public static PosOptionReceiptPrintFlags ToPosOptionReceiptPrintFlags(this PosOptionReceiptPrintFlagsViewV01 posOptionReceiptPrintFlagsViewV01)
        {
            if (posOptionReceiptPrintFlagsViewV01 == null) return null;

            return new PosOptionReceiptPrintFlags
            {
                ReceiptPrintAgreeCreditCard = posOptionReceiptPrintFlagsViewV01.ReceiptPrintAgreeCreditCard,
                ReceiptPrintAudit = posOptionReceiptPrintFlagsViewV01.ReceiptPrintAudit,
                ReceiptPrintCustomerName = posOptionReceiptPrintFlagsViewV01.ReceiptPrintCustomerName,
                ReceiptPrintCustomerNumber = posOptionReceiptPrintFlagsViewV01.ReceiptPrintCustomerNumber,
                ReceiptPrintCustomerBalance = posOptionReceiptPrintFlagsViewV01.ReceiptPrintCustomerBalance,
                ReceiptPrintCardNumber = posOptionReceiptPrintFlagsViewV01.ReceiptPrintCardNumber,
                ReceiptPrintDollars = posOptionReceiptPrintFlagsViewV01.ReceiptPrintDollars,
                ReceiptPrintTransComment = posOptionReceiptPrintFlagsViewV01.ReceiptPrintTransComment,
                ReceiptPrintBalanceBelow = posOptionReceiptPrintFlagsViewV01.ReceiptPrintBalanceBelow,
                ReceiptBalanceBelowAmount = posOptionReceiptPrintFlagsViewV01.ReceiptBalanceBelowAmount,
                PrintBoardCashValueLimit = posOptionReceiptPrintFlagsViewV01.PrintBoardCashValueLimit
            };
        }
        #endregion
    }
}
