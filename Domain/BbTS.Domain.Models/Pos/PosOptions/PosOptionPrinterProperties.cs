﻿
namespace BbTS.Domain.Models.Pos.PosOptions
{
    /// <summary>
    /// Pos Option Printer Properties
    /// </summary>
    public class PosOptionPrinterProperties
    {
        /// <summary>
        /// User printer
        /// </summary>
        public bool UsePrinter { get; set; }

        /// <summary>
        /// Indicator to set the printer on by default (T/F)
        /// </summary>        
        public bool PrinterOnByDefault { get; set; }

        /// <summary>
        /// Register should combine identical product line items on receipt and remote printer (T/F)
        /// </summary>        
        public bool ReceiptGroupProduct { get; set; }

        /// <summary>
        /// Header
        /// </summary>
        public PosOptionPrinterReceipt Header { get; set; } = new PosOptionPrinterReceipt();

        /// <summary>
        /// Coupon
        /// </summary>
        public PosOptionPrinterReceipt Coupon { get; set; } = new PosOptionPrinterReceipt();

        /// <summary>
        /// Footer
        /// </summary>
        public PosOptionPrinterReceipt Footer { get; set; } = new PosOptionPrinterReceipt();

        /// <summary>
        /// Receipt print flags
        /// </summary>
        public PosOptionReceiptPrintFlags ReceiptPrintFlags { get; set; } = new PosOptionReceiptPrintFlags();

        /// <summary>
        /// Force print flags
        /// </summary>
        public PosOptionForcePrintFlags ForcePrintFlags { get; set; } = new PosOptionForcePrintFlags();
    }

    /// <summary>
    /// View for a <see cref="PosOptionPrinterProperties"/>.  (Version 1)
    /// </summary>
    public class PosOptionPrinterPropertiesViewV01
    {
        /// <summary>
        /// User printer
        /// </summary>
        public bool UsePrinter { get; set; }

        /// <summary>
        /// Indicator to set the printer on by default (T/F)
        /// </summary>        
        public bool PrinterOnByDefault { get; set; }

        /// <summary>
        /// Register should combine identical product line items on receipt and remote printer (T/F)
        /// </summary>        
        public bool ReceiptGroupProduct { get; set; }

        /// <summary>
        /// Header
        /// </summary>
        public PosOptionPrinterReceiptViewV01 Header { get; set; } = new PosOptionPrinterReceiptViewV01();

        /// <summary>
        /// Coupon
        /// </summary>
        public PosOptionPrinterReceiptViewV01 Coupon { get; set; } = new PosOptionPrinterReceiptViewV01();

        /// <summary>
        /// Footer
        /// </summary>
        public PosOptionPrinterReceiptViewV01 Footer { get; set; } = new PosOptionPrinterReceiptViewV01();

        /// <summary>
        /// Receipt print flags
        /// </summary>
        public PosOptionReceiptPrintFlagsViewV01 ReceiptPrintFlags { get; set; } = new PosOptionReceiptPrintFlagsViewV01();

        /// <summary>
        /// Force print flags
        /// </summary>
        public PosOptionForcePrintFlagsViewV01 ForcePrintFlags { get; set; } = new PosOptionForcePrintFlagsViewV01();
    }

    /// <summary>
    /// Extension methods for <see cref="PosOptionPrinterProperties"/> conversion.
    /// </summary>
    public static class PosOptionPrinterPropertiesConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="PosOptionPrinterPropertiesViewV01"/> object based on this <see cref="PosOptionPrinterProperties"/>.
        /// </summary>
        /// <param name="posOptionPrinterProperties"></param>
        /// <returns></returns>
        public static PosOptionPrinterPropertiesViewV01 ToPosOptionPrinterPropertiesViewV01(this PosOptionPrinterProperties posOptionPrinterProperties)
        {
            if (posOptionPrinterProperties == null) return null;

            return new PosOptionPrinterPropertiesViewV01
            {
                UsePrinter = posOptionPrinterProperties.UsePrinter,
                PrinterOnByDefault = posOptionPrinterProperties.PrinterOnByDefault,
                ReceiptGroupProduct = posOptionPrinterProperties.ReceiptGroupProduct,
                Header = posOptionPrinterProperties.Header.ToPosOptionPrinterReceiptViewV01(),
                Coupon = posOptionPrinterProperties.Coupon.ToPosOptionPrinterReceiptViewV01(),
                Footer = posOptionPrinterProperties.Footer.ToPosOptionPrinterReceiptViewV01(),
                ReceiptPrintFlags = posOptionPrinterProperties.ReceiptPrintFlags.ToPosOptionReceiptPrintFlagsViewV01(),
                ForcePrintFlags = posOptionPrinterProperties.ForcePrintFlags.ToPosOptionForcePrintFlagsViewV01()
            };
        }

        /// <summary>
        /// Returns a <see cref="PosOptionPrinterProperties"/> object based on this <see cref="PosOptionPrinterPropertiesViewV01"/>.
        /// </summary>
        /// <param name="posOptionPrinterPropertiesViewV01"></param>
        /// <returns></returns>
        public static PosOptionPrinterProperties ToPosOptionPrinterProperties(this PosOptionPrinterPropertiesViewV01 posOptionPrinterPropertiesViewV01)
        {
            if (posOptionPrinterPropertiesViewV01 == null) return null;

            return new PosOptionPrinterProperties
            {
                UsePrinter = posOptionPrinterPropertiesViewV01.UsePrinter,
                PrinterOnByDefault = posOptionPrinterPropertiesViewV01.PrinterOnByDefault,
                ReceiptGroupProduct = posOptionPrinterPropertiesViewV01.ReceiptGroupProduct,
                Header = posOptionPrinterPropertiesViewV01.Header.ToPosOptionPrinterReceipt(),
                Coupon = posOptionPrinterPropertiesViewV01.Coupon.ToPosOptionPrinterReceipt(),
                Footer = posOptionPrinterPropertiesViewV01.Footer.ToPosOptionPrinterReceipt(),
                ReceiptPrintFlags = posOptionPrinterPropertiesViewV01.ReceiptPrintFlags.ToPosOptionReceiptPrintFlags(),
                ForcePrintFlags = posOptionPrinterPropertiesViewV01.ForcePrintFlags.ToPosOptionForcePrintFlags()
            };
        }
        #endregion
    }
}
