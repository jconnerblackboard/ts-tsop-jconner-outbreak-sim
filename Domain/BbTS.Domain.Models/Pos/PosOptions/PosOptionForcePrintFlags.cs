﻿
namespace BbTS.Domain.Models.Pos.PosOptions
{
    /// <summary>
    /// Pos Option Force Print Flags
    /// </summary>
    public class PosOptionForcePrintFlags
    {
        /// <summary>
        /// Indicator forcing the printing of cash transactions on the receipt (T/F)
        /// </summary>        
        public bool ForcePrintCash { get; set; }

        /// <summary>
        /// Indicator to force the printing check transactions on the receipt (T/F)
        /// </summary>        
        public bool ForcePrintCheck { get; set; }

        /// <summary>
        /// Indicator to print stored value transactions on the receipt (T/F)
        /// </summary>        
        public bool ForcePrintStoredValueTrans { get; set; }

        /// <summary>
        /// Indicator to print Cash Equivalancy transactions on the receipt (T/F)
        /// </summary>        
        public bool ForcePrintCashEquivalent { get; set; }

        /// <summary>
        /// Indicator to print Paid Outs on the receipt (T/F)
        /// </summary>        
        public bool ForcePrintPaidOut { get; set; }

        /// <summary>
        /// Indicator to for the printing of Freezing Accounts on the receipt (T/F)
        /// </summary>        
        public bool ForcePrintFreezeAccount { get; set; }

        /// <summary>
        /// Indicator for the forcing of printing overrings on the receipt (T/F)
        /// </summary>        
        public bool ForcePrintOverrings { get; set; }

        /// <summary>
        /// Indicator for the forcing of printing stored value payments on the receipt (T/F)
        /// </summary>        
        public bool ForcePrintStoredValuePayment { get; set; }

        /// <summary>
        /// Indicates that a board transaction will be forced to print (T/F)
        /// </summary>        
        public bool ForcePrintBoard { get; set; }

        /// <summary>
        /// Indicates if an event transaction will be forced to print on the receipt (T/F)
        /// </summary>        
        public bool ForcePrintEvent { get; set; }
    }

    /// <summary>
    /// View for a <see cref="PosOptionForcePrintFlags"/>.  (Version 1)
    /// </summary>
    public class PosOptionForcePrintFlagsViewV01
    {
        /// <summary>
        /// Indicator forcing the printing of cash transactions on the receipt (T/F)
        /// </summary>        
        public bool ForcePrintCash { get; set; }

        /// <summary>
        /// Indicator to force the printing check transactions on the receipt (T/F)
        /// </summary>        
        public bool ForcePrintCheck { get; set; }

        /// <summary>
        /// Indicator to print stored value transactions on the receipt (T/F)
        /// </summary>        
        public bool ForcePrintStoredValueTrans { get; set; }

        /// <summary>
        /// Indicator to print Cash Equivalancy transactions on the receipt (T/F)
        /// </summary>        
        public bool ForcePrintCashEquivalent { get; set; }

        /// <summary>
        /// Indicator to print Paid Outs on the receipt (T/F)
        /// </summary>        
        public bool ForcePrintPaidOut { get; set; }

        /// <summary>
        /// Indicator to for the printing of Freezing Accounts on the receipt (T/F)
        /// </summary>        
        public bool ForcePrintFreezeAccount { get; set; }

        /// <summary>
        /// Indicator for the forcing of printing overrings on the receipt (T/F)
        /// </summary>        
        public bool ForcePrintOverrings { get; set; }

        /// <summary>
        /// Indicator for the forcing of printing stored value payments on the receipt (T/F)
        /// </summary>        
        public bool ForcePrintStoredValuePayment { get; set; }

        /// <summary>
        /// Indicates that a board transaction will be forced to print (T/F)
        /// </summary>        
        public bool ForcePrintBoard { get; set; }

        /// <summary>
        /// Indicates if an event transaction will be forced to print on the receipt (T/F)
        /// </summary>        
        public bool ForcePrintEvent { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="PosOptionForcePrintFlags"/> conversion.
    /// </summary>
    public static class PosOptionForcePrintFlagsConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="PosOptionForcePrintFlagsViewV01"/> object based on this <see cref="PosOptionForcePrintFlags"/>.
        /// </summary>
        /// <param name="posOptionForcePrintFlags"></param>
        /// <returns></returns>
        public static PosOptionForcePrintFlagsViewV01 ToPosOptionForcePrintFlagsViewV01(this PosOptionForcePrintFlags posOptionForcePrintFlags)
        {
            if (posOptionForcePrintFlags == null) return null;

            return new PosOptionForcePrintFlagsViewV01
            {
                ForcePrintCash = posOptionForcePrintFlags.ForcePrintCash,
                ForcePrintCheck = posOptionForcePrintFlags.ForcePrintCheck,
                ForcePrintStoredValueTrans = posOptionForcePrintFlags.ForcePrintStoredValueTrans,
                ForcePrintCashEquivalent = posOptionForcePrintFlags.ForcePrintCashEquivalent,
                ForcePrintPaidOut = posOptionForcePrintFlags.ForcePrintPaidOut,
                ForcePrintFreezeAccount = posOptionForcePrintFlags.ForcePrintFreezeAccount,
                ForcePrintOverrings = posOptionForcePrintFlags.ForcePrintOverrings,
                ForcePrintStoredValuePayment = posOptionForcePrintFlags.ForcePrintStoredValuePayment,
                ForcePrintBoard = posOptionForcePrintFlags.ForcePrintBoard,
                ForcePrintEvent = posOptionForcePrintFlags.ForcePrintEvent
            };
        }

        /// <summary>
        /// Returns a <see cref="PosOptionForcePrintFlags"/> object based on this <see cref="PosOptionForcePrintFlagsViewV01"/>.
        /// </summary>
        /// <param name="posOptionForcePrintFlagsViewV01"></param>
        /// <returns></returns>
        public static PosOptionForcePrintFlags ToPosOptionForcePrintFlags(this PosOptionForcePrintFlagsViewV01 posOptionForcePrintFlagsViewV01)
        {
            if (posOptionForcePrintFlagsViewV01 == null) return null;

            return new PosOptionForcePrintFlags
            {
                ForcePrintCash = posOptionForcePrintFlagsViewV01.ForcePrintCash,
                ForcePrintCheck = posOptionForcePrintFlagsViewV01.ForcePrintCheck,
                ForcePrintStoredValueTrans = posOptionForcePrintFlagsViewV01.ForcePrintStoredValueTrans,
                ForcePrintCashEquivalent = posOptionForcePrintFlagsViewV01.ForcePrintCashEquivalent,
                ForcePrintPaidOut = posOptionForcePrintFlagsViewV01.ForcePrintPaidOut,
                ForcePrintFreezeAccount = posOptionForcePrintFlagsViewV01.ForcePrintFreezeAccount,
                ForcePrintOverrings = posOptionForcePrintFlagsViewV01.ForcePrintOverrings,
                ForcePrintStoredValuePayment = posOptionForcePrintFlagsViewV01.ForcePrintStoredValuePayment,
                ForcePrintBoard = posOptionForcePrintFlagsViewV01.ForcePrintBoard,
                ForcePrintEvent = posOptionForcePrintFlagsViewV01.ForcePrintEvent
            };
        }
        #endregion
    }
}
