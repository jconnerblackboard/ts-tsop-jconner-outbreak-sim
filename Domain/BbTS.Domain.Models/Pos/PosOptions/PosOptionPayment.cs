﻿using BbTS.Domain.Models.Definitions.Pos;

namespace BbTS.Domain.Models.Pos.PosOptions
{
    /// <summary>
    /// Pos Option Payment
    /// </summary>
    public class PosOptionPayment
    {
        /// <summary>
        /// Payment type
        /// </summary>
        public PaymentType Type { get; set; }

        /// <summary>
        /// Amount
        /// </summary>        
        public decimal Amount { get; set; }
    }

    /// <summary>
    /// View for a <see cref="PosOptionPayment"/>.  (Version 1)
    /// </summary>
    public class PosOptionPaymentViewV01
    {
        /// <summary>
        /// Payment type
        /// </summary>
        public PaymentType Type { get; set; }

        /// <summary>
        /// Amount
        /// </summary>        
        public decimal Amount { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="PosOptionPayment"/> conversion.
    /// </summary>
    public static class PosOptionPaymentConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="PosOptionPaymentViewV01"/> object based on this <see cref="PosOptionPayment"/>.
        /// </summary>
        /// <param name="posOptionPayment"></param>
        /// <returns></returns>
        public static PosOptionPaymentViewV01 ToPosOptionPaymentViewV01(this PosOptionPayment posOptionPayment)
        {
            if (posOptionPayment == null) return null;

            return new PosOptionPaymentViewV01
            {
                Type = posOptionPayment.Type,
                Amount = posOptionPayment.Amount
            };
        }

        /// <summary>
        /// Returns a <see cref="PosOptionPayment"/> object based on this <see cref="PosOptionPaymentViewV01"/>.
        /// </summary>
        /// <param name="posOptionPaymentViewV01"></param>
        /// <returns></returns>
        public static PosOptionPayment ToPosOptionPayment(this PosOptionPaymentViewV01 posOptionPaymentViewV01)
        {
            if (posOptionPaymentViewV01 == null) return null;

            return new PosOptionPayment
            {
                Type = posOptionPaymentViewV01.Type,
                Amount = posOptionPaymentViewV01.Amount
            };
        }
        #endregion
    }
}
