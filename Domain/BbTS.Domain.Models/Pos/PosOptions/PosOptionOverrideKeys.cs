﻿
namespace BbTS.Domain.Models.Pos.PosOptions
{
    /// <summary>
    /// Pos Option Override Keys
    /// </summary>
    public class PosOptionOverrideKeys
    {
        /// <summary>
        /// Discount key 1
        /// </summary>
        public PosOptionPayment DiscountKey1 { get; set; } = new PosOptionPayment();

        /// <summary>
        /// Discount key 2
        /// </summary>
        public PosOptionPayment DiscountKey2 { get; set; } = new PosOptionPayment();

        /// <summary>
        /// Discount key 3
        /// </summary>
        public PosOptionPayment DiscountKey3 { get; set; } = new PosOptionPayment();

        /// <summary>
        /// Discount key 4
        /// </summary>
        public PosOptionPayment DiscountKey4 { get; set; } = new PosOptionPayment();

        /// <summary>
        /// Surcharge key 1
        /// </summary>
        public PosOptionPayment SurchargeKey1 { get; set; } = new PosOptionPayment();

        /// <summary>
        /// Surcharge key 2
        /// </summary>
        public PosOptionPayment SurchargeKey2 { get; set; } = new PosOptionPayment();

        /// <summary>
        /// Tax key 1
        /// </summary>
        public PosOptionPayment TaxKey1 { get; set; } = new PosOptionPayment();

        /// <summary>
        /// Tax key 2
        /// </summary>
        public PosOptionPayment TaxKey2 { get; set; } = new PosOptionPayment();
    }

    /// <summary>
    /// View for a <see cref="PosOptionOverrideKeys"/>.  (Version 1)
    /// </summary>
    public class PosOptionOverrideKeysViewV01
    {
        /// <summary>
        /// Discount key 1
        /// </summary>
        public PosOptionPaymentViewV01 DiscountKey1 { get; set; } = new PosOptionPaymentViewV01();

        /// <summary>
        /// Discount key 2
        /// </summary>
        public PosOptionPaymentViewV01 DiscountKey2 { get; set; } = new PosOptionPaymentViewV01();

        /// <summary>
        /// Discount key 3
        /// </summary>
        public PosOptionPaymentViewV01 DiscountKey3 { get; set; } = new PosOptionPaymentViewV01();

        /// <summary>
        /// Discount key 4
        /// </summary>
        public PosOptionPaymentViewV01 DiscountKey4 { get; set; } = new PosOptionPaymentViewV01();

        /// <summary>
        /// Surcharge key 1
        /// </summary>
        public PosOptionPaymentViewV01 SurchargeKey1 { get; set; } = new PosOptionPaymentViewV01();

        /// <summary>
        /// Surcharge key 2
        /// </summary>
        public PosOptionPaymentViewV01 SurchargeKey2 { get; set; } = new PosOptionPaymentViewV01();

        /// <summary>
        /// Tax key 1
        /// </summary>
        public PosOptionPaymentViewV01 TaxKey1 { get; set; } = new PosOptionPaymentViewV01();

        /// <summary>
        /// Tax key 2
        /// </summary>
        public PosOptionPaymentViewV01 TaxKey2 { get; set; } = new PosOptionPaymentViewV01();
    }

    /// <summary>
    /// Extension methods for <see cref="PosOptionOverrideKeys"/> conversion.
    /// </summary>
    public static class PosOptionOverrideKeysConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="PosOptionOverrideKeysViewV01"/> object based on this <see cref="PosOptionOverrideKeys"/>.
        /// </summary>
        /// <param name="posOptionOverrideKeys"></param>
        /// <returns></returns>
        public static PosOptionOverrideKeysViewV01 ToPosOptionOverrideKeysViewV01(this PosOptionOverrideKeys posOptionOverrideKeys)
        {
            if (posOptionOverrideKeys == null) return null;

            return new PosOptionOverrideKeysViewV01
            {
                DiscountKey1 = posOptionOverrideKeys.DiscountKey1.ToPosOptionPaymentViewV01(),
                DiscountKey2 = posOptionOverrideKeys.DiscountKey2.ToPosOptionPaymentViewV01(),
                DiscountKey3 = posOptionOverrideKeys.DiscountKey3.ToPosOptionPaymentViewV01(),
                DiscountKey4 = posOptionOverrideKeys.DiscountKey4.ToPosOptionPaymentViewV01(),
                SurchargeKey1 = posOptionOverrideKeys.SurchargeKey1.ToPosOptionPaymentViewV01(),
                SurchargeKey2 = posOptionOverrideKeys.SurchargeKey2.ToPosOptionPaymentViewV01(),
                TaxKey1 = posOptionOverrideKeys.TaxKey1.ToPosOptionPaymentViewV01(),
                TaxKey2 = posOptionOverrideKeys.TaxKey2.ToPosOptionPaymentViewV01()
            };
        }

        /// <summary>
        /// Returns a <see cref="PosOptionOverrideKeys"/> object based on this <see cref="PosOptionOverrideKeysViewV01"/>.
        /// </summary>
        /// <param name="posOptionOverrideKeysViewV01"></param>
        /// <returns></returns>
        public static PosOptionOverrideKeys ToPosOptionOverrideKeys(this PosOptionOverrideKeysViewV01 posOptionOverrideKeysViewV01)
        {
            if (posOptionOverrideKeysViewV01 == null) return null;

            return new PosOptionOverrideKeys
            {
                DiscountKey1 = posOptionOverrideKeysViewV01.DiscountKey1.ToPosOptionPayment(),
                DiscountKey2 = posOptionOverrideKeysViewV01.DiscountKey2.ToPosOptionPayment(),
                DiscountKey3 = posOptionOverrideKeysViewV01.DiscountKey3.ToPosOptionPayment(),
                DiscountKey4 = posOptionOverrideKeysViewV01.DiscountKey4.ToPosOptionPayment(),
                SurchargeKey1 = posOptionOverrideKeysViewV01.SurchargeKey1.ToPosOptionPayment(),
                SurchargeKey2 = posOptionOverrideKeysViewV01.SurchargeKey2.ToPosOptionPayment(),
                TaxKey1 = posOptionOverrideKeysViewV01.TaxKey1.ToPosOptionPayment(),
                TaxKey2 = posOptionOverrideKeysViewV01.TaxKey2.ToPosOptionPayment()
            };
        }
        #endregion
    }
}
