﻿
namespace BbTS.Domain.Models.Pos.PosOptions
{
    /// <summary>
    /// Pos Option Default Values
    /// </summary>
    public class PosOptionDefaultValues
    {
        /// <summary>
        /// Default Board Plan Meal Type Association (from BOARDMEALTYPES)
        /// </summary>        
        public int? DefaultBoardMealTypeId { get; set; }

        /// <summary>
        /// Default Cash Equivalancy Transaction Board Meal Type Association (from BOARDMEALTYPES table)
        /// </summary>        
        public int? DefaultEquivBoardMealTypeId { get; set; }

        /// <summary>
        /// Default Board Plan Id
        /// </summary>        
        public int? DefaultBoardPlanId { get; set; }

        /// <summary>
        /// Product Id
        /// </summary>        
        public int? ProductId { get; set; }
    }

    /// <summary>
    /// View for a <see cref="PosOptionDefaultValues"/>.  (Version 1)
    /// </summary>
    public class PosOptionDefaultValuesViewV01
    {
        /// <summary>
        /// Default Board Plan Meal Type Association (from BOARDMEALTYPES)
        /// </summary>        
        public int? DefaultBoardMealTypeId { get; set; }

        /// <summary>
        /// Default Cash Equivalancy Transaction Board Meal Type Association (from BOARDMEALTYPES table)
        /// </summary>        
        public int? DefaultEquivBoardMealTypeId { get; set; }

        /// <summary>
        /// Default Board Plan Id
        /// </summary>        
        public int? DefaultBoardPlanId { get; set; }

        /// <summary>
        /// Product Id
        /// </summary>        
        public int? ProductId { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="PosOptionDefaultValues"/> conversion.
    /// </summary>
    public static class PosOptionDefaultValuesConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="PosOptionDefaultValuesViewV01"/> object based on this <see cref="PosOptionDefaultValues"/>.
        /// </summary>
        /// <param name="posOptionDefaultValues"></param>
        /// <returns></returns>
        public static PosOptionDefaultValuesViewV01 ToPosOptionDefaultValuesViewV01(this PosOptionDefaultValues posOptionDefaultValues)
        {
            if (posOptionDefaultValues == null) return null;

            return new PosOptionDefaultValuesViewV01
            {
                DefaultBoardMealTypeId = posOptionDefaultValues.DefaultBoardMealTypeId,
                DefaultEquivBoardMealTypeId = posOptionDefaultValues.DefaultEquivBoardMealTypeId,
                DefaultBoardPlanId = posOptionDefaultValues.DefaultBoardPlanId,
                ProductId = posOptionDefaultValues.ProductId
            };
        }

        /// <summary>
        /// Returns a <see cref="PosOptionDefaultValues"/> object based on this <see cref="PosOptionDefaultValuesViewV01"/>.
        /// </summary>
        /// <param name="posOptionDefaultValuesViewV01"></param>
        /// <returns></returns>
        public static PosOptionDefaultValues ToPosOptionDefaultValues(this PosOptionDefaultValuesViewV01 posOptionDefaultValuesViewV01)
        {
            if (posOptionDefaultValuesViewV01 == null) return null;

            return new PosOptionDefaultValues
            {
                DefaultBoardMealTypeId = posOptionDefaultValuesViewV01.DefaultBoardMealTypeId,
                DefaultEquivBoardMealTypeId = posOptionDefaultValuesViewV01.DefaultEquivBoardMealTypeId,
                DefaultBoardPlanId = posOptionDefaultValuesViewV01.DefaultBoardPlanId,
                ProductId = posOptionDefaultValuesViewV01.ProductId
            };
        }
        #endregion
    }
}
