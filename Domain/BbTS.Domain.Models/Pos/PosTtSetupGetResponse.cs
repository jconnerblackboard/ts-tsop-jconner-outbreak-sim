﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Pos
{
    /// <summary>
    /// Container for the response to a pos group get request.
    /// </summary>
    public class PosTtSetupGetResponse
    {
        /// <summary>
        /// Parameterless constructor for (de)serialization.
        /// </summary>
        public PosTtSetupGetResponse() {}

        /// <summary>
        /// The requested list of Group.  When PosGuid is specified in the request, the list will have 1 entry.
        /// </summary>
        public List<PosTtSetup> Properties { get; set; } = new List<PosTtSetup>();

        /// <summary>
        /// The original request for this reponse
        /// </summary>
        public int? Request { get; set; }

        /// <summary>
        /// Parameterized constructor that requires the request id and the request.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="properties"></param>
        public PosTtSetupGetResponse(int? request, List<PosTtSetup> properties)
        {
            Request = request;
            Properties = properties;
        }
    }
}
