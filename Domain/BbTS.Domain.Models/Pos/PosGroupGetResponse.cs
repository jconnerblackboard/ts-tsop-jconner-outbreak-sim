﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Pos
{
    /// <summary>
    /// Container for the response to a pos group get request.
    /// </summary>
    public class PosGroupGetResponse
    {
        /// <summary>
        /// Parameterless constructor for (de)serialization.
        /// </summary>
        public PosGroupGetResponse()
        {
        }

        /// <summary>
        /// The unique identifier for this post request and response.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The requested list of Group.  When PosGuid is specified in the request, the list will have 1 entry.
        /// </summary>
        public List<TsPosGroup> Properties { get; set; } = new List<TsPosGroup>();

        /// <summary>
        /// The original request for this reponse
        /// </summary>
        public PosGroupGetRequest Request { get; set; }

        /// <summary>
        /// Parameterized constructor that requires the request id and the request.
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="request"></param>
        /// <param name="properties"></param>
        public PosGroupGetResponse(string requestId, PosGroupGetRequest request, List<TsPosGroup> properties)
        {
            RequestId = requestId;
            Request = request;
            Properties = properties;
        }
    }
}
