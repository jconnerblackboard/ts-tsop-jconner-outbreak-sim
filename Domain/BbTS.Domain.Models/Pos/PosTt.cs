﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Pos
{
    /// <summary>
    /// This object represents a Transact pos_tt object. Point of Sale - Transaction Terminal
    /// </summary>
    [Serializable]
    [DataContract]
    public class PosTt
    {
        /// <summary>
        /// The Identity of the table
        /// </summary>
        [DataMember]
        public int Pos_Id { get; set; }
        /// <summary>
        /// Indicates if the TT can use cash drawer (T/F)
        /// </summary>
        [DataMember]
        public string Use_Cash_Drawer { get; set; }
        /// <summary>
        /// Indicates if the Transaction Terminal can use a printer (T/F)
        /// </summary>
        [DataMember]
        public string Use_Printer { get; set; }
        /// <summary>
        /// Whether a turnstile is enabled
        /// </summary>
        [DataMember]
        public string TurnstileRelay_Enabled { get; set; }
        /// <summary>
        /// Point of sale options
        /// </summary>
        /// [DataMember]
        [DataMember]
        public int? Pos_Option_Id { get; set; }
        /// <summary>
        /// The FK to the Pos_Tt_Setup table
        /// </summary>
        [DataMember]
        public int? Pos_Tt_Setup_Id { get; set; }
        /// <summary>
        /// Whether the card utility is enabled or not
        /// </summary>
        [DataMember]
        public string Card_Utility_Enabled { get; set; }
        /// <summary>
        /// 1 - AT3000, 2 - MF4100, 3 = PR5000
        /// </summary>
        [DataMember]
        public int Terminal_Type { get; set; }
    }
}