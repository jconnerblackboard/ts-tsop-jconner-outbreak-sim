﻿namespace BbTS.Domain.Models.Pos
{
    /// <summary>
    /// Pos group.
    /// </summary>
    public class PosGroup
    {
        /// <summary>
        /// Identifier assigned by BbTS DB.
        /// </summary>
        public int PosGroupId { get; set; }

        /// <summary>
        /// Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Parent pos group instance.
        /// </summary>
        public PosGroup Parent { get; set; }
    }

    /// <summary>
    /// This class rerepsents a TS PosGroup
    /// </summary>
    public class TsPosGroup : PosGroup
    {
        /// <summary>
        /// The merchant id of this group
        /// </summary>
        public int MerchantId { get; set; }

        /// <summary>
        /// The parent group Id
        /// </summary>
        public int ParentGroupId { get; set; }
    }
}