﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Pos
{
    /// <summary>
    /// Container for the response to a pos properties get request.
    /// </summary>
    public class PosPropertiesGetResponse
    {
        /// <summary>
        /// Parameterless constructor for (de)serialization.
        /// </summary>
        public PosPropertiesGetResponse()
        {
        }

        /// <summary>
        /// The unique identifier for the pos (null if all pos properties were requested.)
        /// </summary>
        public string PosGuid { get; internal set; }

        /// <summary>
        /// The requested list of properties.  When PosGuid is specified in the request, the list will have 1 entry.
        /// </summary>
        public List<PosProperties> Properties { get; set; } = new List<PosProperties>();

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="posGuid">The unique identifier for the pos (null if all pos properties were requested.)</param>
        public PosPropertiesGetResponse(string posGuid)
        {
            PosGuid = posGuid;
        }
    }
}
