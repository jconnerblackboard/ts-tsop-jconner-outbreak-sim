﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Pos
{
    /// <summary>
    /// Container for the response to a pos properties get request.
    /// </summary>
    public class PosGroupGetRequest
    {
        /// <summary>
        /// Parameterless constructor for (de)serialization.
        /// </summary>
        public PosGroupGetRequest()
        {
        }

        /// <summary>
        /// The unique identifier for this post request and response.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The unique identifier for the pos group (null if all pos groups were requested.)
        /// </summary>
        public int? PosGroupId { get; internal set; }

        /// <summary>
        /// The unique identifier for the profitcenter (null if all were requested.)
        /// </summary>
        public int? ProfitCenterId { get; internal set; }

        /// <summary>
        /// The requested list of properties.  When PosGuid is specified in the request, the list will have 1 entry.
        /// </summary>
        public List<TsPosGroup> Properties { get; set; } = new List<TsPosGroup>();

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="posGroupId">The unique identifier for the pos (null if all pos properties were requested.)</param>
        /// <param name="profitCenterId">The profit center to use for this request</param>
        public PosGroupGetRequest(string requestId, int? posGroupId, int? profitCenterId)
        {
            RequestId = requestId;
            PosGroupId = posGroupId;
            ProfitCenterId = profitCenterId;
        }
    }
}
