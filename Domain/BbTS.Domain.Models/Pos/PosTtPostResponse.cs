﻿namespace BbTS.Domain.Models.Pos
{
    /// <summary>
    /// Container for a response to a request for a pos properties post.
    /// </summary>
    public class PosTtPostResponse
    {

        /// <summary>
        /// Parameterless constructor for (de)serialization.
        /// </summary>
        public PosTtPostResponse() {}

        /// <summary>
        /// The unique identifier for this post request and response.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The request that spawned this response.
        /// </summary>
        public PosTtPostRequest Request { get; set; }

        /// <summary>
        /// The properties that were created as part of this operation.
        /// </summary>
        public PosTt Properties { get; set; }

        /// <summary>
        /// Parameterized constructor that requires the request id and the request.
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="request"></param>
        /// <param name="properties"></param>
        public PosTtPostResponse(string requestId, PosTtPostRequest request, PosTt properties)
        {
            Request = request;
            RequestId = requestId;
            Properties = properties;
        }
    }
}
