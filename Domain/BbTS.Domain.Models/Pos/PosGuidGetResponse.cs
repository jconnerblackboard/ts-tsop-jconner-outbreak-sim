﻿using System;

namespace BbTS.Domain.Models.Pos
{
    /// <summary>
    /// Container for the response to a pos guid get request.
    /// </summary>
    public class PosGuidGetResponse
    {
        /// <summary>
        /// Workstation Id
        /// </summary>
        public int? WorkstationId { get; set; }

        /// <summary>
        /// Workstation name
        /// </summary>
        public string WorkstationName { get; set; }

        /// <summary>
        /// Profit center Id
        /// </summary>
        public int ProfitCenterId { get; set; }

        /// <summary>
        /// Pos guid
        /// </summary>
        public Guid PosGuid { get; set; }

        /// <summary>
        /// Pos Id
        /// </summary>
        public int PosId { get; set; }

        /// <summary>
        /// Pos name
        /// </summary>
        public string PosName { get; set; }
    }
}
