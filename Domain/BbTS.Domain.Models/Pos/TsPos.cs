﻿using System;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Pos
{
    /// <summary>
    /// This object represents a Transact Pos object. Point of Sale
    /// </summary>
    [Serializable]
    public class TsPos
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Pos_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int ProfitCenter_Id { get; set; }
        /// <summary>
        /// Name of POS
        /// </summary>
        [XmlAttribute]
        public string Name { get; set; }
        /// <summary>
        /// 01 - register (Pos_cr)
        /// 02 - transaction terminal(Pos_tt)
        /// 03 - vending_mf_reader(Pos_vending_mf_reader)
        /// 04 - copier(Pos_copier)
        /// 05 - laundry(Pos_laundry)
        /// 06 - (unassigned)
        /// 07 - Wireless Terminal(Pos_wirelesstt)
        /// 08 - marketplace(Pos_marketplace)
        /// 09 - transaction integration agent(Pos_Tia)
        /// 10 - account management center(Pos_amc)
        /// 11 - workstation(Pos_ws)
        /// 12 - value transfer station(Pos_vts)
        /// 13 - external client(Pos_external_client)
        /// 14 - gate fees
        /// </summary>
        [XmlAttribute]
        public int PosType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Pos_Group_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Bb_Paygate_Config_Store_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int CreditCardProcessingMethodId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int PaymentExpressGroupAccountId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public string EmvDeviceIdSuffix { get; set; }
    }
}