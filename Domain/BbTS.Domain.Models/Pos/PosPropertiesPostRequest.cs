﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace BbTS.Domain.Models.Pos
{
    /// <summary>
    /// Container for a request to set pos properties.
    /// </summary>
    public class PosPropertiesPostRequest : PosProperties
    {
        /// <summary>
        /// The unique identifier for this post request and response.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The numerical identifier of the pos tt the device belongs to.
        /// </summary>
        public int? PosTtId { get; set; }

        /// <summary>
        /// The numerical identifier of the pos tt setup the device is linked to.
        /// </summary>
        [Required]
        public int? PosTtSetupId { get; set; }

        /// <summary>
        /// The numerical identifier of the pos option the device is linked to.
        /// </summary>
        [Required]
        public int? PosOptionId { get; set; }
    }

    /// <summary>
    /// Container for a request to set pos properties with extra attributes necessary to support the Gui
    /// </summary>
    public class PosPropertiesPostRequestView : PosPropertiesPostRequest
    {
        /// <summary>
        /// The guid of the device
        /// </summary>
        public string DeviceGuid { get; set; }

        /// <summary>
        /// The ProfitCenter list of a PosPropertiesPostRequest object
        /// </summary>
        [DisplayName("Profit Center")]
        public List<SelectListItem> ProfitCenterList { get; set; }

        /// <summary>
        /// The Credit card processing list of a PosPropertiesPostRequest object
        /// </summary>
        [DisplayName("Credit Card Processing Method")]
        public List<SelectListItem> CreditCardProcessingMethodList { get; set; }

        /// <summary>
        /// The Payment Express Group Account processing list of a PosPropertiesPostRequest object
        /// </summary>
        [DisplayName("Payment Express Group Account")]
        public List<SelectListItem> PaymentExpressGroupAccountList { get; set; }

        /// <summary>
        /// The ProfitCenter list of a PosPropertiesPostRequest object
        /// </summary>
        [DisplayName("POS Group")]
        public List<SelectListItem> PosGroupList { get; set; }

        /// <summary>
        /// The Pos_Option list of the PosPropertiesPostRequest object
        /// </summary>
        [DisplayName("POS Option")]
        public List<SelectListItem> PosOptionList { get; set; }

        /// <summary>
        /// The Pos_Tt_Setup list of the PosPropertiesPostRequest object
        /// </summary>
        [DisplayName("POS TT Setup")]
        public List<SelectListItem> PosTtSetupList { get; set; }

        /// <summary>
        /// Use printer option
        /// </summary>
        [Required]
        [DisplayName("Use Printer")]
        public bool UsePrinter { get; set; }

        /// <summary>
        /// Use cash drawer option
        /// </summary>
        [Required]
        [DisplayName("Use Cash Drawer")]
        public bool UseCashDrawer { get; set; }

        /// <summary>
        /// Turnstile relay option
        /// </summary>
        [Required]
        [DisplayName("Turnstile Relay")]
        public bool TurnstileRelayEnabled { get; set; }

        /// <summary>
        /// Card utility option
        /// </summary>
        [Required]
        [DisplayName("Card Utility")]
        public bool CardUtilityEnabled { get; set; }

        [DisplayName("Install Software Updates")]
        /// <summary>
        /// The specific time when updates will be applied.
        /// </summary>
        public string UpdateTime { get; set; } = "2:00";
    }
}
