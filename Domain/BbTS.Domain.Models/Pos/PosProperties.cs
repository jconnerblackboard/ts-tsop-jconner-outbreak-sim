﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BbTS.Domain.Models.Pos
{
    /// <summary>
    /// Container class for pos properties.
    /// </summary>
    public class PosProperties
    {
        /// <summary>
        /// The numnerical identifier assigned to the pos entry associated with the device.
        /// </summary>
        public int PosId { get; set; }

        /// <summary>
        /// The numberical identifier for the profit center the device resides in.
        /// </summary>
        [Required]
        public int ProfitCenterId { get; set; }

        /// <summary>
        /// The name assigned to the pos representation of the device.
        /// </summary>
        [DisplayName("POS Name")]
        [Required, StringLength(30)]
        public string PosName { get; set; }

        /// <summary>
        /// The type of pos the device is associated with.
        /// </summary>
        public int PosType { get; set; }

        /// <summary>
        /// The numerical identifier of the pos group the device belongs to.
        /// </summary>
        public int? PosGroupId { get; set; }

        /// <summary>
        /// The BbPaygate Store Id.
        /// </summary>
        public int? BbPaygateStoreId { get; set; }

        /// <summary>
        /// The identifier of hte credit card processing method.
        /// </summary>
        public int? CreditCardProcessingMethodId { get; set; }

        /// <summary>
        /// The payment express group id expressed as a numberical value.
        /// </summary>
        public int? PaymentExpressGroupAccountId { get; set; }

        /// <summary>
        /// The suffix for the device when processing EMV transactions.
        /// </summary>
        [DisplayName("EMV Device Suffix")]
        public string EmvDeviceSuffix { get; set; }

        /// <summary>
        /// Emv hit station
        /// </summary>
        [DisplayName("Station")]
        [StringLength(32)]
        public string EmvStation { get; set; }

        /// <summary>
        /// The device id recorded in the POS entry for the device.  When fully registered, this value will match the DeviceId in the device table.
        /// </summary>
        public int? PosDeviceid { get; set; }

        /// <summary>
        /// The unique identifier (Guid) for the pos.
        /// </summary>
        public string PosGuid { get; set; }

        /// <summary>
        /// Pos credential settings
        /// </summary>
        public PosCredentialSettings PosCredentialSettings { get; set; }

        /// <summary>
        /// The time of day that the device should make a request for updated settings.  Defaults to 2:00 AM if no value supplied.
        /// </summary>
        public TimeSpan? SettingsUpdateTime { get; set; }
    }

    /// <summary>
    /// This method is used when human friendly descriptions are needed for web pages.
    /// </summary>
    public class PosPropertiesView : PosProperties
    {
        /// <summary>
        /// The name of the profit center
        /// </summary>
        [DisplayName("Profit Center Name")]
        public string ProfitCenterName { get; set; }
        /// <summary>
        /// The name of the merchant
        /// </summary>
        public string MerchantName { get; set; }
        /// <summary>
        /// The name of the associated device
        /// </summary>
        [DisplayName("Device Name")]
        public string DeviceName { get; set; }
        /// <summary>
        /// The guid of the device for this pos record
        /// </summary>
        public string DeviceGuid { get; set; }
    }
}
