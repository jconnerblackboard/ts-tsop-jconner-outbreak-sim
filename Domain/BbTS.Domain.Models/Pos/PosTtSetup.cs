﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming                                                                                            

namespace BbTS.Domain.Models.Pos
{
    /// <summary>                                                                                                                  
    /// This object represents a Transact Pos_Tt_Setup object. POS Transaction Terminal Setup                                      
    /// </summary>               
    [Serializable]
    [DataContract]
    public class PosTtSetup
    {
        /// <summary>                                                                                                              
        ///                                                                                                                        
        /// </summary>
        [DataMember]
        public int Pos_Tt_Setup_Id { get; set; }
        /// <summary>                                                                                                              
        ///                                                                                                                        
        /// </summary>
        [DataMember]
        public int Merchant_Id { get; set; }
        /// <summary>                                                                                                              
        ///                                                                                                                        
        /// </summary>
        [DataMember]
        public string Name { get; set; }
        /// <summary>                                                                                                              
        /// Send Customer Data after approved transaction (T/F)                                                                    
        /// </summary>
        [DataMember]
        public string Data_Output_Send_On_Approve { get; set; }
        /// <summary>                                                                                                              
        /// Send Customer Data after denied transaction (T/F)                                                                      
        /// </summary>
        [DataMember]
        public string Data_Output_Send_On_Deny { get; set; }
        /// <summary>                                                                                                              
        /// Constant column to define FK on DOMAIN table                                                                           
        /// </summary>
        [DataMember]
        public int Data_Output_Data_To_Send_Type { get; set; }
        /// <summary>                                                                                                              
        /// (1) Track 2 Data, (2) Card Number, (3) Customer Number, (4) Customer Defined Field or Customer Defined Group           
        /// </summary>     
        [DataMember]
        public int Data_Output_Data_To_Send_Id { get; set; }
        /// <summary>                                                                                                              
        /// Indicates if the terminal should send data when offline.  Only applies to Track 2 Data or Card Number (T/F)            
        /// </summary>
        [DataMember]
        public string Data_Output_Send_Data_Offline { get; set; }
        /// <summary>                                                                                                              
        /// Constant column to define FK on DOMAIN table                                                                           
        /// </summary>    
        [DataMember]
        public int Data_Output_Serial_Baud_Type { get; set; }
        /// <summary>                                                                                                              
        /// Baud rate, values 2400, 4800, 9600                                                                                     
        /// </summary>    
        [DataMember]
        public int Data_Output_Serial_Baud_Id { get; set; }
        /// <summary>                                                                                                              
        /// Constant column to define FK on DOMAIN table                                                                           
        /// </summary> 
        [DataMember]
        public int Data_Output_Serial_Bits_Type { get; set; }
        /// <summary>                                                                                                              
        /// Bits per byte to send on serial link, values 7 or 8                                                                    
        /// </summary> 
        [DataMember]
        public int Data_Output_Serial_Bits_Id { get; set; }
        /// <summary>                                                                                                              
        /// Constant column to define FK on DOMAIN table                                                                           
        /// </summary> 
        [DataMember]
        public int Data_Output_Serial_Stop_Type { get; set; }
        /// <summary>                                                                                                              
        /// Serial port stop bits (0) 1 Stop bit, (1) 1.5 Stop bits, (2) 2 Stop bits                                               
        /// </summary> 
        [DataMember]
        public int Data_Output_Serial_Stop_Id { get; set; }
        /// <summary>                                                                                                              
        /// Constant column to define FK on DOMAIN table                                                                           
        /// </summary> 
        [DataMember]
        public int Data_Output_Serial_Parity_Type { get; set; }
        /// <summary>                                                                                                              
        /// (0) No Parity, (1) Odd Parity, (2) Even Parity, (3) Mark Parity, (4) Space Parity                                      
        /// </summary>                                                                                                             
        [DataMember]
        public int Data_Output_Serial_Parity_Id { get; set; }
        /// <summary>                                                                                                              
        /// Constant column to define FK on DOMAIN table                                                                           
        /// </summary>                                                                                                             
        [DataMember]
        public int Data_Output_Prefix_Type { get; set; }
        /// <summary>                                                                                                              
        /// (1) None, (2) Carriage Return, (3) Line Feed, (4) Carriage Return + Line Feed, (5) Line Feed + Carriage Return, (6) Tab
        /// </summary>                                                                                                             
        [DataMember]
        public int Data_Output_Prefix_Id { get; set; }
        /// <summary>                                                                                                              
        /// Constant column to define FK on DOMAIN table                                                                           
        /// </summary>                                                                                                             
        [DataMember]
        public int Data_Output_Suffix_Type { get; set; }
        /// <summary>                                                                                                              
        /// (1) None, (2) Carriage Return, (3) Line Feed, (4) Carriage Return + Line Feed, (5) Line Feed + Carriage Return, (6) Tab
        /// </summary>                                                                                                             
        [DataMember]
        public int Data_Output_Suffix_Id { get; set; }
        /// <summary>                                                                                                              
        /// Constant column to define FK on DOMAIN table                                                                           
        /// </summary>                                                                                                             
        [DataMember]
        public int Data_Output_Padding_Type { get; set; }
        /// <summary>                                                                                                              
        /// (1) None, (2) Left Padding, (3) Right Padding                                                                          
        /// </summary>                                                                                                             
        [DataMember]
        public int Data_Output_Padding_Id { get; set; }
        /// <summary>                                                                                                              
        /// Minimum total length of numeric, string, or card number field after padding                                             
        /// </summary>                                                                                                             
        [DataMember]
        public int Data_Output_Pad_Length { get; set; }
        /// <summary>                                                                                                              
        ///                                                                                                                        
        /// </summary>                                                                                                             
        [DataMember]
        public string Data_Output_Pad_Value { get; set; }
        /// <summary>                                                                                                              
        /// Customer-defined field ID.  NULL if data output is not for a customer defined field                                    
        /// </summary>                                                                                                             
        [DataMember]
        public int Data_Output_Cust_Def_Fld_Id { get; set; }
        /// <summary>                                                                                                              
        /// Customer-defined group ID.  NULL if data output is not for a customer defined group                                    
        /// </summary>                                                                                                             
        [DataMember]
        public int Data_Output_Cust_Def_Grp_Id { get; set; }
        /// <summary>                                                                                                              
        /// Value to send for T values for Boolean CDF field                                                                       
        /// </summary>                                                                                                             
        [DataMember]
        public string Data_Output_Cdf_Bool_True { get; set; }
        /// <summary>                                                                                                              
        /// Value to send for F values for Boolean CDF field                                                                       
        /// </summary>                                                                                                             
        [DataMember]
        public string Data_Output_Cdf_Bool_False { get; set; }
        /// <summary>                                                                                                              
        /// Prefix to send before Dollar CDF field data                                                                            
        /// </summary>                                                                                                             
        [DataMember]
        public string Data_Output_Cdf_Dol_Prefix { get; set; }
        /// <summary>                                                                                                              
        /// Use decimal point when formatting Dollar CDF field data (T/F)                                                          
        /// </summary>                                                                                                             
        [DataMember]
        public string Data_Output_Cdf_Dol_Decimal { get; set; }
        /// <summary>                                                                                                              
        /// Maximum length to send for CDF Text field data                                                                         
        /// </summary>                                                                                                             
        [DataMember]
        public int Data_Output_Cdf_Text_Len { get; set; }
        /// <summary>                                                                                                              
        ///                                                                                                                        
        /// </summary>                                                                                                             
        [DataMember]
        public string Data_Output_LTrim_CustNumber { get; set; }
    }                                                                                                                              
}                                                                                                                                  