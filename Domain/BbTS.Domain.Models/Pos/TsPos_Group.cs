﻿using System;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Pos
{
    /// <summary>
    /// This object represents a Transact Pos Group object.
    /// </summary>
    [Serializable]
    public class TsPos_Group
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Pos_Group_Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Merchant_Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Parent_Pos_Group_Id { get; set; }
    }
}
