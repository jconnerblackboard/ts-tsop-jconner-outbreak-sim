﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Definitions.Device;
using BbTS.Domain.Models.Definitions.Door;

namespace BbTS.Domain.Models.Communication.BbHost
{
    /// <summary>
    /// Container class representing the response packet from a stats callback message.
    /// </summary>
    public class StatsResponsePacket
    {
        #region Envision Header

        /// <summary>
        /// Length of the message.
        /// </summary>
        public UInt16 Length { get; set; }

        /// <summary>
        /// The IdByte
        /// </summary>
        public byte IdByte { get; set; }

        /// <summary>
        /// The LenComp
        /// </summary>
        public UInt16 LenComp { get; set; }

        #endregion

        #region Inner Header

        /// <summary>
        /// The sequence number
        /// </summary>
        public UInt16 SequenceNumber { get; set; }

        /// <summary>
        /// The Pos Id
        /// </summary>
        public UInt32 PosId { get; set; }

        /// <summary>
        /// The Module Char
        /// </summary>
        public byte ModuleChar { get; set; }

        /// <summary>
        /// The Process Number
        /// </summary>
        public UInt16 ProcessNumber { get; set; }

        #endregion

        #region Stats Payload

        /// <summary>
        /// Zero
        /// </summary>
        public UInt16 Zero { get; set; }

        /// <summary>
        /// Stats Type
        /// </summary>
        public UInt16 StatsType { get; set; }

        /// <summary>
        /// Stats Id
        /// </summary>
        public UInt32 StatsId { get; set; }

        /// <summary>
        /// Session Count
        /// </summary>
        public UInt32 SessionCount { get; set; }

        /// <summary>
        /// Request Count
        /// </summary>
        public UInt32 RequestCount { get; set; }

        /// <summary>
        /// Bad LRC Count
        /// </summary>
        public UInt32 BadLrcCount { get; set; }

        /// <summary>
        /// Bad Header count
        /// </summary>
        public UInt32 BadHeaderCount { get; set; }

        /// <summary>
        /// No Response Count
        /// </summary>
        public UInt32 NoResponseCount { get; set; }

        /// <summary>
        /// Mode
        /// </summary>
        public byte Mode { get; set; }

        #endregion

        /// <summary>
        /// CRC
        /// </summary>
        public UInt16 Crc { get; set; }

        /// <summary>
        /// Initialization
        /// </summary>
        public StatsResponsePacket()
        {
            //-------------------------------------------------------------------------------------
            // Envision Header
            //-------------------------------------------------------------------------------------
            Length = 0;
            IdByte = 0;
            LenComp = 0;
            //-------------------------------------------------------------------------------------
            // Inner Header
            //-------------------------------------------------------------------------------------
            SequenceNumber = 0;
            PosId = 0;
            ModuleChar = 0;
            ProcessNumber = 0;
            //-------------------------------------------------------------------------------------
            // Stats Payload
            //-------------------------------------------------------------------------------------
            Zero = 0;
            StatsType = 0;
            StatsId = 0;
            SessionCount = 0;
            RequestCount = 0;
            BadLrcCount = 0;
            BadHeaderCount = 0;
            NoResponseCount = 0;
            Mode = (byte)DoorStateValue.Unknown;
            //-------------------------------------------------------------------------------------
            // CRC
            //-------------------------------------------------------------------------------------
            Crc = 0;
        }

        /// <summary>
        /// Clone this packet.
        /// </summary>
        /// <returns>Memberwise clone of this.</returns>
        public StatsResponsePacket Clone()
        {
            return (StatsResponsePacket)MemberwiseClone();
        }
    }
}
