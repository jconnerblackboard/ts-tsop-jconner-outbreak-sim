﻿using System;
using System.Collections.Generic;
using BbTS.Domain.Models.DoorAccess;

namespace BbTS.Domain.Models.Communication.BbHost
{
    /// <summary>
    /// Container class for information on a door stats connection to the host.
    /// </summary>
    public class StatsConnection
    {
        /// <summary>
        /// Unique identifier for this 
        /// </summary>
        public Guid Id { get; set; } = Guid.NewGuid();

        /// <summary>
        /// The name of the door corresponding to this stats connection.
        /// </summary>
        public string DoorName { get; set; }

        /// <summary>
        /// The unique identifier of the master controlller that controls the door to which this is attached.
        /// </summary>
        public int MasterControllerId { get; set; }
        
        /// <summary>
        /// The ip address of the host where the master controller is attached.
        /// </summary>
        public string HostConnectionIpAddress { get; set; }

        /// <summary>
        /// Get the port that the host is listening on for this door change request.
        /// </summary>
        public int HostConnectionPort { get; set; }

        /// <summary>
        /// The numerical identifier of the door to which this is attached.
        /// </summary>
        public int DoorId { get; set; }

        /// <summary>
        /// The address of the door on the master controller.
        /// </summary>
        public short DoorAddress { get; set; }

        /// <summary>
        /// Time that communication was first initiated with the host.
        /// </summary>
        public DateTimeOffset InitiatedTime { get; set; }

        /// <summary>
        /// Last time that the communications with the host was successful.
        /// </summary>
        public DateTimeOffset HostLastCommunicationTime { get; set; }

        /// <summary>
        /// Last time communications was successful to any client.
        /// </summary>
        public DateTimeOffset ClientLastCommunicatedTime { get; set; }

        /// <summary>
        /// The current status of the door.
        /// </summary>
        public DoorStatus DoorStatusCurrent { get; set; }

        /// <summary>
        /// The previous status of the door.
        /// </summary>
        public DoorStatus DoorStatusPrevious { get; set; }

        /// <summary>
        /// List of clients that are listening to the status of the door.
        /// </summary>
        public List<string> ListeningClients { get; set; } = new List<string>();

        /// <summary>
        /// Deep copy.
        /// </summary>
        /// <returns></returns>
        public StatsConnection Clone()
        {
            var clone = (StatsConnection)MemberwiseClone();
            clone.ListeningClients = new List<string>(ListeningClients);
            return clone;
        }
    }
}
