﻿namespace BbTS.Domain.Models.Communication
{
    /// <summary>
    /// Interface class to enable generic read/write operations to/from Azure Table Storage.
    /// </summary>
    public interface IAzureTableStorage
    {
        /// <summary>
        /// Set the partition key
        /// </summary>
        void SetPartitionKey(string key);

        /// <summary>
        /// Set the Row key.
        /// </summary>
        void SetRowKey(string key);
    }
}
