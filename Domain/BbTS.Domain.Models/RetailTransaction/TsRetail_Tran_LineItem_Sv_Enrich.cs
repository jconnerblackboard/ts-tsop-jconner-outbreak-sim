using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Retail_Tran_LineItem_Sv_Enrich
{
    /// <summary>
    /// This object represents a Transact Retail_Tran_LineItem_Sv_Enrich object. Retail Transaction Line Item Stored Value Enrichment Table
    /// </summary>
    [Serializable]
    public class TsRetail_Tran_LineItem_Sv_Enrich
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Transaction_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int LineItem_SequenceNumber { get; set; }
        /// <summary>
        /// The sequence number in context of this transaction in which the enrichment was given
        /// </summary>
        [XmlAttribute]
        public int Sv_Enrich_SequenceNumber { get; set; }
        /// <summary>
        /// Percentage of Enrichment (in millipercent)
        /// </summary>
        [XmlAttribute]
        public int Percentage { get; set; }
        /// <summary>
        /// The difference of the rounded amount after the calculation and the actual amount that was calculated without rounding.  Negative numbers indicate a loss from rounding while positive numbers indicate a gain from rounding.
        /// </summary>
        [XmlAttribute]
        public DateTime RoundingAmount { get; set; }
        /// <summary>
        /// Amount of Enrichment (in millidollars)
        /// </summary>
        [XmlAttribute]
        public int Amount { get; set; }
    }
}