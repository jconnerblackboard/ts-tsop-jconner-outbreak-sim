using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Sv_Account_History_Rtl_Trn_Sv
{
    /// <summary>
    /// This object represents a Transact Sv_Account_History_Rtl_Trn_Sv object. Stored Value History for Retail Transactions - Deposits
    /// </summary>
    [Serializable]
    public class TsSv_Account_History_Rtl_Trn_Sv
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Sv_Account_History_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Transaction_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int LineItem_SequenceNumber { get; set; }
    }
}