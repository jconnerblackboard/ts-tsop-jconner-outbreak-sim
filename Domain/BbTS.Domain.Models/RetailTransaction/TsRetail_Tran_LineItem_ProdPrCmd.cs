using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Retail_Tran_LineItem_ProdPrcMd
{
    /// <summary>
    /// This object represents a Transact Retail_Tran_LineItem_ProdPrCmd object. A line item modifier that reflects a modification of the retail selling price.
    /// </summary>
    [Serializable]
    public class TsRetail_Tran_LineItem_ProdPrcMd
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Transaction_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int LineItem_SequenceNumber { get; set; }
        /// <summary>
        /// The sequence number for this RetailPriceModifier allowing more than one price modification to occur on each retail transaction line item.
        /// </summary>
        [XmlAttribute]
        public int PriceModifier_SequenceNumber { get; set; }
        /// <summary>
        /// 0 - Discount Key
        /// 1 - Surcharge Key
        /// 2 - Promotion
        /// 3 - Price Override
        /// 4 - Variable Price
        /// 5 - Tender Discount
        /// 6 - Tender Surcharge
        /// 7 - Quantity Discount
        /// </summary>
        [XmlAttribute]
        public int RetailPrice_Modifier_Type { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int ProdPromo_Id { get; set; }
        /// <summary>
        /// The unit price that was used as the basis of the price modification.
        /// </summary>
        [XmlAttribute]
        public int Previous_Price { get; set; }
        /// <summary>
        /// The percent adjustment that was applied to the unit retail price to arrive at the modified selling price.
        /// </summary>
        [XmlAttribute]
        public int Percent { get; set; }
        /// <summary>
        /// The flat amount of the price adjustment that was removed from the unit selling price to arrive at the modified selling price.
        /// </summary>
        [XmlAttribute]
        public int Amount { get; set; }
        /// <summary>
        /// 0 - Percentage
        /// 1 - Amount
        /// 2 - Manually Entered Price
        /// 3 - Promotional Price
        /// </summary>
        [XmlAttribute]
        public int Calculation_Method_Type { get; set; }
        /// <summary>
        /// The unit price that was the result of the price modification.
        /// </summary>
        [XmlAttribute]
        public int New_Price { get; set; }
    }
}