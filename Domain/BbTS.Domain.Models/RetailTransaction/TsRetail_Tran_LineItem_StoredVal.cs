using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Retail_Tran_LineItem_StoredVal
{
    /// <summary>
    /// This object represents a Transact Retail_Tran_LineItem_StoredVal object. Retail Transaction Line Item - Stored Value Used
    /// </summary>
    [Serializable]
    public class TsRetail_Tran_LineItem_StoredVal
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Transaction_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int LineItem_SequenceNumber { get; set; }
        /// <summary>
        /// Amount of Stored Value Deposited (in millidollars)
        /// </summary>
        [XmlAttribute]
        public int StoredValue_Amount { get; set; }
        /// <summary>
        /// The amount charged the customer for a card purchase (in millidollars)
        /// </summary>
        [XmlAttribute]
        public int Card_Purchase_Fee_Amount { get; set; }
        /// <summary>
        /// Actual Unit Price (in millidollars).  The actual per-unit price paid by the customer for this particular sale. It is obtained by applying applicable price derivation rules to the regular unit price (retailprice).
        /// </summary>
        [XmlAttribute]
        public int ActualUnit_Price { get; set; }
        /// <summary>
        /// Unit Cost of the Stored Value Amount - cost to the retailer (in millidollars)
        /// </summary>
        [XmlAttribute]
        public int UnitCost_Price { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Sv_Account_Id { get; set; }
        /// <summary>
        /// Indicates if the stored value deposit was an enrichment
        /// </summary>
        [XmlAttribute]
        public string Enrichment_Flag { get; set; }
        /// <summary>
        /// If an enrichment, this sequence number in combination with the transaction_id, will point to the parents (base deposit), where this enrichment was based. 
        /// </summary>
        [XmlAttribute]
        public int Enrichment_Parent_LineItem_Seq { get; set; }
    }
}