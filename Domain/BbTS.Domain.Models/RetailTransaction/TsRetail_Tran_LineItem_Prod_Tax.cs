using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Retail_Tran_LineItem_Prod_Tax
{
    /// <summary>
    /// This object represents a Transact Retail_Tran_LineItem_Prod_Tax object. A Line Item to record taxation implications of a single Product Line Item rather than an entire RetailTransaction
    /// </summary>
    [Serializable]
    public class TsRetail_Tran_LineItem_Prod_Tax
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Transaction_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int LineItem_SequenceNumber { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int TaxSchedule_Id { get; set; }
        /// <summary>
        /// The  percentage of the taxable amount that is liable for tax.   Usually this is 100% but where an aggregate item contains taxed and non-taxed goods this value may be less than 100%.
        /// </summary>
        [XmlAttribute]
        public int Taxable_Percent { get; set; }
        /// <summary>
        /// The monetary amount for which tax is applicable.
        /// </summary>
        [XmlAttribute]
        public int Taxable_Amount { get; set; }
        /// <summary>
        /// The  percentage of the taxable portion of the taxable amount that is being collected as tax by this LineItem.
        /// </summary>
        [XmlAttribute]
        public int Tax_Percent { get; set; }
        /// <summary>
        /// The  monetary value tax that is being collected by this LineItem.
        /// </summary>
        [XmlAttribute]
        public int Tax_Amount { get; set; }
        /// <summary>
        /// 0 - Normal 1 - Tax Exempt 2 - Tax Override (Tax Key)
        /// </summary>
        [XmlAttribute]
        public int Tax_Exempt_Override_Type { get; set; }
    }
}