﻿using System;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.RetailTransaction
{
    /// <summary>
    /// This object represents a Transact Tender object. Tenders
    /// </summary>
    [Serializable]
    public class TsTender
    {
        /// <summary>
        /// Tender Identifier
        /// </summary>
        [XmlAttribute]
        public int Tender_Id { get; set; }
        /// <summary>
        /// 0 - [none] (cash, check)
        /// 1 - Internal(Stored Value)
        /// 2 - Credit Card
        /// ...
        /// n - (future examples: another 3rd party Auth )
        /// </summary>
        [XmlAttribute]
        public int Auth_Source { get; set; }
        /// <summary>
        /// (0) Cash, (1) Check, (2) Stored Value, (3) Credit Card, (4) Board to Cash Equivalent
        /// </summary>
        [XmlAttribute]
        public int Tender_Type { get; set; }
        /// <summary>
        /// Name of tender
        /// </summary>
        [XmlAttribute]
        public string Name { get; set; }
        /// <summary>
        /// Display name for devices that only support 10 character tender names
        /// </summary>
        [XmlAttribute]
        public string ShortName { get; set; }
        /// <summary>
        /// Code name of tender
        /// </summary>
        [XmlAttribute]
        public string Code_Name { get; set; }
        /// <summary>
        /// Key top Line 1 Caption
        /// </summary>
        [XmlAttribute]
        public string KeyTop1 { get; set; }
        /// <summary>
        /// Key top Line 2 Caption
        /// </summary>
        [XmlAttribute]
        public string KeyTop2 { get; set; }
    }
}