using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Transaction_Laundry_Machine
        {
        /// <summary>
        /// This object represents a Transact Transaction_Laundry_Machine object. Laundry Machine Transaction
        /// </summary>
        [Serializable]
        public class TsTransaction_Laundry_Machine
        {
          /// <summary>
          /// 
          /// </summary>
          [XmlAttribute]
          public int Transaction_Id { get; set; }
          /// <summary>
          /// Machine Number
          /// </summary>
          [XmlAttribute]
          public int MachineNum { get; set; }
          /// <summary>
          /// 
          /// </summary>
          [XmlAttribute]
          public int Pos_Id { get; set; }
        }
}