using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Retail_Tran_LineItem_Tndr_Cgv
{
    /// <summary>
    /// This object represents a Transact Retail_Tran_LineItem_Tndr_Cgv object. Retail Transaction Line Item Tender - Customer Group Values - what the customer belonged to at the time of the transaction
    /// </summary>
    [Serializable]
    public class TsRetail_Tran_LineItem_Tndr_Cgv
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Transaction_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int LineItem_SequenceNumber { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Customer_Def_Grp_Def_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Customer_Def_Grp_Def_Item_Id { get; set; }
    }
}