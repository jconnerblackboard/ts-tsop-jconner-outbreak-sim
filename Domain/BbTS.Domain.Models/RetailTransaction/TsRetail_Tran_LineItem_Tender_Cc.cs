using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Retail_Tran_LineItem_Tender_Cc
{
    /// <summary>
    /// This object represents a Transact Retail_Tran_LineItem_Tender_Cc object. Retail Transaction Line Item Tender - Credit Card Used
    /// </summary>
    [Serializable]
    public class TsRetail_Tran_LineItem_Tender_Cc
    {
        /// <summary>
        /// The FK to the Transaction table.
        /// </summary>
        [XmlAttribute]
        public int Transaction_Id { get; set; }
        /// <summary>
        /// The sequence number of this record.
        /// </summary>
        [XmlAttribute]
        public int LineItem_SequenceNumber { get; set; }
        /// <summary>
        /// AmericanExpress = 0
        /// China Union Pay = 1
        /// DinersClub = 2
        /// Discover = 3
        /// Japan Credit Bureau = 4
        /// Maestro = 5
        /// MasterCard = 6
        /// Solo = 7
        /// Switch = 8
        /// Visa = 9
        /// VisaElectron = 10
        /// Laser = 11
        /// Other = 12
        /// </summary>
        [XmlAttribute]
        public int Credit_Card_Type_Id { get; set; }
        /// <summary>
        /// Issuer Identification Number (IIN).  First 6 of the credit card number.
        /// </summary>
        [XmlAttribute]
        public int Credit_Card_Iin { get; set; }
        /// <summary>
        /// Last 4 of the credit card number
        /// </summary>
        [XmlAttribute]
        public int Credit_Card_Last4 { get; set; }
        /// <summary>
        /// The FK to the Bb_Paygate_Transaction_Id table.
        /// </summary>
        [XmlAttribute]
        public int CreditCardReferenceId { get; set; }
        /// <summary>
        /// Type of credit card processing used. 1=Host, 2=D2G, 3=EMV
        /// </summary>
        [XmlAttribute]
        public int CreditCardProcessingMethodId { get; set; }
    }
}