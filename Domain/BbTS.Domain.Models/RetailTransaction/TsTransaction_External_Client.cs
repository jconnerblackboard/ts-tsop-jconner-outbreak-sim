using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Transaction_External_Client
        {
        /// <summary>
        /// This object represents a Transact Transaction_External_Client object. Transaction to External Client Transaction Mapping Table
        /// </summary>
        [Serializable]
        public class TsTransaction_External_Client
        {
          /// <summary>
          /// 
          /// </summary>
          [XmlAttribute]
          public int Transaction_Id { get; set; }
          /// <summary>
          /// Identifier generated by the external client for the transaction
          /// </summary>
          [XmlAttribute]
          public string External_Client_Transactn_Guid { get; set; }
        }
}