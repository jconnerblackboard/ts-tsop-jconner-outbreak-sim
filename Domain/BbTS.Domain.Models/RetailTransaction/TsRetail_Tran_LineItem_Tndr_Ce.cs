using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Retail_Tran_LineItem_Tndr_Ce
{
    /// <summary>
    /// This object represents a Transact Retail_Tran_LineItem_Tndr_Ce object. Retail Transaction Line Item Tender - Cash Equivalent
    /// </summary>
    [Serializable]
    public class TsRetail_Tran_LineItem_Tndr_Ce
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Transaction_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int LineItem_SequenceNumber { get; set; }
        /// <summary>
        /// The maximum amount the customer could have spent during this transaction lineitem
        /// </summary>
        [XmlAttribute]
        public int CashEquiv_Amount_Limit { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int BoardMt_Id { get; set; }
        /// <summary>
        /// Guest Meal Indicator (T/F)
        /// </summary>
        [XmlAttribute]
        public string GuestMeal { get; set; }
    }
}