using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Retail_Tran_LineItem_Tndr_Cdv
{
    /// <summary>
    /// This object represents a Transact Retail_Tran_LineItem_Tndr_Cdv object. Retail Transaction Line Item Tender - Customer Defined Field Definitions (custom attributes)
    /// </summary>
    [Serializable]
    public class TsRetail_Tran_LineItem_Tndr_Cdv
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Transaction_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int LineItem_SequenceNumber { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Customer_Def_Field_Def_Id { get; set; }
        /// <summary>
        /// Attribute at the time of the transaction
        /// </summary>
        [XmlAttribute]
        public string Field_Value { get; set; }
    }
}