using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Retail_Tran_LineItem
{
    /// <summary>
    /// This object represents a Transact Retail_Tran_LineItem object. Retail Transaction Line Item
    /// </summary>
    [Serializable]
    public class TsRetail_Tran_LineItem
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Transaction_Id { get; set; }
        /// <summary>
        /// The sequence number of line item within the context of this Retail_Tran.
        /// </summary>
        [XmlAttribute]
        public int LineItem_SequenceNumber { get; set; }
        /// <summary>
        /// Indicates if the line item was voided (T/F)
        /// </summary>
        [XmlAttribute]
        public string Void_Flag { get; set; }
    }
}