﻿using BbTS.Domain.Models.Definitions.Transaction;

namespace BbTS.Domain.Models.RetailTransaction
{
    /// <summary>
    /// This object represents a Transact Tender object. Tenders
    /// </summary>
    public class TsTender2
    {
        /// <summary>
        /// Tender Identifier
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Auth source
        /// </summary>
        public AuthorizationSource AuthSource { get; set; }

        /// <summary>
        /// TenderType
        /// </summary>
        public TenderType Type { get; set; }

        /// <summary>
        /// Name of tender
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Display name for devices that only support 10 character tender names
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// Code name of tender
        /// </summary>
        public string CodeName { get; set; }

        /// <summary>
        /// Key top Line 1 Caption
        /// </summary>
        public string KeyTop1 { get; set; }

        /// <summary>
        /// Key top Line 2 Caption
        /// </summary>
        public string KeyTop2 { get; set; }
    }
}
