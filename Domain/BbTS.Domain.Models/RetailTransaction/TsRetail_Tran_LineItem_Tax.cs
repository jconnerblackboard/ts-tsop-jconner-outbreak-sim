using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Retail_Tran_LineItem_Tax
{
    /// <summary>
    /// This object represents a Transact Retail_Tran_LineItem_Tax object. Retail Transaction Line Item Tender - Tax
    /// </summary>
    [Serializable]
    public class TsRetail_Tran_LineItem_Tax
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Transaction_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int LineItem_SequenceNumber { get; set; }
        /// <summary>
        /// Key number pressed
        /// </summary>
        [XmlAttribute]
        public int Tax_KeyNum { get; set; }
        /// <summary>
        /// 0 - None
        /// 1 - Amount (Dollar Format)
        /// 2 - Open
        /// 3 - Percent (Pct Format)
        /// </summary>
        [XmlAttribute]
        public int Tax_Type { get; set; }
        /// <summary>
        /// Percent Taxable (in millipercent)
        /// </summary>
        [XmlAttribute]
        public int Taxable_Percent { get; set; }
        /// <summary>
        /// Taxable Amount (in millidollars)
        /// </summary>
        [XmlAttribute]
        public int Taxable_Amount { get; set; }
        /// <summary>
        /// Taxed Percent (in millipercent)
        /// </summary>
        [XmlAttribute]
        public int Tax_Percent { get; set; }
        /// <summary>
        /// Taxed Amount (in millidollars)
        /// </summary>
        [XmlAttribute]
        public int Tax_Amount { get; set; }
    }
}