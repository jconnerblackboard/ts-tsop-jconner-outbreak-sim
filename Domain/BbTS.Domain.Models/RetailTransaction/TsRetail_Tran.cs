﻿using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Retail_Tran
{
    /// <summary>
    /// This object represents a Transact retail_tran object. Retail Transaction
    /// </summary>
    [Serializable]
    public class TsRetail_Tran
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Transaction_Id { get; set; }
        /// <summary>
        ///	0 - Sale 
        ///   1 - Return
        /// </summary>
        [XmlAttribute]
        public int Retail_Tran_Type { get; set; }
        /// <summary>
        /// Number of the Cash Drawer
        /// </summary>
        [XmlAttribute]
        public int Cashdrawer_Num { get; set; }
        /// <summary>
        /// Units Sold for this transaction
        /// </summary>
        [XmlAttribute]
        public int Unit_Count { get; set; }
        /// <summary>
        /// Count of Line Items Scanned
        /// </summary>
        [XmlAttribute]
        public int LineItems_Scanned_Count { get; set; }
        /// <summary>
        /// Percent of Line Items Scanned (in millipercent)
        /// </summary>
        [XmlAttribute]
        public int LineItems_Scanned_Percent { get; set; }
        /// <summary>
        /// Count of Line Items Keyed
        /// </summary>
        [XmlAttribute]
        public int LineItems_Keyed_Count { get; set; }
        /// <summary>
        /// Percentage of Line Items Keyed (in millipercent)
        /// </summary>
        [XmlAttribute]
        public int LineItems_Keyed_Percent { get; set; }
        /// <summary>
        /// Datetime of receipt
        /// </summary>
        [XmlAttribute]
        public DateTime Receipt_DateTime { get; set; }
    }
}
