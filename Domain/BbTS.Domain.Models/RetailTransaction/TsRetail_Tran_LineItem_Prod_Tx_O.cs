using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Retail_Tran_LineItem_Prod_Tx_O
{
    /// <summary>
    /// This object represents a Transact Retail_Tran_LineItem_Prod_Tx_O object. A line item modifier to the SaleReturnTaxLineItem component of a RetailTransaction that provides supplementary data regarding tax overrides. (Where the amount of tax collected is reduced rather than exempted).
    /// </summary>
    [Serializable]
    public class TsRetail_Tran_LineItem_Prod_Tx_O
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Transaction_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int LineItem_SequenceNumber { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int TaxSchedule_Id { get; set; }
        /// <summary>
        /// 0 - TaxKey
        /// </summary>
        [XmlAttribute]
        public int Tax_Override_Reason_Type { get; set; }
        /// <summary>
        /// The monetary amount for which tax is applicable at the new rate.
        /// </summary>
        [XmlAttribute]
        public int Taxable_Amount { get; set; }
        /// <summary>
        /// The original amount of Tax that should've been collected but wasn't because of this TaxOverrideModifier.
        /// </summary>
        [XmlAttribute]
        public int Original_Tax_Amount { get; set; }
        /// <summary>
        /// The new amount of Tax that is being collected because of this TaxOverrideModifier.
        /// </summary>
        [XmlAttribute]
        public int New_Tax_Amount { get; set; }
        /// <summary>
        /// The original tax rate that should've been collected but wasn't because of this TaxOverrideModifier.
        /// </summary>
        [XmlAttribute]
        public int Original_Tax_Percent { get; set; }
        /// <summary>
        /// The new tax rate that is being collected  because of this TaxOverrideModifier.
        /// </summary>
        [XmlAttribute]
        public int New_Tax_Percent { get; set; }
    }
}