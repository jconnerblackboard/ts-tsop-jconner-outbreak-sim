using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.TiaTransactionLog
        {
        /// <summary>
        /// This object represents a Transact TiaTransactionLog object. 
        /// </summary>
        [Serializable]
        public class TsTiaTransactionLog
        {
          /// <summary>
          /// 
          /// </summary>
          [XmlAttribute]
          public string TransactionGuid { get; set; }
          /// <summary>
          /// 
          /// </summary>
          [XmlAttribute]
          public int TransactionId { get; set; }
          /// <summary>
          /// 
          /// </summary>
          [XmlAttribute]
          public int CustomerId { get; set; }
          /// <summary>
          /// 
          /// </summary>
          [XmlAttribute]
          public int AvailableBalance { get; set; }
          /// <summary>
          /// 
          /// </summary>
          [XmlAttribute]
          public int TenderId { get; set; }
          /// <summary>
          /// 
          /// </summary>
          [XmlAttribute]
          public int PosId { get; set; }
          /// <summary>
          /// 
          /// </summary>
          [XmlAttribute]
          public int ErrorId { get; set; }
          /// <summary>
          /// 
          /// </summary>
          [XmlAttribute]
          public int PostedDateTime { get; set; }
        }
}
