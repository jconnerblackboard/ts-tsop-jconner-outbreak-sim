using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Transaction_Communication
        {
        /// <summary>
        /// This object represents a Transact Transaction_Communication object. Transaction Communication Details
        /// </summary>
        [Serializable]
        public class TsTransaction_Communication
        {
          /// <summary>
          /// 
          /// </summary>
          [XmlAttribute]
          public int Transaction_Id { get; set; }
          /// <summary>
          /// 
          /// </summary>
          [XmlAttribute]
          public int Host_Computer_Id { get; set; }
          /// <summary>
          /// Version of the protocol
          /// </summary>
          [XmlAttribute]
          public string Protocol_Version { get; set; }
          /// <summary>
          /// The entire contents of the request
          /// </summary>
          [XmlAttribute]
          public string Request_Payload { get; set; }
          /// <summary>
          /// The entire contents of any reponses
          /// </summary>
          [XmlAttribute]
          public string Response_Payload { get; set; }
        }
}