using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Retail_Tran_Total
{
    /// <summary>
    /// This object represents a Transact Retail_Tran_Total object. Retail Transaction Total
    /// </summary>
    [Serializable]
    public class TsRetail_Tran_Total
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Transaction_Id { get; set; }
        /// <summary>
        /// Transaction Total (in millidollars)
        /// </summary>
        [XmlAttribute]
        public int Amount { get; set; }
    }
}