using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Retail_Tran_LineItem_ProdPromo
{
    /// <summary>
    /// This object represents a Transact Retail_Tran_LineItem_ProdPromo object. 
    /// </summary>
    [Serializable]
    public class TsRetail_Tran_LineItem_ProdPromo
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Transaction_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int LineItem_SequenceNumber { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int ProdPromo_Id { get; set; }
    }
}