﻿using System.Collections.Generic;
using System.Linq;

namespace BbTS.Domain.Models.RetailTransaction
{
    /// <summary>
    /// Container class for tax schedules on a device.
    /// </summary>
    public class TaxScheduleDeviceSetting
    {
        /// <summary>
        /// The tax schedule id.
        /// </summary>
        public int TaxScheduleId { get; set; }

        /// <summary>
        /// The name of the tax schedule.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The id of the tax account.
        /// </summary>
        public int TaxAccountId { get; set; }

        /// <summary>
        /// The name of the tax account.
        /// </summary>
        public string TaxAccountName { get; set; }

        /// <summary>
        /// Indicated if the tax exempt quantity is enabled.
        /// </summary>
        public bool TaxExemptQuantityEnabled { get; set; }

        /// <summary>
        /// The tax exempt quantity.
        /// </summary>
        public int TaxExemptQuantity { get; set; }

        /// <summary>
        /// Indicated if the taxable amount minimum is enabled.
        /// </summary>
        public bool TaxableAmountMinimumEnabled { get; set; }

        /// <summary>
        /// The taxable amount minimum value.
        /// </summary>
        public decimal TaxableAmountMinimum { get; set; }

        /// <summary>
        /// Indicated if the taxable amount maximum is enabled.
        /// </summary>
        public bool TaxableAmountMaximumEnabled { get; set; }

        /// <summary>
        /// The taxable amount maximum value.
        /// </summary>
        public decimal TaxableAmountMaximum { get; set; }

        /// <summary>
        /// The linked tax schedule id.
        /// </summary>
        public int TaxLinkTaxScheduleId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool TaxLinkTaxTaxes { get; set; }

        /// <summary>
        /// The tenders associated with this tax schedule.
        /// </summary>
        public List<TaxScheduleTender> TaxScheduleTenders { get; set; } = new List<TaxScheduleTender>();
    }

    /// <summary>
    /// View for a <see cref="TaxScheduleDeviceSetting"/>.  (Version 1)
    /// </summary>
    public class TaxScheduleDeviceSettingViewV01
    {
        /// <summary>
        /// The tax schedule id.
        /// </summary>
        public int TaxScheduleId { get; set; }

        /// <summary>
        /// The name of the tax schedule.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The id of the tax account.
        /// </summary>
        public int TaxAccountId { get; set; }

        /// <summary>
        /// The name of the tax account.
        /// </summary>
        public string TaxAccountName { get; set; }

        /// <summary>
        /// Indicated if the tax exempt quantity is enabled.
        /// </summary>
        public bool TaxExemptQuantityEnabled { get; set; }

        /// <summary>
        /// The tax exempt quantity.
        /// </summary>
        public int TaxExemptQuantity { get; set; }

        /// <summary>
        /// Indicated if the taxable amount minimum is enabled.
        /// </summary>
        public bool TaxableAmountMinimumEnabled { get; set; }

        /// <summary>
        /// The taxable amount minimum value.
        /// </summary>
        public decimal TaxableAmountMinimum { get; set; }

        /// <summary>
        /// Indicated if the taxable amount maximum is enabled.
        /// </summary>
        public bool TaxableAmountMaximumEnabled { get; set; }

        /// <summary>
        /// The taxable amount maximum value.
        /// </summary>
        public decimal TaxableAmountMaximum { get; set; }

        /// <summary>
        /// The linked tax schedule id.
        /// </summary>
        public int TaxLinkTaxScheduleId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool TaxLinkTaxTaxes { get; set; }

        /// <summary>
        /// The tenders associated with this tax schedule.
        /// </summary>
        public List<TaxScheduleTenderViewV01> TaxScheduleTenders { get; set; } = new List<TaxScheduleTenderViewV01>();
    }

    /// <summary>
    /// Extension methods for <see cref="TaxScheduleDeviceSetting"/> conversion.
    /// </summary>
    public static class TaxScheduleDeviceSettingConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="TaxScheduleDeviceSettingViewV01"/> object based on this <see cref="TaxScheduleDeviceSetting"/>.
        /// </summary>
        /// <param name="taxScheduleDeviceSetting"></param>
        /// <returns></returns>
        public static TaxScheduleDeviceSettingViewV01 ToTaxScheduleDeviceSettingViewV01(this TaxScheduleDeviceSetting taxScheduleDeviceSetting)
        {
            if (taxScheduleDeviceSetting == null) return null;

            return new TaxScheduleDeviceSettingViewV01
            {
                TaxScheduleId = taxScheduleDeviceSetting.TaxScheduleId,
                Name = taxScheduleDeviceSetting.Name,
                TaxAccountId = taxScheduleDeviceSetting.TaxAccountId,
                TaxAccountName = taxScheduleDeviceSetting.Name,
                TaxExemptQuantityEnabled = taxScheduleDeviceSetting.TaxExemptQuantityEnabled,
                TaxExemptQuantity = taxScheduleDeviceSetting.TaxExemptQuantity,
                TaxableAmountMinimumEnabled = taxScheduleDeviceSetting.TaxableAmountMinimumEnabled,
                TaxableAmountMinimum = taxScheduleDeviceSetting.TaxableAmountMinimum,
                TaxableAmountMaximumEnabled = taxScheduleDeviceSetting.TaxableAmountMaximumEnabled,
                TaxableAmountMaximum = taxScheduleDeviceSetting.TaxableAmountMaximum,
                TaxLinkTaxScheduleId = taxScheduleDeviceSetting.TaxLinkTaxScheduleId,
                TaxLinkTaxTaxes = taxScheduleDeviceSetting.TaxLinkTaxTaxes,
                TaxScheduleTenders = taxScheduleDeviceSetting.TaxScheduleTenders.Select(taxScheduleTender => taxScheduleTender.ToTaxScheduleTenderViewV01()).ToList()
            };
        }

        /// <summary>
        /// Returns a <see cref="TaxScheduleDeviceSetting"/> object based on this <see cref="TaxScheduleDeviceSettingViewV01"/>.
        /// </summary>
        /// <param name="taxScheduleDeviceSettingViewV01"></param>
        /// <returns></returns>
        public static TaxScheduleDeviceSetting ToTaxScheduleDeviceSetting(this TaxScheduleDeviceSettingViewV01 taxScheduleDeviceSettingViewV01)
        {
            if (taxScheduleDeviceSettingViewV01 == null) return null;

            return new TaxScheduleDeviceSetting
            {
                TaxScheduleId = taxScheduleDeviceSettingViewV01.TaxScheduleId,
                Name = taxScheduleDeviceSettingViewV01.Name,
                TaxAccountId = taxScheduleDeviceSettingViewV01.TaxAccountId,
                TaxAccountName = taxScheduleDeviceSettingViewV01.Name,
                TaxExemptQuantityEnabled = taxScheduleDeviceSettingViewV01.TaxExemptQuantityEnabled,
                TaxExemptQuantity = taxScheduleDeviceSettingViewV01.TaxExemptQuantity,
                TaxableAmountMinimumEnabled = taxScheduleDeviceSettingViewV01.TaxableAmountMinimumEnabled,
                TaxableAmountMinimum = taxScheduleDeviceSettingViewV01.TaxableAmountMinimum,
                TaxableAmountMaximumEnabled = taxScheduleDeviceSettingViewV01.TaxableAmountMaximumEnabled,
                TaxableAmountMaximum = taxScheduleDeviceSettingViewV01.TaxableAmountMaximum,
                TaxLinkTaxScheduleId = taxScheduleDeviceSettingViewV01.TaxLinkTaxScheduleId,
                TaxLinkTaxTaxes = taxScheduleDeviceSettingViewV01.TaxLinkTaxTaxes,
                TaxScheduleTenders = taxScheduleDeviceSettingViewV01.TaxScheduleTenders.Select(taxScheduleTenderViewV01 => taxScheduleTenderViewV01.ToTaxScheduleTender()).ToList()
            };
        }
        #endregion
    }
}
