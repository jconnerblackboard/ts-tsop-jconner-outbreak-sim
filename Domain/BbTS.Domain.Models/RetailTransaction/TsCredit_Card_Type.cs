﻿using System;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.RetailTransaction
{
    /// <summary>
    /// This object represents a Transact Credit_Card_Type object. Credit Card Types
    /// </summary>
    [Serializable]
    public class TsCredit_Card_Type
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Credit_Card_Type_Id { get; set; }
        /// <summary>
        /// Name of Credit Card Type
        /// </summary>
        [XmlAttribute]
        public string Name { get; set; }
        /// <summary>
        /// Description of Credit Card Type
        /// </summary>
        [XmlAttribute]
        public string Description { get; set; }
    }
}
