using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Transaction_Cashier
        {
        /// <summary>
        /// This object represents a Transact Transaction_Cashier object. Transaction Cashiers
        /// </summary>
        [Serializable]
        public class TsTransaction_Cashier
        {
          /// <summary>
          /// 
          /// </summary>
          [XmlAttribute]
          public int Transaction_Id { get; set; }
          /// <summary>
          /// 
          /// </summary>
          [XmlAttribute]
          public int Cashier_Id { get; set; }
        }
}