using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Retail_Tran_LineItem_Tender_Sv
{
    /// <summary>
    /// This object represents a Transact Retail_Tran_LineItem_Tender_Sv object. Retail Transaction Line Item Tender - Stored Value Tender Used
    /// </summary>
    [Serializable]
    public class TsRetail_Tran_LineItem_Tender_Sv
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Transaction_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int LineItem_SequenceNumber { get; set; }
    }
}