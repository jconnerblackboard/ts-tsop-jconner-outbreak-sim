using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Retail_Tran_LineItem_Mdb_Info
{
    /// <summary>
    /// This object represents a Transact Retail_Tran_LineItem_Mdb_Info object. The Multi-Drop Bus information retrieved from the specific manufacturer of the vending device
    /// </summary>
    [Serializable]
    public class TsRetail_Tran_LineItem_Mdb_Info
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Transaction_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Lineitem_SequenceNumber { get; set; }
        /// <summary>
        /// A hexidecimal representation of the Item Number of the selected product.  This number is defined by the manufacturer.
        /// </summary>
        [XmlAttribute]
        public string Itemnum_Value { get; set; }
    }
}