﻿using System;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.RetailTransaction
{
    /// <summary>
    /// This object represents a Transact TaxSchedule object. Tax Schedule
    /// </summary>
    [Serializable]
    public class TsTaxSchedule
    {
        /// <summary>
        /// Tax Schedule Identifier
        /// </summary>
        [XmlAttribute]
        public int TaxSchedule_Id { get; set; }
        /// <summary>
        ///
        /// </summary>
        [XmlAttribute]
        public int Link_TaxSchedule_Id { get; set; }
        /// <summary>
        ///
        /// </summary>
        [XmlAttribute]
        public int Merchant_Id { get; set; }
        /// <summary>
        ///
        /// </summary>
        [XmlAttribute]
        public int TaxAccount_Id { get; set; }
        /// <summary>
        /// Name of Tax Schedule
        /// </summary>
        [XmlAttribute]
        public string Name { get; set; }
        /// <summary>
        /// Code name of Tax Schedule
        /// </summary>
        [XmlAttribute]
        public string CodeName { get; set; }
        /// <summary>
        /// Indicates if the quantity of a product being sold can be tax exempt (T/F)
        /// </summary>
        [XmlAttribute]
        public string TaxExempt_Quantity_Enabled { get; set; }
        /// <summary>
        /// Qty that can be tax exempt
        /// </summary>
        [XmlAttribute]
        public int TaxExempt_Quantity { get; set; }
        /// <summary>
        /// Indicates if the minimum amount of the sum of product being sold can be taxed (T/F)
        /// </summary>
        [XmlAttribute]
        public string Taxable_Amount_Minimum_Enabled { get; set; }
        /// <summary>
        /// Minimum amount to be taxable
        /// </summary>
        [XmlAttribute]
        public int Taxable_Amount_Minimum { get; set; }
        /// <summary>
        /// Indicates if the maximum amount of the sum of product being sold can be taxed (T/F)
        /// </summary>
        [XmlAttribute]
        public string Taxable_Amount_Maximum_Enabled { get; set; }
        /// <summary>
        /// Maximum amount to be taxable
        /// </summary>
        [XmlAttribute]
        public int Taxable_Amount_Maximum { get; set; }
        /// <summary>
        /// Shared Schedule
        /// </summary>
        [XmlAttribute]
        public int TaxLink_TaxSchedule_Id { get; set; }
        /// <summary>
        /// Tax Link should not tax any other tax Indicator (T/F)
        /// </summary>
        [XmlAttribute]
        public string TaxLink_Tax_Taxes { get; set; }
    }
}