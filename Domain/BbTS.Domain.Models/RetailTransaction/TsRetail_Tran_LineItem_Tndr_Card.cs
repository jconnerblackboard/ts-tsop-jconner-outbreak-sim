using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Retail_Tran_LineItem_Tndr_Card
{
    /// <summary>
    /// This object represents a Transact Retail_Tran_LineItem_Tndr_Card object. Retail Transaction Line Item Tender - Customer Card Used to identify customer table
    /// </summary>
    [Serializable]
    public class TsRetail_Tran_LineItem_Tndr_Card
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Transaction_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int LineItem_SequenceNumber { get; set; }
        /// <summary>
        /// Card number used to identify the customer in the stored value transaction
        /// </summary>
        [XmlAttribute]
        public string CardNum { get; set; }
        /// <summary>
        /// The issue number of the card captured during the transaction, if applicable
        /// </summary>
        [XmlAttribute]
        public string Issue_Number { get; set; }
        /// <summary>
        /// Indicates if an issue number was captured during the transaction (T/F)
        /// </summary>
        [XmlAttribute]
        public string Issue_Number_Captured { get; set; }
    }
}