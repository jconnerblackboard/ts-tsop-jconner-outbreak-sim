using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Session_Control
{
    /// <summary>
    /// This object represents a Transact Session_Control object. Cashier Session Auditing
    /// </summary>
    [Serializable]
    public class TsSession_Control
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Transaction_Id { get; set; }
        /// <summary>
        /// Cash Drawer the cashier is using
        /// </summary>
        [XmlAttribute]
        public int Cashdrawer_Num { get; set; }
        /// <summary>
        /// Indicates if the session is currently open
        /// </summary>
        [XmlAttribute]
        public string Session_Open { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int SessionEnd_Transaction_Id { get; set; }
        /// <summary>
        /// Datetime the session started
        /// </summary>
        [XmlAttribute]
        public DateTime SessionStart_DateTime { get; set; }
        /// <summary>
        /// Datetime the session ended
        /// </summary>
        [XmlAttribute]
        public DateTime SessionEnd_DateTime { get; set; }
        /// <summary>
        /// The total values of taxes, tenders, discounts, surcharges, and tips
        /// </summary>
        [XmlAttribute]
        public int Session_ControlTotal { get; set; }
    }
}