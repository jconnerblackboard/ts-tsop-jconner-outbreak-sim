using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Retail_Tran_LineItem_Tndr_Cust
{
    /// <summary>
    /// This object represents a Transact Retail_Tran_LineItem_Tndr_Cust object. Retail Transaction Line Item Tender - Customer for tenders that have a cust association(sv, board, cashequiv), this is the associated customer, and how that customer was determined
    /// </summary>
    [Serializable]
    public class TsRetail_Tran_LineItem_Tndr_Cust
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Transaction_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int LineItem_SequenceNumber { get; set; }
        /// <summary>
        /// 0 - card (magstripe)
        /// 1 - contactless
        /// </summary>
        [XmlAttribute]
        public int Physical_Id_Type { get; set; }
        /// <summary>
        /// 0 - swiped
        /// 1 - manually entered
        /// </summary>
        [XmlAttribute]
        public int Cust_EntryMethod_Type { get; set; }
        /// <summary>
        /// Indicates if a secondary authorization was entered (T/F)
        /// </summary>
        [XmlAttribute]
        public string Secondary_Auth_Entered { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Cust_Id { get; set; }
    }
}