﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.RetailTransaction
{
    /// <summary>
    /// Tender get response
    /// </summary>
    public class TenderGetResponse
    {
        /// <summary>
        /// Tenders
        /// </summary>
        public List<TsTender2> Tenders { get; set; }
    }
}
