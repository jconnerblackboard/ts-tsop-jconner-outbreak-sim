﻿using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming



namespace BbTS.Domain.Models.Transaction
{
    /// <summary>
    /// This object represents a Transact transaction object. Transaction
    /// </summary>
    [Serializable]
    public class TsTransaction
    {
        /// <summary>
        /// Transaction Identifier
        /// </summary>
        [XmlAttribute]
        public int Transaction_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Pos_Id { get; set; }
        /// <summary>
        /// Date which the transaction took place
        /// </summary>
        [XmlAttribute]
        public int BusinessDay_date { get; set; }
        /// <summary>
        /// Period Number of transaction
        /// </summary>
        [XmlAttribute]
        public int Period_Number { get; set; }
        /// <summary>
        /// Transaction Number as defined by the system or device
        /// </summary>
        [XmlAttribute]
        public int Transaction_Number { get; set; }
        /// <summary>
        /// The sequence number for the Transaction, within the confines of the POS and BusinessDay.
        /// </summary>
        [XmlAttribute]
        public int Sequence_Number { get; set; }
        /// <summary>
        /// 0 - Retail_Tran
        //  1 - control_transaction
        /// </summary>
        [XmlAttribute]
        public int TransactionType_Code { get; set; }
        /// <summary>
        /// Beginning of Transaction DateTime
        /// </summary>
        [XmlAttribute]
        public DateTime Begin_DateTime { get; set; }
        /// <summary>
        /// Ending of transaction DateTime
        /// </summary>
        [XmlAttribute]
        public DateTime End_DateTime { get; set; }
        /// <summary>
        /// Cancelled Indicator (T/F)
        /// </summary>
        [XmlAttribute]
        public string Cancelled_Flag { get; set; }
        /// <summary>
        /// Voided Indicator (T/F)
        /// </summary>
        [XmlAttribute]
        public string Voided_Flag { get; set; }
        /// <summary>
        /// Suspended Indicator (T/F)
        /// </summary>
        [XmlAttribute]
        public string Suspended_Flag { get; set; }
        /// <summary>
        /// DateTime the suspended transaction is available for processing
        /// </summary>
        [XmlAttribute]
        public DateTime Suspended_Unsuspend_DateTime { get; set; }
        /// <summary>
        /// Reserved for future use
        /// </summary>
        [XmlAttribute]
        public string Training_Flag { get; set; }
        /// <summary>
        /// Indicates if the transaction took place off-line (T/F)
        /// </summary>
        [XmlAttribute]
        public string KeyedOffline_Flag { get; set; }
        /// <summary>
        /// Time the Database recorded the transaction
        /// </summary>
        [XmlAttribute]
        public DateTime DbRecord_Create_DateTime { get; set; }
        /// <summary>
        /// 0 - Accepted Online (by External Auth)
        //1 - Accepted Offline(at Device Only)
        //    2 - Denied Online(by External Auth)
        //3 - Denied Offline(at Device Only)
        /// </summary>
        [XmlAttribute]
        public int Validation_Type { get; set; }
        /// <summary>
        /// Get the description from PackDeniedCodes.(nMsgId)
        //  ie.
        //  0 - Accepted
        //  1 - POS Not available
        /// </summary>
        [XmlAttribute]
        public int Validation_Code { get; set; }
        /// <summary>
        /// 0 - No Cashier Present
        //1 - Cashier Present(see transaction_cashier)
        /// </summary>
        [XmlAttribute]
        public int Attended_Type { get; set; }
        /// <summary>
        /// Indicates that the transaction is currently in process and is not finalized.
        /// </summary>
        [XmlAttribute]
        public string InProcess_Flag { get; set; }
    }
}