using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Retail_Tran_LineItem_Sv_Card
{
    /// <summary>
    /// This object represents a Transact Retail_Tran_LineItem_Sv_Card object. Retail Transaction LineItem Stored Value Card Table
    /// </summary>
    [Serializable]
    public class TsRetail_Tran_LineItem_Sv_Card
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Transaction_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int LineItem_SequenceNumber { get; set; }
        /// <summary>
        /// Cardnumber used to identify customer
        /// </summary>
        [XmlAttribute]
        public string CardNum { get; set; }
        /// <summary>
        /// Issue number of card
        /// </summary>
        [XmlAttribute]
        public string Issue_Number { get; set; }
        /// <summary>
        /// Indicates if the Issue Number was captured during the transaction (T/F)
        /// </summary>
        [XmlAttribute]
        public string Issue_Number_Captured { get; set; }
    }
}