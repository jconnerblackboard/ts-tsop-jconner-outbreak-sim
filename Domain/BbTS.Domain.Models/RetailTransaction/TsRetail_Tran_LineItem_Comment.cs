using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Retail_Tran_LineItem_Comment
{
    /// <summary>
    /// This object represents a Transact Retail_Tran_LineItem_Comment object. Retail Transaction Line Item - Comments
    /// </summary>
    [Serializable]
    public class TsRetail_Tran_LineItem_Comment
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Transaction_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int LineItem_SequenceNumber { get; set; }
        /// <summary>
        /// Text of Comment
        /// </summary>
        [XmlAttribute]
        public string Comment_Text { get; set; }
    }
}