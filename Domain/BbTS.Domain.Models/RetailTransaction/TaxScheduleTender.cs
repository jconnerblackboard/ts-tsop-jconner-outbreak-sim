﻿namespace BbTS.Domain.Models.RetailTransaction
{
    /// <summary>
    /// Container class to hold tender values associated with a tax schedule.
    /// </summary>
    public class TaxScheduleTender
    {
        /// <summary>
        /// The tax schedule associated with this <see cref="TaxScheduleTender"/>
        /// </summary>
        public int TaxScheduleId { get; set; }

        /// <summary>
        /// The associated tender id.
        /// </summary>
        public int TenderId { get; set; }
        
        /// <summary>
        /// The tax percent.
        /// </summary>
        public decimal TaxRate { get; set; }
    }

    /// <summary>
    /// View for a <see cref="TaxScheduleTender"/>.  (Version 1)
    /// </summary>
    public class TaxScheduleTenderViewV01
    {
        /// <summary>
        /// The tax schedule associated with this <see cref="TaxScheduleTender"/>
        /// </summary>
        public int TaxScheduleId { get; set; }

        /// <summary>
        /// The associated tender id.
        /// </summary>
        public int TenderId { get; set; }

        /// <summary>
        /// The tax percent.
        /// </summary>
        public decimal TaxRate { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="TaxScheduleTender"/> conversion.
    /// </summary>
    public static class TaxScheduleTenderConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="TaxScheduleTenderViewV01"/> object based on this <see cref="TaxScheduleTender"/>.
        /// </summary>
        /// <param name="taxScheduleTender"></param>
        /// <returns></returns>
        public static TaxScheduleTenderViewV01 ToTaxScheduleTenderViewV01(this TaxScheduleTender taxScheduleTender)
        {
            if (taxScheduleTender == null) return null;

            return new TaxScheduleTenderViewV01
            {
                TaxScheduleId = taxScheduleTender.TaxScheduleId,
                TenderId = taxScheduleTender.TenderId,
                TaxRate = taxScheduleTender.TaxRate
            };
        }

        /// <summary>
        /// Returns a <see cref="TaxScheduleTender"/> object based on this <see cref="TaxScheduleTenderViewV01"/>.
        /// </summary>
        /// <param name="taxScheduleTenderViewV01"></param>
        /// <returns></returns>
        public static TaxScheduleTender ToTaxScheduleTender(this TaxScheduleTenderViewV01 taxScheduleTenderViewV01)
        {
            if (taxScheduleTenderViewV01 == null) return null;

            return new TaxScheduleTender
            {
                TaxScheduleId = taxScheduleTenderViewV01.TaxScheduleId,
                TenderId = taxScheduleTenderViewV01.TenderId,
                TaxRate = taxScheduleTenderViewV01.TaxRate
            };
        }
        #endregion
    }
}
