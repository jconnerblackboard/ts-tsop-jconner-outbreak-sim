using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Retail_Tran_LineItem_Tndr_Srch
{
    /// <summary>
    /// This object represents a Transact Retail_Tran_LineItem_Tndr_Srch object. Retail Transaction Line Item Tender - Surcharge
    /// </summary>
    [Serializable]
    public class TsRetail_Tran_LineItem_Tndr_Srch
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Transaction_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int LineItem_SequenceNumber { get; set; }
        /// <summary>
        /// The order in which this surcharge was taken in regards to this retail_tran_lineitem_tender.
        /// </summary>
        [XmlAttribute]
        public int Tndr_Srch_SequenceNumber { get; set; }
        /// <summary>
        /// 0 -  ProfitCenter Period-based Discount
        /// 1 -  Policy Discount
        /// </summary>
        [XmlAttribute]
        public int Surcharge_Reason_Type { get; set; }
        /// <summary>
        /// Percentage of Surcharge (in millipercent)
        /// </summary>
        [XmlAttribute]
        public int Percentage { get; set; }
        /// <summary>
        /// The difference of the rounded amount after the calculation and the actual amount that was calculated without rounding.  Negative numbers indicate a loss from rounding while positive numbers indicate a gain from rounding.
        /// </summary>
        [XmlAttribute]
        public DateTime RoundingAmount { get; set; }
        /// <summary>
        /// Amount of Surcharge (in millidollars)
        /// </summary>
        [XmlAttribute]
        public int Amount { get; set; }
        /// <summary>
        /// Indicates if the discount was prorated across all of the Items (T/F)
        /// </summary>
        [XmlAttribute]
        public string Prorated_Flag { get; set; }
        /// <summary>
        /// Shows how the surcharge was calculated: 0 - None  1 - Amount  2 - Open  3 - Percent
        /// </summary>
        [XmlAttribute]
        public int Calculation_Type { get; set; }
    }
}
