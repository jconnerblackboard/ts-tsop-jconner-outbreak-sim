using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Retail_Tran_LineItem_Prod_Tx_E
{
    /// <summary>
    /// This object represents a Transact Retail_Tran_LineItem_Prod_Tx_E object. A line item modifier to the RETAIL_TRAN_LINEITEM_PROD_TAX component of a RetailTransaction that provides supplementary data regarding tax exemptions.
    /// </summary>
    [Serializable]
    public class TsRetail_Tran_LineItem_Prod_Tx_E
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Transaction_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int LineItem_SequenceNumber { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int TaxSchedule_Id { get; set; }
        /// <summary>
        /// 0 - Stored Value Account Type
        /// 1 - Tax Schedule Tax Group Quantity
        /// 2 - Tax Schedule Transaction Subtotal
        /// </summary>
        [XmlAttribute]
        public int Tax_Exempt_Reason_Type { get; set; }
        /// <summary>
        /// The Taxable amount which the customer would've been liable to pay tax on had they not been granted this exemption.
        /// </summary>
        [XmlAttribute]
        public int Exempt_Taxable_Amount { get; set; }
        /// <summary>
        /// The amount of tax that the customer would've been liable to pay had thus TaxExemption not been granted.
        /// </summary>
        [XmlAttribute]
        public int Exempt_Tax_Amount { get; set; }
    }
}