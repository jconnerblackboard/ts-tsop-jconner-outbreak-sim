﻿using BbTS.Domain.Models.Definitions.Transaction;

namespace BbTS.Domain.Models.RetailTransaction
{
    /// <summary>
    /// Container class for tenders on a device.
    /// </summary>
    public class TenderDeviceSetting
    {
        /// <summary>
        /// The identifier of the tender
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The name of the tender.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The type of tender.
        /// </summary>
        public TenderType Type { get; set; }

        /// <summary>
        /// The shortened name for the tender.
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// The precision that cash rounding is applied at.
        /// </summary>
        public decimal RoundingIncrement { get; set; }
    }

    /// <summary>
    /// View for a <see cref="TenderDeviceSetting"/>.  (Version 1)
    /// </summary>
    public class TenderDeviceSettingViewV01
    {
        /// <summary>
        /// The identifier of the tender
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The name of the tender.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The type of tender.
        /// </summary>
        public TenderType Type { get; set; }

        /// <summary>
        /// The shortened name for the tender.
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// The precision that cash rounding is applied at.
        /// </summary>
        public decimal RoundingIncrement { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="TenderDeviceSetting"/> conversion.
    /// </summary>
    public static class TenderDeviceSettingConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="TenderDeviceSettingViewV01"/> object based on this <see cref="TenderDeviceSetting"/>.
        /// </summary>
        /// <param name="tenderDeviceSetting"></param>
        /// <returns></returns>
        public static TenderDeviceSettingViewV01 ToTenderDeviceSettingViewV01(this TenderDeviceSetting tenderDeviceSetting)
        {
            if (tenderDeviceSetting == null) return null;

            return new TenderDeviceSettingViewV01
            {
                Id = tenderDeviceSetting.Id,
                Name = tenderDeviceSetting.Name,
                Type = tenderDeviceSetting.Type,
                ShortName = tenderDeviceSetting.ShortName,
                RoundingIncrement = tenderDeviceSetting.RoundingIncrement
            };
        }

        /// <summary>
        /// Returns a <see cref="TenderDeviceSetting"/> object based on this <see cref="TenderDeviceSettingViewV01"/>.
        /// </summary>
        /// <param name="tenderDeviceSettingViewV01"></param>
        /// <returns></returns>
        public static TenderDeviceSetting ToTenderDeviceSetting(this TenderDeviceSettingViewV01 tenderDeviceSettingViewV01)
        {
            if (tenderDeviceSettingViewV01 == null) return null;

            return new TenderDeviceSetting
            {
                Id = tenderDeviceSettingViewV01.Id,
                Name = tenderDeviceSettingViewV01.Name,
                Type = tenderDeviceSettingViewV01.Type,
                ShortName = tenderDeviceSettingViewV01.ShortName,
                RoundingIncrement = tenderDeviceSettingViewV01.RoundingIncrement
            };
        }
        #endregion
    }
}
