using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Sv_Account_History_Rtl_Trn_Tdr
{
    /// <summary>
    /// This object represents a Transact Sv_Account_History_Rtl_Trn_Tdr object. Stored Value History for Retail Transactions - Charges
    /// </summary>
    [Serializable]
    public class TsSv_Account_History_Rtl_Trn_Tdr
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Sv_Account_History_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Transaction_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int LineItem_SequenceNumber { get; set; }
        /// <summary>
        /// The order in which the sv account transaction depleted
        /// </summary>
        [XmlAttribute]
        public int Depletion_Order { get; set; }
        /// <summary>
        /// Indicates if the Retail Transaction involved more than one Tender (as a hint to why it's not a 1:1 with products for example)
        /// </summary>
        [XmlAttribute]
        public string Split_Tender_Flag { get; set; }
    }
}