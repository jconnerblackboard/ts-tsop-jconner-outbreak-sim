using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.StoredValueDenial
{
    /// <summary>
    /// This object represents a Transact StoredValueDenial object. Stored Value Denial table
    /// </summary>
    [Serializable]
    public class TsStoredValueDenial
    {
        /// <summary>
        /// Primary key of the table.
        /// </summary>
        [XmlAttribute]
        public int StoredValueDenialId { get; set; }
        /// <summary>
        /// FK to the Customer table
        /// </summary>
        [XmlAttribute]
        public int CustomerId { get; set; }
        /// <summary>
        /// FK to the Pos table
        /// </summary>
        [XmlAttribute]
        public int PosId { get; set; }
        /// <summary>
        /// FK to the Tender table
        /// </summary>
        [XmlAttribute]
        public int TenderId { get; set; }
        /// <summary>
        /// FK to the Transaction table
        /// </summary>
        [XmlAttribute]
        public int TransactionId { get; set; }
        /// <summary>
        /// Whether the transaction was online or not
        /// </summary>
        [XmlAttribute]
        public string IsOnline { get; set; }
        /// <summary>
        /// FK to the Denied_Message table
        /// </summary>
        [XmlAttribute]
        public int DeniedMessageId { get; set; }
        /// <summary>
        /// The customers available balance
        /// </summary>
        [XmlAttribute]
        public int AvailableBalance { get; set; }
        /// <summary>
        /// Date this record was last updated
        /// </summary>
        [XmlAttribute]
        public DateTime ModifiedDate { get; set; }
        /// <summary>
        /// Card number used at time of denial
        /// </summary>
        [XmlAttribute]
        public string CardNumber { get; set; }
    }
}
