﻿using System;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.RetailTransaction
{
    /// <summary>
    /// This object represents a Transact TaxGroups object. Contains Tax Groups per merchant.  Used for products.
    /// </summary>
    [Serializable]
    public class TsTaxGroups
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int TaxGroup_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Merchant_Id { get; set; }
        /// <summary>
        /// Tax Group Name
        /// </summary>
        [XmlAttribute]
        public string TaxGroup { get; set; }
    }
}
