using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.TransactionObjectLog
        {
        /// <summary>
        /// This object represents a Transact TransactionObjectLog object. This table contains Xml documents that represent transactions in the system.
        /// </summary>
        [Serializable]
        public class TsTransactionObjectLog
        {
          /// <summary>
          /// Primary Key of the table.
          /// </summary>
          [XmlAttribute]
          public int TransactionObjectLogId { get; set; }
          /// <summary>
          /// The Xml representation of a transaction object.
          /// </summary>
          [XmlAttribute]
          public string Object { get; set; }
          /// <summary>
          /// A enum of states that transction can be in (unknown, complete, rejected).
          /// </summary>
          [XmlAttribute]
          public int Disposition { get; set; }
          /// <summary>
          /// 
          /// </summary>
          [XmlAttribute]
          public int TransactionId { get; set; }
          /// <summary>
          /// When this object was created.
          /// </summary>
          [XmlAttribute]
          public DateTime CreatedDateTime { get; set; }
          /// <summary>
          /// When this object was last modified.
          /// </summary>
          [XmlAttribute]
          public DateTime ModifiedDateTime { get; set; }
        }
}