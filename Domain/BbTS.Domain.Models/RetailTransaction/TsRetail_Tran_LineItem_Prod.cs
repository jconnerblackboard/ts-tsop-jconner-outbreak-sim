using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Retail_Tran_LineItem_Prod
{
    /// <summary>
    /// This object represents a Transact Retail_Tran_LineItem_Prod object. Retail Transaction Line Item Product
    /// </summary>
    [Serializable]
    public class TsRetail_Tran_LineItem_Prod
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Transaction_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int LineItem_SequenceNumber { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int ProdDetail_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int TaxSchedule_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int TaxGroup_Id { get; set; }
        /// <summary>
        /// Retail Price of Product (in millidollars). The regular or lookup per-unit price for the item before any discounts, etc have been applied.
        /// </summary>
        [XmlAttribute]
        public int RetailPrice { get; set; }
        /// <summary>
        /// Retail Price Quantity
        /// </summary>
        [XmlAttribute]
        public int RetailPrice_Quantity { get; set; }
        /// <summary>
        /// Actual Unit Price (in millidollars).  The actual per-unit price paid by the customer for this particular sale. It is obtained by applying applicable price derivation rules to the regular unit price (retailprice).
        /// </summary>
        [XmlAttribute]
        public int ActualUnit_Price { get; set; }
        /// <summary>
        /// Actual Unit Price Quantity
        /// </summary>
        [XmlAttribute]
        public int ActualUnit_Price_Quantity { get; set; }
        /// <summary>
        /// Quantity Amount.  The number of retail selling units sold to or returned by a customer. For services the number ofunits (e.g. hours or job) sold or in the case of refunds, reduced to zero revenue.
        /// </summary>
        [XmlAttribute]
        public int Quantity { get; set; }
        /// <summary>
        /// Units Amount. The number of units sold, when selling bulk merchandise. Eg: A sale of 20 Gallons of Gas: Quantity=20, Units=1, UnitOfMeasure=Ga Eg: A sale of 3 cans of Beans: Quantity=3, Units=3, UnitOfMeasure=Ea
        /// </summary>
        [XmlAttribute]
        public int Units { get; set; }
        /// <summary>
        /// 0 - Count
        /// 1 - Weight (Pounds)
        /// </summary>
        [XmlAttribute]
        public int Unit_MeasureType { get; set; }
        /// <summary>
        /// Extended Amount of Line Item (in millidollars). The product of multiplying Quantity by the retail selling unit price derived from price lookup (and any applicable price derivation rules). This retail sale unit price excludes sales and/or value added tax.
        /// </summary>
        [XmlAttribute]
        public int Extended_Amount { get; set; }
        /// <summary>
        /// Amount of any Unit Discounts (in millidollars). The monetary total per-unit of all Discounts and RetailPriceModifiers that were applied to this Item
        /// </summary>
        [XmlAttribute]
        public int Unit_Discount_Amount { get; set; }
        /// <summary>
        /// Extended Amount of Discounts (in millidollars). The monetary total of all Discounts and RetailPriceModifiers that were applied to this Item
        /// </summary>
        [XmlAttribute]
        public int Extended_Discount_Amount { get; set; }
        /// <summary>
        /// 0 - MenuItem
        /// 1 - Scanned (barcode, etc)
        /// 2 - Keyed (Manual Product Barcode)
        /// </summary>
        [XmlAttribute]
        public int Product_EntryMethod_Type { get; set; }
        /// <summary>
        /// 0 - Lookup
        /// 1 - Manually Entered
        /// </summary>
        [XmlAttribute]
        public int RetailPrice_EntryMethod_Type { get; set; }
        /// <summary>
        /// Unit Cost Amount (in millidollars). The unit cost of the Item to the retail enterprise at the time of the Transaction.
        /// </summary>
        [XmlAttribute]
        public int Unit_CostPrice { get; set; }
        /// <summary>
        /// The number of units applicable to the cost of the Item to the retail enterprise at the time of the Transaction e.g. 3 for 99c
        /// </summary>
        [XmlAttribute]
        public int Unit_CostPrice_Quantity { get; set; }
        /// <summary>
        /// Price per Unit (in millidollars). The unit MSRP of the Item at the time of the Transaction
        /// </summary>
        [XmlAttribute]
        public int Unit_ListPrice { get; set; }
        /// <summary>
        /// The number of units applicable to the MSRP of the Item at the time of the Transaction e.g. 3 for 99c
        /// </summary>
        [XmlAttribute]
        public int Unit_ListPrice_Quantity { get; set; }
        /// <summary>
        /// Indicates if there was a product promotion at the time of the transaction (T/F)
        /// </summary>
        [XmlAttribute]
        public string ProdPromo_Flag { get; set; }
        /// <summary>
        /// Product Promotion Price
        /// </summary>
        [XmlAttribute]
        public int ProdPromo_Price { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int ProdPromo_Id { get; set; }
        /// <summary>
        /// Indicates there is at least one price modification on this product, which can be determined by looking at the  new RETAIL_TRAN_LINEITEM_PRODPRCMD table (T/F) 
        /// </summary>
        [XmlAttribute]
        public string RetailPrice_Modified_Flag { get; set; }
    }
}
