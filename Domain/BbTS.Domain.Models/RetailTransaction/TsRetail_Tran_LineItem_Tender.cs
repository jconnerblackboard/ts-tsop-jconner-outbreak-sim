using System;
using System.Xml.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Retail_Tran_LineItem_Tender
{
    /// <summary>
    /// This object represents a Transact Retail_Tran_LineItem_Tender object. Retail Transaction Line Item Tender - Tender Used
    /// </summary>
    [Serializable]
    public class TsRetail_Tran_LineItem_Tender
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Transaction_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int LineItem_SequenceNumber { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Tender_Id { get; set; }
        /// <summary>
        /// Amount of  Tender Used after any applicable discounts, surcharges (in millidollars)
        /// </summary>
        [XmlAttribute]
        public int Tender_Amount { get; set; }
        /// <summary>
        /// Tip Amount (in millidollars)
        /// </summary>
        [XmlAttribute]
        public int Tip_Amount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int RoundingAmount { get; set; }
    }
}