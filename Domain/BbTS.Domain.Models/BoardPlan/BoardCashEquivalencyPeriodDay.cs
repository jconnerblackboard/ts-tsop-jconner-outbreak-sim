﻿using System;

namespace BbTS.Domain.Models.BoardPlan
{
    /// <summary>
    /// Container class for a board cash equivalency period day.
    /// </summary>
    public class BoardCashEquivalencyPeriodDay
    {
        /// <summary>
        /// Name of the period day.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Board start time.
        /// </summary>
        public DateTime? BoardPeriodStartTime { get; set; }

        /// <summary>
        /// Board end time.
        /// </summary>
        public DateTime? BoardPeriodStopTime { get; set; }

        /// <summary>
        /// Cash equivalency start time.
        /// </summary>
        public DateTime? CashEquivPeriodStartTime { get; set; }

        /// <summary>
        /// Cash equivalency end time.
        /// </summary>
        public DateTime? CashEquivPeriodStopTime { get; set; }
    }

    /// <summary>
    /// View for a <see cref="BoardCashEquivalencyPeriodDay"/>.  (Version 1)
    /// </summary>
    public class BoardCashEquivalencyPeriodDayViewV01
    {
        /// <summary>
        /// Name of the period day.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Board start time.
        /// </summary>
        public DateTime? BoardPeriodStartTime { get; set; }

        /// <summary>
        /// Board end time.
        /// </summary>
        public DateTime? BoardPeriodStopTime { get; set; }

        /// <summary>
        /// Cash equivalency start time.
        /// </summary>
        public DateTime? CashEquivPeriodStartTime { get; set; }

        /// <summary>
        /// Cash equivalency end time.
        /// </summary>
        public DateTime? CashEquivPeriodStopTime { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="BoardCashEquivalencyPeriodDay"/> conversion.
    /// </summary>
    public static class BoardCashEquivalencyPeriodDayConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="BoardCashEquivalencyPeriodDayViewV01"/> object based on this <see cref="BoardCashEquivalencyPeriodDay"/>.
        /// </summary>
        /// <param name="boardCashEquivalencyPeriodDay"></param>
        /// <returns></returns>
        public static BoardCashEquivalencyPeriodDayViewV01 ToBoardCashEquivalencyPeriodDayViewV01(this BoardCashEquivalencyPeriodDay boardCashEquivalencyPeriodDay)
        {
            if (boardCashEquivalencyPeriodDay == null) return null;

            return new BoardCashEquivalencyPeriodDayViewV01
            {
                Name = boardCashEquivalencyPeriodDay.Name,
                BoardPeriodStartTime = boardCashEquivalencyPeriodDay.BoardPeriodStartTime,
                BoardPeriodStopTime = boardCashEquivalencyPeriodDay.BoardPeriodStopTime,
                CashEquivPeriodStartTime = boardCashEquivalencyPeriodDay.CashEquivPeriodStartTime,
                CashEquivPeriodStopTime = boardCashEquivalencyPeriodDay.CashEquivPeriodStopTime
            };
        }

        /// <summary>
        /// Returns a <see cref="BoardCashEquivalencyPeriodDay"/> object based on this <see cref="BoardCashEquivalencyPeriodDayViewV01"/>.
        /// </summary>
        /// <param name="boardCashEquivalencyPeriodDayViewV01"></param>
        /// <returns></returns>
        public static BoardCashEquivalencyPeriodDay ToBoardCashEquivalencyPeriodDay(this BoardCashEquivalencyPeriodDayViewV01 boardCashEquivalencyPeriodDayViewV01)
        {
            if (boardCashEquivalencyPeriodDayViewV01 == null) return null;

            return new BoardCashEquivalencyPeriodDay
            {
                Name = boardCashEquivalencyPeriodDayViewV01.Name,
                BoardPeriodStartTime = boardCashEquivalencyPeriodDayViewV01.BoardPeriodStartTime,
                BoardPeriodStopTime = boardCashEquivalencyPeriodDayViewV01.BoardPeriodStopTime,
                CashEquivPeriodStartTime = boardCashEquivalencyPeriodDayViewV01.CashEquivPeriodStartTime,
                CashEquivPeriodStopTime = boardCashEquivalencyPeriodDayViewV01.CashEquivPeriodStopTime
            };
        }
        #endregion
    }
}
