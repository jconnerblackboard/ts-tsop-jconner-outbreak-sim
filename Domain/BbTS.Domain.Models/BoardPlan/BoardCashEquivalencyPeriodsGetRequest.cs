﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Transaction.Processing;

namespace BbTS.Domain.Models.BoardPlan
{
    /// <summary>
    /// Container class for a board cash equivalency periods get request.
    /// </summary>
    public class BoardCashEquivalencyPeriodsGetRequest : IProcessingRequest
    {
        /// <summary>
        /// Unique identifier for the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The unique identifier for the device.
        /// </summary>
        public string DeviceGuid { get; set; }

        /// <summary>
        /// The last period number.
        /// </summary>
        public int LastPeriodNum { get; set; }

        /// <summary>
        /// The default board plan Id.
        /// </summary>
        public int DefaultBoardPlanId { get; set; }

        /// <summary>
        /// The current period num.
        /// </summary>
        public int PeriodNum { get; set; }
    }
}
