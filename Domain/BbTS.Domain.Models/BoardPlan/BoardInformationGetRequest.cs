﻿using System;
using BbTS.Domain.Models.Transaction.Processing;

namespace BbTS.Domain.Models.BoardPlan
{
    /// <summary>
    /// Container class for a board information get request.
    /// </summary>
    public class BoardInformationGetRequest : IProcessingRequest
    {
        /// <summary>
        /// Unique identifier for the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The unique identifier for the originator.
        /// </summary>
        public string OriginatorGuid { get; set; }
        
        /// <summary>
        /// The unique identifier for the customer.
        /// </summary>
        public string CustomerGuid { get; set; }

        /// <summary>
        /// Indicates whether or not the transaction should be force posted.
        /// </summary>
        public bool ForcePost { get; set; }

        /// <summary>
        /// The meal type id.
        /// </summary>
        public int MealTypeId { get; set; }

        /// <summary>
        /// Indicates whether or not this board should be treated as a guest meal.
        /// </summary>
        public bool IsGuestMeal { get; set; }

        /// <summary>
        /// The date and time of the board transaction.
        /// </summary>
        public DateTime TransactionDateTime { get; set; }

        /// <summary>
        /// The board plan id to use.
        /// </summary>
        public int? BoardPlanId { get; set; }
    }
}
