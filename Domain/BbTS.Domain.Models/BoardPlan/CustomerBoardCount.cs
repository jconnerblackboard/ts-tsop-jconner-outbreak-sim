﻿using BbTS.Domain.Models.Definitions.Board;
using System;
using System.Runtime.Serialization;

namespace BbTS.Domain.Models.BoardPlan
{
    /// <summary>
    /// This object represents a Transact CustomerBoardCount object. 
    /// </summary>
    [Serializable]
    [DataContract]
    public class CustomerBoardCount
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int BoardPlanId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public string BoardPlanName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int Priority { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool BoardPlanIsActive { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public DateTime BoardPlanActiveFrom { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public DateTime BoardPlanActiveTo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool CustomerBoardIsActive { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public DateTime CustomerBoardActiveFrom { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public DateTime CustomerBoardActiveTo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int RegularGridAllowed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int RegularGridUsed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int RegularGridLeft { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int RegularGridConsumed { get; set; }
        /// <summary>
        /// 
        /// </summary>

        [DataMember]
        public int GuestGridAllowed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int GuestGridUsed { get; set; }
        /// <summary>
        /// 
        /// </summary>

        [DataMember]
        public int GuestGridLeft { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int GuestGridConsumed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int PeriodAllowed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int PeriodUsed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int PeriodLeft { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int PeriodConsumed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int DayAllowed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int DayUsed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int DayLeft { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int DayConsumed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int WeekAllowed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int WeekUsed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int WeekLeft { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int WeekConsumed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int MonthAllowed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int MonthUsed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int MonthLeft { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int MonthConsumed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int SemesterQuarterAllowed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int SemesterQuarterUsed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int SemesterQuarterLeft { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int SemesterQuarterConsumed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int YearAllowed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int YearUsed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int YearLeft { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int YearConsumed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int GuestTotalAllowed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int GuestTotalUsed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int GuestTotalLeft { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int GuestTotalConsumed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int ExtraAllowed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int ExtraUsed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int ExtraLeft { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int ExtraConsumed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int TransferLeft { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int TransferConsumed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int ExtraTransferConsumedtype { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public ResetFrequency TransferMealsBy { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public ResetFrequency GuestReset { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public ResetFrequency ExtraReset { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public ResetFrequency LimitPeriodBy { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public ResetFrequency ExtraIfExceeded { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int ExtraIfExceededControl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool BpCashequivforGuest { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool BpRegMealsBeforeGuest { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public decimal CashEquivalentAmount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int BoardCountMaxAvailable { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int CurrentPeriod { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int ActualPeriod { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int DayOfWeek { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int TypeUsed { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool PeriodRolled { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool DayRolled { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool WeekRolled { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public DateTime WeekRolledDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool MonthRolled { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public DateTime MonthRolledDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool SemesterQuarterRolled { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public DateTime SemesterQuarterRolledDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool YearRolled { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public DateTime YearRolledDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool GuestRolled { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool ExtraRolled { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool ShowBoardCount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool PrintBoardCount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int MaxCountContribution { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool IsOnline { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int ErrorCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public string ErrorText { get; set; }
    }
}