﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.BoardPlan
{
    public class BoardPlanVerifyResponse
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value> The identifier. </value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value> The name. </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the is active.
        /// </summary>
        /// <value> The is active. </value>
        public bool? IsActive { get; set; }

        /// <summary>
        /// Gets or sets the active from.
        /// </summary>
        /// <value> The active from. </value>
        public DateTime? ActiveFrom { get; set; }

        /// <summary>
        /// Gets or sets the active to.
        /// </summary>
        /// <value> The active to. </value>
        public DateTime? ActiveTo { get; set; }

        /// <summary>
        /// The unique GUID identifier associated with the board plan.
        /// </summary>
        public Guid? BoardPlanGuid { get; set; }
    }
}
