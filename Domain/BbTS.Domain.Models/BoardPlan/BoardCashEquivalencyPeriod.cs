﻿namespace BbTS.Domain.Models.BoardPlan
{
    /// <summary>
    /// Container class for board cash equivalency periods.
    /// </summary>
    public class BoardCashEquivalencyPeriod
    {
        /// <summary>
        /// Period Number.
        /// </summary>
        public int PeriodNumber { get; set; }

        /// <summary>
        /// Period definitions for Monday.  (Monday = 1)
        /// </summary>
        public BoardCashEquivalencyPeriodDay Monday { get; set; }

        /// <summary>
        /// Period definitions for Tuesday.
        /// </summary>
        public BoardCashEquivalencyPeriodDay Tuesday { get; set; }

        /// <summary>
        /// Period definitions for Wednesday.
        /// </summary>
        public BoardCashEquivalencyPeriodDay Wednesday { get; set; }

        /// <summary>
        /// Period definitions for Thursday.
        /// </summary>
        public BoardCashEquivalencyPeriodDay Thursday { get; set; }

        /// <summary>
        /// Period definitions for Friday.
        /// </summary>
        public BoardCashEquivalencyPeriodDay Friday { get; set; }

        /// <summary>
        /// Period definitions for Saturday.
        /// </summary>
        public BoardCashEquivalencyPeriodDay Saturday { get; set; }

        /// <summary>
        /// Period definitions for Sunday. (Sunday = 0)
        /// </summary>
        public BoardCashEquivalencyPeriodDay Sunday { get; set; }

        /// <summary>
        /// Offline cash equiv value.
        /// </summary>
        public decimal OfflineCashEquivValue { get; set; }
    }

    /// <summary>
    /// View for a <see cref="BoardCashEquivalencyPeriod"/>.  (Version 1)
    /// </summary>
    public class BoardCashEquivalencyPeriodViewV01
    {
        /// <summary>
        /// Period Number.
        /// </summary>
        public int PeriodNumber { get; set; }

        /// <summary>
        /// Period definitions for Monday.  (Monday = 1)
        /// </summary>
        public BoardCashEquivalencyPeriodDayViewV01 Monday { get; set; }

        /// <summary>
        /// Period definitions for Tuesday.
        /// </summary>
        public BoardCashEquivalencyPeriodDayViewV01 Tuesday { get; set; }

        /// <summary>
        /// Period definitions for Wednesday.
        /// </summary>
        public BoardCashEquivalencyPeriodDayViewV01 Wednesday { get; set; }

        /// <summary>
        /// Period definitions for Thursday.
        /// </summary>
        public BoardCashEquivalencyPeriodDayViewV01 Thursday { get; set; }

        /// <summary>
        /// Period definitions for Friday.
        /// </summary>
        public BoardCashEquivalencyPeriodDayViewV01 Friday { get; set; }

        /// <summary>
        /// Period definitions for Saturday.
        /// </summary>
        public BoardCashEquivalencyPeriodDayViewV01 Saturday { get; set; }

        /// <summary>
        /// Period definitions for Sunday. (Sunday = 0)
        /// </summary>
        public BoardCashEquivalencyPeriodDayViewV01 Sunday { get; set; }

        /// <summary>
        /// Offline cash equiv value.
        /// </summary>
        public decimal OfflineCashEquivValue { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="BoardCashEquivalencyPeriod"/> conversion.
    /// </summary>
    public static class BoardCashEquivalencyPeriodConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="BoardCashEquivalencyPeriodViewV01"/> object based on this <see cref="BoardCashEquivalencyPeriod"/>.
        /// </summary>
        /// <param name="boardCashEquivalencyPeriod"></param>
        /// <returns></returns>
        public static BoardCashEquivalencyPeriodViewV01 ToBoardCashEquivalencyPeriodViewV01(this BoardCashEquivalencyPeriod boardCashEquivalencyPeriod)
        {
            if (boardCashEquivalencyPeriod == null) return null;

            return new BoardCashEquivalencyPeriodViewV01
            {
                PeriodNumber = boardCashEquivalencyPeriod.PeriodNumber,
                Monday = boardCashEquivalencyPeriod.Monday.ToBoardCashEquivalencyPeriodDayViewV01(),
                Tuesday = boardCashEquivalencyPeriod.Tuesday.ToBoardCashEquivalencyPeriodDayViewV01(),
                Wednesday = boardCashEquivalencyPeriod.Wednesday.ToBoardCashEquivalencyPeriodDayViewV01(),
                Thursday = boardCashEquivalencyPeriod.Thursday.ToBoardCashEquivalencyPeriodDayViewV01(),
                Friday = boardCashEquivalencyPeriod.Friday.ToBoardCashEquivalencyPeriodDayViewV01(),
                Saturday = boardCashEquivalencyPeriod.Saturday.ToBoardCashEquivalencyPeriodDayViewV01(),
                Sunday = boardCashEquivalencyPeriod.Sunday.ToBoardCashEquivalencyPeriodDayViewV01(),
                OfflineCashEquivValue = boardCashEquivalencyPeriod.OfflineCashEquivValue
            };
        }

        /// <summary>
        /// Returns a <see cref="BoardCashEquivalencyPeriod"/> object based on this <see cref="BoardCashEquivalencyPeriodViewV01"/>.
        /// </summary>
        /// <param name="boardCashEquivalencyPeriodViewV01"></param>
        /// <returns></returns>
        public static BoardCashEquivalencyPeriod ToBoardCashEquivalencyPeriod(this BoardCashEquivalencyPeriodViewV01 boardCashEquivalencyPeriodViewV01)
        {
            if (boardCashEquivalencyPeriodViewV01 == null) return null;

            return new BoardCashEquivalencyPeriod
            {
                PeriodNumber = boardCashEquivalencyPeriodViewV01.PeriodNumber,
                Monday = boardCashEquivalencyPeriodViewV01.Monday.ToBoardCashEquivalencyPeriodDay(),
                Tuesday = boardCashEquivalencyPeriodViewV01.Tuesday.ToBoardCashEquivalencyPeriodDay(),
                Wednesday = boardCashEquivalencyPeriodViewV01.Wednesday.ToBoardCashEquivalencyPeriodDay(),
                Thursday = boardCashEquivalencyPeriodViewV01.Thursday.ToBoardCashEquivalencyPeriodDay(),
                Friday = boardCashEquivalencyPeriodViewV01.Friday.ToBoardCashEquivalencyPeriodDay(),
                Saturday = boardCashEquivalencyPeriodViewV01.Saturday.ToBoardCashEquivalencyPeriodDay(),
                Sunday = boardCashEquivalencyPeriodViewV01.Sunday.ToBoardCashEquivalencyPeriodDay(),
                OfflineCashEquivValue = boardCashEquivalencyPeriodViewV01.OfflineCashEquivValue
            };
        }
        #endregion
    }
}
