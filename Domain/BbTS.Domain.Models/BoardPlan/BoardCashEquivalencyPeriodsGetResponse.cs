﻿namespace BbTS.Domain.Models.BoardPlan
{
    /// <summary>
    /// Container class for the response to a board cash equivalency periods get request.
    /// </summary>
    public class BoardCashEquivalencyPeriodsGetResponse
    {
        /// <summary>
        /// Unique identifier for the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The requested board cash equivalency period.
        /// </summary>
        public BoardCashEquivalencyPeriod BoardCashEquivalencyPeriod { get; set; }

        /// <summary>
        /// The error code associated with the response.  0 means success.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Reason for failure (if there was a failure error code).
        /// </summary>
        public string DeniedText { get; set; }
    }
}
