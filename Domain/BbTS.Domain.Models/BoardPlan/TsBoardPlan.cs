﻿using System;

namespace BbTS.Domain.Models.BoardPlan
{
    /// <summary>
    /// This object represents a record in the Transact system for a board plan
    /// </summary>
    public class TsBoardPlan
    {
        /// <summary>
        /// The identity of this board plan
        /// </summary>
        public Int32 BoardPlanId { get; set; }
        /// <summary>
        /// The name of this board plan
        /// </summary>
        public String BoardPlanName { get; set; }
        /// <summary>
        /// Whether this record is active or not
        /// </summary>
        public Boolean IsActive { get; set; }
        /// <summary>
        /// How this board plan is limited (e.g. D=Day, W=Week, M=Month, S=Semester/Quarter, Y=Year)
        /// </summary>
        public String LimitPeriodBy { get; set; }
        /// <summary>
        /// How many board plan usages are allowed per week
        /// </summary>
        public Int32 WeekAllowed { get; set; }
        /// <summary>
        /// How many board plan usages are allowed per month
        /// </summary>
        public Int32 MonthAllowed { get; set; }
        /// <summary>
        /// How many board plan usages are allowed per semester/quarter
        /// </summary>
        public Int32 SemesterQuarterAllowed { get; set; }
        /// <summary>
        /// How many board plan usages are allowed per year
        /// </summary>
        public Int32 YearAllowed { get; set; }
        /// <summary>
        /// How many board plan usages are allowed per guest
        /// </summary>
        public Int32 GuestAllowed { get; set; }
        /// <summary>
        /// Whether the holiday calendar should be used
        /// </summary>
        public Boolean UseHolidayCalander { get; set; }
        /// <summary>
        /// Whether multiple board plans can be used in a single transaction
        /// </summary>
        public String AllowMultipleMeals { get; set; }
        /// <summary>
        /// The board plan start date
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// The board plan end date
        /// </summary>
        public DateTime EndDate { get; set; }
        /// <summary>
        /// The frequency for which guest meals are reset (e.g. D=Day, W=Week, M=Month, S=Semester/Quarter, Y=Year)
        /// </summary>
        public String GuestReset { get; set; }
        /// <summary>
        /// How much cash a board plan is worth for a guest
        /// </summary>
        public Boolean CashEquivalentForGuest { get; set; }
        /// <summary>
        /// Whether regular meals must be used before guest counts can be consumed
        /// </summary>
        public Boolean RegularMealsBeforeGuest { get; set; }
        /// <summary>
        /// The maximum number of meals to be transfered between periods
        /// </summary>
        public Int32 MaximumTransferMeals { get; set; }
        /// <summary>
        /// The frequency for transfering meals (e.g. D=Day, W=Week, M=Month, S=Semester/Quarter, Y=Year)
        /// </summary>
        public String TransferMealsBy { get; set; }
        /// <summary>
        /// Whether to carry over previous transfers
        /// </summary>
        public String TransferPreviousTransfers { get; set; }
        /// <summary>
        /// The frequency for extra meals (e.g. D=Day, W=Week, M=Month, S=Semester/Quarter, Y=Year)
        /// </summary>
        public String ExtraReset { get; set; }
        /// <summary>
        /// What type to use if extra are exceeded
        /// </summary>
        public String ExtraIfExceeded { get; set; }
        /// <summary>
        /// Update Customers board grid Default(0), Never(-1), Monday-Friday(1-7)
        /// </summary>
        public Int32 ControlGridUpdate { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 AllowedPeriod1 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 AllowedPeriod2 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 AllowedPeriod3 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 AllowedPeriod4 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 AllowedPeriod5 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 AllowedPeriod6 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 AllowedDayOfWeek1 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 AllowedDayOfWeek2 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 AllowedDayOfWeek3 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 AllowedDayOfWeek4 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 AllowedDayOfWeek5 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 AllowedDayOfWeek6 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 AllowedDayOfWeek7 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek1Period1 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek1Period2 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek1Period3 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek1Period4 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek1Period5 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek1Period6 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek2Period1 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek2Period2 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek2Period3 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek2Period4 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek2Period5 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek2Period6 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek3Period1 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek3Period2 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek3Period3 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek3Period4 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek3Period5 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek3Period6 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek4Period1 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek4Period2 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek4Period3 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek4Period4 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek4Period5 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek4Period6 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek5Period1 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek5Period2 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek5Period3 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek5Period4 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek5Period5 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek5Period6 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek6Period1 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek6Period2 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek6Period3 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek6Period4 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek6Period5 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek6Period6 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek7Period1 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek7Period2 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek7Period3 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek7Period4 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek7Period5 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 GuestAllowedDayOfWeek7Period6 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek1Period1 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek1Period2 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek1Period3 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek1Period4 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek1Period5 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek1Period6 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek2Period1 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek2Period2 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek2Period3 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek2Period4 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek2Period5 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek2Period6 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek3Period1 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek3Period2 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek3Period3 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek3Period4 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek3Period5 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek3Period6 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek4Period1 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek4Period2 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek4Period3 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek4Period4 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek4Period5 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek4Period6 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek5Period1 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek5Period2 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek5Period3 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek5Period4 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek5Period5 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek5Period6 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek6Period1 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek6Period2 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek6Period3 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek6Period4 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek6Period5 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek6Period6 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek7Period1 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek7Period2 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek7Period3 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek7Period4 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek7Period5 { get; set; }
        /// <summary>
        /// The number of meals allowed for this time span
        /// </summary>
        public Int32 RegularAllowedDayOfWeek7Period6 { get; set; }
        /// <summary>
        /// Board plan guid
        /// </summary>
        public string BoardPlanGuid { get; set; }
    }
}
