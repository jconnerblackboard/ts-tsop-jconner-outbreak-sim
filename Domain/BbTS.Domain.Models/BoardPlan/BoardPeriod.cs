﻿using System;

namespace BbTS.Domain.Models.BoardPlan
{
    /// <summary>
    /// Container class to hold information about a board period.
    /// </summary>
    public class BoardPeriod
    {
        /// <summary>
        /// Board Period Number.
        /// </summary>
        public int Period { get; set; }

        /// <summary>
        /// Board Period Day.
        /// </summary>
        public DayOfWeek BoardPeriodDay { get; set; }

        /// <summary>
        /// Profit Center ID.
        /// </summary>
        public int ProfitCenterId { get; set; }

        /// <summary>
        /// Board Period Name.
        /// </summary>
        public string BoardPeriodName { get; set; }

        /// <summary>
        /// Board Period Start Date.
        /// </summary>
        public DateTime? BoardPeriodStartDateTime { get; set; }

        /// <summary>
        /// Board Period Start Date.
        /// </summary>
        public DateTime? BoardPeriodStopDateTime { get; set; }

        /// <summary>
        /// CE Period Start Date.
        /// </summary>
        public DateTime? CePeriodStartDateTime { get; set; }

        /// <summary>
        /// CE Period Start Date.
        /// </summary>
        public DateTime? CePeriodStopDateTime { get; set; }

        /// <summary>
        /// The UTC offset of the included DateTime objects.
        /// </summary>
        public string DateTimeOffset { get; set; }
    }

    /// <summary>
    /// View for a <see cref="BoardPeriod"/>.  (Version 1)
    /// </summary>
    public class BoardPeriodViewV01
    {
        /// <summary>
        /// Board Period Number.
        /// </summary>
        public int Period { get; set; }

        /// <summary>
        /// Board Period Day.
        /// </summary>
        public DayOfWeek BoardPeriodDay { get; set; }

        /// <summary>
        /// Profit Center ID.
        /// </summary>
        public int ProfitCenterId { get; set; }

        /// <summary>
        /// Board Period Name.
        /// </summary>
        public string BoardPeriodName { get; set; }

        /// <summary>
        /// Board Period Start Date.
        /// </summary>
        public DateTime? BoardPeriodStartDateTime { get; set; }

        /// <summary>
        /// Board Period Start Date.
        /// </summary>
        public DateTime? BoardPeriodStopDateTime { get; set; }

        /// <summary>
        /// CE Period Start Date.
        /// </summary>
        public DateTime? CePeriodStartDateTime { get; set; }

        /// <summary>
        /// CE Period Start Date.
        /// </summary>
        public DateTime? CePeriodStopDateTime { get; set; }

        /// <summary>
        /// The UTC offset of the included DateTime objects.
        /// </summary>
        public string DateTimeOffset { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="BoardPeriod"/> conversion.
    /// </summary>
    public static class BoardPeriodConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="BoardPeriodViewV01"/> object based on this <see cref="BoardPeriod"/>.
        /// </summary>
        /// <param name="boardPeriod"></param>
        /// <returns></returns>
        public static BoardPeriodViewV01 ToBoardPeriodViewV01(this BoardPeriod boardPeriod)
        {
            if (boardPeriod == null) return null;

            return new BoardPeriodViewV01
            {
                Period = boardPeriod.Period,
                BoardPeriodDay = boardPeriod.BoardPeriodDay,
                ProfitCenterId = boardPeriod.ProfitCenterId,
                BoardPeriodName = boardPeriod.BoardPeriodName,
                BoardPeriodStartDateTime = boardPeriod.BoardPeriodStartDateTime,
                BoardPeriodStopDateTime = boardPeriod.BoardPeriodStopDateTime,
                CePeriodStartDateTime = boardPeriod.CePeriodStartDateTime,
                CePeriodStopDateTime = boardPeriod.CePeriodStopDateTime,
                DateTimeOffset = boardPeriod.DateTimeOffset
            };
        }

        /// <summary>
        /// Returns a <see cref="BoardPeriod"/> object based on this <see cref="BoardPeriodViewV01"/>.
        /// </summary>
        /// <param name="boardPeriodViewV01"></param>
        /// <returns></returns>
        public static BoardPeriod ToBoardPeriod(this BoardPeriodViewV01 boardPeriodViewV01)
        {
            if (boardPeriodViewV01 == null) return null;

            return new BoardPeriod
            {
                Period = boardPeriodViewV01.Period,
                BoardPeriodDay = boardPeriodViewV01.BoardPeriodDay,
                ProfitCenterId = boardPeriodViewV01.ProfitCenterId,
                BoardPeriodName = boardPeriodViewV01.BoardPeriodName,
                BoardPeriodStartDateTime = boardPeriodViewV01.BoardPeriodStartDateTime,
                BoardPeriodStopDateTime = boardPeriodViewV01.BoardPeriodStopDateTime,
                CePeriodStartDateTime = boardPeriodViewV01.CePeriodStartDateTime,
                CePeriodStopDateTime = boardPeriodViewV01.CePeriodStopDateTime,
                DateTimeOffset = boardPeriodViewV01.DateTimeOffset
            };
        }
        #endregion
    }
}
