﻿using System;

namespace BbTS.Domain.Models.BoardPlan
{
    /// <summary>
    ///     BoardPlan Summary Model
    /// </summary>
    public class TsBoardPlanSummary
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="TsBoardPlanSummary"/> is active.
        /// </summary>
        /// <value>
        ///   <c>true</c> if active; otherwise, <c>false</c>.
        /// </value>
        public bool? Active { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the stop date.
        /// </summary>
        public DateTime? StopDate { get; set; }
    }
}
