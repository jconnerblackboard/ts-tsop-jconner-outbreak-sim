﻿using System;

namespace BbTS.Domain.Models.BoardPlan
{
    /// <summary>
    /// Board plan exclusion
    /// </summary>
    public class BoardPlanExclusion
    {
        /// <summary>
        /// Profit center Id
        /// </summary>
        public int ProfitCenterId { get; set; }

        /// <summary>
        /// Profit center name
        /// </summary>
        public string ProfitCenterName { get; set; }

        /// <summary>
        /// Transaction Number
        /// </summary>
        public int TransactionNumber { get; set; }

        /// <summary>
        /// Transaction date time
        /// </summary>
        public DateTime? TransactionDateTime { get; set; }

        /// <summary>
        /// Board plan Id
        /// </summary>
        public int BoardPlanId { get; set; }

        /// <summary>
        /// Board plan name
        /// </summary>
        public string BoardPlanName { get; set; }

        /// <summary>
        /// Meal period Id
        /// </summary>
        public int MealPeriodId { get; set; }

        /// <summary>
        /// Meal period name
        /// </summary>
        public string MealPeriodName { get; set; }

        /// <summary>
        /// Excluded profit center list
        /// </summary>
        public string ExcludedProfitCenterList { get; set; }

        /// <summary>
        /// Excluded board plan list
        /// </summary>
        public string ExcludedBoardPlanList { get; set; }

        /// <summary>
        /// Excluded meal period list
        /// </summary>
        public string ExcludedMealPeriodList { get; set; }

        /// <summary>
        /// Period exclusion Id
        /// </summary>
        public decimal PeriodExclusionId { get; set; }

        /// <summary>
        /// Period exclusion name
        /// </summary>
        public string PeriodExclusionName { get; set; }
    }
}
