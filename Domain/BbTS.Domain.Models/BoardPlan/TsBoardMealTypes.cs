﻿using System;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.BoardPlan
{
    /// <summary>
    /// This object represents a Transact BoardMealTypes object. Board Meal Types (limited to 8 records)
    /// </summary>
    [Serializable]
    public class TsBoardMealTypes
    {
        /// <summary>
        /// Board Meal Type Identifier
        /// </summary>
        [XmlAttribute]
        public int BoardMt_Id { get; set; }
        /// <summary>
        /// Meal Type Name
        /// </summary>
        [XmlAttribute]
        public string MtName { get; set; }
        /// <summary>
        /// Period Number to apply the Board Transaction to.  0 means 'Current Period'.
        /// </summary>
        [XmlAttribute]
        public int ApplyTo { get; set; }
    }
}