﻿namespace BbTS.Domain.Models.BoardPlan
{
    /// <summary>
    /// Container class for the response to a board information get request.
    /// </summary>
    public class BoardInformationGetResponse
    {
        /// <summary>
        /// Unique identifier for the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The board information for the requested customer at the requested device.
        /// </summary>
        public CustomerBoardCount CustomerBoardCount { get; set; }
    }
}
