﻿namespace BbTS.Domain.Models.BoardPlan
{
    /// <summary>
    /// Container class for a meal type on a device
    /// </summary>
    public class MealTypeDeviceSetting
    {
        /// <summary>
        /// Board Meal Type Identifier
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Meal Type Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Period Number to apply the Board Transaction to.  0 means 'Current Period'.
        /// </summary>
        public int ApplyTo { get; set; }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="tsBoardMealType"><see cref="TsBoardMealTypes"/> to convert to a <see cref="MealTypeDeviceSetting"/></param>
        public MealTypeDeviceSetting(TsBoardMealTypes tsBoardMealType)
        {
            Id = tsBoardMealType.BoardMt_Id;
            Name = tsBoardMealType.MtName;
            ApplyTo = tsBoardMealType.ApplyTo;
        }

        /// <summary>
        /// Empty constructor.
        /// </summary>
        public MealTypeDeviceSetting()
        {
        }
    }

    /// <summary>
    /// View for a <see cref="MealTypeDeviceSetting"/>.  (Version 1)
    /// </summary>
    public class MealTypeDeviceSettingViewV01
    {
        /// <summary>
        /// Board Meal Type Identifier
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Meal Type Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Period Number to apply the Board Transaction to.  0 means 'Current Period'.
        /// </summary>
        public int ApplyTo { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="MealTypeDeviceSetting"/> conversion.
    /// </summary>
    public static class MealTypeDeviceSettingConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="MealTypeDeviceSettingViewV01"/> object based on this <see cref="MealTypeDeviceSetting"/>.
        /// </summary>
        /// <param name="mealTypeDeviceSetting"></param>
        /// <returns></returns>
        public static MealTypeDeviceSettingViewV01 ToMealTypeDeviceSettingViewV01(this MealTypeDeviceSetting mealTypeDeviceSetting)
        {
            if (mealTypeDeviceSetting == null) return null;

            return new MealTypeDeviceSettingViewV01
            {
                Id = mealTypeDeviceSetting.Id,
                Name = mealTypeDeviceSetting.Name,
                ApplyTo = mealTypeDeviceSetting.ApplyTo
            };
        }

        /// <summary>
        /// Returns a <see cref="MealTypeDeviceSetting"/> object based on this <see cref="MealTypeDeviceSettingViewV01"/>.
        /// </summary>
        /// <param name="mealTypeDeviceSettingViewV01"></param>
        /// <returns></returns>
        public static MealTypeDeviceSetting ToMealTypeDeviceSetting(this MealTypeDeviceSettingViewV01 mealTypeDeviceSettingViewV01)
        {
            if (mealTypeDeviceSettingViewV01 == null) return null;

            return new MealTypeDeviceSetting
            {
                Id = mealTypeDeviceSettingViewV01.Id,
                Name = mealTypeDeviceSettingViewV01.Name,
                ApplyTo = mealTypeDeviceSettingViewV01.ApplyTo
            };
        }
        #endregion
    }
}
