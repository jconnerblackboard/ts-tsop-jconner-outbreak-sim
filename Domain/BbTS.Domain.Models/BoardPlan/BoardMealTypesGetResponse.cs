﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.BoardPlan
{
    /// <summary>
    /// Container class for the response to a board meal types get request.
    /// </summary>
    public class BoardMealTypesGetResponse
    {
        /// <summary>
        /// Board meal types
        /// </summary>
        public List<MealTypeDeviceSetting> MealTypes { get; set; }
    }
}
