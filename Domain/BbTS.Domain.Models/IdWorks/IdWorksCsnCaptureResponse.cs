﻿namespace BbTS.Domain.Models.IdWorks
{
    /// <summary>
    /// Container class for the response to an ID Works CSN Capture request.
    /// </summary>
    public class IdWorksCsnCaptureResponse
    {
        /// <summary>
        /// Unique identifier for the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The response code.  0 = Success.
        /// </summary>
        public string ResponseCode { get; set; }

        /// <summary>
        /// Response message.
        /// </summary>
        public string Message { get; set; }
    }
}
