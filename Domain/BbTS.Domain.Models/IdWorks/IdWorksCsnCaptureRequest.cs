﻿namespace BbTS.Domain.Models.IdWorks
{
    /// <summary>
    /// Container class for an ID Works CSN Capture request.
    /// </summary>
    public class IdWorksCsnCaptureRequest
    {
        /// <summary>
        /// Unique identifier for the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// Track 2 Data.
        /// </summary>
        public string Track2Data { get; set; }

        /// <summary>
        /// Card Serial Number.
        /// </summary>
        public string CardSerialNumber { get; set; }

    }
}
