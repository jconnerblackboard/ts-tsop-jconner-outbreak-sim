﻿using System;
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.IdWorks
{
    [Serializable]
    public class Idw_Card
    {
        public int Record_Id { get; set; }
        public string Person_Number { get; set; }
        public string Card_Number { get; set; }
        public string Card_Status { get; set; }
        public string Card_Format { get; set; }
        public string Description { get; set; }
        public DateTime Creation_Date { get; set; }
        public DateTime Start_Date { get; set; }
        public DateTime End_Date { get; set; }
        public DateTime Print_Date { get; set; }
        public DateTime Cancelled_Date { get; set; }
        public int Prints_Count { get; set; }
        public string First_Name { get; set; }
        public string Middle_Name { get; set; }
        public string Last_Name { get; set; }
        public string Library_Number { get; set; }
        public string Card_Date { get; set; }
        public string Title { get; set; }
        public string Department { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Province { get; set; }
        public string Zip_Code { get; set; }
        public string Country { get; set; }
        public string Company { get; set; }
        public string Office { get; set; }
        public string Email_Address { get; set; }
        public string Postal_Code { get; set; }
        public string Social_Security { get; set; }
        public DateTime Date_Of_Birth { get; set; }
        public byte[] Photo_Blob { get; set; }
        public string Photo_File { get; set; }
        public byte[] Sign_Blob { get; set; }
        public string Sign_File { get; set; }
        public string CustomField1 { get; set; }
        public string CustomField2 { get; set; }
        public string CustomField3 { get; set; }
        public string CustomField4 { get; set; }
        public string CustomField5 { get; set; }
        public string CustomField6 { get; set; }
        public string CustomField7 { get; set; }
        public string CustomField8 { get; set; }
        public string CustomField9 { get; set; }
        public string CustomField10 { get; set; }
        public string CustomField11 { get; set; }
        public string CustomField12 { get; set; }
        public string CustomField13 { get; set; }
        public string CustomField14 { get; set; }
        public string CustomField15 { get; set; }
        public string CustomField16 { get; set; }
        public string CustomField17 { get; set; }
        public string CustomField18 { get; set; }
        public string CustomField19 { get; set; }
        public string CustomField20 { get; set; }
        public string Card_Idm { get; set; }
    }
}
