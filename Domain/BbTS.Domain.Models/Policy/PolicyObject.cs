﻿using System;

namespace BbTS.Domain.Models.Policy
{
    /// <summary>
    /// An abstract class for describing a policy-related instance.
    /// </summary>
    public abstract class PolicyObject : ManagedElement
    {
        /// <summary>
        /// Unique identifier for this PolicyObject (GUID)
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// A user-friendly name of a policy-related object.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// A set of keywords for characterizing /categorizing policy objects.
        /// </summary>
        public string PolicyKeywords { get; set; }

        /// <summary>
        /// PolicyObject constructor.  Generates the Id, but this can be changed later if needed.
        /// </summary>
        protected PolicyObject()
        {
            Id = Guid.NewGuid().ToString("D");
        }
    }
}