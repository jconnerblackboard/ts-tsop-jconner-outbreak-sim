﻿namespace BbTS.Domain.Models.Policy.Conditions
{
    /// <summary>
    /// A condition that always evaluates to true.
    /// </summary>
    public class TrueCondition : PolicyCondition
    {
    }
}
