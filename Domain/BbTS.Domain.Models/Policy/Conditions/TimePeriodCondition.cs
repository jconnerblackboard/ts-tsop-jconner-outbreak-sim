﻿using System;

namespace BbTS.Domain.Models.Policy.Conditions
{
    /// <summary>
    /// A class that provides the capability of enabling / disabling a policy rule according to a pre-determined schedule.
    /// </summary>
    public class TimePeriodCondition : PolicyCondition
    {
        /// <summary>
        /// The beginning of the range of calendar dates on which a policy rule is valid.  The ending date/time must be later than the beginning date/time.
        /// </summary>
        public DateTimeOffset? TimePeriodBegin { get; set; }

        /// <summary>
        /// The ending of the range of calendar dates on which a policy rule is valid.  The ending date/time must be later than the beginning date/time.
        /// </summary>
        public DateTimeOffset? TimePeriodEnd { get; set; }

        /// <summary>
        /// A mask identifying the months of the year in which a policy rule is valid.
        /// </summary>
        public string MonthOfYearMask { get; set; }

        /// <summary>
        /// A mask identifying the days of the month on which a policy rule is valid, counting forward from the beginning of the month.
        /// </summary>
        public string DayOfMonthForwardMask { get; set; }

        /// <summary>
        /// A mask identifying the days of the month on which a policy rule is valid, counting backward from the end of the month.
        /// </summary>
        public string DayOfMonthBackwardMask { get; set; }

        /// <summary>
        /// A mask identifying the days of the week on which a policy rule is valid.
        /// </summary>
        public string DayOfWeekMask { get; set; }

        /// <summary>
        /// The beginning of the range of times at which a policy rule is valid.  If the end time is earlier than the beginning, then the interval spans midnight.
        /// </summary>
        public TimeSpan? TimeOfDayBegin { get; set; }

        /// <summary>
        /// The ending of the range of times at which a policy rule is valid.  If the end time is earlier than the beginning, then the interval spans midnight.
        /// </summary>
        public TimeSpan? TimeOfDayEnd { get; set; }
        
        /// <summary>
        /// This property indicates whether the times represented in the TimePeriod property and
        /// in the various Mask properties represent local times or UTC times.  There is no provision
        /// for mixing of local times and UTC times:  the value of this property applies to all of
        /// the other time-related properties.
        /// </summary>
        public bool IsLocalTime { get; set; }
    }
}
