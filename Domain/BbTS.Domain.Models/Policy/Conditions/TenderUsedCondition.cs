﻿namespace BbTS.Domain.Models.Policy.Conditions
{
    /// <summary>
    /// A condition that evaluates to true if the tender used matches the specified tender.
    /// </summary>
    public class TenderUsedCondition : PolicyCondition
    {
        /// <summary>
        /// BbTS DB generated ID (corresponds to TENDER_ID in POLICY_CONDITION_VALUE_TENDER).
        /// </summary>
        public int TenderId { get; set; }
    }
}
