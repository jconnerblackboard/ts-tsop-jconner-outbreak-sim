﻿namespace BbTS.Domain.Models.Policy.Conditions
{
    public class ConnectivityCondition : PolicyCondition
    {
        public string ResourceId { get; set; }
        public string Name { get; set; }
        public bool IsConnected { get; set; }
    }
}
