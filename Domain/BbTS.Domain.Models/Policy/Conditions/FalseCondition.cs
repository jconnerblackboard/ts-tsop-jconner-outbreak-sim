﻿namespace BbTS.Domain.Models.Policy.Conditions
{
    /// <summary>
    /// A condition that always evaluates to false.
    /// </summary>
    public class FalseCondition : PolicyCondition
    {
    }
}
