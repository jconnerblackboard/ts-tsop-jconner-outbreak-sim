﻿namespace BbTS.Domain.Models.Policy.Conditions
{
    /// <summary>
    /// A class representing a rule-specific or reusable policy condition to be evaluated in conjunction with a policy rule.
    /// </summary>
    public abstract class PolicyCondition : PolicyObject
    {
        /// <summary>
        /// BbTS DB assigned identifier.
        /// </summary>
        public int PolicyConditionId { get; set; }

        /// <summary>
        /// Indicates how many times this PolicyCondition is referenced in the entire policy tree.
        /// </summary>
        public int UsageCount { get; set; }
    }
}