﻿using System.Collections.Generic;
using BbTS.Domain.Models.Definitions.Policy;

namespace BbTS.Domain.Models.Policy.Conditions
{
    /// <summary>
    /// A subclass of PolicyCondition that introduces the ConditionListType property, used for assigning DNF / CNF semantics to subordinate policy conditions.
    /// </summary>
    public class CompoundPolicyCondition : PolicyCondition
    {
        /// <summary>
        /// Child policy conditions.
        /// </summary>
        public List<PolicyCondition> Children { get; set; }

        /// <summary>
        /// List of negation flags that apply to this compound policy condition's children.  (TRUE indicates that the child condition is negated, FALSE indicates that it is not negated.)
        /// </summary>
        public List<bool> NegationFlags { get; set; }

        /// <summary>
        /// DNF or CNF.
        /// </summary>
        public ConditionListType ConditionListType { get; set; }

        /// <summary>
        /// Parameter-less constructor that ensures the Children list is created.
        /// </summary>
        public CompoundPolicyCondition()
        {
            Name = "<compound>";
            Children = new List<PolicyCondition>();
            NegationFlags = new List<bool>();
            ConditionListType = ConditionListType.Disjunctive;
        }
    }
}
