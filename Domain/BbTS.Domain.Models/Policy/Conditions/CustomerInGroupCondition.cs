﻿namespace BbTS.Domain.Models.Policy.Conditions
{
    /// <summary>
    /// A condition that evaluates to true if the customer is in the specified custom defined group.
    /// </summary>
    public class CustomerInGroupCondition : PolicyCondition
    {
        /// <summary>
        /// BbTS DB generated ID (corresponds to CUSTOMER_DEF_GRP_DEF_ITEM_ID in POLICY_CONDITION_VALUE_CUSTGRP).
        /// </summary>
        public int CustomerDefinedGroupItemId { get; set; }
    }
}
