﻿namespace BbTS.Domain.Models.Policy.Conditions
{
    /// <summary>
    /// A condition that evaluates to true if the POS is in the specified POS group.
    /// </summary>
    public class PosInGroupCondition : PolicyCondition
    {
        /// <summary>
        /// BbTS DB generated ID (corresponds to POS_GROUP_ID in POLICY_CONDITION_VALUE_POS_GRP).
        /// </summary>
        public int PosGroupId { get; set; }
    }
}
