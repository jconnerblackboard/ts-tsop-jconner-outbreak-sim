﻿using System;

// ReSharper disable InconsistentNaming
namespace BbTS.Domain.Models.Policy
{
    /// <summary>
    /// Defines a POLICY_TIMEPERIOD_CONDITION row.
    /// </summary>
    public class TsPolicy_TimePeriod_Condition
    {
        /// <summary>
        /// Policy Condition Association (from POLICY_CONDITION table)
        /// </summary>
        public int Policy_Condition_Id { get; set; }

        /// <summary>
        /// Beginning Date of Time Period Condition.  A Null column means and date prior to the end date is valid
        /// </summary>
        public DateTime? TimePeriod_Begin_Date { get; set; }

        /// <summary>
        /// Ending Date of Time Period Condtion. A null column means any date after the beginning date is valid
        /// </summary>
        public DateTime? TimePeriod_End_Date { get; set; }

        /// <summary>
        /// 12 byte Indicators (T or F) which show which months are valid.
        /// </summary>
        public string Month_Of_Year_Mask { get; set; }

        /// <summary>
        /// 62 byte indicator (T or F) which show what day of month is valid.  The first 31 bytes indicate the dates 1 thru 31 which could be valid.
        /// The second 31 bytes indicate in reverse order which dates can be valid.  So a string starting with T in the last 31 bytes indicates that
        /// the "last day of the month" is always valid
        /// </summary>
        public string Day_Of_Month_Mask { get; set; }

        /// <summary>
        /// 7 byte indicators (T or F) showing which day of the week is valid, starting with Sunday and ending with Saturday
        /// </summary>
        public string Day_Of_Week_Mask { get; set; }

        /// <summary>
        /// Time of Day where condition is valid.  Stored as HHMMSS - 24 hour
        /// </summary>
        public int Start_Time_Of_Day { get; set; }

        /// <summary>
        /// Ending time of day where policy is valid. Stored as HHMMSS - 24 hour
        /// </summary>
        public int End_Time_Of_Day { get; set; }
    }
}
