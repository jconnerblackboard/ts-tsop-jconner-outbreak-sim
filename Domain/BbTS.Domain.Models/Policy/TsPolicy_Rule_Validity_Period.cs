﻿// ReSharper disable InconsistentNaming
namespace BbTS.Domain.Models.Policy
{
    /// <summary>
    /// Defines a POLICY_RULE_VALIDITY_PERIOD row.
    /// </summary>
    public class TsPolicy_Rule_Validity_Period
    {
        /// <summary>
        /// Policy Condition Association (from POLICY_CONDITION table)
        /// </summary>
        public int Policy_Condition_Id { get; set; }

        /// <summary>
        /// Policy Set Association (from POLICY_SET table)
        /// </summary>
        public int Policy_Set_Id { get; set; }
    }
}
