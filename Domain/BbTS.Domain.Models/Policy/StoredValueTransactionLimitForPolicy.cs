﻿namespace BbTS.Domain.Models.Policy
{
    /// <summary>
    /// Holds transaction limit information used in policy logic.
    /// </summary>
    public class StoredValueTransactionLimitForPolicy
    {
        /// <summary>
        /// Stored value transaction limit ID.  Corresponds to SV_Transaction_Limit_ID in the database.
        /// </summary>
        public int SvTransactionLimitId { get; set; }

        /// <summary>
        /// Stored value account type ID.
        /// </summary>
        public int SvAccountTypeId { get; set; }

        /// <summary>
        /// Purchase amount limit.
        /// </summary>
        public decimal PurchaseAmountLimit { get; set; }

        /// <summary>
        /// Purchase count limit.
        /// </summary>
        public int PurchaseCountLimit { get; set; }

        /// <summary>
        /// Amount limit consumed.
        /// </summary>
        public decimal AmountLimitConsumed { get; set; }

        /// <summary>
        /// Count limit consumed.
        /// </summary>
        public int CountLimitConsumed { get; set; }
    }
}
