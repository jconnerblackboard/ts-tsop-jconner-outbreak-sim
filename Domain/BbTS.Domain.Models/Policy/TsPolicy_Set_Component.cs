﻿// ReSharper disable InconsistentNaming
namespace BbTS.Domain.Models.Policy
{
    /// <summary>
    /// Defines a POLICY_SET_COMPONENT row.
    /// </summary>
    public class TsPolicy_Set_Component
    {
        /// <summary>
        /// Policy Set Assocation (from POLICY_SET table)
        /// </summary>
        public int Parent_Policy_Set_Id { get; set; }

        /// <summary>
        /// Policy Set Assocation (from POLICY_SET table)
        /// </summary>
        public int Child_Policy_Set_Id { get; set; }

        /// <summary>
        /// Priority in which the children should be evaluated
        /// </summary>
        public int Priority { get; set; }
    }
}
