﻿using System;
using System.Collections.Generic;

namespace BbTS.Domain.Models.Policy
{
    /// <summary>
    /// Information about the customer that is needed for processing policy conditions.
    /// </summary>
    public class CustomerInformationForPolicy
    {
        /// <summary>
        /// Customer GUID.
        /// </summary>
        public Guid CustomerId { get; set; }

        /// <summary>
        /// List of CUSTOMER_DEF_GRP_DEF_ITEM_ID values that are both assigned to the customer and are configured in at least one CustomerInGroupCondition in the policy tree.
        /// </summary>
        public List<int> CustomerDefinedGroupDefinitionItemIdList { get; set; }
    }
}