﻿namespace BbTS.Domain.Models.Policy.Actions
{
    /// <summary>
    /// A class representing a rule-specific or reusable policy action to be performed if the condition for a policy rule evaluates to TRUE.
    /// </summary>
    public abstract class PolicyAction : PolicyObject
    {
        /// <summary>
        /// BbTS DB assigned identifier.
        /// </summary>
        public int PolicyActionId { get; set; }

        /// <summary>
        /// Indicates how many times this PolicyAction is referenced in the entire policy tree.
        /// </summary>
        public int UsageCount { get; set; }
    }
}