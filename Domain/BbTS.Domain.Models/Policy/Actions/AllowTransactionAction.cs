﻿namespace BbTS.Domain.Models.Policy.Actions
{
    /// <summary>
    /// An action that will result in the transaction being allowed.
    /// </summary>
    public class AllowTransactionAction : PolicyAction
    {
    }
}