﻿namespace BbTS.Domain.Models.Policy.Actions
{
    /// <summary>
    /// An action that will enforce the specified transaction limit and either allow or deny the transaction as a result.
    /// </summary>
    public class ApplyStoredValueTransactionLimit : PolicyAction
    {
        /// <summary>
        /// BbTS DB generated ID (corresponds to column SV_TRANSACTION_LIMIT_ID in POLICY_ACTION_SV_ACCOUNT_TYPE).
        /// </summary>
        public int StoredValueTransactionLimitId { get; set; }
    }
}
