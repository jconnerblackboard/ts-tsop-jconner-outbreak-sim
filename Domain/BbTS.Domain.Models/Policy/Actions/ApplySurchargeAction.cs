﻿namespace BbTS.Domain.Models.Policy.Actions
{
    /// <summary>
    /// An action that will apply a surcharge to the transaction.
    /// </summary>
    public class ApplySurchargeAction : PolicyAction
    {
        /// <summary>
        /// Surcharge rate.  (If 7.50% surcharge, Rate == 0.075)
        /// </summary>
        public decimal Rate { get; set; }
    }
}