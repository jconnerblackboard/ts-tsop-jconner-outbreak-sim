﻿namespace BbTS.Domain.Models.Policy.Actions
{
    /// <summary>
    /// An action that will result in the transaction being denied.
    /// </summary>
    public class DenyTransactionAction : PolicyAction
    {
    }
}