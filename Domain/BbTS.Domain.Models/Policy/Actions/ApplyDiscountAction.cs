﻿namespace BbTS.Domain.Models.Policy.Actions
{
    /// <summary>
    /// An action that will apply a discount to the transaction.
    /// </summary>
    public class ApplyDiscountAction : PolicyAction
    {
        /// <summary>
        /// Discount rate.  (If 7.50% discount, Rate == 0.075)
        /// </summary>
        public decimal Rate { get; set; }
    }
}