﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Policy.Actions
{
    /// <summary>
    /// An action that will use the specified account(s) when processing the transaction.
    /// </summary>
    public class UseStoredValueAccounts : PolicyAction
    {
        /// <summary>
        /// List of BbTS DB generated stored valud account type IDs (corresponds to SV_ACCOUNT_TYPE_ID in POLICY_ACTION_SV_ACCOUNT_TYPE and is in DEPLETION_ORDER).
        /// </summary>
        public List<int> StoredValueAccountTypeIds { get; set; }
    }
}