﻿// ReSharper disable InconsistentNaming
namespace BbTS.Domain.Models.Policy
{
    /// <summary>
    /// Defines a COMPOUND_POLICY_CONDITION row.
    /// </summary>
    public class TsCompound_Policy_Condition
    {
        /// <summary>
        /// 
        /// </summary>
        public int Policy_Condition_Id { get; set; }

        /// <summary>
        ///  1 - DISJUNCTIVE (Default) 2 - CONJUNCTIVE     If a compound_policy_condition is CONJUNCTIVE,  you should evaluate it by ANDing its children.  If its  DISJUNCTIVE, you should OR its children.  Since  you treat all children equally, the concept of group_number is unnecessary.
        /// </summary>
        public int Condition_List_Type { get; set; }
    }
}
