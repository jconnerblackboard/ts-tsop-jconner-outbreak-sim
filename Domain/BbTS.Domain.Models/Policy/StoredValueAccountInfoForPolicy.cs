﻿namespace BbTS.Domain.Models.Policy
{
    /// <summary>
    /// Holds stored value account information used in policy logic.
    /// </summary>
    public class StoredValueAccountInfoForPolicy
    {
        /// <summary>
        /// Stored value account type ID.
        /// </summary>
        public int AccountTypeId { get; set; }

        /// <summary>
        /// Tax exempt flag.
        /// </summary>
        public bool IsTaxExempt { get; set; }

        /// <summary>
        /// Stored value account ID.
        /// </summary>
        public int AccountId { get; set; }

        /// <summary>
        /// Depletion order.
        /// </summary>
        public int DepletionOrder { get; set; }

        /// <summary>
        /// Account balance.
        /// </summary>
        public decimal Balance { get; set; }

        /// <summary>
        /// Credit limit.
        /// </summary>
        public decimal CreditLimit { get; set; }

        /// <summary>
        /// Available balance.
        /// </summary>
        public decimal AvailableBalance { get; set; }

        /// <summary>
        /// Original available balance.
        /// </summary>
        public decimal OriginalAvailableBalance { get; set; }
    }
}
