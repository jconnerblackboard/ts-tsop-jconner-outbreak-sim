﻿// ReSharper disable InconsistentNaming
namespace BbTS.Domain.Models.Policy
{
    /// <summary>
    /// Defines a POLICY_ACTION_IN_POLICY_RULE row.
    /// </summary>
    public class TsPolicy_Action_In_Policy_Rule
    {
        /// <summary>
        /// Policy Action Association (from POLICY_ACTION table)
        /// </summary>
        public int Child_Policy_Action_Id { get; set; }

        /// <summary>
        /// Policy Set Association (from POLICY_SET table)
        /// </summary>
        public int Policy_Set_Id { get; set; }

        /// <summary>
        /// Order in which the action should take place
        /// </summary>
        public int Action_Order { get; set; }
    }
}
