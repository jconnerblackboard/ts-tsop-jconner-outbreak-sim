﻿// ReSharper disable InconsistentNaming
namespace BbTS.Domain.Models.Policy
{
    /// <summary>
    /// Defines a POLICY_SET row.
    /// </summary>
    public class TsPolicy_Set
    {
        /// <summary>
        /// Policy Set Identifier
        /// </summary>
        public int Policy_Set_Id { get; set; }

        /// <summary>
        /// Name of Policy Set
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// GUI Caption of Policy Set
        /// </summary>
        public string Caption { get; set; }

        /// <summary>
        /// Description of Policy Set
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Determines if this rule and all child rules should be (1) FirstMatching or (2) All Matching
        /// </summary>
        public int Policy_Decision_Strategy { get; set; }

        /// <summary>
        /// Comma Delimited List of Keywords for searchability/categorization, etc.
        /// </summary>
        public string Policy_Keywords { get; set; }

        /// <summary>
        /// Reusable Policy Container Association (from REUSABLE_POLICY_CONTAINER table)
        /// </summary>
        public int Reusable_Policy_Container_Id { get; set; }
    }
}
