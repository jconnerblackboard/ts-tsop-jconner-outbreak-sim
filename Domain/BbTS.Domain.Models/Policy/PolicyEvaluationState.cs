﻿using System;
using BbTS.Domain.Models.Pos;

namespace BbTS.Domain.Models.Policy
{
    /// <summary>
    /// Class to hold state information that will be used when making decisions in the policy tree.
    /// </summary>
    public class PolicyEvaluationState
    {
        /// <summary>
        /// The date/time that will be used in any date/time-related condition checks.
        /// </summary>
        public DateTimeOffset EvaluationDateTime { get; set; }

        /// <summary>
        /// Customer information that will be used for any customer-related condition checks.
        /// </summary>
        public CustomerInformationForPolicy CustomerInformationForPolicy { get; set; }

        /// <summary>
        /// Pos group that will be used for PosInGroup condition checks.
        /// </summary>
        public PosGroup PosGroup { get; set; }

        /// <summary>
        /// Tender ID that will be used for TenderUsed condition checks.
        /// </summary>
        public int TenderId { get; set; }
    }
}