﻿// ReSharper disable InconsistentNaming
namespace BbTS.Domain.Models.Policy
{
    /// <summary>
    /// Defines a POLICY_SET + POLICY_RULE row.
    /// </summary>
    public class TsPolicy_Rule : TsPolicy_Set
    {
        public int Enabled_Status { get; set; }

        public string Rule_Usage { get; set; }

        public char Mandatory { get; set; }

        public int Sequenced_Action { get; set; }

        public int Policy_Condition_Id { get; set; }

        public int Execution_Strategy_Type_Id { get; set; }
    }
}
