﻿using System.Collections.Generic;
using BbTS.Domain.Models.Definitions.Policy;

namespace BbTS.Domain.Models.Policy
{
    /// <summary>
    /// An abstract class that represents a set of policies that form a coherent set.
    /// The set of contained policies has a common decision strategy and a common set
    /// of policy roles.  Subclasses include PolicyGroup and PolicyRule.
    /// </summary>
    public abstract class PolicySet : PolicyObject
    {
        /// <summary>
        /// Parent PolicySets.
        /// </summary>
        public List<PolicySet> Parents { get; set; }

        /// <summary>
        /// Child PolicySets.
        /// </summary>
        public List<PolicySet> Children { get; set; }

        /// <summary>
        /// BbTS DB assigned identifier.
        /// </summary>
        public int PolicySetId { get; set; }

        /// <summary>
        /// The evaluation method used for policies contained in the PolicySet (Children).
        /// </summary>
        public PolicyDecisionStrategy DecisionStrategy { get; set; }
    }
}