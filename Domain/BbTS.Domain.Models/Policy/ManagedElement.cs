﻿namespace BbTS.Domain.Models.Policy
{
    //ManagedElement, ManagedSystemElement, LogicalElement, System, and AdminDomain
    //are defined in the CIM (Common Information Model) schema.
    //
    //We're stubbing them out here, but an arguement could be made for removing them
    //and making PolicyObject our top-level class.

    /// <summary>
    /// Managed element class (top-level abstract base class)
    /// </summary>
    public abstract class ManagedElement
    {
        /// <summary>
        /// A one-line description of this policy-related object.
        /// </summary>
        public string Caption { get; set; }

        /// <summary>
        /// A long description of this policy-related object.
        /// </summary>
        public string Description { get; set; }
    }

    #region Empty hierarchy classes

    /// <summary>
    /// ManagedSystemElement stub.
    /// </summary>
    public abstract class ManagedSystemElement : ManagedElement { }

    /// <summary>
    /// LogicalElement stub.
    /// </summary>
    public abstract class LogicalElement : ManagedSystemElement { }

    /// <summary>
    /// System stub.
    /// </summary>
    public abstract class System : LogicalElement { }

    /// <summary>
    /// AdminDomain stub.
    /// </summary>
    public abstract class AdminDomain : System { }

    #endregion
}