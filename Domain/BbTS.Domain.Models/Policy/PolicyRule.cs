﻿using System.Collections.Generic;
using BbTS.Domain.Models.Definitions.Policy;
using BbTS.Domain.Models.Policy.Actions;
using BbTS.Domain.Models.Policy.Conditions;

namespace BbTS.Domain.Models.Policy
{
    /// <summary>
    /// The central class for representing the "If Condition then Action" semantics associated with a policy rule.
    /// </summary>
    public class PolicyRule : PolicySet
    {
        /// <summary>
        /// Consider using BbTS.Core.Policy.PolicyBuilder.NewPolicyRule instead of this constructor whenever a new PolicyRule instance is needed.
        /// </summary>
        // ReSharper disable once EmptyConstructor
        public PolicyRule()
        {
            //Empty constructor to allow for XML comment instructions.
        }

        /// <summary>
        /// Holds a list of time period conditions that specify when this rule is valid.
        /// </summary>
        public List<TimePeriodCondition> Validity { get; set; }

        /// <summary>
        /// A compound list of conditions that determine if this rule evaluates to true.
        /// </summary>
        public CompoundPolicyCondition Conditions { get; set; }

        /// <summary>
        /// Actions to be performed if the condition for a policy rule evaluates to TRUE.
        /// </summary>
        public List<PolicyAction> Actions { get; set; }

        /// <summary>
        /// This property indicates whether a policy rule is currently enabled, from an administrative point of view.  Its purpose is to allow
        /// a policy administrator to enable or disable a policy rule without having to add it to, or remove it from, the policy repository.
        /// </summary>
        public EnabledStatus EnabledStatus { get; set; }

        /// <summary>
        /// This property is used to provide guidelines on how this policy should be used.
        /// </summary>
        public string RuleUsage { get; set; }

        /// <summary>
        /// A flag indicating that the evaluation of the PolicyConditions and execution of PolicyActions (if the condition list evaluates to TRUE) is required.
        /// </summary>
        public bool Mandatory { get; set; }

        /// <summary>
        /// This property gives a policy administrator a way of specifying how the ordering of the policy actions associated with this PolicyRule is to be interpreted.
        /// </summary>
        public SequencedActions SequencedActions { get; set; }
        
        /// <summary>
        /// Defines the execution strategy to be used upon the sequenced actions aggregated by this PolicyRule.
        /// </summary>
        public ExecutionStrategyType ExecutionStrategyType { get; set; }
    }
}