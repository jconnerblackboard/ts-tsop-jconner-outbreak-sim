﻿namespace BbTS.Domain.Models.Policy
{
    /// <summary>
    /// A container for either a set of related PolicyRules or a set of related PolicyGroups.
    /// </summary>
    public class PolicyGroup : PolicySet
    {
        /// <summary>
        /// Consider using BbTS.Core.Policy.PolicyBuilder.NewPolicyGroup instead of this constructor whenever a new PolicyGroup instance is needed.
        /// </summary>
        // ReSharper disable once EmptyConstructor
        public PolicyGroup()
        {
            //Empty constructor to allow for XML comment instructions.
        }
    }
}