﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// ReSharper disable InconsistentNaming
namespace BbTS.Domain.Models.Policy
{
    /// <summary>
    /// Defines a POLICY_CONDITION row.
    /// </summary>
    public class TsPolicy_Condition
    {
        public int Policy_Condition_Id { get; set; }

        public int Reusable_Policy_Container_Id { get; set; }

        public string Name { get; set; }

        public string Caption { get; set; }

        public string Description { get; set; }

        public int Policy_Condition_Type_Id { get; set; }

        public string Policy_Keywords { get; set; }
    }
}
