﻿// ReSharper disable InconsistentNaming
namespace BbTS.Domain.Models.Policy
{
    /// <summary>
    /// Defines a POLICY_COND_IN_POLICY_COND row.
    /// </summary>
    public class TsPolicy_Cond_In_Policy_Cond
    {
        /// <summary>
        /// 
        /// </summary>
        public int Parent_Policy_Condition_Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Child_Policy_Condition_Id { get; set; }

        /// <summary>
        /// Display_order lets the UI record and recover the ordering of elements for display
        /// </summary>
        public int Display_Order { get; set; }

        /// <summary>
        /// Indicates if the condition is negated (for example.  Customer NOT in ...or Customer IN
        /// </summary>
        public char Condition_Negated { get; set; }
    }
}
