﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// ReSharper disable InconsistentNaming
namespace BbTS.Domain.Models.Policy
{
    /// <summary>
    /// Defines a POLICY_ACTION row.
    /// </summary>
    public class TsPolicy_Action
    {
        public int Policy_Action_Id { get; set; }

        public int Reusable_Policy_Container_Id { get; set; }

        public string Name { get; set; }

        public string Caption { get; set; }

        public string Description { get; set; }

        public string Policy_Keywords { get; set; }

        public int Policy_Action_Type_Id { get; set; }
    }
}
