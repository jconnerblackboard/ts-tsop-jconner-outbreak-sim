﻿using System;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming :: TS Object Names

namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// This object represents a Transact Building object. 
    /// </summary>
    [Serializable]
    public class TsBuilding
    {
        /// <summary>
        /// The identity of this object
        /// </summary>
        [XmlAttribute]
        public int Building_Id { get; set; }
        /// <summary>
        /// The naame of the building
        /// </summary>
        [XmlAttribute]
        public string Name { get; set; }
        /// <summary>
        /// Globally unique identity for this record
        /// </summary>
        [XmlAttribute]
        public string DomainId { get; set; }
    }
}
