﻿using System;
using System.Collections.Generic;

namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// Rd log get response
    /// </summary>
    public class ReuseDelayLogGetResponse
    {
        /// <summary>
        /// Reuse delay logs
        /// </summary>
        public List<TsReuseDelayLog> ReuseDelays { get; set; }
    }
}
