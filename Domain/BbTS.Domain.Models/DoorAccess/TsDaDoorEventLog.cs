﻿using System;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming :: TS Object Names :: TS Object Names

namespace BbTS.Domain.Models.DoorAccess
{

    /// <summary>
    /// This object represents a Da_DoorEventLog object.  All Door Status Changes are logged to this table.
    /// </summary>
    [Serializable]
    public class TsDa_DoorEventLog
    {
        /// <summary>
        /// Door Event Log Identifier
        /// </summary>
        [XmlAttribute]
        public int Log_Id { get; set; }
        /// <summary>
        /// Computer Association (from HOSTNAMES table)
        /// </summary>
        [XmlAttribute]
        public int Computer_Id { get; set; }
        /// <summary>
        /// Door Association (from DOOR table)
        /// </summary>
        [XmlAttribute]
        public int Door_Id { get; set; }
        /// <summary>
        /// Transaction Sequence Number from the MasterController.   Increments for any transaction, include State Changes (Door Event Log).  Could be used to track the order that stuff happens at a door/mastercontroller
        /// </summary>
        [XmlAttribute]
        public int Mc_TranSeqNum { get; set; }
        /// <summary>
        /// T if StatusChange occured Online
        /// </summary>
        [XmlAttribute]
        public string Is_Online { get; set; }
        /// <summary>
        /// DateTime that this data was generated according to the Device
        /// </summary>
        [XmlAttribute]
        public DateTime ActualDateTime { get; set; }
        /// <summary>
        /// DateTime that this data was inserted into the table according to the DB's clock
        /// </summary>
        [XmlAttribute]
        public DateTime PostDateTime { get; set; }
        /// <summary>
        /// 0 - Controlled, 1 - Unlocked, 2 - Locked, 3 - Momentary Unlock
        /// </summary>
        [XmlAttribute]
        public int Door_ControlMode { get; set; }
        /// <summary>
        /// T means True
        /// </summary>
        [XmlAttribute]
        public string Is_Exit_Cycle { get; set; }
        /// <summary>
        /// T means True
        /// </summary>
        [XmlAttribute]
        public string Is_Unlocked { get; set; }
        /// <summary>
        /// T means True
        /// </summary>
        [XmlAttribute]
        public string Is_Open { get; set; }
        /// <summary>
        /// T means True
        /// </summary>
        [XmlAttribute]
        public string Is_Held { get; set; }
        /// <summary>
        /// T means True
        /// </summary>
        [XmlAttribute]
        public string Is_Forced { get; set; }
        /// <summary>
        /// T means True
        /// </summary>
        [XmlAttribute]
        public string Is_Tampered { get; set; }
        /// <summary>
        /// T means True
        /// </summary>
        [XmlAttribute]
        public string Requires_Pin { get; set; }
        /// <summary>
        /// Door Controller Communication Lost (T means True)
        /// </summary>
        [XmlAttribute]
        public string DoorCtrl_Comm_Lost { get; set; }
        /// <summary>
        /// TB1 (Proximity Reader) Auxillary Cardswipe Tamper - T means True
        /// </summary>
        [XmlAttribute]
        public string Tb_1_Aux_Cs_Tamper { get; set; }
        /// <summary>
        /// TB2 (Proximity Reader) Auxillary Cardswipe Tamper - T means True
        /// </summary>
        [XmlAttribute]
        public string Tb_2_Aux_Cs_Tamper { get; set; }
        /// <summary>
        /// Serial Port 1 Cardswipe Communication Lost - T means True
        /// </summary>
        [XmlAttribute]
        public string Serial_1_Cs_Comm_Lost { get; set; }
        /// <summary>
        /// Serial Port 2 Cardswipe Communication Lost - T means True
        /// </summary>
        [XmlAttribute]
        public string Serial_2_Cs_Comm_Lost { get; set; }
        /// <summary>
        /// Door Controller AC Power Failed - T means True
        /// </summary>
        [XmlAttribute]
        public string DoorCtrl_Ac_Power_Fail { get; set; }
        /// <summary>
        /// Door Controller Battery Low - T means True
        /// </summary>
        [XmlAttribute]
        public string DoorCtrl_Battery_Low { get; set; }
        /// <summary>
        /// (relates to Door Control State) 0 - As Scheduled, 1 - Indefinate Override, 2 - Span Override(Override until Next Time Block)
        /// </summary>
        [XmlAttribute]
        public int Override_Status { get; set; }
        /// <summary>
        /// FK to master controler table
        /// </summary>
        [XmlAttribute]
        public int MasterController_Id { get; set; }
    }
}
