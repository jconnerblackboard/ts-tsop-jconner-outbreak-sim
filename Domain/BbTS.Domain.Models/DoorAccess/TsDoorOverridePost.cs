﻿using BbTS.Domain.Models.Definitions.Door;

namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// This object represents a record in the Transact system for a door override
    /// </summary>
    public class TsDoorOverridePost : TsDoorOverrideBase
    {
        /// <summary>
        /// Id
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// Start date
        /// </summary>
        public double? StartDate { get; set; }

        /// <summary>
        /// End date
        /// </summary>
        public double? StopDate { get; set; }

        /// <summary>
        /// Override type
        /// </summary>
        public int OverrideTypeId
        {
            get { return (int)OverrideType; }
            set { OverrideType = (DoorOverrideType)OverrideTypeId; }
        }
    }
}
