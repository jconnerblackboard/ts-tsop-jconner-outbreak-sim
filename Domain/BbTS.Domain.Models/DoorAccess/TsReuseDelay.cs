﻿using System;

namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// This object represents a record in the Transact system for a reuse delay
    /// </summary>
    public class TsReuseDelay
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Time
        /// </summary>
        public TimeSpan Time { get; set; }
    }
}
