﻿using System;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming :: TS Object Names

namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// This object represents a Transact MasterController object. 
    /// </summary>
    [Serializable]
    public class TsMasterController
    {
        ///<summary>
        ///
        ///</summary>
        [XmlAttribute]
        public int MasterController_Id { get; set; }
        ///<summary>
        ///
        ///</summary>
        [XmlAttribute]
        public string Name { get; set; }
        ///<summary>
        ///
        ///</summary>
        [XmlAttribute]
        public string Abbreviated_Name { get; set; }
        ///<summary>
        ///
        ///</summary>
        [XmlAttribute]
        public string Multiplexor_Enabled { get; set; }
        ///<summary>
        ///
        ///</summary>
        [XmlAttribute]
        public string Pinpad_Enabled { get; set; }
        ///<summary>
        ///
        ///</summary>
        [XmlAttribute]
        public int Door_Access_Download_Sched_Id { get; set; }
    }
}
