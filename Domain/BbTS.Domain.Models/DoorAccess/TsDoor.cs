﻿using System;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming :: TS Object Names

namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// This object represents a Transact Door object.
    /// </summary>
    [Serializable]
    public class TsDoor
    {
        /// <summary>
        /// Door Identifier
        /// </summary>
        [XmlAttribute]
        public int Door_Id { get; set; }
        /// <summary>
        /// Master Controller that this Door's Door Controller is connected to
        /// </summary>
        [XmlAttribute]
        public int MasterController_Id { get; set; }
        /// <summary>
        /// Address 0..7.  This tells the Master Controller which door it is talking to
        /// </summary>
        [XmlAttribute]
        public int Address { get; set; }
        /// <summary>
        /// Merchant Association (from MERCHANT table)
        /// </summary>
        [XmlAttribute]
        public int Merchant_Id { get; set; }
        /// <summary>
        /// Identifier unique by Merchant.  This field is visible and editable by the user.
        /// </summary>
        [XmlAttribute]
        public string Door_Identifier { get; set; }
        /// <summary>
        /// Text description of Door
        /// </summary>
        [XmlAttribute]
        public string Description { get; set; }
        /// <summary>
        /// Building Association (from BUILDING table)
        /// </summary>
        [XmlAttribute]
        public int Building_Id { get; set; }
        /// <summary>
        /// Building Area Association (from AREA table)
        /// </summary>
        [XmlAttribute]
        public int Area_Id { get; set; }
        /// <summary>
        /// The Door Group that this door belongs to (NULL mean none)
        /// </summary>
        [XmlAttribute]
        public int Door_Group_Id { get; set; }
        /// <summary>
        /// T means use the Settings from DA_DOOR_DEFAULT
        /// </summary>
        [XmlAttribute]
        public string Use_Door_Default { get; set; }
        /// <summary>
        /// 1..255 seconds
        /// </summary>
        [XmlAttribute]
        public int Door_Unlock_Time_Seconds { get; set; }
        /// <summary>
        /// 1..255 seconds
        /// </summary>
        [XmlAttribute]
        public int Min_Siren_Time_Seconds { get; set; }
        /// <summary>
        /// A held alarm is when the door is open longer than HELD_ALLOW_DELAY_TIME_SECONDS
        /// 0 - Disabled.No alarm is sent to the DA Host and no siren sounds.
        /// 1 - Silent.A alarm is sent to the DA Host after the held alarm delay time has expired.
        /// 2 - Siren.Siren sounds after the held alarm delay time has expired.
        /// </summary>
        [XmlAttribute]
        public int Held_Alarm_Type { get; set; }
        /// <summary>
        /// 1..255 seconds
        /// </summary>
        [XmlAttribute]
        public int Held_Alarm_Delay_Time_Seconds { get; set; }
        /// <summary>
        /// A force alarm is defined as a door detected as open (door contacts are separated)
        /// and no valid card swipe was received.
        /// 0 - Disabled.No forced alarm is sent to the DA Host and no siren sounds.
        /// 1 - Silent.A forced alarm is sent to the DA Host after the forced alarm delay time has expired.
        /// 2 - Siren.Siren sounds after the forced alarm delay time has expired.
        /// </summary>
        [XmlAttribute]
        public int Force_Alarm_Type { get; set; }
        /// <summary>
        /// 1..255 seconds
        /// </summary>
        [XmlAttribute]
        public int Force_Alarm_Delay_Time_Seconds { get; set; }
        /// <summary>
        /// T means use the SCHEDULE_ID from DA_DOOR_DEFAULT
        /// </summary>
        [XmlAttribute]
        public string Use_Door_Default_Schedule { get; set; }
        /// <summary>
        /// If USE_DEFAULT_SCHEDULE != T then this will be the Door State Schedule
        /// </summary>
        [XmlAttribute]
        public int Schedule_Id { get; set; }
        /// <summary>
        /// OFFLINE_OPERATION_TYPE
        /// 0 - Site Code Check Only
        /// 1 - Online Emulation
        /// 2 - No Access
        /// </summary>
        [XmlAttribute]
        public int Offline_Operation_Type { get; set; }
        /// <summary>
        /// Instructions that the Door Access Monitor is shown for any alarm at this Door
        /// </summary>
        [XmlAttribute]
        public string Alarm_Instructions { get; set; }
        /// <summary>
        /// Globally unique identity for this record
        /// </summary>
        [XmlAttribute]
        public string DomainId { get; set; }
    }
}
