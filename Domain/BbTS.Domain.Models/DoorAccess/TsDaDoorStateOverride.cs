﻿using System;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming :: TS Object Names

namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// This object represents a Transact Da_DoorStateOverride object. Maintains a list of Requested Door State Overrides
    /// </summary>
    [Serializable]
    public class TsDa_DoorStateOverride
    {
        /// <summary>
        /// Door Access Door State Override Identifier
        /// </summary>
        [XmlAttribute]
        public int Da_DoorStateOverride_Id { get; set; }
        /// <summary>
        /// The User that issued the DoorState Override in DAMonitor
        /// </summary>
        [XmlAttribute]
        public string UserName { get; set; }
        /// <summary>
        /// Door Association (from DOOR table)
        /// </summary>
        [XmlAttribute]
        public int Door_Id { get; set; }
        /// <summary>
        /// The COMPUTER_ID that the Request was sent to (the BbHost)
        /// </summary>
        [XmlAttribute]
        public int Computer_Id { get; set; }
        /// <summary>
        /// DoorState OVERRIDE_TYPE
        /// 0 - Reset To Schedule
        /// 1 - Momentary Unlock
        /// 2 - Query Status
        /// 3 - New DOOR_STATE Indefinate
        /// 4 - New DOOR_STATE for Current Period
        /// </summary>
        [XmlAttribute]
        public int Override_Type { get; set; }
        /// <summary>
        /// OVERRIDE_DOOR_STATE (Required field if Override_Type is New DOOR_STATE)
        /// 0 - Controlled
        /// 1 - Unlocked
        /// 2 - Locked
        /// 3 - Momentary Unlock
        /// </summary>
        [XmlAttribute]
        public int Override_Door_State { get; set; }
        /// <summary>
        /// OVERRIDE_STATUS
        /// 0 - Sent To Host
        /// 1 - Accepted by Host
        /// 2 - Sent To Controller
        /// x - Pending because of...
        /// x - Denied because...
        /// </summary>
        [XmlAttribute]
        public int Override_Status { get; set; }
        /// <summary>
        /// RETRY_SECS
        /// For a DA DoorChange the DA Monitor.The Number of seconds that the host will try to send the override request to the MC before giving up
        /// </summary>
        [XmlAttribute]
        public int Retry_Secs { get; set; }
        /// <summary>
        /// The DateTime (according to the Database Clock), that the Override request was sent to the Host.  Since this record actually needs to be INSERTED and COMMITED before it is really sent to the host, the datetime will probably be off a few milliseconds or so.
        /// </summary>
        [XmlAttribute]
        public DateTime ToHost_Dt { get; set; }
        /// <summary>
        /// The DateTime (according to the Database Clock) that the Host sent the request to the Controller.  This field is updated by a Stored Proc that is called by the Host.
        /// </summary>
        [XmlAttribute]
        public DateTime ToController_Dt { get; set; }
        /// <summary>
        /// Door Event Log Association (from DA_DOOREVENTLOG table)
        /// </summary>
        [XmlAttribute]
        public int Log_Id { get; set; }
    }
}
