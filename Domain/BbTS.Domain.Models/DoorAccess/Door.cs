﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.DoorAccess
{
    public class Door
    {
        public int Id { get; set; }
        public int Type { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int BuidlingId { get; set; }
        public int MerchantId { get; set; }

        protected bool Equals(Door other)
        {
            return 
                Id == other.Id &&
                Type == other.Type &&
                string.Equals(Name, other.Name) &&
                string.Equals(Description, other.Description) &&
                BuidlingId == other.BuidlingId &&
                MerchantId == other.MerchantId;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = Id;
                hashCode = (hashCode * 397) ^ Type;
                hashCode = (hashCode * 397) ^ (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Description != null ? Description.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ BuidlingId;
                hashCode = (hashCode * 397) ^ MerchantId;
                return hashCode;
            }
        }

        public override bool Equals(object other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            if (other.GetType() != this.GetType()) return false;
            return Equals((Door)other);
        }
    }
}
