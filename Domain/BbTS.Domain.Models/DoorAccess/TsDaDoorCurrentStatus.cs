﻿using System;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming :: TS Object Names

namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// This object represents a Transact Da_DoorCurrentStatus object. Door Access Door Current Status: A quick status table for the Door Access Monitor.  Note that this data is a summary table of the most recent data in DA_DOOREVENTLOG.
    /// </summary>
    [Serializable]
    public class TsDa_DoorCurrentStatus
    {
        /// <summary>
        /// Door Association (from DOOR table)
        /// </summary>
        [XmlAttribute]
        public int Door_Id { get; set; }
        /// <summary>
        /// Transaction Sequence Number from the MasterController. 
        /// </summary>
        [XmlAttribute]
        public int Mc_TranSeqNum { get; set; }
        /// <summary>
        /// T means the MasterController was Online when this info was recorded on the MasterController: Note:  This does NOT mean that the MasterController (and its Doors) is Currently online.
        /// </summary>
        [XmlAttribute]
        public string Is_Online { get; set; }
        /// <summary>
        /// 0 - Controlled
        /// 1 - Unlocked
        /// 2 - Locked
        /// 3 - Momentary Unlock
        /// </summary>
        [XmlAttribute]
        public int Door_ControlMode { get; set; }
        /// <summary>
        /// T means True
        /// </summary>
        [XmlAttribute]
        public string Is_ExitCycle { get; set; }
        /// <summary>
        /// T means True
        /// </summary>
        [XmlAttribute]
        public string Is_Unlocked { get; set; }
        /// <summary>
        /// T means True
        /// </summary>
        [XmlAttribute]
        public string Is_Open { get; set; }
        /// <summary>
        /// T means True
        /// </summary>
        [XmlAttribute]
        public string Requires_Pin { get; set; }
        /// <summary>
        /// (relates to Door Control State)
        /// 0 - As Scheduled
        /// 1 - Indefinate Override
        /// 2 - Span Override(Override until Next Time Block)
        /// </summary>
        [XmlAttribute]
        public int Override_Status { get; set; }
        /// <summary>
        /// The LastUpdateDT of this record
        /// </summary>
        [XmlAttribute]
        public DateTime LastUpdateDt { get; set; }
        /// <summary>
        /// Computer Association (from HOSTNAMES table)
        /// </summary>
        [XmlAttribute]
        public int Computer_Id { get; set; }
        /// <summary>
        /// Master Controller Association (from MASTERCONTROLLER table)
        /// </summary>
        [XmlAttribute]
        public int MasterController_Id { get; set; }
        /// <summary>
        /// If the DoorState was Overridden, this indicates with DoorStateOverride caused it
        /// </summary>
        [XmlAttribute]
        public int Da_DoorStateOverride_Id { get; set; }
    }
}
