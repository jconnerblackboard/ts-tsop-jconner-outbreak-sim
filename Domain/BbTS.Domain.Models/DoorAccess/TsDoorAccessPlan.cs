﻿
namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// This object represents a record in the Transact system for a door access plan
    /// </summary>
    public class TsDoorAccessPlan
    {
        /// <summary>
        /// The identity of the access plan
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The name of the access plan
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Whether this plan is active or not
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// The access plan start date
        /// </summary>
        public string StartDate { get; set; }

        /// <summary>
        /// The access plan end date
        /// </summary>
        public string StopDate { get; set; }

        /// <summary>
        /// If this is a plan that contains Persona doors, what authorization type is to be used 
        /// </summary>
        public int DsrAuthorizationTypeId { get; set; }
    }
}
