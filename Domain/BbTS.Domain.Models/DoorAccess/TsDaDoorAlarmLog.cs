﻿using System;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming :: TS Object Names

namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// This object represents a Transact Da_DoorAlarmLog object. Door Access Alarm Log.  Stores a log of all the Alarms that occurred
    /// </summary>
    [Serializable]
    public class TsDa_DoorAlarmLog
    {
        /// <summary>
        /// Door Access Door Alarm Log Identifier
        /// </summary>
        [XmlAttribute]
        public int Da_DoorAlarmLog_Id { get; set; }
        /// <summary>
        /// Computer Association (from HOSTNAMES table)
        /// </summary>
        [XmlAttribute]
        public int Computer_Id { get; set; }
        /// <summary>
        /// Door Association (from DOOR table)
        /// </summary>
        [XmlAttribute]
        public int Door_Id { get; set; }
        /// <summary>
        /// DOORALARMTYPE
        /// 00 - Held Open
        /// 01 - Forced Open
        /// 02 - Tampered
        /// 03 - dc_comm_lost
        /// 04 - tb_1_aux_cs_tamper
        /// 05 - tb_2_aux_cs_tamper
        /// 06 - serial_1_cs_comm_lost
        /// 07 - serial_2_cs_comm_lost
        /// 08 - dc_ac_power_fail
        /// 09 - dc_battery_low
        /// </summary>
        [XmlAttribute]
        public int DoorAlarmType { get; set; }
        /// <summary>
        /// The Master Controller Transaction Sequence Number as of the Alarm Start
        /// </summary>
        [XmlAttribute]
        public int Start_McTranSeqNum { get; set; }
        /// <summary>
        /// The Master Controller's DateTime as of the Alarm Start
        /// </summary>
        [XmlAttribute]
        public DateTime Start_ActualDateTime { get; set; }
        /// <summary>
        /// 'T' means the MasterController was Online when this Alarm started on the MasterController
        /// </summary>
        [XmlAttribute]
        public string Start_IsOnline { get; set; }
        /// <summary>
        /// The Database's DateTime as of when it INSERTED the Alarm record
        /// </summary>
        [XmlAttribute]
        public DateTime Start_PostDateTime { get; set; }
        /// <summary>
        /// Indicator as to whether the alarm log entry is active (T/F)
        /// </summary>
        [XmlAttribute]
        public string Is_Active { get; set; }
        /// <summary>
        /// The Master Controller Transaction Sequence Number as of the Alarm Stop
        /// </summary>
        [XmlAttribute]
        public int Stop_McTranSeqNum { get; set; }
        /// <summary>
        /// The Master Controller's DateTime as of the Alarm Stop
        /// </summary>
        [XmlAttribute]
        public DateTime Stop_ActualDateTime { get; set; }
        /// <summary>
        /// 'T' means the MasterController was Online when this Alarm stopped on the MasterController
        /// </summary>
        [XmlAttribute]
        public string Stop_IsOnline { get; set; }
        /// <summary>
        /// The Database's DateTime as of when it Updated the alarm record as stopped
        /// </summary>
        [XmlAttribute]
        public DateTime Stop_PostDateTime { get; set; }
        /// <summary>
        /// Master Controller Association (from MASTERCONTROLLER table)
        /// </summary>
        [XmlAttribute]
        public int MasterController_Id { get; set; }
        /// <summary>
        /// T means this alarm record was Acknowledged
        /// </summary>
        [XmlAttribute]
        public string Is_Acked { get; set; }
        /// <summary>
        /// The associated Ack Note
        /// </summary>
        [XmlAttribute]
        public int Da_DoorAlarm_AckInfo_Id { get; set; }
        /// <summary>
        /// Globally unique identity for this record.
        /// </summary>
        [XmlAttribute]
        public string DomainId { get; set; }
    }
}
