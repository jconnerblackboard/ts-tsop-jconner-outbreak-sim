﻿using System;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.DoorAccess
{
  /// <summary>
  /// This object represents a Transact customer_da_request_history object. Customer Owned record of DA Transactions (including Denied Transactions).  Useful for Customer Tracking and for Active Customer Count
  /// NOTE: Notice all the NULLable columns.  Allows these things to be deleted while still providing information for anything else that still remains.
  /// </summary>
  [Serializable]
  public class TsCustomer_Da_Request_History
  {
    /// <summary>
    /// Customer Door Access Request History Identifier
    /// </summary>
    [XmlAttribute]
    public int Customer_Da_Request_History_Id { get; set; }
    /// <summary>
    /// Customer Association (from Customer table)
    /// </summary>
    [XmlAttribute]
    public int Cust_Id { get; set; }
    /// <summary>
    /// Transaction Sequence Number from the MasterController.   Increments for any transaction, include State Changes (Door Event Log).  Could be used to track the order that stuff happens at a door/mastercontroller
    /// </summary>
    [XmlAttribute]
    public int Mc_TranSeqNum { get; set; }
    /// <summary>
    /// DateTime that this data was generated according to the Device
    /// </summary>
    [XmlAttribute]
    public DateTime ActualDateTime { get; set; }
    /// <summary>
    /// DateTime that this data was inserted into the table according to the DB's clock
    /// </summary>
    [XmlAttribute]
    public DateTime PostDateTime { get; set; }
    /// <summary>
    /// 0 if DA Request was accepted, otherwise this is the errorcode for the Denied Request (hint see enve.dll)
    /// </summary>
    [XmlAttribute]
    public int ResultCode { get; set; }
    /// <summary>
    /// 0 - CardNum  1 - future feature such as thumbprint  NOTE: Makes sense to do this now, because this separation will help to minimze the impact of adding future ID'n methods.  (but at little cost if we never add future ID'n methods)
    /// </summary>
    [XmlAttribute]
    public int Cust_Identification_Type { get; set; }
    /// <summary>
    /// Card number that was used for this DA Request
    /// </summary>
    [XmlAttribute]
    public string CardNum { get; set; }
    /// <summary>
    /// The issue number of the card at the time of the transaction
    /// </summary>
    [XmlAttribute]
    public string Issue_Number { get; set; }
    /// <summary>
    /// Indicates if an issue number was captured during the transaction (T/F)
    /// </summary>
    [XmlAttribute]
    public string Issue_Number_Captured { get; set; }
    /// <summary>
    /// Door Association (from DOOR table)
    /// </summary>
    [XmlAttribute]
    public int Door_Id { get; set; }
    /// <summary>
    /// Merchant Association (from MERCHANT table)
    /// </summary>
    [XmlAttribute]
    public int Merchant_Id { get; set; }
    /// <summary>
    /// Building Association (from BUILDING table)
    /// </summary>
    [XmlAttribute]
    public int Building_Id { get; set; }
    /// <summary>
    /// Area Association (from AREA table)
    /// </summary>
    [XmlAttribute]
    public int Area_Id { get; set; }
    /// <summary>
    /// The group identifier for this door
    /// </summary>
    [XmlAttribute]
    public int Door_Group_Id { get; set; }
  }
}
