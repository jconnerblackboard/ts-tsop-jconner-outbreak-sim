﻿using System.Collections.Generic;
using BbTS.Domain.Models.Definitions.Door;

namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// Container class for a request to change a door state.
    /// </summary>
    public class DoorAaStateChangeRequest
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The numerical identifier for the door.
        /// </summary>
        public List<int> DoorIds { get; set; }

        /// <summary>
        /// The new state to change the door to.
        /// </summary>
        public DoorAaStateChangeStateOverrideValues StateOverrideValue { get; set; }
    }
}
