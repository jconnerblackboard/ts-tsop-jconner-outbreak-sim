﻿using System;

namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// This object represents a record in the Transact system for a door override
    /// </summary>
    public class TsDoorOverride : TsDoorOverrideBase
    {
        /// <summary>
        /// The identity of the customer door override
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Door name
        /// </summary>
        public string DoorName { get; set; }

        /// <summary>
        /// Door description
        /// </summary>
        public string DoorDescription { get; set; }

        /// <summary>
        /// Door group name
        /// </summary>
        public string DoorGroupName { get; set; }

        /// <summary>
        /// Start date
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// End date
        /// </summary>
        public DateTime? StopDate { get; set; }

        /// <summary>
        /// Merchant Id
        /// </summary>
        public Int16 MerchantId { get; set; }

        /// <summary>
        /// Merchant name
        /// </summary>
        public string MerchantName { get; set; }

        /// <summary>
        /// Building name
        /// </summary>
        public string BuildingName { get; set; }

        /// <summary>
        /// Area name
        /// </summary>
        public string AreaName { get; set; }

        /// <summary>
        /// Schedule name
        /// </summary>
        public string ScheduleName { get; set; }

        /// <summary>
        /// Reuse delay name
        /// </summary>
        public string ReuseDelayName { get; set; }
    }
}
