﻿using System;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming :: TS Object Names

namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// This object represents a Transact da_deniedtransaction object. Door Access Denied Transactions
    /// </summary>
    [Serializable]
    public class TsDa_DeniedTransaction
    {
        /// <summary>
        /// Door Access Denied Transaction Identifier
        /// </summary>
        [XmlAttribute]
        public int DeniedTran_Id { get; set; }
        /// <summary>
        /// Transaction Sequence Number from the MasterController.   Increments for any transaction, include State Changes (Door Event Log).  Could be used to track the order that stuff happens at a door/mastercontroller
        /// </summary>
        [XmlAttribute]
        public int Mc_TranSeqNum { get; set; }
        /// <summary>
        /// T means Online
        /// </summary>
        [XmlAttribute]
        public string IsOnline { get; set; }
        /// <summary>
        /// 0 - Accepted Online (by Database Procs)
        /// 1 - Accepted Offline(at Device Only)
        /// </summary>
        [XmlAttribute]
        public int Validation_Type { get; set; }
        /// <summary>
        /// DateTime that this data was generated according to the Device
        /// </summary>
        [XmlAttribute]
        public DateTime ActualDateTime { get; set; }
        /// <summary>
        /// DateTime that this data was inserted into the table according to the DB's clock
        /// </summary>
        [XmlAttribute]
        public DateTime PostDateTime { get; set; }
        /// <summary>
        /// Tender Type Association (from DA_TENDER_TYPE table)
        /// </summary>
        [XmlAttribute]
        public int Tender_Type_Id { get; set; }
        /// <summary>
        /// Master Controller Association (from MASTERCONTROLLER table)
        /// </summary>
        [XmlAttribute]
        public int MasterController_Id { get; set; }
        /// <summary>
        /// Door Association (from DOOR table)
        /// </summary>
        [XmlAttribute]
        public int Door_Id { get; set; }
        /// <summary>
        /// 0 - Cardnum (look up cardnum from DA_DENIEDTRANSACTION_CARD)
        /// 1 - future feature such as thumbprint
        /// NOTE: Makes sense to do this now, because this separation will help to minimize the impact of adding future ID'n methods.  (but at little cost if we never add future ID'n methods)
        /// </summary>
        [XmlAttribute]
        public int Cust_Identification_Type { get; set; }
        /// <summary>
        /// Customer Association (from Customer table)
        /// </summary>
        [XmlAttribute]
        public int Cust_Id { get; set; }
        /// <summary>
        /// Host Computer that processed this
        /// </summary>
        [XmlAttribute]
        public int HostComp_Id { get; set; }
        /// <summary>
        /// Denied Code
        /// </summary>
        [XmlAttribute]
        public int DeniedCode { get; set; }
    }
}
