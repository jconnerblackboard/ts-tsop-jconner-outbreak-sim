﻿using BbTS.Domain.Models.Definitions.Door;

namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// This object represents a record in the Transact system for a door override
    /// </summary>
    public class TsDoorOverrideBase
    {
        /// <summary>
        /// Door Id
        /// </summary>
        public int DoorId { get; set; }

        /// <summary>
        /// Override type
        /// </summary>
        public DoorOverrideType OverrideType { get; set; }

        /// <summary>
        /// Permission schedule Id
        /// </summary>
        public int ScheduleId { get; set; }

        /// <summary>
        /// Reuse delay
        /// </summary>
        public int? ReuseDelayId { get; set; }

        /// <summary>
        /// Reuse delay object Id
        /// </summary>
        public int? ReuseDelayObjectId { get; set; }

        /// <summary>
        /// Reuse delay active
        /// </summary>
        public bool ReuseDelayActive { get; set; }
    }
}
