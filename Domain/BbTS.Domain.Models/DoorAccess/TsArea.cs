﻿using System;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming :: TS Object Names

namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// This object represents a Transact area object. 
    /// </summary>
    [Serializable]
    public class TsArea
    {
        /// <summary>
        /// The identity of the table
        /// </summary>
        [XmlAttribute]
        public int Area_Id { get; set; }
        /// <summary>
        /// The FK to the building table
        /// </summary>
        [XmlAttribute]
        public int Building_Id { get; set; }
        /// <summary>
        /// The name of the area
        /// </summary>
        [XmlAttribute]
        public string Name { get; set; }
        /// <summary>
        /// Globally unique identity for this record
        /// </summary>
        [XmlAttribute]
        public string DomainId { get; set; }
    }
}
