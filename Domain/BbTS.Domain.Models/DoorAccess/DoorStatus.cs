﻿namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// Container class for the status of a door.
    /// </summary>
    public class DoorStatus
    {
        /// <summary>
        /// Get/Set the Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The unique identifier of the master controlller that controls the door to which this is attached.
        /// </summary>
        public int MasterControllerId { get; set; }

        /// <summary>
        /// The numerical identifier of the door to which this status is attached.
        /// </summary>
        public int DoorId { get; set; }

        /// <summary>
        /// The address of the door on the master controller.
        /// </summary>
        public short Address { get; set; }

        /// <summary>
        /// Get/Set the IsExitCycle value
        /// </summary>
        public bool? IsExitCycle { get; set; }

        /// <summary>
        /// Get/Set the IsUnlocked value
        /// </summary>
        public bool? IsUnlocked { get; set; }

        /// <summary>
        /// Get/Set the IsOpen value
        /// </summary>
        public bool? IsOpen { get; set; }

        /// <summary>
        /// Get/Set the IsHeld value
        /// </summary>
        public bool? IsHeld { get; set; }

        /// <summary>
        /// Get/Set the IsForced value
        /// </summary>
        public bool? IsForced { get; set; }

        /// <summary>
        /// Get/Set the IsTampered value
        /// </summary>
        public bool? IsTampered { get; set; }

        /// <summary>
        /// Get/Set the Override Mode
        /// </summary>
        public string OverrideMode { get; set; }

        /// <summary>
        /// Get/Set the Control Mode
        /// </summary>
        public string ControlMode { get; set; }
    }
}
