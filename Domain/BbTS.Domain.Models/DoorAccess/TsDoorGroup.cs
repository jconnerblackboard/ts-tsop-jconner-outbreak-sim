﻿
namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// This object represents a record in the Transact system for a door group
    /// </summary>
    public class TsDoorGroup
    {
        /// <summary>
        /// The identity of the door group
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// The name of the door group
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Count of doors in this group
        /// </summary>
        public int DoorCount { get; set; }
    }
}
