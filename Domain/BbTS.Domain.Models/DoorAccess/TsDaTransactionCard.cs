﻿using System;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming :: TS Object Names

namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// This object represents a Transact da_transaction_card object. DA_TRANSACTION is a is 1 to 0 or 1 relation to DA_TRANSACTION_CARD.  DA_TRANSACTION_CARD is mandatory if a CARD was used to identify the customer (CUST_IDENTIFICATION_TYPE was 0)
    /// </summary>
    [Serializable]
    public class TsDa_Transaction_Card
    {
        /// <summary>
        /// Door Access Transaction Association (from DA_TRANSACTION table)
        /// </summary>
        [XmlAttribute]
        public int Tran_Id { get; set; }
        /// <summary>
        /// Card number Association (from CARD table)
        /// </summary>
        [XmlAttribute]
        public string CardNum { get; set; }
        /// <summary>
        /// The issue number of the card captured during the transaction, if applicable
        /// </summary>
        [XmlAttribute]
        public string Issue_Number { get; set; }
        /// <summary>
        /// Indicates if an issue number was captured during the transaction (T/F)
        /// </summary>
        [XmlAttribute]
        public string Issue_Number_Captured { get; set; }
        /// <summary>
        /// T means Swiped vs. manually entered
        /// </summary>
        [XmlAttribute]
        public string Swiped { get; set; }
        /// <summary>
        /// T means a PIN was entered
        /// </summary>
        [XmlAttribute]
        public string Pin_Entered { get; set; }
    }
}
