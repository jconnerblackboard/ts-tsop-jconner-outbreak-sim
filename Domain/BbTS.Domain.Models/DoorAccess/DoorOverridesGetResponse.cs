﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// The Door Overrides List Response.
    /// </summary>
    public class DoorOverridesGetResponse
    {
        /// <summary>
        /// Gets or sets the Door Overrides.
        /// </summary>
        public List<TsDoorOverride> DoorOverrides { get; set; }
    }
}
