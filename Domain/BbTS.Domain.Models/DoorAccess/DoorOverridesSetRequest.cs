﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// The Door Overrides List Response.
    /// </summary>
    [XmlRoot("DoorOverridesSet")]
    public class DoorOverridesSetRequest
    {
        /// <summary>
        /// Request Id
        /// </summary>
        [XmlIgnore]
        public Guid? RequestId { get; set; }

        /// <summary>
        /// Gets or sets the Door Overrides.
        /// </summary>
        [XmlArray, XmlArrayItem("DoorOverride")]
        public List<TsDoorOverridePost> DoorOverrides { get; set; }
    }
}
