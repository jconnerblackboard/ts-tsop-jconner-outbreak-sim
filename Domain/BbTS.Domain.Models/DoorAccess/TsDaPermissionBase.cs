﻿
namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// This object represents a base record in the Transact system for a Da_Permission_Schedule
    /// </summary>
    public class TsDaPermissionBase
    {
        /// <summary>
        /// Schedule Id
        /// </summary>
        public int ScheduleId { get; set; }

        /// <summary>
        /// Schedule Name
        /// </summary>
        public string ScheduleName { get; set; }
    }
}
