﻿using System;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming :: TS Object Names

namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// This object represents a Transact da_dooralarm_ackinfo object. Door Access Alarm Acknowledgement Information
    /// </summary>
    [Serializable]
    public class TsDa_DoorAlarm_AckInfo
    {
        /// <summary>
        /// Door Access Alarm Acknowledgement Information Identifier
        /// </summary>
        [XmlAttribute]
        public int Da_DoorAlarm_AckInfo_Id { get; set; }
        /// <summary>
        /// Username of person acknowledging alarm
        /// </summary>
        [XmlAttribute]
        public string UserName { get; set; }
        /// <summary>
        /// DateTime that the user acknowledged the associated alarm record(s)
        /// </summary>
        [XmlAttribute]
        public DateTime Ack_DateTime { get; set; }
        /// <summary>
        /// T means the user choose to automatically acknowledge all previous open alarms
        /// </summary>
        [XmlAttribute]
        public string Ack_AllPrev { get; set; }
        /// <summary>
        /// Text of the Acknowledgement.
        /// </summary>
        [XmlAttribute]
        public string Ack_Note { get; set; }
        /// <summary>
        /// Globally unique identity for this record.
        /// </summary>
        [XmlAttribute]
        public string DomainId { get; set; }
    }
}
