﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// The Reuse Delays List Response.
    /// </summary>
    public class ReuseDelayGetResponse
    {
        /// <summary>
        /// Gets or sets the Reuse Delays.
        /// </summary>
        public List<TsReuseDelay> ReuseDelays { get; set; }
    }
}
