﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// The Building with areas List Response.
    /// </summary>
    public class BuildingAreasGetResponse
    {
        /// <summary>
        /// Gets or sets the Buildings.
        /// </summary>
        public List<Building> Buildings { get; set; }
    }
}
