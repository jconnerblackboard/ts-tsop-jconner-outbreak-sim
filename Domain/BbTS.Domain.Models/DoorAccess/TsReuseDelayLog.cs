﻿
namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// This object represents a record in the Transact system for a reuse delay log
    /// </summary>
    public class TsReuseDelayLog
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string ReuseDelayName { get; set; }

        /// <summary>
        /// int
        /// </summary>
        public int ObjectId { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string ObjectType { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string ObjectName { get; set; }

        /// <summary>
        /// Last access datetime (as string)
        /// </summary>
        public string LastAccess { get; set; }

        /// <summary>
        /// Delay Time (as string)
        /// </summary>
        public string DelayTime { get; set; }

        /// <summary>
        /// Time remaining (as string)
        /// </summary>
        public string TimeRemaining { get; set; }
    }
}
