﻿using System;
using System.Collections.Generic;

namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// Rd log reset request
    /// </summary>
    public class ReuseDelayLogResetRequest
    {
        /// <summary>
        /// Request Id
        /// </summary>
        public Guid? RequestId { get; set; }

        /// <summary>
        /// Reuse delay log Ids
        /// </summary>
        public List<int> ReuseDelayLogIds { get; set; }
    }
}
