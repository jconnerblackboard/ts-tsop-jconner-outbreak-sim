﻿using System;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming :: TS Object Names

namespace BbTS.Domain.Models.DoorAccess
{

    /// <summary>
    /// This object represents a Transact Da_DeniedTransaction_Card object. Door Access Denied Transactions Child table for Card Info
    /// </summary>
    [Serializable]
    public class TsDa_DeniedTransaction_Card
    {
        /// <summary>
        /// Door Access Denied Transaction Association (from DA_DENIEDTRANSACTION table)
        /// </summary>
        [XmlAttribute]
        public int DeniedTran_Id { get; set; }
        /// <summary>
        /// CARDNUM used for this request
        /// </summary>
        [XmlAttribute]
        public string CardNum { get; set; }
        /// <summary>
        /// The issue number of the card captured during the transaction, if applicable
        /// </summary>
        [XmlAttribute]
        public string Issue_Number { get; set; }
        /// <summary>
        /// Indicates if an issue number was captured during the transaction (T/F)
        /// </summary>
        [XmlAttribute]
        public string Issue_Number_Captured { get; set; }
        /// <summary>
        /// T is the card was swiped vs. manually entered
        /// </summary>
        [XmlAttribute]
        public string Swiped { get; set; }
        /// <summary>
        /// T if a PIN was entered for this request
        /// </summary>
        [XmlAttribute]
        public string Pin_Entered { get; set; }
    }
}
