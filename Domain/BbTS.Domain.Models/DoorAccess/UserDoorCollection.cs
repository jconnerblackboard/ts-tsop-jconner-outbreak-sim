﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.DoorAccess
{
    public class UserDoorCollection
    {
        public List<Door> Doors { get; set; }
        public List<Building> Buildings { get; set; }
        public List<Merchant> Merchants { get; set; }

        protected bool Equals(UserDoorCollection other)
        {
            return 
                Equals(Doors, other.Doors) &&
                Equals(Buildings, other.Buildings) &&
                Equals(Merchants, other.Merchants);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = (Doors != null ? Doors.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Buildings != null ? Buildings.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Merchants != null ? Merchants.GetHashCode() : 0);
                return hashCode;
            }
        }

        public override bool Equals(object other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            if (other.GetType() != this.GetType()) return false;
            return Equals((UserDoorCollection)other);
        } 
    }
}
