﻿
namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// Area
    /// </summary>
    public class Area
    {
        /// <summary>
        /// Area Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Area name
        /// </summary>
        public string Name { get; set; }
    }
}
