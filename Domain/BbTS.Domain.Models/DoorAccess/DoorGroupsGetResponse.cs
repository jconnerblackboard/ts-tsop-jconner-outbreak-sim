﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// The Door Groups List Response.
    /// </summary>
    public class DoorGroupsGetResponse
    {
        /// <summary>
        /// Gets or sets the Door Groups.
        /// </summary>
        public List<TsDoorGroup> DoorGroups { get; set; }
    }
}
