﻿using System.Collections.Generic;
using BbTS.Domain.Models.General;

namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// 
    /// </summary>
    public class DoorStateChangeResponse
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The response message as a base 64 string.
        /// </summary>
        public List<StringPair> HostResponseMessages { get; set; }
    }
}
