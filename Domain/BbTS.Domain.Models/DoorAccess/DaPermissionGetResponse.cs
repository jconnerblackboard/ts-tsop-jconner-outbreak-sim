﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// The Door access permission get response.
    /// </summary>
    public class DaPermissionGetResponse
    {
        /// <summary>
        /// Permissions
        /// </summary>
        public List<TsDaPermissionBase> Permissions { get; set; }
    }
}
