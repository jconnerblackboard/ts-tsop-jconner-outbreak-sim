﻿using System;

namespace BbTS.Domain.Models.DoorAccess
{
    public class CustomerDoorAccessLog
    {
        public string InstitutionId { get; set; }
        /// <summary>
        /// The customer's guid.
        /// </summary>
        public string CustomerGuid { get; set; }
        /// <summary>
        /// Custnum, --VARCHAR2(22 BYTE)
        /// </summary>
        public string CustNum { get; set; }
        /// <summary>
        /// Building Name
        /// </summary>
        public string BuildingName { get; set; }
        /// <summary>
        /// Area Name
        /// </summary>
        public string AreaName { get; set; }
        /// <summary>
        /// DomainId from the door table.
        /// </summary>
        public Guid DoorId { get; set; }
        /// <summary>
        /// Door_Identifier,  --VARCHAR2(10 BYTE)
        /// </summary>
        public string DoorIdentifier { get; set; }
        /// <summary>
        /// Description, --VARCHAR2(30 BYTE)
        /// </summary>
        public string DoorDescription { get; set; }
        /// <summary>
        /// Udf_Functions.Udf_DateTimeNumberTo8601(DAT.actualdatetime) AS EntryDateTime --FLOAT
        /// </summary>
        public DateTimeOffset EntryDateTime { get; set; }
    }
}
