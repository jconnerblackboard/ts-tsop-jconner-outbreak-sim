﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Definitions.Door;

namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// Container class for a request to change a door state.
    /// </summary>
    public class DoorStateChangeRequest
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The numerical identifier for the door.
        /// </summary>
        public List<int> DoorIds { get; set; }

        /// <summary>
        /// Optional audit message.  Leave blank or null to omit audit log message.
        /// </summary>
        public string AuditLogMessage { get; set; }

        /// <summary>
        /// The new state to change the door to.
        /// </summary>
        public DoorStateChangeStateOverrideValues StateOverrideValue { get; set; }

        /// <summary>
        /// The duration to maintain the state override.
        /// </summary>
        public DoorStateChangeDurationOverrideValue DurationOverrideValue { get; set; }
    }
}
