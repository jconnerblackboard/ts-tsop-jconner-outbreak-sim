﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// Building
    /// </summary>
    public class Building
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Area
        /// </summary>
        public List<Area> Areas { get; set; }

        /// <summary>
        /// Compares another instance of building with this one
        /// </summary>
        /// <param name="other">Other instance of building</param>
        /// <returns>Equals</returns>
        protected bool Equals(Building other)
        {
            return Id == other.Id && string.Equals(Name, other.Name);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            unchecked
            {
                return (Id * 397) ^ (Name != null ? Name.GetHashCode() : 0);
            }
        }

        /// <inheritdoc />
        public override bool Equals(object other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            if (other.GetType() != this.GetType()) return false;
            return Equals((Building)other);
        }
    }
}
