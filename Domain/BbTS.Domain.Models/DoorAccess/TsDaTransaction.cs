﻿using System;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming :: TS Object Names

namespace BbTS.Domain.Models.DoorAccess
{
    /// <summary>
    /// This object represents a Transact da_transaction object. Stores all Accepted Door Access Requests
    /// </summary>
    [Serializable]
    public class TsDa_Transaction
    {
        /// <summary>
        /// Identity for this table
        /// </summary>
        [XmlAttribute]
        public int Tran_Id { get; set; }
        /// <summary>
        /// Transaction Sequence Number from the MasterController.   Increments for any transaction, include State Changes (Door Event Log).  Could be used to track the order that stuff happens at a door/mastercontroller
        /// </summary>
        [XmlAttribute]
        public int Mc_TranSeqNum { get; set; }
        /// <summary>
        /// T means Online
        /// </summary>
        [XmlAttribute]
        public string IsOnline { get; set; }
        /// <summary>
        /// 0 - Accepted Online (by Database Procs)
        /// 1 - Accepted Offline(at Device Only)
        /// </summary>
        [XmlAttribute]
        public int Validation_Type { get; set; }
        /// <summary>
        /// DateTime that this data was generated according to the Device
        /// </summary>
        [XmlAttribute]
        public DateTime ActualDateTime { get; set; }
        /// <summary>
        /// DateTime that this data was inserted into the table according to the DB's clock
        /// </summary>
        [XmlAttribute]
        public DateTime PostDateTime { get; set; }
        /// <summary>
        ///
        /// </summary>
        [XmlAttribute]
        public int Tender_Type_Id { get; set; }
        /// <summary>
        /// The identity of the door record
        /// </summary>
        [XmlAttribute]
        public int Door_Id { get; set; }
        /// <summary>
        /// 0 - Cardnum
        /// 1 - future feature such as thumbprint
        /// NOTE: Makes sense to do this now, because this seperation will help to minimze the impact of adding future ID'n methods.  (but at little cost if we never add future ID'n methods)
        /// </summary>
        [XmlAttribute]
        public int Cust_Identification_Type { get; set; }
        /// <summary>
        ///
        /// </summary>
        [XmlAttribute]
        public int Cust_Id { get; set; }
        /// <summary>
        ///
        /// </summary>
        [XmlAttribute]
        public int MasterController_Id { get; set; }
        /// <summary>
        ///
        /// </summary>
        [XmlAttribute]
        public int HostComp_Id { get; set; }
    }
}
