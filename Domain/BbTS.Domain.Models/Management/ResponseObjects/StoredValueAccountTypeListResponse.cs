﻿using System.Collections.Generic;
using BbTS.Domain.Models.StoredValue;

namespace BbTS.Domain.Models.Management.ResponseObjects
{
    /// <summary>
    /// The Stored Value Account Type List Response.
    /// </summary>
    public class StoredValueAccountTypeListResponse : OperationResponse
    {
        /// <summary>
        /// Gets or sets the StoredValueAccountTypes.
        /// </summary>
        public List<TsStoredValueAccountType> StoredValueAccountTypes { get; set; }
    }
}
