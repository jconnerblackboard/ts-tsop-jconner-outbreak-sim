﻿using BbTS.Domain.Models.Customer.Management.BoardPlan;
using System.Collections.Generic;

namespace BbTS.Domain.Models.Management.ResponseObjects
{
    /// <summary>
    ///     The Board Plan Response
    /// </summary>
    public class BoardPlanResponse : OperationResponse
    {
        /// <summary>
        /// Gets or sets the customer board plans.
        /// </summary>
        /// <value>
        /// The customer board plans.
        /// </value>
        public CustomerManagementBoardPlan CustomerBoardPlan { get; set; }
        public List<CustomerManagementBoardPlan> CustomerBoardPlans { get; set; }
    }
}
