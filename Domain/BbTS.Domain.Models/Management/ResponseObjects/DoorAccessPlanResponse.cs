﻿
namespace BbTS.Domain.Models.Management.ResponseObjects
{
    /// <summary>
    ///     The Door Access Plan List response
    /// </summary>
    public class DoorAccessPlanResponse : OperationResponse
    {
        /// <summary>
        /// Gets or sets the door access plans.
        /// </summary>
        /// <value>
        /// The door access plans.
        /// </value>
        public int DoorAccessPlan { get; set; }
    }
}
