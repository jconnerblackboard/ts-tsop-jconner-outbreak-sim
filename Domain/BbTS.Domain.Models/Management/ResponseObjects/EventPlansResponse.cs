﻿using System.Collections.Generic;
using BbTS.Domain.Models.Event;

namespace BbTS.Domain.Models.Management.ResponseObjects
{
    /// <summary>
    /// The Event Plan List Response.
    /// </summary>
    public class EventPlansResponse : OperationResponse
    {
        /// <summary>
        /// Gets or sets the Event Plans.
        /// </summary>
        public IEnumerable<TsEventPlan> EventPlans { get; set; }
    }
}
