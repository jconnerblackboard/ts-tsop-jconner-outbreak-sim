﻿using System.Collections.Generic;
using BbTS.Domain.Models.BoardPlan;

namespace BbTS.Domain.Models.Management.ResponseObjects
{
    /// <summary>
    /// The Board Plan List Response
    /// </summary>
    public class BoardPlanSummaryListResponse : OperationResponse
    {
        /// <summary>
        /// Gets or sets the Board Plans.
        /// </summary>
        public List<TsBoardPlanSummary> BoardPlans { get; set; }
    }
}
