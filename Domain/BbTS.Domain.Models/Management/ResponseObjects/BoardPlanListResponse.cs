﻿using System.Collections.Generic;
using BbTS.Domain.Models.Customer.Management.BoardPlan;

namespace BbTS.Domain.Models.Management.ResponseObjects
{
    /// <summary>
    ///     The Board Plan List Response
    /// </summary>
    public class BoardPlanListResponse : OperationResponse
    {
        /// <summary>
        /// Gets or sets the customer board plans.
        /// </summary>
        /// <value>
        /// The customer board plans.
        /// </value>
        public List<CustomerManagementBoardPlan> CustomerBoardPlans { get; set; }
    }
}
