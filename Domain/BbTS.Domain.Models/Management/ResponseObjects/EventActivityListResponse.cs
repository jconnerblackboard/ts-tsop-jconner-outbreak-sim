﻿using System.Collections.Generic;
using BbTS.Domain.Models.Event;

namespace BbTS.Domain.Models.Management.ResponseObjects
{
    /// <summary>
    /// The Event List Response.
    /// </summary>
    public class EventActivityListResponse : OperationResponse
    {
        /// <summary>
        /// Gets or sets the Events.
        /// </summary>
        public IEnumerable<TsEventActivity> Events { get; set; }
    }
}
