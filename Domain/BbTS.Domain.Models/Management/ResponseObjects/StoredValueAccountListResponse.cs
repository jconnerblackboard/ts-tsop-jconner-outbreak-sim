﻿using System.Collections.Generic;
using BbTS.Domain.Models.Customer.Management.StoredValue;

namespace BbTS.Domain.Models.Management.ResponseObjects
{
    /// <summary>
    ///     The Stored Value Account Plan List response
    /// </summary>
    public class StoredValueAccountListResponse : OperationResponse
    {
        /// <summary>
        /// Gets or sets the stored value accounts.
        /// </summary>
        public List<CustomerSvAccount> StoredValueAccounts { get; set; }
    }
}
