﻿using System.Collections.Generic;
using BbTS.Domain.Models.Customer.Management.EventPlan;

namespace BbTS.Domain.Models.Management.ResponseObjects
{
    /// <summary>
    /// Customer Event Plan List Response
    /// </summary>
    public class EventPlanListResponse : OperationResponse
    {
        /// <summary>
        /// Gets or sets the event plans.
        /// </summary>
        /// <value>
        /// The event plans.
        /// </value>
        public List<CustomerEventPlan> EventPlans { get; set; }
    }
}
