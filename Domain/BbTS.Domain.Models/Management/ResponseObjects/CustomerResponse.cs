﻿using BbTS.Domain.Models.Customer.Management;

namespace BbTS.Domain.Models.Management.ResponseObjects
{
    /// <summary>
    ///     The customer response
    /// </summary>
    public class CustomerResponse : OperationResponse
    {
        /// <summary>
        /// Gets or sets the customer.
        /// </summary>
        /// <value>
        /// The customer.
        /// </value>
        public CustomerManagementData Customer { get; set; }
    }
}
