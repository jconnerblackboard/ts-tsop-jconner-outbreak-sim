﻿using BbTS.Domain.Models.Customer.Management;

namespace BbTS.Domain.Models.Management.ResponseObjects
{
    /// <summary>
    ///     The card number response
    /// </summary>
    public class CardResponse : OperationResponse
    {
        /// <summary>
        /// Gets or sets the customer card.
        /// </summary>
        /// <value>
        /// The customer card.
        /// </value>
        public CustomerCard CardNumber { get; set; }
    }
}
