﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Management.ResponseObjects
{
    /// <summary>
    ///     The Door Access Plan List response
    /// </summary>
    public class DoorAccessPlanListResponse : OperationResponse
    {
        /// <summary>
        /// Gets or sets the door access plans.
        /// </summary>
        /// <value>
        /// The door access plans.
        /// </value>
        public List<int> DoorAccessPlans { get; set; }
    }
}
