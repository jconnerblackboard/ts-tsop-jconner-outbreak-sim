﻿using BbTS.Domain.Models.Customer.Management.EventPlan;

namespace BbTS.Domain.Models.Management.ResponseObjects
{
    /// <summary>
    ///     The Get event plan override by Event Plan Id for a customer response
    /// </summary>
    public class EventPlanOverrideResponse : OperationResponse
    {
        /// <summary>
        /// Gets or sets the event plan overrides.
        /// </summary>
        public CustomerEventPlanOverride EventPlanOverride { get; set; }
    }
}
