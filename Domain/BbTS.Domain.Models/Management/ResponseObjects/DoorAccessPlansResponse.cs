﻿using System.Collections.Generic;
using BbTS.Domain.Models.DoorAccess;

namespace BbTS.Domain.Models.Management.ResponseObjects
{
    /// <summary>
    /// The Door Access List Response.
    /// </summary>
    public class DoorAccessPlansResponse : OperationResponse
    {
        /// <summary>
        /// Gets or sets the Door Access Plans.
        /// </summary>
        public List<TsDoorAccessPlan> DoorAccessPlans { get; set; }
    }
}
