﻿using BbTS.Domain.Models.Customer.Management.StoredValue;

namespace BbTS.Domain.Models.Management.ResponseObjects
{
    /// <summary>
    ///     The Stored Value Account Plan response
    /// </summary>
    public class StoredValueAccountResponse : OperationResponse
    {
        /// <summary>
        /// Gets or sets the stored value account.
        /// </summary>
        public CustomerSvAccount StoredValueAccount { get; set; }
    }
}
