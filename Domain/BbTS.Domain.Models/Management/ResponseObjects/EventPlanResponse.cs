﻿using BbTS.Domain.Models.Customer.Management.EventPlan;

namespace BbTS.Domain.Models.Management.ResponseObjects
{
    /// <summary>
    /// Customer Event Plan Response
    /// </summary>
    public class EventPlanResponse : OperationResponse
    {
        /// <summary>
        /// Gets or sets the event plans.
        /// </summary>
        /// <value>
        /// The event plans.
        /// </value>
        public CustomerEventPlan CustomerEventPlan { get; set; }
    }
}
