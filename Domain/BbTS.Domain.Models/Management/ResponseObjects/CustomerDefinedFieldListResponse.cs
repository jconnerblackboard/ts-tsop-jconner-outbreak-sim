﻿using System.Collections.Generic;
using BbTS.Domain.Models.Customer;

namespace BbTS.Domain.Models.Management.ResponseObjects
{
    /// <summary>
    ///     The Get all customer defined fields
    /// </summary>
    public class CustomerDefinedFieldListResponse : OperationResponse
    {
        /// <summary>
        /// Gets or sets the customer defined fields.
        /// </summary>
        /// <value>
        /// The customer defined fields.
        /// </value>
        public List<TsCustomerDefinedField> CustomerDefinedFields { get; set; }
    }
}
