﻿using System.Collections.Generic;
using BbTS.Domain.Models.Customer.Management;

namespace BbTS.Domain.Models.Management.ResponseObjects
{
    /// <summary>
    ///     The Card number list response
    /// </summary>
    public class CardListResponse : OperationResponse
    {
        /// <summary>
        /// Gets or sets the card numbers.
        /// </summary>
        /// <value>
        /// The card numbers.
        /// </value>
        public List<CustomerCard> CardNumbers { get; set; }
    }
}
