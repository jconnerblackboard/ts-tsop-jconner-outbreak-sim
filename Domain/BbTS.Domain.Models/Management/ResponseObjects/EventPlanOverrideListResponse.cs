﻿using System.Collections.Generic;
using BbTS.Domain.Models.Customer.Management.EventPlan;

namespace BbTS.Domain.Models.Management.ResponseObjects
{
    /// <summary>
    ///     The Get all event plans overrides by Event Plan Id for a customer response
    /// </summary>
    public class EventPlanOverrideListResponse : OperationResponse
    {
        /// <summary>
        /// Gets or sets the event plan overrides.
        /// </summary>
        public List<CustomerEventPlanOverride> EventPlanOverrides { get; set; }
    }
}
