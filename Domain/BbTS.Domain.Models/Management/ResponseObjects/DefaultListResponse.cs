﻿using System.Collections.Generic;
using BbTS.Domain.Models.Customer;

namespace BbTS.Domain.Models.Management.ResponseObjects
{
    /// <summary>
    ///     The Customer Default List response
    /// </summary>
    public class DefaultListResponse : OperationResponse
    {
        /// <summary>
        /// Gets or sets the customer defaults.
        /// </summary>
        /// <value>
        /// The customer defaults.
        /// </value>
        public IEnumerable<TsCustomerDefault> CustomerDefaults { get; set; }
    }
}
