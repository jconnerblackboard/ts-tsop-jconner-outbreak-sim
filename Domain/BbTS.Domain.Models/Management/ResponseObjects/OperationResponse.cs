﻿using BbTS.Domain.Models.Customer.Management;

namespace BbTS.Domain.Models.Management.ResponseObjects
{
    /// <summary>
    ///     The response base for Management Api
    /// </summary>
    public class OperationResponse
    {
        /// <summary>
        /// Gets or sets the operation.
        /// </summary>
        /// <value>
        /// The operation.
        /// </value>
        public Operation Operation { get; set; }
    }
}
