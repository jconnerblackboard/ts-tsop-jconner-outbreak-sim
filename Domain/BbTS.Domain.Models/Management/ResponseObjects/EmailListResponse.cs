﻿using System.Collections.Generic;
using BbTS.Domain.Models.Customer.Management.Email;

namespace BbTS.Domain.Models.Management.ResponseObjects
{
    /// <summary>
    /// The Customer Email Address List response
    /// </summary>
    public class EmailListResponse
    {
        /// <summary>
        /// Gets or sets the customer email addresses.
        /// </summary>
        public List<CustomerEmailAddress> CustomerEmailAddresses { get; set; }
    }
}
