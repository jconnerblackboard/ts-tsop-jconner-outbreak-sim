﻿using System.Collections.Generic;
using BbTS.Domain.Models.Customer;

namespace BbTS.Domain.Models.Management.ResponseObjects
{
    /// <summary>
    /// The Customer Email Type List Response.
    /// </summary>
    public class CustomerEmailTypeListResponse : OperationResponse
    {
        /// <summary>
        /// Gets or sets the CustomerEmailTypeListResponse.
        /// </summary>
        public List<TsCustomerEmailType> CustomerEmailTypes { get; set; }
    }
}
