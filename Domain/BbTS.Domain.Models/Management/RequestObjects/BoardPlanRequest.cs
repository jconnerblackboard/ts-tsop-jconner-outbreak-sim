﻿
namespace BbTS.Domain.Models.Management.RequestObjects
{
    /// <summary>
    /// The board plan request
    /// </summary>
    public class BoardPlanRequest<T> where T : class
    {
        /// <summary>
        /// Gets or sets the customer board plan.
        /// </summary>
        public T CustomerBoardPlan { get; set; }
    }
}
