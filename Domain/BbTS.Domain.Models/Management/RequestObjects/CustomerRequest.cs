﻿
namespace BbTS.Domain.Models.Management.RequestObjects
{
    /// <summary>
    ///     The customer request
    /// </summary>
    public class CustomerRequest<T> where T : class
    {
        /// <summary>
        /// Gets or sets the customer.
        /// </summary>
        /// <value>
        /// The customer.
        /// </value>
        public T Customer { get; set; }
    }
}
