﻿
namespace BbTS.Domain.Models.Management.RequestObjects
{
    /// <summary>
    /// The Door Access plan request
    /// </summary>
    public class DoorAccessPlanRequest
    {
        /// <summary>
        /// Gets or sets the door access plan.
        /// </summary>
        public PostDoorAccessPlan DoorAccessPlan { get; set; }
    }

    /// <summary>
    /// Door access plan
    /// </summary>
    public class PostDoorAccessPlan
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public int Id { get; set; }
    }
}
