﻿
namespace BbTS.Domain.Models.Management.RequestObjects
{
    /// <summary>
    /// The event plan request
    /// </summary>
    public class EventPlanRequest
    {
        /// <summary>
        /// Gets or sets the event plan.
        /// </summary>
        /// <value>
        /// The event plan.
        /// </value>
        public PutEventPlan EventPlan { get; set; }
    }

    /// <summary>
    ///     The Put Model for Event Plan 
    /// </summary>
    public class PutEventPlan
    {
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="PutEventPlan"/> is active.
        /// </summary>
        public bool Active;
    }
}
