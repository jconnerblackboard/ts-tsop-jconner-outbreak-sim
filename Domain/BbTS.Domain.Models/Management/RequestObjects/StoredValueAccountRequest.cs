﻿
namespace BbTS.Domain.Models.Management.RequestObjects
{
    /// <summary>
    ///     The Stored Value Account Request
    /// </summary>
    public class StoredValueAccountRequest<T> where T : class
    {
        /// <summary>
        /// Gets or sets the stored value account.
        /// </summary>
        public T StoredValueAccount { get; set; }
    }
}
