﻿
namespace BbTS.Domain.Models.Management.RequestObjects
{
    /// <summary>
    ///     The Patch Card Number By Customer Number Request
    /// </summary>
    public class CardRequest<T> where T : class
    {
        /// <summary>
        /// Gets or sets the card number details.
        /// </summary>
        public T CardNumber { get; set; }
    }
}
