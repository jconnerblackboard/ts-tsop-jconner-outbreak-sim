﻿
namespace BbTS.Domain.Models.Management.RequestObjects
{
    /// <summary>
    /// The event plan override request
    /// </summary>
    public class EventPlanOverrideRequest<T> where T : class
    {
        /// <summary>
        /// Gets or sets the event plan override.
        /// </summary>
        public T EventPlanOverride { get; set; }
    }
}
