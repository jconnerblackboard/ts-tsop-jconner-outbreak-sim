﻿
namespace BbTS.Domain.Models.Management.RequestObjects
{
    /// <summary>
    ///     The Customer email Address Request
    /// </summary>
    public class CustomerEmailRequest<T>
    {
        /// <summary>
        /// Gets or sets the customer email address.
        /// </summary>
        public T CustomerEmailAddress { get; set; }
    }
}
