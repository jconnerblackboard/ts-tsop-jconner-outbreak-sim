﻿
namespace BbTS.Domain.Models.System
{
    /// <summary>
    /// Container for a response to a request for a control parameter post.
    /// </summary>
    public class ControlParameterPatchResponse
    {
        /// <summary>
        /// The request that spawned this response.
        /// </summary>
        public ControlParameterPatchRequest Request { get; set; }

        /// <summary>
        /// Control parameter that has been set by this operation
        /// </summary>
        public ControlParameter ControlParameter { get; set; }
    }
}
