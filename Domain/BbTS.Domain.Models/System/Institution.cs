﻿using System;

namespace BbTS.Domain.Models.System
{
    /// <summary>
    ///     Institution Model
    /// </summary>
    public class Institution
    {
        /// <summary>
        ///     Gets or sets the Institution id.
        /// </summary>
        public Guid InstitutionId { get; set; }

        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        ///     Gets or sets the short name.
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        ///     Gets or sets the client id.
        /// </summary>
        public int ClientId { get; set; }

        /// <summary>
        /// Gets or sets the name of the domain.
        /// </summary>
        public string DomainName { get; set; }

        /// <summary>
        /// Gets or sets the name of the redirect domain.
        /// </summary>
        public string RedirectDomainName { get; set; }

        /// <summary>
        /// Gets or sets the name of the database.
        /// </summary>
        public string DatabaseName { get; set; }
    }
}
