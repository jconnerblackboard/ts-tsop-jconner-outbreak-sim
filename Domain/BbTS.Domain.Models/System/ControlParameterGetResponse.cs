﻿namespace BbTS.Domain.Models.System
{
    /// <summary>
    /// Container class for the response to a control parameter get request.
    /// </summary>
    public class ControlParameterGetResponse
    {
        /// <summary>
        /// Name of control parameter group
        /// </summary>
        public string ParameterName { get; set; }

        /// <summary>
        /// The requested control parameter.
        /// </summary>
        public ControlParameter ControlParameter { get; set; }
    }
}
