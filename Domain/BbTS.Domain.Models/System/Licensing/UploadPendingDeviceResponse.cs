﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Core;

namespace BbTS.Domain.Models.System.Licensing
{
    /// <summary>
    /// Container class for a licensing server device upload response.
    /// </summary>
    public class UploadPendingDeviceResponse
    {
        /// <summary>
        /// The unique identifier for this request.
        /// </summary>
        public Guid Id { get; internal set; }

        /// <summary>
        /// The date and time this response was received.
        /// </summary>
        public DateTime ResponseDateTime { get; internal set; } = DateTime.Now;

        /// <summary>
        /// The result of the request.
        /// </summary>
        public bool Successful { get; set; }

        /// <summary>
        /// Information returned from the licensing server about the request.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// The request associated with this response
        /// </summary>
        public UploadPendingDeviceRequest Request { get; internal set; }

        /// <summary>
        /// Parameterized Constructor.  Must have the request because the ID of this response gets set to the request id.
        /// </summary>
        /// <param name="request">The request that created this response.</param>
        public UploadPendingDeviceResponse(UploadPendingDeviceRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request), "When creating an UploadPendingDeviceResponse, the request was null.  The request must not be null. ");
            }

            if (request.Id == null)
            {
                throw new ArgumentNullException(nameof(request.Id), "When creating an UploadPendingDeviceResponse, the request Id was null.  The request Id must not be null. ");
            }

            Request = request;
            Id = request.Id;
        }

    }
}
