﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.System.Licensing
{
    /// <summary>
    /// Container class for a licensing server device upload request.
    /// </summary>
    public class UploadPendingDeviceRequest
    {
        /// <summary>
        /// The unique identifier for this request.
        /// </summary>
        public Guid Id { get; internal set; } = Guid.NewGuid();

        /// <summary>
        /// The date and time this request was created.
        /// </summary>
        public DateTime RequestDateTime { get; internal set; } = DateTime.Now;

        /// <summary>
        /// The information associated with the pending device.
        /// </summary>
        public LicenseServerObject DeviceInformation { get; set; }
    }
}
