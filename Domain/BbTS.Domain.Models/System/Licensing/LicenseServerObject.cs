﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Definitions.Licensing;

namespace BbTS.Domain.Models.System.Licensing
{
    /// <summary>
    /// Container class for objects sent to and received from the Sequoia License Server.
    /// </summary>
    public class LicenseServerObject
    {
        /// <summary>
        /// The ip address of the licensing server.
        /// </summary>
        public string LicensingServerIpAddress { get; set; }

        /// <summary>
        /// The serial number or the device identifier.
        /// </summary>
        public string SerialNumberOrDeviceId { get; set; }

        /// <summary>
        /// The device name. 
        /// </summary>
        public string DeviceName { get; set; }

        /// <summary>
        /// The device type.
        /// </summary>
        public LicenseServerDeviceType DeviceType { get; set; }

        /// <summary>
        /// The device subtype.
        /// </summary>
        public LicenseServerDeviceSubType DeviceSubType { get; set; }

        /// <summary>
        /// The unique identifier for the transaction system (the TransactionInstanceGuid in the ControlParameter table).
        /// </summary>
        public string CustomerKey { get; set; }

        /// <summary>
        /// The license key for the transaction system.
        /// </summary>
        public string CustomerSecondKey { get; set; }

        /// <summary>
        /// The requesting entity's name (can be left blank).
        /// </summary>
        public string RequesterName { get; set; }

        /// <summary>
        /// The requesting entity's contact information (can be left blank).
        /// </summary>
        public string RequesterContact { get; set; }
    }
}
