﻿using BbTS.Domain.Models.System.Security;

namespace BbTS.Domain.Models.System
{
    /// <summary>
    /// Container class for a response to an Authentication request (<see cref="AuthenticationRequestToken"/>)
    /// </summary>
    public class ValidateUserResponseToken : ResponseToken
    {
        /// <summary>
        /// The session token created as part of a successful authentication request.
        /// </summary>
        public string SessionToken { get; set; }
    }
}
