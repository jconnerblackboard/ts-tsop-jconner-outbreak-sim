﻿
namespace BbTS.Domain.Models.System
{
    /// <summary>
    /// Container for a request to set control parameter.
    /// </summary>
    public class ControlParameterPatchRequest
    {
        /// <summary>
        /// The unique identifier for this post request and response.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// Id of the parent control parameter
        /// </summary>
        public int? ParentControlParameterId { get; set; }

        /// <summary>
        /// Name of the parameter
        /// </summary>
        public string ParameterName { get; set; }

        /// <summary>
        /// Value of the paramter
        /// </summary>
        public string ParameterValue { get; set; }

        /// <summary>
        /// Description of the parameter
        /// </summary>
        public string ParameterDescription { get; set; }

        /// <summary>
        /// Type of the parameter
        /// </summary>
        public string ParameterType { get; set; }

        /// <summary>
        /// Object Id
        /// </summary>
        public int? ObjectId { get; set; }

        /// <summary>
        /// Object name
        /// </summary>
        public string ObjectName { get; set; }
    }
}
