﻿using System;
using System.Linq;

namespace BbTS.Domain.Models.System
{
    public class ExternalClientUser
    {
        public string ExternalClientId { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public int ClientId { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordHashSalt { get; set; }
        public bool IsActive { get; set; }
        public int HashType { get; set; }
        public int IterationCount { get; set; }

        public ExternalClientUser Clone()
        {
            return (ExternalClientUser)MemberwiseClone();
        }

        public bool Equals(ExternalClientUser other)
        {
            String expectedPassword = new string(PasswordHash.Select(Convert.ToChar).ToArray());
            String actualPassword = new string(other.PasswordHash.Select(Convert.ToChar).ToArray());
            String expectedSalt = new string(PasswordHashSalt.Select(Convert.ToChar).ToArray());
            String actualSalt = new string(other.PasswordHashSalt.Select(Convert.ToChar).ToArray());

            return
                ExternalClientId == other.ExternalClientId &&
                UserName == other.UserName &&
                FullName == other.FullName &&
                ClientId == other.ClientId &&
                expectedPassword == actualPassword &&
                expectedSalt == actualSalt &&
                IsActive == other.IsActive &&
                HashType == other.HashType &&
                IterationCount == other.IterationCount;
        }

        public bool Equals(User other)
        {
            String expectedPassword = new string(PasswordHash.Select(Convert.ToChar).ToArray());
            String actualPassword = new string(other.PasswordHash.Select(Convert.ToChar).ToArray());
            String expectedSalt = new string(PasswordHashSalt.Select(Convert.ToChar).ToArray());
            String actualSalt = new string(other.PasswordHashSalt.Select(Convert.ToChar).ToArray());

            return
                ExternalClientId == other.ExternalClientId &&
                UserName == other.Username &&
                FullName == other.FullName &&
                expectedPassword == actualPassword &&
                expectedSalt == actualSalt &&
                IsActive == other.IsActive &&
                HashType == other.HashType &&
                IterationCount == other.IterationCount;
        }

    }
}
