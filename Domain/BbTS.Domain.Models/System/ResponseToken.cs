﻿using System;

namespace BbTS.Domain.Models.System
{
    /// <summary>
    /// Response token.
    /// </summary>
    public class ResponseToken : IEquatable<ResponseToken>
    {
        /// <summary>
        /// RequestId, GUID, echoed back from corresponding Request.
        /// </summary>
        public string Id;

        /// <summary>
        /// Result domain.
        /// </summary>
        public int ResultDomain;

        /// <summary>
        /// Result domain ID.
        /// </summary>
        public int ResultDomainId;

        /// <summary>
        /// Message.
        /// </summary>
        public string Message;

        /// <summary>
        /// This method checks for equality between two objects
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(ResponseToken other)
        {
            return Id == other.Id &&
                   ResultDomain == other.ResultDomain &&
                   ResultDomainId == other.ResultDomainId &&
                   Message == other.Message;
        }
    }

    /// <summary>
    /// Creating this class as the eventual replacement for ResponseToken.  This will be referenced
    /// across the code base (notably BbTS.Client, TransactAPI, MF4100 libraries, etc.).  In 3.13, 
    /// ResponseToken will be dropped, references will be updated, and this will become the only
    /// implementation of the class.
    /// </summary>
    public class ActionResultToken : ResponseToken
    {
        /// <summary>
        /// This method will clone an object
        /// </summary>
        /// <returns>An <see cref="ActionResultToken"/> object</returns>
        public ActionResultToken Clone()
		{			
            return (ActionResultToken)MemberwiseClone();
		}

        /// <summary>
        /// Returns true if this ActionResultToken matches the expected values.
        /// </summary>
        /// <param name="domain"></param>
        /// <param name="domainId"></param>
        /// <returns></returns>
        public bool IsResult(int domain, int domainId)
        {
            return ((ResultDomain == domain) && (ResultDomainId == domainId));
		}
    }

    /// <summary>
    /// View for a <see cref="ActionResultToken"/>.  (Version 1)
    /// </summary>
    public class ActionResultTokenViewV01
    {
        /// <summary>
        /// RequestId, GUID, echoed back from corresponding Request.
        /// </summary>
        public string Id;

        /// <summary>
        /// Result domain.
        /// </summary>
        public int ResultDomain;

        /// <summary>
        /// Result domain ID.
        /// </summary>
        public int ResultDomainId;

        /// <summary>
        /// Message.
        /// </summary>
        public string Message;
    }

    /// <summary>
    /// Extension methods for <see cref="ActionResultToken"/> conversion.
    /// </summary>
    public static class ActionResultTokenConverter
    {
        /// <summary>
        /// Returns a <see cref="ActionResultTokenViewV01"/> object based on this <see cref="ActionResultToken"/>.
        /// </summary>
        /// <param name="actionResultToken"></param>
        /// <returns></returns>
        public static ActionResultTokenViewV01 ToActionResultTokenViewV01(this ActionResultToken actionResultToken)
        {
            if (actionResultToken == null) return null;

            return new ActionResultTokenViewV01
            {
                Id = actionResultToken.Id,
                ResultDomain = actionResultToken.ResultDomain,
                ResultDomainId = actionResultToken.ResultDomainId,
                Message = actionResultToken.Message
            };
        }

        /// <summary>
        /// Returns a <see cref="ActionResultToken"/> object based on this <see cref="ActionResultTokenViewV01"/>.
        /// </summary>
        /// <param name="actionResultTokenViewV01"></param>
        /// <returns></returns>
        public static ActionResultToken ToActionResultToken(this ActionResultTokenViewV01 actionResultTokenViewV01)
        {
            if (actionResultTokenViewV01 == null) return null;

            return new ActionResultToken
            {
                Id = actionResultTokenViewV01.Id,
                ResultDomain = actionResultTokenViewV01.ResultDomain,
                ResultDomainId = actionResultTokenViewV01.ResultDomainId,
                Message = actionResultTokenViewV01.Message
            };
        }
    }
}
