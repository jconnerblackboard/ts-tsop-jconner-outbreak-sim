﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.System
{
    /// <summary>
    /// Container class for the response to a password complexity rules get request.
    /// </summary>
    public class PasswordComplexityRulesGetResponse
    {
        /// <summary>
        /// List of password complexity rules.
        /// </summary>
        public List<DomainViewObject> Rules { get; set; } = new List<DomainViewObject>();
    }
}
