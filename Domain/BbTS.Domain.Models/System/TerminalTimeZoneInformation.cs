﻿using System;

namespace BbTS.Domain.Models.System
{
    /// <summary>
    /// Data needed by the MF4100 terminal for setting the time zone.
    /// </summary>
    public class TerminalTimeZoneInformation
    {
        /// <summary>
        /// The number of minutes that a time zone is offset from Coordinated Universal Time (UTC).  UTC = local time + bias.  Arizona (UTC-07:00) would have a Bias of 420.
        /// </summary>
        public int Bias { get; set; }

        /// <summary>
        /// The Daylight Savings Time (DST) offset in minutes.  This value is added to the value of the Bias member to form the bias used during daylight saving time.  In most time zones, the value of this member is –60.
        /// </summary>
        public int DaylightBias { get; set; }

        /// <summary>
        /// The system date and local time when the transition from standard time to daylight saving time occurs on this operating system.
        /// </summary>
        public TerminalSystemTime DaylightDate { get; set; }

        /// <summary>
        /// The system display name for a time zone during Daylight Savings Time (DST).
        /// </summary>
        public string DaylightName { get; set; }

        /// <summary>
        /// The name for a time zone.
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// The Standard Time offset in minutes.
        /// </summary>
        public int StandardBias { get; set; }

        /// <summary>
        /// The system date and local time when the transition from daylight saving time to standard time occurs on this operating system.
        /// </summary>
        public TerminalSystemTime StandardDate { get; set; }

        /// <summary>
        /// The system display name for a time zone during non-Daylight Savings Time (DST).
        /// </summary>
        public string StandardName { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public TerminalTimeZoneInformation()
        {
            //Treat as UTC until we find out otherwise.
            StandardName = "UTC";
            DaylightName = "UTC";
        }
    }

    /// <summary>
    /// Data needed by the MF4100 terminal as part of TerminalTimeZoneInformation
    /// </summary>
    public class TerminalSystemTime
    {
        /// <summary>
        /// The year.  The valid values for this member are 1601 through 30827.
        /// </summary>
        public short Year { get; set; }

        /// <summary>
        /// The month.  The valid values for this member are 1 through 12.
        /// </summary>
        public short Month { get; set; }

        /// <summary>
        /// The day of the week.  The valid values for this member are 0 (Sunday) through 6 (Saturday).
        /// </summary>
        public short DayOfWeek { get; set; }

        /// <summary>
        /// The day of the month.  The valid values for this member are 1 through 31.
        /// </summary>
        public short Day { get; set; }

        /// <summary>
        /// The hour.  The valid values for this member are 0 through 23.
        /// </summary>
        public short Hour { get; set; }

        /// <summary>
        /// The minute.  The valid values for this member are 0 through 59.
        /// </summary>
        public short Minute { get; set; }

        /// <summary>
        /// The second.  The valid values for this member are 0 through 59.
        /// </summary>
        public short Second { get; set; }

        /// <summary>
        /// The millisecond.  The valid values for this member are 0 through 999.
        /// </summary>
        public short Milliseconds { get; set; }
    }

}
