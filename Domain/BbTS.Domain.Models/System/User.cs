﻿using System;
using System.Linq;


namespace BbTS.Domain.Models.System
{
    public class User : IEquatable<User>
    {
        public string Username { get; set; }
        public string FullName { get; set; }
        public string Description { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordHashSalt { get; set; }
        public bool IsActive { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsLocked { get; set; }
        public bool PasswordChangeRequired { get; set; }
        public DateTime PasswordCreateDatetime { get; set; }
        public DateTime? ActiveStartDate { get; set; }
        public DateTime? ActiveEndDate { get; set; }
        public DateTime LastLogoutDatetime { get; set; }
        public bool IgnoreMerchantrightsCust { get; set; }
        public int? InactiveAcctDisableDaysOverride { get; set; }
        public int HashType { get; set; }
        public int IterationCount { get; set; }
        public DateTime? AccountLockedOutDateTime { get; set; }
        public int InvalidLoginAttempts { get; set; }
        public string ExternalClientId { get; set; }
        public string ClientId { get; set; }
        public string DomainId { get; set; }
        // ReSharper disable once InconsistentNaming
        //Only valid to get. Not valid to set.
        public bool UseSSOLogin { get; set; }

        public bool Equals(User other)
        {
            String expectedPassword = new string(PasswordHash.Select(Convert.ToChar).ToArray());
            String actualPassword = new string(other.PasswordHash.Select(Convert.ToChar).ToArray());
            String expectedSalt = new string(PasswordHashSalt.Select(Convert.ToChar).ToArray()); 
            String actualSalt = new string(other.PasswordHashSalt.Select(Convert.ToChar).ToArray());

            bool externalClientIdCheck =
                String.IsNullOrEmpty(ExternalClientId) == String.IsNullOrEmpty(other.ExternalClientId) ||
                ExternalClientId == other.ExternalClientId;

            bool clientIdCheck =
                String.IsNullOrEmpty(ClientId) == String.IsNullOrEmpty(other.ClientId) ||
                ClientId == other.ClientId;

            return 
                AccountLockedOutDateTime == other.AccountLockedOutDateTime &&
                ActiveEndDate == other.ActiveEndDate &&
                ActiveStartDate == other.ActiveStartDate &&
                Description == other.Description &&
                externalClientIdCheck &&
                clientIdCheck &&
                FullName == other.FullName &&
                HashType == other.HashType &&
                IgnoreMerchantrightsCust == other.IgnoreMerchantrightsCust &&
                InactiveAcctDisableDaysOverride == other.InactiveAcctDisableDaysOverride &&
                InvalidLoginAttempts == other.InvalidLoginAttempts &&
                IsActive == other.IsActive &&
                IsAdmin == other.IsAdmin &&
                IsLocked == other.IsLocked &&
                IterationCount == other.IterationCount &&
                LastLogoutDatetime == other.LastLogoutDatetime &&
                Username == other.Username &&
                expectedPassword == actualPassword &&
                expectedSalt == actualSalt &&
                UseSSOLogin == other.UseSSOLogin;
        }
    }
}
