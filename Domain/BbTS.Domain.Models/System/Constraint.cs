﻿using System;

namespace BbTS.Domain.Models.System
{
    /// <summary>
    /// A constraint object
    /// </summary>
    public class Constraint
    {
        /// <summary>
        /// The name of the table the constraint is on
        /// </summary>
        public String TableName { get; set; }
        /// <summary>
        /// The name of the constraint
        /// </summary>
        public String ConstraintName { get; set; }
    }

    /// <summary>
    /// A GeneratedIdentityColumn object
    /// </summary>
    public class GeneratedIdentityColumn
    {
        /// <summary>
        /// The name of the table the identity is on
        /// </summary>
        public String TableName { get; set; }
        /// <summary>
        /// The name of the column
        /// </summary>
        public String ColumnName { get; set; }
    }

}
