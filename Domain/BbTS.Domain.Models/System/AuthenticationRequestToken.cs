﻿namespace BbTS.Domain.Models.System
{
    /// <summary>
    /// Container class for an authentication request.
    /// </summary>
    public class AuthenticationRequestToken
    {
        /// <summary>
        /// The unique (Guid) identifier for the request.
        /// </summary>
        public string Id;
        /// <summary>
        /// The user that is attempting to log in.
        /// </summary>
        public string Username;
        /// <summary>
        /// The password for the user attempting to log in.
        /// </summary>
        public string Password;
        /// <summary>
        /// The identification number for the resource the user is logging into.
        /// </summary>
        public int WorkstationId;
        /// <summary>
        /// Denotes whether or not the user is logging in with elevated privileges.
        /// </summary>
        public bool IsSuperUser;
    }
}
