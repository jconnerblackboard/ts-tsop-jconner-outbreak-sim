﻿namespace BbTS.Domain.Models.System
{
    /// <summary>
    /// Container class for a single sign-on auth request.
    /// </summary>
    public class AuthenticationRequestTokenSso
    {
        /// <summary>
        /// The unique (Guid) identifier for the request.
        /// </summary>
        public string Id;

        /// <summary>
        /// The Azure B2C Active Directory sign auth token
        /// </summary>
        public string AuthToken { get; set; }

        /// <summary>
        /// The user that is attempting to log in.
        /// </summary>
        public int WorkstationId;
    }
}
