﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.System
{
    /// <summary>
    /// Container class for a response to a password set request.
    /// </summary>
    public class PasswordSetResponse
    {
        /// <summary>
        /// Results of the password set request.
        /// </summary>
        public ResponseToken ResponseToken { get; set; }

    }
}
