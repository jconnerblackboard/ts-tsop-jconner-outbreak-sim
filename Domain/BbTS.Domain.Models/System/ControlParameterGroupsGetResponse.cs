﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.System
{
    /// <summary>
    /// Container class for handling a server response to a control parameter group request.
    /// </summary>
    public class ControlParameterGroupsGetResponse
    {
        /// <summary>
        /// A list of all control parameter groups.
        /// </summary>
        public List<string> ParameterGroups { get; set; }
    }
}
