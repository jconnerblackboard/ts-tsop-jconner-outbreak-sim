﻿using System;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.System.Database
{
    /// <summary>
    /// This class represents a column in an index
    /// </summary>
    public class IndexColumn
    {
        /// <summary>
        /// The name of the column
        /// </summary>
        [XmlAttribute]
        public string Name { get; set; }
        /// <summary>
        /// The expression (if the column is a functional based index)
        /// </summary>
        [XmlAttribute]
        public string Expression { get; set; }
        /// <summary>
        /// The position of the column in the index
        /// </summary>
        [XmlAttribute]
        public int Position { get; set; }
        /// <summary>
        /// Whether the column is function based or not
        /// </summary>
        [XmlAttribute]
        public bool IsFunctionBased { get; set; }
    }
}