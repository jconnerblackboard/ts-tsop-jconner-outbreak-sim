﻿namespace BbTS.Domain.Models.System.Database
{
    /// <summary>
    /// This class contains the attributes of the connection string
    /// </summary>
    public class ConnectionInfo
    {
        /// <summary>
        /// The name of the Oracle user connecting to the database
        /// </summary>
        public string User { get; set; }
        /// <summary>
        /// The password of the user connecting to the database
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// The name of the server containing the database
        /// </summary>
        public string ServerName { get; set; }
        /// <summary>
        /// The service name of the database
        /// </summary>
        public string ServiceName { get; set; }
        /// <summary>
        /// The TCP/IP Port of the database
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// This method will return a well formatted connection string, without password
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"User Id={User};Data Source={ServerName}:{Port}/{ServiceName}{(User.ToLower().Equals("sys") ? ";DBA Privilege=SYSDBA" : string.Empty)}";
        }
    }
}