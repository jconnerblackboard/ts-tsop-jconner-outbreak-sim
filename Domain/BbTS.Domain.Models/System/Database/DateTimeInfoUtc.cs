﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.System.Database
{
    /// <summary>
    /// Database DateTime info, UTC.
    /// </summary>
    [Serializable]
    public class DateTimeInfoUtc
    {
        /// <summary>
        /// Year.
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// Month.
        /// </summary>
        public int Month { get; set; }

        /// <summary>
        /// Day.
        /// </summary>
        public int Day { get; set; }

        /// <summary>
        /// Hour.
        /// </summary>
        public int Hour { get; set; }

        /// <summary>
        /// Minutes.
        /// </summary>
        public int Minutes { get; set; }

        /// <summary>
        /// Seconds.
        /// </summary>
        public double Seconds { get; set; }

        /// <summary>
        /// Database timezone offset.
        /// </summary>
        public string DatabaseTimezone { get; set; }
    }
}
