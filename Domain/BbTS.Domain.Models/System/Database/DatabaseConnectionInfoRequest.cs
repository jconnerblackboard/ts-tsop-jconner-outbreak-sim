﻿using BbTS.Domain.Models.Definitions;
using BbTS.Domain.Models.System.Security;

namespace BbTS.Domain.Models.System.Database
{
    /// <summary>
    /// Container class for a database connection information request. 
    /// </summary>
    public class DatabaseConnectionInfoRequest
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The type of database connection string requested.
        /// </summary>
        public DatabaseConnectionStringType ConnectionStringType { get; set; }
    }
}
