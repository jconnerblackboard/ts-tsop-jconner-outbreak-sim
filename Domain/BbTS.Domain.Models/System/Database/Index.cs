﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.System.Database
{

    [XmlRoot("Indexes"), Serializable]
    public class Indexes
    {
        /// <summary>
        /// This is a List of Index objects
        /// </summary>
        [XmlElement("Index")]
        public List<Index> Items { get; set; }
    }

    /// <summary>
    /// This class represents a database index
    /// </summary>
    public class Index
    {
        /// <summary>
        ///  The name of the table 
        /// </summary>
        [XmlAttribute]
        public string TableName { get; set; }
        /// <summary>
        /// The name of the index
        /// </summary>
        [XmlAttribute]
        public string IndexName { get; set; }

        /// <summary>
        /// The type of index this is
        /// </summary>
        [XmlAttribute]
        public IndexType IndexType { get; set; }

        [XmlAttribute]
        public String Tablespace { get; set; }

        /// <summary>
        /// This is the list of columns associated with an index
        /// </summary>
        [XmlElement("Column")]
        public List<IndexColumn> Columns { get; set; }

        /// <summary>
        /// The MD5 hash of the object
        /// </summary>
        [XmlAttribute]
        public string Md5Hash { get; set; }
    }

    public enum IndexType
    {
        NonUnique = 0,
        Unique = 1
    }
}
