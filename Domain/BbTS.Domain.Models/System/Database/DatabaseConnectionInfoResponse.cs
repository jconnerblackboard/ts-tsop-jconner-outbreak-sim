﻿using Newtonsoft.Json;

namespace BbTS.Domain.Models.System.Database
{
    /// <summary>
    /// 
    /// </summary>
    public class DatabaseConnectionInfoResponse
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The encrypted database connection string.
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// Parameterless constructor for (de)serialization.
        /// </summary>
        public DatabaseConnectionInfoResponse()
        {
        }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="requestId">The unique identifier for the request.</param>
        /// <param name="connectionString">The encrypted database connection string.</param>
        [JsonConstructor]
        public DatabaseConnectionInfoResponse(string requestId, string connectionString)
        {
            RequestId = requestId;
            ConnectionString = connectionString;
        }
    }
}
