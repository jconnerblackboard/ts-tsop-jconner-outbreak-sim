﻿using System;

namespace BbTS.Domain.Models.System
{
    public class ControlParameter
    {
        public Int32 ControlParameterId { get; set; }
        public Int32 ParentControlParameterId { get; set; }
        public String ParameterName { get; set; }
        public String ParameterValue { get; set; }
        public String ParameterDescription { get; set; }
        public String ParameterType { get; set; }
        public Int32 ObjectId { get; set; }
        public String ObjectName { get; set; }
        public DateTime ModifiedDate { get; set; }
    }

    public class ControlParameterModified : ControlParameter
    {
        public Boolean IsModified { get; set; }
    }
}