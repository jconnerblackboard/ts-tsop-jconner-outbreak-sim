﻿namespace BbTS.Domain.Models.System
{
    public class UserAccountRules
    {
        public int UserAccountRuleId;
        public int PasswordLifeTimeDays;
        public int PasswordMaxAttempt;
        public int PasswordHistoryCount;
        public int PasswordLengthMinimum;
        public int PasswordExpireWarningDays;
        public int LockoutDurationMinutes;
        public int SessionIdleTimeoutMinutes;
        public int InactiveAccountDisableDays;
        public bool InactiveAccountDisableDaysAllowOverride;
    }
}
