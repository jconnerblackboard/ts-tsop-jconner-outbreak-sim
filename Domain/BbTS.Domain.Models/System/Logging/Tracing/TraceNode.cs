﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BbTS.Domain.Models.MediaServer;

namespace BbTS.Domain.Models.System.Logging.Tracing
{
    public class TraceNode
    {
        public string Arguments { get; set; }
        public List<TraceNode> Children { get; set; }
        public DateTime EntryTimeStamp { get; set; }
        public string ExceptionMessage { get; set; }
        public bool ExceptionThrown { get; set; }
        public DateTime ExitTimeStamp { get; set; }
        public bool IsClosed { get; set; }
        public bool IsOpen { get; set; }
        public string MethodName { get; set; }
        public string ReturnValue { get; set; }

        public TraceNode()
        {
            Children = new List<TraceNode>();
        }

        public TraceNode Clone()
        {
            TraceNode clone = (TraceNode)MemberwiseClone();
            clone.Children = new List<TraceNode>(Children.Select(c => c.Clone()));
            return clone;
        }

    }
}
