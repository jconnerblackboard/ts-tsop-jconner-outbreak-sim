﻿using System;
using BbTS.Domain.Models.Definitions;

namespace BbTS.Domain.Models.System.Logging
{
    public class AuditLogItem : IEquatable<AuditLogItem>
    {
        public string Username { get; set; }
        public AuditLogEventType EventTypeId { get; set; }
        public AuditLogEntityName EntityId { get; set; }
        public string AffectedDataDescription { get; set; }
        public string AffectedDataId { get; set; } // user being edited (for user functions)
        public bool SuccessFlag { get; set; }
        public string Program { get; set; }
        public string OsHostName { get; set; }
        public string IpAddress { get; set; }
        public string OsUser { get; set; }

        public bool Equals(AuditLogItem other)
        {
            return
                Username == other.Username &&
                EventTypeId == other.EventTypeId &&
                EntityId == other.EntityId &&
                AffectedDataDescription == other.AffectedDataDescription &&
                AffectedDataId == other.AffectedDataId &&
                SuccessFlag == other.SuccessFlag &&
                Program == other.Program &&
                OsHostName == other.OsHostName &&
                IpAddress == other.IpAddress &&
                OsUser == other.OsUser;
        }
    }
}
