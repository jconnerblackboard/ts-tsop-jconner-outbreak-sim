﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using BbTS.Domain.Models.Definitions.Logging;

namespace BbTS.Domain.Models.System.Logging.Tracking
{
    public class TrackingReport
    {
        [XmlAttribute]
        public string Name { get; set; }

        [XmlAttribute]
        public string State { get; set; }

        [XmlAttribute]
        public string ChildrenComplete { get; set; }

        [XmlAttribute]
        public string RunTime { get; set; }

        [XmlAttribute]
        public string StartTime { get; set; }

        [XmlAttribute]
        public string EndTime { get; set; }

        public List<String> ErrorMessages { get; set; }
        public List<TrackingReport> Children { get; set; }

        public static TrackingReport CreateReport(ProcessTrackerObject obj)
        {
            var childCompletedCount = obj.Children != null && obj.Children.Count > 0 ? obj.Children.FindAll(c => c.CurrentState == ProcessTrackingObjectState.Completed).Count : 0;
            return new TrackingReport
            {
                Name = obj.Name,
                StartTime = obj.CurrentState == ProcessTrackingObjectState.NotStarted ? "N/A" : obj.StartTime.ToString("yyyy/MM/dd hh:mm:ss.fff"),
                EndTime = obj.CurrentState != ProcessTrackingObjectState.Completed ? "N/A" : obj.EndTime.ToString("yyyy/MM/dd hh:mm:ss.fff"),
                RunTime =
                    obj.CurrentState == ProcessTrackingObjectState.InProgress ? (DateTime.Now - obj.StartTime).ToString("g") :
                        obj.CurrentState == ProcessTrackingObjectState.Completed ? (obj.EndTime - obj.StartTime).ToString("g") : "N/A",
                State = obj.CurrentState.ToString(),
                Children = obj.Children != null && obj.Children.Count > 0 ? new List<TrackingReport>(obj.Children.Select(CreateReport)) : null,
                ErrorMessages = obj.ErrorMessages != null && obj.ErrorMessages.Count > 0 ? new List<string>(obj.ErrorMessages) : null,
                ChildrenComplete =
                    obj.Children != null && obj.Children.Count > 0 ?
                        String.Format("{0} of {1}", childCompletedCount, Math.Max(obj.ExpectedChildCount, childCompletedCount))
                        : null
            };
        }

    }
}
