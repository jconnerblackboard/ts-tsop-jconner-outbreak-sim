﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Definitions.Logging;

namespace BbTS.Domain.Models.System.Logging.Tracking
{
    /// <summary>
    /// Container class used by the EventLogTracker to filter event log messages on a periodic basis.
    /// </summary>
    public class EventLogTrackingObject : IEqualityComparer<EventLogTrackingObject>
    {
        /// <summary>
        /// Unique identifier for this entry.
        /// </summary>
        public Guid Id { get; set; } = Guid.NewGuid();

        /// <summary>
        /// The event category for this log entry.
        /// </summary>
        public LoggingDefinitions.Category EventCategory { get; set; } = LoggingDefinitions.Category.General;

        /// <summary>
        /// The event id for this entry.
        /// </summary>
        public LoggingDefinitions.EventId EventId { get; set; } = LoggingDefinitions.EventId.General;

        /// <summary>
        /// The severity of the entry.
        /// </summary>
        public EventLogEntryType Severity { get; set; } = EventLogEntryType.Information;

        /// <summary>
        /// The event message for this entry.
        /// </summary>
        public string Message { get; set; } = string.Empty;

        /// <summary>
        /// The date and time this event was created.
        /// </summary>
        public DateTime EventDateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// Override equality operator.
        /// </summary>
        /// <param name="obj">Object to compare this to.</param>
        /// <returns></returns>

        public override bool Equals(object obj)
        {
            var comparisonObject = obj as EventLogTrackingObject;

            if (comparisonObject == null) return false;
            return
                EventCategory == comparisonObject.EventCategory &&
                EventId == comparisonObject.EventId &&
                Message == comparisonObject.Message &&
                Severity == comparisonObject.Severity;
        }

        /// <summary>
        /// Override equality operator.
        /// </summary>
        /// <param name="x">Comparer A</param>
        /// <param name="y">Comparer B</param>
        /// <returns></returns>
        public bool Equals(EventLogTrackingObject x, EventLogTrackingObject y)
        {
            return
                x.EventCategory == y.EventCategory &&
                x.EventId == y.EventId &&
                x.Message == y.Message &&
                x.Severity == y.Severity;
        }

        /// <summary>
        /// Override hash code.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int GetHashCode(EventLogTrackingObject obj)
        {
            var hashCode = -2119049347;
            hashCode = hashCode * -1521134295 + EqualityComparer<Guid>.Default.GetHashCode(obj.Id);
            hashCode = hashCode * -1521134295 + obj.EventCategory.GetHashCode();
            hashCode = hashCode * -1521134295 + obj.EventId.GetHashCode();
            hashCode = hashCode * -1521134295 + obj.Severity.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(obj.Message);
            hashCode = hashCode * -1521134295 + obj.EventDateTime.GetHashCode();
            return hashCode;
        }

        /// <summary>
        /// Override hash code (no parameter).
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            var hashCode = -2119049347;
            hashCode = hashCode * -1521134295 + EqualityComparer<Guid>.Default.GetHashCode(Id);
            hashCode = hashCode * -1521134295 + EventCategory.GetHashCode();
            hashCode = hashCode * -1521134295 + EventId.GetHashCode();
            hashCode = hashCode * -1521134295 + Severity.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Message);
            hashCode = hashCode * -1521134295 + EventDateTime.GetHashCode();
            return hashCode;
        }
    }
}
