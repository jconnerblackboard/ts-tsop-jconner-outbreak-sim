﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using BbTS.Domain.Models.Definitions.Logging;

namespace BbTS.Domain.Models.System.Logging.Tracking
{
    public class ProcessTrackerObject
    {
        public const string DefaultName = "Untitled";
        public String Name { get; set; }
        public String Id { get; set; }
        public String ParentId { get; set; }
        public List<String> ErrorMessages { get; set; }
        public ProcessTrackingObjectState CurrentState { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int ExpectedChildCount { get; set; }
        public List<ProcessTrackerObject> Children { get; set; } 

        public ProcessTrackerObject()
        {
            Name = DefaultName;
            Id = Guid.NewGuid().ToString("D");
            ErrorMessages = new List<string>();
            Children = new List<ProcessTrackerObject>();
            CurrentState = ProcessTrackingObjectState.NotStarted;
        }

        public ProcessTrackerObject Clone()
        {
            var clone = (ProcessTrackerObject)MemberwiseClone();
            clone.Children = new List<ProcessTrackerObject>(Children.Select(c => c.Clone()));
            clone.ErrorMessages = new List<string>(ErrorMessages);
            return clone;
        }

        public override string ToString()
        {
            return String.Format(
                "{0}: [id='{1}'] State={2}{3}{4}{5}{6}",
                Name,
                Id,
                CurrentState,
                CurrentState == ProcessTrackingObjectState.Completed ? String.Format(", Runtime: {0}.", (EndTime - StartTime).ToString("g")) : "",
                CurrentState == ProcessTrackingObjectState.NotStarted ? "" : String.Format(", Started={0},", StartTime.ToString("yyyy/MM/dd hh:mm:ss.fff")),
                CurrentState == ProcessTrackingObjectState.Completed ? String.Format(", Completed: {0}.", EndTime.ToString("yyyy/MM/dd hh:mm:ss.fff")) : "",
                String.Format(", Children={0}/{1} completed.", Children.FindAll(c => c.CurrentState == ProcessTrackingObjectState.Completed).Count, ExpectedChildCount));
        }
    }
}
