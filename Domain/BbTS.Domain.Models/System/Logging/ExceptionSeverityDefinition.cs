﻿using System;

namespace BbTS.Domain.Models.System.Logging
{
    public class ExceptionSeverityDefinition 
    {
        public Int32 Code { get; set; }
        public String Message { get; set; }
        public Int32 Severity { get; set; }
    }
}
