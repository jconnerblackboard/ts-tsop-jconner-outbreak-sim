﻿using System;
using System.Xml.Serialization;
// ReSharper disable once InconsistentNaming

namespace BbTS.Domain.Models.System
{
    /// <summary>
    /// This object represents a Transact HostNames object. Contains the Computer name that the host runs on
    /// </summary>
    [Serializable]
    public class TsHostNames
    {
        /// <summary>
        /// Computer ID
        /// </summary>
        [XmlAttribute]
        public int Computer_Id { get; set; }
        /// <summary>
        /// Computer Name
        /// </summary>
        [XmlAttribute]
        public string ComputerName { get; set; }
        /// <summary>
        /// Starting COMM Port
        /// </summary>
        [XmlAttribute]
        public int StartingCommPort { get; set; }
    }
}
