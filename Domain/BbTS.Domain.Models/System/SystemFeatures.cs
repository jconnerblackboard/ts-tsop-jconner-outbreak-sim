﻿using System;

namespace BbTS.Domain.Models.System
{
    /// <summary>
    /// Container class for TS SystemFeatures.
    /// </summary>
    public class SystemFeatures
    {

        #region From SystemFeatures Table

        /// <summary>
        /// License number assigned to the TS instance.
        /// </summary>
        public string LicenseNumber { get; set; }

        /// <summary>
        /// Name of the Campus the License is assigned to.
        /// </summary>
        public string CampusName { get; set; }

        /// <summary>
        /// Time of day the nightly processing kicks off at.
        /// </summary>
        public DateTime NitelyPro { get; set; }

        /// <summary>
        /// Customer number mask.
        /// </summary>
        public string CustNumMask { get; set; }

        /// <summary>
        /// Card number mask.
        /// </summary>
        public string CardNumMask { get; set; }

        /// <summary>
        /// Minimum length for a client login length.
        /// </summary>
        public int MinLoginNameLength { get; set; }

        /// <summary>
        /// The right offset for the card display mask.
        /// </summary>
        public int CardDisplayMaskRightOffset { get; set; }

        /// <summary>
        /// The length of the card display mask.
        /// </summary>
        public int CardDisplayMaskLength { get; set; }

        /// <summary>
        /// The maximum number of allowed persona access points.
        /// </summary>
        public int PersonaAccessPointLimit { get; set; }

        /// <summary>
        /// The system currency value (i.e. USD for United States Dollar).
        /// </summary>
        public string SystemCurrencyValue { get; set; }

        #endregion

        #region Decoded from the license number

        /// <summary>
        /// Gets or sets the Expiration Date.
        /// </summary>
        public DateTimeOffset ExpirationDate { get; set; }

        #endregion

        #region From the ControlParameter Table under TransactInstanceGuid

        /// <summary>
        /// Represents the unique identifier for the Transaction System.
        /// Maps to the TransactInstanceGuid in the ControlParameters table.
        /// </summary>
        public string TransactionSystemId { get; set; }

        #endregion
    }
}
