﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.System
{
    /// <summary>
    /// Container class for handling a server response to a control parameter request.
    /// </summary>
    public class ControlParameterGroupGetResponse
    {
        /// <summary>
        /// Name of control parameter group
        /// </summary>
        public string GroupParameterName { get; set; }

        /// <summary>
        /// List of control parameters
        /// </summary>
        public List<ControlParameter> ControlParameters { get; set; }
    }
}
