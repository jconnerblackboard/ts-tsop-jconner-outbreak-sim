﻿namespace BbTS.Domain.Models.System
{
    /// <summary>
    /// Container class for a response to a change password current user request.
    /// </summary>
    public class ChangePasswordCurrentUserResponse
    {
        /// <summary>
        /// The response to the request.
        /// </summary>
        public ResponseToken ResponseToken { get; set; }

        /// <summary>
        /// The session token created during the request.
        /// </summary>
        public string SessionToken { get; set; }
    }
}
