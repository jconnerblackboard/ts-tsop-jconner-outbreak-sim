﻿using System;
using BbTS.Domain.Models.Definitions.Institution;

namespace BbTS.Domain.Models.System
{
    /// <summary>
    /// Container class representing a TransactionSystem in the Service Point world.
    /// </summary>
    public class TransactionSystem
    {
        private Guid? _institutionId;

        /// <summary>
        ///     Gets or sets the Transaction System Id.
        /// </summary>
        public Guid TransactionSystemId { get; set; }

        /// <summary>
        /// Gets or sets the instance identifier.
        /// </summary>
        public Guid InstanceId { get; set; }

        /// <summary>
        ///     Gets or sets the License Name.
        /// </summary>
        public string LicenseName { get; set; }

        /// <summary>
        ///     Gets or sets the License Number.
        /// </summary>
        public string LicenseNumber { get; set; }

        /// <summary>
        ///     Gets or sets the Expiration Date.
        /// </summary>
        public DateTimeOffset ExpirationDate { get; set; }

        /// <summary>
        ///     Gets or sets the Institution associated with this transaction system.
        /// </summary>
        public Institution Institution { get; set; }

        /// <summary>
        ///     Gets or sets the Institution Id.
        /// </summary>
        public Guid InstitutionId
        {
            get { return _institutionId ?? Institution?.InstitutionId ?? Guid.Empty; }
            set { _institutionId = value; }
        }

        /// <summary>
        ///     Get the validity flag for this transaction system.  
        /// </summary>
        public bool IsValid => TransactionSystemId != Guid.Empty && InstanceId != Guid.Empty && InstitutionId != Guid.Empty && InstitutionId != InstitutionDefinitions.UnknownInstitutionGuid;

    }
}
