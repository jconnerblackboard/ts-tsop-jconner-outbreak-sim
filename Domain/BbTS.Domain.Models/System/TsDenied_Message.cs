﻿using System;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.System
{
    /// <summary>
    /// This object represents a Transact Denied_Message object. Denied Message Table (replaces PackDeniedCodes constants)
    /// </summary>
    [Serializable]
    public class TsDenied_Message
    {
        /// <summary>
        /// Denied Message Identifier
        /// </summary>
        [XmlAttribute]
        public int Denied_Message_Id { get; set; }
        /// <summary>
        /// Denied Message Name
        /// </summary>
        [XmlAttribute]
        public string Denied_Message_Name { get; set; }
        /// <summary>
        /// Denied Message Description
        /// </summary>
        [XmlAttribute]
        public string Denied_Message_Text { get; set; }
        /// <summary>
        /// Denied Message Short Description
        /// </summary>
        [XmlAttribute]
        public string Denied_Message_Short_Text { get; set; }
        /// <summary>
        /// Category of Message
        /// </summary>
        [XmlAttribute]
        public string Denied_Category { get; set; }
    }
}