﻿using System;

namespace BbTS.Domain.Models.System.Security.Certificate
{
    public class CertificateBinding
    {
        public string ApplicationId { get; set; }
        public string CertificateHash { get; set; }
        public string CertificateStoreName { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string FriendlyName { get; set; }
        public bool InCertificateStore { get; set; }
        public string Port { get; set; }
        public string Subject { get; set; }
        public string SubjectName { get; set; }
        public string Thumbprint { get; set; }

        public override string ToString()
        {
            return String.Format(
                "ApplicationId={0}\r\n" +
                "CertificateHash={1}\r\n" +
                "CertificateStoreName={2}\r\n" +
                "ExpirationDate={3}\r\n" +
                "FriendlyName={4}\r\n" +
                "InCertificateStore={5}\r\n" +
                "Port={6}\r\n" +
                "Subject={7}\r\n" +
                "SubjectName={8}\r\n" +
                "Thumbprint={9}\r\n",
                ApplicationId,
                CertificateHash,
                CertificateStoreName,
                ExpirationDate,
                FriendlyName,
                InCertificateStore,
                Port,
                Subject,
                SubjectName,
                Thumbprint);
        }
    }
}
