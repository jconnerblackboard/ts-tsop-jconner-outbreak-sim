﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.System.Security
{
    public class PasswordSetRequestToken
    {
        public String Id { get; set; }

        public String TargetUsername { get; set; }
        public String TargetPassword { get; set; }

        //public String ApplicationName { get; set; }
        //public String OsHostName { get; set; }
        //public String IpAddress { get; set; }
        //public String OsUser { get; set; }
    }
}
