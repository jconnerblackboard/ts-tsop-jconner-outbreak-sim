﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.System.Security
{
    public class PasswordHistoryItem : IEquatable<PasswordHistoryItem>
    {
        public int Id { get; set; }
        public String Username { get; set; }
        public DateTime DateTime { get; set; }
        public String PasswordHash { get; set; }
        public String PasswordHashSalt { get; set; }
        public int HashType { get; set; }
        public int IterationCount { get; set; }

        public bool Equals(PasswordHistoryItem other)
        {
            return
                Id == other.Id &&
                Username == other.Username &&
                DateTime == other.DateTime &&
                PasswordHash == other.PasswordHash &&
                PasswordHashSalt == other.PasswordHashSalt &&
                HashType == other.HashType &&
                IterationCount == other.IterationCount;
        }
    }
}
