﻿namespace BbTS.Domain.Models.System.Security
{
    /// <summary>
    /// A ChangePasswordCurrentUserRequestToken object
    /// </summary>
    public class ChangePasswordCurrentUserRequestToken
    {
        /// <summary>
        /// The request guid
        /// </summary>
        public string Id;   //RequestId, GUID
        /// <summary>
        /// The name of the user
        /// </summary>
        public string Username;
        /// <summary>
        /// The current password
        /// </summary>
        public string CurrentPassword;
        /// <summary>
        /// The new password
        /// </summary>
        public string NewPassword;
        /// <summary>
        /// The identity of the workstation this is being done from
        /// </summary>
        public int WorkstationId;
        /// <summary>
        /// Whether this user is the super user
        /// </summary>
        public bool IsSuperUser;
    }
}
