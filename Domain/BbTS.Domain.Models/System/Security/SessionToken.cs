﻿using System;

namespace BbTS.Domain.Models.System.Security
{
    public class SessionToken : IEquatable<SessionToken>
    {
        public String ClientId { get; set; }
        public String Username { get; set; }
        public String TokenId { get; set; }

        public bool Equals(SessionToken other)
        {
            return
                ClientId == other.ClientId &&
                Username == other.Username &&
                TokenId == other.TokenId;
        }
    }
}
