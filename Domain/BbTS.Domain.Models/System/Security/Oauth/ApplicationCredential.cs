﻿using System;

namespace BbTS.Domain.Models.System.Security.Oauth
{
    /// <summary>
    /// An Application Credential model
    /// </summary>
    public class ApplicationCredential
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the Consumer Key
        /// </summary>
        /// <value>
        /// The Consumer Key
        /// </value>
        public Guid ConsumerKey { get; set; }

        /// <summary>
        /// Gets or sets the Consumer Secret
        /// </summary>
        /// <value>
        /// The Consumer Secret
        /// </value>
        public string ConsumerSecret { get; set; }

        /// <summary>
        /// Gets or sets the Time To Live
        /// </summary>
        /// <value>
        /// The Time To Live
        /// </value>
        public TimeSpan TimeToLive { get; set; }

        #endregion
    }
}
