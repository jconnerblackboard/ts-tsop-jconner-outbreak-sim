﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.System.Security.Oauth
{
    /// <summary>
    /// A Get Application Credential by Consumer Key Response model
    /// </summary>
    public class GetApplicationCredentialByConsumerKeyResponse 
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the application credential.
        /// </summary>
        /// <value>
        /// The application credential.
        /// </value>
        public ApplicationCredential ApplicationCredential { get; set; }

        #endregion
    }
}
