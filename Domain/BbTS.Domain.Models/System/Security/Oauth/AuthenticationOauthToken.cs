﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.System.Security.Oauth
{
    /// <summary>
    /// Container class to hold an oauth token as it goes into and out of the database layer.
    /// </summary>
    public class AuthenticationOauthToken
    {
        /// <summary>
        /// The key to the oauth token store. 
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// The value of the token.
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// The expiration data and time for the token.
        /// </summary>
        public DateTime ExpirationDateTime { get; set; }
    }
}
