﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using BbTS.Domain.Models.Definitions.Security;
using BbTS.Domain.Models.Definitions.Security.Oauth;
using BbTS.Domain.Models.MediaServer;

namespace BbTS.Domain.Models.System.Security.Oauth
{
    /// <summary>
    ///     The OAuth parameters sent on a request
    /// </summary>
    public class OAuthParameters
    {
        /// <summary>
        ///     The parameters
        /// </summary>
        private readonly Hashtable parameters;

        /// <summary>
        ///     Initializes a new instance of the <see cref="OAuthParameters" /> class.
        /// </summary>
        public OAuthParameters()
        {
            parameters = new Hashtable();
            SortedParameters = new SortedSet<NameValuePair>();
        }

        /// <summary>
        /// The base URI of the authentication service.
        /// </summary>
        public string BaseUri { get; set; }

        /// <summary>
        /// The route to take from the BaseUri to request a temporary token.
        /// </summary>
        public string RequestRoute { get; set; } = OAuthConstants.OAuthTemporaryTokenPath;

        /// <summary>
        /// The route to take from the BaseUri to request an access token.
        /// </summary>
        public string TokenRoute { get; set; } = OAuthConstants.OAuthTokenPath;

        /// <summary>
        /// The current OAuth token key returned from the service.
        /// </summary>
        public string TokenKey { get; set; }

        /// <summary>
        /// The current OAuth token secret returned from the service.
        /// </summary>
        public string TokenSecret { get; set; }

        /// <summary>
        /// The Consumer Secret.
        /// </summary>
        public string ConsumerSecret { get; set; }

        /// <summary>
        ///     Gets or sets the sorted parameters.
        /// </summary>
        /// <value>
        ///     The sorted parameters.
        /// </value>
        public SortedSet<NameValuePair> SortedParameters { get; set; }

        /// <summary>
        ///     Gets or sets the consumer key.
        /// </summary>
        /// <remarks>
        ///     The identifier portion of the client credentials (equivalent to a username).  The parameter name reflects a
        ///     deprecated term (Consumer Key) used in previous revisions of the specification, and has been retained to maintain
        ///     backward compatibility.
        /// </remarks>
        /// <value>
        ///     The consumer key (oauth_consumer_key).
        /// </value>
        public string ConsumerKey
        {
            get
            {
                var consumerKey = parameters[OAuthConstants.OAuthConsumerKey] as string;
                return consumerKey ?? string.Empty;
            }

            set
            {
                SortedParameters.Add(new NameValuePair { Name = OAuthConstants.OAuthConsumerKey, Value = value });
                parameters.Add(OAuthConstants.OAuthConsumerKey, value);
            }
        }

        /// <summary>
        ///     Gets or sets the signature method.
        /// </summary>
        /// <remarks>
        ///     The name of the signature method used by the client to sign the request, as defined in Section 3.4.
        /// </remarks>
        /// <value>
        ///     The signature method (oauth_signature_method).
        /// </value>
        public string SignatureMethod
        {
            get
            {
                var signatureMethod = parameters[OAuthConstants.OAuthSignatureMethod] as string;
                return signatureMethod ?? string.Empty;
            }

            set
            {
                // Note: Not using an emum here specifically because it makes parsing and handling the string values
                // more difficult mainly because the string values that are supported by the spec contain hyphens
                if (value == null
                    || !(value.Equals(CryptoConstants.HmacSha1, StringComparison.Ordinal)
                         || value.Equals(CryptoConstants.RsaSha1, StringComparison.Ordinal)
                         || value.Equals(CryptoConstants.Plaintext, StringComparison.Ordinal)))
                {
                    var message = string.Format(
                        CultureInfo.InvariantCulture,
                        "Invalid value specified for SignatureMethod. Acceptable values are: {0}; {1}; {2}",
                        CryptoConstants.HmacSha1,
                        CryptoConstants.RsaSha1,
                        CryptoConstants.Plaintext);
                    throw new ArgumentOutOfRangeException("value", message);
                }

                SortedParameters.Add(new NameValuePair { Name = OAuthConstants.OAuthSignatureMethod, Value = value });
                parameters.Add(OAuthConstants.OAuthSignatureMethod, value);
            }
        }

        /// <summary>
        ///     Gets or sets the timestamp.
        /// </summary>
        /// <remarks>
        ///     The timestamp value as defined in Section 3.3.  The parameter MAY be omitted when using the "PLAINTEXT" signature
        ///     method.
        /// </remarks>
        /// <value>
        ///     The timestamp.
        /// </value>
        public string Timestamp
        {
            get
            {
                var timestamp = parameters[OAuthConstants.OAuthTimestamp] as string;
                return timestamp ?? string.Empty;
            }

            set
            {
                SortedParameters.Add(new NameValuePair { Name = OAuthConstants.OAuthTimestamp, Value = value });
                parameters.Add(OAuthConstants.OAuthTimestamp, value);
            }
        }

        /// <summary>
        ///     Gets or sets the nonce.
        /// </summary>
        /// <remarks>
        ///     The nonce value as defined in Section 3.3.  The parameter MAY be omitted when using the "PLAINTEXT" signature
        ///     method.
        /// </remarks>
        /// <value>
        ///     The nonce.
        /// </value>
        public string Nonce
        {
            get
            {
                var nonce = parameters[OAuthConstants.OAuthNonce] as string;
                return nonce ?? string.Empty;
            }

            set
            {
                SortedParameters.Add(new NameValuePair { Name = OAuthConstants.OAuthNonce, Value = value });
                parameters.Add(OAuthConstants.OAuthNonce, value);
            }
        }

        /// <summary>
        ///     Gets or sets the callback URI.
        /// </summary>
        /// <remarks>
        ///     An absolute URI back to which the server will redirect the resource owner when the Resource Owner Authorization
        ///     step (Section 2.2) is completed. If the client is unable to receive callbacks or a callback URI has been
        ///     established via other means, the parameter value MUST be set to "oob" (case sensitive), to indicate an out-of-band
        ///     configuration.
        /// </remarks>
        /// <value>
        ///     The callback URI.
        /// </value>
        [SuppressMessage("Microsoft.Design", "CA1056:UriPropertiesShouldNotBeStrings",
            Justification = "The values used from these parameters must be typed as string.")]
        public string CallbackUri
        {
            get
            {
                var callbackUri = parameters[OAuthConstants.OAuthCallback] as string;
                return callbackUri;
            }

            set
            {
                if (OAuthConstants.OutOfBand != value)
                {
                    Uri uri;
                    if (!Uri.TryCreate(value, UriKind.Absolute, out uri))
                    {
                        var message = string.Format(CultureInfo.InvariantCulture, "{0} is not a valid Uri", value);
                        throw new ArgumentOutOfRangeException("value", message);
                    }
                }

                SortedParameters.Add(new NameValuePair { Name = OAuthConstants.OAuthCallback, Value = value });
                parameters.Add(OAuthConstants.OAuthCallback, value);
            }
        }

        /// <summary>
        ///     Gets or sets the signature.
        /// </summary>
        /// <remarks>
        ///     The client declares which signature method is used via the "oauth_signature_method" parameter. It then generates a
        ///     signature (or a string of an equivalent value) and includes it in the "oauth_signature" parameter. The server
        ///     verifies the signature as specified for each method. The signature process does not change the request or its
        ///     parameters, with the exception of the "oauth_signature" parameter.
        /// </remarks>
        /// <value>
        ///     The signature.
        /// </value>
        public string Signature
        {
            get
            {
                var signature = parameters[OAuthConstants.OAuthSignature] as string;
                return signature ?? string.Empty;
            }

            set
            {
                SortedParameters.Add(new NameValuePair { Name = OAuthConstants.OAuthSignature, Value = value });
                parameters.Add(OAuthConstants.OAuthSignature, value);
            }
        }

        /// <summary>
        ///     Gets or sets the token.
        /// </summary>
        /// <value>
        ///     The token.
        /// </value>
        public string Token
        {
            get
            {
                var token = parameters[OAuthConstants.OAuthToken] as string;
                return token ?? string.Empty;
            }

            set
            {
                SortedParameters.Add(new NameValuePair { Name = OAuthConstants.OAuthToken, Value = value });
                parameters.Add(OAuthConstants.OAuthToken, value);
            }
        }

        /// <summary>
        ///     Gets or sets the version.
        /// </summary>
        /// <remarks>
        ///     OPTIONAL.  If present, MUST be set to "1.0".  Provides the version of the authentication process as defined in this
        ///     specification.
        /// </remarks>
        /// <value>
        ///     The version.
        /// </value>
        public string Version
        {
            get
            {
                var version = parameters[OAuthConstants.OAuthVersion] as string;
                return version ?? string.Empty;
            }

            set
            {
                SortedParameters.Add(new NameValuePair { Name = OAuthConstants.OAuthVersion, Value = value });
                parameters.Add(OAuthConstants.OAuthVersion, value);
            }
        }

        /// <summary>
        ///     Gets the token store unique identifier.
        /// </summary>
        /// <value>
        ///     The token store unique identifier.
        /// </value>
        public string TokenStoreId
        {
            get
            {
                return string.Format(CultureInfo.InvariantCulture, "{0}|{1}|{2}", Timestamp, Nonce, Token);
            }
        }

        /// <summary>
        /// Combines all the properties into a readable string.
        /// </summary>
        /// <returns>All the values</returns>
        public string ToKeyValuesString()
        {
            return "ConsumerKey:" + ConsumerKey + Environment.NewLine
                + "SignatureMethod:" + SignatureMethod + Environment.NewLine
                + "Timestamp:" + Timestamp + Environment.NewLine
                + "Nonce:" + Nonce + Environment.NewLine
                + "CallbackUri:" + CallbackUri + Environment.NewLine
                + "Signature:" + Signature + Environment.NewLine
                + "Token:" + Token + Environment.NewLine
                + "Version:" + Version + Environment.NewLine;
        }
    }
}
