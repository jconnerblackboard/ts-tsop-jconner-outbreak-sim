﻿namespace BbTS.Domain.Models.System.Security.Oauth
{
    public class KeySecretPair
    {
        public string Key { get; set; }
        public string Secret { get; set; }

        public KeySecretPair Clone()
        {
            return (KeySecretPair)MemberwiseClone();
        }

        public KeySecretPair()
        {
            Key = string.Empty;
            Secret = string.Empty;
        }

        public KeySecretPair(string key, string secret)
        {
            Key = key;
            Secret = secret;
        }
    }
}
