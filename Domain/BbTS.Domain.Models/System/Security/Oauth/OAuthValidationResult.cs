﻿namespace BbTS.Domain.Models.System.Security.Oauth
{
    /// <summary>
    ///     Defines information that describes the result of validation of an OAuth API request
    /// </summary>
    public class OAuthValidationResult
    {
        /// <summary>
        /// Gets or sets a value indicating whether OAuth parameters are valid.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is valid]; otherwise, <c>false</c>.
        /// </value>
        public bool IsValid { get; set; }

        /// <summary>
        /// Gets or sets the authentication parameters.
        /// </summary>
        /// <value>
        /// The authentication parameters.
        /// </value>
        public OAuthParameters OAuthParameters { get; set; }

        /// <summary>
        /// Gets or sets the security token.
        /// </summary>
        /// <value>
        /// The security token.
        /// </value>
        public OAuth1ASecurityToken SecurityToken { get; set; }
    }
}
