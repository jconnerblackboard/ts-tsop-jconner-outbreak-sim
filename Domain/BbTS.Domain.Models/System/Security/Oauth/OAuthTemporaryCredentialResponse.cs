﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using BbTS.Core;
using BbTS.Domain.Models.Definitions.Security.Oauth;

namespace BbTS.Domain.Models.System.Security.Oauth
{
    /// <summary>
    ///     Service model that represents the OAuth Temporary Credentials
    /// </summary>
    [DataContract]
    public class OAuthTemporaryCredentialResponse
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="OAuthTemporaryCredentialResponse" /> class.
        /// </summary>
        public OAuthTemporaryCredentialResponse()
        {
            Token = Guid.NewGuid().ToString();
            TokenSecret = RandomDataGenerator.GenerateRandomString(20);
            CallbackConfirmed = "true"; // Default to true for 'oob'
        }

        /// <summary>
        ///     Gets or sets the token.
        /// </summary>
        /// <value>
        ///     The token.
        /// </value>
        [DataMember(Name = OAuthConstants.OAuthToken)]
        public string Token { get; set; }

        /// <summary>
        ///     Gets or sets the token secret.
        /// </summary>
        /// <value>
        ///     The token secret.
        /// </value>
        [DataMember(Name = OAuthConstants.OAuthTokenSecret)]
        public string TokenSecret { get; set; }

        /// <summary>
        ///     Gets or sets the callback confirmed.
        /// </summary>
        /// <value>
        ///     The callback confirmed.
        /// </value>
        [DataMember(Name = OAuthConstants.OAuthCallbackConfirmed)]
        public string CallbackConfirmed { get; set; }
    }
}
