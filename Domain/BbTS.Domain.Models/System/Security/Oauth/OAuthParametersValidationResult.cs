﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.System.Security.Oauth
{
    /// <summary>
    ///     Defines information that describes the result of validation of OAuth parameters
    /// </summary>
    public class OAuthParametersValidationResult
    {
        /// <summary>
        ///     Gets or sets the HTTP status code.
        /// </summary>
        /// <value>
        ///     The HTTP status code.
        /// </value>
        public HttpStatusCode HttpStatusCode { get; set; }

        /// <summary>
        ///     Gets or sets the response description.
        /// </summary>
        /// <value>
        ///     The response description.
        /// </value>
        public string ResponseDescription { get; set; }
    }
}
