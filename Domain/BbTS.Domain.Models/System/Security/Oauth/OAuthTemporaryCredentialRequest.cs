﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Definitions.Security.Oauth;

namespace BbTS.Domain.Models.System.Security.Oauth
{
    /// <summary>
    ///     This class represents the OAuth parameters that are submitted on a request for the temporary credentials<br />
    ///     Please see
    ///     <a href="http://tools.ietf.org/html/rfc5849">
    ///         RFC 5849 - The OAuth 1.0 Protocol
    ///     </a>
    ///     for more information
    /// </summary>
    public class OAuthTemporaryCredentialRequest
    {
    }
}
