﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.System.Security.Oauth
{
    /// <summary>
    /// Fake response. This class is used internally to support workflows where the API request is not flowing through Service Stack.
    /// </summary>
    public class ServiceStackResponseSurrogate
    {
        /// <summary>
        /// Gets or sets the status code.
        /// </summary>
        /// <value>
        /// The status code.
        /// </value>
        public int StatusCode
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the status description.
        /// </summary>
        /// <value>
        /// The status description.
        /// </value>
        public string StatusDescription
        {
            get;
            set;
        }

        /// <summary>
        /// Calls Response.End() on ASP.NET HttpResponse otherwise is an alias for Close().
        /// Useful when you want to prevent ASP.NET to provide it's own custom error page.
        /// </summary>
        public void End()
        {
            // This method does nothing for this fake response object
        }
    }
}
