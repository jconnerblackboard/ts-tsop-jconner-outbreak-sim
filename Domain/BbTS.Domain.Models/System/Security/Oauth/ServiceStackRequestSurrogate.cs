﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.System.Security.Oauth
{
    /// <summary>
    /// Container class to mock up values contained in a service stack request.
    /// </summary>
    public class ServiceStackRequestSurrogate
    {
        /// <summary>
        /// The absolute URI of the request.
        /// </summary>
        public string AbsoluteUri { get; set; }

        /// <summary>
        /// The Verb of the request <example>PUT, POST, PATCH, DELETE, GET</example>
        /// </summary>
        public string Verb { get; set; }

        /// <summary>
        /// This value should be set to the route template of the request.
        /// </summary>
        public string PathInfo { get; set; }

        /// <summary>
        /// The content type of the request.
        /// </summary>
        public string ContentType { get; set; }

        /// <summary>
        /// The query string of the request.
        /// </summary>
        public NameValueCollection QueryString { get; set; }

        /// <summary>
        /// The form data in the request.
        /// </summary>
        public NameValueCollection FormData { get; set; }
    }
}
