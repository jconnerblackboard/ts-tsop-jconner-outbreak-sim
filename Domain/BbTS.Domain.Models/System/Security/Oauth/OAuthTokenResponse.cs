﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Definitions.Security.Oauth;

namespace BbTS.Domain.Models.System.Security.Oauth
{
    /// <summary>
    ///     Service model to capture output request information for OAuth Token Request
    /// </summary>
    [DataContract]
    public class OAuthTokenResponse
    {
        /// <summary>
        ///     Gets or sets the token.
        /// </summary>
        /// <remarks>
        ///     The token value used to associate the request with the resource owner.  If the request is not associated with a
        ///     resource owner (no token available), clients MAY omit the parameter.
        /// </remarks>
        /// <value>
        ///     The token.
        /// </value>
        [DataMember(Name = OAuthConstants.OAuthToken)]
        public string Token { get; set; }

        /// <summary>
        ///     Gets or sets the token secret.
        /// </summary>
        /// <value>
        ///     The token shared secret.
        /// </value>
        [DataMember(Name = OAuthConstants.OAuthTokenSecret)]
        public string TokenSecret { get; set; }
    }
}
