﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.System.Security.Oauth
{
    /// <summary>
    ///     Represents a security token and secret for OAuth 1a
    /// </summary>
    public class OAuth1ASecurityToken
    {
        /// <summary>
        ///     Gets or sets the token.
        /// </summary>
        /// <value>
        ///     The token.
        /// </value>
        public string Token { get; set; }

        /// <summary>
        ///     Gets or sets the secret.
        /// </summary>
        /// <value>
        ///     The secret.
        /// </value>
        public string Secret { get; set; }

        /// <summary>
        ///     Gets or sets the time automatic live.
        /// </summary>
        /// <value>
        ///     The time automatic live.
        /// </value>
        public TimeSpan TimeToLive { get; set; }
    }
}
