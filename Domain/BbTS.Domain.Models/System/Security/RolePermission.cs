﻿// ReSharper disable once CheckNamespace
// ReSharper disable InconsistentNaming
namespace BbTS.Domain.Models.MediaServer
{
    /// <summary>
    /// RolePermission object
    /// </summary>
    public class RolePermission
    {
        /// <summary>
        /// Name of the RoleResource
        /// </summary>
        public string RoleResourceName { get; set; }
        /// <summary>
        /// Name of the RolePermission
        /// </summary>
        public string RolePermissionName { get; set; }
        /// <summary>
        /// Id of the Merchant
        /// </summary>
        public int MerchantId { get; set; }
    }

    /// <summary>
    /// This is the list of permission items
    /// </summary>
    public enum RolePermssionId
    {
        /// <summary>
        /// An unknown role permission
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// View permission for the agent plugin service
        /// </summary>
        TransactWeb_AgentPluginMaintenance_View = 5,
        /// <summary>
        /// Edit permission for the agent plugin service
        /// </summary>
        TransactWeb_AgentPluginMaintenance_Edit = 6,
        /// <summary>
        /// The BlackboardEmvReconciliation - Edit
        /// </summary>
        TransactWeb_BlackboardEmvReconciliation_Edit = 11,
        /// <summary>
        /// The BlackboardEmvReconciliation - view
        /// </summary>
        TransactWeb_BlackboardEmvReconciliation_View = 12,
        /// <summary>
        /// The CreditCardConfiguration - Edit
        /// </summary>
        TransactWeb_CreditCardConfiguration_Edit = 21,
        /// <summary>
        /// The CreditCardConfiguration - view
        /// </summary>
        TransactWeb_CreditCardConfiguration_View = 22,
        /// <summary>
        /// The Customer_Ui - Edit
        /// </summary>
        TransactWeb_Customer_Ui_Edit = 31,
        /// <summary>
        /// The Customer_Ui - view
        /// </summary>
        TransactWeb_Customer_Ui_View = 32,
        /// <summary>
        /// The DeviceReportingRole - Edit
        /// </summary>
        TransactWeb_DeviceReportingRole_Edit = 41,
        /// <summary>
        /// The DeviceReportingRole - view
        /// </summary>
        TransactWeb_DeviceReportingRole_View = 42,
        /// <summary>
        /// The Door_Access_Builder_Schedule - Edit
        /// </summary>
        TransactWeb_Door_Access_Builder_Schedule_Edit = 51,
        /// <summary>
        /// The Door_Access_Builder_Schedule - view
        /// </summary>
        TransactWeb_Door_Access_Builder_Schedule_View = 52,
        /// <summary>
        /// The Door_Lockdown_Type - Edit
        /// </summary>
        TransactWeb_Door_Lockdown_Type_Edit = 61,
        /// <summary>
        /// The Door_Lockdown_Type - view
        /// </summary>
        TransactWeb_Door_Lockdown_Type_View = 62,
        /// <summary>
        /// The MediaServer_Ui - Edit
        /// </summary>
        TransactWeb_MediaServer_Ui_Edit = 71,
        /// <summary>
        /// The MediaServer_Ui - view
        /// </summary>
        TransactWeb_MediaServer_Ui_View = 72,
        /// <summary>
        /// The Persona_Commlog - Edit
        /// </summary>
        TransactWeb_Persona_Commlog_Edit = 81,
        /// <summary>
        /// The Persona_Commlog - view
        /// </summary>
        TransactWeb_Persona_Commlog_View = 82,
        /// <summary>
        /// The PointofService - Edit
        /// </summary>
        TransactWeb_PointofService_Edit = 91,
        /// <summary>
        /// The PointofService - view
        /// </summary>
        TransactWeb_PointofService_View = 92,
        /// <summary>
        /// The System_Configuration - Edit
        /// </summary>
        TransactWeb_System_Configuration_Edit = 101,
        /// <summary>
        /// The System_Configuration - view
        /// </summary>
        TransactWeb_System_Configuration_View = 102
    }
}