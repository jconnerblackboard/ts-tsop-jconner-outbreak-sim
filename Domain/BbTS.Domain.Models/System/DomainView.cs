﻿
namespace BbTS.Domain.Models.System
{
    /// <summary>
    /// Domain View
    /// </summary>
    public class DomainView : DomainViewObject
    {
        /// <summary>
        /// Type of domain
        /// </summary>
        public int DomainType { get; set; }

        /// <summary>
        /// Domain
        /// </summary>
        public string Domain { get; set; }
    }
}
