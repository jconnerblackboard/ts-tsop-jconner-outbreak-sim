﻿using System;

namespace BbTS.Domain.Models.System
{
    public class DomainViewObject : IEquatable<DomainViewObject>, IComparable<DomainViewObject>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        /// <summary>
        /// IEquatable override for equality
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(DomainViewObject other)
        {
            return other.Id == Id && other.Name == Name && other.Description == Description;
        }

        public int CompareTo(DomainViewObject other)
        {
            if (other.Id > Id) return -1;
            if (other.Id < Id) return 1;
            return 0;
        }
    }
}
