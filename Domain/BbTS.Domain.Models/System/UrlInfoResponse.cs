﻿
namespace BbTS.Domain.Models.System.Database
{
    /// <summary>
    /// 
    /// </summary>
    public class UrlInfoResponse
    {
        /// <summary>
        /// Management portal url
        /// </summary>
        public string ManagementPortalUrl { get; set; }

        /// <summary>
        /// Reporting system url
        /// </summary>
        public string ReportingSystemUrl { get; set; }
    }
}
