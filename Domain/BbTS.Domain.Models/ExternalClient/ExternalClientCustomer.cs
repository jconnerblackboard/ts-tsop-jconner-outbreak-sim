﻿using System;

namespace BbTS.Domain.Models.ExternalClient
{
    /// <summary>
    /// External client customer entity
    /// </summary>
    public class ExternalClientCustomer
    {
        /// <summary>
        /// External Client Id
        /// </summary>
        public int ExternalClientId { get; set; }

        /// <summary>
        /// Customer Id
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// The identifier of the customer for the specified external client
        /// </summary>
        public Guid CustomerClientGuid { get; set; }
    }
}
