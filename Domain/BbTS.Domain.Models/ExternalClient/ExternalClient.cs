﻿namespace BbTS.Domain.Models.ExternalClient
{
    public class ExternalClient
    {
        public int ExternalClientId { get; set; }
        public string Username { get; set; }
        public int ClientId { get; set; }
        public string FullName { get; set; }
        public byte[] PasswordHash { get; set; }
        public string PasswordHashSalt { get; set; }
        public int HashType { get; set; }
        public int IterationCount { get; set; }
    }
}
