﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for an event hub notification customer extension object.
    /// </summary>
    [DataContract]
    public class EventHubNotificationCustomerExtension : EventHubNotificationBase, IEquatable<EventHubNotificationCustomerExtension>
    {
        /// <summary>
        /// Gets or sets the extensions.
        /// </summary>
        /// <value>The extensions.</value>
        [JsonProperty("extensions")]
        public IDictionary<string, object> Extensions { get; set; } = new Dictionary<string, object>();

        /// <summary>
        /// Gets or sets the category
        /// </summary>
        [JsonProperty("category")]
        public string Category { get; set; }

        public bool Equals(EventHubNotificationCustomerExtension other)
        {
            if (other == null ||
                CustomerGuid != other.CustomerGuid &&
                CustomerNumber != other.CustomerNumber ||
                Extensions.Keys.Count != other.Extensions.Keys.Count ||
                !Extensions.Keys.OrderBy(k => k.ToString()).SequenceEqual(other.Extensions.Keys.OrderBy(m => m.ToString())))
            {
                return false;
            }

            return true;
        }
    }
}
