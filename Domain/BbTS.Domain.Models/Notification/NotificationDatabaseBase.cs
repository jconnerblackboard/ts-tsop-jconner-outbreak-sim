﻿using System;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Base object for a notification object originating in the database.  Common properties for all objects.
    /// </summary>
    public class NotificationDatabaseBase
    {
        /// <summary>
        /// The unique identifier for the customer.
        /// </summary>
        [XmlElement("CustomerGuid")]
        public string CustomerGuid { get; set; }

        /// <summary>
        /// The full 22 digit number for the customer.
        /// </summary>
        [XmlElement("CustomerNumber")]
        public string CustomerNumber { get; set; }
    }
}
