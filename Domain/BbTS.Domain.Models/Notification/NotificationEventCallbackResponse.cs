﻿using System;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for a notification event callback request.
    /// </summary>
    public class NotificationEventCallbackResponse
    {
        /// <summary>
        /// Unique identifier for the request.
        /// </summary>
        public Guid RequestId { get; set; }

        /// <summary>
        /// The result of the wait one call.
        /// </summary>
        public int? Status { get; set; }

        /// <summary>
        /// The response message from the wait one call.
        /// </summary>
        public string Message { get; set; }
    }
}
