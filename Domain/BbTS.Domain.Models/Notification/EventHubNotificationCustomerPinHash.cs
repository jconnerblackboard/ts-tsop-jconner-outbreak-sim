﻿using System;
using System.Runtime.Serialization;
using BbTS.Domain.Models.Definitions.Notification;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for the customer pin hash object to be pushed up to the event hub.
    /// </summary>
    [DataContract]
    public class EventHubNotificationCustomerPinHash : EventHubNotificationBase, IEquatable<EventHubNotificationCustomerPinHash>
    {
        /// <summary>
        /// The customer's PIN.
        /// </summary>
        [DataMember(Name = "pin")]
        public string Pin { get; set; }

        /// <summary>
        /// The algorithm used to hash the PIN.
        /// </summary>
        [DataMember(Name = "algorithmIdentifier")]
        public string AlgorithmIdentifier { get; set; }

        public bool Equals(EventHubNotificationCustomerPinHash other)
        {
            return other != null &&
                   CustomerGuid == other.CustomerGuid &&
                   CustomerNumber == other.CustomerNumber &&
                   Pin == other.Pin &&
                   AlgorithmIdentifier == other.AlgorithmIdentifier;
        }
    }
}
