﻿using System;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for a notification get request.
    /// </summary>
    public class NotificationGetRequest
    {
        /// <summary>
        /// Unique identifier for the request.
        /// </summary>
        public Guid RequestId { get; set; } = Guid.NewGuid();
    }
}
