﻿using System;
using System.Collections.Generic;
using BbTS.Domain.Models.Definitions.Notification;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for the response to a notification get request.
    /// </summary>
    public class NotificationGetResponse
    {
        /// <summary>
        /// Unique identifier for the request.
        /// </summary>
        public Guid RequestId { get; set; }

        /// <summary>
        /// List of notification received from the latest callback event.
        /// </summary>
        public List<NotificationObject> Notifications { get; set; } = new List<NotificationObject>();
    }
}
