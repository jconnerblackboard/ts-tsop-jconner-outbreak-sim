﻿using System;
using System.Runtime.Serialization;
using BbTS.Domain.Models.Definitions.Notification;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for the stored value object to be pushed up to the event hub.
    /// </summary>
    [DataContract]
    public class EventHubNotificationStoredValueTransaction : EventHubNotificationBase, IEquatable<EventHubNotificationStoredValueTransaction>
    {
        /// <summary>
        /// The amount of the transaction.
        /// </summary>
        [DataMember(Name = "amount")]
        public decimal Amount { get; set; }

        /// <summary>
        /// The date and time the transaction took place.
        /// </summary>
        [DataMember(Name = "dateTime")]
        public DateTimeOffset DateTime { get; set; }

        /// <summary>
        /// The name of the profit center the transaction took place in.
        /// </summary>
        [DataMember(Name = "profitCenterName")]
        public string ProfitCenterName { get; set; }

        /// <summary>
        /// The remaining customer balance after the transaction.
        /// </summary>
        [DataMember(Name = "balance")]
        public decimal Balance { get; set; }

        /// <summary>
        /// The numerical 22 digit card number.
        /// </summary>
        [DataMember(Name = "cardNumber")]
        public string CardNumber { get; set; }

        /// <summary>
        /// The numerical identifier for the account.
        /// </summary>
        [DataMember(Name = "svAccountNumber")]
        public int SvAccountNumber { get; set; }

        /// <summary>
        /// The type name of the account.  Defined in BbTSMain.
        /// </summary>
        [DataMember(Name = "svAccountTypeName")]
        public string SvAccountTypeName { get; set; }

        /// <summary>
        /// The name of the originator of the transaction.
        /// </summary>
        [DataMember(Name = "posName")]
        public string PosName { get; set; }

        /// <summary>
        /// The merchant where the transaction was performed.
        /// </summary>
        [DataMember(Name = "merchantName")]
        public string MerchantName { get; set; }

        /// <summary>
        /// The numerical identifier for the stored value transaction history.
        /// </summary>
        [DataMember(Name = "svAccountHistoryId")]
        public string SvAccountHistoryId { get; set; }

        /// <summary>
        /// The unique identifier for the transaction.
        /// </summary>
        [DataMember(Name = "transactionGuid")]
        public string TransactionGuid { get; set; }

        /// <summary>
        /// The type of transaction that was performed.
        /// </summary>
        [DataMember(Name = "transactionType")]
        public string TransactionType { get; set; }

        /// <summary>
        /// The status of the transaction.
        /// </summary>
        [DataMember(Name = "transactionStatus")]
        public string TransactionStatus { get; set; }

        /// <summary>
        /// The currency code associated with the transaction.
        /// </summary>
        [DataMember(Name = "currencyCode")]
        public string CurrencyCode { get; set; }

        /// <summary>
        /// The unique identifier for the Stored Value Account.
        /// </summary>
        [DataMember(Name = "svAccountTypeGuid")]
        public string SvAccountTypeGuid { get; set; }

        /// <summary>
        /// The unique identifier for the Pos.
        /// </summary>
        [DataMember(Name = "posGuid")]
        public string PosGuid { get; set; }

        /// <summary>
        /// The unique identifier for the Profit Center.
        /// </summary>
        [DataMember(Name = "profitCenterGuid")]
        public string ProfitCenterGuid { get; set; }

        /// <summary>
        /// The unique identifier for the Merchant.
        /// </summary>
        [DataMember(Name = "merchantGuid")]
        public string MerchantGuid { get; set; }

        /// <summary>
        /// Equality overload.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(EventHubNotificationStoredValueTransaction other)
        {
            return other != null &&
                   Amount == other.Amount &&
                   DateTime.Date.Equals(other.DateTime.Date) &&
                   Balance == other.Balance &&
                   ProfitCenterName == other.ProfitCenterName &&
                   CardNumber == other.CardNumber &&
                   SvAccountNumber == other.SvAccountNumber &&
                   SvAccountTypeName == other.SvAccountTypeName &&
                   PosName == other.PosName &&
                   MerchantName == other.MerchantName &&
                   SvAccountHistoryId == other.SvAccountHistoryId &&
                   TransactionGuid == other.TransactionGuid &&
                   TransactionType == other.TransactionType &&
                   TransactionStatus == other.TransactionStatus &&
                   CurrencyCode == other.CurrencyCode &&
                   CustomerGuid == other.CustomerGuid &&
                   CustomerNumber == other.CustomerNumber &&
                   SvAccountTypeGuid == other.SvAccountTypeGuid &&
                   PosGuid == other.PosGuid &&
                   ProfitCenterGuid == other.ProfitCenterGuid &&
                   MerchantGuid == other.MerchantGuid;
        }
    }
}
