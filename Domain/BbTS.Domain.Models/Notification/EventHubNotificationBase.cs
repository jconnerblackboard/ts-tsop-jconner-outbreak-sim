﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for common properties of a notification sub object.
    /// </summary>
    [DataContract]
    public class EventHubNotificationBase
    {
        /// <summary>
        /// The unique identifier for the customer.
        /// </summary>
        [DataMember(Name = "customerGuid")]
        public string CustomerGuid { get; set; }

        /// <summary>
        /// The full 22 digit number for the customer.
        /// </summary>
        [DataMember(Name = "customerNumber")]
        public string CustomerNumber { get; set; }

        /// <summary>
        /// List of external (client) IDs for the customer.
        /// </summary>
        [DataMember(Name = "externalIds")]
        public List<string> ExternalIds { get; set; }
    }
}
