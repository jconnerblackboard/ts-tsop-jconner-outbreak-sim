﻿using System.Xml.Serialization;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class used to deserialize xml information coming from the database that relates to a notificaiton stored value entity.
    /// </summary>
    [XmlRoot("StoredValueTransaction")]
    public class NotificationStoredValueTransactionDatabase : NotificationDatabaseBase
    {
        /// <summary>
        /// The amount of the transaction.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// The date and time the transaction took place.
        /// </summary>
        public string DateTime { get; set; }

        /// <summary>
        /// The name of the profit center the transaction took place in.
        /// </summary>
        public string ProfitCenterName { get; set; }

        /// <summary>
        /// The remaining customer balance after the transaction.
        /// </summary>
        public decimal Balance { get; set; }

        /// <summary>
        /// The numerical 22 digit card number.
        /// </summary>
        public string CardNumber { get; set; }

        /// <summary>
        /// The numerical identifier for the account.
        /// </summary>
        public int SvAccountNumber { get; set; }

        /// <summary>
        /// The type name of the account.  Defined in BbTSMain.
        /// </summary>
        public string SvAccountTypeName { get; set; }

        /// <summary>
        /// The name of the originator of the transaction.
        /// </summary>
        public string PosName { get; set; }

        /// <summary>
        /// The merchant where the transaction was performed.
        /// </summary>
        public string MerchantName { get; set; }

        /// <summary>
        /// The numerical identifier for the stored value transaction history.
        /// </summary>
        public string SvAccountHistoryId { get; set; }

        /// <summary>
        /// The unique identifier for the transaction.
        /// </summary>
        public string TransactionGuid { get; set; }

        /// <summary>
        /// The type of transaction that was performed.
        /// </summary>
        public string TransactionType { get; set; }

        /// <summary>
        /// The status of the transaction.
        /// </summary>
        public string TransactionStatus { get; set; }

        /// <summary>
        /// The currency code associated with the transaction.
        /// </summary>
        public string CurrencyCode { get; set; }

        /// <summary>
        /// The unique identifier for the Stored Value Account.
        /// </summary>
        [XmlElement("SVAccountTypeGuid")]
        public string SvAccountTypeGuid { get; set; }

        /// <summary>
        /// The unique identifier for the Pos.
        /// </summary>
        public string PosGuid { get; set; }

        /// <summary>
        /// The unique identifier for the Profit Center.
        /// </summary>
        public string ProfitCenterGuid { get; set; }

        /// <summary>
        /// The unique identifier for the Merchant.
        /// </summary>
        public string MerchantGuid { get; set; }

        /// <summary>
        /// The date and time in julian format.
        /// </summary>
        public string JulianDateTime { get; set; }
    }
}
