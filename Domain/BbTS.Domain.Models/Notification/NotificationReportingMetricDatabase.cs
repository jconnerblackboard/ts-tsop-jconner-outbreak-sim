﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for a notificaiton metric.  Related to event hub, azure table storage, and other like products.
    /// </summary>
    [XmlType("Metric")]
    public class NotificationReportingMetricDatabase
    {
        /// <summary>
        /// Unique identification enumeration for the metric.
        /// </summary>
        [XmlAttribute("DataKey")]
        public string DataKey { get; set; }

        /// <summary>
        /// The currency type of the transaction.
        /// </summary>
        [XmlAttribute("Currency")]
        public string Currency { get; set; }

        /// <summary>
        /// The count of transactions that occured of this metric type.
        /// </summary>
        [XmlAttribute("TransactionCount")]
        public int TransactionCount { get; set; }

        /// <summary>
        /// The value of the transaction.
        /// </summary>
        [XmlAttribute("TransactionValue")]
        public decimal TransactionValue { get; set; }

        /// <summary>
        /// The user count (if this metric is related to users).
        /// </summary>
        [XmlAttribute("UserCount")]
        public int UserCount { get; set; }

        /// <summary>
        /// The DPAN count.
        /// </summary>
        [XmlAttribute("DPANCount")]
        public int DpanCount { get; set; }

        /// <summary>
        /// The device count (if this metric is related to devices).
        /// </summary>
        [XmlAttribute("DeviceCount")]
        public int DeviceCount { get; set; }

        /// <summary>
        /// The frequency of this reporting metrics row.
        /// </summary>
        [XmlAttribute("Frequency")]
        public int Frequency { get; set; }
    }

    /// <summary>
    /// Container class for a set of notification metrics.
    /// </summary>
    [XmlType("ReportingMetrics")]
    public class NotificationDatabaseReportingMetrics : NotificationDatabaseBase
    {
        /// <summary>
        /// Date the related object of the metric occured
        /// </summary>
        [XmlAttribute("Date")]
        public string Date { get; set; }

        /// <summary>
        /// List of metrics.
        /// </summary>
        [XmlArray("Metrics"), XmlArrayItem("Metric")]
        public List<NotificationReportingMetricDatabase> Metrics { get; set; } = new List<NotificationReportingMetricDatabase>();
    }
}
