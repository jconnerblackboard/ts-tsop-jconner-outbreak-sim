﻿using System;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for the information about a customer to be included in a customer notification.
    /// </summary>
    [XmlRoot("Customer")]
    public class NotificationCustomerDatabase : NotificationDatabaseBase
    {
        /// <summary>
        /// The first name of the customer
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// The middle name of the customer
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// The last name of the customer
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Whether the customer is active or not
        /// </summary>
        public string IsActive { get; set; }

        /// <summary>
        /// The first date the customer account is considered active
        /// </summary>
        public string StartDate { get; set; }

        /// <summary>
        /// The last date the customer account is considered active
        /// </summary>
        public string EndDate { get; set; }

        /// <summary>
        /// The Mobile Id Card Type
        /// </summary>
        public string MobileIdCardType { get; set; }

        /// <summary>
        /// The Mobile Id IIN Pool
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public string MobileIdIINPool { get; set; }

        /// <summary>
        /// Mobile id procurement allowed.
        /// </summary>
        public string MobileIdProcurementAllowed { get; set; }

        /// <summary>
        /// Mobile id reference text.
        /// </summary>
        public string MobileIdRefText { get; set; }

        /// <summary>
        /// Mobile id is free.
        /// </summary>
        public string MobileIdIsFree { get; set; }

        /// <summary>
        /// Mobile id price.
        /// </summary>
        public string MobileIdPrice { get; set; }

        /// <summary>
        /// Mobile id expire date.
        /// </summary>
        public string MobileIdExpireDate { get; set; }

        /// <summary>
        /// CDF Group / Items [Student|Employee|Other] / Default [None]
        /// </summary>
        public string MobilePersonType { get; set; }

        /// <summary>
        /// CDF Group / Items [Undergraduate|Graduate] / Default [None]
        /// </summary>
        public string MobileStudentClass { get; set; }

        /// <summary>
        /// CDF Group / Items[On - Campus | Off - Campus] / Default [None]
        /// </summary>
        public string MobileStudentResidence { get; set; }

        /// <summary>
        /// CDF String / Length 30 / Default Empty
        /// </summary>
        public string MobileOrgLabel { get; set; }

        /// <summary>
        /// CDF String / Length 30 / Default Empty
        /// </summary>
        public string MobileRole { get; set; }

        /// <summary>
        /// CDF String / Length 30 / Default Empty 
        /// </summary>
        public string MobileOrg { get; set; }
    }
}
