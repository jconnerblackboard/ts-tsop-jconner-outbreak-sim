﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for a notification customer extension database object.
    /// </summary>
    [Serializable]
    [XmlRoot("CustomerExtensions")]
    public class NotificationCustomerExtensionDatabase : NotificationDatabaseBase
    {
        /// <summary>
        /// The category.
        /// </summary>
        [XmlElement("Category")]
        public string Category { get; set; }

        /// <summary>
        /// The list of customer extensions.
        /// </summary>
        [XmlArrayItem(ElementName = "Extension")]
        public List<Extension> Extensions { get; set; } = new List<Extension>();
    }

    /// <summary>
    /// Container class for a single customer extension object (CDF).
    /// </summary>
    [XmlRoot("Extension")]
    public class Extension
    {
        /// <summary>
        /// The name part of the pair.
        /// </summary>
        [XmlAttribute]
        public string Name { get; set; }

        /// <summary>
        /// The value part of the pair.
        /// </summary>
        [XmlAttribute]
        public string Value { get; set; }

        /// <summary>
        /// The data type
        /// </summary>
        [XmlAttribute]
        public string Type { get; set; }
    }
}
