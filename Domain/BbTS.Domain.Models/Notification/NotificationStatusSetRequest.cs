﻿using System;
using BbTS.Domain.Models.Definitions.Notification;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for a notificaiton status set request.
    /// </summary>
    public class NotificationStatusSetRequest
    {
        /// <summary>
        /// Unique identifier for the request.
        /// </summary>
        public Guid RequestId { get; set; } = Guid.NewGuid();

        /// <summary>
        /// The numerical identifier for the notification.
        /// </summary>
        public int NotificationId { get; set; }

        /// <summary>
        /// The state of the notification processing event.
        /// </summary>
        public NotificationStatusType StatusType { get; set; }
    }
}
