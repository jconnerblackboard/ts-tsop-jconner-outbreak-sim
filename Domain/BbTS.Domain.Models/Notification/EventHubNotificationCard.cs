﻿using System;
using System.Runtime.Serialization;
using BbTS.Domain.Models.Definitions.Card;
using BbTS.Domain.Models.Definitions.Notification;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for the card object to be pushed up to the event hub.
    /// </summary>
    [DataContract]
    public class EventHubNotificationCard : EventHubNotificationBase, IEquatable<EventHubNotificationCard>
    {
        /// <summary>
        /// The unique identifier for the card.
        /// </summary>
        [DataMember(Name = "cardNumber")]
        public string CardNumber { get; set; }

        /// <summary>
        /// The status of the card.
        /// </summary>
        [DataMember(Name = "status")]
        public string Status { get; set; }

        /// <summary>
        /// Is the card a primary card.
        /// </summary>
        [DataMember(Name = "isPrimary")]
        public bool IsPrimary { get; set; }

        /// <summary>
        /// The type of card.
        /// </summary>
        [DataMember(Name = "type")]
        public string Type { get; set; }

        /// <summary>
        /// Comments about the card.
        /// </summary>
        [DataMember(Name = "comment")]
        public string Comment { get; set; }

        /// <summary>
        /// The active start date of the card.
        /// </summary>
        [DataMember(Name = "activeStartDate")]
        public DateTimeOffset ActiveStartDate { get; set; } = DateTimeOffset.MinValue;

        /// <summary>
        /// The active end date of the card.
        /// </summary>
        [DataMember(Name = "activeEndDate")]
        public DateTimeOffset ActiveEndDate { get; set; } = DateTimeOffset.MaxValue;

        /// <summary>
        /// The unique identifier for the card.
        /// </summary>
        [DataMember(Name = "cardGuid")]
        public string CardGuid { get; set; }

        /// <summary>
        /// The name of the card.
        /// </summary>
        [DataMember(Name = "cardName")]
        public string CardName { get; set; }

        /// <summary>
        /// The card serial number.
        /// </summary>
        [DataMember(Name = "cardSerialNumber")]
        public string CardSerialNumber { get; set; }

        /// <summary>
        /// Override
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(EventHubNotificationCard other)
        {
            return other != null &&
                   CustomerGuid == other.CustomerGuid &&
                   CustomerNumber == other.CustomerNumber &&
                   CardNumber == other.CardNumber &&
                   CardGuid == other.CardGuid &&
                   CardName == other.CardName &&
                   CardSerialNumber == other.CardSerialNumber &&
                   ActiveEndDate == other.ActiveEndDate &&
                   ActiveStartDate == other.ActiveStartDate &&
                   Type == other.Type &&
                   IsPrimary == other.IsPrimary &&
                   Status == other.Status &&
                   Comment == other.Comment;

        }
    }
}
