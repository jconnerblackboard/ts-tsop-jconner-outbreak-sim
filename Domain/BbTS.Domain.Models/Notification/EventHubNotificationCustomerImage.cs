﻿using System;
using System.Runtime.Serialization;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for the customer image (photo and thumbnail) object to be pushed up to the event hub.
    /// </summary>
    [DataContract]
    public class EventHubNotificationCustomerImage : EventHubNotificationBase, IEquatable<EventHubNotificationCustomerImage>
    {
        /// <summary>
        /// The identifier for the image.
        /// </summary>
        [DataMember(Name = "imageId")]
        public string ImageId { get; set; }

        public bool Equals(EventHubNotificationCustomerImage other)
        {
            return other != null &&
                   CustomerNumber == other.CustomerNumber &&
                   CustomerGuid == other.CustomerGuid &&
                   ImageId == other.ImageId;
        }
    }
}
