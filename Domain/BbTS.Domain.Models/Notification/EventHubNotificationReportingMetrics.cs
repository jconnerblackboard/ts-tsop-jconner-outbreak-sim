﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for the card object to be pushed up to the event hub.
    /// </summary>
    [DataContract]
    public class EventHubNotificationReportingMetrics : EventHubNotificationBase, IEquatable<EventHubNotificationReportingMetrics>
    {
        /// <summary>
        /// List of event hub notificaiton metrics.
        /// </summary>
        [DataMember(Name = "metrics")]
        public List<EventHubNotificationReportingMetric> Metrics { get; set; } = new List<EventHubNotificationReportingMetric>();

        public bool Equals(EventHubNotificationReportingMetrics other)
        {
            var equal = other != null && Metrics.OrderBy(m => m.DataKey).SequenceEqual(other.Metrics.OrderBy(m => m.DataKey));
            return equal;
        }
    }

    /// <summary>
    /// Container class for a single event hub notification object.
    /// </summary>
    public class EventHubNotificationReportingMetric : IEquatable<EventHubNotificationReportingMetric>
    {
        /// <summary>
        /// Date the related object of the metric occured
        /// </summary>
        [DataMember(Name = "date")]
        public DateTime Date { get; set; }

        /// <summary>
        /// Unique identification enumeration for the metric.
        /// </summary>
        [DataMember(Name = "dataKey")]
        public string DataKey { get; set; }

        /// <summary>
        /// The currency type of the transaction.
        /// </summary>
        [DataMember(Name = "currency")]
        public string Currency { get; set; }

        /// <summary>
        /// The count of transactions that occured of this metric type.
        /// </summary>
        [DataMember(Name = "transactionCount")]
        public int TransactionCount { get; set; }

        /// <summary>
        /// The value of the transaction.
        /// </summary>
        [DataMember(Name = "transactionValue")]
        public decimal TransactionValue { get; set; }

        /// <summary>
        /// The user count (if this metric is related to users).
        /// </summary>
        [DataMember(Name = "userCount")]
        public int UserCount { get; set; }

        /// <summary>
        /// The DPAN count.
        /// </summary>
        [DataMember(Name = "dpanCount")]
        public int DpanCount { get; set; }

        /// <summary>
        /// The device count (if this metric is related to devices).
        /// </summary>
        [DataMember(Name = "deviceCount")]
        public int DeviceCount { get; set; }

        /// <summary>
        /// The frequency of this reporting metrics row.
        /// </summary>
        [DataMember(Name = "frequency")]
        public string Frequency { get; set; }

        public bool Equals(EventHubNotificationReportingMetric other)
        {
            return other != null &&
                   Date.Equals(other.Date) &&
                   DataKey == other.DataKey &&
                   Currency == other.Currency &&
                   TransactionCount == other.TransactionCount &&
                   TransactionValue == other.TransactionValue &&
                   UserCount == other.UserCount &&
                   DpanCount == other.DpanCount &&
                   DeviceCount == other.DeviceCount &&
                   Frequency == other.Frequency;
        }
    }
}
