﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for the information about a set of customer images.
    /// </summary>
    [XmlRoot("CustomerImage")]
    public class NotificationCustomerImageDatabase : NotificationDatabaseBase
    {
        /// <summary>
        /// The identifier for the image.
        /// </summary>
        public string Image { get; set; }
    }
}
