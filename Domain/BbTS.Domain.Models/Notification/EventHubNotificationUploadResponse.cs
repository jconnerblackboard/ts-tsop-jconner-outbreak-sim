﻿using System;
using BbTS.Domain.Models.Definitions.Notification;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for the response to an event hub notification upload request.
    /// </summary>
    public class EventHubNotificationUploadResponse
    {
        /// <summary>
        /// Unique identifier for the request.
        /// </summary>
        public Guid RequestId { get; set; }

        /// <summary>
        /// The result of the event hub notification upload operation.
        /// </summary>
        public EventHubNotificationUploadResult Result { get; set; }

        /// <summary>
        /// User friendly message containing an explanation of the results of the upload operation.
        /// </summary>
        public string Message { get; set; }
    }
}
