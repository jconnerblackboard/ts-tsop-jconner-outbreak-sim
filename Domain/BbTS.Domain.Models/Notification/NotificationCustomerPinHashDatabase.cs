﻿using System.Xml.Serialization;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for a notification pin.
    /// </summary>
    [XmlRoot("CustomerPinHash")]
    public class NotificationCustomerPinHashDatabase : NotificationDatabaseBase
    {
        /// <summary>
        /// The customer's PIN.
        /// </summary>
        public string Pin { get; set; }

        /// <summary>
        /// The algorithm used to hash the PIN.
        /// </summary>
        public string AlgorithmIdentifier { get; set; }
    }
}
