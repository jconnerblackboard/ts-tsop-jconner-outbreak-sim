﻿using System;
using System.Runtime.Serialization;
using BbTS.Domain.Models.Definitions.Notification;


namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for an event hub object.
    /// </summary>
    [DataContract]
    [Serializable]
    public class EventHubObject<T>
    {
        /// <summary>
        /// The date and time of the event.
        /// </summary>
        [DataMember(Name = "eventDateTime")]
        public DateTimeOffset EventDateTime { get; set; }

        /// <summary>
        /// The type of event.
        /// </summary>
        [DataMember(Name = "eventType")]
        public string EventType { get; set; }

        /// <summary>
        /// The unique identifier for the institution.
        /// </summary>
        [DataMember(Name = "institutionId")]
        public Guid InstitutionId { get; set; }

        /// <summary>
        /// The name of the product.
        /// </summary>
        [DataMember(Name = "productName")]
        public string ProductName { get; set; } = "TransactionSystem";

        /// <summary>
        /// The name of the service.
        /// </summary>
        [DataMember(Name = "serviceName")]
        public string ServiceName { get; set; } = "TransactionSystem";

        /// <summary>
        /// The payload object to upload.
        /// </summary>
        [DataMember(Name = "resource")]
        public T Resource { get; set; }

        /// <summary>
        /// The payload object to upload.
        /// </summary>
        [DataMember(Name = "resourceType")]
        public string ResourceType { get; set; }

        /// <summary>
        /// The version of the payload object.
        /// </summary>
        [DataMember(Name = "resourceVersion")]
        public string ResourceVersion
        {
            get
            {
                NotificationObjectType type;
                return Enum.TryParse(ResourceType, out type) ? NotificationDefinitions.NotificationObjectTypeVersionGet(type) : "v1.0";
            }
        }

        /// <summary>
        /// The unique identifier for the Object.
        /// </summary>
        public Guid ObjectGuid { get; set; }

    }
}
