﻿using System;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for an event hub customer image upload request.
    /// </summary>
    public class EventHubCustomerImageUploadRequest<T>
    {
        /// <summary>
        /// Unique identifier for the request.
        /// </summary>
        public Guid RequestId { get; set; } = Guid.NewGuid();

        /// <summary>
        /// The object containing the necessary information to upload.
        /// </summary>
        public EventHubObject<T> EventHubObject { get; set; }

    }
}
