﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class containing information about a notificaiton card object.
    /// </summary>
    [Serializable]
    [XmlRoot("Card")]
    public class NotificationCardDatabase : NotificationDatabaseBase
    {
        /// <summary>
        /// The unique identifier for the card.
        /// </summary>
        [XmlElement("CardNumber")]
        public string CardNumber { get; set; }

        /// <summary>
        /// The status of the card.
        /// </summary>
        [XmlElement("Status")]
        public int Status { get; set; }

        /// <summary>
        /// Is the card a primary card.
        /// </summary>
        [XmlElement("IsPrimary")]
        public int IsPrimary { get; set; }

        /// <summary>
        /// The type of card.
        /// </summary>
        [XmlElement("Type")]
        public int Type { get; set; }

        /// <summary>
        /// Comments about the card.
        /// </summary>
        [XmlElement("Comment")]
        public string Comment { get; set; }

        /// <summary>
        /// The active start date of the card.
        /// </summary>
        [XmlElement("ActiveStartDate")]
        public string ActiveStartDate { get; set; }

        /// <summary>
        /// The active end date of the card.
        /// </summary>
        [XmlElement("ActiveEndDate")]
        public string ActiveEndDate { get; set; }

        /// <summary>
        /// The unique identifier for the card.
        /// </summary>
        [XmlElement("CardGuid")]
        public string CardGuid { get; set; }

        /// <summary>
        /// The name of the card.
        /// </summary>
        [XmlElement("CardName")]
        public string CardName { get; set; }

        /// <summary>
        /// The card serial number.
        /// </summary>
        [XmlElement("CardSerialNumber")]
        public string CardSerialNumber { get; set; }
    }
}
