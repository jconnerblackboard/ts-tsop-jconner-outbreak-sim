using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for a notification heart beat.
    /// </summary>
    [XmlRoot("HeartBeat")]
    public class NotificationHeartBeatDatabase
    {
        /// <summary>
        /// Messages left unprocessed in the queue.
        /// </summary>
        [XmlElement("QueueDepth")]
        public int QueueDepth { get; set; }

        /// <summary>
        /// Earliest unprocessed message in the queue.
        /// </summary>
        [XmlElement("RecordDateTimeMinimum")]
        public string RecordDateTimeMinimum { get; set; }

        /// <summary>
        /// The latest unprocessed message in the queue.
        /// </summary>
        [XmlElement("RecordDateTimeMaximum")]
        public string RecordDateTimeMaximum { get; set; }
    }

    /// <summary>
    /// Container class for a heart beat notification to be sent to the event hub.
    /// </summary>
    [DataContract]
    public class EventHubNotificationHeartBeat : EventHubNotificationBase, IEquatable<EventHubNotificationHeartBeat>
    {
        /// <summary>
        /// Messages left unprocessed in the queue.
        /// </summary>
        [DataMember(Name = "queueDepth" )]
        public int QueueDepth { get; set; }

        /// <summary>
        /// Earliest unprocessed message in the queue.
        /// </summary>
        [DataMember(Name = "recordDateTimeMinimum")]
        public DateTimeOffset RecordDateTimeMinimum { get; set; }

        /// <summary>
        /// The latest unprocessed message in the queue.
        /// </summary>
        [DataMember(Name = "recordDateTimeMaximum")]
        public DateTimeOffset RecordDateTimeMaximum { get; set; }

        public bool Equals(EventHubNotificationHeartBeat other)
        {
            return other != null &&
                   QueueDepth == other.QueueDepth &&
                   RecordDateTimeMinimum.Equals(other.RecordDateTimeMinimum) &&
                   RecordDateTimeMaximum.Equals(other.RecordDateTimeMaximum);
        }
    }
}
