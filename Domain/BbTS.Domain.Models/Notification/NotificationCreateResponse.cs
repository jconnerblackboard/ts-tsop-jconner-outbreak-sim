﻿using System;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for a notification event callback request.
    /// </summary>
    public class NotificationCreateResponse
    {
        /// <summary>
        /// Unique identifier for the request.
        /// </summary>
        public Guid RequestId { get; set; }
    }
}
