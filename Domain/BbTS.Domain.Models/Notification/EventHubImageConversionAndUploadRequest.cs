﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for a request to convert an image to the proper upload format.
    /// </summary>
    public class EventHubImageConversionAndUploadRequest
    {
        /// <summary>
        /// Unique identifier for the request.
        /// </summary>
        public Guid RequestId { get; set; } = Guid.NewGuid();

        /// <summary>
        /// The unique identifier for the institution.
        /// </summary>
        public Guid InstitutionId { get; set; }

        /// <summary>
        /// The unique identifier for the customer that owns the image.
        /// </summary>
        public Guid CustomerGuid { get; set; }


    }
}
