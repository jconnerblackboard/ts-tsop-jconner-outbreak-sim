﻿using System;
using System.Collections.Generic;
using System.Linq;
using BbTS.Domain.Models.Definitions.Notification;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for a report about a notifications process run.
    /// </summary>
    public class NotificationProcessingReport
    {
        /// <summary>
        /// Unique identifier for this report.
        /// </summary>
        public Guid Id { get; set; } = Guid.NewGuid();

        /// <summary>
        /// The count of different types.
        /// </summary>
        public Dictionary<NotificationObjectType, int> ObjectTypeCounts { get; internal set; } = new Dictionary<NotificationObjectType, int>();

        /// <summary>
        /// The time the notification processing run was started.
        /// </summary>
        public DateTimeOffset StartTime { get; set; }

        /// <summary>
        /// The time the notification processing run was completed.
        /// </summary>
        public DateTimeOffset EndTime { get; set; }

        /// <summary>
        /// Count of successful operations.
        /// </summary>
        public int SuccessCount { get; set; }

        /// <summary>
        /// Count of failed operations.
        /// </summary>
        public int FailedCount { get; set; }

        /// <summary>
        /// Maximum number of active threads during processing.
        /// </summary>
        public int MaxThreadsUsed { get; set; }

        public NotificationProcessingState Result { get; set; } = NotificationProcessingState.Idle;

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="start">The time the notification processing run was started.</param>
        /// <param name="end">The time the notification processing run was completed.</param>
        public NotificationProcessingReport(DateTimeOffset start, DateTimeOffset end)
        {
            StartTime = start;
            EndTime = end;
        }

        /// <summary>
        /// Get the total object count
        /// </summary>
        /// <returns></returns>
        public int TotalObjectCount
        {
            get
            {
                int total = 0;
                ObjectTypeCounts.Values.ToList().ForEach(c => total += c);
                return total;
            }
        }

        /// <summary>
        /// Increment the count of an object type.
        /// </summary>
        /// <param name="type">The count of the type to increment.</param>
        /// <param name="count">The amount to increment by.</param>
        public void IncrementObjectTypeCount(NotificationObjectType type, int count = 1)
        {
            if (!ObjectTypeCounts.ContainsKey(type))
            {
                ObjectTypeCounts.Add(type, 0);
            }

            ObjectTypeCounts[type] += count;
        }

        /// <summary>
        /// Clear (zero out) the count for a specified type.
        /// </summary>
        /// <param name="type">The type.</param>
        public void ClearObjectTypeCount(NotificationObjectType type)
        {
            if (!ObjectTypeCounts.ContainsKey(type)) return;

            ObjectTypeCounts[type] = 0;
        }

        /// <summary>
        /// Custom to string method.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var result = $"Processing was started at '{StartTime}' and completed at '{EndTime}'.\r\n";
            result = $"{result}" +
                     $"Maximum number of active threads: {MaxThreadsUsed}\r\n";

            foreach (var item in ObjectTypeCounts)
            {
                result = $"{result}" +
                         $"A count of {item.Value} items of type '{item.Key}' were processed.\r\n";
            }

            result = $"{result}" +
                     $"{SuccessCount} operations were successful.\r\n" +
                     $"{FailedCount} operations failed.\r\n" +
                     $"{TotalObjectCount} operations were performed.";

            return result;
        }

        public NotificationProcessingReport Clone()
        {
            var clone = (NotificationProcessingReport)MemberwiseClone();
            clone.ObjectTypeCounts = new Dictionary<NotificationObjectType, int>(ObjectTypeCounts);
            return clone;
        }
    }
}
