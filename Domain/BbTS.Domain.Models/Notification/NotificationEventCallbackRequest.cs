﻿using System;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for a notification event callback request.
    /// </summary>
    public class NotificationEventCallbackRequest
    {
        /// <summary>
        /// Unique identifier for the request.
        /// </summary>
        public Guid RequestId { get; set; } = Guid.NewGuid();

        /// <summary>
        /// Time to wait before expiring notificaiton wait one request.
        /// </summary>
        public int WaitTime { get; set; } = 3;
    }
}
