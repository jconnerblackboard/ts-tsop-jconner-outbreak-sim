﻿using System;
using BbTS.Domain.Models.ReportingMetrics;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for a azure table storage write request.
    /// </summary>
    public class ReportingMetricTableStorageWriteRequest
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public Guid RequestId { get; set; } = Guid.NewGuid();

        /// <summary>
        /// The entity to write.
        /// </summary>
        public TableEntityReportingMetrics Entity { get; set; }
    }
}
