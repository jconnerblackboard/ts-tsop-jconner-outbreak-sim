﻿using System;
using BbTS.Domain.Models.Definitions.Notification;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for a notification object
    /// </summary>
    public class NotificationObject
    {
        /// <summary>
        /// Numerical identifier for the notification
        /// </summary>
        public int NotificationId { get; set; }

        /// <summary>
        /// The unique identifier for the institution.
        /// </summary>
        public Guid InstitutionId { get; set; }

        /// <summary>
        /// The unique identifier for the object.
        /// </summary>
        public Guid ObjectGuid { get; set; }

        /// <summary>
        /// The 22 digit customer identifier, left 0 padded.
        /// </summary>
        public string CustomerNumber { get; set; }

        /// <summary>
        /// The action taken upon the object.
        /// </summary>
        public NotificationEventType EventType { get; set; }

        /// <summary>
        /// The type of notification event.
        /// </summary>
        public NotificationObjectType ObjectType { get; set; }

        /// <summary>
        /// The date and time for this notification event.
        /// </summary>
        public DateTimeOffset EventDateTime { get; set; }

        /// <summary>
        /// The data associated with this notification.
        /// </summary>
        public string Data { get; set; }

        /// <summary>
        /// List of external client ids associated with the object.
        /// </summary>
        public string ExternalClientIds { get; set; }

        /// <summary>
        /// The status of the notification.
        /// </summary>
        public NotificationStatusType Status { get; set; }

        /// <summary>
        /// The date and time of the latest status update.
        /// </summary>
        public DateTimeOffset StatusDateTime { get; set; }
    }
}
