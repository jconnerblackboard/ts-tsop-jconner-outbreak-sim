﻿using BbTS.Domain.Models.Definitions.Notification;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for the response to a notification reporting metrics validation request.
    /// </summary>
    public class NotificationReportingMetricsValidationResponse
    {
        /// <summary>
        /// The result of the validation operation.
        /// </summary>
        public ReportingMetricsValidationResult Result { get; set; }

        /// <summary>
        /// Result message.
        /// </summary>
        public string Message { get; set; }
    }
}
