﻿using System;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for a notifications process request.
    /// </summary>
    public class NotificationsProcessRequest
    {
        /// <summary>
        /// Unique identifier for the request.
        /// </summary>
        public Guid RequestId { get; set; } = Guid.NewGuid();

    }
}
