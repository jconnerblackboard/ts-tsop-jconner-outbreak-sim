﻿using System;
using System.Xml.Serialization;
using BbTS.Domain.Models.Definitions.System;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for the database object related to an access object.
    /// </summary>
    [Serializable]
    [XmlRoot("Access")]
    public class NotificationAccessDatabase : NotificationDatabaseBase
    {
        /// <summary>
        /// The unique numerical identifier for the access object.
        /// </summary>
        [XmlElement("AccessId")]
        public string AccessId { get; set; }

        /// <summary>
        /// The name of the access object.
        /// </summary>
        [XmlElement("AccessName")]
        public string AccessName { get; set; }

        /// <summary>
        /// The active start date of access.
        /// </summary>
        [XmlElement("StartDate")]
        public string StartDate { get; set; }

        /// <summary>
        /// The active end date of access.
        /// </summary>
        [XmlElement("EndDate")]
        public string EndDate { get; set; }
    }
}
