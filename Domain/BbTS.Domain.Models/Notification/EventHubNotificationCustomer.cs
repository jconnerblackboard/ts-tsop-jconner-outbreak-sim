﻿using System;
using System.Runtime.Serialization;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for the customer object to be pushed up to the event hub.
    /// </summary>
    [DataContract]
    [Serializable]
    public class EventHubNotificationCustomer : EventHubNotificationBase, IEquatable<EventHubNotificationCustomer>
    {
        /// <summary>
        /// The first name of the customer
        /// </summary>
        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        /// <summary>
        /// The middle name of the customer
        /// </summary>
        [DataMember(Name = "middleName")]
        public string MiddleName { get; set; }

        /// <summary>
        /// The last name of the customer
        /// </summary>
        [DataMember(Name = "lastName")]
        public string LastName { get; set; }

        /// <summary>
        /// Whether the customer is active or not
        /// </summary>
        [DataMember(Name = "isActive")]
        public bool IsActive { get; set; }

        /// <summary>
        /// The first date the customer account is considered active
        /// </summary>
        [DataMember(Name = "startDate")]
        public DateTimeOffset StartDate { get; set; }

        /// <summary>
        /// The last date the customer account is considered active
        /// </summary>
        [DataMember(Name = "endDate")]
        public DateTimeOffset EndDate { get; set; }

        /// <summary>
        /// The Mobile Id Card Type
        /// </summary>
        [DataMember(Name = "mobileIdCardType")]
        public string MobileIdCardType { get; set; }

        /// <summary>
        /// The Mobile Id IIN Pool
        /// </summary>
        [DataMember(Name = "mobileIdIINPool")]
        // ReSharper disable once InconsistentNaming
        public string MobileIdIINPool { get; set; }

        /// <summary>
        /// Mobile id procurement allowed.
        /// </summary>
        [DataMember(Name = "mobileIdProcurementAllowed")]
        public bool MobileIdProcurementAllowed { get; set; }

        /// <summary>
        /// Mobile id reference text.
        /// </summary>
        [DataMember(Name = "mobileIdRefText")]
        public string MobileIdRefText { get; set; }

        /// <summary>
        /// Mobile id is free.
        /// </summary>
        [DataMember(Name = "mobileIdIsFree")]
        public bool MobileIdIsFree { get; set; }

        /// <summary>
        /// Mobile id price.
        /// </summary>
        [DataMember(Name = "mobileIdPrice")]
        public string MobileIdPrice { get; set; }

        /// <summary>
        /// Mobile id expire date.
        /// </summary>
        [DataMember(Name = "mobileIdExpireDate")]
        public string MobileIdExpireDate { get; set; }

        /// <summary>
        /// CDF Group / Items [Student|Employee|Other] / Default [None]
        /// </summary>
        [DataMember(Name = "mobilePersonType")]
        public string MobilePersonType { get; set; }

        /// <summary>
        /// CDF Group / Items [Undergraduate|Graduate] / Default [None]
        /// </summary>
        [DataMember(Name = "mobileStudentClass")]
        public string MobileStudentClass { get; set; }

        /// <summary>
        /// CDF Group / Items[On - Campus | Off - Campus] / Default [None]
        /// </summary>
        [DataMember(Name = "mobileStudentResidence")]
        public string MobileStudentResidence { get; set; }

        /// <summary>
        /// CDF String / Length 30 / Default Empty
        /// </summary>
        [DataMember(Name = "mobileOrgLabel")]
        public string MobileOrgLabel { get; set; }

        /// <summary>
        /// CDF String / Length 30 / Default Empty
        /// </summary>
        [DataMember(Name = "mobileRole")]
        public string MobileRole { get; set; }

        /// <summary>
        /// CDF String / Length 30 / Default Empty 
        /// </summary>
        [DataMember(Name = "mobileOrg")]
        public string MobileOrg { get; set; }

        /// <summary>
        /// Equalite override.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(EventHubNotificationCustomer other)
        {
            return other != null &&
                   CustomerGuid == other.CustomerGuid &&
                   CustomerNumber == other.CustomerNumber &&
                   FirstName == other.FirstName &&
                   MiddleName == other.MiddleName &&
                   LastName == other.LastName &&
                   IsActive == other.IsActive &&
                   EndDate == other.EndDate &&
                   MobileIdCardType == other.MobileIdCardType &&
                   MobileIdIINPool == other.MobileIdIINPool &&
                   MobileIdProcurementAllowed == other.MobileIdProcurementAllowed &&
                   MobileIdRefText == other.MobileIdRefText &&
                   MobileIdIsFree == other.MobileIdIsFree &&
                   MobileIdPrice == other.MobileIdPrice &&
                   MobileIdExpireDate == other.MobileIdExpireDate &&
                   MobileOrgLabel == other.MobileOrgLabel &&
                   MobileRole == other.MobileRole &&
                   MobileOrg == other.MobileOrg &&
                   MobilePersonType == other.MobilePersonType &&
                   MobileStudentClass == other.MobileStudentClass &&
                   MobileStudentResidence == other.MobileStudentResidence;

        }
    }
}
