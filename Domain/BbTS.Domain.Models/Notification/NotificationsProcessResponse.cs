﻿using System;
using System.Collections.Generic;
using BbTS.Domain.Models.Definitions.Notification;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for the response to a notifications process request.
    /// </summary>
    public class NotificationsProcessResponse
    {
        /// <summary>
        /// Unique identifier for the request.
        /// </summary>
        public Guid RequestId { get; set; }

        /// <summary>
        /// Numerical identifier for the notification
        /// </summary>
        public int NotificationId { get; set; }

        /// <summary>
        /// The state of notification processing.
        /// </summary>
        public NotificationProcessingState ProcessingState { get; set; }

        /// <summary>
        /// The human readable response message indicating what happened during processing.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// The processing function being performed.
        /// </summary>
        public NotificationProcessingFunction Function { get; set; }

        /// <summary>
        /// The unique identifier of the object prodcessed.
        /// </summary>
        public Guid ObjectGuid { get; set; }

        /// <summary>
        /// The type of object processed.
        /// </summary>
        public NotificationObjectType ObjectType { get; set; }

        /// <summary>
        /// Processing result of child objects.
        /// </summary>
        public List<NotificationsProcessResponse> ChildResponses = new List<NotificationsProcessResponse>();

        /// <summary>
        /// The total number of objects processed during this operation.
        /// </summary>
        public int TotalObjectsProcessed { get; set; }

        /// <summary>
        /// Maximum number of active threads during processing.
        /// </summary>
        public int MaxThreadsUsed { get; set; }

        /// <summary>
        /// Time it took to process the notification(s)
        /// </summary>
        public TimeSpan ProcessingDuration { get; set; }
    }
}
