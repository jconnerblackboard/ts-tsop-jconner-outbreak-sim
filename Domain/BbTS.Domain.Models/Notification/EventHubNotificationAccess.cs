﻿using System;
using System.Runtime.Serialization;
using BbTS.Domain.Models.Definitions.System;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for an event hub notification access (door access) object.
    /// </summary>
    [DataContract]
    public class EventHubNotificationAccess : EventHubNotificationBase, IEquatable<EventHubNotificationAccess>
    {
        /// <summary>
        /// The unique numerical identifier for the access object.
        /// </summary>
        [DataMember(Name = "accessId")]
        public string AccessId { get; set; }

        /// <summary>
        /// The name of the access object.
        /// </summary>
        [DataMember(Name = "accessName")]
        public string AccessName { get; set; }

        /// <summary>
        /// The active start date of access.
        /// </summary>
        [DataMember(Name = "startDate")]
        public DateTimeOffset? StartDate { get; set; } = SystemDefinitions.TsopDateTimeMin;

        /// <summary>
        /// The active end date of access.
        /// </summary>
        [DataMember(Name = "endDate")]
        public DateTimeOffset? EndDate { get; set; } = SystemDefinitions.TsopDateTimeMax;

        /// <summary>
        /// Override equals.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(EventHubNotificationAccess other)
        {
            return other != null &&
                   AccessId == other.AccessId &&
                   AccessName == other.AccessName &&
                   StartDate == other.StartDate &&
                   EndDate == other.EndDate &&
                   CustomerGuid == other.CustomerGuid &&
                   CustomerNumber == other.CustomerNumber;
        }
    }
}
