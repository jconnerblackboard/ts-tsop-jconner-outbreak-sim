﻿using System;
using BbTS.Domain.Models.Definitions.Notification;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for the response to a notificaiton status set request.
    /// </summary>
    public class NotificationStatusSetResponse
    {
        /// <summary>
        /// Unique identifier for the request.
        /// </summary>
        public Guid RequestId { get; set; }

        /// <summary>
        /// The result of the status set operation at the data layer.
        /// </summary>
        public NotificationStatusSetResult Result { get; set; }

        /// <summary>
        /// Message associated with the result.
        /// </summary>
        public string Message { get; set; }
    }
}
