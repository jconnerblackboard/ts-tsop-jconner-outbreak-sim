﻿namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for the response to a azure table storage write request.
    /// </summary>
    public class ReportingMetricTableStorageWriteResponse : EventHubNotificationUploadResponse
    {

    }
}
