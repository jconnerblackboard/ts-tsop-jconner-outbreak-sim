﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Definitions.Notification;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for the response to an event hub customer image upload request.
    /// </summary>
    public class EventHubCustomerImageUploadResponse
    {
        /// <summary>
        /// Unique identifier for the request.
        /// </summary>
        public Guid RequestId { get; set; }

        /// <summary>
        /// The result of the event hub notification upload operation.
        /// </summary>
        public EventHubNotificationUploadResult Result { get; set; }

        /// <summary>
        /// User friendly message containing an explanation of the results of the upload operation.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// The name of the image file that was uploaded to the event hub.
        /// </summary>
        public string ImageFileName { get; set; }

        /// <summary>
        /// Clone this.
        /// </summary>
        /// <returns></returns>
        public EventHubCustomerImageUploadResponse Clone()
        {
            return (EventHubCustomerImageUploadResponse)MemberwiseClone();
        }
    }
}
