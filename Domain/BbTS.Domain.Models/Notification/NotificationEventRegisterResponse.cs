﻿using System;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for the response to a notification event register request.
    /// </summary>
    public class NotificationEventRegisterResponse
    {
        /// <summary>
        /// Unique identifier for the request.
        /// </summary>
        public Guid RequestId { get; set; }
    }
}
