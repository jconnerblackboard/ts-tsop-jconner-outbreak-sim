﻿using System;
using BbTS.Domain.Models.Definitions.Notification;

namespace BbTS.Domain.Models.Notification
{
    /// <summary>
    /// Container class for a notification event callback request.
    /// </summary>
    public class NotificationCreateRequest
    {
        /// <summary>
        /// Unique identifier for the request.
        /// </summary>
        public Guid RequestId { get; set; } = Guid.NewGuid();

        /// <summary>
        /// The numerical identifer for the object.
        /// </summary>
        public int ObjectId { get; set; }

        /// <summary>
        /// The type of notification event.
        /// </summary>
        public NotificationEventType EventType { get; set; }

        /// <summary>
        /// The type of object.
        /// </summary>
        public NotificationObjectType ObjectType { get; set; }

        /// <summary>
        /// The unique identifier for the object.
        /// </summary>
        public Guid ObjectGuid { get; set; }
    }
}
