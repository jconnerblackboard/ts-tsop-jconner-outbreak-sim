﻿using System;
using System.Collections.Generic;

namespace BbTS.Domain.Models.Card
{
    /// <summary>
    /// Card assignment history item.
    /// </summary>
    public class CardAssignmentHistoryItem
    {
        /// <summary>
        /// Card history ID.
        /// </summary>
        public int CardHistoryId { get; set; }

        /// <summary>
        /// Card number.
        /// </summary>
        public string CardNumber { get; set; }

        /// <summary>
        /// Issue number.
        /// </summary>
        public string IssueNumber { get; set; }

        /// <summary>
        /// Customer number.
        /// </summary>
        public int? CustomerId { get; set; }

        /// <summary>
        /// Start date time.
        /// </summary>
        public DateTimeOffset StartDateTime { get; set; }
    }
}
