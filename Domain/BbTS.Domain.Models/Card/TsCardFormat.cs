﻿
namespace BbTS.Domain.Models.Card
{
    /// <summary>
    /// Container class with Card Format related device settings.
    /// </summary>
    public class TsCardFormat
    {
        /// <summary>
        /// Unique identifier (Guid) for the card format.
        /// </summary>
        public string CardFormatGuid { get; set; }

        /// <summary>
        /// Name of the card format.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Order in which the formats should be displayed/processed
        /// </summary>
        public int PrioritySequence { get; set; }

        /// <summary>
        /// Site code value.  If SiteCode is null, this property is ignored.
        /// </summary>
        public string SiteCode { get; set; }

        /// <summary>
        /// Start position of the site code.
        /// </summary>
        public int SiteCodeStartPosition { get; set; }

        /// <summary>
        /// (0) - Absolute position from the first digit on track 2; (1) - Relative positioning from the first field separator on track 2.
        /// </summary>
        public int SiteCodeStartType { get; set; }

        /// <summary>
        /// The length of the site code.
        /// </summary>
        public int SiteCodeLength { get; set; }

        /// <summary>
        /// If online, check site code.
        /// </summary>
        public bool SiteCodeCheckOnline { get; set; }

        /// <summary>
        /// If offline, check site code.
        /// </summary>
        public bool SiteCodeCheckOffline { get; set; }

        /// <summary>
        /// The start position of the ID number.
        /// </summary>
        public int IdNumberStartPosition { get; set; }

        /// <summary>
        /// (0) - Absolute position from the first digit on track 2; (1) - Relative positioning from the first field separator on track 2.
        /// </summary>
        public int IdNumberStartType { get; set; }

        /// <summary>
        /// The length of the Id number.
        /// </summary>
        public int IdNumberLength { get; set; }

        /// <summary>
        /// The position of the issue number.
        /// </summary>
        public int IssueNumberStart { get; set; }

        /// <summary>
        /// Absolute position from the first digit on track 2; (1) - Relative positioning from the first field separator on track 2.
        /// </summary>
        public int IssueNumberStartType { get; set; }

        /// <summary>
        /// Maximum length of issue number.
        /// </summary>
        public int IssueNumberLength { get; set; }

        /// <summary>
        /// The minimum number of digits on track 2
        /// </summary>
        public int MinimumDigitsTrack2 { get; set; }

        /// <summary>
        /// The maximum number of digits on track 2
        /// </summary>
        public int MaximumDigitsTrack2 { get; set; }
    }
}
