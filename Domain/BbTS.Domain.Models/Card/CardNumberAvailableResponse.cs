﻿
namespace BbTS.Domain.Models.Card
{
    /// <summary>
    /// Container class for handling a response to a get cards request.
    /// </summary>
    public class CardNumberAvailableResponse
    {
        /// <summary>
        /// Card number
        /// </summary>
        public decimal CardNumber { get; set; }

        /// <summary>
        /// Cards
        /// </summary>
        public bool Available { get; set; }

        /// <summary>
        /// Indicates if the card is temporary if available
        /// </summary>
        public bool Temporary { get; set; }
    }
}
