﻿using BbTS.Domain.Models.Customer;

namespace BbTS.Domain.Models.Card
{
    /// <summary>
    /// Container class for handling a server response to a get card request.
    /// </summary>
    public class CardGetResponse
    {
        /// <summary>
        /// Card's guid
        /// </summary>
        public string CardGuid { get; set; }

        /// <summary>
        /// Card
        /// </summary>
        public TsCard Card { get; set; }
    }
}
