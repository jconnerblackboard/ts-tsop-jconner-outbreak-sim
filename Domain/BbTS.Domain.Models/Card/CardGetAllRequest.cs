﻿using System;
using System.Collections.Generic;

namespace BbTS.Domain.Models.Card
{
    /// <summary>
    /// Container class for handling a get all card request.
    /// </summary>
    public class CardGetAllRequest
    {
        /// <summary>
        /// Card number
        /// </summary>
        public string CardNumber { get; set; }

        /// <summary>
        /// Card type
        /// </summary>
        public int? CardType { get; set; }

        /// <summary>
        /// Customer guid
        /// </summary>
        public string CustomerGuid { get; set; }

        /// <summary>
        /// Card status
        /// </summary>
        public int? CardStatus { get; set; }

        /// <summary>
        /// Card Idm
        /// </summary>
        public string CardIdm { get; set; }

        /// <summary>
        /// Formats properties as optional uri params
        /// </summary>
        /// <returns></returns>
        public string FormUriParams()
        {
            var list = new List<string>();

            if (!string.IsNullOrEmpty(CardNumber))
                list.Add($"{nameof(CardNumber)}={Uri.EscapeUriString(CardNumber)}");
            if (CardType.HasValue)
                list.Add($"{nameof(CardType)}={CardType}");
            if (!string.IsNullOrEmpty(CustomerGuid))
                list.Add($"{nameof(CustomerGuid)}={Uri.EscapeUriString(CustomerGuid)}");
            if (CardStatus.HasValue)
                list.Add($"{nameof(CardStatus)}={CardStatus}");
            if (CardIdm != null)
                list.Add($"{nameof(CardIdm)}={Uri.EscapeUriString(CardIdm)}");

            return string.Join("&", list);
        }
    }
}
