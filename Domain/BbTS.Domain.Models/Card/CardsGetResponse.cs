﻿using BbTS.Domain.Models.Customer;
using System.Collections.Generic;

namespace BbTS.Domain.Models.Card
{
    /// <summary>
    /// Container class for handling a response to a get cards request.
    /// </summary>
    public class CardsGetResponse
    {
        /// <summary>
        /// Cards
        /// </summary>
        public List<TsCard> Cards { get; set; }
    }
}
