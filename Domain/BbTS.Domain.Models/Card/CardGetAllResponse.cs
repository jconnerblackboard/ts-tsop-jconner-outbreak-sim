﻿
namespace BbTS.Domain.Models.Card
{
    /// <summary>
    /// Container class for handling a response to a get all card request.
    /// </summary>
    public class CardGetAllResponse : CardsGetResponse
    {
        /// <summary>
        /// Original request
        /// </summary>
        public CardGetAllRequest Request { get; set; }
    }
}
