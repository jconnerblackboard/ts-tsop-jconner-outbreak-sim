﻿namespace BbTS.Domain.Models.EAccount
{
    /// <summary>
    /// Response for the low balance alerts request.
    /// </summary>
    public class LowBalanceAlertsGetResponse
    {
        /// <summary>
        /// Whether low balance alerts are utilized on this system.
        /// </summary>
        public bool LowBalanceAlerts { get; set; }
    }
}