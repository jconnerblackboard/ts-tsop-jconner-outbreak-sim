﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.EAccount
{
    /// <summary>
    /// PaymentMethod enum to give readable names for eAccount deposit PaymentMethod
    /// </summary>
    public enum PaymentMethod
    {
        /// <summary>
        /// Unspecified payment method
        /// </summary>
        Unspecified = 0,
        /// <summary>
        /// ApplePay payment method
        /// </summary>
        ApplePay = 1
    }
}
