﻿using System;

namespace BbTS.Domain.Models.EAccount
{
    /// <summary>
    /// Request for performing a deposit without knowing the External Client Customer Guid
    /// </summary>
    public class AnonymousDepositRequest
    {
        /// <summary>
        /// The unique identifier of the request.
        /// </summary>
        public string RequestId { get; set; }
        /// <summary>
        /// External Client Terminal Number
        /// </summary>
        public int TerminalNumber { get; set; }
        /// <summary>
        /// Card Number for customer
        /// </summary>
        public string CardNum { get; set; }
        /// <summary>
        /// Email address for customer
        /// </summary>
        public string EmailAddress { get; set; }
        /// <summary>
        /// Customer Number
        /// </summary>
        public string CustNum { get; set; }
        /// <summary>
        /// Customer Last Name
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Customer First Name
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Customer Middle Name
        /// </summary>
        public string MiddleName { get; set; }
        /// <summary>
        /// Stored Value Account Type Id to deposit to
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public int SVAccountTypeId { get; set; }
        /// <summary>
        /// Date Time (with Time Zone) of transaction.
        /// </summary>
        public DateTimeOffset TransactionDateTime { get; set; }
        /// <summary>
        /// Transaction Amount for deposit.
        /// </summary>
        public decimal TransactionAmount { get; set; }
        /// <summary>
        /// Indicates if transaction is online.
        /// </summary>
        public bool IsOnline { get; set; } 
        /// <summary>
        /// External Client Transaction Guid
        /// </summary>
        public Guid ExternalClientTransactionGuid { get; set; }
        /// <summary>
        /// Payment Method used, 0 - Unspecified, 1 - ApplePay
        /// </summary>
        public PaymentMethod PaymentMethod { get; set; }
        /// <summary>
        /// Gateway Transaction Reference, for Payment Express, this is dpsTxnRef
        /// </summary>
        public string GatewayTransactionReference { get; set; }
        /// <summary>
        /// Masked Credit Card Number, containing no more than the first 6 and last 4 digits un-obscured
        /// </summary>
        public string MaskedCreditCardNumber { get; set; }
    }
}
