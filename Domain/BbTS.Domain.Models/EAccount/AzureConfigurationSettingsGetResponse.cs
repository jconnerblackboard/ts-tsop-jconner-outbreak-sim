﻿namespace BbTS.Domain.Models.EAccount
{
    /// <summary>
    /// Response for information about event hub notification settings.
    /// </summary>
    public class AzureConfigurationSettingsGetResponse
    {
        /// <summary>
        /// Event hub end point URI.
        /// </summary>
        public string EventHubEndPointUri { get; set; }

        /// <summary>
        /// Photo storage account connection string.
        /// </summary>
        public string PhotoStorageAccountConnectionString { get; set; }
    }
}
