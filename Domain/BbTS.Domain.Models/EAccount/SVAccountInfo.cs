﻿namespace BbTS.Domain.Models.EAccount
{
    /// <summary>
    /// Class to represent information about one stored value account following a deposit.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public class SVAccountInfo
    {
        /// <summary>
        /// Stored Value Account Type Id
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public int SVAccountTypeId { get; set; }
        /// <summary>
        /// Amount deposited to account.
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// Type of account, 
        /// </summary>
        public int DebitCreditType { get; set; }
        /// <summary>
        /// Indicates if an enrichment was applied during current deposit.
        /// </summary>
        public bool EnrichmentApplied { get; set; }
        /// <summary>
        /// Balance in account at the end of the deposit
        /// </summary>
        public decimal EndingBalance { get; set; }
    }
}
