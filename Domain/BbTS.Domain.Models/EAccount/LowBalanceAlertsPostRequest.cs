﻿using System;

namespace BbTS.Domain.Models.EAccount
{
    /// <summary>
    /// Request for low balance alerts set.
    /// </summary>
    public class LowBalanceAlertsPostRequest
    {
        /// <summary>
        /// The unique identifier of the request.
        /// </summary>
        public Guid RequestId { get; set; }

        /// <summary>
        /// Whether low balance alerts are utilized on this system.
        /// </summary>
        public bool LowBalanceAlerts { get; set; }
    }
}