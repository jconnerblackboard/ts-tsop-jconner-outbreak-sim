﻿using System;

namespace BbTS.Domain.Models.EAccount
{
    /// <summary>
    /// Response value for low balance alerts set.  Returned value indicates the setting after the post completed.
    /// </summary>
    public class LowBalanceAlertsPostResponse
    {
        /// <summary>
        /// The unique identifier of the request.
        /// </summary>
        public Guid RequestId { get; set; }

        /// <summary>
        /// Whether low balance alerts are utilized on this system.
        /// </summary>
        public bool LowBalanceAlerts { get; set; }
    }
}