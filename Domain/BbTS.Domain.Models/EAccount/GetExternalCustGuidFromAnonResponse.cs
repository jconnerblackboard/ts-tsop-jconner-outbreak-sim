﻿using System;

namespace BbTS.Domain.Models.EAccount
{
    /// <summary>
    /// Response to translation of customer info to external client customer guid
    /// </summary>
    public class GetExternalCustGuidFromAnonResponse
    {
        /// <summary>
        /// External Client Customer Guid
        /// </summary>
        public Guid? ClientCustomerGuid { get; set; }
        /// <summary>
        /// CustId value, set when customer not registered with external client
        /// </summary>
        public decimal? CustId { get; set; }
        /// <summary>
        /// Transaction denied text.
        /// </summary>
        public string DeniedText { get; set; }
        /// <summary>
        /// Transaction error code.
        /// </summary>
        public int ErrorCode { get; set; }
    }
}
