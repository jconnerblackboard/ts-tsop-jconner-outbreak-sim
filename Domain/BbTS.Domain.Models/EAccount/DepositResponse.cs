﻿using System;
using System.Collections.Generic;

namespace BbTS.Domain.Models.EAccount
{
    /// <summary>
    /// Information about a deposit that was attempted.
    /// </summary>
    public class DepositResponse
    {
        /// <summary>
        /// The unique identifier of the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// Transaction number
        /// </summary>
        public string TranNumber { get; set; }

        /// <summary>
        /// Total amount of transaction
        /// </summary>
        public decimal TransactionTotal { get; set; }

        /// <summary>
        /// Location name for transaction
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// Retail Tran Type Category Text
        /// </summary>
        public string RetailTranCategoryText { get; set; }

        /// <summary>
        /// Retail Tran Type Display Text
        /// </summary>
        public string RetailTranTypeText { get; set; }

        /// <summary>
        /// Info about all affected SV Accounts
        /// </summary>
        public List<SVAccountInfo> TransactionAccountInfo{ get; set; } = new List<SVAccountInfo>();

        /// <summary>
        /// Merchant Name
        /// </summary>
        public string MerchantName { get; set; }

        /// <summary>
        /// Profit Center Name
        /// </summary>
        public string ProfitCenterName { get; set; }

        /// <summary>
        /// POS Name
        /// </summary>
        public string PosName { get; set; }

        /// <summary>
        /// Customer Number
        /// </summary>
        public string CustNum { get; set; }

        /// <summary>
        /// Card Number for customer
        /// </summary>
        public string CardNum { get; set; }

        /// <summary>
        /// Email address for customer
        /// </summary>
        public string EmailAddress { get; set; }

        /// <summary>
        /// Customer Last Name
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Customer First Name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Customer Middle Name
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// DateTime transaciton posted
        /// </summary>
        public DateTimeOffset PostTransactionDateTime { get; set; }

        /// <summary>
        /// Transaction denied text.
        /// </summary>
        public string DeniedText { get; set; }

        /// <summary>
        /// Transaction error code.
        /// </summary>
        public int ErrorCode { get; set; }
    }
}
