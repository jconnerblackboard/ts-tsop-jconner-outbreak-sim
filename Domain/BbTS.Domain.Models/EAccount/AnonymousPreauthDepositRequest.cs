﻿using System;

namespace BbTS.Domain.Models.EAccount
{
    /// <summary>
    /// Request for pre-authorizing a deposit without knowing the External Client Customer Guid
    /// </summary>
    public class AnonymousPreauthDepositRequest
    {
        /// <summary>
        /// The unique identifier of the request.
        /// </summary>
        public string RequestId { get; set; }
        /// <summary>
        /// External Client Terminal Number
        /// </summary>
        public int TerminalNumber { get; set; }
        /// <summary>
        /// Card Number for customer
        /// </summary>
        public string CardNum { get; set; }
        /// <summary>
        /// Email address for customer
        /// </summary>
        public string EmailAddress { get; set; }
        /// <summary>
        /// Customer Number
        /// </summary>
        public string CustNum { get; set; }
        /// <summary>
        /// Customer Last Name
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Customer First Name
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Customer Middle Name
        /// </summary>
        public string MiddleName { get; set; }
        /// <summary>
        /// Stored Value Account Type Id to deposit to
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public int SVAccountTypeId { get; set; }
        /// <summary>
        /// Date Time (with Time Zone) of transaction.
        /// </summary>
        public DateTimeOffset TransactionDateTime { get; set; }
        /// <summary>
        /// Transaction Amount for deposit.
        /// </summary>
        public decimal TransactionAmount { get; set; }
    }
}
