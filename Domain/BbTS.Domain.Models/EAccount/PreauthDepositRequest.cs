﻿using System;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace BbTS.Domain.Models.EAccount
{
    /// <summary>
    /// Request for preauthorizing a deposit
    /// </summary>
    public class PreauthDepositRequest
    {
        /// <summary>
        /// The unique identifier of the request.
        /// </summary>
        public string RequestId { get; set; }
        /// <summary>
        /// External Client Terminal Number
        /// </summary>
        public int TerminalNumber { get; set; }
        /// <summary>
        /// External Client Customer Guid
        /// </summary>
        public Guid ? ClientCustomerGuid { get; set; }
        /// <summary>
        /// Stored Value Account Type Id to deposit to
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public int SVAccountTypeId { get; set; }
        /// <summary>
        /// Date Time (with Time Zone) of transaction.
        /// </summary>
        public DateTimeOffset TransactionDateTime { get; set; }
        /// <summary>
        /// Transaction Amount for deposit.
        /// </summary>
        public decimal TransactionAmount { get; set; }
        /// <summary>
        /// CustId value, not sent by API clients, this is to be filled in for internal
        /// use for anonymous operations.
        /// </summary>
        [SoapIgnore]
        [XmlIgnore]
        [JsonIgnore]
        public decimal? CustId { get; set; }
    }
}
