﻿namespace BbTS.Domain.Models.EAccount
{
    /// <summary>
    /// Response to preauthorization for deposit
    /// </summary>
    public class PreauthDepositResponse
    {
        /// <summary>
        /// The unique identifier of the request.
        /// </summary>
        public string RequestId { get; set; }
        /// <summary>
        /// The error code associated with the response.  0 means success.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Reason for failure (if there was a failure error code).
        /// </summary>
        public string DeniedText { get; set; }
        /// <summary>
        /// The available balance.
        /// </summary>
        public decimal AvailableBalance { get; set; }
        /// <summary>
        /// Merchant name.
        /// </summary>
        public string MerchantName { get; set; }
        /// <summary>
        /// Profit center name.
        /// </summary>
        public string ProfitCenterName { get; set; }
        /// <summary>
        /// POS Name
        /// </summary>
        public string PosName { get; set; }
        /// <summary>
        /// Customer number
        /// </summary>
        public string CustNum { get; set; }
        /// <summary>
        /// Customer card number
        /// </summary>
        public string CardNum { get; set; }
        /// <summary>
        /// Customer email address
        /// </summary>
        public string EmailAddress { get; set; }
        /// <summary>
        /// Customer last name
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Customer first name
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Customer middle name
        /// </summary>
        public string MiddleName { get; set; }
    }
}
