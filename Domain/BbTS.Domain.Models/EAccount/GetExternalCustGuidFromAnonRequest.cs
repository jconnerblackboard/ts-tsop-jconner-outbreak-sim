﻿namespace BbTS.Domain.Models.EAccount
{
    /// <summary>
    /// Request to translate from anonymous deposit info to external client customer Guid
    /// </summary>
    public class GetExternalCustGuidFromAnonRequest
    {
        /// <summary>
        /// External Client Terminal Number
        /// </summary>
        public int TerminalNumber { get; set; }
        /// <summary>
        /// Card Number for customer
        /// </summary>
        public string CardNum { get; set; }
        /// <summary>
        /// Email address for customer
        /// </summary>
        public string EmailAddress { get; set; }
        /// <summary>
        /// Customer Number
        /// </summary>
        public string CustNum { get; set; }
        /// <summary>
        /// Customer Last Name
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Customer First Name
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Customer Middle Name
        /// </summary>
        public string MiddleName { get; set; }
    }
}
