﻿using System;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace BbTS.Domain.Models.EAccount
{
    /// <summary>
    /// Request to perform a deposit
    /// </summary>
    public class DepositRequest
    {
        /// <summary>
        /// The unique identifier of the request.
        /// </summary>
        public string RequestId { get; set; }
        /// <summary>
        /// External Client Terminal Number
        /// </summary>
        public int TerminalNumber { get; set; }
        /// <summary>
        /// External Client Customer Guid
        /// </summary>
        public Guid ? ClientCustomerGuid { get; set; }
        /// <summary>
        /// Stored Value Account Type Id to deposit to
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public int SVAccountTypeId { get; set; }
        /// <summary>
        /// Date Time (with Time Zone) of transaction.
        /// </summary>
        public DateTimeOffset TransactionDateTime { get; set; }
        /// <summary>
        /// Transaction Amount for deposit.
        /// </summary>
        public decimal TransactionAmount { get; set; }
        /// <summary>
        /// Indicates if transaction is online.
        /// </summary>
        public bool IsOnline { get; set; }
        /// <summary>
        /// External Client Transaction Guid
        /// </summary>
        public Guid ExternalClientTransactionGuid { get; set; }
        /// <summary>
        /// Payment Method used, 0 - Unspecified, 1 - ApplePay
        /// </summary>
        public PaymentMethod PaymentMethod { get; set; }
        /// <summary>
        /// Gateway Transaction Reference, for Payment Express, this is dpsTxnRef
        /// </summary>
        public string GatewayTransactionReference { get; set; }
        /// <summary>
        /// Masked Credit Card Number, containing no more than the first 6 and last 4 digits un-obscured
        /// </summary>
        public string MaskedCreditCardNumber { get; set; }
        /// <summary>
        /// CustId value, not sent by API clients, this is to be filled in for internal
        /// use for anonymous operations.
        /// </summary>
        [SoapIgnore]
        [XmlIgnore]
        [JsonIgnore]
        public decimal ? CustId { get; set; }
    }
}
