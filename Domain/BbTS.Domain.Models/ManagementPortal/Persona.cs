﻿using System;
using System.Collections.Generic;
using BbTS.Domain.Models.System;

namespace BbTS.Domain.Models.ManagementPortal
{
    public class PersonaLoggingRetention
    {
        public List<RetentionPeriod> RetentionPeriodList { get; set; }
        public List<AutomaticPurge> AutomaticPurgeList { get; set; }
    }

    public class AutomaticPurge : ControlParameter
    {
        public new Int32 ParameterValue { get; set; }
        public Boolean IsChecked { get; set; }
        public Int32 MaxRetentionPeriod { get; set; }
        public Int32 MinRetentionPeriod { get; set; }
        public String RetentionPeriodDescriptor { get; set; }
    }

    public class RetentionPeriod : ControlParameter
    {
        public new Int32 ParameterValue { get; set; }
        public Boolean IsReadOnly { get; set; }
        public Enum LowMediumHigh { get; set; }
    }
}
