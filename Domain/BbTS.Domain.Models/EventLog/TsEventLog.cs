﻿
namespace BbTS.Domain.Models.EventLog
{
    /// <summary>
    /// Event log
    /// </summary>
    public class TsEventLog : TsEventLogSummary
    {
        /// <summary>
        /// The task defined in the event.
        /// </summary>
        public int TaskCategory { get; set; }

        /// <summary>
        /// The message string.
        /// </summary>
        public string Message { get; set; }
    }
}