﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.EventLog
{
    /// <summary>
    /// Container class for handling a response to a get all event logs request.
    /// </summary>
    public class EventLogGetSummaryResponse
    {
        /// <summary>
        /// Original request
        /// </summary>
        public EventLogGetSummaryRequest Request { get; set; }

        /// <summary>
        /// Event logs
        /// </summary>
        public List<TsEventLogSummary> EventLogs { get; set; }
    }
}
