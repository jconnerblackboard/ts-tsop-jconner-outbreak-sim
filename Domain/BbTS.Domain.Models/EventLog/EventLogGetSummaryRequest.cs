﻿using System;
using System.Collections.Generic;

namespace BbTS.Domain.Models.EventLog
{
    /// <summary>
    /// Container class for handling a get all event logs request.
    /// </summary>
    public class EventLogGetSummaryRequest
    {
        /// <summary>
        /// Provider Name
        /// </summary>
        public string ProviderName { get; set; }

        /// <summary>
        /// Computer Name
        /// </summary>
        public string ComputerName { get; set; }

        /// <summary>
        /// Severity
        /// </summary>
        public byte? Severity { get; set; }

        /// <summary>
        /// Created from
        /// </summary>
        public DateTime? CreatedFromDate { get; set; }

        /// <summary>
        /// Created until
        /// </summary>
        public DateTime? CreatedUntilDate { get; set; }

        /// <summary>
        /// Formats properties as optional uri params
        /// </summary>
        /// <returns></returns>
        public string FormUri()
        {
            var list = new List<string>();

            if (ProviderName != null)
                list.Add($"{nameof(ProviderName)}={ProviderName}");
            if (ComputerName != null)
                list.Add($"{nameof(ComputerName)}={ComputerName}");
            if (Severity.HasValue)
                list.Add($"{nameof(Severity)}={Severity}");
            if (CreatedFromDate.HasValue)
                list.Add($"{nameof(CreatedFromDate)}={CreatedFromDate}");
            if (CreatedUntilDate.HasValue)
                list.Add($"{nameof(CreatedUntilDate)}={CreatedUntilDate}");

            return string.Join("&", list);
        }
    }
}
