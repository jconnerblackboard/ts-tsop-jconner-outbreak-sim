﻿namespace BbTS.Domain.Models.EventLog
{
    /// <summary>
    /// Container for a request to add event log.
    /// </summary>
    public class EventLogPostRequest
    {
        /// <summary>
        /// The unique identifier for this post request and response.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// Identifies the provider that logged the event.
        /// </summary>
        public string ProviderName { get; set; }

        /// <summary>
        /// The name of the computer on which the event occurred.
        /// </summary>
        public string ComputerName { get; set; }

        /// <summary>
        /// The task defined in the event.
        /// </summary>
        public short TaskCategory { get; set; }

        /// <summary>
        /// The identifier that the provider used to identify the event.
        /// </summary>
        public int EventId { get; set; }

        /// <summary>
        /// Contains the severity level of the event.
        /// </summary>
        public byte Severity { get; set; }

        /// <summary>
        /// The message string.
        /// </summary>
        public string Message { get; set; }
    }
}
