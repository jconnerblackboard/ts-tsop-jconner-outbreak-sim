﻿
namespace BbTS.Domain.Models.EventLog
{
    /// <summary>
    /// Container class for handling a server response to a get event log request.
    /// </summary>
    public class EventLogGetResponse
    {
        /// <summary>
        /// Event Log Id
        /// </summary>
        public int EventLogId { get; set; }

        /// <summary>
        /// Customer
        /// </summary>
        public TsEventLog EventLog { get; set; }
    }
}
