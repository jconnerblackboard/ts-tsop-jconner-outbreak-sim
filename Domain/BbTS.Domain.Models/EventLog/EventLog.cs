﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.EventLog
{
    /// <summary>
    /// This object represents a Transact eventlog object. Table of Event logs
    /// </summary>
    [Serializable]
    [DataContract]
    public class EventLog
    {
        /// <summary>
        /// System Generated Eveng log ID Number.
        /// </summary>
        [XmlAttribute]
        [DataMember]
        public int EventLogId { get; set; }
        /// <summary>
        /// Identifies the provider that logged the event.
        /// </summary>
        [XmlAttribute]
        [DataMember]
        public string ProviderName { get; set; }
        /// <summary>
        /// The name of the computer on which the event occurred.
        /// </summary>
        [XmlAttribute]
        [DataMember]
        public string ComputerName { get; set; }
        /// <summary>
        /// The identifier that the provider used to identify the event.
        /// </summary>
        [XmlAttribute]
        [DataMember]
        public int EventId { get; set; }
        /// <summary>
        /// Contains the severity level of the event.
        /// </summary>
        [XmlAttribute]
        [DataMember]
        public int Severity { get; set; }
        /// <summary>
        /// The task defined in the event.
        /// </summary>
        [XmlAttribute]
        [DataMember]
        public int TaskCategory { get; set; }
        /// <summary>
        /// The time stamp that identifies when the event was logged.
        /// </summary>
        [XmlAttribute]
        [DataMember]
        public DateTime CreatedDateTime { get; set; }
        /// <summary>
        /// The message string.
        /// </summary>
        [XmlAttribute]
        [DataMember]
        public string Message { get; set; }
    }
}