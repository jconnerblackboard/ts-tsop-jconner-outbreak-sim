﻿using System;

namespace BbTS.Domain.Models.EventLog
{
    /// <summary>
    /// Event log
    /// </summary>
    public class TsEventLogSummary
    {
        /// <summary>
        /// System Generated Eveng log ID Number.
        /// </summary>
        public int EventLogId { get; set; }

        /// <summary>
        /// Identifies the provider that logged the event.
        /// </summary>
        public string ProviderName { get; set; }

        /// <summary>
        /// The name of the computer on which the event occurred.
        /// </summary>
        public string ComputerName { get; set; }

        /// <summary>
        /// The identifier that the provider used to identify the event.
        /// </summary>
        public int EventId { get; set; }

        /// <summary>
        /// Contains the severity level of the event.
        /// </summary>
        public int Severity { get; set; }

        /// <summary>
        /// The time stamp that identifies when the event was logged.
        /// </summary>
        public DateTime CreatedDateTime { get; set; }
    }
}