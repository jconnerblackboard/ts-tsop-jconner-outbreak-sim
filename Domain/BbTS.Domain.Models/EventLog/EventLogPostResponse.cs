﻿
namespace BbTS.Domain.Models.EventLog
{
    /// <summary>
    /// Container for a response to a request for an event log post.
    /// </summary>
    public class EventLogPostResponse
    {
        /// <summary>
        /// The request that spawned this response.
        /// </summary>
        public EventLogPostRequest Request { get; internal set; }

        /// <summary>
        /// Parameterized constructor that requires the request id and the request.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="eventLog"></param>
        public EventLogPostResponse(EventLogPostRequest request)
        {
            Request = request;
        }
    }
}
