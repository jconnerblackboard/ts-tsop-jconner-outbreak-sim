﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BbTS.Domain.Models.MediaServer
{
    /// <summary>
    /// The playlist class
    /// </summary>
    public class Playlist
    {
        /// <summary>
        /// The identity of the playlist
        /// </summary>
        [Key]
        public Int32 Id { get; set; }

        /// <summary>
        /// The name of the playlist
        /// </summary>
        [Required]
        [DisplayName("Play List Name")]
        [StringLength(50, ErrorMessage = "Must be 50 characters or less.")]
        public String Name { get; set; }

        /// <summary>
        /// The items associated with this playlist
        /// </summary>
        [DisplayName("Play List Items")]
        public List<PlaylistItem> PlaylistItems { get; set; }
    }

    /// <summary>
    /// This overload is used by the Delete view
    /// </summary>
    public class PlaylistDelete : Playlist
    {
        /// <summary>
        /// A list of the device groups associated to this playlist
        /// </summary>
        public List<DeviceGroup> DeviceGroups { get; set; }
    }
}