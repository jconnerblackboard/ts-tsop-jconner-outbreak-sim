﻿using System;
using System.ComponentModel;

namespace BbTS.Domain.Models.MediaServer
{
    /// <summary>
    /// Pos object
    /// </summary>
    public class Pos
    {
        /// <summary>
        /// The identity of the record
        /// </summary>
        public Int32 PosId { get; set; }
        /// <summary>
        /// The name of the pos device
        /// </summary>
        public String Name { get; set; }
        /// <summary>
        /// The type of device this is
        /// </summary>
        public PosType PosType { get; set; }
        /// <summary>
        /// The mac address of the device
        /// </summary>
        public String MacAddress { get; set; }
        /// <summary>
        /// ProfitCenter name
        /// </summary>
        public String ProfitCenterName { get; set; }
        /// <summary>
        /// Merchant name
        /// </summary>
        public String MerchantName { get; set; }
        /// <summary>
        /// TS Device Group
        /// </summary>
        public String DeviceGroup { get; set; }
    }

    /// <summary>
    /// The type of pos device for text purposes
    /// </summary>
    public enum PosType
    {
        /// <summary>
        /// This is a VR4100 device
        /// </summary>
        [Description("VR4100")]
        VR4100 = 3,
        /// <summary>
        /// This is a MF4100 copy device
        /// </summary>
        [Description("MF4100 Copy")]
        MF4100C = 4,
        /// <summary>
        /// This is a MF4100 laundry device
        /// </summary>
        [Description("MF4100 Laundry")]
        MF4100L = 5
    }
}