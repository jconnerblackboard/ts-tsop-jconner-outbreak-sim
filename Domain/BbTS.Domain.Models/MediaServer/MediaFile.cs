﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BbTS.Domain.Models.MediaServer
{
    /// <summary>
    /// The MediaFile class
    /// </summary>
    public class MediaFile
    {
        /// <summary>
        /// The identity of the MediaFile
        /// </summary>
        public Int32 Id { get; set; }

        /// <summary>
        /// The name of MediaFile
        /// </summary>
        [Required]
        [DisplayName("Friendly Name")]
        [StringLength(100, ErrorMessage = "Must be 100 characters or less.")]
        public String Name { get; set; }

        /// <summary>
        /// The physical name of the file
        /// </summary>        
        [DisplayName("File Name")]
        [StringLength(100, ErrorMessage = "Must be 100 characters or less.")]
        public String FileName { get; set; }

        /// <summary>
        /// The size of the file
        /// </summary>
        [DisplayName("Size (bytes)")]
        public Int32 FileSizeBytes { get; set; }

        /// <summary>
        /// Where the file is located
        /// </summary>
        [DisplayName("File Location")]
        [StringLength(255, ErrorMessage = "Must be 255 characters or less.")]
        public String Location { get; set; }

        /// <summary>
        /// The mime type of the file
        /// </summary>
        [DisplayName("Content Type")]
        [StringLength(30, ErrorMessage = "Must be 30 characters or less.")]
        public String ContentType { get; set; }

        /// <summary>
        /// What type of device this file should be encoded for
        /// </summary>
        [Required]
        [DisplayName("Target Device")]
        public Int32 TargetDeviceType { get; set; }

        /// <summary>
        /// Whether the video encoding process is complete
        /// </summary>
        [DisplayName("Encoding Complete")]
        public Boolean IsConversionComplete { get; set; }
    }

    /// <summary>
    /// This overload is used by the delete view
    /// </summary>
    public class MediaFileDelete : MediaFile
    {
        /// <summary>
        /// The playlists associated with this file
        /// </summary>
        public List<Playlist> Playlists { get; set; }

        /// <summary>
        /// File size in megabytes
        /// </summary>
        [DisplayName("Size (MB)")]
        public Decimal FileSizeMb { get; set; }
    }

    /// <summary>
    /// MediaFile object with overridden attributes
    /// </summary>
    public class MediaFileDetail : MediaFile
    {
        /// <summary>
        /// The type of device
        /// </summary>
        [DisplayName("Target Device")]
        public TargetDeviceType TargetDeviceTypeText { get; set; }

        /// <summary>
        /// Supported File Types
        /// </summary>
        public String SupportedFileTypes { get; set; }

        /// <summary>
        /// Parent Id (MediaFileId)
        /// </summary>
        public Int32 ParentId { get; set; }

        /// <summary>
        /// Md5Hash
        /// </summary>
        public String Md5Hash { get; set; }

        /// <summary>
        /// Length of a video
        /// </summary>
        public Int32 VideoLength { get; set; }

        /// <summary>
        /// File size in megabytes
        /// </summary>
        [DisplayName("Size (MB)")]
        public Decimal FileSizeMb { get; set; }

        /// <summary>
        /// Whether to re-encode the video or not
        /// </summary>
        [DisplayName("Re-submit file for encoding")]
        public Boolean Reencode { get; set; }
    }

    /// <summary>
    /// The type of device
    /// </summary>
    public enum TargetDeviceType
    {
        /// <summary>
        /// The file should be converted for all device types
        /// </summary>
        [Description("All device types")]
        All = 0,
        /// <summary>
        /// Convert this file for a MF4100
        /// </summary>
        [Description("MF4100")]
        MF4100 = 1,
        /// <summary>
        /// Convert this file for a VR4100
        /// </summary>
        [Description("VR4100")]
        VR4100 = 2
    }
}