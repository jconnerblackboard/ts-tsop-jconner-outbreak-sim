﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BbTS.Domain.Models.MediaServer
{
    /// <summary>
    /// RssFeed object class
    /// </summary>
    public class RssFeed
    {
        /// <summary>
        /// The identity of the RssFeed record
        /// </summary>
        [Key]
        public Int32 Id { get; set; }

        /// <summary>
        /// The URL of the rss feed
        /// </summary>
        [Required]
        [DisplayName("RSS URL")]
        [StringLength(255, ErrorMessage = "Must be 255 characters or less.")]
        public String Url { get; set; }

        /// <summary>
        /// How often the device should check this feed.
        /// </summary>
        [Required]
        [DisplayName("Refresh interval (minutes)")]
        public Int32 CallBackFrequency { get; set; }

        /// <summary>
        /// The background color of this feed
        /// </summary>
        [Required]
        [DisplayName("The background color of the scroll area")]
        public String BackgroundColor { get; set; }

        /// <summary>
        /// The foreground color of this feed
        /// </summary>
        [Required]
        [DisplayName("The foreground color of the scroll area")]
        public String ForegroundColor { get; set; }

        /// <summary>
        /// The number of items to return to the device
        /// </summary>
        [Required]
        [DisplayName("Maximum number of RSS Feed Items")]
        public Int32 MaxItemCount { get; set; }

        /// <summary>
        /// The maximum length of the title
        /// </summary>
        [Required]
        [DisplayName("The maximum length of the Title attribute")]
        public Int32 MaxLengthTitle { get; set; }

        /// <summary>
        /// The maximum length of the description
        /// </summary>
        [Required]
        [DisplayName("The maximum length of the Description attribute")]
        public Int32 MaxLengthDescription { get; set; }

        /// <summary>
        /// Whether to display the title or not
        /// </summary>
        [DisplayName("Display the Title attribute")]
        public Int32 DisplayTitle { get; set; }

        /// <summary>
        /// Whether to display the description or not
        /// </summary>
        [DisplayName("Display the Description attribute")]
        public Int32 DisplayDescription { get; set; }

        /// <summary>
        /// Whether this is a high-priority (0) or normal-priority (1) item.
        /// </summary>
        [DisplayName("Priority of this feed")]
        public Int32 Priority { get; set; }

        /// <summary>
        /// The alert level of this feed item
        /// </summary>
        [DisplayName("Alert sound")]
        public Int32 Alert { get; set; }

    }

    /// <summary>
    /// This overload is for the delete RssFeed view
    /// </summary>
    public class RssFeedDelete : RssFeed
    {
        /// <summary>
        /// The DeviceGroups associated with this feed
        /// </summary>
        public List<DeviceGroup> DeviceGroups { get; set; }
    }
}