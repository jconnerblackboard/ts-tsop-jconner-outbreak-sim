﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BbTS.Domain.Models.MediaServer
{
    /// <summary>
    /// Device object
    /// </summary>
    public class Device
    {
        /// <summary>
        /// The identity of the device object
        /// </summary>
        [Required]
        public Int32 Id { get; set; }

        /// <summary>
        /// The name of the device
        /// </summary>
        [Required]
        [StringLength(50, ErrorMessage = "Must be 50 characters or less.")]
        public String Name { get; set; }

        /// <summary>
        /// The group the device belongs to
        /// </summary>
        [Required]
        public Int32 DeviceGroupId { get; set; }

        /// <summary>
        /// The PosId of the device
        /// </summary>
        [Required]
        public Int32 PosId { get; set; }
    }

    /// <summary>
    /// The device object with additional user friendly details
    /// </summary>
    public class DeviceView : Device
    {

        /// <summary>
        /// The name of the device group
        /// </summary>
        public String DeviceGroupName { get; set; }

        /// <summary>
        /// The type of device
        /// </summary>
        public Int32 Type { get; set; }

        /// <summary>
        /// The MAC Address of the device
        /// </summary>
        public String MacAddress { get; set; }

        /// <summary>
        /// The profit center name that the device belongs to
        /// </summary>
        public String ProfitCenterName { get; set; }
    }
}