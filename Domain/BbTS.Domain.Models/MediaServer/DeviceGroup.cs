﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BbTS.Domain.Models.MediaServer
{
    /// <summary>
    /// DeviceGroup class
    /// </summary>
    public class DeviceGroup
    {
        /// <summary>
        /// Initialize the colors to black on white
        /// </summary>
        public DeviceGroup()
        {
            ForegroundColor = "#000000";
            BackgroundColor = "#FFFFFF";
            OverrideMessageForegroundColor = "#000000";
            OverrideMessageBackgroundColor = "#FFFFFF";
        }

        /// <summary>
        /// The Id of the device group
        /// </summary>
        public Int32 Id { get; set; }

        /// <summary>
        /// The name of the device group
        /// </summary>
        [Required]
        [StringLength(50, ErrorMessage = "Must be 50 characters or less.")]
        [DisplayName("Device Group Name")]
        public String Name { get; set; }

        /// <summary>
        /// The playlist of the device group
        /// </summary>
        public Int32 PlayListId { get; set; }

        /// <summary>
        /// The background color of the status bar
        /// </summary>
        [DisplayName("The background color of the status bar")]
        public String BackgroundColor { get; set; }

        /// <summary>
        /// The foreground color of the status bar 
        /// </summary>
        [DisplayName("The foreground color of the status bar")]
        public String ForegroundColor { get; set; }

        /// <summary>
        /// The override message 
        /// </summary>
        [StringLength(100, ErrorMessage = "Must be 100 characters or less.")]
        [DisplayName("Idle Scroll Message")]
        public String ScrollOverrideMessage { get; set; }

        /// <summary>
        /// The background color of the override message
        /// </summary>
        [DisplayName("Idle scroll message background color")]
        public String OverrideMessageBackgroundColor { get; set; }

        /// <summary>
        /// The foreground color of the override message
        /// </summary>
        [DisplayName("Idle scroll message foreground color")]
        public String OverrideMessageForegroundColor { get; set; }
    }

    /// <summary>
    /// DeviceGroup Detail class
    /// </summary>
    public class DeviceGroupDetail : DeviceGroup
    {
        /// <summary>
        /// The name of the playlist of the device group
        /// </summary>
        [DisplayName("Play List")]
        public String PlayListName { get; set; }
    }

    /// <summary>
    /// PosDetail class
    /// </summary>
    public class PosDetail : Terminal.Pos
    {
        /// <summary>
        /// Whether a Pos device is selected or not
        /// </summary>
        public Boolean IsSelected { get; set; }
        /// <summary>
        /// Whether the item is in use
        /// </summary>
        public Boolean IsInUse { get; set; }
        /// <summary>
        /// The device group Id of the pos device
        /// </summary>
        public Int32? DeviceGroupId { get; set; }
        /// <summary>
        /// The name of the device group the pos device is assigned to
        /// </summary>
        public String AssignedDeviceGroup { get; set; }
    }

    /// <summary>
    /// RssFeedDeviceGroup class
    /// </summary>
    public class RssFeedDeviceGroup : RssFeed
    {
        /// <summary>
        /// Whether a Rss Feed is selected or not
        /// </summary>
        public Boolean IsSelected { get; set; }
    }
}