﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BbTS.Domain.Models.MediaServer
{
    /// <summary>
    /// The login class
    /// </summary>
    public class Login
    {
        /// <summary>
        /// The name of the user
        /// </summary>
        [Required]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        /// <summary>
        /// The password of the user
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        /// <summary>
        /// Whether or not the user should be remembered
        /// </summary>
        [Display(Name = "Remember me?")]
        public Boolean RememberMe { get; set; }
    }

    /// <summary>
    /// The class used for changing a users password
    /// </summary>
    public class PasswordChange : Login
    {

        /// <summary>
        /// These are the rules the password must adhere to
        /// </summary>
        public List<String> PasswordRules { get; set; }

        /// <summary>
        /// The original password of the user
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current Password")]
        public string PasswordCurrent { get; set; }

        /// <summary>
        /// The password confirmation to ensure passwords match
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password Confirmation")]
        public string PasswordConfirmation { get; set; }

        /// <summary>
        /// The password change required message 
        /// </summary>
        public String PasswordChangeRequiredMessage { get; set; }
    }
}