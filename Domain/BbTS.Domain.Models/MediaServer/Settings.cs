﻿using System;

namespace BbTS.Domain.Models.MediaServer
{
    /// <summary>
    /// Name/Value pair for simple serialization
    /// </summary>
    public class NameValuePair
    {
        /// <summary>
        /// The name in the name/value pair relationship
        /// </summary>
        public String Name { get; set; }
        /// <summary>
        /// The value in the name/value pair relationship
        /// </summary>
        public String Value { get; set; }
    }
}
