﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BbTS.Domain.Models.MediaServer
{
    /// <summary>
    /// PlaylistItem class
    /// </summary>
    public class PlaylistItem
    {
        /// <summary>
        /// The identity of the playlist item
        /// </summary>
        [Key]
        public Int32 Id { get; set; }

        /// <summary>
        /// The playlist this item is associated with
        /// </summary>
        [Required]
        public Int32 PlaylistId { get; set; }

        /// <summary>
        /// The media file that is associated to this playlist item
        /// </summary>
        [Required]
        [DisplayName("Media File")]
        public Int32 MediaFileId { get; set; }

        /// <summary>
        /// Media file name
        /// </summary>
        [DisplayName("Media Name")]
        public String MediaName { get; set; }

        /// <summary>
        /// Whether audio is enabled or not
        /// </summary>
        [Required]
        [DisplayName("Volume")]
        public Int32 Volume { get; set; }

        /// <summary>
        /// The sequence this item should be played in
        /// </summary>
        [Required]
        [DisplayName("Sequence")]
        public Int32 PlaySequenceId { get; set; }

        /// <summary>
        /// The start date a video is enabled
        /// </summary>
        [Required]
        [DisplayName("Start Date")]
        public DateTime StartDateTime { get; set; }

        /// <summary>
        /// Then end date for a video
        /// </summary>
        [Required]
        [DisplayName("End Date")]
        public DateTime EndDateTime { get; set; }

        /// <summary>
        /// How long a image item should be displayed
        /// </summary>
        [DisplayName("Image Display (seconds)")]
        public Int32 DisplayDuration { get; set; }

        /// <summary>
        /// This keeps track of the file size of the item
        /// </summary>
        [DisplayName("Size (MB)")]
        public Decimal FileSizeMb { get; set; }


        public String Md5Hash { get; set; }
        public Int32 VideoLength { get; set; }
    }

    /// <summary>
    /// The PlaylistItem detail class
    /// </summary>
    public class PlaylistItemDetail : PlaylistItem
    {
        /// <summary>
        /// The name of the playlist
        /// </summary>
        [DisplayName("Play List")]
        public String PlaylistName { get; set; }

        /// <summary>
        /// Whether this is an image or not
        /// </summary>
        public Boolean IsImage { get; set; }
    }
}