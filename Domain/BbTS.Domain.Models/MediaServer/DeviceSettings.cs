﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.MediaServer
{
    /// <summary>
    /// Device Settings class
    /// </summary>
    [XmlType("MediaServerDeviceSettings")]
    public class DeviceSettings
    {
        public Int32 DeviceId { get; set; }
        public DeviceGroup DeviceGroup { get; set; }
        public List<PlaylistItem> Playlist { get; set; }
        public List<RssFeed> RssFeeds { get; set; }

        /// <summary>
        /// The DeviceSettings
        /// </summary>
        public DeviceSettings()
        {
            MF4100Settings = new MF4100Setting();
            VR4100Settings = new VR4100Setting();
        }

        /// <summary>
        /// The settings for the MF4100
        /// </summary>
        [XmlElement("MF4100Settings")]
        public MF4100Setting MF4100Settings { get; set; }
        /// <summary>
        /// The settings for the VR4100
        /// </summary>
        [XmlElement("VR4100Settings")]
        public VR4100Setting VR4100Settings { get; set; }
    }

    /// <summary>
    /// The MF4100 settings class
    /// </summary>
    public class MF4100Setting
    {
        /// <summary>
        /// The initializer for MF4100 settings
        /// </summary>
        public MF4100Setting()
        {
            Interval = 130;
            PixelShiftCount = 40;
        }

        /// <summary>
        /// The scroll interval 
        /// </summary>
        [DisplayName("Scroll Interval (in milliseconds)")]
        [Range(50, 500, ErrorMessage = "Value must be between 50 and 500")]
        public Int32 Interval { get; set; }

        /// <summary>
        /// The number of pixels that are shifted
        /// </summary>
        [DisplayName("Number of pixels to shift each interval")]
        [Range(5, 100, ErrorMessage = "Value must be between 5 and 100")]
        public Int32 PixelShiftCount { get; set; }
    }

    /// <summary>
    /// The VR4100 settings class
    /// </summary>
    public class VR4100Setting
    {
        /// <summary>
        /// 
        /// </summary>
        public VR4100Setting()
        {
            Interval = 130;
            PixelShiftCount = 20;
        }

        /// <summary>
        /// The initializer for VR4100 settings
        /// </summary>
        [DisplayName("Scroll Interval (in milliseconds)")]
        [Range(50, 500, ErrorMessage = "Value must be between 50 and 500")]
        public Int32 Interval { get; set; }

        /// <summary>
        /// The number of pixels that are shifted
        /// </summary>
        [DisplayName("Number of pixels to shift each interval")]
        [Range(5, 100, ErrorMessage = "Value must be between 5 and 100")]
        public Int32 PixelShiftCount { get; set; }
    }

    public class ScrollSettings
    {
        public Int32 Interval { get; set; }
        public Int32 PixelShiftCount { get; set; }
    }
}