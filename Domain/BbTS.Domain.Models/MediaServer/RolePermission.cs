﻿using System;

namespace BbTS.Domain.Models.MediaServer
{
    /// <summary>
    /// RolePermission object
    /// </summary>
    public class RolePermission
    {
        /// <summary>
        /// Name of the RoleResource
        /// </summary>
        public String RoleResourceName { get; set; }
        /// <summary>
        /// Name of the RolePermission
        /// </summary>
        public String RolePermissionName { get; set; }
        /// <summary>
        /// Id of the Merchant
        /// </summary>
        public Int32 MerchantId { get; set; }
    }
}