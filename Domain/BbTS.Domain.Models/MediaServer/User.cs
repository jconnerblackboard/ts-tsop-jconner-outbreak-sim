﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Principal;

namespace BbTS.Domain.Models.MediaServer
{
    /// <summary>
    /// The base User object
    /// </summary>
    public class User
    {
        /// <summary>
        /// The user name
        /// </summary>
        [DisplayName("User Name")]
        public String UserName { get; set; }
        /// <summary>
        /// The full name of the user
        /// </summary>
        [DisplayName("Full Name")]
        public String FullName { get; set; }
        /// <summary>
        /// The status of the users account
        /// </summary>
        [DisplayName("Account Status")]
        public AccountStatus AccountStatus { get; set; }


    }

    /// <summary>
    /// This object represents the account status
    /// </summary>
    public class AccountStatus
    {
        /// <summary>
        /// The Domain that represents the account status
        /// </summary>
        public Int32 Domain { get; set; }
        /// <summary>
        /// The DomainId that represents the account status
        /// </summary>
        public Int32 DomainId { get; set; }
        /// <summary>
        /// The message of the account status
        /// </summary>
        public String Message { get; set; }
    }

    /// <summary>
    /// The User object augmented with security roles
    /// </summary>
    public class UserRoles : User
    {
        /// <summary>
        /// The roles associated with the user
        /// </summary>
        public List<int> Roles { get; set; }

        /// <summary>
        /// Session token
        /// </summary>
        public string SessionToken { get; set; }
    }

    /// <summary>
    /// The User object augmented with password details
    /// </summary>
    public class UserValidate : User
    {
        /// <summary>
        /// The hash of the user
        /// </summary>
        public Byte[] PasswordHash { get; set; }
        /// <summary>
        /// The hash salt of the users password
        /// </summary>
        public String PasswordHashSalt { get; set; }
        /// <summary>
        /// Whether the user is active or not
        /// </summary>
        [DisplayName("Is Active")]
        public Boolean IsActive { get; set; }
        /// <summary>
        /// Whether the password change is required
        /// </summary>
        [DisplayName("Password Change Required")]
        public Boolean PasswordChangeRequired { get; set; }
        /// <summary>
        /// Whether this user account is locked or not 
        /// </summary>
        [DisplayName("Is Locked")]
        public Boolean IsLocked { get; set; }
        /// <summary>
        /// Whether the password is valid or not 
        /// </summary>
        [DisplayName("Is Password Valid")]
        public Boolean IsPasswordValid { get; set; }
    }

    /// <summary>
    /// The principal object for this user
    /// </summary>
    public class UserPrincipal : IPrincipal
    {
        /// <summary>
        /// The UserPrincipal object
        /// </summary>
        /// <param name="identity"></param>
        public UserPrincipal(IIdentity identity)
        {
            Identity = identity;
        }

        /// <summary>
        /// IIdentity method
        /// </summary>
        public IIdentity Identity
        {
            get;
            private set;
        }

        /// <summary>
        /// The User Object including roles
        /// </summary>
        public UserRoles User { get; set; }

        /// <summary>
        /// This returns whether the user is in a particular role or not
        /// </summary>
        /// <param name="roles">The roles the user must have</param>
        /// <returns>Boolean</returns>
        public Boolean IsInRole(String roles)
        {
            // If the roles are populated, let them stand on their own.
            if(User.Roles != null)
                return roles.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Any(x => User.Roles.Contains((int)Enum.Parse(typeof(RolePermssionId),x.Trim(),true)));

            // There are no roles - therefore, not in role
            return false;
        }
    }
}