﻿using System;

namespace BbTS.Domain.Models.MediaServer
{

    /// <summary>
    /// The DeviceGroupRssFeed class
    /// </summary>
    public class DeviceGroupRssFeed
    {
        /// <summary>
        /// The identity of the DeviceGroupRssFeed item
        /// </summary>
        public Int32 Id { get; set; }
        /// <summary>
        /// The device group 
        /// </summary>
        public Int32 DeviceGroupId { get; set; }
        /// <summary>
        /// The Rss Feed id
        /// </summary>
        public Int32 RssFeedId { get; set; }
    }
}