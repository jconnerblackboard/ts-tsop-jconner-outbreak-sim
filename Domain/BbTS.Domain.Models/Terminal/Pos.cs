﻿using System;
using System.ComponentModel;

namespace BbTS.Domain.Models.Terminal {
    /// <summary>
    /// Pos object
    /// </summary>
    public class Pos {
        /// <summary>
        /// The identity of the record
        /// </summary>
        [DisplayName("POS Id")]
        public Int32 PosId { get; set; }
        /// <summary>
        /// The name of the pos device
        /// </summary>
        [DisplayName("POS Name")]
        public String Name { get; set; }
        /// <summary>
        /// The type of device this is
        /// </summary>
        [DisplayName("POS Type")]
        public PosType PosType { get; set; }
        /// <summary>
        /// The mac address of the device
        /// </summary>
        [DisplayName("MAC Address")]
        public String MacAddress { get; set; }
        /// <summary>
        /// ProfitCenter name
        /// </summary>
        [DisplayName("Profit Center Name")]
        public String ProfitCenterName { get; set; }
        /// <summary>
        /// Merchant name
        /// </summary>
        [DisplayName("Merchant Name")]
        public String MerchantName { get; set; }
        /// <summary>
        /// TS Device Group
        /// </summary>
        [DisplayName("Device Group")]
        public String DeviceGroup { get; set; }
    }

    /// <summary>
    /// The type of pos device for text purposes
    /// </summary>
    public enum PosType {
        /// <summary>
        /// This is a register device
        /// </summary>
        [Description("Register")]      
        Register = 1,
        /// <summary>
        /// This is a transation terminal
        /// </summary>
        [Description("Transaction Terminal")]    
        TransactionTerminal = 2,
        /// <summary>
        /// This is a VR4100 device
        /// </summary>
        [Description("VR4100")]
        // ReSharper disable once InconsistentNaming
        VR4100 = 3,
        /// <summary>
        /// This is a MF4100 copy device
        /// </summary>
        [Description("MF4100 Copy")]
        // ReSharper disable once InconsistentNaming
        MF4100C = 4,
        /// <summary>
        /// This is a MF4100 laundry device
        /// </summary>
        [Description("MF4100 Laundry")]
        // ReSharper disable once InconsistentNaming
        MF4100L = 5,
        /// <summary>
        /// This is unassigned at the moment
        /// </summary>
        [Description("Unassigned")]    
        Unassigned = 6,
        /// <summary>
        /// This is a wireless device
        /// </summary>
        [Description("Wireless terminal")]    
        WirelessTerminal = 7,
        /// <summary>
        /// This is a market place device
        /// </summary>
        [Description("Marketplace")]    
        MarketPlace = 8,
        /// <summary>
        /// This is a Transaction integration agent device
        /// </summary>
        [Description("Transaction Integration Agent")]    
        Tia = 9,
        /// <summary>
        /// This is an account management center device
        /// </summary>
        [Description("Account Management Center")]    
        Amc = 10,
        /// <summary>
        /// This is a Workstation device
        /// </summary>
        [Description("Workstation")]    
        Workstation = 11,
        /// <summary>
        /// This is a value transfer station
        /// </summary>
        [Description("Value Transfer Station")]    
        Vts = 12,
        /// <summary>
        /// This is an external client
        /// </summary>
        [Description("External Client")]    
        ExternalClient = 13,
        /// <summary>
        /// This is a gate fee devive
        /// </summary>
        [Description("Gate Fee")]    
        GateFee = 14
    }
}