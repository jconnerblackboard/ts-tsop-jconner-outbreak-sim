﻿using System;

namespace BbTS.Domain.Models.Terminal
{
    /// <summary>
    /// Container class to handle fetching EMV settings for HIT operations from the resource layer.
    /// </summary>
    public class EmvHitSettings : IEquatable<EmvHitSettings>
    {
        /// <summary>
        /// Name of EMV Environment in use
        /// </summary>
        public string Environment { get; set; }

        /// <summary>
        /// The url for HIT operations.
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Username for HIT operations.
        /// </summary>
        public string HitUserName { get; set; }

        /// <summary>
        /// Key for HIT operations.
        /// </summary>
        public string HitKey { get; set; }

        /// <summary>
        /// Station for associated terminal.
        /// </summary>
        public string Station { get; set; }

        /// <summary>
        /// The currency code in use.
        /// </summary>
        public string CurrencyCode { get; set; }

        /// <summary>
        /// The device id.
        /// </summary>
        public string DeviceId { get; set; }

        /// <summary>
        /// The vendor id.
        /// </summary>
        public string VendorId { get; set; }

        /// <summary>
        /// Equality operator.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(EmvHitSettings other)
        {
            return
                other != null &&
                Environment == other.Environment &&
                Url == other.Url &&
                HitUserName == other.HitUserName &&
                HitKey == other.HitKey &&
                Station == other.Station &&
                CurrencyCode == other.CurrencyCode &&
                DeviceId == other.DeviceId &&
                VendorId == other.VendorId;
        }
    }

    /// <summary>
    /// View for a <see cref="EmvHitSettings"/>.  (Version 1)
    /// </summary>
    public class EmvHitSettingsViewV01
    {
        /// <summary>
        /// Name of EMV Environment in use
        /// </summary>
        public string Environment { get; set; }

        /// <summary>
        /// The url for HIT operations.
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Username for HIT operations.
        /// </summary>
        public string HitUserName { get; set; }

        /// <summary>
        /// Key for HIT operations.
        /// </summary>
        public string HitKey { get; set; }

        /// <summary>
        /// Station for associated terminal.
        /// </summary>
        public string Station { get; set; }

        /// <summary>
        /// The currency code in use.
        /// </summary>
        public string CurrencyCode { get; set; }

        /// <summary>
        /// The device id.
        /// </summary>
        public string DeviceId { get; set; }

        /// <summary>
        /// The vendor id.
        /// </summary>
        public string VendorId { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="EmvHitSettings"/> conversion.
    /// </summary>
    public static class EmvHitSettingsConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="EmvHitSettingsViewV01"/> object based on this <see cref="EmvHitSettings"/>.
        /// </summary>
        /// <param name="emvHitSettings"></param>
        /// <returns></returns>
        public static EmvHitSettingsViewV01 ToEmvHitSettingsViewV01(this EmvHitSettings emvHitSettings)
        {
            if (emvHitSettings == null) return null;

            return new EmvHitSettingsViewV01
            {
                Environment = emvHitSettings.Environment,
                Url = emvHitSettings.Url,
                HitUserName = emvHitSettings.HitUserName,
                HitKey = emvHitSettings.HitKey,
                Station = emvHitSettings.Station,
                CurrencyCode = emvHitSettings.CurrencyCode,
                DeviceId = emvHitSettings.DeviceId,
                VendorId = emvHitSettings.VendorId
            };
        }

        /// <summary>
        /// Returns a <see cref="EmvHitSettings"/> object based on this <see cref="EmvHitSettingsViewV01"/>.
        /// </summary>
        /// <param name="emvHitSettingsViewV01"></param>
        /// <returns></returns>
        public static EmvHitSettings ToEmvHitSettings(this EmvHitSettingsViewV01 emvHitSettingsViewV01)
        {
            if (emvHitSettingsViewV01 == null) return null;

            return new EmvHitSettings
            {
                Environment = emvHitSettingsViewV01.Environment,
                Url = emvHitSettingsViewV01.Url,
                HitUserName = emvHitSettingsViewV01.HitUserName,
                HitKey = emvHitSettingsViewV01.HitKey,
                Station = emvHitSettingsViewV01.Station,
                CurrencyCode = emvHitSettingsViewV01.CurrencyCode,
                DeviceId = emvHitSettingsViewV01.DeviceId,
                VendorId = emvHitSettingsViewV01.VendorId
            };
        }
        #endregion
    }
}
