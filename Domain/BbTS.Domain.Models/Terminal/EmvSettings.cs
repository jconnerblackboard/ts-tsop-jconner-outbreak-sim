﻿using System;

namespace BbTS.Domain.Models.Terminal
{
    /// <summary>
    /// Container class to handle fetching EMV settings from the resource layer.
    /// </summary>
    public class EmvSettings : IEquatable<EmvSettings>
    {
        /// <summary>
        /// The identity of the device.
        /// </summary>
        public int Identity { get; set; }

        /// <summary>
        /// The name of the device.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The address of the device.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// The device type.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// The port the device operates on.
        /// </summary>
        public string Port { get; set; }

        /// <summary>
        ///  The device priority.
        /// </summary>
        public string Priority { get; set; }

        /// <summary>
        /// Is the device enabled.
        /// </summary>
        public int Enabled { get; set; }

        /// <summary>
        /// The time in seconds before the device communication goes idle and disconnects.
        /// </summary>
        public int IdleDisconnectTimeout { get; set; }

        /// <summary>
        /// The currency code in use.
        /// </summary>
        public string CurrencyCode { get; set; }

        /// <summary>
        /// The device id.
        /// </summary>
        public string DeviceId { get; set; }

        /// <summary>
        /// The vendor id.
        /// </summary>
        public string VendorId { get; set; }

        /// <summary>
        /// Equality operator.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(EmvSettings other)
        {
            return
                other != null &&
                Identity == other.Identity &&
                Name == other.Name &&
                Address == other.Address &&
                Type == other.Type &&
                Port == other.Port &&
                Priority == other.Priority &&
                Enabled == other.Enabled &&
                CurrencyCode == other.CurrencyCode &&
                DeviceId == other.DeviceId &&
                VendorId == other.VendorId &&
                IdleDisconnectTimeout == other.IdleDisconnectTimeout;

        }
    }
}
