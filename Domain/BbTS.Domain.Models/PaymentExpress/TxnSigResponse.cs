﻿namespace BbTS.Domain.Models.PaymentExpress
{
    public class TxnSigResponse
    {
        public string CmdSeq { get; set; }
        public string ReCo { get; set; }
    }
}
