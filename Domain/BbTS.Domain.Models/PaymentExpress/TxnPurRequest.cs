﻿namespace BbTS.Domain.Models.PaymentExpress
{
    public class TxnPurRequest
    {
        public string TxnRef { get; set; }
        public string Amount { get; set; }  //Numeric value with implied decimal point according to the transaction currency. Value range 0-9999999
        public string MerchantReference { get; set; }
        public string BillingId { get; set; }
        public string SlotId { get; set; }
        public string OneSwipeFlag { get; set; }
        public string OemDataFormat { get; set; }
        public string OemData { get; set; }
        public string CashOutAmount { get; set; }
    }
}
