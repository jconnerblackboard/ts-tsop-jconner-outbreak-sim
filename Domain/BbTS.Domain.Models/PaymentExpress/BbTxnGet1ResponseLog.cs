using System;
using BbTS.Domain.Models.System;

namespace BbTS.Domain.Models.PaymentExpress
{
    public class BbTxnGet1ResponseLog : TxnGet1Response
    {
        public ActionResultToken Result { get; set; }
        public DateTime TimeStamp { get; set; }

        public BbTxnGet1ResponseLog()
        {
            //parameter-less constructor for serialization purposes
        }

        public BbTxnGet1ResponseLog(TxnGet1Response response)
        {
            CmdSeq = response.CmdSeq;
            ReCo = response.ReCo;
            CardSuffix = response.CardSuffix;
            CardId = response.CardId;
            AmountRequested = response.AmountRequested;
            AmountAuthorized = response.AmountAuthorized;
            TxnState = response.TxnState;
            CardNumber2 = response.CardNumber2;
            DpsBillingId = response.DpsBillingId;
            Stan = response.Stan;
            SettlementDate = response.SettlementDate;
            AuthCode = response.AuthCode;
            CardHolderName = response.CardHolderName;
            DpsTxnRef = response.DpsTxnRef;
            TxnReCo = response.TxnReCo;
            MerchantReference = response.MerchantReference;
            TxnType = response.TxnType;
            TxnTime = response.TxnTime;
            MaskedPan = response.MaskedPan;
            CardExpiry = response.CardExpiry;
            AmountSurcharge = response.AmountSurcharge;
            AmountCashOut = response.AmountCashOut;
            SignatureState = response.SignatureState;
            TxnRef = response.TxnRef;
        }
    }
}
