﻿using System;
using BbTS.Domain.Models.System;

namespace BbTS.Domain.Models.PaymentExpress
{
    public class BbTxnPurResponseLog : TxnPurResponse
    {
        public ActionResultToken Result { get; set; }
        public DateTime TimeStamp { get; set; }

        public BbTxnPurResponseLog()
        {
            //parameter-less constructor for serialization purposes
        }

        public BbTxnPurResponseLog(TxnPurResponse response)
        {
            TxnRef = response.TxnRef;
            ReCo = response.ReCo;
            Amount = response.Amount;
            DpsTxnRef = response.DpsTxnRef;
            SurchargeAmount = response.SurchargeAmount;
            CashOutAmount = response.CashOutAmount;
            ResultPromptId = response.ResultPromptId;
            SignatureRequired = response.SignatureRequired;
            GratuityAmount = response.GratuityAmount;
        }
    }
}
