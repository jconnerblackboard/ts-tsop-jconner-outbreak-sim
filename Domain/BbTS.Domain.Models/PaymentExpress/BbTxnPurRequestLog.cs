﻿using System;

namespace BbTS.Domain.Models.PaymentExpress
{
    public class BbTxnPurRequestLog : TxnPurRequest
    {
        public DateTime TimeStamp { get; set; }
        public string DeviceId { get; set; }

        public BbTxnPurRequestLog()
        {
            //parameter-less constructor for serialization purposes
        }

        public BbTxnPurRequestLog(TxnPurRequest request)
        {
            TxnRef = request.TxnRef;
            Amount = request.Amount;
            MerchantReference = request.MerchantReference;
            BillingId = request.BillingId;
            SlotId = request.SlotId;
            OneSwipeFlag = request.OneSwipeFlag;
            OemDataFormat = request.OemDataFormat;
            OemData = request.OemData;
            CashOutAmount = request.CashOutAmount;
        }
    }
}
