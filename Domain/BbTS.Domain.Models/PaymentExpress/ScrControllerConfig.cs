﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.PaymentExpress
{
    [XmlRoot(ElementName = "xml")] //This is what PxScrController.exe expects to see as the xml root.
    public class ScrControllerConfig
    {
        public ScrControllerServerTag Server { get; set; }

        [XmlArrayItem(ElementName = "HostInterface")]
        public List<ScrControllerHostInterfaceTag> HostInterfaces { get; set; }

        public ScrControllerConfig()
        {
            HostInterfaces = new List<ScrControllerHostInterfaceTag>();
        }

        public ScrControllerConfig DeepCopy()
        {
            ScrControllerConfig deepCopy = new ScrControllerConfig
            {
                Server = Server.ShallowCopy(),
                HostInterfaces = new List<ScrControllerHostInterfaceTag>(HostInterfaces.Select(item => item.ShallowCopy()))
            };
            return deepCopy;
        }

        public bool Matches(ScrControllerConfig other)
        {
            return
                other != null &&
                Server.Matches(other.Server) &&
                HostInterfaces.Count == other.HostInterfaces.Count &&
                HostInterfaces.Where((t, i) => t.Matches(other.HostInterfaces[i])).Any();
        }
    }

    [XmlRoot(ElementName = "Server")] //This is what PxScrController.exe expects to see
    public class ScrControllerServerTag
    {
        public string LogDrive { get; set; }
        public string LogLevel { get; set; }
        public string ClientPort { get; set; }
        public string XmlClientPort { get; set; }
        public string DeviceId { get; set; }
        public string VendorId { get; set; }
        public string Currency { get; set; }
        public string ProtocolVersion { get; set; }
        public string EventMask { get; set; }
        public string SignatureSupport { get; set; }
        public string ComPort { get; set; }
        public string BaudRate { get; set; }
        public string EnableOffline { get; set; }
        public string DefaultIdlePromptId { get; set; }
        public string DefaultIdlePromptBacklight { get; set; }
		public string UseScrIdlePromptManagement { get; set; }
		public string UseScrTxnResultDisplayManagement { get; set; }
		public string TxnSuccessResultDisplaySeconds { get; set; }
		public string TxnErrorResultDisplaySeconds { get; set; }		

        public ScrControllerServerTag ShallowCopy()
        {
            return (ScrControllerServerTag)MemberwiseClone();
        }

        public bool Matches(ScrControllerServerTag other)
        {
            return
                other != null &&
                LogDrive == other.LogDrive &&
                LogLevel == other.LogLevel &&
                ClientPort == other.ClientPort &&
                XmlClientPort == other.XmlClientPort &&
                DeviceId == other.DeviceId &&
                VendorId == other.VendorId &&
                Currency == other.Currency &&
                ProtocolVersion == other.ProtocolVersion &&
                EventMask == other.EventMask &&
                SignatureSupport == other.SignatureSupport &&
                ComPort == other.ComPort &&
                BaudRate == other.BaudRate &&
                EnableOffline == other.EnableOffline &&
                DefaultIdlePromptId == other.DefaultIdlePromptId &&
                DefaultIdlePromptBacklight == other.DefaultIdlePromptBacklight &&
                UseScrIdlePromptManagement == other.UseScrIdlePromptManagement &&
                UseScrTxnResultDisplayManagement == other.UseScrTxnResultDisplayManagement &&
                TxnSuccessResultDisplaySeconds == other.TxnSuccessResultDisplaySeconds &&
                TxnErrorResultDisplaySeconds == other.TxnErrorResultDisplaySeconds;
        }
    }

    [XmlRoot(ElementName = "HostInterface")] //This is what PxScrController.exe expects to see
    public class ScrControllerHostInterfaceTag
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Address { get; set; }
        public string Port { get; set; }
        public string Priority { get; set; }
        public string Enabled { get; set; }
        
        /// <summary>
        /// Idle disconnect time in seconds.
        /// </summary>
        public string IdleDisconnectTimeout { get; set; }

        public ScrControllerHostInterfaceTag ShallowCopy()
        {
            return (ScrControllerHostInterfaceTag)MemberwiseClone();
        }

        public bool Matches(ScrControllerHostInterfaceTag other)
        {
            return
                other != null &&
                Id == other.Id &&
                Name == other.Name &&
                Type == other.Type &&
                Address == other.Address &&
                Port == other.Port &&
                Priority == other.Priority &&
                Enabled == other.Enabled &&
                IdleDisconnectTimeout == other.IdleDisconnectTimeout;
        }
    }
}
