﻿using System;
using BbTS.Domain.Models.System;

namespace BbTS.Domain.Models.PaymentExpress
{
    public class BbTxnSigResponseLog : TxnSigResponse
    {
        public ActionResultToken Result { get; set; }
        public DateTime TimeStamp { get; set; }
        public string TxnRef { get; set; }

        public BbTxnSigResponseLog()
        {
            //parameter-less constructor for serialization purposes
        }

        public BbTxnSigResponseLog(TxnSigResponse response)
        {
            CmdSeq = response.CmdSeq;
            ReCo = response.ReCo;
        }
    }
}
