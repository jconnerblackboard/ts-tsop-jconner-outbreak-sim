﻿namespace BbTS.Domain.Models.PaymentExpress
{
    public class TxnPurResponse
    {
        /// <summary>
        /// Transaction reference
        /// </summary>
        public string TxnRef { get; set; }
        /// <summary>
        /// Response code
        /// </summary>
        public string ReCo { get; set; }
        /// <summary>
        /// Purchase amount
        /// </summary>
        public string Amount { get; set; } 
        /// <summary>
        /// Transaction reference given by DPS
        /// </summary>
        public string DpsTxnRef { get; set; }
        /// <summary>
        /// Any surcharge amounts
        /// </summary>
        public string SurchargeAmount { get; set; }
        /// <summary>
        /// Net amount of cash received for this transaction
        /// </summary>
        public string CashOutAmount { get; set; }
        /// <summary>
        /// This field indicates the prompt ID to indicate to the user the result of their transaction.
        /// </summary>
        public string ResultPromptId { get; set; }
        /// <summary>
        /// This can be 0 or 1.  If it is 1, the POS must print a receipt signature and have the cardholder’s signature validated by the POS operator.
        /// </summary>
        public string SignatureRequired { get; set; }
        /// <summary>
        /// This field contains the amount of gratuity included in the authorized amount.
        /// </summary>
        public string GratuityAmount { get; set; }
    }
}
