﻿namespace BbTS.Domain.Models.PaymentExpress
{
    public class TxnGet1Response
    {
        public string CmdSeq { get; set; }
        public string ReCo { get; set; }
        public string CardSuffix { get; set; }
        public string CardId { get; set; }
        public string AmountRequested { get; set; }
        public string AmountAuthorized { get; set; }
        public string TxnState { get; set; }
        public string CardNumber2 { get; set; }
        public string DpsBillingId { get; set; }
        public string Stan { get; set; }
        public string SettlementDate { get; set; }
        public string AuthCode { get; set; }
        public string CardHolderName { get; set; }
        public string DpsTxnRef { get; set; }
        public string TxnReCo { get; set; }
        public string MerchantReference { get; set; }
        public string TxnType { get; set; }
        public string TxnTime { get; set; }
        public string MaskedPan { get; set; }
        public string CardExpiry { get; set; }
        public string AmountSurcharge { get; set; }
        public string AmountCashOut { get; set; }
        public string SignatureState { get; set; }
        public string TxnRef { get; set; }
    }
}
