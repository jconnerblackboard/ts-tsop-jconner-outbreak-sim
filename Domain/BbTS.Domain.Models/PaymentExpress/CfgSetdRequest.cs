﻿namespace BbTS.Domain.Models.PaymentExpress
{
    public class CfgSetdRequest
    {
        public string CmdSeq { get; set; }
        public string DeviceId { get; set; }
        public string CurrencyCode { get; set; }
        public string ProtocolVersionPos { get; set; }
        public string VendorId { get; set; }
        public string EventMask { get; set; }
        public string EnableOffline { get; set; }
        public string SignatureSupported { get; set; }
        public string TxnResultDisplaySec { get; set; }
    }
}
