﻿using System;
using BbTS.Domain.Models.System;

namespace BbTS.Domain.Models.PaymentExpress
{
    public class BbTxnRefResponseLog : TxnRefResponse
    {
        public ActionResultToken Result { get; set; }
        public DateTime TimeStamp { get; set; }

        public BbTxnRefResponseLog()
        {
            //parameter-less constructor for serialization purposes
        }

        public BbTxnRefResponseLog(TxnRefResponse response)
        {
            TxnRef = response.TxnRef;
            ReCo = response.ReCo;
            Amount = response.Amount;
            DpsTxnRef = response.DpsTxnRef;
            ResultPromptId = response.ResultPromptId;
            SignatureRequired = response.SignatureRequired;
        }
    }
}
