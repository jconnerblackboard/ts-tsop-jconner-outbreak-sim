﻿namespace BbTS.Domain.Models.PaymentExpress
{
    public class TxnSigRequest
    {
        public string CmdSeq { get; set; }
        public string SignatureResult { get; set; }  //00 if the signature is accepted, or Z9 if the signature is declined.
        public string SlotId { get; set; }
    }
}
