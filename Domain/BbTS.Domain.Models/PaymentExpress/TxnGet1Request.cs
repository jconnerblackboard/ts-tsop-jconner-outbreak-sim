﻿namespace BbTS.Domain.Models.PaymentExpress
{
    public class TxnGet1Request
    {
        public string CmdSeq { get; set; }
        public string SlotId { get; set; }
    }
}
