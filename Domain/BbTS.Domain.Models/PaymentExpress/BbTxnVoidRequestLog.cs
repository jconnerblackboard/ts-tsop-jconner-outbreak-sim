﻿using System;

namespace BbTS.Domain.Models.PaymentExpress
{
    public class BbTxnVoidRequestLog : TxnVoidRequest
    {
        public DateTime TimeStamp { get; set; }
        public string TxnRef { get; set; }

        public BbTxnVoidRequestLog()
        {
            //parameter-less constructor for serialization purposes
        }

        public BbTxnVoidRequestLog(TxnVoidRequest request)
        {
            CmdSeq = request.CmdSeq;
            SlotId = request.SlotId;
        }
    }
}
