﻿using System;
using BbTS.Domain.Models.System;

namespace BbTS.Domain.Models.PaymentExpress
{
    public class BbTxnVoidResponseLog : TxnVoidResponse
    {
        public ActionResultToken Result { get; set; }
        public DateTime TimeStamp { get; set; }

        public BbTxnVoidResponseLog()
        {
            //parameter-less constructor for serialization purposes
        }

        public BbTxnVoidResponseLog(TxnVoidResponse response)
        {
            CmdSeq = response.CmdSeq;
            ReCo = response.ReCo;
            TxnRef = response.TxnRef;
        }
    }
}
