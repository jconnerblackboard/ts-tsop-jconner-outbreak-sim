using System;
using BbTS.Domain.Models.System;

namespace BbTS.Domain.Models.PaymentExpress
{
    public class BbTxnGetrResponseLog : TxnGetrResponse
    {
        public ActionResultToken Result { get; set; }
        public DateTime TimeStamp { get; set; }

        public BbTxnGetrResponseLog()
        {
            //parameter-less constructor for serialization purposes
        }

        public BbTxnGetrResponseLog(TxnGetrResponse response)
        {
            CmdSeq = response.CmdSeq;
            ReCo = response.ReCo;
            ReceiptText = response.ReceiptText;
            LineNumber = response.LineNumber;
            LineCount = response.LineCount;
            TxnRef = response.TxnRef;
            Width = response.Width;
        }
    }
}
