﻿namespace BbTS.Domain.Models.PaymentExpress
{
    public class TxnVoidResponse
    {
        public string CmdSeq { get; set; }
        public string ReCo { get; set; }
        public string TxnRef { get; set; }
    }
}
