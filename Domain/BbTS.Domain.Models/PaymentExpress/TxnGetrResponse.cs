﻿namespace BbTS.Domain.Models.PaymentExpress
{
    public class TxnGetrResponse
    {
        public string CmdSeq { get; set; }
        public string ReCo { get; set; }
        public string ReceiptText { get; set; }
        public string LineNumber { get; set; }
        public string LineCount { get; set; }
        public string TxnRef { get; set; }
        public string Width { get; set; }
    }
}
