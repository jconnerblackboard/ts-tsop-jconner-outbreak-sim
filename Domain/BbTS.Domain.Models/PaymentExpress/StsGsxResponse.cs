﻿namespace BbTS.Domain.Models.PaymentExpress
{
    public class StsGsxResponse
    {
        public string CmdSeq { get; set; }
        public string ReCo { get; set; }
        public string SignedOn { get; set; }
        public string SignOnTime { get; set; }
        public string TimeZone { get; set; }
        public string Serial { get; set; }
        public string CurrencyId { get; set; }
        public string Timeout { get; set; }
        public string DpsPinPad { get; set; }
        public string FirmwareVersion { get; set; }
        public string ScrRemovalDetection { get; set; }
        public string SkpRemovalDetection { get; set; }
        public string ContactlessAntenna { get; set; }
        public string SimDetection { get; set; }
        public string StationId { get; set; }
    }
}
