﻿namespace BbTS.Domain.Models.PaymentExpress
{
    public class TxnRefResponse
    {
        public string TxnRef { get; set; }
        public string ReCo { get; set; }
        public string Amount { get; set; } 
        public string DpsTxnRef { get; set; }
        public string ResultPromptId { get; set; }
        public string SignatureRequired { get; set; }
    }
}
