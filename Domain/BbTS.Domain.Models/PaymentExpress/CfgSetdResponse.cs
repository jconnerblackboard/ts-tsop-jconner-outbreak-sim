﻿namespace BbTS.Domain.Models.PaymentExpress
{
    public class CfgSetdResponse
    {
        public string CmdSeq { get; set; }
        public string ReCo { get; set; }
        public string ProtocolVersionScr { get; set; }
        public string VendorId { get; set; }
        public string EventMask { get; set; }
        public string OfflineEnabled { get; set; }
    }
}
