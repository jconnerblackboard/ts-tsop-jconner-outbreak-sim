﻿namespace BbTS.Domain.Models.PaymentExpress
{
    public class TxnVoidRequest
    {
        public string CmdSeq { get; set; }
        public string SlotId { get; set; }
    }
}
