﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BbTS.Domain.Models.PaymentExpress
{
    public class PaymentExpressRequestResponse
    {
        [DisplayName("Identity")]
        public long PaymentExpressReqRespId { get; set; }
        [DisplayName("Transaction Reference")]
        public String TransactionReference { get; set; }
        [DisplayName("POS Id")]
        public Int32 TerminalId { get; set; }
        [DisplayName("Merchant Reference")]
        public String MerchantReference { get; set; }
        [DisplayName("Group Account Id")]
        public long PaymentExpressGroupAccountId { get; set; }
        [DisplayName("EMV Device Id")]
        public String EmvDeviceId { get; set; }
        [DisplayFormat(DataFormatString = "{0:C}")]
        [DisplayName("Amount Requested")]
        public Decimal AmountRequested { get; set; }
        [DisplayName("Purchase Response Code")]
        public String PurchaseResponseCode { get; set; }
        [DisplayName("Request Date/Time")]
        public DateTime RequestDateTime { get; set; }
        [DisplayName("Response Date/Time")]
        public DateTime ResponseDateTime { get; set; }
        [DisplayName("Card Number")]
        public String MaskedPan { get; set; }
        [DisplayName("Card Id")]
        public Int32 CardId { get; set; }
        [DisplayName("Command Sequence")]
        public Int32 CommandSequence { get; set; }
        [DisplayName("Transaction State Id")]
        public Int32 TransactionStateId { get; set; }
        [DisplayName("STAN")]
        public Int32 Stan { get; set; }
        [DisplayName("Settlement Date")]
        public DateTime SettlementDate { get; set; }
        [DisplayName("Authorization Code")]
        public String AuthorizationCode { get; set; }
        [DisplayName("Void Response Code")]
        public String VoidResponseCode { get; set; }
        [DisplayName("Void Request Date/Time")]
        public DateTime VoidRequestDateTime { get; set; }
        [DisplayName("Void Response Date/Time")]
        public DateTime VoidResponseDateTime { get; set; }
        [DisplayName("Blackboard Transaction Id")]
        public long TransactionId { get; set; }
        [DisplayName("In Process")]
        public Int32 InProcess { get; set; }
        [DisplayName("Reconciliation State Id")]
        public Int32 ReconciliationStateId { get; set; }
    }

    public class PaymentExpressRequestResponseView : PaymentExpressRequestResponse
    {
        [DisplayName("POS Name")]
        public String TerminalName { get; set; }
        [DisplayName("Group Account Name")]
        public String PaymentExpressGroupAccountName { get; set; }
        [DisplayFormat(DataFormatString = "{0:C}")]
        [DisplayName("Amount Authorized")]
        public Decimal AmountAuthorized { get; set; }
        [DisplayName("Purchase Response Value")]
        public String PurchaseResponseCodeValue { get; set; }
        [DisplayName("DPS Transaction Reference")]
        public String DpsTransactionReference { get; set; }
        [DisplayName("Card Suffix")]
        public String CardSuffix { get; set; }
        [DisplayName("Card Name")]
        public String CardName { get; set; }
        [DisplayName("Transaction State")]
        public String TransactionStateName { get; set; }
        [DisplayName("Transaction Response Code")]
        public String TransactionResponseCode { get; set; }
        [DisplayName("Transaction Response")]
        public String TransactionResponseValue { get; set; }
        [DisplayName("Transaction Type Id")]
        public Int32 TransactionTypeId { get; set; }
        [DisplayName("Transaction Type")]
        public String TransactionTypeName { get; set; }
        [DisplayName("Response Domain")]
        public Int32 RespActionResultTokenDomain { get; set; }
        [DisplayName("Response Domain Id")]
        public Int32 RespActionResultTokenDomainId { get; set; }
        [DisplayName("Response Token")]
        public String RespActionResultTokenId { get; set; }
        [DisplayName("Response Token Message")]
        public String RespActionResultTokenMessage { get; set; }
        [DisplayName("Card Info Date/Time")]
        public DateTime Get1DateTime { get; set; }
        [DisplayName("Card Info Domain")]
        public Int32 Get1ActionResultTokenDomain { get; set; }
        [DisplayName("Card Info Domain Id")]
        public Int32 Get1ActionResultTokenDomainId { get; set; }
        [DisplayName("Card Info Token")]
        public String Get1ActionResultTokenId { get; set; }
        [DisplayName("Card Info Token Message")]
        public String Get1ActionResultTokenMessage { get; set; }
        [DisplayName("Void Response")]
        public String VoidResponseValue { get; set; }
        [DisplayName("Void Domain")]
        public Int32 VoidActionResultTokenDomain { get; set; }
        [DisplayName("Void Domain Id")]
        public Int32 VoidActionResultTokenDomainId { get; set; }
        [DisplayName("Void Token")]
        public String VoidActionResultTokenId { get; set; }
        [DisplayName("Void Token Message")]
        public String VoidActionResultTokenMessage { get; set; }
        [DisplayName("Reconciliation State")]
        public String ReconciliationStateName { get; set; }           
    }
}
