﻿namespace BbTS.Domain.Models.PaymentExpress
{
    public class TxnGetrRequest
    {
        public string CmdSeq { get; set; }
        public string LineNumber { get; set; }
        public string LineCount { get; set; }
        public string SlotId { get; set; }
        public string ReceiptType { get; set; }
    }
}
