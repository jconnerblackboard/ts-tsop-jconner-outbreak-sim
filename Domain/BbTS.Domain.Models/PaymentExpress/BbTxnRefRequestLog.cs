﻿using System;

namespace BbTS.Domain.Models.PaymentExpress
{
    public class BbTxnRefRequestLog : TxnRefRequest
    {
        public DateTime TimeStamp { get; set; }
        public string DeviceId { get; set; }

        public BbTxnRefRequestLog()
        {
            //parameter-less constructor for serialization purposes
        }

        public BbTxnRefRequestLog(TxnRefRequest request)
        {
            TxnRef = request.TxnRef;
            Amount = request.Amount;
            MerchantReference = request.MerchantReference;
            SlotId = request.SlotId;
            DpsTxnRef = request.DpsTxnRef;
            OemDataFormat = request.OemDataFormat;
            OemData = request.OemData;
            RefundToken = request.RefundToken;
        }
    }
}
