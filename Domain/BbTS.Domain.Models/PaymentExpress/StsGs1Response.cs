﻿namespace BbTS.Domain.Models.PaymentExpress
{
    public class StsGs1Response
    {
        public string CmdSeq { get; set; }
        public string ReCo { get; set; }
        public string MsgCount { get; set; }
        public string CardPresent { get; set; }
        public string Status { get; set; }
        public string TxnState { get; set; }
        public string Time { get; set; }
        public string Online { get; set; }
        public string NumOffline { get; set; }
        public string FirmwarePending { get; set; }
    }
}
