﻿using System;

namespace BbTS.Domain.Models.PaymentExpress
{
    public class BbTxnSigRequestLog : TxnSigRequest
    {
        public DateTime TimeStamp { get; set; }
        public string TxnRef { get; set; }

        public BbTxnSigRequestLog()
        {
            //parameter-less constructor for serialization purposes
        }

        public BbTxnSigRequestLog(TxnSigRequest request)
        {
            CmdSeq = request.CmdSeq;
            SignatureResult = request.SignatureResult;
            SlotId = request.SlotId;
        }
    }
}
