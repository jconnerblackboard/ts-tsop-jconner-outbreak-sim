﻿using System;

namespace BbTS.Domain.Models.PaymentExpress
{
    public class BbTxnGetrRequestLog : TxnGetrRequest
    {
        public DateTime TimeStamp { get; set; }

        public BbTxnGetrRequestLog()
        {
            //parameter-less constructor for serialization purposes
        }

        public BbTxnGetrRequestLog(TxnGetrRequest request)
        {
            CmdSeq = request.CmdSeq;
            LineNumber = request.LineNumber;
            LineCount = request.LineCount;
            SlotId = request.SlotId;
            ReceiptType = request.ReceiptType;
        }
    }
}
