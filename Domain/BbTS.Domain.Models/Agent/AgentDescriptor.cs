﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Agent
{
    /// <summary>
    /// Container class for a descriptor associated with an agent.
    /// </summary>
    public class AgentDescriptor
    {
        /// <summary>
        /// The name of the agent.
        /// </summary>
        public string AgentName { get; set; }

        /// <summary>
        /// The description of the agent.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The display name of the agent.
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// The unique identifier for the agent.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The version of this agent.
        /// </summary>
        public string Version { get; set; }
    }

    /// <summary>
    /// Comparison class for an agent descriptor.
    /// </summary>
    public class AgentDescriptorComparer : IEqualityComparer<AgentDescriptor>
    {
        /// <summary>
        /// Override for equality.
        /// </summary>
        /// <param name="x">Left side.</param>
        /// <param name="y">Right side.</param>
        /// <returns>Equality results.</returns>
        public bool Equals(AgentDescriptor x, AgentDescriptor y)
        {
            if (x == null) return y == null;
            if (y == null) return false;
            return x.Id == y.Id && x.Version == y.Version;
        }

        /// <summary>
        /// Get the hash code for an agent descriptor.
        /// </summary>
        /// <param name="obj">The descriptor to hash.</param>
        /// <returns>Hash code.</returns>
        public int GetHashCode(AgentDescriptor obj)
        {
            string hash = $"{obj.Id}{obj.Version}";
            return hash.GetHashCode();
        }
    }
}
