﻿namespace BbTS.Domain.Models.Agent
{
    public class UpdateDescriptor : AgentDescriptor
    {
        public byte[] UpdateBytes { get; set; }
    }
}
