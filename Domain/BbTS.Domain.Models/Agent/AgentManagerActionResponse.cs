﻿using BbTS.Domain.Models.Definitions.Agent;

namespace BbTS.Domain.Models.Agent
{
    /// <summary>
    /// Container class for the response to an action taken on the agent plugin manager ui.
    /// </summary>
    public class AgentManagerActionResponse
    {
        /// <summary>
        /// The unique identifier for the request that preempted the processing routine.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// Reason for failure (if there was a failure error code).
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// The code associated with the return message.
        /// </summary>
        public AgentServiceDefinitions.AgentServiceActionResultDomainIdCode Code { get; set; }

        /// <summary>
        /// The human readable message associated with the code.
        /// </summary>
        public string CodeString { get; set; }

        /// <summary>
        /// The action that was taken.
        /// </summary>
        public AgentPluginManagerAction Action { get; set; }

        /// <summary>
        /// The unique identifier for the agent.
        /// </summary>
        public string AgentId { get; set; }

        /// <summary>
        /// The name of the agent.
        /// </summary>
        public string AgentName { get; set; }

        /// <summary>
        /// empty constructor for serialization.
        /// </summary>
        public AgentManagerActionResponse()
        {

        }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="agent"></param>
        /// <param name="action"></param>
        /// <param name="code"></param>
        /// <param name="message"></param>
        public AgentManagerActionResponse(AgentDescriptor agent, AgentPluginManagerAction action, AgentServiceDefinitions.AgentServiceActionResultDomainIdCode code, string message)
        {
            AgentId = agent.Id;
            AgentName = agent.AgentName;
            Action = action;
            Code = code;
            Message = message;

            CodeString = AgentServiceDefinitions.ResultCodeToString(code);
        }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="agentId">The unique identifier for the agent.</param>
        /// <param name="agentName">The name of the agent.</param>
        /// <param name="action">The action that was taken.</param>
        /// <param name="code">The code associated with the return message.</param>
        /// <param name="message">The return message</param>
        public AgentManagerActionResponse(string agentId, string agentName, AgentPluginManagerAction action, AgentServiceDefinitions.AgentServiceActionResultDomainIdCode code, string message)
        {
            AgentId = agentId;
            AgentName = agentName;
            Action = action;
            Code = code;
            Message = message;

            CodeString = AgentServiceDefinitions.ResultCodeToString(code);
        }
    }
}
