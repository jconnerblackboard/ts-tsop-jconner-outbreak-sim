﻿using BbTS.Domain.Models.Definitions.Agent;
using BbTS.Domain.Models.Transaction;

namespace BbTS.Domain.Models.Agent
{
    /// <summary>
    /// Container class for the response to an install agent request.
    /// </summary>
    public class AgentActionResponse : ProcessingResult
    {
        /// <summary>
        /// The action taken.
        /// </summary>
        public AgentPluginManagerAction Action { get; set; }
    }
}
