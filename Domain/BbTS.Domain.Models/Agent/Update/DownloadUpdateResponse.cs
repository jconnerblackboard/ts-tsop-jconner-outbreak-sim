﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.System;

namespace BbTS.Domain.Models.Agent.Update
{
    public class DownloadUpdateResponse
    {
        public ActionResultToken ResultToken { get; set; }

        public AgentDescriptor AgentDescriptor { get; set; }

        public string DownloadBytesAsString { get; set; }
    }
}
