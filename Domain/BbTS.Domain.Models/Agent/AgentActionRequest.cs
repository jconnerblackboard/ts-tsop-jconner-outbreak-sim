﻿using System;

namespace BbTS.Domain.Models.Agent
{
    /// <summary>
    /// Container class for an install agent request.
    /// </summary>
    public class AgentActionRequest
    {
        /// <summary>
        /// The unique identifier for this request.
        /// </summary>
        public string RequestId { get; set; } = Guid.NewGuid().ToString("D");

        /// <summary>
        /// The descriptor for the agent to install.
        /// </summary>
        public AgentDescriptor Descriptor { get; set; }
    }
}
