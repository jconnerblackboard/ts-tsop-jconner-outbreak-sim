﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Agent
{
    /// <summary>
    /// Domain object to hold Plugin validation data retrieved from the database layer.
    /// </summary>
    public class PluginCredentialObject
    {
        /// <summary>
        /// Get or Set the PluginValidationId which is the primary key of the table.
        /// </summary>
        public int PluginCredentialId { get; set; }

        /// <summary>
        /// Get or set the PluginValidationGuid which is the unique identifier for the plugin.
        /// </summary>
        public string PluginCredentialGuid { get; set; }
    }
}
