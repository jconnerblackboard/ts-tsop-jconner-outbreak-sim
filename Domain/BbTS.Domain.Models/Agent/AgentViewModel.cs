﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Agent
{
    /// <summary>
    /// Container class for the view model of an agent.
    /// </summary>
    public class AgentViewModel
    {
        /// <summary>
        /// The name of the agent.
        /// </summary>
        public string AgentName { get; set; }

        /// <summary>
        /// The description of the agent.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The unique identifier for the agent.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The installed version of this agent.
        /// </summary>
        public string InstalledVersion { get; set; }

        /// <summary>
        /// The available update version of this agent.
        /// </summary>
        public string UpdateVersion { get; set; }

        /// <summary>
        /// Empty constructor.
        /// </summary>
        public AgentViewModel()
        {
        }

        /// <summary>
        /// Create a base view model from an AgentDescriptor.
        /// </summary>
        /// <param name="descriptor"></param>
        public AgentViewModel(AgentDescriptor descriptor)
        {
            AgentName = descriptor.AgentName;
            Description = descriptor.Description;
            Id = descriptor.Id;
        }
    }

    /// <summary>
    /// Comparer 
    /// </summary>
    public class AgentViewModelComparer : IEqualityComparer<AgentViewModel>
    {
        /// <summary>
        /// Equality
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bool Equals(AgentViewModel x, AgentViewModel y)
        {
            if (x == null) return y == null;
            if (y == null) return false;
            return x.Id == y.Id;
        }

        /// <summary>
        /// Hash.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int GetHashCode(AgentViewModel obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}
