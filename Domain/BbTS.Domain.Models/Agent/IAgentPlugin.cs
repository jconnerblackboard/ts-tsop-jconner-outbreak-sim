﻿using System.Collections.Generic;
using BbTS.Domain.Models.Configuration;
using BbTS.Domain.Models.Definitions.Agent;
using BbTS.Domain.Models.Status;

namespace BbTS.Domain.Models.Agent
{
    /// <summary>
    /// Interface describing an agent or plugin.
    /// </summary>
    public interface IAgentPlugin
    {
        /// <summary>
        /// The display name for the plugin.
        /// </summary>
        string DisplayName { get; set; }

        /// <summary>
        /// The custom configuration settings for the plugin in the form of a set of key/value pairs.
        /// </summary>
        CustomConfigurationModel Configuration { get; set; }

        /// <summary>
        /// The name of the Agent/Plugin.
        /// </summary>
        /// <returns></returns>
        string AgentName();

        /// <summary>
        /// The Unique identifier for the plugin.
        /// </summary>
        /// <returns></returns>
        string Id();

        /// <summary>
        /// The plugin description.
        /// </summary>
        /// <returns></returns>
        string Description();

        /// <summary>
        /// the version of the plugin.
        /// </summary>
        string Version { get; set; }

        /// <summary>
        /// Get the status of this agent.
        /// </summary>
        /// <returns></returns>
        AgentServiceDefinitions.AgentStatus AgentStatusGet();

        /// <summary>
        /// Load the agent and do any initialization that might be needed.
        /// </summary>
        /// <param name="message">result of the operation.</param>
        /// <returns>Success or failure of the operation.</returns>
        bool LoadAgent(out string message);

        /// <summary>
        /// Unload the agent and perform any cleanup that might be necessary.
        /// </summary>
        /// <param name="message">result of the operation.</param>
        /// <returns>Success or failure of the operation.</returns>
        bool UnloadAgent(out string message);

        /// <summary>
        /// Temporarily enable or disable the agent.
        /// </summary>
        /// <param name="value">enable = true, disable = false.</param>
        /// <param name="message">result of the operation.</param>
        /// <returns>Success or failure of the operation.</returns>
        bool EnableAgent(bool value, out string message);

        /// <summary>
        /// Run a management interval.  The interval is bound by the manager interval.
        /// </summary>
        void RunManagementInterval();

        /// <summary>
        /// Perform a healthcheck on the agent/plugin.
        /// </summary>
        /// <returns>List of healthcheck items and the results of each check.</returns>
        List<HealthcheckItem> Healthcheck();
    }
}
