﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Agent
{
    /// <summary>
    /// Container class to authorize an agent to run.
    /// </summary>
    public class AuthorizeAgentRequest
    {
        /// <summary>
        /// Unique identifier for the request.
        /// </summary>
        public string RequestId { get; set; } = Guid.NewGuid().ToString("D");

        /// <summary>
        /// The description of the agent.
        /// </summary>
        public AgentDescriptor Descriptor { get; set; }
    }
}
