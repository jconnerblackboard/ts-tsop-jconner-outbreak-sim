﻿using System;
using BbTS.Domain.Models.XmlDocument;

namespace BbTS.Domain.Models.Document
{
    /// <summary>
    /// 
    /// </summary>
    public class DocumentPutPostResponse
    {
        /// <summary>
        /// 
        /// </summary>
        public DocumentPutPostRequest Request { get; set; }

        /// <summary>
        /// The request identifier
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public TsXmlDocument TsXmlDocument { get; set; }

        /// <summary>
        /// Initializer with request passed in
        /// </summary>
        /// <param name="request">A <see cref="DocumentPutPostRequest"/> object</param>
        public DocumentPutPostResponse(DocumentPutPostRequest request)
        {
            Request = request;
        }
    }
}