﻿using System;

namespace BbTS.Domain.Models.Document
{
    /// <summary>
    /// Main class for DocumentRequest object
    /// </summary>
    public class DocumentGetRequest
    {
        /// <summary>
        /// The request identifier
        /// </summary>
        public Guid Id { get; set; }
        
       
        /// <summary>
        /// The type of document to fetch
        /// </summary>
        public string Type { get; set; }
    }
}
