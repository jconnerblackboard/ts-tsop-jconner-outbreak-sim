﻿using System;
using BbTS.Domain.Models.XmlDocument;

namespace BbTS.Domain.Models.Document
{
    /// <summary>
    /// 
    /// </summary>
    public class DocumentPutPostRequest
    {
        /// <summary>
        /// The request identifier
        /// </summary>
        public Guid Id { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public TsXmlDocument TsXmlDocument { get; set; }
    }
}