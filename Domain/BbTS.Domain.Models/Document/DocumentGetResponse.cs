﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.XmlDocument;

namespace BbTS.Domain.Models.Document
{
    /// <summary>
    /// 
    /// </summary>
    public class DocumentGetResponse
    {
        /// <summary>
        /// The type of document requested.
        /// </summary>
        public string RequestType { get; internal set; }

        /// <summary>
        /// A Transact XmlDocument
        /// </summary>
        public TsXmlDocument TsXmlDocument { get; internal set; }

        /// <summary>
        /// Initializer with request passed in
        /// </summary>
        /// <param name="document">the document fetched from the data resource layer.</param>
        /// <param name="requestType">The type of document requested.</param>
        public DocumentGetResponse(string requestType, TsXmlDocument document)
        {
            RequestType = requestType; 
            TsXmlDocument = document;
        }
    }
}
