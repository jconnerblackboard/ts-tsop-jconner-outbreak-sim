﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.General
{
    /// <summary>
    /// Container class for a row based file delimited by a special character.
    /// </summary>
    public class DelimitedDataFile
    {
        /// <summary>
        /// Delimiter for column separation.  Default is ",".
        /// </summary>
        public char ColumnDelimiter { get; set; } = ',';

        /// <summary>
        /// List of rows.
        /// </summary>
        public List<string[]> Rows { get; set; } = new List<string[]>();

    }
}
