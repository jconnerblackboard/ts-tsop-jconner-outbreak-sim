﻿using System;
using System.Runtime.Serialization;

namespace BbTS.Domain.Models.General
{
    /// <summary>
    /// Class to represent a serializable key and value string set.
    /// </summary>
    [Serializable]
    [DataContract]
    public class StringPair
    {
        /// <summary>
        /// Key
        /// </summary>
        [DataMember]
        public string Key { get; set; }

        /// <summary>
        /// Value
        /// </summary>
        [DataMember]
        public string Value { get; set; }

        /// <summary>
        /// empty constructor 
        /// </summary>
        public StringPair()
        {
        }

        /// <summary>
        /// parameterized constructor
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="value">value</param>
        public StringPair(string key, string value)
        {
            Key = key;
            Value = value;
        }
    }

    /// <summary>
    /// View for a <see cref="StringPair"/>.  (Version 1)
    /// </summary>
    public class StringPairViewV01
    {
        /// <summary>
        /// Key
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Value
        /// </summary>
        public string Value { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="StringPair"/> conversion.
    /// </summary>
    public static class StringPairConverter
    {
        /// <summary>
        /// Returns a <see cref="StringPairViewV01"/> object based on this <see cref="StringPair"/>.
        /// </summary>
        /// <param name="stringPair"></param>
        /// <returns></returns>
        public static StringPairViewV01 ToStringPairViewV01(this StringPair stringPair)
        {
            if (stringPair == null) return null;

            return new StringPairViewV01
            {
                Key = stringPair.Key,
                Value = stringPair.Value
            };
        }

        /// <summary>
        /// Returns a <see cref="StringPair"/> object based on this <see cref="StringPairViewV01"/>.
        /// </summary>
        /// <param name="stringPairViewV01"></param>
        /// <returns></returns>
        public static StringPair ToStringPair(this StringPairViewV01 stringPairViewV01)
        {
            if (stringPairViewV01 == null) return null;

            return new StringPair
            {
                Key = stringPairViewV01.Key,
                Value = stringPairViewV01.Value
            };
        }
    }
}
