﻿using System;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.XmlDocument
{
  /// <summary>
  /// This object represents a Transact XmlDocument object. This table contains Xml documents.
  /// </summary>
  [Serializable]
  public class TsXmlDocument
  {
    /// <summary>
    /// Primary Key of the table.
    /// </summary>
    [XmlAttribute]
    public int XmlDocumentId { get; set; }
    /// <summary>
    /// The type of document contained in this row.
    /// </summary>
    [XmlAttribute]
    public string DocumentType { get; set; }
    /// <summary>
    /// The Xml document.
    /// </summary>
    [XmlAttribute]
    public string Document { get; set; }
  }
}
