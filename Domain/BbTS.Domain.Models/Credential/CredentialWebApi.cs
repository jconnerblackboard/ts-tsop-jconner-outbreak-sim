﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Definitions.Web.Api;

namespace BbTS.Domain.Models.Credential
{
    /// <summary>
    /// Container class for a credential used by BbTS Web API transactional service.
    /// </summary>
    public class CredentialWebApi
    {
        /// <summary>
        /// The value associated with this credential (i.e. CardNumber, Track2Data, etc.)
        /// </summary>
        public string CredentialValue { get; set; }

        /// <summary>
        /// The PIN number associated with this credential.
        /// </summary>
        public string Pin { get; set; }

        /// <summary>
        /// The type of credential (i.e.: ManualEntry, Swiped).
        /// </summary>
        public ServiceCredentialType Type { get; set; }

        /// <summary>
        /// Deep copy of this object.
        /// </summary>
        /// <returns></returns>
        public CredentialWebApi Clone()
        {
            return MemberwiseClone() as CredentialWebApi;
        }
    }
}
