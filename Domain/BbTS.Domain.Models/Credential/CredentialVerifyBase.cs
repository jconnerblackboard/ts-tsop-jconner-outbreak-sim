﻿using BbTS.Domain.Models.Definitions.Credential;

namespace BbTS.Domain.Models.Credential
{
    /// <summary>
    /// Container class representing the base information needed for a customer verification request.
    /// </summary>
    public class CredentialVerifyBase
    {
        /// <summary>
        /// The type of credential supplied.
        /// </summary>
        public CredentialType Type { get; set; }

        /// <summary>
        /// The credential value.
        /// </summary>
        public string Value { get; set; }
    }

    /// <summary>
    /// View for a <see cref="CredentialVerifyBase"/>.  (Version 1)
    /// </summary>
    public class CredentialVerifyBaseViewV01
    {
        /// <summary>
        /// The type of credential supplied.
        /// </summary>
        public CredentialType Type { get; set; }

        /// <summary>
        /// The credential value.
        /// </summary>
        public string Value { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="CredentialVerifyBase"/> conversion.
    /// </summary>
    public static class CredentialVerifyBaseConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="CredentialVerifyBaseViewV01"/> object based on this <see cref="CredentialVerifyBase"/>.
        /// </summary>
        /// <param name="credentialVerifyBase"></param>
        /// <returns></returns>
        public static CredentialVerifyBaseViewV01 ToCredentialVerifyBaseViewV01(this CredentialVerifyBase credentialVerifyBase)
        {
            if (credentialVerifyBase == null) return null;

            return new CredentialVerifyBaseViewV01
            {
                Type = credentialVerifyBase.Type,
                Value = credentialVerifyBase.Value
            };
        }

        /// <summary>
        /// Returns a <see cref="CredentialVerifyBase"/> object based on this <see cref="CredentialVerifyBaseViewV01"/>.
        /// </summary>
        /// <param name="credentialVerifyBaseViewV01"></param>
        /// <returns></returns>
        public static CredentialVerifyBase ToCredentialVerifyBase(this CredentialVerifyBaseViewV01 credentialVerifyBaseViewV01)
        {
            if (credentialVerifyBaseViewV01 == null) return null;

            return new CredentialVerifyBase
            {
                Type = credentialVerifyBaseViewV01.Type,
                Value = credentialVerifyBaseViewV01.Value
            };
        }
        #endregion
    }
}
