﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BbTS.Domain.Models.Credential
{
    /// <summary>
    /// Container class for a credential verify request.  (Version 1)
    /// </summary>
    public class CredentialVerifyRequestV01
    {
        /// <summary>
        /// The unique identifier of the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// List of credentials to be verified.
        /// </summary>
        public List<CredentialVerifyBaseViewV01> Credentials { get; set; }
    }

    /// <summary>
    /// Extension methods to create credential verify requests. 
    /// </summary>
    public static class CredentialVerifyRequestConverter
    {
        #region Version 1
        /// <summary>
        /// Extracts the Request ID from the request.
        /// </summary>
        /// <param name="credentialVerifyRequestV01"></param>
        /// <returns></returns>
        public static Guid RequestIdExtract(this CredentialVerifyRequestV01 credentialVerifyRequestV01)
        {
            if (credentialVerifyRequestV01 == null) return Guid.Empty;

            try
            {
                return Guid.Parse(credentialVerifyRequestV01.RequestId);
            }
            catch (ArgumentNullException)
            {
                return Guid.Empty;
            }
            catch (FormatException)
            {
                return Guid.Empty;
            }
        }

        /// <summary>
        /// Returns a list of credentials contained within the request.
        /// </summary>
        /// <param name="credentialVerifyRequestV01"></param>
        /// <returns></returns>
        public static List<CredentialVerifyBase> CredentialsExtract(this CredentialVerifyRequestV01 credentialVerifyRequestV01)
        {
            return credentialVerifyRequestV01.Credentials.Select(credentialVerifyBaseViewV01 => credentialVerifyBaseViewV01.ToCredentialVerifyBase()).ToList();
        }
        #endregion
    }
}
