﻿namespace BbTS.Domain.Models.Credential
{
    /// <summary>
    /// Holds track 1 and track 2 card data.
    /// </summary>
    public class CardData
    {
        /// <summary>
        /// Track 1 card data as supplied by the card reader.  It is expected that the start and end sentinels have been removed.
        /// </summary>
        public string Track1 { get; set; }

        /// <summary>
        /// Typically, this will be '^' (0x5E)
        /// </summary>
        public char Track1FieldSeparator { get; set; } = '^';

        /// <summary>
        /// Track 2 card data as supplied by the card reader.  It is expected that the start and end sentinels have been removed.
        /// </summary>
        public string Track2 { get; set; }

        /// <summary>
        /// Typically, this will be '=' (0x3D)
        /// </summary>
        public char Track2FieldSeparator { get; set; } = '=';
    }
}