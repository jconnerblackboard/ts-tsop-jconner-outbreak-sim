﻿using BbTS.Domain.Models.Definitions.Credential;

namespace BbTS.Domain.Models.Credential
{
    /// <summary>
    /// A rule for locating and reading a portion of the card data.
    /// </summary>
    public class CardParseRule
    {
        /// <summary>
        /// The 0-based location within the card data to begin parsing, starting from the Anchor.
        /// </summary>
        public int Location { get; set; }

        /// <summary>
        /// Specifies place to start counting from when using the Location value to find data.
        /// </summary>
        public ParseAnchor Anchor { get; set; }

        /// <summary>
        /// Length of data to parse.
        /// </summary>
        public int Length { get; set; }
    }
}