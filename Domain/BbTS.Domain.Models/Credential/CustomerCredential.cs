﻿using System;
using BbTS.Domain.Models.Definitions.Credential;

//using BbTS.Domain.Models.Definitions.ArtsDataModel;

namespace BbTS.Domain.Models.Credential
{
    /// <summary>
    /// Container class for a customer credential in the transaction object structure.
    /// </summary>
    public class CustomerCredential
    {
        /// <summary>
        /// The unique identifier for the customer.
        /// </summary>
        public Guid? CustomerGuid { get; set; }

        /// <summary>
        /// The physical type of media used in the transaction.
        /// "0 - card (magstripe), 1 - contactless"
        /// </summary>
        public CustomerTenderPhysicalIdType PhysicalIdType { get; set; }

        /// <summary>
        /// The method used to capture the customer's credentials.
        /// "0 - swiped, 1 - manually entered"
        /// </summary>
        public CustomerTenderEntryMethodType EntryMethodType { get; set; }

        /// <summary>
        /// Was a secondary auth entered?
        /// </summary>
        public bool SecondaryAuthEntered { get; set; }

        /// <summary>
        ///  The numerical value associated with the customer. 
        /// </summary>
        [Obsolete]
        public int CustomerId { get; set; }

        /// <summary>
        /// The customer's card information.
        /// </summary>
        public CardCapture CustomerCardInfo { get; set; }

        /// <summary>
        /// Deep Copy.
        /// </summary>
        /// <returns></returns>
        public CustomerCredential Clone()
        {
            var customerCredential = (CustomerCredential)MemberwiseClone();
            customerCredential.CustomerCardInfo = CustomerCardInfo.Clone();
            return customerCredential;
        }
    }

    /// <summary>
    /// View for a <see cref="CustomerCredential"/>.  (Version 1)
    /// </summary>
    public class CustomerCredentialViewV01
    {
        /// <summary>
        /// The unique identifier for the customer.
        /// </summary>
        public Guid? CustomerGuid { get; set; }

        /// <summary>
        /// The physical type of media used in the transaction.
        /// "0 - card (magstripe), 1 - contactless"
        /// </summary>
        public CustomerTenderPhysicalIdType PhysicalIdType { get; set; }

        /// <summary>
        /// The method used to capture the customer's credentials.
        /// "0 - swiped, 1 - manually entered"
        /// </summary>
        public CustomerTenderEntryMethodType EntryMethodType { get; set; }

        /// <summary>
        /// Was a secondary auth entered?
        /// </summary>
        public bool SecondaryAuthEntered { get; set; }

        /// <summary>
        ///  The numberical value associated with the customer. 
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// The customer's card information.
        /// </summary>
        public CardCaptureViewV01 CustomerCardInfo { get; set; }
    }

    /// <summary>
    /// View for a <see cref="CustomerCredential"/>.  (Version 2)
    /// </summary>
    public class CustomerCredentialViewV02
    {
        /// <summary>
        /// The unique identifier for the customer.
        /// </summary>
        public Guid? CustomerGuid { get; set; }

        /// <summary>
        /// The physical type of media used in the transaction.
        /// "0 - card (magstripe), 1 - contactless"
        /// </summary>
        public CustomerTenderPhysicalIdType PhysicalIdType { get; set; }

        /// <summary>
        /// The method used to capture the customer's credentials.
        /// "0 - swiped, 1 - manually entered"
        /// </summary>
        public CustomerTenderEntryMethodType EntryMethodType { get; set; }

        /// <summary>
        /// Was a secondary auth entered?
        /// </summary>
        public bool SecondaryAuthEntered { get; set; }

        /// <summary>
        /// The customer's card information.
        /// </summary>
        public CardCaptureViewV01 CustomerCardInfo { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="CustomerCredential"/> conversion.
    /// </summary>
    public static class CustomerCredentialConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="CustomerCredentialViewV01"/> object based on this <see cref="CustomerCredential"/>.
        /// </summary>
        /// <param name="customerCredential"></param>
        /// <returns></returns>
        public static CustomerCredentialViewV01 ToCustomerCredentialViewV01(this CustomerCredential customerCredential)
        {
            if (customerCredential == null) return null;

            return new CustomerCredentialViewV01
            {
                CustomerGuid = customerCredential.CustomerGuid,
                PhysicalIdType = customerCredential.PhysicalIdType,
                EntryMethodType = customerCredential.EntryMethodType,
                SecondaryAuthEntered = customerCredential.SecondaryAuthEntered,
                CustomerId = customerCredential.CustomerId,
                CustomerCardInfo = customerCredential.CustomerCardInfo.ToCardCaptureViewV01()
            };
        }

        /// <summary>
        /// Returns a <see cref="CustomerCredential"/> object based on this <see cref="CustomerCredentialViewV01"/>.
        /// </summary>
        /// <param name="customerCredentialViewV01"></param>
        /// <returns></returns>
        public static CustomerCredential ToCustomerCredential(this CustomerCredentialViewV01 customerCredentialViewV01)
        {
            if (customerCredentialViewV01 == null) return null;

            return new CustomerCredential
            {
                CustomerGuid = customerCredentialViewV01.CustomerGuid,
                PhysicalIdType = customerCredentialViewV01.PhysicalIdType,
                EntryMethodType = customerCredentialViewV01.EntryMethodType,
                SecondaryAuthEntered = customerCredentialViewV01.SecondaryAuthEntered,
                CustomerId = customerCredentialViewV01.CustomerId,
                CustomerCardInfo = customerCredentialViewV01.CustomerCardInfo.ToCardCapture()
            };
        }
        #endregion

        #region Version 2
        /// <summary>
        /// Returns a <see cref="CustomerCredentialViewV02"/> object based on this <see cref="CustomerCredential"/>.
        /// </summary>
        /// <param name="customerCredential"></param>
        /// <returns></returns>
        public static CustomerCredentialViewV02 ToCustomerCredentialViewV02(this CustomerCredential customerCredential)
        {
            if (customerCredential == null) return null;

            return new CustomerCredentialViewV02
            {
                CustomerGuid = customerCredential.CustomerGuid,
                PhysicalIdType = customerCredential.PhysicalIdType,
                EntryMethodType = customerCredential.EntryMethodType,
                SecondaryAuthEntered = customerCredential.SecondaryAuthEntered,
                CustomerCardInfo = customerCredential.CustomerCardInfo.ToCardCaptureViewV01()
            };
        }

        /// <summary>
        /// Returns a <see cref="CustomerCredential"/> object based on this <see cref="CustomerCredentialViewV02"/>.
        /// </summary>
        /// <param name="customerCredentialViewV02"></param>
        /// <returns></returns>
        public static CustomerCredential ToCustomerCredential(this CustomerCredentialViewV02 customerCredentialViewV02)
        {
            if (customerCredentialViewV02 == null) return null;

            return new CustomerCredential
            {
                CustomerGuid = customerCredentialViewV02.CustomerGuid,
                PhysicalIdType = customerCredentialViewV02.PhysicalIdType,
                EntryMethodType = customerCredentialViewV02.EntryMethodType,
                SecondaryAuthEntered = customerCredentialViewV02.SecondaryAuthEntered,
                CustomerCardInfo = customerCredentialViewV02.CustomerCardInfo.ToCardCapture()
            };
        }
        #endregion
    }
}
