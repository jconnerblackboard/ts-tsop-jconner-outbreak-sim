﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BbTS.Domain.Models.Credential
{
    /// <summary>
    /// A class that represents a single credential presentation.
    /// </summary>
    public class CredentialPresentation
    {
        /// <summary>
        /// Uniquely identifies this instance.
        /// </summary>
        public Guid CredentialPresentationGuid { get; set; }

        /// <summary>
        /// Text that serves to describe this CredentialPresentation.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Optional link to a customer.
        /// </summary>
        public Guid? CustomerGuid { get; set; }

        /// <summary>
        /// List of credential information that, together, represents a single credential presentation.
        /// For example, if a card is swiped and an issue number is captured, this credential information
        /// will include one CardNumber item and one IssueNumber item.
        /// </summary>
        public List<CredentialVerifyBase> Credentials { get; set; }
    }

    /// <summary>
    /// View for a <see cref="CredentialPresentation"/>.  (Version 1)
    /// </summary>
    public class CredentialPresentationViewV01
    {
        /// <summary>
        /// Uniquely identifies this instance.
        /// </summary>
        public Guid CredentialPresentationGuid { get; set; }

        /// <summary>
        /// Text that serves to describe this CredentialPresentation.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Optional link to a customer.
        /// </summary>
        public Guid? CustomerGuid { get; set; }

        /// <summary>
        /// List of credential information that, together, represents a single credential presentation.
        /// For example, if a card is swiped and an issue number is captured, this credential information
        /// will include one CardNumber item and one IssueNumber item.
        /// </summary>
        public List<CredentialVerifyBaseViewV01> Credentials { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="CredentialPresentation"/> conversion.
    /// </summary>
    public static class CredentialPresentationConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="CredentialPresentationViewV01"/> object based on this <see cref="CredentialPresentation"/>.
        /// </summary>
        /// <param name="credentialPresentation"></param>
        /// <returns></returns>
        public static CredentialPresentationViewV01 ToCredentialPresentationViewV01(this CredentialPresentation credentialPresentation)
        {
            if (credentialPresentation == null) return null;

            return new CredentialPresentationViewV01
            {
                CredentialPresentationGuid = credentialPresentation.CredentialPresentationGuid,
                Text = credentialPresentation.Text,
                CustomerGuid = credentialPresentation.CustomerGuid,
                Credentials = credentialPresentation.Credentials.Select(credentialVerifyBase => credentialVerifyBase.ToCredentialVerifyBaseViewV01()).ToList()
            };
        }

        /// <summary>
        /// Returns a <see cref="CredentialPresentation"/> object based on this <see cref="CredentialPresentationViewV01"/>.
        /// </summary>
        /// <param name="credentialPresentationViewV01"></param>
        /// <returns></returns>
        public static CredentialPresentation ToCredentialPresentation(this CredentialPresentationViewV01 credentialPresentationViewV01)
        {
            if (credentialPresentationViewV01 == null) return null;

            return new CredentialPresentation
            {
                CredentialPresentationGuid = credentialPresentationViewV01.CredentialPresentationGuid,
                Text = credentialPresentationViewV01.Text,
                CustomerGuid = credentialPresentationViewV01.CustomerGuid,
                Credentials = credentialPresentationViewV01.Credentials.Select(credentialVerifyBaseViewV01 => credentialVerifyBaseViewV01.ToCredentialVerifyBase()).ToList()
            };
        }
        #endregion
    }
}
