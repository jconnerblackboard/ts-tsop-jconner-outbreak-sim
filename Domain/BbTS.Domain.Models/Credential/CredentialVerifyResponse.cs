﻿using BbTS.Domain.Models.Customer;

namespace BbTS.Domain.Models.Credential
{
    /// <summary>
    /// Container class for the response to a credential verify request.  (Version 1)
    /// </summary>
    public class CredentialVerifyResponseV01
    {
        /// <summary>
        /// The unique identifier of the request.
        /// </summary>
        public string RequestId { get; internal set; }

        /// <summary>
        /// Customer information.
        /// </summary>
        public CredentialVerifyCustomerViewV01 Customer { get; set; }

        /// <summary>
        /// Parameterized constructor.  The response requires the request id to be created.
        /// </summary>
        /// <param name="requestId">The unique identifier of the request.</param>
        public CredentialVerifyResponseV01(string requestId)
        {
            RequestId = requestId;
        }
    }
}
