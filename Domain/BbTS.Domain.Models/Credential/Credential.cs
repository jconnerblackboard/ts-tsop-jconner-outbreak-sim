﻿using System;

namespace BbTS.Domain.Models.Credential
{
    public class Credential : IEquatable<Credential>
    {
        public String CredentialNumber { get; set; }
        public String IssueNumber { get; set; }
        public Int32 CustomerId { get; set; }
        public Int32 CredentialTypeId { get; set; }
        public Int32 CredentialStatusId { get; set; }
        public String CredentialStatusText { get; set; }
        public DateTime CredentialStatusDateTime { get; set; }
        public String CredentialLost { get; set; }
        public String CredentialIdm { get; set; }
        public String CredentialId { get; set; }
        public Int32 IssuerId { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public DateTime ModifieDateTime { get; set; }


        public bool Equals(Credential other)
        {
            return
                CredentialNumber == other.CredentialNumber &&
                IssueNumber == other.IssueNumber &&
                CustomerId == other.CustomerId &&
                CredentialTypeId == other.CredentialTypeId &&
                CredentialStatusId == other.CredentialStatusId &&
                CredentialStatusText == other.CredentialStatusText &&
                CredentialLost == other.CredentialLost &&
                CredentialIdm == other.CredentialIdm &&
                CredentialId == other.CredentialId &&
                IssuerId == other.IssuerId;
        }
    }

    public class CredentialView : Credential, IEquatable<CredentialView>
    {
        public String CustomerNumber { get; set; }
        public String LastName { get; set; }
        public String CredentialType { get; set; }
        public String CredentialStatus { get; set; }
        public DateTime CredentialExpirationDateTime { get; set; }
        public String Issuer { get; set; }

        public bool Equals(CredentialView other)
        {
            return
                CustomerNumber == other.CustomerNumber &&
                LastName == other.LastName && 
                CredentialType == other.CredentialType && 
                CredentialStatus == other.CredentialStatus && 
                IssuerId == other.IssuerId &&
                CredentialNumber == other.CredentialNumber &&
                IssueNumber == other.IssueNumber &&
                CustomerId == other.CustomerId &&
                CredentialType == other.CredentialType &&
                CredentialStatus == other.CredentialStatus &&
                CredentialStatusText == other.CredentialStatusText &&
                CredentialLost == other.CredentialLost &&
                CredentialIdm == other.CredentialIdm &&
                CredentialId == other.CredentialId &&
                Issuer == other.Issuer;
        }
    }
}
