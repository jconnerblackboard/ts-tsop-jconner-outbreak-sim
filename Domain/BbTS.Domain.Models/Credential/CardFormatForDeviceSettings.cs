﻿using BbTS.Domain.Models.Definitions.Card;

namespace BbTS.Domain.Models.Credential
{
    /// <summary>
    /// Container class with Card Format related device settings.
    /// </summary>
    public class CardFormatForDeviceSettings
    {
        /// <summary>
        /// Unique identifier (Guid) for the card format.
        /// </summary>
        public string CardFormatGuid { get; set; }

        /// <summary>
        /// Name of the card format.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Order in which the formats should be displayed/processed
        /// </summary>
        public int PrioritySequence { get; set; }

        /// <summary>
        /// Site code value.  If SiteCode is null, this property is ignored.
        /// </summary>
        public string SiteCode { get; set; }

        /// <summary>
        /// Start position of the site code.
        /// </summary>
        public int SiteCodeStartPosition { get; set; }

        /// <summary>
        /// (0) - Absolute position from the first digit on track 2; (1) - Relative positioning from the first field separator on track 2.
        /// </summary>
        public CardFormatStartType SiteCodeStartType { get; set; }

        /// <summary>
        /// The length of the site code.
        /// </summary>
        public int SiteCodeLength { get; set; }

        /// <summary>
        /// If online, check site code.
        /// </summary>
        public bool SiteCodeCheckOnline { get; set; }

        /// <summary>
        /// If offline, check site code.
        /// </summary>
        public bool SiteCodeCheckOffline { get; set; }

        /// <summary>
        /// The start position of the ID number.
        /// </summary>
        public int IdNumberStartPosition { get; set; }

        /// <summary>
        /// (0) - Absolute position from the first digit on track 2; (1) - Relative positioning from the first field separator on track 2.
        /// </summary>
        public CardFormatStartType IdNumberStartType { get; set; }

        /// <summary>
        /// The length of the Id number.
        /// </summary>
        public int IdNumberLength { get; set; }

        /// <summary>
        /// The position of the issue number.
        /// </summary>
        public int IssueNumberStart { get; set; }

        /// <summary>
        /// Absolute position from the first digit on track 2; (1) - Relative positioning from the first field separator on track 2.
        /// </summary>
        public CardFormatStartType IssueNumberStartType { get; set; }

        /// <summary>
        /// Maximum length of issue number.
        /// </summary>
        public int IssueNumberLength { get; set; }

        /// <summary>
        /// The minimum number of digits on track 2
        /// </summary>
        public int MinimumDigitsTrack2 { get; set; }

        /// <summary>
        /// The maximum number of digits on track 2
        /// </summary>
        public int MaximumDigitsTrack2 { get; set; }
    }

    /// <summary>
    /// View for a <see cref="CardFormatForDeviceSettings"/>.  (Version 1)
    /// </summary>
    public class CardFormatForDeviceSettingsViewV01
    {
        /// <summary>
        /// Unique identifier (Guid) for the card format.
        /// </summary>
        public string CardFormatGuid { get; set; }

        /// <summary>
        /// Name of the card format.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Order in which the formats should be displayed/processed
        /// </summary>
        public int PrioritySequence { get; set; }

        /// <summary>
        /// Site code value.  If SiteCode is null, this property is ignored.
        /// </summary>
        public string SiteCode { get; set; }

        /// <summary>
        /// Start position of the site code.
        /// </summary>
        public int SiteCodeStartPosition { get; set; }

        /// <summary>
        /// (0) - Absolute position from the first digit on track 2; (1) - Relative positioning from the first field separator on track 2.
        /// </summary>
        public CardFormatStartType SiteCodeStartType { get; set; }

        /// <summary>
        /// The length of the site code.
        /// </summary>
        public int SiteCodeLength { get; set; }

        /// <summary>
        /// If online, check site code.
        /// </summary>
        public bool SiteCodeCheckOnline { get; set; }

        /// <summary>
        /// If offline, check site code.
        /// </summary>
        public bool SiteCodeCheckOffline { get; set; }

        /// <summary>
        /// The start position of the ID number.
        /// </summary>
        public int IdNumberStartPosition { get; set; }

        /// <summary>
        /// (0) - Absolute position from the first digit on track 2; (1) - Relative positioning from the first field separator on track 2.
        /// </summary>
        public CardFormatStartType IdNumberStartType { get; set; }

        /// <summary>
        /// The length of the Id number.
        /// </summary>
        public int IdNumberLength { get; set; }

        /// <summary>
        /// The position of the issue number.
        /// </summary>
        public int IssueNumberStart { get; set; }

        /// <summary>
        /// Absolute position from the first digit on track 2; (1) - Relative positioning from the first field separator on track 2.
        /// </summary>
        public CardFormatStartType IssueNumberStartType { get; set; }

        /// <summary>
        /// Maximum length of issue number.
        /// </summary>
        public int IssueNumberLength { get; set; }

        /// <summary>
        /// The minimum number of digits on track 2
        /// </summary>
        public int MinimumDigitsTrack2 { get; set; }

        /// <summary>
        /// The maximum number of digits on track 2
        /// </summary>
        public int MaximumDigitsTrack2 { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="CardFormatForDeviceSettings"/> conversion.
    /// </summary>
    public static class CardFormatForDeviceSettingsConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="CardFormatForDeviceSettingsViewV01"/> object based on this <see cref="CardFormatForDeviceSettings"/>.
        /// </summary>
        /// <param name="cardFormatForDeviceSettings"></param>
        /// <returns></returns>
        public static CardFormatForDeviceSettingsViewV01 ToCardFormatForDeviceSettingsViewV01(this CardFormatForDeviceSettings cardFormatForDeviceSettings)
        {
            if (cardFormatForDeviceSettings == null) return null;

            return new CardFormatForDeviceSettingsViewV01
            {
                CardFormatGuid = cardFormatForDeviceSettings.CardFormatGuid,
                Name = cardFormatForDeviceSettings.Name,
                PrioritySequence = cardFormatForDeviceSettings.PrioritySequence,
                SiteCode = cardFormatForDeviceSettings.SiteCode,
                SiteCodeStartPosition = cardFormatForDeviceSettings.SiteCodeStartPosition,
                SiteCodeStartType = cardFormatForDeviceSettings.SiteCodeStartType,
                SiteCodeLength = cardFormatForDeviceSettings.SiteCodeLength,
                SiteCodeCheckOnline = cardFormatForDeviceSettings.SiteCodeCheckOnline,
                SiteCodeCheckOffline = cardFormatForDeviceSettings.SiteCodeCheckOffline,
                IdNumberStartPosition = cardFormatForDeviceSettings.IdNumberStartPosition,
                IdNumberStartType = cardFormatForDeviceSettings.IdNumberStartType,
                IdNumberLength = cardFormatForDeviceSettings.IdNumberLength,
                IssueNumberStart = cardFormatForDeviceSettings.IssueNumberStart,
                IssueNumberStartType = cardFormatForDeviceSettings.IssueNumberStartType,
                IssueNumberLength = cardFormatForDeviceSettings.IssueNumberLength,
                MinimumDigitsTrack2 = cardFormatForDeviceSettings.MinimumDigitsTrack2,
                MaximumDigitsTrack2 = cardFormatForDeviceSettings.MaximumDigitsTrack2
            };
        }

        /// <summary>
        /// Returns a <see cref="CardFormatForDeviceSettings"/> object based on this <see cref="CardFormatForDeviceSettingsViewV01"/>.
        /// </summary>
        /// <param name="cardFormatForDeviceSettingsViewV01"></param>
        /// <returns></returns>
        public static CardFormatForDeviceSettings ToCardFormatForDeviceSettings(this CardFormatForDeviceSettingsViewV01 cardFormatForDeviceSettingsViewV01)
        {
            if (cardFormatForDeviceSettingsViewV01 == null) return null;

            return new CardFormatForDeviceSettings
            {
                CardFormatGuid = cardFormatForDeviceSettingsViewV01.CardFormatGuid,
                Name = cardFormatForDeviceSettingsViewV01.Name,
                PrioritySequence = cardFormatForDeviceSettingsViewV01.PrioritySequence,
                SiteCode = cardFormatForDeviceSettingsViewV01.SiteCode,
                SiteCodeStartPosition = cardFormatForDeviceSettingsViewV01.SiteCodeStartPosition,
                SiteCodeStartType = cardFormatForDeviceSettingsViewV01.SiteCodeStartType,
                SiteCodeLength = cardFormatForDeviceSettingsViewV01.SiteCodeLength,
                SiteCodeCheckOnline = cardFormatForDeviceSettingsViewV01.SiteCodeCheckOnline,
                SiteCodeCheckOffline = cardFormatForDeviceSettingsViewV01.SiteCodeCheckOffline,
                IdNumberStartPosition = cardFormatForDeviceSettingsViewV01.IdNumberStartPosition,
                IdNumberStartType = cardFormatForDeviceSettingsViewV01.IdNumberStartType,
                IdNumberLength = cardFormatForDeviceSettingsViewV01.IdNumberLength,
                IssueNumberStart = cardFormatForDeviceSettingsViewV01.IssueNumberStart,
                IssueNumberStartType = cardFormatForDeviceSettingsViewV01.IssueNumberStartType,
                IssueNumberLength = cardFormatForDeviceSettingsViewV01.IssueNumberLength,
                MinimumDigitsTrack2 = cardFormatForDeviceSettingsViewV01.MinimumDigitsTrack2,
                MaximumDigitsTrack2 = cardFormatForDeviceSettingsViewV01.MaximumDigitsTrack2
            };
        }
        #endregion
    }
}
