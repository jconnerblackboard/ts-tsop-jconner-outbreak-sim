﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Credential
{
    /// <summary>
    /// Container class representing the basic information for a card credential as returned from the Validation.CardFormatIsValid oracle proc.
    /// </summary>
    public class BasicCardCredential
    {
        /// <summary>
        /// Value represents the base card number.
        /// </summary>
        public string CardNumber { get; set; }

        /// <summary>
        /// Value represents the issue number.
        /// </summary>
        public string IssueNumber { get; set; }

        /// <summary>
        /// Value represents the site code.
        /// </summary>
        public string SiteCode { get; set; }

        /// <summary>
        /// Indicates whether or not the card is valid.
        /// </summary>
        public bool IsValid { get; set; }

        /// <summary>
        /// Value represents the unparsed track 2 data.
        /// </summary>
        public string RawTrack2 { get; set; }
    }
}
