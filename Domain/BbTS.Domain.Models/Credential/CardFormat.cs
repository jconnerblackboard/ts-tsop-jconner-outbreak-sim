﻿namespace BbTS.Domain.Models.Credential
{
    /// <summary>
    /// Describes a card format.
    /// </summary>
    public class CardFormat
    {
        /// <summary>
        /// A unique identifier for the card format.
        /// </summary>
        public string Guid { get; set; }

        /// <summary>
        /// True if the card format is active.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Name of the card format.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Order in which the formats should be displayed/processed
        /// </summary>
        public int PrioritySequence { get; set; }

        /// <summary>
        /// The minimum number of digits allowed for this format.
        /// </summary>
        public int MinimumDigits { get; set; }

        /// <summary>
        /// The maximum number of digits allowed for this format.
        /// </summary>
        public int MaximumDigits { get; set; }

        /// <summary>
        /// Rule for parsing the site code from raw card data.  Can be null.
        /// </summary>
        public CardParseRule SiteCode { get; set; }

        /// <summary>
        /// Site code value.  If SiteCode is null, this property is ignored.
        /// </summary>
        public string SiteCodeValue { get; set; }

        /// <summary>
        /// Site code should be checked when online.  If SiteCode is null, this property is ignored.
        /// </summary>
        public bool SiteCodeCheckOnline { get; set; }

        /// <summary>
        /// Site code should be checked when offline.  If SiteCode is null, this property is ignored.
        /// </summary>
        public bool SiteCodeCheckOffline { get; set; }

        /// <summary>
        /// Rule for parsing the card number from raw card data.  Can not be null.
        /// </summary>
        public CardParseRule CardNumber { get; set; }

        /// <summary>
        /// Rule for parsing the issue number from raw card data.  Can be null.
        /// </summary>
        public CardParseRule IssueNumber { get; set; }
    }
}