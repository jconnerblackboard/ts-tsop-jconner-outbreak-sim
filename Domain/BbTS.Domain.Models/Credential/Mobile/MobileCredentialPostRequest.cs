﻿using System;

namespace BbTS.Domain.Models.Credential.Mobile
{
    /// <summary>
    /// Container class for a mobile credential post request.
    /// </summary>
    public class MobileCredentialPostRequest
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public Guid RequestId { get; set; }

        /// <summary>
        /// The mobile credential.
        /// </summary>
        public CardCredentialComplete Credential { get; set; }
    }
}
