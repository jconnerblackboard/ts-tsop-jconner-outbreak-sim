﻿using System;
using BbTS.Domain.Models.Definitions.Card;

namespace BbTS.Domain.Models.Credential.Mobile
{
    /// <summary>
    /// Container class for a mobile credential patch request.
    /// </summary>
    public class MobileCredentialPatchRequest
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public Guid RequestId { get; set; }

        /// <summary>
        /// The mobile credential.
        /// </summary>
        public CardCredentialComplete Credential { get; set; }
    }
}
