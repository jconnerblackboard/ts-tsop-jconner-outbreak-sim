﻿namespace BbTS.Domain.Models.Credential.Mobile
{
    /// <summary>
    /// Container class for the response to a mobile credential get request.
    /// </summary>
    public class MobileCredentialGetResponse
    {
        /// <summary>
        /// The mobile credential.
        /// </summary>
        public CardCredentialComplete Credential { get; set; }
    }
}
