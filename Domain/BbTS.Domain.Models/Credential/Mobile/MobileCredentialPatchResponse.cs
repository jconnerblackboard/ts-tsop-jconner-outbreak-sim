﻿using System;
using BbTS.Domain.Models.Transaction;

namespace BbTS.Domain.Models.Credential.Mobile
{
    /// <summary>
    /// Container class for the response to a mobile credential patch request.
    /// </summary>
    public class MobileCredentialPatchResponse : ProcessingResult
    {

    }
}
