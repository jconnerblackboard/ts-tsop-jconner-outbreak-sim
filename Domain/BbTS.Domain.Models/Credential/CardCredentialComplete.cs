﻿using System;
using BbTS.Domain.Models.Definitions.Notification;

namespace BbTS.Domain.Models.Credential
{
    /// <summary>
    /// Container class for a mobile credential.
    /// </summary>
    public class CardCredentialComplete
    {
        /// <summary>
        /// The numerical identifier for the card.
        /// </summary>
        public string CardNumber { get; set; }

        /// <summary>
        /// The issue number assigned to the card.
        /// </summary>
        public string IssueNumber { get; set; }

        /// <summary>
        /// The numerical identifier for the customer that the card belongs to.
        /// </summary>
        public Guid? CustomerGuid { get; set; }

        /// <summary>
        /// The status of the card.
        /// </summary>
        public NotificationCardStatus CardStatus { get; set; } = NotificationCardStatus.Unknown;

        /// <summary>
        /// The type of card.
        /// </summary>
        public NotificationCardType CardType { get; set; } = NotificationCardType.Unknown;

        /// <summary>
        /// Text associated with the card status.
        /// </summary>
        public string CardStatusText { get; set; }

        /// <summary>
        /// The date and time the card status was last altered.
        /// </summary>
        public DateTimeOffset? CardStatusDateTime { get; set; }

        /// <summary>
        /// Is the card lost?
        /// </summary>
        public bool? LostFlag { get; set; }

        /// <summary>
        /// The card IDM value.
        /// </summary>
        public string CardIdm { get; set; }

        /// <summary>
        /// The domain id.
        /// </summary>
        public Guid? DomainId { get; set; }

        /// <summary>
        /// The numerical identifier for the issuer.
        /// </summary>
        public int? IssuerId { get; set; }

        /// <summary>
        /// The date and time the card becomes active.
        /// </summary>
        public DateTimeOffset? ActiveStartDate { get; set; }

        /// <summary>
        /// The date and time the card becomes inactive.
        /// </summary>
        public DateTimeOffset? ActiveEndDate { get; set; }

        /// <summary>
        /// The name of the card.
        /// </summary>
        public string CardName { get; set; }

        /// <summary>
        /// Indicates whether or not this card credential is a customer's primary card credential.
        /// </summary>
        public bool? IsPrimary { get; set; }
        
        /// <summary>
        /// Deep Copy.
        /// </summary>
        /// <returns></returns>
        public CardCredentialComplete Clone()
        {
            return (CardCredentialComplete)MemberwiseClone();
        }
    }
}
