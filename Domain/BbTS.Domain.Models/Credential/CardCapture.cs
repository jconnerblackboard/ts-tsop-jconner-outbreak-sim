﻿namespace BbTS.Domain.Models.Credential
{
    /// <summary>
    /// This class primarily represents how card data is stored when captured at a POS.
    /// </summary>
    public class CardCapture
    {
        /// <summary>
        /// Card number.
        /// </summary>
        public string CardNumber { get; set; }

        /// <summary>
        /// Issue number.
        /// </summary>
        public string IssueNumber { get; set; }

        /// <summary>
        /// Issue number captured.  i.e. Read from magstripe, etc.
        /// </summary>
        public bool IssueNumberCaptured { get; set; }

        /// <summary>
        /// Create a clone of this instance.
        /// </summary>
        /// <returns></returns>
        public CardCapture Clone()
        {
            return (CardCapture)MemberwiseClone();
        }
    }

    /// <summary>
    /// View for a <see cref="CardCapture"/>.  (Version 1)
    /// </summary>
    public class CardCaptureViewV01
    {
        /// <summary>
        /// Card number.
        /// </summary>
        public string CardNumber { get; set; }

        /// <summary>
        /// Issue number.
        /// </summary>
        public string IssueNumber { get; set; }

        /// <summary>
        /// Issue number captured.  i.e. Read from magstripe, etc.
        /// </summary>
        public bool IssueNumberCaptured { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="CardCapture"/> conversion.
    /// </summary>
    public static class CardCaptureConverter
    {
        /// <summary>
        /// Returns a <see cref="CardCaptureViewV01"/> object based on this <see cref="CardCapture"/>.
        /// </summary>
        /// <param name="cardCapture"></param>
        /// <returns></returns>
        public static CardCaptureViewV01 ToCardCaptureViewV01(this CardCapture cardCapture)
        {
            if (cardCapture == null) return null;

            return new CardCaptureViewV01
            {
                CardNumber = cardCapture.CardNumber,
                IssueNumber = cardCapture.IssueNumber,
                IssueNumberCaptured = cardCapture.IssueNumberCaptured
            };
        }

        /// <summary>
        /// Returns a <see cref="CardCapture"/> object based on this <see cref="CardCaptureViewV01"/>.
        /// </summary>
        /// <param name="cardCaptureViewV01"></param>
        /// <returns></returns>
        public static CardCapture ToCardCapture(this CardCaptureViewV01 cardCaptureViewV01)
        {
            if (cardCaptureViewV01 == null) return null;

            return new CardCapture
            {
                CardNumber = cardCaptureViewV01.CardNumber,
                IssueNumber = cardCaptureViewV01.IssueNumber,
                IssueNumberCaptured = cardCaptureViewV01.IssueNumberCaptured
            };
        }
    }
}
