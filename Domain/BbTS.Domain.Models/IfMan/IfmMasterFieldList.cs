﻿// ReSharper disable InconsistentNaming
using System;
namespace BbTS.Domain.Models.IfMan
{
    [Serializable]
    public class IfmMasterFieldList
    {
        public int FieldNumber { get; set; }
        public string FieldName { get; set; }
        public int FieldType { get; set; }
        public int FieldOrder { get; set; }
    }
}