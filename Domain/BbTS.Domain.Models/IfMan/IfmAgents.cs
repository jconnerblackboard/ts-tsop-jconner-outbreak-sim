﻿// ReSharper disable InconsistentNaming
using System;
namespace BbTS.Domain.Models.IfMan
{
    [Serializable]
    public class IfmAgents
    {
        public int Agent_Id { get; set; }
        public string AgentName { get; set; }
        public int AgentType_Id { get; set; }
        public string FileName { get; set; }
        public int WaitTime { get; set; }
        public int WaitTimeType { get; set; }
        public string SendChanges { get; set; }
        public string GetChanges { get; set; }
        public int Status { get; set; }
        public string Image_Enabled { get; set; }
    }
}
