﻿// ReSharper disable InconsistentNaming
using System;
namespace BbTS.Domain.Models.IfMan
{
    [Serializable]
    public class IfmMasterQueue_SendTo
    {
        public int Change_Id { get; set; }
        public int Agent_Id { get; set; }
        public double SentDt { get; set; }
        public double PostDt { get; set; }
        public int ResultCode { get; set; }
        public string Result_ExtraMsg { get; set; }
        public string Image_Sent { get; set; }
    }
}