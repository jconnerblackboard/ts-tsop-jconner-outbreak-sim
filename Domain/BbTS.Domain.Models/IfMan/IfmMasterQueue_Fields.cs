﻿// ReSharper disable InconsistentNaming
using System;
namespace BbTS.Domain.Models.IfMan
{
    [Serializable]
    public class IfmMasterQueue_Fields
    {
        public int Change_Id { get; set; }
        public string Master_Field { get; set; }
        public string FieldValue { get; set; }
        public int MaxFieldLength { get; set; }
    }
}