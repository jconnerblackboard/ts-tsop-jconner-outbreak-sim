﻿// ReSharper disable InconsistentNaming
using System;
namespace BbTS.Domain.Models.IfMan
{
    [Serializable]
    public class IfmMasterQueue_KeyFields
    {
        public int Change_Id { get; set; }
        public int KeyFieldType { get; set; }
        public string KeyValue { get; set; }
        public string NewKeyValue { get; set; }
    }
}