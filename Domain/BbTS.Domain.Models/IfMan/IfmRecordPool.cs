﻿// ReSharper disable InconsistentNaming
using System;
namespace BbTS.Domain.Models.IfMan
{
    [Serializable]
    public class IfmRecordPool
    {
        public int Record_Id { get; set; }
        public int Agent_Id { get; set; }
        public double LastChangeDt { get; set; }
    }
}