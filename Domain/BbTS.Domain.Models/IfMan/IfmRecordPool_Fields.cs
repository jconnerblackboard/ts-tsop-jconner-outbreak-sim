﻿// ReSharper disable InconsistentNaming
using System;
namespace BbTS.Domain.Models.IfMan
{
    [Serializable]
    public class IfmRecordPool_Fields
    {
        public int Record_Id { get; set; }
        public int FieldNumber { get; set; }
        public string FieldValue { get; set; }
        public int MaxFieldLength { get; set; }
    }
}