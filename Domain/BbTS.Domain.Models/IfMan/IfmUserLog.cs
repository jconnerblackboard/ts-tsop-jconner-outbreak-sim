﻿// ReSharper disable InconsistentNaming
using System;
namespace BbTS.Domain.Models.IfMan
{
    [Serializable]
    public class IfmUserLog
    {
        public double DateTime { get; set; }
        public string LoginName { get; set; }
        public string OsUser { get; set; }
        public string Machine { get; set; }
        public string Program { get; set; }
        public string Description { get; set; }
    }
}