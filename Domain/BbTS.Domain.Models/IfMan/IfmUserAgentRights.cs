﻿// ReSharper disable InconsistentNaming
using System;
namespace BbTS.Domain.Models.IfMan
{
    [Serializable]
    public class IfmUserAgentRights
    {
        public int User_Id { get; set; }
        public int AgentType_Id { get; set; }
        public string Rights { get; set; }
    }
}