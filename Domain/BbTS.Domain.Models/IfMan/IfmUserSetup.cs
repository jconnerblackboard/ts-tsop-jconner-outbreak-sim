﻿// ReSharper disable InconsistentNaming
using System;
namespace BbTS.Domain.Models.IfMan
{
    [Serializable]
    public class IfmUserSetup
    {
        public int User_Id { get; set; }
        public string LoginName { get; set; }
        public string FullName { get; set; }
        public string Description { get; set; }
        public byte[] HashedPassword { get; set; }
        public string Rights { get; set; }
    }
}