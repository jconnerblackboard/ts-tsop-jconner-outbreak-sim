﻿// ReSharper disable InconsistentNaming
using System;
namespace BbTS.Domain.Models.IfMan
{
    [Serializable]
    public class IfmAgentsAvailable
    {
        public int AgentType_Id { get; set; }
        public string AgentTypeName { get; set; }
        public string AgentDllFile { get; set; }
        public string CanSendChanges { get; set; }
        public string CanGetChanges { get; set; }
    }
}
