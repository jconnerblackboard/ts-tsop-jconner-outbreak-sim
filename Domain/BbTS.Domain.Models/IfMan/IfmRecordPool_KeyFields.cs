﻿// ReSharper disable InconsistentNaming
using System;
namespace BbTS.Domain.Models.IfMan
{
    [Serializable]
    public class IfmRecordPool_KeyFields
    {
        public int Record_Id { get; set; }
        public int KeyFieldType { get; set; }
        public int Agent_Id { get; set; }
        public string KeyValue { get; set; }
        public string NewKeyValue { get; set; }
    }
}