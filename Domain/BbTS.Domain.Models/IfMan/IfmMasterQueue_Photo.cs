﻿// ReSharper disable InconsistentNaming
using System;
namespace BbTS.Domain.Models.IfMan
{
    [Serializable]
    public class IfmMasterQueue_Photo
    {
        public int Change_Id { get; set; }
        public byte[] Photo_Blob { get; set; }
    }
}