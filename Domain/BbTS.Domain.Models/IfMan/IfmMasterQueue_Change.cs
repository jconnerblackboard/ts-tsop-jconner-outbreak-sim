﻿// ReSharper disable InconsistentNaming
using System;
namespace BbTS.Domain.Models.IfMan
{
    [Serializable]
    public class IfmMasterQueue_Change
    {
        public int Change_Id { get; set; }
        public int FromAgent_Id { get; set; }
        public string ChangeType { get; set; }
        public double ChangeDt { get; set; }
        public double PostDt { get; set; }
        public string Image_Recieved { get; set; }
    }
}