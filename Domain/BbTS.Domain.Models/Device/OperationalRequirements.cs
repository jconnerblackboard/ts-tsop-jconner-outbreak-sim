﻿using System;
using System.Collections.Generic;
using BbTS.Domain.Models.Definitions.Credential;
using BbTS.Domain.Models.Pos.PosOptions;

namespace BbTS.Domain.Models.Device
{
    /// <summary>
    /// Container class for Operator requirements, Customer requirements and product version on a device.
    /// </summary>
    public class OperationalRequirements
    {
        /// <summary>
        /// Identification requirements for a customer using the device.
        /// </summary>
        public List<CredentialType> CustomerIdentificationRequirement { get; set; } = new List<CredentialType>();

        /// <summary>
        /// Login requirements for an operator of the device.
        /// </summary>
        public List<CredentialType> OperatorLoginRequirements { get; set; } = new List<CredentialType>();

        /// <summary>
        /// The product version of Transact OnPremise running on the server.
        /// </summary>
        public string TsopProductVersion { get; set; }

        /// <summary>
        /// Indicates whether or not a client (or device) can show the meal count for a board period.
        /// </summary>
        public bool ShowMealPeriodCountReport { get; set; }

        /// <summary>
        /// Default values
        /// </summary>
        public PosOptionDefaultValues DefaultValues { get; set; }

        /// <summary>
        /// Constraints
        /// </summary>
        public PosOptionConstraints Constraints { get; set; }

        /// <summary>
        /// Feature availability
        /// </summary>
        public PosOptionFeatureAvailability FeatureAvailability { get; set; }

        /// <summary>
        /// Cash drawer properties
        /// </summary>
        public PosOptionCashDrawerProperties CashDrawerProperties { get; set; }

        /// <summary>
        /// Printer properties
        /// </summary>
        public PosOptionPrinterProperties PrinterProperties { get; set; }

        /// <summary>
        /// Override keys
        /// </summary>
        public PosOptionOverrideKeys OverrideKeys { get; set; }

        /// <summary>
        /// The time of day that the device should make a request for updated settings.
        /// </summary>
        public TimeSpan SoftwareUpdateTime { get; set; }
    }

    /// <summary>
    /// View for a <see cref="OperationalRequirements"/>.  (Version 1)
    /// </summary>
    public class OperationalRequirementsViewV01
    {
        /// <summary>
        /// Identification requirements for a customer using the device.
        /// </summary>
        public List<CredentialType> CustomerIdentificationRequirement { get; set; } = new List<CredentialType>();

        /// <summary>
        /// Login requirements for an operator of the device.
        /// </summary>
        public List<CredentialType> OperatorLoginRequirements { get; set; } = new List<CredentialType>();

        /// <summary>
        /// The product version of Transact OnPremise running on the server.
        /// </summary>
        public string TsopProductVersion { get; set; }

        /// <summary>
        /// Indicates whether or not a client (or device) can show the meal count for a board period.
        /// </summary>
        public bool ShowMealPeriodCountReport { get; set; }

        /// <summary>
        /// Default values
        /// </summary>
        public PosOptionDefaultValuesViewV01 DefaultValues { get; set; }

        /// <summary>
        /// Constraints
        /// </summary>
        public PosOptionConstraintsViewV01 Constraints { get; set; }

        /// <summary>
        /// Feature availability
        /// </summary>
        public PosOptionFeatureAvailabilityViewV01 FeatureAvailability { get; set; }

        /// <summary>
        /// Cash drawer properties
        /// </summary>
        public PosOptionCashDrawerPropertiesViewV01 CashDrawerProperties { get; set; }

        /// <summary>
        /// Printer properties
        /// </summary>
        public PosOptionPrinterPropertiesViewV01 PrinterProperties { get; set; }

        /// <summary>
        /// Override keys
        /// </summary>
        public PosOptionOverrideKeysViewV01 OverrideKeys { get; set; }

        /// <summary>
        /// The time of day that the device should make a request for updated settings.
        /// </summary>
        public TimeSpan SoftwareUpdateTime { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="OperationalRequirements"/> conversion.
    /// </summary>
    public static class OperationalRequirementsConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="OperationalRequirementsViewV01"/> object based on this <see cref="OperationalRequirements"/>.
        /// </summary>
        /// <param name="operationalRequirements"></param>
        /// <returns></returns>
        public static OperationalRequirementsViewV01 ToOperationalRequirementsViewV01(this OperationalRequirements operationalRequirements)
        {
            if (operationalRequirements == null) return null;

            return new OperationalRequirementsViewV01
            {
                CustomerIdentificationRequirement = operationalRequirements.CustomerIdentificationRequirement,
                OperatorLoginRequirements = operationalRequirements.OperatorLoginRequirements,
                TsopProductVersion = operationalRequirements.TsopProductVersion,
                ShowMealPeriodCountReport = operationalRequirements.ShowMealPeriodCountReport,
                DefaultValues = operationalRequirements.DefaultValues.ToPosOptionDefaultValuesViewV01(),
                Constraints = operationalRequirements.Constraints.ToPosOptionConstraintsViewV01(),
                FeatureAvailability = operationalRequirements.FeatureAvailability.ToPosOptionFeatureAvailabilityViewV01(),
                CashDrawerProperties = operationalRequirements.CashDrawerProperties.ToPosOptionCashDrawerPropertiesViewV01(),
                PrinterProperties = operationalRequirements.PrinterProperties.ToPosOptionPrinterPropertiesViewV01(),
                OverrideKeys = operationalRequirements.OverrideKeys.ToPosOptionOverrideKeysViewV01(),
                SoftwareUpdateTime = operationalRequirements.SoftwareUpdateTime
            };
        }

        /// <summary>
        /// Returns a <see cref="OperationalRequirements"/> object based on this <see cref="OperationalRequirementsViewV01"/>.
        /// </summary>
        /// <param name="operationalRequirementsViewV01"></param>
        /// <returns></returns>
        public static OperationalRequirements ToOperationalRequirements(this OperationalRequirementsViewV01 operationalRequirementsViewV01)
        {
            if (operationalRequirementsViewV01 == null) return null;

            return new OperationalRequirements
            {
                CustomerIdentificationRequirement = operationalRequirementsViewV01.CustomerIdentificationRequirement,
                OperatorLoginRequirements = operationalRequirementsViewV01.OperatorLoginRequirements,
                TsopProductVersion = operationalRequirementsViewV01.TsopProductVersion,
                ShowMealPeriodCountReport = operationalRequirementsViewV01.ShowMealPeriodCountReport,
                DefaultValues = operationalRequirementsViewV01.DefaultValues.ToPosOptionDefaultValues(),
                Constraints = operationalRequirementsViewV01.Constraints.ToPosOptionConstraints(),
                FeatureAvailability = operationalRequirementsViewV01.FeatureAvailability.ToPosOptionFeatureAvailability(),
                CashDrawerProperties = operationalRequirementsViewV01.CashDrawerProperties.ToPosOptionCashDrawerProperties(),
                PrinterProperties = operationalRequirementsViewV01.PrinterProperties.ToPosOptionPrinterProperties(),
                OverrideKeys = operationalRequirementsViewV01.OverrideKeys.ToPosOptionOverrideKeys(),
                SoftwareUpdateTime = operationalRequirementsViewV01.SoftwareUpdateTime
            };
        }
        #endregion
    }
}
