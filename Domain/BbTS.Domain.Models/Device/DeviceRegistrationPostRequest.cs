﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Definitions.Device;

namespace BbTS.Domain.Models.Device
{
    /// <summary>
    /// Container class to handle a device (slate) registration request.
    /// </summary>
    public class DeviceRegistrationPostRequest
    {
        /// <summary>
        /// The unique identifier of the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The name of the device.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The MAC address for the device.
        /// </summary>
        public string MacAddress { get; set; }

        /// <summary>
        /// The Serial Number of the device.
        /// </summary>
        public string SerialNumber { get; set; }

        /// <summary>
        /// The type of device trying to register.
        /// </summary>
        public DeviceRegistrationDeviceType DeviceType { get; set; }
    }
}
