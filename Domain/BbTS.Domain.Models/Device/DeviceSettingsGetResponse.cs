﻿namespace BbTS.Domain.Models.Device
{
    /// <summary>
    /// Container class to hold the response to a DeviceSettingsGet call.
    /// </summary>
    public class DeviceSettingsGetResponseV01
    {
        /// <summary>
        /// The <see cref="DeviceSettingsViewV01"/> retrieved as part of the request.
        /// </summary>
        public DeviceSettingsViewV01 Settings { get; set; }

        /// <summary>
        /// Parameterless constructor for (de)serialization.
        /// </summary>
        public DeviceSettingsGetResponseV01()
        {

        }

        /// <summary>
        /// Parameterized settings.
        /// </summary>
        /// <param name="settings">The <see cref="DeviceSettingsViewV01"/> retrieved as part of the request.</param>
        public DeviceSettingsGetResponseV01(DeviceSettingsViewV01 settings)
        {
            Settings = settings;
        }
    }
}
