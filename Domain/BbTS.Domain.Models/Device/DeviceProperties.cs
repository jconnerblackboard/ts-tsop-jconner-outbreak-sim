﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using BbTS.Domain.Models.Definitions.Device;

namespace BbTS.Domain.Models.Device
{
    /// <summary>
    /// Container class for information about a device registration.
    /// </summary>
    public class DeviceProperties : IEquatable<DeviceProperties>
    {
        /// <summary>
        /// The human reabable description of the device.
        /// </summary>        
        [Required]
        [DisplayName("Friendly Description")]
        [StringLength(100, ErrorMessage = "Must be 100 characters or less.")]
        public string Description { get; set; }

        /// <summary>
        /// The unique identifier assigned to the device.
        /// </summary>
        [Required]
        [DisplayName("Device Identifier")]
        [StringLength(36, ErrorMessage = "Must be 36 characters or less.")]
        public string DeviceGuid { get; set; }

        /// <summary>
        /// The numerical value associated with the device.
        /// </summary>
        [Required]
        [DisplayName("Device Identity")]
        public int DeviceId { get; set; }

        /// <summary>
        /// The MAC address for the device.
        /// </summary>
        [Required]
        [DisplayName("MAC Address")]
        public string MacAddress { get; set; }

        /// <summary>
        /// The Serial Number of the device.
        /// </summary>
        [Required]
        [DisplayName("Serial Number")]
        public string SerialNumber { get; set; }

        /// <summary>
        /// The type of device trying to register.
        /// </summary>
        [Required]
        [DisplayName("Device Type")]
        public DeviceRegistrationDeviceType DeviceType { get; set; }

        /// <summary>
        /// The date and time the license for the device expires
        /// </summary>
        [Required]
        [DisplayName("License Expiration Date")]
        public DateTime? LicenseExpirationDate { get; set; }

        /// <summary>
        /// Indicates if the device has been registered.
        /// </summary>
        [Required]
        [DisplayName("Is Registered")]
        public bool IsRegistered { get; set; }

        /// <summary>
        /// Pos name
        /// </summary>
        public string PosName { get; set; }

        /// <summary>
        /// Equality operator.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(DeviceProperties other)
        {
            return
                other != null &&
                Description == other.Description &&
                DeviceGuid == other.DeviceGuid &&
                DeviceId == other.DeviceId &&
                MacAddress == other.MacAddress &&
                SerialNumber == other.SerialNumber &&
                DeviceType == other.DeviceType &&
                LicenseExpirationDate == other.LicenseExpirationDate &&
                IsRegistered == other.IsRegistered;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class DevicePropertiesEdit : DeviceProperties
    {
        /// <summary>
        /// The POS list of a DeviceProperties object
        /// </summary>
        [DisplayName("Associated Point-of-Sale")]
        public List<SelectListItem> PosList { get; set; }

        /// <summary>
        /// The posGuid currently selected
        /// </summary>
        [DisplayName("Associated Point-of-Sale unique identifier")]
        public string PosGuid { get; set; }
    }

    /// <summary>
    /// View for a <see cref="DeviceProperties"/>.  (Version 1)
    /// </summary>
    public class DevicePropertiesViewV01
    {
        /// <summary>
        /// The human reabable description of the device.
        /// </summary>        
        public string Description { get; set; }

        /// <summary>
        /// The unique identifier assigned to the device.
        /// </summary>
        public string DeviceGuid { get; set; }

        /// <summary>
        /// The numerical value associated with the device.
        /// </summary>
        public int DeviceId { get; set; }

        /// <summary>
        /// The MAC address for the device.
        /// </summary>
        public string MacAddress { get; set; }

        /// <summary>
        /// The Serial Number of the device.
        /// </summary>
        public string SerialNumber { get; set; }

        /// <summary>
        /// The type of device trying to register.
        /// </summary>
        public DeviceRegistrationDeviceType DeviceType { get; set; }

        /// <summary>
        /// The date and time the license for the device expires
        /// </summary>
        public DateTime? LicenseExpirationDate { get; set; }

        /// <summary>
        /// Indicates if the device has been registered.
        /// </summary>
        public bool IsRegistered { get; set; }

        /// <summary>
        /// Pos name
        /// </summary>
        public string PosName { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="DeviceProperties"/> conversion.
    /// </summary>
    public static class DevicePropertiesConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="DevicePropertiesViewV01"/> object based on this <see cref="DeviceProperties"/>.
        /// </summary>
        /// <param name="deviceProperties"></param>
        /// <returns></returns>
        public static DevicePropertiesViewV01 ToDevicePropertiesViewV01(this DeviceProperties deviceProperties)
        {
            if (deviceProperties == null) return null;

            return new DevicePropertiesViewV01
            {
                Description = deviceProperties.Description,
                DeviceGuid = deviceProperties.DeviceGuid,
                DeviceId = deviceProperties.DeviceId,
                MacAddress = deviceProperties.MacAddress,
                SerialNumber = deviceProperties.SerialNumber,
                DeviceType = deviceProperties.DeviceType,
                LicenseExpirationDate = deviceProperties.LicenseExpirationDate,
                IsRegistered = deviceProperties.IsRegistered,
                PosName = deviceProperties.PosName
            };
        }

        /// <summary>
        /// Returns a <see cref="DeviceProperties"/> object based on this <see cref="DevicePropertiesViewV01"/>.
        /// </summary>
        /// <param name="devicePropertiesViewV01"></param>
        /// <returns></returns>
        public static DeviceProperties ToDeviceProperties(this DevicePropertiesViewV01 devicePropertiesViewV01)
        {
            if (devicePropertiesViewV01 == null) return null;

            return new DeviceProperties
            {
                Description = devicePropertiesViewV01.Description,
                DeviceGuid = devicePropertiesViewV01.DeviceGuid,
                DeviceId = devicePropertiesViewV01.DeviceId,
                MacAddress = devicePropertiesViewV01.MacAddress,
                SerialNumber = devicePropertiesViewV01.SerialNumber,
                DeviceType = devicePropertiesViewV01.DeviceType,
                LicenseExpirationDate = devicePropertiesViewV01.LicenseExpirationDate,
                IsRegistered = devicePropertiesViewV01.IsRegistered,
                PosName = devicePropertiesViewV01.PosName
            };
        }
        #endregion
    }
}
