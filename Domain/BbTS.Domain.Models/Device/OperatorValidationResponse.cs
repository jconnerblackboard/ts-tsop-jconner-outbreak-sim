﻿namespace BbTS.Domain.Models.Device
{
    /// <summary>
    /// Container class to handle a response to an operator validation request.
    /// </summary>
    public class OperatorValidationResponse
    {
        /// <summary>
        /// The unique identifier of the request.
        /// </summary>
        public string RequestId { get; internal set; }

        /// <summary>
        /// The unique identifier assigned to the session (created by the server).
        /// </summary>
        public string SessionId { get; internal set; }

        /// <summary>
        /// The unique identifier assigned to the session (created by the server).
        /// </summary>
        public string OperatorId { get; internal set; }

        /// <summary>
        /// Parameterized constructor requiring the request id.
        /// </summary>
        /// <param name="requestId">The unique identifier of the request.</param>
        /// <param name="sessionId">The unique identifier assigned to the session.</param>
        /// <param name="operatorId">The unique identifier assigned to the operator.</param>
        public OperatorValidationResponse(string requestId, string sessionId, string operatorId)
        {
            RequestId = requestId;
            SessionId = sessionId;
            OperatorId = operatorId;
        }
    }
}
