﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Definitions.Device;

namespace BbTS.Domain.Models.Device
{
    /// <summary>
    /// Container class for handling a server response to a device heartbeat request.
    /// </summary>
    public class DeviceHeartbeatResponse
    {
        /// <summary>
        /// The unique identifier of the request.
        /// </summary>
        public string RequestId { get; internal set; }

        /// <summary>
        /// The unique identifier assigned to the device
        /// </summary>
        public string DeviceId { get; internal set; }

        /// <summary>
        /// The current date and time in utc of the server.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public DateTime DateTimeUTC { get; set; }

        /// <summary>
        /// The UTC offset for the server's installation location.
        /// </summary>
        public string DateTimeOffset { get; set; }

        /// <summary>
        /// TSOP product version
        /// </summary>
        public string TsopProductVersion { get; set; }

        /// <summary>
        /// List of update actions the device needs to perform.
        /// </summary>
        public List<DeviceUpdateActions> UpdateActions { get; set; }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="requestId">The unique identifier of the request.</param>
        /// <param name="deviceId">The unique identifier assigned to the device</param>
        public DeviceHeartbeatResponse(string requestId, string deviceId)
        {
            RequestId = requestId;
            DeviceId = deviceId;
        }
    }
}
