﻿using System.Collections.Generic;
using BbTS.Domain.Models.Credential;

namespace BbTS.Domain.Models.Device
{
    /// <summary>
    /// Container class to handle operator validation requests.
    /// </summary>
    public class OperatorValidationRequest
    {
        /// <summary>
        /// The unique identifier of the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The unique identifier assigned to the device.
        /// </summary>
        public string DeviceId { get; set; }

        /// <summary>
        /// List of credentials for the operator to verify.
        /// </summary>
        public List<CredentialVerifyBase> Credentials { get; set; } 
    }
}
