﻿using System.Collections.Generic;
using System.Linq;
using BbTS.Domain.Models.BoardPlan;
using BbTS.Domain.Models.Credential;
using BbTS.Domain.Models.Event;
using BbTS.Domain.Models.Operator;
using BbTS.Domain.Models.RetailTransaction;
using BbTS.Domain.Models.StoredValue;
using BbTS.Domain.Models.Merchant;
using BbTS.Domain.Models.ProfitCenter;
using BbTS.Domain.Models.Pos;
using BbTS.Domain.Models.Products;
using BbTS.Domain.Models.Terminal;

namespace BbTS.Domain.Models.Device
{
    /// <summary>
    /// Container class to hold settings for a device.
    /// </summary>
    public class DeviceSettings
    {
        /// <summary>
        /// Merchant Guid and Name.
        /// </summary>
        public MerchantDeviceSettings Merchant { get; set; }

        /// <summary>
        /// Profit Center Guid and Name.
        /// </summary>
        public ProfitCenterDeviceSettings ProfitCenter { get; set; }

        /// <summary>
        /// List of all meal periods.
        /// </summary>
        public List<BoardPeriod> Periods { get; set; } = new List<BoardPeriod>();

        /// <summary>
        /// List of board cash equivalency periods.
        /// </summary>
        public List<BoardCashEquivalencyPeriod > BoardCashEquivalencyPeriods = new List<BoardCashEquivalencyPeriod>();

        /// <summary>
        /// List of card formats.
        /// </summary>
        public List<CardFormatForDeviceSettings> CardFormats { get; set; } = new List<CardFormatForDeviceSettings>();

        /// <summary>
        /// List of operators allowed to operate the device.
        /// </summary>
        public List<Operator.Operator> Operators { get; set; } = new List<Operator.Operator>();

        /// <summary>
        /// List of all events supported by the device.
        /// </summary>
        public List<EventDeviceSetting> Events { get; set; } = new List<EventDeviceSetting>();

        /// <summary>
        /// List of all meal types.
        /// </summary>
        public List<MealTypeDeviceSetting> MealTypes { get; set; } = new List<MealTypeDeviceSetting>();

        /// <summary>
        /// Operator requirements, Customer requirements and product version on a device.
        /// </summary>
        public OperationalRequirements OperationalRequirements { get; set; }

        /// <summary>
        /// List of products for the device.
        /// </summary>
        public List<Products.Product> Products { get; set; } = new List<Products.Product>();

        /// <summary>
        /// List of roles that can be assigned to an operator.
        /// </summary>
        public List<OperatorRole> Roles { get; set; } = new List<OperatorRole>();

        /// <summary>
        /// List of all stored value accounts supported on the device.
        /// </summary>
        public List<StoredValueAccountDeviceSetting> StoredValueAccounts = new List<StoredValueAccountDeviceSetting>();

        /// <summary>
        /// List of tax schedules for the device.
        /// </summary>
        public List<TaxScheduleDeviceSetting> TaxSchedules = new List<TaxScheduleDeviceSetting>();

        /// <summary>
        /// List of tenders for the device.
        /// </summary>
        public List<TenderDeviceSetting> Tenders = new List<TenderDeviceSetting>();

        /// <summary>
        /// Device properties
        /// </summary>
        public DeviceProperties Properties { get; set; }

        /// <summary>
        /// Pos credential settings
        /// </summary>
        public PosCredentialSettings PosCredentialSettings { get; set; }

        /// <summary>
        /// Pos credential settings
        /// </summary>
        public EmvHitSettings EmvHitSettings { get; set; }
    }

    /// <summary>
    /// View for a <see cref="DeviceSettings"/>.  (Version 1)
    /// </summary>
    public class DeviceSettingsViewV01
    {
        /// <summary>
        /// Merchant Guid and Name.
        /// </summary>
        public MerchantDeviceSettingsViewV01 Merchant { get; set; }

        /// <summary>
        /// Profit Center Guid and Name.
        /// </summary>
        public ProfitCenterDeviceSettingsViewV01 ProfitCenter { get; set; }

        /// <summary>
        /// List of all meal periods.
        /// </summary>
        public List<BoardPeriodViewV01> Periods { get; set; } = new List<BoardPeriodViewV01>();

        /// <summary>
        /// List of board cash equivalency periods.
        /// </summary>
        public List<BoardCashEquivalencyPeriodViewV01> BoardCashEquivalencyPeriods = new List<BoardCashEquivalencyPeriodViewV01>();

        /// <summary>
        /// List of card formats.
        /// </summary>
        public List<CardFormatForDeviceSettingsViewV01> CardFormats { get; set; } = new List<CardFormatForDeviceSettingsViewV01>();

        /// <summary>
        /// List of operators allowed to operate the device.
        /// </summary>
        public List<OperatorViewV01> Operators { get; set; } = new List<OperatorViewV01>();

        /// <summary>
        /// List of all events supported by the device.
        /// </summary>
        public List<EventDeviceSettingViewV01> Events { get; set; } = new List<EventDeviceSettingViewV01>();

        /// <summary>
        /// List of all meal types.
        /// </summary>
        public List<MealTypeDeviceSettingViewV01> MealTypes { get; set; } = new List<MealTypeDeviceSettingViewV01>();

        /// <summary>
        /// Operator requirements, Customer requirements and product version on a device.
        /// </summary>
        public OperationalRequirementsViewV01 OperationalRequirements { get; set; }

        /// <summary>
        /// List of products for the device.
        /// </summary>
        public List<ProductViewV01> Products { get; set; } = new List<ProductViewV01>();

        /// <summary>
        /// List of roles that can be assigned to an operator.
        /// </summary>
        public List<OperatorRoleViewV01> Roles { get; set; } = new List<OperatorRoleViewV01>();

        /// <summary>
        /// List of all stored value accounts supported on the device.
        /// </summary>
        public List<StoredValueAccountDeviceSettingViewV01> StoredValueAccounts = new List<StoredValueAccountDeviceSettingViewV01>();

        /// <summary>
        /// List of tax schedules for the device.
        /// </summary>
        public List<TaxScheduleDeviceSettingViewV01> TaxSchedules = new List<TaxScheduleDeviceSettingViewV01>();

        /// <summary>
        /// List of tenders for the device.
        /// </summary>
        public List<TenderDeviceSettingViewV01> Tenders = new List<TenderDeviceSettingViewV01>();

        /// <summary>
        /// Device properties
        /// </summary>
        public DevicePropertiesViewV01 Properties { get; set; }

        /// <summary>
        /// Pos credential settings
        /// </summary>
        public PosCredentialSettingsViewV01 PosCredentialSettings { get; set; }

        /// <summary>
        /// Pos credential settings
        /// </summary>
        public EmvHitSettingsViewV01 EmvHitSettings { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="DeviceSettings"/> conversion.
    /// </summary>
    public static class DeviceSettingsConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="DeviceSettingsViewV01"/> object based on this <see cref="DeviceSettings"/>.
        /// </summary>
        /// <param name="deviceSettings"></param>
        /// <returns></returns>
        public static DeviceSettingsViewV01 ToDeviceSettingsViewV01(this DeviceSettings deviceSettings)
        {
            if (deviceSettings == null) return null;

            return new DeviceSettingsViewV01
            {
                Merchant = deviceSettings.Merchant.ToMerchantDeviceSettingsViewV01(),
                ProfitCenter = deviceSettings.ProfitCenter.ToProfitCenterDeviceSettingsViewV01(),
                Periods = deviceSettings.Periods.Select(boardPeriod => boardPeriod.ToBoardPeriodViewV01()).ToList(),
                BoardCashEquivalencyPeriods = deviceSettings.BoardCashEquivalencyPeriods.Select(boardCashEquivalencyPeriod => boardCashEquivalencyPeriod.ToBoardCashEquivalencyPeriodViewV01()).ToList(),
                CardFormats = deviceSettings.CardFormats.Select(cardFormatForDeviceSettings => cardFormatForDeviceSettings.ToCardFormatForDeviceSettingsViewV01()).ToList(),
                Operators = deviceSettings.Operators.Select(deviceOperator => deviceOperator.ToOperatorViewV01()).ToList(),
                Events = deviceSettings.Events.Select(eventDeviceSetting => eventDeviceSetting.ToEventDeviceSettingViewV01()).ToList(),
                MealTypes = deviceSettings.MealTypes.Select(mealTypeDeviceSetting => mealTypeDeviceSetting.ToMealTypeDeviceSettingViewV01()).ToList(),
                OperationalRequirements = deviceSettings.OperationalRequirements.ToOperationalRequirementsViewV01(),
                Products = deviceSettings.Products.Select(product => product.ToProductViewV01()).ToList(),
                Roles = deviceSettings.Roles.Select(operatorRole => operatorRole.ToOperatorRoleViewV01()).ToList(),
                StoredValueAccounts = deviceSettings.StoredValueAccounts.Select(storedValueAccountDeviceSetting => storedValueAccountDeviceSetting.ToStoredValueAccountDeviceSettingViewV01()).ToList(),
                TaxSchedules = deviceSettings.TaxSchedules.Select(taxScheduleDeviceSetting => taxScheduleDeviceSetting.ToTaxScheduleDeviceSettingViewV01()).ToList(),
                Tenders = deviceSettings.Tenders.Select(tenderDeviceSetting => tenderDeviceSetting.ToTenderDeviceSettingViewV01()).ToList(),
                Properties = deviceSettings.Properties.ToDevicePropertiesViewV01(),
                PosCredentialSettings = deviceSettings.PosCredentialSettings.ToPosCredentialSettingsViewV01(),
                EmvHitSettings = deviceSettings.EmvHitSettings.ToEmvHitSettingsViewV01()
            };
        }

        /// <summary>
        /// Returns a <see cref="DeviceSettings"/> object based on this <see cref="DeviceSettingsViewV01"/>.
        /// </summary>
        /// <param name="deviceSettingsViewV01"></param>
        /// <returns></returns>
        public static DeviceSettings ToDeviceSettings(this DeviceSettingsViewV01 deviceSettingsViewV01)
        {
            if (deviceSettingsViewV01 == null) return null;

            return new DeviceSettings
            {
                Merchant = deviceSettingsViewV01.Merchant.ToMerchantDeviceSettings(),
                ProfitCenter = deviceSettingsViewV01.ProfitCenter.ToProfitCenterDeviceSettings(),
                Periods = deviceSettingsViewV01.Periods.Select(boardPeriodViewV01 => boardPeriodViewV01.ToBoardPeriod()).ToList(),
                BoardCashEquivalencyPeriods = deviceSettingsViewV01.BoardCashEquivalencyPeriods.Select(boardCashEquivalencyPeriodViewV01 => boardCashEquivalencyPeriodViewV01.ToBoardCashEquivalencyPeriod()).ToList(),
                CardFormats = deviceSettingsViewV01.CardFormats.Select(cardFormatForDeviceSettingsViewV01 => cardFormatForDeviceSettingsViewV01.ToCardFormatForDeviceSettings()).ToList(),
                Operators = deviceSettingsViewV01.Operators.Select(deviceOperatorViewV01 => deviceOperatorViewV01.ToOperator()).ToList(),
                Events = deviceSettingsViewV01.Events.Select(eventDeviceSettingViewV01 => eventDeviceSettingViewV01.ToEventDeviceSetting()).ToList(),
                MealTypes = deviceSettingsViewV01.MealTypes.Select(mealTypeDeviceSettingViewV01 => mealTypeDeviceSettingViewV01.ToMealTypeDeviceSetting()).ToList(),
                OperationalRequirements = deviceSettingsViewV01.OperationalRequirements.ToOperationalRequirements(),
                Products = deviceSettingsViewV01.Products.Select(productViewV01 => productViewV01.ToProduct()).ToList(),
                Roles = deviceSettingsViewV01.Roles.Select(operatorRoleViewV01 => operatorRoleViewV01.ToOperatorRole()).ToList(),
                StoredValueAccounts = deviceSettingsViewV01.StoredValueAccounts.Select(storedValueAccountDeviceSettingViewV01 => storedValueAccountDeviceSettingViewV01.ToStoredValueAccountDeviceSetting()).ToList(),
                TaxSchedules = deviceSettingsViewV01.TaxSchedules.Select(taxScheduleDeviceSettingViewV01 => taxScheduleDeviceSettingViewV01.ToTaxScheduleDeviceSetting()).ToList(),
                Tenders = deviceSettingsViewV01.Tenders.Select(tenderDeviceSettingViewV01 => tenderDeviceSettingViewV01.ToTenderDeviceSetting()).ToList(),
                Properties = deviceSettingsViewV01.Properties.ToDeviceProperties(),
                PosCredentialSettings = deviceSettingsViewV01.PosCredentialSettings.ToPosCredentialSettings(),
                EmvHitSettings = deviceSettingsViewV01.EmvHitSettings.ToEmvHitSettings()
            };
        }
        #endregion
    }
}
