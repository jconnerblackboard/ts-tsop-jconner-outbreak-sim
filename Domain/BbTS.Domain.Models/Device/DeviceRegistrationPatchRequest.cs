﻿namespace BbTS.Domain.Models.Device
{
    /// <summary>
    /// Container class for a device registration information patch request.
    /// </summary>
    public class DeviceRegistrationPatchRequest
    {
        /// <summary>
        /// The unique identifier for this request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The unique identifier of the device to update.
        /// </summary>
        public string DeviceGuid { get; set; }

        /// <summary>
        /// The pos identifier to associate the device with.
        /// </summary>
        public string PosGuid { get; set; }
    }
}
