﻿namespace BbTS.Domain.Models.Device
{
    /// <summary>
    /// Container class to hold device login request information.
    /// </summary>
    public class DeviceLoginRequest
    {
        /// <summary>
        /// The unique identifier of the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The unique identifier assigned to the device.
        /// </summary>
        public string DeviceId { get; set; }

        /// <summary>
        /// The unique identifier assigned to the operator.
        /// </summary>
        public string OperatorId { get; set; }

        /// <summary>
        /// The unique identifier assigned to the session (created by the client).
        /// </summary>
        public string SessionId { get; set; }
    }
}
