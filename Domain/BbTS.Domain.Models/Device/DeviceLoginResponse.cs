﻿namespace BbTS.Domain.Models.Device
{
    /// <summary>
    /// Container class to handle the response to a device login request.
    /// </summary>
    public class DeviceLoginResponse
    {
        /// <summary>
        /// The unique identifier of the request.
        /// </summary>
        public string RequestId { get; internal set; }

        /// <summary>
        /// Indicates whether or not the operator needs to change their PIN.
        /// </summary>
        public bool PinChangeRequired { get; set; }

        /// <summary>
        /// Parameterized constructor requiring the request id.
        /// </summary>
        /// <param name="requestId">The unique identifier of the request.</param>
        public DeviceLoginResponse(string requestId)
        {
            RequestId = requestId;
        }
    }
}
