﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Device
{
    /// <summary>
    /// Container class to handle device logout requests.
    /// </summary>
    public class DeviceLogoutRequest
    {
        /// <summary>
        /// The unique identifier of the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The unique identifier assigned to the device.
        /// </summary>
        public string DeviceId { get; set; }

        /// <summary>
        /// The unique identifier assigned to the operator.
        /// </summary>
        public string OpoeratorId { get; set; }

        /// <summary>
        /// The unique identifier assigned to the session (created by the client).
        /// </summary>
        public string SessionId { get; set; }
    }
}
