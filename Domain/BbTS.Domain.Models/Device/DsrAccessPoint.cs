﻿using System;

namespace BbTS.Domain.Models.Device
{
    /// <summary>
    /// This object represents an instance of the DsrAccessPoint in the Oracle database
    /// </summary>
    public class DsrAccessPoint
    {
        public Int32 DsrAccessPointId { get; set; }
        public String SerialNumber { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public Int32 DsrServiceId { get; set; }
        public Boolean IsConfirmed { get; set; }
        public Boolean IsOnline { get; set; }
        public String SynchStatus { get; set; }
        public Double? LastSeen { get; set; }
        public Double? LastCommunicationError { get; set; }
        public Int32? DsrHardwareSettingGroupId { get; set; }
        public Int32? BuildingId { get; set; }
        public Int32? AreaId { get; set; }
        public Int32? DoorGroupId { get; set; }
        public Int32 CryptoAlgorithmTypeId { get; set; }
        public String SharedSecretKeyHexString { get; set; }
        public Int32 SharedSecretKeyLifespanSeconds { get; set; }
        public Int32? DsrAccessPointTypeId { get; set; }
        public String DsrAccessPointTypeName { get; set; }
        public Int32? MerchantId { get; set; }
        public Guid DsrAccessPointGuid { get; set; }
        public Int32? AccessModeId { get; set; }
        public Boolean IsActive { get; set; }

    }
}
