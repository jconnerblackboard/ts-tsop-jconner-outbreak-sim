﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Definitions.Licensing;

namespace BbTS.Domain.Models.Device
{
    /// <summary>
    /// Container class for Allegion type door locks.
    /// </summary>
    public class AllegionLock
    {
        /// <summary>
        /// The Id for the master controller equipment.
        /// </summary>
        public int MasterControllerEquipmentId { get; set; }

        /// <summary>
        /// The id of the master controller.
        /// </summary>
        public int MasterControllerId { get; set; }

        /// <summary>
        /// The address where the door is installed at (on the master controller).
        /// </summary>
        public int Address { get; set; }

        /// <summary>
        /// The date and time the device was last updated.
        /// </summary>
        public DateTime UpdateDate { get; set; }

        /// <summary>
        /// The latest date and time.
        /// </summary>
        public DateTime LatestDate { get; set; }

        /// <summary>
        /// The model of the lock.
        /// </summary>
        public string LockModel { get; set; }

        /// <summary>
        /// Serial number (or mac address) of the device.
        /// </summary>
        public string LockSerialNumber { get; set; }

        /// <summary>
        /// The lock comm model.
        /// </summary>
        public string CommModel { get; set; }

        /// <summary>
        /// The serial number of the comm device.
        /// </summary>
        public string CommSerialNumber { get; set; }
        
        /// <summary>
        /// Name of the device.
        /// </summary>
        public string DeviceName { get; set; }
    }
}
