﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BbTS.Domain.Models.Device
{
    /// <summary>
    /// Response to a deviced registration information get request.
    /// </summary>
    public class DevicePropertiesGetResponse
    {
        /// <summary>
        /// The unique identifier assigned to the device.  Null if all devices were requested.
        /// </summary>
        public string DeviceGuid { get; set; }

        /// <summary>
        /// The list of devices requested.  
        /// </summary>
        public List<DeviceProperties> DeviceList { get; set; } = new List<DeviceProperties>();

        /// <summary>
        /// Parameterless constructor for (de)serialization.
        /// </summary>
        public DevicePropertiesGetResponse()
        {
        }

        /// <summary>
        /// Parameterized constructor requring the device guid.
        /// </summary>
        /// <param name="deviceGuid"></param>
        [JsonConstructor]
        public DevicePropertiesGetResponse(string deviceGuid)
        {
            DeviceGuid = deviceGuid;
        }
    }
}
