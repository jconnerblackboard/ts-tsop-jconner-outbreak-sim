﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Device
{
    /// <summary>
    /// Container class for a response to a DeviceOperatorsGet request.
    /// </summary>
    public class DeviceOperatorsGetResponse
    {
        /// <summary>
        /// The unique identifier for the device.
        /// </summary>
        public string DeviceId { get; internal set; }

        /// <summary>
        /// A list of operators allowed to use the device.
        /// </summary>
        public List<Operator.Operator> Operators { get; set; }

        /// <summary>
        /// Parameterized constructor requiring device id to create a response.
        /// </summary>
        /// <param name="deviceId">The unique identifier for the device.</param>
        public DeviceOperatorsGetResponse(string deviceId)
        {
            DeviceId = deviceId;
        }

    }
}
