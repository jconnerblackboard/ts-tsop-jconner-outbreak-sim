﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Device
{
    /// <summary>
    /// Container class to handle the response to a device logout request.
    /// </summary>
    public class DeviceLogoutResponse
    {
        /// <summary>
        /// The unique identifier of the request.
        /// </summary>
        public string RequestId { get; internal set; }

        /// <summary>
        /// Parameterized constructor requiring the request id.
        /// </summary>
        /// <param name="requestId">The unique identifier of the request.</param>
        public DeviceLogoutResponse(string requestId)
        {
            RequestId = requestId;
        }
    }
}
