﻿namespace BbTS.Domain.Models.Device
{
    /// <summary>
    /// Container class for a device registration information patch request.
    /// </summary>
    public class DevicePropertiesDeleteRequest
    {
        /// <summary>
        /// The unique identifier for this request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The unique identifier of the device to delete.
        /// </summary>
        public string DeviceGuid { get; set; }
    }
}
