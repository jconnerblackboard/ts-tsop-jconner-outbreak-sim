﻿using System;
using BbTS.Domain.Models.Definitions.Device;

namespace BbTS.Domain.Models.Device
{
    /// <summary>
    /// Container class for a heartbeat pulse from a device.
    /// </summary>
    public class DeviceHeartbeatRequest
    {
        /// <summary>
        /// The unique identifier of the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The unique identifier assigned to the device
        /// </summary>
        public string DeviceId { get; set; }

        /// <summary>
        /// The current date and time in utc on the device.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public DateTime DateTimeUTC { get; set; }

        /// <summary>
        /// The UTC offset for the device's installation location.
        /// </summary>
        public string DateTimeOffset { get; set; }

        /// <summary>
        /// The current device image version installed on the device.
        /// </summary>
        public string SoftwareVersion { get; set; }

        /// <summary>
        /// The type of device trying to register.
        /// </summary>
        public DeviceRegistrationDeviceType DeviceType { get; set; }
    }
}
