﻿using BbTS.Domain.Models.Merchant;
using BbTS.Domain.Models.ProfitCenter;
using System;

namespace BbTS.Domain.Models.Device
{
    /// <summary>
    /// Device class
    /// </summary>
    public class DeviceInfo : DeviceProperties
    {
        /// <summary>
        /// Pos Guid
        /// </summary>
        public Guid PosGuid { get; set; }

        /// <summary>
        /// Merchant Guid
        /// </summary>
        public Guid MerchantGuid { get; set; }

        /// <summary>
        /// Merchant Name
        /// </summary>
        public string MerchantName { get; set; }

        /// <summary>
        /// Profit Center Guid
        /// </summary>
        public Guid ProfitCenterGuid { get; set; }

        /// <summary>
        /// Profit Center Name
        /// </summary>
        public string ProfitCenterName { get; set; }

        /// <summary>
        /// The time of day that the device should make a request for updated settings.
        /// </summary>
        public TimeSpan SettingsUpdateTime { get; set; }

        /// <summary>
        /// Get device properties
        /// </summary>
        /// <returns>Device properties</returns>
        public DeviceProperties GetProperties()
        {
            return new DeviceProperties()
            {
                Description = Description,
                DeviceGuid = DeviceGuid,
                DeviceId = DeviceId,
                DeviceType = DeviceType,
                IsRegistered = IsRegistered,
                LicenseExpirationDate = LicenseExpirationDate,
                MacAddress = MacAddress,
                SerialNumber = SerialNumber,
                PosName = PosName
            };
        }

        /// <summary>
        /// Get merchant device settings
        /// </summary>
        /// <returns>Merchant device settings</returns>
        public MerchantDeviceSettings GetMerchantSettings()
        {
            return new MerchantDeviceSettings()
            {
                MerchantGuid = MerchantGuid,
                MerchantName = MerchantName
            };
        }

        /// <summary>
        /// Get profit center device settings
        /// </summary>
        /// <returns>Profit center device settings</returns>
        public ProfitCenterDeviceSettings GetProfitCenterSettings()
        {
            return new ProfitCenterDeviceSettings()
            {
                ProfitCenterGuid = ProfitCenterGuid,
                ProfitCenterName = ProfitCenterName
            };
        }
    }
}
