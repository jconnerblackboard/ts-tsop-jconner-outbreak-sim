﻿using BbTS.Domain.Models.Definitions.Device;

namespace BbTS.Domain.Models.Device
{
    /// <summary>
    /// Container class representing the data in the V_IP_READER view.  This is used for the Integration Monitor project.
    /// </summary>
    public class IpReaderViewObject
    {
        /// <summary>
        /// The Mac Address of the device.
        /// </summary>
        public string MacAddress { get; set; }

        /// <summary>
        /// The connection id (what host it is assigned to)
        /// </summary>
        public int ConnectionId { get; set; }

        /// <summary>
        /// The name of the device.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The type of reader.
        /// </summary>
        public int ReaderType { get; set; }

        /// <summary>
        /// The Pos to MasterController ID.
        /// </summary>
        public int PosIdMasterControllerId { get; set; }

        /// <summary>
        /// The reader ID.
        /// </summary>
        public int ReaderId { get; set; }

        /// <summary>
        /// The hardware type.
        /// </summary>
        public int HardwareType { get; set; }

        /// <summary>
        /// Hardware model,
        /// </summary>
        public HardwareModel HardwareModel { get; set; }
    
        /// <summary>
        /// The type of communication the device uses.
        /// </summary>
        public int CommunicationType { get; set; }

        /// <summary>
        /// Is the device active?
        /// </summary>
        public bool IsActive { get; set; }

    }
}
