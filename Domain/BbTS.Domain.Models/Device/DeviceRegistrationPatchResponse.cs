﻿namespace BbTS.Domain.Models.Device
{
    /// <summary>
    /// Container class for the response to a device registration information patch request.
    /// </summary>
    public class DeviceRegistrationPatchResponse
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public DeviceRegistrationPatchResponse()
        {
        }

        /// <summary>
        /// The request that spawned this response.
        /// </summary>
        public DeviceRegistrationPatchRequest Request { get; internal set; }

        /// <summary>
        /// Parameterized constructor requiring request and registration information.
        /// </summary>
        /// <param name="request">The request that spawned this response.</param>
        public DeviceRegistrationPatchResponse(DeviceRegistrationPatchRequest request)
        {
            Request = request;
        }
    }
}
