﻿namespace BbTS.Domain.Models.Device
{
    /// <summary>
    /// Container class to handle a device (slate) registration response.
    /// </summary>
    public class DeviceRegistrationPostResponse
    {
        /// <summary>
        /// Parameterless constructor for deserialization
        /// </summary>
        public DeviceRegistrationPostResponse()
        {
        }

        /// <summary>
        /// The unique identifier of the request.
        /// </summary>
        public string RequestId { get; internal set; }

        /// <summary>
        /// The unique identifier assigned to the device.
        /// </summary>
        public string DeviceId { get; set; }

        /// <summary>
        /// Parameterized constructor.  RequestId is required to create a <see cref="DeviceRegistrationPostResponse"/> object.
        /// </summary>
        /// <param name="requestId">The unique identifier of the request associated with this response object.</param>
        public DeviceRegistrationPostResponse(string requestId)
        {
            RequestId = requestId;
        }
    }
}
