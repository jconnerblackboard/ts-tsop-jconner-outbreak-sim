﻿using System;
using BbTS.Domain.Models.Definitions.Container;

namespace BbTS.Domain.Models.Container
{
    /// <summary>
    /// Contains a timestamp of the specified type.
    /// </summary>
    public class TransactionTimestamp
    {
        /// <summary>
        /// The type of timestamp.
        /// </summary>
        public TimestampType TimestampType { get; set; }

        /// <summary>
        /// Local Date/Time.
        /// </summary>
        public DateTimeOffset Timestamp { get; set; }

        /// <summary>
        /// Returns a deep copy of this object.
        /// </summary>
        /// <returns></returns>
        public TransactionTimestamp Clone()
        {
            return (TransactionTimestamp)MemberwiseClone();
        }
    }

    /// <summary>
    /// View for a <see cref="TransactionTimestamp"/>.  (Version 1)
    /// </summary>
    public class TransactionTimestampViewV01
    {
        /// <summary>
        /// The type of timestamp.
        /// </summary>
        public TimestampType TimestampType { get; set; }

        /// <summary>
        /// Local Date/Time.
        /// </summary>
        public DateTime Timestamp { get; set; }
    }

    /// <summary>
    /// View for a <see cref="TransactionTimestamp"/>.  (Version 2)
    /// </summary>
    public class TransactionTimestampViewV02
    {
        /// <summary>
        /// The type of timestamp.
        /// </summary>
        public TimestampType TimestampType { get; set; }

        /// <summary>
        /// Local Date/Time.
        /// </summary>
        public DateTimeOffset Timestamp { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="TransactionTimestamp"/> conversion.
    /// </summary>
    public static class TransactionTimestampConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="TransactionTimestampViewV01"/> object based on this <see cref="TransactionTimestamp"/>.
        /// </summary>
        /// <param name="transactionTimestamp"></param>
        /// <returns></returns>
        public static TransactionTimestampViewV01 ToTransactionTimestampViewV01(this TransactionTimestamp transactionTimestamp)
        {
            if (transactionTimestamp == null) return null;

            return new TransactionTimestampViewV01
            {
                TimestampType = transactionTimestamp.TimestampType,
                Timestamp = transactionTimestamp.Timestamp.DateTime
            };
        }

        /// <summary>
        /// Returns a <see cref="TransactionTimestamp"/> object based on this <see cref="TransactionTimestampViewV01"/>.
        /// </summary>
        /// <param name="transactionTimestampViewV01"></param>
        /// <returns></returns>
        public static TransactionTimestamp ToTransactionTimestamp(this TransactionTimestampViewV01 transactionTimestampViewV01)
        {
            if (transactionTimestampViewV01 == null) return null;

            return new TransactionTimestamp
            {
                TimestampType = transactionTimestampViewV01.TimestampType,
                Timestamp = transactionTimestampViewV01.Timestamp
            };
        }
        #endregion

        #region Version 2
        /// <summary>
        /// Returns a <see cref="TransactionTimestampViewV02"/> object based on this <see cref="TransactionTimestamp"/>.
        /// </summary>
        /// <param name="transactionTimestamp"></param>
        /// <returns></returns>
        public static TransactionTimestampViewV02 ToTransactionTimestampViewV02(this TransactionTimestamp transactionTimestamp)
        {
            if (transactionTimestamp == null) return null;

            return new TransactionTimestampViewV02
            {
                TimestampType = transactionTimestamp.TimestampType,
                Timestamp = transactionTimestamp.Timestamp
            };
        }

        /// <summary>
        /// Returns a <see cref="TransactionTimestamp"/> object based on this <see cref="TransactionTimestampViewV02"/>.
        /// </summary>
        /// <param name="transactionTimestampViewV02"></param>
        /// <returns></returns>
        public static TransactionTimestamp ToTransactionTimestamp(this TransactionTimestampViewV02 transactionTimestampViewV02)
        {
            if (transactionTimestampViewV02 == null) return null;

            return new TransactionTimestamp
            {
                TimestampType = transactionTimestampViewV02.TimestampType,
                Timestamp = transactionTimestampViewV02.Timestamp
            };
        }
        #endregion
    }
}