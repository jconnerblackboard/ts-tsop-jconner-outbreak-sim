﻿using System;
using BbTS.Domain.Models.Definitions.Originator;

namespace BbTS.Domain.Models.Container.Elements
{
    /// <summary>
    /// Container class for an originator element.
    /// </summary>
    public class OriginatorElement : Element
    {
        /// <summary>
        /// The unique identifier for the originator.
        /// </summary>
        public Guid OriginatorId { get; set; }

        /// <summary>
        /// The type of originator.
        /// </summary>
        public OriginatorType? Type { get; set; } = null;

        /// <summary>
        /// The type of operation mode the originator is in.
        /// </summary>
        public AttendType? AttendType { get; set; } = null;

        /// <summary>
        /// Deep Copy.
        /// </summary>
        /// <returns></returns>
        public override Element Clone()
        {
            var clone = (OriginatorElement)MemberwiseClone();
            return clone;
        }
    }

    /// <summary>
    /// View for a <see cref="OriginatorElement"/>.  (Version 1)
    /// </summary>
    public class OriginatorElementViewV01
    {
        /// <summary>
        /// Guid for this transaction element.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// The order in which this Element should be processed.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// The unique identifier for the originator.
        /// </summary>
        public Guid OriginatorId { get; set; }

        /// <summary>
        /// The type of originator.
        /// </summary>
        public OriginatorType? Type { get; set; }

        /// <summary>
        /// The type of operation mode the originator is in.
        /// </summary>
        public AttendType? AttendType { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="OriginatorElement"/> conversion.
    /// </summary>
    public static class OriginatorElementConverter
    {
        /// <summary>
        /// Returns a <see cref="OriginatorElementViewV01"/> object based on this <see cref="OriginatorElement"/>.
        /// </summary>
        /// <param name="originatorElement"></param>
        /// <returns></returns>
        public static OriginatorElementViewV01 ToOriginatorElementViewV01(this OriginatorElement originatorElement)
        {
            if (originatorElement == null) return null;

            return new OriginatorElementViewV01
            {
                Id = originatorElement.Id,
                Sequence = originatorElement.Sequence,
                OriginatorId = originatorElement.OriginatorId,
                Type = originatorElement.Type,
                AttendType = originatorElement.AttendType
            };
        }

        /// <summary>
        /// Returns a <see cref="OriginatorElement"/> object based on this <see cref="OriginatorElementViewV01"/>.
        /// </summary>
        /// <param name="originatorElementViewV01"></param>
        /// <returns></returns>
        public static OriginatorElement ToOriginatorElement(this OriginatorElementViewV01 originatorElementViewV01)
        {
            if (originatorElementViewV01 == null) return null;

            return new OriginatorElement
            {
                Id = originatorElementViewV01.Id,
                Sequence = originatorElementViewV01.Sequence,
                OriginatorId = originatorElementViewV01.OriginatorId,
                Type = originatorElementViewV01.Type,
                AttendType = originatorElementViewV01.AttendType
            };
        }
    }
}
