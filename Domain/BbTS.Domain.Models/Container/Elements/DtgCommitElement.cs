﻿using System;

namespace BbTS.Domain.Models.Container.Elements
{
    /// <summary>
    /// Direct to Gateway credit card commit.
    /// </summary>
    public class DtgCommitElement : Element
    {
        /// <summary>
        /// The OrderId that should be marked as completed.
        /// </summary>
        public string CompletedOrderId { get; set; }

        /// <inheritdoc />
        public override Element Clone()
        {
            return (DtgCommitElement)MemberwiseClone();
        }
    }

    /// <summary>
    /// View for a <see cref="DtgCommitElement"/>.  (Version 1)
    /// </summary>
    public class DtgCommitElementViewV01
    {
        /// <summary>
        /// Guid for this transaction element.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// The order in which this Element should be processed.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// The OrderId that should be marked as completed.
        /// </summary>
        public string CompletedOrderId { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="DtgCommitElement"/> conversion.
    /// </summary>
    public static class DtgCommitElementConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="DtgCommitElementViewV01"/> object based on this <see cref="DtgCommitElement"/>.
        /// </summary>
        /// <param name="dtgCommitElement"></param>
        /// <returns></returns>
        public static DtgCommitElementViewV01 ToDtgCommitElementViewV01(this DtgCommitElement dtgCommitElement)
        {
            if (dtgCommitElement == null) return null;

            return new DtgCommitElementViewV01
            {
                Id = dtgCommitElement.Id,
                Sequence = dtgCommitElement.Sequence,
                CompletedOrderId = dtgCommitElement.CompletedOrderId
            };
        }

        /// <summary>
        /// Returns a <see cref="DtgCommitElement"/> object based on this <see cref="DtgCommitElementViewV01"/>.
        /// </summary>
        /// <param name="dtgCommitElementViewV01"></param>
        /// <returns></returns>
        public static DtgCommitElement ToDtgCommitElement(this DtgCommitElementViewV01 dtgCommitElementViewV01)
        {
            if (dtgCommitElementViewV01 == null) return null;

            return new DtgCommitElement
            {
                Id = dtgCommitElementViewV01.Id,
                Sequence = dtgCommitElementViewV01.Sequence,
                CompletedOrderId = dtgCommitElementViewV01.CompletedOrderId
            };
        }
        #endregion
    }
}
