﻿using System;
using BbTS.Domain.Models.Definitions.Container;

namespace BbTS.Domain.Models.Container.Elements
{
    /// <summary>
    /// Cashier login or logout.
    /// </summary>
    public class ControlElement : Element
    {
        /// <summary>
        /// Transaction number.
        /// </summary>
        public int TransactionNumber { get; set; }

        /// <summary>
        /// Specifies login or logout.
        /// </summary>
        public ControlType ControlType { get; set; }

        /// <summary>
        /// Guid that is generated at sign-on and will be linked with all transactions that take place until sign-off
        /// </summary>
        public Guid SessionId { get; set; }

        /// <inheritdoc />
        public override Element Clone()
        {
            return (ControlElement)MemberwiseClone();
        }
    }

    /// <summary>
    /// View for a <see cref="ControlElement"/>.  (Version 1)
    /// </summary>
    public class ControlElementViewV01
    {
        /// <summary>
        /// Guid for this transaction element.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// The order in which this Element should be processed.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Transaction number.
        /// </summary>
        public int TransactionNumber { get; set; }

        /// <summary>
        /// Specifies login or logout.
        /// </summary>
        public ControlType ControlType { get; set; }

        /// <summary>
        /// Guid that is generated at sign-on and will be linked with all transactions that take place until sign-off
        /// </summary>
        public Guid SessionId { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="ControlElement"/> conversion.
    /// </summary>
    public static class ControlElementConverter
    {
        /// <summary>
        /// Returns a <see cref="ControlElementViewV01"/> object based on this <see cref="ControlElement"/>.
        /// </summary>
        /// <param name="controlElement"></param>
        /// <returns></returns>
        public static ControlElementViewV01 ToControlElementViewV01(this ControlElement controlElement)
        {
            if (controlElement == null) return null;

            return new ControlElementViewV01
            {
                Id = controlElement.Id,
                Sequence = controlElement.Sequence,
                TransactionNumber = controlElement.TransactionNumber,
                ControlType = controlElement.ControlType,
                SessionId = controlElement.SessionId
            };
        }

        /// <summary>
        /// Returns a <see cref="ControlElement"/> object based on this <see cref="ControlElementViewV01"/>.
        /// </summary>
        /// <param name="controlElementViewV01"></param>
        /// <returns></returns>
        public static ControlElement ToControlElement(this ControlElementViewV01 controlElementViewV01)
        {
            if (controlElementViewV01 == null) return null;

            return new ControlElement
            {
                Id = controlElementViewV01.Id,
                Sequence = controlElementViewV01.Sequence,
                TransactionNumber = controlElementViewV01.TransactionNumber,
                ControlType = controlElementViewV01.ControlType,
                SessionId = controlElementViewV01.SessionId
            };
        }
    }
}
