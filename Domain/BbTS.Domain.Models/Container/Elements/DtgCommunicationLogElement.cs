﻿using System;
using BbTS.Domain.Models.Definitions.Container;

namespace BbTS.Domain.Models.Container.Elements
{
    /// <summary>
    /// Direct to Gateway communication log.
    /// </summary>
    public class DtgCommunicationLogElement : Element
    {
        /// <summary>
        /// Request date/time.
        /// </summary>
        public DateTime RequestDateTime { get; set; }

        /// <summary>
        /// Response date/time.
        /// </summary>
        public DateTime ResponseDateTime { get; set; }

        /// <summary>
        /// Amount authorized.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Specifies if the credit card was swiped or not.
        /// </summary>
        public bool Swiped { get; set; }

        /// <summary>
        /// Specifies if the cardholder's signature was captured.
        /// </summary>
        public bool SignatureCaptured { get; set; }

        /// <summary>
        /// Unique identifier for this authorization.  Typically a GUID.
        /// </summary>
        public string OrderId { get; set; }

        /// <summary>
        /// Identifier supplied by the payment processor.  Typically a GUID.
        /// </summary>
        public string ExternalTransactionId { get; set; }

        /// <summary>
        /// Specifies if the credit card information was obtained from track 1, track 2, or manually entered.
        /// </summary>
        public int TransactionDataSourceId { get; set; }

        /// <summary>
        /// Masked credit card number.
        /// </summary>
        public string CardNumberMasked { get; set; }

        /// <summary>
        /// Transaction type name.
        /// </summary>
        public string TransactionTypeName { get; set; }

        /// <summary>
        /// Transaction commit name.
        /// </summary>
        public string TransactionCommitName { get; set; }

        /// <summary>
        /// Transaction state name.
        /// </summary>
        public string TransactionStateName { get; set; }

        /// <summary>
        /// Authorization code.
        /// </summary>
        public string AuthCode { get; set; }

        /// <summary>
        /// Cardholder present ID.
        /// </summary>
        public string CardholderPresentId { get; set; }

        /// <summary>
        /// Client ID.
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// Pipeline text.
        /// </summary>
        public string PipelineText { get; set; }

        /// <summary>
        /// Request response code ID.
        /// </summary>
        public RequestResponseCode RequestResponseCodeId { get; set; }

        /// <summary>
        /// Indicates the source of a result code.
        /// </summary>
        public BbtsCreditCardDomain ResultDomain { get; set; }

        /// <summary>
        /// A result code.
        /// </summary>
        public int ResultDomainId { get; set; }

        /// <summary>
        /// XML provided by payment processor corresponding to the MessageList tag.
        /// </summary>
        public string MessageListText { get; set; }

        /// <summary>
        /// XML provided by payment processor corresponding to the Overview tag.
        /// </summary>
        public string OverviewText { get; set; }

        /// <inheritdoc />
        public override Element Clone()
        {
            return (DtgCommunicationLogElement)MemberwiseClone();
        }
    }

    /// <summary>
    /// View for a <see cref="DtgCommunicationLogElement"/>.  (Version 1)
    /// </summary>
    public class DtgCommunicationLogElementViewV01
    {
        /// <summary>
        /// Guid for this transaction element.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// The order in which this Element should be processed.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Request date/time.
        /// </summary>
        public DateTime RequestDateTime { get; set; }

        /// <summary>
        /// Response date/time.
        /// </summary>
        public DateTime ResponseDateTime { get; set; }

        /// <summary>
        /// Amount authorized.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Specifies if the credit card was swiped or not.
        /// </summary>
        public bool Swiped { get; set; }

        /// <summary>
        /// Specifies if the cardholder's signature was captured.
        /// </summary>
        public bool SignatureCaptured { get; set; }

        /// <summary>
        /// Unique identifier for this authorization.  Typically a GUID.
        /// </summary>
        public string OrderId { get; set; }

        /// <summary>
        /// Identifier supplied by the payment processor.  Typically a GUID.
        /// </summary>
        public string ExternalTransactionId { get; set; }

        /// <summary>
        /// Specifies if the credit card information was obtained from track 1, track 2, or manually entered.
        /// </summary>
        public int TransactionDataSourceId { get; set; }

        /// <summary>
        /// Masked credit card number.
        /// </summary>
        public string CardNumberMasked { get; set; }

        /// <summary>
        /// Transaction type name.
        /// </summary>
        public string TransactionTypeName { get; set; }

        /// <summary>
        /// Transaction commit name.
        /// </summary>
        public string TransactionCommitName { get; set; }

        /// <summary>
        /// Transaction state name.
        /// </summary>
        public string TransactionStateName { get; set; }

        /// <summary>
        /// Authorization code.
        /// </summary>
        public string AuthCode { get; set; }

        /// <summary>
        /// Cardholder present ID.
        /// </summary>
        public string CardholderPresentId { get; set; }

        /// <summary>
        /// Client ID.
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// Pipeline text.
        /// </summary>
        public string PipelineText { get; set; }

        /// <summary>
        /// Request response code ID.
        /// </summary>
        public RequestResponseCode RequestResponseCodeId { get; set; }

        /// <summary>
        /// Indicates the source of a result code.
        /// </summary>
        public BbtsCreditCardDomain ResultDomain { get; set; }

        /// <summary>
        /// A result code.
        /// </summary>
        public int ResultDomainId { get; set; }

        /// <summary>
        /// XML provided by payment processor corresponding to the MessageList tag.
        /// </summary>
        public string MessageListText { get; set; }

        /// <summary>
        /// XML provided by payment processor corresponding to the Overview tag.
        /// </summary>
        public string OverviewText { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="DtgCommunicationLogElement"/> conversion.
    /// </summary>
    public static class DtgCommunicationLogElementConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="DtgCommunicationLogElementViewV01"/> object based on this <see cref="DtgCommunicationLogElement"/>.
        /// </summary>
        /// <param name="dtgCommunicationLogElement"></param>
        /// <returns></returns>
        public static DtgCommunicationLogElementViewV01 ToDtgCommunicationLogElementViewV01(this DtgCommunicationLogElement dtgCommunicationLogElement)
        {
            if (dtgCommunicationLogElement == null) return null;

            return new DtgCommunicationLogElementViewV01
            {
                Id = dtgCommunicationLogElement.Id,
                Sequence = dtgCommunicationLogElement.Sequence,
                RequestDateTime = dtgCommunicationLogElement.RequestDateTime,
                ResponseDateTime = dtgCommunicationLogElement.ResponseDateTime,
                Amount = dtgCommunicationLogElement.Amount,
                Swiped = dtgCommunicationLogElement.Swiped,
                SignatureCaptured = dtgCommunicationLogElement.SignatureCaptured,
                OrderId = dtgCommunicationLogElement.OrderId,
                ExternalTransactionId = dtgCommunicationLogElement.ExternalTransactionId,
                TransactionDataSourceId = dtgCommunicationLogElement.TransactionDataSourceId,
                CardNumberMasked = dtgCommunicationLogElement.CardNumberMasked,
                TransactionTypeName = dtgCommunicationLogElement.TransactionTypeName,
                TransactionCommitName = dtgCommunicationLogElement.TransactionCommitName,
                TransactionStateName = dtgCommunicationLogElement.TransactionStateName,
                AuthCode = dtgCommunicationLogElement.AuthCode,
                CardholderPresentId = dtgCommunicationLogElement.CardholderPresentId,
                ClientId = dtgCommunicationLogElement.ClientId,
                PipelineText = dtgCommunicationLogElement.PipelineText,
                RequestResponseCodeId = dtgCommunicationLogElement.RequestResponseCodeId,
                ResultDomain = dtgCommunicationLogElement.ResultDomain,
                ResultDomainId = dtgCommunicationLogElement.ResultDomainId,
                MessageListText = dtgCommunicationLogElement.MessageListText,
                OverviewText = dtgCommunicationLogElement.OverviewText
            };
        }

        /// <summary>
        /// Returns a <see cref="DtgCommunicationLogElement"/> object based on this <see cref="DtgCommunicationLogElementViewV01"/>.
        /// </summary>
        /// <param name="dtgCommunicationLogElementViewV01"></param>
        /// <returns></returns>
        public static DtgCommunicationLogElement ToDtgCommunicationLogElement(this DtgCommunicationLogElementViewV01 dtgCommunicationLogElementViewV01)
        {
            if (dtgCommunicationLogElementViewV01 == null) return null;

            return new DtgCommunicationLogElement
            {
                Id = dtgCommunicationLogElementViewV01.Id,
                Sequence = dtgCommunicationLogElementViewV01.Sequence,
                RequestDateTime = dtgCommunicationLogElementViewV01.RequestDateTime,
                ResponseDateTime = dtgCommunicationLogElementViewV01.ResponseDateTime,
                Amount = dtgCommunicationLogElementViewV01.Amount,
                Swiped = dtgCommunicationLogElementViewV01.Swiped,
                SignatureCaptured = dtgCommunicationLogElementViewV01.SignatureCaptured,
                OrderId = dtgCommunicationLogElementViewV01.OrderId,
                ExternalTransactionId = dtgCommunicationLogElementViewV01.ExternalTransactionId,
                TransactionDataSourceId = dtgCommunicationLogElementViewV01.TransactionDataSourceId,
                CardNumberMasked = dtgCommunicationLogElementViewV01.CardNumberMasked,
                TransactionTypeName = dtgCommunicationLogElementViewV01.TransactionTypeName,
                TransactionCommitName = dtgCommunicationLogElementViewV01.TransactionCommitName,
                TransactionStateName = dtgCommunicationLogElementViewV01.TransactionStateName,
                AuthCode = dtgCommunicationLogElementViewV01.AuthCode,
                CardholderPresentId = dtgCommunicationLogElementViewV01.CardholderPresentId,
                ClientId = dtgCommunicationLogElementViewV01.ClientId,
                PipelineText = dtgCommunicationLogElementViewV01.PipelineText,
                RequestResponseCodeId = dtgCommunicationLogElementViewV01.RequestResponseCodeId,
                ResultDomain = dtgCommunicationLogElementViewV01.ResultDomain,
                ResultDomainId = dtgCommunicationLogElementViewV01.ResultDomainId,
                MessageListText = dtgCommunicationLogElementViewV01.MessageListText,
                OverviewText = dtgCommunicationLogElementViewV01.OverviewText
            };
        }
        #endregion
    }
}
