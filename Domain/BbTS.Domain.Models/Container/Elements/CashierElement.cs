﻿using System;
using BbTS.Domain.Models.Definitions.Container;

namespace BbTS.Domain.Models.Container.Elements
{
    /// <summary>
    /// The Cashier associated with the transaction.
    /// </summary>
    [Obsolete("Use OperatorElement instead.")]
    public class CashierElement : Element
    {
        /// <summary>
        /// The unique identifier for the cashier.
        /// </summary>
        public string CashierGuid { get; set; }

        /// <summary>
        /// Cashier Id.
        /// </summary>
        public int CashierId { get; set; }
        
        /// <summary>
        /// Specifies if the cashier was operating under normal rights or if Manager Override was used.
        /// </summary>
        public CashierMode CashierMode { get; set; }

        /// <summary>
        /// Guid that is generated at sign-on and will be linked with all transactions that take place until sign-off
        /// </summary>
        public string SessionId { get; set; }

        /// <summary>
        /// The drawer number the cashier is currently using.
        /// </summary>
        public int CashDrawerNumber { get; set; }

        /// <inheritdoc />
        public override Element Clone()
        {
            return (CashierElement)MemberwiseClone();
        }
    }

    /// <summary>
    /// View for a <see cref="CashierElement"/>.  (Version 1)
    /// </summary>
    public class CashierElementViewV01
    {
        /// <summary>
        /// Guid for this transaction element.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// The order in which this Element should be processed.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// The unique identifier for the cashier.
        /// </summary>
        public string CashierGuid { get; set; }

        /// <summary>
        /// Cashier Id.
        /// </summary>
        public int CashierId { get; set; }

        /// <summary>
        /// Specifies if the cashier was operating under normal rights or if Manager Override was used.
        /// </summary>
        public CashierMode CashierMode { get; set; }

        /// <summary>
        /// Guid that is generated at sign-on and will be linked with all transactions that take place until sign-off
        /// </summary>
        public string SessionId { get; set; }

        /// <summary>
        /// The drawer number the cashier is currently using.
        /// </summary>
        public int CashDrawerNumber { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="CashierElement"/> conversion.
    /// </summary>
    public static class CashierElementConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="CashierElementViewV01"/> object based on this <see cref="CashierElement"/>.
        /// </summary>
        /// <param name="cashierElement"></param>
        /// <returns></returns>
        public static CashierElementViewV01 ToCashierElementViewV01(this CashierElement cashierElement)
        {
            if (cashierElement == null) return null;

            return new CashierElementViewV01
            {
                Id = cashierElement.Id,
                Sequence = cashierElement.Sequence,
                CashierGuid = cashierElement.CashierGuid,
                CashierId = cashierElement.CashierId,
                CashierMode = cashierElement.CashierMode,
                SessionId = cashierElement.SessionId,
                CashDrawerNumber = cashierElement.CashDrawerNumber
            };
        }

        /// <summary>
        /// Returns a <see cref="CashierElement"/> object based on this <see cref="CashierElementViewV01"/>.
        /// </summary>
        /// <param name="cashierElementViewV01"></param>
        /// <returns></returns>
        public static CashierElement ToCashierElement(this CashierElementViewV01 cashierElementViewV01)
        {
            if (cashierElementViewV01 == null) return null;

            return new CashierElement
            {
                Id = cashierElementViewV01.Id,
                Sequence = cashierElementViewV01.Sequence,
                CashierGuid = cashierElementViewV01.CashierGuid,
                CashierId = cashierElementViewV01.CashierId,
                CashierMode = cashierElementViewV01.CashierMode,
                SessionId = cashierElementViewV01.SessionId,
                CashDrawerNumber = cashierElementViewV01.CashDrawerNumber
            };
        }
        #endregion
    }
}
