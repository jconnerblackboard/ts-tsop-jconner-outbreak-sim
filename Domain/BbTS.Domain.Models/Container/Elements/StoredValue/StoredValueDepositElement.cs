﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Credential;

namespace BbTS.Domain.Models.Container.Elements.StoredValue
{
    /// <summary>
    /// Container element class to hold transactional information about a stored value deposit.
    /// </summary>
    public class StoredValueDepositElement : Element
    {
        /// <summary>
        /// The amount of the deposit.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Customer credential associated with this transaction.
        /// </summary>
        public CredentialWebApi Credential { get; set; }

        /// <summary>
        /// Element.Clone override.
        /// </summary>
        /// <returns>Deep copy of a <see cref="StoredValueDepositElement"/> object.</returns>
        public override Element Clone()
        {
            var clone = MemberwiseClone() as StoredValueDepositElement;
            if (clone != null)
            {
                clone.Credential = Credential.Clone();
            }
            return clone;
        }
    }
}
