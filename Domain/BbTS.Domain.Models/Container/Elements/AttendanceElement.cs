﻿using System;
using BbTS.Domain.Models.ArtsDataModel;
using BbTS.Domain.Models.Credential;
using BbTS.Domain.Models.Definitions.ArtsDataModel;
using BbTS.Domain.Models.Definitions.Container;
using BbTS.Domain.Models.Definitions.Credential;

namespace BbTS.Domain.Models.Container.Elements
{
    /// <summary>
    /// Attended Event transaction.
    /// </summary>
    public class AttendanceElement : Element
    {
        /// <summary>
        /// Transaction number.
        /// </summary>
        public int TransactionNumber { get; set; }

        /// <summary>
        /// The type of transaction [Regular, Guest, Non-customer] + [Entry, Re-entry, Swipe-out, Reverse]
        /// </summary>
        public AttendedEventTranType TransactionType { get; set; }

        /// <summary>
        /// The number of the event.
        /// </summary>
        public int EventNumber { get; set; }

        /// <summary>
        /// Customer credential.
        /// </summary>
        public CustomerCredential CustomerCredential { get; set; }

        /// <inheritdoc />
        public override Element Clone()
        {
            var attendanceElement = (AttendanceElement)MemberwiseClone();
            attendanceElement.CustomerCredential.Clone();
            return attendanceElement;
        }
    }

    /// <summary>
    /// View for a <see cref="AttendanceElement"/>.  (Version 1)
    /// </summary>
    public class AttendanceElementViewV01
    {
        /// <summary>
        /// Guid for this transaction element.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// The order in which this Element should be processed.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Transaction number.
        /// </summary>
        public int TransactionNumber { get; set; }

        /// <summary>
        /// The BbTS customer card used.
        /// </summary>
        public CardCaptureViewV01 CustomerCard { get; set; }

        /// <summary>
        /// The type of transaction [Regular, Guest, Non-customer] + [Entry, Re-entry, Swipe-out, Reverse]
        /// </summary>
        public AttendedEventTranType TransactionType { get; set; }

        /// <summary>
        /// The number of the event.
        /// </summary>
        public int EventNumber { get; set; }

        /// <summary>
        /// Customer guid.
        /// </summary>
        public Guid? CustomerGuid { get; set; }
    }

    /// <summary>
    /// View for a <see cref="AttendanceElement"/>.  (Version 2)
    /// </summary>
    public class AttendanceElementViewV02
    {
        /// <summary>
        /// Guid for this transaction element.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// The order in which this Element should be processed.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Transaction number.
        /// </summary>
        public int TransactionNumber { get; set; }

        /// <summary>
        /// The type of transaction [Regular, Guest, Non-customer] + [Entry, Re-entry, Swipe-out, Reverse]
        /// </summary>
        public AttendedEventTranType TransactionType { get; set; }

        /// <summary>
        /// The number of the event.
        /// </summary>
        public int EventNumber { get; set; }

        /// <summary>
        /// Customer credential.
        /// </summary>
        public CustomerCredentialViewV02 CustomerCredential { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="AttendanceElement"/> conversion.
    /// </summary>
    public static class AttendanceElementConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="AttendanceElementViewV01"/> object based on this <see cref="AttendanceElement"/>.
        /// </summary>
        /// <param name="attendanceElement"></param>
        /// <returns></returns>
        public static AttendanceElementViewV01 ToAttendanceElementViewV01(this AttendanceElement attendanceElement)
        {
            if (attendanceElement == null) return null;

            return new AttendanceElementViewV01
            {
                Id = attendanceElement.Id,
                Sequence = attendanceElement.Sequence,
                TransactionNumber = attendanceElement.TransactionNumber,
                CustomerCard = attendanceElement.CustomerCredential.CustomerCardInfo.ToCardCaptureViewV01(),
                TransactionType = attendanceElement.TransactionType,
                EventNumber = attendanceElement.EventNumber,
                CustomerGuid = attendanceElement.CustomerCredential.CustomerGuid
            };
        }

        /// <summary>
        /// Returns a <see cref="AttendanceElement"/> object based on this <see cref="AttendanceElementViewV01"/>.
        /// </summary>
        /// <param name="attendanceElementViewV01"></param>
        /// <returns></returns>
        public static AttendanceElement ToAttendanceElement(this AttendanceElementViewV01 attendanceElementViewV01)
        {
            if (attendanceElementViewV01 == null) return null;

            CustomerCredential customerCredential = null;
            bool isCustomerTransaction =
                attendanceElementViewV01.TransactionType != AttendedEventTranType.NonCustomerEntry &&
                attendanceElementViewV01.TransactionType != AttendedEventTranType.NonCustomerSwipeOut &&
                attendanceElementViewV01.TransactionType != AttendedEventTranType.NonCustomerReverse;

            if (isCustomerTransaction && attendanceElementViewV01.CustomerCard != null)
            {
                customerCredential = new CustomerCredential
                {
                    CustomerGuid = attendanceElementViewV01.CustomerGuid,
                    PhysicalIdType = CustomerTenderPhysicalIdType.Card,
                    EntryMethodType = attendanceElementViewV01.CustomerCard.IssueNumberCaptured ? CustomerTenderEntryMethodType.Swiped : CustomerTenderEntryMethodType.ManuallyEntered,
                    SecondaryAuthEntered = false,
                    CustomerCardInfo = attendanceElementViewV01.CustomerCard.ToCardCapture()
                };
            }

            return new AttendanceElement
            {
                Id = attendanceElementViewV01.Id,
                Sequence = attendanceElementViewV01.Sequence,
                TransactionNumber = attendanceElementViewV01.TransactionNumber,
                TransactionType = attendanceElementViewV01.TransactionType,
                EventNumber = attendanceElementViewV01.EventNumber,
                CustomerCredential = customerCredential
            };
        }
        #endregion

        #region Version 2
        /// <summary>
        /// Returns a <see cref="AttendanceElementViewV02"/> object based on this <see cref="AttendanceElement"/>.
        /// </summary>
        /// <param name="attendanceElement"></param>
        /// <returns></returns>
        public static AttendanceElementViewV02 ToAttendanceElementViewV02(this AttendanceElement attendanceElement)
        {
            if (attendanceElement == null) return null;

            return new AttendanceElementViewV02
            {
                Id = attendanceElement.Id,
                Sequence = attendanceElement.Sequence,
                TransactionNumber = attendanceElement.TransactionNumber,
                TransactionType = attendanceElement.TransactionType,
                EventNumber = attendanceElement.EventNumber,
                CustomerCredential = attendanceElement.CustomerCredential.ToCustomerCredentialViewV02()
            };
        }

        /// <summary>
        /// Returns a <see cref="AttendanceElement"/> object based on this <see cref="AttendanceElementViewV02"/>.
        /// </summary>
        /// <param name="attendanceElementViewV02"></param>
        /// <returns></returns>
        public static AttendanceElement ToAttendanceElement(this AttendanceElementViewV02 attendanceElementViewV02)
        {
            if (attendanceElementViewV02 == null) return null;

            return new AttendanceElement
            {
                Id = attendanceElementViewV02.Id,
                Sequence = attendanceElementViewV02.Sequence,
                TransactionNumber = attendanceElementViewV02.TransactionNumber,
                TransactionType = attendanceElementViewV02.TransactionType,
                EventNumber = attendanceElementViewV02.EventNumber,
                CustomerCredential = attendanceElementViewV02.CustomerCredential.ToCustomerCredential()
            };
        }
        #endregion
    }
}
