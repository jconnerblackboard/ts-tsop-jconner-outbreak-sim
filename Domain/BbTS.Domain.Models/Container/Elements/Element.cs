﻿using System;

namespace BbTS.Domain.Models.Container.Elements
{
    /// <summary>
    /// Ancestor class for all Element descendants.
    /// </summary>
    public abstract class Element
    {
        /// <summary>
        /// Guid for this transaction element.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// The order in which this Element should be processed.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Returns a deep copy of this object.
        /// </summary>
        /// <returns></returns>
        public abstract Element Clone();
    }
}
