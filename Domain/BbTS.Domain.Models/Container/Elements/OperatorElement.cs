﻿using System;
using BbTS.Domain.Models.Definitions.Container;

namespace BbTS.Domain.Models.Container.Elements
{
    /// <summary>
    /// The Operator associated with the transaction.
    /// </summary>
    public class OperatorElement : Element
    {
        /// <summary>
        /// The unique identifier for the Operator.
        /// </summary>
        public Guid OperatorGuid { get; set; }

        /// <summary>
        /// Specifies if the Operator was operating under normal rights or if Manager Override was used.
        /// </summary>
        public CashierMode OperatorMode { get; set; }

        /// <summary>
        /// Guid that is generated at sign-on and will be linked with all transactions that take place until sign-off
        /// </summary>
        public Guid SessionId { get; set; }

        /// <summary>
        /// The drawer number the Operator is currently using.
        /// </summary>
        public int CashDrawerNumber { get; set; }

        /// <inheritdoc />
        public override Element Clone()
        {
            return (OperatorElement)MemberwiseClone();
        }

        /// <summary>
        /// Empty Constructor.
        /// </summary>
        public OperatorElement()
        {
        }

        /// <summary>
        /// Constructor to convert a cashier element into an operator element.
        /// </summary>
        /// <param name="cashier"></param>
        public OperatorElement(CashierElement cashier)
        {
            OperatorGuid = Guid.Parse(cashier.CashierGuid);
            OperatorMode = cashier.CashierMode;
            SessionId = Guid.Parse(cashier.SessionId);
            CashDrawerNumber = cashier.CashDrawerNumber;
        }
    }

    /// <summary>
    /// View for a <see cref="OperatorElement"/>.  (Version 1)
    /// </summary>
    public class OperatorElementViewV01
    {
        /// <summary>
        /// Guid for this transaction element.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// The order in which this Element should be processed.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// The unique identifier for the Operator.
        /// </summary>
        public Guid OperatorGuid { get; set; }

        /// <summary>
        /// Specifies if the Operator was operating under normal rights or if Manager Override was used.
        /// </summary>
        public CashierMode OperatorMode { get; set; }

        /// <summary>
        /// Guid that is generated at sign-on and will be linked with all transactions that take place until sign-off
        /// </summary>
        public Guid SessionId { get; set; }

        /// <summary>
        /// The drawer number the Operator is currently using.
        /// </summary>
        public int CashDrawerNumber { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="OperatorElement"/> conversion.
    /// </summary>
    public static class OperatorElementConverter
    {
        /// <summary>
        /// Returns a <see cref="OperatorElementViewV01"/> object based on this <see cref="OperatorElement"/>.
        /// </summary>
        /// <param name="operatorElement"></param>
        /// <returns></returns>
        public static OperatorElementViewV01 ToOperatorElementViewV01(this OperatorElement operatorElement)
        {
            if (operatorElement == null) return null;

            return new OperatorElementViewV01
            {
                Id = operatorElement.Id,
                Sequence = operatorElement.Sequence,
                OperatorGuid = operatorElement.OperatorGuid,
                OperatorMode = operatorElement.OperatorMode,
                SessionId = operatorElement.SessionId,
                CashDrawerNumber = operatorElement.CashDrawerNumber
            };
        }

        /// <summary>
        /// Returns a <see cref="OperatorElement"/> object based on this <see cref="OperatorElementViewV01"/>.
        /// </summary>
        /// <param name="operatorElementViewV01"></param>
        /// <returns></returns>
        public static OperatorElement ToOperatorElement(this OperatorElementViewV01 operatorElementViewV01)
        {
            if (operatorElementViewV01 == null) return null;

            return new OperatorElement
            {
                Id = operatorElementViewV01.Id,
                Sequence = operatorElementViewV01.Sequence,
                OperatorGuid = operatorElementViewV01.OperatorGuid,
                OperatorMode = operatorElementViewV01.OperatorMode,
                SessionId = operatorElementViewV01.SessionId,
                CashDrawerNumber = operatorElementViewV01.CashDrawerNumber
            };
        }
    }
}
