﻿using System;
using BbTS.Domain.Models.Definitions.Container;

namespace BbTS.Domain.Models.Container.Elements
{
    /// <summary>
    /// Container to hold binary serialized transaction data produced by the MF4100 Pos Client.
    /// 
    /// </summary>
    public class BinaryTransactionElement : Element
    {
        /// <summary>
        /// Type of the transaction as specified in the file name.
        /// </summary>
        public TransactionType TransactionType { get; set; }

        /// <summary>
        /// Full name (with path and extension) of the file that contained the binary data.
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Hex string representing the binary serialized contents of the transaction file.
        /// </summary>
        // Typically, this will consist of:
        //    int OfflineTransactionHeader
        //    ushort OfflineTransactionVersion
        //    TransactionType transactionType (int)
        //    Binary serialized data, typically a Pos4100Types.Transaction descendant, but not always.
        public string HexData { get; set; }

        /// <inheritdoc />
        public override Element Clone()
        {
            return (BinaryTransactionElement)MemberwiseClone();
        }
    }

    /// <summary>
    /// View for a <see cref="BinaryTransactionElement"/>.  (Version 1)
    /// </summary>
    public class BinaryTransactionElementViewV01
    {
        /// <summary>
        /// Guid for this transaction element.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// The order in which this Element should be processed.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Type of the transaction as specified in the file name.
        /// </summary>
        public TransactionType TransactionType { get; set; }

        /// <summary>
        /// Full name (with path and extension) of the file that contained the binary data.
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Hex string representing the binary serialized contents of the transaction file.
        /// </summary>
        public string HexData { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="BinaryTransactionElement"/> conversion.
    /// </summary>
    public static class BinaryTransactionElementConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="BinaryTransactionElementViewV01"/> object based on this <see cref="BinaryTransactionElement"/>.
        /// </summary>
        /// <param name="binaryTransactionElement"></param>
        /// <returns></returns>
        public static BinaryTransactionElementViewV01 ToBinaryTransactionElementViewV01(this BinaryTransactionElement binaryTransactionElement)
        {
            if (binaryTransactionElement == null) return null;

            return new BinaryTransactionElementViewV01
            {
                Id = binaryTransactionElement.Id,
                Sequence = binaryTransactionElement.Sequence,
                TransactionType = binaryTransactionElement.TransactionType,
                FileName = binaryTransactionElement.FileName,
                HexData = binaryTransactionElement.HexData
            };
        }

        /// <summary>
        /// Returns a <see cref="BinaryTransactionElement"/> object based on this <see cref="BinaryTransactionElementViewV01"/>.
        /// </summary>
        /// <param name="binaryTransactionElementViewV01"></param>
        /// <returns></returns>
        public static BinaryTransactionElement ToBinaryTransactionElement(this BinaryTransactionElementViewV01 binaryTransactionElementViewV01)
        {
            if (binaryTransactionElementViewV01 == null) return null;

            return new BinaryTransactionElement
            {
                Id = binaryTransactionElementViewV01.Id,
                Sequence = binaryTransactionElementViewV01.Sequence,
                TransactionType = binaryTransactionElementViewV01.TransactionType,
                FileName = binaryTransactionElementViewV01.FileName,
                HexData = binaryTransactionElementViewV01.HexData
            };
        }
        #endregion
    }
}
