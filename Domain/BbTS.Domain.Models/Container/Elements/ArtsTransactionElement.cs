﻿using System;
using System.Collections.Generic;
using System.Linq;
using BbTS.Domain.Models.ArtsDataModel;
using BbTS.Domain.Models.Definitions.Container;

namespace BbTS.Domain.Models.Container.Elements
{
    /// <summary>
    /// Retail transaction using the ARTS data model.
    /// </summary>
    public class ArtsTransactionElement : Element
    {
        /// <summary>
        /// Transaction number.
        /// </summary>
        public int TransactionNumber { get; set; }

        /// <summary>
        /// True if this transaction is a cancelled transaction.
        /// </summary>
        public bool CancelledFlag { get; set; }

        /// <summary>
        /// Specifies if the transaction was accepted or denied while online or offline.
        /// </summary>
        public ValidationType ValidationType { get; set; }

        /// <summary>
        /// Specifies if the transaction is a sale or return.
        /// </summary>
        public RetailTranType TranType { get; set; }

        /// <summary>
        /// Control total before this transaction.
        /// </summary>
        public decimal? ControlTotalBeforeTransaction { get; set; }

        /// <summary>
        /// Control total after this transaction.  Must always be >= ControlTotalBeforeTransaction.
        /// </summary>
        public decimal? ControlTotalAfterTransaction { get; set; }

        /// <summary>
        /// List of LineItem descendants.
        /// </summary>
        public List<LineItem> LineItems { get; set; } = new List<LineItem>();

        /// <inheritdoc />
        public override Element Clone()
        {
            var artsTransactionElement = (ArtsTransactionElement)MemberwiseClone();
            artsTransactionElement.LineItems = new List<LineItem>();
            foreach (var lineItem in LineItems)
            {
                artsTransactionElement.LineItems.Add(lineItem.Clone());
            }

            return artsTransactionElement;
        }
    }

    /// <summary>
    /// View for a <see cref="ArtsTransactionElement"/> that contains MF4100-specific line items.  (Version 1)
    /// </summary>
    public class ArtsElementMf4100ViewV01
    {
        /// <summary>
        /// Guid for this transaction element.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// The order in which this Element should be processed.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Transaction number.
        /// </summary>
        public int TransactionNumber { get; set; }

        /// <summary>
        /// True if this transaction is a cancelled transaction.
        /// </summary>
        public bool CancelledFlag { get; set; }

        /// <summary>
        /// Specifies if the transaction was accepted or denied while online or offline.
        /// </summary>
        public ValidationType ValidationType { get; set; }

        /// <summary>
        /// Specifies if the transaction is a sale or return.
        /// </summary>
        public RetailTranType TranType { get; set; }

        /// <summary>
        /// Control total before this transaction.
        /// </summary>
        public decimal? ControlTotalBeforeTransaction { get; set; }

        /// <summary>
        /// Control total after this transaction.  Must always be >= ControlTotalBeforeTransaction.
        /// </summary>
        public decimal? ControlTotalAfterTransaction { get; set; }

        /// <summary>
        /// List of line item products in the transaction.
        /// </summary>
        public List<LineItemProductViewV01> LineItemProducts { get; set; } = new List<LineItemProductViewV01>();

        /// <summary>
        /// List of line item products in the transaction.
        /// </summary>
        public List<LineItemProductPromotionViewV01> LineItemProductPromos { get; set; } = new List<LineItemProductPromotionViewV01>();

        /// <summary>
        /// List of line item discounts in the transaction.
        /// </summary>
        public List<LineItemDiscountViewV01> LineItemDiscounts { get; set; } = new List<LineItemDiscountViewV01>();

        /// <summary>
        /// List of line item surcharges in the transaction.
        /// </summary>
        public List<LineItemSurchargeViewV01> LineItemSurcharges { get; set; } = new List<LineItemSurchargeViewV01>();

        /// <summary>
        /// List of line item product taxes in the transaction.
        /// </summary>
        public List<LineItemProductTaxViewV01> LineItemProductTaxes { get; set; } = new List<LineItemProductTaxViewV01>();

        /// <summary>
        /// List of line item product price modifiers in the transaction.
        /// </summary>
        public List<LineItemProductPriceModifierViewV01> LineItemProductPriceModifiers { get; set; } = new List<LineItemProductPriceModifierViewV01>();

        /// <summary>
        /// List of line item product tax exemptions in the transaction.
        /// </summary>
        public List<LineItemProductTaxExemptViewV01> LineItemProductTaxExemptions { get; set; } = new List<LineItemProductTaxExemptViewV01>();

        /// <summary>
        /// List of line item product tax overrides in the transaction.
        /// </summary>
        public List<LineItemProductTaxOverrideViewV01> LineItemProductTaxOverrides { get; set; } = new List<LineItemProductTaxOverrideViewV01>();

        /// <summary>
        /// List of line item tender cash in the transaction.
        /// </summary>
        public List<LineItemTenderCashViewV02> LineItemTenderCashItems { get; set; } = new List<LineItemTenderCashViewV02>();

        /// <summary>
        /// List of line item tender check in the transaction.
        /// </summary>
        public List<LineItemTenderCheckViewV02> LineItemTenderCheckItems { get; set; } = new List<LineItemTenderCheckViewV02>();

        /// <summary>
        /// List of line item tender DtG in the transaction.
        /// </summary>
        public List<LineItemTenderDtgViewV01> LineItemTenderDtgItems { get; set; } = new List<LineItemTenderDtgViewV01>();

        /// <summary>
        /// List of line item tender EMV in the transaction.
        /// </summary>
        public List<LineItemTenderEmvViewV01> LineItemTenderEmvItems { get; set; } = new List<LineItemTenderEmvViewV01>();

        /// <summary>
        /// List of line item tender stored value in the transaction.
        /// </summary>
        public List<LineItemTenderStoredValueViewV02> LineItemTenderStoredValueItems { get; set; } = new List<LineItemTenderStoredValueViewV02>();

        /// <summary>
        /// List of line item tender cash equivalence in the transaction.
        /// </summary>
        public List<LineItemTenderCashEquivalenceViewV02> LineItemTenderCashEquivalenceItems { get; set; } = new List<LineItemTenderCashEquivalenceViewV02>();

        /// <summary>
        /// List of line item deposits in the transaction.
        /// </summary>
        public List<LineItemDepositViewV02> LineItemDeposits { get; set; } = new List<LineItemDepositViewV02>();

        /// <summary>
        /// List of line item enrichments in the transaction.
        /// </summary>
        public List<LineItemEnrichmentViewV01> LineItemEnrichments { get; set; } = new List<LineItemEnrichmentViewV01>();
    }

    /// <summary>
    /// View for a <see cref="ArtsTransactionElement"/>.  (Version 1)
    /// </summary>
    public class ArtsElementViewV01
    {
        /// <summary>
        /// Guid for this transaction element.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// The order in which this Element should be processed.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Transaction number.
        /// </summary>
        public int TransactionNumber { get; set; }

        /// <summary>
        /// True if this transaction is a cancelled transaction.
        /// </summary>
        public bool CancelledFlag { get; set; }

        /// <summary>
        /// Specifies if the transaction was accepted or denied while online or offline.
        /// </summary>
        public ValidationType ValidationType { get; set; }

        /// <summary>
        /// Specifies if the transaction is a sale or return.
        /// </summary>
        public RetailTranType TranType { get; set; }

        /// <summary>
        /// Control total before this transaction.
        /// </summary>
        public decimal? ControlTotalBeforeTransaction { get; set; }

        /// <summary>
        /// Control total after this transaction.  Must always be >= ControlTotalBeforeTransaction.
        /// </summary>
        public decimal? ControlTotalAfterTransaction { get; set; }

        /// <summary>
        /// List of line item products in the transaction.
        /// </summary>
        public List<LineItemProductViewV01> LineItemProducts { get; set; } = new List<LineItemProductViewV01>();

        /// <summary>
        /// List of line item products in the transaction.
        /// </summary>
        public List<LineItemProductPromotionViewV01> LineItemProductPromos { get; set; } = new List<LineItemProductPromotionViewV01>();

        /// <summary>
        /// List of line item product taxes in the transaction.
        /// </summary>
        public List<LineItemProductTaxViewV01> LineItemProductTaxes { get; set; } = new List<LineItemProductTaxViewV01>();

        /// <summary>
        /// List of line item product price modifiers in the transaction.
        /// </summary>
        public List<LineItemProductPriceModifierViewV01> LineItemProductPriceModifiers { get; set; } = new List<LineItemProductPriceModifierViewV01>();

        /// <summary>
        /// List of line item product tax exemptions in the transaction.
        /// </summary>
        public List<LineItemProductTaxExemptViewV01> LineItemProductTaxExemptions { get; set; } = new List<LineItemProductTaxExemptViewV01>();

        /// <summary>
        /// List of line item product tax overrides in the transaction.
        /// </summary>
        public List<LineItemProductTaxOverrideViewV01> LineItemProductTaxOverrides { get; set; } = new List<LineItemProductTaxOverrideViewV01>();

        /// <summary>
        /// List of line item tenders in the transaction.
        /// </summary>
        public List<LineItemTenderViewV01> LineItemTenders { get; set; } = new List<LineItemTenderViewV01>();

        /// <summary>
        /// List of line item deposits in the transaction.
        /// </summary>
        public List<LineItemDepositViewV01> LineItemDeposits { get; set; } = new List<LineItemDepositViewV01>();

        /// <summary>
        /// List of line item enrichments in the transaction.
        /// </summary>
        public List<LineItemEnrichmentViewV01> LineItemEnrichments { get; set; } = new List<LineItemEnrichmentViewV01>();
    }

    /// <summary>
    /// View for a <see cref="ArtsTransactionElement"/>.  (Version 2)
    /// </summary>
    public class ArtsElementViewV02
    {
        /// <summary>
        /// Guid for this transaction element.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// The order in which this Element should be processed.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Transaction number.
        /// </summary>
        public int TransactionNumber { get; set; }

        /// <summary>
        /// True if this transaction is a cancelled transaction.
        /// </summary>
        public bool CancelledFlag { get; set; }

        /// <summary>
        /// Specifies if the transaction was accepted or denied while online or offline.
        /// </summary>
        public ValidationType ValidationType { get; set; }

        /// <summary>
        /// Specifies if the transaction is a sale or return.
        /// </summary>
        public RetailTranType TranType { get; set; }

        /// <summary>
        /// Control total before this transaction.
        /// </summary>
        public decimal? ControlTotalBeforeTransaction { get; set; }

        /// <summary>
        /// Control total after this transaction.  Must always be >= ControlTotalBeforeTransaction.
        /// </summary>
        public decimal? ControlTotalAfterTransaction { get; set; }

        /// <summary>
        /// List of line item products in the transaction.
        /// </summary>
        public List<LineItemProductViewV01> LineItemProducts { get; set; } = new List<LineItemProductViewV01>();

        /// <summary>
        /// List of line item products in the transaction.
        /// </summary>
        public List<LineItemProductPromotionViewV01> LineItemProductPromos { get; set; } = new List<LineItemProductPromotionViewV01>();

        /// <summary>
        /// List of line item discounts in the transaction.
        /// </summary>
        public List<LineItemDiscountViewV01> LineItemDiscounts { get; set; } = new List<LineItemDiscountViewV01>();

        /// <summary>
        /// List of line item surcharges in the transaction.
        /// </summary>
        public List<LineItemSurchargeViewV01> LineItemSurcharges { get; set; } = new List<LineItemSurchargeViewV01>();

        /// <summary>
        /// List of line item product taxes in the transaction.
        /// </summary>
        public List<LineItemProductTaxViewV01> LineItemProductTaxes { get; set; } = new List<LineItemProductTaxViewV01>();

        /// <summary>
        /// List of line item product price modifiers in the transaction.
        /// </summary>
        public List<LineItemProductPriceModifierViewV01> LineItemProductPriceModifiers { get; set; } = new List<LineItemProductPriceModifierViewV01>();

        /// <summary>
        /// List of line item product tax exemptions in the transaction.
        /// </summary>
        public List<LineItemProductTaxExemptViewV01> LineItemProductTaxExemptions { get; set; } = new List<LineItemProductTaxExemptViewV01>();

        /// <summary>
        /// List of line item product tax overrides in the transaction.
        /// </summary>
        public List<LineItemProductTaxOverrideViewV01> LineItemProductTaxOverrides { get; set; } = new List<LineItemProductTaxOverrideViewV01>();

        /// <summary>
        /// List of line item tender cash in the transaction.
        /// </summary>
        public List<LineItemTenderCashViewV02> LineItemTenderCashItems { get; set; } = new List<LineItemTenderCashViewV02>();

        /// <summary>
        /// List of line item tender check in the transaction.
        /// </summary>
        public List<LineItemTenderCheckViewV02> LineItemTenderCheckItems { get; set; } = new List<LineItemTenderCheckViewV02>();

        /// <summary>
        /// List of line item tender EMV Hit in the transaction.
        /// </summary>
        public List<LineItemTenderEmvHitViewV02> LineItemTenderEmvHitItems { get; set; } = new List<LineItemTenderEmvHitViewV02>();

        /// <summary>
        /// List of line item tender stored value in the transaction.
        /// </summary>
        public List<LineItemTenderStoredValueViewV02> LineItemTenderStoredValueItems { get; set; } = new List<LineItemTenderStoredValueViewV02>();

        /// <summary>
        /// List of line item tender cash equivalence in the transaction.
        /// </summary>
        public List<LineItemTenderCashEquivalenceViewV02> LineItemTenderCashEquivalenceItems { get; set; } = new List<LineItemTenderCashEquivalenceViewV02>();

        /// <summary>
        /// List of tender discounts in the transaction.
        /// </summary>
        public List<LineItemTenderDiscountViewV01> LineItemTenderDiscounts { get; set; } = new List<LineItemTenderDiscountViewV01>();

        /// <summary>
        /// List of tender surcharges in the transaction.
        /// </summary>
        public List<LineItemTenderSurchargeViewV01> LineItemTenderSurcharges { get; set; } = new List<LineItemTenderSurchargeViewV01>();

        /// <summary>
        /// List of line item deposits in the transaction.
        /// </summary>
        public List<LineItemDepositViewV02> LineItemDeposits { get; set; } = new List<LineItemDepositViewV02>();

        /// <summary>
        /// List of line item enrichments in the transaction.
        /// </summary>
        public List<LineItemEnrichmentViewV01> LineItemEnrichments { get; set; } = new List<LineItemEnrichmentViewV01>();
    }

    /// <summary>
    /// Extension methods for <see cref="ArtsTransactionElement"/> conversion.
    /// </summary>
    public static class ArtsElementConverter
    {
        #region Mf4100 Version 1
        /// <summary>
        /// Returns a <see cref="ArtsElementMf4100ViewV01"/> object based on this <see cref="ArtsTransactionElement"/>.
        /// </summary>
        /// <param name="artsElement"></param>
        /// <returns></returns>
        public static ArtsElementMf4100ViewV01 ToArtsElementMf4100ViewV01(this ArtsTransactionElement artsElement)
        {
            var lineItemProducts = artsElement.LineItems.OfType<LineItemProduct>().ToList();
            var lineItemProductViewV01List = lineItemProducts.Select(lineItemProduct => lineItemProduct.ToLineItemProductViewV01()).ToList();

            var lineItemProductPromotions = artsElement.LineItems.OfType<LineItemProductPromotion>().ToList();
            var lineItemProductPromotionViewV01List = lineItemProductPromotions.Select(lineItemProductPromotion => lineItemProductPromotion.ToLineItemProductPromotionViewV01()).ToList();


            var lineItemDiscounts = artsElement.LineItems.OfType<LineItemDiscount>().ToList();
            var lineItemDiscountViewV01List = lineItemDiscounts.Select(lineItemDiscount => lineItemDiscount.ToLineItemDiscountViewV01()).ToList();

            var lineItemSurcharges = artsElement.LineItems.OfType<LineItemSurcharge>().ToList();
            var lineItemSurchargeViewV01List = lineItemSurcharges.Select(lineItemSurcharge => lineItemSurcharge.ToLineItemSurchargeViewV01()).ToList();

            var lineItemProductTaxes = artsElement.LineItems.OfType<LineItemProductTax>().ToList();
            var lineItemProductTaxViewV01List = lineItemProductTaxes.Select(lineItemProductTax => lineItemProductTax.ToLineItemProductTaxViewV01()).ToList();

            var lineItemProductPriceModifiers = artsElement.LineItems.OfType<LineItemProductPriceModifier>().ToList();
            var lineItemProductPriceModifierViewV01List = lineItemProductPriceModifiers.Select(lineItemProductPriceModifier => lineItemProductPriceModifier.ToLineItemProductPriceModifierViewV01()).ToList();

            var lineItemProductTaxExemptions = artsElement.LineItems.OfType<LineItemProductTaxExempt>().ToList();
            var lineItemProductTaxExemptViewV01List = lineItemProductTaxExemptions.Select(lineItemProductTaxExempt => lineItemProductTaxExempt.ToLineItemProductTaxExemptViewV01()).ToList();

            var lineItemProductTaxOverrides = artsElement.LineItems.OfType<LineItemProductTaxOverride>().ToList();
            var lineItemProductTaxOverrideViewV01List = lineItemProductTaxOverrides.Select(lineItemProductTaxOverride => lineItemProductTaxOverride.ToLineItemProductTaxOverrideViewV01()).ToList();


            var lineItemTenderCashList = artsElement.LineItems.OfType<LineItemTenderCash>().ToList();
            var lineItemTenderCashViewV02List = lineItemTenderCashList.Select(lineItemTenderCash => lineItemTenderCash.ToLineItemTenderCashViewV02()).ToList();

            var lineItemTenderCheckList = artsElement.LineItems.OfType<LineItemTenderCheck>().ToList();
            var lineItemTenderCheckViewV02List = lineItemTenderCheckList.Select(lineItemTenderCheck => lineItemTenderCheck.ToLineItemTenderCheckViewV02()).ToList();

            var lineItemTenderDtgList = artsElement.LineItems.OfType<LineItemTenderDtg>().ToList();
            var lineItemTenderDtgViewV01List = lineItemTenderDtgList.Select(lineItemTenderDtg => lineItemTenderDtg.ToLineItemTenderDtgViewV01()).ToList();

            var lineItemTenderEmvList = artsElement.LineItems.OfType<LineItemTenderEmv>().ToList();
            var lineItemTenderEmvViewV01List = lineItemTenderEmvList.Select(lineItemTenderEmv => lineItemTenderEmv.ToLineItemTenderEmvViewV01()).ToList();

            var lineItemTenderStoredValueList = artsElement.LineItems.OfType<LineItemTenderWithCustomer>().ToList();
            var lineItemTenderStoredValueViewV02List = lineItemTenderStoredValueList.Select(lineItemTenderStoredValue => lineItemTenderStoredValue.ToLineItemTenderStoredValueViewV02()).ToList();

            var lineItemTenderCashEquivalenceList = artsElement.LineItems.OfType<LineItemTenderCashEquivalence>().ToList();
            var lineItemTenderCashEquivalenceViewV02List = lineItemTenderCashEquivalenceList.Select(lineItemTenderCashEquivalence => lineItemTenderCashEquivalence.ToLineItemTenderCashEquivalenceViewV02()).ToList();


            var lineItemDeposits = artsElement.LineItems.OfType<LineItemStoredValue>().ToList();
            var lineItemDepositViewV02List = lineItemDeposits.Select(lineItemDeposit => lineItemDeposit.ToLineItemDepositViewV02()).ToList();

            var lineItemEnrichments = artsElement.LineItems.OfType<LineItemStoredValueEnrichment>().ToList();
            var lineItemEnrichmentViewV01List = lineItemEnrichments.Select(lineItemEnrichment => lineItemEnrichment.ToLineItemEnrichmentViewV01()).ToList();

            return new ArtsElementMf4100ViewV01
            {
                Id = artsElement.Id,
                Sequence = artsElement.Sequence,
                TransactionNumber = artsElement.TransactionNumber,
                CancelledFlag = artsElement.CancelledFlag,
                ValidationType = artsElement.ValidationType,
                TranType = artsElement.TranType,
                ControlTotalBeforeTransaction = artsElement.ControlTotalBeforeTransaction,
                ControlTotalAfterTransaction = artsElement.ControlTotalAfterTransaction,
                LineItemProducts = lineItemProductViewV01List,
                LineItemProductPromos = lineItemProductPromotionViewV01List,

                LineItemDiscounts = lineItemDiscountViewV01List,
                LineItemSurcharges = lineItemSurchargeViewV01List,
                LineItemProductTaxes = lineItemProductTaxViewV01List,
                LineItemProductPriceModifiers = lineItemProductPriceModifierViewV01List,
                LineItemProductTaxExemptions = lineItemProductTaxExemptViewV01List,
                LineItemProductTaxOverrides = lineItemProductTaxOverrideViewV01List,

                LineItemTenderCashItems = lineItemTenderCashViewV02List,
                LineItemTenderCheckItems = lineItemTenderCheckViewV02List,
                LineItemTenderDtgItems = lineItemTenderDtgViewV01List,
                LineItemTenderEmvItems = lineItemTenderEmvViewV01List,
                LineItemTenderStoredValueItems = lineItemTenderStoredValueViewV02List,
                LineItemTenderCashEquivalenceItems = lineItemTenderCashEquivalenceViewV02List,

                LineItemDeposits = lineItemDepositViewV02List,
                LineItemEnrichments = lineItemEnrichmentViewV01List
            };
        }

        /// <summary>
        /// Returns a <see cref="ArtsTransactionElement"/> object based on this <see cref="ArtsElementMf4100ViewV01"/>.
        /// </summary>
        /// <param name="artsElementMf4100ViewV01"></param>
        /// <returns></returns>
        public static ArtsTransactionElement ToArtsElement(this ArtsElementMf4100ViewV01 artsElementMf4100ViewV01)
        {
            if (artsElementMf4100ViewV01 == null) return null;

            var artsElement = new ArtsTransactionElement
            {
                Id = artsElementMf4100ViewV01.Id,
                Sequence = artsElementMf4100ViewV01.Sequence,
                TransactionNumber = artsElementMf4100ViewV01.TransactionNumber,
                CancelledFlag = artsElementMf4100ViewV01.CancelledFlag,
                ValidationType = artsElementMf4100ViewV01.ValidationType,
                TranType = artsElementMf4100ViewV01.TranType,
                ControlTotalBeforeTransaction = artsElementMf4100ViewV01.ControlTotalBeforeTransaction,
                ControlTotalAfterTransaction = artsElementMf4100ViewV01.ControlTotalAfterTransaction,
                LineItems = new List<LineItem>()
            };

            foreach (var lineItemProductViewV01 in artsElementMf4100ViewV01.LineItemProducts)
            {
                artsElement.LineItems.Add(lineItemProductViewV01.ToLineItemProduct());
            }

            foreach (var lineItemProductPromotionViewV01 in artsElementMf4100ViewV01.LineItemProductPromos)
            {
                artsElement.LineItems.Add(lineItemProductPromotionViewV01.ToLineItemProductPromotion());
            }


            foreach (var lineItemDiscountViewV01 in artsElementMf4100ViewV01.LineItemDiscounts)
            {
                artsElement.LineItems.Add(lineItemDiscountViewV01.ToLineItemDiscount());
            }

            foreach (var lineItemSurchargeViewV01 in artsElementMf4100ViewV01.LineItemSurcharges)
            {
                artsElement.LineItems.Add(lineItemSurchargeViewV01.ToLineItemSurcharge());
            }

            foreach (var lineItemProductTaxViewV01 in artsElementMf4100ViewV01.LineItemProductTaxes)
            {
                artsElement.LineItems.Add(lineItemProductTaxViewV01.ToLineItemProductTax());
            }

            foreach (var lineItemProductPriceModifierViewV01 in artsElementMf4100ViewV01.LineItemProductPriceModifiers)
            {
                artsElement.LineItems.Add(lineItemProductPriceModifierViewV01.ToLineItemProductPriceModifier());
            }

            foreach (var lineItemProductTaxExemptViewV01 in artsElementMf4100ViewV01.LineItemProductTaxExemptions)
            {
                artsElement.LineItems.Add(lineItemProductTaxExemptViewV01.ToLineItemProductTaxExempt());
            }

            foreach (var lineItemProductTaxOverrideViewV01 in artsElementMf4100ViewV01.LineItemProductTaxOverrides)
            {
                artsElement.LineItems.Add(lineItemProductTaxOverrideViewV01.ToLineItemProductTaxOverride());
            }


            foreach (var lineItemTenderCashViewV01 in artsElementMf4100ViewV01.LineItemTenderCashItems)
            {
                artsElement.LineItems.Add(lineItemTenderCashViewV01.ToLineItemTenderCash());
            }

            foreach (var lineItemTenderCheckViewV01 in artsElementMf4100ViewV01.LineItemTenderCheckItems)
            {
                artsElement.LineItems.Add(lineItemTenderCheckViewV01.ToLineItemTenderCheck());
            }

            foreach (var lineItemTenderDtgViewV01 in artsElementMf4100ViewV01.LineItemTenderDtgItems)
            {
                artsElement.LineItems.Add(lineItemTenderDtgViewV01.ToLineItemTenderDtg());
            }

            foreach (var lineItemTenderEmvViewV01 in artsElementMf4100ViewV01.LineItemTenderEmvItems)
            {
                artsElement.LineItems.Add(lineItemTenderEmvViewV01.ToLineItemTenderEmv());
            }

            foreach (var lineItemTenderStoredValueViewV02 in artsElementMf4100ViewV01.LineItemTenderStoredValueItems)
            {
                artsElement.LineItems.Add(lineItemTenderStoredValueViewV02.ToLineItemTenderStoredValue());
            }

            foreach (var lineItemTenderCashEquivalenceViewV02 in artsElementMf4100ViewV01.LineItemTenderCashEquivalenceItems)
            {
                artsElement.LineItems.Add(lineItemTenderCashEquivalenceViewV02.ToLineItemTenderCashEquivalence());
            }


            foreach (var lineItemDepositViewV02 in artsElementMf4100ViewV01.LineItemDeposits)
            {
                artsElement.LineItems.Add(lineItemDepositViewV02.ToLineItemDeposit());
            }

            foreach (var lineItemEnrichmentViewV01 in artsElementMf4100ViewV01.LineItemEnrichments)
            {
                artsElement.LineItems.Add(lineItemEnrichmentViewV01.ToLineItemEnrichment());
            }

            //Sequence is a new property...if it is > 0 in all line items, we can use it to restore original order.
            bool hasSequenceInformation = artsElement.LineItems.All(lineItem => lineItem.Sequence > 0);
            if (hasSequenceInformation)
            {
                artsElement.LineItems.Sort((x, y) => x.Sequence.CompareTo(y.Sequence));
            }

            return artsElement;
        }
        #endregion

        #region Version 1
        /// <summary>
        /// Returns a <see cref="ArtsElementViewV01"/> object based on this <see cref="ArtsTransactionElement"/>.
        /// </summary>
        /// <param name="artsElement"></param>
        /// <returns></returns>
        public static ArtsElementViewV01 ToArtsElementViewV01(this ArtsTransactionElement artsElement)
        {
            var lineItemProducts = artsElement.LineItems.OfType<LineItemProduct>().ToList();
            var lineItemProductViewV01List = lineItemProducts.Select(lineItemProduct => lineItemProduct.ToLineItemProductViewV01()).ToList();

            var lineItemProductPromotions = artsElement.LineItems.OfType<LineItemProductPromotion>().ToList();
            var lineItemProductPromotionViewV01List = lineItemProductPromotions.Select(lineItemProductPromotion => lineItemProductPromotion.ToLineItemProductPromotionViewV01()).ToList();

            var lineItemProductTaxes = artsElement.LineItems.OfType<LineItemProductTax>().ToList();
            var lineItemProductTaxViewV01List = lineItemProductTaxes.Select(lineItemProductTax => lineItemProductTax.ToLineItemProductTaxViewV01()).ToList();

            var lineItemProductPriceModifiers = artsElement.LineItems.OfType<LineItemProductPriceModifier>().ToList();
            var lineItemProductPriceModifierViewV01List = lineItemProductPriceModifiers.Select(lineItemProductPriceModifier => lineItemProductPriceModifier.ToLineItemProductPriceModifierViewV01()).ToList();

            var lineItemProductTaxExemptions = artsElement.LineItems.OfType<LineItemProductTaxExempt>().ToList();
            var lineItemProductTaxExemptViewV01List = lineItemProductTaxExemptions.Select(lineItemProductTaxExempt => lineItemProductTaxExempt.ToLineItemProductTaxExemptViewV01()).ToList();

            var lineItemProductTaxOverrides = artsElement.LineItems.OfType<LineItemProductTaxOverride>().ToList();
            var lineItemProductTaxOverrideViewV01List = lineItemProductTaxOverrides.Select(lineItemProductTaxOverride => lineItemProductTaxOverride.ToLineItemProductTaxOverrideViewV01()).ToList();

            var lineItemTenders = artsElement.LineItems.OfType<LineItemTender>().ToList();
            var lineItemTenderViewV01List = lineItemTenders.Select(lineItemTender => lineItemTender.ToLineItemTenderViewV01()).ToList();

            var lineItemDeposits = artsElement.LineItems.OfType<LineItemStoredValue>().ToList();
            var lineItemDepositViewV01List = lineItemDeposits.Select(lineItemDeposit => lineItemDeposit.ToLineItemDepositViewV01()).ToList();

            var lineItemEnrichments = artsElement.LineItems.OfType<LineItemStoredValueEnrichment>().ToList();
            var lineItemEnrichmentViewV01List = lineItemEnrichments.Select(lineItemEnrichment => lineItemEnrichment.ToLineItemEnrichmentViewV01()).ToList();

            return new ArtsElementViewV01
            {
                Id = artsElement.Id,
                Sequence = artsElement.Sequence,
                TransactionNumber = artsElement.TransactionNumber,
                CancelledFlag = artsElement.CancelledFlag,
                ValidationType = artsElement.ValidationType,
                TranType = artsElement.TranType,
                ControlTotalBeforeTransaction = artsElement.ControlTotalBeforeTransaction,
                ControlTotalAfterTransaction = artsElement.ControlTotalAfterTransaction,
                LineItemProducts = lineItemProductViewV01List,
                LineItemProductPromos = lineItemProductPromotionViewV01List,
                LineItemProductTaxes = lineItemProductTaxViewV01List,
                LineItemProductPriceModifiers = lineItemProductPriceModifierViewV01List,
                LineItemProductTaxExemptions = lineItemProductTaxExemptViewV01List,
                LineItemProductTaxOverrides = lineItemProductTaxOverrideViewV01List,
                LineItemTenders = lineItemTenderViewV01List,
                LineItemDeposits = lineItemDepositViewV01List,
                LineItemEnrichments = lineItemEnrichmentViewV01List
            };
        }

        /// <summary>
        /// Returns a <see cref="ArtsTransactionElement"/> object based on this <see cref="ArtsElementViewV01"/>.
        /// </summary>
        /// <param name="artsElementViewV01"></param>
        /// <returns></returns>
        public static ArtsTransactionElement ToArtsElement(this ArtsElementViewV01 artsElementViewV01)
        {
            if (artsElementViewV01 == null) return null;

            var artsElement = new ArtsTransactionElement
            {
                Id = artsElementViewV01.Id,
                Sequence = artsElementViewV01.Sequence,
                TransactionNumber = artsElementViewV01.TransactionNumber,
                CancelledFlag = artsElementViewV01.CancelledFlag,
                ValidationType = artsElementViewV01.ValidationType,
                TranType = artsElementViewV01.TranType,
                ControlTotalBeforeTransaction = artsElementViewV01.ControlTotalBeforeTransaction,
                ControlTotalAfterTransaction = artsElementViewV01.ControlTotalAfterTransaction,
                LineItems = new List<LineItem>()
            };

            foreach (var lineItemProductViewV01 in artsElementViewV01.LineItemProducts)
            {
                artsElement.LineItems.Add(lineItemProductViewV01.ToLineItemProduct());
            }

            foreach (var lineItemProductPromotionViewV01 in artsElementViewV01.LineItemProductPromos)
            {
                artsElement.LineItems.Add(lineItemProductPromotionViewV01.ToLineItemProductPromotion());
            }

            foreach (var lineItemProductTaxViewV01 in artsElementViewV01.LineItemProductTaxes)
            {
                artsElement.LineItems.Add(lineItemProductTaxViewV01.ToLineItemProductTax());
            }

            foreach (var lineItemProductPriceModifierViewV01 in artsElementViewV01.LineItemProductPriceModifiers)
            {
                artsElement.LineItems.Add(lineItemProductPriceModifierViewV01.ToLineItemProductPriceModifier());
            }

            foreach (var lineItemProductTaxExemptViewV01 in artsElementViewV01.LineItemProductTaxExemptions)
            {
                artsElement.LineItems.Add(lineItemProductTaxExemptViewV01.ToLineItemProductTaxExempt());
            }

            foreach (var lineItemProductTaxOverrideViewV01 in artsElementViewV01.LineItemProductTaxOverrides)
            {
                artsElement.LineItems.Add(lineItemProductTaxOverrideViewV01.ToLineItemProductTaxOverride());
            }

            foreach (var lineItemTenderViewV01 in artsElementViewV01.LineItemTenders)
            {
                artsElement.LineItems.Add(lineItemTenderViewV01.ToLineItemTender());
            }

            foreach (var lineItemDepositViewV01 in artsElementViewV01.LineItemDeposits)
            {
                artsElement.LineItems.Add(lineItemDepositViewV01.ToLineItemDeposit());
            }

            foreach (var lineItemEnrichmentViewV01 in artsElementViewV01.LineItemEnrichments)
            {
                artsElement.LineItems.Add(lineItemEnrichmentViewV01.ToLineItemEnrichment());
            }

            //Sequence is a new property...if it is > 0 in all line items, we can use it to restore original order.
            bool hasSequenceInformation = artsElement.LineItems.All(lineItem => lineItem.Sequence > 0);
            if (hasSequenceInformation)
            {
                artsElement.LineItems.Sort((x, y) => x.Sequence.CompareTo(y.Sequence));
            }

            return artsElement;
        }
        #endregion

        #region Version 2
        /// <summary>
        /// Returns a <see cref="ArtsElementViewV02"/> object based on this <see cref="ArtsTransactionElement"/>.
        /// </summary>
        /// <param name="artsElement"></param>
        /// <returns></returns>
        public static ArtsElementViewV02 ToArtsElementViewV02(this ArtsTransactionElement artsElement)
        {
            var lineItemProducts = artsElement.LineItems.OfType<LineItemProduct>().ToList();
            var lineItemProductViewV01List = lineItemProducts.Select(lineItemProduct => lineItemProduct.ToLineItemProductViewV01()).ToList();

            var lineItemProductPromotions = artsElement.LineItems.OfType<LineItemProductPromotion>().ToList();
            var lineItemProductPromotionViewV01List = lineItemProductPromotions.Select(lineItemProductPromotion => lineItemProductPromotion.ToLineItemProductPromotionViewV01()).ToList();


            var lineItemDiscounts = artsElement.LineItems.OfType<LineItemDiscount>().ToList();
            var lineItemDiscountViewV01List = lineItemDiscounts.Select(lineItemDiscount => lineItemDiscount.ToLineItemDiscountViewV01()).ToList();

            var lineItemSurcharges = artsElement.LineItems.OfType<LineItemSurcharge>().ToList();
            var lineItemSurchargeViewV01List = lineItemSurcharges.Select(lineItemSurcharge => lineItemSurcharge.ToLineItemSurchargeViewV01()).ToList();

            var lineItemProductTaxes = artsElement.LineItems.OfType<LineItemProductTax>().ToList();
            var lineItemProductTaxViewV01List = lineItemProductTaxes.Select(lineItemProductTax => lineItemProductTax.ToLineItemProductTaxViewV01()).ToList();

            var lineItemProductPriceModifiers = artsElement.LineItems.OfType<LineItemProductPriceModifier>().ToList();
            var lineItemProductPriceModifierViewV01List = lineItemProductPriceModifiers.Select(lineItemProductPriceModifier => lineItemProductPriceModifier.ToLineItemProductPriceModifierViewV01()).ToList();

            var lineItemProductTaxExemptions = artsElement.LineItems.OfType<LineItemProductTaxExempt>().ToList();
            var lineItemProductTaxExemptViewV01List = lineItemProductTaxExemptions.Select(lineItemProductTaxExempt => lineItemProductTaxExempt.ToLineItemProductTaxExemptViewV01()).ToList();

            var lineItemProductTaxOverrides = artsElement.LineItems.OfType<LineItemProductTaxOverride>().ToList();
            var lineItemProductTaxOverrideViewV01List = lineItemProductTaxOverrides.Select(lineItemProductTaxOverride => lineItemProductTaxOverride.ToLineItemProductTaxOverrideViewV01()).ToList();


            var lineItemTenderCashList = artsElement.LineItems.OfType<LineItemTenderCash>().ToList();
            var lineItemTenderCashViewV02List = lineItemTenderCashList.Select(lineItemTenderCash => lineItemTenderCash.ToLineItemTenderCashViewV02()).ToList();

            var lineItemTenderCheckList = artsElement.LineItems.OfType<LineItemTenderCheck>().ToList();
            var lineItemTenderCheckViewV02List = lineItemTenderCheckList.Select(lineItemTenderCheck => lineItemTenderCheck.ToLineItemTenderCheckViewV02()).ToList();

            var lineItemTenderEmvHitList = artsElement.LineItems.OfType<LineItemTenderEmvHit>().ToList();
            var lineItemTenderEmvHitViewV02List = lineItemTenderEmvHitList.Select(lineItemTenderEmvHit => lineItemTenderEmvHit.ToLineItemTenderEmvHitViewV02()).ToList();

            var lineItemTenderStoredValueList = artsElement.LineItems.OfType<LineItemTenderWithCustomer>().ToList();
            var lineItemTenderStoredValueViewV02List = lineItemTenderStoredValueList.Select(lineItemTenderStoredValue => lineItemTenderStoredValue.ToLineItemTenderStoredValueViewV02()).ToList();

            var lineItemTenderCashEquivalenceList = artsElement.LineItems.OfType<LineItemTenderCashEquivalence>().ToList();
            var lineItemTenderCashEquivalenceViewV02List = lineItemTenderCashEquivalenceList.Select(lineItemTenderCashEquivalence => lineItemTenderCashEquivalence.ToLineItemTenderCashEquivalenceViewV02()).ToList();


            var lineItemTenderDiscountList = artsElement.LineItems.OfType<LineItemTenderDiscount>().ToList();
            var lineItemTenderDiscountViewV01List = lineItemTenderDiscountList.Select(lineItemTenderDiscount => lineItemTenderDiscount.ToLineItemTenderDiscountViewV01()).ToList();

            var lineItemTenderSurchargeList = artsElement.LineItems.OfType<LineItemTenderSurcharge>().ToList();
            var lineItemTenderSurchargeViewV01List = lineItemTenderSurchargeList.Select(lineItemTenderSurcharge => lineItemTenderSurcharge.ToLineItemTenderSurchargeViewV01()).ToList();


            var lineItemDeposits = artsElement.LineItems.OfType<LineItemStoredValue>().ToList();
            var lineItemDepositViewV02List = lineItemDeposits.Select(lineItemDeposit => lineItemDeposit.ToLineItemDepositViewV02()).ToList();

            var lineItemEnrichments = artsElement.LineItems.OfType<LineItemStoredValueEnrichment>().ToList();
            var lineItemEnrichmentViewV01List = lineItemEnrichments.Select(lineItemEnrichment => lineItemEnrichment.ToLineItemEnrichmentViewV01()).ToList();

            return new ArtsElementViewV02
            {
                Id = artsElement.Id,
                Sequence = artsElement.Sequence,
                TransactionNumber = artsElement.TransactionNumber,
                CancelledFlag = artsElement.CancelledFlag,
                ValidationType = artsElement.ValidationType,
                TranType = artsElement.TranType,
                ControlTotalBeforeTransaction = artsElement.ControlTotalBeforeTransaction,
                ControlTotalAfterTransaction = artsElement.ControlTotalAfterTransaction,
                LineItemProducts = lineItemProductViewV01List,
                LineItemProductPromos = lineItemProductPromotionViewV01List,
                
                LineItemDiscounts = lineItemDiscountViewV01List,
                LineItemSurcharges = lineItemSurchargeViewV01List,
                LineItemProductTaxes = lineItemProductTaxViewV01List,
                LineItemProductPriceModifiers = lineItemProductPriceModifierViewV01List,
                LineItemProductTaxExemptions = lineItemProductTaxExemptViewV01List,
                LineItemProductTaxOverrides = lineItemProductTaxOverrideViewV01List,

                LineItemTenderCashItems = lineItemTenderCashViewV02List,
                LineItemTenderCheckItems = lineItemTenderCheckViewV02List,
                LineItemTenderEmvHitItems = lineItemTenderEmvHitViewV02List,
                LineItemTenderStoredValueItems = lineItemTenderStoredValueViewV02List,
                LineItemTenderCashEquivalenceItems = lineItemTenderCashEquivalenceViewV02List,

                LineItemTenderDiscounts = lineItemTenderDiscountViewV01List,
                LineItemTenderSurcharges = lineItemTenderSurchargeViewV01List,

                LineItemDeposits = lineItemDepositViewV02List,
                LineItemEnrichments = lineItemEnrichmentViewV01List
            };
        }

        /// <summary>
        /// Returns a <see cref="ArtsTransactionElement"/> object based on this <see cref="ArtsElementViewV02"/>.
        /// </summary>
        /// <param name="artsElementViewV02"></param>
        /// <returns></returns>
        public static ArtsTransactionElement ToArtsElement(this ArtsElementViewV02 artsElementViewV02)
        {
            if (artsElementViewV02 == null) return null;

            var artsElement = new ArtsTransactionElement
            {
                Id = artsElementViewV02.Id,
                Sequence = artsElementViewV02.Sequence,
                TransactionNumber = artsElementViewV02.TransactionNumber,
                CancelledFlag = artsElementViewV02.CancelledFlag,
                ValidationType = artsElementViewV02.ValidationType,
                TranType = artsElementViewV02.TranType,
                ControlTotalBeforeTransaction = artsElementViewV02.ControlTotalBeforeTransaction,
                ControlTotalAfterTransaction = artsElementViewV02.ControlTotalAfterTransaction,
                LineItems = new List<LineItem>()
            };

            foreach (var lineItemProductViewV01 in artsElementViewV02.LineItemProducts)
            {
                artsElement.LineItems.Add(lineItemProductViewV01.ToLineItemProduct());
            }

            foreach (var lineItemProductPromotionViewV01 in artsElementViewV02.LineItemProductPromos)
            {
                artsElement.LineItems.Add(lineItemProductPromotionViewV01.ToLineItemProductPromotion());
            }


            foreach (var lineItemDiscountViewV01 in artsElementViewV02.LineItemDiscounts)
            {
                artsElement.LineItems.Add(lineItemDiscountViewV01.ToLineItemDiscount());
            }

            foreach (var lineItemSurchargeViewV01 in artsElementViewV02.LineItemSurcharges)
            {
                artsElement.LineItems.Add(lineItemSurchargeViewV01.ToLineItemSurcharge());
            }

            foreach (var lineItemProductTaxViewV01 in artsElementViewV02.LineItemProductTaxes)
            {
                artsElement.LineItems.Add(lineItemProductTaxViewV01.ToLineItemProductTax());
            }

            foreach (var lineItemProductPriceModifierViewV01 in artsElementViewV02.LineItemProductPriceModifiers)
            {
                artsElement.LineItems.Add(lineItemProductPriceModifierViewV01.ToLineItemProductPriceModifier());
            }

            foreach (var lineItemProductTaxExemptViewV01 in artsElementViewV02.LineItemProductTaxExemptions)
            {
                artsElement.LineItems.Add(lineItemProductTaxExemptViewV01.ToLineItemProductTaxExempt());
            }

            foreach (var lineItemProductTaxOverrideViewV01 in artsElementViewV02.LineItemProductTaxOverrides)
            {
                artsElement.LineItems.Add(lineItemProductTaxOverrideViewV01.ToLineItemProductTaxOverride());
            }


            foreach (var lineItemTenderCashViewV02 in artsElementViewV02.LineItemTenderCashItems)
            {
                artsElement.LineItems.Add(lineItemTenderCashViewV02.ToLineItemTenderCash());
            }

            foreach (var lineItemTenderCheckViewV02 in artsElementViewV02.LineItemTenderCheckItems)
            {
                artsElement.LineItems.Add(lineItemTenderCheckViewV02.ToLineItemTenderCheck());
            }

            foreach (var lineItemTenderEmvHitViewV02 in artsElementViewV02.LineItemTenderEmvHitItems)
            {
                artsElement.LineItems.Add(lineItemTenderEmvHitViewV02.ToLineItemTenderEmvHit());
            }

            foreach (var lineItemTenderStoredValueViewV02 in artsElementViewV02.LineItemTenderStoredValueItems)
            {
                artsElement.LineItems.Add(lineItemTenderStoredValueViewV02.ToLineItemTenderStoredValue());
            }

            foreach (var lineItemTenderCashEquivalenceViewV02 in artsElementViewV02.LineItemTenderCashEquivalenceItems)
            {
                artsElement.LineItems.Add(lineItemTenderCashEquivalenceViewV02.ToLineItemTenderCashEquivalence());
            }


            foreach (var lineItemTenderDiscountViewV01 in artsElementViewV02.LineItemTenderDiscounts)
            {
                artsElement.LineItems.Add(lineItemTenderDiscountViewV01.ToLineItemTenderDiscount());
            }

            foreach (var lineItemTenderSurchargeViewV01 in artsElementViewV02.LineItemTenderSurcharges)
            {
                artsElement.LineItems.Add(lineItemTenderSurchargeViewV01.ToLineItemTenderSurcharge());
            }


            foreach (var lineItemDepositViewV02 in artsElementViewV02.LineItemDeposits)
            {
                artsElement.LineItems.Add(lineItemDepositViewV02.ToLineItemDeposit());
            }

            foreach (var lineItemEnrichmentViewV01 in artsElementViewV02.LineItemEnrichments)
            {
                artsElement.LineItems.Add(lineItemEnrichmentViewV01.ToLineItemEnrichment());
            }

            //Sequence is a new property...if it is > 0 in all line items, we can use it to restore original order.
            bool hasSequenceInformation = artsElement.LineItems.All(lineItem => lineItem.Sequence > 0);
            if (hasSequenceInformation)
            {
                artsElement.LineItems.Sort((x, y) => x.Sequence.CompareTo(y.Sequence));
            }

            return artsElement;
        }
        #endregion
    }
}
