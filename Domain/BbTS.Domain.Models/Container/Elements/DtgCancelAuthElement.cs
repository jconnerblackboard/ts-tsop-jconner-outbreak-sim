﻿using System;

namespace BbTS.Domain.Models.Container.Elements
{
    /// <summary>
    /// Direct to Gateway cancel credit card auth.
    /// </summary>
    public class DtgCancelAuthElement : Element
    {
        /// <summary>
        /// The OrderId that was cancelled.
        /// </summary>
        public string CancelledOrderId { get; set; }

        /// <summary>
        /// Commit value type.
        /// </summary>
        public string CommitValueType { get; set; }

        /// <inheritdoc />
        public override Element Clone()
        {
            return (DtgCancelAuthElement)MemberwiseClone();
        }
    }

    /// <summary>
    /// View for a <see cref="DtgCancelAuthElement"/>.  (Version 1)
    /// </summary>
    public class DtgCancelAuthElementViewV01
    {
        /// <summary>
        /// Guid for this transaction element.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// The order in which this Element should be processed.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Cancelled order ID.
        /// </summary>
        public string CancelledOrderId { get; set; }

        /// <summary>
        /// Commit value type.
        /// </summary>
        public string CommitValueType { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="DtgCancelAuthElement"/> conversion.
    /// </summary>
    public static class DtgCancelAuthElementConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="DtgCancelAuthElementViewV01"/> object based on this <see cref="DtgCancelAuthElement"/>.
        /// </summary>
        /// <param name="dtgCancelAuthElement"></param>
        /// <returns></returns>
        public static DtgCancelAuthElementViewV01 ToDtgCancelAuthElementViewV01(this DtgCancelAuthElement dtgCancelAuthElement)
        {
            if (dtgCancelAuthElement == null) return null;

            return new DtgCancelAuthElementViewV01
            {
                Id = dtgCancelAuthElement.Id,
                Sequence = dtgCancelAuthElement.Sequence,
                CancelledOrderId = dtgCancelAuthElement.CancelledOrderId,
                CommitValueType = dtgCancelAuthElement.CommitValueType
            };
        }

        /// <summary>
        /// Returns a <see cref="DtgCancelAuthElement"/> object based on this <see cref="DtgCancelAuthElementViewV01"/>.
        /// </summary>
        /// <param name="dtgCancelAuthElementViewV01"></param>
        /// <returns></returns>
        public static DtgCancelAuthElement ToDtgCancelAuthElement(this DtgCancelAuthElementViewV01 dtgCancelAuthElementViewV01)
        {
            if (dtgCancelAuthElementViewV01 == null) return null;

            return new DtgCancelAuthElement
            {
                Id = dtgCancelAuthElementViewV01.Id,
                Sequence = dtgCancelAuthElementViewV01.Sequence,
                CancelledOrderId = dtgCancelAuthElementViewV01.CancelledOrderId,
                CommitValueType = dtgCancelAuthElementViewV01.CommitValueType
            };
        }
        #endregion
    }
}
