﻿using System;
using BbTS.Domain.Models.ArtsDataModel;
using BbTS.Domain.Models.Credential;
using BbTS.Domain.Models.Definitions.ArtsDataModel;
using BbTS.Domain.Models.Definitions.Credential;

namespace BbTS.Domain.Models.Container.Elements
{
    /// <summary>
    /// Board element.
    /// </summary>
    public class BoardElement : Element
    {
        /// <summary>
        /// Transaction number.
        /// </summary>
        public int TransactionNumber { get; set; }

        ///// <summary>
        ///// The BbTS customer card used.
        ///// </summary>
        //public CardCapture CustomerCard { get; set; }

        /// <summary>
        /// Meal type ID.
        /// </summary>
        public int MealTypeId { get; set; }

        /// <summary>
        /// True if this is a guest meal.
        /// </summary>
        public bool IsGuestMeal { get; set; }

        /// <summary>
        /// Number of board counts to use.
        /// </summary>
        public int CountUsed { get; set; }

        ///// <summary>
        ///// The method used to capture the customer's credentials.
        ///// "0 - swiped, 1 - manually entered"
        ///// </summary>
        //public CustomerTenderEntryMethodType EntryMethodType { get; set; }

        /// <summary>
        /// Customer credential.
        /// </summary>
        public CustomerCredential CustomerCredential { get; set; }

        /// <inheritdoc />
        public override Element Clone()
        {
            var boardElement = (BoardElement)MemberwiseClone();
            boardElement.CustomerCredential.Clone();
            return boardElement;
        }
    }

    /// <summary>
    /// View for a <see cref="BoardElement"/>.  (Version 1)
    /// </summary>
    public class BoardElementViewV01
    {
        /// <summary>
        /// Guid for this transaction element.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// The order in which this Element should be processed.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Transaction number.
        /// </summary>
        public int TransactionNumber { get; set; }

        /// <summary>
        /// The BbTS customer card used.
        /// </summary>
        public CardCaptureViewV01 CustomerCard { get; set; }

        /// <summary>
        /// Meal type ID.
        /// </summary>
        public int MealTypeId { get; set; }

        /// <summary>
        /// True if this is a guest meal.
        /// </summary>
        public bool IsGuestMeal { get; set; }

        /// <summary>
        /// Number of board counts to use.
        /// </summary>
        public int CountUsed { get; set; }

        /// <summary>
        /// The method used to capture the customer's credentials.
        /// "0 - swiped, 1 - manually entered"
        /// </summary>
        public CustomerTenderEntryMethodType EntryMethodType { get; set; }
    }

    /// <summary>
    /// View for a <see cref="BoardElement"/>.  (Version 2)
    /// </summary>
    public class BoardElementViewV02
    {
        /// <summary>
        /// Guid for this transaction element.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// The order in which this Element should be processed.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Transaction number.
        /// </summary>
        public int TransactionNumber { get; set; }

        /// <summary>
        /// Meal type ID.
        /// </summary>
        public int MealTypeId { get; set; }

        /// <summary>
        /// True if this is a guest meal.
        /// </summary>
        public bool IsGuestMeal { get; set; }

        /// <summary>
        /// Number of board counts to use.
        /// </summary>
        public int CountUsed { get; set; }

        /// <summary>
        /// Customer credential.
        /// </summary>
        public CustomerCredentialViewV02 CustomerCredential { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="BoardElement"/> conversion.
    /// </summary>
    public static class BoardElementConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="BoardElementViewV01"/> object based on this <see cref="BoardElement"/>.
        /// </summary>
        /// <param name="boardElement"></param>
        /// <returns></returns>
        public static BoardElementViewV01 ToBoardElementViewV01(this BoardElement boardElement)
        {
            if (boardElement == null) return null;

            return new BoardElementViewV01
            {
                Id = boardElement.Id,
                Sequence = boardElement.Sequence,
                TransactionNumber = boardElement.TransactionNumber,
                CustomerCard = boardElement.CustomerCredential?.CustomerCardInfo?.ToCardCaptureViewV01(),
                MealTypeId = boardElement.MealTypeId,
                IsGuestMeal = boardElement.IsGuestMeal,
                CountUsed = boardElement.CountUsed,
                EntryMethodType = boardElement.CustomerCredential?.EntryMethodType ?? CustomerTenderEntryMethodType.ManuallyEntered
            };
        }

        /// <summary>
        /// Returns a <see cref="BoardElement"/> object based on this <see cref="BoardElementViewV01"/>.
        /// </summary>
        /// <param name="boardElementViewV01"></param>
        /// <returns></returns>
        public static BoardElement ToBoardElement(this BoardElementViewV01 boardElementViewV01)
        {
            if (boardElementViewV01 == null) return null;

            CustomerCredential customerCredential = null;
            if (boardElementViewV01.CustomerCard != null)
            {
                customerCredential = new CustomerCredential
                {
                    CustomerGuid = null,  //Board element view version 1 does not have a customer guid
                    PhysicalIdType = CustomerTenderPhysicalIdType.Card,
                    EntryMethodType = boardElementViewV01.CustomerCard.IssueNumberCaptured ? CustomerTenderEntryMethodType.Swiped : CustomerTenderEntryMethodType.ManuallyEntered,
                    SecondaryAuthEntered = false,
                    CustomerCardInfo = boardElementViewV01.CustomerCard.ToCardCapture()
                };
            }

            return new BoardElement
            {
                Id = boardElementViewV01.Id,
                Sequence = boardElementViewV01.Sequence,
                TransactionNumber = boardElementViewV01.TransactionNumber,
                MealTypeId = boardElementViewV01.MealTypeId,
                IsGuestMeal = boardElementViewV01.IsGuestMeal,
                CountUsed = boardElementViewV01.CountUsed,
                CustomerCredential = customerCredential
            };
        }
        #endregion

        #region Version 2
        /// <summary>
        /// Returns a <see cref="BoardElementViewV02"/> object based on this <see cref="BoardElement"/>.
        /// </summary>
        /// <param name="boardElement"></param>
        /// <returns></returns>
        public static BoardElementViewV02 ToBoardElementViewV02(this BoardElement boardElement)
        {
            if (boardElement == null) return null;

            return new BoardElementViewV02
            {
                Id = boardElement.Id,
                Sequence = boardElement.Sequence,
                TransactionNumber = boardElement.TransactionNumber,
                MealTypeId = boardElement.MealTypeId,
                IsGuestMeal = boardElement.IsGuestMeal,
                CountUsed = boardElement.CountUsed,
                CustomerCredential = boardElement.CustomerCredential.ToCustomerCredentialViewV02()
            };
        }

        /// <summary>
        /// Returns a <see cref="BoardElement"/> object based on this <see cref="BoardElementViewV02"/>.
        /// </summary>
        /// <param name="boardElementViewV02"></param>
        /// <returns></returns>
        public static BoardElement ToBoardElement(this BoardElementViewV02 boardElementViewV02)
        {
            if (boardElementViewV02 == null) return null;

            return new BoardElement
            {
                Id = boardElementViewV02.Id,
                Sequence = boardElementViewV02.Sequence,
                TransactionNumber = boardElementViewV02.TransactionNumber,
                MealTypeId = boardElementViewV02.MealTypeId,
                IsGuestMeal = boardElementViewV02.IsGuestMeal,
                CountUsed = boardElementViewV02.CountUsed,
                CustomerCredential = boardElementViewV02.CustomerCredential.ToCustomerCredential()
            };
        }
        #endregion
    }
}
