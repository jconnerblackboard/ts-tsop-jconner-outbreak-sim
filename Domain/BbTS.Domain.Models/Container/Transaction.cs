﻿using System;
using System.Collections.Generic;
using System.Linq;
using BbTS.Domain.Models.Container.Elements;
using BbTS.Domain.Models.Definitions.Container;
using BbTS.Domain.Models.General;

namespace BbTS.Domain.Models.Container
{
    /// <summary>
    /// Primary container class for a BbTS transaction.
    /// </summary>
    public class Transaction
    {
        /// <summary>
        /// Unique identifier (guid) for this transaction.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Unique identifier (guid) for the originator of this transaction.  Every originator would be assigned a guid that it would use forever.
        /// </summary>
        public Guid OriginatorId { get; set; }

        /// <summary>
        /// Overall state of the transaction.
        /// </summary>
        public Disposition Disposition { get; set; }

        /// <summary>
        /// True if this transaction has already occured (as would be in the case of an offline transaction).
        /// </summary>
        public bool ForcePost { get; set; }

        /// <summary>
        /// List of all meaningful timestamps, i.e. Start, Finish, Submit, Post, etc.
        /// </summary>
        public List<TransactionTimestamp> TransactionTimestamps { get; set; } = new List<TransactionTimestamp>();

        /// <summary>
        /// List of transaction elements
        /// </summary>
        public List<Element> Elements { get; set; } = new List<Element>();

        /// <summary>
        /// Any additional metadata that is available for this transaction.  Examples are things like IP Address, Phase of the Moon, etc.
        /// </summary>
        public List<StringPair> NameValues { get; set; } = new List<StringPair>();

        /// <summary>
        /// Return a deep copy of this object.
        /// </summary>
        /// <returns><see cref="Transaction"/> object that is a clone of this.</returns>
        public Transaction Clone()
        {
            var transaction = (Transaction)MemberwiseClone();

            transaction.TransactionTimestamps = new List<TransactionTimestamp>();
            foreach (var transactionTimestamp in TransactionTimestamps)
            {
                transaction.TransactionTimestamps.Add(transactionTimestamp.Clone());
            }

            transaction.Elements = new List<Element>();
            foreach (var element in Elements)
            {
                transaction.Elements.Add(element.Clone());
            }

            transaction.NameValues = new List<StringPair>();
            foreach (var stringPair in NameValues)
            {
                transaction.NameValues.Add(new StringPair { Key = stringPair.Key, Value = stringPair.Value });
            }

            return transaction;
        }
    }

    /// <summary>
    /// View for a <see cref="Transaction"/> that contains MF4100-specific elements.  (Version 1)
    /// </summary>
    public class TransactionMf4100ViewV01
    {
        /// <summary>
        /// Unique identifier (guid) for this transaction.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// POS ID.  The MF4100 does not have an originator ID locally, so we will need to map the POS ID to an originator ID in the middle tier.
        /// </summary>
        public int PosId { get; set; }

        /// <summary>
        /// Overall state of the transaction.
        /// </summary>
        public Disposition Disposition { get; set; }

        /// <summary>
        /// True if this transaction has already occured (as would be in the case of an offline transaction).
        /// </summary>
        public bool ForcePost { get; set; }

        /// <summary>
        /// List of all meaningful timestamps, i.e. Start, Finish, Submit, Post, etc.
        /// </summary>
        public List<TransactionTimestampViewV02> TransactionTimestamps { get; set; } = new List<TransactionTimestampViewV02>();

        /// <summary>
        /// The operator performing this transaction.
        /// </summary>
        public CashierElementViewV01 CashierElement { get; set; }

        /// <summary>
        /// The control element used for login and logout of the operator.
        /// </summary>
        public ControlElementViewV01 ControlElement { get; set; }

        /// <summary>
        /// ARTS element.
        /// </summary>
        public ArtsElementMf4100ViewV01 ArtsElement { get; set; }

        /// <summary>
        /// Attendance element.
        /// </summary>
        public AttendanceElementViewV02 AttendanceElement { get; set; }

        /// <summary>
        /// Board element.
        /// </summary>
        public BoardElementViewV02 BoardElement { get; set; }

        /// <summary>
        /// Binary transaction element.
        /// </summary>
        public BinaryTransactionElementViewV01 BinaryTransactionElement { get; set; }

        /// <summary>
        /// Direct to gateway cancel auth element.
        /// </summary>
        public DtgCancelAuthElementViewV01 DtgCancelAuthElement { get; set; }

        /// <summary>
        /// Direct to gateway commit element.
        /// </summary>
        public DtgCommitElementViewV01 DtgCommitElement { get; set; }

        /// <summary>
        /// Direct to gateway communication log element.
        /// </summary>
        public DtgCommunicationLogElementViewV01 DtgCommunicationLogElement { get; set; }

        /// <summary>
        /// Any additional metadata that is available for this transaction.  Examples are things like IP Address, Phase of the Moon, etc.
        /// </summary>
        public List<StringPairViewV01> NameValues { get; set; } = new List<StringPairViewV01>();
    }

    /// <summary>
    /// View for a <see cref="Transaction"/>.  (Version 1)
    /// </summary>
    public class TransactionViewV01
    {
        /// <summary>
        /// Unique identifier (guid) for this transaction.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Unique identifier (guid) for the originator of this transaction.  Every originator would be assigned a guid that it would use forever.
        /// </summary>
        public Guid OriginatorId { get; set; }

        /// <summary>
        /// Overall state of the transaction.
        /// </summary>
        public Disposition Disposition { get; set; }

        /// <summary>
        /// True if this transaction has already occured (as would be in the case of an offline transaction).
        /// </summary>
        public bool ForcePost { get; set; }

        /// <summary>
        /// List of all meaningful timestamps, i.e. Start, Finish, Submit, Post, etc.
        /// </summary>
        public List<TransactionTimestampViewV01> TransactionTimestamps { get; set; } = new List<TransactionTimestampViewV01>();

        /// <summary>
        /// The device or client where the transaction originated from.
        /// </summary>
        public OriginatorElementViewV01 OriginatorElement { get; set; }

        /// <summary>
        /// The operator performing this transaction.
        /// </summary>
        public OperatorElementViewV01 OperatorElement { get; set; }

        /// <summary>
        /// The control element used for login and logout of the operator.
        /// </summary>
        public ControlElementViewV01 ControlElement { get; set; }

        /// <summary>
        /// List of ARTS elements that were performed as part of the transaction.
        /// </summary>
        public List<ArtsElementViewV01> ArtsTransactions { get; set; } = new List<ArtsElementViewV01>();

        /// <summary>
        /// List of attendance transactions that were performed as part of the transaction.
        /// </summary>
        public List<AttendanceElementViewV01> AttendanceTransactions { get; set; } = new List<AttendanceElementViewV01>();

        /// <summary>
        /// List of board transactions that were performed as part of the transaction.
        /// </summary>
        public List<BoardElementViewV01> BoardTransactions { get; set; } = new List<BoardElementViewV01>();

        /// <summary>
        /// Any additional metadata that is available for this transaction.  Examples are things like IP Address, Phase of the Moon, etc.
        /// </summary>
        public List<StringPairViewV01> NameValues { get; set; } = new List<StringPairViewV01>();
    }

    /// <summary>
    /// View for a <see cref="Transaction"/>.  (Version 2)
    /// </summary>
    public class TransactionViewV02
    {
        /// <summary>
        /// Unique identifier (guid) for this transaction.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Unique identifier (guid) for the originator of this transaction.  Every originator would be assigned a guid that it would use forever.
        /// </summary>
        public Guid OriginatorId { get; set; }

        /// <summary>
        /// Overall state of the transaction.
        /// </summary>
        public Disposition Disposition { get; set; }

        /// <summary>
        /// True if this transaction has already occured (as would be in the case of an offline transaction).
        /// </summary>
        public bool ForcePost { get; set; }

        /// <summary>
        /// List of all meaningful timestamps, i.e. Start, Finish, Submit, Post, etc.
        /// </summary>
        public List<TransactionTimestampViewV02> TransactionTimestamps { get; set; } = new List<TransactionTimestampViewV02>();

        /// <summary>
        /// The device or client where the transaction originated from.
        /// </summary>
        public OriginatorElementViewV01 OriginatorElement { get; set; }

        /// <summary>
        /// The operator performing this transaction.
        /// </summary>
        public OperatorElementViewV01 OperatorElement { get; set; }

        /// <summary>
        /// The control element used for login and logout of the operator.
        /// </summary>
        public ControlElementViewV01 ControlElement { get; set; }

        /// <summary>
        /// ARTS element.
        /// </summary>
        public ArtsElementViewV02 ArtsElement { get; set; }

        /// <summary>
        /// Attendance element.
        /// </summary>
        public AttendanceElementViewV02 AttendanceElement { get; set; }

        /// <summary>
        /// Board element.
        /// </summary>
        public BoardElementViewV02 BoardElement { get; set; }

        /// <summary>
        /// Any additional metadata that is available for this transaction.  Examples are things like IP Address, Phase of the Moon, etc.
        /// </summary>
        public List<StringPairViewV01> NameValues { get; set; } = new List<StringPairViewV01>();
    }

    /// <summary>
    /// Extension methods for <see cref="Transaction"/> conversion.
    /// </summary>
    public static class TransactionConverter
    {
        #region Mf4100 Version 1
        /// <summary>
        /// Returns a <see cref="TransactionMf4100ViewV01"/> object based on this <see cref="Transaction"/>.
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static TransactionMf4100ViewV01 ToTransactionMf4100ViewV01(this Transaction transaction)
        {
            if (transaction == null) return null;

            int posId = 0;
            var posIdNameValue = transaction.NameValues.FirstOrDefault(sp => sp.Key == "PosId");
            if (posIdNameValue != null) posId = int.Parse(posIdNameValue.Value);

            var transactionMf4100ViewV01 = new TransactionMf4100ViewV01
            {
                Id = transaction.Id,
                PosId = posId,
                Disposition = transaction.Disposition,
                ForcePost = transaction.ForcePost,
                TransactionTimestamps = transaction.TransactionTimestamps.Select(transactionTimestamp => transactionTimestamp.ToTransactionTimestampViewV02()).ToList(),
                NameValues = transaction.NameValues.Select(nameValue => nameValue.ToStringPairViewV01()).ToList()
            };

            var cashierElement = transaction.Elements.FirstOrDefault(e => e is CashierElement) as CashierElement;
            if (cashierElement != null) transactionMf4100ViewV01.CashierElement = cashierElement.ToCashierElementViewV01();

            var controlElement = transaction.Elements.FirstOrDefault(e => e is ControlElement) as ControlElement;
            if (controlElement != null) transactionMf4100ViewV01.ControlElement = controlElement.ToControlElementViewV01();

            var artsElement = transaction.Elements.FirstOrDefault(e => e is ArtsTransactionElement) as ArtsTransactionElement;
            if (artsElement != null) transactionMf4100ViewV01.ArtsElement = artsElement.ToArtsElementMf4100ViewV01();

            var attendanceElement = transaction.Elements.FirstOrDefault(e => e is AttendanceElement) as AttendanceElement;
            if (attendanceElement != null) transactionMf4100ViewV01.AttendanceElement = attendanceElement.ToAttendanceElementViewV02();

            var boardElement = transaction.Elements.FirstOrDefault(e => e is BoardElement) as BoardElement;
            if (boardElement != null) transactionMf4100ViewV01.BoardElement = boardElement.ToBoardElementViewV02();

            var binaryTransactionElement = transaction.Elements.FirstOrDefault(e => e is BinaryTransactionElement) as BinaryTransactionElement;
            if (binaryTransactionElement != null) transactionMf4100ViewV01.BinaryTransactionElement = binaryTransactionElement.ToBinaryTransactionElementViewV01();

            var dtgCancelAuthElement = transaction.Elements.FirstOrDefault(e => e is DtgCancelAuthElement) as DtgCancelAuthElement;
            if (dtgCancelAuthElement != null) transactionMf4100ViewV01.DtgCancelAuthElement = dtgCancelAuthElement.ToDtgCancelAuthElementViewV01();

            var dtgCommitElement = transaction.Elements.FirstOrDefault(e => e is DtgCommitElement) as DtgCommitElement;
            if (dtgCommitElement != null) transactionMf4100ViewV01.DtgCommitElement = dtgCommitElement.ToDtgCommitElementViewV01();

            var dtgCommunicationLogElement = transaction.Elements.FirstOrDefault(e => e is DtgCommunicationLogElement) as DtgCommunicationLogElement;
            if (dtgCommunicationLogElement != null) transactionMf4100ViewV01.DtgCommunicationLogElement = dtgCommunicationLogElement.ToDtgCommunicationLogElementViewV01();

            return transactionMf4100ViewV01;
        }

        /// <summary>
        /// Returns a <see cref="Transaction"/> object based on this <see cref="TransactionMf4100ViewV01"/>.
        /// </summary>
        /// <param name="transactionMf4100ViewV01"></param>
        /// <returns></returns>
        public static Transaction ToTransaction(this TransactionMf4100ViewV01 transactionMf4100ViewV01)
        {
            if (transactionMf4100ViewV01 == null) return null;

            var transaction = new Transaction
            {
                Id = transactionMf4100ViewV01.Id,
                OriginatorId = Guid.Empty,
                Disposition = transactionMf4100ViewV01.Disposition,
                ForcePost = transactionMf4100ViewV01.ForcePost,
                TransactionTimestamps = transactionMf4100ViewV01.TransactionTimestamps.Select(transactionTimestampViewV02 => transactionTimestampViewV02.ToTransactionTimestamp()).ToList(),
                NameValues = transactionMf4100ViewV01.NameValues.Select(stringPairViewV01 => stringPairViewV01.ToStringPair()).ToList()
            };

            if (transactionMf4100ViewV01.CashierElement != null) transaction.Elements.Add(transactionMf4100ViewV01.CashierElement.ToCashierElement());
            if (transactionMf4100ViewV01.ControlElement != null) transaction.Elements.Add(transactionMf4100ViewV01.ControlElement.ToControlElement());
            if (transactionMf4100ViewV01.ArtsElement != null) transaction.Elements.Add(transactionMf4100ViewV01.ArtsElement.ToArtsElement());
            if (transactionMf4100ViewV01.AttendanceElement != null) transaction.Elements.Add(transactionMf4100ViewV01.AttendanceElement.ToAttendanceElement());
            if (transactionMf4100ViewV01.BoardElement != null) transaction.Elements.Add(transactionMf4100ViewV01.BoardElement.ToBoardElement());
            if (transactionMf4100ViewV01.BinaryTransactionElement != null) transaction.Elements.Add(transactionMf4100ViewV01.BinaryTransactionElement.ToBinaryTransactionElement());
            if (transactionMf4100ViewV01.DtgCancelAuthElement != null) transaction.Elements.Add(transactionMf4100ViewV01.DtgCancelAuthElement.ToDtgCancelAuthElement());
            if (transactionMf4100ViewV01.DtgCommitElement != null) transaction.Elements.Add(transactionMf4100ViewV01.DtgCommitElement.ToDtgCommitElement());
            if (transactionMf4100ViewV01.DtgCommunicationLogElement != null) transaction.Elements.Add(transactionMf4100ViewV01.DtgCommunicationLogElement.ToDtgCommunicationLogElement());

            transaction.Elements.Sort((x, y) => x.Sequence.CompareTo(y.Sequence));

            return transaction;
        }
        #endregion

        #region Version 1
        /// <summary>
        /// Returns a <see cref="TransactionViewV01"/> object based on this <see cref="Transaction"/>.
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static TransactionViewV01 ToTransactionViewV01(this Transaction transaction)
        {
            if (transaction == null) return null;

            var transactionViewV01 = new TransactionViewV01
            {
                Id = transaction.Id,
                OriginatorId = transaction.OriginatorId,
                Disposition = transaction.Disposition,
                ForcePost = transaction.ForcePost,
                TransactionTimestamps = transaction.TransactionTimestamps.Select(transactionTimestamp => transactionTimestamp.ToTransactionTimestampViewV01()).ToList(),
                NameValues = transaction.NameValues.Select(stringPair => stringPair.ToStringPairViewV01()).ToList()
            };

            var originatorElement = transaction.Elements.FirstOrDefault(e => e is OriginatorElement) as OriginatorElement;
            if (originatorElement != null) transactionViewV01.OriginatorElement = originatorElement.ToOriginatorElementViewV01();

            var operatorElement = transaction.Elements.FirstOrDefault(e => e is OperatorElement) as OperatorElement;
            if (operatorElement != null) transactionViewV01.OperatorElement = operatorElement.ToOperatorElementViewV01();

            var controlElement = transaction.Elements.FirstOrDefault(e => e is ControlElement) as ControlElement;
            if (controlElement != null) transactionViewV01.ControlElement = controlElement.ToControlElementViewV01();

            var artsElement = transaction.Elements.FirstOrDefault(e => e is ArtsTransactionElement) as ArtsTransactionElement;
            if (artsElement != null) transactionViewV01.ArtsTransactions.Add(artsElement.ToArtsElementViewV01());

            var attendanceElement = transaction.Elements.FirstOrDefault(e => e is AttendanceElement) as AttendanceElement;
            if (attendanceElement != null) transactionViewV01.AttendanceTransactions.Add(attendanceElement.ToAttendanceElementViewV01());

            var boardElement = transaction.Elements.FirstOrDefault(e => e is BoardElement) as BoardElement;
            if (boardElement != null) transactionViewV01.BoardTransactions.Add(boardElement.ToBoardElementViewV01());

            return transactionViewV01;
        }

        /// <summary>
        /// Returns a <see cref="Transaction"/> object based on this <see cref="TransactionViewV01"/>.
        /// </summary>
        /// <param name="transactionViewV01"></param>
        /// <returns></returns>
        public static Transaction ToTransaction(this TransactionViewV01 transactionViewV01)
        {
            if (transactionViewV01 == null) return null;

            //See TSOP-1810.  The PR5000 is not sending an OriginatorId in the transaction object, so we will use the OriginatorElement for now.
            var originatorElement = transactionViewV01.OriginatorElement.ToOriginatorElement();
            transactionViewV01.OriginatorId = originatorElement?.OriginatorId ?? Guid.Empty;

            var transaction = new Transaction
            {
                Id = transactionViewV01.Id,
                OriginatorId = transactionViewV01.OriginatorId,
                Disposition = transactionViewV01.Disposition,
                ForcePost = transactionViewV01.ForcePost,
                TransactionTimestamps = transactionViewV01.TransactionTimestamps.Select(transactionTimestampViewV01 => transactionTimestampViewV01.ToTransactionTimestamp()).ToList(),
                NameValues = transactionViewV01.NameValues.Select(stringPairViewV01 => stringPairViewV01.ToStringPair()).ToList()
            };

            if (transactionViewV01.OriginatorElement != null) transaction.Elements.Add(transactionViewV01.OriginatorElement.ToOriginatorElement());
            if (transactionViewV01.OperatorElement != null) transaction.Elements.Add(transactionViewV01.OperatorElement.ToOperatorElement());
            if (transactionViewV01.ControlElement != null) transaction.Elements.Add(transactionViewV01.ControlElement.ToControlElement());

            if (transactionViewV01.ArtsTransactions != null)
            {
                foreach (var artsElementViewV01 in transactionViewV01.ArtsTransactions)
                {
                    transaction.Elements.Add(artsElementViewV01.ToArtsElement());
                }
            }

            if (transactionViewV01.AttendanceTransactions != null)
            {
                foreach (var attendanceElementViewV01 in transactionViewV01.AttendanceTransactions)
                {
                    transaction.Elements.Add(attendanceElementViewV01.ToAttendanceElement());
                }
            }

            if (transactionViewV01.BoardTransactions != null)
            {
                foreach (var boardElementViewV01 in transactionViewV01.BoardTransactions)
                {
                    transaction.Elements.Add(boardElementViewV01.ToBoardElement());
                }
            }

            transaction.Elements.Sort((x, y) => x.Sequence.CompareTo(y.Sequence));

            return transaction;
        }
        #endregion

        #region Version 2
        /// <summary>
        /// Returns a <see cref="TransactionViewV02"/> object based on this <see cref="Transaction"/>.
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static TransactionViewV02 ToTransactionViewV02(this Transaction transaction)
        {
            if (transaction == null) return null;

            var transactionViewV02 = new TransactionViewV02
            {
                Id = transaction.Id,
                OriginatorId = transaction.OriginatorId,
                Disposition = transaction.Disposition,
                ForcePost = transaction.ForcePost,
                TransactionTimestamps = transaction.TransactionTimestamps.Select(transactionTimestamp => transactionTimestamp.ToTransactionTimestampViewV02()).ToList(),
                NameValues = transaction.NameValues.Select(nameValue => nameValue.ToStringPairViewV01()).ToList()
            };

            var originatorElement = transaction.Elements.FirstOrDefault(e => e is OriginatorElement) as OriginatorElement;
            if (originatorElement != null) transactionViewV02.OriginatorElement = originatorElement.ToOriginatorElementViewV01();

            var operatorElement = transaction.Elements.FirstOrDefault(e => e is OperatorElement) as OperatorElement;
            if (operatorElement != null) transactionViewV02.OperatorElement = operatorElement.ToOperatorElementViewV01();

            var controlElement = transaction.Elements.FirstOrDefault(e => e is ControlElement) as ControlElement;
            if (controlElement != null) transactionViewV02.ControlElement = controlElement.ToControlElementViewV01();

            var artsElement = transaction.Elements.FirstOrDefault(e => e is ArtsTransactionElement) as ArtsTransactionElement;
            if (artsElement != null) transactionViewV02.ArtsElement = artsElement.ToArtsElementViewV02();

            var attendanceElement = transaction.Elements.FirstOrDefault(e => e is AttendanceElement) as AttendanceElement;
            if (attendanceElement != null) transactionViewV02.AttendanceElement = attendanceElement.ToAttendanceElementViewV02();

            var boardElement = transaction.Elements.FirstOrDefault(e => e is BoardElement) as BoardElement;
            if (boardElement != null) transactionViewV02.BoardElement = boardElement.ToBoardElementViewV02();

            return transactionViewV02;
        }

        /// <summary>
        /// Returns a <see cref="Transaction"/> object based on this <see cref="TransactionViewV02"/>.
        /// </summary>
        /// <param name="transactionViewV02"></param>
        /// <returns></returns>
        public static Transaction ToTransaction(this TransactionViewV02 transactionViewV02)
        {
            if (transactionViewV02 == null) return null;

            var transaction = new Transaction
            {
                Id = transactionViewV02.Id,
                OriginatorId = transactionViewV02.OriginatorId,
                Disposition = transactionViewV02.Disposition,
                ForcePost = transactionViewV02.ForcePost,
                TransactionTimestamps = transactionViewV02.TransactionTimestamps.Select(transactionTimestampViewV02 => transactionTimestampViewV02.ToTransactionTimestamp()).ToList(),
                NameValues = transactionViewV02.NameValues.Select(stringPairViewV01 => stringPairViewV01.ToStringPair()).ToList()
            };

            if (transactionViewV02.OriginatorElement != null) transaction.Elements.Add(transactionViewV02.OriginatorElement.ToOriginatorElement());
            if (transactionViewV02.OperatorElement != null) transaction.Elements.Add(transactionViewV02.OperatorElement.ToOperatorElement());
            if (transactionViewV02.ControlElement != null) transaction.Elements.Add(transactionViewV02.ControlElement.ToControlElement());
            if (transactionViewV02.ArtsElement != null) transaction.Elements.Add(transactionViewV02.ArtsElement.ToArtsElement());
            if (transactionViewV02.AttendanceElement != null) transaction.Elements.Add(transactionViewV02.AttendanceElement.ToAttendanceElement());
            if (transactionViewV02.BoardElement != null) transaction.Elements.Add(transactionViewV02.BoardElement.ToBoardElement());

            transaction.Elements.Sort((x, y) => x.Sequence.CompareTo(y.Sequence));

            return transaction;
        }
        #endregion
    }
}
