﻿using System;
using System.Collections.Generic;

namespace BbTS.Domain.Models.Container
{
    /// <summary>
    /// Container class to hold a tree pattern of name/value pairs that might contain children nodes of name/value pairs.
    /// </summary>
    public class NameValueTree
    {
        /// <summary>
        /// Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Value.
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// The original type of the parsed node
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// List of children ( <see cref="NameValueTree"/> types )
        /// </summary>
        public List<NameValueTree> Children { get; set; } = new List<NameValueTree>();
    }
}
