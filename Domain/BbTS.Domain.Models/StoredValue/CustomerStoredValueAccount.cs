﻿using BbTS.Domain.Models.Definitions.ArtsDataModel;

namespace BbTS.Domain.Models.StoredValue
{
    /// <summary>
    /// Container class for customer stored value accounts.
    /// </summary>
    public class CustomerStoredValueAccount
    {
        /// <summary>
        /// The customer's numerical identifier.
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// The debit or credit type of the stored value account.
        /// </summary>
        public DebitCreditType DebitCreditType { get; set; }

        /// <summary>
        /// The amount in the stored value account.
        /// </summary>
        public int Amount { get; set; }

        /// <summary>
        /// The name of the stored value account.
        /// </summary>
        public string StoredValueAccountName { get; set; }

        /// <summary>
        /// The balance after the transaction.
        /// </summary>
        public int BalanceAfterTransaction { get; set; }
    }
}
