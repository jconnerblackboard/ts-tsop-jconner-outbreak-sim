﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.StoredValue
{
    /// <summary>
    /// Sv account type get response container
    /// </summary>
    public class StoredValueAccountTypeGetResponse
    {
        /// <summary>
        /// Stored value account types
        /// </summary>
        public List<StoredValueAccountType> StoredValueAccountTypes { get; set; }
    }
}
