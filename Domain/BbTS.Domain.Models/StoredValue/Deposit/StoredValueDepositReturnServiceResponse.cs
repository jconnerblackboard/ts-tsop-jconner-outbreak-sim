﻿using BbTS.Domain.Models.Container;

namespace BbTS.Domain.Models.StoredValue.Deposit
{
    /// <summary>
    /// Container class for a stored value deposit return response from the service layer.
    /// </summary>
    public class StoredValueDepositReturnServiceResponse
    {
        /// <summary>
        /// The stored value deposit return request from the service layer associated with this response.
        /// </summary>
        public StoredValueDepositReturnServiceRequest Request { get; internal set; }

        /// <summary>
        /// Transaction associated with this response
        /// </summary>
        public TransactionViewV01 Transaction { get; set; }

        /// <summary>
        /// Parameterized constructor for this response to ensure that request is properly associated.
        /// </summary>
        /// <param name="request"></param>
        public StoredValueDepositReturnServiceResponse(StoredValueDepositReturnServiceRequest request)
        {
            Request = request;
        }
    }
}
