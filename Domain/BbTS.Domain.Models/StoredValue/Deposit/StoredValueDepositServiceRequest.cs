﻿using System;
using BbTS.Domain.Models.Container;

namespace BbTS.Domain.Models.StoredValue.Deposit
{
    /// <summary>
    /// Container class for a stored value deposit request from the service layer.
    /// </summary>
    public class StoredValueDepositServiceRequest
    {
        /// <summary>
        /// Unique identifier attached to this request.
        /// </summary>
        public Guid RequestId { get; set; }

        /// <summary>
        /// Transaction object associated with this request.
        /// </summary>
        public TransactionViewV01 Transaction { get; set; }
    }
}
