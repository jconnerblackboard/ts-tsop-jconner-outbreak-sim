﻿using BbTS.Domain.Models.Container;

namespace BbTS.Domain.Models.StoredValue.Deposit
{
    /// <summary>
    /// Container class for a stored value deposit response from the service layer.
    /// </summary>
    public class StoredValueDepositServiceResponse
    {
        /// <summary>
        /// The stored value deposit response from the service layer associated with this response.
        /// </summary>
        public StoredValueDepositServiceRequest Request { get; internal set; }

        /// <summary>
        /// Transaction associated with this response
        /// </summary>
        public TransactionViewV01 Transaction { get; set; }

        /// <summary>
        /// Parameterized constructor for this response to ensure that request is properly associated.
        /// </summary>
        /// <param name="request"></param>
        public StoredValueDepositServiceResponse(StoredValueDepositServiceRequest request)
        {
            Request = request;
        }
    }
}
