﻿using System;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.StoredValue
{
    /// <summary>
    /// This object represents a Transact sv_account object. Stored Value Account table
    /// </summary>
    [Serializable]
    public class TsSv_Account
    {
        /// <summary>
        /// Stored Value Account Identifier
        /// </summary>
        [XmlAttribute]
        public int Sv_Account_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int Sv_Account_Type_Id { get; set; }
        /// <summary>
        /// Name of stored value account
        /// </summary>
        [XmlAttribute]
        public string Name { get; set; }
        /// <summary>
        /// Account Number
        /// </summary>
        [XmlAttribute]
        public int Account_Number { get; set; }
        /// <summary>
        /// Balance Forward
        /// </summary>
        [XmlAttribute]
        public int Balance_Forward { get; set; }
        /// <summary>
        /// Balance of account
        /// </summary>
        [XmlAttribute]
        public int Balance { get; set; }
        /// <summary>
        /// Credit Limit on account
        /// </summary>
        [XmlAttribute]
        public int Credit_Limit { get; set; }
        /// <summary>
        /// (0) - Individual, (1) - Joint
        /// </summary>
        [XmlAttribute]
        public int Individual_Or_Joint { get; set; }
    }
}