﻿
namespace BbTS.Domain.Models.StoredValue
{
    /// <summary>
    /// This object represents a record in the Transact system for a stored value account
    /// </summary>
    public class StoredValueEnrichment
    {
        /// <summary>
        /// The Account Type that gets enriched
        /// </summary>
        public int SvEnrichmentAccountTypeId { get; set; }

        /// <summary>
        /// The account type that will trigger an enrichment
        /// </summary>
        public int SvAccountTypeId { get; set; }

        /// <summary>
        /// Minimum transaction amount that will trigger this enrichment
        /// </summary>
        public decimal MinimumAmount { get; set; }

        /// <summary>
        /// The enrichment percentage to calculate with the transaction amount (value is between 0 - 1.0)
        /// </summary>
        public decimal Percentage { get; set; }

        /// <summary>
        /// Indicates if the Enrichment is active
        /// </summary>
        public bool IsActive { get; set; }
    }
}
