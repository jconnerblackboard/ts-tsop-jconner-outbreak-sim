﻿using System.Collections.Generic;
using System.Linq;

namespace BbTS.Domain.Models.StoredValue
{
    /// <summary>
    /// Container class for stored value account definitions on a device.
    /// </summary>
    public class StoredValueAccountDeviceSetting
    {
        /// <summary>
        /// The identifier for the type of stored value account.
        /// </summary>
        public int TypeId { get; set; }

        /// <summary>
        /// The name for the stored value account.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Enrichment information.
        /// </summary>
        public List<StoredValueAccountEnrichmentDeviceSetting> Enrichments { get; set; } = new List<StoredValueAccountEnrichmentDeviceSetting>();
    }

    /// <summary>
    /// View for a <see cref="StoredValueAccountDeviceSetting"/>.  (Version 1)
    /// </summary>
    public class StoredValueAccountDeviceSettingViewV01
    {
        /// <summary>
        /// The identifier for the type of stored value account.
        /// </summary>
        public int TypeId { get; set; }

        /// <summary>
        /// The name for the stored value account.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Enrichment information.
        /// </summary>
        public List<StoredValueAccountEnrichmentDeviceSettingViewV01> Enrichments { get; set; } = new List<StoredValueAccountEnrichmentDeviceSettingViewV01>();
    }

    /// <summary>
    /// Extension methods for <see cref="StoredValueAccountDeviceSetting"/> conversion.
    /// </summary>
    public static class StoredValueAccountDeviceSettingConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="StoredValueAccountDeviceSettingViewV01"/> object based on this <see cref="StoredValueAccountDeviceSetting"/>.
        /// </summary>
        /// <param name="storedValueAccountDeviceSetting"></param>
        /// <returns></returns>
        public static StoredValueAccountDeviceSettingViewV01 ToStoredValueAccountDeviceSettingViewV01(this StoredValueAccountDeviceSetting storedValueAccountDeviceSetting)
        {
            if (storedValueAccountDeviceSetting == null) return null;

            return new StoredValueAccountDeviceSettingViewV01
            {
                TypeId = storedValueAccountDeviceSetting.TypeId,
                Name = storedValueAccountDeviceSetting.Name,
                Enrichments = storedValueAccountDeviceSetting.Enrichments.Select(enrichment => enrichment.ToStoredValueAccountEnrichmentDeviceSettingViewV01()).ToList()
            };
        }

        /// <summary>
        /// Returns a <see cref="StoredValueAccountDeviceSetting"/> object based on this <see cref="StoredValueAccountDeviceSettingViewV01"/>.
        /// </summary>
        /// <param name="storedValueAccountDeviceSettingViewV01"></param>
        /// <returns></returns>
        public static StoredValueAccountDeviceSetting ToStoredValueAccountDeviceSetting(this StoredValueAccountDeviceSettingViewV01 storedValueAccountDeviceSettingViewV01)
        {
            if (storedValueAccountDeviceSettingViewV01 == null) return null;

            return new StoredValueAccountDeviceSetting
            {
                TypeId = storedValueAccountDeviceSettingViewV01.TypeId,
                Name = storedValueAccountDeviceSettingViewV01.Name,
                Enrichments = storedValueAccountDeviceSettingViewV01.Enrichments.Select(enrichmentView => enrichmentView.ToStoredValueAccountEnrichmentDeviceSetting()).ToList()
            };
        }
        #endregion
    }
}
