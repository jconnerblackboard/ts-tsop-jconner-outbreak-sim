﻿using BbTS.Domain.Models.Definitions.Transaction;

namespace BbTS.Domain.Models.StoredValue
{
    /// <summary>
    /// This object represents a record in the Transact system for a stored value account name
    /// </summary>
    public class TsStoredValueAccountType
    {
        /// <summary>
        /// The identity of the StoredValueAccountType
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The human friendly name of the StoredValueAccountType
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// A short name for the StoredValueAccountType
        /// </summary>
        public string NameTerminalDisplay { get; set; }

        /// <summary>
        /// A code name for the StoredValueAccountType
        /// </summary>
        public string NameCode { get; set; }

        /// <summary>
        /// Whether the StoredValueAccountType is tax exempt or not
        /// </summary>
        public string TaxExempt { get; set; }

        /// <summary>
        /// Type of Stored value account
        /// </summary>
        public StoredValueAccountShareType StoredValueAccountShareType { get; set; }

        /// <summary>
        /// If it is a group account, this is the identity of the group account
        /// </summary>
        public int GroupAccountId { get; set; }
    }
}
