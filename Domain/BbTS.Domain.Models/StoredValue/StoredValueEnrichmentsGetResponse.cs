﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.StoredValue
{
    /// <summary>
    /// Sv account type get response container
    /// </summary>
    public class StoredValueEnrichmentsGetResponse
    {
        /// <summary>
        /// Stored value account types
        /// </summary>
        public List<StoredValueEnrichment> StoredValueEnrichments { get; set; }
    }
}
