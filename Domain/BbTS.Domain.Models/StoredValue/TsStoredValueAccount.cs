﻿using System;

namespace BbTS.Domain.Models.StoredValue
{
    /// <summary>
    /// This object represents a record in the Transact system for a stored value account
    /// </summary>
    class TsStoredValueAccount
    {
        /// <summary>
        /// The identity of this record
        /// </summary>
        public Int32 StoredValueAccountId { get; set; }
        /// <summary>
        /// The type of stored value account
        /// </summary>
        public Int32 StoredValueAccountTypeId { get; set; }
        /// <summary>
        /// The name of this stored value account
        /// </summary>
        public String Name { get; set; }
        /// <summary>
        /// The stored value account number
        /// </summary>
        public Int32 AccountNumber { get; set; }
        /// <summary>
        /// The stored value account carried forward (expressed in thousandths of a penny)
        /// </summary>
        public Int32 BalanceForward { get; set; }
        /// <summary>
        /// The stored value account balance (expressed in thousandths of a penny)
        /// </summary>
        public Int32 Balance { get; set; }
        /// <summary>
        /// The stored value account credit limit (expressed in thousandths of a penny)
        /// </summary>
        public Int32 CreditLimit { get; set; }
        /// <summary>
        /// Whether this account is an individual or joint account
        /// </summary>
        public Boolean IsJoint { get; set; }
    }
}
