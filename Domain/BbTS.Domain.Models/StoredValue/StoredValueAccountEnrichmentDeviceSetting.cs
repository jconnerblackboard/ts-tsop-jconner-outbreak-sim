﻿namespace BbTS.Domain.Models.StoredValue
{
    /// <summary>
    /// Container class for stored value account enrichment definitions on a device.
    /// </summary>
    public class StoredValueAccountEnrichmentDeviceSetting
    {
        /// <summary>
        /// The identifier for the type of stored value account.
        /// </summary>
        public int TypeId { get; set; }

        /// <summary>
        /// Minimum amount (for the original deposit) that triggers this enrichment.
        /// </summary>
        public decimal MinimumAmount { get; set; }

        /// <summary>
        /// Enrichment rate.  A 25% enrichment rate is represented as 0.25.
        /// </summary>
        public decimal EnrichmentRate { get; set; }
    }

    /// <summary>
    /// View for a <see cref="StoredValueAccountEnrichmentDeviceSetting"/>.  (Version 1)
    /// </summary>
    public class StoredValueAccountEnrichmentDeviceSettingViewV01
    {
        /// <summary>
        /// The identifier for the type of stored value account.
        /// </summary>
        public int TypeId { get; set; }

        /// <summary>
        /// Minimum amount (for the original deposit) that triggers this enrichment.
        /// </summary>
        public decimal MinimumAmount { get; set; }

        /// <summary>
        /// Enrichment rate.  A 25% enrichment rate is represented as 0.25.
        /// </summary>
        public decimal EnrichmentRate { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="StoredValueAccountEnrichmentDeviceSetting"/> conversion.
    /// </summary>
    public static class StoredValueAccountEnrichmentDeviceSettingConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="StoredValueAccountEnrichmentDeviceSettingViewV01"/> object based on this <see cref="StoredValueAccountEnrichmentDeviceSetting"/>.
        /// </summary>
        /// <param name="storedValueAccountEnrichmentDeviceSetting"></param>
        /// <returns></returns>
        public static StoredValueAccountEnrichmentDeviceSettingViewV01 ToStoredValueAccountEnrichmentDeviceSettingViewV01(this StoredValueAccountEnrichmentDeviceSetting storedValueAccountEnrichmentDeviceSetting)
        {
            if (storedValueAccountEnrichmentDeviceSetting == null) return null;

            return new StoredValueAccountEnrichmentDeviceSettingViewV01
            {
                TypeId = storedValueAccountEnrichmentDeviceSetting.TypeId,
                MinimumAmount = storedValueAccountEnrichmentDeviceSetting.MinimumAmount,
                EnrichmentRate = storedValueAccountEnrichmentDeviceSetting.EnrichmentRate
            };
        }

        /// <summary>
        /// Returns a <see cref="StoredValueAccountEnrichmentDeviceSetting"/> object based on this <see cref="StoredValueAccountEnrichmentDeviceSettingViewV01"/>.
        /// </summary>
        /// <param name="storedValueAccountEnrichmentDeviceSettingViewV01"></param>
        /// <returns></returns>
        public static StoredValueAccountEnrichmentDeviceSetting ToStoredValueAccountEnrichmentDeviceSetting(this StoredValueAccountEnrichmentDeviceSettingViewV01 storedValueAccountEnrichmentDeviceSettingViewV01)
        {
            if (storedValueAccountEnrichmentDeviceSettingViewV01 == null) return null;

            return new StoredValueAccountEnrichmentDeviceSetting
            {
                TypeId = storedValueAccountEnrichmentDeviceSettingViewV01.TypeId,
                MinimumAmount = storedValueAccountEnrichmentDeviceSettingViewV01.MinimumAmount,
                EnrichmentRate = storedValueAccountEnrichmentDeviceSettingViewV01.EnrichmentRate
            };
        }
        #endregion
    }
}
