﻿using BbTS.Domain.Models.Definitions.Transaction;
using System;

namespace BbTS.Domain.Models.StoredValue
{
    /// <summary>
    /// This object represents a record in the Transact system for a stored value account
    /// </summary>
    public class StoredValueAccountType
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The guid of the StoredValueAccountType
        /// </summary>
        public Guid Guid { get; set; }

        /// <summary>
        /// The human friendly name of the StoredValueAccountType
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// A short name for the StoredValueAccountType
        /// </summary>
        public string NameTerminalDisplay { get; set; }

        /// <summary>
        /// A code name for the StoredValueAccountType
        /// </summary>
        public string NameCode { get; set; }

        /// <summary>
        /// Whether the StoredValueAccountType is tax exempt or not
        /// </summary>
        public bool TaxExempt { get; set; }

        /// <summary>
        /// Type of Stored value account
        /// </summary>
        public StoredValueAccountShareType StoredValueAccountShareType { get; set; }
    }
}
