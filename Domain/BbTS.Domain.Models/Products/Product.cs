﻿using System;
using BbTS.Domain.Models.Definitions.Transaction;

namespace BbTS.Domain.Models.Products
{
    /// <summary>
    /// Product class used by a POS.
    /// </summary>
    public class Product
    {
        /// <summary>
        /// Product ID.
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// Product detail ID.
        /// </summary>
        public int ProductDetailId { get; set; }

        /// <summary>
        /// Product number.
        /// </summary>
        public string ProductNumber { get; set; }

        /// <summary>
        /// Product description.  (This is used as the product name in most use cases.)
        /// </summary>
        public string ProductDescription { get; set; }

        /// <summary>
        /// Product type.
        /// </summary>
        public ProductType ProductType { get; set; }

        /// <summary>
        /// Tax schedule ID.
        /// </summary>
        public int TaxScheduleId { get; set; }

        /// <summary>
        /// Tax group ID.
        /// </summary>
        public int? TaxGroupId { get; set; }

        /// <summary>
        /// Retail price of product.
        /// </summary>
        public decimal RetailPrice { get; set; }

        /// <summary>
        /// Minimum price that can be charged when using variable pricing.
        /// </summary>
        public decimal VariablePriceMinimum { get; set; }

        /// <summary>
        /// Maximum price that can be charged when using variable pricing.
        /// </summary>
        public decimal VariablePriceMaximum { get; set; }

        /// <summary>
        /// If true, a receipt must be printed if this product is part of the transaction.
        /// </summary>
        public bool ForceReceipt { get; set; }

        /// <summary>
        /// Indicates if the product price can be modified by a Discount or Surcharge operation initiated by the cashier.
        /// </summary>
        public bool AllowOverride { get; set; }

        /// <summary>
        /// Threshold at which the DiscountPrice1 should be used for the product price.
        /// </summary>
        public int DiscountQuantity1 { get; set; }

        /// <summary>
        /// Price to be used for the product when the quantity of this product in the transaction >= DiscountQuantity1.
        /// </summary>
        public decimal DiscountPrice1 { get; set; }

        /// <summary>
        /// Threshold at which the DiscountPrice2 should be used for the product price.
        /// </summary>
        public int DiscountQuantity2 { get; set; }

        /// <summary>
        /// Price to be used for the product when the quantity of this product in the transaction >= DiscountQuantity2.
        /// </summary>
        public decimal DiscountPrice2 { get; set; }

        /// <summary>
        /// Threshold at which the DiscountPrice3 should be used for the product price.
        /// </summary>
        public int DiscountQuantity3 { get; set; }

        /// <summary>
        /// Price to be used for the product when the quantity of this product in the transaction >= DiscountQuantity3.
        /// </summary>
        public decimal DiscountPrice3 { get; set; }

        /// <summary>
        /// Last modified date/time (UTC)
        /// </summary>
        public DateTime LastModified { get; set; }

        /// <summary>
        /// Indicates if the product price can be discounted by policy-based discount rule.
        /// </summary>
        public bool AllowPolicyDiscount { get; set; }

        /// <summary>
        /// Indicates if the product price can be surcharged by policy-based surcharge rule.
        /// </summary>
        public bool AllowPolicySurcharge { get; set; }

        /// <summary>
        /// Text to show on a product key, line 1
        /// </summary>
        public string Keytop1 { get; set; }

        /// <summary>
        /// Text to show on a product key, line 2
        /// </summary>
        public string Keytop2 { get; set; }

        /// <summary>
        /// The UTC offset of the included DateTime objects.
        /// </summary>
        public string DateTimeOffset { get; set; }

        /// <summary>
        /// Return a deep copy of this object.
        /// </summary>
        /// <returns><see cref="Product"/> object that is a clone of this.</returns>
        public Product Clone()
        {
            var product = (Product)MemberwiseClone();
            return product;
        }
    }

    /// <summary>
    /// View for a <see cref="Product"/>.  (Version 1)
    /// </summary>
    public class ProductViewV01
    {
        /// <summary>
        /// Product ID.
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// Product detail ID.
        /// </summary>
        public int ProductDetailId { get; set; }

        /// <summary>
        /// Product number.
        /// </summary>
        public string ProductNumber { get; set; }

        /// <summary>
        /// Product description.  (This is used as the product name in most use cases.)
        /// </summary>
        public string ProductDescription { get; set; }

        /// <summary>
        /// Product type.
        /// </summary>
        public ProductType ProductType { get; set; }

        /// <summary>
        /// Tax schedule ID.
        /// </summary>
        public int TaxScheduleId { get; set; }

        /// <summary>
        /// Tax group ID.
        /// </summary>
        public int? TaxGroupId { get; set; }

        /// <summary>
        /// Retail price of product.
        /// </summary>
        public decimal RetailPrice { get; set; }

        /// <summary>
        /// Minimum price that can be charged when using variable pricing.
        /// </summary>
        public decimal VariablePriceMinimum { get; set; }

        /// <summary>
        /// Maximum price that can be charged when using variable pricing.
        /// </summary>
        public decimal VariablePriceMaximum { get; set; }

        /// <summary>
        /// If true, a receipt must be printed if this product is part of the transaction.
        /// </summary>
        public bool ForceReceipt { get; set; }

        /// <summary>
        /// Indicates if the product price can be modified by a Discount or Surcharge operation initiated by the cashier.
        /// </summary>
        public bool AllowOverride { get; set; }

        /// <summary>
        /// Threshold at which the DiscountPrice1 should be used for the product price.
        /// </summary>
        public int DiscountQuantity1 { get; set; }

        /// <summary>
        /// Price to be used for the product when the quantity of this product in the transaction >= DiscountQuantity1.
        /// </summary>
        public decimal DiscountPrice1 { get; set; }

        /// <summary>
        /// Threshold at which the DiscountPrice2 should be used for the product price.
        /// </summary>
        public int DiscountQuantity2 { get; set; }

        /// <summary>
        /// Price to be used for the product when the quantity of this product in the transaction >= DiscountQuantity2.
        /// </summary>
        public decimal DiscountPrice2 { get; set; }

        /// <summary>
        /// Threshold at which the DiscountPrice3 should be used for the product price.
        /// </summary>
        public int DiscountQuantity3 { get; set; }

        /// <summary>
        /// Price to be used for the product when the quantity of this product in the transaction >= DiscountQuantity3.
        /// </summary>
        public decimal DiscountPrice3 { get; set; }

        /// <summary>
        /// Last modified date/time (UTC)
        /// </summary>
        public DateTime LastModified { get; set; }

        /// <summary>
        /// Indicates if the product price can be discounted by policy-based discount rule.
        /// </summary>
        public bool AllowPolicyDiscount { get; set; }

        /// <summary>
        /// Indicates if the product price can be surcharged by policy-based surcharge rule.
        /// </summary>
        public bool AllowPolicySurcharge { get; set; }

        /// <summary>
        /// Text to show on a product key, line 1
        /// </summary>
        public string Keytop1 { get; set; }

        /// <summary>
        /// Text to show on a product key, line 2
        /// </summary>
        public string Keytop2 { get; set; }

        /// <summary>
        /// The UTC offset of the included DateTime objects.
        /// </summary>
        public string DateTimeOffset { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="Product"/> conversion.
    /// </summary>
    public static class ProductConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="ProductViewV01"/> object based on this <see cref="Product"/>.
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public static ProductViewV01 ToProductViewV01(this Product product)
        {
            if (product == null) return null;

            return new ProductViewV01
            {
                ProductId = product.ProductId,
                ProductDetailId = product.ProductDetailId,
                ProductNumber = product.ProductNumber,
                ProductDescription = product.ProductDescription,
                ProductType = product.ProductType,
                TaxScheduleId = product.TaxScheduleId,
                TaxGroupId = product.TaxGroupId,
                RetailPrice = product.RetailPrice,
                VariablePriceMinimum = product.VariablePriceMinimum,
                VariablePriceMaximum = product.VariablePriceMaximum,
                ForceReceipt = product.ForceReceipt,
                AllowOverride = product.AllowOverride,
                DiscountQuantity1 = product.DiscountQuantity1,
                DiscountPrice1 = product.DiscountPrice1,
                DiscountQuantity2 = product.DiscountQuantity2,
                DiscountPrice2 = product.DiscountPrice2,
                DiscountQuantity3 = product.DiscountQuantity3,
                DiscountPrice3 = product.DiscountPrice3,
                LastModified = product.LastModified,
                AllowPolicyDiscount = product.AllowPolicyDiscount,
                AllowPolicySurcharge = product.AllowPolicySurcharge,
                Keytop1 = product.Keytop1,
                Keytop2 = product.Keytop2,
                DateTimeOffset = product.DateTimeOffset
            };
        }

        /// <summary>
        /// Returns a <see cref="Product"/> object based on this <see cref="ProductViewV01"/>.
        /// </summary>
        /// <param name="productViewV01"></param>
        /// <returns></returns>
        public static Product ToProduct(this ProductViewV01 productViewV01)
        {
            if (productViewV01 == null) return null;

            return new Product
            {
                ProductId = productViewV01.ProductId,
                ProductDetailId = productViewV01.ProductDetailId,
                ProductNumber = productViewV01.ProductNumber,
                ProductDescription = productViewV01.ProductDescription,
                ProductType = productViewV01.ProductType,
                TaxScheduleId = productViewV01.TaxScheduleId,
                TaxGroupId = productViewV01.TaxGroupId,
                RetailPrice = productViewV01.RetailPrice,
                VariablePriceMinimum = productViewV01.VariablePriceMinimum,
                VariablePriceMaximum = productViewV01.VariablePriceMaximum,
                ForceReceipt = productViewV01.ForceReceipt,
                AllowOverride = productViewV01.AllowOverride,
                DiscountQuantity1 = productViewV01.DiscountQuantity1,
                DiscountPrice1 = productViewV01.DiscountPrice1,
                DiscountQuantity2 = productViewV01.DiscountQuantity2,
                DiscountPrice2 = productViewV01.DiscountPrice2,
                DiscountQuantity3 = productViewV01.DiscountQuantity3,
                DiscountPrice3 = productViewV01.DiscountPrice3,
                LastModified = productViewV01.LastModified,
                AllowPolicyDiscount = productViewV01.AllowPolicyDiscount,
                AllowPolicySurcharge = productViewV01.AllowPolicySurcharge,
                Keytop1 = productViewV01.Keytop1,
                Keytop2 = productViewV01.Keytop2,
                DateTimeOffset = productViewV01.DateTimeOffset
            };
        }
        #endregion
    }
}
