﻿using System;
using Newtonsoft.Json.Serialization;

namespace BbTS.Domain.Models.Serialization
{
    public class Pos4100AssemblySerializationBinder : DefaultSerializationBinder
    {
        //Used in deserialization to map the assembly name read from JSON to the assembly name in the project.
        public override Type BindToType(string assemblyName, string typeName)
        {
            return assemblyName.Contains("BbTS.Models.CF") ? Type.GetType(typeName + ", BbTS.Domain.Models") : base.BindToType(assemblyName, typeName);
        }
    }

}
