﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Messaging.HostMonitor.Comparison
{
    /// <summary>
    /// Comparison class for checking if two HostMonitorMessage objects are equal.
    /// </summary>
    public class HostMonitorMessageEqualityComparer : IEqualityComparer<HostMonitorMessage>
    {
        /// <summary>
        /// Two objects are equal if their Ids are equal.
        /// </summary>
        /// <param name="x">base</param>
        /// <param name="y">comparison</param>
        /// <returns></returns>
        public bool Equals(HostMonitorMessage x, HostMonitorMessage y)
        {
            if (x == null) return y == null;
            if (y == null) return false;
            return x.Id == y.Id;
        }

        /// <summary>
        /// Basis of comparison is the ID.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int GetHashCode(HostMonitorMessage obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}
