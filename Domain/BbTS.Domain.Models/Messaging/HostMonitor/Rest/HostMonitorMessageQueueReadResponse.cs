﻿using System;

namespace BbTS.Domain.Models.Messaging.HostMonitor.Rest
{
    /// <summary>
    /// The response for a host monitor msmq read request.
    /// </summary>
    public class HostMonitorMessageQueueReadResponse
    {
        /// <summary>
        /// The prevous count of records before the read operation.
        /// </summary>
        public int PreviousRecordCount { get; set; }

        /// <summary>
        /// The number of records stored in the domain controller.
        /// </summary>
        public int RecordCount { get; set; }

        /// <summary>
        /// The time the read operation was invoked before this time.
        /// </summary>
        public DateTimeOffset PreviousReadTime { get; set; }

        /// <summary>
        /// The time the read operation was invoked.
        /// </summary>
        public DateTimeOffset ReadTime { get; set; }

        /// <summary>
        /// Any message that occured during the read.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Deep copy.
        /// </summary>
        /// <returns></returns>
        public HostMonitorMessageQueueReadResponse Clone()
        {
            return (HostMonitorMessageQueueReadResponse)MemberwiseClone();
        }
    }
}
