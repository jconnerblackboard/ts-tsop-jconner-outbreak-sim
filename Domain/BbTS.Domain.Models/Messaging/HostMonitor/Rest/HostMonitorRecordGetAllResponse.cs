﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Messaging.HostMonitor.Rest
{
    /// <summary>
    /// Container for the response to a host monitor records get all request.
    /// </summary>
    public class HostMonitorRecordGetAllResponse
    {
        /// <summary>
        /// List of records currently stored in the host monitor message processing agent.
        /// </summary>
        public List<HostMonitorMessageServiceRecord> Records { get; set; } = new List<HostMonitorMessageServiceRecord>();
    }
}
