﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Messaging.HostMonitor
{
    /// <summary>
    /// Container class for a callback in the host monitor service.
    /// </summary>
    public class HostMonitorCallbackResponse
    {
        /// <summary>
        /// Unique identifier for the response.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Messages thrown from the callback.
        /// </summary>
        public List<HostMonitorMessage> Messages { get; set; } = new List<HostMonitorMessage>();
    }
}
