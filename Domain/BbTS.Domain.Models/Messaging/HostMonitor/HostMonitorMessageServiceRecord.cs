﻿using System;

namespace BbTS.Domain.Models.Messaging.HostMonitor
{
    /// <summary>
    /// Container class for a record associated to an MSMQ host monitor message.  Used for storage, retrieval and client message management.
    /// </summary>
    public class HostMonitorMessageServiceRecord : MarshalByRefObject
    {
        /// <summary>
        /// Unique identifier for this message.
        /// </summary>
        public string Id { get; set; } = Guid.NewGuid().ToString("D");

        /// <summary>
        /// The date and time (with timezone information) when the message was retrieved from the queue.
        /// </summary>
        public DateTimeOffset RetrievalTime { get; set; } = DateTimeOffset.Now;

        /// <summary>
        /// The host monitor message retrieved from the queue.
        /// </summary>
        public HostMonitorMessage HostMonitorMessage { get; set; }
    }
}
