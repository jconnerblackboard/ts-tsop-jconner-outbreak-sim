﻿using System;
using BbTS.Domain.Models.Definitions.Monitoring;

namespace BbTS.Domain.Models.Messaging.Subscription.Rest
{
    /// <summary>
    /// Container class for the response to a host monitor subscription request.
    /// </summary>
    public class SubscriptionRegistrationRequest
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// Indicates whether or not the first response from the service will include all messages currently in the queue.
        /// </summary>
        public bool RetrieveAllMessagesInQueue { get; set; } = true;

        /// <summary>
        /// The unique identifier (Guid) for the client subscribing to the service.
        /// </summary>
        public string ClientGuid { get; set; }

        /// <summary>
        /// The URI where message callbacks will be sent.
        /// </summary>
        public string CallbackUri { get; set; }

        /// <summary>
        /// The registration action to take as part of this request.
        /// </summary>
        public SubscriptionAction SubscriptionAction { get; set; }

        /// <summary>
        /// The subscription service the client is attached to: 0 - HostMonitor
        /// </summary>
        public SubscriptionServiceType SubscriptionServiceType { get; set; }

        /// <summary>
        /// The date and time the request was received.
        /// </summary>
        public DateTimeOffset RequestReceivedDateTime { get; set; }
    }
}
