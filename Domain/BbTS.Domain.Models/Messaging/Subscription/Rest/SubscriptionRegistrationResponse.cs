﻿using BbTS.Domain.Models.Transaction;

namespace BbTS.Domain.Models.Messaging.Subscription.Rest
{
    /// <summary>
    /// Container class for the response to a host monitor subscription request.
    /// </summary>
    public class SubscriptionRegistrationResponse : ProcessingResult
    {
    }
}
