﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Messaging.SecurityMonitor
{
    /// <summary>
    /// Container class for a request to register a table listener on DA_DoorEventLog.
    /// </summary>
    public class RegisterDoorListForAlertCallbackRequest
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The list of doors to register for callback events.
        /// </summary>
        public List<int> DoorIds { get; set; }
    }
}
