﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Messaging.SecurityMonitor
{
    /// <summary>
    /// Container class for the response to a request to register a table listener on DA_DoorEventLog.
    /// </summary>
    public class RegisterDoorListForAlertCallbackResponse
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public string RequestId { get; set; }
    }
}
