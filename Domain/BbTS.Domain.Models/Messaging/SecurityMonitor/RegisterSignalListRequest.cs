﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;

namespace BbTS.Domain.Models.Messaging.SecurityMonitor
{
    /// <summary>
    /// Container class for a request to register a list of doors for DBMS_ALERT events.
    /// </summary>
    public class RegisterSignalListRequest
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The list of doors to register for callback events.
        /// </summary>
        public int ListId { get; set; } 

        /// <summary>
        /// The username of the agent registering the doors.
        /// </summary>
        public string Username { get; set; }
    }

    /// <summary>
    /// Equality Comparer for RegisterSignalListRequest
    /// </summary>
    public class RegistrationSignalListRequestComparer : IEqualityComparer<RegisterSignalListRequest>
    {
        /// <summary>
        /// Define the parameters that indicate equality.
        /// </summary>
        /// <param name="x">Base</param>
        /// <param name="y">Comparee</param>
        /// <returns>Equality value.</returns>
        public bool Equals(RegisterSignalListRequest x, RegisterSignalListRequest y)
        {
            if (x == null) return y == null;
            if (y == null) return false;

            return 
                x.ListId == y.ListId &&
                string.Equals(x.Username, y.Username, StringComparison.CurrentCultureIgnoreCase);
        }

        /// <summary>
        /// Get the hash code for the object.
        /// </summary>
        /// <param name="obj">the object.</param>
        /// <returns>hash code value.</returns>
        public int GetHashCode(RegisterSignalListRequest obj)
        {
            string hash = string.Empty;

            hash += obj.Username;
            hash += obj.ListId.ToString(CultureInfo.InvariantCulture);

            return hash.GetHashCode();
        }
    }
}
