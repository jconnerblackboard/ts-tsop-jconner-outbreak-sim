﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Messaging.SecurityMonitor
{
    /// <summary>
    /// Container class for the response to a request to register a list of doors for DBMS_ALERT events.
    /// </summary>
    public class RegisterSignalListResponse
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public string RequestId { get; set; }
    }
}
