﻿using BbTS.Domain.Models.DoorAccess;

namespace BbTS.Domain.Models.Messaging.SecurityMonitor
{
    /// <summary>
    /// Container class for the callback response of a security monitor subscription.
    /// </summary>
    public class SecurityMonitorCallbackResponse
    {
        /// <summary>
        /// Unique identifier for the response.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The door state information.
        /// </summary>
        public DoorStatus DoorStatus { get; set; } 
    }
}
