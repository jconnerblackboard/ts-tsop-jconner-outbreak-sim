﻿namespace BbTS.Domain.Models.Messaging.SecurityMonitor
{
    /// <summary>
    /// Container class for the callback response from an DBMS_ALERT event.
    /// </summary>
    public class DbmsCallbackResponse
    {
        /// <summary>
        /// Result of the callback.
        /// </summary>
        public int Result { get; set; }

        /// <summary>
        /// Name of the DBMS_ALERT that triggered the callback.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Message that was provided by the alert (XML Result).
        /// </summary>
        public string Message { get; set; }
    }
}
