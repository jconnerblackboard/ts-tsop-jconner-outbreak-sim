﻿using System;

namespace BbTS.Domain.Models.Messaging.SecurityMonitor
{
    /// <summary>
    /// Container class for the reponse to a request to associate a set of doors with a security monitor subscribed client.
    /// </summary>
    public class DoorClientAssociationtResponse
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public Guid RequestId { get; set; }

        /// <summary>
        /// The error code thrown.  0 = success.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// The result message.
        /// </summary>
        public string Message { get; set; }
    }
}
