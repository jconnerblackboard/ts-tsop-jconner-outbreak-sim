﻿using System;
using System.Collections.Generic;

namespace BbTS.Domain.Models.Messaging.SecurityMonitor
{
    /// <summary>
    /// Container class for a reques to associate a set of doors with a security monitor subscribed client.
    /// </summary>
    public class DoorClientAssociationRequest
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public Guid RequestId { get; set; }

        /// <summary>
        /// The unique identifier for the client.
        /// </summary>
        public Guid ClientId { get; set; }

        /// <summary>
        /// The list of doors to associate.
        /// </summary>
        public List<int> DoorIds { get; set; }
    }
}
