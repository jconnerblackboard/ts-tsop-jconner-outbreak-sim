﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Messaging.SecurityMonitor
{
    /// <summary>
    /// Container class for the door state information in a DBMS_ALERT response for security monitor
    /// </summary>
    [DataContract(Name = "ROW")]
    [XmlRoot("ROW")]
    [Serializable]
    public class DbmsDoorState
    {
        /// <summary>
        /// The row id of the current status of the door.
        /// </summary>
        [DataMember(Name = "DA_DOORCURRENTSTATUS_ROWID")]
        [XmlElement("DA_DOORCURRENTSTATUS_ROWID")]
        public string DaDoorCurrentStatusRowId { get; set; }

        /// <summary>
        /// Is the door in an exit cycle.
        /// </summary>
        [DataMember(Name= "IS_EXITCYCLE")]
        [XmlElement("IS_EXITCYCLE")]
        public string IsExitCycle { get; set; }

        /// <summary>
        /// The control mode of the door.
        /// </summary>
        [DataMember(Name = "DOOR_CONTROLMODE")]
        [XmlElement("DOOR_CONTROLMODE")]
        public int? DoorControlMode { get; set; }

        /// <summary>
        /// Is the door currently open.
        /// </summary>
        [DataMember(Name = "IS_OPEN")]
        [XmlElement("IS_OPEN")]
        public string IsOpen { get; set; }

        /// <summary>
        /// Is the door currently unlocked.
        /// </summary>
        [DataMember(Name = "IS_UNLOCKED")]
        [XmlElement("IS_UNLOCKED")]
        public string IsUnlocked { get; set; }

        /// <summary>
        /// Last date and time the status was updated (julian date format).
        /// </summary>
        [DataMember(Name = "LASTUPDATEDT")]
        [XmlElement("LASTUPDATEDT")]
        public double? LastUpdatedDateTime { get; set; }

        /// <summary>
        /// The override status of the door.
        /// </summary>
        [DataMember(Name = "OVERRIDE_STATUS")]
        [XmlElement("OVERRIDE_STATUS")]
        public int? OverrideStatus { get; set; }

        /// <summary>
        /// Does the door require a pin for entry.
        /// </summary>
        [DataMember(Name = "REQUIRES_PIN")]
        [XmlElement("REQUIRES_PIN")]
        public string RequiresPin { get; set; }

        /// <summary>
        /// The numerical identifier for the door.
        /// </summary>
        [DataMember(Name = "DOOR_ID")]
        [XmlElement("DOOR_ID")]
        public int? DoorId { get; set; }

        /// <summary>
        /// The numerical identifier for the merchant that owns the door.
        /// </summary>
        [DataMember(Name = "MERCHANT_ID")]
        [XmlElement("MERCHANT_ID")]
        public int? MerchantId { get; set; }

        /// <summary>
        /// The name of the door.
        /// </summary>
        [DataMember(Name = "DOORNAME")]
        [XmlElement("DOORNAME")]
        public string DoorName { get; set; }

        /// <summary>
        /// The description of the door.
        /// </summary>
        [DataMember(Name = "DESCRIPTION")]
        [XmlElement("DESCRIPTION")]
        public string Description { get; set; }

        /// <summary>
        /// The name of the building the door is attached to.
        /// </summary>
        [DataMember(Name = "BUILDINGNAME")]
        [XmlElement("BUILDINGNAME")]
        public string BuildingName { get; set; }

        /// <summary>
        /// The name of the area the door is attached to.
        /// </summary>
        [DataMember(Name = "AREANAME")]
        [XmlElement("AREANAME")]
        public string AreaName { get; set; }

        /// <summary>
        /// Computer name.
        /// </summary>
        [DataMember(Name = "COMPUTERNAME")]
        [XmlElement("COMPUTERNAME")]
        public string ComputerName { get; set; }

        /// <summary>
        /// The name of the merchant the door is attached to.
        /// </summary>
        [DataMember(Name = "MERCHANTNAME")]
        [XmlElement("MERCHANTNAME")]
        public string MerchantName { get; set; }

        /// <summary>
        /// The name of the group the door belongs to.
        /// </summary>
        [DataMember(Name = "DOORGROUPNAME")]
        [XmlElement("DOORGROUPNAME")]
        public string DoorGroupName { get; set; }

        /// <summary>
        /// The numerical identifier for the master controller that controls the door.
        /// </summary>
        [DataMember(Name = "MASTERCONTROLLER_ID")]
        [XmlElement("MASTERCONTROLLER_ID")]
        public int? MasterControllerId { get; set; }

        /// <summary>
        /// Computer Id
        /// </summary>
        [DataMember(Name = "COMPUTER_ID")]
        [XmlElement("COMPUTER_ID")]
        public int? ComputerId { get; set; }

        /// <summary>
        /// The address of the door on the master controller.
        /// </summary>
        [DataMember(Name = "DOOR_ADDRESS")]
        [XmlElement("DOOR_ADDRESS")]
        public short? DoorAddress { get; set; }

        /// <summary>
        /// The amount of time the door remains unlocked when an temp unlock command is received.
        /// </summary>
        [DataMember(Name = "DOOR_UNLOCK_TIME_SECONDS")]
        [XmlElement("DOOR_UNLOCK_TIME_SECONDS")]
        public int? DoorUnlockTimeSeconds { get; set; }
    }

    /// <summary>
    /// Container class for a "ROWSET".
    /// </summary>
    [DataContract(Name = "ROWSET")]
    [XmlRoot("ROWSET")]
    public class DbmsDoorStateSet
    {
        /// <summary>
        /// The one and only row in the set.
        /// </summary>
        [XmlElement("ROW")]
        public DbmsDoorState Row { get; set; }
    }
}
