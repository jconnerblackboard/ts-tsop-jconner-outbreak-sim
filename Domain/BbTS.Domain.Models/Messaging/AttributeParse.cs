﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Messaging
{
    /// <summary>
    /// Container class for the parsed parameters from a host monitor attribute decorated web api controller method.
    /// </summary>
    public class AttributeParse
    {
        /// <summary>
        /// The remote ip address of the client.
        /// </summary>
        public string ClientIp { get; set; }

        /// <summary>
        /// The endpoint that was called.
        /// </summary>
        public string Endpoint { get; set; }

        /// <summary>
        /// Method arguments and return value.
        /// </summary>
        public List<object> Parameters { get; set; } = new List<object>();
    }
}
