﻿using System;

namespace BbTS.Domain.Models.Messaging
{
    /// <summary>
    /// Container class for a host monitor message.
    /// </summary>
    public class HostMonitorMessage
    {
        /// <summary>
        /// The unique identifier for this message.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The host type that threw this message.
        /// </summary>
        public string HostType { get; set; }

        /// <summary>
        /// The source of the message.
        /// </summary>
        public string MessageSource { get; set; }

        /// <summary>
        /// The identifier for the device.
        /// </summary>
        public string DeviceIdentifier { get; set; }

        /// <summary>
        /// The date, time and timezone information when the message was thrown.
        /// </summary>
        public DateTimeOffset DateTimeOffset { get; set; }

        /// <summary>
        /// A card number associated with the message.
        /// </summary>
        public string CardNumber { get; set; }

        /// <summary>
        /// The transaction number associated with the message.
        /// </summary>
        public string TransactionNumber { get; set; }

        /// <summary>
        /// The amount of the transaction if applicable.
        /// </summary>
        public string Amount { get; set; }

        /// <summary>
        /// Any relevant text associated with the message.
        /// </summary>
        public string Message { get; set; }
    }
}
