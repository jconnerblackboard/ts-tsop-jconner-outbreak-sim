﻿using BbTS.Domain.Models.BoardPlan;
using System.Collections.Generic;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Customer board exclusion get response container
    /// </summary>
    public class CustomerBoardExclusionsGetResponse
    {
        /// <summary>
        /// Board plan exclusions
        /// </summary>
        public List<BoardPlanExclusion> BoardPlanExclusions { get; set; }
    }
}
