﻿using System;
using System.Collections.Generic;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Container class for handling a get customer transaction request.
    /// </summary>
    public class CustomerTransactionHistoryGetRequest
    {
        /// <summary>
        /// Customer guid
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Board plan guid
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Formats properties as optional uri params
        /// </summary>
        /// <returns></returns>
        public string FormUriParams()
        {
            var list = new List<string>();

            if (StartDate.HasValue)
                list.Add($"{nameof(StartDate)}={Uri.EscapeUriString(StartDate.ToString())}");
            if (EndDate.HasValue)
                list.Add($"{nameof(EndDate)}={Uri.EscapeUriString(EndDate.ToString())}");

            return string.Join("&", list);
        }
    }
}
