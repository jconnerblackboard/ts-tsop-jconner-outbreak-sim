﻿using System;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// This object represents a Transact Customer_Def_Field_Def object. Customer Defined Field Definitions Table
    /// </summary>
    [Serializable]
    public class TsCustomer_Def_Field_Def
    {
        /// <summary>
        /// Unique Identifier
        /// </summary>
        [XmlAttribute]
        public int Customer_Def_Field_Def_Id { get; set; }
        /// <summary>
        /// Title of Definition
        /// </summary>
        [XmlAttribute]
        public string Title { get; set; }
        /// <summary>
        /// 0 - String   1 - Numeric  2 - Dollar (store '$1.25' as '1250')  3 - Text (Memo)  4 - StringList (stored as CR delimited)  5 - Boolean (stored as T/F)
        /// </summary>
        [XmlAttribute]
        public int FieldType { get; set; }
        /// <summary>
        /// Maximum Length
        /// </summary>
        [XmlAttribute]
        public int MaxLength { get; set; }
        /// <summary>
        /// Indicates if the transaction will store the value of this record (T/F)
        /// </summary>
        [XmlAttribute]
        public string Store_Per_Transaction { get; set; }
        /// <summary>
        /// User Interface Position for Top Display
        /// </summary>
        [XmlAttribute]
        public int Ui_Display_Top { get; set; }
        /// <summary>
        /// User Interface Position for Left Display
        /// </summary>
        [XmlAttribute]
        public int Ui_Display_Left { get; set; }
        /// <summary>
        /// User Interface Display Height
        /// </summary>
        [XmlAttribute]
        public int Ui_Display_Height { get; set; }
        /// <summary>
        /// User Interface Display Width
        /// </summary>
        [XmlAttribute]
        public int Ui_Display_Width { get; set; }
        /// <summary>
        /// Whether this CDF will appear as report selection criteria.
        /// </summary>
        [XmlAttribute]
        public string IsReportEnabled { get; set; }
        /// <summary>
        /// Guid when used for reporting purposes.
        /// </summary>
        [XmlAttribute]
        public string ReportTemplateFieldGuid { get; set; }
        /// <summary>
        /// Guid when used in reporting for aggregation purposes.
        /// </summary>
        [XmlAttribute]
        public string ReportTemplateFieldAggrGuid { get; set; }
        /// <summary>
        /// Whether this column is indexed.
        /// </summary>
        [XmlAttribute]
        public string IsIndexed { get; set; }
    }
}