﻿using System;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// This object represents a record in the Transact system for the customer transaction
    /// </summary>
    public class TsCustomerTransaction
    {
        /// <summary>
        /// Transaction number
        /// </summary>
        public int TransactionNumber { get; set; }

        /// <summary>
        /// Transaction key
        /// </summary>
        public string TransactionKey { get; set; }

        /// <summary>
        /// Transaction viewer
        /// </summary>
        public string TransactionViewer { get; set; }

        /// <summary>
        /// Photo
        /// </summary>
        public bool IsDenied { get; set; }

        /// <summary>
        /// Transaction date time
        /// </summary>
        public DateTime TransactionDateTime { get; set; }

        /// <summary>
        /// Profit center where transaction was made
        /// </summary>
        public string ProfitCenterName { get; set; }

        /// <summary>
        /// Pos where transaction was made
        /// </summary>
        public string PosName { get; set; }

        /// <summary>
        /// Transaction type
        /// </summary>
        public string TransactionType { get; set; }

        /// <summary>
        /// Plan account
        /// </summary>
        public string PlanAccount { get; set; }

        /// <summary>
        /// Amount
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Is online
        /// </summary>
        public bool IsOnline { get; set; }
    }
}
