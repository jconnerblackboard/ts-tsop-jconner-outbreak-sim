﻿using BbTS.Domain.Models.Definitions.Door;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Customer Door Access
    /// </summary>
    public class CustomerDaStateTimeSchedule
    {
        /// <summary>
        /// Time of day (in minutes)
        /// </summary>
        public int StartingMinute { get; set; }

        /// <summary>
        /// State
        /// </summary>
        public DoorControlMode State { get; set; }
    }
}
