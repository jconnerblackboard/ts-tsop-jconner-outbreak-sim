﻿using System;
using BbTS.Domain.Models.Definitions.Card;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Container class for handling a server response to a get customer guid from card number request.
    /// </summary>
    public class CustomerGuidFromCardNumberResponse
    {
        /// <summary>
        /// A unique alternate key for this customer
        /// </summary>
        public string CustomerGuid { get; set; }

        /// <summary>
        /// Card Issue Number
        /// </summary>
        public string IssueNumber { get; set; }

        /// <summary>
        /// Whether the card is lost or not
        /// </summary>
        public bool CardIsLost { get; set; }
        
        /// <summary>
        /// (0) - Active (1) - Frozen  (2) - Retired (cannot be changed to active without utility) - (3) Customer Deleted
        /// </summary>
        public CardStatusType CardStatus { get; set; }
        
        /// <summary>
        /// The first date the card is considered active
        /// </summary>
        public DateTime? CardActiveStartDate { get; set; }
        
        /// <summary>
        /// The last date the card is considered active
        /// </summary>
        public DateTime? CardActiveEndDate { get; set; }
        
        /// <summary>
        /// Whether the customer is active or not
        /// </summary>
        public bool CustomerIsActive { get; set; }
        
        /// <summary>
        /// The first date the customer account is considered active
        /// </summary>
        public DateTime ? CustomerActiveStartDate { get; set; }
        
        /// <summary>
        /// The last date the customer account is considered active
        /// </summary>
        public DateTime ? CustomerActiveEndDate { get; set; }

        /// <summary>
        /// System PIN for customer.
        /// </summary>
        public int? PinNumber { get; set; }
    }
}
