﻿using System;
using System.Collections.Generic;
using BbTS.Domain.Models.Definitions.ArtsDataModel;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// A ValidateSystemPolicyResponse object.
    /// </summary>
    public class ValidateSystemPolicyResponse
    {
        /// <summary>
        /// Customer GUID.
        /// </summary>
        public Guid CustomerGuid { get; set; }

        /// <summary>
        /// Available balance for the customer.
        /// </summary>
        public decimal AvailableBalance { get; set; }

        /// <summary>
        /// Specifies if tax should be charged or not.
        /// </summary>
        public bool TaxExempt { get; set; }

        /// <summary>
        /// Discount/Surcharge rules for the customer.
        /// </summary>
        public List<DiscountSurchargeRule> DiscountSurchargeRules { get; set; }

        /// <summary>
        /// Detailed log of the policy evaluation that generated this response.
        /// </summary>
        public List<string> EvaluationLog { get; set; }
    }

    /// <summary>
    /// A DiscountSurchargeRule object.
    /// </summary>
    public class DiscountSurchargeRule
    {
        /// <summary>
        /// The type of rule (0 = discount, 1 = surcharge)
        /// </summary>
        public DiscountSurchargeType Type { get; set; }

        /// <summary>
        /// The discount or surcharge rate.
        /// </summary>
        public decimal Rate { get; set; }
    }
}
