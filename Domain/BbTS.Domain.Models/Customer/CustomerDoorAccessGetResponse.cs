﻿using System;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Customer Door Access Get response container
    /// </summary>
    public class CustomerDoorAccessGetResponse
    {
        /// <summary>
        /// Customer Id
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Door Id
        /// </summary>
        public int DoorId { get; set; }

        /// <summary>
        /// Date
        /// </summary>
        public DateTime? Date { get; set; }

        /// <summary>
        /// Customer door access
        /// </summary>
        public CustomerDoorAccess DoorAccess { get; set; }
    }
}