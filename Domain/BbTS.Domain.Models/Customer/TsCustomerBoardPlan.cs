﻿using System;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Customer board plan
    /// </summary>
    public class TsCustomerBoardPlan : TsCustomerBoardPlanPost
    {
        /// <summary>
        /// Board plan name
        /// </summary>
        public string BoardPlanName { get; set; }

        /// <summary>
        /// Plan active
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Week used
        /// </summary>
        public int WeekUsed { get; set; }

        /// <summary>
        /// Month used
        /// </summary>
        public int MonthUsed { get; set; }

        /// <summary>
        /// Semester used
        /// </summary>
        public int SemesterUsed { get; set; }

        /// <summary>
        /// Year used
        /// </summary>
        public int YearUsed { get; set; }

        /// <summary>
        /// Week allowed
        /// </summary>
        public int? WeekAllowed { get; set; }

        /// <summary>
        /// Month allowed
        /// </summary>
        public int? MonthAllowed { get; set; }

        /// <summary>
        /// Semester allowed
        /// </summary>
        public int? SemesterAllowed { get; set; }

        /// <summary>
        /// Year allowed
        /// </summary>
        public int? YearAllowed { get; set; }

        /// <summary>
        /// Active from
        /// </summary>
        public DateTime? ActiveFrom { get; set; }

        /// <summary>
        /// Active to
        /// </summary>
        public DateTime? ActiveTo { get; set; }

        /// <summary>
        /// Is assigned to customer
        /// </summary>
        public bool IsAssigned { get; set; }

        /// <summary>
        /// Is plan removed (but has used records) from customer
        /// </summary>
        public bool IsRemoved { get; set; }
    }
}
