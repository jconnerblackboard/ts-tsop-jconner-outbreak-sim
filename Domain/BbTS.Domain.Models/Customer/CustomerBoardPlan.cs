﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Container class for a customer board balance.
    /// </summary>
    public class CustomerBoardPlan
    {
        /// <summary>
        /// The unique (int) identifier for the board plan.
        /// </summary>
        public int BoardPlanId { get; set; }

        /// <summary>
        /// The name of the board plan.
        /// </summary>
        public string BoardPlanName { get; set; }

        /// <summary>
        /// The priority of the board plan.
        /// </summary>
        public int BoardPlanPriority { get; set; }

        /// <summary>
        /// Indicates whether or not the board plan is active.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Count of regular grid meals left.
        /// </summary>
        public int RegularGridLeft { get; set; }

        /// <summary>
        /// Count of guest grid meals left.
        /// </summary>
        public int GuestGridLeft { get; set; }

        /// <summary>
        /// Count of currend period meals left.
        /// </summary>
        public int PeriodLeft { get; set; }

        /// <summary>
        /// Count of meals left today.
        /// </summary>
        public int DayLeft { get; set; }

        /// <summary>
        /// Count of meals this week.
        /// </summary>
        public int WeekLeft { get; set; }

        /// <summary>
        /// Count of meals left this month.
        /// </summary>
        public int MonthLeft { get; set; }

        /// <summary>
        /// Count of meals left this semester.
        /// </summary>
        public int SemesterQuarterLeft { get; set; }

        /// <summary>
        /// Count of meals left this year.
        /// </summary>
        public int YearLeft { get; set; }

        /// <summary>
        /// Total guest meals left.
        /// </summary>
        public int GuestTotalLeft { get; set; }

        /// <summary>
        /// Count of extra meals left.
        /// </summary>
        public int ExtraLeft { get; set; }

        /// <summary>
        /// Count of meals left for transfer.
        /// </summary>
        public int TransferLeft { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string TransferMealsBy { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string GuestReset { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ExtraReset { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string LimitPeriodBy { get; set; }
    }
}
