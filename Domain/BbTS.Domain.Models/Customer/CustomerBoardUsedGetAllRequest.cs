﻿using System.Text;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Container class for handling a get all customer board used request.
    /// </summary>
    public class CustomerBoardUsedGetAllRequest
    {
        /// <summary>
        /// Customer number
        /// </summary>
        public string CustomerGuid { get; set; }

        /// <summary>
        /// Board number
        /// </summary>
        public string BoardPlanGuid { get; set; }

        /// <summary>
        /// Formats properties as optional uri params
        /// </summary>
        /// <returns></returns>
        public string FormUri()
        {
            var sb = new StringBuilder("customer");

            if (CustomerGuid != null)
                sb.Append($"/{CustomerGuid}");

            sb.Append("/board");
            if (BoardPlanGuid != null)
                sb.Append($"/{BoardPlanGuid}");

            return sb.ToString();
        }
    }
}
