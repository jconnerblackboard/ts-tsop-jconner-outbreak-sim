﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Customer board plans get response
    /// </summary>
    public class CustomerBoardPlansGetResponse
    {
        /// <summary>
        /// Customer board plans
        /// </summary>
        public List<TsCustomerBoardPlan> CustomerBoardPlans { get; set; }
    }
}
