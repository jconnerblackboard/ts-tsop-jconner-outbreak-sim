﻿using System;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Data needed to evaluate policy rules for a customer.
    /// </summary>
    public class PolicyEvaluationData
    {
        /// <summary>
        /// Customer GUID.
        /// </summary>
        public Guid CustomerGuid { get; set; }

        /// <summary>
        /// TenderId of the Stored Value tender being used.
        /// </summary>
        public int TenderId { get; set; }

        /// <summary>
        /// Originator GUID.
        /// </summary>
        public Guid OriginatorId { get; set; }

        /// <summary>
        /// The evaluation date/time.  (This equals the TimestampType.Start entry in the transaction's TransactionTimestamps list).
        /// </summary>
        public DateTimeOffset EvaluationDateTime { get; set; }
    }
}