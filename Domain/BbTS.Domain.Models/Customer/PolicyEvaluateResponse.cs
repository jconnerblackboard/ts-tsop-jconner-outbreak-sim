﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// A PolicyEvaluateResponse object.  (Version 1)
    /// </summary>
    public class PolicyEvaluateResponseV01
    {
        /// <summary>
        /// The unique identifier for the request that preempted the processing routine.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// The error code associated with the response.  0 means success.
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Reason for failure (if there was a failure error code).
        /// </summary>
        public string DeniedText { get; set; }

        /// <summary>
        /// Customer GUID.
        /// </summary>
        public Guid CustomerGuid { get; set; }

        /// <summary>
        /// Available balance for the customer.
        /// </summary>
        public decimal AvailableBalance { get; set; }

        /// <summary>
        /// Specifies if tax should be charged or not.
        /// </summary>
        public bool TaxExempt { get; set; }

        /// <summary>
        /// Discount/Surcharge rules for the customer.
        /// </summary>
        public List<DiscountSurchargeRuleViewV01> DiscountSurchargeRules { get; set; }

        /// <summary>
        /// Detailed log of the policy evaluation that generated this response.
        /// </summary>
        public List<string> EvaluationLog { get; set; }
    }

    /// <summary>
    /// Extension methods to extract values from the response.
    /// </summary>
    public static class PolicyEvaluateResponseConverter
    {
        #region Version 1
        public static PolicyEvaluationResult PolicyEvaluationResultExtract(this PolicyEvaluateResponseV01 response)
        {
            if (response == null) return null;

            return new PolicyEvaluationResult
            {
                RequestId = response.RequestId,
                ErrorCode = response.ErrorCode,
                DeniedText = response.DeniedText,
                CustomerGuid = response.CustomerGuid,
                AvailableBalance = response.AvailableBalance,
                TaxExempt = response.TaxExempt,
                DiscountSurchargeRules = response.DiscountSurchargeRules.Select(ruleView => ruleView.ToDiscountSurchargeRule()).ToList(),
                EvaluationLog = response.EvaluationLog
            };
        }
        #endregion
    }
}
