﻿using System;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// A PolicyEvaluateRequest object.  (Version 1)
    /// </summary>
    public class PolicyEvaluateRequestV01
    {
        /// <summary>
        /// Unique ID assigned to the request.
        /// </summary>
        public Guid RequestId { get; set; }

        /// <summary>
        /// Customer GUID.
        /// </summary>
        public Guid CustomerGuid { get; set; }

        /// <summary>
        /// TenderId of the Stored Value tender being used.
        /// </summary>
        public int TenderId { get; set; }

        /// <summary>
        /// Originator GUID.
        /// </summary>
        public Guid OriginatorId { get; set; }

        /// <summary>
        /// The evaluation date/time.  (This equals the TimestampType.Start entry in the transaction's TransactionTimestamps list).
        /// </summary>
        public DateTimeOffset EvaluationDateTime { get; set; }

        /// <summary>
        /// True if the evaluation log (used in debugging) should be sent in the response.
        /// </summary>
        public bool IncludeEvaluationLog { get; set; }
    }

    /// <summary>
    /// Extension methods to create policy evaluate requests.
    /// </summary>
    public static class PolicyEvaluateRequestConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="PolicyEvaluateRequestV01"/> object based on this <see cref="PolicyEvaluationData"/>.
        /// </summary>
        /// <param name="evaluationData"></param>
        /// <param name="includeEvaluationLog"></param>
        /// <returns></returns>
        public static PolicyEvaluateRequestV01 NewPolicyEvaluateRequestV01(this PolicyEvaluationData evaluationData, bool includeEvaluationLog)
        {
            return new PolicyEvaluateRequestV01
            {
                RequestId = Guid.NewGuid(),
                CustomerGuid = evaluationData.CustomerGuid,
                TenderId = evaluationData.TenderId,
                OriginatorId = evaluationData.OriginatorId,
                EvaluationDateTime = evaluationData.EvaluationDateTime,
                IncludeEvaluationLog = includeEvaluationLog
            };
        }

        /// <summary>
        /// Returns policy evaluation data contained within the request.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static PolicyEvaluationData PolicyEvaluationDataExtract(this PolicyEvaluateRequestV01 request)
        {
            if (request == null) return null;

            return new PolicyEvaluationData
            {
                CustomerGuid = request.CustomerGuid,
                TenderId = request.TenderId,
                OriginatorId = request.OriginatorId,
                EvaluationDateTime = request.EvaluationDateTime
            };
        }
        #endregion
    }
}
