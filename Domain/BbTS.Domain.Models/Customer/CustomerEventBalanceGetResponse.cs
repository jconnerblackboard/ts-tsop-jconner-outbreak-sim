﻿using System.Collections.Generic;


namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Container class for a response to a CustomerEventBalanceGet request.
    /// </summary>
    public class CustomerEventBalanceGetResponse
    {
        /// <summary>
        /// The unique (Guid) identifier for the customer.
        /// </summary>
        public string CustomerGuid { get; internal set; }

        /// <summary>
        /// The unique identifier for the device.
        /// </summary>
        public string DeviceGuid { get; internal set; }

        /// <summary>
        /// List of events allowed and their balances assigned to the customer.
        /// </summary>
        public List<CustomerEventBalance> Events { get; set; } = new List<CustomerEventBalance>();

        /// <summary>
        /// Parameterized constructor requiring the customerGuid used in the request.
        /// </summary>
        /// <param name="customerGuid">The unique (Guid) identifier for the customer.</param>
        /// <param name="deviceGuid">The unique (Guid) identifier for the device.</param>
        public CustomerEventBalanceGetResponse(string customerGuid, string deviceGuid)
        {
            CustomerGuid = customerGuid;
            DeviceGuid = deviceGuid;
        }
    }

}