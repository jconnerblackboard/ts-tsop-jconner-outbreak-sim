﻿using System;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Customer board exclusion post response container
    /// </summary>
    public class CustomerOperationResult
    {
        /// <summary>
        /// Request Id
        /// </summary>
        public Guid RequestId { get; set; }

        /// <summary>
        /// Determines if request was successful
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Error message
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}
