﻿
namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Customer post email object
    /// </summary>
    public class TsCustomerPostEmail
    {
        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Default display override
        /// </summary>
        public bool DefaultDisplayOverride { get; set; }

        /// <summary>
        /// Email type
        /// </summary>
        public int TypeId { get; set; }
    }
}
