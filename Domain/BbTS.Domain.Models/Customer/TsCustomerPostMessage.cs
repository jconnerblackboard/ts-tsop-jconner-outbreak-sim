﻿
namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Customer pos message
    /// </summary>
    public class TsCustomerPostMessage
    {
        /// <summary>
        /// Message Id
        /// </summary>
        public int MessageId { get; set; }

        /// <summary>
        /// Start date
        /// </summary>
        public double? StartDate { get; set; }

        /// <summary>
        /// Start date
        /// </summary>
        public double? EndDate { get; set; }

        /// <summary>
        /// Max display count
        /// </summary>
        public int MaxDisplayCount { get; set; }

        /// <summary>
        /// Message Text
        /// </summary>
        public string MessageText { get; set; }
    }
}
