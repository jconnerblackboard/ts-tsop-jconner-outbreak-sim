﻿using System;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// This object represents a record in the Transact system for a door access plan associated to a customer
    /// </summary>
    public class TsCustomerDoorAccessPlan
    {
        /// <summary>
        /// The PK of the customer object
        /// </summary>
        public int CustomerId { get; set; }
        /// <summary>
        /// The identity of the access plan
        /// </summary>
        public Int32 AccessPlanId { get; set; }
    }
}
