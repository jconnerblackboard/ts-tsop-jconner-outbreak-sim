﻿namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Container class for a customer's event balance.
    /// </summary>
    public class CustomerEventBalance
    {
        /// <summary>
        /// Id of this event
        /// </summary>
        public int EventId { get; set; }
        /// <summary>
        /// Number of this event
        /// </summary>
        public int EventNumber { get; set; }
        /// <summary>
        /// Name of this event
        /// </summary>
        public string EventName { get; set; }
        /// <summary>
        /// Regular Accesses allowed to this event
        /// </summary>
        public int RegularAllowed { get; set; }
        /// <summary>
        /// Guest Accesses allowed to this event
        /// </summary>
        public int GuestAllowed { get; set; }
        /// <summary>
        /// Regular Accesses remaining for this event
        /// </summary>
        public int RegularRemaining { get; set; }
        /// <summary>
        /// Guest Accesses remaining for this event
        /// </summary>
        public int GuestRemaining { get; set; }
        /// <summary>
        /// Use limit type, Limit by Year(Y), Semester(S), Month(M), Week(W), Day(D)
        /// </summary>
        public string LimitBy { get; set; }
    }
}
