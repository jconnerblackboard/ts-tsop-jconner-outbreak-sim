﻿using System;
using System.Collections.Generic;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Container class representing customer information related to credential verify
    /// </summary>
    public class CredentialVerifyCustomer
    {
        /// <summary>
        /// The unique identifier for the customer (GUID).
        /// </summary>
        public string CustomerGuid { get; set; }

        /// <summary>
        /// The first name of the customer.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// The last name of the customer.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// The birthdate of the customer.
        /// </summary>
        public DateTime? Birthdate { get; set; }

        /// <summary>
        /// The email address(es) of the customer.
        /// </summary>
        public List<string> EmailAddresses { get; set; } = new List<string>();

        /// <summary>
        /// A small visual representation of the image of the customer.
        /// </summary>
        public CustomerImage Thumbnail { get; set; } = new CustomerImage();

        /// <summary>
        /// Pos waiting message
        /// </summary>
        public bool PosMessageWaiting { get; set; }

        /// <summary>
        /// Show Attended Account Balance
        /// </summary>
        public bool AttendedShowAccountBalance { get; set; }

        /// <summary>
        /// Print Attended Account Balance
        /// </summary>
        public bool AttendedPrintAccountBalance { get; set; }

        /// <summary>
        /// Show Unattended Account Balance
        /// </summary>
        public bool UnattendedShowAccountBalance { get; set; }

        /// <summary>
        /// Print Unattended Account Balance
        /// </summary>
        public bool UnattendedPrintAccountBalance { get; set; }

        /// <summary>
        /// Show Birthday
        /// </summary>
        public bool ShowBirthday { get; set; }

        /// <summary>
        /// Show Name
        /// </summary>
        public bool ShowName { get; set; }

        /// <summary>
        /// Print Name
        /// </summary>
        public bool PrintName { get; set; }

        /// <summary>
        /// Print customer number
        /// </summary>
        public bool PrintCustomerNumber { get; set; }

        /// <summary>
        /// Print card number
        /// </summary>
        public bool PrintCardNumber { get; set; }
    }

    /// <summary>
    /// View for a <see cref="CredentialVerifyCustomer"/>.  (Version 1)
    /// </summary>
    public class CredentialVerifyCustomerViewV01
    {
        /// <summary>
        /// The unique identifier for the customer (GUID).
        /// </summary>
        public string CustomerGuid { get; set; }

        /// <summary>
        /// The first name of the customer.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// The last name of the customer.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// The birthdate of the customer.
        /// </summary>
        public DateTime? Birthdate { get; set; }

        /// <summary>
        /// The email address(es) of the customer.
        /// </summary>
        public List<string> EmailAddresses { get; set; } = new List<string>();

        /// <summary>
        /// A small visual representation of the image of the customer.
        /// </summary>
        public CustomerImageViewV01 Thumbnail { get; set; } = new CustomerImageViewV01();

        /// <summary>
        /// Pos waiting message
        /// </summary>
        public bool PosMessageWaiting { get; set; }

        /// <summary>
        /// Show Attended Account Balance
        /// </summary>
        public bool AttendedShowAccountBalance { get; set; }

        /// <summary>
        /// Print Attended Account Balance
        /// </summary>
        public bool AttendedPrintAccountBalance { get; set; }

        /// <summary>
        /// Show Unattended Account Balance
        /// </summary>
        public bool UnattendedShowAccountBalance { get; set; }

        /// <summary>
        /// Print Unattended Account Balance
        /// </summary>
        public bool UnattendedPrintAccountBalance { get; set; }

        /// <summary>
        /// Show Birthday
        /// </summary>
        public bool ShowBirthday { get; set; }

        /// <summary>
        /// Show Name
        /// </summary>
        public bool ShowName { get; set; }

        /// <summary>
        /// Print Name
        /// </summary>
        public bool PrintName { get; set; }

        /// <summary>
        /// Print customer number
        /// </summary>
        public bool PrintCustomerNumber { get; set; }

        /// <summary>
        /// Print card number
        /// </summary>
        public bool PrintCardNumber { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="CredentialVerifyCustomer"/> conversion.
    /// </summary>
    public static class CredentialVerifyCustomerConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="CredentialVerifyCustomerViewV01"/> object based on this <see cref="CredentialVerifyCustomer"/>.
        /// </summary>
        /// <param name="credentialVerifyCustomer"></param>
        /// <returns></returns>
        public static CredentialVerifyCustomerViewV01 ToCredentialVerifyCustomerViewV01(this CredentialVerifyCustomer credentialVerifyCustomer)
        {
            if (credentialVerifyCustomer == null) return null;

            return new CredentialVerifyCustomerViewV01
            {
                CustomerGuid = credentialVerifyCustomer.CustomerGuid,
                FirstName = credentialVerifyCustomer.FirstName,
                LastName = credentialVerifyCustomer.LastName,
                Birthdate = credentialVerifyCustomer.Birthdate,
                EmailAddresses = credentialVerifyCustomer.EmailAddresses,
                Thumbnail = credentialVerifyCustomer.Thumbnail.ToCustomerImageViewV01(),
                PosMessageWaiting = credentialVerifyCustomer.PosMessageWaiting,
                AttendedShowAccountBalance = credentialVerifyCustomer.AttendedShowAccountBalance,
                AttendedPrintAccountBalance = credentialVerifyCustomer.AttendedPrintAccountBalance,
                UnattendedShowAccountBalance = credentialVerifyCustomer.UnattendedShowAccountBalance,
                UnattendedPrintAccountBalance = credentialVerifyCustomer.UnattendedPrintAccountBalance,
                ShowBirthday = credentialVerifyCustomer.ShowBirthday,
                ShowName = credentialVerifyCustomer.ShowName,
                PrintName = credentialVerifyCustomer.PrintName,
                PrintCustomerNumber = credentialVerifyCustomer.PrintCustomerNumber,
                PrintCardNumber = credentialVerifyCustomer.PrintCardNumber
            };
        }

        /// <summary>
        /// Returns a <see cref="CredentialVerifyCustomer"/> object based on this <see cref="CredentialVerifyCustomerViewV01"/>.
        /// </summary>
        /// <param name="credentialVerifyCustomerViewV01"></param>
        /// <returns></returns>
        public static CredentialVerifyCustomer ToCredentialVerifyCustomer(this CredentialVerifyCustomerViewV01 credentialVerifyCustomerViewV01)
        {
            if (credentialVerifyCustomerViewV01 == null) return null;

            return new CredentialVerifyCustomer
            {
                CustomerGuid = credentialVerifyCustomerViewV01.CustomerGuid,
                FirstName = credentialVerifyCustomerViewV01.FirstName,
                LastName = credentialVerifyCustomerViewV01.LastName,
                Birthdate = credentialVerifyCustomerViewV01.Birthdate,
                EmailAddresses = credentialVerifyCustomerViewV01.EmailAddresses,
                Thumbnail = credentialVerifyCustomerViewV01.Thumbnail.ToCustomerImage(),
                PosMessageWaiting = credentialVerifyCustomerViewV01.PosMessageWaiting,
                AttendedShowAccountBalance = credentialVerifyCustomerViewV01.AttendedShowAccountBalance,
                AttendedPrintAccountBalance = credentialVerifyCustomerViewV01.AttendedPrintAccountBalance,
                UnattendedShowAccountBalance = credentialVerifyCustomerViewV01.UnattendedShowAccountBalance,
                UnattendedPrintAccountBalance = credentialVerifyCustomerViewV01.UnattendedPrintAccountBalance,
                ShowBirthday = credentialVerifyCustomerViewV01.ShowBirthday,
                ShowName = credentialVerifyCustomerViewV01.ShowName,
                PrintName = credentialVerifyCustomerViewV01.PrintName,
                PrintCustomerNumber = credentialVerifyCustomerViewV01.PrintCustomerNumber,
                PrintCardNumber = credentialVerifyCustomerViewV01.PrintCardNumber
            };
        }
        #endregion
    }
}
