﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Container class for handling a response to a get all customer board used request.
    /// </summary>
    public class CustomerBoardUsedGetAllResponse
    {
        /// <summary>
        /// Original request
        /// </summary>
        public CustomerBoardUsedGetAllRequest Request { get; set; }

        /// <summary>
        /// Customer board details used
        /// </summary>
        public List<TsCustomerBoardDetailUsed> CustomerBoardsUsed { get; set; }
    }
}
