﻿
namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    ///     The address
    /// </summary>
    public class TsAddressType
    {
        /// <summary>
        /// Unique identifier
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the relation ship.
        /// </summary>
        public bool DeriveName { get; set; }

        /// <summary>
        /// Gets or sets the relation ship.
        /// </summary>
        public bool DeriveRelationship { get; set; }
    }
}
