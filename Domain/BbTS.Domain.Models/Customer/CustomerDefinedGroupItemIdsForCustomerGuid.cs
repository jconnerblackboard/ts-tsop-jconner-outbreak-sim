﻿using System;
using System.Collections.Generic;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Holds a list of customer defined group item IDs associated with a customer.
    /// </summary>
    public class CustomerDefinedGroupItemIdsForCustomerGuid
    {
        /// <summary>
        /// Customer GUID.
        /// </summary>
        public Guid CustomerId { get; set; }

        /// <summary>
        /// List of CUSTOMER_DEF_GRP_DEF_ITEM_ID values that are both assigned to the customer and are configured in at least one CustomerInGroupCondition in the policy tree.
        /// </summary>
        public List<int> CustomerDefinedGroupDefinitionItemIdList { get; set; }
    }
}
