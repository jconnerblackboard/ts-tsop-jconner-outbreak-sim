﻿using System;
using System.Collections.Generic;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// This object represents a record in the Transact system for the customer default object
    /// </summary>
    public class TsCustomerDefault
    {
        /// <summary>
        /// When creating a new customer, use this as the default id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Default IsActive status
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// The date/time for the default start date
        /// </summary>
        public DateTime ActiveStartDate { get; set; }

        /// <summary>
        /// The date/time for the default end date
        /// </summary>
        public DateTime ActiveEndDate { get; set; }

        /// <summary>
        /// Whether to add a card number with a value matching the customer number
        /// </summary>
        public bool AddCardNumberEqualToNewCustomerNumber { get; set; }

        /// <summary>
        /// The default issue number for the card when it is created
        /// </summary>
        public string DefaultIssueNumber { get; set; }

        /// <summary>
        /// Gets or sets the default stored value account types.
        /// </summary>
        public List<int> DefaultStoredValueAccountTypes { get; set; }
    }
}
