﻿namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    ///     Customer Defined Group Value Model
    /// </summary>
    public class TsCustomerDefinedGroupValue
    {
        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        /// <value>
        /// The position.
        /// </value>
        public int Position { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public string Value { get; set; }
    }
}