﻿using BbTS.Domain.Models.Definitions.ArtsDataModel;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// A DiscountSurchargeRule object.
    /// </summary>
    public class DiscountSurchargeRule
    {
        /// <summary>
        /// The type of rule (0 = discount, 1 = surcharge)
        /// </summary>
        public DiscountSurchargeType Type { get; set; }

        /// <summary>
        /// The discount or surcharge rate.
        /// </summary>
        public decimal Rate { get; set; }
    }

    /// <summary>
    /// View for a <see cref="DiscountSurchargeRule"/>.  (Version 1)
    /// </summary>
    public class DiscountSurchargeRuleViewV01
    {
        /// <summary>
        /// The type of rule (0 = discount, 1 = surcharge)
        /// </summary>
        public DiscountSurchargeType Type { get; set; }

        /// <summary>
        /// The discount or surcharge rate.
        /// </summary>
        public decimal Rate { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="DiscountSurchargeRule"/> conversion.
    /// </summary>
    public static class DiscountSurchargeRuleConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="DiscountSurchargeRuleViewV01"/> object based on this <see cref="DiscountSurchargeRule"/>.
        /// </summary>
        /// <param name="discountSurchargeRule"></param>
        /// <returns></returns>
        public static DiscountSurchargeRuleViewV01 ToDiscountSurchargeRuleViewV01(this DiscountSurchargeRule discountSurchargeRule)
        {
            if (discountSurchargeRule == null) return null;

            return new DiscountSurchargeRuleViewV01
            {
                Type = discountSurchargeRule.Type,
                Rate = discountSurchargeRule.Rate
            };
        }

        /// <summary>
        /// Returns a <see cref="DiscountSurchargeRule"/> object based on this <see cref="DiscountSurchargeRuleViewV01"/>.
        /// </summary>
        /// <param name="discountSurchargeRuleViewV01"></param>
        /// <returns></returns>
        public static DiscountSurchargeRule ToDiscountSurchargeRule(this DiscountSurchargeRuleViewV01 discountSurchargeRuleViewV01)
        {
            if (discountSurchargeRuleViewV01 == null) return null;

            return new DiscountSurchargeRule
            {
                Type = discountSurchargeRuleViewV01.Type,
                Rate = discountSurchargeRuleViewV01.Rate
            };
        }
        #endregion
    }
}