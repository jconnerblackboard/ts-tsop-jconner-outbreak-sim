﻿using System;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Customer post card object
    /// </summary>
    public class TsCustomerPostCard
    {
        /// <summary>
        /// Card number
        /// </summary>
        public decimal CardNumber { get; set; }

        /// <summary>
        /// Card Type
        /// </summary>
        public int CardType { get; set; }

        /// <summary>
        /// Card status
        /// </summary>
        public int CardStatus { get; set; }

        /// <summary>
        /// Is lost
        /// </summary>
        public bool Lost { get; set; }

        /// <summary>
        /// Idm number
        /// </summary>
        public decimal? IdmNumber { get; set; }

        /// <summary>
        /// Issue number
        /// </summary>
        public Int16? IssueNumber { get; set; }

        /// <summary>
        /// Status text / comment
        /// </summary>
        public string CardStatusText { get; set; }

        /// <summary>
        /// Card unassigned - valid only for temporary cards
        /// </summary>
        public bool Unassign { get; set; }
    }
}
