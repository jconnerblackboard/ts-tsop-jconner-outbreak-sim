﻿using System;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Customer post request container
    /// </summary>
    public class CustomerPostResponse : CustomerOperationResult
    {
        /// <summary>
        /// Customer Id
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Customer guid
        /// </summary>
        public Guid CustomerGuid { get; set; }
    }
}
