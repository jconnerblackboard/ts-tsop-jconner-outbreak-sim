﻿
namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// This object represents a record in the Transact system for a customer email type object
    /// </summary>
    public class TsCustomerEmailType
    {
        /// <summary>
        /// The identity of the email type record
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// The name of the email type record
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Whether the email should be unique or not
        /// </summary>
        public bool RequireUniqueEmail { get; set; }
        /// <summary>
        /// If this email address is considered to be the default or not 
        /// </summary>
        public bool Default { get; set; }
    }
}

