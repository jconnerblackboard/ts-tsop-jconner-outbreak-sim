﻿using System;
using System.Collections.Generic;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Container class for handling a get all customer request.
    /// </summary>
    public class CustomerGetAllRequest
    {
        /// <summary>
        /// Customer number
        /// </summary>
        public Int64? CustomerNumber { get; set; }

        /// <summary>
        /// Lastname
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Firstname
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Middle name
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Default Card number
        /// </summary>
        public string DefaultCardNumber { get; set; }

        /// <summary>
        /// Active
        /// </summary>
        public bool? IsActive { get; set; }

        /// <summary>
        /// Formats properties as optional uri params
        /// </summary>
        /// <returns></returns>
        public string FormUriParams()
        {
            var list = new List<string>();

            if (CustomerNumber.HasValue)
                list.Add($"{nameof(CustomerNumber)}={CustomerNumber}");
            if (!string.IsNullOrEmpty(LastName))
                list.Add($"{nameof(LastName)}={Uri.EscapeUriString(LastName)}");
            if (!string.IsNullOrEmpty(FirstName))
                list.Add($"{nameof(FirstName)}={Uri.EscapeUriString(FirstName)}");
            if (!string.IsNullOrEmpty(MiddleName))
                list.Add($"{nameof(MiddleName)}={Uri.EscapeUriString(MiddleName)}");
            if (DefaultCardNumber != null)
                list.Add($"{nameof(DefaultCardNumber)}={Uri.EscapeUriString(DefaultCardNumber)}");
            if (IsActive.HasValue)
                list.Add($"{nameof(IsActive)}={IsActive}");

            return string.Join("&", list);
        }
    }
}
