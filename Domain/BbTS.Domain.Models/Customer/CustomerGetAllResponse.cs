﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Container class for handling a response to a get all customers request.
    /// </summary>
    public class CustomerGetAllResponse
    {
        /// <summary>
        /// Original request
        /// </summary>
        public CustomerGetAllRequest Request { get; set; }

        /// <summary>
        /// Customers
        /// </summary>
        public List<TsCustomer> Customers { get; set; }
    }
}
