﻿using System;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// This object represents a Transact Customer_Def_Field_Def object. Customer Defined Field Definitions Table
    /// </summary>
    [Serializable]
    public class TsCustomerDefFieldValue
    {
        /// <summary>
        /// Customer Id
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Customer Entered Value
        /// </summary>
        public string FieldValue { get; set; }

        /// <summary>
        /// Customer defined field
        /// </summary>
        public TsCustomer_Def_Field_Def DefinedField { get; set; }
    }
}