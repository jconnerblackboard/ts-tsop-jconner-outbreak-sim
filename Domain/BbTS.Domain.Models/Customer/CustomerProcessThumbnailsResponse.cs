﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Customer
{
    public class CustomerProcessThumbnailsResponse
    {
        public string SingleUseTokenValue { get; set; }
        public bool AuthorizedByOauth { get; set; }
    }
}
