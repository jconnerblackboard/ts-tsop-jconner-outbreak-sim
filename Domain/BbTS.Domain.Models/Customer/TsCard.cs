﻿using System;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// This object represents a record in the Transact system for a card object
    /// </summary>
    public class TsCard
    {
        /// <summary>
        /// Card Number:  Must be a 22 digit number
        /// </summary>
        [XmlAttribute]
        public string CardNum { get; set; }

        /// <summary>
        /// The issue number is an incrementing number denoting the number of times the card has been issued to a cardholder, if this feature is implemented in the system.
        /// </summary>
        [XmlAttribute]
        public string Issue_Number { get; set; }

        /// <summary>
        /// Customer ID.  Null means customer was deleted.
        /// </summary>
        [XmlAttribute]
        public int Cust_Id { get; set; }

        /// <summary>
        /// (0) - Standard: Assignable to no more than One Customer (1) - Temporary:  Assignable to One Customer.  Available to be assigned to another Customer when Unassigned. (2) Mobile. (3) Wake-up.
        /// </summary>
        [XmlAttribute]
        public int Card_Type { get; set; }

        /// <summary>
        /// (0) - Active (1) - Frozen  (2) - Retired (cannot be changed to active without utility) - (3) Customer Deleted
        /// </summary>
        [XmlAttribute]
        public int Card_Status { get; set; }

        /// <summary>
        /// Reason for card status change
        /// </summary>
        [XmlAttribute]
        public string Card_Status_Text { get; set; }

        /// <summary>
        /// Date/Time status was assigned
        /// </summary>
        [XmlAttribute]
        public DateTime Card_Status_Datetime { get; set; }

        /// <summary>
        /// Lost Card Indicator (T/F)
        /// </summary>
        [XmlAttribute]
        public string Lost_Flag { get; set; }

        /// <summary>
        /// FeliCa IDm Number (Serial Number for FeliCa cards)
        /// </summary>
        [XmlAttribute]
        public string Card_Idm { get; set; }

        /// <summary>
        /// Alternate unique identity for card.
        /// </summary>
        [XmlAttribute]
        public string DomainId { get; set; }

        /// <summary>
        /// The identity of the system responsible for record creation
        /// </summary>
        [XmlAttribute]
        public int IssuerId { get; set; }

        /// <summary>
        /// When this record was created
        /// </summary>
        [XmlAttribute]
        public DateTime CreatedDateTime { get; set; }

        /// <summary>
        /// When this record was last modified
        /// </summary>
        [XmlAttribute]
        public DateTime ModifiedDateTime { get; set; }

        /// <summary>
        /// An integer based representation of a card number
        /// </summary>
        [XmlAttribute]
        public decimal CardNumber { get; set; }

        /// <summary>
        /// The number of digits in the card number
        /// </summary>
        [XmlAttribute]
        public int CardNumberLength { get; set; }

       /// <summary>
        /// The active start date of the card.
        /// </summary>
        [XmlIgnore]
        public DateTimeOffset ActiveStartDate { get; set; }

        /// <summary>
        /// The active start date of the card in a string format which is safe for Xml serialization
        /// </summary>
        [XmlAttribute("ActiveStartDate")]
        public String ActiveStartDateString
        {
            get { return ActiveStartDate.ToString("o"); } // o = yyyy-MM-ddTHH:mm:ss.fffffffzzz
            set { ActiveStartDate = DateTimeOffset.Parse(value); }
        }

        /// <summary>
        /// The active end date of the card.
        /// </summary>
        [XmlIgnore]
        public DateTimeOffset ActiveEndDate { get; set; }

        /// <summary>
        /// The active start date of the card in a string format which is safe for Xml serialization
        /// </summary>
        [XmlAttribute("ActiveEndDate")]
        public String ActiveEndDateString {
            get { return ActiveEndDate.ToString("o"); } // o = yyyy-MM-ddTHH:mm:ss.fffffffzzz
            set { ActiveEndDate = DateTimeOffset.Parse(value); }
        }

        /// <summary>
        /// The card name of the card.
        /// </summary>
        [XmlAttribute]
        public string CardName { get; set; }
    }
}
