﻿
namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    ///     The address
    /// </summary>
    public class TsAddress
    {
        /// <summary>
        /// Unique identifier
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the street1.
        /// </summary>
        public string Street1 { get; set; }

        /// <summary>
        /// Gets or sets the street2.
        /// </summary>
        public string Street2 { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Gets or sets the zip code.
        /// </summary>
        public string ZipCode { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets the phone number1.
        /// </summary>
        public string Phone1 { get; set; }

        /// <summary>
        /// Gets or sets the phone number2.
        /// </summary>
        public string Phone2 { get; set; }

        /// <summary>
        /// Gets or sets the notes.
        /// </summary>
        public string Notes { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the relation ship.
        /// </summary>
        public string Relationship { get; set; }

        /// <summary>
        /// Address type
        /// </summary>
        public TsAddressType AddressType { get; set; }
    }
}
