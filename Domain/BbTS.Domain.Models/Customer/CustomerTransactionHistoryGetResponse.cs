﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Container class for handling a get customer transaction response.
    /// </summary>
    public class CustomerTransactionHistoryGetResponse
    {
        /// <summary>
        /// Customer transactions
        /// </summary>
        public List<TsCustomerTransaction> CustomerTransactions { get; set; }
    }
}
