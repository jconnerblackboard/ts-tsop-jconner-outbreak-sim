﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Customer board plan
    /// </summary>
    [XmlRoot("CustomerBoardPlans")]
    public class CustomerBoardPlansPostRequest
    {
        /// <summary>
        /// RequestId
        /// </summary>
        [XmlIgnore]
        public Guid? RequestId { get; set; }

        /// <summary>
        /// Board plans
        /// </summary>
        [XmlArray, XmlArrayItem("BoardPlan")]
        public List<TsCustomerBoardPlanPost> CustomerBoardPlans { get; set; }

        /// <summary>
        /// Board plan Ids to be reset
        /// </summary>
        [XmlArray, XmlArrayItem("BoardPlanId")]
        public List<int> BoardUsedIdsToReset { get; set; }
    }
}
