﻿
namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Customer delete response 
    /// </summary>
    public class CustomerDeleteResponse
    {
        /// <summary>
        /// Customer Id
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Error message
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}
