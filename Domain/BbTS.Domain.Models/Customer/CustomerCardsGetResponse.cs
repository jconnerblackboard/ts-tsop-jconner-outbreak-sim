﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Container class for handling a server response to a get customer board request.
    /// </summary>
    public class CustomerCardsGetResponse
    {
        /// <summary>
        /// Customer guid
        /// </summary>
        public string CustomerGuid { get; set; }

        /// <summary>
        /// Customer
        /// </summary>
        public List<TsCard> CustomerCards { get; set; }
    }
}
