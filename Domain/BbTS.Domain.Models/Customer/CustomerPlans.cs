﻿
namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Customer plans
    /// </summary>
    public class CustomerPlans
    {
        /// <summary>
        /// Reuse delay count
        /// </summary>
        public int ReuseDelayCount { get; set; }

        /// <summary>
        /// Board plans count
        /// </summary>
        public int BoardPlansCount { get; set; }

        /// <summary>
        /// Event plan
        /// </summary>
        public string EventPlan { get; set; }

        /// <summary>
        /// DA plans count
        /// </summary>
        public int DoorAccessPlansCount { get; set; }

        /// <summary>
        /// Door overrides count
        /// </summary>
        public int DoorOverridesCount { get; set; }

        /// <summary>
        /// Board exclusions count
        /// </summary>
        public int BoardExclusionsCount { get; set; }
    }
}
