﻿using System;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Customer pos message
    /// </summary>
    public class CustomerPosMessage
    {
        /// <summary>
        /// Customer Id
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Message Id
        /// </summary>
        public int MessageId { get; set; }

        /// <summary>
        /// Start date
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Start date
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Max display count
        /// </summary>
        public int MaxDisplayCount { get; set; }

        /// <summary>
        /// Message Text
        /// </summary>
        public string MessageText { get; set; }
    }
}
