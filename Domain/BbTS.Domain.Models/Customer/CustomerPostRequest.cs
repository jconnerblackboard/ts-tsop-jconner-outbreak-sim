﻿using System;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Customer post request container
    /// </summary>
    public class CustomerPostRequest
    {
        /// <summary>
        /// Request Id
        /// </summary>
        public Guid? RequestId { get; set; }

        /// <summary>
        /// Customer
        /// </summary>
        public TsCustomerPost Customer { get; set; }
    }
}
