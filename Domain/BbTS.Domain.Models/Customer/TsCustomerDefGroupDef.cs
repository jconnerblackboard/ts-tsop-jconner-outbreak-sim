﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// This object represents a Transact Customer_Def_Grp_Def object. Customer Defined Group Definition Table
    /// </summary>
    public class TsCustomerDefGroupDef
    {
        /// <summary>
        /// Unique Identifier
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Name of Definition
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Indicates if the transaction will store the name group of this record (T/F)
        /// </summary>
        public bool StorePerTransaction { get; set; }
        /// <summary>
        /// Order in which the user defined field is displayed from top to bottom
        /// </summary>
        public int UiDisplayTop { get; set; }
        /// <summary>
        /// Order in which the user defined field is displayed from left to right
        /// </summary>
        public int UiDisplayLeft { get; set; }
        /// <summary>
        /// User Interface Display Height
        /// </summary>
        public int UiDisplayHeight { get; set; }
        /// <summary>
        /// User Interface Display Width
        /// </summary>
        public int UiDisplayWidth { get; set; }

        /// <summary>
        /// Group items
        /// </summary>
        public List<TsCustomerDefGroupDefItem> GroupItems { get; set; }
    }
}
