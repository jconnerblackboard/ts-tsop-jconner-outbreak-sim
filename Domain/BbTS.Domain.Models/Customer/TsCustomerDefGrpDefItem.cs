﻿using System;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// This object represents a Transact Customer_Def_Grp_Def_Item object. Sub Group Table of Customer Defined Group Definitions
    /// </summary>
    [Serializable]
    public class TsCustomer_Def_Grp_Def_Item
    {
        /// <summary>
        /// Customer Defined Group Definition Item Identifier
        /// </summary>
        [XmlAttribute]
        public int Customer_Def_Grp_Def_Item_Id { get; set; }
        /// <summary>
        /// Customer Defined Group Definition Association (from CUSTOMER_DEF_GRP_DEF table)
        /// </summary>
        [XmlAttribute]
        public int Customer_Def_Grp_Def_Id { get; set; }
        /// <summary>
        /// Name of Subgroup
        /// </summary>
        [XmlAttribute]
        public string Name { get; set; }
        /// <summary>
        /// Position of Sub Group within the Group
        /// </summary>
        [XmlAttribute]
        public int Position { get; set; }
    }
}
