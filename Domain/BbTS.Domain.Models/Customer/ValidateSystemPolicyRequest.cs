﻿using System;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// A ValidateSystemPolicyRequest object.
    /// </summary>
    public class ValidateSystemPolicyRequest
    {
        /// <summary>
        /// Customer GUID.
        /// </summary>
        public Guid CustomerGuid { get; set; }

        /// <summary>
        /// TenderId of the Stored Value tender being used.
        /// </summary>
        public int TenderId { get; set; }

        /// <summary>
        /// Originator GUID.
        /// </summary>
        public Guid OriginatorId { get; set; }

        /// <summary>
        /// The evaluation date/time.  (This equals the TimestampType.Start entry in the transaction's TransactionTimestamps list).
        /// </summary>
        public DateTimeOffset EvaluationDateTime { get; set; }
    }
}
