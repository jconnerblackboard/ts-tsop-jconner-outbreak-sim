﻿
namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Container class for handling a server response to a get customer photo request.
    /// </summary>
    public class CustomerPhotoGetResponse
    {
        /// <summary>
        /// Customer guid
        /// </summary>
        public string CustomerGuid { get; set; }

        /// <summary>
        /// Customer photo
        /// </summary>
        public TsCustomerPhoto CustomerPhoto { get; set; }
    }
}
