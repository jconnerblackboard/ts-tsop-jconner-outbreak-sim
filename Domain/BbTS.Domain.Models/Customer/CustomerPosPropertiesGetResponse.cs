﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Customer pos properties get response container
    /// </summary>
    public class CustomerPosPropertiesGetResponse
    {
        /// <summary>
        /// Customer pos display rules
        /// </summary>
        public List<int> PosDisplayRules { get; set; }

        /// <summary>
        /// Customer pos message
        /// </summary>
        public CustomerPosMessage PosMessage { get; set; }
    }
}
