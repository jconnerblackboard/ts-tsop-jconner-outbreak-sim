﻿using BbTS.Domain.Models.Definitions.StoredValue;

namespace BbTS.Domain.Models.Customer.Management.StoredValue
{
    /// <summary>
    ///     Stored Value Account Model
    /// </summary>
    public class PatchCustomerSvAccount
    {
        #region Backing Fields

        /// <summary>
        /// Gets or sets the type of the stored value account association.
        /// </summary>
        private StoredValueAccountAssociationType _storedValueAccountAssociationType;

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        private string _name;

        /// <summary>
        /// Gets or sets the credit limit.
        /// </summary>
        private double _creditLimit;

        #endregion

        #region public properties

        /// <summary>
        /// Gets or sets the type of the stored value account association.
        /// </summary>
        public StoredValueAccountAssociationType StoredValueAccountAssociationType
        {
            get { return _storedValueAccountAssociationType; }
            set
            {
                _storedValueAccountAssociationType = value;
                StoredValueAccountAssociationTypeIsSet = true;
            }
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                NameIsSet = true;
            }
        }

        /// <summary>
        /// Gets or sets the credit limit.
        /// </summary>
        public double CreditLimit
        {
            get { return _creditLimit; }
            set
            {
                _creditLimit = value;
                CreditLimitIsSet = true;
            }
        }

        #endregion

        #region internal properties indicating which field was set through request

        /// <summary>
        /// Gets or sets a value indicating whether [stored value account association type is set].
        /// </summary>
        public bool StoredValueAccountAssociationTypeIsSet { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether [name is set].
        /// </summary>
        public bool NameIsSet { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether [credit limit is set].
        /// </summary>
        public bool CreditLimitIsSet { get; private set; }

        #endregion
    }
}
