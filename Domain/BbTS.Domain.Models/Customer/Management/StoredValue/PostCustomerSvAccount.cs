﻿using BbTS.Domain.Models.Definitions.StoredValue;

namespace BbTS.Domain.Models.Customer.Management.StoredValue
{
    /// <summary>
    ///     Stored Value Account Model
    /// </summary>
    public class PostCustomerSvAccount
    {
        /// <summary>
        /// Gets or sets the stored value account type identifier.
        /// </summary>
        /// <value>
        /// The stored value account type identifier.
        /// </value>
        public int StoredValueAccountTypeId { get; set; }

        /// <summary>
        /// Gets or sets the type of the stored value account association.
        /// </summary>
        /// <value>
        /// The type of the stored value account association.
        /// </value>
        public StoredValueAccountAssociationType StoredValueAccountAssociationType { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the balance forward.
        /// </summary>
        /// <value>
        /// The balance forward.
        /// </value>
        public double BalanceForward { get; set; }

        /// <summary>
        /// Gets or sets the credit limit.
        /// </summary>
        /// <value>
        /// The credit limit.
        /// </value>
        public double CreditLimit { get; set; }
    }
}
