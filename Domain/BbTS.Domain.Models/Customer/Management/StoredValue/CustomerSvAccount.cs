﻿using System;
using BbTS.Domain.Models.Definitions.Transaction;

namespace BbTS.Domain.Models.Customer.Management.StoredValue
{
    /// <summary>
    ///     Stored Value Account Model
    /// </summary>
    public class CustomerSvAccount : PostCustomerSvAccount
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public Int64 Id { get; set; }

        /// <summary>
        /// Gets or sets the account number.
        /// </summary>
        /// <value>
        /// The account number.
        /// </value>
        public Int64 AccountNumber { get; set; }

        /// <summary>
        /// Gets or sets the type of the stored value account share.
        /// </summary>
        /// <value>
        /// The type of the stored value account share.
        /// </value>
        public StoredValueAccountShareType StoredValueAccountShareType { get; set; }

        /// <summary>
        /// Gets or sets the currency code iso alphabetic.
        /// </summary>
        /// <value>
        /// The currency code iso alphabetic.
        /// </value>
        public string CurrencyCodeIsoAlphabetic { get; set; }

        /// <summary>
        /// Gets or sets the balance.
        /// </summary>
        /// <value>
        /// The balance.
        /// </value>
        public double Balance { get; set; }

        /// <summary>
        /// Sv Account Type name
        /// </summary>
        public string TypeName { get; set; }
    }
}
