﻿using BbTS.Core.Serialization;
using BbTS.Domain.Models.Definitions.Card;
using Newtonsoft.Json;

namespace BbTS.Domain.Models.Customer.Management
{
    /// <summary>
    ///     The card of customer
    /// </summary>
    public class PatchCustomerCard
    {
        #region Backing Fields

        /// <summary>
        /// The primary
        /// </summary>
        private bool _primary;

        /// <summary>
        /// The issue number
        /// </summary>
        private string _issueNumber;

        /// <summary>
        /// The status
        /// </summary>
        private CardStatusType _cardStatusType;

        /// <summary>
        /// The lost
        /// </summary>
        private bool _lost;

        /// <summary>
        /// The card <c>idm</c> number
        /// </summary>
        private string _idmNumber;

        /// <summary>
        /// The comment
        /// </summary>
        private string _commentText;

        #endregion

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="PatchCustomerCard"/> is primary.
        /// </summary>
        /// <value>
        ///   <c>true</c> if primary; otherwise, <c>false</c>.
        /// </value>
        public bool Primary
        {
            get { return _primary; }
            set
            {
                _primary = value;
                PrimaryIsSet = true;
            }
        }

        /// <summary>
        /// Gets or sets the issue number.
        /// </summary>
        /// <value>
        /// The issue number.
        /// </value>
        public string IssueNumber
        {
            get { return _issueNumber; }
            set
            {
                _issueNumber = value;
                IssueNumberIsSet = true;
            }
        }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        [JsonConverter(typeof(CustomDefaultStringEnumConverter), (int)CardStatusType.Unknown, typeof(AllCapsNamingStrategy), true)]
        public CardStatusType CardStatusType
        {
            get { return _cardStatusType; }
            set
            {
                _cardStatusType = value;
                CardStatusTypeIsSet = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="PatchCustomerCard"/> is lost.
        /// </summary>
        /// <value>
        ///   <c>true</c> if lost; otherwise, <c>false</c>.
        /// </value>
        public bool Lost
        {
            get { return _lost; }
            set
            {
                _lost = value;
                LostIsSet = true;
            }
        }

        /// <summary>
        /// Gets or sets the card IDM number.
        /// </summary>
        /// <value>
        /// The card IDM number.
        /// </value>
        public string IdmNumber
        {
            get { return _idmNumber; }
            set
            {
                _idmNumber = value;
                IdmNumberIsSet = true;
            }
        }

        /// <summary>
        /// Gets or sets the comment.
        /// </summary>
        /// <value>
        /// The comment.
        /// </value>
        public string CommentText
        {
            get { return _commentText; }
            set
            {
                _commentText = value;
                CommentTextIsSet = true;
            }
        }

        #region Properties indicating which field was set through request

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="PatchCustomerCard"/> is primary.
        /// </summary>
        /// <value>
        ///   <c>true</c> if primary; otherwise, <c>false</c>.
        /// </value>
        public bool PrimaryIsSet { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether [issue number is set].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [issue number is set]; otherwise, <c>false</c>.
        /// </value>
        public bool IssueNumberIsSet { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether [status is set].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [status is set]; otherwise, <c>false</c>.
        /// </value>
        public bool CardStatusTypeIsSet { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether [lost is set].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [lost is set]; otherwise, <c>false</c>.
        /// </value>
        public bool LostIsSet { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether [card <c>idm</c> number is set].
        /// </summary>
        /// <value>
        /// <c>true</c> if [card <c>idm</c> number is set]; otherwise, <c>false</c>.
        /// </value>
        public bool IdmNumberIsSet { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether [comment is set].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [comment is set]; otherwise, <c>false</c>.
        /// </value>
        public bool CommentTextIsSet { get; private set; }

        #endregion
    }
}
