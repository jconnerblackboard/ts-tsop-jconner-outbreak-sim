﻿using System;
using System.Collections.Generic;

namespace BbTS.Domain.Models.Customer.Management
{
    /// <summary>
    ///     The PATCH customer 
    /// </summary>
    public class PatchCustomer
    {
        #region Backing Fields

        /// <summary>
        /// The first name
        /// </summary>
        private string _firstName;

        /// <summary>
        /// The middle name
        /// </summary>
        private string _middleName;

        /// <summary>
        /// The last name
        /// </summary>
        private string _lastName;

        /// <summary>
        /// The birth date
        /// </summary>
        private DateTime? _birthDate;

        /// <summary>
        /// The gender
        /// </summary>
        private int? _gender;

        /// <summary>
        /// The pin
        /// </summary>
        private string _pin;

        /// <summary>
        /// The active
        /// </summary>
        private bool? _active;

        /// <summary>
        /// The active start date
        /// </summary>
        private DateTime? _activeStartDate;

        /// <summary>
        /// The active end date
        /// </summary>
        private DateTime? _activeEndDate;

        /// <summary>
        /// The email address default
        /// </summary>
        private string _emailAddressDefault;

        /// <summary>
        /// The image
        /// </summary>
        private string _image;

        /// <summary>
        /// The addresses
        /// </summary>
        private List<CustomerAddress> _addresses;

        /// <summary>
        /// The defined fields
        /// </summary>
        private List<CustomerDefinedField> _definedFields;

        #endregion

        #region public properties

        /// <summary>
        ///     Gets or sets the FirstName.
        /// </summary>
        public string FirstName
        {
            get { return _firstName; }
            set
            {
                _firstName = value;
                FirstNameIsSet = true;
            }
        }

        /// <summary>
        ///     Gets or sets the Middle Name.
        /// </summary>
        public string MiddleName
        {
            get { return _middleName; }
            set
            {
                _middleName = value;
                MiddleNameIsSet = true;
            }
        }

        /// <summary>
        ///     Gets or sets the Last Name.
        /// </summary>
        public string LastName
        {
            get { return _lastName; }
            set
            {
                _lastName = value;
                LastNameIsSet = true;
            }
        }

        /// <summary>
        ///     Gets or sets the Birth date.
        /// </summary>
        public DateTime? BirthDate
        {
            get { return _birthDate; }
            set
            {
                _birthDate = value;
                BirthDateIsSet = true;
            }
        }

        /// <summary>
        ///     Gets or sets the Gender.
        /// </summary>
        public int? Gender
        {
            get { return _gender; }
            set
            {
                _gender = value;
                GenderIsSet = true;
            }
        }

        /// <summary>
        /// Gets or sets the pin.
        /// </summary>
        public string Pin
        {
            get { return _pin; }
            set
            {
                _pin = value;
                PinIsSet = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Customer"/> is active.
        /// </summary>
        public bool? Active
        {
            get { return _active; }
            set
            {
                _active = value;
                ActiveIsSet = true;
            }
        }

        /// <summary>
        /// Gets or sets the active start date.
        /// </summary>
        public DateTime? ActiveStartDate
        {
            get { return _activeStartDate; }
            set
            {
                _activeStartDate = value;
                ActiveStartDateIsSet = true;
            }
        }

        /// <summary>
        /// Gets or sets the active end date.
        /// </summary>
        public DateTime? ActiveEndDate
        {
            get { return _activeEndDate; }
            set
            {
                _activeEndDate = value;
                ActiveEndDateIsSet = true;
            }
        }

        /// <summary>
        /// Gets or sets the email address default.
        /// </summary>
        public string EmailAddressDefault
        {
            get { return _emailAddressDefault; }
            set
            {
                _emailAddressDefault = value;
                EmailAddressDefaultIsSet = true;
            }
        }

        /// <summary>
        ///     Gets or sets the Image .
        /// </summary>
        public string Image
        {
            get { return _image; }
            set
            {
                _image = value;
                ImageIsSet = true;
            }
        }

        /// <summary>
        /// Gets or sets the addresses.
        /// </summary>
        public List<CustomerAddress> Addresses
        {
            get { return _addresses; }
            set
            {
                _addresses = value;
                AddressesIsSet = true;
            }
        }

        /// <summary>
        /// Gets or sets the defined fields.
        /// </summary>
        public List<CustomerDefinedField> DefinedFields
        {
            get { return _definedFields; }
            set
            {
                _definedFields = value;
                DefinedFieldsIsSet = true;
            }
        }

        #endregion

        #region internal properties indicating which field was set through request

        /// <summary>
        /// Gets or sets a value indicating whether [first name is set].
        /// </summary>
        internal bool FirstNameIsSet { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [middle name is set].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [middle name is set]; otherwise, <c>false</c>.
        /// </value>
        internal bool MiddleNameIsSet { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [last name is set].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [last name is set]; otherwise, <c>false</c>.
        /// </value>
        internal bool LastNameIsSet { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [birth date is set].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [birth date is set]; otherwise, <c>false</c>.
        /// </value>
        internal bool BirthDateIsSet { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [gender is set].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [gender is set]; otherwise, <c>false</c>.
        /// </value>
        internal bool GenderIsSet { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [pin is set].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [pin is set]; otherwise, <c>false</c>.
        /// </value>
        internal bool PinIsSet { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [active is set].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [active is set]; otherwise, <c>false</c>.
        /// </value>
        internal bool ActiveIsSet { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [active start date is set].
        /// </summary>
        /// <value>
        /// <c>true</c> if [active start date is set]; otherwise, <c>false</c>.
        /// </value>
        internal bool ActiveStartDateIsSet { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [active end date is set].
        /// </summary>
        /// <value>
        /// <c>true</c> if [active end date is set]; otherwise, <c>false</c>.
        /// </value>
        internal bool ActiveEndDateIsSet { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [email address default is set].
        /// </summary>
        /// <value>
        /// <c>true</c> if [email address default is set]; otherwise, <c>false</c>.
        /// </value>
        internal bool EmailAddressDefaultIsSet { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [image is set].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [image is set]; otherwise, <c>false</c>.
        /// </value>
        internal bool ImageIsSet { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [addresses is set].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [addresses is set]; otherwise, <c>false</c>.
        /// </value>
        internal bool AddressesIsSet { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [defined fields is set].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [defined fields is set]; otherwise, <c>false</c>.
        /// </value>
        internal bool DefinedFieldsIsSet { get; set; }

        #endregion
    }
}
