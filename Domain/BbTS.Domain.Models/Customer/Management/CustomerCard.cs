﻿using BbTS.Core.Serialization;
using BbTS.Domain.Models.Definitions.Card;
using Newtonsoft.Json;

namespace BbTS.Domain.Models.Customer.Management
{
    /// <summary>
    ///     The card of customer
    /// </summary>
    public class CustomerCard
    {
        /// <summary>
        /// Gets or sets the card number.
        /// </summary>
        /// <value>
        /// The card number.
        /// </value>
        public string CardNumber { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="CustomerCard"/> is primary.
        /// </summary>
        /// <value>
        ///   <c>true</c> If primary; otherwise, <c>false</c>.
        /// </value>
        public bool Primary { get; set; }

        /// <summary>
        /// Gets or sets the issue number.
        /// </summary>
        /// <value>
        /// The issue number.
        /// </value>
        public string IssueNumber { get; set; }

        /// <summary>
        /// Gets or sets the type of the card.
        /// </summary>
        /// <value>
        /// The type of the card.
        /// </value>
        [JsonConverter(typeof(CustomDefaultStringEnumConverter), (int)CardStatusType.Unknown, typeof(AllCapsNamingStrategy), true)]
        public CardType CardType { get; set; }

        /// <summary>
        /// Gets or sets the type of the card status.
        /// </summary>
        /// <value>
        /// The type of the card status.
        /// </value>
        [JsonConverter(typeof(CustomDefaultStringEnumConverter), (int)CardStatusType.Unknown, typeof(AllCapsNamingStrategy), true)]
        public CardStatusType CardStatusType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="CustomerCard"/> is lost.
        /// </summary>
        /// <value>
        ///   <c>true</c> If lost; otherwise, <c>false</c>.
        /// </value>
        public bool Lost { get; set; }

        /// <summary>
        /// Gets or sets the card IDM number.
        /// </summary>
        /// <value>
        /// The card IDM number.
        /// </value>
        public string IdmNumber { get; set; }

        /// <summary>
        /// Gets or sets the comment text.
        /// </summary>
        /// <value>
        /// The comment text.
        /// </value>
        public string CommentText { get; set; }

        /// <summary>
        /// Gets or sets the last modified date time.
        /// </summary>
        /// <value>
        /// The last modified date time.
        /// </value>
        public string LastModifiedDateTime { get; set; }
    }
}
