﻿using BbTS.Core.Serialization;
using System.Xml.Serialization;
using BbTS.Domain.Models.Definitions.DefinedField;
using Newtonsoft.Json;

namespace BbTS.Domain.Models.Customer.Management
{
    /// <summary>
    ///     Customer Defined Field Model
    /// </summary>
    public class CustomerDefinedField
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the type of the customer defined field.
        /// </summary>
        /// <value>
        /// The type of the customer defined field.
        /// </value>
        [XmlIgnore]
        [JsonConverter(typeof(CustomDefaultStringEnumConverter), (int)DefinedFieldType.None, typeof(AllCapsNamingStrategy), true)]
        public DefinedFieldType CustomerDefinedFieldType { get; set; }

        /// <summary>
        ///  Gets or sets the type id of the customer defined field.
        /// </summary>
        [JsonIgnore]
        public int DefinedFieldTypeId
        {
            get { return (int)CustomerDefinedFieldType; }
            set { CustomerDefinedFieldType = (DefinedFieldType)value; }
        }

        /// <summary>
        /// Gets or sets the caption.
        /// </summary>
        /// <value>
        /// The caption.
        /// </value>
        public string Caption { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public string Value { get; set; }
    }
}
