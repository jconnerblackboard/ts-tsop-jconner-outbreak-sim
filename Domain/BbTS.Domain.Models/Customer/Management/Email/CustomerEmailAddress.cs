﻿
namespace BbTS.Domain.Models.Customer.Management.Email
{
    /// <summary>
    ///     Customer Email Address Model
    /// </summary>
    public class CustomerEmailAddress : CustomerEmailAddressBase
    {
        /// <summary>
        /// Gets or sets the name of the type.
        /// </summary>
        /// <value>
        /// The name of the type.
        /// </value>
        public string TypeName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [default type].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [default type]; otherwise, <c>false</c>.
        /// </value>
        public bool DefaultType { get; set; }
    }
}
