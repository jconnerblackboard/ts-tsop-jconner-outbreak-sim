﻿
namespace BbTS.Domain.Models.Customer.Management.Email
{
    /// <summary>
    ///     Customer Email Address Model
    /// </summary>
    public class PatchCustomerEmailAddress
    {
        #region private

        /// <summary>
        /// The email address
        /// </summary>
        private string _emailAddress;

        /// <summary>
        /// The default override
        /// </summary>
        private bool _defaultOverride;

        #endregion

        /// <summary>
        /// Gets or sets the email address.
        /// </summary>
        /// <value>
        /// The email address.
        /// </value>
        public string EmailAddress
        {
            get { return _emailAddress; }
            set
            {
                _emailAddress = value;
                EmailAddressIsSet = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [default override].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [default override]; otherwise, <c>false</c>.
        /// </value>
        public bool DefaultOverride
        {
            get { return _defaultOverride; }
            set
            {
                _defaultOverride = value;
                DefaultOverrideIsSet = true;
            }
        }

        #region internal properties indicating which field was set through request

        /// <summary>
        /// Gets or sets a value indicating whether [email address is set].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [email address is set]; otherwise, <c>false</c>.
        /// </value>
        public bool EmailAddressIsSet { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether [default override is set].
        /// </summary>
        /// <value>
        /// <c>true</c> if [default override is set]; otherwise, <c>false</c>.
        /// </value>
        public bool DefaultOverrideIsSet { get; private set; }

        #endregion
    }
}
