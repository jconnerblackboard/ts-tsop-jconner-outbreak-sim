﻿
namespace BbTS.Domain.Models.Customer.Management.Email
{
    /// <summary>
    ///     Customer Email Address Model
    /// </summary>
    public class CustomerEmailAddressBase
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the email address.
        /// </summary>
        /// <value>
        /// The email address.
        /// </value>
        public string EmailAddress { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [default override].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [default override]; otherwise, <c>false</c>.
        /// </value>
        public bool DefaultOverride { get; set; }
    }
}
