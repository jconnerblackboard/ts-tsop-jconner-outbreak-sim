﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace BbTS.Domain.Models.Customer.Management
{
    /// <summary>
    /// Customer
    /// </summary>
    public class CustomerManagementData : PostCustomer
    {
        /// <summary>
        /// Gets or sets the open date time.
        /// </summary>
        /// <value>
        /// The open date time.
        /// </value>
        public string OpenDateTime { get; set; }

        /// <summary>
        /// Gets or sets the card numbers.
        /// </summary>
        /// <value>
        /// The card numbers.
        /// </value>
        public List<CustomerCard> CardNumbers { get; set; }

        /// <summary>
        /// Creates a copy of PostCustomer as CustomerManagementData
        /// </summary>
        /// <param name="customer">Customer to be copied</param>
        /// <returns>Copied customer</returns>
        public static CustomerManagementData CopyFromBase(PostCustomer customer)
        {
            var clone = new CustomerManagementData();

            foreach(var propertyInfo in typeof(PostCustomer).GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(x => x.CanRead && x.CanWrite))
            {
                propertyInfo.SetValue(clone, propertyInfo.GetValue(customer));
            }

            return clone;
        }
    }
}
