﻿using BbTS.Core.Serialization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Customer.Management
{
    /// <summary>
    ///     The post customer request options
    /// </summary>
    [SuppressMessage("ReSharper", "ValueParameterNotUsed", Justification = "XML requires setter to parse")]
    [XmlRoot("Customer")]
    public class PostCustomer
    {
        /// <summary>
        /// Gets or sets the customer number.
        /// </summary>
        public string CustomerNumber { get; set; }

        /// <summary>
        /// Gets or sets the FirstName.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the Middle Name.
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Gets or sets the Last Name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the Birth date.
        /// </summary>
        [JsonConverter(typeof(CustomDateFormatConverter), "yyyy-MM-dd")]
        public DateTime? BirthDate { get; set; }

        /// <summary>
        /// Gets birth date as OA
        /// </summary>
        [JsonIgnore]
        public double? BirthOaDate
        {
            get { return BirthDate?.ToOADate(); }
            set { }
        }

        /// <summary>
        /// Gets or sets the Gender.
        /// </summary>
        public int? Gender { get; set; }

        /// <summary>
        /// Gets sex
        /// </summary>
        [JsonIgnore]
        public string Sex
        {
            get { return CustomerGenderResolver.ResloveSex(Gender); }
            set { }
        }

        /// <summary>
        /// Gets or sets the pin.
        /// </summary>
        public string Pin { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="PostCustomer"/> is active.
        /// </summary>
        public bool? Active { get; set; }

        /// <summary>
        /// Gets or sets the active start date.
        /// </summary>
        [JsonConverter(typeof(CustomDateFormatConverter), "yyyy-MM-dd")]
        public DateTime? ActiveStartDate { get; set; }

        /// <summary>
        /// Gets active start date as OA
        /// </summary>
        [JsonIgnore]
        public double? ActiveStartOaDate
        {
            get { return ActiveStartDate?.ToOADate(); }
            set { }
        }

        /// <summary>
        /// Gets or sets the active end date.
        /// </summary>
        [JsonConverter(typeof(CustomDateFormatConverter), "yyyy-MM-dd")]
        public DateTime? ActiveEndDate { get; set; }

        /// <summary>
        /// Gets active end date as OA
        /// </summary>
        [JsonIgnore]
        public double? ActiveEndOaDate
        {
            get { return ActiveEndDate?.ToOADate(); }
            set { }
        }

        /// <summary>
        /// Gets or sets the email address default.
        /// </summary>
        public string EmailAddressDefault { get; set; }

        /// <summary>
        /// Gets or sets the Image .
        /// </summary>
        public string Image { get; set; }

        /// <summary>
        /// Gets or sets the addresses.
        /// </summary>
        [XmlArray, XmlArrayItem("Address", typeof(CustomerAddress))]
        public List<CustomerAddress> Addresses { get; set; }

        /// <summary>
        /// Gets or sets the defined fields.
        /// </summary>
        [XmlArray, XmlArrayItem("DefinedField", typeof(CustomerDefinedField))]
        public List<CustomerDefinedField> DefinedFields { get; set; }
    }
}
