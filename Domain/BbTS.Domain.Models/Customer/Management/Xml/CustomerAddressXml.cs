﻿using System.Diagnostics.CodeAnalysis;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Customer.Management.Xml
{
    /// <summary>
    ///     The address
    /// </summary>
    [SuppressMessage("ReSharper", "ValueParameterNotUsed", Justification = "XML requires setter to parse")]
    public class CustomerAddressXml
    {
        private readonly CustomerAddress _address;

        #region XML properties

        /// <summary>
        /// Address Type
        /// </summary>
        [XmlElement("AddressType")]
        public EditedFieldElement AddressTypeElement
        {
            get { return new EditedFieldElement(_address?.AddressTypeIsSet ?? false, _address?.AddressTypeId.ToString()); }
            set { }
        }

        /// <summary>
        /// Street1
        /// </summary>
        [XmlElement("Street1")]
        public EditedFieldElement Street1Element
        {
            get { return new EditedFieldElement(_address?.Street1IsSet ?? false, _address?.Street1); }
            set { }
        }

        /// <summary>
        /// Street2
        /// </summary>
        [XmlElement("Street2")]
        public EditedFieldElement Street2Element
        {
            get { return new EditedFieldElement(_address?.Street2IsSet ?? false, _address?.Street2); }
            set { }
        }

        /// <summary>
        /// State
        /// </summary>
        [XmlElement("State")]
        public EditedFieldElement StateElement
        {
            get { return new EditedFieldElement(_address?.StateIsSet ?? false, _address?.State); }
            set { }
        }

        /// <summary>
        /// Postal Code
        /// </summary>
        [XmlElement("PostalCode")]
        public EditedFieldElement PostalCodeElement
        {
            get { return new EditedFieldElement(_address?.PostalCodeIsSet ?? false, _address?.PostalCode); }
            set { }
        }

        /// <summary>
        /// Country
        /// </summary>
        [XmlElement("Country")]
        public EditedFieldElement CountryElement
        {
            get { return new EditedFieldElement(_address?.CountryIsSet ?? false, _address?.Country); }
            set { }
        }

        /// <summary>
        /// Phone Number 1
        /// </summary>
        [XmlElement("PhoneNumber1")]
        public EditedFieldElement PhoneNumber1Element
        {
            get { return new EditedFieldElement(_address?.PhoneNumber1IsSet ?? false, _address?.PhoneNumber1); }
            set { }
        }

        /// <summary>
        /// Phone Number 2
        /// </summary>
        [XmlElement("PhoneNumber2")]
        public EditedFieldElement PhoneNumber2Element
        {
            get { return new EditedFieldElement(_address?.PhoneNumber2IsSet ?? false, _address?.PhoneNumber2); }
            set { }
        }

        /// <summary>
        /// Notes
        /// </summary>
        [XmlElement("Notes")]
        public EditedFieldElement NotesElement
        {
            get { return new EditedFieldElement(_address?.NotesIsSet ?? false, _address?.Notes); }
            set { }
        }

        /// <summary>
        /// Name
        /// </summary>
        [XmlElement("Name")]
        public EditedFieldElement NameElement
        {
            get { return new EditedFieldElement(_address?.NameIsSet ?? false, _address?.Name); }
            set { }
        }

        /// <summary>
        /// Notes
        /// </summary>
        [XmlElement("Relationship")]
        public EditedFieldElement RelationshipElement
        {
            get { return new EditedFieldElement(_address?.RelationshipIsSet ?? false, _address?.Relationship); }
            set { }
        }

        #endregion

        /// <summary>
        /// Create customer address xml object
        /// </summary>
        public CustomerAddressXml() { }

        /// <summary>
        /// Create customer address xml object
        /// </summary>
        /// <param name="address">Address</param>
        public CustomerAddressXml(CustomerAddress address)
        {
            _address = address;
        }
    }
}
