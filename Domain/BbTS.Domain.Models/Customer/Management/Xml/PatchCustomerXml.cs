﻿using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Customer.Management.Xml
{
    /// <summary>
    ///     The PATCH customer xml object
    /// </summary>
    [SuppressMessage("ReSharper", "ValueParameterNotUsed", Justification = "XML requires setter to parse")]
    [XmlRoot("Customer")]
    public class PatchCustomerXml
    {
        private readonly PatchCustomer _customer;

        #region XML properties

        /// <summary>
        /// First name
        /// </summary>
        [XmlElement("FirstName")]
        public EditedFieldElement FirstNameElement
        {
            get { return new EditedFieldElement(_customer?.FirstNameIsSet ?? false, _customer?.FirstName); }
            set { }
        }

        /// <summary>
        /// Middle name
        /// </summary>
        [XmlElement("MiddleName")]
        public EditedFieldElement MiddleNameElement
        {
            get { return new EditedFieldElement(_customer?.MiddleNameIsSet ?? false, _customer?.MiddleName); }
            set { }
        }

        /// <summary>
        /// Last name
        /// </summary>
        [XmlElement("LastName")]
        public EditedFieldElement LastNameElement
        {
            get { return new EditedFieldElement(_customer?.LastNameIsSet ?? false, _customer?.LastName); }
            set { }
        }

        /// <summary>
        /// Birth date
        /// </summary>
        [XmlElement("BirthOaDate")]
        public EditedFieldElement BirthOaDateElement
        {
            get { return new EditedFieldElement(_customer?.BirthDateIsSet ?? false, _customer?.BirthDate?.ToOADate().ToString(CultureInfo.InvariantCulture)); }
            set { }
        }

        /// <summary>
        /// Sex
        /// </summary>
        [XmlElement("Sex")]
        public EditedFieldElement SexElement
        {
            get { return new EditedFieldElement(_customer?.GenderIsSet ?? false, CustomerGenderResolver.ResloveSex(_customer?.Gender)); }
            set { }
        }

        /// <summary>
        /// Pin
        /// </summary>
        [XmlElement("Pin")]
        public EditedFieldElement PinElement
        {
            get { return new EditedFieldElement(_customer?.PinIsSet ?? false, _customer?.Pin); }
            set { }
        }

        /// <summary>
        /// Active
        /// </summary>
        [XmlElement("Active")]
        public EditedFieldElement ActiveElement
        {
            get { return new EditedFieldElement(_customer?.ActiveIsSet ?? false, _customer?.Active?.ToString()); }
            set { }
        }

        /// <summary>
        /// Active start date
        /// </summary>
        [XmlElement("ActiveStartOaDate")]
        public EditedFieldElement ActiveStartOaDateElement
        {
            get { return new EditedFieldElement(_customer?.ActiveStartDateIsSet ?? false, _customer?.ActiveStartDate?.ToOADate().ToString(CultureInfo.InvariantCulture)); }
            set { }
        }

        /// <summary>
        /// Active end date
        /// </summary>
        [XmlElement("ActiveEndOaDate")]
        public EditedFieldElement ActiveEndOaDateElement
        {
            get { return new EditedFieldElement(_customer?.ActiveEndDateIsSet ?? false, _customer?.ActiveEndDate?.ToOADate().ToString(CultureInfo.InvariantCulture)); }
            set { }
        }

        /// <summary>
        /// Email address default
        /// </summary>
        [XmlElement("EmailAddressDefault")]
        public EditedFieldElement EmailAddressDefaultElement
        {
            get { return new EditedFieldElement(_customer?.EmailAddressDefaultIsSet ?? false, _customer?.EmailAddressDefault); }
            set { }
        }

        /// <summary>
        /// Image
        /// </summary>
        [XmlElement("Image")]
        public EditedFieldElement ImageElement
        {
            get { return new EditedFieldElement(_customer?.ImageIsSet ?? false, _customer?.Image); }
            set { }
        }

        /// <summary>
        /// Addresses
        /// </summary>
        [XmlElement("Addresses")]
        public CustomerAddressElement AddressesElement
        {
            get { return new CustomerAddressElement(_customer?.AddressesIsSet ?? false, _customer?.Addresses); }
            set { }
        }

        /// <summary>
        /// Defined fields
        /// </summary>
        [XmlElement("DefinedFields")]
        public CustomerDefinedFieldElement DefinedFieldsElement
        {
            get { return new CustomerDefinedFieldElement(_customer?.DefinedFieldsIsSet ?? false, _customer?.DefinedFields); }
            set { }
        }

        #endregion

        /// <summary>
        /// Creates new Patchcustomer xml object
        /// </summary>
        public PatchCustomerXml() { }

        /// <summary>
        /// Creates new Patchcustomer xml object
        /// </summary>
        /// <param name="customer">Customer</param>
        public PatchCustomerXml(PatchCustomer customer)
        {
            _customer = customer;
        }
    }
}
