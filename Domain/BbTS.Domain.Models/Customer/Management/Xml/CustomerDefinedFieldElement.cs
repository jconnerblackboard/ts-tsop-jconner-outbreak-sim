﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Customer.Management.Xml
{
    /// <summary>
    /// Represents Xml element
    /// </summary>
    public class CustomerDefinedFieldElement
    {
        /// <summary>
        /// Parameterless ctor
        /// </summary>
        public CustomerDefinedFieldElement() { }

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="edited">Edited</param>
        /// <param name="fields">Defined fields</param>
        public CustomerDefinedFieldElement(bool edited, List<CustomerDefinedField> fields)
        {
            Edited = edited;
            DefinedFields = fields;
        }

        /// <summary>
        /// Indicates if value was edited by user or ignored
        /// </summary>
        [XmlAttribute]
        public bool Edited { get; set; }

        /// <summary>
        /// Value
        /// </summary>
        [XmlElement("DefinedField")]
        public List<CustomerDefinedField> DefinedFields { get; set; }
    }
}
