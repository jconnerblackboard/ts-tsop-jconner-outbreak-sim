﻿using System.Xml.Serialization;

namespace BbTS.Domain.Models.Customer.Management.Xml
{
    /// <summary>
    /// Represents Xml element
    /// </summary>
    public class EditedFieldElement
    {
        /// <summary>
        /// Parameterless ctor
        /// </summary>
        public EditedFieldElement() { }

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="edited">Edited</param>
        /// <param name="value">Value</param>
        public EditedFieldElement(bool edited, string value)
        {
            Edited = edited;
            Value = value;
        }

        /// <summary>
        /// Indicates if value was edited by user or ignored
        /// </summary>
        [XmlAttribute]
        public bool Edited { get; set; }

        /// <summary>
        /// Value
        /// </summary>
        [XmlText]
        public string Value { get; set; }
    }
}
