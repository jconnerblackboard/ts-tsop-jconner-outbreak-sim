﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Customer.Management.Xml
{
    /// <summary>
    /// Represents Xml element
    /// </summary>
    public class CustomerAddressElement
    {
        /// <summary>
        /// Parameterless ctor
        /// </summary>
        public CustomerAddressElement() { }

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="edited">Edited</param>
        /// <param name="addresses">Addresses</param>
        public CustomerAddressElement(bool edited, List<CustomerAddress> addresses)
        {
            Edited = edited;
            Addresses = addresses?.Select(x => new CustomerAddressXml(x)).ToList();
        }

        /// <summary>
        /// Indicates if value was edited by user or ignored
        /// </summary>
        [XmlAttribute]
        public bool Edited { get; set; }

        /// <summary>
        /// Value
        /// </summary>
        [XmlElement("Address")]
        public List<CustomerAddressXml> Addresses { get; set; }
    }
}
