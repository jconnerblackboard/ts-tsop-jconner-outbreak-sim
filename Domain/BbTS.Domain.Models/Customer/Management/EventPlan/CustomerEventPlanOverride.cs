﻿
namespace BbTS.Domain.Models.Customer.Management.EventPlan
{
    /// <summary>
    ///     EventPlan Overrides Model
    /// </summary>
    public class CustomerEventPlanOverride : PutCustomerEventPlanOverride
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }
    }
}
