﻿
namespace BbTS.Domain.Models.Customer.Management.EventPlan
{
    /// <summary>
    /// This object represents more or less a record in the Transact system for an event plan associcated to a customer
    /// </summary>
    public class CustomerEventPlan
    {
        /// <summary>
        /// The identity of the event plan associated with this customer
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Whether this event plan is enabled or not
        /// </summary>
        public bool Active { get; set; }
    }
}
