﻿using System;
using BbTS.Domain.Models.Definitions.Event;

namespace BbTS.Domain.Models.Customer.Management.EventPlan
{
    /// <summary>
    ///     The Event Plan Override
    /// </summary>
    public class PutCustomerEventPlanOverride
    {
        /// <summary>
        /// Gets or sets the regular accesses.
        /// </summary>
        /// <value>
        /// The regular accesses.
        /// </value>
        public int? RegularAccesses { get; set; }

        /// <summary>
        /// Gets or sets the guest accesses.
        /// </summary>
        /// <value>
        /// The guest accesses.
        /// </value>
        public int? GuestAccesses { get; set; }

        /// <summary>
        /// Gets or sets the type of the event override duration limit.
        /// </summary>
        /// <value>
        /// The type of the event override duration limit.
        /// </value>
        public EventLimitType EventOverrideDurationLimitType { get; set; }

        /// <summary>
        /// Gets or sets the reset date time.
        /// </summary>
        /// <value>
        /// The reset date time.
        /// </value>
        public DateTime? ResetDateTime { get; set; }
    }
}
