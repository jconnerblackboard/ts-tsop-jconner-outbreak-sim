﻿using System;
using System.Collections.Generic;
using BbTS.Domain.Models.Commerce;

namespace BbTS.Domain.Models.Customer.Management
{
    /// <summary>
    ///     The Operation.
    /// </summary>
    public class Operation
    {
        /// <summary>
        ///     Gets or sets the Result.
        /// </summary>
        /// <value>
        ///     The Result.
        /// </value>
        public string Result { get; set; }

        /// <summary>
        /// Gets or sets the error.
        /// </summary>
        /// <value>
        /// The error.
        /// </value>
        public List<ResponseError> Errors { get; set; } = new List<ResponseError>();

        /// <summary>
        /// Gets or sets the route.
        /// </summary>
        /// <value>
        /// The route.
        /// </value>
        public Route Route { get; set; } = new Route
        {
            Institution = new Commerce.Institution
            {
                Id = new Guid().ToString("D"),
                Name = "Unknown",
                RequestValue = "Unknown",
                RequestScheme = "Unknown"

            }
        };

        /// <summary>
        ///     Gets or sets the Request Timestamp.
        /// </summary>
        /// <value>
        ///     The Request Timestamp.
        /// </value>
        public string RequestTimestampUtc { get; set; }

        /// <summary>
        ///     Gets or sets the Response Timestamp.
        /// </summary>
        /// <value>
        ///     The Response Timestamp.
        /// </value>
        public string ResponseTimestampUtc { get; set; }
    }
}
