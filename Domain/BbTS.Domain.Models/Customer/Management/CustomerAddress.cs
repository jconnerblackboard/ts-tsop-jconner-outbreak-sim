﻿using BbTS.Core.Serialization;
using System.Xml.Serialization;
using BbTS.Domain.Models.Definitions.Customer;
using Newtonsoft.Json;

namespace BbTS.Domain.Models.Customer.Management
{
    /// <summary>
    ///     The address
    /// </summary>
    public class CustomerAddress
    {
        #region Backing fields

        /// <summary>
        /// The address type
        /// </summary>
        private CustomerAddressType _addressType;

        /// <summary>
        /// The street1
        /// </summary>
        private string _street1;

        /// <summary>
        /// The street2
        /// </summary>
        private string _street2;

        /// <summary>
        /// The city
        /// </summary>
        private string _city;

        /// <summary>
        /// The state
        /// </summary>
        private string _state;

        /// <summary>
        /// The postal code
        /// </summary>
        private string _postalCode;

        /// <summary>
        /// The country
        /// </summary>
        private string _country;

        /// <summary>
        /// The phone number1
        /// </summary>
        private string _phoneNumber1;

        /// <summary>
        /// The phone number2
        /// </summary>
        private string _phoneNumber2;

        /// <summary>
        /// The notes
        /// </summary>
        private string _notes;

        /// <summary>
        /// The name
        /// </summary>
        private string _name;

        /// <summary>
        /// The relationship
        /// </summary>
        private string _relationship;

        #endregion

        #region public properties

        /// <summary>
        /// Gets or sets the type of the address.
        /// </summary>
        [XmlIgnore]
        [JsonConverter(typeof(CustomDefaultStringEnumConverter), (int)CustomerAddressType.Unknown, typeof(AllCapsNamingStrategy), true)]
        public CustomerAddressType AddressType
        {
            get { return _addressType; }
            set
            {
                AddressTypeIsSet = true;
                _addressType = value;
            }
        }

        /// <summary>
        /// Gets or sets the type of the address.
        /// </summary>
        [XmlElement("AddressType")]
        [JsonIgnore]
        public int AddressTypeId
        {
            get { return (int)_addressType; }
            set { _addressType = (CustomerAddressType)value; }
        }

        /// <summary>
        /// Gets or sets the street1.
        /// </summary>
        public string Street1
        {
            get { return _street1; }
            set
            {
                Street1IsSet = true;
                _street1 = value;
            }
        }

        /// <summary>
        /// Gets or sets the street2.
        /// </summary>
        public string Street2
        {
            get { return _street2; }
            set
            {
                Street2IsSet = true;
                _street2 = value;
            }
        }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        public string City
        {
            get { return _city; }
            set
            {
                CityIsSet = true;
                _city = value;
            }
        }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        public string State
        {
            get { return _state; }
            set
            {
                StateIsSet = true;
                _state = value;
            }
        }

        /// <summary>
        /// Gets or sets the postal code.
        /// </summary>
        public string PostalCode
        {
            get { return _postalCode; }
            set
            {
                PostalCodeIsSet = true;
                _postalCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        public string Country
        {
            get { return _country; }
            set
            {
                CountryIsSet = true;
                _country = value;
            }
        }

        /// <summary>
        /// Gets or sets the phone number1.
        /// </summary>
        public string PhoneNumber1
        {
            get { return _phoneNumber1; }
            set
            {
                PhoneNumber1IsSet = true;
                _phoneNumber1 = value;
            }
        }

        /// <summary>
        /// Gets or sets the phone number2.
        /// </summary>
        public string PhoneNumber2
        {
            get { return _phoneNumber2; }
            set
            {
                PhoneNumber2IsSet = true;
                _phoneNumber2 = value;
            }
        }

        /// <summary>
        /// Gets or sets the notes.
        /// </summary>
        public string Notes
        {
            get { return _notes; }
            set
            {
                NotesIsSet = true;
                _notes = value;
            }
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set
            {
                NameIsSet = true;
                _name = value;
            }
        }

        /// <summary>
        /// Gets or sets the relation ship.
        /// </summary>
        public string Relationship
        {
            get { return _relationship; }
            set
            {
                RelationshipIsSet = true;
                _relationship = value;
            }
        }

        #endregion

        #region internal properties indicating which fields were set through request

        /// <summary>
        /// Gets or sets a value indicating whether [address type is set].
        /// </summary>
        internal bool AddressTypeIsSet { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [street1 is set].
        /// </summary>
        internal bool Street1IsSet { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [street2 is set].
        /// </summary>
        internal bool Street2IsSet { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [city is set].
        /// </summary>
        internal bool CityIsSet { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [state is set].
        /// </summary>
        internal bool StateIsSet { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [postal code is set].
        /// </summary>
        internal bool PostalCodeIsSet { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [country is set].
        /// </summary>
        internal bool CountryIsSet { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [phone number1 is set].
        /// </summary>
        internal bool PhoneNumber1IsSet { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [phone number2 is set].
        /// </summary>
        internal bool PhoneNumber2IsSet { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [notes is set].
        /// </summary>
        internal bool NotesIsSet { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [name is set].
        /// </summary>
        internal bool NameIsSet { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [relationship is set].
        /// </summary>
        internal bool RelationshipIsSet { get; set; }

        #endregion
    }
}
