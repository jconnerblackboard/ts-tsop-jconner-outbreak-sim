﻿using System;

namespace BbTS.Domain.Models.Customer.Management.BoardPlan
{
    /// <summary>
    ///     The customer board plan
    /// </summary>
    public class PatchCustomerBoardPlan
    {
        #region Backing Fields

        /// <summary>
        /// The priority
        /// </summary>
        private int? _priority;

        /// <summary>
        /// The active
        /// </summary>
        private bool? _active;

        /// <summary>
        /// The override plan start date
        /// </summary>
        private DateTime? _overridePlanStartDate;

        /// <summary>
        /// The override plan stop date
        /// </summary>
        private DateTime? _overridePlanStopDate;

        /// <summary>
        /// The Point of Sale option show counts
        /// </summary>
        private bool? _posOptionShowCounts;

        /// <summary>
        /// The Point of Sale option print counts
        /// </summary>
        private bool? _posOptionPrintCounts;

        #endregion

        #region public properties

        /// <summary>
        /// Gets or sets the new board plan identifier.
        /// </summary>
        public int NewBoardPlanId { get; set; }

        /// <summary>
        /// Gets or sets the priority.
        /// </summary>
        public int? Priority
        {
            get { return _priority; }
            set
            {
                _priority = value;
                PriorityIsSet = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="CustomerManagementBoardPlan"/> is active.
        /// </summary>
        public bool? Active
        {
            get { return _active; }
            set
            {
                _active = value;
                ActiveIsSet = true;
            }
        }

        /// <summary>
        /// Gets or sets the override plan start date.
        /// </summary>
        public DateTime? OverridePlanStartDate
        {
            get { return _overridePlanStartDate; }
            set
            {
                _overridePlanStartDate = value;
                OverridePlanStartDateIsSet = true;
            }
        }

        /// <summary>
        /// Gets or sets the override plan stop date.
        /// </summary>
        public DateTime? OverridePlanStopDate
        {
            get { return _overridePlanStopDate; }
            set
            {
                _overridePlanStopDate = value;
                OverridePlanStopDateIsSet = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [Point of Sale option show counts].
        /// </summary>
        public bool? PosOptionShowCounts
        {
            get { return _posOptionShowCounts; }
            set
            {
                _posOptionShowCounts = value;
                PosOptionShowCountsIsSet = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [Point of Sale option print counts].
        /// </summary>
        public bool? PosOptionPrintCounts
        {
            get { return _posOptionPrintCounts; }
            set
            {
                _posOptionPrintCounts = value;
                PosOptionPrintCountsIsSet = true;
            }
        }

        #endregion

        #region internal properties indicating which field was set through request

        /// <summary>
        /// Gets or sets a value indicating whether [priority is set].
        /// </summary>
        public bool PriorityIsSet { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether [active is set].
        /// </summary>
        public bool ActiveIsSet { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether [override plan start date is set].
        /// </summary>
        public bool OverridePlanStartDateIsSet { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether [override plan stop date is set].
        /// </summary>
        public bool OverridePlanStopDateIsSet { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether [position option show counts is set].
        /// </summary>
        public bool PosOptionShowCountsIsSet { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether [position option print counts is set].
        /// </summary>
        public bool PosOptionPrintCountsIsSet { get; private set; }

        #endregion
    }
}
