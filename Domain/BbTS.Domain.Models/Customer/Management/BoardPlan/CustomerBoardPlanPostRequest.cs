﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Customer.Management.BoardPlan
{
    /// <summary>
    /// Container class for a call to the domain layer to handle a customer board plan post request.
    /// </summary>
    public class CustomerBoardPlanPostRequest
    {
        /// <summary>
        /// The uniqiue identifier for the request.
        /// </summary>
        public string RequestId { get; set; } = Guid.NewGuid().ToString("D");

        /// <summary>
        /// the customer number associated with the request.
        /// </summary>
        public string CustomerNumber { get; set; }

        /// <summary>
        /// The body of the post request serialized to object.
        /// </summary>
        public PostCustomerBoardPlan BoardPlanPostRequestBody { get; set; }

        /// <summary>
        /// The request scheme.
        /// </summary>
        public string RequestScheme { get; set; }

        /// <summary>
        /// The request value.
        /// </summary>
        public string RequestValue { get; set; }
        }
}
