﻿using System;

namespace BbTS.Domain.Models.Customer.Management.BoardPlan
{
    public class CustomerBoardPlanGetRequest
    {
        /// <summary>
        /// The uniqiue identifier for the request.
        /// </summary>
        public string RequestId { get; set; } = Guid.NewGuid().ToString("D");

        /// <summary>
        /// the customer number associated with the request.
        /// </summary>
        public string CustomerNumber { get; set; }

        /// <summary>
        /// Id of the board plan to fetch (null if fetch all)
        /// </summary>
        public int ? BoardPlanId { get; set; }

        /// <summary>
        /// The request scheme.
        /// </summary>
        public string RequestScheme { get; set; }

        /// <summary>
        /// The request value.
        /// </summary>
        public string RequestValue { get; set; }
    }
}

