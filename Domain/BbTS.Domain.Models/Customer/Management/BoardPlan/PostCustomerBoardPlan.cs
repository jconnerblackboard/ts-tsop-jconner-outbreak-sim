﻿using System;

namespace BbTS.Domain.Models.Customer.Management.BoardPlan
{
    /// <summary>
    ///     The customer board plan
    /// </summary>
    public class PostCustomerBoardPlan
    {
        /// <summary>
        /// Gets or sets the board plan identifier.
        /// </summary>
        /// <value>
        /// The board plan identifier.
        /// </value>
        public int BoardPlanId { get; set; }

        /// <summary>
        /// Gets or sets the priority.
        /// </summary>
        /// <value>
        /// The priority.
        /// </value>
        public int Priority { get; set; } = 1;

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="CustomerManagementBoardPlan"/> is active.
        /// </summary>
        /// <value>
        ///   <c>True</c> if active; otherwise, <c>false</c>.
        /// </value>
        public bool Active { get; set; }

        /// <summary>
        /// Gets or sets the override plan start date.
        /// </summary>
        /// <value>
        /// The override plan start date.
        /// </value>
        public DateTime? OverridePlanStartDate { get; set; }

        /// <summary>
        /// Gets or sets the override plan stop date.
        /// </summary>
        /// <value>
        /// The override plan stop date.
        /// </value>
        public DateTime? OverridePlanStopDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [position option show counts].
        /// </summary>
        /// <value>
        /// <c>True</c> if [position option show counts]; otherwise, <c>false</c>.
        /// </value>
        public bool PosOptionShowCounts { get; set; } = true;

        /// <summary>
        /// Gets or sets a value indicating whether [position option print counts].
        /// </summary>
        /// <value>
        /// <c>True</c> if [position option print counts]; otherwise, <c>false</c>.
        /// </value>
        public bool PosOptionPrintCounts { get; set; } = true;
    }
}
