﻿using System;

namespace BbTS.Domain.Models.Customer.Management.BoardPlan
{
    /// <summary>
    ///     The customer board plan
    /// </summary>
    public class CustomerManagementBoardPlan
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the priority.
        /// </summary>
        public int Priority { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="CustomerBoardPlanVerifyRequest"/> is active.
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Gets or sets the override plan start date.
        /// </summary>
        public DateTime? OverridePlanStartDate { get; set; }

        /// <summary>
        /// Gets or sets the override plan stop date.
        /// </summary>
        public DateTime? OverridePlanStopDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [position option show counts].
        /// </summary>
        public bool PosOptionShowCounts { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [position option print counts].
        /// </summary>
        public bool PosOptionPrintCounts { get; set; }
    }
}
