﻿using System;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// This object represents a Transact Customer_Def_Grp_Def object. Customer Defined Group Definition Table
    /// </summary>
    [Serializable]
    public class TsCustomer_Def_Grp_Def
    {
        /// <summary>
        /// Unique Identifier
        /// </summary>
        [XmlAttribute]
        public int Customer_Def_Grp_Def_Id { get; set; }
        /// <summary>
        /// Name of Definition
        /// </summary>
        [XmlAttribute]
        public string Name { get; set; }
        /// <summary>
        /// Indicates if the transaction will store the name group of this record (T/F)
        /// </summary>
        [XmlAttribute]
        public string Store_Per_Transaction { get; set; }
        /// <summary>
        /// Order in which the user defined field is displayed from top to bottom
        /// </summary>
        [XmlAttribute]
        public int Ui_Display_Top { get; set; }
        /// <summary>
        /// Order in which the user defined field is displayed from left to right
        /// </summary>
        [XmlAttribute]
        public int Ui_Display_Left { get; set; }
        /// <summary>
        /// User Interface Display Height
        /// </summary>
        [XmlAttribute]
        public int Ui_Display_Height { get; set; }
        /// <summary>
        /// User Interface Display Width
        /// </summary>
        [XmlAttribute]
        public int Ui_Display_Width { get; set; }
        /// <summary>
        ///
        /// </summary>
        [XmlAttribute]
        public string IsReportEnabled { get; set; }
        /// <summary>
        ///
        /// </summary>
        [XmlAttribute]
        public string ReportTemplateFieldGuid { get; set; }
        /// <summary>
        ///
        /// </summary>
        [XmlAttribute]
        public string ReportTemplateFieldAggrGuid { get; set; }
        /// <summary>
        ///
        /// </summary>
        [XmlAttribute]
        public string IsIndexed { get; set; }
    }
}
