﻿
namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// This object represents a record in the Transact system for the customer object
    /// </summary>
    public class TsCustomerListInfo
    {
        /// <summary>
        /// The PK of the customer object
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// The friendly number for the customer - suitable for downstream usage
        /// </summary>
        public string CustomerNumber { get; set; }

        /// <summary>
        /// The default card number for this customer
        /// </summary>
        public string DefaultCardNumber { get; set; }

        /// <summary>
        /// The first name of the customer
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// The middle name of the customer
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// The last name of the customer
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// A unique alternate key for this customer
        /// </summary>
        public string CustomerGuid { get; set; }
    }
}
