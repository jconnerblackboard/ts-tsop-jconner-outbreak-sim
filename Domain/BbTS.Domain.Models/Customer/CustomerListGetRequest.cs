﻿using System;
using System.Collections.Generic;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Container class for handling a get all customer request.
    /// </summary>
    public class CustomerListGetRequest
    {
        /// <summary>
        /// Min customer number
        /// </summary>
        public string CustomerNumber { get; set; }

        /// <summary>
        /// Lastname
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Firstname
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Middle name
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Min Card number
        /// </summary>
        public string CardNumber { get; set; }

        /// <summary>
        /// Offset of the result
        /// </summary>
        public int Offset { get; set; }

        /// <summary>
        /// Number of items per page
        /// </summary>
        public int? PageSize { get; set; }

        /// <summary>
        /// Formats properties as optional uri params
        /// </summary>
        /// <returns></returns>
        public string FormUriParams()
        {
            var list = new List<string>();

            if (!string.IsNullOrEmpty(CustomerNumber))
                list.Add($"{nameof(CustomerNumber)}={Uri.EscapeUriString(CustomerNumber)}");
            if (!string.IsNullOrEmpty(LastName))
                list.Add($"{nameof(LastName)}={Uri.EscapeUriString(LastName)}");
            if (!string.IsNullOrEmpty(FirstName))
                list.Add($"{nameof(FirstName)}={Uri.EscapeUriString(FirstName)}");
            if (!string.IsNullOrEmpty(MiddleName))
                list.Add($"{nameof(MiddleName)}={Uri.EscapeUriString(MiddleName)}");
            if (!string.IsNullOrEmpty(CardNumber))
                list.Add($"{nameof(CardNumber)}={Uri.EscapeUriString(CardNumber)}");
            if (Offset > 0)
                list.Add($"{nameof(Offset)}={Offset}");
            if (PageSize.HasValue)
                list.Add($"{nameof(PageSize)}={PageSize}");

            return string.Join("&", list);
        }
    }
}
