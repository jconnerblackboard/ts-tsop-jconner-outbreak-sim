﻿namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    ///     Customer Defined Group Value Model
    /// </summary>
    public class CustomerDefinedGroupValue
    {
        /// <summary>
        /// Group Value Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        public int Position { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public string Value { get; set; }
    }
}