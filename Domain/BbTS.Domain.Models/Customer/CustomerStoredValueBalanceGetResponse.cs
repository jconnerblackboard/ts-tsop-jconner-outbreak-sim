﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Container class for a response to a CustomerBoardBalanceGet request.
    /// </summary>
    public class CustomerStoredValueBalanceGetResponse
    {
        /// <summary>
        /// The unique (Guid) identifier for the customer.
        /// </summary>
        public string CustomerGuid { get; internal set; }

        /// <summary>
        /// The unique identifier for the device.
        /// </summary>
        public string DeviceGuid { get; internal set; }

        /// <summary>
        /// List of board plans and their balances assigned to the customer.
        /// </summary>
        public List<CustomerStoredValueBalance> StoredValueAccounts { get; set; } = new List<CustomerStoredValueBalance>();

        /// <summary>
        /// Parameterized constructor requiring the customerGuid used in the request.
        /// </summary>
        /// <param name="customerGuid">The unique (Guid) identifier for the customer.</param>
        /// <param name="deviceGuid">The unique (Guid) identifier for the device.</param>
        public CustomerStoredValueBalanceGetResponse(string customerGuid, string deviceGuid)
        {
            CustomerGuid = customerGuid;
            DeviceGuid = deviceGuid;
        }
    }
}
