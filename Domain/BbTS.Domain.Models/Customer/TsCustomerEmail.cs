﻿
namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Customer email entity
    /// </summary>
    public class TsCustomerEmail
    {
        /// <summary>
        /// Customer Id
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Email Address
        /// </summary>
        public string EmailAddress { get; set; }

        /// <summary>
        /// Default display override
        /// </summary>
        public bool DefaultDisplayOverride { get; set; }

        /// <summary>
        /// Customer email type id
        /// </summary>
        public int CustomerEmailTypeId { get; set; }

        /// <summary>
        /// Customer email type
        /// </summary>
        public TsCustomerEmailType CustomerEmailType { get; set; }
    }
}
