﻿using System;
using System.Collections.Generic;
using BbTS.Domain.Models.Transaction;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Holds results of a PolicyEvaluate operation.
    /// </summary>
    public class PolicyEvaluationResult : ProcessingResult
    {
        /// <summary>
        /// Customer GUID.
        /// </summary>
        public Guid CustomerGuid { get; set; }

        /// <summary>
        /// Available balance for the customer.
        /// </summary>
        public decimal AvailableBalance { get; set; }

        /// <summary>
        /// Specifies if tax should be charged or not.
        /// </summary>
        public bool TaxExempt { get; set; }

        /// <summary>
        /// Discount/Surcharge rules for the customer.
        /// </summary>
        public List<DiscountSurchargeRule> DiscountSurchargeRules { get; set; }

        /// <summary>
        /// Detailed log of the policy evaluation that generated this response.
        /// </summary>
        public List<string> EvaluationLog { get; set; }
    }
}