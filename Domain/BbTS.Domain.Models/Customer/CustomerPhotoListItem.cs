﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Customer
{
    public class CustomerPhotoListItem
    {
        public int CustomerId { get; set; }
        public byte[] PhotoBytes { get; set; }
        public double ModifiedDateTime { get; set; }
    }
}
