﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Customer post object
    /// </summary>
    [XmlRoot("Customer")]
    public class TsCustomerPost
    {
        /// <summary>
        /// Customer number
        /// </summary>
        public decimal CustomerNumber { get; set; }

        /// <summary>
        /// The default card number for this customer
        /// </summary>
        public decimal? DefaultCardNumber { get; set; }

        /// <summary>
        /// The first name of the customer
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// The middle name of the customer
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// The last name of the customer
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// The customer's birthdate
        /// </summary>
        public double? BirthDate { get; set; }

        /// <summary>
        /// The sex of the customer
        /// </summary>
        public string Sex { get; set; }

        /// <summary>
        /// Whether the customer is active or not
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Pin
        /// </summary>
        public string Pin { get; set; }

        /// <summary>
        /// Whether Persona "extended" door unlock is allowed or not
        /// </summary>
        public bool DoorExtendedUnlockAllowed { get; set; }

        /// <summary>
        /// The first date the customer account is considered active
        /// </summary>
        public double? ActiveStartDate { get; set; }

        /// <summary>
        /// The last date the customer account is considered active
        /// </summary>
        public double? ActiveEndDate { get; set; }

        /// <summary>
        /// Photo
        /// </summary>
        public byte[] Photo { get; set; }

        /// <summary>
        /// Indicates if photo should be set
        /// </summary>
        public bool SetPhoto { get; set; }

        /// <summary>
        /// Last user name that modified this entry
        /// </summary>
        public string LastModUserName { get; set; }

        /// <summary>
        /// Pos properties
        /// </summary>
        [XmlArray, XmlArrayItem("PosDisplayRule")]
        public List<int> PosDisplayRules { get; set; }

        /// <summary>
        /// Group value Ids
        /// </summary>
        [XmlArray, XmlArrayItem("DefinedGrpItemId")]
        public List<int> DefinedGroupItemIds { get; set; }

        /// <summary>
        /// Customer Addresses
        /// </summary>
        [XmlArray, XmlArrayItem("Address")]
        public TsAddress[] CustomerAddresses { get; set; }

        /// <summary>
        /// Defined fields
        /// </summary>
        [XmlArray, XmlArrayItem("DefinedField")]
        public List<TsCustomerPostDefinedField> DefinedFields { get; set; }

        /// <summary>
        /// Emails
        /// </summary>
        [XmlArray, XmlArrayItem("Email")]
        public List<TsCustomerPostEmail> Emails { get; set; }

        /// <summary>
        /// Cards
        /// </summary>
        [XmlArray, XmlArrayItem("Card")]
        public List<TsCustomerPostCard> Cards { get; set; }

        /// <summary>
        /// Pos message
        /// </summary>
        public TsCustomerPostMessage PosMessage { get; set; }
    }
}
