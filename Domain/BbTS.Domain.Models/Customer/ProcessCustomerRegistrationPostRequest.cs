﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Service level container object for a request to process a customer registration (from EAccounts).
    /// </summary>
    public class ProcessCustomerRegistrationPostRequest
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public string RequestId { get; set; } = Guid.NewGuid().ToString("D");

        /// <summary>
        /// Numerical identifier for the client.
        /// </summary>
        public int ClientId {get; set;}

        /// <summary>
        /// Customer card number.
        /// </summary>
        public string Cardnum {get; set;}

        /// <summary>
        /// Customer email address.
        /// </summary>
        public string Emailaddress {get; set;}

        /// <summary>
        /// The numerical identifier for the customer.
        /// </summary>
        public string Custnum {get; set;}

        /// <summary>
        /// The customer's last name.
        /// </summary>
        public string Lastname {get; set;}

        /// <summary>
        /// The customer's first name.
        /// </summary>
        public string Firstname {get; set;}

        /// <summary>
        /// The customer's middle name.
        /// </summary>
        public string Middlename { get; set; }
    }
}
