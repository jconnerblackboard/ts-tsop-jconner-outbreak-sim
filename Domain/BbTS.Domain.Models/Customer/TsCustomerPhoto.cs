﻿using System;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// This object represents a record in the Transact system for the customer photo
    /// </summary>
    public class TsCustomerPhoto
    {
        /// <summary>
        /// Firstname
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Lastname
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Active end date
        /// </summary>
        public DateTime ActiveEndDate { get; set; }

        /// <summary>
        /// Photo
        /// </summary>
        public byte[] Photo { get; set; }
    }
}
