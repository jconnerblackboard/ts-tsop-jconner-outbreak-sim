﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    ///     The Get all customer defined fields
    /// </summary>
    public class CustomerDefinedFieldsResponse
    {
        /// <summary>
        /// Gets or sets the customer defined fields.
        /// </summary>
        public List<CustomerDefinedField> CustomerDefinedFields { get; set; }

        /// <summary>
        /// Selected customer group value Ids
        /// </summary>
        public List<int> SelectedCustomerGroupValueIds { get; set; }
    }
}
