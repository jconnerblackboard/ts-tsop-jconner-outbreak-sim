﻿
namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Container class for handling a server response to a get customer request.
    /// </summary>
    public class CustomerGetResponse
    {
        /// <summary>
        /// Customer number
        /// </summary>
        public string CustomerGuid { get; set; }

        /// <summary>
        /// Customer
        /// </summary>
        public TsCustomer Customer { get; set; }
    }
}
