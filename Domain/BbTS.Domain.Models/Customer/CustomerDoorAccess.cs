﻿using BbTS.Domain.Models.Definitions.Door;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Customer Door Access
    /// </summary>
    public class CustomerDoorAccess
    {
        /// <summary>
        /// Boolean string of schedule permission
        /// </summary>
        public List<CustomerDaPermissionTimeSchedule> PermissionDayScheduleDow1 { get; set; }

        /// <summary>
        /// Boolean string of schedule permission
        /// </summary>
        public List<CustomerDaPermissionTimeSchedule> PermissionDayScheduleDow2 { get; set; }

        /// <summary>
        /// Boolean string of schedule permission
        /// </summary>
        public List<CustomerDaPermissionTimeSchedule> PermissionDayScheduleDow3 { get; set; }

        /// <summary>
        /// Boolean string of schedule permission
        /// </summary>
        public List<CustomerDaPermissionTimeSchedule> PermissionDayScheduleDow4 { get; set; }

        /// <summary>
        /// Boolean string of schedule permission
        /// </summary>
        public List<CustomerDaPermissionTimeSchedule> PermissionDayScheduleDow5 { get; set; }

        /// <summary>
        /// Boolean string of schedule permission
        /// </summary>
        public List<CustomerDaPermissionTimeSchedule> PermissionDayScheduleDow6 { get; set; }

        /// <summary>
        /// Boolean string of schedule permission
        /// </summary>
        public List<CustomerDaPermissionTimeSchedule> PermissionDayScheduleDow7 { get; set; }

        /// <summary>
        /// Door state schedule
        /// </summary>
        public List<CustomerDaStateTimeSchedule> DoorStateSchedule { get; set; }

        /// <summary>
        /// Door state day schedule
        /// </summary>
        public List<CustomerDaStateTimeSchedule> DoorStateDayScheduleDow1 { get; set; }

        /// <summary>
        /// Door state day schedule
        /// </summary>
        public List<CustomerDaStateTimeSchedule> DoorStateDayScheduleDow2 { get; set; }

        /// <summary>
        /// Door state day schedule
        /// </summary>
        public List<CustomerDaStateTimeSchedule> DoorStateDayScheduleDow3 { get; set; }

        /// <summary>
        /// Door state day schedule
        /// </summary>
        public List<CustomerDaStateTimeSchedule> DoorStateDayScheduleDow4 { get; set; }

        /// <summary>
        /// Door state day schedule
        /// </summary>
        public List<CustomerDaStateTimeSchedule> DoorStateDayScheduleDow5 { get; set; }

        /// <summary>
        /// Door state day schedule
        /// </summary>
        public List<CustomerDaStateTimeSchedule> DoorStateDayScheduleDow6 { get; set; }

        /// <summary>
        /// Door state day schedule
        /// </summary>
        public List<CustomerDaStateTimeSchedule> DoorStateDayScheduleDow7 { get; set; }

        /// <summary>
        /// Error code
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// Creates state time schedule from string format
        /// </summary>
        /// <param name="timeSchedule">Time of day schedule</param>
        /// <returns>State time schedule</returns>
        public static List<CustomerDaStateTimeSchedule> StatesFromString(string timeSchedule)
        {
            var list = new List<CustomerDaStateTimeSchedule>();
            foreach (var item in timeSchedule?.Split(';'))
            {
                var schedule = item.Split(',');
                if (schedule.Length == 2 && (!string.IsNullOrEmpty(schedule[0]) || !string.IsNullOrEmpty(schedule[1])))
                {
                    list.Add(new CustomerDaStateTimeSchedule()
                    {
                        StartingMinute = (int)DateTime.ParseExact(schedule[0].PadLeft(4, '0'), "HHmm", CultureInfo.InvariantCulture).TimeOfDay.TotalMinutes,
                        State = (DoorControlMode)Enum.Parse(typeof(DoorControlMode), schedule[1])
                    });
                }
            }

            return list;
        }

        /// <summary>
        /// Creates state time schedule from string format
        /// </summary>
        /// <param name="permissions">Permissions for every minute in string</param>
        /// <returns>State time schedule</returns>
        public static List<CustomerDaPermissionTimeSchedule> PermissionsFromBinaryString(string permissions)
        {
            var list = new List<CustomerDaPermissionTimeSchedule>();

            var minutes = 0;
            while (!string.IsNullOrEmpty(permissions))
            {
                var count = permissions.TakeWhile(x => x == permissions.First()).Count();
                list.Add(new CustomerDaPermissionTimeSchedule()
                {
                    StartingMinute = minutes,
                    Allowed = permissions.First() != '0'
                });

                minutes += count;
                permissions = permissions.Substring(count);
            }

            return list;
        }
    }
}
