﻿using System;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Container class for handling a server response to a get customer board used request.
    /// </summary>
    public class CustomerBoardUsedGetResponse
    {
        /// <summary>
        /// Customer guid
        /// </summary>
        public string CustomerGuid { get; set; }

        /// <summary>
        /// Board guid
        /// </summary>
        public string BoardPlanGuid { get; set; }

        /// <summary>
        /// Customer
        /// </summary>
        public TsCustomerBoardDetailUsed CustomerBoardUsed { get; set; }
    }
}
