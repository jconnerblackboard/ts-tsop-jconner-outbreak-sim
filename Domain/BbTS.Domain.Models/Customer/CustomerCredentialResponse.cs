﻿using System;
using BbTS.Domain.Models.System;

namespace BbTS.Domain.Models.Customer
{
    public class CustomerCredentialResponse : IEquatable<CustomerCredentialResponse>
    {
        public ActionResultToken ActionResultToken { get; set; }
        public string Credential { get; set; }

        public bool Equals(CustomerCredentialResponse other)
        {
            return
                Credential == other.Credential && 
                ActionResultToken == other.ActionResultToken;
        }
    }
}
