﻿using BbTS.Domain.Models.Definitions.Customer;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Customer Gender Resolver for setting Customer Gender which is of Integer Type  based on value of CustomerDataEntity column Sex of Type Char in database.
    /// </summary>
    public static class CustomerGenderResolver
    {
        /// <summary>
        /// Get Sex Numeric Identifier 
        /// </summary>
        /// <param name="sex">sex</param>
        /// <returns>Numeric identifier</returns>
        public static int ResolveGender(string sex)
        {
            switch (sex)
            {
                case "M":
                    return 1;
                case "F":
                    return 2;
                case "N":
                    return 0;
                default:
                    return 9;
            }
        }

        /// <summary>
        /// Get Sex Enum Identifier 
        /// </summary>
        /// <param name="sex">sex</param>
        /// <returns>Enum identifier</returns>
        public static CustomerGender SexToEnum(string sex)
        {
            switch (sex)
            {
                case "M":
                    return CustomerGender.Male;
                case "F":
                    return CustomerGender.Female;
                default:
                    return CustomerGender.Unspecified;
            }
        }

        /// <summary>
        /// Get Sex String Identifier 
        /// </summary>
        /// <param name="gender">gender</param>
        /// <returns>String identifier</returns>
        public static string EnumToSex(CustomerGender gender)
        {
            switch (gender)
            {
                case CustomerGender.Male:
                    return "M";
                case CustomerGender.Female:
                    return "F";
                default:
                    return "N";
            }
        }

        /// <summary>
        /// Get Sex String Identifier
        /// </summary>
        /// <param name="gender">gender</param>
        /// <returns>String identifier</returns>
        public static string ResloveSex(int? gender)
        {
            switch (gender)
            {
                case 1:
                    return "M";
                case 2:
                    return "F";
                case 0:
                    return "N";
                default:
                    return null;
            }
        }
    }
}
