﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BbTS.Domain.Models.Customer
{
    public class CustomerThumbnailUpdatesResponse
    {
        [XmlArrayItem(ElementName = "CustomerId")]
        public List<int> CustomerIds { get; set; } 
    }
}
