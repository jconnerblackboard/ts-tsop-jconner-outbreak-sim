﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Customer addresses get response container
    /// </summary>
    public class CustomerAddressesGetResponse
    {
        /// <summary>
        /// Customer addresses
        /// </summary>
        public List<TsAddress> CustomerAddresses { get; set; }
    }
}
