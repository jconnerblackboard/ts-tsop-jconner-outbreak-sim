﻿using System;
using System.Collections.Generic;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Customer board exclusion post request container
    /// </summary>
    public class CustomerBoardExclusionsPostRequest
    {
        /// <summary>
        /// Request Id
        /// </summary>
        public Guid? RequestId { get; set; }

        /// <summary>
        /// Transaction numbers
        /// </summary>
        public List<int> TransactionNumbers { get; set; }

        /// <summary>
        /// Exclusion enabled
        /// </summary>
        public bool ExclusionEnabled { get; set; }
    }
}
