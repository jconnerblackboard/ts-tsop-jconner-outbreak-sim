﻿
namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Customer board plan
    /// </summary>
    public class TsCustomerBoardPlanPost
    {
        /// <summary>
        /// Board plan Id
        /// </summary>
        public int BoardPlanId { get; set; }

        /// <summary>
        /// Priority
        /// </summary>
        public int Priority { get; set; }
    }
}
