﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Service level container object for the response to a request to process a customer registration (from EAccounts).
    /// </summary>
    public class ProcessCustomerRegistrationPostResponse
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// Gets or sets the error code.
        /// </summary>
        /// <value>The error code.</value>
        public int ErrorCode { get; set; }

        /// <summary>
        /// User message associated with the result of the request.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets a value indicating whether this instance has error.
        /// </summary>
        /// <value><c>true</c> if this instance has error; otherwise, <c>false</c>.</value>
        public bool HasError => ErrorCode != 0;

        /// <summary>
        /// Gets or sets the registered external client GUID.
        /// </summary>
        public Guid? ExternalClientGuid { get; set; }

        /// <summary>
        /// The unique identifier for the customer.
        /// </summary>
        public Guid? CustomerGuid { get; set; }
    }
}
