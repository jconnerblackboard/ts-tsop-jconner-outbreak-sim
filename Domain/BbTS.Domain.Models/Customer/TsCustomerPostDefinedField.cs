﻿
namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Customer post defined field object
    /// </summary>
    public class TsCustomerPostDefinedField
    {
        /// <summary>
        /// Defined Field Id
        /// </summary>
        public int DefinedFieldId { get; set; }

        /// <summary>
        /// Defined Field Value
        /// </summary>
        public string DefinedFieldValue { get; set; }
    }
}
