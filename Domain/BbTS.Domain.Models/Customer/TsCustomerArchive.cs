﻿using System;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// This object represents a Transact customer object. Main Table for Customer Setup
    /// </summary>
    [Serializable]
    public class TsCustomerArchive
    {
        /// <summary>
        /// System Generated Customer ID Number
        /// </summary>
        [XmlAttribute]
        public int Cust_Id { get; set; }
        /// <summary>
        /// Customer Number
        /// </summary>
        [XmlAttribute]
        public string CustNum { get; set; }
        /// <summary>
        /// Primary Card Number
        /// </summary>
        [XmlAttribute]
        public string DefaultCardNum { get; set; }
        /// <summary>
        /// First Name
        /// </summary>
        [XmlAttribute]
        public string FirstName { get; set; }
        /// <summary>
        /// Middle Name
        /// </summary>
        [XmlAttribute]
        public string MiddleName { get; set; }
        /// <summary>
        /// Last Name
        /// </summary>
        [XmlAttribute]
        public string LastName { get; set; }
        /// <summary>
        /// Birthday
        /// </summary>
        [XmlAttribute]
        public DateTime BirthDate { get; set; }
        /// <summary>
        /// Customer Sex (M-Male, F-Female, N-Not Specified)
        /// </summary>
        [XmlAttribute]
        public string Sex { get; set; }
        /// <summary>
        /// System PIN for customer
        /// </summary>
        [XmlAttribute]
        public int PinNumber { get; set; }
        /// <summary>
        /// Determines if the customer is active (T/F)
        /// </summary>
        [XmlAttribute]
        public string Is_Active { get; set; }
        /// <summary>
        /// The date in which the customer becomes active.  A null value means since the beginning of time.
        /// </summary>
        [XmlAttribute]
        public DateTime Active_Start_Date { get; set; }
        /// <summary>
        /// The ending date in which the customer is active.  A null value means all dates after the start date
        /// </summary>
        [XmlAttribute]
        public DateTime Active_End_Date { get; set; }
        /// <summary>
        /// Date/Time the customer was added to the system
        /// </summary>
        [XmlAttribute]
        public DateTime OpenDateTime { get; set; }
        /// <summary>
        /// Last Modified Date/Time
        /// </summary>
        [XmlAttribute]
        public DateTime LastMod_DateTime { get; set; }
        /// <summary>
        /// Last Modified User Name
        /// </summary>
        [XmlAttribute]
        public string LastMod_UserName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public string DoorExtendedUnlockAllowed { get; set; }
        /// <summary>
        /// The customer number represented as a integer based value
        /// </summary>
        [XmlAttribute]
        public decimal CustomerNumber { get; set; }
    }
}
