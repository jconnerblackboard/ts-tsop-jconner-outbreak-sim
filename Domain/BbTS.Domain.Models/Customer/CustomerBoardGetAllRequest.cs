﻿using System.Text;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Container class for handling a get all customer board request.
    /// </summary>
    public class CustomerBoardGetAllRequest
    {
        /// <summary>
        /// Customer guid
        /// </summary>
        public string CustomerGuid { get; set; }

        /// <summary>
        /// Board plan guid
        /// </summary>
        public string BoardPlanGuid { get; set; }

        /// <summary>
        /// Formats URI
        /// </summary>
        /// <returns></returns>
        public string FormUri()
        {
            var sb = new StringBuilder("customer");

            if (CustomerGuid != null)
                sb.Append($"/{CustomerGuid}");

            sb.Append("/board");
            if (BoardPlanGuid != null)
                sb.Append($"/{BoardPlanGuid}");

            return sb.ToString();
        }
    }
}
