﻿using System.Collections.Generic;
using BbTS.Domain.Models.Definitions.DefinedField;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    ///     Customer Defined Field Model
    /// </summary>
    public class CustomerDefinedField
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the caption.
        /// </summary>
        /// <value>
        /// The caption.
        /// </value>
        public string Caption { get; set; }

        /// <summary>
        /// Gets or sets the type of the customer defined field.
        /// </summary>
        /// <value>
        /// The type of the customer defined field.
        /// </value>
        public DefinedFieldType CustomerDefinedFieldType { get; set; }

        /// <summary>
        /// Gets or sets the field length maximum.
        /// </summary>
        /// <value>
        /// The field length maximum.
        /// </value>
        public int FieldLengthMaximum { get; set; }

        /// <summary>
        /// User Interface Position for Top Display
        /// </summary>
        public int UiDisplayTop { get; set; }

        /// <summary>
        /// User Interface Position for Left Display
        /// </summary>
        public int UiDisplayLeft { get; set; }

        /// <summary>
        /// User Interface Display Height
        /// </summary>
        public int UiDisplayHeight { get; set; }

        /// <summary>
        /// User Interface Display Width
        /// </summary>
        public int UiDisplayWidth { get; set; }

        /// <summary>
        /// Field value
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Gets or sets the group values.
        /// </summary>
        /// <value>
        /// The group values.
        /// </value>
        public List<CustomerDefinedGroupValue> GroupValues { get; set; }
    }
}
