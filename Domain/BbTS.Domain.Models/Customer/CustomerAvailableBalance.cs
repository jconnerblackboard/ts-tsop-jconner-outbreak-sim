﻿namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Customer available balance.
    /// </summary>
    public class CustomerAvailableBalance
    {
        /// <summary>
        /// Customer's available balance.
        /// </summary>
        public decimal AvailableBalance { get; set; }

        /// <summary>
        /// True if at least one of the customer's stored value accounts are tax exempt.
        /// </summary>
        public bool TaxExempt { get; set; }
    }
}
