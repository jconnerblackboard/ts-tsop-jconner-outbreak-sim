﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Container class for handling a response to a get all customers request.
    /// </summary>
    public class CustomerListGetResponse
    {
        /// <summary>
        /// Original request
        /// </summary>
        public CustomerListGetRequest Request { get; set; }

        /// <summary>
        /// Total customer count
        /// </summary>
        public int TotalCustomerCount { get; set; }

        /// <summary>
        /// Customers
        /// </summary>
        public List<TsCustomerListInfo> Customers { get; set; }
    }
}
