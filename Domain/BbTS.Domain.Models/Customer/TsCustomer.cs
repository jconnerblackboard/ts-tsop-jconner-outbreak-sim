﻿using System;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// This object represents a record in the Transact system for the customer object
    /// </summary>
    public class TsCustomer : IEquatable<TsCustomer>
    {
        /// <summary>
        /// The PK of the customer object
        /// </summary>
        public Int32? CustomerId { get; set; }
        /// <summary>
        /// The friendly number for the customer - suitable for downstream usage
        /// </summary>
        public String CustomerNumber { get; set; }
        /// <summary>
        /// The default card number for this customer
        /// </summary>
        public string DefaultCardNumber { get; set; }
        /// <summary>
        /// The first name of the customer
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// The middle name of the customer
        /// </summary>
        public string MiddleName { get; set; }
        /// <summary>
        /// The last name of the customer
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// The customer's birthdate
        /// </summary>
        public DateTime? BirthDate { get; set; }
        /// <summary>
        /// The sex of the customer
        /// </summary>
        public string Sex { get; set; }
        /// <summary>
        /// The pin number of the customer
        /// </summary>
        public int PinNumber { get; set; }
        /// <summary>
        /// Whether the customer is active or not
        /// </summary>
        public bool IsActive { get; set; }
        /// <summary>
        /// The first date the customer account is considered active
        /// </summary>
        public DateTime? ActiveStartDate { get; set; }
        /// <summary>
        /// The last date the customer account is considered active
        /// </summary>
        public DateTime? ActiveEndDate { get; set; }
        /// <summary>
        /// Date/Time the customer was added to the system
        /// </summary>
        public DateTime OpenDateTime { get; set; }
        /// <summary>
        /// The last date/time this record was modified
        /// </summary>
        public DateTime LastModifiedDateTime { get; set; }
        /// <summary>
        /// The identity of the last user to modify this customer
        /// </summary>
        public string LastModifiedUsername { get; set; }
        /// <summary>
        /// Whether Persona "extended" door unlock is allowed or not
        /// </summary>
        public bool DoorExtendedUnlockAllowed { get; set; }
        /// <summary>
        /// A unique alternate key for this customer
        /// </summary>
        public string CustomerGuid { get; set; }

        /// <summary>
        /// Customer message text.
        /// </summary>
        public string MessageText { get; set; }

        /// <summary>
        /// Show attended account balance.
        /// </summary>
        public bool AttendedShowAcctBalance { get; set; }

        /// <summary>
        /// Print account balance when attended.
        /// </summary>
        public bool AttendedPrintAcctBalance { get; set; }

        /// <summary>
        /// Show unattended account balance.
        /// </summary>
        public bool UnattendedShowAcctBalance { get; set; }

        /// <summary>
        /// Print unattended account balance.
        /// </summary>
        public bool UnattendedPrintAcctBalance { get; set; }

        /// <summary>
        /// Show customer name.
        /// </summary>
        public bool CustomerShowName { get; set; }

        /// <summary>
        /// Show customer's birthday.
        /// </summary>
        public bool CustomerShowBirthday { get; set; }

        /// <summary>
        /// Print customer's name.
        /// </summary>
        public bool CustomerPrintName { get; set; }

        /// <summary>
        /// Print customer number.
        /// </summary>
        public bool CustomerPrintNumber { get; set; }

        /// <summary>
        /// Print customer card number.
        /// </summary>
        public bool CustomerPrintCardNumber { get; set; }

        /// <summary>
        /// Equality check.
        /// </summary>
        /// <param name="other">compare to</param>
        /// <returns>Equality value.</returns>
        public bool Equals(TsCustomer other)
        {
            return
                other != null &&
                CustomerId == other.CustomerId &&
                CustomerNumber == other.CustomerNumber &&
                CustomerGuid == other.CustomerGuid &&
                (DefaultCardNumber == other.DefaultCardNumber || String.IsNullOrEmpty(DefaultCardNumber) == String.IsNullOrEmpty(other.DefaultCardNumber)) &&
                FirstName == other.FirstName &&
                MiddleName == other.MiddleName &&
                LastName == other.LastName &&
                BirthDate == other.BirthDate &&
                Sex == other.Sex &&
                PinNumber == other.PinNumber &&
                IsActive == other.IsActive &&
                ActiveStartDate == other.ActiveStartDate &&
                ActiveEndDate == other.ActiveEndDate &&
                OpenDateTime == other.OpenDateTime &&
                LastModifiedUsername == other.LastModifiedUsername &&
                DoorExtendedUnlockAllowed == other.DoorExtendedUnlockAllowed;
        }
    }
}
