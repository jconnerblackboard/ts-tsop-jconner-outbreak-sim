﻿using System;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// This object represents a record in the Transact system for a CustomerBoardUsed object
    /// </summary>
    public class TsCustomerBoardDetailUsed
    {
        /// <summary>
        /// Customer Id
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Date/Time of last Board Transaction 
        /// </summary>
        public DateTime LastBoardAccess { get; set; }

        /// <summary>
        /// Number of Board Counts Used for current Week
        /// </summary>
        public int WeekUsed { get; set; }

        /// <summary>
        /// Number of Board Counts Used for current Month
        /// </summary>
        public int MonthUsed { get; set; }

        /// <summary>
        /// Number of Board Counts Used for current Semester/Qtr
        /// </summary>
        public int SemQtrUsed { get; set; }

        /// <summary>
        /// Number of Board Counts Used for current Year
        /// </summary>
        public int YearUsed { get; set; }

        /// <summary>
        /// Number of 'Extra' Board Counts Used
        /// </summary>
        public int ExtraUsed { get; set; }

        /// <summary>
        /// Number of Guest Board Counts Used total
        /// </summary>
        public int GuestUsed { get; set; }

        /// <summary>
        /// Date/Time that Week Used was last rolled
        /// </summary>
        public DateTime WeekRolled { get; set; }

        /// <summary>
        /// Date/Time that Month Used was last rolled
        /// </summary>
        public DateTime MonthRolled { get; set; }

        /// <summary>
        /// Date/Time that Semester/Qtr Used was last rolled
        /// </summary>
        public DateTime SemQtrRolled { get; set; }

        /// <summary>
        /// Date/Time that Year Used was last rolled
        /// </summary>
        public DateTime YearRolled { get; set; }

        /// <summary>
        /// Date/Time that this Board Used record was last updated
        /// </summary>
        public DateTime LastHostUpdate { get; set; }

        /// <summary>
        /// Number of Board Counts Used for Period 1
        /// </summary>
        public int UsedPer1 { get; set; }

        /// <summary>
        /// Number of Board Counts Used for Period 2
        /// </summary>
        public int UsedPer2 { get; set; }

        /// <summary>
        /// Number of Board Counts Used for Period 3
        /// </summary>
        public int UsedPer3 { get; set; }

        /// <summary>
        /// Number of Board Counts Used for Period 4
        /// </summary>
        public int UsedPer4 { get; set; }

        /// <summary>
        /// Number of Board Counts Used for Period 5
        /// </summary>
        public int UsedPer5 { get; set; }

        /// <summary>
        /// Number of Board Counts Used for Period 6
        /// </summary>
        public int UsedPer6 { get; set; }

        /// <summary>
        /// Number of Board Counts Used for Day Of Board Week 1
        /// </summary>
        public int UsedDow1 { get; set; }

        /// <summary>
        /// Number of Board Counts Used for Day Of Board Week 2
        /// </summary>
        public int UsedDow2 { get; set; }

        /// <summary>
        /// Number of Board Counts Used for Day Of Board Week 3
        /// </summary>
        public int UsedDow3 { get; set; }

        /// <summary>
        /// Number of Board Counts Used for Day Of Board Week 4
        /// </summary>
        public int UsedDow4 { get; set; }

        /// <summary>
        /// Number of Board Counts Used for Day Of Board Week 5
        /// </summary>
        public int UsedDow5 { get; set; }

        /// <summary>
        /// Number of Board Counts Used for Day Of Board Week 6
        /// </summary>
        public int UsedDow6 { get; set; }

        /// <summary>
        /// Number of Board Counts Used for Day Of Board Week 7
        /// </summary>
        public int UsedDow7 { get; set; }

        /// <summary>
        /// Board plan Id
        /// </summary>
        public int BoardPlanId { get; set; }
    }
}
