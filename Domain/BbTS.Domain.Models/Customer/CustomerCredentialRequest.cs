﻿using System;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// A CustomerCredentialRequest object
    /// </summary>
    public class CustomerCredentialRequest : IEquatable<CustomerCredentialRequest>
    {
        public string Credential { get; set; }
        public string CredentialGuid { get; set; }
        public bool MakePrimary { get; set; }
        public string CredentialIssuerIdentifier { get; set; }
        public string CredentialType { get; set; }

        public CustomerCredentialRequest Clone()
        {
            return (CustomerCredentialRequest)MemberwiseClone();
        }

        public bool Equals(CustomerCredentialRequest other)
        {
            return
                Credential == other.Credential &&
                CredentialGuid == other.CredentialGuid &&
                MakePrimary == other.MakePrimary &&
                CredentialIssuerIdentifier == other.CredentialIssuerIdentifier &&
                CredentialType == other.CredentialType;
        }
    }
}
