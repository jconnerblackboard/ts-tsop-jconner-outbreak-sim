﻿namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Container class to hold a Base64 String representation of a customer image along with some meta data.
    /// </summary>
    public class CustomerImage
    {
        /// <summary>
        /// The type of image (jpeg, gid, png, bmp).
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// The Base64 String representation of the image.
        /// </summary>
        public string Image { get; set; }
    }

    /// <summary>
    /// View for a <see cref="CustomerImage"/>.  (Version 1)
    /// </summary>
    public class CustomerImageViewV01
    {
        /// <summary>
        /// The type of image (jpeg, gid, png, bmp).
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// The Base64 String representation of the image.
        /// </summary>
        public string Image { get; set; }
    }

    /// <summary>
    /// Extension methods for <see cref="CustomerImage"/> conversion.
    /// </summary>
    public static class CustomerImageConverter
    {
        #region Version 1
        /// <summary>
        /// Returns a <see cref="CustomerImageViewV01"/> object based on this <see cref="CustomerImage"/>.
        /// </summary>
        /// <param name="customerImage"></param>
        /// <returns></returns>
        public static CustomerImageViewV01 ToCustomerImageViewV01(this CustomerImage customerImage)
        {
            if (customerImage == null) return null;

            return new CustomerImageViewV01
            {
                Type = customerImage.Type,
                Image = customerImage.Image
            };
        }

        /// <summary>
        /// Returns a <see cref="CustomerImage"/> object based on this <see cref="CustomerImageViewV01"/>.
        /// </summary>
        /// <param name="customerImageViewV01"></param>
        /// <returns></returns>
        public static CustomerImage ToCustomerImage(this CustomerImageViewV01 customerImageViewV01)
        {
            if (customerImageViewV01 == null) return null;

            return new CustomerImage
            {
                Type = customerImageViewV01.Type,
                Image = customerImageViewV01.Image
            };
        }
        #endregion
    }
}
