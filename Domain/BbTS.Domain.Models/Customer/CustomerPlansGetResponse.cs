﻿
namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Customer plans get response
    /// </summary>
    public class CustomerPlansGetResponse
    {
        /// <summary>
        /// Customer plans
        /// </summary>
        public CustomerPlans CustomerPlans { get; set; }
    }
}
