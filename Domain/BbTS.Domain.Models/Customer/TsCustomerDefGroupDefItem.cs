﻿
namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// This object represents a Transact Customer_Def_Grp_Def_Item object. Sub Group Table of Customer Defined Group Definitions
    /// </summary>
    public class TsCustomerDefGroupDefItem
    {
        /// <summary>
        /// Customer Defined Group Definition Item Identifier
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Customer Defined Group Definition Association (from CUSTOMER_DEF_GRP_DEF table)
        /// </summary>
        public int CustomerDefGroupDefId { get; set; }
        /// <summary>
        /// Name of Subgroup
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Position of Sub Group within the Group
        /// </summary>
        public int Position { get; set; }
    }
}
