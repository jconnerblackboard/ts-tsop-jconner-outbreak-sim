﻿using System;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// This object represents a more detailed record in the Transact system for a board plan associcated to a customer
    /// </summary>
    public class TsCustomerBoardDetail : TsCustomerBoard
    {
        /// <summary>
        /// Customer number
        /// </summary>
        public Int64 CustomerNumber { get; set; }

        /// <summary>
        /// Customer guid
        /// </summary>
        public string CustomerGuid { get; set; }
    }
}
