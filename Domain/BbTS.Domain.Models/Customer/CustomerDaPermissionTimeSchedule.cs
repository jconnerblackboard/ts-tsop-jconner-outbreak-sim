﻿
namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Customer Door Access
    /// </summary>
    public class CustomerDaPermissionTimeSchedule
    {
        /// <summary>
        /// Time of day (in minutes)
        /// </summary>
        public int StartingMinute { get; set; }

        /// <summary>
        /// Allowed
        /// </summary>
        public bool Allowed { get; set; }
    }
}
