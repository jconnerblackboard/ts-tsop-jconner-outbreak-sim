﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BbTS.Domain.Models.Customer
{
    public class CustomerMobileAttributes : IEquatable<CustomerMobileAttributes>
    {
        public bool Allowed { get; set; }
// ReSharper disable once InconsistentNaming : the proper name is IIN as it is an acronym
        public string IINPool { get; set; }
        public string CardType { get; set; }
        public string ReferenceText { get; set; }

        public bool Equals(CustomerMobileAttributes other)
        {
            return
                Allowed == other.Allowed &&
                IINPool == other.IINPool &&
                CardType == other.CardType &&
                ReferenceText == other.ReferenceText;
        }
    }
}
