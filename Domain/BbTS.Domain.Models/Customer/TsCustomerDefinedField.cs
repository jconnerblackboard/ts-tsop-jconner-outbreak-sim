﻿using System.Collections.Generic;
using BbTS.Domain.Models.Definitions.DefinedField;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    ///     Customer Defined Field Model
    /// </summary>
    public class TsCustomerDefinedField
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the caption.
        /// </summary>
        /// <value>
        /// The caption.
        /// </value>
        public string Caption { get; set; }

        /// <summary>
        /// Gets or sets the type of the customer defined field.
        /// </summary>
        /// <value>
        /// The type of the customer defined field.
        /// </value>
        public DefinedFieldType CustomerDefinedFieldType { get; set; }

        /// <summary>
        /// Gets or sets the field length maximum.
        /// </summary>
        /// <value>
        /// The field length maximum.
        /// </value>
        public int FieldLengthMaximum { get; set; }

        /// <summary>
        /// Gets or sets the group values.
        /// </summary>
        /// <value>
        /// The group values.
        /// </value>
        public List<TsCustomerDefinedGroupValue> GroupValues { get; set; }
    }
}
