﻿using System.Collections.Generic;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Container class for handling a response to a get all customer board request.
    /// </summary>
    public class CustomerBoardGetAllResponse
    {
        /// <summary>
        /// Original request
        /// </summary>
        public CustomerBoardGetAllRequest Request { get; set; }

        /// <summary>
        /// Customer board details used
        /// </summary>
        public List<TsCustomerBoardDetail> CustomerBoards { get; set; }
    }
}
