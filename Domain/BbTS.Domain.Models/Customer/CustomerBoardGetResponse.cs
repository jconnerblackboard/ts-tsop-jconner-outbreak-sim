﻿
namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Container class for handling a server response to a get customer board request.
    /// </summary>
    public class CustomerBoardGetResponse
    {
        /// <summary>
        /// Customer guid
        /// </summary>
        public string CustomerGuid { get; set; }

        /// <summary>
        /// Board guid
        /// </summary>
        public string BoardPlanGuid { get; set; }

        /// <summary>
        /// Customer
        /// </summary>
        public TsCustomerBoardDetail CustomerBoard { get; set; }
    }
}
