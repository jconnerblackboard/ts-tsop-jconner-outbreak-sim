﻿using System;
using System.Collections.Generic;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Customer DA Plans post request container
    /// </summary>
    public class CustomerAccessPlansPostRequest
    {
        /// <summary>
        /// Request Id
        /// </summary>
        public Guid? RequestId { get; set; }

        /// <summary>
        /// Door access plan ids
        /// </summary>
        public List<int> DoorAccessPlanIds { get; set; }
    }
}
