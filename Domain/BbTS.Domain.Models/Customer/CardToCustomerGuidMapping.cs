﻿namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Container class that maps a card number/issue code to a customer guid.  CustomerId included for convenience.
    /// </summary>
    public class CardToCustomerGuidMapping
    {
        /// <summary>
        /// Customer card number.
        /// </summary>
        public string CardNumber { get; set; }

        /// <summary>
        /// Customer issue code.
        /// </summary>
        public string IssueCode { get; set; }

        /// <summary>
        /// Customer guid.
        /// </summary>
        public string CustomerGuid { get; set; }

        /// <summary>
        /// Customer ID.
        /// </summary>
        public int CustomerId { get; set; }
    }
}
