﻿namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// Container class for a customer's stored value account balance.
    /// </summary>
    public class CustomerStoredValueBalance
    {
        /// <summary>
        /// The stored value account type name.
        /// </summary>
        public string SvAccountTypeName { get; set; }

        /// <summary>
        /// The account name of the stored value account.
        /// </summary>
        public string SvAccountShortName { get; set; }

        /// <summary>
        /// The account balance amount.
        /// </summary>
        public decimal Balance { get; set; }

        /// <summary>
        /// The account credit amount.
        /// </summary>
        public decimal Credit { get; set; }
    }
}
