﻿using System;
using BbTS.Domain.Models.BoardPlan;

namespace BbTS.Domain.Models.Customer
{
    /// <summary>
    /// This object represents a record in the Transact system for a board plan associcated to a customer
    /// </summary>
    public class TsCustomerBoard : TsBoardPlan
    {
        /// <summary>
        /// The identity of the custoemr
        /// </summary>
        public int CustomerId { get; set; }
        /// <summary>
        /// The home profit center Id
        /// </summary>
        public int HomeProfitCenterId { get; set; }
        /// <summary>
        /// Whether this board plan is active or not
        /// </summary>
        public new bool IsActive { get; set; }
        /// <summary>
        /// Number of 'Extra' Board Counts allowed
        /// </summary>
        public int ExtraAllowed { get; set; }
        /// <summary>
        /// Whether board counts should be shown
        /// </summary>
        public bool ShowBoardCount { get; set; }
        /// <summary>
        /// Whether board counts should be printed
        /// </summary>
        public bool PrintBoardCount { get; set; }
        /// <summary>
        /// The number of transfers available
        /// </summary>
        public int TransferAvailable { get; set; }
        /// <summary>
        /// The board plan start date
        /// </summary>
        public new DateTime StartDate { get; set; }
        /// <summary>
        /// The board plan end date
        /// </summary>
        public new DateTime EndDate { get; set; }
        /// <summary>
        /// The priority for consumption purposes
        /// </summary>
        public int Priority { get; set; }
    }
}
