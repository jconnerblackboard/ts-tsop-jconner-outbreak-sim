﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Status
{
    /// <summary>
    /// container class for a BbTS Service healthcheck item.
    /// </summary>
    public class BbTsServiceHealthcheckItem
    {
        /// <summary>
        /// Name of the check.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Status of the check.
        /// </summary>
        public string Status { get; set; }
    }
}
