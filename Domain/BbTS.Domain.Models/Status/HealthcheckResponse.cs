﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Definitions.Logging;

namespace BbTS.Domain.Models.Status
{
    /// <summary>
    /// Response object for queries to the healthcheck endpoint for BbTS.Web.Api.
    /// </summary>
    [Serializable]
    [DataContract]
    public class HealthcheckResponse
    {
        /// <summary>
        /// The date and time the healthcheck was initiated.
        /// </summary>
        [DataMember]
        public DateTime DateTimeInitiated { get; set; }

        /// <summary>
        /// The date and time the healthcheck was completed.
        /// </summary>
        [DataMember]
        public DateTime DateTimeCompleted { get; set; }

        /// <summary>
        /// List of healthcheck tests performed.
        /// </summary>
        [DataMember]
        public List<HealthcheckItem> HealthcheckItems { get; set; } = new List<HealthcheckItem>();

    }

    /// <summary>
    /// Represents an individual healtcheck test.
    /// </summary>
    [Serializable]
    [DataContract]
    public class HealthcheckItem
    {
        /// <summary>
        /// The name of the healthcheck item.  (For example: "Database Connectivity Check")
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// The result of the healthcheck. (For example: Pass, Fail)
        /// </summary>
        [DataMember]
        public string Result { get; set; }

        /// <summary>
        /// Any additional information related to the healthcheck.
        /// </summary>
        [DataMember]
        public string Message { get; set; }

        /// <summary>
        /// The event category the error will be logged under
        /// </summary>
        [DataMember]
        public short EventCategory { get; set; } = (short)LoggingDefinitions.Category.WebApi;

        /// <summary>
        /// The event category the error will be logged under
        /// </summary>
        [DataMember]
        public int EventId { get; set; }

        /// <summary>
        /// Override ToString to provide a user friendly message.
        /// </summary>
        /// <returns>Formatted string like the following:  Healthcheck Item "{Name}" had a result of "{Result}" with the message "{Message}".</returns>
        public override string ToString()
        {
            var message = $"Healthcheck Item '{Name}' had a result of '{Result}' with the message '{Message}'";
            message += message.EndsWith(".") ? "" : ".";
            return message;
        }
    }
}
