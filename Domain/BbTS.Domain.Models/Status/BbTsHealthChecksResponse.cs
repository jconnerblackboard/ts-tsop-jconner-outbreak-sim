﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Domain.Models.Status
{
    /// <summary>
    /// Response to a health check request on the btts service subsystem.
    /// </summary>
    public class BbTsHealthChecksResponse
    {
        /// <summary>
        /// Gets or sets the health checks.
        /// </summary>
        /// <value>
        /// The health checks.
        /// </value>
        public List<BbTsServiceHealthcheckItem> HealthChecks { get; set; }
    }
}
