﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.ServiceProcess;
using System.Threading;
using BbTS.Core.Configuration;
using BbTS.Core.System.Services;
using BbTS.Domain.Agent.LocationAnalysis.Code;
using BbTS.Domain.Models.Configuration;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Definitions.Service;
using BbTS.Domain.Models.Exceptions.Configuration;
using BbTS.Monitoring.Logging;

namespace BbTS.Domain.Agent.LocationAnalysis.StandAloneService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        private static void Main(string[] args)
        {
            var dir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            if (dir != null) Directory.SetCurrentDirectory(dir);

            var assemblyName = Assembly.GetExecutingAssembly().GetName().Name;
            if (args.Length >= 1)
            {
                LoggingManager.Instance.LogMessage(
                    assemblyName + ": Executing from command line.",
                    LoggingManager.LogSource,
                    (short)LoggingDefinitions.Category.Service,
                    (int)LoggingDefinitions.EventId.ServiceCommandLineExecuteMode
                    );

                var message = string.Format(assemblyName + ": operation mode={0}", args[0]);
                LoggingManager.Instance.LogMessage(
                    message,
                    LoggingManager.LogSource,
                    (short)LoggingDefinitions.Category.Service,
                    (int)LoggingDefinitions.EventId.ServiceCommandLineExecuteMode);

                switch (args[0])
                {
                    case "install":
                        BlackboardServiceInstaller.InstallService(assemblyName + ".exe");
                        break;
                    case "uninstall":
                        BlackboardServiceInstaller.UninstallService(assemblyName + ".exe");
                        break;
                    case "start":
                        BlackboardServiceInstaller.StartService(assemblyName, 3 * 60000);
                        break;
                    case "stop":
                        BlackboardServiceInstaller.StopService(assemblyName);
                        break;
                    case "usermode":
                        var retryCount = 3;
                        while (retryCount > 0 && LocationAnalysisAgentManager.Instance.ServiceStatus != ServiceStatus.Started)
                        {
                            retryCount--;
                            try
                            {
                                Configuration configuration;
                                string configLoadMessage;
                                var configPath = $"{assemblyName}.exe.config";
                                if (!CustomConfiguration.LoadCustomConfiguration(configPath, out configuration, out configLoadMessage))
                                {
                                    string errorMessage =
                                        $"{assemblyName} is unable to load the custom configuration file because: \r\n{configLoadMessage}.";
                                    LoggingManager.Instance.LogException(
                                        errorMessage,
                                        EventLogEntryType.Error,
                                        (short)LoggingDefinitions.Category.Service,
                                        (int)LoggingDefinitions.EventId.ServiceStartException);

                                    throw new CustomConfigurationException(errorMessage);
                                }

                                var key = $"{new LocationAnalysisAgent().Id()}_PathToConfig";
                                RegistryConfigurationTool.Instance.ValueSet(key, configPath);

                                if (!LocationAnalysisAgentManager.Instance.Start(CustomConfigurationModel.GenerateCustomConfigurationModel(configuration)))
                                {
                                    LoggingManager.Instance.LogException(
                                        $"{LoggingManager.LogSource} failed to start.  Trying {retryCount} more time(s).",
                                        EventLogEntryType.Error,
                                        (short)LoggingDefinitions.Category.Service,
                                        (int)LoggingDefinitions.EventId.ServiceUsermodeFailedToStart);

                                    if (retryCount > 0) Thread.Sleep(15000);
                                }
                            }
                            catch (Exception ex)
                            {
                                var prepend =
                                    $"{LoggingManager.LogSource}: Exception caught while trying to start the service.  " +
                                    $"Service will try to start {retryCount} more times.\r\n";
                                LoggingManager.Instance.LogException(
                                    ex, prepend, "", EventLogEntryType.Error,
                                    (short)LoggingDefinitions.Category.Service,
                                    (int)LoggingDefinitions.EventId.ServiceUsermodeException);
                            }
                        }
                        while (LocationAnalysisAgentManager.Instance.ServiceStatus == ServiceStatus.Started)
                        {
                            Thread.Sleep(60000);
                        }
                        break;
                    default:
                        {
                            var errorMessage =
                                $"{LoggingManager.LogSource}: UNKNOWN operation mode={args[0]}";
                            LoggingManager.Instance.LogException(
                                errorMessage, LoggingManager.LogSource, EventLogEntryType.Error,
                                (short)LoggingDefinitions.Category.Service);
                            return;
                        }
                }
            }
            else
            {
                ServiceBase[] servicesToRun = { new LocationAnalysisStandAloneService() };
                ServiceBase.Run(servicesToRun);
            }
        }
    }
}
