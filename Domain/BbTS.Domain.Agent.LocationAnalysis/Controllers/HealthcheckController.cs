﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Web.Http;
using BbTS.Core.Configuration;
using BbTS.Core.Logging.Tracing;
using BbTS.Core.Logging.Tracking;
using BbTS.Domain.Agent.LocationAnalysis.Code;
using BbTS.Domain.Models.Configuration;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Definitions.Status;
using BbTS.Domain.Models.Status;
using BbTS.Domain.Models.System.Logging.Tracking;

namespace BbTS.Domain.Agent.LocationAnalysis.Controllers
{
    /// <summary>
    /// Controller class to facilitate healthchecks on the API.
    /// </summary>
    [Trace(AttributePriority = 2)]
    public class HealthcheckController : ApiController
    {
        /// <summary>
        /// Custom Configuration object to pass to lower levels.
        /// </summary>
        public static CustomConfigurationModel CusomConfigurationModel { get; internal set; }

        /// <summary>
        /// Constructor for HistoryController.
        /// </summary>
        [Trace(AttributeExclude = true, AttributePriority = 1)]
        public HealthcheckController()
        {
            if (CusomConfigurationModel != null) return;
            
            var key = $"{new LocationAnalysisAgent().Id()}_PathToConfig";
            var appConfigPath = RegistryConfigurationTool.Instance.ValueGet(key);

            // Load the configuration file for the agent plugin
            TraceEventSource.Instance.Trace($"Looking for configuration file at '{appConfigPath}'");
            if (File.Exists(appConfigPath))
            {
                string message;
                Configuration configuration;
                if (!CustomConfiguration.LoadCustomConfiguration(appConfigPath, out configuration,
                    out message))
                {
                    EventLogTracker.Instance.Log(new EventLogTrackingObject
                    {
                        Message = $"Unable to process the App.Config file found at {appConfigPath}.  {message}",
                        Severity = EventLogEntryType.Error,
                        EventCategory = LoggingDefinitions.Category.Agent,
                        EventId = LoggingDefinitions.EventId.AgentFailedToLoadAgentPlugin
                    });
                }
                CusomConfigurationModel = CustomConfigurationModel.GenerateCustomConfigurationModel(configuration);
                LocationAnalysisAgentManager.Instance.Configuration = LocationAnalysisAgentManager.Instance.Configuration ?? CusomConfigurationModel;
                TraceEventSource.Instance.Trace($"Found {CusomConfigurationModel.Items.Count} items in {appConfigPath}");
            }
        }

        /// <summary>
        /// Request a healthcheck status from the web api service.
        /// </summary>
        /// <returns>List of <see cref="HealthcheckResponse"/> object with the results of the overall healthcheck test.</returns>
        [Route("healthcheck")]
        [HttpGet]
        public HealthcheckResponse HealthcheckRaw()
        {
            var response = new HealthcheckResponse { DateTimeInitiated = DateTime.Now };
            var items = LocationAnalysisAgentManager.Instance.Healthcheck();
            response.DateTimeCompleted = DateTime.Now;
            response.HealthcheckItems = items;
            return response;
        }
    }
}
