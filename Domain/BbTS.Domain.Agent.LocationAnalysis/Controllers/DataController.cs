﻿using System.Configuration;
using System.Diagnostics;
using System.Web.Http;
using BbTS.Core.Configuration;
using BbTS.Core.Logging.Tracing;
using BbTS.Core.Logging.Tracking;
using BbTS.Core.Security.Database;
using BbTS.Domain.Agent.LocationAnalysis.Code;
using BbTS.Domain.Agent.LocationAnalysis.Models;
using BbTS.Domain.Models.Configuration;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.System.Logging.Tracking;
using BbTS.Resource;

namespace BbTS.Domain.Agent.LocationAnalysis.Controllers
{
    /// <summary>
    /// Web API controller for managing data collection.
    /// </summary>
    [RoutePrefix("data")]
    public class DataController : ApiController
    {
        /// <summary>
        /// Custom Configuration object to pass to lower levels.
        /// </summary>
        public static CustomConfigurationModel CustomConfigurationModel { get; internal set; }

        /// <summary>
        /// Constructor for HistoryController.
        /// </summary>
        [Trace(AttributeExclude = true, AttributePriority = 1)]
        public DataController()
        {
            if (CustomConfigurationModel != null) return;

            var key = $"{new LocationAnalysisAgent().Id()}_PathToConfig";
            var appConfigPath = RegistryConfigurationTool.Instance.ValueGet(key);

            // Load the configuration file for the agent plugin
            TraceEventSource.Instance.Trace($"Looking for configuration file at '{appConfigPath}'");
            if (System.IO.File.Exists(appConfigPath))
            {
                string message;
                Configuration configuration;
                if (!CustomConfiguration.LoadCustomConfiguration(appConfigPath, out configuration,
                    out message))
                {
                    EventLogTracker.Instance.Log(new EventLogTrackingObject
                    {
                        Message = $"Unable to process the App.Config file found at {appConfigPath}.  {message}",
                        Severity = EventLogEntryType.Error,
                        EventCategory = LoggingDefinitions.Category.Agent,
                        EventId = LoggingDefinitions.EventId.AgentFailedToLoadAgentPlugin
                    });
                }
                CustomConfigurationModel = CustomConfigurationModel.GenerateCustomConfigurationModel(configuration);
                LocationAnalysisAgentManager.Instance.Configuration = LocationAnalysisAgentManager.Instance.Configuration ?? CustomConfigurationModel;
                TraceEventSource.Instance.Trace($"Found {CustomConfigurationModel.Items.Count} items in {appConfigPath}");
            }
        }

        /// <summary>
        /// Gather customer door access data and save a local copy.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Route("gather")]
        [HttpPost]
        public DataGatherResponse GatherData(DataGatherRequest request)
        {
            var database = ResourceManager.Instance.Resource(DataSource.Oracle);
            database.ConnectionString = DatabaseSecurityTool.CreateConnectionStringFromRegistry();
            return new DataDomainController(database).GatherData(request);
        }
    }
}