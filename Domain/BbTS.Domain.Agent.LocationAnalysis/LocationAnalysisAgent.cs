﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BbTS.Core.Logging.Tracing;
using BbTS.Domain.Agent.LocationAnalysis.Code;
using BbTS.Domain.Models.Agent;
using BbTS.Domain.Models.Configuration;
using BbTS.Domain.Models.Definitions.Agent;
using BbTS.Domain.Models.Definitions.Service;
using BbTS.Domain.Models.Status;

namespace BbTS.Domain.Agent.LocationAnalysis
{
    /// <summary>
    /// For the purpose of extracting and analyzing door access information to determine which customers have come in contact with potentially infected individuals
    /// </summary>
    [Serializable]
    [Trace(AttributePriority = 2)]
    public class LocationAnalysisAgent : MarshalByRefObject, IAgentPlugin
    {
        // <summary>
        /// The Display name of the plugin.
        /// </summary>
        [Trace(AttributeExclude = true, AttributePriority = 2)]
        public string DisplayName { get; set; }

        /// <summary>
        /// The custom configuration object containing configuration items deployed in the App.config file.
        /// </summary>
        [Trace(AttributeExclude = true, AttributePriority = 2)]
        public CustomConfigurationModel Configuration { get; set; }

        /// <summary>
        /// The name of the agent.
        /// </summary>
        /// <returns></returns>
        [Trace(AttributeExclude = true, AttributePriority = 2)]
        public string AgentName()
        {
            return "LocationAnalysisAgent";
        }

        /// <summary>
        /// The Unique identifier for the agent.
        /// </summary>
        /// <returns></returns>
        [Trace(AttributeExclude = true, AttributePriority = 2)]
        public string Id()
        {
            return "c10dcc84-d44d-483f-bed0-86c07555efa1";
        }

        /// <summary>
        /// The plugin description.
        /// </summary>
        /// <returns></returns>
        [Trace(AttributeExclude = true, AttributePriority = 2)]
        public string Description()
        {
            return "For the purpose of extracting door access and transaction information.";
        }

        /// <summary>
        /// The version of the plugin.
        /// </summary>
        [Trace(AttributeExclude = true, AttributePriority = 2)]
        public string Version { get; set; }

        /// <summary>
        /// Get the status of the agent.
        /// </summary>
        /// <returns></returns>
        [Trace(AttributeExclude = true, AttributePriority = 2)]
        public AgentServiceDefinitions.AgentStatus AgentStatusGet()
        {
            return AgentServiceDefinitions.AgentStatus.Unknown;
        }

        /// <summary>
        /// Load the agent and do any initialization that might be needed.
        /// </summary>
        /// <param name="message">result of the operation.</param>
        /// <returns>Success or failure of the operation.</returns>
        public bool LoadAgent(out string message)
        {
            if (LocationAnalysisAgentManager.Instance.ServiceStatus != ServiceStatus.Started)
            {
                if (!LocationAnalysisAgentManager.Instance.Start(Configuration))
                {
                    message = $"Failed to start {AgentName()}";
                    return false;
                }
            }

            message = "Success";
            return true;
        }

        /// <summary>
        /// Unload the agent and perform any cleanup that might be necessary.
        /// </summary>
        /// <param name="message">result of the operation.</param>
        /// <returns>Success or failure of the operation.</returns>
        public bool UnloadAgent(out string message)
        {
            if (!LocationAnalysisAgentManager.Instance.Stop())
            {
                message = $"Failed to stop {AgentName()}";
                return false;
            }

            message = "Success";
            return true;
        }

        /// <summary>
        /// Temporarily enable or disable the agent.
        /// </summary>
        /// <param name="value">enable = true, disable = false.</param>
        /// <param name="message">result of the operation.</param>
        /// <returns>Success or failure of the operation.</returns>
        public bool EnableAgent(bool value, out string message)
        {
            if (value)
            {
                if (LocationAnalysisAgentManager.Instance.ServiceStatus != ServiceStatus.Started)
                {
                    if (!LocationAnalysisAgentManager.Instance.Start(Configuration))
                    {
                        message = $"Failed to start {AgentName()}";
                        return false;
                    }
                }

                message = "Success";
                return true;
            }

            if (LocationAnalysisAgentManager.Instance.ServiceStatus == ServiceStatus.Started)
            {
                if (!LocationAnalysisAgentManager.Instance.Stop())
                {
                    message = $"Failed to stop {AgentName()}";
                    return false;
                }
            }

            message = "Success";
            return true;
        }

        /// <summary>
        /// Run a management interval.  The interval is bound by the manager interval.
        /// </summary>
        public void RunManagementInterval()
        {
        }

        /// <summary>
        /// Perform a healthcheck on the agent/plugin.
        /// </summary>
        /// <returns>List of healthcheck items and the results of each check.</returns>
        public List<HealthcheckItem> Healthcheck()
        {
            return LocationAnalysisAgentManager.Instance.Healthcheck();
        }
    }
}