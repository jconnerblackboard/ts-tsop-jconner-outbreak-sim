﻿using BbTS.Domain.Agent.LocationAnalysis.Code;
using BbTS.Domain.Models.Definitions.Agent;

namespace BbTS.Domain.Agent.LocationAnalysis.Models
{
    /// <summary>
    /// Container class for the response to a data gather request.
    /// </summary>
    public class DataGatherResponse
    {
        /// <summary>
        /// Result of the requested operation.
        /// </summary>
        public RequestResponseResultCode Result { get; set; }

        /// <summary>
        /// Any information about the data gathering process.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Local path to the saved data.
        /// </summary>
        public string DataPath { get; set; }

        /// <summary>
        /// Number of records that were retrieved.
        /// </summary>
        public int RecordCount { get; set; }
    }
}