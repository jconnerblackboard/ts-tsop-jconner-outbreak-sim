﻿using System;

namespace BbTS.Domain.Agent.LocationAnalysis.Models
{
    /// <summary>
    /// container class for a data gather request
    /// </summary>
    public class DataGatherRequest
    {
        /// <summary>
        /// Start time
        /// </summary>
        public DateTimeOffset StartTime { get; set; }

        /// <summary>
        /// End time
        /// </summary>
        public DateTimeOffset EndTime { get; set; }
    }
}