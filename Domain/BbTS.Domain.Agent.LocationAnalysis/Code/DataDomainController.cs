﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using BbTS.Core.Configuration;
using BbTS.Core.Conversion;
using BbTS.Core.Serialization;
using BbTS.Domain.Agent.LocationAnalysis.Models;
using BbTS.Domain.Models.Transaction;
using BbTS.Resource.Database.Abstract;
using BbTS.Resource.Service.Abstract;

namespace BbTS.Domain.Agent.LocationAnalysis.Code
{
    public class DataDomainController
    {
        private ResourceDatabase _database;

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="database"></param>
        public DataDomainController(ResourceDatabase database)
        {
            _database = database;
        }
        #region Data Gathering

        /// <summary>
        /// Gather data for the requested time span.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public DataGatherResponse GatherData(DataGatherRequest request)
        {
            // Convert to server time
            var startTime = request.StartTime.ToLocalTime();
            var endTime = request.EndTime.ToLocalTime();

            // Get the data
            var doorAccessLogs = _database.CustomerDoorAccessLogsGet(startTime.DateTime, endTime.DateTime);
            var transactionLogs = _database.CustomerTransactionLocationLogsGet(startTime.DateTime, endTime.DateTime);
            var eventLogs = _database.CustomerEventLocationLogsGet(startTime.DateTime, endTime.DateTime);
            var boardLogs = _database.CustomerBoardLocationLogsGet(startTime.DateTime, endTime.DateTime);

            var result = new List<CustomerActivityLog>();
            result.AddRange(doorAccessLogs.Select(log => new CustomerActivityLog(log)));
            result.AddRange(transactionLogs.Select(log => new CustomerActivityLog(log)));
            result.AddRange(eventLogs.Select(log => new CustomerActivityLog(log)));
            result.AddRange(boardLogs.Select(log => new CustomerActivityLog(log)));

            // Save a local copy
            var currentWorkingDirectory = Directory.GetCurrentDirectory();
            var filename = $"{currentWorkingDirectory}\\data_export\\{Formatting.GenerateUniqueName("data_export", 255)}";

            string message;
            if (!FileConfigurationManager.VerifyAndCreateDirectory($"{currentWorkingDirectory}\\data_export", out message))
            {
                return new DataGatherResponse
                {
                    Result = RequestResponseResultCode.Error,
                    Message = $"Unable to create save directory: {message}",
                    DataPath = filename,
                    RecordCount = 0
                };
            }
            
            File.WriteAllText(filename, NewtonsoftJson.Serialize(result));

            // Verify file creation.
            if (!File.Exists(filename))
            {
                return new DataGatherResponse
                {
                    Result = RequestResponseResultCode.Error,
                    Message = $"Creation of file '{filename}' failed.  Data was not saved.",
                    DataPath = filename,
                    RecordCount = 0
                };
            }

            return new DataGatherResponse
            {
                Result = RequestResponseResultCode.Success,
                Message = "Success.",
                DataPath = filename,
                RecordCount = result.Count
            };
        }

        #endregion
    }
}