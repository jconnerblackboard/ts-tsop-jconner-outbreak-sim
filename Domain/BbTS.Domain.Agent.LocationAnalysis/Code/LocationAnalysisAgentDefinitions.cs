﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BbTS.Domain.Agent.LocationAnalysis.Code
{
    /// <summary>
    /// Enumeration of result codes for a request response.
    /// </summary>
    public enum RequestResponseResultCode
    {
        /// <summary>
        /// Request was processed successfully.
        /// </summary>
        Success = 0,
        /// <summary>
        /// An error occurred during processing of the request.
        /// </summary>
        Error = 1
    }

    /// <summary>
    /// Constants for the Outbreak Analysis Agent
    /// </summary>
    public static class LocationAnalysisAgentDefinitions
    {
    }
}