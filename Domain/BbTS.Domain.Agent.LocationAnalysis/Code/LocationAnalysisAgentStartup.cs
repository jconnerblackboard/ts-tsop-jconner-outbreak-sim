﻿using System.Web.Http;
using System.Web.Http.Dispatcher;
using BbTS.Core.Logging.Tracing;
using Owin;

namespace BbTS.Domain.Agent.LocationAnalysis.Code
{
    /// <summary>
    /// Startup class.
    /// </summary>
    public class LocationAnalysisAgentStartup
    {
        /// <summary>
        /// _configuration overload to initialize OWIN self hosted site.
        /// </summary>
        /// <param name="appBuilder"></param>
        [Trace(AttributePriority = 2)]
        public void Configuration(IAppBuilder appBuilder)
        {
            // Configure Web API for self-host. 
            var config = new HttpConfiguration();

            // Set our own assembly resolver where we add the assemblies we need
            var assemblyResolver = new LocationAnalysisAgentAssemblyResolver();
            config.Services.Replace(typeof(IAssembliesResolver), assemblyResolver);

            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                "DefaultApi",
                "api/{controller}/{id}",
                new { id = RouteParameter.Optional }
            );

            appBuilder.UseWebApi(config);

            config.EnsureInitialized();
        }
    }
}