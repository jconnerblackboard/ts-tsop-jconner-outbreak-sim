﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http.Dispatcher;
using BbTS.Core.Logging.Tracing;

namespace BbTS.Domain.Agent.LocationAnalysis.Code
{
    /// <summary>
    /// Assembly resolver for the agent.
    /// </summary>
    public class LocationAnalysisAgentAssemblyResolver : DefaultAssembliesResolver
    {
        /// <summary>
        /// Load custom assemblies for this library
        /// </summary>
        /// <returns></returns>
        [Trace(AttributePriority = 2)]
        public override ICollection<Assembly> GetAssemblies()
        {
            //var basePluginDir = ApplicationConfiguration.GetKeyValueAsString("AgentPluginDirectory", ".");
            //var thisPluginDir = ApiConfigurationManager.Instance.CustomConfigurationModel.ValueGet("PluginId");

            //var pluginDllPath = $"{basePluginDir}\\{thisPluginDir}\\BbTS.Domain.Agent.Template.plug";
            //TraceEventSource.Instance.Trace($"Loading Template plugin from {pluginDllPath}");
            //var baseAssemblies = base.GetAssemblies();
            //var assemblies = new List<Assembly>(baseAssemblies);
            //var controllersAssembly = Assembly.LoadFrom(pluginDllPath);
            //baseAssemblies.Add(controllersAssembly);
            //return assemblies;

            return new[] { GetType().Assembly };
        }
    }
}