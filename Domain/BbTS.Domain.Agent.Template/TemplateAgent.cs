﻿using System;
using System.Collections.Generic;
using BbTS.Core.Logging.Tracing;
using BbTS.Domain.Agent.Template.Code;
using BbTS.Domain.Models.Agent;
using BbTS.Domain.Models.Configuration;
using BbTS.Domain.Models.Definitions.Agent;
using BbTS.Domain.Models.Definitions.Service;
using BbTS.Domain.Models.Definitions.Status;
using BbTS.Domain.Models.Status;

namespace BbTS.Domain.Agent.Template
{
    /// <summary>
    /// Sample Agent Plugin class.
    /// </summary>
    [Serializable]
    [Trace(AttributePriority = 2)]
    public class TemplateAgent : MarshalByRefObject, IAgentPlugin
    {
        /// <summary>
        /// The Display name of the plugin.
        /// </summary>
        [Trace(AttributeExclude = true, AttributePriority = 2)]
        public string DisplayName { get; set; }

        /// <summary>
        /// The custom configuration object containing configuration items deployed in the App.config file.
        /// </summary>
        [Trace(AttributeExclude = true, AttributePriority = 2)]
        public CustomConfigurationModel Configuration { get; set; }

        /// <summary>
        /// The name of the agent.
        /// </summary>
        /// <returns></returns>
        [Trace(AttributeExclude = true, AttributePriority = 2)]
        public string AgentName()
        {
            return "TemplateAgent";
        }

        /// <summary>
        /// The Unique identifier for the agent.
        /// </summary>
        /// <returns></returns>
        [Trace(AttributeExclude = true, AttributePriority = 2)]
        public string Id()
        {
            return "AA1B65AA-9615-482C-9AD6-A02197541CC1";
        }

        /// <summary>
        /// The plugin description.
        /// </summary>
        /// <returns></returns>
        [Trace(AttributeExclude = true, AttributePriority = 2)]
        public string Description()
        {
            return "An example Agent Plugin for tutorial purposes.";
        }

        /// <summary>
        /// The version of the plugin.
        /// </summary>
        [Trace(AttributeExclude = true, AttributePriority = 2)]
        public string Version { get; set; }

        /// <summary>
        /// Get the status of the agent.
        /// </summary>
        /// <returns></returns>
        [Trace(AttributeExclude = true, AttributePriority = 2)]
        public AgentServiceDefinitions.AgentStatus AgentStatusGet()
        {
            return AgentServiceDefinitions.AgentStatus.Unknown;
        }

        /// <summary>
        /// Load the agent and do any initialization that might be needed.
        /// </summary>
        /// <param name="message">result of the operation.</param>
        /// <returns>Success or failure of the operation.</returns>
        public bool LoadAgent(out string message)
        {
            if (TemplateAgentManager.Instance.ServiceStatus != ServiceStatus.Started)
            {
                if (!TemplateAgentManager.Instance.Start(Configuration))
                {
                    message = $"Failed to start {AgentName()}";
                    return false;
                }
            }

            message = "Success";
            return true;
        }

        /// <summary>
        /// Unload the agent and perform any cleanup that might be necessary.
        /// </summary>
        /// <param name="message">result of the operation.</param>
        /// <returns>Success or failure of the operation.</returns>
        public bool UnloadAgent(out string message)
        {
            if (!TemplateAgentManager.Instance.Stop())
            {
                message = $"Failed to stop {AgentName()}";
                return false;
            }

            message = "Success";
            return true;
        }

        /// <summary>
        /// Temporarily enable or disable the agent.
        /// </summary>
        /// <param name="value">enable = true, disable = false.</param>
        /// <param name="message">result of the operation.</param>
        /// <returns>Success or failure of the operation.</returns>
        public bool EnableAgent(bool value, out string message)
        {
            if (value)
            {
                if (TemplateAgentManager.Instance.ServiceStatus != ServiceStatus.Started)
                {
                    if (!TemplateAgentManager.Instance.Start(Configuration))
                    {
                        message = $"Failed to start {AgentName()}";
                        return false;
                    }
                }

                message = "Success";
                return true;
            }

            if (TemplateAgentManager.Instance.ServiceStatus == ServiceStatus.Started)
            {
                if (!TemplateAgentManager.Instance.Stop())
                {
                    message = $"Failed to stop {AgentName()}";
                    return false;
                }
            }

            message = "Success";
            return true;
        }

        /// <summary>
        /// Run a management interval.  The interval is bound by the manager interval.
        /// </summary>
        public void RunManagementInterval()
        {
        }

        /// <summary>
        /// Perform a healthcheck on the agent/plugin.
        /// </summary>
        /// <returns>List of healthcheck items and the results of each check.</returns>
        public List<HealthcheckItem> Healthcheck()
        {
            return TemplateAgentManager.Instance.Healthcheck();
        }
    }
}