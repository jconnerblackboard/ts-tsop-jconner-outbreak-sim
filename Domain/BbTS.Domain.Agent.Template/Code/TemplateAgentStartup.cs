﻿using System.Collections.Generic;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using BbTS.Core.Logging.Tracing;
using BbTS.Core.Serialization;
using Owin;

namespace BbTS.Domain.Agent.Template.Code
{
    /// <summary>
    /// Startup class for self hosted OWIN.
    /// </summary>
    public class TemplateAgentStartup
    {
        /// <summary>
        /// _configuration overload to initialize OWIN self hosted site.
        /// </summary>
        /// <param name="appBuilder"></param>
        [Trace(AttributePriority = 2)]
        public void Configuration(IAppBuilder appBuilder)
        {
            // Configure Web API for self-host. 
            HttpConfiguration config = new HttpConfiguration();

            // Set our own assembly resolver where we add the assemblies we need
            var assemblyResolver = new TemplateAgentAssemblyResolver();
            config.Services.Replace(typeof(IAssembliesResolver), assemblyResolver);

            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                "DefaultApi",
                "api/{controller}/{id}",
                new { id = RouteParameter.Optional }
            );

            appBuilder.UseWebApi(config);

            config.EnsureInitialized();
        }
    }
}