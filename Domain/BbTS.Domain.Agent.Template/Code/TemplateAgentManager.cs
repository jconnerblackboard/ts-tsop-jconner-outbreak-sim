﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.ServiceModel;
using System.Threading;
using System.Web;
using BbTS.Core;
using BbTS.Core.Configuration;
using BbTS.Core.Logging.Tracing;
using BbTS.Core.Security.Database;
using BbTS.Core.System.Services;
using BbTS.Domain.Models.Configuration;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Definitions.Persona;
using BbTS.Domain.Models.Definitions.Service;
using BbTS.Domain.Models.Definitions.Status;
using BbTS.Domain.Models.Status;
using BbTS.Domain.WebApi.Status;
using BbTS.Monitoring.Logging;
using BbTS.Resource;
using BbTS.Resource.Database.Abstract;
using BbTS.Resource.Service.Abstract;
using Microsoft.Owin.Hosting;

namespace BbTS.Domain.Agent.Template.Code
{
    /// <summary>
    /// Manager class for handling web service startup and shutdown routines.
    /// </summary>
    [Trace(AttributePriority = 2)]
    public class TemplateAgentManager
    {
        private IDisposable _server;
        private Thread _runThread;

        #region Private Properties

        private ResourceDatabase _databaseResource;
        private ResourceServiceAbstract _serviceResource;

        #endregion

        #region Public Properties
        /// <summary>
        /// Singleton instance.
        /// </summary>
        [Trace(AttributeExclude = true, AttributePriority = 1)]
        public static TemplateAgentManager Instance { get; internal set; } = new TemplateAgentManager();
        /// <summary>
        /// Status of the service.
        /// </summary>
        [Trace(AttributeExclude = true, AttributePriority = 1)]
        public ServiceStatus ServiceStatus { get; set; }
        /// <summary>
        /// Current operation state of the update management interval process.
        /// </summary>
        [Trace(AttributeExclude = true, AttributePriority = 1)]
        public IntervalOperationRunState ManagerRunState { get; set; } = IntervalOperationRunState.Stopped;
        /// <summary>
        /// The last time the management interval was run.
        /// </summary>
        [Trace(AttributeExclude = true, AttributePriority = 1)]
        public DateTime LastManagementIntervalTime { get; set; }

        /// <summary>
        /// Custom configuration object.
        /// </summary>
        [Trace(AttributeExclude = true, AttributePriority = 1)]
        public CustomConfigurationModel Configuration { get; internal set; }

        /// <summary>
        /// Get the database resource associated with this manager.
        /// </summary>
        [Trace(AttributeExclude = true, AttributePriority = 1)]
        public ResourceDatabase DatabaseResource
        {
            get
            {
                if (_databaseResource == null)
                {
                    Guard.IsNotNull(Configuration, "Configuration");

                    var databaseSource = (DataSource)Enum.Parse(
                        typeof(DataSource),
                        CustomConfiguration.GetKeyValueAsString("DataSource", Configuration, DataSource.Oracle.ToString()));

                    _databaseResource = ResourceManager.Instance.Resource(databaseSource);
                    _databaseResource.ConnectionString = DatabaseSecurityTool.CreateConnectionStringFromRegistry();
                }
                return _databaseResource;
            }
            internal set { _databaseResource = value; }
        }

        /// <summary>
        /// Get the database resource associated with this manager.
        /// </summary>
        [Trace(AttributeExclude = true, AttributePriority = 1)]
        public ResourceServiceAbstract ServiceResource
        {
            get
            {
                if (_serviceResource == null)
                {
                    Guard.IsNotNull(Configuration, "Configuration");

                    var serviceSource = (ServiceSource)Enum.Parse(
                        typeof(ServiceSource),
                        CustomConfiguration.GetKeyValueAsString("ServiceSource", Configuration, ServiceSource.Service.ToString()));

                    _serviceResource = ResourceManager.Instance.ResourceService(serviceSource);
                }
                return _serviceResource;
            }
            internal set { _serviceResource = value; }
        }

        #endregion

        #region Service Bookkeeping

        /// <summary>
        /// Start the Manager.
        /// </summary>
        /// <returns></returns>
        public bool Start(CustomConfigurationModel configuration)
        {
            Guard.IsNotNull(configuration, nameof(configuration));

            Configuration = configuration;
            int tryCount = 3;
            while (ServiceStatus != ServiceStatus.Started && tryCount > 0)
            {
                try
                {
                    tryCount--;
                    if (ServiceStatus == ServiceStatus.Started)
                    {
                        throw new ApplicationException("Attempted to start the service when it is already running.");
                    }

                    LoggingManager.Instance.LogMessage(
                        $"Starting {CustomConfiguration.GetKeyValueAsString("LogSource", Configuration, "Template Agent Service")}",
                        (short)LoggingDefinitions.Category.Service,
                        (int)LoggingDefinitions.EventId.ServiceStarting);

                    ServiceStatus = ServiceStatus.Started;
                    ManagerRunState = IntervalOperationRunState.Starting;
                    var securityMode = Configuration.ValueGet("ServiceSecurityMode");
                    WebHttpSecurityMode preamble = (WebHttpSecurityMode)Enum.Parse(typeof(WebHttpSecurityMode), securityMode);

                    var serviceBase = RegistryConfigurationTool.Instance.ValueGet("ApplicationServerHostAddress");
                    serviceBase = string.IsNullOrEmpty(serviceBase) ? "localhost" : serviceBase;
                    string baseUri = $"{ServiceUtility.GetHttpProtocolPreamble(preamble)}//{serviceBase}/TemplateAgent/api";

                    _server = WebApp.Start<TemplateAgentStartup>(baseUri);

                    _runThread = new Thread(_runLoop) { IsBackground = true };
                    _runThread.Start();

                    return true;
                }
                catch (Exception ex)
                {
                    LoggingManager.Instance.LogException(
                        ex, "", "", EventLogEntryType.Error,
                        (short)LoggingDefinitions.Category.Service,
                        (int)LoggingDefinitions.EventId.ServiceStartException);
                }
            }

            return false;
        }

        /// <summary>
        /// Stop the Update manager
        /// </summary>
        /// <returns></returns>
        public bool Stop()
        {
            try
            {
                // stop the service
                if (ServiceStatus != ServiceStatus.Stopped)
                {
                    // Log stopping message
                    LoggingManager.Instance.LogMessage(
                        $"Stopping {CustomConfiguration.GetKeyValueAsString("LogSource", Configuration, "Template Agent Service")}\r\n",
                        (short)LoggingDefinitions.Category.Service,
                        (int)LoggingDefinitions.EventId.ServiceStopping);

                    ServiceStatus = ServiceStatus.Stopping;
                    return true;
                }
            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(
                    ex, "", "", EventLogEntryType.Error,
                    (short)LoggingDefinitions.Category.Service,
                    (int)LoggingDefinitions.EventId.ServiceDsrSyncServiceShutdownException);
            }
            return false;
        }
        /// <summary>
        /// Perform a set of health checks.
        /// </summary>
        /// <returns>List of healthchecks performed as well as their results.</returns>
        public List<HealthcheckItem> Healthcheck()
        {
            var databaseCheck = new HealthcheckDomainController(Configuration, DatabaseResource, ServiceResource).ValidateDatabaseConnection();
            databaseCheck.Name = "Template Agent Database Connectivity Healthcheck";

            return new List<HealthcheckItem>
            {
                new HealthcheckItem
                {
                    Name = "Template Agent Configuration NullCheck",
                    Result = Configuration == null ? HealthcheckItemResult.Fail.ToString() : HealthcheckItemResult.Pass.ToString(),
                    Message = Configuration == null ?
                        "TemplateAgent Configuration Parameter was not initialized properly" :
                        $"TemplateAgent Configuration Parameter initialized with {Configuration.Items.Count} items."
                },
                databaseCheck
            };
        }

        /// <summary>
        /// Main run loop
        /// </summary>
        private void _runLoop()
        {
            try
            {
                // Log stopping message
                LoggingManager.Instance.LogDebugMessage(
                    $"{LoggingManager.LogSource}: Template manager Thread started.\r\n",
                    (short)LoggingDefinitions.Category.Service,
                    (int)LoggingDefinitions.EventId.ThreadStarted);

                ServiceStatus = ServiceStatus.Started;
                ManagerRunState = IntervalOperationRunState.Sleeping;

                while (ServiceStatus == ServiceStatus.Started)
                {
                    Thread.Sleep(1000);
                }
            }
            catch (ThreadInterruptedException)
            {
                var message = "Template manager Thread interrupted.  This is likely due to a shutdown in progress.";
                LoggingManager.Instance.LogDebugMessage(
                    message,
                    (short)LoggingDefinitions.Category.Service,
                    (int)LoggingDefinitions.EventId.ThreadInterrupted);
            }
            catch (ThreadAbortException)
            {
                var message = "Template manager Thread aborted.  This is likely due to a shutdown in progress.";
                LoggingManager.Instance.LogDebugMessage(
                    message,
                    (short)LoggingDefinitions.Category.Service,
                    (int)LoggingDefinitions.EventId.ThreadInterrupted);
            }

            ManagerRunState = IntervalOperationRunState.Stopped;

            // Log stopping message
            LoggingManager.Instance.LogDebugMessage(
                $"{LoggingManager.LogSource}: Template manager Thread stopped.\r\n",
                (short)LoggingDefinitions.Category.Service,
                (int)LoggingDefinitions.EventId.ThreadStopped);
        }

        #endregion
    }
}