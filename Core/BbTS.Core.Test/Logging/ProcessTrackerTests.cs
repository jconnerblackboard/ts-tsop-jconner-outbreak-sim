﻿using System;
using System.Diagnostics;
using BbTS.Core.Logging.Tracking;
using BbTS.Core.Serialization;
using BbTS.Domain.Models.Definitions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BbTS.Core.Test.Logging
{
    [TestClass]
    public class ProcessTrackerTests
    {
        //public ProcessTracker Tracker { get; set; }

        //[TestInitialize]
        //public void Initialize()
        //{
        //    Tracker = new ProcessTracker();
        //}

        [TestMethod]
        public void NodeProgressUnitTest()
        {
            ProcessTracker tracker =new ProcessTracker();
            
            string message;
            Assert.IsTrue(tracker.Begin(out message), message);

            string id;
            Assert.IsTrue(tracker.CreateAndAddNode("Test01", out id, out message), message);
            ProcessTrackingObjectState state;
            Assert.IsTrue(tracker.NodeProgressGet(id, out state, out message), message);
            Assert.IsTrue(state == ProcessTrackingObjectState.NotStarted, "Node state {0} was not equal to the expected state {1}", state, ProcessTrackingObjectState.NotStarted);

            Assert.IsTrue(tracker.StartNodeProgress(id, out message), message);
            Assert.IsTrue(tracker.NodeProgressGet(id, out state, out message), message);
            Assert.IsTrue(state == ProcessTrackingObjectState.InProgress, "Node state {0} was not equal to the expected state {1}", state, ProcessTrackingObjectState.InProgress);

            Assert.IsTrue(tracker.FinishNodeProgress(id, out message), message);
            Assert.IsTrue(tracker.NodeProgressGet(id, out state, out message), message);
            Assert.IsTrue(state == ProcessTrackingObjectState.Completed, "Node state {0} was not equal to the expected state {1}", state, ProcessTrackingObjectState.Completed);

            Assert.IsTrue(tracker.Finalize(out message), message);

            var report = tracker.ReportCreate();
            Assert.IsNotNull(report, message);

            Trace.WriteLine(String.Format("Tracker report:\r\n{0}", Xml.Serialize(report, true)));
        }

        [TestMethod]
        public void ChileNodeProgressUnitTest()
        {
            ProcessTracker tracker = new ProcessTracker();

            string message;
            Assert.IsTrue(tracker.Begin(out message), message);

            string id;
            Assert.IsTrue(tracker.CreateAndAddNode("Test01", out id, out message, null, 1), message);
            ProcessTrackingObjectState state;
            Assert.IsTrue(tracker.NodeProgressGet(id, out state, out message), message);
            Assert.IsTrue(state == ProcessTrackingObjectState.NotStarted, "Node state {0} was not equal to the expected state {1}", state, ProcessTrackingObjectState.NotStarted);

            Assert.IsTrue(tracker.StartNodeProgress(id, out message), message);
            Assert.IsTrue(tracker.NodeProgressGet(id, out state, out message), message);
            Assert.IsTrue(state == ProcessTrackingObjectState.InProgress, "Node state {0} was not equal to the expected state {1}", state, ProcessTrackingObjectState.InProgress);

            string childId;
            Assert.IsTrue(tracker.CreateAndAddNode("ChildOfTest01", out childId, out message, id), message);
            ProcessTrackingObjectState childState;
            Assert.IsTrue(tracker.NodeProgressGet(childId, out childState, out message), message);
            Assert.IsTrue(childState == ProcessTrackingObjectState.NotStarted, "Child Node state {0} was not equal to the expected state {1}", childState, ProcessTrackingObjectState.NotStarted);

            Assert.IsTrue(tracker.StartNodeProgress(childId, out message), message);
            Assert.IsTrue(tracker.NodeProgressGet(childId, out childState, out message), message);
            Assert.IsTrue(childState == ProcessTrackingObjectState.InProgress, "Child Node state {0} was not equal to the expected state {1}", childState, ProcessTrackingObjectState.InProgress);

            Assert.IsTrue(tracker.FinishNodeProgress(childId, out message), message);
            Assert.IsTrue(tracker.NodeProgressGet(childId, out childState, out message), message);
            Assert.IsTrue(childState == ProcessTrackingObjectState.Completed, "Child Node state {0} was not equal to the expected state {1}", childState, ProcessTrackingObjectState.Completed);

            Assert.IsTrue(tracker.FinishNodeProgress(id, out message), message);
            Assert.IsTrue(tracker.NodeProgressGet(id, out state, out message), message);
            Assert.IsTrue(state == ProcessTrackingObjectState.Completed, "Node state {0} was not equal to the expected state {1}", state, ProcessTrackingObjectState.Completed);

            Assert.IsTrue(tracker.Finalize(out message), message);

            var report = tracker.ReportCreate();
            Assert.IsNotNull(report, message);

            Trace.WriteLine(String.Format("Tracker report:\r\n{0}", Xml.Serialize(report, true)));
        }
    }
}
