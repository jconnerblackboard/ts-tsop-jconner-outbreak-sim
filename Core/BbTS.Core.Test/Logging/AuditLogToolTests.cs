﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using BbTS.Core.Logging.AuditLogging;
using BbTS.Domain.Models.System.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BbTS.Core.Test.Logging
{
    [TestClass]
    public class AuditLogToolTests
    {
        public const String ApplicationName = "BbTS.Core.Logging.Test";
        public const String OsHostName = "machinename";
        public const String IpAddress = "10.20.30.40";
        public const String OsUser = "domain\\osuser";

        [TestMethod]
        public void TestExtractAuditLogItemsSuccess()
        {
            NameValueCollection headers = new NameValueCollection
                {
                    { "BbTS", String.Format(
                        "ApplicationName={0}," + "OsHostName={1}," + "IpAddress={2},"+"OsUser={3}",
                        ApplicationName, OsHostName, IpAddress, OsUser)}
                };
            AuditLogItem expected = new AuditLogItem
                {
                    Program = ApplicationName,
                    IpAddress = IpAddress,
                    OsHostName = OsHostName,
                    OsUser = OsUser
                };

            AuditLogItem actual = AuditLogTool.ExtractAuditLogItems(headers);

            Assert.AreEqual(true, expected.Equals(actual));
        }

        [TestMethod]
        public void TestKeyValuePairParseSuccess()
        {
            const string kvPair1 = "key=value";
            const string kvPair2 = " key = value ";
            const string kvPair3 = "key=\"value\"";
            const string kvPair4 = " key = \"value\" ";

            KeyValuePair<string,string> expected = new KeyValuePair<string, string>("key", "value");

            KeyValuePair<string, string> actual1 = AuditLogTool.ParseNameValuePair(kvPair1, '=', true);
            Assert.AreEqual(expected.Key, actual1.Key);
            Assert.AreEqual(expected.Value, actual1.Value);

            KeyValuePair<string, string> actual2 = AuditLogTool.ParseNameValuePair(kvPair2, '=', true);
            Assert.AreEqual(expected.Key, actual2.Key);
            Assert.AreEqual(expected.Value, actual2.Value);

            KeyValuePair<string, string> actual3 = AuditLogTool.ParseNameValuePair(kvPair3, '=', true);
            Assert.AreEqual(expected.Key, actual3.Key);
            Assert.AreEqual(expected.Value, actual3.Value);

            KeyValuePair<string, string> actual4 = AuditLogTool.ParseNameValuePair(kvPair4, '=', true);
            Assert.AreEqual(expected.Key, actual4.Key);
            Assert.AreEqual(expected.Value, actual4.Value);

        }
    }
}
