﻿using System;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Definitions.System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BbTS.Core.Test.Conversion
{
    [TestClass]
    public class DataValidationTests
    {
        #region Success Cases

        [TestMethod]
        public void SuccessEqualityTest()
        {
            var value1 = 1;
            var value2 = 1;
            
            Assert.IsTrue(DataValidation.ValidateValue(value1, value2, ValidateValueOperator.Equal));
        }

        [TestMethod]
        public void SuccessInequalityTest()
        {
            var value1 = 1;
            var value2 = 2;

            Assert.IsTrue(DataValidation.ValidateValue(value1, value2, ValidateValueOperator.NotEqual));
        }

        [TestMethod]
        public void SuccessGreaterThanTest()
        {
            var value1 = 2;
            var value2 = 1;

            Assert.IsTrue(DataValidation.ValidateValue(value1, value2, ValidateValueOperator.GreaterThan));
        }

        [TestMethod]
        public void SuccessGreaterThanOrEqualTest()
        {
            var value1 = 2;
            var value2 = 1;
            var value3 = 1;

            Assert.IsTrue(DataValidation.ValidateValue(value1, value2, ValidateValueOperator.GreatherThanOrEqual));
            Assert.IsTrue(DataValidation.ValidateValue(value3, value2, ValidateValueOperator.GreatherThanOrEqual));
        }

        [TestMethod]
        public void SuccessLessThanTest()
        {
            var value1 = 1;
            var value2 = 2;

            Assert.IsTrue(DataValidation.ValidateValue(value1, value2, ValidateValueOperator.LessThan));
        }

        [TestMethod]
        public void SuccessLessThanOrEqualTest()
        {
            var value1 = 1;
            var value2 = 2;
            var value3 = 1;

            Assert.IsTrue(DataValidation.ValidateValue(value1, value2, ValidateValueOperator.LessThanOrEqual));
            Assert.IsTrue(DataValidation.ValidateValue(value3, value2, ValidateValueOperator.LessThanOrEqual));
        }

        #endregion

        #region Fail Cases

        [TestMethod]
        public void FailEqualityTest()
        {
            var value1 = 1;
            var value2 = 2;

            Assert.IsFalse(DataValidation.ValidateValue(value1, value2, ValidateValueOperator.Equal));
        }

        [TestMethod]
        public void FailInequalityTest()
        {
            var value1 = 1;
            var value2 = 1;

            Assert.IsFalse(DataValidation.ValidateValue(value1, value2, ValidateValueOperator.NotEqual));
        }

        [TestMethod]
        public void FailGreaterThanTest()
        {
            var value1 = 1;
            var value2 = 2;
            var value3 = 1;

            Assert.IsFalse(DataValidation.ValidateValue(value1, value2, ValidateValueOperator.GreaterThan));
            Assert.IsFalse(DataValidation.ValidateValue(value1, value3, ValidateValueOperator.GreaterThan));
        }

        [TestMethod]
        public void FailGreaterThanOrEqualTest()
        {
            var value1 = 1;
            var value2 = 2;

            Assert.IsFalse(DataValidation.ValidateValue(value1, value2, ValidateValueOperator.GreatherThanOrEqual));
        }

        [TestMethod]
        public void FailLessThanTest()
        {
            var value1 = 2;
            var value2 = 1;
            var value3 = 1;

            Assert.IsFalse(DataValidation.ValidateValue(value1, value2, ValidateValueOperator.LessThan));
            Assert.IsFalse(DataValidation.ValidateValue(value3, value2, ValidateValueOperator.LessThan));
        }

        [TestMethod]
        public void FailLessThanOrEqualTest()
        {
            var value1 = 2;
            var value2 = 1;

            Assert.IsFalse(DataValidation.ValidateValue(value1, value2, ValidateValueOperator.LessThanOrEqual));
        }


        #endregion
    }
}
