﻿using System;
using BbTS.Core.Conversion;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BbTS.Core.Test.Conversion
{
    [TestClass]
    public class ConversionTests
    {
        [TestMethod]
        public void GenerateUniqueNameUnitTest()
        {
            const string base1 = "basename";
            const string dateFormat = "yyyyMMddhhmmssfff";

            // Success case
            {
                int maxLength = base1.Length + dateFormat.Length;
                string name = Formatting.GenerateUniqueName(base1, maxLength);
                Assert.IsTrue(name.Length == maxLength);
                Assert.IsTrue(name.StartsWith(base1));
            }

            // Trim case
            {
                int maxLength = base1.Length + dateFormat.Length - 1;
                string name = Formatting.GenerateUniqueName(base1, maxLength);
                Assert.IsTrue(name.Length == maxLength);

                string substring = base1.Substring(0, base1.Length - 1);
                Assert.IsTrue(name.StartsWith(substring));
            }

            // invalid where maxlength <= dateformat.length case
            // I.E. the require string length is less than what it would take to make it unique.
            {
                int maxLength = dateFormat.Length;
                string name = Formatting.GenerateUniqueName(base1, maxLength);
                Assert.IsTrue(name.Length == maxLength);

                name = Formatting.GenerateUniqueName(base1, 2);
                Assert.IsTrue(name.Length == maxLength);
            }

        }
    }
}
