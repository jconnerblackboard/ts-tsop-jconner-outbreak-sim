﻿using System;
using BbTS.Core.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BbTS.Core.Test.Configuration
{
    [TestClass]
    public class ConfigurationTests
    {
        [TestMethod]
        [DeploymentItem("App.Config")]
        public void AlterConfigurationTestUnitTestSuccess()
        {
            ApplicationConfiguration.SetConfigurationValue("TestKey", "TestValue");
            Assert.AreEqual("TestValue", ApplicationConfiguration.GetKeyValueAsString("TestKey", null));
        }
    }
}
