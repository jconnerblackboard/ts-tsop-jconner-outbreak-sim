﻿using BbTS.Core.System.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BbTS.Core.Test.System
{
    [TestClass]
    public class ServiceManagerTest
    {
        [TestMethod]
        [Ignore]
        public void OverloadInvalidStateTests()
        {
            #region Overload calls - Normal APIs
            try
            {
                ServiceManager.ServiceControllerStatusSet("BlackboardTransactApi", 10000, "Foo");
                Assert.Fail("Services failed to test exception handling properly.");
            }
            catch
            {
                // The above call should fail, so being here is good
                Assert.IsTrue(true);
            }

            try
            {
                ServiceManager.ServiceControllerStatusSet("BlackboardTransactApi", 10000, "Restart");
                Assert.IsTrue(true);
            }
            catch
            {
                Assert.Fail("Services failed to restart normally.");
            }
            #endregion

            #region Overload calls - Hydra APIs
            try
            {
               var actionResultToken = ServiceManager.HydraServiceControllerStatusSet("BlackboardTransactApi", 10000, "Foo");
               Assert.IsTrue(actionResultToken.ResultDomain.Equals(232));
               Assert.IsTrue(actionResultToken.ResultDomainId.Equals(3));
               
            }
            catch
            {
                // The above call should fail with an ResultDomainId of 3 - being here is bad
                Assert.Fail("Services failed to test exception handling properly.");
            }

            try
            {
                var actionResultToken = ServiceManager.HydraServiceControllerStatusSet("BlackboardTransactApi", 10000, "Restart");
                Assert.IsTrue(actionResultToken.ResultDomain.Equals(232));
                Assert.IsTrue(actionResultToken.ResultDomainId.Equals(0));
            }
            catch
            {
                Assert.Fail("Services failed to restart normally.");
            }
            #endregion
        }

        [TestMethod]
        [Ignore]
        public void ServiceManagerTests()
        {
            #region Normal calls - positive results
            // These should all succeed as plenty of time is allocated for the services to respond.
            try
            {
                ServiceManager.ServiceControllerStatusSet("BlackboardTransactApi", 10000, ServiceManager.ServiceState.Stop);
                Assert.IsTrue(true);
            }
            catch
            {
                Assert.Fail("Services failed to stop normally.");
            }

            try
            {
                ServiceManager.ServiceControllerStatusSet("BlackboardTransactApi", 10000, ServiceManager.ServiceState.Start);
                Assert.IsTrue(true);
            }
            catch
            {
                Assert.Fail("Services failed to start normally.");
            }

            try
            {
                ServiceManager.ServiceControllerStatusSet("BlackboardTransactApi", 10000, ServiceManager.ServiceState.Restart);
                Assert.IsTrue(true);
            }
            catch
            {
                Assert.Fail("Services failed to restart normally.");
            }
            #endregion

            #region Failure calls - negative results
            // These should all fail as not enough time is allowed for the service operations
            try
            {
                // Stop always reports ok, even when a miniscule amount of time is allowed.  Start will fail with this little time.
                ServiceManager.ServiceControllerStatusSet("BlackboardTransactApi", 1, ServiceManager.ServiceState.Stop);
                ServiceManager.ServiceControllerStatusSet("BlackboardTransactApi", 1, ServiceManager.ServiceState.Start);
                Assert.Fail("Services failed to test exception handling properly.");
            }
            catch
            {
                // The above call should fail, so being here is good
                Assert.IsTrue(true);
            }

            try
            {
                ServiceManager.ServiceControllerStatusSet("BlackboardTransactApi", 1, ServiceManager.ServiceState.Restart);
                Assert.Fail("Services failed to test exception handling properly.");
            }
            catch
            {
                // The above call should fail, so being here is good
                Assert.IsTrue(true);
            }
            #endregion

            #region Hydra module calls
            // These should all succeed as plenty of time is allocated for the services to respond.
            try
            {
               var actionResultToken = ServiceManager.HydraServiceControllerStatusSet("BlackboardTransactApi", 10000, ServiceManager.ServiceState.Stop);
                Assert.IsTrue(actionResultToken.ResultDomain.Equals(232));
                Assert.IsTrue(actionResultToken.ResultDomainId.Equals(0));
            }
            catch
            {
                Assert.Fail("Services failed to stop normally.");
            }

            try
            {
                var actionResultToken = ServiceManager.HydraServiceControllerStatusSet("BlackboardTransactApi", 10000, ServiceManager.ServiceState.Start);
                Assert.IsTrue(actionResultToken.ResultDomain.Equals(232));
                Assert.IsTrue(actionResultToken.ResultDomainId.Equals(0));
            }
            catch
            {
                Assert.Fail("Services failed to start normally.");
            }

            try
            {
                var actionResultToken = ServiceManager.HydraServiceControllerStatusSet("BlackboardTransactApi", 10000, ServiceManager.ServiceState.Restart);
                Assert.IsTrue(actionResultToken.ResultDomain.Equals(232));
                Assert.IsTrue(actionResultToken.ResultDomainId.Equals(0));
            }
            catch
            {
                Assert.Fail("Services failed to restart normally.");
            }
            #endregion
        }
    }
}
