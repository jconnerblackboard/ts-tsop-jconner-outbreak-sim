﻿using System;
using BbTS.Core.Security.Session;
using BbTS.Domain.Models.System.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BbTS.Core.Test.Security
{
    [TestClass]
    public class SessionTests
    {
        [TestMethod]
        public void TestTokenCreateSuccess()
        {
            SessionToken expected = new SessionToken
            {
                ClientId = "someclientkey",
                Username = "someuser",
                TokenId = "62D437D4256F4D6D93FDE4DD86028ED9"
            };

            SessionToken actual = SessionValidationTool.CreateToken("someclientkey", "someuser");

            Assert.AreEqual(expected.ClientId, actual.ClientId);
            Assert.AreEqual(expected.Username, actual.Username);
            Assert.AreNotEqual(expected.TokenId, actual.TokenId);
            Assert.AreEqual(expected.TokenId.Length, actual.TokenId.Length);
        }

        [TestMethod]
        public void TestTokenEncodeSuccess()
        {
            SessionToken control1 = new SessionToken
            {
                ClientId = "013CD062B7684FF0858B9F8525F4AB12",
                Username = "someuser",
                TokenId = "62D437D4256F4D6D93FDE4DD86028ED9"
            };

            SessionToken control2 = new SessionToken
            {
                ClientId = "1EB4C9FA0D664FD5AF5059E6325634D2",
                Username = "someotheruser",
                TokenId = "CE311C815E7D4D66A5F9444154BF0EAA"
            };

            String token1 = SessionValidationTool.EncodeToken(control1);
            String token2 = SessionValidationTool.EncodeToken(control1);
            Assert.AreEqual(token1, token2);

            String token3 = SessionValidationTool.EncodeToken(control2);
            Assert.AreNotEqual(token1, token3);

            const string token4 = "MGtKQnNGWkZyVjNGbTBDdDJ0emdtOGJFWHRwci9KU2szbVVLd1BnTUNaODNhL0hzNDFHbVBGa25oaDJqVUZXeTlOdVNSZmlacGFaaXptZUYzYmZsS1NudUMxcUFxNWFKZE9pelNZbGpwdDQ9";
            Assert.AreEqual(token1, token4);
        }

        [TestMethod]
        public void TestTokenDecodeSuccess()
        {
            const string token = "MGtKQnNGWkZyVjNGbTBDdDJ0emdtOGJFWHRwci9KU2szbVVLd1BnTUNaODNhL0hzNDFHbVBGa25oaDJqVUZXeTlOdVNSZmlacGFaaXptZUYzYmZsS1NudUMxcUFxNWFKZE9pelNZbGpwdDQ9";
            SessionToken expected = new SessionToken
            {
                ClientId = "013CD062B7684FF0858B9F8525F4AB12",
                Username = "someuser",
                TokenId = "62D437D4256F4D6D93FDE4DD86028ED9"
            };

            SessionToken actual = SessionValidationTool.DecodeToken(token);
            Assert.AreEqual(expected.TokenId, actual.TokenId);
            Assert.AreEqual(expected.ClientId, actual.ClientId);
            Assert.AreEqual(expected.Username, actual.Username);

        }

        [TestMethod]
        public void TokenGenerate()
        {
            //SessionToken control1 = new SessionToken
            //{
            //    ClientId = "013CD062B7684FF0858B9F8525F4AB12",
            //    Username = "Admin",
            //    TokenId = "FD4AEFAAF0EE4AA1B4FA34F41D6E40D0"
            //};

            //SessionToken control2 = new SessionToken
            //{
            //    ClientId = "013CD062B7684FF0858B9F8525F4AB12",
            //    Username = "NotAdmin",
            //    TokenId = "1DCA5E4A295C449585B7F449C3BB39D3"
            //};

            //String token1 = SessionValidationTool.EncodeToken(control1);
            //String token2 = SessionValidationTool.EncodeToken(control1);
            //Assert.AreEqual(token1, token2);

            //String token3 = SessionValidationTool.EncodeToken(control2);
            //Assert.AreNotEqual(token1, token3);

            //const string token4 = "MGtKQnNGWkZyVjNGbTBDdDJ0emdtOGJFWHRwci9KU2szbVVLd1BnTUNaODNhL0hzNDFHbVBGa25oaDJqVUZXeTlOdVNSZmlacGFaaXptZUYzYmZsS1NudUMxcUFxNWFKZE9pelNZbGpwdDQ9";
            //Assert.AreEqual(token1, token4);
        }
    }
}
