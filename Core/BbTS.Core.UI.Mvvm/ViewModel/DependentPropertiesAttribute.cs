﻿using System;

namespace BbTS.Core.UI.Mvvm.ViewModel
{
    /// <summary>
    /// An attribute class that will help manage property change notifications.
    /// See http://www.codeproject.com/Articles/375192/WPF-The-calculated-property-dependency-problem for more info.
    /// Note that I have implemented the variation suggested in the "Almost perfect..." comment.
    /// </summary>
    public class DependentPropertiesAttribute : Attribute
    {
        /// <summary>
        /// Dependent properties attribute.
        /// </summary>
        /// <param name="dependentProperties"></param>
        public DependentPropertiesAttribute(params string[] dependentProperties)
        {
            Properties = dependentProperties;
        }

        /// <summary>
        /// Gets the properties.
        /// </summary>
        public string[] Properties { get; }
    }
}