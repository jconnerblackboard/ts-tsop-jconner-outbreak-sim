﻿using System;
using System.Windows;

namespace BbTS.Core.UI.Mvvm.ViewModel
{
    /// <summary>
    /// Helper class to make sure an action is executed on the UI thread.
    /// </summary>
    public static class DispatchHelper
    {
        /// <summary>
        /// Executes the action, using BeginInvoke if necessary.
        /// </summary>
        /// <param name="action"></param>
        public static void BeginInvoke(Action action)
        {
            var dispatchObject = Application.Current.Dispatcher;
            if (dispatchObject == null || dispatchObject.CheckAccess())
            {
                action();
            }
            else
            {
                dispatchObject.BeginInvoke(action);
            }
        }

        /// <summary>
        /// Executes the action, using Invoke if necessary.
        /// </summary>
        /// <param name="action"></param>
        public static void Invoke(Action action)
        {
            var dispatchObject = Application.Current.Dispatcher;
            if (dispatchObject == null || dispatchObject.CheckAccess())
            {
                action();
            }
            else
            {
                dispatchObject.Invoke(action);
            }
        }
    }
}