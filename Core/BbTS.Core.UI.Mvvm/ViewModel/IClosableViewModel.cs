﻿using System;

namespace BbTS.Core.UI.Mvvm.ViewModel
{
    /// <summary>
    /// Allows the View Model to close the View.
    /// </summary>
    public interface IClosableViewModel
    {
        /// <summary>
        /// Handles the close request.
        /// </summary>
        event EventHandler CloseView;

        /// <summary>
        /// Closes the View.
        /// </summary>
        void OnCloseView();
    }
}
