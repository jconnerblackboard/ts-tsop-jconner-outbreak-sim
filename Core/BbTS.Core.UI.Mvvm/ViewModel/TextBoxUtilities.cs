﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace BbTS.Core.UI.Mvvm.ViewModel
{
    /// <summary>
    /// Helper class for an auto-scrolling textbox.  See O. R. Mapper's solution at https://stackoverflow.com/questions/10097417/how-do-i-create-an-autoscrolling-textbox for more info.
    /// </summary>
    public static class TextBoxUtilities
    {
        /// <summary>
        /// Attached property that allows for scroll to end behavior.
        /// </summary>
        public static readonly DependencyProperty AlwaysScrollToEndProperty = DependencyProperty.RegisterAttached("AlwaysScrollToEnd", typeof(bool), typeof(TextBoxUtilities), new PropertyMetadata(false, AlwaysScrollToEndChanged));

        private static void AlwaysScrollToEndChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var tb = sender as TextBox;
            if (tb != null)
            {
                bool alwaysScrollToEnd = (e.NewValue != null) && (bool)e.NewValue;
                if (alwaysScrollToEnd)
                {
                    tb.ScrollToEnd();
                    tb.TextChanged += TextChanged;
                }
                else
                {
                    tb.TextChanged -= TextChanged;
                }
            }
            else
            {
                throw new InvalidOperationException("The attached AlwaysScrollToEnd property can only be applied to TextBox instances.");
            }
        }

        /// <summary>
        /// Gets the current status of the scroll to end property.
        /// </summary>
        /// <param name="textBox"></param>
        /// <returns></returns>
        public static bool GetAlwaysScrollToEnd(TextBox textBox)
        {
            if (textBox == null)
            {
                throw new ArgumentNullException(nameof(textBox));
            }

            var value = textBox.GetValue(AlwaysScrollToEndProperty);
            return value != null && (bool)value;
        }

        /// <summary>
        /// Allows scroll to end behavior to be set in the code-behind.
        /// </summary>
        /// <param name="textBox"></param>
        /// <param name="alwaysScrollToEnd"></param>
        public static void SetAlwaysScrollToEnd(TextBox textBox, bool alwaysScrollToEnd)
        {
            if (textBox == null)
            {
                throw new ArgumentNullException(nameof(textBox));
            }

            textBox.SetValue(AlwaysScrollToEndProperty, alwaysScrollToEnd);
        }

        private static void TextChanged(object sender, TextChangedEventArgs e)
        {
            ((TextBox)sender).ScrollToEnd();
        }
    }
}
