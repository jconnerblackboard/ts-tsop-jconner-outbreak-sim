﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace BbTS.Core.UI.Mvvm.ViewModel
{
    /// <summary>
    /// Base class for WPF MVVM ViewModel implementations.  This base class simplifies property change notifications for descendant classes.
    /// </summary>
    public class BbViewModelBase : INotifyPropertyChanged
    {
        /// <summary>
        /// Enhanced RaisePropertyChanged function.
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="calledProperties"></param>
        protected void RaiseProperty(string propertyName, List<string> calledProperties = null)
        {
            RaisePropertyChanged(propertyName);

            if (calledProperties == null)
            {
                calledProperties = new List<string>();
            }

            calledProperties.Add(propertyName);

            //Get all properties in this class
            PropertyInfo[] properties = GetType().GetProperties();
            foreach (PropertyInfo p in properties)
            {
                if (p.Name == propertyName) continue; //Skip the property if it is the one being processed
                if (calledProperties.Contains(p.Name)) continue; //Skip the property if it has already been processed

                //Get all attributes on this property, but only those of type DependentPropertiesAttribute
                var attributes = p.GetCustomAttributes(typeof (DependentPropertiesAttribute), true) as DependentPropertiesAttribute[];

                if (attributes == null) continue; //Skip the property if there are no DependentPropertiesAttribute items

                //Look for attributes matching the propertyName
                //It only takes one match to update affected properties, so as soon as found one match, we're done
                foreach (var attribute in attributes)
                {
                    if (attribute.Properties == null) continue;
                    if (attribute.Properties.All(tp => tp != propertyName)) continue;

                    RaiseProperty(p.Name, calledProperties);
                    break;
                }
            }
        }

        /// <summary>
        /// Property changed event handler.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raise property changed.
        /// </summary>
        /// <param name="propertyName"></param>
        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}