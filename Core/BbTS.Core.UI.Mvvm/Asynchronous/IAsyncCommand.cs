﻿using System.Threading.Tasks;
using System.Windows.Input;

namespace BbTS.Core.UI.Mvvm.Asynchronous
{
    /// <summary>
    /// Asynchronous command interface.  See https://msdn.microsoft.com/magazine/dn630647
    /// IAsyncCommand can be used for any asynchronous ICommand implementation, and is intended to be exposed from ViewModels and consumed by the View and by unit tests.
    /// </summary>
    public interface IAsyncCommand : ICommand
    {
        /// <summary>
        /// Execute the command asynchronously.
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        Task ExecuteAsync(object parameter);
    }
}