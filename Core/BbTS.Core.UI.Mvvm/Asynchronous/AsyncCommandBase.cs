﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BbTS.Core.UI.Mvvm.Asynchronous
{
    /// <summary>
    /// <see cref="AsyncCommandBase"/> handles some of the common boilerplate code common to all asynchronous ICommands.
    /// </summary>
    public abstract class AsyncCommandBase : IAsyncCommand
    {
        /// <summary>
        /// Determines if the command can execute.
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public abstract bool CanExecute(object parameter);

        /// <summary>
        /// Executes the command asyncrhonously.
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public abstract Task ExecuteAsync(object parameter);

        /// <summary>
        /// Executes the command by awaiting <see cref="ExecuteAsync"/>.
        /// </summary>
        /// <param name="parameter"></param>
        public async void Execute(object parameter)
        {
            await ExecuteAsync(parameter);
        }

        /// <summary>
        /// Event handler for when CanExecute changes.
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        /// <summary>
        /// Signals that CanExecute has changed.
        /// </summary>
        protected void RaiseCanExecuteChanged()
        {
            CommandManager.InvalidateRequerySuggested();
        }
    }
}