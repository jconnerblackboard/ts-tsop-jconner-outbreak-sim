﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;

namespace BbTS.Core.UI.Mvvm.Asynchronous
{
    /// <summary>
    /// Task watcher class.  See https://msdn.microsoft.com/magazine/dn605875
    /// </summary>
    /// <typeparam name="TResult"></typeparam>
    public sealed class NotifyTaskCompletion<TResult> : INotifyPropertyChanged
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="task"></param>
        public NotifyTaskCompletion(Task<TResult> task)
        {
            Task = task;
            TaskCompletion = WatchTaskAsync(task);
        }

        private async Task WatchTaskAsync(Task task)
        {
            try
            {
                await task;
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch
            {
            }

            var propertyChanged = PropertyChanged;
            if (propertyChanged == null) return;

            propertyChanged(this, new PropertyChangedEventArgs("Status"));
            propertyChanged(this, new PropertyChangedEventArgs("IsCompleted"));
            propertyChanged(this, new PropertyChangedEventArgs("IsNotCompleted"));
            if (task.IsCanceled)
            {
                propertyChanged(this, new PropertyChangedEventArgs("IsCanceled"));
            }
            else if (task.IsFaulted)
            {
                propertyChanged(this, new PropertyChangedEventArgs("IsFaulted"));
                propertyChanged(this, new PropertyChangedEventArgs("Exception"));
                propertyChanged(this, new PropertyChangedEventArgs("InnerException"));
                propertyChanged(this, new PropertyChangedEventArgs("ErrorMessage"));
            }
            else
            {
                propertyChanged(this, new PropertyChangedEventArgs("IsSuccessfullyCompleted"));
                propertyChanged(this, new PropertyChangedEventArgs("Result"));
            }
        }

        /// <summary>
        /// Task.
        /// </summary>
        public Task<TResult> Task { get; }

        /// <summary>
        /// Property that represents the operation completing but doesn’t propagate exceptions (or return a result).
        /// </summary>
        public Task TaskCompletion { get; private set; }

        /// <summary>
        /// Result of task.
        /// </summary>
        public TResult Result => (Task.Status == TaskStatus.RanToCompletion) ? Task.Result : default(TResult);

        /// <summary>
        /// Status of task.
        /// </summary>
        public TaskStatus Status => Task.Status;

        /// <summary>
        /// True if task is completed.
        /// </summary>
        public bool IsCompleted => Task.IsCompleted;

        /// <summary>
        /// True if task is not completed.
        /// </summary>
        public bool IsNotCompleted => !Task.IsCompleted;

        /// <summary>
        /// True if task is successfully completed.
        /// </summary>
        public bool IsSuccessfullyCompleted => Task.Status == TaskStatus.RanToCompletion;

        /// <summary>
        /// True if task is canceled.
        /// </summary>
        public bool IsCanceled => Task.IsCanceled;

        /// <summary>
        /// True if task is faulted.
        /// </summary>
        public bool IsFaulted => Task.IsFaulted;

        /// <summary>
        /// Task exception.
        /// </summary>
        public AggregateException Exception => Task.Exception;

        /// <summary>
        /// Task inner exception.
        /// </summary>
        public Exception InnerException => Exception?.InnerException;

        /// <summary>
        /// Task inner exception error message.
        /// </summary>
        public string ErrorMessage => InnerException?.Message;

        /// <summary>
        /// Property changed event handler.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
    }
}