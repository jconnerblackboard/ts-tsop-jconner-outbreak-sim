﻿using System;
using System.Windows;
using System.Windows.Media.Animation;

namespace BbTS.Core.UI.Mvvm.View
{
    /// <summary>
    /// Animation extensions for manipulating controls.
    /// </summary>
    public static class AnimationExtensions
    {
        /// <summary>
        /// Toggles the opacity of a control.
        /// </summary>
        /// <param name="control">The control.</param>
        public static void ToggleControlFade(this FrameworkElement control)
        {
            control.ToggleControlFade(5000, 1.0, 0.0);
        }

        /// <summary>
        /// Toggles the opacity of a control.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <param name="fadeTimeInMilliseconds">The fade time in milliseconds.</param>
        /// <param name="opacityStart">The opacity start.</param>
        /// <param name="opacityStop">The opacity stop.</param>
        public static void ToggleControlFade(this FrameworkElement control, int fadeTimeInMilliseconds, double opacityStart, double opacityStop)
        {
            var storyboard = new Storyboard();
            var duration = new TimeSpan(0, 0, 0, 0, fadeTimeInMilliseconds);

            control.Opacity = opacityStart;
            var animation = new DoubleAnimation { From = opacityStart, To = opacityStop, Duration = new Duration(duration) };

            Storyboard.SetTargetName(animation, control.Name);
            Storyboard.SetTargetProperty(animation, new PropertyPath("Opacity", 0));
            storyboard.Children.Add(animation);

            storyboard.Begin(control);
        }
    }
}
