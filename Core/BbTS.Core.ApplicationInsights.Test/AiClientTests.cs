﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BbTS.Core.Configuration;
using Microsoft.ApplicationInsights.DataContracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BbTS.Core.ApplicationInsights.Test
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class AiClientTests
    {
        public string InstrumentationKey => ApplicationConfiguration.GetKeyValueAsString("InstrumentationKey", "");
        public const string InstitutionId = "6b158ab6-aa18-4206-80a7-2cca794535e7";

        [TestMethod]
        public void ThreadedAddEventsAndClear()
        {
            var client = new AiClient(InstrumentationKey, InstitutionId);
            var list = Enumerable.Repeat("test value", 100000).ToList();
            client.StartTracking("unitTest");
            Parallel.ForEach(list, item =>
            {
                client.TrackNode("testNode", item, DateTimeOffset.Now, new TimeSpan(0, 0, 1));
            });
            client.StopTracking(new Dictionary<string, string>());
            Assert.IsTrue(client.Count == 100003);
            client.Clear();
            Assert.IsTrue(client.Count == 0);
        }

        [TestMethod]
        public void ConstructorHandleInstitutionVoidTest()
        {
            var aiClient = new AiClient(InstrumentationKey, null);
            Assert.IsNotNull(aiClient);
            Trace.WriteLine("Check the application event log for Category 40 and id 200.  The message should read: " +
                            $"Failed to properly initialize the institutionId in the Application Insights client.  Falling back to {new Guid():D}");
        }

        [TestMethod]
        public void TraceNodeTest()
        {
            var client = new AiClient(InstrumentationKey, InstitutionId);
            client.TrackTrace(Values.TraceUnitTestMessage, SeverityLevel.Information);
            Assert.IsTrue(client.HasNodeWithMessage(Values.TraceUnitTestMessage));
        }

        [TestMethod]
        public void FlushTest()
        {
            var client = new AiClient(InstrumentationKey, InstitutionId);
            client.Flush();
        }

        [TestMethod]
        public void StopTrackingFailBadPropertiesTest()
        {
            var client = new AiClient(InstrumentationKey, InstitutionId);
            client.StartTracking(Values.UnitTestTrackingName);
            client.TrackTrace(Values.TraceUnitTestMessage, SeverityLevel.Information);
            client.StopTracking(new Dictionary<string, string> { {"IsSuccess", "false"}});
        }

        [TestMethod]
        public void TrackExceptionTest()
        {
            var client = new AiClient(InstrumentationKey, InstitutionId);
            client.TrackException(
                new Exception("Unit Test Exception #1"),
                false,
                new Dictionary<string, string>{{"UnitTest","Properties"}},
                new Dictionary<string, double>{{ "Exception number", 1}});
            client.TrackException(
                "Unit Test Exception #2",
                false,
                new Dictionary<string, string> { { "UnitTest", "Properties" } },
                new Dictionary<string, double> { { "Exception number", 2 } });
            Assert.IsTrue(client.HasExceptionNodeWithMessage("Unit Test Exception #1"));
            Assert.IsTrue(client.HasExceptionNodeWithMessage("Unit Test Exception #2"));
            client.Write(true);
            client.Flush();
            client.TrackException(
                "Unit Test Exception #3",
                true,
                new Dictionary<string, string> { { "UnitTest", "Properties" } },
                new Dictionary<string, double> { { "Exception number", 3 } });
        }

        [TestMethod]
        public void WriteTest()
        {
            var client = new AiClient(InstrumentationKey, InstitutionId);
            client.StartTracking(Values.UnitTestTrackingName);
            client.TrackNode("UnitTestNode", Values.UnitTestTrackingName, DateTimeOffset.Now, new TimeSpan(0, 0, 0, 1));
            client.TrackTrace(Values.TraceUnitTestMessage, SeverityLevel.Information);
            Thread.Sleep(2000);
            client.StopTracking(new Dictionary<string, string> { { "IsSuccess", "false" } });
            client.Write(true);
            client.Flush();
        }
    }
}
