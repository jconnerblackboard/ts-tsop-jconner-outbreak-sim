﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Core.ApplicationInsights.Test
{
    public class Values
    {
        public const string TraceUnitTestMessage = "Trace message for unit test";
        public const string UnitTestTrackingName = "UnitTestTrackingName";
    }
}
