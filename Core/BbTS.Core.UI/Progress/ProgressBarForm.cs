﻿using System;
using System.Windows.Forms;
using BbTS.Core.Logging.Tracking;
using Formatting = BbTS.Core.Conversion.Formatting;

namespace BbTS.Core.UI.Progress
{
    public partial class ProgressBarForm : Form
    {
        public delegate void UpdateProgressTracker(int min, int max, int value);
        public delegate void UpdateProgressLabel(string message, string timeRemaining);
        public delegate void UpdatePercentLabel(string message);
        public ProgressTracker ProgressTracker { get; internal set; }
        public ProgressBarForm(ProgressTracker tracker)
        {
            InitializeComponent();
            ControlBox = false;
            ProgressTracker = tracker;
            Progress.Minimum = ProgressTracker.Min;
            Progress.Maximum = ProgressTracker.Max;
            Progress.Value = ProgressTracker.Value;

            ProgressTracker.ProgressUpdateEvent += ProgressTracker_ProgressUpdateEvent;
        }

        private void ProgressTracker_ProgressUpdateEvent(int min, int max, int value)
        {
            try
            {
                _updateProgressTracker(min, max, value);
                _updateProgressLabel(ProgressTracker.ProgressMessage(), ProgressTracker.EstimatedTimeRemaining());
                _updatePercentLabel(ProgressTracker.PercentMessage());
            }
            catch (Exception ex)
            {
                MessageBox.Show(Formatting.FormatException(ex), @"Exception!", MessageBoxButtons.OK);
                Close();
            }
        }

        private void _updateProgressTracker(int min, int max, int value)
        {
            if (Progress.InvokeRequired)
            {
                UpdateProgressTracker d = _updateProgressTracker;
                Invoke(d, min, max, value);
            }
            else
            {
                Progress.Minimum = min;
                Progress.Maximum = max;
                Progress.Value = value;
            }
        }

        private void _updateProgressLabel(string message, string timeRemaining)
        {
            if (ProgressLabel.InvokeRequired)
            {
                UpdateProgressLabel d = _updateProgressLabel;
                Invoke(d, message, timeRemaining);
            }
            else
            {
                ProgressLabel.Text = $"{message}. {timeRemaining}";
            }
        }

        private void _updatePercentLabel(string message)
        {
            if (ProgressLabel.InvokeRequired)
            {
                UpdatePercentLabel d = _updatePercentLabel;
                Invoke(d, message);
            }
            else
            {
                PercentLabel.Text = $"{message}";
            }
        }

        private void _cancelProgress()
        {
            try
            {
                string message;
                if (!ProgressTracker.TryAbort(out message))
                {
                    MessageBox.Show(message, @"Error Canceling Progress", MessageBoxButtons.OK);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Formatting.FormatException(ex), @"Error Canceling Progress", MessageBoxButtons.OK);
            }
        }

        private void CancelProgressButton_Click(object sender, EventArgs e)
        {
            _cancelProgress();
        }
    }
}
