﻿using System;
using System.Threading;
using BbTS.Core.Conversion;

namespace BbTS.Core.UI.Progress
{
    public delegate void ProgressTrackerUpdateDelegate(int min, int max, int value);
    public class ProgressTracker
    {
        private static readonly object ValueLock = new object();

        public static event ProgressTrackerUpdateDelegate ProgressUpdateEvent;

        public CancellationTokenSource CancellationToken { get; set; }
        public int Min { get; internal set; }
        public int Max { get; internal set; }
        public int Value { get; internal set; }

        public ProgressTracker(int min, int max)
        {
            if (min >= max)
            {
                throw new ArgumentException($"Minimum value {min} cannot equal or exceed the maximum value {max}");
            }

            Min = min;
            Max = max;
            Value = min;
        }

        /// <summary>
        /// Reset the value counter to the min value
        /// </summary>
        public void Reset(int min, int max)
        {
            lock (ValueLock)
            {
                if (min >= max)
                {
                    throw new ArgumentException($"Minimum value {min} cannot equal or exceed the maximum value {max}");
                }
                Min = min;
                Max = max;
                Value = min;
                OnProgressUpdateEvent(Min, Max, Value);
            }
        }

        /// <summary>
        /// Increment the count.  Count will not exceed the max
        /// </summary>
        public void Increment()
        {
            lock (ValueLock)
            {
                Value = Math.Min(Max, Value + 1);
                OnProgressUpdateEvent(Min, Max, Value);
            }
        }

        /// <summary>
        /// Try to set the value of the progress to a specified value
        /// </summary>
        /// <param name="value">value to set to</param>
        /// <param name="message">results message</param>
        /// <returns></returns>
        public bool TrySet(int value, out string message)
        {
            lock (ValueLock)
            {
                if (value > Max)
                {
                    message = $"Unable to set value to {value} because it exceeds the max value {Max}.";
                    return false;
                }

                if (value < Min)
                {
                    message = $"Unable to set value to {value} because it is less than the min value {Min}.";
                    return false;
                }

                Value = value;
                OnProgressUpdateEvent(Min, Max, Value);
            }
            message = "Success";
            return true;
        }

        /// <summary>
        /// Try to abort a progress tracker
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool TryAbort(out string message)
        {
            try
            {
                CancellationToken?.Cancel();
                message = "Success";
                return true;
            }
            catch (Exception ex)
            {
                message = Formatting.FormatException(ex);
                return false;
            }
        }

        /// <summary>
        /// Formulate a progress message
        /// </summary>
        /// <returns></returns>
        public string ProgressMessage()
        {
            lock (ValueLock)
            {
                int normalizedValue = Value - Min;
                int normalizedMax = Max - Min;
                return $"{normalizedValue} of {normalizedMax} completed";
            }
        }

        /// <summary>
        /// Get the remining time to complete.
        /// </summary>
        /// <returns></returns>
        public string EstimatedTimeRemaining()
        {
            return $"Time remaining: <unknown>";
        }

        /// <summary>
        /// Formulate a percentage message
        /// </summary>
        /// <returns></returns>
        public string PercentMessage()
        {
            lock (ValueLock)
            {
                int normalizedValue = Value - Min;
                int normalizedMax = Max - Min;
                float total = (float)normalizedValue/normalizedMax;
                int percent = (int)(total*100);
                return $"{percent}%";
            }
        }

        /// <summary>
        /// Fire a progress event
        /// </summary>
        /// <param name="min">min value</param>
        /// <param name="max">max value</param>
        /// <param name="value">current value</param>
        private static void OnProgressUpdateEvent(int min, int max, int value)
        {
            ProgressUpdateEvent?.Invoke(min, max, value);
        }
        
    }
}
