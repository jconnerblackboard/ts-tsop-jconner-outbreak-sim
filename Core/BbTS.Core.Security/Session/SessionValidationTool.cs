﻿using System;
using System.Text;
using BbTS.Domain.Models.System.Security;

using Crypto = BbTS.Core.Security.Encryption.Crypto;

namespace BbTS.Core.Security.Session
{
    public class SessionValidationTool
    {
        /// <summary>
        /// Create a session token
        /// </summary>
        /// <param name="key"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        public static SessionToken CreateToken(String key, String username)
        {
            SessionToken sessionToken = new SessionToken
                {
                    ClientId = key,
                    Username = username,
                    TokenId = Guid.NewGuid().ToString("N")
                };

            return sessionToken;
        }

        /// <summary>
        /// Encrypt a session token.
        /// </summary>
        /// <param name="token">Session Token to encrypt</param>
        /// <returns>string containing the encrypted token</returns>
        public static string EncodeToken(SessionToken token)
        {
            string encryptedToken = Convert.ToBase64String(Encoding.UTF8.GetBytes(
                Crypto.Encrypt(token.TokenId + "," + token.ClientId + "," + token.Username,
                               Crypto.GetPasswordStorePassPhrase,
                               Crypto.GetPasswordStoreSalt,
                               Crypto.GetPasswordStoreIterations,
                               Crypto.GetSystemInitializationVector,
                               Crypto.GetPasswordStoreKeySize)));
            return encryptedToken;
        }

        /// <summary>
        /// Decode a token string and return the parts as a SessionToken object
        /// </summary>
        /// <param name="encryptedTokenString">Encrypted session token string</param>
        /// <returns>Decrypted SessionToken object</returns>
        public static SessionToken DecodeToken(String encryptedTokenString)
        {
            if (!String.IsNullOrEmpty(encryptedTokenString))
            {
                // "Crack" open the token and get the components.
                string userIdToken = Crypto.Decrypt(
                    Encoding.UTF8.GetString(Convert.FromBase64String(encryptedTokenString)),
                    Crypto.GetPasswordStorePassPhrase,
                    Crypto.GetPasswordStoreSalt,
                    Crypto.GetPasswordStoreIterations,
                    Crypto.GetSystemInitializationVector,
                    Crypto.GetPasswordStoreKeySize);

                // Split the token components into consumer key and timestamp ticks
                string[] tokenParts = userIdToken.Split(new[] { ',' });

                if (tokenParts.Length != 3)
                {
                    throw new ArgumentException("Malformed Session Token: Required number of token parts not present.");
                }
                string tokenId = tokenParts[0];
                string clientKey = tokenParts[1];
                string username = tokenParts[2];

                if (String.IsNullOrEmpty(tokenId)) throw new ArgumentException("Malformed Session Token: Invalid token id.");
                if (String.IsNullOrEmpty(clientKey)) throw new ArgumentException("Malformed Session Token: Invalid client key.");
                if (String.IsNullOrEmpty(username)) throw new ArgumentException("Malformed Session Token: Invalid username.");


                return new SessionToken
                    {
                        TokenId = tokenId,
                        ClientId = clientKey,
                        Username = username
                    };
            }
            throw new ArgumentException("Malformed Session Token: Empty session token.");
        }

    }
}
