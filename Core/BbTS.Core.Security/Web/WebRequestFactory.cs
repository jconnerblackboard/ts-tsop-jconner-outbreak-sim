﻿using System;
using System.IO;
using System.Net;
using System.Threading;

namespace BbTS.Core.Security.Web
{
	public class WebRequestFactory
	{
		// ReSharper disable InconsistentNaming
		private static readonly WebRequestFactory _instance = new WebRequestFactory();
		// ReSharper restore InconsistentNaming
		public static WebRequestFactory Instance
		{
			get { return _instance; }
		}
		public enum WebRequestKind { SystemNetHttpWebRequest, MonoNetHttpWebRequest }
		public WebRequestKind Kind { get; set; }

		private static bool OsIsWinXp
		{
			get
			{ 
				return Environment.OSVersion.Platform == PlatformID.Win32NT && 
					Environment.OSVersion.Version.Major == 5 && 
					Environment.OSVersion.Version.Minor == 1; 
			}
		}

		private void SetupMonoNet()
		{
			String caPath = Path.GetDirectoryName(global::System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\ca.pem";
			Mono.Net.ServicePointManager.CaCerts = new OpenSSL.X509.X509Chain(OpenSSL.Core.BIO.File(caPath, "r"));
		}

		WebRequestFactory()
		{
			if(OsIsWinXp)
			{
				Kind = WebRequestKind.MonoNetHttpWebRequest;
			}
			else
			{
                // README!!
                // Uncomment the line 1 below and comment out line 2 to enable TLS version 1.2, 1.1, 1.0
                // Line 1, Uncomment to enable TLS version 1.2, 1.1, 1.0 and comment out Line 2
                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                // Line 2, Uncomment to enable TLS version 1.2 ONLY, comment out Line 1
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                Kind = WebRequestKind.SystemNetHttpWebRequest;
			}
		}

		//Fill-in parameters for encuring a lazy call to SetupMonoNet()
		private static bool _monoNetInitialized;
		private static object _monoNetLock = new object();
		private static object _monoSetupDummy;

	    private global::System.Net.WebRequest createMonoWebRequest(Uri uri, String userAgent)
	    {
            //Perform a one-time call to SetupMonoNet() and ensure it's run to set up CaCerts before any Mono.Net.HttpWebRequest calls are made.
            LazyInitializer.EnsureInitialized(ref _monoSetupDummy, ref _monoNetInitialized, ref _monoNetLock, delegate { SetupMonoNet(); return new object(); });
            var monoWebRequest = new Mono.Net.HttpWebRequest(uri) { UserAgent = userAgent };
            return monoWebRequest;
	    }

	    public global::System.Net.WebRequest CreateWebRequest( Uri uri, String userAgent )
		{
            if (uri.Scheme == "https" && Kind == WebRequestKind.MonoNetHttpWebRequest)
            {
                return createMonoWebRequest(uri, userAgent);
            }
            global::System.Net.WebRequest webRequest= global::System.Net.WebRequest.Create(uri);
			global::System.Net.HttpWebRequest httpWebRequest = webRequest as global::System.Net.HttpWebRequest;
			if ( httpWebRequest != null )
			{
				httpWebRequest.UserAgent = userAgent;
                httpWebRequest.ServicePoint.Expect100Continue = false;
                httpWebRequest.ProtocolVersion = HttpVersion.Version11;
            }				
			return webRequest;
		}
	}
}
