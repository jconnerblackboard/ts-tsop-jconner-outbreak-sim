﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace BbTS.Core.Security.Network
{
    public class NetworkTools
    {
        /// <summary>
        /// Get the local ip address
        /// </summary>
        /// <returns></returns>
        public static string LocalIpAddressGet()
        {
            foreach (IPAddress ip in Dns.GetHostEntry(Dns.GetHostName()).AddressList)
            {
                if (ip != null && ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            return "127.0.0.1";
        }

        /// <summary>
        /// Get the computer's host name
        /// </summary>
        /// <returns>Computer's host name</returns>
        public static string HostName()
        {
            return  Dns.GetHostEntry(Dns.GetHostName()).HostName;
        }

        /// <summary>
        /// Get the currently logged in logged in user
        /// </summary>
        /// <returns>Logged in user's name</returns>
        public static string OsUser()
        {
            return Environment.UserName;
        }

        /// <summary>
        /// Get the local FQDN
        /// </summary>
        /// <returns></returns>
        public static string GetLocalhostFqdn()
        {
            string domainName = string.Empty;
            try
            {
                domainName = IPGlobalProperties.GetIPGlobalProperties().DomainName;
            }
            catch
            {
            }
            string fqdn = "localhost";
            try
            {
                fqdn = Dns.GetHostName();
                if (!string.IsNullOrEmpty(domainName))
                {
                    if (!fqdn.ToLowerInvariant().EndsWith("." + domainName.ToLowerInvariant()))
                    {
                        fqdn += "." + domainName;
                    }
                }
            }
            catch
            {
            }
            return fqdn;
        }

        /// <summary>
        /// Get the ip address from the context
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string GetClientIp(OperationContext context)
        {
            var props = context.IncomingMessageProperties;
            var endpointProperty = props[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
            if (endpointProperty != null)
            {
                var ip = endpointProperty.Address;
                return ip;
            }
            throw new Exception("Unable to obtain client Ip address.");
        }
    }
}
