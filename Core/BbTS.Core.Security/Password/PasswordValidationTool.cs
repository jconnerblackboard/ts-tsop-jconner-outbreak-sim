﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.System;
using BbTS.Domain.Models.System.Security;


namespace BbTS.Core.Security.Password
{
    public class PasswordValidationTool
    {
        private const int ApiResultDomain = 224;

        public static ResponseToken ValidatePassword(
            string password,
            List<DomainViewObject> codes,
            UserAccountRules rules )
        {
            String message = string.Empty;

            // Password is blank?
            if (String.IsNullOrEmpty(password))
            {
                var domainViewObject = codes.FirstOrDefault(c => c.Id == 9);
                if (domainViewObject != null) message = domainViewObject.Description;

                return new ResponseToken { ResultDomain = ApiResultDomain, ResultDomainId = 9, Message = message };
            }

            // Validate password length
            if (password.Length < rules.PasswordLengthMinimum || password.Length > 30)
            {
                var domainViewObject = codes.FirstOrDefault(c => c.Id == 6);
                if (domainViewObject != null) message = domainViewObject.Description;

                return new ResponseToken { ResultDomain = ApiResultDomain, ResultDomainId = 6, Message = message };
            }

            // Validate complexity rules
            if (!Regex.IsMatch(password, "[a-zA-Z]") || !Regex.IsMatch(password, "[0-9]"))
            {
                var domainViewObject = codes.FirstOrDefault(c => c.Id == 5);
                if (domainViewObject != null) message = domainViewObject.Description;
                return new ResponseToken { ResultDomain = ApiResultDomain, ResultDomainId = 5, Message = message };
            }

            //Successful validation result
            {
                var domainViewObject = codes.FirstOrDefault(c => c.Id == 0);
                if (domainViewObject != null) message = domainViewObject.Description;
            }

            return new ResponseToken { ResultDomain = ApiResultDomain, ResultDomainId = 0, Message = message };
        }

        /// <summary>
        /// Validate a user's password history against a proposed password change item
        /// </summary>
        /// <param name="password">Password to check against the history</param>
        /// <param name="rules">System wide user account rules</param>
        /// <param name="history">User's password history</param>
        /// <param name="codes"></param>
        /// <param name="user">User that owns the password</param>
        /// <returns></returns>
        public static ResponseToken ValidateUserPasswordHistory(
             string password,
             UserAccountRules rules,
             List<PasswordHistoryItem> history,
             List<DomainViewObject> codes,
             User user)
        {
            String message = string.Empty;

            int count = Math.Min(history.Count, rules.PasswordHistoryCount);
            

            for (int i = 0; i < count; ++i)
            {
                byte[] passwordHash = PasswordCreationTool.GeneratePasswordHash(
                    password,
                    Encoding.ASCII.GetBytes(history[i].PasswordHashSalt),
                    user.IterationCount,
                    2,
                    32);
                string passwordHashString = Formatting.ByteArrayToString(passwordHash);

                if (passwordHashString == history[i].PasswordHash)
                {
                    var domainViewObject = codes.FirstOrDefault(c => c.Id == 4);
                    if (domainViewObject != null) message = domainViewObject.Description;
                    return new ResponseToken { ResultDomain = ApiResultDomain, ResultDomainId = 4, Message = message };
                }
            }
            {
                var domainViewObject = codes.FirstOrDefault(c => c.Id == 0);
                if (domainViewObject != null) message = domainViewObject.Description;
            }
            return new ResponseToken { ResultDomain = ApiResultDomain, ResultDomainId = 0, Message = message };
         }
    }
}
