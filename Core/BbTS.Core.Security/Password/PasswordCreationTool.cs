﻿using System;
using System.Text;

using EncryptionTool = BbTS.Core.Security.Encryption.EncryptionTool;

namespace BbTS.Core.Security.Password
{
    public class PasswordCreationTool
    {
        private static readonly Random Random = new Random();

        /// <summary>
        /// Generate a password hash.
        /// </summary>
        /// <param name="password">Plain text password</param>
        /// <param name="salt">salt</param>
        /// <param name="iterationCount">iteration count</param>
        /// <param name="type">2=PBK, 3=DHE</param>
        /// <param name="size"></param>
        /// <returns></returns>
        public static byte[] GeneratePasswordHash(string password, byte[] salt, int iterationCount, int type, int size)
        {
            switch (type)
            {
                case 1: // SHA256 (old style)
                    return EncryptionTool.ComputeHashSHA256(
                        Encoding.ASCII.GetBytes(password),
                        salt);
                case 2: // PBKDF2 Sha2
                    return EncryptionTool.ComputeHashPBKDF2SHA1(
                        Encoding.ASCII.GetBytes(password),
                        salt,
                        size,
                        iterationCount);
                case 3: // PBKDF2 Sha2
                    return EncryptionTool.ComputeChainedHash(
                        Encoding.ASCII.GetBytes(password),
                        salt,
                        size,
                        iterationCount);
            }
            throw new ArgumentException("Wrong encryption type value: " + type.ToString());
        }

        /// <summary>
        /// Generate a salt string
        /// </summary>
        /// <returns></returns>
        public static byte[] GenerateSalt(int size)
        {
            const string availableCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567980";

            String saltString = string.Empty;
            for (int i = 0; i < size; ++i)
            {
                int index = GenerateRandomValue(availableCharacters.Length - 1);
                saltString += availableCharacters[index];
            }

            byte[] salt = Encoding.ASCII.GetBytes(saltString);
            return salt;
        }

        /// <summary>
        /// Generate a random value between 0 and size.
        /// </summary>
        /// <param name="size">Max random value</param>
        /// <returns></returns>
        public static int GenerateRandomValue(int size)
        {
            // Just a simple implementation of a random number between int.MinValue and int.MaxValue
            return Random.Next(0, size);
        }
    }
}
