﻿using BbTS.Core.Configuration;
using BbTS.Core.Conversion;
using BbTS.Core.Security.Encryption;
using BbTS.Domain.Models.Definitions;
using BbTS.Domain.Models.Exceptions.System;

namespace BbTS.Core.Security.Database
{
    /// <summary>
    /// Utility class to handle database security functions.
    /// </summary>
    public static class DatabaseSecurityTool
    {
        /// <summary>
        /// Create a connection string from base elements and an encrypted password
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="encryptedPassword">Encrypted password</param>
        /// <returns>Decrypted connection string.</returns>
        public static string CreateConnectionString(string connectionString, string encryptedPassword)
        {
            var decryptedPasswordString = EncryptionTool.DecryptOraclePassword(encryptedPassword);
            return connectionString + ";password=" + decryptedPasswordString;
        }

        /// <summary>
        /// Create a connection string using values from the registry.
        /// </summary>
        /// <returns></returns>
        public static string CreateConnectionStringFromRegistry(DatabaseConnectionStringType formatType = DatabaseConnectionStringType.OracleManaged)
        {
            var passwordCipher = RegistryConfigurationTool.Instance.ValueGet("OracleEnvisionPassword");
            if (string.IsNullOrWhiteSpace(passwordCipher))
                throw new RegistryConfigurationException($"Value 'OracleEnvisionPassword' was not found under {RegistryConfigurationTool.ConfigKey}");

            return CreateConnectionString("envision", passwordCipher, formatType);
        }

        /// <summary>
        /// Create an external client connection string using values from the registry.
        /// </summary>
        /// <returns></returns>
        public static string CreateExternalClientConnectionStringFromRegistry(DatabaseConnectionStringType formatType = DatabaseConnectionStringType.OracleManaged)
        {
            var passwordCipher = RegistryConfigurationTool.Instance.ValueGet("OracleExternalClientPassword");
            if (string.IsNullOrWhiteSpace(passwordCipher))
                throw new RegistryConfigurationException($"Value 'OracleExternalClientPassword' was not found under {RegistryConfigurationTool.ConfigKey}");

            return CreateConnectionString("externalclient", passwordCipher, formatType);
        }

        /// <summary>
        /// Create a reporting connection string using values from the registry.
        /// </summary>
        /// <returns></returns>
        public static string CreateRptngConnectionStringFromRegistry(DatabaseConnectionStringType formatType = DatabaseConnectionStringType.OracleManaged)
        {
            var passwordCipher = RegistryConfigurationTool.Instance.ValueGet("OracleRptngPassword");
            if (string.IsNullOrWhiteSpace(passwordCipher))
                throw new RegistryConfigurationException($"Value 'OracleRptngPassword' was not found under {RegistryConfigurationTool.ConfigKey}");

            return CreateConnectionString("rptng", passwordCipher, formatType);
        }

        private static string CreateConnectionString(string userId, string passwordCipher, DatabaseConnectionStringType formatType)
        {
            var aesEncryptionProvider = new AesEncryptionProvider();
            var password = aesEncryptionProvider.Decrypt(passwordCipher.Base64Decode());

            var server = RegistryConfigurationTool.Instance.ValueGet("OracleServerHostAddress");
            if (string.IsNullOrWhiteSpace(server))
                throw new RegistryConfigurationException($"Value 'OracleServerHostAddress' was not found under {RegistryConfigurationTool.ConfigKey}");

            var port = RegistryConfigurationTool.Instance.ValueGet("OraclePort");
            if (string.IsNullOrWhiteSpace(port))
                throw new RegistryConfigurationException($"Value 'OraclePort' was not found under {RegistryConfigurationTool.ConfigKey}");

            var serviceName = RegistryConfigurationTool.Instance.ValueGet("OracleServiceName");
            if (string.IsNullOrWhiteSpace(serviceName))
                throw new RegistryConfigurationException($"Value 'OracleServiceName' was not found under {RegistryConfigurationTool.ConfigKey}");

            switch (formatType)
            {
                case DatabaseConnectionStringType.OracleManaged:
                    return $"User Id = {userId}; Password = {password}; Data Source = {server}{(!string.IsNullOrEmpty(port) ? ":" + port : "")}/{serviceName};Connection Timeout=120";
                case DatabaseConnectionStringType.DevartDirectConnect:
                    return $"User Id={userId}; Data Source={server}; Port={port};Password={password};Service Name={serviceName};Direct=True";
                case DatabaseConnectionStringType.DevartSidNoDirect:
                    return $"User Id={userId}; Data Source={server}; Port={port};Password={password};SID={serviceName};Direct=False";
                case DatabaseConnectionStringType.TnsName:
                    return $"Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST={server})(PORT={port}))(CONNECT_DATA=(SERVICE_NAME={serviceName})));User Id={userId};Password={password};";
                default:
                    return null;
            }
        }
    }
}
