﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BbTS.Core.Security.Encryption;

namespace BbTS.Core.Security.FileSignature
{
    /// <summary>
    /// Tools to help verify the custom signature applied to a .xml, .install (treated like xml) or .sql file.
    /// </summary>
    public static class FileSignatureTools
    {
        internal static string GetFileContent(string filePath, bool removeSignature)
        {
            //Supported file.
            if ((!(filePath.ToLower().EndsWith(".xml"))) && (!(filePath.ToLower().EndsWith(".sql"))) && (!(filePath.ToLower().EndsWith(".install"))))
            {
                throw new Exception("File type is not supported. Extension must be '.sql' or '.xml' or '.install': " + filePath);
            }

            //Must exist.
            if (!(File.Exists(filePath)))
            {
                throw new Exception("File does not exist: " + filePath);
            }

            string fileContent = File.ReadAllText(filePath).Trim();

            if(removeSignature)
            {
                fileContent = RemoveExistingSignature(fileContent);
            }

            return fileContent;
        }

        /// <summary>
        /// Allows the file data to be returned without the signature.
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static string RemoveExistingSignature(string fileContent)
        {
                //Split into lines. Do not remove whitespace, other than from ends.
                string[] fileLines = fileContent.Trim().Split(new[] { Environment.NewLine }, StringSplitOptions.None);

                //Check for a BbSig line at the file end and remove it.
                if (fileLines.Last().Contains("BbSIG: "))
                {
                    Array.Resize(ref fileLines, fileLines.Length - 1);
                }
                fileContent = string.Join("\r\n", fileLines).Trim();

            return fileContent;
        }

        /// <summary>
        /// Get the file hash we need to use for verification.
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static string GetHashData(string filePath)
        {

            string fileContent = GetFileContent(filePath,true);

            //Get the hash.
            var fileBytes = Encoding.ASCII.GetBytes(fileContent);
            SHA256Managed hashManager = new SHA256Managed();

            string hash = "";
            var hashBytes = hashManager.ComputeHash(fileBytes);
            foreach (byte x in hashBytes)
            {
                hash += String.Format("{0:x2}", x);
            }
            return hash;
        }

        public static bool ValidateSignature(string filePath)
        {
            string fileContent = GetFileContent(filePath,false);

            //Split into lines. No need to remove whitespace, other than from ends.
            string[] fileLines = fileContent.Trim().Split(new[] { Environment.NewLine }, StringSplitOptions.None);

            //File have a sig line?
            if(!(fileLines.Last().Contains("BbSIG: ")))
            {
                //Sig line isn't found, indicate sig is invalid.
                return false;
            }

            //Signature line found.
            try
            {
                string sigLine = fileLines.Last();
                Regex sigRegex = new Regex(@"(?<=BbSIG:\s).*?(?=\s|$)");
                string sigData = sigRegex.Match(sigLine).Value;

                AesEncryptionProvider aes = new AesEncryptionProvider();
                string sigHash = aes.DecryptFromBase64String(sigData);

                if (sigHash == GetHashData(filePath))
                {
                    return true;
                }
            }
            catch
            {
                //Signature data is corrupt. We'll just go on to return false.
            }
            return false;
        }
    }
}
