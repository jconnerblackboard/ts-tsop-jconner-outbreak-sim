// // Copyright (c) 2010,2011,2013  Blackboard Inc.
// // All rights reserved.
// // 
// // Code derived from an unreleased version of External Client
// // Written by Mike Peterson, revised by Jeff Conner
#region

using System;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.ServiceModel.Description;
using System.ServiceModel.Security;
using System.Text;
using System.Xml;
using Microsoft.Win32;

#endregion

namespace BbTS.Core.Security.Encryption
{
    public static class SystemHelper
    {
        /// <summary>
        /// Asserts the null parameters.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        public static void AssertNullParameters(params object[] parameters)
        {
            foreach (var o in parameters)
            {
                if (o == null)
                {
                    throw new SystemException("Null Parameter");
                }
            }
        }
    }

    /// <summary>
    /// Class to encrypt and decrypt data
    /// </summary>
    public static class Crypto
    {
        // NOTE: Must encrypt and externalize these fields
        private const int DefaultKeySize = 256;
        private const string DefaultPassPhrase = "cat4sale";
        private const int DefaultPasswordIterations = 2;
        private const string DefaultSaltValue = "yummyyummysalt";
        private const string DefaultInitializationVector = "arikyixzkmhywvrc";

        // certficate constraint guids
        private static class CertificateConstraints
        {
            public static Guid ValidationDisabled
            {
                get { return new Guid("66F35F92-446E-44f0-A57A-EC20EE5A1D42"); }
            }
        }


        /// <summary>
        /// Sets the client certifcate constraints.
        /// </summary>
        /// <param name="wcfClientClientCredentials">The WCF client client credentials.</param>
        public static void SetClientCertifcateConstraints(ClientCredentials wcfClientClientCredentials)
        {
            string validationStateGuid = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Blackboard\BbSP", "A9DF0E4E-09C1-45aa-8165-382F8FF7C087", null);

            if (!string.IsNullOrEmpty(validationStateGuid))
            {
                if (CertificateConstraints.ValidationDisabled.Equals(new Guid(validationStateGuid)))
                {
                    wcfClientClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode =
                        X509CertificateValidationMode.None;
                }
            }

        }

        /// <summary>
        /// Gets the get password store pass phrase.
        /// </summary>
        /// <value>The get password store pass phrase.</value>
        public static string GetPasswordStorePassPhrase
        {
            get { return DefaultPassPhrase; }
        }


        /// <summary>
        /// Gets the get password store salt.
        /// </summary>
        /// <value>The get password store salt.</value>
        public static string GetPasswordStoreSalt
        {
            get { return DefaultSaltValue; }
        }

        /// <summary>
        /// Gets the size of the get password store key.
        /// </summary>
        /// <value>The size of the get password store key.</value>
        public static int GetPasswordStoreKeySize
        {
            get { return DefaultKeySize; }
        }

        /// <summary>
        /// Gets the get password store iterations.
        /// </summary>
        /// <value>The get password store iterations.</value>
        public static int GetPasswordStoreIterations
        {
            get { return DefaultPasswordIterations; }
        }


        /// <summary>
        /// Gets the get system initialization vector.
        /// </summary>
        /// <value>The get system initialization vector.</value>
        public static string GetSystemInitializationVector
        {
            get { return DefaultInitializationVector; }
        }

        /// <summary>
        /// Encrypts the specified plain text.
        /// </summary>
        /// <param name="plainText">The plain text.</param>
        /// <param name="passPhrase">The pass phrase.</param>
        /// <param name="saltValue">The salt value.</param>
        /// <param name="passwordIterations">The password iterations.</param>
        /// <param name="initVector">The init vector.</param>
        /// <param name="keySize">Size of the key.</param>
        /// <returns></returns>
        public static char[] Encrypt(char[] plainText,
                                     byte[] passPhrase,
                                     byte[] saltValue,
                                     int passwordIterations,
                                     byte[] initVector,
                                     int keySize)
        {
            SystemHelper.AssertNullParameters(plainText, passPhrase, saltValue, passwordIterations, initVector, keySize);

            if (passwordIterations == -1)
            {
                passwordIterations = DefaultPasswordIterations;
            }
            if (keySize == -1)
            {
                keySize = DefaultKeySize;
            }

            byte[] initVectorBytes = initVector;
            byte[] saltValueBytes = saltValue;
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            Rfc2898DeriveBytes password = new Rfc2898DeriveBytes(
                passPhrase,
                saltValueBytes,
                passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged { Mode = CipherMode.CBC };
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(
                keyBytes,
                initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();


            CryptoStream cryptoStream = new CryptoStream(memoryStream,
                                                         encryptor,
                                                         CryptoStreamMode.Write);

            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            char[] cipherCharArray = new char[(int)(Math.Ceiling((double)cipherTextBytes.Length / 3) * 4)];
            memoryStream.Close();
            cryptoStream.Close();
            Convert.ToBase64CharArray(cipherTextBytes, 0, cipherTextBytes.Length, cipherCharArray, 0);
            Array.Clear(cipherTextBytes, 0, cipherTextBytes.Length);
            Array.Clear(plainText, 0, plainText.Length);
            return cipherCharArray;
        }

        /// <summary>
        /// Encrypts the specified plain text.
        /// </summary>
        /// <param name="plainText">The plain text.</param>
        /// <param name="passPhrase">The pass phrase. pass null to use default</param>
        /// <param name="saltValue">The salt value. pass null to use default</param>
        /// <param name="passwordIterations">The password iterations. pass -1 to use default</param>
        /// <param name="initVector">The init vector.</param>
        /// <param name="keySize">Size of the key. pass -1 to use default</param>
        /// <returns></returns>
        public static string Encrypt(string plainText,
                                     string passPhrase,
                                     string saltValue,
                                     int passwordIterations,
                                     string initVector,
                                     int keySize)
        {
            SystemHelper.AssertNullParameters(plainText, initVector);
            if (passPhrase == null)
            {
                passPhrase = DefaultPassPhrase;
            }
            if (saltValue == null)
            {
                saltValue = DefaultSaltValue;
            }
            if (passwordIterations == -1)
            {
                passwordIterations = DefaultPasswordIterations;
            }
            if (keySize == -1)
            {
                keySize = DefaultKeySize;
            }

            byte[] initVectorBytes = Encoding.UTF8.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.UTF8.GetBytes(saltValue);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            Rfc2898DeriveBytes password = new Rfc2898DeriveBytes(
                passPhrase,
                saltValueBytes,
                passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);

            RijndaelManaged symmetricKey = new RijndaelManaged { Mode = CipherMode.CBC };

            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(
                keyBytes,
                initVectorBytes);

            MemoryStream memoryStream = new MemoryStream();


            CryptoStream cryptoStream = new CryptoStream(memoryStream,
                                                         encryptor,
                                                         CryptoStreamMode.Write);

            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            string cipherText = Convert.ToBase64String(cipherTextBytes);

            return cipherText;
        }

        /// <summary>
        /// Decrypts the specified cipher text.
        /// </summary>
        /// <param name="cipherText">The cipher text.</param>
        /// <param name="passPhrase">The pass phrase. pass null to use default</param>
        /// <param name="saltValue">The salt value. pass null to use default</param>
        /// <param name="passwordIterations">The password iterations. pass -1 to use default</param>
        /// <param name="initVector">The init vector.</param>
        /// <param name="keySize">Size of the key. pass -1 to use default</param>
        /// <returns></returns>
        public static string Decrypt(string cipherText,
                                     string passPhrase,
                                     string saltValue,
                                     int passwordIterations,
                                     string initVector,
                                     int keySize)
        {
            SystemHelper.AssertNullParameters(cipherText, initVector);
            if (passPhrase == null)
            {
                passPhrase = DefaultPassPhrase;
            }
            if (saltValue == null)
            {
                saltValue = DefaultSaltValue;
            }
            if (passwordIterations == -1)
            {
                passwordIterations = DefaultPasswordIterations;
            }
            if (keySize == -1)
            {
                keySize = DefaultKeySize;
            }

            byte[] initVectorBytes = Encoding.UTF8.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.UTF8.GetBytes(saltValue);
            byte[] cipherTextBytes = Convert.FromBase64String(cipherText);
            Rfc2898DeriveBytes password = new Rfc2898DeriveBytes(
                passPhrase,
                saltValueBytes,
                passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged { Mode = CipherMode.CBC };
            ICryptoTransform decryptor = symmetricKey.CreateDecryptor(
                keyBytes,
                initVectorBytes);
            MemoryStream memoryStream = new MemoryStream(cipherTextBytes);
            CryptoStream cryptoStream = new CryptoStream(memoryStream,
                                                         decryptor,
                                                         CryptoStreamMode.Read);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];
            int decryptedByteCount = cryptoStream.Read(plainTextBytes,
                                                       0,
                                                       plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
            string plainText = Encoding.UTF8.GetString(plainTextBytes,
                                                       0,
                                                       decryptedByteCount);
            return plainText;
        }

        /// <summary>
        /// Decrypts the specified cipher text.
        /// </summary>
        /// <param name="cipherText">The cipher text.</param>
        /// <param name="passPhrase">The pass phrase.</param>
        /// <param name="saltValue">The salt value.</param>
        /// <param name="passwordIterations">The password iterations.</param>
        /// <param name="initVector">The init vector.</param>
        /// <param name="keySize">Size of the key.</param>
        /// <returns></returns>
        public static char[] Decrypt(char[] cipherText,
                                     byte[] passPhrase,
                                     byte[] saltValue,
                                     int passwordIterations,
                                     byte[] initVector,
                                     int keySize)
        {
            SystemHelper.AssertNullParameters(cipherText, initVector);

            if (passwordIterations == -1)
            {
                passwordIterations = DefaultPasswordIterations;
            }
            if (keySize == -1)
            {
                keySize = DefaultKeySize;
            }

            byte[] initVectorBytes = initVector;
            byte[] saltValueBytes = saltValue;
            byte[] cipherTextBytes = Convert.FromBase64CharArray(cipherText, 0, cipherText.Length);
            Rfc2898DeriveBytes password = new Rfc2898DeriveBytes(
                passPhrase,
                saltValueBytes,
                passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged { Mode = CipherMode.CBC };
            ICryptoTransform decryptor = symmetricKey.CreateDecryptor(
                keyBytes,
                initVectorBytes);
            MemoryStream memoryStream = new MemoryStream(cipherTextBytes);
            CryptoStream cryptoStream = new CryptoStream(memoryStream,
                                                         decryptor,
                                                         CryptoStreamMode.Read);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];
            int decryptedByteCount = cryptoStream.Read(plainTextBytes,
                                                       0,
                                                       plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
            char[] plainText = Encoding.UTF8.GetChars(plainTextBytes,
                                                      0,
                                                      decryptedByteCount);
            Array.Clear(plainTextBytes, 0, plainTextBytes.Length);
            return plainText;
        }

        /// <summary>
        /// Randoms the IV.
        /// </summary>
        /// <returns></returns>
        public static string RandomIV()
        {
            StringBuilder sb = new StringBuilder();
            byte[] randomNumber = new byte[1];

            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            for (int index = 0; index < 16; index++)
            {
                rng.GetBytes(randomNumber);
                int rand = Convert.ToInt32(randomNumber[0]);
                int letter = rand % 26 + 97;
                sb.Append(Convert.ToChar(letter));
            }
            return sb.ToString();
        }

        /// <summary>
        /// Gets the certitificate by subject name
        /// </summary>
        /// <param name="subjectName">Name of the subject.</param>
        /// <returns></returns>
        public static X509Certificate2 GetCertificate(string subjectName)
        {
            X509Store store = new X509Store(StoreLocation.LocalMachine);
            store.Open(OpenFlags.ReadOnly);
            X509Certificate2Collection certCollection = store.Certificates;
            X509Certificate2 cert = null;
            foreach (X509Certificate2 c in certCollection)
            {
                if (c.Subject.Equals("CN=" + subjectName))
                {
                    cert = c;
                    break;
                }
            }
            if (cert == null)
            {
                throw new CryptographicException("The X.509 certificate could not be found.");
            }
            store.Close();
            return cert;
        }

        /// <summary>
        /// Encrypts the specified doc.
        /// .NET ONLY!
        /// </summary>
        /// <param name="xmlDocument">The XML document.</param>
        /// <param name="elementNameToEncrypt">The element name to encrypt.</param>
        /// <param name="subjectName">Name of the subject.</param>
        public static void Encrypt(XmlDocument xmlDocument, string elementNameToEncrypt, string subjectName)
        {
            X509Certificate2 cert = GetCertificate(subjectName);
            XmlElement elementToEncrypt = xmlDocument.GetElementsByTagName(elementNameToEncrypt)[0] as XmlElement;
            if (elementToEncrypt != null)
            {
                EncryptedXml eXml = new EncryptedXml();
                EncryptedData edElement = eXml.Encrypt(elementToEncrypt, cert);
                EncryptedXml.ReplaceElement(elementToEncrypt, edElement, false);
            }
        }

        /// <summary>
        /// Decrypts the specified doc.
        /// .NET ONLY!
        /// </summary>
        /// <param name="xmlDocument">The XML document.</param>
        public static void Decrypt(XmlDocument xmlDocument)
        {
            EncryptedXml exml = new EncryptedXml(xmlDocument);
            exml.DecryptDocument();
        }

        /// <summary>
        /// Encrypts the specified plain text using RSA
        /// </summary>
        /// <param name="plainText">The plain text.</param>
        /// <param name="subjectName">Name of the subject.</param>
        /// <returns></returns>
        public static string Encrypt(string plainText, string subjectName)
        {
            X509Certificate2 cert = GetCertificate(subjectName);
            var rsaProvider = (RSACryptoServiceProvider)cert.PublicKey.Key;
            return Convert.ToBase64String(rsaProvider.Encrypt(Encoding.UTF8.GetBytes(plainText), false));
        }

        /// <summary>
        /// Encrypt using given symmetric key
        /// </summary>
        /// <param name="plainTextString">The plain text string.</param>
        /// <param name="symmetricKey">The symmetric key.</param>
        /// <returns></returns>
        public static string Encrypt(string plainTextString, AesManaged symmetricKey)
        {

            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainTextString);
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(symmetricKey.Key, symmetricKey.IV);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            return Convert.ToBase64String(cipherTextBytes);
        }


        /// <summary>
        /// Decrypts the specified encrypted data string.
        /// </summary>
        /// <param name="cipherDataString">The cipher data string.</param>
        /// <param name="symmetricKey">The symmetric key.</param>
        /// <returns></returns>
        public static string Decrypt(string cipherDataString, AesManaged symmetricKey)
        {
            byte[] cipherData = Convert.FromBase64String(cipherDataString);
            ICryptoTransform decryptor = symmetricKey.CreateDecryptor(symmetricKey.Key, symmetricKey.IV);
            MemoryStream memoryStream = new MemoryStream(cipherData);
            CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            byte[] decryptionBuffer = new byte[cipherData.Length];
            int decryptedByteCount = cryptoStream.Read(decryptionBuffer, 0, decryptionBuffer.Length);
            byte[] decryptedBytes = new byte[decryptedByteCount];
            Array.Copy(decryptionBuffer, decryptedBytes, decryptedBytes.Length);
            return Encoding.UTF8.GetString(decryptedBytes);
        }


        /// <summary>
        /// Decrypts the specified cipher text.
        /// </summary>
        /// <param name="cipherText">The cipher text.</param>
        /// <param name="subjectName">Name of the subject.</param>
        /// <returns></returns>
        public static string Decrypt(string cipherText, string subjectName)
        {
            X509Certificate2 signatureCertificate = GetCertificate(subjectName);
            var rsaProvider = (RSACryptoServiceProvider)signatureCertificate.PrivateKey;
            return Encoding.UTF8.GetString(rsaProvider.Decrypt(Convert.FromBase64String(cipherText), false));

        }


        /// <summary>
        /// Gets a symmetric key
        /// </summary>
        /// <param name="keyMaterial">The key material.</param>
        /// <param name="lifeSpan"></param>
        /// <returns></returns>
        public static AesManaged GetSymmetricKey(string keyMaterial, long lifeSpan)
        {
            string[] keyIVSplit = keyMaterial.Split('|');
            byte[] key = Convert.FromBase64String(keyIVSplit[0]);
            byte[] iv = Convert.FromBase64String(keyIVSplit[1]);
            long ticks = long.Parse(keyIVSplit[2]);
            if (DateTime.Now.Ticks - ticks < (TimeSpan.TicksPerSecond * (lifeSpan + lifeSpan)))
            {
                return new AesManaged { Key = key, IV = iv, Mode = CipherMode.CBC };
            }
            throw new SystemException("Key lifespan expired");
        }

        /// <summary>
        /// Generates the symmetric key.
        /// </summary>
        /// <returns></returns>
        public static string GenerateSymmetricKey()
        {
            AesManaged symmetricKey = new AesManaged { Mode = CipherMode.CBC };
            symmetricKey.GenerateKey();
            symmetricKey.GenerateIV();
            return Convert.ToBase64String(symmetricKey.Key) + "|" + Convert.ToBase64String(symmetricKey.IV) + "|" + DateTime.Now.Ticks;
        }
    }
}