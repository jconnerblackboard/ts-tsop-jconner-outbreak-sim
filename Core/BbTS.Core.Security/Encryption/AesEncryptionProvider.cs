﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace BbTS.Core.Security.Encryption
{
    /// <summary>
    ///     Implements encryption 
    /// </summary>
    public class AesEncryptionProvider
    {
        /// <summary>
        /// Salt Value to create encryption key
        /// </summary>
        private const string SaltValue = "LRT%YUR#VBNL@1";

        /// <summary>
        /// Pass phrase to create encryption key
        /// <a href="https://www.fourmilab.ch/javascrypt/pass_phrase.html">pass phrase generator website</a>
        /// </summary>
        private const string PassPhrase = " OPTICIAN BORON GAMBIER LUTEOLIN CLEAN CUSPED";

        /// <summary>
        /// Initialization vector
        /// </summary>
        private const string InitializationVector = "HR$2pIjHR$2pIj12";

        /// <summary>
        /// Key size for creating encryption key
        /// </summary>
        private const int KeySize = 256;

        /// <summary>
        /// The encryption key
        /// </summary>
        private string key;

        /// <summary>
        /// Initializes a new instance of the <see cref="AesEncryptionProvider"/> class.
        /// </summary>
        public AesEncryptionProvider()
        {
            CreateEncryptionKey();
        }

        /// <summary>
        /// Encrypts plain text
        /// </summary>
        /// <param name="plaintext">Plain text</param>
        /// <returns>
        /// Encrypted string
        /// </returns>
        /// <exception cref="ArgumentNullException">Plain text</exception>
        public string Encrypt(string plaintext)
        {
            Guard.IsNotNullOrEmpty(plaintext, "plaintext");

            byte[] encrypted = null;

            using (Aes aesAlgorithm = Aes.Create())
            {
                if (aesAlgorithm != null)
                {
                    aesAlgorithm.Key = Encoding.Default.GetBytes(key);
                    aesAlgorithm.IV = Encoding.Default.GetBytes(InitializationVector);

                    // Create a encryptor to perform the stream transform.
                    ICryptoTransform encryptor = aesAlgorithm.CreateEncryptor(aesAlgorithm.Key, aesAlgorithm.IV);

                    // Create the streams used for encryption. 
                    using (var memoryStreamEncrypt = new MemoryStream())
                    {
                        using (var cryptStreamEncrypt = new CryptoStream(memoryStreamEncrypt, encryptor, CryptoStreamMode.Write))
                        {
                            using (var streamWriterEncrypt = new StreamWriter(cryptStreamEncrypt))
                            {
                                // Write all data to the stream.
                                streamWriterEncrypt.Write(plaintext);
                            }
                            encrypted = memoryStreamEncrypt.ToArray();
                        }
                    }
                }
            }

            return Encoding.Default.GetString(encrypted);
        }

        /// <summary>
        /// Encrypts plain text to a base 64 string.
        /// </summary>
        /// <param name="plaintext">Plain text</param>
        /// <returns>
        /// Encrypted string
        /// </returns>
        /// <exception cref="ArgumentNullException">Plain text</exception>
        public string EncryptToBase64String(string plaintext)
        {
            Guard.IsNotNullOrEmpty(plaintext, "plaintext");

            byte[] encrypted = null;

            using (Aes aesAlgorithm = Aes.Create())
            {
                if (aesAlgorithm != null)
                {
                    aesAlgorithm.Key = Encoding.Default.GetBytes(key);
                    aesAlgorithm.IV = Encoding.Default.GetBytes(InitializationVector);

                    // Create a encryptor to perform the stream transform.
                    ICryptoTransform encryptor = aesAlgorithm.CreateEncryptor(aesAlgorithm.Key, aesAlgorithm.IV);

                    // Create the streams used for encryption. 
                    using (var memoryStreamEncrypt = new MemoryStream())
                    {
                        using (var cryptStreamEncrypt = new CryptoStream(memoryStreamEncrypt, encryptor, CryptoStreamMode.Write))
                        {
                            using (var streamWriterEncrypt = new StreamWriter(cryptStreamEncrypt))
                            {
                                // Write all data to the stream.
                                streamWriterEncrypt.Write(plaintext);
                            }
                            encrypted = memoryStreamEncrypt.ToArray();
                        }
                    }
                }
            }

            return Convert.ToBase64String(encrypted);
        }

        /// <summary>
        /// Decrypts byte array
        /// </summary>
        /// <param name="encryptedValue">Encrypted value</param>
        /// <returns>
        /// Decrypted string
        /// </returns>
        /// <exception cref="ArgumentNullException">Encrypted Value</exception>
        public string Decrypt(string encryptedValue)
        {
            Guard.IsNotNullOrEmpty(encryptedValue, "plaintext");

            string plaintext = null;

            byte[] encryptedBytes = Encoding.Default.GetBytes(encryptedValue);

            using (Aes aesAlgorithm = Aes.Create())
            {
                aesAlgorithm.Key = Encoding.Default.GetBytes(key);
                aesAlgorithm.IV = Encoding.Default.GetBytes(InitializationVector);

                // Create a decryptor to perform the stream transform.
                ICryptoTransform decryptor = aesAlgorithm.CreateDecryptor(aesAlgorithm.Key, aesAlgorithm.IV);

                // Create the streams used for decryption. 
                using (var memoryStreamDecrypt = new MemoryStream(encryptedBytes))
                {
                    using (var cryptoStreamDecrypt = new CryptoStream(memoryStreamDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (var streamReaderDecrypt = new StreamReader(cryptoStreamDecrypt))
                        {
                            plaintext = streamReaderDecrypt.ReadToEnd();
                        }
                    }
                }
            }

            return plaintext;
        }

        /// <summary>
        /// Decrypts a base 64 string representing a byte array back into a string.
        /// </summary>
        /// <param name="encryptedValue">Encrypted value</param>
        /// <returns>
        /// Decrypted string
        /// </returns>
        /// <exception cref="ArgumentNullException">Encrypted Value</exception>
        public string DecryptFromBase64String(string encryptedValue)
        {
            Guard.IsNotNullOrEmpty(encryptedValue, "plaintext");

            string plaintext;

            var encryptedBytes = Convert.FromBase64String(encryptedValue);

            using (var aesAlgorithm = Aes.Create())
            {
                aesAlgorithm.Key = Encoding.Default.GetBytes(key);
                aesAlgorithm.IV = Encoding.Default.GetBytes(InitializationVector);

                // Create a decryptor to perform the stream transform.
                var decryptor = aesAlgorithm.CreateDecryptor(aesAlgorithm.Key, aesAlgorithm.IV);

                // Create the streams used for decryption. 
                using (var memoryStreamDecrypt = new MemoryStream(encryptedBytes))
                {
                    using (var cryptoStreamDecrypt = new CryptoStream(memoryStreamDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (var streamReaderDecrypt = new StreamReader(cryptoStreamDecrypt))
                        {
                            plaintext = streamReaderDecrypt.ReadToEnd();
                        }
                    }
                }
            }

            return plaintext;
        }

        /// <summary>
        /// Creates Encryption key 
        /// </summary>
        private void CreateEncryptionKey()
        {
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(SaltValue);

            using (var password = new Rfc2898DeriveBytes(PassPhrase, saltValueBytes, 2))
            {
                byte[] keyBytes = password.GetBytes(KeySize / 8);
                key = Encoding.Default.GetString(keyBytes);
            }
        }
    }
}
