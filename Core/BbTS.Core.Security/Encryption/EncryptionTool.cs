﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace BbTS.Core.Security.Encryption
{
    public class EncryptionTool
    {
        /// <summary>
        /// Generate the encryption key data
        /// </summary>
        /// <returns></returns>
        public static byte[] get_key()
        {
            byte[] keyData = new byte[]
                {
                    0x8A, 0x26, 0x54, 0x80, 0x8D, 0xE8, 0x5A, 0x58,
                    0x7A, 0xFA, 0x32, 0x02, 0xD1, 0xD9, 0x09, 0xCA,
                    0x9E, 0x6D, 0xB8, 0x35, 0xF1, 0x34, 0xF0, 0xEA,
                    0x6B, 0x3D, 0x14, 0x09, 0x90, 0x3C, 0x27, 0x67
                };

            // dejumble?
            byte tempByte = keyData[20];
            keyData[20] = keyData[27];
            keyData[27] = keyData[8];
            keyData[8] = keyData[05];
            keyData[05] = keyData[13];
            keyData[13] = keyData[10];
            keyData[10] = tempByte;

            return keyData;
        }

        /// <summary>
        /// Decrypt the data.
        /// </summary>
        /// <param name="data">Data to decrypt</param>
        /// <param name="key">Key, (use get_key method)</param>
        /// <param name="iv">Initialization Vector</param>
        /// <param name="cipherMode"></param>
        /// <param name="paddingMode"></param>
        /// <param name="blockSize"></param>
        /// <returns>Decrypted data string</returns>
        public static byte[] Decrypt(
            byte[] data,
            byte[] key,
            byte[] iv,
            CipherMode cipherMode,
            PaddingMode paddingMode,
            int blockSize)
        {
            MemoryStream ms = new MemoryStream();
            Rijndael alg = Rijndael.Create();
            alg.Mode = cipherMode;
            alg.Padding = paddingMode;
            alg.Key = key;
            alg.IV = iv;
            alg.BlockSize = blockSize;
            CryptoStream cs = new CryptoStream(ms, alg.CreateDecryptor(), CryptoStreamMode.Write);
            cs.Write(data, 0, data.Length);
            cs.FlushFinalBlock();
            cs.Close();
            byte[] decryptedData = ms.ToArray();
            return decryptedData;
        }

        /// <summary>
        /// Decrypt the data.
        /// </summary>
        /// <param name="data">Data to decrypt</param>
        /// <param name="key">Key, (use get_key method)</param>
        /// <param name="iv">Initialization Vector</param>
        /// <returns>Decrypted data string</returns>
        public static byte[] Decrypt(byte[] data, byte[] key, byte[] iv)
        {
            // Create a MemoryStream that is going to accept the
            // decrypted bytes 

            MemoryStream ms = new MemoryStream();

            // Create a symmetric algorithm. 
            // We are going to use Rijndael because it is strong and
            // available on all platforms. 
            // You can use other algorithms, to do so substitute the next
            // line with something like 
            //     TripleDES alg = TripleDES.Create(); 

            Rijndael alg = Rijndael.Create();

            // Now set the key and the IV. 
            // We need the IV (Initialization Vector) because the algorithm
            // is operating in its default 
            // mode called CBC (Cipher Block Chaining). The IV is XORed with
            // the first block (8 byte) 
            // of the data after it is decrypted, and then each decrypted
            // block is XORed with the previous 
            // cipher block. This is done to make encryption more secure. 
            // There is also a mode called ECB which does not need an IV,
            // but it is much less secure. 

            alg.Key = key;
            alg.IV = iv;

            // Create a CryptoStream through which we are going to be
            // pumping our data. 
            // CryptoStreamMode.Write means that we are going to be
            // writing data to the stream 
            // and the output will be written in the MemoryStream
            // we have provided. 

            CryptoStream cs = new CryptoStream(ms,
                                               alg.CreateDecryptor(), CryptoStreamMode.Write);

            // Write the data and make it do the decryption 

            cs.Write(data, 0, data.Length);

            // Close the crypto stream (or do FlushFinalBlock). 
            // This will tell it that we have done our decryption
            // and there is no more data coming in, 
            // and it is now a good time to remove the padding
            // and finalize the decryption process. 
            cs.FlushFinalBlock();
            cs.Close();

            // Now get the decrypted data from the MemoryStream. 
            // Some people make a mistake of using GetBuffer() here,
            // which is not the right way. 

            byte[] decryptedData = ms.ToArray();

            return decryptedData;
        }

        // DecryptStringFromBytes takes the Data, IV, and key and returns the plain text password
        public static string DecryptStringFromBytes(byte[] cipherText, byte[] key, byte[] iv)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (key == null || key.Length <= 0)
                throw new ArgumentNullException("key");
            if (iv == null || iv.Length <= 0)
                throw new ArgumentNullException("iv");

            // Declare the string used to hold
            // the decrypted text.
            string plaintext;

            // Create an RijndaelManaged object
            // with the specified key and IV.
            using (RijndaelManaged rijAlg = new RijndaelManaged())
            {
                rijAlg.Padding = PaddingMode.Zeros;
                rijAlg.BlockSize = 128;
                rijAlg.KeySize = 256;
                rijAlg.Key = key;
                rijAlg.IV = iv;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for decryption.
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {
                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }

            // remove character literals from the end of the string
            plaintext = plaintext.TrimEnd(new char[] { '\0' });

            return plaintext;
        }

        // DecryptString takes the Data, IV, and key, trims any extra characters, and returns the plain text password
        public static string DecryptString(byte[] data, byte[] key, byte[] iv, int len)
        {
            String myString = DecryptStringFromBytes(data, key, iv);
            // grab the correct number of bytes
            myString = myString.Substring(0, len);
            return myString;
        }

        // EncryptString takes a plain text string ("Password") and returns a tuple 
        //   of 2 strings - Data (Item1) and IV (Item2) for storage in the XML file
        public static Tuple<string, string> EncryptString(string plainText)
        {
            byte[] encrypted;

            // create encryption object, grab our key, gen a new IV
            RijndaelManaged myRijndael = new RijndaelManaged { Key = get_key() };
            myRijndael.GenerateIV();

            //Get an encryptor.
            ICryptoTransform encryptor = myRijndael.CreateEncryptor(myRijndael.Key, myRijndael.IV);

            //Encrypt the data.
            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    ASCIIEncoding textConverter = new ASCIIEncoding();

                    //Convert the data to a byte array.
                    byte[] toEncrypt = textConverter.GetBytes(plainText);

                    //Write all data to the crypto stream and flush it.
                    csEncrypt.Write(toEncrypt, 0, toEncrypt.Length);
                    csEncrypt.FlushFinalBlock();
                }
                //Get encrypted array of bytes.
                encrypted = msEncrypt.ToArray();
            }

            // convert to hex strings for serialization e.g. "A0B0C0D0..."
            String hexData = BitConverter.ToString(encrypted);
            hexData = hexData.Replace("-", "");
            String hexIV = BitConverter.ToString(myRijndael.IV);
            hexIV = hexIV.Replace("-", "");
            return new Tuple<string, string>(hexData, hexIV);
        }

        public static byte[] GenerateSalt(int length)
        {
            var salt = new byte[length];

            // Strong runtime pseudo-random number generator, on Windows uses CryptAPI
            // on Unix /dev/urandom
            RNGCryptoServiceProvider random = new RNGCryptoServiceProvider();

            random.GetNonZeroBytes(salt);

            return salt;
        }

        public static bool CompareHashedPasswords(byte[] array1, byte[] array2)
        {
            const byte zero = 0;
            int maxLength = array1.Length > array2.Length ? array1.Length : array2.Length;
            bool wereEqual = array1.Length == array2.Length;

            byte[] paddedArray1 = new byte[maxLength];
            byte[] paddedArray2 = new byte[maxLength];
            for (int i = 0; i < maxLength; i++)
            {
                paddedArray1[i] = array1.Length > i ? array1[i] : zero;
                paddedArray2[i] = array2.Length > i ? array2[i] : zero;
            }
            bool compareResult = true;
            for (int i = 0; i < maxLength; i++)
            {
                compareResult = compareResult & paddedArray1[i] == paddedArray2[i];
            }
            return compareResult & wereEqual;
        }

        public static byte[] ComputeHash(byte[] data, byte[] salt)
        {
            HashAlgorithm algorithm = new SHA256Managed();

            // Allocate memory to store both the Data and Salt together
            byte[] dataAndSalt = new byte[data.Length + salt.Length];

            // Copy both the data and salt into the new array
            Array.Copy(salt, dataAndSalt, salt.Length);
            Array.Copy(data, 0, dataAndSalt, salt.Length, data.Length);

            // Calculate the hash
            return algorithm.ComputeHash(dataAndSalt);
        }

        /// <summary>
        /// Decrypts the oracle password.
        /// </summary>
        /// <param name="encryptedPassword">The encrypted password.</param>
        /// <returns></returns>
        public static string DecryptOraclePassword(string encryptedPassword)
        {
            byte[] cryptoKey = { 90, 163, 234, 141, 94, 167, 68, 19, 11, 185, 223, 25, 30, 18, 222, 162, 43, 69, 75, 118, 135, 210, 205, 162, 181, 196, 178, 55, 196, 216, 59, 22 };
            byte[] cryptoVector = { 38, 25, 19, 31, 156, 82, 126, 20, 194, 227, 52, 21, 109, 35, 119, 55 };

            byte[] cypherText = Convert.FromBase64String(encryptedPassword);
            string plaintext;

            RijndaelManaged rijndaelManaged = new RijndaelManaged
                {
                    Key = cryptoKey,
                    IV = cryptoVector
                };

            ICryptoTransform decryptor = rijndaelManaged.CreateDecryptor(rijndaelManaged.Key, rijndaelManaged.IV);

            using (MemoryStream memoryStream = new MemoryStream(cypherText))
            {
                using (CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                {
                    using (StreamReader streamReader = new StreamReader(cryptoStream))
                    {
                        // Read the decrypted bytes from the decrypting stream
                        // and place them in a string.
                        plaintext = streamReader.ReadToEnd();
                    }
                }
            }

            return plaintext;
        }

        /// <summary>
        /// Encrypts the oracle password.
        /// </summary>
        /// <param name="password">The decrypted password.</param>
        /// <returns></returns>
        public static string EncryptOraclePassword(string password)
        {
            byte[] encrypted;

            byte[] cryptoKey = new byte[] { 90, 163, 234, 141, 94, 167, 68, 19, 11, 185, 223, 25, 30, 18, 222, 162, 43, 69, 75, 118, 135, 210, 205, 162, 181, 196, 178, 55, 196, 216, 59, 22 };
            byte[] cryptoVector = new byte[] { 38, 25, 19, 31, 156, 82, 126, 20, 194, 227, 52, 21, 109, 35, 119, 55 };


            // create encryption object, grab our key, gen a new IV
            RijndaelManaged myRijndael = new RijndaelManaged { Key = cryptoKey, IV = cryptoVector };

            //Get an encryptor.
            ICryptoTransform encryptor = myRijndael.CreateEncryptor(myRijndael.Key, myRijndael.IV);

            //Encrypt the data.
            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    ASCIIEncoding textConverter = new ASCIIEncoding();

                    //Convert the data to a byte array.
                    byte[] toEncrypt = textConverter.GetBytes(password);

                    //Write all data to the crypto stream and flush it.
                    csEncrypt.Write(toEncrypt, 0, toEncrypt.Length);
                    csEncrypt.FlushFinalBlock();
                }
                //Get encrypted array of bytes.
                encrypted = msEncrypt.ToArray();
            }

            return Convert.ToBase64String(encrypted);
        }

        /// <summary>
        /// Calculate the MD5 hash of a stream and return the result as a string
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static byte[] ComputeMd5Hash(Stream stream)
        {
            MD5CryptoServiceProvider provider = new MD5CryptoServiceProvider();
            return provider.ComputeHash(stream);
        }

        public static byte[] ComputeHashSHA256(byte[] data, byte[] salt)
        {
            return ComputeHash(data, salt);
        }

        public static byte[] ComputeHashPBKDF2SHA1(byte[] data, byte[] salt, int hashLengthBytes, int iterationCount)
        {
            Rfc2898DeriveBytes deriver = new Rfc2898DeriveBytes(data, salt, iterationCount);
            return deriver.GetBytes(hashLengthBytes);
        }
        public static byte[] ComputeChainedHash(byte[] data, byte[] salt, int hashLengthBytes, int iterationCount)
        {
            byte[] sha256Hash = ComputeHashSHA256(data, salt);
            return ComputeHashPBKDF2SHA1(sha256Hash, salt, hashLengthBytes, iterationCount);
        }
    }
}