﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Security.Cryptography.X509Certificates;
using BbTS.Core.System.SystemProcess;
using BbTS.Domain.Models.System.Security.Certificate;

namespace BbTS.Core.Security.Certificate
{
    public class CertificateTool
    {
        /// <summary>
        /// Get current SSL certificate bindings. This will delete existing bindings that are poorly formated, expired or a matching
        /// certificate can't be found in the windows certificate store.
        /// </summary>
        /// <param name="port"></param>
        public static List<CertificateBinding> GetSslCertificateBindings(string port)
        {
            // Get bindings
            string certBindingsRaw = ProcessUtility.Instance.ExecuteProcessCommand("netsh", string.Format("http show sslcert ipport=0.0.0.0:{0}", port));

            // Parse netsh output into a collection of bindings
            List<CertificateBinding> certificateBindings = ParseNetShShowSslCertOutput(certBindingsRaw);

            return new List<CertificateBinding>(
                certificateBindings.FindAll(
                    c => c.InCertificateStore &&
                         !IsBindingExpired(c) &&
                         !String.IsNullOrEmpty(c.CertificateStoreName)));
        }

        /// <summary>
        /// Parse the standard output of netsh http show sslcert into collection
        /// </summary>
        /// <param name="sslCertificateBindings"></param>
        /// <returns>Collection of certificates bound to ports with port as key</returns>
        public static List<CertificateBinding> ParseNetShShowSslCertOutput(string sslCertificateBindings)
        {
            // Split out header 'SSL Certificate bindings'
            string[] stringSeparators = { "IP:port" };
            string[] bindings = sslCertificateBindings.Split(stringSeparators, StringSplitOptions.None);

            List<CertificateBinding> certificateBindings = new List<CertificateBinding>();

            // Skip item 0 as the split breaks out the header "SSL Certificate bindings:" into an entry 
            foreach (var bindingString in bindings)
            {

                CertificateBinding binding = CreateBindingFromStringParse(bindingString);

                // Convert result set into object.
                certificateBindings.Add(binding);

            }
            return certificateBindings;
        }

        /// <summary>
        /// Populate class attributes from output of netsh command
        /// </summary>
        /// <param name="bindingString"></param>
        public static CertificateBinding CreateBindingFromStringParse(string bindingString)
        {
            CertificateBinding binding = new CertificateBinding();

            // Get Port
            string pattern = "\\s*\\:\\s\\d\\.\\d.\\d.\\d:(?'Port'\\d+)";
            Regex rx = new Regex(pattern);

            Match metaData = rx.Match(bindingString);

            if (metaData.Success)
            {
                binding.Port = metaData.Groups["Port"].Captures[0].ToString();
            }

            // Get Certificate Hash
            pattern = "Certificate\\sHash\\s*\\:\\s(?'CertificateHash'\\w+)";
            rx = new Regex(pattern);

            metaData = rx.Match(bindingString);

            if (metaData.Success)
            {
                binding.CertificateHash = metaData.Groups["CertificateHash"].Captures[0].ToString();
            }

            // Get Application ID
            pattern = "Application\\sID\\s*\\:\\s(?'ApplicationID'[A-Za-z0-9{}-]*)";
            rx = new Regex(pattern);

            metaData = rx.Match(bindingString);

            if (metaData.Success)
            {
                binding.ApplicationId = metaData.Groups["ApplicationID"].Captures[0].ToString();
            }

            // Get Store Name
            pattern = "Certificate\\sStore\\sName\\s*\\:\\s(?'CertificateStoreName'[A-Za-z0-9{}-]*)";
            rx = new Regex(pattern);

            metaData = rx.Match(bindingString);

            if (metaData.Success)
            {
                binding.CertificateStoreName = metaData.Groups["CertificateStoreName"].Captures[0].ToString();
            }

            // Find matching certificate in store
            X509Certificate2Collection certs = GetAuthorizedCertificates();
            foreach (X509Certificate2 cert in certs)
            {
                if (cert != null &&
                    cert.Thumbprint != null &&
                    String.Equals(cert.Thumbprint, binding.CertificateHash, StringComparison.CurrentCultureIgnoreCase))
                {
                    binding.Subject = cert.Subject;
                    binding.FriendlyName = cert.FriendlyName;
                    binding.Thumbprint = cert.Thumbprint;
                    binding.SubjectName = cert.SubjectName.Name;
                    binding.ExpirationDate = cert.NotAfter;
                    binding.InCertificateStore = true;
                }
            }
            return binding;
        }

        /// <summary>
        /// Has this binding expired
        /// </summary>
        /// <returns>true = yes, false = no</returns>
        public static bool IsBindingExpired(CertificateBinding binding)
        {
            return binding.ExpirationDate <= DateTime.Now;
        }

        /// <summary>
        ///   Return collection of certificates from the My Windows certificate story
        /// </summary>
        /// <param name="authorizedOnly">Return only those certificates supported by Bb Devices</param>
        /// <returns></returns>
        public static X509Certificate2Collection GetAuthorizedCertificates(Boolean authorizedOnly = false)
        {
            List<string> authorizedIssuingOrganizations = new List<string> { "VeriSign, Inc.", "The Go Daddy Group, Inc.", "GoDaddy.com, Inc." };
            X509Certificate2Collection authorizedCerts = new X509Certificate2Collection();

            // Get certificates from My store on the local machine
            X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            store.Open(OpenFlags.ReadOnly);
            X509Certificate2Collection allCerts = store.Certificates;

            if (authorizedOnly)
            {
                foreach (X509Certificate2 cert in allCerts)
                {
                    if (cert == null) continue;

                    // Get Organization from X500 Distinguished Name
                    const string organization = "O=\\\"(?'organization'.*)\\\"";
                    Regex rx = new Regex(organization);

                    if (cert.IssuerName.Name == null) continue;
                    Match regexMatch = rx.Match(cert.IssuerName.Name);

                    // If the certificate distinguished name contains a entry for organization (O=)
                    if (!regexMatch.Success) continue;

                    // And the issuing organization is supported by Bb Devices
                    if (authorizedIssuingOrganizations.Contains(regexMatch.Groups["organization"].Captures[0].ToString()))
                    {
                        authorizedCerts.Add(cert);
                    }
                }
            }
            else
            {
                authorizedCerts = allCerts;
            }
            return authorizedCerts;
        }

        /// <summary>
        /// Get a certificate from its thumbprint
        /// </summary>
        /// <param name="thumbprint"></param>
        /// <returns></returns>
        public static X509Certificate2 GetCertificateByThumbprint(string thumbprint)
        {
            X509Store certStore = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            // Try to open the store.

            certStore.Open(OpenFlags.ReadOnly);
            // Find the certificate that matches the thumbprint.
            X509Certificate2Collection certCollection = certStore.Certificates.Find(X509FindType.FindByThumbprint, thumbprint, false);

            certStore.Close();

            if (certCollection.Count > 0)
            {
                return certCollection[0];
            }

            string message = string.Format("No certificates were found for thumbprint {0}", thumbprint);
            throw new Exception(message);
        }
    }
}
