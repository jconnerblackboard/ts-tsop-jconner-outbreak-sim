﻿using System;
using System.Globalization;
using BbTS.Domain.Models.Exceptions.Security.Oauth;

namespace BbTS.Core.Security.Oauth
{
    /// <summary>
    /// Utility class for testing oauth properties for accuracy.
    /// </summary>
    public class OauthGuard
    {
        /// <summary>
        ///     Determines whether value is not null.
        /// </summary>
        /// <param name="value">
        ///     The value.
        /// </param>
        /// <param name="name">
        ///     The parameter name.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     Thrown if value is null
        /// </exception>
        public static void IsNotNull(object value, string name)
        {
            if (value == null)
            {
                throw new OauthException(string.Format(CultureInfo.CurrentCulture, "parameter {0} is null.", name));
            }
        }

        /// <summary>
        ///     Determines whether the value is not null or empty.
        /// </summary>
        /// <param name="value">
        ///     The value.
        /// </param>
        /// <param name="name">
        ///     The parameter name.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     Thrown if the value is null or empty
        /// </exception>
        public static void IsNotNullOrEmpty(string value, string name)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new OauthException(
                    string.Format(CultureInfo.CurrentCulture, "parameter {0} is null or empty.", name));
            }
        }

        /// <summary>
        /// Determines whether the value is null or whitespace.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="name">The name.</param>
        /// <exception cref="ArgumentNullException">
        ///     Thrown if the value is null
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if the value is empty or whitespace</exception>
        public static void IsNotNullOrWhiteSpace(string value, string name)
        {
            IsNotNull(value, name);

            if (value.Trim().Length == 0)
            {
                throw new OauthException(string.Format(CultureInfo.CurrentCulture, "parameter {0} is empty or whitespace.", name));
            }
        }

        /// <summary>
        /// Verify that the given guid is in a valid guid format.
        /// </summary>
        /// <param name="guid">The value to verify.</param>
        public static void IsGuid(string guid)
        {
            Guid value;
            if (!Guid.TryParse(guid, out value))
            {
                throw new OauthException($"parameter {guid} is not a valid Guid.");
            }
        }
    }
}
