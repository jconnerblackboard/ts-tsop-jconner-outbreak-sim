﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http.Controllers;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Definitions.Security;
using BbTS.Domain.Models.Definitions.Security.Oauth;
using BbTS.Domain.Models.Definitions.Service;
using BbTS.Domain.Models.Exceptions.Security.Oauth;
using BbTS.Domain.Models.MediaServer;
using BbTS.Domain.Models.System.Security.Oauth;

namespace BbTS.Core.Security.Oauth
{
    /// <summary>
    /// Static tool class to support basic OAuth functions.
    /// </summary>
    public static class OAuthFunctions
    {
        /// <summary>
        ///     Parses the parameters from the Authorization header.
        /// </summary>
        /// <param name="authorizationHeader">
        ///     The authorization header.
        /// </param>
        /// <returns>
        ///     OAuth parameters identified in the authorization header
        /// </returns>
        public static OAuthParameters ParseAuthorizationHeader(string authorizationHeader)
        {
            if (string.IsNullOrWhiteSpace(authorizationHeader))
            {
                return null;
            }

            var oauthParameters = new OAuthParameters();

            // TODO Decide whether we want to use the Authorization realm for anything or not
            // Drop the optional realm
            if (authorizationHeader.StartsWith(OAuthConstants.AuthorizationRealm, StringComparison.OrdinalIgnoreCase))
            {
                authorizationHeader = authorizationHeader.Substring(authorizationHeader.IndexOf(",", StringComparison.Ordinal) + 1);
            }

            authorizationHeader = Formatting.RemoveWhitespace(authorizationHeader);

            // Authorization header values should be comma separated
            var headerValues = authorizationHeader.Split(new[] { "," }, StringSplitOptions.None);

            // Loop through the parameters starting with the second because the first is the Authorization scheme
            foreach (var parameter in headerValues)
            {
                // Skip any non-OAuth parameters
                if (!parameter.StartsWith(OAuthConstants.OAuthPrefix, StringComparison.Ordinal))
                {
                    continue;
                }

                // Split the parameter into its constituent name/value parts, trim leading/trailing whitespace, and PercentDecode
                var nameValuePair = parameter.Split('=');
                var name = nameValuePair[0].Trim().PercentDecode();

                // Strip off the leading and trailing quotes
                var value = nameValuePair[1].Trim();
                if (value.StartsWith("\"", StringComparison.Ordinal) && value.EndsWith("\"", StringComparison.Ordinal))
                {
                    value = value.Substring(1, value.Length - 2).PercentDecode();
                }

                // Set the appropriate property. Note: This effectively skips any "extra" parameters
                switch (name)
                {
                    case OAuthConstants.OAuthCallback:
                        oauthParameters.CallbackUri = value;
                        break;

                    case OAuthConstants.OAuthConsumerKey:
                        oauthParameters.ConsumerKey = value;
                        break;

                    case OAuthConstants.OAuthNonce:
                        oauthParameters.Nonce = value;
                        break;

                    case OAuthConstants.OAuthSignature:
                        oauthParameters.Signature = value;
                        break;

                    case OAuthConstants.OAuthSignatureMethod:
                        oauthParameters.SignatureMethod = value;
                        break;

                    case OAuthConstants.OAuthToken:
                        oauthParameters.Token = value;
                        break;

                    case OAuthConstants.OAuthTimestamp:
                        oauthParameters.Timestamp = value;
                        break;

                    case OAuthConstants.OAuthVersion:
                        oauthParameters.Version = value;
                        break;
                }
            }

            return oauthParameters;
        }

        /// <summary>
        /// Extract <see cref="OAuthParameters"/> from an IEnumerable string of "header" values obtained from parsing an <see cref="HttpActionContext"/> request object.
        /// </summary>
        /// <param name="authorizationHeaderValue">The scheme and parameter of the authorization header.</param>
        /// <returns></returns>
        public static OAuthParameters ExtractOAuthParameters(AuthenticationHeaderValue authorizationHeaderValue)
        {
            OAuthParameters parameters;
            try
            {
                // Make sure there is at least one "Authorization" header in the list.
                // Use the first one found (there should really only be one).
                var authorizationHeader = authorizationHeaderValue.Parameter;
                if (string.IsNullOrEmpty(authorizationHeader))
                {
                    throw new OauthException(OauthDefinitions.Message(OauthExceptionCode.AuthorizationHeaderNotFound));
                }

                // Split the header to make sure it starts with "OAuth".
                var authorizationScheme = authorizationHeaderValue.Scheme;
                if (!authorizationScheme.StartsWith(OAuthConstants.OAuthAuthorizationScheme, StringComparison.OrdinalIgnoreCase))
                {
                    throw new OauthException(OauthDefinitions.Message(OauthExceptionCode.AuthorizationSchemaNotFound));
                }

                // Extract the OAuth parameters from the Authorization header.
                parameters = ParseAuthorizationHeader(authorizationHeader);
            }
            catch (Exception)
            {
                throw new OauthException(OauthDefinitions.Message(OauthExceptionCode.AuthorizationHeaderNotFound));
            }

            return parameters;
        }

        /// <summary>
        /// Extract OAuth header parameters from a possibly fragmented string.
        /// </summary>
        /// <param name="headerList">List of headers</param>
        /// <returns>Parsed parameters or null if none are found.</returns>
        public static OAuthParameters ExtractOauthHeaderParameters(HttpRequestHeaders headerList)
        {
            var header = headerList.Select(item => item.Value.FirstOrDefault(s => s.ToLower().Contains("oauth"))).Where(value => !string.IsNullOrWhiteSpace(value)).ToList();
            
            if (header.Count > 0)
            {
                var oauth = header.FirstOrDefault();
                if (!string.IsNullOrEmpty(oauth))
                {
                    var oauthParameters = new List<string>(oauth.Split(new[] { "OAuth" }, StringSplitOptions.RemoveEmptyEntries));
                    if (oauthParameters.Count == 0)
                    {
                        return null;
                    }
                    var index = oauthParameters.FindIndex(s => s.ToLower().Contains("oauth_consumer_key"));
                    return index > -1 ? ParseAuthorizationHeader(oauthParameters[index]) : null;
                }
            }
            return null;
        }

        /// <summary>
        /// Extracts the response items.
        /// </summary>
        /// <param name="encodedData">The form URL encoded string.</param>
        /// <returns>The response items in a collection</returns>
        public static NameValueCollection ExtractOauthReturnParameters(string encodedData)
        {
            // Example - oauth_token=hh5s93j4hdidpola&oauth_token_secret=hdhd0244k9j7ao03&oauth_callback_confirmed=true
            var decoded = HttpUtility.HtmlDecode(encodedData);
            decoded = WebUtility.UrlDecode(decoded);

            // Separate items
            // oauth_token=hh5s93j4hdidpola
            // oauth_token_secret=hdhd0244k9j7ao03
            // oauth_callback_confirmed=true
            var items = decoded.Split('&');

            // Separate key and value into a consumable structure.
            var result = new NameValueCollection(items.Length);
            foreach (var keyValuePair in items.Select(item => item.Split('=')))
            {
                result.Add(keyValuePair[0], keyValuePair[1]);
            }

            return result;
        }

        /// <summary>
        /// Creates the o authentication signature parameters.
        /// </summary>
        /// <param name="tokenKey">The token key.</param>
        /// <param name="tokenSecret">The token secret.</param>
        /// <param name="httpMethod">The HTTP method.</param>
        /// <param name="fullApiPath">The full API path.</param>
        /// <param name="contentType">Type of the content.</param>
        /// <param name="consumerKey">The consumer key.</param>
        /// <param name="consumerSecret">The consumer secret.</param>
        /// <returns>Signature input values</returns>
        public static OAuthSignatureParameters CreateOAuthSignatureParameters(
            string tokenKey,
            string tokenSecret,
            string httpMethod,
            string fullApiPath,
            string contentType,
            string consumerKey,
            string consumerSecret)
        {
            var oauthParameters = new OAuthParameters
            {
                CallbackUri = OAuthConstants.OutOfBand,
                ConsumerKey = consumerKey,
                Nonce = GenerateNonce(),
                SignatureMethod = CryptoConstants.HmacSha1,
                Token = string.IsNullOrEmpty(tokenKey) ? null : tokenKey,
                Timestamp = GenerateTimestamp().ToString(CultureInfo.InvariantCulture)
            };

            return new OAuthSignatureParameters(
                httpMethod,
                new Uri(fullApiPath),
                contentType,
                oauthParameters,
                consumerSecret,
                tokenSecret,
                _parseQueryString(fullApiPath),
                new NameValueCollection());
        }

        /// <summary>
        /// Append oauth signature and parameters to the end of a base url.
        /// </summary>
        /// <param name="baseUrl">Base url</param>
        /// <param name="parameters">Parameters to attach.  Signature will be generated inline.</param>
        /// <returns>A parameterized url string with base and oauth parameters.</returns>
        public static string CreateParameterizedOauthUrl(string baseUrl, OAuthSignatureParameters parameters)
        {
            return $"{baseUrl}?" +
                   $"oauth_consumer_key={parameters.OauthParameters.ConsumerKey}&amp;" +
                   $"oauth_signature_method={parameters.OauthParameters.SignatureMethod}&amp;" +
                   $"oauth_timestamp={parameters.OauthParameters.Timestamp}&amp;" +
                   $"oauth_nonce={parameters.OauthParameters.Nonce}&amp;" +
                   $"oauth_version={parameters.OauthParameters.Version}&amp;" +
                   $"oauth_signature={GenerateSignature(parameters)}";
            // TODO: Replace the above code with the below commmented out code when we figure out how to properly talk
            // TODO: to eaccounts.
            //return $"{baseUrl}?" +
            //       $"oauth_consumer_key={parameters.OauthParameters.ConsumerKey}&" +
            //       $"oauth_signature_method={parameters.OauthParameters.SignatureMethod}&" +
            //       $"oauth_timestamp={parameters.OauthParameters.Timestamp}&" +
            //       $"oauth_nonce={parameters.OauthParameters.Nonce}&" +
            //       $"oauth_version={parameters.OauthParameters.Version}&" +
            //       $"oauth_signature={GenerateSignature(parameters).PercentEncode()}";
        }

        /// <summary>
        /// Builds the authorization header.
        /// </summary>
        /// <param name="oauthSignatureParameters">The authentication signature parameters.</param>
        /// <returns>Properly formatted Authorization header with OAuth protocol parameters</returns>
        public static string BuildAuthorizationHeader(OAuthSignatureParameters oauthSignatureParameters)
        {
            Guard.IsNotNull(oauthSignatureParameters, "oauthSignatureParameters");
            Guard.IsNotNull(oauthSignatureParameters.OauthParameters, "oauthSignatureParameters.AuthParameters");

            var stringBuilder = new StringBuilder();

            // Append the required protocol parameters.
            stringBuilder.Append(OAuthConstants.OAuthAuthorizationScheme + " ");
            stringBuilder.Append(BuildAuthorizationHeaderParameter(OAuthConstants.OAuthConsumerKey, oauthSignatureParameters.OauthParameters.ConsumerKey) + ",");
            stringBuilder.Append(BuildAuthorizationHeaderParameter(OAuthConstants.OAuthSignatureMethod, oauthSignatureParameters.OauthParameters.SignatureMethod) + ",");
            stringBuilder.Append(BuildAuthorizationHeaderParameter(OAuthConstants.OAuthNonce, oauthSignatureParameters.OauthParameters.Nonce) + ",");
            stringBuilder.Append(BuildAuthorizationHeaderParameter(OAuthConstants.OAuthSignature, GenerateSignature(oauthSignatureParameters)) + ",");
            stringBuilder.Append(BuildAuthorizationHeaderParameter(OAuthConstants.OAuthCallback, oauthSignatureParameters.OauthParameters.CallbackUri) + ",");
            if (!string.IsNullOrEmpty(oauthSignatureParameters.OauthParameters.Token))
                stringBuilder.Append(BuildAuthorizationHeaderParameter(OAuthConstants.OAuthToken, oauthSignatureParameters.OauthParameters.Token) + ",");
            stringBuilder.Append(BuildAuthorizationHeaderParameter(OAuthConstants.OAuthTimestamp, oauthSignatureParameters.OauthParameters.Timestamp));

            return stringBuilder.ToString();
        }

        /// <summary>
        /// Builds the authorization header parameter.
        /// </summary>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <param name="parameterValue">The parameter value.</param>
        /// <returns>Formatted header parameter entry</returns>
        private static string BuildAuthorizationHeaderParameter(string parameterName, string parameterValue)
        {
            return parameterName + "=\"" + parameterValue.PercentEncode() + "\"";
        }

        /// <summary>
        /// Generates the signature.
        /// </summary>
        /// <param name="signatureParameters">The signature parameters.</param>
        /// <returns>
        /// The OAuth signature used for validation of incoming requests
        /// </returns>
        public static string GenerateSignature(OAuthSignatureParameters signatureParameters)
        {
            Guard.IsNotNull(signatureParameters, "signatureParameters");
            Guard.IsNotNull(signatureParameters.OauthParameters, "signatureParameters.OAuthParameters");

            var baseSignature = GenerateBaseSignature(
                signatureParameters.HttpMethod,
                signatureParameters.ServerUri,
                signatureParameters.OauthParameters,
                signatureParameters.ContentType,
                signatureParameters.QueryString,
                signatureParameters.FormData,
                null);
            var secret = string.Format(CultureInfo.InvariantCulture, "{0}&{1}", signatureParameters.ClientSecret.PercentEncode(), signatureParameters.TokenSecret.PercentEncode());

            return GenerateHashedSignature(signatureParameters.OauthParameters.SignatureMethod, secret, baseSignature);
        }

        /// <summary>
        /// Generates the signature.
        /// </summary>
        /// <param name="signatureParameters">The signature parameters.</param>
        /// <returns>
        /// The OAuth signature used for validation of incoming requests
        /// </returns>
        public static string GenerateSignatureV2(OAuthSignatureParameters signatureParameters)
        {
            Guard.IsNotNull(signatureParameters, "signatureParameters");
            Guard.IsNotNull(signatureParameters.OauthParameters, "signatureParameters.OAuthParameters");

            var baseSignature = GenerateBaseSignature(
                signatureParameters.HttpMethod,
                signatureParameters.ServerUri,
                signatureParameters.OauthParameters,
                signatureParameters.ContentType,
                signatureParameters.QueryString,
                signatureParameters.FormData,
                null);
            var secret = string.Format(CultureInfo.InvariantCulture, "{0}&{1}", signatureParameters.ClientSecret, signatureParameters.TokenSecret);

            return GenerateHashedSignature(signatureParameters.OauthParameters.SignatureMethod, secret, baseSignature);
        }

        /// <summary>
        /// Generates the signature.
        /// </summary>
        /// <param name="consumerSecret">The consumer secret</param>
        /// <param name="tokenSecret">The token secret</param>
        /// <param name="oauthParameters">The OAuth parameters.</param>
        /// <param name="request">The HTTP Request</param>
        /// <param name="portOverride">The port override. This value can be null.</param>
        /// <returns>
        /// The OAuth signature used for validation of incoming requests
        /// </returns>
        public static string GenerateSignature(string consumerSecret, string tokenSecret, OAuthParameters oauthParameters, ServiceStackRequestSurrogate request, int? portOverride)
        {
            if (oauthParameters == null)
            {
                return null;
            }

            var baseSignature = GenerateBaseSignature(oauthParameters, request, portOverride);
            var secret = string.Format(CultureInfo.InvariantCulture, "{0}&{1}", consumerSecret.PercentEncode(), tokenSecret.PercentEncode());

            return GenerateHashedSignature(oauthParameters.SignatureMethod, secret, baseSignature);
        }

        /// <summary>
        /// Generates the OAuth base signature. For more information, please
        /// <see cref="http://tools.ietf.org/html/rfc5849#section-3.4.1" />.
        /// </summary>
        /// <param name="oauthParameters">The automatic authentication parameters.</param>
        /// <param name="request">The HTTP Request</param>
        /// <param name="portOverride">The port override. This value can be null.</param>
        /// <returns>
        /// OAuth base signature
        /// </returns>
        public static string GenerateBaseSignature(OAuthParameters oauthParameters, ServiceStackRequestSurrogate request, int? portOverride)
        {
            Guard.IsNotNull(oauthParameters, "oauthParameters");
            Guard.IsNotNull(request, "request");

            return GenerateBaseSignature(
                request.Verb,
                new Uri(request.AbsoluteUri),
                oauthParameters,
                request.ContentType,
                request.QueryString,
                request.FormData,
                portOverride);
        }

        /// <summary>
        /// Generates the OAuth base signature. For more information, please
        /// <see cref="http://tools.ietf.org/html/rfc5849#section-3.4.1" />.
        /// </summary>
        /// <param name="apiMethod">The HTTP request method.  For example: "HEAD", "GET", "POST", etc.</param>
        /// <param name="serverUri">The server URI.</param>
        /// <param name="oauthParameters">The authentication parameters.</param>
        /// <param name="contentType">The content Type.</param>
        /// <param name="queryString">The query string.</param>
        /// <param name="formData">The form data.</param>
        /// <param name="portOverride">The port override. Typically used during development. This value can be null.</param>
        /// <returns>
        /// OAuth base signature
        /// </returns>
        public static string GenerateBaseSignature(
            string apiMethod,
            Uri serverUri,
            OAuthParameters oauthParameters,
            string contentType, // can be null, an example is a GET with no request data
            NameValueCollection queryString,
            NameValueCollection formData,
            int? portOverride) // portOverride can be null
        {

            var baseSignature = new StringBuilder();

            // The HTTP request method in uppercase.
            baseSignature.Append(apiMethod.ToUpper(CultureInfo.InvariantCulture));
            baseSignature.Append("&");

            // The base string URI from Section 3.4.1.2, after being encoded (Section 3.6).
            var baseStringUri = new StringBuilder();
            var scheme = serverUri.Scheme.ToLowerInvariant();
            baseStringUri.Append(scheme);
            baseStringUri.Append("://");
            baseStringUri.Append(serverUri.Host.ToLowerInvariant());
            if ((scheme == "http" && serverUri.Port != 80)
                || (scheme == "https" && serverUri.Port != 443))
            {
                baseStringUri.Append(":" + portOverride.GetValueOrDefault(serverUri.Port));
            }
            baseStringUri.Append(serverUri.AbsolutePath);
            baseSignature.Append(baseStringUri.ToString().PercentEncode());

            // Parameter sources. A list is used here instead of a SortedSet because parameters can be specified more than once
            // For example, a URL parameter and an entity-body parameter may both have the same name. Both parameters must be included
            // in the base signature. When we prepare the base signature we will sort the list.

            // The request parameters as normalized in Section 3.4.1.3.2, after being encoded (Section 3.6).
            var sortedParameters = (from string i in queryString select new NameValuePair { Name = i.PercentEncode(), Value = queryString[i].PercentEncode() }).ToList();

            // OAuth HTTP "Authorization" header fields
            //sortedParameters.AddRange(from nvp in oauthParameters.SortedParameters where !nvp.Name.Equals(OAuthConstants.OAuthSignature, StringComparison.OrdinalIgnoreCase) select new NameValuePair {Name = nvp.Name.PercentEncode(), Value = nvp.Value.PercentEncode()});
            sortedParameters.AddRange(_getAuthorizationFieldList(oauthParameters));

            // HTTP request entity-body
            if (contentType == WebServiceDefinitions.ContentTypeAsString(RequestHeaderSupportedContentType.FormEncoded))
            {
                sortedParameters.AddRange(from string i in formData select new NameValuePair { Name = i.PercentEncode(), Value = formData[i].PercentEncode() });
            }

            // Build the normalized parameter string from the complete list and append it to the base signature
            var baseParameterString = new StringBuilder();
            sortedParameters.OrderBy(i => i.Name).ThenBy(i => i.Value).ForEach(i => baseParameterString.AppendFormat("&{0}={1}", i.Name, i.Value));
            baseSignature.AppendFormat("&{0}", baseParameterString.ToString().Substring(1).PercentEncode());

            return baseSignature.ToString();
        }

        /// <summary>
        /// Creates the validation result.
        /// </summary>
        /// <param name="statusCode">The status code.</param>
        /// <param name="description">The description.</param>
        /// <returns>Result object</returns>
        public static OAuthParametersValidationResult CreateValidationResult(HttpStatusCode statusCode, string description)
        {
            return new OAuthParametersValidationResult { HttpStatusCode = statusCode, ResponseDescription = description };
        }

        /// <summary>
        ///     Generates the hashed signature.
        /// </summary>
        /// <param name="cryptoProviderType">
        ///     Type of the crypto provider.
        /// </param>
        /// <param name="secretKey">
        ///     The consumer key.
        /// </param>
        /// <param name="signatureBase">
        ///     The oauth signature base.
        /// </param>
        /// <returns>
        ///     The hashed signature
        /// </returns>
        public static string GenerateHashedSignature(string cryptoProviderType, string secretKey, string signatureBase)
        {
            Guard.IsNotNullOrWhiteSpace(cryptoProviderType, "cryptoProviderType");
            Guard.IsNotNullOrWhiteSpace(secretKey, "secretKey");
            Guard.IsNotNullOrWhiteSpace(signatureBase, "signatureBase");

            if (cryptoProviderType != CryptoConstants.HmacSha1)
            {
                throw new OauthException($"Crypto provider type {cryptoProviderType} is unsupported at this time.");
            }

            var encoding = Encoding.UTF8;
            var hashAlgorithm = new HMACSHA1();

            var dataBuffer = encoding.GetBytes(signatureBase);
            var keyBytes = encoding.GetBytes(secretKey);
            hashAlgorithm.Key = keyBytes;
            var hashBytes = hashAlgorithm.ComputeHash(dataBuffer);

            return Convert.ToBase64String(hashBytes);
        }

        /// <summary>
        ///     Generate the timestamp for the signature or for comparison purposes
        ///     <see cref="http://tools.ietf.org/html/rfc5849#section-3.3" />
        /// </summary>
        /// <returns>
        ///     Number of seconds since 01/01/1970 as a positive integer
        /// </returns>
        public static double GenerateTimestamp()
        {
            return Math.Round((DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0)).TotalSeconds);
        }

        /// <summary>
        /// Generate a nonce
        /// </summary>
        /// <returns>A randomly generated value</returns>
        public static string GenerateNonce()
        {
            return new Random().Next().ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Parses the query string.
        /// </summary>
        /// <param name="fullApiPath">The full API path.</param>
        /// <returns>Name value collection</returns>
        private static NameValueCollection _parseQueryString(string fullApiPath)
        {
            var queryStringIndex = fullApiPath.IndexOf("?", StringComparison.Ordinal);
            if (queryStringIndex < 0)
            {
                return new NameValueCollection();
            }

            return HttpUtility.ParseQueryString(fullApiPath.Substring(queryStringIndex + 1));
        }

        /// <summary>
        ///     Generates the token.
        /// </summary>
        /// <returns>
        ///     A token response
        /// </returns>
        public static OAuthTokenResponse GenerateToken()
        {
            return new OAuthTokenResponse { Token = Guid.NewGuid().ToString(), TokenSecret = RandomDataGenerator.GenerateRandomString(20) };
        }

        /// <summary>
        ///     Generates the temporary token.
        /// </summary>
        /// <returns>
        ///     Temporary OAuth Credentials<br />
        ///     For more information, please
        ///     <see cref="http://tools.ietf.org/html/rfc5849#section-2.1" />
        /// </returns>
        public static OAuthTemporaryCredentialResponse GenerateTemporaryToken()
        {
            return new OAuthTemporaryCredentialResponse { Token = Guid.NewGuid().ToString(), TokenSecret = RandomDataGenerator.GenerateRandomString(20) };
        }

        /// <summary>
        /// Get the fields that would be in the Authorization header as a list of strings.
        /// </summary>
        /// <param name="parameters"><see cref="OAuthParameters"/> object with the appropriate parameters present.</param>
        /// <param name="addTokenIfEmpty">Add the token value even if it is empty.</param>
        /// <returns><see cref="IEnumerable{T}"/> of <see cref="NameValuePair"/></returns>
        private static IEnumerable<NameValuePair> _getAuthorizationFieldList(OAuthParameters parameters, bool addTokenIfEmpty = false)
        {
            var oauthCallback = new NameValuePair
            {
                Name = $"{OAuthConstants.OAuthCallback}".PercentEncode(),
                Value = parameters.CallbackUri?.PercentEncode()
            };

            var consumerKey = new NameValuePair
            {
                Name = $"{OAuthConstants.OAuthConsumerKey}".PercentEncode(),
                Value = parameters.ConsumerKey.PercentEncode()
            };

            var nonce = new NameValuePair
            {
                Name = $"{OAuthConstants.OAuthNonce}".PercentEncode(),
                Value = parameters.Nonce.PercentEncode()
            };

            var signatureMethod = new NameValuePair
            {
                Name = $"{OAuthConstants.OAuthSignatureMethod}".PercentEncode(),
                Value = parameters.SignatureMethod.PercentEncode()
            };

            var timestamp = new NameValuePair
            {
                Name = $"{OAuthConstants.OAuthTimestamp}".PercentEncode(),
                Value = parameters.Timestamp.PercentEncode()
            };

            var token = new NameValuePair
            {
                Name = $"{OAuthConstants.OAuthToken}".PercentEncode(),
                Value = parameters.Token?.PercentEncode()
            };

            var version = new NameValuePair
            {
                Name = $"{OAuthConstants.OAuthVersion}".PercentEncode(),
                Value = parameters.Version?.PercentEncode()
            };

            var list = new List<NameValuePair>();
            if (oauthCallback.Value != null) list.Add(oauthCallback);
            list.Add(consumerKey);
            list.Add(nonce);
            list.Add(signatureMethod);
            list.Add(timestamp);
            if (token.Value != null) list.Add(token);
            if (version.Value != null) list.Add(version);

            return list;
        }
    }
}
