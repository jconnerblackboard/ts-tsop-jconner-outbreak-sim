﻿using System;
using System.IO;
using WinSCP;

namespace BbTS.Core.Communication
{
    /// <summary>
    /// Utility class for communicating over SFTP.
    /// </summary>
    public class SftpUtility
    {
        /// <summary>
        /// The hostname of the server hosting SFTP.
        /// </summary>
        public string Hostname { get; set; }

        /// <summary>
        /// The user name in which to use when authenticating.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// The path to the private key (relative to the WinSCP.exe executable).
        /// </summary>
        public string SshPrivateKeyPath { get; set; }

        /// <summary>
        /// Ignore the validation of the host SSH Key.
        /// </summary>
        public bool AcceptAnySshHostKey { get; set; }

        /// <summary>
        /// The ssh fingerprint of the host server.
        /// </summary>
        public string SshHostKeyFingerprint { get; set; }

        /// <summary>
        /// The path to the WinSCP executable.
        /// </summary>
        public string ExecutablePath { get; set; } = @"E:/Software/WinSCP/WinSCP.exe";

        /// <summary>
        /// Location of the temporary directory to use.
        /// </summary>
        public string TempDirectory { get; set; } = Directory.GetCurrentDirectory();

        /// <summary>
        /// Parameterized constructor requiring hostname and private key path.
        /// </summary>
        /// <param name="hostname">Hostname of the server.</param>
        /// <param name="username">Username to use when authenticating with the server.</param>
        /// <param name="sshPrivateKeyPath">Path to the ssh private key (relative to the WinSCP.exe file).</param>
        /// <param name="acceptAnySshHostKey">Accept any host fingerprint.</param>
        /// <param name="sshHostKeyFingerprint">The host fingerprint to use.</param>
        public SftpUtility(
            string hostname = "tsop-reportingmetrics.blackboard.com",
            string username = null,
            string sshPrivateKeyPath = null,
            bool acceptAnySshHostKey = true,
            string sshHostKeyFingerprint = null)
        {
            Hostname = hostname;
            Username = username ?? "cdeluca";
            SshPrivateKeyPath = sshPrivateKeyPath ?? "private_putty.ppk";
            AcceptAnySshHostKey = acceptAnySshHostKey;
            SshHostKeyFingerprint = sshHostKeyFingerprint ?? "ssh-rsa 2048 78:cc:9e:26:e0:20:f3:1a:98:c0:9a:78:23:f9:9a:14";
        }

        /// <summary>
        /// Save the contents of a file to a remote path.
        /// </summary>
        /// <param name="fileContents"></param>
        /// <param name="filename">The name of the file to create and upload.</param>
        /// <param name="remotePath"></param>
        public TransferOperationResult Save(string fileContents, string filename, string remotePath)
        {
            if (!Directory.Exists(TempDirectory))
            {
                throw new DirectoryNotFoundException($"Temp directory at location '{TempDirectory}' doesn't exit.  Cannot create temp file for transfter.  Aborting process.");
            }

            // Setup session options
            var sessionOptions = new SessionOptions
            {
                Protocol = Protocol.Sftp,
                HostName = Hostname,
                UserName = Username,
                SshPrivateKeyPath = SshPrivateKeyPath,
                GiveUpSecurityAndAcceptAnySshHostKey = AcceptAnySshHostKey,
                SshHostKeyFingerprint = SshHostKeyFingerprint
            };

            using (var session = new Session { ExecutablePath = ExecutablePath })
            {
                // Connect
                session.Open(sessionOptions);

                if (!session.FileExists(remotePath))
                {
                    CreateDirectory(remotePath);
                }

                // Upload files
                var transferOptions = new TransferOptions { TransferMode = TransferMode.Binary };
                
                var tempLocation = $"{TempDirectory}\\{filename}";
                File.AppendAllText(tempLocation, fileContents);

                var transferResult = session.PutFiles(tempLocation, remotePath, false, transferOptions);

                File.Delete(tempLocation);

                // Throw on any error
                transferResult.Check();

                return transferResult;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="remotePath"></param>
        public void CreateDirectory(string remotePath)
        {// Setup session options
            var sessionOptions = new SessionOptions
            {
                Protocol = Protocol.Sftp,
                HostName = Hostname,
                UserName = Username,
                SshPrivateKeyPath = SshPrivateKeyPath,
                GiveUpSecurityAndAcceptAnySshHostKey = AcceptAnySshHostKey,
                SshHostKeyFingerprint = SshHostKeyFingerprint
            };

            using (var session = new Session { ExecutablePath = ExecutablePath })
            {
                // Connect
                session.Open(sessionOptions);

                //decompose and create from base.
                var components = remotePath.Split('/');
                var tempPath = string.Empty;
                foreach (var component in components)
                {
                    tempPath = $"{tempPath}/{component}";
                    if (!session.FileExists(tempPath))
                    {
                        session.CreateDirectory(tempPath);
                    }
                }
            }
        }
    }
}
