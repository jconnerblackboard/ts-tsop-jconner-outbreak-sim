﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using BbTS.Core.Configuration;
using BbTS.Core.Conversion;
using BbTS.Core.Security.Encryption;
using BbTS.Domain.Models.Communication;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Definitions.Notification;
using BbTS.Domain.Models.Notification;
using BbTS.Monitoring.Logging;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace BbTS.Core.Communication.AzureTableStorage
{
    /// <summary>
    /// Utility class for generic entity CRUD operations against an Azure Table Storage
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class AzureTableStorageUtility<T> where T : TableEntity, IAzureTableStorage, new()
    {
        /// <summary>
        /// Connection string for the table containing the map of institution id to name.
        /// </summary>
        private const string InstitutionNameMapConnectionString = "DefaultEndpointsProtocol=https;AccountName=bb-tsop-prod-us-east-us-east-me;AccountKey=21c7phoEJmLJEmOMzPRNgVQhXvlOGjE5dlIgbiOw9SFBzca2YROdFAdSLIL0IpAUX20NgHlVQRwax8GT6yxeCg==;TableEndpoint=https://bb-tsop-prod-us-east-us-east-me.table.cosmosdb.azure.com:443/;";
        
        private readonly CloudTable _table;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="tableName"></param>
        public AzureTableStorageUtility(AzureTableStorageTableNames tableName)
        {
            // README!!
            // Uncomment the line 1 below and comment out line 2 to enable TLS version 1.2, 1.1, 1.0
            // Line 1, Uncomment to enable TLS version 1.2, 1.1, 1.0 and comment out Line 2
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            // Line 2, Uncomment to enable TLS version 1.2 ONLY, comment out Line 1
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            
            switch (tableName)
            {
                case AzureTableStorageTableNames.ProcessingLog:
                case AzureTableStorageTableNames.ProcessingJobStartLog:
                case AzureTableStorageTableNames.InstitutionName:
                case AzureTableStorageTableNames.MetricsRules:
                case AzureTableStorageTableNames.MetricsProcessingStartTime:
                {
                    TableStorageConnectionString = InstitutionNameMapConnectionString;
                }
                    break;
                default:
                {
                    // Create the table if it doesn't exist.
                    TableStorageConnectionString = _connectionStringGet();
                }
                    break;
            }

            // Create the table client.
            _table = _getTable(tableName.ToString());
        }

        /// <summary>
        /// Connection string for azure table storage.
        /// </summary>
        public string TableStorageConnectionString { get; internal set; }

        /// <summary>
        /// Get the azure table storage connection string based upon the ServicePointBaseURI registry setting.
        /// </summary>
        /// <returns>Azure Table Storage connection string.</returns>
        private string _connectionStringGet()
        {
            var aesEncryptionProvider = new AesEncryptionProvider();
            var connectionConfiguration = RegistryConfigurationTool.Instance.ValueGet("ReportingMetricsConnectionString");
            Guard.IsNotNullOrWhiteSpace(connectionConfiguration, nameof(connectionConfiguration));
            var baseString = aesEncryptionProvider.Decrypt(connectionConfiguration.Base64Decode());
            return baseString;
        }

        /// <summary>
        /// Insert (or replace) operation.
        /// </summary>
        /// <param name="entity">Entity to insert.</param>
        /// <returns></returns>
        public ReportingMetricTableStorageWriteResponse Insert(T entity)
        {
            try
            {
                var insertOperation = TableOperation.InsertOrReplace(entity);
                var result = _table.Execute(insertOperation);
                return new ReportingMetricTableStorageWriteResponse
                {
                    Message = result.Etag,
                    Result =
                        result.HttpStatusCode < 300 && result.HttpStatusCode >= 200
                            ? EventHubNotificationUploadResult.Success
                            : EventHubNotificationUploadResult.Failure
                };
            }
            catch (Exception e)
            {
                return new ReportingMetricTableStorageWriteResponse
                {
                    Message = Formatting.FormatException(e),
                    Result = EventHubNotificationUploadResult.Failure
                };
            }

        }

        /// <summary>
        /// Insert (or replace) operation.
        /// </summary>
        /// <param name="entityList">List of entities to insert.</param>
        /// <returns></returns>
        public ReportingMetricTableStorageWriteResponse BulkInsert(List<T> entityList)
        {
            try
            {
                var batchOperation = new TableBatchOperation();
                entityList.ForEach(e => batchOperation.Insert(e));

                var result = _table.ExecuteBatch(batchOperation);
                var failedResult = result?.FirstOrDefault(r => r.HttpStatusCode > 300 || r.HttpStatusCode < 200);

                return new ReportingMetricTableStorageWriteResponse
                {
                    Message = failedResult == null ? EventHubNotificationUploadResult.Success.ToString() : EventHubNotificationUploadResult.Failure.ToString(),
                    Result = failedResult == null ? EventHubNotificationUploadResult.Success : EventHubNotificationUploadResult.Failure
                };
            }
            catch (Exception e)
            {
                return new ReportingMetricTableStorageWriteResponse
                {
                    Message = Formatting.FormatException(e),
                    Result = EventHubNotificationUploadResult.Failure
                };
            }

        }

        /// <summary>
        /// Read all entities.
        /// </summary>
        /// <returns></returns>
        public List<T> ReadAll()
        {
            var query = new TableQuery<T>();
            var entities = _table.ExecuteQuery(query).ToList();

            return entities;
        }

        /// <summary>
        /// Read all entities.
        /// </summary>
        /// <param name="partitionKey">Partition key to read</param>
        /// <returns></returns>
        public List<T> ReadAll(string partitionKey)
        {
            var whereClause = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, partitionKey);
            var query = new TableQuery<T>().Where(whereClause);
            var entities = _table.ExecuteQuery(query).ToList();

            return entities;
        }

        /// <summary>
        /// Read all entities based on the provided search criteria.
        /// </summary>
        /// <param name="whereClause">Where clause for the query</param>
        /// <returns></returns>
        public List<T> Read(string whereClause)
        {
            var query = new TableQuery<T>().Where(whereClause);
            var entities = _table.ExecuteQuery(query).ToList();

            return entities;
        }

        /// <summary>
        /// Find a specific entry.
        /// </summary>
        /// <param name="partitionKey">Partiiton key to search for.</param>
        /// <param name="rowKey">Row key to search for.</param>
        /// <returns></returns>
        public T Find(string partitionKey, string rowKey)
        {
            var query = new TableQuery<T>().Where(
                TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, partitionKey),
                    TableOperators.And,
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, rowKey)));

            var tableSet = _table.ExecuteQuery(query).ToList();

            return tableSet.Count >= 1 ? tableSet.First() : null;
        }

        #region Private Functions

        /// <summary>
        /// Create a table for the sample application to process messages in. 
        /// </summary>
        /// <returns>A CloudTable object</returns>
        private CloudTable _getTable(string tableName)
        {
            // Retrieve storage account information from connection string.
            var storageAccount = _createStorageAccountFromConnectionString(TableStorageConnectionString);

            // Create a table client for interacting with the table service
            var tableClient = storageAccount.CreateCloudTableClient();

            // Create a table client for interacting with the table service 
            var table = tableClient.GetTableReference(tableName);
            if (!table.ExistsAsync().Result)
            {
                throw new ApplicationException($"Table '{tableName} not found!");
            }

            return table;
        }

        /// <summary>
        /// Validate the connection string information in app.config and throws an exception if it looks like 
        /// the user hasn't updated this to valid values. 
        /// </summary>
        /// <param name="storageConnectionString">Connection string for the storage service or the emulator</param>
        /// <returns>CloudStorageAccount object</returns>
        private CloudStorageAccount _createStorageAccountFromConnectionString(string storageConnectionString)
        {
            CloudStorageAccount storageAccount;
            try
            {
                storageAccount = CloudStorageAccount.Parse(storageConnectionString);
            }
            catch (FormatException)
            {
                Console.WriteLine(@"Invalid storage account information provided. Please confirm the AccountName and AccountKey are valid in the app.config file - then restart the application.");
                throw;
            }
            catch (ArgumentException)
            {
                Console.WriteLine(@"Invalid storage account information provided. Please confirm the AccountName and AccountKey are valid in the app.config file - then restart the sample.");
                Console.ReadLine();
                throw;
            }

            return storageAccount;
        }

#endregion
    }
}
