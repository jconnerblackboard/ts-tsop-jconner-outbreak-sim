        /// Private constructor.  Use the singleton.
﻿using System;
using System.Diagnostics;
using System.Net;
using System.Text;
using BbTS.Core.Configuration;
using BbTS.Core.Conversion;
using BbTS.Core.Logging.Tracing;
using BbTS.Core.Security.Encryption;
using BbTS.Domain.Models.Communication.EventHub;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Monitoring.Logging;
using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using TransportType = Microsoft.ServiceBus.Messaging.TransportType;

namespace BbTS.Core.Communication.EventHub
{
    /// <summary>
    /// Utility class for logging information to an external event hub.
    /// </summary>
    public class EventHubLoggingUtility
    {
        /// <summary>
        /// Singleton Instance.
        /// </summary>
        [Trace(AttributeExclude = true, AttributePriority = 2)]
        public static EventHubLoggingUtility Instance { get; internal set; } = new EventHubLoggingUtility();

        private EventHubClient _hubClient;

        private static readonly object SendLock = new object();

        /// <summary>
        /// The name of the event hub.
        /// </summary>
        [Trace(AttributeExclude = true, AttributePriority = 2)]
        public string HubName { get; set; } = "tsopeventhublog";

        /// <summary>
        /// Private constructor.  Use the singleton.
        /// </summary>
        [Trace(AttributeExclude = true, AttributePriority = 2)]
        private EventHubLoggingUtility()
        {
            // README!!
            // Uncomment the line 1 below and comment out line 2 to enable TLS version 1.2, 1.1, 1.0
            // Line 1, Uncomment to enable TLS version 1.2, 1.1, 1.0 and comment out Line 2
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            // Line 2, Uncomment to enable TLS version 1.2 ONLY, comment out Line 1
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            // ServicePointManager.DefaultConnectionLimit = 1000;
        }

        #region Private Functions.

        /// <summary>
        /// Get the event hub client.  Initialize first if needed.
        /// </summary>
        /// <returns></returns>
        [Trace(AttributeExclude = true, AttributePriority = 2)]
        private EventHubClient _getClient()
        {
            _initializeHub();
            return _hubClient;
        }

        /// <summary>
        /// Initialize the event hub.
        /// </summary>
        [Trace(AttributeExclude = true, AttributePriority = 2)]
        private void _initializeHub()
        {
            if (_hubClient != null && !_hubClient.IsClosed) return;

            var builder = new ServiceBusConnectionStringBuilder(EventHubLoggingEndPointUri)
            {
                OperationTimeout = TimeSpan.FromSeconds(15),
                TransportType = TransportType.Amqp
            };

            var messagingFactory = MessagingFactory.CreateFromConnectionString(builder.ToString());
            _hubClient = messagingFactory.CreateEventHubClient(HubName);

            var retryPolicy = new RetryExponential(TimeSpan.FromSeconds(3), TimeSpan.FromSeconds(15), 3);
            _hubClient.RetryPolicy = retryPolicy;
        }

        #endregion

        /// <summary>
        /// Get the event hub endpoint uri.
        /// </summary>
        /// <returns>The unencrypted event hub endpoint uri.</returns>
        [Trace(AttributeExclude = true, AttributePriority = 2)]
        public string EventHubLoggingEndPointUri
        {
            get
            {
                var aesEncryptionProvider = new AesEncryptionProvider();

                var connectionConfiguration = RegistryConfigurationTool.Instance.ValueGet("EventHubLoggingEndpoint");
                Guard.IsNotNullOrWhiteSpace(connectionConfiguration, nameof(connectionConfiguration));
                var baseString = aesEncryptionProvider.Decrypt(connectionConfiguration.Base64Decode());
                if (!baseString.Contains("TransportType=Amqp"))
                {
                    baseString = $"{baseString};TransportType=Amqp";
                }
                return baseString;
            }
        }

        /// <summary>
        /// Send a message to the event hub.
        /// </summary>
        /// <param name="obj">The object containing relevant information to send.</param>
        public void SendEventHubMessage(EventHubLoggingObject obj)
        {
            try
            {
                var bytes = Encoding.UTF8.GetBytes(obj.Data);
                var message = new EventData(bytes)
                {
                    Properties =
                    {
                        ["InstitutionId"] = obj.InstitutionId,
                        ["Severity"] = obj.Severity,
                        ["Message"] = obj.Message,
                        ["LogSource"] = obj.LogSource,
                        ["EventId"] = obj.EventId,
                        ["EventCategory"] = obj.EventCategory,
                        ["EventDateTime"] = obj.EventDateTime.ToString("O")
                    }
                };
                lock (SendLock)
                {
                    var hubClient = _getClient();
                    hubClient.Send(message);
                }
            }
            catch (TimeoutException te)
            {
                var message = $"TimeoutException has occured while logging an object to an event hub: '{Formatting.FormatException(te)}'\r\n\r\n" +
                              $"{EventHubObjectToString(obj)}";

                LoggingManager.Instance.LogException(
                    message, EventLogEntryType.Warning,
                    (short)LoggingDefinitions.Category.EventHubAgent,
                    (int)LoggingDefinitions.EventId.EventHubAgentNotificationsHubSendNotificationTimeout);
                throw;

            }
            catch (Exception ex)
            {
                var message = $"Exception has occured: '{Formatting.FormatException(ex)}'\r\n\r\n" +
                              $"{EventHubObjectToString(obj)}";
                LoggingManager.Instance.LogException(
                    message, EventLogEntryType.Error,
                    (short)LoggingDefinitions.Category.EventHubAgent,
                    (int)LoggingDefinitions.EventId.EventHubAgentNotificationsHubSendNotificationFailure);
                throw;
            }
        }

        /// <summary>
        /// Convert an EventHubObject to a human readable string.
        /// </summary>
        /// <param name="obj">the object</param>
        /// <returns></returns>
        public static string EventHubObjectToString(EventHubLoggingObject obj)
        {
            return
                $"RequestId: '{obj.RequestId}'\r\n" +
                $"EventDateTime: '{obj.EventDateTime:O}\r\n" +
                $"EventId: '{obj.EventId}\r\n" +
                $"EventCategory: '{obj.EventCategory}\r\n" +
                $"InstitutionId: '{obj.InstitutionId}\r\n" +
                $"Message: '{obj.Message}\r\n" +
                $"Data: '{obj.Data}\r\n";
        }
    }
}
