﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Communication.BbHost;
using BbTS.Domain.Models.Definitions.Door;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Monitoring.Logging;

namespace BbTS.Core.Communication.BbHost
{
    /// <summary>
    /// Host Reconnect Event:
    /// ConnectionClients will subscribe to this event when
    /// communication with the host is either lost or
    /// cannot be established.
    /// </summary>
    public delegate void HostReconnectEvent();

    /// <summary>
    /// Status Change Event:
    /// Triggering this event will happen when an event is
    /// triggered in the database for alert incidents.
    /// </summary>
    public delegate void StatusChangeEvent();

    /// <summary>
    /// Communication class for subscribing to and handling callbacks from the BbHost's stats protocol for door access.
    /// </summary>
    public class StatsConnectionClient
    {
        /// <summary>
        /// Host reconnect event.
        /// </summary>
        public static event HostReconnectEvent HostReconnectEvent;

        /// <summary>
        /// Status changed event.
        /// </summary>
        public static event StatusChangeEvent StatusChangeEvent;

        #region Private Member Variables

        private TcpClient _clientForStats;
        private SslStream _sslStreamForStats;

        private TcpClient _clientForHostCommuncation;
        private SslStream _sslStreamForHostComminication;

        //private readonly string _server;
        private readonly int _port;
        private bool _continue;
        private StatsResponsePacket _responsePacket;
        private static byte[] _buffer = new byte[2048];
        private string _posid;
        private Thread _statsThread;
        private byte _lastMode;
        private readonly object _lockObject = new object();

        #endregion

        /// <summary>
        /// Initialization
        /// </summary>
        /// <param name="server">The ip address of the server.</param>
        /// <param name="doorId">The id of the door.</param>
        /// <param name="doorAddress">The address on the master controller where the door is attached.</param>
        /// <param name="masterControllerId">The id of the master controller where the door is attached.</param>
        /// <param name="doorName">The name of the door.</param>
        public StatsConnectionClient(string server, int doorId, short doorAddress, int masterControllerId, string doorName = "")
        {
            _port = 1321; // 1321
            _continue = true;
            _responsePacket = new StatsResponsePacket();
            _lastMode = 0x0;

            StatsConnection = new StatsConnection
            {
                HostConnectionIpAddress = server,
                DoorName = doorName,
                DoorAddress = doorAddress,
                DoorId = doorId,
                MasterControllerId = masterControllerId
            };
        }

        /// <summary>
        /// Initialization
        /// </summary>
        /// <param name="statsConnection">A preloaded stats connection to use.</param>
        public StatsConnectionClient(StatsConnection statsConnection)
        {
            _port = 1321; // 1321
            _continue = true;
            _responsePacket = new StatsResponsePacket();
            _lastMode = 0x0;

            StatsConnection = statsConnection;
        }

        #region Properties

        /// <summary>
        /// Get the response packet
        /// </summary>
        public StatsResponsePacket ResponsePacket
        {
            get { lock (_lockObject) { return _responsePacket; } }
            set { lock (_lockObject) { _responsePacket = value; } }
        }

        /// <summary>
        /// Get the continue value.
        /// </summary>
        public bool Continue
        {
            get { lock (_lockObject) { return _continue; } }
            set { lock (_lockObject) { _continue = value; } }
        }

        /// <summary>
        /// Get/set the pos id.
        /// </summary>
        public string PosId
        {
            get { lock (_lockObject) { return _posid; } }
            set { lock (_lockObject) { _posid = value; } }
        }

        /// <summary>
        /// Get/Set the bytes read.
        /// </summary>
        public byte[] BytesRead { get; set; }

        /// <summary>
        /// The door status associated with this stats client.
        /// </summary>
        public StatsConnection StatsConnection { get; set; }

        #endregion

        #region Functions

        #region Stats Listener management.

        /// <summary>
        /// Connect to a socket connection.
        /// </summary>
        public void OpenStatsConnection()
        {
            try
            {
                _clientForStats = new TcpClient(StatsConnection.HostConnectionIpAddress, _port);
                _sslStreamForStats = new SslStream(_clientForStats.GetStream(), false, CertificateValidationCallback, CertificateSelectionCallback);

                try
                {
                    var cert = X509Certificate.CreateFromCertFile("ca.pem");
                    var certs = new X509CertificateCollection { cert };

                    _sslStreamForStats.AuthenticateAsClient(StatsConnection.HostConnectionIpAddress, certs, SslProtocols.Tls12, false);
                }
                catch (AuthenticationException ex)
                {
                    Formatting.FormatException(ex);
                    _clientForStats.Close();
                    return;
                }

                // connect to the host
                _statsThread = new Thread(SendHostStatsConnectionMessage) { IsBackground = true };

                // send the subscription packet
                _statsThread.Start();
            }
            catch (SocketException sockex)
            {
                var message = $"Cannot connect to host.  Queue for retry.  Exception was: {Formatting.FormatException(sockex)}";
                LoggingManager.Instance.LogDebugMessage(
                    message, EventLogEntryType.Warning,
                    (int)LoggingDefinitions.EventId.CommunicationUtility,
                    (short)LoggingDefinitions.EventCategory.CommunicationUtilityCouldNotConnect);
            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(
                    ex, "", "", EventLogEntryType.Error,
                    (int)LoggingDefinitions.EventId.CommunicationUtility);
            }
        }

        /// <summary>
        /// Close communication.
        /// </summary>
        public void CloseStatsConnection()
        {
            try
            {
                _clientForStats.Close();
            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(
                   ex, "", "", EventLogEntryType.Error,
                   (int)LoggingDefinitions.EventId.CommunicationUtility);
            }
        }

        /// <summary>
        /// Send socket message
        /// </summary>
        public void SendHostStatsConnectionMessage()
        {
            try
            {
                // create the subscription payload
                byte[] payload = HostPacketsUtility.Instance.CreateEmptySubscriptionPayload();
                HostPacketsUtility.Instance.UpdateStatsPayloadInfo(ref payload, (UInt32)StatsConnection.MasterControllerId);

                // create the subscription packet
                byte[] packet = HostPacketsUtility.Instance.CreateHostPacket(payload, (UInt32)StatsConnection.MasterControllerId, (int)DoorStatusMonitorPacketTypes.StatsSubscription);

                SendStatsConnection(packet);
            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(
                   ex, "", "", EventLogEntryType.Error,
                   (int)LoggingDefinitions.EventId.CommunicationUtility);
            }
        }

        /// <summary>
        /// Send socket message
        /// </summary>
        /// <param name="data">bytes to send.</param>
        public void SendStatsConnection(byte[] data)
        {
            try
            {
                // write the data
                _sslStreamForStats.Write(data, 0, data.Length);
                _sslStreamForStats.Flush();

                // enter the read loop
                _readLoopForStats();
            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(
                   ex, "", "", EventLogEntryType.Error,
                   (int)LoggingDefinitions.EventId.CommunicationUtility);
            }
        }

        /// <summary>
        /// Enter the read loop.
        /// </summary>
        private void _readLoopForStats()
        {
            try
            {
                _sslStreamForStats.BeginRead(_buffer, 0, _buffer.Length, _handleReadForStats, _sslStreamForStats);
            }
            catch (IOException iex)
            {
                var message = $"Connection to the host has been lost.  Queueing for reconnect.  Exception was: {Formatting.FormatException(iex)}";
                LoggingManager.Instance.LogDebugMessage(
                    message, EventLogEntryType.Warning,
                    (int)LoggingDefinitions.EventId.CommunicationUtility,
                    (short)LoggingDefinitions.EventCategory.CommunicationUtilityCouldNotConnect);
            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(
                   ex, "", "", EventLogEntryType.Error,
                   (int)LoggingDefinitions.EventId.CommunicationUtility);
            }
        }

        /// <summary>
        /// Handle incoming communication from the host.
        /// </summary>
        /// <param name="ar"></param>
        private void _handleReadForStats(IAsyncResult ar)
        {
            var stream = (SslStream)ar.AsyncState;

            try
            {
                stream.EndRead(ar);

                lock (_lockObject)
                {
                    StatsResponsePacket responsePacket;

                    if (StatsPacketUtility.Parse(_buffer, out responsePacket))
                    {
                        var message = $"State for door attached to master controller '{responsePacket.PosId}' was changed to '{responsePacket.Mode}'";
                        LoggingManager.Instance.LogMessage(message);
                        _responsePacket = responsePacket;
                    }
                }

                // if the mode changed fire a communications event
                if (_lastMode != ResponsePacket.Mode)
                {

                    _lastMode = ResponsePacket.Mode;
                    OnStatusChangeEvent();
                }

                Thread.Sleep(10);

                _buffer = new byte[2048];

                stream.BeginRead(_buffer, 0, _buffer.Length, _handleReadForStats, stream);
            }
            catch (IOException iex)
            {
                var message = $"Connection to the host has been lost.  Queueing for reconnect.  Exception was: {Formatting.FormatException(iex)}";
                LoggingManager.Instance.LogDebugMessage(
                    message, EventLogEntryType.Warning,
                    (int)LoggingDefinitions.EventId.CommunicationUtility,
                    (short)LoggingDefinitions.EventCategory.CommunicationUtilityCouldNotConnect);
            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(
                   ex, "", "", EventLogEntryType.Error,
                   (int)LoggingDefinitions.EventId.CommunicationUtility);
            }
        }

        #endregion


        /// <summary>
        /// Connect to a socket connection.
        /// </summary>
        public byte[] SendHostRequest(byte[] data)
        {
            _clientForHostCommuncation = new TcpClient(StatsConnection.HostConnectionIpAddress, _port);
            _sslStreamForHostComminication = new SslStream(_clientForHostCommuncation.GetStream(), false, CertificateValidationCallback, CertificateSelectionCallback);

            try
            {
                var cert = X509Certificate.CreateFromCertFile("ca.pem");
                var certs = new X509CertificateCollection { cert };

                _sslStreamForHostComminication.AuthenticateAsClient(StatsConnection.HostConnectionIpAddress, certs, SslProtocols.Tls12, false);
            }
            catch (AuthenticationException ex)
            {
                LoggingManager.Instance.LogException(
                    ex, "", "", EventLogEntryType.Error,
                    (int)LoggingDefinitions.EventId.CommunicationUtility);
                throw;
            }
            // write the data
            _sslStreamForHostComminication.Write(data, 0, data.Length);
            _sslStreamForHostComminication.Flush();

            // Read the response
            var buffer = new byte[2048];
            var bytes = _sslStreamForHostComminication.Read(buffer, 0, buffer.Length);
            var responseBytes = new byte[bytes];
            Buffer.BlockCopy(buffer, 0, responseBytes, 0, responseBytes.Length);


            return responseBytes;
        }

        /// <summary>
        /// Private function to parse the response from the host.
        /// </summary>
        /// <param name="sslStream">String in communication with the host.</param>
        /// <returns></returns>
        private static string _readHostResponse(SslStream sslStream)
        {
            // Read the  message sent by the server.
            // The end of the message is signaled using the
            // "<EOF>" marker.
            byte[] buffer = new byte[2048];
            StringBuilder messageData = new StringBuilder();
            //do
            //{
                var bytes = sslStream.Read(buffer, 0, buffer.Length);

                // Use Decoder class to convert from bytes to UTF8
                // in case a character spans two buffers.
                Decoder decoder = Encoding.UTF8.GetDecoder();
                char[] chars = new char[decoder.GetCharCount(buffer, 0, bytes)];
                decoder.GetChars(buffer, 0, bytes, chars, 0);
                messageData.Append(chars);
                // Check for EOF.
            //    if (messageData.ToString().IndexOf("<EOF>", StringComparison.Ordinal) != -1)
            //    {
            //        break;
            //    }
            //} while (bytes != 0);

            return messageData.ToString();
        }




        #endregion

        #region Certificate Handling

        /// <summary>
        /// The following method is invoked by the RemoteCertificateValidationDelegate.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="certificate"></param>
        /// <param name="chain"></param>
        /// <param name="sslPolicyErrors"></param>
        /// <returns></returns>
        public static bool ValidateServerCertificate(
              object sender,
              X509Certificate certificate,
              X509Chain chain,
              SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors == SslPolicyErrors.None)
                return true;

            Console.WriteLine("Certificate error: {0}", sslPolicyErrors);

            // Do not allow this client to communicate with unauthenticated servers.
            return false;
        }

        /// <summary>
        /// Needs definition.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="targetHost"></param>
        /// <param name="localCertificates"></param>
        /// <param name="remoteCertificate"></param>
        /// <param name="acceptableIssuers"></param>
        /// <returns></returns>
        public static X509Certificate CertificateSelectionCallback(
            object sender,
            string targetHost,
            X509CertificateCollection localCertificates,
            X509Certificate remoteCertificate,
            string[] acceptableIssuers)
        {
            return localCertificates[0];
        }

        /// <summary>
        /// Needs definition.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="certificate"></param>
        /// <param name="chain"></param>
        /// <param name="sslPolicyErrors"></param>
        /// <returns></returns>
        public static bool CertificateValidationCallback(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            //if (sslPolicyErrors != SslPolicyErrors.None)
            //{
            //    Console.WriteLine("SSL Certificate Validation Error!");
            //    Console.WriteLine(sslPolicyErrors.ToString());
            //    return false;
            //}
            //else
            return true;
        }


        #endregion

        #region Callback Functions

        /// <summary>
        /// Call on all attached listeners to reconnect.
        /// </summary>
        public static void OnHostReconnectEvent()
        {
            HostReconnectEvent?.Invoke();
        }

        /// <summary>
        /// Call on all attached listeners for a status changed event.
        /// </summary>
        public static void OnStatusChangeEvent()
        {
            StatusChangeEvent?.Invoke();
        }

        #endregion
    }
}
