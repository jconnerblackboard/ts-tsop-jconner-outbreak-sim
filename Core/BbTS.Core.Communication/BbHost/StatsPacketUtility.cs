﻿using System;
using BbDeviceDashboard.Tools;
using BbTS.Domain.Models.Communication.BbHost;

namespace BbTS.Core.Communication.BbHost
{
    /// <summary>
    /// Utility class for creating, parsing and handling BbHost Stats Packets.
    /// </summary>
    public static class StatsPacketUtility
    {
        /// <summary>
        /// Parse a byte array for the Stats Packet
        /// </summary>
        /// <param name="packet">byte packet to parse</param>
        /// <param name="result">where to place the result.</param>
        /// <returns></returns>
        public static bool Parse(byte[] packet, out StatsResponsePacket result)
        {
            result = new StatsResponsePacket();

            // check for a valid size
            if (packet.Length < 45)
            {
                return false;
            }

            // position in the stream
            int position = 0;

            //-------------------------------------------------------------------------------------
            // Envision Header
            //-------------------------------------------------------------------------------------
            result.Length = (UInt16)BitConverter.ToInt16(packet, position); position += 2;
            result.IdByte = packet[position]; position++;
            result.LenComp = (UInt16)BitConverter.ToInt16(packet, position); position += 2;
            //-------------------------------------------------------------------------------------
            // Inner Header
            //-------------------------------------------------------------------------------------
            result.SequenceNumber = (UInt16)BitConverter.ToInt16(packet, position); position += 2;
            result.PosId = (UInt32)BitConverter.ToInt16(packet, position); position += 4;
            result.ModuleChar = packet[position]; position++;
            result.ProcessNumber = (UInt16)BitConverter.ToInt16(packet, position); position += 2;
            //-------------------------------------------------------------------------------------
            // Stats Payload
            //-------------------------------------------------------------------------------------
            result.Zero = (UInt16)BitConverter.ToInt16(packet, position); position += 2;
            result.StatsType = (UInt16)BitConverter.ToInt16(packet, position); position += 2;
            result.StatsId = (UInt32)BitConverter.ToInt16(packet, position); position += 4;
            result.SessionCount = (UInt32)BitConverter.ToInt16(packet, position); position += 4;
            result.RequestCount = (UInt32)BitConverter.ToInt16(packet, position); position += 4;
            result.BadLrcCount = (UInt32)BitConverter.ToInt16(packet, position); position += 4;
            result.BadHeaderCount = (UInt32)BitConverter.ToInt16(packet, position); position += 4;
            result.NoResponseCount = (UInt32)BitConverter.ToInt16(packet, position); position += 4;
            result.Mode = packet[position]; position++;
            //-------------------------------------------------------------------------------------
            // CRC
            //-------------------------------------------------------------------------------------
            result.Crc = (UInt16)BitConverter.ToInt16(packet, position);

            byte[] fullPacket = MakeStatsBytePacket(result);
            byte[] crcPacket = new byte[38];

            Buffer.BlockCopy(fullPacket, 5, crcPacket, 0, 38);

            // initialize with the Blackboard CRC style
            CrcHelper.Init(CrcHelper.CRCCode.BBCRC16);
            ulong crcCheck = CrcHelper.crctable(crcPacket);

            return result.Crc == (UInt16)crcCheck;
        }

        /// <summary>
        /// Make a stats byte packet from source packet
        /// </summary>
        /// <returns></returns>
        public static byte[] MakeStatsBytePacket(StatsResponsePacket source)
        {
            byte[] packet = new byte[45];

            int position = 0;

            //-------------------------------------------------------------------------------------
            // Envision Header
            //-------------------------------------------------------------------------------------
            byte[] lengthArray = BitConverter.GetBytes(source.Length);
            Buffer.BlockCopy(lengthArray, 0, packet, position, lengthArray.Length);
            position += lengthArray.Length;

            packet[position] = source.IdByte;
            position++;

            byte[] lencompArray = BitConverter.GetBytes(source.LenComp);
            Buffer.BlockCopy(lencompArray, 0, packet, position, lencompArray.Length);
            position += lencompArray.Length;

            //-------------------------------------------------------------------------------------
            // Inner Header
            //-------------------------------------------------------------------------------------
            byte[] seqnumArray = BitConverter.GetBytes(source.SequenceNumber);
            Buffer.BlockCopy(seqnumArray, 0, packet, position, seqnumArray.Length);
            position += seqnumArray.Length;

            byte[] posidArray = BitConverter.GetBytes(source.PosId);
            Buffer.BlockCopy(posidArray, 0, packet, position, posidArray.Length);
            position += posidArray.Length;

            packet[position] = source.ModuleChar;
            position++;

            byte[] procnumArray = BitConverter.GetBytes(source.ProcessNumber);
            Buffer.BlockCopy(procnumArray, 0, packet, position, procnumArray.Length);
            position += procnumArray.Length;

            //-------------------------------------------------------------------------------------
            // Stats Payload
            //-------------------------------------------------------------------------------------
            byte[] zeroArray = BitConverter.GetBytes(source.Zero);
            Buffer.BlockCopy(zeroArray, 0, packet, position, zeroArray.Length);
            position += zeroArray.Length;

            byte[] statsTypeArray = BitConverter.GetBytes(source.StatsType);
            Buffer.BlockCopy(statsTypeArray, 0, packet, position, statsTypeArray.Length);
            position += statsTypeArray.Length;

            byte[] statsIdArray = BitConverter.GetBytes(source.StatsId);
            Buffer.BlockCopy(statsIdArray, 0, packet, position, statsIdArray.Length);
            position += statsIdArray.Length;

            byte[] sessionCountArray = BitConverter.GetBytes(source.SessionCount);
            Buffer.BlockCopy(sessionCountArray, 0, packet, position, sessionCountArray.Length);
            position += sessionCountArray.Length;

            byte[] requestCountArray = BitConverter.GetBytes(source.RequestCount);
            Buffer.BlockCopy(requestCountArray, 0, packet, position, requestCountArray.Length);
            position += requestCountArray.Length;

            byte[] badLrcCountArray = BitConverter.GetBytes(source.BadLrcCount);
            Buffer.BlockCopy(badLrcCountArray, 0, packet, position, badLrcCountArray.Length);
            position += badLrcCountArray.Length;

            byte[] badHeaderCountArray = BitConverter.GetBytes(source.BadHeaderCount);
            Buffer.BlockCopy(badHeaderCountArray, 0, packet, position, badHeaderCountArray.Length);
            position += badHeaderCountArray.Length;

            byte[] noResponseCountArray = BitConverter.GetBytes(source.NoResponseCount);
            Buffer.BlockCopy(noResponseCountArray, 0, packet, position, noResponseCountArray.Length);
            position += noResponseCountArray.Length;

            packet[position] = source.Mode;
            position++;

            //-------------------------------------------------------------------------------------
            // CRC
            //-------------------------------------------------------------------------------------
            byte[] crcArray = BitConverter.GetBytes(source.Crc);
            Buffer.BlockCopy(crcArray, 0, packet, position, crcArray.Length);
            return packet;
        }
    }
}
