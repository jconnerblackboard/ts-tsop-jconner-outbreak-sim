﻿using System;
using System.Diagnostics;
using BbTS.Domain.Models.Definitions.Door;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Monitoring.Logging;

namespace BbTS.Core.Communication.BbHost
{
    /// <summary>
    /// Utility class for creating host packets for BbHost functions.
    /// </summary>
    public class HostPacketsUtility
    {
        private static readonly object SequenceLock = new object();
        private static short _sequenceNumber;

        private static short NextSequenceNumber
        {
            get
            {
                lock (SequenceLock)
                {
                    return ++_sequenceNumber;
                }
            }
        }

        private static readonly object ChangeRequestLock = new object();
        private static int _changeRequestNumber;
        private static int ChangeRequestNumberAndIncrement
        {
            get
            {
                lock (ChangeRequestLock)
                {
                    return ++_changeRequestNumber;
                }
            }
        }

        private static HostPacketsUtility _instance;
        /// <summary>
        /// Instanced accessor.
        /// </summary>
        public static HostPacketsUtility Instance
        {
            get { return _instance ?? (_instance = new HostPacketsUtility()); }
            set { _instance = value; }
        }

        /// <summary>
        /// Private constructor.  Initialize counters.
        /// </summary>
        private HostPacketsUtility()
        {
            Random random = new Random();
            _sequenceNumber = (short)random.Next(1, 1000);
            _changeRequestNumber = random.Next(1, 1000);
        }

        /// <summary>
        /// Create an empty subscription payload packet
        /// </summary>
        /// <returns></returns>
        public byte[] CreateEmptySubscriptionPayload()
        {
            return new byte[7];
        }

        /// <summary>
        /// Create an empty door status change payload packet
        /// </summary>
        /// <returns></returns>
        public byte[] CreateEmptyDoorStatusChangePayload()
        {
            return new byte[16];
        }

        /// <summary>
        /// Create an empty mux mode change payload packet
        /// </summary>
        /// <returns></returns>
        public byte[] CreateEmptyMuxModeChangePpayload()
        {
            return new byte[16];
        }

        /// <summary>
        /// Create the envision header
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="procnum"></param>
        /// <returns></returns>
        public byte[] CreateEnvisionHeader2(UInt32 pid, short procnum)
        {
            // header is the header1 and header2 portion
            byte[] header = new byte[9];

            // get the sequence number
            byte[] sequenceNumber = BitConverter.GetBytes(NextSequenceNumber);

            // set the POS id
            byte[] posId = BitConverter.GetBytes(pid);

            // set the module char
            byte moduleChar = Convert.ToByte('R');

            // set the process number
            byte[] processNumber = BitConverter.GetBytes(procnum);

            // position in the stream
            int position = 0;

            // copy the sequence number into the header
            Buffer.BlockCopy(sequenceNumber, 0, header, position, sequenceNumber.Length);
            position += sequenceNumber.Length;

            // copy the spos_id into the header
            Buffer.BlockCopy(posId, 0, header, position, posId.Length);
            position += posId.Length;

            // copy the module char into the header
            header[position] = moduleChar;
            position++;

            // copy the spos_id into the header
            Buffer.BlockCopy(processNumber, 0, header, position, processNumber.Length);

            return header;
        }

        /// <summary>
        /// Update the envision header1 values (length, idbyte, lencomp)
        /// </summary>
        /// <param name="packetSize"></param>
        /// <returns></returns>
        public byte[] CreateHeader1(Int16 packetSize)
        {
            // create the header
            byte[] header = new byte[5];

            // update the size header field
            byte[] length = BitConverter.GetBytes(packetSize);
            Buffer.BlockCopy(length, 0, header, 0, length.Length);

            // update the lencomp field
            Int16 iLencomp = (Int16)(15000 - packetSize);
            byte[] lencomp = BitConverter.GetBytes(iLencomp);
            Buffer.BlockCopy(lencomp, 0, header, 3, lencomp.Length);

            // update the size header field
            byte idByte = (byte)(0x01 ^ lencomp[0]);
            header[2] = idByte;

            return header;
        }

        /// <summary>
        /// Update/set the payload value
        /// </summary>
        /// <param name="payload"></param>
        /// <param name="posId"></param>
        public void UpdateStatsPayloadInfo(ref byte[] payload, UInt32 posId)
        {
            // make sure there are enough bytes for the payload
            if (payload.Length < 7)
            {
                return;
            }

            // position in the payload array
            int position = 0;

            // set the subscription message
            payload[position] = Convert.ToByte('T');
            position++;

            // set the type
            Int16 iType = 4;
            byte[] type = BitConverter.GetBytes(iType);
            Buffer.BlockCopy(type, 0, payload, position, type.Length);
            position += type.Length;

            // set the id
            byte[] id = BitConverter.GetBytes(posId);
            Buffer.BlockCopy(id, 0, payload, position, id.Length);
        }

        /// <summary>
        /// Update/set the crc value
        /// </summary>
        /// <param name="packet"></param>
        public void UpdateCrc(ref byte[] packet)
        {
            // create a packet wihtout the crc value attached so it
            // can be computed.
            byte[] packetWithoutCrc = new byte[packet.Length - 2];
            Buffer.BlockCopy(packet, 0, packetWithoutCrc, 0, packetWithoutCrc.Length);

            // put the crc back in the packet
            BbDeviceDashboard.Tools.CrcHelper.Init(BbDeviceDashboard.Tools.CrcHelper.CRCCode.BBCRC16);
            UInt16 longCrc = (UInt16)BbDeviceDashboard.Tools.CrcHelper.crctable(packetWithoutCrc);

            byte[] crc = BitConverter.GetBytes(longCrc);
            Buffer.BlockCopy(crc, 0, packet, packetWithoutCrc.Length, crc.Length);
        }

        /// <summary>
        /// Update/set the change door state payload
        /// </summary>
        /// <param name="masterControllerId">The identifier of the master controller the door is attached to.</param>
        /// <param name="doorAddress">The address of the door on the master controller.</param>
        /// <param name="newState">The new state.</param>
        /// <param name="duration">The duration of the state change.</param>
        public byte[] CreateDoorStateChangePacket(int masterControllerId, short doorAddress, Int16 newState, Int16 duration)
        {
            var payload = new byte[16];

            try
            {
                // position in the payload array
                int position = 0;

                // set the door id
                Int32 id = masterControllerId;
                byte[] idAsBytes = BitConverter.GetBytes(id);
                Buffer.BlockCopy(idAsBytes, 0, payload, position, idAsBytes.Length);
                position += idAsBytes.Length;

                // set the door index
                Int16 address = doorAddress;
                byte[] addressAsBytes = BitConverter.GetBytes(address);
                Buffer.BlockCopy(addressAsBytes, 0, payload, position, addressAsBytes.Length);
                position += addressAsBytes.Length;

                // set the override value
                byte[] newStateAsBytes = BitConverter.GetBytes(newState);
                Buffer.BlockCopy(newStateAsBytes, 0, payload, position, newStateAsBytes.Length);
                position += newStateAsBytes.Length;

                // set the duration value
                byte[] durationAsBytes = BitConverter.GetBytes(duration);
                Buffer.BlockCopy(durationAsBytes, 0, payload, position, durationAsBytes.Length);
                position += durationAsBytes.Length;

                // set the retry value
                Int16 retry = 30;
                byte[] retryAsBytes = BitConverter.GetBytes(retry);
                Buffer.BlockCopy(retryAsBytes, 0, payload, position, retryAsBytes.Length);
                position += retryAsBytes.Length;

                // set the change request id
                Int32 changeRequestId = ChangeRequestNumberAndIncrement;
                byte[] changeRequestIdAsBytes = BitConverter.GetBytes(changeRequestId);
                Buffer.BlockCopy(changeRequestIdAsBytes, 0, payload, position, changeRequestIdAsBytes.Length);

                return CreateHostPacket(payload, (uint)masterControllerId, (short)HostPacketTypes.DoorStateChange);
            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(
                   ex, "", "", EventLogEntryType.Error,
                   (short)LoggingDefinitions.Category.HostPacketsUtility);
                throw;
            }
        }

        /// <summary>
        /// Create a host packet.  Add the inner/outter headers and calculate the crc.
        /// </summary>
        /// <param name="payload"></param>
        /// <param name="posId"></param>
        /// <param name="packetType"></param>
        /// <returns></returns>
        public byte[] CreateHostPacket(byte[] payload, UInt32 posId, short packetType)
        {
            // packet size
            var packetSize = (Int16)(9 + payload.Length + 2);

            // get the header
            byte[] header2 = CreateEnvisionHeader2(posId, packetType);

            // update the fields in the header
            byte[] header1 = CreateHeader1(packetSize);

            // final packet
            byte[] packet = new byte[packetSize];

            // position in the stream
            int position = 0;

            // copy the sequence number into the header
            Buffer.BlockCopy(header2, 0, packet, position, header2.Length);
            position += header2.Length;

            // copy the pos_id into the header
            Buffer.BlockCopy(payload, 0, packet, position, payload.Length);
            position += payload.Length;

            // set the crc
            Int16 crc = 0;
            byte[] crcAsBytes = BitConverter.GetBytes(crc);

            // copy the crc
            Buffer.BlockCopy(crcAsBytes, 0, packet, position, crcAsBytes.Length);

            // update the crc value
            UpdateCrc(ref packet);

            position = 0;

            // create the final packet
            byte[] finalPacket = new byte[packetSize + 5];

            // copy header1 into the final packet
            Buffer.BlockCopy(header1, 0, finalPacket, position, header1.Length);
            position += header1.Length;

            // copy the rest of the packet
            Buffer.BlockCopy(packet, 0, finalPacket, position, packet.Length);

            return finalPacket;
        }

        #region Points

        /*
        /// <summary>
        /// Update/set the change door state payload
        /// </summary>
        /// <param name="payload"></param>
        /// <param name="point"></param>
        /// <param name="commandSubtype"></param>
        /// <param name="overrideStatus"></param>
        /// <param name="overrideOption1"></param>
        /// <param name="overrideOption2"></param>*/
        //public static void UpdateMuxModeChangePayload(
        //    ref byte[] payload,
        //    Source.Interfaces.ISmPoint point,
        //    byte commandSubtype,
        //    byte overrideStatus,
        //    byte overrideOption1,
        //    byte overrideOption2)
        //{
        //    // make sure there are enough bytes for the payload
        //    if (payload.Length < 16)
        //    {
        //        return;
        //    }

        //    try
        //    {
        //        // position in the payload array
        //        int position = 0;

        //        // set the door id
        //        Int32 id = Int32.Parse(point.mastercontroller_id());
        //        byte[] idAsBytes = BitConverter.GetBytes(id);
        //        Buffer.BlockCopy(idAsBytes, 0, payload, position, idAsBytes.Length);
        //        position += idAsBytes.Length;

        //        // set the command subtype
        //        commandSubtype = (byte)((int)commandSubtype + 1);
        //        payload[position] = commandSubtype;
        //        position++;

        //        // set the point number
        //        byte pointNumber = byte.Parse(point.point_num());
        //        payload[position] = pointNumber;
        //        position++;

        //        // set the command subtype
        //        byte direction = (byte)point.type();
        //        payload[position] = direction;
        //        position++;

        //        // set the command subtype
        //        payload[position] = overrideStatus;
        //        position++;

        //        // set the command subtype
        //        payload[position] = overrideOption1;
        //        position++;

        //        // set the command subtype
        //        payload[position] = overrideOption2;
        //        position++;

        //        // set the change request id
        //        Int32 changeRequestId = ChangeRequestNumberAndIncrement;
        //        byte[] changeRequestIdAsBytes = BitConverter.GetBytes(changeRequestId);
        //        Buffer.BlockCopy(changeRequestIdAsBytes, 0, payload, position, changeRequestIdAsBytes.Length);
        //        position += changeRequestIdAsBytes.Length;

        //        // set the retry value
        //        Int16 retry = 30;
        //        byte[] retryAsBytes = BitConverter.GetBytes(retry);
        //        Buffer.BlockCopy(retryAsBytes, 0, payload, position, retryAsBytes.Length);
        //        position += retryAsBytes.Length;

        //    }
        //    catch (Exception ex)
        //    {
        //        LoggingManager.Instance.LogException(
        //           ex, "", "", EventLogEntryType.Error,
        //           (short)LoggingDefinitions.Category.HostPacketsUtility);
        //    }
        //}

        #endregion
    }

}
