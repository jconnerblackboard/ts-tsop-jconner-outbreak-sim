﻿using System;
using System.Collections.Generic;
using System.IO;
using BbTS.Core.Configuration;
using BbTS.Core.Conversion;
using System.Text.RegularExpressions;
using System.Linq;
using BbTS.Domain.Models.General;

namespace BbTS.Core.System.FileUtilities
{
    public class DirectoryTools
    {
        public static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, true);
                }
            }
        }

        /// <summary>
        /// Recursively get a list of {files} in the {path} with the supplied {extension}.
        /// </summary>
        /// <param name="files">list of found files</param>
        /// <param name="path">root path</param>
        /// <param name="searchString">search string (i.e. for all xml files pass *.xml)</param>
        /// <param name="message">results message</param>
        /// <returns></returns>
        public static bool GetFilePathsWithExtension(ref List<string> files, string path, string searchString, out string message)
        {
            try
            {
                // Verify and create the directory if it doesn't exist
                if (!FileConfigurationManager.VerifyAndCreateDirectory(path, out message)) return false;

                var directories = Directory.GetDirectories(path);
                if (files == null) files = new List<string>(Directory.GetFiles(path, searchString));
                else files.AddRange(new List<string>(Directory.GetFiles(path, searchString)));

                foreach (var directory in directories)
                {
                    var subDirectories = Directory.GetDirectories(directory);
                    if (subDirectories.Length != 0)
                    {
                        if (!GetFilePathsWithExtension(ref files, directory, searchString, out message)) return false;
                    }
                    files.AddRange(new List<string>(Directory.GetFiles(directory, searchString)));
                }
            }
            catch (Exception ex)
            {
                message = $"Unable to retrieve complete list of files.  Please verify that the full path to the directory was specified in the configuration file.\r\n\r\nError was {Formatting.FormatException(ex)}";
                return false;
            }

            message = "Success";
            return true;
        }

        /// <summary>
        /// Groups entry names in the directory by the subpart of the name based on regex
        /// </summary>
        /// <param name="sourceDirName">Directory to process</param>
        /// <param name="regexPattern">Match pattern</param>
        /// <returns>Tuple of subpart of the entry names with the occurrence count</returns>
        public static IEnumerable<Tuple<string, int>> GetEntryNamesCountByPattern(string sourceDirName, string regexPattern)
        {
            var entryNames = Directory.EnumerateFileSystemEntries(sourceDirName, "*", SearchOption.TopDirectoryOnly).Select(Path.GetFileName);
            var regex = new Regex(regexPattern);

            var entries = new List<string>();
            foreach (var entryName in entryNames)
            {
                var match = regex.Match(entryName);
                if (match.Success)
                    entries.Add(match.Value);
            }

            return entries.GroupBy(x => x).Select(x => Tuple.Create(x.Key, x.Count()));
        }

        /// <summary>
        /// Deletes oldest (based on entry name) exceeding entries which are grouped by the subpart of their name
        /// </summary>
        /// <param name="sourceDirName">Directory to process</param>
        /// <param name="entryNames">Subpart of the entry names that are looked up in the directory</param>
        /// <param name="keepCount">Determines how many entries can be kept</param>
        public static void DeleteExceedingEntriesByContainedName(string sourceDirName, IEnumerable<string> entryNames, int keepCount)
        {
            foreach(var entryName in entryNames)
            {
                var directoryInfo = new DirectoryInfo(sourceDirName);
                var entries = directoryInfo.EnumerateFileSystemInfos($"{entryName}*", SearchOption.TopDirectoryOnly).OrderByDescending(x => x.Name).Skip(keepCount);
                foreach (var entry in entries)
                {
                    // We need to check if it's directory as the FileSystemInfo class does not allow to delete non-empty directories
                    if ((entry.Attributes & FileAttributes.Directory) == FileAttributes.Directory)
                        Directory.Delete(entry.FullName, true);
                    else
                        entry.Delete();
                }

            }
        }

        /// <summary>
        /// Read a delimited data file.
        /// </summary>
        /// <param name="path">Path to the file.</param>
        /// <param name="columnDelimiter">Column delimiter.</param>
        /// <returns></returns>
        public static DelimitedDataFile ReadDelimitedDataFile(string path, char columnDelimiter = ',')
        {
            if (!File.Exists(path))
            {
                throw new FileNotFoundException($"File was not found at the given location '{path}'", path);
            }

            var fileString = File.ReadAllLines(path);

            var delimitedDataFile = new DelimitedDataFile { ColumnDelimiter = columnDelimiter };
            foreach (var rowString in fileString)
            {
                delimitedDataFile.Rows.Add(rowString.Split(columnDelimiter));
            }

            return delimitedDataFile;
        }

        /// <summary>
        /// Delete the entire directory tree under (and including) path.
        /// </summary>
        /// <param name="path">Path to delete</param>
        public static void DeleteDirectoryTree(string path)
        {
            if (Directory.Exists(path))
            {
                Directory.Delete(path, true);
            }
        }

    }
}
