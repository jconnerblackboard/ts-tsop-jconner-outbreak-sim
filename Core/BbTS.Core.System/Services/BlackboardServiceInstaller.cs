using System;
using System.Configuration.Install;
using System.Diagnostics;
using System.ServiceProcess;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Monitoring.Logging;

namespace BbTS.Core.System.Services
{
    /// <summary>
    /// util class to make installing/stop/start/uninstall service a breeze
    /// </summary>
    public static class BlackboardServiceInstaller
    {
        /// <summary>
        /// Starts the service.
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        /// <param name="timeoutMilliseconds">The timeout milliseconds.</param>
        public static void StartService(string serviceName, int timeoutMilliseconds)
        {
            ServiceController service = new ServiceController(serviceName);
            try
            {
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                service.Start();
                service.WaitForStatus(ServiceControllerStatus.Running, timeout);
                LoggingManager.Instance.LogMessage(
                    $"{LoggingManager.LogSource} service was started.",
                    (short)LoggingDefinitions.Category.Service,
                    (int)LoggingDefinitions.EventId.ServiceStarted);
            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(
                    ex,
                    $"{serviceName} service was unable to start.",
                    "",
                    EventLogEntryType.Error,
                    (short)LoggingDefinitions.Category.Service,
                    (int)LoggingDefinitions.EventId.ServiceStartException);
                Environment.Exit(-1);
            }
        }

        /// <summary>
        /// Stops the service.
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        public static void StopService(string serviceName)
        {
            ServiceController service = new ServiceController(serviceName);
            try
            {
                TimeSpan timeout = TimeSpan.FromMilliseconds(30000);
                service.Stop();
                service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
                LoggingManager.Instance.LogMessage(
                    $"{LoggingManager.LogSource} service stopped.",
                    (short)LoggingDefinitions.Category.Service,
                    (int)LoggingDefinitions.EventId.SerivceStopped);
            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(
                   ex,
                    $"{serviceName} service was unable to stop.",
                   "",
                   EventLogEntryType.Error,
                    (short)LoggingDefinitions.Category.Service,
                    (int)LoggingDefinitions.EventId.ServiceStopException);
                Environment.Exit(-1);
            }
        }

        /// <summary>
        /// Installs the service.
        /// </summary>
        /// <param name="exeFilename">The exe filename.</param>
        public static void InstallService(string exeFilename)
        {
            try
            {
                AssemblyInstaller assemblyInstaller = new AssemblyInstaller(exeFilename, null) { UseNewContext = true };
                assemblyInstaller.Install(null);
                LoggingManager.Instance.LogMessage(
                    $"{LoggingManager.LogSource} service installed.",
                    (short)LoggingDefinitions.Category.Service,
                    (int)LoggingDefinitions.EventId.ServiceInstalled);
            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(
                   ex,
                    $"{exeFilename}: unable to install service.",
                   "",
                   EventLogEntryType.Error,
                    (short)LoggingDefinitions.Category.Service,
                    (int)LoggingDefinitions.EventId.SerivceInstallException);

                Environment.Exit(-1);
            }
        }

        /// <summary>
        /// Uninstalls the service.
        /// </summary>
        /// <param name="exeFilename">The exe filename.</param>
        public static void UninstallService(string exeFilename)
        {
            try
            {
                AssemblyInstaller assemblyInstaller = new AssemblyInstaller(exeFilename, null) { UseNewContext = true };
                assemblyInstaller.Uninstall(null);
                LoggingManager.Instance.LogMessage(
                    $"{LoggingManager.LogSource} service removed.",
                    (short)LoggingDefinitions.Category.Service,
                    (int)LoggingDefinitions.EventId.ServiceUninstalled);
            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(
                    ex,
                    $"{exeFilename}: unable to remove service.",
                    "",
                    EventLogEntryType.Error,
                    (short)LoggingDefinitions.Category.Service,
                    (int)LoggingDefinitions.EventId.ServiceUninstallException);
                Environment.Exit(-1);
            }
        }
    }
}