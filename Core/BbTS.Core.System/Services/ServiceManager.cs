﻿using System;
using System.ServiceProcess;
using BbTS.Domain.Models.Definitions.Terminal;
using BbTS.Domain.Models.System;

namespace BbTS.Core.System.Services
{
    /// <summary>
    /// The controller class
    /// </summary>
    public class ServiceManager
    {
        #region Enums
        /// <summary>
        /// The various states supported by this class
        /// </summary>
        public enum ServiceState
        {
            Start,
            Stop,
            Restart
        }
        #endregion

        #region Overload for string based service status values
        /// <summary>
        /// This overload allows for a string to be passed for the service state so the enum doesn't have to be used
        /// </summary>
        /// <param name="serviceName">The name of the service.</param>
        /// <param name="timeoutMilliseconds">The amount of time (in milliseconds) to wait for the service to respond properly.</param>
        /// <param name="serviceState">The state of the service desired.</param>
        public static void ServiceControllerStatusSet(String serviceName, Int32 timeoutMilliseconds, String serviceState)
        {
            ServiceControllerStatusSet(serviceName, timeoutMilliseconds, (ServiceState)Enum.Parse(typeof(ServiceState), serviceState));
        }
        #endregion

        #region Hydra overloads
        /// <summary>
        /// This overload allows for a string to be passed for the service state so the enum doesn't have to be used and returns an object suitable for Hydra usage
        /// </summary>
        /// <param name="serviceName">The name of the service.</param>
        /// <param name="timeoutMilliseconds">The amount of time (in milliseconds) to wait for the service to respond properly.</param>
        /// <param name="serviceState">The state of the service desired.</param>
        /// <returns>A <see cref="ActionResultToken"/> object</returns>
        public static ActionResultToken HydraServiceControllerStatusSet(String serviceName, Int32 timeoutMilliseconds, String serviceState)
        {

            try
            {
                ServiceControllerStatusSet(serviceName, timeoutMilliseconds, (ServiceState)Enum.Parse(typeof(ServiceState), serviceState));
            }
            catch(Exception ex)
            {
                var actionResultToken = DomainTerminalDefinitions.Instance.GetActionResultCode(TerminalActionResultDomainIdCode.ServiceControllerStatusError);
                actionResultToken.Message += ex;
                return actionResultToken;
            }

            return DomainTerminalDefinitions.Instance.GetActionResultCode(TerminalActionResultDomainIdCode.Success);
        }

        /// <summary>
        /// This method controls the starting/stopping of the requested service and returns an object suitable for Hydra usage
        /// </summary>
        /// <param name="serviceName">The name of the service.</param>
        /// <param name="timeoutMilliseconds">The amount of time (in milliseconds) to wait for the service to respond properly.</param>
        /// <param name="serviceState">The state of the service desired.</param>
        /// <returns>A <see cref="ActionResultToken"/> object</returns>
        public static ActionResultToken HydraServiceControllerStatusSet(String serviceName, Int32 timeoutMilliseconds, ServiceState serviceState)
        {
            try
            {
                ServiceControllerStatusSet(serviceName, timeoutMilliseconds, serviceState);
            }
            catch(Exception ex)
            {
                var actionResultToken = DomainTerminalDefinitions.Instance.GetActionResultCode(TerminalActionResultDomainIdCode.ServiceControllerStatusError);
                actionResultToken.Message += ex;
                return actionResultToken;
            }

            return DomainTerminalDefinitions.Instance.GetActionResultCode(TerminalActionResultDomainIdCode.Success);
        }
        #endregion

        #region Public methods
        /// <summary>
        /// This method controls the starting/stopping of the requested service.
        /// </summary>
        /// <param name="serviceName">The name of the service.</param>
        /// <param name="timeoutMilliseconds">The amount of time (in milliseconds) to wait for the service to respond properly.</param>
        /// <param name="serviceState">The state of the service desired.</param>
        public static void ServiceControllerStatusSet(String serviceName, Int32 timeoutMilliseconds, ServiceState serviceState)
        {
            using (ServiceController service = new ServiceController(serviceName))
            {
                TimeSpan timeoutTimeSpan = new TimeSpan(0, 0, 0, 0, timeoutMilliseconds);

                switch (serviceState)
                {
                    case ServiceState.Start:
                        StartService(service, timeoutTimeSpan);
                        break;
                    case ServiceState.Stop:
                        StopService(service, timeoutTimeSpan);
                        break;
                    case ServiceState.Restart:
                        RestartService(service, timeoutTimeSpan);
                        break;
                }
            }
        }
        #endregion

        #region Private methods
        /// <summary>
        /// This method will start a Windows service
        /// </summary>
        /// <param name="service">The name of the service.</param>
        /// <param name="timeoutTimeSpan">The amount of time to wait for the service to respond properly.</param>
        public static void StartService(ServiceController service, TimeSpan timeoutTimeSpan)
        {
            try
            {
                // If the service is already started or starting, exit true when the service is fully running
                if(service.Status.Equals(ServiceControllerStatus.Running) || service.Status.Equals(ServiceControllerStatus.StartPending))
                {
                    service.WaitForStatus(ServiceControllerStatus.Running, timeoutTimeSpan);
                    return;
                }

                // Try to start the stopped/stopping service
                if(service.Status == ServiceControllerStatus.Stopped || service.Status.Equals(ServiceControllerStatus.StopPending))
                {
                    // Wait for the stop to completely
                    service.WaitForStatus(ServiceControllerStatus.Stopped, timeoutTimeSpan);

                    // Start the service and wait for it to complete starting
                    service.Start();
                    service.WaitForStatus(ServiceControllerStatus.Running, timeoutTimeSpan);
                    return;
                }
            }
            catch(Exception e)
            {
                throw new Exception(String.Format("Unable to start the service:{0}. \r\nDetails:{1}", service, e));
            }

            // If you get here, something didn't go right
            throw new Exception(String.Format("Starting the {0} service resulted in an unexpected condition.", service));
        }

        /// <summary>
        /// This method will stop a Windows service
        /// </summary>
        /// <param name="service">The name of the service.</param>
        /// <param name="timeoutTimeSpan">The amount of time to wait for the service to respond properly.</param>
        public static void StopService(ServiceController service, TimeSpan timeoutTimeSpan)
        {
            try
            {
                // If the service is already started or starting, exit true when the service is fully running
                if(service.Status.Equals(ServiceControllerStatus.Stopped) || service.Status.Equals(ServiceControllerStatus.StopPending))
                {
                    service.WaitForStatus(ServiceControllerStatus.Stopped, timeoutTimeSpan);
                    return;
                }

                // Try to stop the running/start pending service
                if(service.Status == ServiceControllerStatus.Running || service.Status.Equals(ServiceControllerStatus.StartPending))
                {
                    // Wait for the start to completely
                    service.WaitForStatus(ServiceControllerStatus.Running, timeoutTimeSpan);

                    // Stop the service and wait for it to complete starting
                    service.Stop();
                    service.WaitForStatus(ServiceControllerStatus.Stopped, timeoutTimeSpan);
                    return;
                }
            }
            catch(Exception e)
            {
                throw new Exception(String.Format("Unable to start the service:{0}. \r\nDetails:{1}", service, e));
            }

            // If you get here, something didn't go right
            throw new Exception(String.Format("Stopping the {0} service resulted in an unexpected condition.", service));
        }

        /// <summary>
        /// This method will stop then start the requested service
        /// </summary>
        /// <param name="service">The name of the service.</param>
        /// <param name="timeoutTimeSpan">The amount of time to wait for the service to respond properly.</param>
        public static void RestartService(ServiceController service, TimeSpan timeoutTimeSpan)
        {
            try
            {
                // Stop and start the service and then go home
                StopService(service, timeoutTimeSpan);
                StartService(service, timeoutTimeSpan);
            }
            catch(Exception e)
            {
                throw new Exception(String.Format("Unable to restart the service:{0}. \r\nDetails:{1}", service, e));
            }
        }
        #endregion
    }
}

