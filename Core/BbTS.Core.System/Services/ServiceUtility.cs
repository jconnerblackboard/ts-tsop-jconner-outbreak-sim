﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using BbTS.Domain.Models.Definitions.Service;

namespace BbTS.Core.System.Services
{
    public class ServiceUtility
    {
        /// <summary>
        /// Get the Http protocol preable from the security mode
        /// </summary>
        /// <param name="securityMode"></param>
        /// <returns></returns>
        public static string GetHttpProtocolPreamble(WebHttpSecurityMode securityMode)
        {
            switch (securityMode)
            {
                case WebHttpSecurityMode.Transport:
                    return "https:";
                default:
                    return "http:";
            }
        }
    }
}
