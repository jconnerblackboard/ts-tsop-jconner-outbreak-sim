using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BbTS.Domain.Models.System;

namespace BbTS.Core.System.Time
{
    public class DateTimeUtility
    {
        /// <summary>
        /// Get the first rule matching the time zone information.
        /// </summary>
        /// <param name="tzi">Timezone to check</param>
        /// <returns>Timezone rules associated with tzi</returns>
        public static TimeZoneInfo.AdjustmentRule CurrentAdjustmentRule(TimeZoneInfo tzi)
        {
            List<TimeZoneInfo.AdjustmentRule> rules = CurrentAdjustmentRules(tzi);
            DateTime now = DateTime.Now;
            foreach (var rule in rules)
            {
                if (rule.DateEnd >= now && rule.DateStart <= now)
                {
                    return rule;
                }
            }
            return null;
        }

        /// <summary>
        /// Get the first rule matching the time zone information.  Time zone is assumed to be TimeZoneInfo.Local.
        /// </summary>
        /// <returns>Timezone rules associated with tzi</returns>
        public static TimeZoneInfo.AdjustmentRule CurrentAdjustmentRule(out TimeZoneInfo tzi)
        {
            tzi = TimeZoneInfo.Local;
            return CurrentAdjustmentRule(tzi);
        }

        /// <summary>
        /// Get all of the rules matching the time zone information.
        /// </summary>
        /// <param name="tzi">Timezone to check</param>
        /// <returns>Timezone rules associated with tzi</returns>
        public static List<TimeZoneInfo.AdjustmentRule> CurrentAdjustmentRules(TimeZoneInfo tzi)
        {
            return new List<TimeZoneInfo.AdjustmentRule>(tzi.GetAdjustmentRules());
        }

        /// <summary>
        /// Get all of the rules matching the time zone information.  Time zone is assumed to be TimeZoneInfo.Local.
        /// </summary>
        /// <returns>Timezone rules associated with tzi</returns>
        public static List<TimeZoneInfo.AdjustmentRule> CurrentAdjustmentRules()
        {
            TimeZoneInfo tzi = TimeZoneInfo.Local;
            return CurrentAdjustmentRules(tzi);
        }

        /// <summary>
        /// Returns the transition date for the specified year and transition rule.
        /// </summary>
        /// <param name="year"></param>
        /// <param name="transitionTime"></param>
        /// <returns></returns>
        private static DateTime TransitionDateGet(int year, TimeZoneInfo.TransitionTime transitionTime)
        {
            if (transitionTime.IsFixedDateRule) return new DateTime(year, transitionTime.Month, transitionTime.Day);

            var date = new DateTime(year, transitionTime.Month, 1);
            if (transitionTime.Week == 5)
            {
                //last DayOfWeek in the month
                date = date.AddMonths(1);
                date = date.AddDays(-1);
                while (date.DayOfWeek != transitionTime.DayOfWeek)
                {
                    date = date.AddDays(-1);
                }
            }
            else
            {
                //nth DayOfWeek in the month
                date = date.AddDays(-1);

                for (int i = 0; i < transitionTime.Week; i++)
                {
                    date = date.AddDays(1);
                    while (date.DayOfWeek != transitionTime.DayOfWeek)
                    {
                        date = date.AddDays(1);
                    }
                }
            }

            return date;
        }

        /// <summary>
        /// Create a DST device specific command relevant to the server
        /// </summary>
        public static TerminalTimeZoneInformation CreateTerminalTimeZoneInformation()
        {
            TimeZoneInfo tzi;
            var rule = CurrentAdjustmentRule(out tzi);

            if (rule == null)
            {
                return new TerminalTimeZoneInformation
                {
                    Bias = (int)(tzi.BaseUtcOffset.TotalMinutes * -1),
                    DaylightBias = 0,
                    DaylightDate = null,
                    DaylightName = tzi.DaylightName,
                    DisplayName = tzi.DisplayName,
                    StandardBias = 0,
                    StandardDate = null,
                    StandardName = tzi.StandardName
                };
            }

            int year = DateTimeOffset.Now.Year;
            var startDate = TransitionDateGet(year, rule.DaylightTransitionStart);
            var endDate = TransitionDateGet(year, rule.DaylightTransitionEnd);

            var start = new DateTimeOffset(
                startDate.Year,
                startDate.Month,
                startDate.Day,
                rule.DaylightTransitionStart.TimeOfDay.Hour,
                rule.DaylightTransitionStart.TimeOfDay.Minute,
                rule.DaylightTransitionStart.TimeOfDay.Second,
                new TimeSpan(tzi.BaseUtcOffset.Hours, tzi.BaseUtcOffset.Minutes, tzi.BaseUtcOffset.Seconds));
            var end = new DateTimeOffset(
                endDate.Year,
                endDate.Month,
                endDate.Day,
                rule.DaylightTransitionEnd.TimeOfDay.Hour,
                rule.DaylightTransitionEnd.TimeOfDay.Minute,
                rule.DaylightTransitionEnd.TimeOfDay.Second,
                new TimeSpan(tzi.BaseUtcOffset.Hours, tzi.BaseUtcOffset.Minutes, tzi.BaseUtcOffset.Seconds));

            return new TerminalTimeZoneInformation
            {
                Bias = (int)(tzi.BaseUtcOffset.TotalMinutes * -1),
                DaylightBias = (int)(rule.DaylightDelta.TotalMinutes * -1),
                DaylightDate = new TerminalSystemTime
                {
                    Day = (short)start.Day,
                    DayOfWeek = (short)start.DayOfWeek,
                    Hour = (short)start.Hour,
                    Milliseconds = (short)start.Millisecond,
                    Minute = (short)start.Minute,
                    Month = (short)start.Month,
                    Second = (short)start.Second,
                    Year = (short)start.Year
                },
                DaylightName = tzi.DaylightName,
                DisplayName = tzi.DisplayName,
                StandardBias = 0,
                StandardDate = new TerminalSystemTime
                {
                    Day = (short)end.Day,
                    DayOfWeek = (short)end.DayOfWeek,
                    Hour = (short)end.Hour,
                    Milliseconds = (short)end.Millisecond,
                    Minute = (short)end.Minute,
                    Month = (short)end.Month,
                    Second = (short)end.Second,
                    Year = (short)end.Year
                },
                StandardName = tzi.StandardName
            };
        }
    }
}
