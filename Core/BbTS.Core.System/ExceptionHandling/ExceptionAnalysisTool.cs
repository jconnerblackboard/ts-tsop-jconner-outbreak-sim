﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Exceptions.Security.Oauth;
using BbTS.Domain.Models.Exceptions.Service;

namespace BbTS.Core.System.ExceptionHandling
{
    /// <summary>
    /// Tool for analyzing and handling a variety of exception related functions.
    /// </summary>
    public class ExceptionAnalysisTool
    {
        /// <summary>
        /// Check to see if the base exception is a <see cref="WebServiceCommunicationFailedException"/> or contains one
        /// in its inner exception collection.
        /// </summary>
        /// <param name="ex">The base <see cref="Exception"/> object to analyze.</param>
        /// <returns>The base exception as a <see cref="WebServiceCommunicationFailedException"/> or null if it isn't one.</returns>
        public static WebServiceCommunicationFailedException HasAWebServiceCommunicationFailedException(Exception ex)
        {
            // First check if the base exception is a WebServiceCommunicationFailedException
            var webException = ex as WebServiceCommunicationFailedException;
            if (webException != null)
            {
                return webException;
            }

            // If the base exception is an AggregateException check to see if it contains a WebServiceCommunicationFailedException among the
            // inner exceptions.  Throw the first one found.
            var aggregrateException = ex as AggregateException;
            return aggregrateException?.InnerExceptions.OfType<WebServiceCommunicationFailedException>().FirstOrDefault();
        }

        /// <summary>
        /// Check to see if the base exception is a <see cref="OauthException"/> or contains one
        /// in its inner exception collection.
        /// </summary>
        /// <param name="ex">The base <see cref="Exception"/> object to analyze.</param>
        /// <returns>The base exception as a <see cref="OauthException"/> or null if it isn't one.</returns>
        public static OauthException HasAnOauthException(Exception ex)
        {
            // First check if the base exception is a OauthException
            var oex = ex as OauthException;
            if (oex != null)
            {
                return oex;
            }

            // If the base exception is an AggregateException check to see if it contains a WebServiceCommunicationFailedException among the
            // inner exceptions.  Throw the first one found.
            var aggregrateException = ex as AggregateException;
            return aggregrateException?.InnerExceptions.OfType<OauthException>().FirstOrDefault();
        }
    }
}
