﻿using System;
using System.Diagnostics;

namespace BbTS.Core.System.ExceptionHandling
{
    public delegate void ServiceLevelExceptionEvent(string code, Exception ex, EventLogEntryType severity);
    public class ExceptionEvents
    {
        public static event ServiceLevelExceptionEvent ServiceLevelExceptionEvent;

        /// <summary>
        /// Fire a Service Level Exception Event
        /// </summary>
        /// <param name="code">Exception code.  Most likely a GUID represention the exception id</param>
        /// <param name="ex">The exception to report</param>
        /// <param name="severity">The severity level of the exception</param>
        public static void FireServiceLevelExceptionEvent(string code, Exception ex, EventLogEntryType severity)
        {
            if (ServiceLevelExceptionEvent != null)
            {
                ServiceLevelExceptionEvent(code, ex, severity);
            }
        }
    }
}
