﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;

namespace BbTS.Core.System.ExceptionHandling
{
    public class ServiceLevelErrorHandler : IErrorHandler
    {
        public void ProvideFault(Exception error, MessageVersion version, ref Message fault)
        {
            FaultCode code = new FaultCode(Guid.NewGuid().ToString("D"), "Blackboard External Client");
            String exceptionMessage = String.Format(
                "A processing error has occured.  The error has been assigned an id of {0}.  " +
                "Please consult the party responsible for adminstration of the service for more information.",
                code.Name);
            FaultException faultException = new FaultException(exceptionMessage, code);
            ExceptionEvents.FireServiceLevelExceptionEvent(code.Name, error, EventLogEntryType.Error);
            MessageFault messageFault = faultException.CreateMessageFault();
            fault = Message.CreateMessage(version, messageFault, faultException.Action);
        }

        public bool HandleError(Exception error)
        {
            return true;
        }
    }
}
