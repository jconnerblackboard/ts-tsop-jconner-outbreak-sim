﻿using System;
using System.Diagnostics;
using System.Net;
using System.Reflection;
using System.ServiceModel.Web;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Definitions;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Exceptions;
using BbTS.Domain.Models.Exceptions.Domain;
using BbTS.Domain.Models.Exceptions.Resource;
using BbTS.Domain.Models.Exceptions.WebApi;
using BbTS.Domain.Models.System;
using BbTS.Monitoring.Logging;
using PostSharp.Aspects;

namespace BbTS.Core.System.ExceptionHandling
{
    [Serializable]
    public class WcfExceptionHandling : OnExceptionAspect
    {
        private string _methodName;

        /// <summary> 
        /// Method executed at build time. Initializes the aspect instance. After the execution 
        /// of <see cref="CompileTimeInitialize"/>, the aspect is serialized as a managed  
        /// resource inside the transformed assembly, and deserialized at runtime. 
        /// </summary> 
        /// <param name="method">Method to which the current aspect instance  
        /// has been applied.</param> 
        /// <param name="aspectInfo">Unused.</param> 
        public override void CompileTimeInitialize(MethodBase method, AspectInfo aspectInfo)
        {
            if (method.DeclaringType != null) _methodName = method.DeclaringType.FullName + "." + method.Name;
        }

        /// <summary>
        /// Handle exceptions.
        /// </summary>
        /// <param name="args"></param>
        public override void OnException(MethodExecutionArgs args)
        {
            var id = string.Empty;
            var referenceId = Guid.NewGuid().ToString("D");
            var code = HttpStatusCode.InternalServerError;
            short eventCategory = (short)LoggingDefinitions.Category.WcfExceptionHandling;
            int eventId = (int)LoggingDefinitions.EventId.General;

            if (args.Exception is NotImplementedException)
            {
                code = HttpStatusCode.NotImplemented;
            }

            if (args.Exception is DomainAwareResultException)
            {
                code = HttpStatusCode.BadRequest;
            }

            var webApiException = args.Exception as WebApiException;
            if (webApiException != null)
            {
                code = webApiException.HttpStatusCode;
            }

            var resourceLayerException = args.Exception as ResourceLayerException;
            if (resourceLayerException != null)
            {
                code = HttpStatusCode.BadRequest;
                eventCategory = resourceLayerException.EventCategory;
                eventId = resourceLayerException.EventId;
                id = resourceLayerException.RequestId;
                referenceId = string.IsNullOrWhiteSpace(resourceLayerException.ReferenceId) ? referenceId : resourceLayerException.ReferenceId;
            }

            // log and throw a default exception with formatted exception text.
            LoggingManager.Instance.LogException(
                args.Exception, "", "", EventLogEntryType.Error,
                eventCategory,
                eventId);

            //throw new NotImplementedException();
            var token = new ExceptionResponseToken
            {
                ReferenceId = referenceId,
                Id = id,
                Message = Formatting.FormatException(args.Exception),
                ResultDomainId = (int)code,
                ResultDomain = (int)DomainValue.WcfExceptionHandler
            };

            throw new WebFaultException<ExceptionResponseToken>(token, code);
        }

        /// <summary>
        /// Create a web fault exception to throw
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        private WebFaultException<T> _createException<T>(T value, HttpStatusCode code)
        {
            return new WebFaultException<T>(value, code);
        }
    }
}
