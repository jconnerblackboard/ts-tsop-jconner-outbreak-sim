﻿using System;
using System.Reflection;
using System.Web.Http;
using BbTS.Domain.Models.Exceptions.Transaction;
using BbTS.Domain.Models.Exceptions.WebApi;

namespace BbTS.Core.System.ExceptionHandling
{
    /// <summary>
    /// Tool class for formatting service exceptions originating in the BbTS Web Api product.
    /// </summary>
    public class ServiceExceptionTool
    {
        /// <summary>
        /// Create an exception
        /// </summary>
        /// <typeparam name="T">Type created.</typeparam>
        /// <param name="exception"><see cref="WebApiException"/> object to format.</param>
        /// <returns></returns>
        public static HttpError Create<T>(WebApiException exception) where T : Exception
        {
            var properties = exception.GetType().GetProperties(BindingFlags.Instance
                                                             | BindingFlags.Public
                                                             | BindingFlags.DeclaredOnly);
            var error = new HttpError();
            foreach (var propertyInfo in properties)
            {
                error.Add(propertyInfo.Name, propertyInfo.GetValue(exception, null));
            }
            return error;
        }

        /// <summary>
        /// Create an exception
        /// </summary>
        /// <typeparam name="T">Type created.</typeparam>
        /// <param name="exception"><see cref="WebApiException"/> object to format.</param>
        /// <returns></returns>
        public static HttpError Create<T>(TransactionProcessingException exception) where T : Exception
        {
            var properties = exception.GetType().GetProperties(BindingFlags.Instance
                                                             | BindingFlags.Public
                                                             | BindingFlags.DeclaredOnly);
            var error = new HttpError();
            foreach (var propertyInfo in properties)
            {
                error.Add(propertyInfo.Name, propertyInfo.GetValue(exception, null));
            }
            return error;
        }
    }
}
