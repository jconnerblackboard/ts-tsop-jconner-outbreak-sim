﻿using BbTS.Core.Configuration;
using BbTS.Core.System.Services;
using BbTS.Domain.Models.Exceptions.System;
using System;
using System.ServiceModel;

namespace BbTS.Core.System.Url
{
    /// <summary>
    /// Utility class to handle database security functions.
    /// </summary>
    public static class UrlTools
    {
        /// <summary>
        /// Preamble
        /// </summary>
        public static WebHttpSecurityMode SecurityMode => (WebHttpSecurityMode)Enum.Parse(typeof(WebHttpSecurityMode), ApplicationConfiguration.GetKeyValueAsString("ServiceSecurityMode", WebHttpSecurityMode.Transport.ToString()));

        /// <summary>
        /// Gets management portal url
        /// </summary>
        /// <returns></returns>
        public static string GetManagementPortalUrlString()
        {
            var serviceHostName = RegistryConfigurationTool.Instance.ValueGet("ManagementPortalServerHostAddress");
            if (string.IsNullOrWhiteSpace(serviceHostName))
            {
                throw new RegistryConfigurationException($"Value 'ManagementPortalServerHostAddress' was not found under {RegistryConfigurationTool.ConfigKey}");
            }

            return $"{ServiceUtility.GetHttpProtocolPreamble(SecurityMode)}//{serviceHostName}/Transact";
        }

        /// <summary>
        /// Gets reporting system url
        /// </summary>
        /// <returns></returns>
        public static string GetReportingSystemUrlString()
        {
            var serviceHostName = RegistryConfigurationTool.Instance.ValueGet("ReportingSystemServerHostAddress");
            if (string.IsNullOrWhiteSpace(serviceHostName))
            {
                throw new RegistryConfigurationException($"Value 'ReportingSystemServerHostAddress' was not found under {RegistryConfigurationTool.ConfigKey}");
            }

            return $"{ServiceUtility.GetHttpProtocolPreamble(SecurityMode)}//{serviceHostName}/ReportingSystem";
        }
    }
}
