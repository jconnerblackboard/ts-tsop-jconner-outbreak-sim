﻿using System.Diagnostics;
using BbTS.Core.Configuration;
using BbTS.Core.Serialization;
using BbTS.Core.System.Services;
using BbTS.Domain.Models.Definitions.PaymentExpress;
using BbTS.Domain.Models.Exceptions.Emv;
using BbTS.Domain.Models.PaymentExpress;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Management;
using System.ServiceProcess;


namespace BbTS.Core.System.Terminal
{
    public delegate void EmvServiceMessageEvent(string message);

    public class PaymentExpressDomainException : ApplicationException
    {
        public PaymentExpressDomainException(ClientPaymentExpressDomainId operationDomainId, Exception e) : base(e.Message,e)
        {
            OperationDomainId = operationDomainId;
        }

        public ClientPaymentExpressDomainId OperationDomainId { get; private set; }
    }
    public static class PaymentExpressUtility
    {

        private static readonly TimeSpan ServiceResponseTimeout = new TimeSpan(0, 1, 0);

        private static int _loggingLevel = -1;

        private static bool LogDebugEnabled
        {
            get
            {
                try
                {
                    _loggingLevel = _loggingLevel < 0 ?
                        ApplicationConfiguration.GetKeyValueAsInt("ExceptionLevelLogging", 5) :
                        _loggingLevel;
                }
                catch (Exception)
                {
                    _loggingLevel = 5;
                }
                return _loggingLevel == 0;
            }
        }

        public static event EmvServiceMessageEvent EmvServiceMessageEvent;
        private static void FireEmvServiceMessageEvent(string message)
        {
            if (EmvServiceMessageEvent != null) EmvServiceMessageEvent(message);
        }

        /// <summary>
        /// Backup the supplied config file to a file in the following format:
        /// 
        /// sourcefilepath\sourcefilebasenameYYYYMMDDHHmmss.txt
        /// 
        /// where:
        /// YYYY = current year
        /// MM = current month
        /// DD = current day
        /// HH = current hour
        /// mm = current minute
        /// ss = current minute
        /// 
        /// </summary>
        /// <param name="configFilePath">path to the SCR Controller config file</param>
        /// <param name="dateTime">timestamp</param>
        public static string GenerateBackupFilePath(string configFilePath, DateTime dateTime)
        {
            string timestamp = String.Format(
                "{0}{1}{2}{3}{4}{5}",
                dateTime.Year,
                dateTime.Month.ToString("D2"),
                dateTime.Day.ToString("D2"),
                dateTime.Hour.ToString("D2"),
                dateTime.Minute.ToString("D2"),
                dateTime.Second.ToString("D2"));

            string directory = Path.GetDirectoryName(configFilePath);
            string baseName = Path.GetFileNameWithoutExtension(configFilePath);
            string extension = Path.GetExtension(configFilePath);

            return String.Format(
                "{0}\\{1}{2}{3}",
                directory,
                baseName,
                timestamp,
                extension);
        }

        /// <summary>
        /// Backup the supplied config file to a file in the following format:
        /// 
        /// 
        /// if the backupFilePath exists then a guid will be attached to the end of the last s like the following:
        /// 
        /// backupFilePathGUID.txt
        /// 
        /// </summary>
        /// <param name="configFilePath">path to the SCR Controller config file</param>
        /// <param name="backupFilePath">path to the backup file to be created</param>
        /// <returns>backup file name</returns>
        public static string BackupFile(string configFilePath, string backupFilePath)
        {
            if (!File.Exists(configFilePath))
            {
                string message = String.Format("SCR Configuration File not found at {0}", configFilePath);
                throw new FileNotFoundException(message);
            }

            string directory = Path.GetDirectoryName(backupFilePath);
            string baseName = Path.GetFileNameWithoutExtension(backupFilePath);
            string extension = Path.GetExtension(backupFilePath);

            if (File.Exists(backupFilePath))
            {
                backupFilePath = String.Format(
                "{0}\\{1}{2}{3}",
                directory,
                baseName,
                Guid.NewGuid().ToString("N"),
                extension);
            }

            File.Copy(configFilePath, backupFilePath);

            return backupFilePath;
        }

        /// <summary>
        /// Get a list of potential Ingenico Device Names (the name the driver installs the device under)
        /// from the application configuration file.
        /// </summary>
        /// <returns></returns>
        public static List<string> GetIngenicoKeyWordList()
        {
            List<string> names = new List<string>();

            // Get the raw list of device names from the application configuration file (App.config)
            string configString = ApplicationConfiguration.GetKeyValueAsString("IngenicoDeviceKeyWords");
            if (String.IsNullOrEmpty(configString)) return names;

            // Split up the list
            string[] configParts = configString.Split(new [] { ',' });
            if (configParts.Length == 0) return names;

            names = new List<string>(configParts);
            return names;
        }

        /// <summary>
        /// Get the COM port associated with the EMV device matching the partialDeviceName.  
        /// Throws exceptions.
        /// </summary>
        /// <param name="deviceNames">Partial (or full) device name to look for.  Any part of the device name will return results</param>
        /// <returns></returns>
        public static string EmvDeviceComPortGet(List<string> deviceNames)
        {
            ManagementScope scope = new ManagementScope();

            SelectQuery query = new SelectQuery("SELECT * FROM Win32_PnPEntity");
            using (ManagementObjectSearcher searcher = new ManagementObjectSearcher(scope, query))
            {

                foreach (var item in searcher.Get())
                {
                    string name = item["Name"] != null ? item["Name"].ToString() : "<null>";
                    bool isValidDevice =
                        name != "<null>" &&
                        deviceNames.Select(n => n.ToLower()).All(name.ToLower().Contains) &&
                        name.ToLower().Contains("(com");

                    if (isValidDevice)
                    {
                        int indexOfComText = name.ToLower().IndexOf("(com", StringComparison.Ordinal);
                        if (indexOfComText >= 0)
                        {
                            // Strip off all the crap at the beginning
                            char[] parenthesis = { '(', ')', ' ' };
                            string comport = name.Substring(indexOfComText);
                            comport = comport.TrimStart(parenthesis);
                            comport = comport.TrimStart("com".ToCharArray());
                            comport = comport.TrimStart("COM".ToCharArray());

                            string portnumber = string.Empty;
                            foreach (var character in comport)
                            {
                                string charString = string.Empty;
                                charString += character;
                                int number;
                                if (int.TryParse(charString, out number))
                                {
                                    portnumber += number.ToString(CultureInfo.InvariantCulture);
                                }
                                else
                                {
                                    // Exit as soon as numbers don't parse anymore.
                                    // This probably means that we have reached the end of the port number
                                    break;
                                }
                            }
                            return string.Format("COM{0}", portnumber);
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Get the Com port on the SCR controller
        /// </summary>
        /// <param name="configFilePath">Path to the SCR Controller config file</param>
        /// <returns></returns>
        public static string ScrControllerConfigComPortGet(string configFilePath)
        {
            ScrControllerConfig config = ScrControllerConfigGet(configFilePath);
            if (config != null) return config.Server.ComPort;
            return string.Empty;
        }

        /// <summary>
        /// Get config data for the SCR controller
        /// </summary>
        /// <param name="configFilePath">Path to the SCR Controller config file</param>
        /// <returns></returns>
        public static ScrControllerConfig ScrControllerConfigGet(string configFilePath)
        {
            if (!File.Exists(configFilePath))
            {
                string message = String.Format("SCR Configuration File not found at {0}", configFilePath);
                throw new FileNotFoundException(message);
            }

            string xml = File.ReadAllText(configFilePath);
            ScrControllerConfig config = Xml.Deserialize<ScrControllerConfig>(xml);
            return config;
        }

        /// <summary>
        /// Update the SCR Controller configuration file with new settings
        /// WARNING: Does not back up the SCR Controller config file before overwriting.
        /// </summary>
        /// <param name="config">Config data to use </param>
        /// <param name="configFilePath">Path to the SCR Controller config file</param>
        public static void ScrControllerConfigSet(ScrControllerConfig config, string configFilePath)
        {
            if (!File.Exists(configFilePath))
            {
                string message = String.Format("SCR Configuration File not found at {0}", configFilePath);
                throw new FileNotFoundException(message);
            }
            File.WriteAllText(configFilePath, Xml.Serialize(config, true));
        }

        /// <summary>
        /// Update the SCR Controller configuration file with the new device com port.
        /// WARNING: Does not back up the SCR Controller config file before overwriting.
        /// </summary>
        /// <param name="comPort">Com Port of the Ingenico Device</param>
        /// <param name="configFilePath">Path to the SCR Controller config file</param>
        public static void ScrControllerConfigComPortSet(string comPort, string configFilePath)
        {
            ScrControllerConfig config = ScrControllerConfigGet(configFilePath);
            config.Server.ComPort = comPort;
            ScrControllerConfigSet(config,configFilePath);
        }

        /// <summary>
        /// Get the PxSCR controller confiuguration file directory
        /// </summary>
        /// <returns>Path to the directory or an empty string if it isn't found in the registry</returns>
        public static string PxScrConfigPathGet()
        {
            return Registry.GetValue(
                DomainPaymentExpressDefinitions.EmvConfigFileRegistryKey,
                DomainPaymentExpressDefinitions.EmvConfigFileRegistryValueName,
                string.Empty) as string;
        }

        /// <summary>
        /// Check to see if the TwDrvService registry entry exists.
        /// </summary>
        /// <returns></returns>
        public static bool TwDrvServiceRegistryEntryExists()
        {
            return Registry.GetValue(
                DomainPaymentExpressDefinitions.TwDrvServiceRegistryKey,
                DomainPaymentExpressDefinitions.TwDrvServiceRegistryValue,
                null) != null;
        }

        /// <summary>
        /// Get the COM ports that will be ignored in the TwDrvService COM port scans.
        /// </summary>
        /// <returns>Format Example: 
        ///             Exists: COM1,COM2,COM4,COM5 
        ///             Doesn't Exist: null</returns>
        public static List<string> TwDrvServiceIgnoreComPortsGet()
        {
            // Check to see if the key exists
            if (!TwDrvServiceRegistryEntryExists()) return null;

            string ports = Registry.GetValue(
                DomainPaymentExpressDefinitions.TwDrvServiceRegistryKey,
                DomainPaymentExpressDefinitions.TwDrvServiceRegistryValue,
                string.Empty) as string;

            return 
                String.IsNullOrEmpty(ports) ?
                new List<string>() :
                new List<string>(ports.Split(','));
        }

        /// <summary>
        /// Set the ignore list for the TwDrvService Ignore com port registry entry
        /// </summary>
        /// <param name="ignoredPorts"></param>
        public static void TwDrvServiceIgnoreComPortsSet(List<string> ignoredPorts)
        {
            // Check to see if the key exists
            if (!TwDrvServiceRegistryEntryExists()) return;

            string joined = string.Join(",", ignoredPorts.ToArray());

            Registry.SetValue(
                DomainPaymentExpressDefinitions.TwDrvServiceRegistryKey,
                DomainPaymentExpressDefinitions.TwDrvServiceRegistryValue,
                joined);
        }

        /// <summary>
        /// Update the TwDrvService service to ignore the ingenico com port.
        /// </summary>
        /// <param name="detectedEmvComPort"></param>
        public static void TwDrvServiceUpdate(string detectedEmvComPort)
        {
            if (!TwDrvServiceRegistryEntryExists()) return;

            List<string> ignoredComPorts = TwDrvServiceIgnoreComPortsGet();

            // if the ignore port is already set or the registry key doesn't exist then there is nothing to do.
            if (ignoredComPorts == null ||
                (ignoredComPorts.Count > 0 && ignoredComPorts.Contains(detectedEmvComPort))) return;

            using (ServiceController twDrvService = new ServiceController(DomainPaymentExpressDefinitions.ScrControllerServiceName))
            {
                // Stop the TwDrvServices
                if (LogDebugEnabled) FireEmvServiceMessageEvent("Stopping TwDrvService");
                ServiceManager.StopService(twDrvService, ServiceResponseTimeout);
                if (LogDebugEnabled) FireEmvServiceMessageEvent("TwDrvService Stoppped");

                // Update ignore list
                if (LogDebugEnabled) FireEmvServiceMessageEvent("Updating TwDrvService Com Port ignore list to include " + detectedEmvComPort.ToUpper());
                ignoredComPorts.Add(detectedEmvComPort.ToUpper());
                TwDrvServiceIgnoreComPortsSet(ignoredComPorts);

                // Start service
                if (LogDebugEnabled) FireEmvServiceMessageEvent("Starting TwDrvService");
                ServiceManager.StartService(twDrvService, ServiceResponseTimeout);
                if (LogDebugEnabled) FireEmvServiceMessageEvent("TwDrvService Started");
            }
        }

        /// <summary>
        /// Update the PxSCR configuration file with the com port of the installed device.
        /// </summary>
        public static string PxScrControllerComPortUpdate()
        {
            string detectedComPort = EmvDeviceComPortGet(GetIngenicoKeyWordList());
            if (string.IsNullOrEmpty(detectedComPort))
            {
                throw new EmvComPortNotFoundException(String.Format("Unable to detect the com port of the Ingenico pin pad device."));
            }
            TwDrvServiceUpdate(detectedComPort);
            PxScrControllerConfigUpdate(null, null, null, null, detectedComPort);
            return detectedComPort;
        }

        /// <summary>
        /// Update the PxSCR controller configuration file. Returns true if the service required a start
        /// (either from not previously running or from a change in the file requiring a restart).
        /// </summary>
        /// <param name="deviceId">DPS device id of the terminal (using the pinpad)</param>
        /// <param name="vendorId">Identified for blackboard as a DPS provider (Blackboard Inc.)</param>
        /// <param name="currency">Currency code for the pinpad</param>
        /// <param name="hostInterfaces">Interface to payment express service</param>
        /// <param name="comPort">Com port of the pinpad device</param>
        public static bool PxScrControllerConfigUpdate(string deviceId, string vendorId, string currency, List<ScrControllerHostInterfaceTag> hostInterfaces, string comPort)
        {
            bool returnValue = false;
            ClientPaymentExpressDomainId currentState = ClientPaymentExpressDomainId.Unknown;
            try
            {
                currentState = ClientPaymentExpressDomainId.RegistryValueNotFound;

                string configFilePath = PxScrConfigPathGet();

                currentState = ClientPaymentExpressDomainId.ConfigDirectoryDoesNotExist;
                string backupDirectory = Path.GetDirectoryName(configFilePath);

                //Make sure directories exist and that we have at least one host interface
                if (!File.Exists(configFilePath)) throw new DirectoryNotFoundException(String.Format("PxSCRController Config File not found: {0}", configFilePath));

                currentState = ClientPaymentExpressDomainId.BackupDirectoryDoesNotExist;

                if(String.IsNullOrEmpty(backupDirectory))
                    throw new DirectoryNotFoundException(String.Format("Unable to create PxSCRController backup directory because the path is null or empty."));

                if (!Directory.Exists(backupDirectory))
                {
                    currentState = ClientPaymentExpressDomainId.CouldNotCreateBackupDirectory;
                    DirectoryInfo directoryInfo = Directory.CreateDirectory(backupDirectory);
                    if (!directoryInfo.Exists) throw new DirectoryNotFoundException(String.Format("Unable to create PxSCRController backup directory: {0}", backupDirectory));
                }

                currentState = ClientPaymentExpressDomainId.MissingHostInterface;
                if (hostInterfaces != null && hostInterfaces.Count < 1) throw new ArgumentException("HostInterfaces cannot be empty");

                //Get the original configuration and create a deep copy as the basis for the new configuration
                ScrControllerConfig originalConfiguration = ScrControllerConfigGet(configFilePath);
                ScrControllerConfig newConfiguration = originalConfiguration.DeepCopy();

                //Save the new settings to new configuration
                newConfiguration.Server.DeviceId = deviceId ?? newConfiguration.Server.DeviceId;
                newConfiguration.Server.VendorId = vendorId ?? newConfiguration.Server.VendorId;
                newConfiguration.Server.Currency = currency ?? newConfiguration.Server.Currency;
                newConfiguration.HostInterfaces = hostInterfaces ?? newConfiguration.HostInterfaces;
                newConfiguration.Server.ComPort = comPort ?? newConfiguration.Server.ComPort;

                currentState = ClientPaymentExpressDomainId.ServiceNotFound;

                using (ServiceController pxScrService = new ServiceController(DomainPaymentExpressDefinitions.ScrControllerServiceName))
                {
                    //Check if new configuration differs from original configuration.  If so, backup the old and save the new.
                    if (!newConfiguration.Matches(originalConfiguration))
                    {
                        currentState = ClientPaymentExpressDomainId.CouldNotSaveConfiguration;
                        string backupFileName = GenerateBackupFilePath(configFilePath, DateTime.Now);
                        BackupFile(configFilePath, backupFileName);

                        currentState = ClientPaymentExpressDomainId.ServiceStopFailed;
                        ServiceManager.StopService(pxScrService, ServiceResponseTimeout);

                        currentState = ClientPaymentExpressDomainId.CouldNotSaveConfiguration;
                        ScrControllerConfigSet(newConfiguration, configFilePath);
                    }
                    currentState = ClientPaymentExpressDomainId.ServiceStartFailed;

                    if (pxScrService.Status != ServiceControllerStatus.Running)
                    {
                        returnValue = true;
                    }
                    ServiceManager.StartService(pxScrService, ServiceResponseTimeout);
                    currentState = ClientPaymentExpressDomainId.Success;
                }
            }
            catch (Exception e)
            {
                throw new PaymentExpressDomainException(currentState, e);
            }
            return returnValue;
        }
    }
}
