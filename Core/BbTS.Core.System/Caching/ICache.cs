﻿using System;

namespace BbTS.Core.System.Caching
{
    /// <summary>
    ///     Defines the methods and properties that all cache implementations must implement
    /// </summary>
    public interface ICache
    {
        /// <summary>
        ///     Gets the name of the cache.
        /// </summary>
        /// <value>
        ///     The name of the cache.
        /// </value>
        string CacheName { get; }

        /// <summary>
        ///     Puts the specified data into the cache with the specified key.
        /// </summary>
        /// <typeparam name="T">
        ///     Type of data being added to the cache
        /// </typeparam>
        /// <param name="key">
        ///     The key.
        /// </param>
        /// <param name="data">
        ///     The data.
        /// </param>
        void Put<T>(string key, T data);

        /// <summary>
        ///     Puts the specified data into the cache with the specified key and specified lifetime.
        /// </summary>
        /// <typeparam name="T">
        ///     Type of data being added to the cache
        /// </typeparam>
        /// <param name="key">
        ///     The key.
        /// </param>
        /// <param name="data">
        ///     The data.
        /// </param>
        /// <param name="lifetime">
        ///     The lifetime.
        /// </param>
        void Put<T>(string key, T data, TimeSpan lifetime);

        /// <summary>
        ///     Gets the instance of data specified by key.
        /// </summary>
        /// <typeparam name="T">
        ///     Type of data being retrieved from the cache
        /// </typeparam>
        /// <param name="key">
        ///     The key.
        /// </param>
        /// <returns>
        ///     Instance of data matching the specified key
        /// </returns>
        T Get<T>(string key);

        /// <summary>
        ///     Gets the specified key.
        /// </summary>
        /// <param name="key">
        ///     The key.
        /// </param>
        /// <returns>
        ///     Instance of data matching the specified key
        /// </returns>
        object Get(string key);
    }
}
