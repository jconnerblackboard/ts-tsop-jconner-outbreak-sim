﻿namespace BbTS.Core.System.Caching
{
    /// <summary>
    /// MemoryCache Configuration
    /// </summary>
    public class MemoryCacheConfiguration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MemoryCacheConfiguration" /> class.
        /// </summary>
        /// <param name="cacheName">Name of the cache.</param>
        /// <param name="defaultCacheExpirationTimeInSeconds">The default cache expiration time in seconds.</param>
        /// <param name="cacheMemoryLimitMegabytes">The cache memory limit megabytes.</param>
        /// <param name="physicalMemoryLimitPercentage">The physical memory limit percentage.</param>
        /// <param name="pollingIntervalInSeconds">The polling interval in seconds.</param>
        public MemoryCacheConfiguration(string cacheName, double defaultCacheExpirationTimeInSeconds, int cacheMemoryLimitMegabytes, int physicalMemoryLimitPercentage, int pollingIntervalInSeconds)
        {
            CacheName = cacheName;
            DefaultCacheExpirationTimeInSeconds = defaultCacheExpirationTimeInSeconds;
            CacheMemoryLimitMegabytes = cacheMemoryLimitMegabytes;
            PhysicalMemoryLimitPercentage = physicalMemoryLimitPercentage;
            PollingIntervalInSeconds = pollingIntervalInSeconds;
        }

        /// <summary>
        /// Gets the name of the cache.
        /// </summary>
        /// <value>
        /// The name of the cache.
        /// </value>
        public string CacheName { get; private set; }

        /// <summary>
        /// Gets the default cache expiration time in seconds.
        /// </summary>
        /// <value>
        /// The default cache expiration time in seconds.
        /// </value>
        public double DefaultCacheExpirationTimeInSeconds { get; private set; }

        /// <summary>
        /// Gets the cache memory limit megabytes.
        /// </summary>
        /// <value>
        /// The cache memory limit megabytes.
        /// </value>
        public int CacheMemoryLimitMegabytes { get; private set; }

        /// <summary>
        /// Gets the physical memory limit percentage.
        /// </summary>
        /// <value>
        /// The physical memory limit percentage.
        /// </value>
        public int PhysicalMemoryLimitPercentage { get; private set; }

        /// <summary>
        /// Gets the polling interval in seconds.
        /// </summary>
        /// <value>
        /// The polling interval.
        /// </value>
        public int PollingIntervalInSeconds { get; private set; }
    }
}
