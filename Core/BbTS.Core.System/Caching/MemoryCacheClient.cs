﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Definitions.System.Caching;

namespace BbTS.Core.System.Caching
{
    /// <summary>
    /// MemoryCacheClient used by the applications that is hosted on a single server to cache its objects 
    /// </summary>
    public sealed class MemoryCacheClient : ICache, IDisposable
    {
        /// <summary>
        /// The cache store
        /// </summary>
        private readonly MemoryCache store;

        /// <summary>
        /// The configuration
        /// </summary>
        private readonly MemoryCacheConfiguration configuration;

        /// <summary>
        /// The disposed
        /// </summary>
        private bool disposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="MemoryCacheClient" /> class.
        /// </summary>
        /// <param name="memoryCacheConfiguration">The memory cache configuration.</param>
        public MemoryCacheClient(MemoryCacheConfiguration memoryCacheConfiguration)
        {
            // set the configuration
            configuration = memoryCacheConfiguration;

            // Set the cache settings
            var cacheSettings = new NameValueCollection(3)
                                    {
                                        {
                                            MemoryCacheSettingConstants.CacheMemoryLimitMegabytes,
                                            Convert.ToString(configuration.CacheMemoryLimitMegabytes, CultureInfo.InvariantCulture)
                                        },
                                        {
                                            MemoryCacheSettingConstants.PhysicalMemoryLimitPercentage,
                                            Convert.ToString(
                                                configuration.PhysicalMemoryLimitPercentage,
                                                CultureInfo.InvariantCulture)
                                        },
                                        {
                                            MemoryCacheSettingConstants.PollingInterval,
                                            Convert.ToString(
                                                TimeSpan.FromSeconds(configuration.PollingIntervalInSeconds),
                                                CultureInfo.InvariantCulture)
                                        }
                                    };

            // create MemoryCache
            store = new MemoryCache(configuration.CacheName, cacheSettings);
        }

        /// <summary>
        /// Finalizes an instance of the BbFX.Core.Caching.MemoryCacheClient class.
        /// </summary>
        ~MemoryCacheClient()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets the name of the cache.
        /// </summary>
        /// <value>
        /// The name of the cache.
        /// </value>
        /// <seealso cref="P:BbFX.Core.Caching.ICache.CacheName"/>
        public string CacheName
        {
            get
            {
                return configuration.CacheName;
            }
        }

        /// <summary>
        /// Puts the specified data into the cache with the specified key.
        /// </summary>
        /// <typeparam name="T">Type of data being added to the cache</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="data">The data.</param>
        public void Put<T>(string key, T data)
        {
            var policy = new CacheItemPolicy { AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(configuration.DefaultCacheExpirationTimeInSeconds) };
            store.Set(key, data, policy);
        }

        /// <summary>
        /// Puts the specified data into the cache with the specified key and specified lifetime.
        /// </summary>
        /// <typeparam name="T">Type of data being added to the cache</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="data">The data.</param>
        /// <param name="lifetime">The lifetime.</param>
        public void Put<T>(string key, T data, TimeSpan lifetime)
        {
            var policy = new CacheItemPolicy { AbsoluteExpiration = DateTimeOffset.Now.Add(lifetime) };
            store.Set(key, data, policy);
        }

        /// <summary>
        /// Gets the instance of data specified by key.
        /// </summary>
        /// <typeparam name="T">Type of data being retrieved from the cache</typeparam>
        /// <param name="key">The key.</param>
        /// <returns>
        /// Instance of data matching the specified key
        /// </returns>
        public T Get<T>(string key)
        {
            var t = default(T);

            if (store.Contains(key))
            {
                t = (T)store[key];
            }

            return t;
        }

        /// <summary>
        /// Gets the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>
        /// Instance of data matching the specified key
        /// </returns>
        public object Get(string key)
        {
            object t = null;

            if (store.Contains(key))
            {
                t = store[key];
            }

            return t;
        }

        #region IDisposable implementation

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called. 
            if (!disposed)
            {
                // If disposing equals true, dispose all managed 
                // and unmanaged resources. 
                if (disposing)
                {
                    // Dispose managed resources.
                    if (store != null)
                    {
                        store.Dispose();
                    }
                }

                // Note disposing has been done.
                disposed = true;
            }
        }

        #endregion
    }
}
