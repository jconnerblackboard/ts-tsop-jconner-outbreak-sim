﻿using System;
using System.Collections;

namespace BbTS.Core.System.Caching
{
    /// <summary>
    ///     Simple cache implementation
    /// </summary>
    /// <remarks>
    ///     Does not support lifetime
    /// </remarks>
    public class SimpleCache : ICache
    {
        /// <summary>
        /// The default cache name.
        /// </summary>
        private const string DefaultCacheName = "default";

        /// <summary>
        ///     The store
        /// </summary>
        private readonly Hashtable store;

        /// <summary>
        ///     Initializes a new instance of the <see cref="SimpleCache" /> class.
        /// </summary>
        public SimpleCache()
        {
            store = new Hashtable();
        }

        /// <summary>
        ///     Gets the name of the cache.
        /// </summary>
        /// <value>
        ///     The name of the cache.
        /// </value>
        /// <seealso cref="P:BbFX.Core.Caching.ICache.CacheName"/>
        public string CacheName
        {
            get
            {
                return DefaultCacheName;
            }
        }

        /// <summary>
        ///     Puts the specified data into the cache with the specified key.
        /// </summary>
        /// <typeparam name="T">
        ///     Type of data being added to the cache
        /// </typeparam>
        /// <param name="key">
        ///     The key.
        /// </param>
        /// <param name="data">
        ///     The data.
        /// </param>
        public void Put<T>(string key, T data)
        {
            store.Add(key, data);
        }

        /// <summary>
        ///     Puts the specified data into the cache with the specified key and specified lifetime.
        /// </summary>
        /// <remarks>
        ///     Lifetime is ignored in this implementation
        /// </remarks>
        /// <typeparam name="T">
        ///     Type of data being added to the cache
        /// </typeparam>
        /// <param name="key">
        ///     The key.
        /// </param>
        /// <param name="data">
        ///     The data.
        /// </param>
        /// <param name="lifetime">
        ///     The lifetime.
        /// </param>
        public void Put<T>(string key, T data, TimeSpan lifetime)
        {
            store.Add(key, data);
        }

        /// <summary>
        ///     Gets the instance of data specified by key.
        /// </summary>
        /// <typeparam name="T">
        ///     Type of data being retrieved from the cache
        /// </typeparam>
        /// <param name="key">
        ///     The key.
        /// </param>
        /// <returns>
        ///     Instance of data matching the specified key
        /// </returns>
        public T Get<T>(string key)
        {
            var t = default(T);

            if (store.ContainsKey(key))
            {
                t = (T)store[key];
            }

            return t;
        }

        /// <summary>
        ///     Gets the specified key.
        /// </summary>
        /// <param name="key">
        ///     The key.
        /// </param>
        /// <returns>
        ///     Instance of data matching the specified key
        /// </returns>
        public object Get(string key)
        {
            object t = null;

            if (store.ContainsKey(key))
            {
                t = store[key];
            }

            return t;
        }
    }
}
