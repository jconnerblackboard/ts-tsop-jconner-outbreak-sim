﻿using System;
using System.Diagnostics;
using System.Text;

namespace BbTS.Core.System.SystemProcess
{
    public class ProcessUtility
    {
        private static ProcessUtility _instance = new ProcessUtility();

        public static ProcessUtility Instance
        {
            get { return _instance; }
            internal set { _instance = value; }
        }

        /// <summary>
        ///   Execute passed command and arguments
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="args"></param>
        /// <param name="createNoWindow"></param>
        /// <param name="useShellExecute"></param>
        /// <returns>StandardOutput or StandardError depending on retrn from command</returns>
        public string ExecuteProcessCommand(string fileName, string args, bool createNoWindow = true, bool useShellExecute = false)
        {
            Process process = new Process();
            string standardOutput;
            try
            {
                process.StartInfo.FileName = fileName;
                process.StartInfo.Arguments = args;
                process.StartInfo.CreateNoWindow = createNoWindow;
                process.StartInfo.UseShellExecute = useShellExecute;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.Start();
                standardOutput = process.StandardOutput.ReadToEnd();
                string standardError = process.StandardError.ReadToEnd();
                process.WaitForExit();

                if (process.ExitCode != 0)
                {

                    StringBuilder message = new StringBuilder();

                    if (standardError.Length > 0)
                    {
                        message.Append(string.Format("StandardError:\n\t{0}\n", standardError));
                    }

                    if (standardOutput.Length > 0)
                    {
                        message.Append(string.Format("StandardOutput:\n\t{0}\n", standardOutput));
                    }

                    message.Append(string.Format("Command {0} {1}", fileName, args));

                    throw new Exception(message.ToString());
                }
            }

            catch (Exception ex)
            {
                string message = "The call to " + fileName + " failed.\n" + ex.Message;
                throw new Exception(message);
            }

            finally
            {
                process.Close();
            }
            
            return standardOutput;
        }
    }
}
