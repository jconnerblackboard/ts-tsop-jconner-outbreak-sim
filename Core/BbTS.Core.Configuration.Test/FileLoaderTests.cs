﻿using System;
using System.ComponentModel;
using System.Net;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BbTS.Core.Configuration.Test
{
    [TestClass]
    public class FileLoaderTests
    {
        #region Debug Output

        private static void Write(string text)
        {
            System.Diagnostics.Debug.Write(text);
        }

        private static void WriteLine(string text = "")
        {
            System.Diagnostics.Debug.WriteLine(text);
        }

        #endregion

        private volatile bool _isDownloadComplete = false;
        private volatile int _lastProgress = -1;

        private void HandleDownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            Write(".");
            if (e.ProgressPercentage == _lastProgress) return;

            WriteLine(e.ProgressPercentage.ToString());
            _lastProgress = e.ProgressPercentage;
        }

        private void HandleDownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            Write("!");
            _isDownloadComplete = true;
        }

        [TestMethod]
        public void FileLoaderTest()
        {
            //file:///G:/Users/dclark.BBBB/Pictures/Work/BbNewLogo.psd
            var fileLoader = new FileLoader();
            //fileLoader.FileGet(new Uri("https://tsdv-choco.transactpd.net/downloads/MF4100/MF4100.bin"), @"C:\Users\dclark\Downloads\MF4100.bin");
            _isDownloadComplete = false;
            fileLoader.FileGetAsync(
                new Uri("file:///G:/Users/dclark.BBBB/Pictures/Work/BbNewLogo.psd"),
                //new Uri("https://tsdv-choco.transactpd.net/downloads/MF4100/MF4100.bin"),
                @"C:\Users\dclark\Downloads\BbNewLogo.psd",
                HandleDownloadProgressChanged,
                HandleDownloadFileCompleted);
            while (!_isDownloadComplete)
            {
                Thread.Sleep(100);
            }
        }
    }
}
