﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BbTS.Core.Peripheral.Test
{
    [TestClass]
    public class SerialScaleTests
    {
        private static void WriteLine(string text)
        {
            System.Diagnostics.Debug.WriteLine(text);
        }

        [TestMethod]
        public void TestMethod1()
        {
            var serialScale = new SerialScale(1, ScaleType.Nci4000I, ScaleProtocol.Ecr);
            serialScale.Open();
            decimal weight;
            ScaleStatus status = serialScale.WeightCommand(out weight);
            serialScale.Close();
            WriteLine(weight.ToString("N"));
        }
    }
}
