﻿using System;
using System.Collections.Generic;
using BbTS.Core.Serialization;
using BbTS.Domain.Models.Container.Elements;
using BbTS.Domain.Models.Definitions.Container;
using BbTS.Domain.Models.Definitions.Originator;
using BbTS.Domain.Models.Device;
using BbTS.Domain.Models.General;
using Newtonsoft.Json;

namespace BbTS.Core.Transaction.Builders
{
    using Domain.Models.Container;

    /// <summary>
    /// Builder class designed to create and assemble a Transaction.  Sequence (of items in the Element list) is maintained internally.
    /// </summary>
    public class TransactionBuilder
    {
        private int _sequence;
        private bool _forcePost;
        private Disposition _disposition;

        /// <summary>
        /// The transaction object.
        /// </summary>
        public Transaction Transaction { get; }

        /// <summary>
        /// Originator ID.
        /// </summary>
        public Guid OriginatorId { get; }

        /// <summary>
        /// Device settings.
        /// </summary>
        public DeviceSettings DeviceSettings { get; }

        /// <summary>
        /// Creates a new TransactionBuilder instance.
        /// </summary>
        /// <param name="originatorId"></param>
        /// <param name="originatorType"></param>
        /// <param name="attendType"></param>
        /// <param name="deviceSettings"></param>
        /// <param name="forcePost"></param>
        public TransactionBuilder(Guid originatorId, OriginatorType? originatorType, AttendType? attendType, DeviceSettings deviceSettings, bool forcePost)
        {
            OriginatorId = originatorId;
            DeviceSettings = deviceSettings;
            _forcePost = forcePost;

            _sequence = 1;

            Transaction = new Transaction
            {
                Id = Guid.NewGuid(),
                OriginatorId = originatorId,
                Disposition = Disposition.Unknown,
                ForcePost = forcePost,
                TransactionTimestamps = new List<TransactionTimestamp>(),
                Elements = new List<Element>(),
                NameValues = new List<StringPair>(),
            };

            var originatorElement = new OriginatorElement
            {
                OriginatorId = originatorId,
                Type = originatorType,
                AttendType = attendType
            };

            AddElement(originatorElement);
        }

        /// <summary>
        /// Gets or sets the Disposition of the transaction.
        /// </summary>
        public Disposition Disposition
        {
            get { return _disposition; }
            set
            {
                _disposition = value;
                Transaction.Disposition = _disposition;
            }
        }

        /// <summary>
        /// Gets or sets the ForcePost transaction flag.
        /// </summary>
        public bool ForcePost
        {
            get { return _forcePost; }
            set
            {
                _forcePost = value;
                Transaction.ForcePost = _forcePost;
            }
        }

        /// <summary>
        /// Adds a timestamp of the specified type to the transaction.  Timestamp is expected to be in Local time.
        /// </summary>
        /// <param name="timestampType"></param>
        /// <param name="timestamp"></param>
        public void AddTimestamp(TimestampType timestampType, DateTime timestamp)
        {
            Transaction.TransactionTimestamps.Add(new TransactionTimestamp { TimestampType = timestampType, Timestamp = timestamp });
        }

        /// <summary>
        /// Adds an Element descendant to the Elements list, generates its ID, and sets its Sequence.  If it is known that the element already has an ID, then ID generation may be skipped.
        /// </summary>
        /// <param name="element"></param>
        /// <param name="generateId"></param>
        public void AddElement(Element element, bool generateId = true)
        {
            if (generateId) element.Id = Guid.NewGuid();
            element.Sequence = _sequence++;
            Transaction.Elements.Add(element);
        }

        /// <summary>
        /// Adds a name value pair to the transaction.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void AddNameValue(string key, string value)
        {
            Transaction.NameValues.Add(new StringPair { Key = key, Value = value });
        }

        /// <summary>
        /// Serialize the current Transaction object to JSON.
        /// </summary>
        /// <param name="isIndented"></param>
        /// <returns></returns>
        public string ToJson(bool isIndented = false)
        {
            return NewtonsoftJson.Serialize(Transaction, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.Auto, Formatting = isIndented ? Formatting.Indented : Formatting.None });
        }
    }
}
