﻿using BbTS.Domain.Models.Definitions.ArtsDataModel;

namespace BbTS.Core.Transaction.Builders
{
    /// <summary>
    /// Tracks information about a policy discount or surcharge that may be applied to the transaction.
    /// </summary>
    public class PolicyDiscountSurchargeTracker
    {
        /// <summary>
        /// The type of rule (0 = discount, 1 = surcharge)
        /// </summary>
        public DiscountSurchargeType Type { get; set; }

        /// <summary>
        /// The discount or surcharge rate.
        /// </summary>
        public decimal Rate { get; set; }

        /// <summary>
        /// Exact (pre-rounding) discount or surcharge amount.
        /// </summary>
        public decimal PreciseAmount { get; set; }

        /// <summary>
        /// Actual (post-rounding) discount or surcharge amount.
        /// </summary>
        public decimal Amount { get; set; }
    }
}