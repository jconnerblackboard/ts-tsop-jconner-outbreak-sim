﻿using System;
using System.Collections.Generic;
using System.Linq;
using BbTS.Domain.Models.ArtsDataModel;
using BbTS.Domain.Models.Container.Elements;
using BbTS.Domain.Models.Credential;
using BbTS.Domain.Models.Customer;
using BbTS.Domain.Models.Definitions.ArtsDataModel;
using BbTS.Domain.Models.Definitions.Transaction;
using BbTS.Domain.Models.Products;
using BbTS.Domain.Models.System;
using BbTS.Domain.Models.Terminal;
using BbTS.Domain.Models.Transaction.Financial;

namespace BbTS.Core.Transaction.Builders
{
    /// <summary>
    /// Domain class containing business logic related to creating an ArtsTransactionElement instance.
    /// </summary>
    public class ArtsTransactionElementBuilder
    {
        private List<PolicyDiscountSurchargeTracker> _policyDiscountSurchargeTrackers;
        private readonly List<TaxSchedule> _taxSchedules;

        #region private methods

        /// <summary>
        /// Adds a product price modifier line item.
        /// </summary>
        /// <param name="productSequenceNumber"></param>
        /// <param name="priceModifier"></param>
        /// <returns>True if the productSequenceNumber exists in the artsTransactionElement</returns>
        private bool AddPriceModifier(int productSequenceNumber, LineItemProductPriceModifier priceModifier)
        {
            var productLineItem = (LineItemProduct)ArtsElement.LineItems.Find(p => p.LineItemSequenceNumber == productSequenceNumber);
            if (productLineItem == null) return false;

            var priceModifiers = ArtsElement.LineItemProductPriceModifierExtract().Where(pm => pm.LineItemSequenceNumber == productSequenceNumber).ToList();

            //Finalize values in price modifier line item and add to line items
            priceModifier.Sequence = SequenceNumberGet();
            priceModifier.LineItemSequenceNumber = productSequenceNumber;
            priceModifier.PriceModifierSequenceNumber = GetNextPriceModifierSequenceNumber(priceModifiers);
            ArtsElement.LineItems.Add(priceModifier);
            //Update price in product line item
            productLineItem.ActualUnitPrice = priceModifier.NewUnitPrice;
            productLineItem.ExtendedAmount = productLineItem.ActualUnitPrice * productLineItem.Quantity;
            productLineItem.RetailPriceModifiedFlag = true;
            if (priceModifier.Type == ProductPriceModifierType.VariablePrice) productLineItem.RetailPriceEntryMethodType = RetailPriceEntryMethodType.ManuallyEntered;
            return true;
        }

        /// <summary>
        /// Calculate policy-based discounts and surcharges for the specified product line item and add appropriate price modifier line items.
        /// </summary>
        /// <param name="productLineItem"></param>
        /// <param name="products"></param>
        /// <param name="policyDiscountSurchargeRules"></param>
        /// <param name="accumulatedError"></param>
        private void ApplyTenderDiscountsAndSurchargesToProduct(LineItemProduct productLineItem, List<Product> products, List<DiscountSurchargeRule> policyDiscountSurchargeRules, ref decimal accumulatedError)
        {
            Product product = products.Find(pd => pd.ProductDetailId == productLineItem.ProductDetailId);
            var priceModifiers = ArtsElement.LineItemProductPriceModifierExtract().Where(pm => pm.LineItemSequenceNumber == productLineItem.LineItemSequenceNumber).ToList();
            decimal workingPrice = productLineItem.ExtendedAmount;  //This is our means to keep precision as each discount or surcharge is applied.
            for (int i = 0; i < policyDiscountSurchargeRules.Count; i++)
            {
                var rule = policyDiscountSurchargeRules[i];
                var tracker = _policyDiscountSurchargeTrackers[i];

                if (!product.AllowPolicyDiscount && !product.AllowPolicySurcharge) continue;

                var discountSurchargeAmount = new RoundedDecimal(workingPrice * rule.Rate, 2);
                discountSurchargeAmount.AdjustForAccumulatedError(ref accumulatedError);
                var pm = new LineItemProductPriceModifier
                {
                    ProductPromotionId = 0,
                    Amount = discountSurchargeAmount.RoundedValue,
                    Percent = workingPrice == 0 ? 0.0m : discountSurchargeAmount.PreciseValue / workingPrice,
                    PreviousUnitPrice = productLineItem.ActualUnitPrice,
                    CalculationMethod = ProductPriceModifierCalculationMethod.Percentage
                };
                switch (rule.Type)
                {
                    case DiscountSurchargeType.Discount:
                        pm.Type = ProductPriceModifierType.TenderDiscount;
                        pm.NewUnitPrice = pm.PreviousUnitPrice - discountSurchargeAmount.RoundedValue;
                        workingPrice -= discountSurchargeAmount.PreciseValue;
                        break;
                    case DiscountSurchargeType.Surcharge:
                        pm.Type = ProductPriceModifierType.TenderSurcharge;
                        pm.NewUnitPrice = pm.PreviousUnitPrice + discountSurchargeAmount.RoundedValue;
                        workingPrice += discountSurchargeAmount.PreciseValue;
                        break;
                }

                //Add the price modifier to both our temporary list and the transaction.
                AddPriceModifier(productLineItem.LineItemSequenceNumber, pm);
                priceModifiers.Add(pm);
                tracker.PreciseAmount += discountSurchargeAmount.PreciseValue;
                tracker.Amount += discountSurchargeAmount.RoundedValue;
            }
        }

        /// <summary>
        /// Calculates tax for the specified product line item and adds tax-related line items.
        /// </summary>
        /// <param name="productLineItem"></param>
        /// <param name="preTaxTotal"></param>
        /// <param name="taxSchedules"></param>
        /// <param name="taxCalculationMode"></param>
        /// <param name="tenderId"></param>
        /// <param name="accumulatedError"></param>
        private void ApplyTaxToProduct(LineItemProduct productLineItem, decimal preTaxTotal, List<TaxSchedule> taxSchedules, TaxCalculationMode taxCalculationMode, int tenderId, ref decimal accumulatedError)
        {
            if (productLineItem == null) return;

            decimal taxableAmount = productLineItem.ExtendedAmount; //Start with original price, later we may add taxes that are also taxable

            //Calculate normal tax (all products must have a tax schedule, so this loop will run at least once)
            decimal normalTaxAccumulatedError = accumulatedError;
            TaxSchedule taxSchedule = taxSchedules.Find(ts => ts.TaxScheduleId == productLineItem.TaxScheduleId);
            TaxSchedule currentTaxSchedule = taxSchedule;
            LineItemProductTax lineItemProductTax;
            while (currentTaxSchedule != null)
            {
                lineItemProductTax = new LineItemProductTax
                {
                    Sequence = SequenceNumberGet(),
                    LineItemSequenceNumber = productLineItem.LineItemSequenceNumber,
                    TaxScheduleId = currentTaxSchedule.TaxScheduleId,
                    TaxAccountName = currentTaxSchedule.TaxAccountName,
                    TaxablePercent = 1.0m, //The entire amount is taxable (we don't do split tender transactions on the terminal.)
                    TaxableAmount = taxableAmount
                };

                //Start with 0 tax
                decimal taxRate = 0.0m;
                var taxAmount = new RoundedDecimal(0.0m, 2);

                //Use tenderTax if present (normal case)
                TenderTax tenderTax = currentTaxSchedule.TenderTaxList.Find(tt => tt.TenderId == tenderId);
                if (tenderTax != null)
                {
                    taxRate = tenderTax.TaxRate;
                    taxAmount = new RoundedDecimal(taxableAmount * taxRate, 2);

                    lineItemProductTax.TaxExemptOverrideType = TaxExemptOverrideType.Normal;
                }

                //See if the product is tax exempt
                LineItemProductTaxExempt lineItemProductTaxExempt = null;
                TaxExemptReason? taxExemptReason = IsTaxExempt(ArtsElement, productLineItem, taxSchedules, preTaxTotal, taxCalculationMode);
                if (taxExemptReason.HasValue)
                {
                    RoundedDecimal exemptTaxAmount = taxAmount;

                    taxRate = 0.0m;
                    taxAmount = new RoundedDecimal(0.0m, 2);

                    lineItemProductTax.TaxExemptOverrideType = TaxExemptOverrideType.Exempt;
                    lineItemProductTaxExempt = new LineItemProductTaxExempt
                    {
                        Sequence = SequenceNumberGet(),
                        LineItemSequenceNumber = productLineItem.LineItemSequenceNumber,
                        TaxExemptReason = taxExemptReason.Value,
                        ExemptTaxableAmount = taxableAmount,
                        ExemptTaxAmount = exemptTaxAmount.RoundedValue
                    };
                }

                //Finally, add the taxData to the list, but only if there were meaningful operations
                if ((tenderTax != null) || (lineItemProductTax.TaxExemptOverrideType != TaxExemptOverrideType.Normal))
                {
                    taxAmount.AdjustForAccumulatedError(ref normalTaxAccumulatedError);

                    lineItemProductTax.TaxPercent = taxRate;
                    lineItemProductTax.TaxAmount = taxAmount.RoundedValue;

                    //If this tax is taxable, update the taxable amount so that the next calculation taxes the tax.
                    if (currentTaxSchedule.TaxLinkTaxTaxes)
                    {
                        taxableAmount += taxAmount.PreciseValue;
                    }

                    //Add the tax data to the list
                    ArtsElement.LineItems.Add(lineItemProductTax);
                    if (lineItemProductTaxExempt != null) ArtsElement.LineItems.Add(lineItemProductTaxExempt);
                }

                //Check for tax link, if present, set tax schedule and repeat process.
                int taxScheduleId = currentTaxSchedule.TaxLinkTaxScheduleId;
                currentTaxSchedule = taxSchedules.Find(ts => ts.TaxScheduleId == taxScheduleId);
            }

            //If there is no tax override, we can continue to the next product
            var lineItemTax = (LineItemTax)ArtsElement.LineItems.Find(li => li is LineItemTax);
            if (lineItemTax == null)
            {
                accumulatedError = normalTaxAccumulatedError;
                return;
            }

            #region Tax Override

            //Since, in the case of tax links, there can be multiple tax schedules, we will only link the override to the TaxScheduleId of the first tax schedule.
            taxSchedule = taxSchedules.Find(ts => ts.TaxScheduleId == productLineItem.TaxScheduleId); // artsProductElement.TaxScheduleId);

            //Calculate overridden (original) values (taking all tax schedules into account)
            var productTaxLineItems = ArtsElement.LineItemProductTaxExtract().Where(pt => pt.LineItemSequenceNumber == productLineItem.LineItemSequenceNumber).ToList();
            decimal overriddenTaxAmount = productTaxLineItems.Sum(t => t.TaxAmount);  //Determine the total tax applied
            decimal overriddenTaxPercent = overriddenTaxAmount / taxableAmount;  //Determine the effective tax percent


            //Calculate override (new) values
            var newTaxAmount = new RoundedDecimal(taxableAmount * lineItemTax.TaxPercent, 2);
            decimal newTaxPercent = lineItemTax.TaxPercent;

            newTaxAmount.AdjustForAccumulatedError(ref accumulatedError);

            lineItemProductTax = new LineItemProductTax
            {
                Sequence = SequenceNumberGet(),
                LineItemSequenceNumber = productLineItem.LineItemSequenceNumber,
                TaxScheduleId = taxSchedule.TaxScheduleId,
                TaxAccountName = taxSchedule.TaxAccountName,
                TaxablePercent = 1.0m,
                TaxableAmount = taxableAmount,
                TaxPercent = newTaxPercent,
                TaxAmount = newTaxAmount.RoundedValue,
                TaxExemptOverrideType = TaxExemptOverrideType.Override,
            };
            var lineItemProductTaxOverride = new LineItemProductTaxOverride
            {
                Sequence = SequenceNumberGet(),
                LineItemSequenceNumber = productLineItem.LineItemSequenceNumber,
                TaxableAmount = taxableAmount,
                OriginalTaxAmount = overriddenTaxAmount,
                NewTaxAmount = newTaxAmount.RoundedValue,
                OriginalTaxPercent = overriddenTaxPercent,
                NewTaxPercent = newTaxPercent,
            };

            //Clear out old tax data then add the override
            ArtsElement.LineItems.RemoveAll(li =>
                li.LineItemSequenceNumber == productLineItem.LineItemSequenceNumber &&
                (li is LineItemProductTax || li is LineItemProductTaxOverride || li is LineItemProductTaxExempt));

            ArtsElement.LineItems.Add(lineItemProductTax);
            ArtsElement.LineItems.Add(lineItemProductTaxOverride);

            //Update tax key information
            lineItemTax.TaxableAmount += lineItemProductTax.TaxableAmount;
            lineItemTax.TaxAmount += lineItemProductTax.TaxAmount;

            #endregion
        }

        /// <summary>
        /// Checks if a product line item is tax exempt.
        /// </summary>
        /// <param name="artsElement"></param>
        /// <param name="productLineItem"></param>
        /// <param name="taxSchedules"></param>
        /// <param name="preTaxTotal"></param>
        /// <param name="taxCalculationMode"></param>
        /// <returns></returns>
        private static TaxExemptReason? IsTaxExempt(ArtsTransactionElement artsElement, LineItemProduct productLineItem, List<TaxSchedule> taxSchedules, decimal preTaxTotal, TaxCalculationMode taxCalculationMode)
        {
            TaxSchedule taxSchedule = taxSchedules.Find(ts => ts.TaxScheduleId == productLineItem.TaxScheduleId);
            if (taxSchedule == null) return null;

            if (taxCalculationMode == TaxCalculationMode.Exempt)
            {
                return TaxExemptReason.StoredValueAccountType;
            }

            if (taxSchedule.TaxExemptQuantityEnabled)
            {
                //This basically means that if we exceed a certain number of products that have this
                //tax schedule/tax group, all of the items will be tax exempt (even if part of a promotion).
                var productLineItems = artsElement.LineItemProductsExtract();

                //Compare each transaction product's tax schedule (and any linked tax schedules) to this tax schedule and this productLineItem's tax group
                int productCount = 0;

                foreach (LineItemProduct p in productLineItems)
                {
                    TaxSchedule otherProductTaxSchedule = taxSchedules.Find(ts => ts.TaxScheduleId == p.TaxScheduleId);
                    while (otherProductTaxSchedule != null)
                    {
                        if (otherProductTaxSchedule.TaxScheduleId == taxSchedule.TaxScheduleId &&
                            p.TaxGroupId.HasValue && productLineItem.TaxGroupId.HasValue &&
                            p.TaxGroupId.Value == productLineItem.TaxGroupId.Value)
                        {
                            //One match is all we need, so we don't need to follow any tax links
                            ++productCount;
                            break;
                        }

                        otherProductTaxSchedule = taxSchedules.Find(ts => ts.TaxScheduleId == otherProductTaxSchedule.TaxLinkTaxScheduleId);
                    }

                    //Quit early if we go over the quantity
                    if (productCount > taxSchedule.TaxExemptQuantity) break;
                }

                if (productCount > taxSchedule.TaxExemptQuantity)
                {
                    return TaxExemptReason.TaxGroupQuantity;
                }
            }

            if (((taxSchedule.TaxableAmountMinimumEnabled) && (preTaxTotal < taxSchedule.TaxableAmountMinimum)) ||
                ((taxSchedule.TaxableAmountMaximumEnabled) && (preTaxTotal > taxSchedule.TaxableAmountMaximum)))
            {
                return TaxExemptReason.TransactionSubtotal;
            }

            return null;
        }

        /// <summary>
        /// Gets the next sequence number to be used by the next line item.
        /// </summary>
        /// <returns>Sequence number</returns>
        private int SequenceNumberGet()
        {
            int sequence = 1;
            foreach (var lineItem in ArtsElement.LineItems)
            {
                if (lineItem.Sequence < sequence) continue;
                sequence = lineItem.Sequence + 1;
            }

            return sequence;
        }

        /// <summary>
        /// Gets the next legacy sequence number to be used by the next line item.
        /// </summary>
        /// <returns>Sequence number</returns>
        private int GetNextLineItemSequenceNumber()
        {
            int sequence = 1;
            foreach (var lineItem in ArtsElement.LineItems)
            {
                if (lineItem.LineItemSequenceNumber < sequence) continue;
                sequence = lineItem.LineItemSequenceNumber + 1;
            }

            return sequence;
        }

        /// <summary>
        /// Gets the next sequence number to be used by the next product price modifier.
        /// </summary>
        /// <param name="priceModifiers"></param>
        /// <returns>Sequence number</returns>
        private static int GetNextPriceModifierSequenceNumber(List<LineItemProductPriceModifier> priceModifiers)
        {
            int sequence = 1;
            foreach (var priceModifier in priceModifiers)
            {
                if (priceModifier.PriceModifierSequenceNumber < sequence) continue;
                sequence = priceModifier.PriceModifierSequenceNumber + 1;
            }

            return sequence;
        }

        /// <summary>
        /// Returns the price of a product, including any modifiers if present.
        /// </summary>
        /// <param name="productLineItem"></param>
        /// <param name="priceModifiers"></param>
        /// <returns></returns>
        private static decimal GetModifiedPrice(LineItemProduct productLineItem, List<LineItemProductPriceModifier> priceModifiers)
        {
            //If no price modifiers, just return the product price
            if (!productLineItem.RetailPriceModifiedFlag) return productLineItem.RetailPrice;

            //Otherwise, return the most recent price modifier price (denoted by sequence)
            decimal price = 0.00m;
            int i = -1;
            foreach (var priceModifier in priceModifiers)
            {
                if (priceModifier.PriceModifierSequenceNumber <= i) continue;
                i = priceModifier.PriceModifierSequenceNumber;
                price = priceModifier.NewUnitPrice;
            }

            return price;
        }

        /// <summary>
        /// Returns a subtotal that equals the sum of the prices of all products in the transaction.
        /// </summary>
        /// <returns>Pre-tax subtotal</returns>
        public decimal ModifiedPriceSubtotal()
        {
            var productLineItems = ArtsElement.LineItemProductsExtract();
            return productLineItems.Sum(productLineItem => productLineItem.ExtendedAmount);
        }

        /// <summary>
        /// Returns a subtotal that equals the sum of all taxes in the transaction
        /// </summary>
        /// <returns>Total tax amount</returns>
        private decimal TaxSubtotal()
        {
            var taxes = ArtsElement.LineItemProductTaxExtract();
            return taxes.Sum(tax => tax.TaxAmount);
        }

        /// <summary>
        /// Adds tender information common to all tender types
        /// </summary>
        /// <param name="tenderLineItem"></param>
        /// <param name="tenderId"></param>
        /// <param name="tenderAmount"></param>
        /// <param name="roundingAmount"></param>
        /// <param name="tenderTax"></param>
        private void AddBaseTenderInfo(LineItemTender tenderLineItem, int tenderId, decimal tenderAmount, decimal roundingAmount, decimal? tenderTax)
        {
            tenderLineItem.Sequence = SequenceNumberGet();
            tenderLineItem.TenderId = tenderId;
            tenderLineItem.TenderAmount = tenderAmount;
            tenderLineItem.RoundingAmount = roundingAmount;
            tenderLineItem.TaxAmount = tenderTax;
            tenderLineItem.LineItemSequenceNumber = GetNextLineItemSequenceNumber();
        }

        #endregion

        /// <summary>
        /// The ArtsTransactionElement object.
        /// </summary>
        public ArtsTransactionElement ArtsElement { get; }

        /// <summary>
        /// Creates a new instance of the ArtsTransactionElement class.
        /// </summary>
        /// <param name="transactionNumber"></param>
        /// <param name="controlTotal"></param>
        /// <param name="taxSchedules"></param>
        public ArtsTransactionElementBuilder(int transactionNumber, decimal controlTotal, List<TaxSchedule> taxSchedules)
        {
            ArtsElement = new ArtsTransactionElement
            {
                Id = Guid.NewGuid(),
                TransactionNumber = transactionNumber,
                ControlTotalBeforeTransaction = controlTotal,
                ControlTotalAfterTransaction = controlTotal,  //this will be updated when CalculateChangeInControlTotal is called
                LineItems = new List<LineItem>()
            };
            _taxSchedules = taxSchedules;
            _policyDiscountSurchargeTrackers = new List<PolicyDiscountSurchargeTracker>();
        }

        /// <summary>
        /// Adds a product.
        /// </summary>
        /// <param name="product"></param>
        /// <param name="quantity"></param>
        /// <param name="productEntryMethod"></param>
        /// <param name="priceEntryMethod"></param>
        /// <returns>LineItemSequenceNumber</returns>
        public int AddProduct(Product product, int quantity, ProductEntryMethodType productEntryMethod, RetailPriceEntryMethodType priceEntryMethod)
        {
            var productLineItem = new LineItemProduct
            {
                Sequence = SequenceNumberGet(),
                LineItemSequenceNumber = GetNextLineItemSequenceNumber(),
                ProductDetailId = product.ProductDetailId,
                ProductDescription = product.ProductDescription,
                TaxScheduleId = product.TaxScheduleId,
                TaxAccountName = _taxSchedules?.Find(ts => ts.TaxScheduleId == product.TaxScheduleId)?.TaxAccountName,
                TaxGroupId = product.TaxGroupId,
                RetailPrice = product.RetailPrice,
                RetailPriceQuantity = quantity,
                ActualUnitPrice = product.RetailPrice, //If a price modifier is added later, this value will change to reflect the new price
                ActualUnitPriceQuantity = 1,
                Quantity = quantity,
                Units = quantity,
                UnitListPrice = product.RetailPrice,
                UnitListPriceQuantity = quantity,
                UnitMeasureType = UnitMeasureType.Count,
                ExtendedAmount = product.RetailPrice * quantity,  //actualUnitPrice * quantity    If a price modifier is added later, this value will change to reflect the new price
                ProductEntryMethodType = productEntryMethod,
                RetailPriceEntryMethodType = priceEntryMethod
            };

            ArtsElement.LineItems.Add(productLineItem);
            return productLineItem.LineItemSequenceNumber;
        }

        /// <summary>
        /// When a product is added that is of type ProductType.PromptForPrice, this function should be called to set the price modifier to the price entered by the cashier.
        /// </summary>
        /// <param name="productSequenceNumber"></param>
        /// <param name="price"></param>
        /// <returns></returns>
        public bool SetPriceForVariablePriceProduct(int productSequenceNumber, decimal price)
        {
            return AddPriceModifier(productSequenceNumber, new LineItemProductPriceModifier
            {
                Type = ProductPriceModifierType.VariablePrice,
                Amount = 0.00m,  //Amount should be $0.00 for a variable priced product because the price of the product starts out as the value entered at the time of sale, so there is no "previous price".
                CalculationMethod = ProductPriceModifierCalculationMethod.ManuallyEnteredPrice,
                PreviousUnitPrice = price,
                NewUnitPrice = price
            });
        }

        /// <summary>
        /// When a product is added that is of type ProductType.Weighed or ProductType.WeighedZeroTare, this function should be called to set the price of the product based on netWeight.
        /// </summary>
        /// <param name="productSequenceNumber"></param>
        /// <param name="netWeight"></param>
        /// <param name="retailPrice"></param>
        /// <returns></returns>
        public bool SetPriceForWeighedProduct(int productSequenceNumber, decimal netWeight, out decimal retailPrice)
        {
            retailPrice = 0.00m;

            var productLineItem = (LineItemProduct)ArtsElement.LineItems.Find(p => p.LineItemSequenceNumber == productSequenceNumber);
            if (productLineItem == null) return false;

            retailPrice = decimal.Truncate(productLineItem.UnitListPrice * netWeight * 100) / 100m;

            productLineItem.RetailPrice = retailPrice;
            productLineItem.ActualUnitPrice = retailPrice;
            productLineItem.UnitMeasureType = UnitMeasureType.Weight;
            productLineItem.ExtendedAmount = productLineItem.ActualUnitPrice * productLineItem.Quantity;
            productLineItem.Units = (int)(netWeight * 1000);
            return true;
        }

        /// <summary>
        /// Adds a deposit line item to the transaction.  This function is used for both primary and enrichment deposits.
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="storedValueAccountId"></param>
        /// <param name="customerCredential"></param>
        /// <param name="parentLineItemSequence">For primary deposit, this should be null.  For enrichment deposits, this should be the LineItemSequenceNumber of the primary deposit.</param>
        /// <returns></returns>
        public int AddDeposit(decimal amount, int storedValueAccountId, CustomerCredential customerCredential, int? parentLineItemSequence = null)
        {
            var lineItemDeposit = new LineItemStoredValue
            {
                Sequence = SequenceNumberGet(),
                LineItemSequenceNumber = GetNextLineItemSequenceNumber(),
                DebitCreditType = DebitCreditType.Credit,
                StoredValueAmount = amount,
                StoredValueAccountTypeId = storedValueAccountId,
                EnrichmentFlag = parentLineItemSequence != null,
                EnrichmentParentLineItemSequence = parentLineItemSequence ?? 0,
                CustomerCredential = customerCredential
            };

            ArtsElement.LineItems.Add(lineItemDeposit);
            return lineItemDeposit.LineItemSequenceNumber;
        }

        /// <summary>
        /// Adds additional information about an enrichment deposit to the transaction.
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="rate"></param>
        /// <param name="parentLineItemSequence"></param>
        /// <param name="enrichmentSequence"></param>
        /// <returns></returns>
        public int AddEnrichmentInformation(RoundedDecimal amount, decimal rate, int parentLineItemSequence, int enrichmentSequence)
        {
            var lineItemEnrichmentInformation = new LineItemStoredValueEnrichment
            {
                Sequence = SequenceNumberGet(),
                LineItemSequenceNumber = parentLineItemSequence,
                StoredValueEnrichmentSequence = enrichmentSequence,
                Percentage = rate,
                RoundingAmount = amount.RoundingAmount,
                Amount = amount.RoundedValue
            };

            ArtsElement.LineItems.Add(lineItemEnrichmentInformation);
            return lineItemEnrichmentInformation.LineItemSequenceNumber;
        }

        /// <summary>
        /// Adds tender information for a cash tender.
        /// </summary>
        /// <param name="tender"></param>
        /// <param name="tenderAmount"></param>
        /// <param name="roundingAmount"></param>
        /// <param name="tenderTax"></param>
        /// <returns></returns>
        public int AddCashTender(Tender tender, decimal tenderAmount, decimal roundingAmount, decimal? tenderTax)
        {
            var tenderLineItem = new LineItemTenderCash();

            AddBaseTenderInfo(tenderLineItem, tender.Id, tenderAmount, roundingAmount, tenderTax);
            ArtsElement.LineItems.Add(tenderLineItem);
            return tenderLineItem.LineItemSequenceNumber;
        }

        /// <summary>
        /// Adds tender information for a check tender.
        /// </summary>
        /// <param name="tender"></param>
        /// <param name="tenderAmount"></param>
        /// <param name="roundingAmount"></param>
        /// <param name="tenderTax"></param>
        /// <returns></returns>
        public int AddCheckTender(Tender tender, decimal tenderAmount, decimal roundingAmount, decimal? tenderTax)
        {
            var tenderLineItem = new LineItemTenderCheck();

            AddBaseTenderInfo(tenderLineItem, tender.Id, tenderAmount, roundingAmount, tenderTax);
            ArtsElement.LineItems.Add(tenderLineItem);
            return tenderLineItem.LineItemSequenceNumber;
        }

        /// <summary>
        /// Adds tender information for a stored value tender.
        /// </summary>
        /// <param name="tender"></param>
        /// <param name="tenderAmount"></param>
        /// <param name="roundingAmount"></param>
        /// <param name="tenderTax"></param>
        /// <param name="customerCredential"></param>
        /// <returns></returns>
        public int AddStoredValueTender(Tender tender, decimal tenderAmount, decimal roundingAmount, decimal? tenderTax, CustomerCredential customerCredential)
        {
            var tenderLineItem = new LineItemTenderWithCustomer { CustomerCredential = customerCredential };

            AddBaseTenderInfo(tenderLineItem, tender.Id, tenderAmount, roundingAmount, tenderTax);
            ArtsElement.LineItems.Add(tenderLineItem);

            foreach (var tracker in _policyDiscountSurchargeTrackers)
            {
                if (tracker.Amount <= 0.00m) continue;

                switch (tracker.Type)
                {
                    case DiscountSurchargeType.Discount:
                        AddTenderDiscount(tenderLineItem.Sequence, tenderLineItem.LineItemSequenceNumber, tracker.PreciseAmount, tracker.Amount, tracker.Rate);
                        break;
                    case DiscountSurchargeType.Surcharge:
                        AddTenderSurcharge(tenderLineItem.Sequence, tenderLineItem.LineItemSequenceNumber, tracker.PreciseAmount, tracker.Amount, tracker.Rate);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            return tenderLineItem.LineItemSequenceNumber;
        }

        /// <summary>
        /// Adds tender information for an EMV tender.
        /// </summary>
        /// <param name="tender"></param>
        /// <param name="tenderAmount"></param>
        /// <param name="roundingAmount"></param>
        /// <param name="tenderTax"></param>
        /// <param name="txnRef"></param>
        /// <returns></returns>
        [Obsolete("This should only be used for MF4100")]
        public int AddEmvTender(Tender tender, decimal tenderAmount, decimal roundingAmount, decimal? tenderTax, string txnRef)
        {
            var tenderLineItem = new LineItemTenderEmv
            {
                TransactionReference = txnRef,
                //AmountRequested = tenderAmount,
                //AmountAuthorized = tenderAmount,
                //AuthorizationCode = "ABC123",
                //CardId = 1,
                //CardSuffix = "",
                //CommandSequence = 0,
                //DpsTransactionReference = "dummy",
                EmvDeviceId = "uni2cafeusd0001",
                //Get1DateTime = DateTime.Now,
                Get1Result = new ActionResultToken { Id = "7aaf6b0b-36c0-40e6-af05-a2752556fc8d", ResultDomain = 236, ResultDomainId = 1, Message = "Fake Success" },
                //MaskedPan = "411111XXXXXX1111",
                //MerchantReference = "dummy",
                //PaymentExpressRequestDateTime = DateTime.Now,
                //PaymentExpressResponseDateTime = DateTime.Now,
                //PurchaseResponseCode = "00",
                Result = new ActionResultToken { Id = "b8b6b5f7-9b5d-484a-baa2-36b7f50be0d4", ResultDomain = 236, ResultDomainId = 1, Message = "Fake Success" },
                //SettlementDateTime = DateTime.Now,
                //Stan = 0
            };

            AddBaseTenderInfo(tenderLineItem, tender.Id, tenderAmount, roundingAmount, tenderTax);
            ArtsElement.LineItems.Add(tenderLineItem);
            return tenderLineItem.LineItemSequenceNumber;
        }

        /// <summary>
        /// Adds tender information for an EMV HIT tender.
        /// </summary>
        /// <param name="tender"></param>
        /// <param name="tenderAmount"></param>
        /// <param name="roundingAmount"></param>
        /// <param name="tenderTax"></param>
        /// <param name="txnRef"></param>
        /// <param name="emvHitSettings"></param>
        /// <returns></returns>
        public int AddEmvHitTender(Tender tender, decimal tenderAmount, decimal roundingAmount, decimal? tenderTax, string txnRef, EmvHitSettings emvHitSettings)
        {
            var tenderLineItem = new LineItemTenderEmvHit
            {
                AmountRequested = tenderAmount,
                AmountAuthorized = tenderAmount,
                AuthorizationCode = "INATOR",
                CardId = EmvCreditCardId.Visa,
                DpsTransactionReference = "dummy",
                EmvDeviceId = emvHitSettings.DeviceId,
                MaskedPan = "411111XXXXXX1111",
                MerchantReference = "dummy",
                PaymentExpressRequestDateTime = DateTimeOffset.Now,
                PaymentExpressResponseDateTime = DateTimeOffset.Now,
                PurchaseResponseCode = "00",
                Result = new ActionResultToken { Id = "036cc193-fb0b-4006-bd4c-4e2769df01e1", ResultDomain = 236, ResultDomainId = 1, Message = "Fake Success" },
                SettlementDateTime = DateTimeOffset.Now,
                SystemTraceAuditNumber = 0,
                TransactionReference = txnRef
            };

            AddBaseTenderInfo(tenderLineItem, tender.Id, tenderAmount, roundingAmount, tenderTax);
            ArtsElement.LineItems.Add(tenderLineItem);
            return tenderLineItem.LineItemSequenceNumber;
        }

        /// <summary>
        /// Adds tender information for a cash equivalence tender.
        /// </summary>
        /// <param name="tender"></param>
        /// <param name="tenderAmount"></param>
        /// <param name="roundingAmount"></param>
        /// <param name="tenderTax"></param>
        /// <param name="customerCredential"></param>
        /// <param name="boardMealTypeId"></param>
        /// <param name="cashEquivAmountLimit"></param>
        /// <param name="isGuestMeal"></param>
        /// <returns></returns>
        public int AddCashEquivalenceTender(Tender tender, decimal tenderAmount, decimal roundingAmount, decimal? tenderTax, CustomerCredential customerCredential, int boardMealTypeId, decimal cashEquivAmountLimit, bool isGuestMeal)
        {
            var tenderLineItem = new LineItemTenderCashEquivalence
            {
                CustomerCredential = customerCredential,
                BoardMealTypeId = boardMealTypeId,
                CashEquivalenceAmountLimit = cashEquivAmountLimit,
                IsGuestMeal = isGuestMeal
            };

            AddBaseTenderInfo(tenderLineItem, tender.Id, tenderAmount, roundingAmount, tenderTax);
            ArtsElement.LineItems.Add(tenderLineItem);
            return tenderLineItem.LineItemSequenceNumber;
        }

        /// <summary>
        /// Adds tender discount information.
        /// </summary>
        /// <param name="tenderSequence"></param>
        /// <param name="tenderLineItemSequenceNumber"></param>
        /// <param name="preciseAmount"></param>
        /// <param name="amount"></param>
        /// <param name="rate"></param>
        /// <returns></returns>
        public int AddTenderDiscount(int tenderSequence, int tenderLineItemSequenceNumber, decimal preciseAmount, decimal amount, decimal rate)
        {
            var lineItemTenderDiscount = new LineItemTenderDiscount
            {
                Sequence = SequenceNumberGet(),
                LineItemSequenceNumber = tenderLineItemSequenceNumber,  //Have to do it this way for legacy reasons
                TenderSequence = tenderSequence,
                DiscountSurchargeReason = DiscountSurchargeReason.PolicyDiscountSurcharge,
                Rate = rate,
                RoundingAmount = amount - preciseAmount,
                Amount = amount,
                ProratedFlag = false,
                CalculationType = CalculationType.Percent
            };

            ArtsElement.LineItems.Add(lineItemTenderDiscount);
            return lineItemTenderDiscount.LineItemSequenceNumber;
        }

        /// <summary>
        /// Adds tender surcharge information.
        /// </summary>
        /// <param name="tenderSequence"></param>
        /// <param name="tenderLineItemSequenceNumber"></param>
        /// <param name="preciseAmount"></param>
        /// <param name="amount"></param>
        /// <param name="rate"></param>
        /// <returns></returns>
        public int AddTenderSurcharge(int tenderSequence, int tenderLineItemSequenceNumber, decimal preciseAmount, decimal amount, decimal rate)
        {
            var lineItemTenderSurcharge = new LineItemTenderSurcharge
            {
                Sequence = SequenceNumberGet(),
                LineItemSequenceNumber = tenderLineItemSequenceNumber,  //Have to do it this way for legacy reasons
                TenderSequence = tenderSequence,
                DiscountSurchargeReason = DiscountSurchargeReason.PolicyDiscountSurcharge,
                Rate = rate,
                RoundingAmount = amount - preciseAmount,
                Amount = amount,
                ProratedFlag = false,
                CalculationType = CalculationType.Percent
            };

            ArtsElement.LineItems.Add(lineItemTenderSurcharge);
            return lineItemTenderSurcharge.LineItemSequenceNumber;
        }

        /// <summary>
        /// Calculates tax and adds tax-related line items.  Calling this method will remove any pre-existing tax related line items from the transaction before adding new tax line items.
        /// </summary>
        /// <param name="taxSchedules"></param>
        /// <param name="taxCalculationMode"></param>
        /// <param name="tenderId"></param>
        /// <returns>Total tax for tender.</returns>
        public decimal CalculateTaxForTender(List<TaxSchedule> taxSchedules, TaxCalculationMode taxCalculationMode, int tenderId)
        {
            //Remove any tax line items that might exist
            ArtsElement.LineItems.RemoveAll(li => li is LineItemProductTax || li is LineItemProductTaxOverride || li is LineItemProductTaxExempt);

            //TaxCalculationMode.NotTaxed case (all other TaxCalculationMode values are handled within ApplyTaxToProduct)
            if (taxCalculationMode == TaxCalculationMode.NotTaxed) return 0.00m;

            //Determine the amount of the transaction before taxes are applied
            decimal preTaxTotal = ModifiedPriceSubtotal();  //We just need this for tax exempt checks further down

            //Cycle through each product line item and apply tax.  This will add line items of various types to the transaction.
            var productLineItems = ArtsElement.LineItemProductsExtract();

            //A note on accumulated error:
            //When multiple products are involved in a transaction, the tax must be applied proportionally to each product in the transaction.
            //Since the percentage calculations often result in tax amounts that must be rounded to the penny, we need to keep track of rounding error
            //and add or subtract a penny whenever the error is +/- a half penny.  The accumulatedError value tracks this and allows the error
            //to be factored in to the calculations performed on the next product in line.  By the end of the transaction, we are guaranteed that
            //accumulated error conforms to -0.005 < accumulatedError <= 0.005, and that the overall amount of tax charged is correct to the nearest
            //penny (using an "away from zero" rounding rule).
            decimal accumulatedError = 0;
            foreach (var productLineItem in productLineItems)
            {
                ApplyTaxToProduct(productLineItem, preTaxTotal, taxSchedules, taxCalculationMode, tenderId, ref accumulatedError);
            }

            //Determine total tax calculated for this tender
            //Note that once we build in split tender support, this math will need to be revisited to properly factor in multiple tenders.
            decimal tenderTaxTotal = 0.00m;
            foreach (var productLineItem in productLineItems)
            {
                var productTaxLineItems = ArtsElement.LineItemProductTaxExtract().Where(pt => pt.LineItemSequenceNumber == productLineItem.LineItemSequenceNumber).ToList();
                tenderTaxTotal += productTaxLineItems.Sum(t => t.TaxAmount);
            }

            return tenderTaxTotal;
        }

        /// <summary>
        /// Calculates discounts and surcharges and adds appropriate price modifier line items.  Do not call this function more than once for a transaction!  Unlike CalculateTaxForTender,
        /// this function does not have logic to remove old tender discount/surcharge line items and restore the transaction to the state prior to their addition.
        /// </summary>
        /// <param name="discountSurchargeRules"></param>
        /// <param name="products"></param>
        /// <param name="tenderId"></param>
        public void CalculateDiscountsAndSurchargesForTender(List<DiscountSurchargeRule> discountSurchargeRules, List<Product> products, int tenderId)
        {
            _policyDiscountSurchargeTrackers = new List<PolicyDiscountSurchargeTracker>();
            foreach (var discountSurchargeRule in discountSurchargeRules)
            {
                var tracker = new PolicyDiscountSurchargeTracker
                {
                    Type = discountSurchargeRule.Type,
                    Rate = discountSurchargeRule.Rate,
                    PreciseAmount = 0.00m,
                    Amount = 0.00m
                };

                _policyDiscountSurchargeTrackers.Add(tracker);
            }

            var productLineItems = ArtsElement.LineItemProductsExtract();
            //See note on accumulated error in CalculateTaxForTender.  We must track error for discounts and surcharges the same way we do for taxes.
            decimal accumulatedError = 0;
            foreach (var productLineItem in productLineItems)
            {
                ApplyTenderDiscountsAndSurchargesToProduct(productLineItem, products, discountSurchargeRules, ref accumulatedError);
            }
        }

        /// <summary>
        /// Calculates and returns the total amount required to pay for the transaction, including all discounts, surcharges, taxes, and rounding.
        /// </summary>
        /// <param name="tender"></param>
        /// <param name="roundingAmount"></param>
        /// <returns></returns>
        public decimal CalculateTransactionTotal(Tender tender, out decimal roundingAmount)
        {
            return ArtsElement.CalculateTransactionTotal(tender, out roundingAmount);
        }

        /// <summary>
        /// Calculates (and returns) the change in control total and updates the ArtsTransactionElement with the control total information.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public decimal CalculateChangeInControlTotal()
        {
            //Products
            var productLineItems = ArtsElement.LineItemProductsExtract();
            decimal changeInControlTotal = productLineItems.Sum(productLineItem => Math.Abs(productLineItem.ExtendedAmount));

            //Price Modifiers
            var priceModifiers = ArtsElement.LineItemProductPriceModifierExtract();
            changeInControlTotal += priceModifiers.Sum(priceModifier => Math.Abs(priceModifier.Amount));

            //Taxes
            var taxes = ArtsElement.LineItemProductTaxExtract();
            changeInControlTotal += taxes.Sum(productTax => Math.Abs(productTax.TaxAmount));

            //Deposits
            var deposits = ArtsElement.LineItemDepositsExtract();
            changeInControlTotal += deposits.Sum(deposit => Math.Abs(deposit.StoredValueAmount));

            //Tender rounding
            var tenderLineItem = (LineItemTender)ArtsElement.LineItems.Find(li => li is LineItemTender);
            changeInControlTotal += Math.Abs(tenderLineItem.RoundingAmount);

            //Save the final values
            ArtsElement.ControlTotalAfterTransaction += changeInControlTotal;

            return changeInControlTotal;
        }
    }
}