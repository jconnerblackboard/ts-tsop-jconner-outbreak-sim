﻿using System;
using System.Collections.Generic;
using System.Linq;
using BbTS.Domain.Models.ArtsDataModel;
using BbTS.Domain.Models.Container;
using BbTS.Domain.Models.Container.Elements;
using BbTS.Domain.Models.Transaction.Validation;
using BbTS.Domain.Models.Definitions.Transaction;
using BbTS.Domain.Models.Exceptions.Transaction;
using BbTS.Domain.Models.Transaction;

namespace BbTS.Core.Transaction
{
    /// <summary>
    /// Utility class for <see cref="TransactionViewV01"/> validation.
    /// </summary>
    public class TransactionUtility
    {
        /// <summary>
        /// Build a user friendly message from a list of transaction validation results.
        /// </summary>
        /// <param name="results">The transaction validation results.</param>
        /// <returns>User friendly message.</returns>
        public static string BuildTransactionValidationUserMessage(List<TransactionValidationResult> results)
        {
            var query = results
            .Select(x => x.RequestId)
            .GroupBy(x => x, (y, z) => new { Name = y, Count = z.Count() });

            string message = string.Empty;

            foreach (var item in query)
            {
                var resultsForRequest = results.FindAll(p => p.RequestId == item.Name).ToList();
                message = $"{message}Request '{item.Name}':";
                foreach (var subresult in resultsForRequest)
                {
                    message = $"{message} {subresult.CreateString()},";
                }
                message = message.TrimEnd(',');
                message += " ";
            }
            return message;
        }

        /// <summary>
        /// Build a user friendly message from a list of transaction validation results.
        /// </summary>
        /// <param name="results">The transaction validation results.</param>
        /// <returns>User friendly message.</returns>
        public static string BuildTransactionValidationFailUserMessage(List<TransactionValidationResult> results)
        {
            var query = results
            .Select(x => x.RequestId)
            .GroupBy(x => x, (y, z) => new { Name = y, Count = z.Count() });

            string message = string.Empty;

            foreach (var item in query)
            {
                var resultsForRequest = results.FindAll(p => p.RequestId == item.Name && p.Result == TransactionValidationResultEnumeration.Fail).ToList();
                message = $"{message}Request '{item.Name}':";
                foreach (var subresult in resultsForRequest)
                {
                    message = $"{message} {subresult.CreateString()},";
                }
                message = message.TrimEnd(',');
                message += " ";
            }
            return message;
        }

        /// <summary>
        /// Function to extract customer transaction information from a transaction object.
        /// </summary>
        /// <param name="artsElement">The transaction object.</param>
        /// <param name="transactionDateTime">The date and time the transaction occured.</param>
        /// <returns>Extracted customer information.</returns>
        public static List<CustomerTransactionInfoValidationRequest> CustomerTransactionInfoExtract(ArtsElementViewV01 artsElement, DateTime transactionDateTime)
        {
            var list = new List<CustomerTransactionInfoValidationRequest>();

            // Look for stored value transactions.
            {
                var lineItemTenderWithCustomerList = artsElement.LineItemTenders.FindAll(item => item.TenderType == TenderType.StoredValue);
                foreach (var item in lineItemTenderWithCustomerList)
                {
                    if (item.LineItemTenderWithCustomer?.CustomerCredential?.CustomerCardInfo == null)
                    {
                        var message = $"Tender type {item.TenderType} specified but line item stored value or customer information was not provided.";
                        throw new TransactionValidationException(
                            new TransactionValidationResult("", TransactionFunctionDefinition.CustomerTransactionInfoValidate,
                            message, TransactionValidationResultEnumeration.Fail), 
                            message);
                    }
                    list.Add(
                        new CustomerTransactionInfoValidationRequest
                        {
                            CardNumber = item.LineItemTenderWithCustomer.CustomerCredential.CustomerCardInfo.CardNumber,
                            IssueNumber = item.LineItemTenderWithCustomer.CustomerCredential.CustomerCardInfo.IssueNumber,
                            IssueNumberCaptured = item.LineItemTenderWithCustomer.CustomerCredential.CustomerCardInfo.IssueNumberCaptured,
                            TransactionDateTime = transactionDateTime,
                            CustomerGuid = item.LineItemTenderWithCustomer.CustomerCredential.CustomerGuid?.ToString("D")
                        });
                }
            }

            // Look for cash equiv functions
            {
                var lineItemTenderWithCustomerList = artsElement.LineItemTenders.FindAll(item => item.TenderType == TenderType.BoardCashEquiv);
                foreach (var item in lineItemTenderWithCustomerList)
                {
                    if (item.LineItemTenderCashEquivalence?.CustomerCredential?.CustomerCardInfo == null)
                    {
                        var message = $"Tender type {item.TenderType} specified but line item board cash equiv or customer information was not provided.";
                        throw new TransactionValidationException(
                            new TransactionValidationResult("", TransactionFunctionDefinition.CustomerTransactionInfoValidate,
                            message, TransactionValidationResultEnumeration.Fail),
                            message);
                    }

                    list.Add(
                        new CustomerTransactionInfoValidationRequest
                        {
                            CardNumber = item.LineItemTenderCashEquivalence.CustomerCredential.CustomerCardInfo.CardNumber,
                            IssueNumber = item.LineItemTenderCashEquivalence.CustomerCredential.CustomerCardInfo.IssueNumber,
                            IssueNumberCaptured = item.LineItemTenderCashEquivalence.CustomerCredential.CustomerCardInfo.IssueNumberCaptured,
                            TransactionDateTime = transactionDateTime,
                            CustomerGuid = item.LineItemTenderCashEquivalence.CustomerCredential.CustomerGuid?.ToString("D")
                        });
                }
            }

            return list;
        }

        /// <summary>
        /// Extract all line item products from a transaction.
        /// </summary>
        /// <param name="transaction">The transaction object.</param>
        /// <returns>The list of line item products.</returns>
        public static List<LineItemProductViewV01> LineItemProductsExtract(TransactionViewV01 transaction)
        {
            var list = new List<LineItemProductViewV01>();
            foreach (var artsElement in transaction.ArtsTransactions)
            {
                var lineItems = artsElement.LineItemProducts;
                list.AddRange(lineItems);
            }

            return list;
        }

        /// <summary>
        /// Extract all line item product price modifier elements from a transaction.
        /// </summary>
        /// <param name="transaction">The transaction object.</param>
        /// <returns>The list of line item products.</returns>
        public static List<LineItemProductPriceModifierViewV01> LineItemProductPriceModifierExtract(TransactionViewV01 transaction)
        {
            var list = new List<LineItemProductPriceModifierViewV01>();
            foreach (var artsElement in transaction.ArtsTransactions)
            {
                var lineItems = artsElement.LineItemProductPriceModifiers;
                list.AddRange(lineItems);
            }

            return list;
        }

        /// <summary>
        /// Extract all line item product tax elements from a transaction.
        /// </summary>
        /// <param name="transaction">The transaction object.</param>
        /// <returns>The list of line item products.</returns>
        public static List<LineItemProductTaxViewV01> LineItemProductTaxExtract(TransactionViewV01 transaction)
        {
            var list = new List<LineItemProductTaxViewV01>();
            foreach (var artsElement in transaction.ArtsTransactions)
            {
                var lineItems = artsElement.LineItemProductTaxes;
                list.AddRange(lineItems);
            }

            return list;
        }

        /// <summary>
        /// Extract all line item product tax override elements from a transaction.
        /// </summary>
        /// <param name="transaction">The transaction object.</param>
        /// <returns>The list of line item products.</returns>
        public static List<LineItemProductTaxOverrideViewV01> LineItemProductTaxOverrideExtract(TransactionViewV01 transaction)
        {
            var list = new List<LineItemProductTaxOverrideViewV01>();
            foreach (var artsElement in transaction.ArtsTransactions)
            {
                var lineItems = artsElement.LineItemProductTaxOverrides;
                list.AddRange(lineItems);
            }

            return list;
        }

        /// <summary>
        /// Extract all line item product promos from a transaction.
        /// </summary>
        /// <param name="transaction">The transaction object.</param>
        /// <returns>The list of line item products.</returns>
        public static List<LineItemProductPromotionViewV01> LineItemProductPromosExtract(TransactionViewV01 transaction)
        {
            var list = new List<LineItemProductPromotionViewV01>();
            var artsElements = transaction.ArtsTransactions;
            foreach (var artsElement in artsElements)
            {
                var lineItems = artsElement.LineItemProductPromos;
                list.AddRange(lineItems);
            }

            return list;
        }

        /// <summary>
        /// Extract all line item tenders from a transaction.
        /// </summary>
        /// <param name="transaction">The transaction object.</param>
        /// <returns>The list of line item products.</returns>
        public static List<LineItemTenderViewV01> LineItemTendersExtract(TransactionViewV01 transaction)
        {
            var list = new List<LineItemTenderViewV01>();
            foreach (var artsElement in transaction.ArtsTransactions)
            {
                var lineItems = artsElement.LineItemTenders;
                foreach (var item in lineItems)
                {
                    list.Add(item);
                }
            }

            return list;
        }

        /// <summary>
        /// Extract all line item tenders with customer information from a transaction.
        /// </summary>
        /// <param name="transaction">The transaction object.</param>
        /// <returns>The list of line item products.</returns>
        public static List<LineItemTenderStoredValueViewV01> LineItemTenderCustomersExtract(TransactionViewV01 transaction)
        {
            var list = new List<LineItemTenderStoredValueViewV01>();
            foreach (var artsElement in transaction.ArtsTransactions)
            {
                var lineItems = artsElement.LineItemTenders.FindAll(item => item.TenderType == TenderType.StoredValue);
                foreach (var item in lineItems)
                {
                    if (item.LineItemTenderWithCustomer?.CustomerCredential?.CustomerCardInfo == null)
                    {
                        var message = $"Tender type {item.TenderType} specified but line item tender stored value or associated customer information was not provided.";
                        throw new TransactionValidationException(
                            new TransactionValidationResult("", TransactionFunctionDefinition.CustomerTransactionInfoValidate,
                            message, TransactionValidationResultEnumeration.Fail),
                            message);
                    }
                    list.Add(item.LineItemTenderWithCustomer);
                }
            }

            return list;
        }

        /// <summary>
        /// Extract all arts transaction attributes from a transaction.
        /// </summary>
        /// <param name="transaction">The transaction object.</param>
        /// <returns>The list of line item products.</returns>
        public static List<RetailTransactionAttributesViewV01> ArtsTransactionAttributesExtract(TransactionViewV01 transaction)
        {
            var list = new List<RetailTransactionAttributesViewV01>();
            foreach (var artsElement in transaction.ArtsTransactions)
            {
                list.Add(new RetailTransactionAttributesViewV01
                {
                    AttendType = -1,
                    PeriodNumber = -1,
                    RetailTranType = (int)artsElement.TranType,
                    TransactionTypeCode = 0,
                    ValidationType = (int)artsElement.ValidationType
                });
            }
            return list;
        }
    }
}
