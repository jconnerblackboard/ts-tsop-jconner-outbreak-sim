﻿using System;
using System.Collections.Generic;
using System.Linq;
using BbTS.Domain.Models.ArtsDataModel;
using BbTS.Domain.Models.Container.Elements;
using BbTS.Domain.Models.Credential;
using BbTS.Domain.Models.Definitions.ArtsDataModel;
using BbTS.Domain.Models.Definitions.Container;
using BbTS.Domain.Models.Definitions.Transaction;
using BbTS.Domain.Models.Transaction.Financial;

namespace BbTS.Core.Transaction
{
    // ReSharper disable RedundantNameQualifier
    // ReSharper disable RedundantUsingDirective
    using BbTS.Domain.Models.Container;
    // ReSharper restore RedundantNameQualifier
    // ReSharper restore RedundantUsingDirective

    /// <summary>
    /// Extension methods for working with <see cref="Transaction"/> and <see cref="Element"/> instances.
    /// </summary>
    public static class TransactionTools
    {
        /// <summary>
        /// Returns the first occurance of a timestamp with the specified timestamp type.
        /// </summary>
        /// <param name="transaction"></param>
        /// <param name="timestampType"></param>
        /// <returns></returns>
        public static TransactionTimestamp TimestampExtract(this Transaction transaction, TimestampType timestampType)
        {
            return transaction?.TransactionTimestamps.FirstOrDefault(t => t.TimestampType == timestampType);
        }

        /// <summary>
        /// Returns the first occurance of a transaction number in the transaction, if present.
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static int? TransactionNumberExtract(this Transaction transaction)
        {
            if (transaction == null) return null;

            foreach (var element in transaction.Elements)
            {
                var artsElement = element as ArtsTransactionElement;
                if (artsElement != null) return artsElement.TransactionNumber;

                var attendanceElement = element as AttendanceElement;
                if (attendanceElement != null) return attendanceElement.TransactionNumber;

                var boardElement = element as BoardElement;
                if (boardElement != null) return boardElement.TransactionNumber;

                var controlElement = element as ControlElement;
                if (controlElement != null) return controlElement.TransactionNumber;
            }

            return null;
        }

        /// <summary>
        /// Returns the first occurance of an attended event ID in the transaction, if present.
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static int? AttendedEventIdExtract(this Transaction transaction)
        {
            var attendanceElement = transaction?.Elements.FirstOrDefault(e => e is AttendanceElement) as AttendanceElement;
            return attendanceElement?.EventNumber;
        }

        /// <summary>
        /// Returns the first occurance of an attended event transaction type in the transaction, if present.
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static AttendedEventTranType? AttendedEventTransactionTypeExtract(this Transaction transaction)
        {
            var attendanceElement = transaction?.Elements.FirstOrDefault(e => e is AttendanceElement) as AttendanceElement;
            return attendanceElement?.TransactionType;
        }

        /// <summary>
        /// Returns the first occurance of a board meal type ID in the transaction, if present.
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static int? BoardMealTypeExtract(this Transaction transaction)
        {
            if (transaction == null) return null;

            foreach (var element in transaction.Elements)
            {
                var boardElement = element as BoardElement;
                if (boardElement != null) return boardElement.MealTypeId;

                var artsElement = element as ArtsTransactionElement;
                if (artsElement == null) continue;
                foreach (var lineItem in artsElement.LineItems)
                {
                    var lineItemTenderCashEquivalence = lineItem as LineItemTenderCashEquivalence;
                    if (lineItemTenderCashEquivalence != null) return lineItemTenderCashEquivalence.BoardMealTypeId;
                }
            }

            return null;
        }

        /// <summary>
        /// Returns the first occurance of an operator guid in the transaction, if present.
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static Guid? OperatorGuidExtract(this Transaction transaction)
        {
            var operatorElement = transaction?.OperatorElementExtract();
            return operatorElement?.OperatorGuid;
        }

        /// <summary>
        /// Returns the first occurance of a customer guid in the transaction, if present.
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static Guid? CustomerGuidExtract(this Transaction transaction)
        {
            if (transaction == null) return null;

            foreach (var element in transaction.Elements)
            {
                //Check arts element(s)
                var artsElement = element as ArtsTransactionElement;
                if (artsElement != null)
                {
                    foreach (var lineItem in artsElement.LineItems)
                    {
                        var lineItemTenderWithCustomer = lineItem as LineItemTenderWithCustomer;  //Stored value tender line items
                        if (lineItemTenderWithCustomer != null) return lineItemTenderWithCustomer.CustomerCredential.CustomerGuid;

                        var lineItemTenderCashEquivalence = lineItem as LineItemTenderCashEquivalence;  //Cash equivalence tender line item
                        if (lineItemTenderCashEquivalence != null) return lineItemTenderCashEquivalence.CustomerCredential.CustomerGuid;

                        var lineItemDeposit = lineItem as LineItemStoredValue;  //Deposit line item
                        if (lineItemDeposit != null) return lineItemDeposit.CustomerCredential.CustomerGuid;
                    }
                }

                //Check attendance element(s)
                var attendanceElement = element as AttendanceElement;
                if (attendanceElement?.CustomerCredential?.CustomerGuid != null)
                {
                    return attendanceElement.CustomerCredential.CustomerGuid;
                }

                //Check board elements(s)
                var boardElement = element as BoardElement;
                if (boardElement?.CustomerCredential?.CustomerGuid != null)
                {
                    return boardElement.CustomerCredential.CustomerGuid;
                }
            }

            return null;
        }

        /// <summary>
        /// Returns the first occurance of a customer card capture object in the transaction, if present.
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static CardCapture CustomerCardCaptureExtract(this Transaction transaction)
        {
            var credential = transaction?.CustomerCredentialExtract();
            return credential?.CustomerCardInfo;
        }

        /// <summary>
        /// Returns the total transaction amount, if present.
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static decimal? TransactionAmountExtract(this Transaction transaction)
        {
            if (transaction == null) return null;

            foreach (var element in transaction.Elements)
            {
                var artsElement = element as ArtsTransactionElement;
                if (artsElement == null) continue;

                return artsElement.LineItemTendersExtract().Sum(lineItemTender => lineItemTender.TenderAmount);
            }

            return null;
        }

        /// <summary>
        /// Returns the first encountered tender ID, if present.
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static int? TenderIdExtract(this Transaction transaction)
        {
            if (transaction == null) return null;

            foreach (var element in transaction.Elements)
            {
                var artsElement = element as ArtsTransactionElement;
                if (artsElement == null) continue;

                foreach (var lineItem in artsElement.LineItems)
                {
                    var lineItemTender = lineItem as LineItemTender;
                    if (lineItemTender != null) return lineItemTender.TenderId;
                }
            }

            return null;
        }

        /// <summary>
        /// Extract all line item tenders from a transaction.
        /// </summary>
        /// <param name="transaction">The transaction object.</param>
        /// <returns>The list of line item tenders.</returns>
        public static List<LineItemTender> LineItemTendersExtract(this Transaction transaction)
        {
            var list = new List<LineItemTender>();
            var artsElements = transaction.ArtsElementsExtract();
            foreach (var artsElement in artsElements)
            {
                var lineItemTenders = artsElement.LineItemTendersExtract();
                list.AddRange(lineItemTenders);
            }

            return list;
        }

        /// <summary>
        /// Attempts to determine the <see cref="TransactionClassification"/> for this <see cref="Transaction"/> instance.
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static TransactionClassification TransactionClassificationGet(this Transaction transaction)
        {
            var classification = TransactionClassification.Unknown;

            int elementCount = 0;
            foreach (var element in transaction.Elements)
            {
                if (element is ControlElement)
                {
                    classification = TransactionClassification.Control;
                    elementCount++;
                }
                else if (element is ArtsTransactionElement)
                {
                    classification = TransactionClassification.Arts;
                    elementCount++;
                }
                else if (element is AttendanceElement)
                {
                    classification = TransactionClassification.Attendance;
                    elementCount++;
                }
                else if (element is BoardElement)
                {
                    classification = TransactionClassification.Board;
                    elementCount++;
                }
            }

            return elementCount == 1 ? classification : TransactionClassification.Unknown;
        }

        /// <summary>
        /// Retrieves tender flags for the ARTS element.  (Split-tender compatible.)
        /// </summary>
        /// <param name="artsElement"></param>
        /// <returns></returns>
        public static TenderFlags TenderFlagsGet(this ArtsTransactionElement artsElement)
        {
            var tenderFlags = TenderFlags.None;
            var lineItemTenders = artsElement.LineItemTendersExtract();

            foreach (var lineItemTender in lineItemTenders)
            {
                var tenderType = lineItemTender.TenderTypeGet();

                switch (tenderType)
                {
                    case TenderType.Cash:
                        tenderFlags |= TenderFlags.Cash;
                        break;
                    case TenderType.Check:
                        tenderFlags |= TenderFlags.Check;
                        break;
                    case TenderType.StoredValue:
                        tenderFlags |= TenderFlags.StoredValue;
                        break;
                    case TenderType.CreditCard:
                        tenderFlags |= TenderFlags.CreditCard;
                        break;
                    case TenderType.BoardCashEquiv:
                        tenderFlags |= TenderFlags.CashEquivalence;
                        break;
                    default:
                        tenderFlags |= TenderFlags.None;
                        break;
                }
            }

            return tenderFlags;
        }

        /// <summary>
        /// Extracts the first occurance of customer credential information from a transaction, if present.
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static CustomerCredential CustomerCredentialExtract(this Transaction transaction)
        {
            if (transaction == null) return null;

            foreach (var element in transaction.Elements)
            {
                //Check attendance element
                var attendanceElement = element as AttendanceElement;
                if (attendanceElement?.CustomerCredential != null)
                {
                    return attendanceElement.CustomerCredential;
                }

                //Check board element
                var boardElement = element as BoardElement;
                if (boardElement?.CustomerCredential != null)
                {
                    return boardElement.CustomerCredential;
                }

                //Check arts element
                var artsElement = element as ArtsTransactionElement;
                if (artsElement == null) continue;

                foreach (var lineItem in artsElement.LineItems)
                {
                    //Check stored value
                    var lineItemTenderWithCustomer = lineItem as LineItemTenderWithCustomer;
                    if (lineItemTenderWithCustomer?.CustomerCredential != null)
                    {
                        return lineItemTenderWithCustomer.CustomerCredential;
                    }

                    //Check cash equivalence
                    var lineItemTenderCashEquivalence = lineItem as LineItemTenderCashEquivalence;
                    if (lineItemTenderCashEquivalence?.CustomerCredential != null)
                    {
                        return lineItemTenderCashEquivalence.CustomerCredential;
                    }

                    //Check deposit line item
                    var lineItemDeposit = lineItem as LineItemStoredValue;
                    if (lineItemDeposit?.CustomerCredential != null)
                    {
                        return lineItemDeposit.CustomerCredential;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Extracts customer credential information from an ARTS element.
        /// </summary>
        /// <param name="artsElement"></param>
        /// <returns></returns>
        public static List<CustomerCredential> CustomerCredentialExtract(this ArtsTransactionElement artsElement)
        {
            var credentialList = new List<CustomerCredential>();

            //Stored value tender
            credentialList.AddRange(artsElement.LineItemTenderStoredValueExtract().Select(lineItem => lineItem.CustomerCredential));
            //Cash equivalence tender
            credentialList.AddRange(artsElement.LineItemTenderCashEquivalenceExtract().Select(lineItem => lineItem.CustomerCredential));
            //Stored value deposits
            credentialList.AddRange(artsElement.LineItemDepositsExtract().Select(lineItem => lineItem.CustomerCredential));

            return credentialList;
        }

        /// <summary>
        /// Extracts customer credential information from an attendance element.
        /// </summary>
        /// <param name="attendanceElement"></param>
        /// <returns></returns>
        public static CustomerCredential CustomerCredentialExtract(this AttendanceElement attendanceElement)
        {
            return attendanceElement.CustomerCredential;
        }

        /// <summary>
        /// Extracts customer credential information from a board element.
        /// </summary>
        /// <param name="boardElement"></param>
        /// <returns></returns>
        public static CustomerCredential CustomerCredentialExtract(this BoardElement boardElement)
        {
            return boardElement.CustomerCredential;
        }

        /// <summary>
        /// Extracts the <see cref="OriginatorElement"/> instance from a <see cref="Transaction"/> instance, if present.
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static OriginatorElement OriginatorElementExtract(this Transaction transaction)
        {
            return transaction.Elements.FirstOrDefault(e => e is OriginatorElement) as OriginatorElement;
        }

        /// <summary>
        /// Extracts the <see cref="OperatorElement"/> instance from a <see cref="Transaction"/> instance, if present.
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static OperatorElement OperatorElementExtract(this Transaction transaction)
        {
            return transaction.Elements.FirstOrDefault(e => e is OperatorElement) as OperatorElement;
        }

        /// <summary>
        /// Extracts the <see cref="ControlElement"/> instance from a <see cref="Transaction"/> instance, if present.
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static ControlElement ControlElementExtract(this Transaction transaction)
        {
            return transaction.Elements.FirstOrDefault(e => e is ControlElement) as ControlElement;
        }

        /// <summary>
        /// Extracts all <see cref="ArtsTransactionElement"/> instances from a <see cref="Transaction"/> instance.
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static List<ArtsTransactionElement> ArtsElementsExtract(this Transaction transaction)
        {
            return transaction.Elements.OfType<ArtsTransactionElement>().ToList();
        }

        /// <summary>
        /// Extracts all <see cref="AttendanceElement"/> instances from a <see cref="Transaction"/> instance.
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static List<AttendanceElement> AttendanceElementsExtract(this Transaction transaction)
        {
            return transaction.Elements.OfType<AttendanceElement>().ToList();
        }

        /// <summary>
        /// Extracts all <see cref="BoardElement"/> instances from a <see cref="Transaction"/> instance.
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static List<BoardElement> BoardElementsExtract(this Transaction transaction)
        {
            return transaction.Elements.OfType<BoardElement>().ToList();
        }

        /// <summary>
        /// Extract all line item tenders from an ARTS element.
        /// </summary>
        /// <param name="artsElement">The arts element object.</param>
        /// <returns>The list of line item products.</returns>
        public static List<LineItemTender> LineItemTendersExtract(this ArtsTransactionElement artsElement)
        {
            return artsElement.LineItems.OfType<LineItemTender>().ToList();
        }

        /// <summary>
        /// Extract all line item tender discounts from an ARTS element.
        /// </summary>
        /// <param name="artsElement"></param>
        /// <returns></returns>
        public static List<LineItemTenderDiscount> LineItemTenderDiscountsExtract(this ArtsTransactionElement artsElement)
        {
            return artsElement.LineItems.OfType<LineItemTenderDiscount>().ToList();
        }

        /// <summary>
        /// Extract all line item tender surcharges from an ARTS element.
        /// </summary>
        /// <param name="artsElement"></param>
        /// <returns></returns>
        public static List<LineItemTenderSurcharge> LineItemTenderSurchargesExtract(this ArtsTransactionElement artsElement)
        {
            return artsElement.LineItems.OfType<LineItemTenderSurcharge>().ToList();
        }

        /// <summary>
        /// Calculates and returns the total amount required to pay for the transaction, including all discounts, surcharges, taxes, and rounding.
        /// </summary>
        /// <param name="artsElement"></param>
        /// <param name="tender"></param>
        /// <param name="roundingAmount"></param>
        /// <returns></returns>
        public static decimal CalculateTransactionTotal(this ArtsTransactionElement artsElement, Tender tender, out decimal roundingAmount)
        {
            roundingAmount = 0.00m;

            decimal transactionTotal = 0.00m;
            transactionTotal += artsElement.ModifiedPriceSubtotal();
            transactionTotal += artsElement.TaxSubtotal();
            transactionTotal += artsElement.DepositSubtotal();
            if (tender.RoundingIncrement <= 0.01m) return transactionTotal; //RoundingIncrement must always be >= $0.01, but we only need to take special action if it is > $0.01.

            decimal roundedAmount = new RoundedDecimal(transactionTotal / tender.RoundingIncrement, 0).RoundedValue * tender.RoundingIncrement;
            roundingAmount = roundedAmount - transactionTotal;  //the amount that was added or subtracted to arrive at the rounded amount (positive = added / net = subtracted)
            return roundedAmount;
        }

        /// <summary>
        /// Returns a subtotal that equals the sum of the prices of all products in the ARTS element.
        /// </summary>
        /// <param name="artsElement"></param>
        /// <returns>Pre-tax subtotal</returns>
        public static decimal ModifiedPriceSubtotal(this ArtsTransactionElement artsElement)
        {
            var productLineItems = artsElement.LineItemProductsExtract();
            return productLineItems.Sum(productLineItem => productLineItem.ExtendedAmount);
        }

        /// <summary>
        /// Returns the price of a product, including any modifiers if present.
        /// </summary>
        /// <param name="productLineItem"></param>
        /// <param name="priceModifiers"></param>
        /// <returns></returns>
        public static decimal GetModifiedPrice(this LineItemProduct productLineItem, List<LineItemProductPriceModifier> priceModifiers)
        {
            //If no price modifiers, just return the product price
            if (!productLineItem.RetailPriceModifiedFlag) return productLineItem.RetailPrice;

            //Otherwise, return the most recent price modifier price (denoted by sequence)
            decimal price = 0.00m;
            int i = -1;
            foreach (var priceModifier in priceModifiers)
            {
                if (priceModifier.PriceModifierSequenceNumber <= i) continue;
                i = priceModifier.PriceModifierSequenceNumber;
                price = priceModifier.NewUnitPrice;
            }

            return price;
        }

        /// <summary>
        /// Returns a subtotal that equals the sum of all taxes in the ARTS element.
        /// </summary>
        /// <param name="artsTransactionElement"></param>
        /// <returns>Total tax amount</returns>
        public static decimal TaxSubtotal(this ArtsTransactionElement artsTransactionElement)
        {
            var taxes = artsTransactionElement.LineItemProductTaxExtract();
            return taxes.Sum(tax => tax.TaxAmount);
        }

        /// <summary>
        /// Returns a subtotal that equals the sum of all non-enrichment deposits in the ARTS element.
        /// </summary>
        /// <param name="artsTransactionElement"></param>
        /// <returns>Total tax amount</returns>
        public static decimal DepositSubtotal(this ArtsTransactionElement artsTransactionElement)
        {
            var deposits = artsTransactionElement.LineItemDepositsExtract();
            return deposits.Where(x => !x.EnrichmentFlag).Sum(x => x.StoredValueAmount);
        }

        /// <summary>
        /// Extract a list of line item products from the ARTS element.
        /// </summary>
        /// <param name="artsElement">The arts transaction.</param>
        /// <returns>List of line item products.</returns>
        public static List<LineItemProduct> LineItemProductsExtract(this ArtsTransactionElement artsElement)
        {
            return artsElement.LineItems.OfType<LineItemProduct>().ToList();
        }

        /// <summary>
        /// Extract a list of line item product taxes from the ARTS element.
        /// </summary>
        /// <param name="artsElement">The arts transaction.</param>
        /// <returns>The list of <see cref="LineItemProductTax"/>.</returns>
        public static List<LineItemProductTax> LineItemProductTaxExtract(this ArtsTransactionElement artsElement)
        {
            return artsElement.LineItems.OfType<LineItemProductTax>().ToList();
        }

        /// <summary>
        /// Extract a list of line item product tax exemptions from the ARTS element.
        /// </summary>
        /// <param name="artsElement">The arts transaction.</param>
        /// <returns>The list of <see cref="LineItemProductTaxExempt"/>.</returns>
        public static List<LineItemProductTaxExempt> LineItemProductTaxExemptExtract(this ArtsTransactionElement artsElement)
        {
            return artsElement.LineItems.OfType<LineItemProductTaxExempt>().ToList();
        }

        /// <summary>
        /// Extract a list of line item product tax overrides from the ARTS element.
        /// </summary>
        /// <param name="artsElement">The arts transaction.</param>
        /// <returns>The list of <see cref="LineItemProductTaxOverride"/>.</returns>
        public static List<LineItemProductTaxOverride> LineItemProductTaxOverridesExtract(this ArtsTransactionElement artsElement)
        {
            return artsElement.LineItems.OfType<LineItemProductTaxOverride>().ToList();
        }

        /// <summary>
        /// Extract a list of line item product price modifiers from the ARTS element.
        /// </summary>
        /// <param name="artsElement">The arts transaction.</param>
        /// <returns>The list of <see cref="LineItemProductPriceModifier"/>.</returns>
        public static List<LineItemProductPriceModifier> LineItemProductPriceModifierExtract(this ArtsTransactionElement artsElement)
        {
            return artsElement.LineItems.OfType<LineItemProductPriceModifier>().ToList();
        }

        /// <summary>
        /// Extract a list of line item product promos from the ARTS element.
        /// </summary>
        /// <param name="artsElement">The arts transaction.</param>
        /// <returns>The list of <see cref="LineItemProductPromotion"/>.</returns>
        public static List<LineItemProductPromotion> LineItemProductPromosExtract(this ArtsTransactionElement artsElement)
        {
            return artsElement.LineItems.OfType<LineItemProductPromotion>().ToList();
        }

        /// <summary>
        /// Extract a list of line item tender cash equiv from the ARTS element.
        /// </summary>
        /// <param name="artsElement">The arts transaction.</param>
        /// <returns>The list of <see cref="LineItemTenderCashEquivalence"/>.</returns>
        public static List<LineItemTenderCashEquivalence> LineItemTenderCashEquivalenceExtract(this ArtsTransactionElement artsElement)
        {
            return artsElement.LineItems.OfType<LineItemTenderCashEquivalence>().ToList();
        }

        /// <summary>
        /// Extract a list of line item tender cash from the ARTS element.
        /// </summary>
        /// <param name="artsElement">The arts transaction.</param>
        /// <returns>The list of <see cref="LineItemTenderCash"/>.</returns>
        public static List<LineItemTenderCash> LineItemTenderCashExtract(this ArtsTransactionElement artsElement)
        {
            return artsElement.LineItems.OfType<LineItemTenderCash>().ToList();
        }

        /// <summary>
        /// Extract a list of line item tender check from the ARTS element.
        /// </summary>
        /// <param name="artsElement">The arts transaction.</param>
        /// <returns>The list of <see cref="LineItemTenderCheck"/>.</returns>
        public static List<LineItemTenderCheck> LineItemTenderCheckExtract(this ArtsTransactionElement artsElement)
        {
            return artsElement.LineItems.OfType<LineItemTenderCheck>().ToList();
        }

        /// <summary>
        /// Extract a list of line item tender stored value from the ARTS element.
        /// </summary>
        /// <param name="artsElement">The arts transaction.</param>
        /// <returns>The list of <see cref="LineItemTenderWithCustomer"/>.</returns>
        public static List<LineItemTenderWithCustomer> LineItemTenderStoredValueExtract(this ArtsTransactionElement artsElement)
        {
            return artsElement.LineItems.OfType<LineItemTenderWithCustomer>().ToList();
        }

        /// <summary>
        /// Extract a list of line item tender emv from the ARTS element.
        /// </summary>
        /// <param name="artsElement">The arts transaction.</param>
        /// <returns>The list of <see cref="LineItemTenderEmv"/>.</returns>
        public static List<LineItemTenderEmv> LineItemTenderEmvExtract(this ArtsTransactionElement artsElement)
        {
            return artsElement.LineItems.OfType<LineItemTenderEmv>().ToList();
        }

        /// <summary>
        /// Extract a list of line item tender stored value from the ARTS element.
        /// </summary>
        /// <param name="artsElement">The arts transaction.</param>
        /// <returns>The list of <see cref="LineItemTenderWithCustomer"/>.</returns>
        public static List<LineItemTenderWithCustomer> LineItemTenderWithCustomerExtract(this ArtsTransactionElement artsElement)
        {
            return artsElement.LineItems.OfType<LineItemTenderWithCustomer>().ToList();
        }

        /// <summary>
        /// Extract a list of line item deposits from the ARTS element.
        /// </summary>
        /// <param name="artsElement"></param>
        /// <returns></returns>
        public static List<LineItemStoredValue> LineItemDepositsExtract(this ArtsTransactionElement artsElement)
        {
            return artsElement.LineItems.OfType<LineItemStoredValue>().ToList();
        }

        /// <summary>
        /// Extract a list of line item enrichments from the ARTS element.
        /// </summary>
        /// <param name="artsElement"></param>
        /// <returns></returns>
        public static List<LineItemStoredValueEnrichment> LineItemEnrichmentsExtract(this ArtsTransactionElement artsElement)
        {
            return artsElement.LineItems.OfType<LineItemStoredValueEnrichment>().ToList();
        }

        /// <summary>
        /// The count of (non-voided) products that are in the ARTS element.
        /// </summary>
        /// <param name="artsElement"></param>
        /// <returns>Count of non-voided products.</returns>
        public static int ProductCountGet(this ArtsTransactionElement artsElement)
        {
            var products = artsElement.LineItemProductsExtract();
            var nonVoidedProducts = products.FindAll(product => product.VoidFlag == false);
            return nonVoidedProducts.Count;
        }

        /// <summary>
        /// The count of (non-voided) products in the list.
        /// </summary>
        /// <param name="products">List of line item products.</param>
        /// <returns>Count of non-voided products.</returns>
        public static int ProductCountGet(this List<LineItemProduct> products)
        {
            var nonVoidedProducts = products.FindAll(product => product.VoidFlag == false);
            return nonVoidedProducts.Count;
        }

        /// <summary>
        /// The the count of products that were keyed.
        /// </summary>
        /// <param name="artsElement">The arts element containing the products.</param>
        /// <returns>Count of products that were keyed.</returns>
        public static int KeyedCountGet(this ArtsTransactionElement artsElement)
        {
            var products = artsElement.LineItemProductsExtract();
            var keyed = products.FindAll(item => item.VoidFlag == false && item.ProductEntryMethodType == ProductEntryMethodType.Keyed);
            return keyed.Count;
        }

        /// <summary>
        /// The the count of products that were keyed.
        /// </summary>
        /// <param name="products">List of line item products.</param>
        /// <returns>Count of products that were keyed.</returns>
        public static int KeyedCountGet(this List<LineItemProduct> products)
        {
            var keyed = products.FindAll(item => item.VoidFlag == false && item.ProductEntryMethodType == ProductEntryMethodType.Keyed);
            return keyed.Count;
        }

        /// <summary>
        /// Get a count of products that were scanned.
        /// </summary>
        /// <param name="artsElement">The arts element containing the products.</param>
        /// <returns>Count of products that were scanned.</returns>
        public static int ScannedCountGet(this ArtsTransactionElement artsElement)
        {
            var products = artsElement.LineItemProductsExtract();
            var scanned = products.FindAll(item => item.VoidFlag == false && item.ProductEntryMethodType == ProductEntryMethodType.Scanned);
            return scanned.Count;
        }

        /// <summary>
        /// Get a count of products that were scanned.
        /// </summary>
        /// <param name="products">List of line item products.</param>
        /// <returns>Count of products that were scanned.</returns>
        public static int ScannedCountGet(this List<LineItemProduct> products)
        {
            var scanned = products.FindAll(item => item.VoidFlag == false && item.ProductEntryMethodType == ProductEntryMethodType.Scanned);
            return scanned.Count;
        }

        /// <summary>
        /// Get the count of all units processed in the ARTS element.
        /// </summary>
        /// <param name="artsElement">The arts element containing the products.</param>
        /// <returns>The count of all units processed in the arts transaction.</returns>
        public static int UnitCountGet(this ArtsTransactionElement artsElement)
        {
            var count = 0;
            var products = artsElement.LineItemProductsExtract();
            products.FindAll(item => item.VoidFlag == false).ForEach(p => count += p.ActualUnitPriceQuantity);
            return count;
        }

        /// <summary>
        /// Get the count of all units processed in the ARTS element.
        /// </summary>
        /// <param name="products">List of line item products.</param>
        /// <returns>The count of all units processed in the arts transaction.</returns>
        public static int UnitCountGet(this List<LineItemProduct> products)
        {
            var count = 0;
            products.FindAll(item => item.VoidFlag == false).ForEach(p => count += p.ActualUnitPriceQuantity);
            return count;
        }
    }
}
