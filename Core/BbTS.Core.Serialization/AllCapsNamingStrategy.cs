﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Serialization;

namespace BbTS.Core.Serialization
{
    public class AllCapsNamingStrategy : NamingStrategy
    {
        protected override string ResolvePropertyName(string name )
        {
            return name.ToUpperInvariant();
        }
    }
}
