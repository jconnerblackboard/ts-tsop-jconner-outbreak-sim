﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BbTS.Core.Serialization
{
    public class NewtonsoftJson
    {
        /// <summary>
        /// Serialize an object to a json string
        /// </summary>
        /// <typeparam name="T">DataContract class</typeparam>
        /// <param name="value">the object to serialize</param>
        /// <param name="handling"><see cref="TypeNameHandling"/> object to pass to settings</param>
        /// <returns>serialized json string representing the object</returns>
        public static string Serialize<T>(T value, TypeNameHandling handling)
        {
            var settings = new JsonSerializerSettings { TypeNameHandling = handling };
            return Serialize(value, settings);
        }

        /// <summary>
        /// Serialize an object to a json string
        /// </summary>
        /// <typeparam name="T">DataContract class</typeparam>
        /// <param name="value">the object to serialize</param>
        /// <param name="settings"><see cref="JsonSerializerSettings"/> object with the desired settings that will be passed to the serialize</param>
        /// <returns>serialized json string representing the object</returns>
        public static string Serialize<T>(T value, JsonSerializerSettings settings = null)
        {
            JsonSerializer serializer = new JsonSerializer();
            serializer.Converters.Add(new JavaScriptDateTimeConverter());
            serializer.NullValueHandling = NullValueHandling.Ignore;

            return settings == null ? JsonConvert.SerializeObject(value) : JsonConvert.SerializeObject(value, settings);
        }

        /// <summary>
        /// Deserialize an object from a json string
        /// </summary>
        /// <typeparam name="T">DataContract class</typeparam>
        /// <param name="json">serialized json string representing the object</param>
        /// <param name="handling"><see cref="TypeNameHandling"/> object to pass to settings</param>
        /// <returns>deserialized object</returns>
        public static T Deserialize<T>(string json, TypeNameHandling handling)
        {
            var settings = new JsonSerializerSettings { TypeNameHandling = handling };
            return Deserialize<T>(json, settings);
        }

        /// <summary>
        /// Deserialize an object from a json string
        /// </summary>
        /// <typeparam name="T">DataContract class</typeparam>
        /// <param name="json">serialized json string representing the object</param>
        /// <param name="settings"><see cref="JsonSerializerSettings"/> object with the desired settings that will be passed to the serialize</param>
        /// <returns>deserialized object</returns>
        public static T Deserialize<T>(string json, JsonSerializerSettings settings = null)
        {
            return settings == null ? JsonConvert.DeserializeObject<T>(json) : JsonConvert.DeserializeObject<T>(json, settings);
        }

        /// <summary>
        /// Try to deserialize the json object from a string
        /// </summary>
        /// <typeparam name="T">DataContract class</typeparam>
        /// <param name="json">serialized json string representing the object</param>
        /// <param name="deserializedObject">deserialized object</param>
        /// <param name="handling"><see cref="TypeNameHandling"/> object to pass to settings</param>
        /// <returns>operation success (true/false)</returns>
        public static bool TryDeserialize<T>(string json, out T deserializedObject, TypeNameHandling handling)
        {
            var settings = new JsonSerializerSettings { TypeNameHandling = handling };
            return TryDeserialize(json, out deserializedObject, settings);
        }

        /// <summary>
        /// Try to deserialize the json object from a string
        /// </summary>
        /// <typeparam name="T">DataContract class</typeparam>
        /// <param name="json">serialized json string representing the object</param>
        /// <param name="deserializedObject">deserialized object</param>
        /// <param name="settings"><see cref="JsonSerializerSettings"/> object with the desired settings that will be passed to the serialize</param>
        /// <returns>operation success (true/false)</returns>
        public static bool TryDeserialize<T>(string json, out T deserializedObject, JsonSerializerSettings settings = null)
        {
            try
            {
                deserializedObject = settings == null ? JsonConvert.DeserializeObject<T>(json) : JsonConvert.DeserializeObject<T>(json, settings);
                return true;
            }
            catch
            {
                deserializedObject = default(T);
                return false;
            }
        }
    }
}
