﻿using System;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;

namespace BbTS.Core.Serialization
{
    public class Json
    {
        /// <summary>
        /// Serialize an object to a json string
        /// </summary>
        /// <typeparam name="T">DataContract class</typeparam>
        /// <param name="value">the object to serialize</param>
        /// <returns>serialized json string representing the object</returns>
        public static String Serialize<T>(T value)
        {
            MemoryStream stream = new MemoryStream();
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
            serializer.WriteObject(stream, value);
            stream.Position = 0;
            StreamReader reader = new StreamReader(stream);
            String json = reader.ReadToEnd();
            return json;
        }

        /// <summary>
        /// Deserialize an object from a json string
        /// </summary>
        /// <typeparam name="T">DataContract class</typeparam>
        /// <param name="json">serialized json string representing the object</param>
        /// <returns>deserialized object</returns>
        public static T Deserialize<T>(string json)
        {
            var instance = Activator.CreateInstance<T>();
            using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(json)))
            {
                var serializer = new DataContractJsonSerializer(instance.GetType());
                return (T)serializer.ReadObject(ms);
            }
        }

        /// <summary>
        /// Try to deserialize the json object from a string
        /// </summary>
        /// <typeparam name="T">DataContract class</typeparam>
        /// <param name="json">serialized json string representing the object</param>
        /// <param name="deserializedObject">deserialized object</param>
        /// <returns>operation success (true/false)</returns>
        public static bool TryDeserialize<T>(string json, ref T deserializedObject)
        {
            try
            {
                deserializedObject = Deserialize<T>(json);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}