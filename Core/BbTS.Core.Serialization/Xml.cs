﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace BbTS.Core.Serialization
{
    public class Xml
    {

        private static Xml _instance = new Xml();

        public static Xml Instance
        {
            get { return _instance; }
            internal set { _instance = value; }
        }

        /// <summary>
        /// This method will deserialze a string of Xml to an object
        /// </summary>
        /// <typeparam name="T">The TypeOf object being dealt with</typeparam>
        /// <param name="xml">The string of Xml characters being deserialized</param>
        /// <returns>An Object of T</returns>
        public static T Deserialize<T>(String xml)
        {
            using (MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(xml)))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof (T));
                StreamReader reader = new StreamReader(memoryStream, Encoding.UTF8);
                return (T)xmlSerializer.Deserialize(reader);
            }
        }

        /// <summary>
        /// This method will serialize an object to string
        /// </summary>
        /// <typeparam name="T">The TypeOf object being dealt with</typeparam>
        /// <param name="value">The object being serialized</param>
        /// <param name="makePretty"></param>
        /// <returns>A string of xml representing the passed in object</returns>
        public static String Serialize<T>(T value, bool makePretty = false)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");

            XmlWriterSettings settings = new XmlWriterSettings
            {
                OmitXmlDeclaration = true,
                Encoding = new UnicodeEncoding(false, false),
                Indent = makePretty,
                NewLineOnAttributes = makePretty
            };

            using (StringWriter stringWriter = new StringWriter())
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(stringWriter, settings))
                {
                    serializer.Serialize(xmlWriter, value, ns);
                }
                return stringWriter.ToString();
            }
        }

        /// <summary>
        /// This method will serialize an object to disk
        /// </summary>
        /// <typeparam name="T">The TypeOf object being dealt with</typeparam>
        /// <param name="value">The object being serialized</param>
        /// <param name="directory">The directory the file will be saved to</param>
        /// <param name="fileName">The file name for the serialized object</param>
        public static void SerializeToDisk<T>(T value, string directory, string fileName)
        {
            if (value == null) { return; }

            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            File.WriteAllText($"{directory}\\{fileName}", Serialize(value));
        }

        /// <summary>
        /// This method will try deserialze a string of Xml to an object
        /// </summary>
        /// <typeparam name="T">The TypeOf object being dealt with</typeparam>
        /// <param name="xml">The string of Xml characters being deserialized</param>
        /// <param name="deserializedObject">The deserialized object</param>
        /// <returns>An Object of T</returns>
        public static bool TryDeserialize<T>(String xml, ref T deserializedObject)
        {
            try
            {
                deserializedObject = Deserialize<T>(xml);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Format the string for Xml style
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static String FormatXml(String xml)
        {
            string formattedMessage = "";
            int tabCount = 0;
            bool lastSawClosingTag = false;
            for (int i = 0; i < xml.Length; ++i)
            {

                switch (xml[i])
                {
                    case '>':
                    {
                        formattedMessage += xml[i];
                        if (i < xml.Length - 1 &&
                            xml[i + 1] == '<')
                        {
                            formattedMessage += "\r\n";
                        }
                    }
                        break;
                    case '<':
                    {
                        if (i < xml.Length - 1 &&
                            xml[i + 1] == '/')
                        {
                            tabCount--;
                            if (lastSawClosingTag)
                            {
                                for (int currtab = 0; currtab < tabCount; currtab++)
                                {
                                    formattedMessage += "\t";
                                }
                            }
                            formattedMessage += xml[i];
                            lastSawClosingTag = true;
                        }
                        else if (i < xml.Length - 1 &&
                                 xml[i + 1] == '?')
                        {
                            formattedMessage += xml[i];
                        }
                        else
                        {
                            lastSawClosingTag = false;
                            for (int currtab = 0; currtab < tabCount; currtab++)
                            {
                                formattedMessage += "\t";
                            }
                            formattedMessage += xml[i];
                            tabCount++;
                        }
                    }
                        break;
                    case '/':
                    {
                        if (i < xml.Length - 1 &&
                            xml[i + 1] == '>')
                        {
                            tabCount--;
                            lastSawClosingTag = true;
                        }
                        formattedMessage += xml[i];
                    }
                        break;
                    default:
                    {
                        formattedMessage += xml[i];
                    }
                        break;
                }
            }
            return formattedMessage;
        }
    }
}
