﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Reflection;

namespace BbTS.Core.Serialization
{
    /// <summary>
    /// Customize the value to be deserialized for Json datetime handling
    /// format = standard c# DateTime format, i.e.aa "yyyy-MM-dd")]
    /// </summary>
    public class CustomDateFormatConverter : IsoDateTimeConverter
    {
        public CustomDateFormatConverter(string format)
        {
            DateTimeFormat = format;
        }
    }
}
