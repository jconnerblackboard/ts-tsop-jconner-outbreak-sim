﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace BbTS.Core.Serialization
{
    /// <summary>
    /// This is a ContractResolver for Json.NET that will serialize base class members before descendant class members.  The primary
    /// use case for this is when classes are being serailized for display as beautified JSON and is purely cosmetic.
    /// </summary>
    public class BaseFirstContractResolver : DefaultContractResolver
    {
        static BaseFirstContractResolver()
        {
            Instance = new BaseFirstContractResolver();
        }

        // As of 7.0.1, Json.NET suggests using a static instance for "stateless" contract resolvers, for performance reasons.
        // http://www.newtonsoft.com/json/help/html/ContractResolver.htm
        // http://www.newtonsoft.com/json/help/html/M_Newtonsoft_Json_Serialization_DefaultContractResolver__ctor_1.htm
        // "Use the parameterless constructor and cache instances of the contract resolver within your application for optimal performance."
        public static BaseFirstContractResolver Instance { get; }

        protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
        {
            var properties = base.CreateProperties(type, memberSerialization);
            return properties?.OrderBy(p => p.DeclaringType.BaseTypesAndSelf().Count()).ToList();
        }
    }

    public static class TypeExtensions
    {
        public static IEnumerable<Type> BaseTypesAndSelf(this Type type)
        {
            while (type != null)
            {
                yield return type;
                type = type.BaseType;
            }
        }
    }
}