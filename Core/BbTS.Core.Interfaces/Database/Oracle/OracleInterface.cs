﻿using System;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using BbTS.Core.Configuration;
using BbTS.Core.Security.Database;
using BbTS.Domain.Models.Definitions;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Monitoring.Logging;


using Devart.Data.Oracle;

namespace BbTS.Core.Interfaces.Database.Oracle
{

    /// <summary>
    /// This class contains database access functionality
    /// </summary>
    public class OracleInterface
    {
        private static OracleInterface _instance = new OracleInterface();

        public static OracleInterface Instance
        {
            get { return _instance; }
            internal set { _instance = value; }
        }

        #region Connection/Parameter Settings

        /// <summary>
        /// This method returns a connection object
        /// </summary>
        /// <param name="connectionName">The name of the connection string to be used</param>
        /// <returns>A <see cref="OracleConnection"/></returns>
        public OracleConnection OracleConnectionGet(String connectionName)
        {
            // To fetch from web.config instead of SystemConfiguration.xml
            // ConfigurationManager.ConnectionStrings[connectionName].ToString()
            String connectionString = ConnectionStringGet();

            return new OracleConnection(connectionString);
        }

        /// <summary>
        /// Build an Transact Connection string using an entry in SystemConfiguration.xml
        /// </summary>
        /// <returns></returns>
        public String ConnectionStringGet()
        {
            var type = (DatabaseConnectionStringType)Enum.Parse(typeof(DatabaseConnectionStringType), RegistryConfigurationTool.Instance.ValueGet("TransactApiDatabaseConnectionStringFormat"), true);

            return DatabaseSecurityTool.CreateConnectionStringFromRegistry(type);
        }

        /// <summary>
        /// Open a connection and attach retry logic in case of failure.
        /// </summary>
        /// <param name="connection"></param>
        public void OpenConnection(OracleConnection connection)
        {
            DateTime beginDatetime = DateTime.Now;
            int retries = 3;
            while (retries > 0)
            {
                try
                {
                    beginDatetime = DateTime.Now;
                    connection.Open();
                    return;
                }
                catch (Exception ex)
                {
                    string append = $"Elapsed Time: {DateTime.Now.Subtract(beginDatetime)}";

                    var severity = retries > 1
                        ? EventLogEntryType.Information
                        : retries == 1 ? EventLogEntryType.Warning : EventLogEntryType.Error;

                    LoggingManager.Instance.LogException(
                        ex,
                        "",
                        append,
                        severity,
                        (short)LoggingDefinitions.Category.Database,
                        (int)LoggingDefinitions.EventId.DatabaseConnectionFailed);
                    retries--;
                    if (retries <= 0)
                    {
                        throw;
                    }
                }
            }
        }

        public OracleParameter OracleParameterGet(string parmName, OracleDbType parmDataType, int parmDataSize, ParameterDirection parmDirection, byte[] parmValue)
        {
            return new OracleParameter { ParameterName = parmName, OracleDbType = parmDataType, Size = parmDataSize, Value = parmValue, Direction = parmDirection };
        }

        /// <summary>
        /// This method creates an OracleParameter - DateTime value overload
        /// </summary>
        /// <param name="parmName">The name of the parameter</param>
        /// <param name="parmDataType">The data type of the parameter</param>
        /// <param name="parmDataSize">The size of the parameter</param>
        /// <param name="parmDirection">The direction of the parameter</param>
        /// <param name="parmValue">The value of the parameter</param>
        /// <returns>A <see cref="OracleParameter"/> object</returns>
        public OracleParameter OracleParameterGet(string parmName, OracleDbType parmDataType, int parmDataSize, ParameterDirection parmDirection, DateTime parmValue)
        {
            return OracleParameterGet(parmName, parmDataType, parmDataSize, parmDirection, parmValue.ToString(CultureInfo.InvariantCulture));
        }

        /// <summary>
        /// This method creates an OracleParameter - Int32 value overload
        /// </summary>
        /// <param name="parmName">The name of the parameter</param>
        /// <param name="parmDataType">The data type of the parameter</param>
        /// <param name="parmDataSize">The size of the parameter</param>
        /// <param name="parmDirection">The direction of the parameter</param>
        /// <param name="parmValue">The value of the parameter</param>
        /// <returns>A <see cref="OracleParameter"/> object</returns>
        public OracleParameter OracleParameterGet(String parmName, OracleDbType parmDataType, Int32 parmDataSize, ParameterDirection parmDirection, Int32? parmValue)
        {
            return OracleParameterGet(parmName, parmDataType, parmDataSize, parmDirection, parmValue.ToString());
        }

        /// <summary>
        /// This method creates an OracleParameter - float value overload
        /// </summary>
        /// <param name="parmName">The name of the parameter</param>
        /// <param name="parmDataType">The data type of the parameter</param>
        /// <param name="parmDataSize">The size of the parameter</param>
        /// <param name="parmDirection">The direction of the parameter</param>
        /// <param name="parmValue">The value of the parameter</param>
        /// <returns>A <see cref="OracleParameter"/> object</returns>
        public OracleParameter OracleParameterGet(String parmName, OracleDbType parmDataType, Int32 parmDataSize, ParameterDirection parmDirection, float? parmValue)
        {
            return OracleParameterGet(parmName, parmDataType, parmDataSize, parmDirection, parmValue.ToString());
        }

        /// <summary>
        /// This method creates an OracleParameter - float value overload
        /// </summary>
        /// <param name="parmName">The name of the parameter</param>
        /// <param name="parmDataType">The data type of the parameter</param>
        /// <param name="parmDataSize">The size of the parameter</param>
        /// <param name="parmDirection">The direction of the parameter</param>
        /// <param name="parmValue">The value of the parameter</param>
        /// <returns>A <see cref="OracleParameter"/> object</returns>
        public OracleParameter OracleParameterGet(String parmName, OracleDbType parmDataType, Int32 parmDataSize, ParameterDirection parmDirection, double? parmValue)
        {
            return OracleParameterGet(parmName, parmDataType, parmDataSize, parmDirection, parmValue.ToString());
        }

        /// <summary>
        /// This method creates an OracleParameter - No value overload
        /// </summary>
        /// <param name="parmName">The name of the parameter</param>
        /// <param name="parmDataType">The data type of the parameter</param>
        /// <param name="parmDataSize">The size of the parameter</param>
        /// <param name="parmDirection">The direction of the parameter</param>
        /// <returns>A <see cref="OracleParameter"/> object</returns>
        public OracleParameter OracleParameterGet(String parmName, OracleDbType parmDataType, Int32 parmDataSize, ParameterDirection parmDirection)
        {
            return OracleParameterGet(parmName, parmDataType, parmDataSize, parmDirection, String.Empty);
        }

        /// <summary>
        /// This method creates an OracleParameter - Base method 
        /// </summary>
        /// <param name="parmName">The name of the parameter</param>
        /// <param name="parmDataType">The data type of the parameter</param>
        /// <param name="parmDataSize">The size of the parameter</param>
        /// <param name="parmDirection">The direction of the parameter</param>
        /// <param name="parmValue">The value of the parameter</param>
        /// <returns>A <see cref="OracleParameter"/> object</returns>
        public OracleParameter OracleParameterGet(String parmName, OracleDbType parmDataType, Int32 parmDataSize, ParameterDirection parmDirection, String parmValue)
        {
            OracleParameter p = new OracleParameter { ParameterName = parmName, OracleDbType = parmDataType };

            if (!parmDataSize.Equals(0))
                p.Size = parmDataSize;

            p.Direction = parmDirection;

            if (!String.IsNullOrEmpty(parmValue))
            {

                if (!parmDataType.Equals(OracleDbType.Raw) && !parmDataType.Equals(OracleDbType.Date))
                {
                    p.Value = parmValue;
                }
                else if (parmDataType.Equals(OracleDbType.Date))
                {
                    p.Value = new OracleDate(DateTime.Parse(parmValue));
                }
                else
                {
                    byte[] buffer = new byte[p.Size];
                    for (int i = 0; i < p.Size; i++)
                    {
                        buffer[i] = Convert.ToByte(parmValue.Substring(i, 1));
                    }
                    p.Value = buffer;
                }
            }

            if (p.Value == null)
                p.Value = DBNull.Value;

            return p;
        }

        /// <summary>
        /// Get a (nullable) DateTime value for na oracle data reader
        /// </summary>
        /// <param name="r">OracleDataReader object</param>
        /// <param name="parameterName">parameter name to fetch on</param>
        /// <returns></returns>
        public static DateTime? GetNullableDateTime(OracleDataReader r, string parameterName)
        {
            if (r[parameterName] == null || r[parameterName] is DBNull)
            {
                return null;
            }
            return GetDateTime(r, parameterName);
        }

        /// <summary>
        /// Get a (nullable) Integer value for na oracle data reader
        /// </summary>
        /// <param name="r">OracleDataReader object</param>
        /// <param name="parameterName">parameter name to fetch on</param>
        /// <returns></returns>
        public static int? GetNullableInt(OracleDataReader r, string parameterName)
        {
            if (r[parameterName] == null || r[parameterName] is DBNull)
            {
                return null;
            }
            return r.GetInt32(parameterName);
        }

        /// <summary>
        /// Get a DateTime value for na oracle data reader
        /// </summary>
        /// <param name="r">OracleDataReader object</param>
        /// <param name="parameterName">parameter name to fetch on</param>
        /// <returns></returns>
        public static DateTime GetDateTime(OracleDataReader r, string parameterName)
        {
            if (r[parameterName] is double ||
                r[parameterName] is decimal)
            {
                return DateTime.FromOADate(r.GetDouble(parameterName));
            }
#pragma warning disable 612,618
            if (r[parameterName] is OracleDate || r[parameterName] is OracleDateTime || r[parameterName] is DateTime)
#pragma warning restore 612,618
            {
                return r.GetDateTime(parameterName);
            }
            throw new ArgumentException("Oracle parameter type not recognized as DateTime type");
        }

        #endregion
    }
}