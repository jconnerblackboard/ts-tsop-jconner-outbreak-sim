﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using BbTS.Domain.Models.System.Logging;

namespace BbTS.Core.Logging.AuditLogging
{
    public class AuditLogTool
    {
        /// <summary>
        /// Parse an input for a key value pair separated by a delimiter
        /// </summary>
        /// <param name="field">String to parse for a key value pair</param>
        /// <param name="separator">character separating key and value</param>
        /// <param name="trimWhiteSpace">remove white space before and after including '"'</param>
        /// <returns>Parsed KeyValuePair</returns>
        public static KeyValuePair<string, string> ParseNameValuePair(String field, char separator, bool trimWhiteSpace)
        {
            string[] nvPair = field.Split(separator);
            if (nvPair.Length == 2)
            {
                string key = nvPair[0];
                string value = nvPair[1];

                if (trimWhiteSpace)
                {
                    key = key.TrimStart(new char[] { ' ', '\"' });
                    key = key.TrimEnd(new char[] { ' ', '\"' });
                    value = value.TrimStart(new char[] { ' ', '\"' });
                    value = value.TrimEnd(new char[] { ' ', '\"' });
                }
                return new KeyValuePair<string, string>(key, value);
            }
            throw new ArgumentException("Invalid format for input string.  Expecting two values but found " + nvPair.Length.ToString());
        }

        /// <summary>
        /// Extract the audit log items
        /// </summary>
        /// <param name="headers"></param>
        /// <returns></returns>
        public static AuditLogItem ExtractAuditLogItems(NameValueCollection headers)
        {
            if (!headers.HasKeys()) throw new ArgumentException("AuditLogging: Missing header values.");
            if (headers["BbTS"] == null) throw new ArgumentException("AuditLogging: BbTS header is missing.");

            AuditLogItem logItem = new AuditLogItem();

            String bbtsValue = headers["BbTS"];
            List<String> fields = new List<string>(bbtsValue.Split(','));

            // Extract the ApplicationName field
            try
            {
                String field = fields.FirstOrDefault(f => f.Contains("ApplicationName"));
                if (!String.IsNullOrEmpty(field))
                {
                    KeyValuePair<string, string> applicationNameField = ParseNameValuePair(field, '=', true);
                    if (applicationNameField.Key == "ApplicationName") logItem.Program = applicationNameField.Value;
                }
            }
            catch (ArgumentException){}


            // Extract the OsHostName field
            try
            {
                String field = fields.FirstOrDefault(f => f.Contains("OsHostName"));
                if (!String.IsNullOrEmpty(field))
                {
                    KeyValuePair<string, string> applicationNameField = ParseNameValuePair(field, '=', true);
                    if (applicationNameField.Key == "OsHostName") logItem.OsHostName = applicationNameField.Value;
                }
            }
            catch (ArgumentException) { }

            // Extract the IpAddress field
            try
            {
                String field = fields.FirstOrDefault(f => f.Contains("IpAddress"));
                if (!String.IsNullOrEmpty(field))
                {
                    KeyValuePair<string, string> applicationNameField = ParseNameValuePair(field, '=', true);
                    if (applicationNameField.Key == "IpAddress") logItem.IpAddress = applicationNameField.Value;
                }
            }
            catch (ArgumentException) { }

            // Extract the OsUser field
            try
            {
                String field = fields.FirstOrDefault(f => f.Contains("OsUser"));
                if (!String.IsNullOrEmpty(field))
                {
                    KeyValuePair<string, string> applicationNameField = ParseNameValuePair(field, '=', true);
                    if (applicationNameField.Key == "OsUser") logItem.OsUser = applicationNameField.Value;
                }
            }
            catch (ArgumentException) { }

            return logItem;
        }

        /// <summary>
        /// Generate an audit log description message from the AuditLogItem fields
        /// </summary>
        /// <param name="logItem"></param>
        /// <param name="failureMessage"></param>
        /// <returns></returns>
        public static String GenerateAuditLogDescription(AuditLogItem logItem, string failureMessage)
        {
            string failMessage = string.Empty;
            if (!logItem.SuccessFlag) failMessage = String.Format("  Reason: {0}", failureMessage);

            string message = String.Format(
                "Entity [{0}]: User [{1}] has issued a [{2}] action for [{3}] from [{4}] with a result of [{5}].{6}",
                logItem.EntityId.ToString(),
                logItem.Username,
                logItem.EventTypeId.ToString(),
                logItem.AffectedDataId,
                logItem.Program,
                logItem.SuccessFlag ? "success" : "failed",
                failMessage
                );
            return message;
        }
    }
}
