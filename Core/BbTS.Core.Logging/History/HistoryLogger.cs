﻿using System;
using System.Collections.Generic;
using BbTS.Core.Configuration;

namespace BbTS.Core.Logging.History
{
    public class HistoryLogger
    {
        private static readonly object LockObject = new object();

        public static HistoryLogger Instance { get; set; } = new HistoryLogger();
        public List<String> History { get; internal set; } = new List<string>();

        /// <summary>
        /// Log a history message.
        /// </summary>
        /// <param name="message"></param>
        public void Log(string message)
        {
            var logEntry = String.Format("[{0}]: {1}", DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss.fff"), message);
            int maxSize = ApplicationConfiguration.GetKeyValueAsInt("InMemoryLogging.MaxHistorySize", 1000);

            // Abort if the size is 0; i.e. history logging is off
            if (maxSize == 0) return;

            lock (LockObject)
            {
                // Pop the first message if the size has exceeded the max
                if (History.Count >= maxSize) History.RemoveAt(0);

                // Add the new message
                History.Add(logEntry);
            }
        }
    }
}
