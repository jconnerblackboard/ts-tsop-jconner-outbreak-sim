﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using BbTS.Core.Configuration;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.System.Logging.Tracking;
using BbTS.Monitoring.Logging;

namespace BbTS.Core.Logging.Tracking
{
    public class ProcessTracker
    {
        private static ProcessTracker _instance = new ProcessTracker();

        private readonly List<ProcessTrackerObject> _processTrackerObjects = new List<ProcessTrackerObject>();
        private List<ProcessTrackerObject> _storedResults = new List<ProcessTrackerObject>();
        private static readonly object ListLock = new object();

        public ProcessTrackingObjectState State { get; internal set; }
        public DateTime LastStarted { get; internal set; }
        public DateTime LastCompleted { get; internal set; }
        public string Id { get; internal set; }
        public int ExpectedChildNodes { get; set; }

        

        public ProcessTracker()
        {
            State = ProcessTrackingObjectState.NotStarted;
            Id = Guid.NewGuid().ToString("D");
        }

        public static ProcessTracker Instance
        {
            get { return _instance; }
            set { _instance = value; }
        }

        #region Public

        /// <summary>
        /// Clear the process tracker and reset to defaults.
        /// </summary>
        public void Clear()
        {
            State = ProcessTrackingObjectState.NotStarted;
            LastStarted = new DateTime();
            LastCompleted = new DateTime();
            lock (ListLock) _processTrackerObjects.Clear();
        }

        /// <summary>
        /// Create and add a node to the ProcessTracker.
        /// </summary>
        /// <param name="name">Node name</param>
        /// <param name="id">Created node id</param>
        /// <param name="message">Results message</param>
        /// <param name="parentId">Optional parent id to add the node under</param>
        /// <param name="expectedChildCount">The count of expected children</param>
        /// <param name="idOverride">Id to use to override the auto created one</param>
        /// <returns>Node was added successfully or not</returns>
        public bool CreateAndAddNode(
            string name,
            out string id,
            out string message,
            string parentId = null, 
            int expectedChildCount = 0, 
            string idOverride = null)
        {
            var node = new ProcessTrackerObject
            {
                Name = String.IsNullOrEmpty(name) ? ProcessTrackerObject.DefaultName : name,
                ParentId = parentId,
                CurrentState = ProcessTrackingObjectState.NotStarted,
                ExpectedChildCount = expectedChildCount
            };
            if (!String.IsNullOrEmpty(idOverride)) node.Id = idOverride;
            id = node.Id;

            return String.IsNullOrEmpty(parentId) ? _add(node, out message) : _addChild(parentId, node, out message);
        }

        /// <summary>
        /// Get the progress state of a node.
        /// </summary>
        /// <param name="id">Id of the node</param>
        /// <param name="state">node state</param>
        /// <param name="message">Results message</param>
        /// <returns>Success or not.</returns>
        public bool NodeProgressGet(string id, out ProcessTrackingObjectState state, out string message)
        {
            state = ProcessTrackingObjectState.Unknown;
            if (State != ProcessTrackingObjectState.InProgress)
            {
                message = String.Format("Unable to retrieve progress state on node with id {0} because there is no tracking session.  Start the tracking session before recording node progress.", id);
                return false;
            }
            lock (ListLock)
            {
                var node = FindNode(id);
                if (node == null)
                {
                    message = String.Format("Unable to retrieve progress state on node with id {0} because it was not found.", id);
                    return false;
                }
                state = node.CurrentState;
            }
            message = "Success.";
            return true;
        }

        /// <summary>
        /// Locate a node with the provided id.
        /// </summary>
        /// <param name="id">Id of the node.</param>
        /// <param name="objects">ProcessTrackerObjects to check for the id in.  If null will use the root level objects.</param>
        /// <returns>Node object or null if not found.</returns>
        public ProcessTrackerObject FindNode(string id, List<ProcessTrackerObject> objects = null)
        {
            // if the objects passed in aren't null use them otherwise use the root objects.
            var node = objects == null ?
                _processTrackerObjects.FirstOrDefault(n => n.Id == id) :
                objects.FirstOrDefault(n => n.Id == id);

            // if it was found then return the node
            if (node != null) return node;

            // if node check the children
            foreach (var list in objects ?? _processTrackerObjects)
            {
                node = FindNode(id, list.Children);
                if (node != null) return node;
            }

            // Was not found
            return null;
        }

        /// <summary>
        /// Start progress on a tracking node.
        /// </summary>
        /// <param name="id">Id of the object</param>
        /// <param name="message">Results message</param>
        /// <returns>Successful or not.</returns>
        public bool StartNodeProgress(string id, out string message)
        {
            if (State != ProcessTrackingObjectState.InProgress)
            {
                message = String.Format("Unable to start progress on node with id {0} because there is no tracking session.  Start the tracking session before recording node progress.", id);
                return false;
            }
            lock (ListLock)
            {
                var node = FindNode(id);
                if (node == null)
                {
                    message = String.Format("Unable to start progress on node with id {0} because it was not found.", id);
                    return false;
                }

                node.StartTime = DateTime.Now;
                node.CurrentState = ProcessTrackingObjectState.InProgress;
            }

            message = "Success.";
            return true;
        }

        /// <summary>
        /// Finish progress on a tracking node.
        /// </summary>
        /// <param name="id">Id of the object</param>
        /// <param name="message">Results message</param>
        /// <returns>Successful or not.</returns>
        public bool FinishNodeProgress(string id, out string message)
        {
            if (State != ProcessTrackingObjectState.InProgress)
            {
                message = String.Format("Unable to finish progress on node with id {0} because there is no tracking session.  Start the tracking session before recording node progress.", id);
                return false;
            }
            lock (ListLock)
            {
                var node = FindNode(id);
                if (node == null)
                {
                    message = String.Format("Unable to finish progress on node with id {0} because it was not found.", id);
                    return false;
                }

                node.EndTime = DateTime.Now;
                node.CurrentState = ProcessTrackingObjectState.Completed;
            }

            message = "Success.";
            return true;
        }

        /// <summary>
        /// Record an error message for a node.
        /// </summary>
        /// <param name="id">Id of the object</param>
        /// <param name="errmsg">Message to record.</param>
        /// <param name="message">Results message</param>
        /// <returns>Successful or not.</returns>
        public bool RecordErrorMessage(string id, string errmsg, out string message)
        {
            if (State != ProcessTrackingObjectState.InProgress)
            {
                message = String.Format("Unable to record error messages on node with id {0} because there is no tracking session.  Start the tracking session before recording node progress.", id);
                return false;
            }
            lock (ListLock)
            {
                var node = FindNode(id);
                if (node == null)
                {
                    message = String.Format("Unable to record the error message \"{1}\" on node with id {0} because it was not found.", id, errmsg);
                    return false;
                }

                node.ErrorMessages.Add(errmsg);
            }
            message = "Success.";
            return true;
        }

        /// <summary>
        /// Begin the process tracking session.
        /// </summary>
        /// <param name="message">Results message</param>
        /// <returns>Successfully started or not.</returns>
        public bool Begin(out string message)
        {
            if (State == ProcessTrackingObjectState.InProgress)
            {
                message = "Unable to begin tracking while tracking is already in progress.  Finalize the tracking session before beginning a new one.";
                return false;
            }

            State = ProcessTrackingObjectState.InProgress;
            LastStarted = DateTime.Now;
            message = "Success.";
            return true;
        }

        /// <summary>
        /// Finalize the process tracking session.
        /// </summary>
        /// <param name="message">Results message</param>
        /// <returns>Successfully started or not.</returns>
        public bool Finalize(out string message)
        {
            switch (State)
            {
                case ProcessTrackingObjectState.NotStarted:
                {
                    message = "Unable to finalize a tracking session that has not started.";
                    return false;
                }
                case ProcessTrackingObjectState.Completed:
                {
                    message = "Unable to finalize a tracking session that has already completed.";
                    return false;
                }
            }

            State = ProcessTrackingObjectState.Completed;
            LastCompleted = DateTime.Now;

            // Store the node
            if (!_storeFinalizedNode(_rootGet(), out message)) return false;

            message = "Success.";
            return true;
        }

        /// <summary>
        /// Get a list of all stored reports in raw ProcessTrackerObject.
        /// </summary>
        /// <returns></returns>
        public List<ProcessTrackerObject> RawReportCreate()
        {
            var reportList = new List<ProcessTrackerObject>();            
            if (State != ProcessTrackingObjectState.Completed) reportList.Add(_rootGet());
            reportList.AddRange(_storedResults);

            return reportList;
        }

        /// <summary>
        /// Get a list of all stored reports in raw ProcessTrackerObject r.Get a list of all stored reports.
        /// </summary>
        /// <param name="count">Number of reports to fetch. The currently running one will always be fetched as the latest even if it isn't completed yet. </param>
        /// <returns></returns>
        public List<TrackingReport> ReportCreate(int count = 1)
        {
            var reportList = new List<TrackingReport>();

            // Create the latest report if it is still in progress
            string message;
            if (State != ProcessTrackingObjectState.Completed)
            {
                var report = _reportCreate(out message);
                if (report == null)
                {
                    LoggingManager.Instance.LogException(message, EventLogEntryType.Error);
                    return null;
                }
                reportList.Add(report);
            }

            // if we fetched the in process report set the count to that
            int fetched = reportList.Count;

            // fetch the remaining results up to a maximum of the value specified in count.
            foreach (var result in _storedResults)
            {
                if (fetched >= count) break;
                var report = _reportCreate(out message, null, result);
                if (report == null)
                {
                    LoggingManager.Instance.LogException(message, EventLogEntryType.Error);
                }
                reportList.Add(report);
                fetched++;
            }

            return reportList;
        }

        #endregion

        #region Private

        /// <summary>
        /// Add a node to the ProcessTracker.  The Id must be unique.
        /// </summary>
        /// <param name="obj">Object to add</param>
        /// <param name="message">Result message</param>
        /// <returns>Node was successfully added or not.</returns>
        private bool _add(ProcessTrackerObject obj, out string message)
        {
            lock (ListLock)
            {
                if (_processTrackerObjects.FirstOrDefault(o => o.Id == obj.Id) != null)
                {
                    message = string.Format("Unable to add node {0} with id {1} because it already exists.", obj.Name, obj.Id);
                    return false;
                }
                _processTrackerObjects.Add(obj);
            }
            message = "Success.";
            return true;
        }

        /// <summary>
        /// Add a node to the ProcessTracker.  The Id must be unique.
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="obj"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        private bool _addChild(String parentId, ProcessTrackerObject obj, out string message)
        {
            lock (ListLock)
            {
                var parent = FindNode(parentId);
                if (parent == null)
                {
                    message = string.Format("Unable to locate parent node with id {0}.", parentId);
                    return false;
                }

                if (parent.Children.FirstOrDefault(o => o.Id == obj.Id) != null)
                {
                    message = string.Format("Unable to add node {0} with id {1} to the parent node {2} because the child already exists.", obj.Name, obj.Id, parentId);
                    return false;
                }
                parent.Children.Add(obj);
            }
            message = "Success.";
            return true;
        }

        /// <summary>
        /// Create a report starting at the specified node id
        /// </summary>
        /// <param name="message"></param>
        /// <param name="id"></param>
        /// <param name="inNode"></param>
        /// <returns></returns>
        private TrackingReport _reportCreate(out string message, string id = null, ProcessTrackerObject inNode = null)
        {
            // if the passed in node (inNode) is not null use that
            // else
            //      If the passed in id is null use the root of the Tracker
            //      else find the node defined by id
            var node = inNode ?? (id == null ? _rootGet() : FindNode(id));

            if (node == null)
            {
                message = String.Format("Node with id {0} was not found.  Report not generated.", id);
                return null;
            }

            var report = TrackingReport.CreateReport(node);

            // Was not found
            message = "Success.";
            return report;
        }

        /// <summary>
        /// Get the Tracking nodes from the root.
        /// </summary>
        /// <returns></returns>
        private ProcessTrackerObject _rootGet()
        {
            ProcessTrackerObject root;
            lock (ListLock)
            {
                root = new ProcessTrackerObject
                {
                    Name = String.Format("Tracking Report {{{0}}}", Id),
                    Children = new List<ProcessTrackerObject>(_processTrackerObjects.Select(c => c.Clone())),
                    CurrentState = State,
                    EndTime = LastCompleted,
                    StartTime = LastStarted,
                    ExpectedChildCount = State == ProcessTrackingObjectState.Completed || ExpectedChildNodes == 0 ? _processTrackerObjects.Count : ExpectedChildNodes,
                    Id = Id,
                    ParentId = null
                };
            }
            return root;
        }

        /// <summary>
        /// Store a completed tracking root node.
        /// </summary>
        /// <param name="node">Node to store</param>
        /// <param name="message">Results message</param>
        /// <returns>Operation successful or not</returns>
        private bool _storeFinalizedNode(ProcessTrackerObject node, out string message)
        {
            if (node == null)
            {
                message = String.Format("Unable to store node because it is null");
                return false;
            }
            if (node.CurrentState != ProcessTrackingObjectState.Completed)
            {

                message = String.Format("Unable to store finalized node {0} with id {1} because it is not in a completed state ({2}).", node.Name, node.Id, node.CurrentState);
                return false;
            }
            try
            {
                var maxCount = ApplicationConfiguration.GetKeyValueAsInt("SyncReportHistoryStoredCountMax", 1);

                if (_storedResults.Count >= maxCount)
                {
                    _storedResults.RemoveAt(_storedResults.Count - 1);
                }

                _storedResults.Insert(0, node);
            }
            catch (Exception ex)
            {
                message = String.Format("Unable to store finalized node {0} with id {1}.  {2}", node.Name, node.Id, Formatting.FormatException(ex));
                return false;
            }

            message = "Success.";
            return true;
        }

        #endregion
    }
}
