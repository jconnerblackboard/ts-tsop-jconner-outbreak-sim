﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using BbTS.Core.Configuration;
using BbTS.Domain.Models.Configuration;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.System.Logging.Tracking;
using BbTS.Monitoring.Logging;

namespace BbTS.Core.Logging.Tracking
{
    public class EventLogTracker
    {
        private List<EventLogTrackingObject>  _eventLogBuffer = new List<EventLogTrackingObject>();

        private DateTime _purgeLastRunDateTime;

        private static readonly object LockObject = new object();


        /// <summary>
        /// Singleton Instance.
        /// </summary>
        public static EventLogTracker Instance { get; set; } = new EventLogTracker();

        /// <summary>
        /// Value in seconds denoting the time between identical entries before a duplicate log entry is created.
        /// </summary>
        public int TimeBetweenEntries { get; set; } = 120;

        /// <summary>
        /// Time to keep a log entry
        /// </summary>
        public int LogEntryDuration { get; set; } = 600;

        /// <summary>
        /// Log an event using the log buffer to meter duplicate entries.
        /// </summary>
        /// <param name="logObject">Event log object to create an entry for.</param>

        public void Log(EventLogTrackingObject logObject)
        {
            try
            {
                Guard.IsNotNull(logObject, "logObject");
                var index = _eventLogBuffer.FindIndex(entry => entry.Equals(logObject));

                // purge old entries
                _purgeOldEntries();

                // If there is no current entry, then log the event and add it to the buffer.
                if (index < 0)
                {
                    LogImmediate(logObject);
                    lock (LockObject) _eventLogBuffer.Add(logObject);
                    return;
                }

                // If there is an entry and the time between entries has been exceeded, log the entry and update the entry.
                if ((DateTime.Now - _eventLogBuffer[index].EventDateTime).TotalSeconds > TimeBetweenEntries)
                {
                    LogImmediate(logObject);
                    lock (LockObject) _eventLogBuffer[index] = logObject;
                }

            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(ex, EventLogEntryType.Error, (short)LoggingDefinitions.Category.Tracking, (int)LoggingDefinitions.EventId.UnknownFatalError);
                LogImmediate(logObject);
            }
        }

        /// <summary>
        /// Log an event using the log buffer to meter duplicate entries.
        /// </summary>
        /// <param name="exception">Override the message with the exception.  (Log the EventLogTrackingObject.Message as a prepend message.)</param>
        /// <param name="logObject">Event log object to create an entry for.</param>
        public void Log(Exception exception, EventLogTrackingObject logObject)
        {
            try
            {
                Guard.IsNotNull(logObject, "logObject");
                var index = _eventLogBuffer.FindIndex(entry => entry.Equals(logObject));

                // purge old entries
                _purgeOldEntries();

                // If there is no current entry, then log the event and add it to the buffer.
                if (index < 0)
                {
                    LogImmediate(logObject);
                    lock (LockObject) _eventLogBuffer.Add(logObject);
                    return;
                }

                // If there is an entry and the time between entries has been exceeded, log the entry and update the entry.
                if ((DateTime.Now - _eventLogBuffer[index].EventDateTime).TotalSeconds > TimeBetweenEntries)
                {
                    LogImmediate(exception, logObject);
                    lock (LockObject) _eventLogBuffer[index] = logObject;
                }

            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(ex, EventLogEntryType.Error, (short)LoggingDefinitions.Category.Tracking, (int)LoggingDefinitions.EventId.UnknownFatalError);
                LogImmediate(logObject);
            }
        }

        /// <summary>
        /// Ignore buffer and write to the event log immediately.
        /// </summary>
        /// <param name="logObject">Event log object to create an entry for.</param>
        public void LogImmediate(EventLogTrackingObject logObject)
        {
            try
            {
                Guard.IsNotNull(logObject, "logObject");
                LoggingManager.Instance.LogException(logObject.Message, logObject.Severity, (short)logObject.EventCategory, (int)logObject.EventId);

            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(ex, EventLogEntryType.Error, (short)LoggingDefinitions.Category.Tracking, (int)LoggingDefinitions.EventId.UnknownFatalError);                
            }
        }


        /// <summary>
        /// Ignore buffer and write to the event log immediately.
        /// </summary>
        /// <param name="exception">Override the message with the exception.  (Log the EventLogTrackingObject.Message as a prepend message.)</param>
        /// <param name="logObject">Event log object to create an entry for.</param>
        public void LogImmediate(Exception exception, EventLogTrackingObject logObject)
        {
            try
            {
                Guard.IsNotNull(logObject, "logObject");
                LoggingManager.Instance.LogException(exception, logObject.Message, "", logObject.Severity, (short)logObject.EventCategory, (int)logObject.EventId);

            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(ex, EventLogEntryType.Error, (short)LoggingDefinitions.Category.Tracking, (int)LoggingDefinitions.EventId.UnknownFatalError);
            }
        }
        /// <summary>
        /// Ignore buffer and write to the event log immediately if debugging is enabled.
        /// </summary>
        /// <param name="configuration">Configuration object to determine logging level.</param>
        /// <param name="logObject">Event log object to create an entry for.</param>
        public void LogDebug(CustomConfigurationModel configuration, EventLogTrackingObject logObject)
        {
            try
            {
                Guard.IsNotNull(logObject, "logObject");
                Guard.IsNotNull(configuration, "configuration");

                var loggingLevel = int.Parse(CustomConfiguration.GetKeyValueAsString("ExceptionLevelLogging", configuration, "5"));
                if (loggingLevel == 0)
                {
                    LoggingManager.Instance.LogException(logObject.Message, logObject.Severity, (short)logObject.EventCategory, (int)logObject.EventId);
                }
            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(ex, EventLogEntryType.Error, (short)LoggingDefinitions.Category.Tracking, (int)LoggingDefinitions.EventId.UnknownFatalError);
                LogImmediate(logObject);
            }
        }

        /// <summary>
        /// Purge entries older than LogEntryDuration.
        /// </summary>
        private void _purgeOldEntries()
        {
            try
            {
                if ((DateTime.Now - _purgeLastRunDateTime).TotalSeconds < LogEntryDuration)
                {
                    return;
                }

                var remainingLogTrackingObjects = new List<EventLogTrackingObject>();
                lock (LockObject)
                {
                    foreach (var entry in _eventLogBuffer)
                    {
                        if ((DateTime.Now - entry.EventDateTime).TotalSeconds < LogEntryDuration)
                        {
                            remainingLogTrackingObjects.Add(entry);
                        }
                    }

                    _eventLogBuffer = remainingLogTrackingObjects;
                    _purgeLastRunDateTime = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(ex, EventLogEntryType.Error, (short)LoggingDefinitions.Category.Tracking, (int)LoggingDefinitions.EventId.UnknownFatalError);
            }
        }
    }
}
