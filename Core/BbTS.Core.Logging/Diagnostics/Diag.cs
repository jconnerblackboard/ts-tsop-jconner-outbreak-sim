﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;

namespace BbTS.Core.Logging.Diagnostics
{
    // --------------------------------------------------------------------
    // author:  Paul Armstrong
    //
    // The Diag singleton object sends debugging text to a local
    // subnet broadcast via UDP.
    //
    // e.g., If the network interface has an IP address of 192.168.1.5
    // with subnet mask of 255.255.255.0, then the UDP subnet
    // broadcast will be sent to all at 192.168.1.255 via the
    // connected port.
    //
    // Sample use:
    //
    //   Diag.ConnectPort(2222);	// port 2222 on local subnet
    //
    //   Diag.Putline("/-------------"); // print diag console
    //
    //   Diag.Putline("Hello world"); // ditto
    //

    public sealed class Diag
    {
        // singleton and ctor for singleton
        private static volatile Diag _inst;
        private static readonly object Mutx = new Object();

        public static Diag Instance
        {
            get
            {
                if (_inst == null)
                {
                    lock (Mutx)
                    {
                        if (_inst == null)
                            _inst = new Diag();
                    }
                }

                return _inst;
            }
        }

#if (DEBUG)
        // DEBUG implements the functionality

        public static void ConnectPort(int port)
        {
            if (_sock != null && _port == port) return;

            Disconnect(); // safe to disconnect any prior

            IPAddress a = _GetAdapterBroadcastIP(-1);

            _port = port;
            _sock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            //_sock.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
            _endp = new IPEndPoint(a, _port);
        }

        public static void Connect(string ip, int port)
        {
            if (_sock != null && _port == port) return;

            Disconnect(); // safe to disconnect any prior

            IPAddress a = IPAddress.Parse(ip);

            _port = port;
            _sock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            _endp = new IPEndPoint(a, _port);
        }

        public static void Putline(string fmt, params object[] parms)
        {
            if (_sock == null)
                return;
            Putline(String.Format(fmt, parms));
        }

        public static void Putline(string text)
        {
            if (_sock == null) return;

            // create packet (zero terminated)
            ASCIIEncoding enc = new ASCIIEncoding();
            string timestamp = DateTime.Now.ToString("s");
            string packetString = _pfix + timestamp + "| " + text.TrimEnd() + "\n";
            int nbytes = enc.GetByteCount(packetString);
            const int maxPacketSize = 1024;

            _pfix = ""; // Deprecating use: Diag.SetPrefix(); print it once and then clear
            if (nbytes >= maxPacketSize)
                nbytes = maxPacketSize;
            byte[] packetBytes = new byte[nbytes + 1];

            Buffer.BlockCopy(enc.GetBytes(packetString), 0, packetBytes, 0, nbytes);
            packetBytes[nbytes] = 0;

            // send zero terminated packet to subnet
            _sock.SendTo(packetBytes, _endp);
        }

        public static void PutlineHuge(string headerText, string text)
        {
            if (_sock == null) return;

            ASCIIEncoding enc = new ASCIIEncoding();
            string timestamp = DateTime.Now.ToString("s");
            string packetString = _pfix + timestamp + "| ======== " + headerText + " ========\n";
            int nbytes = enc.GetByteCount(packetString);
            byte[] packetBytes = new byte[nbytes + 1];

            Buffer.BlockCopy(enc.GetBytes(packetString), 0, packetBytes, 0, nbytes);
            packetBytes[nbytes] = 0;

            // send zero terminated packet to subnet
            _sock.SendTo(packetBytes, _endp);

            int pos = 0;
            while (pos < text.Length)
            {
                packetString = text.Substring(pos, Math.Min(text.Length - pos, 1000));
                pos += packetString.Length;
                nbytes = enc.GetByteCount(packetString);
                packetBytes = new byte[nbytes + 1];

                Buffer.BlockCopy(enc.GetBytes(packetString), 0, packetBytes, 0, nbytes);
                packetBytes[nbytes] = 0;

                _sock.SendTo(packetBytes, _endp);
            }

            packetString = _pfix + timestamp + "| =========" + new string('=', headerText.Length) + "=========\n";
            nbytes = enc.GetByteCount(packetString);
            packetBytes = new byte[nbytes + 1];

            Buffer.BlockCopy(enc.GetBytes(packetString), 0, packetBytes, 0, nbytes);
            packetBytes[nbytes] = 0;

            _sock.SendTo(packetBytes, _endp);
        }

        public static void Disconnect()
        {
            _endp = null;
            _sock = null;
        }

        // privates

        private static IPAddress _GetAdapterBroadcastIP(int iAdapter)
        {
            int iHardwareAdapter = 0;

            foreach (var nif in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (nif.NetworkInterfaceType == NetworkInterfaceType.Loopback) continue;

                // we are here only if non loopback
                if (iHardwareAdapter++ < iAdapter) continue;

                foreach (var ip in nif.GetIPProperties().UnicastAddresses)
                {
                    if (ip.Address.AddressFamily == AddressFamily.InterNetwork)
                    {
                        // So, for an IP address of 192.168.1.205 and
                        // a netmask 255.255.255.0, we need
                        // 192.168.1.255 for our broadcast subnet. 
                        // Note, under Vista, using 255.255.255.255 as
                        // subnet will not work for security purposes.
                        byte[] a = ip.Address.GetAddressBytes();
                        byte[] m = ip.IPv4Mask.GetAddressBytes();
                        int i, n = a.Length;
                        byte[] r = new byte[a.Length];

                        for (i = 0; i < n; i++)
                            r[i] = (byte)(a[i] | ~m[i]);

                        IPAddress subnetBroadcast = new IPAddress(r);

                        return subnetBroadcast;
                    }
                }
            }

            // Use a fallback if adapter not found.  On Vista,
            // broadcast to 255.255.255.255 will fail for security
            // reasons.

            return IPAddress.Broadcast;
        }

        public static bool IsConnected()
        {
            return _sock != null;
        }

        // information used to connect
        private static string _pfix;
        private static int _port;
        // actual connection itself
        private static Socket _sock;
        private static IPEndPoint _endp;

#else // RELEASE

// NOOPs for release

        public static bool IsConnected()
		{
			return true;	// pretend connected so caller won't try to ConnectPort()
		}
        public static void ConnectPort(int port)
		{
		}
        public static void ConnectPort(byte subnet, int port)
		{
		}
        public static void Connect(string ip, int port)
        {
        }
		public static void SetPrefix(string moduleName)
		{
		}
		public static void Putline(string fmt, params object[] parms)
		{
		}
		public static void PutSTACKTRACE()
		{
		}
		public static void Putline(string text)
		{
		}
	    public static void PutlineHuge(string headerText, string text)
        {
        }
		public static void Disconnect()
		{
		}

#endif
    }
}

