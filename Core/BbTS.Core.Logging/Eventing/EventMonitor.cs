﻿using System;
using System.Diagnostics;
using System.Threading;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Definitions.Service;
using BbTS.Monitoring.Logging;
using Microsoft.Diagnostics.Tracing;
using Microsoft.Diagnostics.Tracing.Parsers;
using Microsoft.Diagnostics.Tracing.Session;

namespace BbTS.Core.Logging.Eventing
{
    public delegate void TraceMessageDelegateFunction(DateTime timestamp, string message);

    public class EventMonitor
    {
        private Thread _monitorThread;
        private ETWTraceEventSource _source;
        private readonly TraceMessageDelegateFunction _delegate;

        /// <summary>
        /// Get/Set the ETW event source.
        /// </summary>
        public EventingSources EventSource { get; internal set; }

        /// <summary>
        /// Get the status of the monitor service.
        /// </summary>
        public ServiceStatus Status { get; internal set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="func">Function to call when a tracing event occurs.</param>
        /// <param name="source">Source name to track ETW events against.</param>
        public EventMonitor(TraceMessageDelegateFunction func, EventingSources source)
        {
            _delegate = func;
            EventSource = source;
            Status = ServiceStatus.Stopped;
        }

        /// <summary>
        /// Start the tracing service
        /// </summary>
        /// <param name="message">Detailed success/failure message.</param>
        /// <returns>Request was successful or not.</returns>
        public bool Start(out string message)
        {
            // Today you have to be Admin to turn on ETW events (anyone can write ETW events).   
            if (!(TraceEventSession.IsElevated() ?? false))
            {
                message = @"To turn on ETW events you need to be Administrator, please run from an Admin process.";
                return false;
            }

            if (Status == ServiceStatus.Started)
            {
                message = "EventMontior::Start called on a service that is already running.  Request will be ignored.";
                return true;
            }
            Status = ServiceStatus.Starting;

            _monitorThread = new Thread(_runMonitor) { IsBackground = true };
            _monitorThread.Start();

            message = String.Format("EventMontior::Start request was processed successfully.");
            return true;
        }

        /// <summary>
        /// Stop the tracing service
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool Stop(out string message)
        {
            if (Status == ServiceStatus.Stopped || Status == ServiceStatus.Uninitialized)
            {
                message = "EventMontior::Stop called on a service that is already stopped.  Request will be ignored.";
                return true;
            }

            Status = ServiceStatus.Stopping;

            if (_source == null)
            {
                message = String.Format("EventMontior::Stop unable to process request because the ETWTraceEventSource wasn't initialized properly.");
                return false;
            }

            _source.Dispose();

            message = String.Format("EventMontior::Stop request was processed successfully.");
            return true;
        }

        /// <summary>
        /// Start the Trace monitor
        /// </summary>
        private void _runMonitor()
        {
            var providerGuid = TraceEventProviders.GetEventSourceGuidFromName(EventingDefinitions.EventSourceProviderStringGet(EventSource));

            // You have to be Admin to turn on ETW events
            if (!(TraceEventSession.IsElevated() ?? false))
            {
                LoggingManager.Instance.LogException(@"To turn on ETW events you need to be an Administrator, please run from an Admin process.", EventLogEntryType.Error);
                return;
            }
            var sessionName = String.Format("DsrSyncTracing Session [{0}]", Guid.NewGuid());
            using (var session = new TraceEventSession(sessionName, null))  // the null second parameter means 'real time session'
            {
                session.StopOnDispose = true;

                // prepare to read from the session, connect the ETWTraceEventSource to the session
                using (_source = new ETWTraceEventSource(sessionName, TraceEventSourceType.Session))
                {
                    // Hook up the parser that knows about EventSources
                    var parser = new DynamicTraceEventParser(_source);
                    parser.All += delegate(TraceEvent data)
                    {
                        if (data.ProviderGuid == providerGuid)
                        {
                            if (data.EventName == "Trace")
                            {
                                string message = (string)data.PayloadByName("message");
                                string timestamp = data.PayloadByName("timestamp") as string;
                                DateTime dateTime;
                                if (!DateTime.TryParse(timestamp, out dateTime)) dateTime = DateTime.Now;

                                if (_delegate != null) _delegate(dateTime, message);
                            }
                        }
                    };

                    // Enable my provider, you can call many of these on the same session to get other events.  
                    session.EnableProvider(providerGuid);

                    // Send service started message
                    LoggingManager.Instance.LogMessage(String.Format("EventMonitor service started for {0} provider", EventingDefinitions.EventSourceProviderStringGet(EventSource)));
                    Status = ServiceStatus.Started;

                    _source.Process();

                    // Send service stopped message
                    LoggingManager.Instance.LogMessage(String.Format("EventMonitor service stopped for {0} provider", EventingDefinitions.EventSourceProviderStringGet(EventSource)));
                    Status = ServiceStatus.Stopped;
                }
            }
        }
    }
}
