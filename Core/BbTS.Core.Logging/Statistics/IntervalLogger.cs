﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using BbTS.Core.Configuration;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Definitions.Service;
using BbTS.Monitoring.Logging;

namespace BbTS.Core.Logging.Statistics
{

    public class IntervalLogger : IDisposable
    {
        private double _currentValue = 0;
        private DateTime _lastIntervalTime;
        private static readonly object CurrentValueLockObject = new object();
        private static readonly object LastIntervalLockObject = new object();

        private Thread _runThread;

        /// <summary>
        /// Maximum count value.
        /// </summary>
        public double MaxValue { get; internal set; }

        /// <summary>
        /// Is the max value provided?  If this is set it will add the Max value into the log statement.
        /// </summary>
        public bool MaxValueSpecified { get; internal set; }

        /// <summary>
        /// Number of seconds per logging interval
        /// </summary>
        public double Interval { get; set; }

        /// <summary>
        /// Time the IntervalLogger was started.
        /// </summary>
        public DateTime StartTime { get; internal set; }

        /// <summary>
        /// Location of the log file
        /// </summary>
        public string LogFileName { get; internal set; }

        /// <summary>
        /// Last time an interval occured (thread safe).
        /// </summary>
        public DateTime LastIntervalTime
        {
            get { lock (LastIntervalLockObject) return _lastIntervalTime; }
            set { lock (LastIntervalLockObject) _lastIntervalTime = value; }
        }

        /// <summary>
        /// Status of the InternalLogger interval function.
        /// </summary>
        public ServiceStatus Status { get; set; }

        /// <summary>
        /// Current value (thread safe).
        /// </summary>
        public double CurrentValue
        {
            get { lock (CurrentValueLockObject) return _currentValue; }
            set { lock (CurrentValueLockObject) _currentValue = value; }
        }

        /// <summary>
        /// Constructor with required parameter values.
        /// </summary>
        /// <param name="logFileName">File to log the count information to.</param>
        /// <param name="interval">Time in seconds between logging events</param>
        /// <param name="currentValue">Current count value.</param>
        /// <param name="maxSpecified">Max value is specified.  Default to false.</param>
        /// <param name="maxValue">Max count value.  Default is 0.</param>
        public IntervalLogger(
            string logFileName,
            double interval = 30,
            double currentValue = 0,
            bool maxSpecified = false,
            double maxValue = 0)
        {
            Status = ServiceStatus.Stopped;
            MaxValue = maxValue;
            MaxValueSpecified = maxSpecified;
            CurrentValue = currentValue;
            Interval = interval;

            LogFileName = FileConfigurationManager.VerifyFilePathAndDirectory(logFileName, ".csv");
        }

        /// <summary>
        /// Genereate a new interval logger
        /// </summary>
        /// <param name="basename"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static IntervalLogger GenerateIntervalLogger(string basename, int count)
        {
            var baseDir = ApplicationConfiguration.GetKeyValueAsString("IntervalLoggerBaseDirectory", "");
            string filename = String.Format("{0}{1}",
                String.IsNullOrEmpty(baseDir) ? "" : String.Format("{0}\\", baseDir),
                basename);

            double interval = ApplicationConfiguration.GetKeyValueAsDouble("IntervalLoggingInterval", 30);

            return new IntervalLogger(filename, interval, 0, true, count);
        }

        /// <summary>
        /// Handle logging event triggers
        /// </summary>
        public void LogIntervalEvent()
        {
            if (!ApplicationConfiguration.GetKeyValueAsBoolean("EnableIntervalLogging"))
            {
                _logDisabledMessage();
                return;
            }
            // Log to the log file ElapsedTime,CurrentValue
            File.AppendAllText(LogFileName, String.Format("{0},{1}\r\n", (DateTime.Now - StartTime).TotalSeconds, CurrentValue));
        }

        /// <summary>
        /// Log a debug message stating that interval logging is disabled.
        /// </summary>
        private void _logDisabledMessage()
        {
            LoggingManager.Instance.LogDebugMessage("IntervalLogging is currently disabled.  To enable IntervalLogging set the EnableIntervalLogging appSetting to 'yes'.",EventLogEntryType.Information);
        }

        /// <summary>
        /// Get the current running time of the logger.
        /// </summary>
        /// <returns></returns>
        public TimeSpan RunningTime()
        {
            return DateTime.Now - StartTime;
        }

        public void Start()
        {
            if (!ApplicationConfiguration.GetKeyValueAsBoolean("EnableIntervalLogging"))
            {
                _logDisabledMessage();
                return;
            }
            if (CurrentValue < 0) CurrentValue = 0;
            if (Status != ServiceStatus.Stopped) throw new ArgumentException(String.Format("IntervalLogger cannot be started because it is in a {0} state.", Status));

            Status = ServiceStatus.Starting;
            StartTime = DateTime.Now;
            LastIntervalTime = new DateTime();

            _runThread = new Thread(_run) { IsBackground = true };
            _runThread.Start(this);
        }

        public void Stop()
        {
            Status = ServiceStatus.Stopping;
            if (_runThread != null) _runThread.Interrupt();
        }

        private void _run(object caller)
        {
            try
            {
                IntervalLogger callingObject = caller as IntervalLogger;
                if (callingObject == null) throw new ArgumentException("Calling object was null");

                // Set the caller status as started
                callingObject.Status = ServiceStatus.Started;

                while (callingObject.Status == ServiceStatus.Started)
                {
                    DateTime now = DateTime.Now;
                    if ((now - callingObject.LastIntervalTime).TotalSeconds > callingObject.Interval)
                    {
                        callingObject.LastIntervalTime = now;
                        callingObject.LogIntervalEvent();
                    }

                    // Sleep a second
                    Thread.Sleep(1000);
                }

                callingObject.Status = ServiceStatus.Stopped;
            }
            catch (ThreadAbortException)
            {
            }
            catch (ThreadInterruptedException)
            {
            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(ex, "", "", EventLogEntryType.Error);
            }

        }

        /// <summary>
        /// Disposing
        /// </summary>
        public void Dispose()
        {
            Status = ServiceStatus.Stopping;
            _runThread.Abort();
        }
    }
}
