﻿using System.Diagnostics.Tracing;

namespace BbTS.Core.Logging.Tracing
{
    [EventSource(Name = "Blackboard Transact Tracing Events"/*, Guid = "{2F7581B5-457D-4F4A-821F-19F4CB28E432}"*/)]
    public class TraceEventSource : EventSource
    {
        public const string EventSourceName = "Blackboard Transact Tracing Events";
        public const EventKeywords Page = (EventKeywords)1;
        public const EventKeywords DataBase = (EventKeywords)2;
        public const EventKeywords Diagnostic = (EventKeywords)4;
        public const EventKeywords Perf = (EventKeywords)8;

        private static TraceEventSource _instance = new TraceEventSource();

        public static TraceEventSource Instance
        {
            get
            {
                return _instance ?? new TraceEventSource();
            }
            internal set { _instance = value; }
        }

        //[Event(1, Message = "{0}", Level = EventLevel.Informational, Keywords = Diagnostic)]
        public void Trace(string message)
        {
            WriteEvent(1, message);
        }
    }
}
