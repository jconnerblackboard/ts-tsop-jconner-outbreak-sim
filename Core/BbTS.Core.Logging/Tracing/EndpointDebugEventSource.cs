﻿using System.Diagnostics.Tracing;


namespace BbTS.Core.Logging.Tracing
{
    [EventSource(Name = "Blackboard Endpoint Debugging Events")]
    public class EndpointDebugEventSource : EventSource
    {
        public const string EventSourceName = "Blackboard Endpoint Debugging Events";
        public const EventKeywords Page = (EventKeywords)1;
        public const EventKeywords DataBase = (EventKeywords)2;
        public const EventKeywords Diagnostic = (EventKeywords)4;
        public const EventKeywords Perf = (EventKeywords)8;

        private static EndpointDebugEventSource _instance = new EndpointDebugEventSource();

        public static EndpointDebugEventSource Instance
        {
            get
            {
                return _instance ?? new EndpointDebugEventSource();
            }
            internal set { _instance = value; }
        }

        //[Event(1, Message = "{0}", Level = EventLevel.Informational, Keywords = Diagnostic)]
        public void Trace(string message)
        {
            WriteEvent(1, message);
        }
    }
}
