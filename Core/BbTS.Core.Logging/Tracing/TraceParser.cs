﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Exceptions.Tracing;
using BbTS.Domain.Models.MediaServer;
using BbTS.Domain.Models.System.Logging.Tracing;
using BbTS.Monitoring.Logging;
using Microsoft.Diagnostics.Tracing.Parsers.FrameworkEventSource;
using Microsoft.Diagnostics.Tracing.Parsers.JScript;

namespace BbTS.Core.Logging.Tracing
{
    public class TraceParser
    {
        private enum TraceState
        {
            Unknown,
            Enter,
            Exit,
            Exception
        };

        public List<TraceNode> Parse(string trace)
        {
            List<TraceNode> nodeList = new List<TraceNode>();
            using (StringReader reader = new StringReader(trace))
            {
                bool eof = false;
                while (!eof)
                {
                    string line = reader.ReadLine();
                    try
                    {
                        if (line == null)
                        {
                            eof = true;
                            continue;
                        }
                        if (!String.IsNullOrEmpty(line))
                        {
                            nodeList.Add(_parse(reader, line, null));
                        }
                    }
                    catch (TraceParsingException tpx)
                    {
                        LoggingManager.Instance.LogDebugMessage(
                            String.Format("TraceParsingException caught: \r\n" +
                                          "Message: {0}\r\n" +
                                          "Offending Line: {1}", tpx.Message, line),
                                          EventLogEntryType.Information);
                    }
                }
            }
            return nodeList;
        }

        private TraceNode _parse(StringReader reader, string line, TraceNode node)
        {
            // If there is nothing more to read return what was given to us
            if (string.IsNullOrEmpty(line)) return node;

            // Parse the line that was given to us and save the result for later.
            string message;
            TraceState state;
            TraceNode parsed;
            if (!_parseLine(out state, line, out parsed, out message))
            {
                if(node == null) throw new TraceParsingException(message);
                return _parse(reader, reader.ReadLine(), node);
            }

            switch (state)
            {
                case TraceState.Enter:
                {
                    parsed.IsOpen = true;
                    if (node != null)
                    {
                        node.Children.Add(_parse(reader, reader.ReadLine(), parsed));
                        return _parse(reader, reader.ReadLine(), node);
                    }
                    return _parse(reader, reader.ReadLine(), parsed);
                }
                case TraceState.Exit:
                {
                    parsed.IsClosed = true;

                    if (parsed.MethodName == node.MethodName)
                    {
                        node.IsClosed = true;
                        node.IsOpen = false;
                        node.ExitTimeStamp = parsed.ExitTimeStamp;
                        node.ReturnValue = parsed.ReturnValue;

                        return node;
                    }
                    return _parse(reader, reader.ReadLine(), parsed);
                }
                case TraceState.Exception:
                {
                    parsed.ExceptionThrown = true;
                    if (parsed.MethodName == node.MethodName)
                    {
                        node.ExceptionThrown = true;
                        node.ExitTimeStamp = parsed.ExitTimeStamp;
                        node.ExceptionMessage = parsed.ExceptionMessage;

                        return node;
                    }
                    return _parse(reader, reader.ReadLine(), parsed);

                    //throw new TraceParsingException(message) { ExceptionNode = node };
                }
            }

            return null;
        }

        /// <summary>
        /// Parse a line of trace output and determine it's state and properties.
        /// </summary>
        /// <param name="state">Exit, Entry, Exception</param>
        /// <param name="line">line of trace from a trace dump</param>
        /// <param name="node">node to populate with trace info</param>
        /// <param name="message">results message</param>
        /// <returns>Success or not.</returns>
        private bool _parseLine(out TraceState state, string line, out TraceNode node, out string message)
        {
            state = TraceState.Unknown;
            node = new TraceNode();

            if (line.Contains("[Enter]"))
            {
                // State is enter.  Open the node.
                state = TraceState.Enter;

                // Extract the entry timestamp
                int indexofEnter = line.IndexOf("[Enter]", StringComparison.Ordinal);
                string timestring = line.Substring(0, indexofEnter);
                node.EntryTimeStamp = DateTime.Parse(timestring.Trim(new[] { '[', ']', ':', ' ' }));

                // Extrace the arguments
                if (line.Contains("arguments:"))
                {
                    // Find the index of the arguments and extract them
                    int indexOfArgs = line.IndexOf("arguments:", StringComparison.Ordinal);
                    string args = line.Substring(indexOfArgs + "arguments:".Length, line.Length - (indexOfArgs + "arguments:".Length));
                    node.Arguments = args.Trim(new[] { '(', ')', ' ', ',' });

                    // Find the index of the method and extrace the method name.
                    int indexOfMethod = line.Contains("method:") ?
                        line.IndexOf("method:", StringComparison.Ordinal) + "method:".Length :
                        indexofEnter + "[Enter]".Length;

                    // Assume the method name is unknown until found.
                    string methodName = "<Unknown>";
                    if (indexOfMethod < indexOfArgs)
                    {
                        string method = line.Substring(indexOfMethod, indexOfArgs - indexOfMethod).Trim(new[] { '(', ')', ' ' });
                        methodName = method;
                    }
                    node.MethodName = methodName;
                }

            }
            else if (line.Contains("[Exit]"))
            {
                // State is exit
                state = TraceState.Exit;

                // Extract the exit timestamp
                int exitIndex = line.IndexOf("[Exit]", StringComparison.Ordinal);
                string timestring = line.Substring(0, exitIndex) ;
                node.ExitTimeStamp = DateTime.Parse(timestring.Trim(new[] { '[', ']', ':', ' ' }));

                // Extrace the arguments
                if (line.Contains("returnValue:"))
                {
                    // Find the index of the arguments
                    int indexOfRetval = line.IndexOf("returnValue:", StringComparison.Ordinal);

                    // Arguments SHOULD be comma separated so split the raw arguments on instances of ','
                    node.ReturnValue = line.Substring(indexOfRetval + "returnValue:".Length, line.Length - (indexOfRetval + "returnValue:".Length)).Trim(new[] { '(', ')', ' ' });

                    // Find the index of the method and extrace the method name.
                    int indexOfMethod = line.Contains("method:") ?
                        line.IndexOf("method:", StringComparison.Ordinal) + "method:".Length :
                        exitIndex + "[Exit]".Length;

                    // Assume the method name is unknown until found.
                    string methodName = "<Unknown>";
                    if (indexOfMethod < indexOfRetval)
                    {
                        string method = line.Substring(indexOfMethod, indexOfRetval - indexOfMethod).Trim(new[] { '(', ')', ' ' });
                        methodName = method;
                    }
                    node.MethodName = methodName;
                }
            }
            else if (line.Contains("[Exception]"))
            {
                // State is exit
                state = TraceState.Exception;

                // Extract the exit timestamp
                int exceptionIndex = line.IndexOf("[Exception]", StringComparison.Ordinal);
                string timestring = line.Substring(0, exceptionIndex);
                node.ExitTimeStamp = DateTime.Parse(timestring.Trim(new[] { '[', ']', ':', ' ' }));

                // Extrace the arguments
                if (line.Contains("exception:"))
                {
                    // Find the index of the arguments
                    int indexOfException = line.IndexOf("exception:", StringComparison.Ordinal);

                    // Arguments SHOULD be comma separated so split the raw arguments on instances of ','
                    node.ExceptionMessage = line.Substring(indexOfException + "exception:".Length, line.Length - (indexOfException + "exception:".Length));

                    // Find the index of the method and extrace the method name.
                    int indexOfMethod = line.Contains("method:") ? 
                        line.IndexOf("method:", StringComparison.Ordinal) + "method:".Length :
                        exceptionIndex + "[Exception]".Length;

                    // Assume the method name is unknown until found.
                    string methodName = "<Unknown>";
                    if (indexOfMethod < indexOfException)
                    {
                        string method = line.Substring(indexOfMethod, indexOfException - indexOfMethod).Trim(new[] { '(', ')', ' ' });
                        methodName = method;
                    }
                    node.MethodName = methodName;
                }
            }
            else
            {
                message = String.Format("Parsed line \"{0}\" does not contain any valid node information.", line);
                return false;
            }

            message = "Success";
            return true;
        }

        private NameValuePair _parseArgument(string arg)
        {
            NameValuePair pair = new NameValuePair();
            var pairs = arg.Split(':');
            switch (pairs.Length)
            {
                case 1:
                {
                    pair.Name = pairs[0];
                }
                    break;
                case 2:
                {
                    pair.Name = pairs[0];
                    pair.Value = pairs[1];
                }
                    break;

            }

            return pair;
        }
    }
}
