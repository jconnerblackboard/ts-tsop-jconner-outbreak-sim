﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Text;
using BbTS.Core.Conversion;
using BbTS.Core.Logging.History;
using BbTS.Core.System.ExceptionHandling;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Monitoring.Logging;
using PostSharp.Aspects;

namespace BbTS.Core.Logging.Tracing
{
    /// <summary> 
    /// Aspect that, when applied on a method, emits a trace message before and 
    /// after the method execution. 
    /// </summary> 
    [Serializable]
    public class TraceAttribute : OnMethodBoundaryAspect
    {
        private string _methodName;

        /// <summary> 
        /// Method executed at build time. Initializes the aspect instance. After the execution 
        /// of <see cref="CompileTimeInitialize"/>, the aspect is serialized as a managed  
        /// resource inside the transformed assembly, and deserialized at runtime. 
        /// </summary> 
        /// <param name="method">Method to which the current aspect instance  
        /// has been applied.</param> 
        /// <param name="aspectInfo">Unused.</param> 
        public override void CompileTimeInitialize(MethodBase method, AspectInfo aspectInfo)
        {
            if (method.DeclaringType != null) _methodName = method.DeclaringType.FullName + "." + method.Name;
        }

        /// <summary> 
        /// Method invoked before the execution of the method to which the current 
        /// aspect is applied. 
        /// </summary> 
        /// <param name="args">Unused.</param> 
        public override void OnEntry(MethodExecutionArgs args)
        {
            var message = $"Enter {_methodName}( [arguments:] ";
            // Write the list of all arguments. 
            if (args.Arguments.Count == 0) message += "null ";
            for (var i = 0; i < args.Arguments.Count; i++)
            {
                message = $"{message}{args.Arguments.GetArgument(i) ?? "null"}, ";
            }
            message = message.TrimEnd(',');
            message += ")";
            TraceEventSource.Instance.Trace(message);
        }

        /// <summary> 
        /// Method invoked after successful execution of the method to which the current 
        /// aspect is applied. 
        /// </summary> 
        /// <param name="args">Unused.</param> 
        public override void OnSuccess(MethodExecutionArgs args)
        {
            var message = $"Exit {_methodName}( [returnValue:] {args.ReturnValue ?? "<null>"} )";
            TraceEventSource.Instance.Trace(message);
        }

        /// <summary> 
        /// Method invoked after failure of the method to which the current 
        /// aspect is applied. 
        /// </summary> 
        /// <param name="args">Unused.</param> 
        public override void OnException(MethodExecutionArgs args)
        {
            //Trace.Unindent();

            var stringBuilder = new StringBuilder(1024);

            // Write the exit message. 
            stringBuilder.Append(_methodName);
            stringBuilder.Append('(');

            // Write the current instance object, unless the method 
            // is static. 
            var instance = args.Instance;
            if (instance != null)
            {
                stringBuilder.Append("this=");
                stringBuilder.Append(instance);
                if (args.Arguments.Count > 0)
                    stringBuilder.Append("; ");
            }

            // Write the list of all arguments. 
            for (var i = 0; i < args.Arguments.Count; i++)
            {
                if (i > 0)
                    stringBuilder.Append(", ");
                stringBuilder.Append(args.Arguments.GetArgument(i) ?? "null");
            }

            // Write the exception message. 
            stringBuilder.AppendFormat("): Exception ");
            stringBuilder.Append(Formatting.FormatException(args.Exception));

            // Finally emit the error. 
            var message = $"[Exception] method: {_methodName}( exception: {stringBuilder} )";
            TraceEventSource.Instance.Trace(message);

            var oauthException = ExceptionAnalysisTool.HasAnOauthException(args.Exception);
            if (oauthException != null)
            {
                LoggingManager.Instance.LogDebugMessage(message, EventLogEntryType.Error, (short)LoggingDefinitions.Category.Oauth);
            }
            else
            {
                LoggingManager.Instance.LogException(message, EventLogEntryType.Error, (short)LoggingDefinitions.Category.Trace);

                // If this is a WebServiceCommunicationFailedException or has one, rethrow as a WebServiceCommunicationFailedException.
                var webCommunicationFailedException = ExceptionAnalysisTool.HasAWebServiceCommunicationFailedException(args.Exception);
                if (webCommunicationFailedException != null) throw webCommunicationFailedException;
            }
        }
    }
}
