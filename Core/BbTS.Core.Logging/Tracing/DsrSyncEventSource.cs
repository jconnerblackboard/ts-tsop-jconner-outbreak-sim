﻿using System;
using System.Diagnostics.Tracing;

namespace BbTS.Core.Logging.Tracing
{
    [EventSource(Name = "Blackboard DsrSync Eventing"/*, Guid = "{2F7581B5-457D-4F4A-821F-19F4CB28E432}"*/)]
    public class DsrSyncEventSource : EventSource
    {
        public const string EventSourceName = "Blackboard DsrSync Eventing";

        private static DsrSyncEventSource _instance = new DsrSyncEventSource();

        public static DsrSyncEventSource Instance
        {
            get { return _instance; }
            internal set { _instance = value; }
        }

        public void Trace(string timestamp, string message)
        {
            WriteEvent(1, timestamp, message);
        }
    }
}
