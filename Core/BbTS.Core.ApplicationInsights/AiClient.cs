﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using BbTS.Core.Conversion;
using BbTS.Core.Logging.Tracking;
using BbTS.Domain.Models.Communication.ApplicationInsights;
using BbTS.Domain.Models.Definitions.Communication;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.System.Logging.Tracking;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;

namespace BbTS.Core.ApplicationInsights
{
    public class AiClient
    {
        #region Properties
        private readonly ConcurrentBag<AiNode> _nodes = new ConcurrentBag<AiNode>();

        private static readonly object NodeLock = new object();

        private string ProcessName { get; set; }

        public DateTimeOffset StartTime { get; set; }

        private TelemetryClient TelemetryClient { get; }

        private Guid ActiveSessionId { get; set; }

        public int Count => _nodes.Count;
        #endregion

        public AiClient(string instrumentationKey, string institutionId)
        {
            TelemetryClient = new TelemetryClient
            {
                InstrumentationKey = instrumentationKey
            };

            if (string.IsNullOrWhiteSpace(institutionId))
            {
                institutionId = Guid.NewGuid().ToString("D");
                EventLogTracker.Instance.LogImmediate(new EventLogTrackingObject
                {
                    EventCategory = LoggingDefinitions.Category.ApplicationInsights,
                    EventDateTime = DateTime.Now,
                    EventId = LoggingDefinitions.EventId.ApplicationInsightsFailedToInitializeInstitutionId,
                    Id = Guid.NewGuid(),
                    Message = $"Failed to properly initialize the institutionId in the Application Insights client.  Falling back to {new Guid():D}",
                    Severity = EventLogEntryType.Warning
                });
            }
            TelemetryClient.Context.GlobalProperties.Add(
                "InstitutionId",
                institutionId);
        }

        #region Functions

        /// <summary>
        /// Start tracking an event hub process.
        /// </summary>
        /// <param name="processName">Name of the process to start tracking.</param>
        /// <param name="metrics">Custom metrics.</param>
        public void StartTracking(string processName, Dictionary<string, double> metrics = null)
        {
            ActiveSessionId = Guid.NewGuid();
            ProcessName = $"{processName}[{ActiveSessionId}]";
            StartTime = DateTimeOffset.Now;
            TelemetryClient.Context.Session.Id = Guid.NewGuid().ToString("D");
            TelemetryClient.Context.Operation.Id = Guid.NewGuid().ToString("D");

            _nodes.Add(new AiNode
            {
                NodeCreateDateTime = StartTime,
                Metrics = metrics,
                Type = AiNodeType.Start
            });
        }

        /// <summary>
        /// Stop tracking an event hub process.
        /// </summary>
        /// <param name="properties">Custom properties.</param>
        public void StopTracking(Dictionary<string, string> properties)
        {
            var result = SeverityLevel.Information;
            var message = $"{ProcessName}(Completed)";
            if (properties.ContainsKey("IsSuccess"))
            {
                if (!Formatting.TfStringToBool(properties["IsSuccess"]))
                {
                    result = SeverityLevel.Error;
                    message = $"{ProcessName}(Failed)";
                }
            }

            var now = DateTimeOffset.Now;
            var span = now - StartTime;

            _nodes.Add(new AiNode
            {
                Name = "Total Duration",
                NodeCreateDateTime = StartTime,
                Properties = properties,
                Type = AiNodeType.Node,
                Span = span
            });

            _nodes.Add(new AiNode
            {
                NodeCreateDateTime = now,
                Properties = properties,
                Type = AiNodeType.End,
                SeverityLevel = result,
                Message = message,
                Span = span
            });
        }

        /// <summary>
        /// Track a dependency node.
        /// </summary>
        /// <param name="name">Name of the node.</param>
        /// <param name="data">Additional (string) data to log.</param>
        /// <param name="startTime">Time the event started</param>
        /// <param name="span">Time the event took to complete.</param>
        /// <param name="target">target field (default to null)</param>
        /// <param name="resultCode">resultCode field (default to null)</param>
        public void TrackNode(string name, string data, DateTimeOffset startTime, TimeSpan span, string target = null, string resultCode = null)
        {
            _nodes.Add(new AiNode
            {
                Name = name,
                Data = data,
                NodeCreateDateTime = startTime,
                Type = AiNodeType.Node,
                Span = span
            });
        }

        /// <summary>
        /// Track a trace message.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="severity"></param>
        /// <param name="properties"></param>
        public void TrackTrace(string message, SeverityLevel severity, Dictionary<string, string> properties = null)
        {
            _nodes.Add(new AiNode
            {
                Message = message,
                SeverityLevel = severity,
                Properties = properties,
                Type = AiNodeType.Trace
            });
        }

        /// <summary>
        /// Track an exception.
        /// </summary>
        /// <param name="exception"></param>
        /// <param name="writeImmediately">writes the exception to AI and clears the client of all nodes.</param>
        /// <param name="properties"></param>
        /// <param name="metrics"></param>
        public void TrackException(Exception exception, bool writeImmediately = false, Dictionary<string, string> properties = null, Dictionary<string, double> metrics = null)
        {
            _nodes.Add(new AiNode
            {
                Exception = exception,
                Properties = properties,
                Metrics = metrics,
                Type = AiNodeType.Exception
            });
        }

        /// <summary>
        /// Track an exception.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="writeImmediately">writes the exception to AI and clears the client of all nodes.</param>
        /// <param name="properties"></param>
        /// <param name="metrics"></param>
        public void TrackException(
            string message, bool writeImmediately = false, Dictionary<string, string> properties = null, Dictionary<string, double> metrics = null)
        {
            _nodes.Add(new AiNode
            {
                Exception = new Exception(message),
                Properties = properties,
                Metrics = metrics,
                Type = AiNodeType.Exception
            });
            if (writeImmediately)
            {
                Write(true);
            }
        }

        /// <summary>
        /// Flush the telemetry client.
        /// </summary>
        public void Flush()
        {
            TelemetryClient.Flush();
        }

        /// <summary>
        /// Send all nodes to the Application Insights instance.
        /// </summary>
        public void Write(bool clear = false)
        {
            foreach (var item in _nodes)
            {
                switch (item.Type)
                {
                    case AiNodeType.Start:
                    {
                        TelemetryClient.TrackEvent(new EventTelemetry
                        {
                            Name = ProcessName,
                            Timestamp = item.NodeCreateDateTime
                        });
                    }
                        break;
                    case AiNodeType.Node:
                    {
                        TelemetryClient.TrackDependency(
                            item.Name,
                            null,
                            ProcessName,
                            item.Data,
                            item.NodeCreateDateTime,
                            item.Span,
                            item.ResultCode,
                            true);
                    }
                        break;
                    case AiNodeType.Exception:
                    {
                        TelemetryClient.TrackException(item.Exception, item.Properties, item.Metrics);
                    }
                        break;
                    case AiNodeType.Trace:
                    case AiNodeType.End:
                    {
                        TelemetryClient.TrackTrace(item.Message, item.SeverityLevel, item.Properties);
                    }
                        break;
                }
            }

            if (clear)
            {
                Clear();
            }
        }

        /// <summary>
        /// Clear out the nodes.
        /// </summary>
        public void Clear()
        {
            lock (NodeLock)
            {
                var count = _nodes.Count;
                for (var i = 0; i < count; ++i)
                {
                    _nodes.TryTake(out var node);
                }
            }
        }

        #endregion

        #region UnitTest Functions

        /// <summary>
        /// Check if a given node exists in nodes with the provided message.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool HasNodeWithMessage(string message)
        {
            return _nodes.Any(n => n.Message == message);
        }

        /// <summary>
        /// Check if a given exception node exists in nodes with the provided message.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool HasExceptionNodeWithMessage(string message)
        {
            return _nodes.Any(n => n.Exception != null && n.Exception.Message == message);
        }

        #endregion
    }
}
