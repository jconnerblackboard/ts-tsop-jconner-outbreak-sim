﻿using System;
using System.Diagnostics;
using System.Net;
using System.Runtime.Serialization.Json;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Exceptions;
using BbTS.Monitoring.Logging;

namespace BbTS.Core.Web
{
    /// <summary>
    /// 
    /// </summary>
    public class JsonFaultExceptionBehavior : WebHttpBehavior
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="endpoint"></param>
        /// <param name="endpointDispatcher"></param>
        protected override void AddServerErrorHandlers(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
            // clear default erro handlers.
            endpointDispatcher.ChannelDispatcher.ErrorHandlers.Clear();

            // add our own error handler.
            endpointDispatcher.ChannelDispatcher.ErrorHandlers.Add(new ErrorHandlerEx());
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ErrorHandlerEx : IErrorHandler
    {
        public bool HandleError(Exception error)
        {
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="error"></param>
        /// <param name="version"></param>
        /// <param name="fault"></param>
        public void ProvideFault(Exception error, MessageVersion version, ref Message fault)
        {
            LoggingManager.Instance.LogException(error, EventLogEntryType.Error, (short)LoggingDefinitions.Category.Service);

            if (error is FaultException)
            {
                var statusCode = HttpStatusCode.BadRequest;

                // extract the our FaultContract object from the exception object.
                var propertyInfo = error.GetType().GetProperty("Detail");
                if (propertyInfo != null)
                {
                    var detail = propertyInfo.GetGetMethod().Invoke(error, null);

                    // create a fault message containing our FaultContract object
                    fault = Message.CreateMessage(version, "", detail, new DataContractJsonSerializer(detail.GetType()));

                    var errorToken = detail as ExceptionResponseToken;
                    if (errorToken != null)
                    {
                        statusCode = (HttpStatusCode)errorToken.ResultDomainId;
                    }
                }

                // tell WCF to use JSON encoding rather than default XML
                var wbf = new WebBodyFormatMessageProperty(WebContentFormat.Json);
                fault.Properties.Add(WebBodyFormatMessageProperty.Name, wbf);


                // return custom error code.
                var rmp = new HttpResponseMessageProperty
                {
                    StatusCode = statusCode,
                    StatusDescription = "See fault object for more information."
                };

                // put appropraite description here..
                fault.Properties.Add(HttpResponseMessageProperty.Name, rmp);
            }

            else
            {
                fault = Message.CreateMessage(version, "", "An non-fault exception is occured.", new DataContractJsonSerializer(typeof(string)));
                var wbf = new WebBodyFormatMessageProperty(WebContentFormat.Json);
                fault.Properties.Add(WebBodyFormatMessageProperty.Name, wbf);

                // return custom error code.
                var rmp = new HttpResponseMessageProperty
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    StatusDescription = "Internal Server Error"
                };

                // put appropraite description here..
                fault.Properties.Add(HttpResponseMessageProperty.Name, rmp);
            }
        }
    }
}
