﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BbTS.Core.Web.Attribute
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class MaxLengthByPropertyAttribute : ValidationAttribute
    {
        #region Properties

        /// <summary>
        /// Gets or sets the property name that will be used during validation.
        /// </summary>
        public string Property { get; }

        /// <inheritdoc />
        public override bool RequiresValidationContext => true;

        private decimal _maxValue;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MaxLengthByPropertyAttribute"/> class.
        /// </summary>
        /// <param name="property">Property to check.</param>
        public MaxLengthByPropertyAttribute(string property)
        {
            Property = property;
        }

        #endregion

        /// <inheritdoc />
        public override string FormatErrorMessage(string name)
        {
            return ErrorMessage ?? $"{name} cannot be longer than {_maxValue}.";
        }

        /// <inheritdoc />
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            // Always valid
            if (string.IsNullOrEmpty(value as string))
                return ValidationResult.Success;

            if (validationContext == null)
                throw new ArgumentNullException(nameof(validationContext));

            var validatedProperty = validationContext.ObjectType.GetProperty(Property);
            if (validatedProperty == null)
                return new ValidationResult($"Could not find a property named '{Property}'.");

            var validatedValue = validatedProperty.GetValue(validationContext.ObjectInstance);
            _maxValue = decimal.Parse(validatedValue?.ToString() ?? "0");

            return _maxValue > 0 && ((string)value).Length > _maxValue ? new ValidationResult(FormatErrorMessage(validationContext.DisplayName)) : ValidationResult.Success;
        }
    }
}
