﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BbTS.Core.Web.Attribute
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class RequiredIfNotNullAttribute : ValidationAttribute
    {
        #region Properties

        /// <summary>
        /// Gets or sets the property names that will be used during validation.
        /// </summary>
        public string[] Properties { get; }

        /// <inheritdoc />
        public override bool RequiresValidationContext => true;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="RequiredIfNotNullAttribute"/> class.
        /// </summary>
        /// <param name="properties">Properties to check.</param>
        public RequiredIfNotNullAttribute(params string[] properties)
        {
            Properties = properties;
        }

        #endregion

        /// <inheritdoc />
        public override string FormatErrorMessage(string name)
        {
            return ErrorMessage ?? $"{name} is required.";
        }

        /// <inheritdoc />
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            // If value is not null/empty string do not check anything
            if (value != null)
                return ValidationResult.Success;

            if (validationContext == null)
                throw new ArgumentNullException(nameof(validationContext));

            foreach (var property in Properties)
            {
                var validatedProperty = validationContext.ObjectType.GetProperty(property);
                if (validatedProperty == null)
                    return new ValidationResult($"Could not find a property named '{property}'.");

                var validatedValue = validatedProperty.GetValue(validationContext.ObjectInstance);
                if (validatedValue != null)
                    return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
            }

            return ValidationResult.Success;
        }
    }
}
