﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace BbTS.Core.Web.Attribute
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public sealed class RegexIfPropertyValueAttribute : ValidationAttribute
    {
        #region Properties

        /// <summary>
        /// Gets or sets the property name that will be used during validation.
        /// </summary>
        public string Property { get; }

        /// <summary>
        /// Value of the property when regex should be used
        /// </summary>
        public object PropertyValue { get; }

        /// <summary>
        /// Regular expression pattern
        /// </summary>
        public string Pattern { get; }

        /// <inheritdoc />
        public override bool RequiresValidationContext => true;

        /// <inheritdoc />
        public override object TypeId { get; } = new object();

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="RegexIfPropertyValueAttribute"/> class.
        /// </summary>
        /// <param name="property">Property to check.</param>
        /// <param name="propertyValue">Value of property when regex should be matched</param>
        /// <param name="pattern">Regular expression pattern</param>
        public RegexIfPropertyValueAttribute(string property, object propertyValue, string pattern)
        {
            Property = property;
            PropertyValue = propertyValue;
            Pattern = pattern;
        }

        #endregion

        /// <inheritdoc />
        public override string FormatErrorMessage(string name)
        {
            return ErrorMessage ?? $"{name} contains invalid value.";
        }

        /// <inheritdoc />
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (validationContext == null)
                throw new ArgumentNullException(nameof(validationContext));

            var validatedProperty = validationContext.ObjectType.GetProperty(Property);
            if (validatedProperty == null)
                return new ValidationResult($"Could not find a property named '{Property}'.");

            var validatedValue = validatedProperty.GetValue(validationContext.ObjectInstance);

            // If one of them is null and other is not then return success
            if ((validatedValue == null && PropertyValue != null) || (validatedValue != null && PropertyValue == null))
                return ValidationResult.Success;

            // Both are null or both have some value
            // Return success if the both have different value
            if (!validatedValue?.Equals(PropertyValue) ?? false)
                return ValidationResult.Success;

            var regex = new Regex(Pattern);
            return regex.IsMatch(value?.ToString() ?? "") ? ValidationResult.Success : new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
        }
    }
}
