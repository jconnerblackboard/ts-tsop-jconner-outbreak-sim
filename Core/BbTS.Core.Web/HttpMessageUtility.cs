﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.BbSp;
using BbTS.Domain.Models.Customer.Management;

namespace BbTS.Core.Web
{
    public static class HttpMessageUtility
    {
        /// <summary>
        /// Get the <see cref="HttpStatusCode"/> associated with the error code.
        /// </summary>
        /// <param name="operation">Operation with potential error codes to parse out.</param>
        /// <param name="reasonPhrase">override the reason phrase.  null to not override.</param>
        /// <returns></returns>
        public static HttpStatusCode HttpStatusCodeFromOperation(Operation operation, out string reasonPhrase)
        {
            reasonPhrase = null;

            var error = operation.Errors?.FirstOrDefault();
            if (error == null)
            {
                return HttpStatusCode.OK;
            }

            switch (error.Code)
            {
                case ErrorMessageIdentifiers.InstitutionRouteIsNotFoundError:
                    return HttpStatusCode.Unauthorized;
                case ErrorMessageIdentifiers.CustomerWithBoardPlanIdNotFoundBusinessError:
                case ErrorMessageIdentifiers.BoardPlanWithIdNotFoundBusinessError:
                case LocalizationMessageIdentifiers.ResourceNotFoundError:
                    reasonPhrase = "The requested resource could not be found.";
                    return HttpStatusCode.NotFound;
                default:
                    return HttpStatusCode.BadRequest;
            }
        }
    }
}
