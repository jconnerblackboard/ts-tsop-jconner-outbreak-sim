﻿using System.Collections.Generic;
using BbTS.Domain.Models.Credential;

namespace BbTS.Core.Credential.Test
{
    public class CardFormatTestData
    {
        public string Name { get; set; }
        public int Seed { get; set; }
        public List<CardFormat> CardFormats { get; set; }
        public CardData CardData { get; set; }
        public CardFormat MatchedCardFormat { get; set; }
        public CardCapture CardCapture { get; set; }
        public CardCapture CardCaptureMf { get; set; }
    }
}