﻿using System;
using System.Collections.Generic;
using System.Text;
using BbTS.Domain.Models.Credential;
using BbTS.Domain.Models.Definitions.Credential;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BbTS.Core.Credential.Test
{
    [TestClass]
    public class CardAnalyzerTest
    {
        #region private

        private Random _random;


        #region Card Formats

        private readonly CardFormat _cardFormatA = new CardFormat
        {
            Name = "Format [ 1 37][19  0 FirstSeparator][ 4 19 FirstSeparator][ 3  6 TrackBeginning (789 True True)]",
            Guid = "50975778-4b36-4adc-9b33-80b33723b36e",
            IsActive = true,
            //PrioritySequence = 1,  //Using list order to define sequence for these unit tests
            MinimumDigits = 1,
            MaximumDigits = 37,
            SiteCode = new CardParseRule { Location = 6, Anchor = ParseAnchor.TrackBeginning, Length = 3 },
            SiteCodeValue = "789",
            SiteCodeCheckOnline = true,
            SiteCodeCheckOffline = true,
            CardNumber = new CardParseRule { Location = 0, Anchor = ParseAnchor.FirstSeparator, Length = 19 },
            IssueNumber = new CardParseRule { Location = 19, Anchor = ParseAnchor.FirstSeparator, Length = 4 }
        };

        private readonly CardFormat _cardFormatB = new CardFormat
        {
            Name = "Format [ 1 37][10 10 FirstSeparator][ 1 20 FirstSeparator][ 3 34 TrackBeginning (567 True True)]",
            Guid = "841880ca-22c5-4a0d-8e19-64a904cf98ab",
            IsActive = true,
            MinimumDigits = 1,
            MaximumDigits = 37,
            SiteCode = new CardParseRule { Location = 34, Anchor = ParseAnchor.TrackBeginning, Length = 3 },
            SiteCodeValue = "567",
            SiteCodeCheckOnline = true,
            SiteCodeCheckOffline = true,
            CardNumber = new CardParseRule { Location = 10, Anchor = ParseAnchor.FirstSeparator, Length = 10 },
            IssueNumber = new CardParseRule { Location = 20, Anchor = ParseAnchor.FirstSeparator, Length = 1 }
        };

        private readonly CardFormat _cardFormatC = new CardFormat
        {
            Name = "Format [ 1 19][19  0 TrackBeginning][ -  - --------------][ -  - -------------- ( False False)]",
            Guid = "ef4087b6-8f7a-4303-962f-758ce82162fd",
            IsActive = true,
            MinimumDigits = 1,
            MaximumDigits = 19,
            CardNumber = new CardParseRule { Location = 0, Anchor = ParseAnchor.TrackBeginning, Length = 19 },
        };

        private readonly CardFormat _cardFormatD = new CardFormat
        {
            Name = "Format [ 9  9][ 8  0 TrackBeginning][ 1  8 TrackBeginning][ -  - -------------- ( False False)]",
            Guid = "fa75a805-46f5-4402-9085-31e0ca44c9a8",
            IsActive = true,
            MinimumDigits = 9,
            MaximumDigits = 9,
            CardNumber = new CardParseRule { Location = 0, Anchor = ParseAnchor.TrackBeginning, Length = 8 },
            IssueNumber = new CardParseRule { Location = 8, Anchor = ParseAnchor.TrackBeginning, Length = 1 }
        };

        private readonly CardFormat _cardFormatE = new CardFormat
        {
            Name = "Format [19 19][19  0 TrackBeginning][ 4 15 TrackBeginning][12  0 TrackBeginning (987654321012 True True)]",
            Guid = "07aa2d98-03f9-4716-a15d-31554b41c058",
            IsActive = true,
            MinimumDigits = 19,
            MaximumDigits = 19,
            SiteCode = new CardParseRule { Location = 0, Anchor = ParseAnchor.TrackBeginning, Length = 12 },
            SiteCodeValue = "987654321012",
            SiteCodeCheckOnline = true,
            SiteCodeCheckOffline = true,
            CardNumber = new CardParseRule { Location = 0, Anchor = ParseAnchor.TrackBeginning, Length = 19 },
            IssueNumber = new CardParseRule { Location = 15, Anchor = ParseAnchor.TrackBeginning, Length = 4 }
        };

        #endregion

        private static void Write(string text)
        {
            System.Diagnostics.Debug.Write(text);
        }

        private static void WriteLine(string text = "")
        {
            System.Diagnostics.Debug.WriteLine(text);
        }

        #region MF4100 code equivalence

        private CardFormatMf4100 ConvertForMf4100(CardFormat cardFormat)
        {
            var cardFormatMf = new CardFormatMf4100
            {
                minDigitCount = (uint)cardFormat.MinimumDigits,
                maxDigitCount = (uint)cardFormat.MaximumDigits,
                IDLocation = (uint)cardFormat.CardNumber.Location + 1,
                IDLength = (uint)cardFormat.CardNumber.Length,
                cardTestFlag = cardFormat.CardNumber.Anchor == ParseAnchor.FirstSeparator ? 0x01u : 0x00u,
            };

            if (cardFormat.SiteCode != null)
            {
                cardFormatMf.SCLocation = (uint)cardFormat.SiteCode.Location + 1;
                cardFormatMf.SCLength = (uint)cardFormat.SiteCode.Length;
                cardFormatMf.cardTestFlag |= cardFormat.SiteCode.Anchor == ParseAnchor.FirstSeparator ? 0x04u : 0x00u;

                cardFormatMf.SC = cardFormat.SiteCodeValue.Replace('=', '-');
                cardFormatMf.lineCheckFlag = cardFormat.SiteCodeCheckOnline ? 0x02u : 0x00u;
                cardFormatMf.lineCheckFlag |= cardFormat.SiteCodeCheckOffline ? 0x20u : 0x00u;
            }

            if (cardFormat.IssueNumber != null)
            {
                cardFormatMf.issueCodeLocation = (uint)cardFormat.IssueNumber.Location + 1;
                cardFormatMf.issueCodeLength = (uint)cardFormat.IssueNumber.Length;
                cardFormatMf.cardTestFlag |= cardFormat.IssueNumber.Anchor == ParseAnchor.FirstSeparator ? 0x02u : 0x00u;
            }

            return cardFormatMf;
        }

        private CardCapture MfParseTrack2(CardData cardData, List<CardFormat> cardFormats, bool isOnline)
        {
            //Convert cardFormats list into the format expected by the MF4100 code
            CardFormatMf4100[] cardFormatsMf4100 = new CardFormatMf4100[cardFormats.Count];
            for (int i = 0; i < cardFormats.Count; i++)
            {
                cardFormatsMf4100[i] = ConvertForMf4100(cardFormats[i]);
            }

            //Convert cardData into the format expected by the MF4100 code
            CardDataMf4100 cardDataMf = new CardDataMf4100 { data = cardData.Track2.Replace(cardData.Track2FieldSeparator, '-') };

            //Perform the parse
            var parseResult = cardDataMf.parseSwipeCardData(cardFormatsMf4100, isOnline);
            cardDataMf.isTransactionSystemCard = parseResult == CardDataMf4100.csReturn.CSRETURN_VALID;

            if (parseResult == CardDataMf4100.csReturn.CSRETURN_VALID)
            {
                return new CardCapture { CardNumber = cardDataMf.getCardNumDSAC(), IssueNumber = cardDataMf.getIssueNumber(), IssueNumberCaptured = true };
            }

            return null;
        }

        #endregion

        private string RandomDigitSequence(int count)
        {
            StringBuilder sb = new StringBuilder(count);
            for (int i = 0; i < count; i++)
            {
                sb.Append(_random.Next(10));
            }

            return sb.ToString();
        }

        private string RuleToString(CardParseRule rule)
        {
            if (rule == null) return " -  - --------------";
            return $"{rule.Length,2} {rule.Location,2} {rule.Anchor}";
        }

        /// <summary>
        /// Generates a random card format.  If track2 is passed in, and if a site code rule is generated, the generated rule will be valid for track2.
        /// </summary>
        /// <param name="track2"></param>
        /// <returns></returns>
        private CardFormat GenerateRandomCardFormat(string track2 = null)
        {
            //Min and max track digits
            var maxTrackDigits = string.IsNullOrEmpty(track2) ? _random.Next(1, 37 + 1) : track2.Length;
            int minTrackDigits = _random.Next(1, maxTrackDigits + 1);

            //Card number rule
            int maxCardNumberLength = maxTrackDigits < 19 ? maxTrackDigits : 19;
            int cardNumberLength = _random.Next(1, maxCardNumberLength + 1);
            var cardNumberRule = new CardParseRule
            {
                Length = cardNumberLength,
                Location = _random.Next(0, maxTrackDigits - cardNumberLength + 1),
                Anchor = _random.Next(2) == 0 ? ParseAnchor.TrackBeginning : ParseAnchor.FirstSeparator
            };

            //Issue number rule
            CardParseRule issueNumberRule = null;
            if (_random.Next(2) == 1)
            {
                int maxIssueNumberLength = maxTrackDigits < 4 ? maxTrackDigits : 4;
                int issueNumberLength = _random.Next(1, maxIssueNumberLength + 1);
                issueNumberRule = new CardParseRule
                {
                    Length = issueNumberLength,
                    Location = _random.Next(0, maxTrackDigits - issueNumberLength + 1),
                    Anchor = _random.Next(2) == 0 ? ParseAnchor.TrackBeginning : ParseAnchor.FirstSeparator
                };
            }

            //Site code rule
            CardParseRule siteCodeRule = null;
            string siteCodeValue = string.Empty;
            bool siteCodeCheckOnline = false;
            bool siteCodeCheckOffline = false;
            if (_random.Next(2) == 1)
            {
                int maxSiteCodeLength = maxTrackDigits < 12 ? maxTrackDigits : 12;
                int siteCodeLength = _random.Next(1, maxSiteCodeLength + 1);
                siteCodeRule = new CardParseRule
                {
                    Length = siteCodeLength,
                    Location = _random.Next(0, maxTrackDigits - siteCodeLength + 1),
                    Anchor = _random.Next(2) == 0 ? ParseAnchor.TrackBeginning : ParseAnchor.FirstSeparator
                };

                if (string.IsNullOrEmpty(track2))
                {
                    siteCodeValue = RandomDigitSequence(siteCodeLength);
                    siteCodeCheckOnline = _random.Next(2) == 1;
                    siteCodeCheckOffline = _random.Next(2) == 1;
                }
                else
                {
                    siteCodeRule.Anchor = ParseAnchor.TrackBeginning;
                    siteCodeValue = track2.Substring(siteCodeRule.Location, siteCodeRule.Length);
                    siteCodeCheckOnline = true;
                    siteCodeCheckOffline = true;
                }
            }

            string name = $"Format [{minTrackDigits,2} {maxTrackDigits,2}][{RuleToString(cardNumberRule)}][{RuleToString(issueNumberRule)}][{RuleToString(siteCodeRule)} ({siteCodeValue} {siteCodeCheckOnline} {siteCodeCheckOffline})]";

            return new CardFormat
            {
                Name = name,
                Guid = Guid.NewGuid().ToString("D"),
                IsActive = true,
                MinimumDigits = minTrackDigits,
                MaximumDigits = maxTrackDigits,
                SiteCode = siteCodeRule,
                SiteCodeValue = siteCodeValue,
                SiteCodeCheckOnline = siteCodeCheckOnline,
                SiteCodeCheckOffline = siteCodeCheckOffline,
                CardNumber = cardNumberRule,
                IssueNumber = issueNumberRule
            };
        }

        private CardData GenerateRandomCardData(bool usePredefinedDigits = false)
        {
            //Generated card data will always be random length, but we can use either pre-defined digits or random digits
            string digits = "1234567890987654321051739246808642937"; //37 digits, no two digit sequence appears in the string more than once.
            digits = usePredefinedDigits ? digits.Substring(0, _random.Next(1, 37 + 1)) : RandomDigitSequence(_random.Next(1, 37 + 1));

            //Presence and index of field separator, -1 indicates no field separator
            int fieldSeparatorIndex;
            switch (_random.Next(2))
            {
                case 1:
                    fieldSeparatorIndex = _random.Next(0, digits.Length);
                    break;
                default:
                    fieldSeparatorIndex = -1;
                    break;
            }

            //Card data
            return new CardData { Track2 = fieldSeparatorIndex < 0 ? digits : digits.Remove(fieldSeparatorIndex, 1).Insert(fieldSeparatorIndex, "=") };
        }

        private static CardCapture NormalizeCardCapture(CardCapture cardCapture)
        {
            if (cardCapture == null) return null;

            var normalizedCardCapture = new CardCapture
            {
                CardNumber = cardCapture.CardNumber.Replace('-', '=').PadLeft(19, '0'),
                IssueNumber = cardCapture.IssueNumber.Replace('D', '='),
                IssueNumberCaptured = cardCapture.IssueNumberCaptured
            };
            return normalizedCardCapture;
        }

        private void OutputCardFormatTest(CardFormatTestData test)
        {
            WriteLine($"{test.Name} (Seed = {test.Seed})");
            WriteLine("       [Track][Card Number Rule    ][Issue Number Rule   ][Site Code Rule                                ]");
            WriteLine("       [Mn Mx][Ln Lo ParseAnchor   ][Ln Lo ParseAnchor   ][Ln Lo ParseAnchor    (SiteCode Online Offline)]");
            WriteLine("----------------------------------------------------------------------------------------------------------");
            foreach (var cf in test.CardFormats)
            {
                WriteLine(cf.Name);
            }
            WriteLine();

            WriteLine($"CardData.Track2({test.CardData.Track2.Length,2:D2}):  {test.CardData.Track2}");
            if (test.CardCapture != null)
            {
                int fieldSeparatorIndex = test.CardData.Track2.IndexOf(test.CardData.Track2FieldSeparator);

                Write("Card Number        :  ");
                int startIndex = test.MatchedCardFormat.CardNumber.Anchor == ParseAnchor.FirstSeparator ? fieldSeparatorIndex + 1 : 0;
                startIndex += test.MatchedCardFormat.CardNumber.Location;
                WriteLine($"{test.CardCapture.CardNumber.PadLeft(startIndex + test.CardCapture.CardNumber.Length)}");

                Write("Issue Code         :  ");
                if (test.MatchedCardFormat.IssueNumber != null)
                {
                    startIndex = test.MatchedCardFormat.IssueNumber.Anchor == ParseAnchor.FirstSeparator ? fieldSeparatorIndex + 1 : 0;
                    startIndex += test.MatchedCardFormat.IssueNumber.Location;
                    Write($"{test.CardCapture.IssueNumber.PadLeft(startIndex + test.CardCapture.IssueNumber.Length)}");
                }

                WriteLine();

                Write("Site Code          :  ");
                if ((test.MatchedCardFormat.SiteCode != null) && test.MatchedCardFormat.SiteCodeCheckOnline)
                {
                    startIndex = test.MatchedCardFormat.SiteCode.Anchor == ParseAnchor.FirstSeparator ? fieldSeparatorIndex + 1 : 0;
                    startIndex += test.MatchedCardFormat.SiteCode.Location;
                    Write($"{test.MatchedCardFormat.SiteCodeValue.PadLeft(startIndex + test.MatchedCardFormat.SiteCode.Length)}");
                }

                WriteLine();
                WriteLine();
                WriteLine(test.MatchedCardFormat.Name);
            }

            var normalizedCardCapture = NormalizeCardCapture(test.CardCapture);
            var normalizedCardCaptureMf = NormalizeCardCapture(test.CardCaptureMf);
            WriteLine();
            WriteLine("                 Card Number          Issue Code");
            WriteLine(normalizedCardCapture != null ? $"New code parse:  {normalizedCardCapture.CardNumber}  {normalizedCardCapture.IssueNumber}" : string.Empty);
            WriteLine(normalizedCardCaptureMf != null ? $"Old code parse:  {normalizedCardCaptureMf.CardNumber}  {normalizedCardCaptureMf.IssueNumber}" : string.Empty);
            WriteLine();
        }

        #endregion

        [TestMethod]
        public void TestMethodRandom()
        {
            var failedTests = new List<CardFormatTestData>();
            int j;
            int seed = Environment.TickCount;
            int maxTests = 10000;
            int matchCount = 0;
            int noMatchCount = 0;

            for (j = 0; j < maxTests; j++, seed++)
            {
                var test = new CardFormatTestData { Name = $"Test {j}", Seed = seed };
                _random = new Random(test.Seed);

                //Card format list
                CardFormat cardFormat;
                test.CardFormats = new List<CardFormat>();
                int numberOfCardFormats = _random.Next(1, 10 + 1);
                for (int i = 0; i < numberOfCardFormats; i++)
                {
                    cardFormat = GenerateRandomCardFormat();
                    cardFormat.PrioritySequence = i;
                    test.CardFormats.Add(cardFormat);
                }

                //Card data (track 2)
                test.CardData = GenerateRandomCardData(true);

                //Generate a card format that has a valid site code rule for the cardData we've generated.  (The card number and issue number rules can still be invalid.)
                if (_random.Next(2) == 1)
                {
                    cardFormat = GenerateRandomCardFormat(test.CardData.Track2);
                    cardFormat.PrioritySequence = numberOfCardFormats;
                    test.CardFormats.Add(cardFormat);
                }

                CardFormat matchedCardFormat;
                test.CardCapture = CardAnalyzer.ParseTrack2(test.CardData, test.CardFormats, true, out matchedCardFormat);
                test.MatchedCardFormat = matchedCardFormat;
                test.CardCaptureMf = MfParseTrack2(test.CardData, test.CardFormats, true);

                //OutputCardFormatTest(test);

                var normalizedCardCapture = NormalizeCardCapture(test.CardCapture);
                var normalizedCardCaptureMf = NormalizeCardCapture(test.CardCaptureMf);

                //No match
                if ((normalizedCardCapture == null) && (test.CardCaptureMf == null))
                {
                    noMatchCount++;
                }
                else if (((normalizedCardCapture == null) && (normalizedCardCaptureMf != null)) ||
                         ((normalizedCardCapture != null) && (normalizedCardCaptureMf == null)))
                {
                    failedTests.Add(test);
                }
                else if ((normalizedCardCapture != null) && (test.CardCaptureMf != null) &&
                         ((normalizedCardCapture.CardNumber != normalizedCardCaptureMf.CardNumber) ||
                          (normalizedCardCapture.IssueNumber != normalizedCardCaptureMf.IssueNumber) ||
                          (normalizedCardCapture.IssueNumberCaptured != normalizedCardCaptureMf.IssueNumberCaptured)))
                {
                    failedTests.Add(test);
                }
                else
                {
                    matchCount++;
                }
            }

            WriteLine($"Match count:  {matchCount}");
            WriteLine($"No-match count:  {noMatchCount}");
            WriteLine($"Failed test count:  {failedTests.Count}");

            if (failedTests.Count > 0)
            {
                foreach (var test in failedTests)
                {
                    OutputCardFormatTest(test);
                }

                Assert.Fail($"{failedTests.Count} out of {j} tests failed.");
            }
        }

        /// <summary>
        /// Single card format
        /// </summary>
        [TestMethod]
        public void TestMethod1()
        {
            var test = new CardFormatTestData
            {
                Name = "TestMethod1",
                CardFormats = new List<CardFormat> { _cardFormatA },
                CardData = new CardData { Track2 = "123456789=123456789012345678901234567" },
            };

            //Library parse
            CardFormat matchedCardFormat;
            test.CardCapture = CardAnalyzer.ParseTrack2(test.CardData, test.CardFormats, true, out matchedCardFormat);
            test.MatchedCardFormat = matchedCardFormat;

            //Parse verification
            Assert.IsNotNull(test.CardCapture, "cardCapture is null.  (ParseTrack2 failed.)");
            Assert.AreEqual("1234567890123456789", test.CardCapture.CardNumber, "Card number is wrong.");
            Assert.AreEqual("0123", test.CardCapture.IssueNumber, "Issue number is wrong.");
            Assert.IsTrue(test.CardCapture.IssueNumberCaptured, "IssueNumberCaptured is false.");

            //MF4100 parse (for control purposes)
            //Note that the MF4100 parsing logic also 0 pads the card number to 19 digits, so we must take this into account when comparing results
            test.CardCaptureMf = MfParseTrack2(test.CardData, test.CardFormats, true);
            Assert.IsNotNull(test.CardCaptureMf, "cardCaptureMf is null.  (MfParseTrack2 failed.)");
            Assert.AreEqual(test.CardCaptureMf.CardNumber, test.CardCapture.CardNumber.PadLeft(19, '0'), "Parsed card number does not match MF4100 parse.");
            Assert.AreEqual(test.CardCaptureMf.IssueNumber, test.CardCapture.IssueNumber, "Parsed issue number does not match MF4100 parse.");
            Assert.AreEqual(test.CardCaptureMf.IssueNumberCaptured, test.CardCapture.IssueNumberCaptured, "IssueNumberCaptured value does not match MF4100 value.");

            //Output
            OutputCardFormatTest(test);
        }

        [TestMethod]
        public void TestMethod2()
        {
            var test = new CardFormatTestData
            {
                Name = "TestMethod2",
                CardFormats = new List<CardFormat> { _cardFormatA, _cardFormatB },
                CardData = new CardData { Track2 = "000000000=123456789012345678901234567" },
            };

            //Library parse
            CardFormat matchedCardFormat;
            test.CardCapture = CardAnalyzer.ParseTrack2(test.CardData, test.CardFormats, true, out matchedCardFormat);
            test.MatchedCardFormat = matchedCardFormat;

            //Parse verification
            Assert.IsNotNull(test.CardCapture, "cardCapture is null.  (ParseTrack2 failed.)");
            Assert.AreEqual("1234567890", test.CardCapture.CardNumber, "Card number is wrong.");
            Assert.AreEqual("1", test.CardCapture.IssueNumber, "Issue number is wrong.");
            Assert.IsTrue(test.CardCapture.IssueNumberCaptured, "IssueNumberCaptured is false.");

            //MF4100 parse (for control purposes)
            //Note that the MF4100 parsing logic also 0 pads the card number to 19 digits, so we must take this into account when comparing results
            test.CardCaptureMf = MfParseTrack2(test.CardData, test.CardFormats, true);
            Assert.IsNotNull(test.CardCaptureMf, "cardCaptureMf is null.  (MfParseTrack2 failed.)");
            Assert.AreEqual(test.CardCaptureMf.CardNumber, test.CardCapture.CardNumber.PadLeft(19, '0'), "Parsed card number does not match MF4100 parse.");
            Assert.AreEqual(test.CardCaptureMf.IssueNumber, test.CardCapture.IssueNumber, "Parsed issue number does not match MF4100 parse.");
            Assert.AreEqual(test.CardCaptureMf.IssueNumberCaptured, test.CardCapture.IssueNumberCaptured, "IssueNumberCaptured value does not match MF4100 value.");

            //Output
            OutputCardFormatTest(test);
        }

        [TestMethod]
        public void TestMethod3()
        {
            var test = new CardFormatTestData
            {
                Name = "TestMethod3",
                CardFormats = new List<CardFormat> { _cardFormatA, _cardFormatB, _cardFormatC, _cardFormatD },
                CardData = new CardData { Track2 = "123456789" },
            };

            //Library parse
            CardFormat matchedCardFormat;
            test.CardCapture = CardAnalyzer.ParseTrack2(test.CardData, test.CardFormats, true, out matchedCardFormat);
            test.MatchedCardFormat = matchedCardFormat;

            //Parse verification
            Assert.IsNotNull(test.CardCapture, "cardCapture is null.  (ParseTrack2 failed.)");
            Assert.AreEqual("12345678", test.CardCapture.CardNumber, "Card number is wrong.");
            Assert.AreEqual("9", test.CardCapture.IssueNumber, "Issue number is wrong.");
            Assert.IsTrue(test.CardCapture.IssueNumberCaptured, "IssueNumberCaptured is false.");

            //MF4100 parse (for control purposes)
            //Note that the MF4100 parsing logic also 0 pads the card number to 19 digits, so we must take this into account when comparing results
            test.CardCaptureMf = MfParseTrack2(test.CardData, test.CardFormats, true);
            Assert.IsNotNull(test.CardCaptureMf, "cardCaptureMf is null.  (MfParseTrack2 failed.)");
            Assert.AreEqual(test.CardCaptureMf.CardNumber, test.CardCapture.CardNumber.PadLeft(19, '0'), "Parsed card number does not match MF4100 parse.");
            Assert.AreEqual(test.CardCaptureMf.IssueNumber, test.CardCapture.IssueNumber, "Parsed issue number does not match MF4100 parse.");
            Assert.AreEqual(test.CardCaptureMf.IssueNumberCaptured, test.CardCapture.IssueNumberCaptured, "IssueNumberCaptured value does not match MF4100 value.");

            //Output
            OutputCardFormatTest(test);
        }

        [TestMethod]
        public void TestMethod4()
        {
            var test = new CardFormatTestData
            {
                Name = "TestMethod4",
                CardFormats = new List<CardFormat> { _cardFormatA, _cardFormatB, _cardFormatD, _cardFormatE },
                CardData = new CardData { Track2 = "9876543210123456789" },
            };

            //Library parse
            CardFormat matchedCardFormat;
            test.CardCapture = CardAnalyzer.ParseTrack2(test.CardData, test.CardFormats, true, out matchedCardFormat);
            test.MatchedCardFormat = matchedCardFormat;

            //Parse verification
            Assert.IsNotNull(test.CardCapture, "cardCapture is null.  (ParseTrack2 failed.)");
            Assert.AreEqual("9876543210123456789", test.CardCapture.CardNumber, "Card number is wrong.");
            Assert.AreEqual("6789", test.CardCapture.IssueNumber, "Issue number is wrong.");
            Assert.IsTrue(test.CardCapture.IssueNumberCaptured, "IssueNumberCaptured is false.");

            //MF4100 parse (for control purposes)
            //Note that the MF4100 parsing logic also 0 pads the card number to 19 digits, so we must take this into account when comparing results
            test.CardCaptureMf = MfParseTrack2(test.CardData, test.CardFormats, true);
            Assert.IsNotNull(test.CardCaptureMf, "cardCaptureMf is null.  (MfParseTrack2 failed.)");
            Assert.AreEqual(test.CardCaptureMf.CardNumber, test.CardCapture.CardNumber.PadLeft(19, '0'), "Parsed card number does not match MF4100 parse.");
            Assert.AreEqual(test.CardCaptureMf.IssueNumber, test.CardCapture.IssueNumber, "Parsed issue number does not match MF4100 parse.");
            Assert.AreEqual(test.CardCaptureMf.IssueNumberCaptured, test.CardCapture.IssueNumberCaptured, "IssueNumberCaptured value does not match MF4100 value.");

            //Output
            OutputCardFormatTest(test);
        }
    }
}



