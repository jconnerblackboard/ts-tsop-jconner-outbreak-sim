﻿using System;
using System.Text;

namespace BbTS.Core.Credential.Test
{
    /// <summary>
    /// Card format logic from the MF4100 code
    /// </summary>
    public class CardDataMf4100
    {
        public string data = "";
        public string type = "";

        private string cardId = "";
        private string issueNumStr;
        private string mgrIdStr;
        private byte[] cardNum = new byte[20];
        private byte[] encodedCardId = new byte[10];
        private byte[] encodedCardIssue = new byte[2];

        private const uint CheckScOnline = 0x02;
        private const uint CheckScOffline = 0x20;

        private const uint IDLocIndex = 1;
        private const uint ISSLocIndex = 2;
        private const uint SCLocIndex = 4;

        private uint cardFSloc = 0;
        private const byte fieldSeparator = 0x2d; // '=' transposed by msc driver to '-'

        private ASCIIEncoding aenc = new ASCIIEncoding();

        public string getCardNumDSAC()
        {
            //I believe that cardNum was padded to 20 bytes because it was going into packed BCD for the Bb Readers devices
            //For DSAC, we only want 19 digits, so we'll start our index at 1 to remove the left-most 0 pad char.  -Dave
            string cardNumber = string.Empty;
            for (int i = 1; i < cardNum.Length; i++)
            {
                cardNumber += ((char)cardNum[i]).ToString();
            }

            return cardNumber;
        }

        public byte[] getEncodedCard()
        {
            return encodedCardId;
        }

        public byte[] getEncodedIssueId()
        {
            return encodedCardIssue;
        }

        public string getIssueNumber()
        {
            return issueNumStr;
        }

        public string Track1 = "";
        public string Track2
        {
            // track2 is just an "alias" for data
            get
            {
                return data;
            }
        }

        public bool isCreditCard { get; set; }
        public bool isTransactionSystemCard { get; set; }

        public string getTrack1()
        {
            return Track1;
        }

        public void setTrack1(string s)
        {
            Track1 = s;
        }

        //public bool isServiceCard()
        //{
        //    if (cardId == null)
        //        return false;

        //    if (SystemConfiguration.SpecialCardIDs.isServiceCard(cardId))
        //    {
        //        if (type.Contains("MAGSTRIPE") && SystemConfiguration.getSystemConfig().serviceCardAccess)
        //            return true;
        //        else if (type.Contains("FELICA") && SystemConfiguration.getSystemConfig().feliCaserviceCardAccess)
        //            return true;
        //    }

        //    return false;
        //}

        public enum csReturn
        {
            CSRETURN_IGNORE = 0,
            CSRETURN_INVALID = 1,
            CSRETURN_VALID = 2
        }

        //public bool isManagerCard()
        //{
        //    if (cardId == null)
        //        return false;

        //    if (SystemConfiguration.getSystemConfig().managerCardIDs == null || SystemConfiguration.getSystemConfig().managerCardIDs.Length == 0)
        //        return false;

        //    foreach (ManagerCardID mc in SystemConfiguration.getSystemConfig().managerCardIDs)
        //    {
        //        if (mc.manId == mgrIdStr)
        //        {
        //            if (SystemConfiguration.getSystemConfig().hostType.ToLower() == "saas")
        //                return true;
        //            if ((mc.issueNumber == issueNumStr) || issueNumStr.Equals("B"))
        //            {
        //                return true;
        //            }
        //        }
        //    }
        //    return false;
        //}

        bool? _CardIsValid = null;
        public bool isValidCard(CardFormatMf4100[] cardFormats, bool isOnline)
        {
            if (!_CardIsValid.HasValue)
            {
                //if (SystemConfiguration.getSystemConfig().hostType.ToLower() == "transact")
                    ValidateCard(isOnline, cardFormats);
                //else
                //    SaaSValidateCard();
            }
            return _CardIsValid.Value;
        }

        //public bool SaaSValidateCard()
        //{
        //    int i, n;

        //    cardFSloc = 0;
        //    for (i = 0; i < cardId.Length; i++)
        //        if ((cardId[i] == fieldSeparator) && (cardFSloc == 0))
        //            cardFSloc = (uint)(i + 1);

        //    // Prefilled in case something downstream might need something here.
        //    // If not needed, may be deleted.
        //    for (i = 0; i < cardNum.Length - 1; i++)
        //        cardNum[i] = (byte)'0';

        //    mgrIdStr = "";

        //    foreach (CardFormat cf in SystemConfiguration.getSystemConfig().cardFormats)
        //    {
        //        if (cf == null)
        //            continue;

        //        //Diag.Putline("cardId Length={0}, cf minDigitCount={1}. cf maxDigitCount={2}", cardId.Length, cf.minDigitCount, cf.maxDigitCount);
        //        if (cardId.Length < cf.minDigitCount || cardId.Length > cf.maxDigitCount)
        //            continue;

        //        if (cf.BV == false)
        //        {   // check for manager encoded card
        //            foreach (ManagerCardID mc in SystemConfiguration.getSystemConfig().managerCardIDs)
        //            {
        //                if (mc.manId == cardId)
        //                    mgrIdStr = cardId;
        //            }
        //            _CardIsValid = true;
        //            return _CardIsValid.Value;
        //        }
        //        else
        //        {
        //            try
        //            {
        //                int low, high, binvalue;
        //                bool isSuccessLow = TryParse(cf.BVLow, out low);
        //                bool isSuccessHigh = TryParse(cf.BVHigh, out high);

        //                StringBuilder sb = new StringBuilder();
        //                n = cf.BVLow.Length;
        //                for (i = 0; n > 0; i++, n--)
        //                    sb.Append(cardId[i]);
        //                string binstring = sb.ToString();
        //                bool isSuccessValue = TryParse(binstring, out binvalue);
        //                //Diag.Putline("SaaSValidateCard: binvalue={0} low={1}, high={2}", binvalue, low, high);

        //                if (!isSuccessLow || !isSuccessHigh || !isSuccessValue)
        //                    continue;
        //                if (binvalue < low || binvalue > high)
        //                    continue;
        //            }
        //            catch (Exception ex)
        //            {
        //                AppLog.logException("CardSwipeMonitor exception: ", ex);
        //            }
        //        }
        //        //Diag.Putline("SaaSValidateCard: VALID");
        //        // check for manager encoded card
        //        foreach (ManagerCardID mc in SystemConfiguration.getSystemConfig().managerCardIDs)
        //        {
        //            if (mc.manId == cardId)
        //                mgrIdStr = cardId;
        //        }
        //        _CardIsValid = true;
        //        return _CardIsValid.Value;
        //    }
        //    _CardIsValid = false;
        //    return _CardIsValid.Value;
        //}

        static bool TryParse(string s, out int value)
        {
            try
            {
                value = int.Parse(s);
                return true;
            }
            catch
            {
                value = 0;
                return false;
            }
        }

        public bool ValidateCard(bool isOnline, CardFormatMf4100[] cardFormats)
        {
            uint scLoc, scLen, idLoc, idLen, issLoc, issLen;

            byte[] issueNum = new byte[4];
            byte[] cardISS = new byte[2];
            int i, j, n;

            //bool isOnline = false;

            // If biometric, skip card format checks.  Data from biometric reader is just Card Number (not full track 2 data).
            // Also setup to skip issue number checks by setting to "B"
            if (type.Contains("BIOMETRIC"))
            {
                // Build cardNum array (20 bytes padded to left with '0') and mgrIdStr (19 characters - padded to left with '0')
                mgrIdStr = "";

                for (i = 0; i < (20 - data.Length); i++)
                {
                    cardNum[i] = (byte)'0';
                    if (i > 0)
                    {
                        mgrIdStr += "0";
                    }
                }

                if (i < 20)
                {
                    for (j = 0; i < 20; i++, j++)
                    {
                        cardNum[i] = (byte)data[j];
                        mgrIdStr += string.Format("{0:X}", cardNum[i] & 0x0f);
                    }
                }

                // Pack into 10 byte packed BCD and set upper nibble to 'F'
                for (i = n = 0; i < 10; i++)
                {
                    encodedCardId[i] = (byte)((cardNum[n] & 0x0f) << 4);
                    encodedCardId[i] |= (byte)(cardNum[n + 1] & 0x0f);
                    n += 2;
                }
                encodedCardId[0] |= 0xf0;

                encodedCardIssue[0] = 0xFF;
                encodedCardIssue[1] = 0xFB;

                // Manager card issue number save
                issueNumStr = "B";

                _CardIsValid = true;
                return _CardIsValid.Value;
            }


            //if (PluginManager.ReaderTypeIsDSAC())
            //{
            //    isOnline = true;  //todo - isOnline should be determined by checking the status of Pos4100.Instance.HostConnection.Online.  Might not be straightforward given current architecture.
            //}
            //else
            //{
            //    isOnline = HostCommMonitor.isOnline();
            //}

            cardFSloc = 0;
            for (i = 0; i < cardId.Length; i++)
                if ((cardId[i] == fieldSeparator) && (cardFSloc == 0))
                    cardFSloc = (uint)(i + 1);

            foreach (CardFormatMf4100 cf in cardFormats)
            {
                if (cf == null)
                    continue;

                if (cardId.Length < cf.minDigitCount || cardId.Length > cf.maxDigitCount)
                    continue;

                bool isSiteCoded = (cf.SCLength > 0);
                bool isOkOnline = (isOnline && (cf.lineCheckFlag & CheckScOnline) == CheckScOnline);
                bool isOkOffline = (!isOnline && (cf.lineCheckFlag & CheckScOffline) == CheckScOffline);

                if (isSiteCoded && (isOkOnline || isOkOffline))
                {
                    /*** Check and validate the optional Site Code ***/
                    scLoc = cf.SCLocation;
                    scLen = cf.SCLength;
                    if ((cf.cardTestFlag & SCLocIndex) == SCLocIndex)
                    {
                        if (cardFSloc == 0)
                            continue;
                        scLoc += cardFSloc;
                    }
                    if ((scLoc < 1) || (scLoc > 37))
                        continue;
                    if (scLen > 12)
                        continue;
                    scLoc--; // convert index from 1-based to 0-based

                    // Handle case where fewer digits on card than loc + len
                    if ((scLoc + scLen) > cardId.Length)
                        continue;

                    for (j = 0, i = (int)scLoc, n = (int)scLen; n > 0; i++, j++, n--)
                        if (cardId[i] != cf.SC[j])
                            break;
                    if (n != 0)
                        continue;
                }

                /*** Check and validate the ID Number ***/
                idLen = cf.IDLength;
                idLoc = cf.IDLocation;
                if ((cf.cardTestFlag & IDLocIndex) == IDLocIndex)
                {
                    if (cardFSloc == 0)
                        continue;
                    idLoc += cardFSloc;
                }
                if ((idLoc < 1) || (idLoc > 37))
                    continue;
                if (idLen > 19)
                    continue;
                idLoc--;    // convert index from 1-based to 0-based

                // Handle case where fever digits on card than loc + len
                if ((idLoc + idLen) > cardId.Length)
                    continue;

                // Build cardNum array (20 bytes padded to left with '0') and mgrIdStr (19 characters - padded to left with '0')
                mgrIdStr = "";

                for (i = 0; i < (20 - idLen); i++)
                {
                    cardNum[i] = (byte)'0';
                    if (i > 0)
                    {
                        mgrIdStr += "0";
                    }
                }

                if (i < 20)
                {
                    for (j = (int)idLoc; i < 20; i++, j++)
                    {
                        cardNum[i] = (byte)cardId[j];
                        mgrIdStr += string.Format("{0:X}", cardNum[i] & 0x0f);
                    }
                }

                // Pack into 10 byte packed BCD and set upper nibble to 'F'
                for (i = n = 0; i < 10; i++)
                {
                    encodedCardId[i] = (byte)((cardNum[n] & 0x0f) << 4);
                    encodedCardId[i] |= (byte)(cardNum[n + 1] & 0x0f);
                    n += 2;
                }
                encodedCardId[0] |= 0xf0;

                /*** Check and validate the optional Issue Code ***/
                issLen = cf.issueCodeLength;
                issLoc = cf.issueCodeLocation;

                // Skip issue code checks if issue code length is 0 (issue code not active)
                if (issLen > 0)
                {
                    if ((cf.cardTestFlag & ISSLocIndex) == ISSLocIndex)
                    {
                        if (cardFSloc == 0)
                            continue;
                        issLoc += cardFSloc;
                    }

                    if ((issLoc < 1) || (issLoc > 37))
                        continue;
                    if (issLen > 4)
                        continue;

                }

                issLoc--;   // convert index from 1-based to 0-based

                // Skip issue code checks if issue code length is 0 (issue code not active)
                if (issLen > 0)
                {
                    // Handle case where fewer digits on card than loc + len
                    if ((issLoc + issLen) > cardId.Length)
                        continue;
                }

                // Copy issue number padded to left with 'F'
                for (i = 0; i < (4 - issLen); i++)
                    issueNum[i] = 0x0F;

                if (i < 4)
                {
                    for (j = (int)issLoc; i < 4; i++, j++)
                        issueNum[i] = (byte)cardId[j];
                }

                // Pack into 2 byte packed BCD
                encodedCardIssue[0] = (byte)((issueNum[0] & 0x0f) << 4);
                encodedCardIssue[0] |= (byte)(issueNum[1] & 0x0f);
                encodedCardIssue[1] = (byte)((issueNum[2] & 0x0f) << 4);
                encodedCardIssue[1] |= (byte)(issueNum[3] & 0x0f);

                // Manager card issue number save
                issueNumStr = "";
                byte bi;
                for (i = 0; i < 2; i++)
                {
                    bi = (byte)((encodedCardIssue[i] >> 4) & 0x0f);
                    if (bi != 0x0f)
                        issueNumStr += string.Format("{0:X}", bi);
                    bi = (byte)(encodedCardIssue[i] & 0x0f);
                    if (bi != 0x0f)
                        issueNumStr += string.Format("{0:X}", bi);
                }

                _CardIsValid = true;
                return _CardIsValid.Value;
            }

            _CardIsValid = false;
            return _CardIsValid.Value;
        }



        // data contains T2 data from MSCdriver:
        //      “xxxxxxxxxxxxxxx”   T2 card data
        //      “ERROR xxxxxxxx”    Error number
        public CardDataMf4100.csReturn parseSwipeCardData(CardFormatMf4100[] cardFormats, bool isOnline)
        {
            try
            {
                _CardIsValid = null;

                if (data != null)
                {
                    if (data.Length > 1)
                    {
                        if (!data.Contains("ERROR"))
                        {
                            cardId = data;
                            if (cardId == null)
                            {
                                cardId = "";
                            }

                            //if (isServiceCard())
                            //{
                            //    return csReturn.CSRETURN_VALID;
                            //}

                            if (isValidCard(cardFormats, isOnline))
                            {
                                //if (!PluginManager.ReaderTypeIsDSAC() && !HostCommMonitor.isOnline() && PluginManager.offlineTransactionsEnabled == false)
                                //{
                                //    return csReturn.CSRETURN_IGNORE;
                                //}
                                // If card matches a card format, check to see if the credential type is not allowed
                                //
                                //else if ((type.Contains("MAGSTRIPE")) && !SystemConfiguration.getSystemConfig().credMagstripeAllowed)
                                //{
                                //    _CardIsValid = false;
                                //    return csReturn.CSRETURN_INVALID;
                                //}
                                //else if ((type.Contains("FELICA")) && !SystemConfiguration.getSystemConfig().credFelicaAllowed)
                                //{
                                //    _CardIsValid = false;
                                //    return csReturn.CSRETURN_INVALID;
                                //}
                                //else if ((type.Contains("MIFARE")) && !SystemConfiguration.getSystemConfig().credMifareAllowed)
                                //{
                                //    _CardIsValid = false;
                                //    return csReturn.CSRETURN_INVALID;
                                //}
                                //else if ((type.Contains("DESFIRE")) && !SystemConfiguration.getSystemConfig().credDesfireAllowed)
                                //{
                                //    _CardIsValid = false;
                                //    return csReturn.CSRETURN_INVALID;
                                //}
                                //else if ((type.Contains("MOBILE")) && !SystemConfiguration.getSystemConfig().credMobileAllowed)
                                //{
                                //    _CardIsValid = false;
                                //    return csReturn.CSRETURN_INVALID;
                                //}
                                //else if ((type.Contains("BIOMETRIC")) && !SystemConfiguration.getSystemConfig().credBiometricAllowed)
                                //{
                                //    _CardIsValid = false;
                                //    return csReturn.CSRETURN_INVALID;
                                //}
                                //else
                                    return csReturn.CSRETURN_VALID;
                            }
                            else
                            {
                                //if (!PluginManager.ReaderTypeIsDSAC() && !HostCommMonitor.isOnline() && PluginManager.offlineTransactionsEnabled == false)
                                //    return csReturn.CSRETURN_IGNORE;
                                //else
                                    return csReturn.CSRETURN_INVALID;
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                //AppLog.log("parseCard: " + ex.ToString());
            }

            return csReturn.CSRETURN_INVALID;
        }

    }

    /// <summary>
    /// describes card format
    /// </summary>
    public class CardFormatMf4100
    {
        uint _SCLength = 0;
        /// <summary>
        /// Gets or sets the length of the SC.
        /// </summary>
        /// <value>The length of the SC.</value>
        public uint SCLength
        {
            get { return _SCLength; }
            set { _SCLength = value; }
        }

        uint _SCLocation = 0;
        /// <summary>
        /// Gets or sets the SC location.
        /// </summary>
        /// <value>The SC location.</value>
        public uint SCLocation
        {
            get { return _SCLocation; }
            set { _SCLocation = value; }
        }

        string _SC = "";
        /// <summary>
        /// Gets or sets the SC.
        /// </summary>
        /// <value>The SC.</value>
        public string SC
        {
            get { return _SC; }
            set { _SC = value; }
        }

        uint _issueCodeLength = 0;
        /// <summary>
        /// Gets or sets the length of the issue code.
        /// </summary>
        /// <value>The length of the issue code.</value>
        public uint issueCodeLength
        {
            get { return _issueCodeLength; }
            set { _issueCodeLength = value; }
        }

        uint _issueCodeLocation = 0;
        /// <summary>
        /// Gets or sets the issue code location.
        /// </summary>
        /// <value>The issue code location.</value>
        public uint issueCodeLocation
        {
            get { return _issueCodeLocation; }
            set { _issueCodeLocation = value; }
        }

        uint _IDLength = 0;
        /// <summary>
        /// Gets or sets the length of the ID.
        /// </summary>
        /// <value>The length of the ID.</value>
        public uint IDLength
        {
            get { return _IDLength; }
            set { _IDLength = value; }
        }

        uint _IDLocation = 0;
        /// <summary>
        /// Gets or sets the ID location.
        /// </summary>
        /// <value>The ID location.</value>
        public uint IDLocation
        {
            get { return _IDLocation; }
            set { _IDLocation = value; }
        }

        uint _minDigitCount = 0;
        /// <summary>
        /// Gets or sets the min digit count.
        /// </summary>
        /// <value>The min digit count.</value>
        public uint minDigitCount
        {
            get { return _minDigitCount; }
            set { _minDigitCount = value; }
        }

        uint _maxDigitCount = 0;
        /// <summary>
        /// Gets or sets the max digit count.
        /// </summary>
        /// <value>The max digit count.</value>
        public uint maxDigitCount
        {
            get { return _maxDigitCount; }
            set { _maxDigitCount = value; }
        }

        uint _cardTestFlag = 0;
        /// <summary>
        /// Gets or sets the card test flag.
        /// </summary>
        /// <value>The card test flag.</value>
        public uint cardTestFlag
        {
            get { return _cardTestFlag; }
            set { _cardTestFlag = value; }
        }
        // offline, online, checkcconline,checkccoffline flags
        uint _lineCheckFlag = 0;
        /// <summary>
        /// Gets or sets the line check flag.
        /// </summary>
        /// <value>The line check flag.</value>
        public uint lineCheckFlag
        {
            get { return _lineCheckFlag; }
            set { _lineCheckFlag = value; }
        }

        bool _BV = false;
        /// <summary>
        /// Gets or sets SaaS Card Bin Validation
        /// </summary>
        /// <value>The BV.</value>
        /// <value><c>true</c> if [BV]; otherwise, <c>false</c>.</value>
        public bool BV
        {
            get { return _BV; }
            set { _BV = value; }
        }

        string _BVLow = "";
        /// <summary>
        /// Gets or sets the Bin Validation Low Range (e.g. 603950 or 60395000)
        /// Typically: 603950 or 603950000 or Null if not used
        /// </summary>
        /// <value>The BVLow.</value>
        public string BVLow
        {
            get { return _BVLow; }
            set { _BVLow = value; }
        }

        string _BVHigh = "";
        /// <summary>
        /// Gets or sets the Bin Validation High Range ((e.g. 603950 or 60395000)
        /// Typically: 603950 or 603950999 or Null if not used
        /// </summary>
        /// <value>The BVHigh.</value>
        public string BVHigh
        {
            get { return _BVHigh; }
            set { _BVHigh = value; }
        }
    }



}
