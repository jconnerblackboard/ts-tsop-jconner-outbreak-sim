﻿using System;
using System.Collections.Generic;
using System.Linq;
using BbTS.Domain.Models.ArtsDataModel;
using BbTS.Domain.Models.Credential;
using BbTS.Domain.Models.Definitions.Notification;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BbTS.Core.Credential.Test
{
    [TestClass]
    public class CredentialUtilityTests
    {
        private const string CustomerGuid = "D6B5EF5E-7FEC-4602-82FD-3DD109914E3D";
        private const string DomainId = "0F50AF71-E011-4205-94F0-501D48B7C1FA";

        public static CardCredentialComplete Base { get; } = new CardCredentialComplete
        {
            ActiveEndDate = new DateTime(2078, 6, 14, 1, 2, 3),
            ActiveStartDate = new DateTime(1978, 6, 14, 1, 2, 3),
            CardIdm = "cardIdm",
            CardName = "cardname",
            CardNumber = "cardnumber",
            CardStatus = NotificationCardStatus.Active,
            CardStatusDateTime = new DateTime(2018, 4, 25, 1, 2, 3),
            CardStatusText = "card status text",
            CardType = NotificationCardType.Standard,
            CustomerGuid = new Guid(CustomerGuid),
            DomainId = new Guid(DomainId),
            IsPrimary = true,
            IssueNumber = "issue number",
            IssuerId = 1,
            LostFlag = false
        };

        /// <summary>
        /// Ensure that the credential map method is working as expected.
        /// </summary>
        [TestMethod]
        public void CredentialMapSimpleSuccessUnitTest()
        {
            var destination = Base.Clone();
            var value = new CardCredentialComplete { ActiveEndDate = DateTimeOffset.MaxValue };

            CredentialUtility.CredentialMap(value, destination);
            Assert.IsTrue(value.ActiveEndDate == destination.ActiveEndDate, "ActiveEndDate mapping failed.");
            var check = CredentialUtility.CompareTo(destination, Base, new List<string> { nameof(destination.ActiveEndDate) });
            Assert.IsTrue(check.Count == 0, "The expected number of mismatched of properties was not received.");

            destination = Base.Clone();
            value = new CardCredentialComplete { ActiveStartDate = DateTimeOffset.MinValue };
            CredentialUtility.CredentialMap(value, destination);
            Assert.IsTrue(value.ActiveStartDate == destination.ActiveStartDate, "ActiveStartDate mapping failed.");
            check = CredentialUtility.CompareTo(destination, Base, new List<string> { nameof(destination.ActiveStartDate) });
            Assert.IsTrue(check.Count == 0, "The expected number of mismatched of properties was not received.");

            destination = Base.Clone();
            value = new CardCredentialComplete { CardIdm = "new value" };
            CredentialUtility.CredentialMap(value, destination);
            Assert.IsTrue(value.CardIdm == destination.CardIdm, "CardIdm mapping failed.");
            check = CredentialUtility.CompareTo(destination, Base, new List<string> { nameof(destination.CardIdm) });
            Assert.IsTrue(check.Count == 0, "The expected number of mismatched of properties was not received.");

            destination = Base.Clone();
            value = new CardCredentialComplete { CardName = "new value" };
            CredentialUtility.CredentialMap(value, destination);
            Assert.IsTrue(value.CardName == destination.CardName, "CardName mapping failed.");
            check = CredentialUtility.CompareTo(destination, Base, new List<string> { nameof(destination.CardName) });
            Assert.IsTrue(check.Count == 0, "The expected number of mismatched of properties was not received.");

            destination = Base.Clone();
            value = new CardCredentialComplete { CardNumber = "new value" };
            CredentialUtility.CredentialMap(value, destination);
            Assert.IsTrue(value.CardNumber == destination.CardNumber, "CardNumber mapping failed.");
            check = CredentialUtility.CompareTo(destination, Base, new List<string> { nameof(destination.CardNumber) });
            Assert.IsTrue(check.Count == 0, "The expected number of mismatched of properties was not received.");

            destination = Base.Clone();
            value = new CardCredentialComplete { CardStatus = NotificationCardStatus.Suspended };
            CredentialUtility.CredentialMap(value, destination);
            Assert.IsTrue(value.CardStatus == destination.CardStatus, "CardStatus mapping failed.");
            check = CredentialUtility.CompareTo(destination, Base, new List<string> { nameof(destination.CardStatus) });
            Assert.IsTrue(check.Count == 0, "The expected number of mismatched of properties was not received.");

            destination = Base.Clone();
            value = new CardCredentialComplete { CardStatusDateTime = DateTimeOffset.MinValue };
            CredentialUtility.CredentialMap(value, destination);
            Assert.IsTrue(value.CardStatusDateTime == destination.CardStatusDateTime, "CardStatusDateTime mapping failed.");
            check = CredentialUtility.CompareTo(destination, Base, new List<string> { nameof(destination.CardStatusDateTime) });
            Assert.IsTrue(check.Count == 0, "The expected number of mismatched of properties was not received.");

            destination = Base.Clone();
            value = new CardCredentialComplete { CardStatusText = "new value" };
            CredentialUtility.CredentialMap(value, destination);
            Assert.IsTrue(value.CardStatusText == destination.CardStatusText, "CardStatusText mapping failed.");
            check = CredentialUtility.CompareTo(destination, Base, new List<string> { nameof(destination.CardStatusText) });
            Assert.IsTrue(check.Count == 0, "The expected number of mismatched of properties was not received.");

            destination = Base.Clone();
            value = new CardCredentialComplete { CardType = NotificationCardType.Temporary };
            CredentialUtility.CredentialMap(value, destination);
            Assert.IsTrue(value.CardType == destination.CardType, "CardType mapping failed.");
            check = CredentialUtility.CompareTo(destination, Base, new List<string> { nameof(destination.CardType) });
            Assert.IsTrue(check.Count == 0, "The expected number of mismatched of properties was not received.");

            destination = Base.Clone();
            value = new CardCredentialComplete { CustomerGuid = Guid.NewGuid() };
            CredentialUtility.CredentialMap(value, destination);
            Assert.IsTrue(value.CustomerGuid == destination.CustomerGuid, "CustomerGuid mapping failed.");
            check = CredentialUtility.CompareTo(destination, Base, new List<string> { nameof(destination.CustomerGuid) });
            Assert.IsTrue(check.Count == 0, "The expected number of mismatched of properties was not received.");

            destination = Base.Clone();
            value = new CardCredentialComplete { DomainId = Guid.NewGuid() };
            CredentialUtility.CredentialMap(value, destination);
            Assert.IsTrue(value.DomainId == destination.DomainId, "DomainId mapping failed.");
            check = CredentialUtility.CompareTo(destination, Base, new List<string> { nameof(destination.DomainId) });
            Assert.IsTrue(check.Count == 0, "The expected number of mismatched of properties was not received.");

            destination = Base.Clone();
            value = new CardCredentialComplete { IsPrimary = !destination.IsPrimary };
            CredentialUtility.CredentialMap(value, destination);
            Assert.IsTrue(value.IsPrimary == destination.IsPrimary, "IsPrimary mapping failed.");
            check = CredentialUtility.CompareTo(destination, Base, new List<string> { nameof(destination.IsPrimary) });
            Assert.IsTrue(check.Count == 0, "The expected number of mismatched of properties was not received.");

            destination = Base.Clone();
            value = new CardCredentialComplete { IssueNumber = "new value" };
            CredentialUtility.CredentialMap(value, destination);
            Assert.IsTrue(value.IssueNumber == destination.IssueNumber, "IssueNumber mapping failed.");
            check = CredentialUtility.CompareTo(destination, Base, new List<string> { nameof(destination.IssueNumber) });
            Assert.IsTrue(check.Count == 0, "The expected number of mismatched of properties was not received.");

            destination = Base.Clone();
            value = new CardCredentialComplete { IssuerId = 9001 };
            CredentialUtility.CredentialMap(value, destination);
            Assert.IsTrue(value.IssuerId == destination.IssuerId, "IssuerId mapping failed.");
            check = CredentialUtility.CompareTo(destination, Base, new List<string> { nameof(destination.IssuerId) });
            Assert.IsTrue(check.Count == 0, "The expected number of mismatched of properties was not received.");

            destination = Base.Clone();
            value = new CardCredentialComplete { LostFlag = !destination.LostFlag };
            CredentialUtility.CredentialMap(value, destination);
            Assert.IsTrue(value.LostFlag == destination.LostFlag, "LostFlag mapping failed.");
            check = CredentialUtility.CompareTo(destination, Base, new List<string> { nameof(destination.LostFlag) });
            Assert.IsTrue(check.Count == 0, "The expected number of mismatched of properties was not received.");
        }

        /// <summary>
        /// Ensure that the compare to method is working as expected.
        /// </summary>
        [TestMethod]
        public void CredentialCompareToUnitTest()
        {
            var value = Base.Clone();
            value.ActiveEndDate = DateTimeOffset.MaxValue;

            var propertyName = "ActiveEndDate";
            var check = CredentialUtility.CompareTo(value, Base, new List<string>());
            Assert.IsTrue(check.Count == 1, "The expected number of mismatched of properties was not received.");
            Assert.IsTrue(check.Contains(propertyName), $"{propertyName} changed property detection failed.");

            value = Base.Clone();
            value.ActiveStartDate = DateTimeOffset.MinValue;
            propertyName = "ActiveStartDate";
            check = CredentialUtility.CompareTo(value, Base, new List<string>());
            Assert.IsTrue(check.Count == 1, "The expected number of mismatched of properties was not received.");
            Assert.IsTrue(check.Contains(propertyName), $"{propertyName} changed property detection failed.");

            value = Base.Clone();
            value.CardIdm = "new value";
            propertyName = "CardIdm";
            check = CredentialUtility.CompareTo(value, Base, new List<string>());
            Assert.IsTrue(check.Count == 1, "The expected number of mismatched of properties was not received.");
            Assert.IsTrue(check.Contains(propertyName), $"{propertyName} changed property detection failed.");

            value = Base.Clone();
            value.CardName = "new value";
            propertyName = "CardName";
            check = CredentialUtility.CompareTo(value, Base, new List<string>());
            Assert.IsTrue(check.Count == 1, "The expected number of mismatched of properties was not received.");
            Assert.IsTrue(check.Contains(propertyName), $"{propertyName} changed property detection failed.");

            value = Base.Clone();
            value.CardNumber = "new value";
            propertyName = "CardNumber";
            check = CredentialUtility.CompareTo(value, Base, new List<string>());
            Assert.IsTrue(check.Count == 1, "The expected number of mismatched of properties was not received.");
            Assert.IsTrue(check.Contains(propertyName), $"{propertyName} changed property detection failed.");

            value = Base.Clone();
            value.CardStatus = NotificationCardStatus.Suspended;
            propertyName = "CardStatus";
            check = CredentialUtility.CompareTo(value, Base, new List<string>());
            Assert.IsTrue(check.Count == 1, "The expected number of mismatched of properties was not received.");
            Assert.IsTrue(check.Contains(propertyName), $"{propertyName} changed property detection failed.");

            value = Base.Clone();
            value.CardStatusDateTime = DateTimeOffset.MinValue;
            propertyName = "CardStatusDateTime";
            check = CredentialUtility.CompareTo(value, Base, new List<string>());
            Assert.IsTrue(check.Count == 1, "The expected number of mismatched of properties was not received.");
            Assert.IsTrue(check.Contains(propertyName), $"{propertyName} changed property detection failed.");

            value = Base.Clone();
            value.CardStatusText = "new value";
            propertyName = "CardStatusText";
            check = CredentialUtility.CompareTo(value, Base, new List<string>());
            Assert.IsTrue(check.Count == 1, "The expected number of mismatched of properties was not received.");
            Assert.IsTrue(check.Contains(propertyName), $"{propertyName} changed property detection failed.");

            value = Base.Clone();
            value.CardType = NotificationCardType.Temporary;
            propertyName = "CardType";
            check = CredentialUtility.CompareTo(value, Base, new List<string>());
            Assert.IsTrue(check.Count == 1, "The expected number of mismatched of properties was not received.");
            Assert.IsTrue(check.Contains(propertyName), $"{propertyName} changed property detection failed.");

            value = Base.Clone();
            value.CustomerGuid = Guid.NewGuid();
            propertyName = "CustomerGuid";
            check = CredentialUtility.CompareTo(value, Base, new List<string>());
            Assert.IsTrue(check.Count == 1, "The expected number of mismatched of properties was not received.");
            Assert.IsTrue(check.Contains(propertyName), $"{propertyName} changed property detection failed.");

            value = Base.Clone();
            value.DomainId = Guid.NewGuid();
            propertyName = "DomainId";
            check = CredentialUtility.CompareTo(value, Base, new List<string>());
            Assert.IsTrue(check.Count == 1, "The expected number of mismatched of properties was not received.");
            Assert.IsTrue(check.Contains(propertyName), $"{propertyName} changed property detection failed.");

            value = Base.Clone();
            value.IsPrimary = !Base.IsPrimary;
            propertyName = "IsPrimary";
            check = CredentialUtility.CompareTo(value, Base, new List<string>());
            Assert.IsTrue(check.Count == 1, "The expected number of mismatched of properties was not received.");
            Assert.IsTrue(check.Contains(propertyName), $"{propertyName} changed property detection failed.");

            value = Base.Clone();
            value.IssueNumber = "new value";
            propertyName = "IssueNumber";
            check = CredentialUtility.CompareTo(value, Base, new List<string>());
            Assert.IsTrue(check.Count == 1, "The expected number of mismatched of properties was not received.");
            Assert.IsTrue(check.Contains(propertyName), $"{propertyName} changed property detection failed.");

            value = Base.Clone();
            value.IssuerId = 9001;
            propertyName = "IssuerId";
            check = CredentialUtility.CompareTo(value, Base, new List<string>());
            Assert.IsTrue(check.Count == 1, "The expected number of mismatched of properties was not received.");
            Assert.IsTrue(check.Contains(propertyName), $"{propertyName} changed property detection failed.");

            value = Base.Clone();
            value.LostFlag = !Base.LostFlag;
            propertyName = "LostFlag";
            check = CredentialUtility.CompareTo(value, Base, new List<string>());
            Assert.IsTrue(check.Count == 1, "The expected number of mismatched of properties was not received.");
            Assert.IsTrue(check.Contains(propertyName), $"{propertyName} changed property detection failed.");
        }
    }
}
