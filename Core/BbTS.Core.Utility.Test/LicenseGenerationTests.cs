﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BbTS.Core.Utility.Test
{
    [TestClass]
    public class LicenseGenerationTests
    {
        // Tests for a valid expiration date after running a version 3.x license generation.
        [TestMethod]
        public void GenerateVersion3xLicense_WithValidExpirationDate()
        {
            // arrange
            DateTime? expirationDate = DateTime.Parse("06/07/1983");
            const string institutionName = "San Deimas HS";
            const bool isLicensed = true;

            const string expectedDate = "06/07/1983";

            LicenseBase license = new License3X
                {
                    InstitutionName = institutionName,
                    ExpirationDate = expirationDate,
                    IncludesDoorAccessLicense = isLicensed
                };

            // act
            license.Generate();
            string unTransposedNumber = LicenseBase.UnTranspose(license.LicenseNumber.Replace("-", ""));

            // assert
            string actualDate = unTransposedNumber.Substring(5, 2) + "/" + unTransposedNumber.Substring(7, 2) + "/" + unTransposedNumber.Substring(9, 4);
            Assert.AreEqual(expectedDate, actualDate, "Date does not match.");
        }

        // Tests for a valid door access license value after running a version 3.x license generation.
        [TestMethod]
        public void GenerateVersion3xLicense_WithValidDoorAccessLicense()
        {
            // arrange
            DateTime? expirationDate = DateTime.Parse("12/25/2013");
            const string institutionName = "WhatsamattaU";
            const bool isLicensed = true;

            const bool expectedIsLicensed = true;

            LicenseBase license = new License3X
                {
                    InstitutionName = institutionName,
                    ExpirationDate = expirationDate,
                    IncludesDoorAccessLicense = isLicensed
                };

            // act
            license.Generate();
            bool actualIsLicensed = ((License3X) license).IncludesDoorAccessLicense;
            Assert.AreEqual(expectedIsLicensed, actualIsLicensed, "Door Access Licensed does not match.");
        }

        // Tests for a valid expiration date after running a version 2.x license generation.
        [TestMethod]
        public void GenerateVersion2xLicense_WithValidExpirationDate()
        {
            // arrange
            DateTime? expirationDate = DateTime.Parse("07/04/2014");
            const string institutionName = "San Deimas HS";

            const string expectedDate = "07/04/2014";

            LicenseBase license = new License2X
                {
                    InstitutionName = institutionName,
                    ExpirationDate = expirationDate,
                };

            // act
            license.Generate();
            string unTransposedNumber = LicenseBase.UnTranspose(license.LicenseNumber.Replace("-", ""));

            // assert
            string actualDate = unTransposedNumber.Substring(3, 2) + "/" + unTransposedNumber.Substring(5, 2) + "/" + unTransposedNumber.Substring(7, 4);
            Assert.AreEqual(expectedDate, actualDate, "Date does not match.");
        }

        [TestMethod]
        public void ExpirationDateTest()
        {
            // Set the various variables
            DateTime? expirationDate = DateTime.Parse( "12/25/2013" );
            const string institutionName = "WhatsamattaU";
            const bool isLicensed = true;

            // New up a license object
            LicenseBase license = new License3X {
                InstitutionName = institutionName,
                ExpirationDate = expirationDate,
                IncludesDoorAccessLicense = isLicensed
            };

            // Generate the license
            license.Generate();

            // Decompose the expiration date from the calculated license
            DateTime calculatedExpirationDate = ((License3X)license).ExpirationDateGet();
            Assert.AreEqual( expirationDate,calculatedExpirationDate,"Date does not match." );
        }
    }
}