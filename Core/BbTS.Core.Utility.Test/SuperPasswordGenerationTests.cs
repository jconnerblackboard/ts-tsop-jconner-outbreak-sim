﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BbTS.Core.Utility.Test
{
    [TestClass]
    public class SuperPasswordGenerationTests
    {
        // Tests that generated SuperPasswords match known good values.
        [TestMethod]
        public void GenerateKnownSuperPasswords()
        {
            //arrange
            Dictionary<DateTime, string> knownPasswords = new Dictionary<DateTime, string>
                {
                    { DateTime.Parse("01/02/2013"), "1423196" },
                    { DateTime.Parse("01/03/2013"), "3100917" },
                    { DateTime.Parse("01/04/2013"), "3746201" },
                    { DateTime.Parse("01/14/2013"), "8726937" },
                    { DateTime.Parse("01/18/2013"), "1292124" },
                    { DateTime.Parse("01/29/2013"), "1816412" },
                    { DateTime.Parse("06/03/2014"), "2887413" }
                    //For future improvement, add more known passwords
                };

            foreach (var item in knownPasswords)
            {
                // act
                string password = SuperPassword.CalculateSuperPassword(item.Key);

                // assert
                Assert.AreEqual(item.Value, password, "Generated SuperPassword does not match.");
            }
        }
    }
}