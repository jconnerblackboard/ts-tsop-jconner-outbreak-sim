﻿using System.Collections.Generic;
using BbTS.Domain.Models.Credential;
using BbTS.Domain.Models.Definitions.Notification;

namespace BbTS.Core.Credential
{
    /// <summary>
    /// Utility class for credential related functions.
    /// </summary>
    public static class CredentialUtility
    {
        /// <summary>
        /// Map credentials from source to destination.  Fields that are null in source will be ignored.
        /// </summary>
        /// <param name="source">Source</param>
        /// <param name="destination">Destination</param>
        /// <returns>Mapped credential</returns>
        public static void CredentialMap(CardCredentialComplete source, CardCredentialComplete destination)
        {
            destination.ActiveEndDate = source.ActiveEndDate ?? destination.ActiveEndDate;
            destination.ActiveStartDate = source.ActiveStartDate ?? destination.ActiveStartDate;
            destination.CardIdm = source.CardIdm ?? destination.CardIdm;
            destination.CardName = source.CardName ?? destination.CardName;
            destination.CardNumber = source.CardNumber ?? destination.CardNumber;
            destination.CardStatus = source.CardStatus == NotificationCardStatus.Unknown ? destination.CardStatus : source.CardStatus;
            destination.CardStatusDateTime = source.CardStatusDateTime ?? destination.CardStatusDateTime;
            destination.CardStatusText = source.CardStatusText ?? destination.CardStatusText;
            destination.CardType = source.CardType == NotificationCardType.Unknown ? destination.CardType : source.CardType;
            destination.CustomerGuid = source.CustomerGuid;
            destination.DomainId = source.DomainId ?? destination.DomainId;
            destination.IsPrimary = source.IsPrimary ?? destination.IsPrimary;
            destination.IssueNumber = source.IssueNumber ?? destination.IssueNumber;
            destination.IssuerId = source.IssuerId ?? destination.IssuerId;
            destination.LostFlag = source.LostFlag ?? destination.LostFlag;
        }

        /// <summary>
        /// Check that properties in source match destination except for the exclude property.
        /// </summary>
        /// <param name="value1">Value1</param>
        /// <param name="value2">Value2</param>
        /// <param name="excludeProperty">List of property names to exclude in comparison check.</param>
        /// <returns>List of property names that were not equal.</returns>
        public static List<string> CompareTo(CardCredentialComplete value1, CardCredentialComplete value2, List<string> excludeProperty)
        {
            var list = new List<string>();
            if(value1.ActiveEndDate != value2.ActiveEndDate && !excludeProperty.Contains(nameof(value1.ActiveEndDate))) list.Add(nameof(value1.ActiveEndDate));
            if (value1.ActiveStartDate != value2.ActiveStartDate && !excludeProperty.Contains(nameof(value1.ActiveStartDate))) list.Add(nameof(value1.ActiveStartDate));
            if (value1.CardIdm != value2.CardIdm && !excludeProperty.Contains(nameof(value1.CardIdm))) list.Add(nameof(value1.CardIdm));
            if (value1.CardName != value2.CardName && !excludeProperty.Contains(nameof(value1.CardName))) list.Add(nameof(value1.CardName));
            if (value1.CardNumber != value2.CardNumber && !excludeProperty.Contains(nameof(value1.CardNumber))) list.Add(nameof(value1.CardNumber));
            if (value1.CardStatus != value2.CardStatus && !excludeProperty.Contains(nameof(value1.CardStatus))) list.Add(nameof(value1.CardStatus));
            if (value1.CardStatusDateTime != value2.CardStatusDateTime && !excludeProperty.Contains(nameof(value1.CardStatusDateTime))) list.Add(nameof(value1.CardStatusDateTime));
            if (value1.CardStatusText != value2.CardStatusText && !excludeProperty.Contains(nameof(value1.CardStatusText))) list.Add(nameof(value1.CardStatusText));
            if (value1.CardType != value2.CardType && !excludeProperty.Contains(nameof(value1.CardType))) list.Add(nameof(value1.CardType));
            if (value1.CustomerGuid != value2.CustomerGuid && !excludeProperty.Contains(nameof(value1.CustomerGuid))) list.Add(nameof(value1.CustomerGuid));
            if (value1.DomainId != value2.DomainId && !excludeProperty.Contains(nameof(value1.DomainId))) list.Add(nameof(value1.DomainId));
            if (value1.IsPrimary != value2.IsPrimary && !excludeProperty.Contains(nameof(value1.IsPrimary))) list.Add(nameof(value1.IsPrimary));
            if (value1.IssueNumber != value2.IssueNumber && !excludeProperty.Contains(nameof(value1.IssueNumber))) list.Add(nameof(value1.IssueNumber));
            if (value1.IssuerId != value2.IssuerId && !excludeProperty.Contains(nameof(value1.IssuerId))) list.Add(nameof(value1.IssuerId));
            if (value1.LostFlag != value2.LostFlag && !excludeProperty.Contains(nameof(value1.LostFlag))) list.Add(nameof(value1.LostFlag));

            return list;
        }
    }
}
