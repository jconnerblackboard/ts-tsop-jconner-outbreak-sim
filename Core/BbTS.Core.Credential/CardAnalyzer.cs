﻿using System.Collections.Generic;
using BbTS.Domain.Models.Credential;
using BbTS.Domain.Models.Definitions.Credential;

namespace BbTS.Core.Credential
{
    /// <summary>
    /// A static class that allows customer card data to be analyzed and parsed.
    /// </summary>
    public static class CardAnalyzer
    {
        private static string ParseValue(CardData cardData, CardParseRule rule)
        {
            //Sanity check the rule
            if ((rule.Location < 0) || (rule.Location > 36)) return null;

            //Set startIndex
            int startIndex = 0;
            if (rule.Anchor == ParseAnchor.FirstSeparator)
            {
                startIndex = cardData.Track2.IndexOf(cardData.Track2FieldSeparator);
                if (startIndex < 0) return null;
                startIndex++;
            }

            startIndex += rule.Location;

            //Make sure there is enough data
            if (startIndex + rule.Length > cardData.Track2.Length) return null;

            //Read the value
            return cardData.Track2.Substring(startIndex, rule.Length);
        }

        /// <summary>
        /// Attempts to parse track 2 data as a BbTS customer card matching one of the supplied card formats.
        /// </summary>
        /// <param name="cardData"></param>
        /// <param name="cardFormats"></param>
        /// <param name="isOnline"></param>
        /// <param name="matchedCardFormat"></param>
        /// <returns></returns>
        public static CardCapture ParseTrack2(CardData cardData, List<CardFormat> cardFormats, bool isOnline, out CardFormat matchedCardFormat)
        {
            foreach (var cardFormat in cardFormats)
            {
                //Make sure card format is active
                if (!cardFormat.IsActive) continue;

                //Match pre-existing behavior...MF4100 makes a check to this effect
                if (cardData.Track2.Length <= 1) continue;

                //Validate length
                if ((cardData.Track2.Length < cardFormat.MinimumDigits) || (cardData.Track2.Length > cardFormat.MaximumDigits)) continue;

                //Validate site code
                if ((cardFormat.SiteCode != null) &&
                    (cardFormat.SiteCode.Length > 0) &&
                    ((isOnline && cardFormat.SiteCodeCheckOnline) || (!isOnline && cardFormat.SiteCodeCheckOffline)))
                {
                    if (cardFormat.SiteCode.Length > 12) continue; //Sanity check
                    if (cardFormat.SiteCodeValue != ParseValue(cardData, cardFormat.SiteCode)) continue;
                }

                //Extract card number
                if (cardFormat.CardNumber.Length > 19) continue; //Sanity check
                string cardNumber = ParseValue(cardData, cardFormat.CardNumber);
                if (string.IsNullOrEmpty(cardNumber)) continue;

                //Extract issue number
                string issueNumber = string.Empty;
                if ((cardFormat.IssueNumber != null) && (cardFormat.IssueNumber.Length > 0))
                {
                    if (cardFormat.IssueNumber.Length > 4) continue; //Sanity check
                    issueNumber = ParseValue(cardData, cardFormat.IssueNumber);
                    if (string.IsNullOrEmpty(issueNumber)) continue;
                }

                //Return the parsed information
                matchedCardFormat = cardFormat;
                return new CardCapture
                {
                    CardNumber = cardNumber,
                    IssueNumber = issueNumber,
                    IssueNumberCaptured = true //Misleading name for this property, true = card swiped, false = card number entered manually
                };
            }

            matchedCardFormat = null;
            return null;
        }

        public static void PadCardNumber(CardData cardData)
        {
            cardData.Track2 = cardData.Track2.PadLeft(19);
        }
    }
}
