﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Core.Media.ImageProcessing
{
    public class ImageTool
    {
        private static ImageTool _instance = new ImageTool();

        public static ImageTool Instance
        {
            get { return _instance; }
            internal set { _instance = value; }
        }

        /// <summary>
        /// Convert an image to a base 64 string.
        /// </summary>
        /// <param name="image">Image to convert.</param>
        /// <param name="format">format to save as.</param>
        /// <returns></returns>
        public string ImageAsBase64String(Image image, ImageFormat format = null)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                image.Save(ms, format ?? ImageFormat.Png);
                return Convert.ToBase64String(ms.ToArray());
            }
        }

        /// <summary>
        /// Create a standard 100x200 Image from a byte array.
        /// </summary>
        /// <param name="bytes">byte array containing the base image</param>
        /// <returns></returns>
        public Image StandardThumbnailFromBytes(byte[] bytes)
        {
            return CreateThumbnail(ImageFromBytes(bytes), 100, 200);
        }

        /// <summary>
        /// Get an image from an array of bytes.
        /// </summary>
        /// <param name="bytes">byte array containing an image</param>
        /// <returns></returns>
        public Image ImageFromBytes(byte[] bytes)
        {
            var converter = new ImageConverter();
            var image = converter.ConvertFrom(bytes) as Image;
            return image;
        }

        /// <summary>
        /// Convert an image to bytes
        /// </summary>
        /// <param name="imageIn"></param>
        /// <returns></returns>
        public byte[] ImageToByteArray(Image imageIn)
        {
            using (var ms = new MemoryStream())
            {
                imageIn.Save(ms, imageIn.RawFormat);
                return ms.ToArray();
            }
        }

        /// <summary>
        /// Convert bytes of a raw image to a new format and scale the image.
        /// </summary>
        /// <param name="bytes">Image bytes.</param>
        /// <param name="width">New image width.</param>
        /// <param name="height">New image height.</param>
        /// <param name="format">New image format.</param>
        /// <param name="withTransparency">set flag to true to make background pixels transparent</param>
        /// <returns></returns>
        public string ImageFromByteScaleAndConvert(byte[] bytes, int width, int height, ImageFormat format, bool withTransparency = true)
        {
            var rawImage = ImageFromBytes(bytes);

            var scaledImage = withTransparency ? ScaleImageWithAlpha(rawImage, width, height) : ScaleImage(rawImage, width, height);
            
            using (var memstream = new MemoryStream())
            {
                scaledImage.Save(memstream, format);
                return Convert.ToBase64String(memstream.GetBuffer());
            }
        }

        /// <summary>
        /// Convert bytes of a raw image to a new format and scale the image.
        /// </summary>
        /// <param name="bytes">Image bytes.</param>
        /// <param name="format">New image format.</param>
        /// <returns></returns>
        public string ImageFromByteConvert(byte[] bytes, ImageFormat format)
        {
            var rawImage = ImageFromBytes(bytes);

            using (var memstream = new MemoryStream())
            {
                rawImage.Save(memstream, format);
                return Convert.ToBase64String(memstream.GetBuffer());
            }
        }

        /// <summary>
        /// Convert bytes of a raw image to a new format and scale the image.
        /// </summary>
        /// <param name="bytes">Image bytes.</param>
        /// <param name="format">New image format.</param>
        /// <param name="path"></param>
        /// <returns></returns>
        public void ImageConvertAndSave(byte[] bytes, ImageFormat format, string path)
        {
            var rawImage = ImageFromBytes(bytes);

            using (var fileStream = new FileStream(path, FileMode.OpenOrCreate))
            {
                rawImage.Save(fileStream, format);
            }
        }

        /// <summary>
        /// Create the thumbnail image.
        /// </summary>
        /// <param name="photo">Customer Photo</param>
        /// <param name="width">Thumbnail width</param>
        /// <param name="height">Thumbnail height</param>
        /// <returns></returns>
        public Image CreateThumbnail(Image photo, int width, int height)
        {
            using (Image convertedImage = ScaleImage(photo, width, height))
            {
                return (Image)convertedImage.Clone();
            }
        }

        /// <summary>
        /// Scale an image to the target size preserving aspect ratio
        /// </summary>
        /// <param name="image"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        public Image ScaleImage(Image image, int width, int height)
        {
            int sourceWidth = image.Width;
            int sourceHeight = image.Height;
            const int sourceX = 0;
            const int sourceY = 0;
            int destX = 0;
            int destY = 0;

            float nPercent;

            float nPercentW = width / (float)sourceWidth;
            float nPercentH = height / (float)sourceHeight;
            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
                destX = Convert.ToInt16((width - sourceWidth * nPercent) / 2);
            }
            else
            {
                nPercent = nPercentW;
                destY = Convert.ToInt16((height - sourceHeight * nPercent) / 2);
            }

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap bitmap = new Bitmap(width, height, PixelFormat.Format24bppRgb);

            bitmap.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            Graphics graphics = Graphics.FromImage(bitmap);
            graphics.Clear(Color.Black);
            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;

            graphics.DrawImage(
                image,
                new Rectangle(destX, destY, destWidth, destHeight),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            graphics.Dispose();
            return bitmap;
        }

        /// <summary>
        /// Scale an image to the target size preserving aspect ratio
        /// </summary>
        /// <param name="image"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        public Image ScaleImageWithAlpha(Image image, int width, int height)
        {
            int sourceWidth = image.Width;
            int sourceHeight = image.Height;
            const int sourceX = 0;
            const int sourceY = 0;
            int destX = 0;
            int destY = 0;

            float nPercent;

            float nPercentW = width / (float)sourceWidth;
            float nPercentH = height / (float)sourceHeight;
            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
                destX = Convert.ToInt16((width - sourceWidth * nPercent) / 2);
            }
            else
            {
                nPercent = nPercentW;
                destY = Convert.ToInt16((height - sourceHeight * nPercent) / 2);
            }

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap bitmap = new Bitmap(width, height, PixelFormat.Format32bppArgb);

            bitmap.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            Graphics graphics = Graphics.FromImage(bitmap);
            graphics.Clear(Color.Transparent);
            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;

            graphics.DrawImage(
                image,
                new Rectangle(destX, destY, destWidth, destHeight),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            graphics.Dispose();
            return bitmap;
        }

        /// <summary>
        /// Split an int list into several sub int lists
        /// </summary>
        /// <param name="source">Source list</param>
        /// <param name="numberOfLists">number of lists to split into</param>
        /// <param name="minItemsPerList">minimum items per list</param>
        /// <returns></returns>
        public List<List<T>> SplitList<T>(List<T> source, int numberOfLists, int minItemsPerList)
        {
            int listCount = Math.Min(numberOfLists, source.Count / minItemsPerList);
            listCount = Math.Max(listCount, 1);
            int numberOfItemsPerList = source.Count / listCount;
            numberOfItemsPerList = source.Count % listCount == 0 ? numberOfItemsPerList : numberOfItemsPerList + 1;
            int currentIndex = 0;

            List<List<T>> lists = new List<List<T>>();
            for (int i = 0; i < listCount; ++i)
            {
                int countToGet = numberOfItemsPerList;
                if (currentIndex + numberOfItemsPerList > source.Count)
                {
                    countToGet = source.Count - currentIndex;
                }
                List<T> subList = source.GetRange(currentIndex, countToGet);
                lists.Add(subList);
                currentIndex += numberOfItemsPerList;
            }

            return lists;
        }
    }
}
