﻿using System.Collections.Generic;
using BbTS.Domain.Models.Definitions.Policy;
using BbTS.Domain.Models.Policy;
using BbTS.Domain.Models.Policy.Actions;
using BbTS.Domain.Models.Policy.Conditions;

namespace BbTS.Core.Policy
{
    /// <summary>
    /// Helper class for building and managing a policy tree.
    /// </summary>
    public class PolicyBuilder
    {
        /// <summary>
        /// Root policy set.  The root has no parents.
        /// </summary>
        public PolicySet Root { get; set; }

        /// <summary>
        /// Creates a new instance of the PolicyBuilder class with Root == null.
        /// </summary>
        public PolicyBuilder()
        {
        }

        /// <summary>
        /// Creates a new instance of the PolicyBuilder class with a single PolicyGroup root node having the given name.
        /// </summary>
        /// <param name="rootName"></param>
        public PolicyBuilder(string rootName)
        {
            Root = new PolicyGroup
            {
                Name = rootName,
                Parents = null,
                Children = new List<PolicySet>(),
                DecisionStrategy = PolicyDecisionStrategy.FirstMatching
            };
        }

        /// <summary>
        /// Generates a new PolicyGroup instance.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static PolicyGroup NewPolicyGroup(string name = null)
        {
            var policyGroup = new PolicyGroup
            {
                Name = name,
                Parents = new List<PolicySet>(),
                Children = new List<PolicySet>(),
                DecisionStrategy = PolicyDecisionStrategy.FirstMatching
            };
            if (policyGroup.Name == null) policyGroup.Name = $"Group {policyGroup.Id.Substring(0, 8)}";

            return policyGroup;
        }

        /// <summary>
        /// Generates a new PolicyRule instance.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static PolicyRule NewPolicyRule(string name = null)
        {
            var policyRule = new PolicyRule
            {
                Name = name,
                Parents = new List<PolicySet>(),
                Children = new List<PolicySet>(),
                DecisionStrategy = PolicyDecisionStrategy.FirstMatching,
                EnabledStatus = EnabledStatus.Enabled,
                Validity = new List<TimePeriodCondition>(),
                Conditions = new CompoundPolicyCondition { UsageCount = 1 },
                Actions = new List<PolicyAction>()
            };
            if (policyRule.Name == null) policyRule.Name = $"Rule {policyRule.Id.Substring(0, 8)}";

            return policyRule;
        }
    }
}
