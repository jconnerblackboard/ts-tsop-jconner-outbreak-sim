﻿using System;
using System.Collections.Generic;
using System.Linq;
using BbTS.Domain.Models.Customer;
using BbTS.Domain.Models.Definitions.ArtsDataModel;
using BbTS.Domain.Models.Definitions.Policy;
using BbTS.Domain.Models.Policy;
using BbTS.Domain.Models.Policy.Actions;
using BbTS.Domain.Models.Policy.Conditions;

namespace BbTS.Core.Policy
{
    /// <summary>
    /// Class for evaluating a policy tree.
    /// </summary>
    public class PolicyEvaluator
    {
        private readonly PolicySet _root;

        private PolicyEvaluationState _evaluationState;
        private List<string> _evaluationLog;
        private List<PolicyActionOutput> _actionOutputList;

        /// <summary>
        /// Gets or sets the indent size increment used in the evaluation log.
        /// </summary>
        public int IndentSize { get; set; } = 6;

        /// <summary>
        /// Include policy object identifiers in the evaluation log.
        /// </summary>
        public bool IncludePolicyObjectId { get; set; } = false;

        #region Private methods

        private string EvaluationTextGet(PolicyCondition condition, bool isNegated, bool result)
        {
            string id = IncludePolicyObjectId ? $", {condition.Id}" : string.Empty;
            return $"'{condition.Name}' (negated = {isNegated}{id}) evaluates to {result}";
        }

        private bool IsEvaluationTimeInTimePeriod(TimePeriodCondition timePeriodCondition)
        {
            var evaluationDateTime = _evaluationState.EvaluationDateTime;

            //Begin/End
            var begin = timePeriodCondition.TimePeriodBegin ?? DateTimeOffset.MinValue;
            var end = timePeriodCondition.TimePeriodEnd ?? DateTimeOffset.MaxValue;
            if ((evaluationDateTime < begin) || (evaluationDateTime >= end)) return false;

            //Month of Year
            if (!string.IsNullOrEmpty(timePeriodCondition.MonthOfYearMask))
            {
                if (timePeriodCondition.MonthOfYearMask[evaluationDateTime.Month - 1] == 'F') return false;
            }

            //Day of Month
            bool dayOfMonthForwardValid = true;
            if (!string.IsNullOrEmpty(timePeriodCondition.DayOfMonthForwardMask))
            {
                if (timePeriodCondition.DayOfMonthForwardMask[evaluationDateTime.Day - 1] == 'F') dayOfMonthForwardValid = false;
            }

            bool dayOfMonthBackwardValid = true;
            if (!string.IsNullOrEmpty(timePeriodCondition.DayOfMonthBackwardMask))
            {
                int daysInMonth = DateTime.DaysInMonth(evaluationDateTime.Year, evaluationDateTime.Month);
                int dayOfMonthBackward = (daysInMonth - evaluationDateTime.Day) + 1;
                if (timePeriodCondition.DayOfMonthBackwardMask[dayOfMonthBackward - 1] == 'F') dayOfMonthBackwardValid = false;
            }

            if (!dayOfMonthForwardValid && !dayOfMonthBackwardValid) return false;

            //Day of Week
            if (!string.IsNullOrEmpty(timePeriodCondition.DayOfWeekMask))
            {
                if (timePeriodCondition.DayOfWeekMask[(int)evaluationDateTime.DayOfWeek] == 'F') return false;
            }

            //Time of Day  //todo - this will need some thorough testing to make sure that everything works as expected
            TimeSpan beginTimeOfDay = timePeriodCondition.TimeOfDayBegin ?? new TimeSpan();
            TimeSpan endTimeOfDay = timePeriodCondition.TimeOfDayEnd ?? new TimeSpan(24, 0, 0);
            TimeSpan evaluationTimeOfDay = evaluationDateTime.ToLocalTime().TimeOfDay;
            if (beginTimeOfDay <= endTimeOfDay)
            {
                //Normal case
                if ((evaluationTimeOfDay < beginTimeOfDay) || (evaluationTimeOfDay >= endTimeOfDay)) return false;
            }
            else
            {
                //Span midnight case
                if ((evaluationTimeOfDay >= endTimeOfDay) && (evaluationTimeOfDay < beginTimeOfDay)) return false;
            }

            return true;
        }

        private bool IsCustomerInGroup(int customDefinedGroupId)
        {
            return
                _evaluationState.CustomerInformationForPolicy?.CustomerDefinedGroupDefinitionItemIdList != null &&
                _evaluationState.CustomerInformationForPolicy.CustomerDefinedGroupDefinitionItemIdList.Any(id => id == customDefinedGroupId);
        }

        private bool IsPosInGroup(int conditionPosGroupId)
        {
            if (_evaluationState.PosGroup == null) return false;
            var posGroup = _evaluationState.PosGroup;
            while (true)
            {
                if (posGroup.PosGroupId == conditionPosGroupId) return true;
                if (posGroup.Parent == null) return false;
                posGroup = posGroup.Parent;
            }
        }

        private bool IsConditionValid(PolicyCondition condition, bool isNegated, int depth)
        {
            string indent = new string(' ', depth * IndentSize);

            //CompoundCondition
            var compoundCondition = condition as CompoundPolicyCondition;
            if ((compoundCondition != null) && (compoundCondition.Children.Count > 0))
            {
                string id = IncludePolicyObjectId ? $", {compoundCondition.Id}" : string.Empty;
                string compoundConditionInfo = $"{compoundCondition.ConditionListType}, {compoundCondition.Children.Count} conditions, negated = {isNegated}{id}";
                _evaluationLog.Add($"{indent} ? Compound condition ({compoundConditionInfo}), evaluating...");

                //Children
                bool childResult = compoundCondition.ConditionListType == ConditionListType.Conjunctive;
                for (int i = 0; i < compoundCondition.Children.Count; i++)
                {
                    var childCondition = compoundCondition.Children[i];
                    bool childNegated = compoundCondition.NegationFlags[i];
                    switch (compoundCondition.ConditionListType)
                    {
                        case ConditionListType.Disjunctive:
                            childResult |= IsConditionValid(childCondition, childNegated, depth + 1);
                            break;
                        case ConditionListType.Conjunctive:
                            childResult &= IsConditionValid(childCondition, childNegated, depth + 1);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }

                if (isNegated) childResult = !childResult;
                _evaluationLog.Add($"{indent}   evaluates to {childResult}");
                return childResult;
            }

            bool result;

            //TrueCondition
            var trueCondition = condition as TrueCondition;
            if (trueCondition != null)
            {
                result = !isNegated;
                _evaluationLog.Add($"{indent} ? True condition {EvaluationTextGet(trueCondition, isNegated, result)}");
                return result;
            }

            //FalseCondition
            var falseCondition = condition as FalseCondition;
            if (falseCondition != null)
            {
                result = isNegated;
                _evaluationLog.Add($"{indent} ? False condition {EvaluationTextGet(falseCondition, isNegated, result)}");
                return result;
            }

            //CustomerInGroupCondition
            var customerInGroupCondition = condition as CustomerInGroupCondition;
            if (customerInGroupCondition != null)
            {
                result = IsCustomerInGroup(customerInGroupCondition.CustomerDefinedGroupItemId);
                if (isNegated) result = !result;
                _evaluationLog.Add($"{indent} ? Customer in group condition {EvaluationTextGet(customerInGroupCondition, isNegated, result)}");
                return result;
            }

            //PosInGroupCondition
            var posInGroupCondition = condition as PosInGroupCondition;
            if (posInGroupCondition != null)
            {
                result = IsPosInGroup(posInGroupCondition.PosGroupId);
                if (isNegated) result = !result;
                _evaluationLog.Add($"{indent} ? Pos in group condition {EvaluationTextGet(posInGroupCondition, isNegated, result)}");
                return result;
            }

            //TenderUsedCondition
            var tenderUsedCondition = condition as TenderUsedCondition;
            if (tenderUsedCondition != null)
            {
                result = tenderUsedCondition.TenderId == _evaluationState.TenderId;
                if (isNegated) result = !result;
                _evaluationLog.Add($"{indent} ? Tender used condition {EvaluationTextGet(tenderUsedCondition, isNegated, result)}");
                return result;
            }

            //TimePeriodCondition
            var timePeriodCondition = condition as TimePeriodCondition;
            if (timePeriodCondition != null)
            {
                result = IsEvaluationTimeInTimePeriod(timePeriodCondition);
                if (isNegated) result = !result;
                _evaluationLog.Add($"{indent} ? Time period condition {EvaluationTextGet(timePeriodCondition, isNegated, result)}");
                return result;
            }

            return false;
        }

        private bool PolicySetEvaluate(PolicySet policySet, PolicySet currentParent, int depth = 0)
        {
            string indent = new string(' ', depth * IndentSize);

            if (policySet == null)
            {
                _evaluationLog.Add($"{indent}== Policy set is null (evaluation defaults to False)");
                return false;
            }

            var policyRule = policySet as PolicyRule;
            string groupOrRule = policyRule == null ? "[ ]" : "( )";
            string incarnation = IncarnationGet(policySet, currentParent);
            string policySetId = IncludePolicyObjectId ? $" ({policySet.Id})" : string.Empty;
            _evaluationLog.Add($"{indent}{groupOrRule}Processing policy {policySet.GroupOrRule()} '{policySet.Name}' {incarnation}{policySetId}");

            bool isRule = (policyRule != null);

            #region PolicyRule
            //PolicyRule
            bool ruleMatch = false;
            if (isRule)
            {
                ruleMatch = true;
                #region Enabled Status
                //Enabled Status
                switch (policyRule.EnabledStatus)
                {
                    case EnabledStatus.Enabled:
                        _evaluationLog.Add($"{indent} - Policy rule is enabled");
                        break;
                    case EnabledStatus.Disabled:
                        ruleMatch = false;
                        _evaluationLog.Add($"{indent} - Policy rule is disabled");
                        break;
                    case EnabledStatus.EnabledForDebug:
                        _evaluationLog.Add($"{indent} - Policy rule is enabled for debug.  (Conditions will be evaluated, actions will not be performed.)");
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                #endregion

                #region Validity Period
                //Validity Period
                if (ruleMatch)
                {
                    if ((policyRule.Validity != null) && (policyRule.Validity.Count > 0))
                    {
                        bool atLeastOneIsValid = false;
                        int j = 0;
                        while (j < policyRule.Validity.Count)
                        {
                            var timePeriodCondition = policyRule.Validity[j];
                            string timePeriodConditionId = IncludePolicyObjectId ? $" ({timePeriodCondition.Id})" : string.Empty;
                            if (!IsEvaluationTimeInTimePeriod(timePeriodCondition))
                            {
                                _evaluationLog.Add($"{indent} @ Validity period '{timePeriodCondition.Name}'{timePeriodConditionId} is invalid for {_evaluationState.EvaluationDateTime}");
                            }
                            else
                            {
                                atLeastOneIsValid = true;
                                _evaluationLog.Add($"{indent} @ Validity period '{timePeriodCondition.Name}'{timePeriodConditionId} is valid for {_evaluationState.EvaluationDateTime}");
                            }

                            j++;
                        }

                        if (atLeastOneIsValid)
                        {
                            _evaluationLog.Add($"{indent} - Evaluation date/time falls within at least one validity period");
                        }
                        else
                        {
                            ruleMatch = false;
                            _evaluationLog.Add($"{indent} - Evaluation date/time does not fall within at least one validity period");
                        }
                    }
                    else
                    {
                        _evaluationLog.Add($"{indent} @ No validity periods defined (always valid)");
                    }
                }
                #endregion

                #region Conditions
                //Conditions
                if (ruleMatch)
                {
                    if ((policyRule.Conditions != null) && (policyRule.Conditions.Children.Count > 0))
                    {
                        if (!IsConditionValid(policyRule.Conditions, false, depth))
                        {
                            ruleMatch = false;
                        }
                    }
                    else
                    {
                        _evaluationLog.Add($"{indent} ? No conditions defined (always true)");
                    }
                }
                #endregion

                #region Actions
                //Actions
                if (ruleMatch)
                {
                    if ((policyRule.Actions != null) && (policyRule.Actions.Count > 0))
                    {
                        foreach (var action in policyRule.Actions)
                        {
                            _actionOutputList.Add(new PolicyActionOutput { Action = action, ActionSource = policyRule, RuleIncarnation = incarnation, Debug = policyRule.EnabledStatus == EnabledStatus.EnabledForDebug });
                            string actionId = IncludePolicyObjectId ? $" ({action.Id})" : string.Empty;
                            _evaluationLog.Add($"{indent} > Action '{action.Name}'{(policyRule.EnabledStatus == EnabledStatus.EnabledForDebug ? " DEBUG" : string.Empty)}{actionId} has been added to the action list");
                        }
                    }
                    else
                    {
                        _evaluationLog.Add($"{indent} > No actions defined (nothing to do)");
                    }
                }
                #endregion
            }
            #endregion

            #region Children
            //Children
            bool childMatch = false;
            if (!isRule || ruleMatch)
            {
                bool hasChildren = (policySet.Children != null) && (policySet.Children.Count > 0);
                if (hasChildren)
                {
                    bool firstMatch = false;
                    int i = 0;
                    while ((i < policySet.Children.Count) && !firstMatch)
                    {
                        var childPolicySet = policySet.Children[i];
                        if (PolicySetEvaluate(childPolicySet, policySet, depth + 1))
                        {
                            childMatch = true;
                            if (policySet.DecisionStrategy == PolicyDecisionStrategy.FirstMatching)
                            {
                                _evaluationLog.Add($"{indent} - Policy {childPolicySet.GroupOrRule()} '{childPolicySet.Name}' met first matching criteria for policy {policyRule.GroupOrRule()} '{policySet.Name}'{policySetId}");
                                firstMatch = true;
                            }
                        }

                        i++;
                    }
                }
            }
            #endregion

            bool result = (!isRule && childMatch) || (isRule && ruleMatch);
            _evaluationLog.Add($"{indent}== Policy {policySet.GroupOrRule()} '{policySet.Name}'{policySetId} evaluated {result}");
            return result;
        }

        #endregion

        /// <summary>
        /// Constructor for a new <see cref="PolicyEvaluator"/> instance.
        /// </summary>
        /// <param name="root"></param>
        public PolicyEvaluator(PolicySet root)
        {
            _root = root;
        }

        /// <summary>
        /// Traverses the policy tree and generates an actionOutputList.
        /// </summary>
        /// <param name="evaluationState"></param>
        /// <param name="evaluationLog"></param>
        /// <param name="actionOutputList"></param>
        /// <returns>True if at least one rule was matched, although this does not guarantee that actionOutputList has any actions</returns>
        public bool PolicyEvaluate(PolicyEvaluationState evaluationState, out List<string> evaluationLog, out List<PolicyActionOutput> actionOutputList)
        {
            _evaluationState = evaluationState;
            _evaluationLog = new List<string>();
            _actionOutputList = new List<PolicyActionOutput>();
            bool result = PolicySetEvaluate(_root, null);
            evaluationLog = _evaluationLog;
            actionOutputList = _actionOutputList;
            return result;
        }

        #region Static helper functions

        /// <summary>
        /// Returns the incarnation string for a given policy set and specified parent.
        /// </summary>
        /// <param name="policySet"></param>
        /// <param name="currentParent"></param>
        /// <returns></returns>
        public static string IncarnationGet(PolicySet policySet, PolicySet currentParent)
        {
            //Incarnation [i/p]
            //The first number, represented by 'i', is the incarnation number of this policy set.
            //It is assigned in the order in which the policy set is encountered during a pre-order traversal
            //The second number, represented by 'p', is the number of parents of this policy set.
            int incarnation = 0;
            int parentCount = 0;
            if ((policySet.Parents != null) && (currentParent != null))
            {
                incarnation = policySet.Parents.IndexOf(currentParent);
                parentCount = policySet.Parents.Count;
            }

            return $"[{incarnation + 1}/{parentCount}]";
        }

        /// <summary>
        /// Extracts the discount/surcharge rules from the action output list.
        /// </summary>
        /// <param name="actionOutputList"></param>
        /// <returns></returns>
        public static List<DiscountSurchargeRule> DiscountSurchargeRulesExtract(List<PolicyActionOutput> actionOutputList)
        {
            var list = new List<DiscountSurchargeRule>();

            //Build discount list (ignoring any outputs with Debug flag set)
            var outputDiscounts = (from output in actionOutputList let action = (output.Action as ApplyDiscountAction) where (action != null) && !output.Debug select output).ToList();
            if (outputDiscounts.Count > 0)
            {
                list.AddRange(outputDiscounts.Select(output => new DiscountSurchargeRule { Type = DiscountSurchargeType.Discount, Rate = ((ApplyDiscountAction)output.Action).Rate }).ToList());
            }

            //Build surcharge list (ignoring any outputs with Debug flag set)
            var outputSurcharges = (from output in actionOutputList let action = (output.Action as ApplySurchargeAction) where (action != null) && !output.Debug select output).ToList();
            if (outputSurcharges.Count > 0)
            {
                list.AddRange(outputSurcharges.Select(output => new DiscountSurchargeRule { Type = DiscountSurchargeType.Surcharge, Rate = ((ApplySurchargeAction)output.Action).Rate }).ToList());
            }

            return list;
        }

        /// <summary>
        /// Extracts the account drain order from the action output list.  If multiple drain order groups, then last one wins.
        /// </summary>
        /// <param name="actionOutputList"></param>
        /// <returns></returns>
        public static List<int> DrainOrderExtract(List<PolicyActionOutput> actionOutputList)
        {
            var list = new List<int>();

            //SV account drain (ignoring any outputs with Debug flag set)
            var outputStoredValueAccounts = (from output in actionOutputList let action = (output.Action as UseStoredValueAccounts) where (action != null) && !output.Debug select output).ToList();
            if (outputStoredValueAccounts.Count > 0)
            {
                list.AddRange(((UseStoredValueAccounts)outputStoredValueAccounts.Last().Action).StoredValueAccountTypeIds);
            }

            return list;
        }

        /// <summary>
        /// Extracts the stored value transaction limit ids.
        /// </summary>
        /// <param name="actionOutputList"></param>
        /// <returns></returns>
        public static List<int> TransactionLimitIdsExtract(List<PolicyActionOutput> actionOutputList)
        {
            var list = new List<int>();

            //SV transaction limit IDs (ignoring any outputs with Debug flag set)
            var transactionLimitIds = (from output in actionOutputList let action = (output.Action as ApplyStoredValueTransactionLimit) where (action != null) && !output.Debug select output).ToList();
            if (transactionLimitIds.Count <= 0) return list;

            list.AddRange(transactionLimitIds.Select(transactionLimitId => ((ApplyStoredValueTransactionLimit)transactionLimitId.Action).StoredValueTransactionLimitId));

            return list;
        }
        
        #endregion
    }
}
