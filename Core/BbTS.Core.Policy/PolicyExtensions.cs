﻿using System.Linq;
using BbTS.Domain.Models.Exceptions.Policy;
using BbTS.Domain.Models.Policy;
using BbTS.Domain.Models.Policy.Actions;
using BbTS.Domain.Models.Policy.Conditions;

namespace BbTS.Core.Policy
{
    /// <summary>
    /// Extensions for building and managing a policy tree.
    /// </summary>
    public static class PolicyExtensions
    {
        /// <summary>
        /// Verifies that the policy set can be a child of the parent.  Generates an InvalidPolicyStructureException when structure would be invalid.
        /// </summary>
        /// <param name="child"></param>
        /// <param name="parent"></param>
        private static void CheckPolicyStructure(PolicySet child, PolicySet parent)
        {
            //Can't be the same as parent
            if (child == parent) throw new InvalidPolicyStructureException($"Cannot link policy sets.  Parent and child policy set objects are the same:  Policy {child.GroupOrRule()} '{child.Name}' ({child.Id}).");

            //Can't already be a child of the parent
            if (parent.Children.Any(c => child == c)) throw new InvalidPolicyStructureException($"Cannot link policy sets.  Policy {child.GroupOrRule()} '{child.Name}' ({child.Id}) is already a child of policy {parent.GroupOrRule()} '{parent.Name}' ({parent.Id}).");

            //Can't be an ancestor of the parent
            if (child.IsAncestorOf(parent)) throw new InvalidPolicyStructureException($"Cannot link policy sets.  Policy {child.GroupOrRule()} '{child.Name}' ({child.Id}) is an ancestor of policy {parent.GroupOrRule()} '{parent.Name}' ({parent.Id}).");
        }

        /// <summary>
        /// Checks if this policy set is an ancestor of the target.
        /// </summary>
        /// <param name="thisPolicySet"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static bool IsAncestorOf(this PolicySet thisPolicySet, PolicySet target)
        {
            if (target.Parents == null) return false;

            foreach (var ancestor in target.Parents)
            {
                if ((thisPolicySet == ancestor) || thisPolicySet.IsAncestorOf(ancestor)) return true;
            }

            return false;
        }

        /// <summary>
        /// Checks if this policy set is a descendant of the target.
        /// </summary>
        /// <param name="thisPolicySet"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static bool IsDescendantOf(this PolicySet thisPolicySet, PolicySet target)
        {
            if (target.Children == null) return false;

            foreach (var descendant in target.Children)
            {
                if ((thisPolicySet == descendant) || thisPolicySet.IsDescendantOf(descendant)) return true;
            }

            return false;
        }

        /// <summary>
        /// Returns group or rule text depending on what the policy set is.
        /// </summary>
        /// <param name="policySet"></param>
        /// <returns></returns>
        public static string GroupOrRule(this PolicySet policySet)
        {
            var policyRule = policySet as PolicyRule;
            return policyRule == null ? "group" : "rule";
        }

        /// <summary>
        /// Adds the policy set as a child.
        /// </summary>
        /// <param name="thisPolicySet"></param>
        /// <param name="child"></param>
        public static void AddChild(this PolicySet thisPolicySet, PolicySet child)
        {
            CheckPolicyStructure(child, thisPolicySet);
            thisPolicySet.Children.Add(child);
            child.Parents.Add(thisPolicySet);
        }

        /// <summary>
        /// Adds the policy set as a child at the specified position.
        /// </summary>
        /// <param name="thisPolicySet"></param>
        /// <param name="child"></param>
        /// <param name="position"></param>
        public static void InsertChild(this PolicySet thisPolicySet, PolicySet child, int position)
        {
            if (position < thisPolicySet.Children.Count)
            {
                if (position < 0) position = 0;
                CheckPolicyStructure(child, thisPolicySet);
                thisPolicySet.Children.Insert(position, child);
                child.Parents.Add(thisPolicySet);
            }
            else
            {
                thisPolicySet.AddChild(child);
            }
        }

        /// <summary>
        /// Adds the policy set as a parent.
        /// </summary>
        /// <param name="thisPolicySet"></param>
        /// <param name="parent"></param>
        public static void AddParent(this PolicySet thisPolicySet, PolicySet parent)
        {
            CheckPolicyStructure(thisPolicySet, parent);
            thisPolicySet.Parents.Add(parent);
            parent.Children.Add(thisPolicySet);
        }

        /// <summary>
        /// Adds a validity period to the policy rule.
        /// </summary>
        /// <param name="policyRule"></param>
        /// <param name="validityPeriod"></param>
        public static void AddValidityPeriod(this PolicyRule policyRule, TimePeriodCondition validityPeriod)
        {
            policyRule.Validity.Add(validityPeriod);
            validityPeriod.UsageCount++;
        }

        /// <summary>
        /// Adds a condition to the compound condition.
        /// </summary>
        /// <param name="compoundCondition"></param>
        /// <param name="condition"></param>
        /// <param name="isNegated"></param>
        public static void AddCondition(this CompoundPolicyCondition compoundCondition, PolicyCondition condition, bool isNegated = false)
        {
            compoundCondition.Children.Add(condition);
            compoundCondition.NegationFlags.Add(isNegated);
            condition.UsageCount++;
        }

        /// <summary>
        /// Adds an action to the policy rule.
        /// </summary>
        /// <param name="policyRule"></param>
        /// <param name="action"></param>
        public static void AddAction(this PolicyRule policyRule, PolicyAction action)
        {
            policyRule.Actions.Add(action);
            action.UsageCount++;
        }
    }
}