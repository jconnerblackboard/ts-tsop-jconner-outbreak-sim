﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BbTS.Core.Conversion.Test
{
    [TestClass]
    public class FormattingTest
    {
        [TestMethod]
        public void PadCardNumberTest1()
        {
            Assert.AreEqual(Formatting.PadCardNumber(null, 10, '0'), "0000000000");
            Assert.AreEqual(Formatting.PadCardNumber("1", 10, '0'),"0000000001");
            Assert.AreEqual(Formatting.PadCardNumber("1234567890123456789", 10, '0'), "1234567890123456789");
            Assert.AreEqual(Formatting.PadCardNumber("1234", 10, '1'), "1111111234");
        }
    }
}
