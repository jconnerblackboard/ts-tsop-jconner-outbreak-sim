using System;
using System.Globalization;
using System.Linq;

namespace BbTS.Core.Utility
{
    public abstract class LicenseBase
    {
        public string InstitutionName { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public string LicenseNumber { get; set; }
        public string VersionString { get; protected set; }

        #region Abstract Methods

        public abstract bool Generate();
        protected abstract bool IsValid();
        protected abstract string Calculate();

        #endregion

        #region Base Transpose Methods

        /// <summary>
        /// Transposes characters in a string.
        /// </summary>
        /// <param name="temp">The string where transposition is performed.</param>
        /// <returns>toReturn [String] - string with transposed characters.</returns>
        protected static string Transpose(string temp)
        {
            if (temp.Length > 1)
            {
                for (int i = 0; i < temp.Length; i++)
                {
                    int j = 0;
                    int k = 1;
                    do
                    {
                        temp = SwapCharacters(temp, j, k);
                        //perform a shift to the right and wrap the last character to the front.
                        temp = temp.Substring(temp.Length - 1, 1) + temp.Substring(0, temp.Length - 1);
                        j += 1;
                        k += 3;
                    } while (k < temp.Length);
                }
            }

            return temp;
        }

        /// <summary>
        /// Untransposes characters in a transposed string.
        /// </summary>
        /// <param name="temp">The string where untransposition is performed.</param>
        /// <returns>toReturn [String] - string with untransposed characters.</returns>
        public static string UnTranspose(string temp)
        {
            if (temp.Length > 1)
            {
                for (int i = 0; i < temp.Length; i++)
                {
                    int j = 0;
                    int k = 1;
                    do
                    {
                        j += 1;
                        k += 3;
                    } while (k < temp.Length);

                    j -= 1;
                    k -= 3;
                    while (k > 2)
                    {
                        temp = temp.Substring(1, temp.Length - 1) + temp.Substring(0, 1);
                        temp = SwapCharacters(temp, j, k);
                        j -= 1;
                        k -= 3;
                    }

                    temp = temp.Substring(1, temp.Length - 1) + temp.Substring(0, 1);
                    temp = SwapCharacters(temp, j, k);
                }
            }

            return temp;
        }

        /// <summary>
        /// Switch characters in a string at the specified indexes.
        /// </summary>
        /// <param name="value">The string where replacement is performed.</param>
        /// <param name="position1">The index of the first character.</param>
        /// <param name="position2">The index of the second character.</param>
        /// <returns>toReturn [String] - string with replaced characters.</returns>
        protected static string SwapCharacters(string value, int position1, int position2)
        {
            char[] array = value.ToCharArray();
            char temp = array[position1];
            array[position1] = array[position2];
            array[position2] = temp;
            return new string(array);
        }

        #endregion
    }

    public class License2X : LicenseBase
    {
        private const string Users = "099";

        public License2X()
        {
            VersionString = "Version 2.x";
        }

        public override bool Generate()
        {
            // Basic error checking
            int institutionNameLength = InstitutionName.Trim().Length;

            if (institutionNameLength == 0)
            {
                throw new BbLicenseException("Instituton name cannot be blank.");
            }

            if (ExpirationDate == null)
            {
                throw new BbLicenseException("Expiration date cannot be blank.");
            }

            if (institutionNameLength < 5 || institutionNameLength > 20)
            {
                throw new BbLicenseException("The length of the Institution Name must be at least 5 characters long and no more than 20 characters long.");
            }

            // Calculate the license
            LicenseNumber = Calculate();

            // return the object to the caller
            return true;
        }

        protected override string Calculate()
        {
            string institute = InstitutionName.Trim();
            string locExpire = String.Format("{0:MMddyyyy}", ExpirationDate);

            long accumGen = institute.Aggregate<char, long>(0, (current, t) => current + ((int) t * 11));
            accumGen = Users.Aggregate(accumGen, (current, t) => current - ((int) t * 3));
            accumGen = locExpire.Aggregate(accumGen, (current, t) => current + ((int) t * 7));

            string accumGenString = (accumGen % 831).ToString(CultureInfo.InvariantCulture);
            accumGenString = accumGenString.PadLeft(3, '0');
            string temp = Users + locExpire + accumGenString;

            temp = Transpose(temp);
            temp = temp.Insert(5, "-");
            temp = temp.Insert(10, "-");

            LicenseNumber = temp;

            if (!IsValid())
            {
                throw new BbLicenseException("An error occured while generating the license");
            }

            return temp;
        }

        protected override bool IsValid()
        {
            string institute = InstitutionName.Trim();
            string licenseNumber = LicenseNumber.Replace("-", "");
            licenseNumber = UnTranspose(licenseNumber);

            string users = licenseNumber.Substring(0, 3);
            string expirationDate = licenseNumber.Substring(3, 2) + "/" + licenseNumber.Substring(5, 2) + "/" + licenseNumber.Substring(7, 4);
            string locExpire = expirationDate.Replace("/", "");

            string accumLicense = licenseNumber.Substring(11, 3);

            long accumGen = institute.Aggregate<char, long>(0, (current, t) => current + ((int) t * 11));
            accumGen = users.Aggregate(accumGen, (current, t) => current - ((int) t * 3));
            accumGen = locExpire.Aggregate(accumGen, (current, t) => current + ((int) t * 7));

            string accumGenString = (accumGen % 831).ToString(CultureInfo.InvariantCulture);
            accumGenString = accumGenString.PadLeft(3, '0');
            return accumGenString.Equals(accumLicense);
        }
    }

    public class License3X : LicenseBase
    {
        public bool IncludesDoorAccessLicense { get; set; }

        public License3X()
        {
            VersionString = "Version 3.x";
        }

        public override bool Generate()
        {
            // Basic error checking
            int institutionNameLength = InstitutionName.Trim().Length;

            if (institutionNameLength == 0)
            {
                throw new BbLicenseException("Instituton name cannot be blank.");
            }

            if (ExpirationDate == null)
            {
                throw new BbLicenseException("Expiration date cannot be blank.");
            }

            if (institutionNameLength < 5 || institutionNameLength > 20)
            {
                throw new BbLicenseException("The length of the Institution Name must be at least 5 characters long and no more than 20 characters long.");
            }

            // Calculate the license
            LicenseNumber = Calculate();

            // return the object to the caller
            return true;
        }

        protected override string Calculate()
        {
            Random random = new Random();
            string institute = InstitutionName.Trim().ToUpper();
            string locExpire = String.Format("{0:MMddyyyy}", ExpirationDate);
            string randomJunk = random.Next(111, 999).ToString(CultureInfo.InvariantCulture);

            long accumGen = institute.Aggregate<char, long>(0, (current, t) => current + ((int) t * 17));
            accumGen = randomJunk.Aggregate(accumGen, (current, t) => current - ((int) t * 3));
            accumGen = locExpire.Aggregate(accumGen, (current, t) => current + ((int) t * 7));

            if (IncludesDoorAccessLicense)
            {
                accumGen -= 42;
            }

            string accumGenString = (accumGen % 831).ToString(CultureInfo.InvariantCulture);
            accumGenString = accumGenString.PadLeft(3, '0');
            string temp = randomJunk + locExpire + accumGenString;

            if (IncludesDoorAccessLicense)
            {
                temp = random.Next(17, 19).ToString(CultureInfo.InvariantCulture) + temp;
            }
            else
            {
                temp = random.Next(10, 16).ToString(CultureInfo.InvariantCulture) + temp;
            }

            temp = Transpose(temp);
            temp = temp.Insert(4, "-");
            temp = temp.Insert(9, "-");
            temp = temp.Insert(14, "-");

            LicenseNumber = temp;

            if (!IsValid())
            {
                throw new BbLicenseException("An error occured while generating the license");
            }

            return temp;
        }

        public DateTime ExpirationDateGet()
        {
            string licenseNumber = LicenseNumber.Replace( "-","" );
            licenseNumber = UnTranspose( licenseNumber );
            
            return new DateTime(Convert.ToInt32(licenseNumber.Substring(9, 4)), Convert.ToInt32(licenseNumber.Substring(5, 2)), Convert.ToInt32(licenseNumber.Substring(7, 2)));
        }

        protected override bool IsValid()
        {
            string institute = InstitutionName.Trim().ToUpper();
            string licenseNumber = LicenseNumber.Replace("-", "");
            licenseNumber = UnTranspose(licenseNumber);

            char[] x = { '7', '8', '9' };
            IncludesDoorAccessLicense = x.Contains(licenseNumber[1]);

            string randomJunk = licenseNumber.Substring(2, 3);
            string expirationDate = licenseNumber.Substring(5, 2) + "/" + licenseNumber.Substring(7, 2) + "/" + licenseNumber.Substring(9, 4);
            string locExpire = expirationDate.Replace("/", "");

            string accumLicense = licenseNumber.Substring(13, 3);

            long accumGen = institute.Aggregate<char, long>(0, (current, t) => current + ((int) t * 17));
            accumGen = randomJunk.Aggregate(accumGen, (current, t) => current - ((int) t * 3));
            accumGen = locExpire.Aggregate(accumGen, (current, t) => current + ((int) t * 7));

            if (IncludesDoorAccessLicense)
            {
                accumGen -= 42;
            }

            string accumGenString = (accumGen % 831).ToString(CultureInfo.InvariantCulture);
            accumGenString = accumGenString.PadLeft(3, '0');
            return accumGenString.Equals(accumLicense);
        }
    }

    /// <summary>
    /// This class defines a License specific exception which inherits a Blackboard exception
    /// </summary>
    [Serializable]
    public class BbLicenseException : BlackboardException
    {
        public BbLicenseException(string message)
            : base(message)
        {
        }
    }
}