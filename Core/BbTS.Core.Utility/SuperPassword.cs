using System;
using System.Globalization;

namespace BbTS.Core.Utility
{
    public static class SuperPassword
    {
        /// <summary>
        /// Calculates the password of the day for the specified date
        /// </summary>
        /// <param name="date">The date used for calculations</param>
        /// <returns>The password of the day</returns>
        public static string CalculateSuperPassword(DateTime date)
        {
            //This code is based on DoCalculateSuperPassword in un0049.pas

            const int binaryDateLength = 25;

            //Change the date into a number
            int dateNumber = (date.Year * 10000) + (date.Month * 100) + (date.Day);
            
            //Convert number to binary
            int[] binaryDate = new int[binaryDateLength];
            for (int i = binaryDateLength - 1; i >= 0; i--)
            {
                binaryDate[(binaryDateLength - 1) - i] = ((dateNumber >> i) & 1);
            }

            //Flip the binary digits
            int[] binaryDateFlipped = new int[binaryDateLength];
            for (int i = 0; i < binaryDateLength; i++)
            {
                binaryDateFlipped[i] = binaryDate[(binaryDateLength - 1) - i];
            }

            //Convert flipped binary number to int
            int flippedDateNumber = 0;
            for (int i = 0; i < binaryDateLength; i++)
            {
                if (binaryDateFlipped[i] == 1)
                {
                    flippedDateNumber += Convert.ToInt32(Math.Pow(2, (binaryDateLength - 1) - i));
                }
            }

            //Convert the number to a string
            string flippedDateString = flippedDateNumber.ToString(CultureInfo.InvariantCulture);

            //If too big (72% of the time), lop off the extra, if too small (3% of the time), add leading zeros
            flippedDateString = (flippedDateString.Length > 7) ? flippedDateString.Substring(0, 7) : flippedDateString.PadLeft(7, '0');

            //Send back 7 character password
            return flippedDateString;
        }
    }
}
