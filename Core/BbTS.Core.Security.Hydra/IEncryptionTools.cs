﻿using System.Runtime.InteropServices;
using RemObjects.Hydra.CrossPlatform;

namespace BbTS.Core.Security.Hydra
{
    /// <summary>
    /// A Hydra interface that allows Delphi programs access to some of our core security logic.
    /// </summary>
    [Guid("03ef637b-d52e-44db-970b-f231fea82f06")]
    public interface IEncryptionTools : IHYCrossPlatformInterface
    {
        /// <summary>
        /// Encrypts a plaintext string and returns it as a Base64 encoded string.
        /// </summary>
        /// <param name="plainText"></param>
        /// <returns></returns>
        string EncryptString(string plainText);

        /// <summary>
        /// Decrypts a Base64 ciphertext string and returns a plaintext string.
        /// </summary>
        /// <param name="cipherText"></param>
        /// <returns></returns>
        string DecryptString(string cipherText);
    }
}
