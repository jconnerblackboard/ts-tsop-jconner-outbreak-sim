﻿using System;
using System.ComponentModel;
using BbTS.Core.Conversion;
using BbTS.Core.Security.Encryption;
using RemObjects.Hydra;

namespace BbTS.Core.Security.Hydra
{
    [Plugin, NonVisualPlugin]
    public partial class EncryptionToolsPlugin : NonVisualPlugin, IEncryptionTools
    {
        public EncryptionToolsPlugin()
        {
            InitializeComponent();
        }

        public EncryptionToolsPlugin(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        /// <summary>
        /// Encrypts a plaintext string and returns it as a Base64 encoded string.
        /// </summary>
        /// <param name="plainText"></param>
        /// <returns></returns>
        public string EncryptString(string plainText)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Decrypts a Base64 ciphertext string and returns a plaintext string.
        /// </summary>
        /// <param name="cipherText"></param>
        /// <returns></returns>
        public string DecryptString(string cipherText)
        {
            var aes = new AesEncryptionProvider();
            return aes.Decrypt(cipherText.Base64Decode());
        }
    }
}
