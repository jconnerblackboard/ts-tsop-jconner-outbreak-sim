﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;
using BbTS.Core.Serialization;

namespace BbTS.Core.Conversion
{
    /// <summary>
    /// Utility class to enable formatting of an MSMQ bosy as Json.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class JsonMessageFormatter<T> : IMessageFormatter
    {
        private readonly Encoding _encoding = Encoding.UTF8;

        /// <summary>
        /// Empty Constructor.
        /// </summary>
        public JsonMessageFormatter()
        {
        }

        /// <summary>
        /// Parameterized Constructor.
        /// </summary>
        /// <param name="encoding">Encoding to use when serializing/deserializing.</param>
        public JsonMessageFormatter(Encoding encoding)
        {
            _encoding = encoding;
        }

        /// <summary>
        /// Clone this formatter.
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            return new JsonMessageFormatter<T>(_encoding);
        }

        /// <summary>
        /// Can read.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool CanRead(Message message)
        {
            if (message == null)
            {
                throw new ArgumentNullException(nameof(message));
            }

            var stream = message.BodyStream;
            return stream != null
                && stream.CanRead
                && stream.Length > 0;
        }

        /// <summary>
        /// Read a message.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public object Read(Message message)
        {
            if (message == null)
            {
                throw new ArgumentNullException(nameof(message));
            }

            using (var reader = new StreamReader(message.BodyStream, _encoding))
            {
                var json = reader.ReadToEnd();
                return NewtonsoftJson.Deserialize<T>(json);
            }
        }

        /// <summary>
        /// Write a message.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="obj"></param>
        public void Write(Message message, object obj)
        {
            if (message == null)
            {
                throw new ArgumentNullException(nameof(message));
            }
            string json = NewtonsoftJson.Serialize(obj);
            message.BodyStream = new MemoryStream(_encoding.GetBytes(json));
        }
    }
}
