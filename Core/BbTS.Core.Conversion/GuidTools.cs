﻿using System;

namespace BbTS.Core.Conversion
{
    /// <summary>
    /// Contains methods for testing Guid equality when one or more of the Guids is stored as a string.
    /// </summary>
    public static class GuidTools
    {
        /// <summary>
        /// Returns true if A and B parse to valid Guids and those Guids are equal.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool EqualsGuid(this string a, string b)
        {
            Guid ga;
            Guid gb;
            if (Guid.TryParse(a, out ga) && Guid.TryParse(b, out gb))
            {
                return ga.Equals(gb);
            }

            return false;
        }

        /// <summary>
        /// Returns true if A parses to a valid Guid and equals B.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool EqualsGuid(this string a, Guid b)
        {
            Guid g;
            return Guid.TryParse(a, out g) && g.Equals(b);
        }

        /// <summary>
        /// Returns true if A parses to a valid Guid and equals B.  Returns false if B is null.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool EqualsGuid(this string a, Guid? b)
        {
            return b != null && EqualsGuid(a, b.Value);
        }

        /// <summary>
        /// Returns true if B parses to a valid Guid and equals A.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool EqualsGuid(this Guid a, string b)
        {
            Guid g;
            return Guid.TryParse(b, out g) && a.Equals(g);
        }

        /// <summary>
        /// Returns true if A equals B.  Just a wrapper for Guid.Equals.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool EqualsGuid(this Guid a, Guid b)
        {
            return a.Equals(b);
        }
    }
}
