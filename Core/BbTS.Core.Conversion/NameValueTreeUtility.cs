﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using BbTS.Core.Serialization;
using BbTS.Domain.Models.Container;

namespace BbTS.Core.Conversion
{
    /// <summary>
    /// Utility class to handle creation, formatting and editing of a NameValueTree
    /// </summary>
    public class NameValueTreeUtility
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static NameValueTree Parse(object obj, string propertyName)
        {
            var node = new NameValueTree
            {
                Name = propertyName
            };


            if (obj != null)
            {
                node.Type = obj.GetType().ToString();

                if (Formatting.IsPrimitave(obj))
                {
                    node.Value = Formatting.FormatPrimitaveToString(obj);
                }
                
                else
                {
                    var enumerable = obj as IEnumerable<object>;
                    if (enumerable != null)
                    {
                        node.Value = node.Type;
                        var objAsList = enumerable.ToList();
                        foreach (var objItem in objAsList)
                        {
                            var list = objItem.GetType().GetProperties().ToList();
                            foreach (var item in list)
                            {
                                var itemValue = item.GetValue(objItem, null);
                                var child = Parse(itemValue, item.Name);
                                node.Children.Add(child);
                            }
                        }
                    }
                    else
                    {
                        node.Value = NewtonsoftJson.Serialize(obj);

                        var list = obj.GetType().GetProperties().ToList();

                        foreach (var item in list)
                        {
                            var itemValue = item.GetValue(obj, null);
                            var child = Parse(itemValue, item.Name);
                            node.Children.Add(child);
                        }
                    }
                }
            }

            return node;
        }

        /// <summary>
        /// Format a tree list into a string.
        /// </summary>
        /// <param name="list">List of <see cref="NameValueTree"/></param>
        /// <returns></returns>
        public static string FormatTreeList(List<NameValueTree> list)
        {
            return "";
        }

        /*
        /// <summary>
        /// Parse an object, convert its properties to a name value tree node.  Leaf nodes are primitive
        /// types.  Complex objects will be tree'd until a primitive is found.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public NameValueTree Parse(object obj)
        {
            if (obj == null) return null;

            var root = new NameValueTree();
            if (obj.GetType().IsPrimitive)
            {
                root.Name = obj.GetType().ToString();
                root.Value = Formatting.FormatPrimitaveToString(obj);
                return root;
            }

            var list = obj.GetType().GetProperties().ToList();

            foreach (var item in list)
            {
                var node = Parse(item.GetValue(this, null), item.Name);
                root.Children.Add(node);
            }

            return root;
        }
        */
    }
}
