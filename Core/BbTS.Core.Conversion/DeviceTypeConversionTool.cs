﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Domain.Models.Definitions.Device;
using BbTS.Domain.Models.Definitions.Licensing;

namespace BbTS.Core.Conversion
{
    /// <summary>
    /// Tool class for converting between device types.
    /// </summary>
    public static class DeviceTypeConversionTool
    {
        /// <summary>
        /// Convert a <see cref="HardwareModel"/> type to a <see cref="LicenseServerDeviceSubType"/>.
        /// </summary>
        /// <param name="model">The hardware model to convert.</param>
        /// <returns>The corresponding LicenseServerDeviceSubType or <see cref="LicenseServerDeviceSubType.Unknown"/> if no matching type is found.</returns>
        public static LicenseServerDeviceSubType HardwareModelToLicenseServerDeviceSubType(HardwareModel model)
        {
            switch (model)
            {
                case HardwareModel.MasterControllerSA3004:
                    return LicenseServerDeviceSubType.SA3004;
                case HardwareModel.MasterControllerSA3032:
                    return LicenseServerDeviceSubType.SA3032;
                case HardwareModel.CR4100:
                    return LicenseServerDeviceSubType.MF4100C;
                case HardwareModel.LC4100:
                    return LicenseServerDeviceSubType.MF4100L;
                case HardwareModel.VR4100:
                    return LicenseServerDeviceSubType.VR4100;
                default:
                    return LicenseServerDeviceSubType.Unknown;
            }
        }

        /// <summary>
        /// Convert a <see cref="HardwareModel"/> type to a <see cref="LicenseServerDeviceSubType"/>.
        /// </summary>
        /// <param name="model">The hardware model to convert.</param>
        /// <returns>The corresponding LicenseServerDeviceType or <see cref="LicenseServerDeviceType.Unknown"/> if no matching type is found.</returns>
        public static LicenseServerDeviceType HardwareModelToLicenseServerDeviceType(HardwareModel model)
        {
            switch (model)
            {
                case HardwareModel.MasterControllerSA3004:
                case HardwareModel.MasterControllerSA3032:
                    return LicenseServerDeviceType.DoorComm;
                case HardwareModel.CR4100:
                case HardwareModel.LC4100:
                case HardwareModel.VR4100:
                    return LicenseServerDeviceType.Pos;
                default:
                    return LicenseServerDeviceType.Unknown;
            }
        }

    }
}
