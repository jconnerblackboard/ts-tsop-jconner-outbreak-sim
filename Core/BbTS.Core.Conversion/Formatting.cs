﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using BbTS.Core.Serialization;
using BbTS.Domain.Models.Definitions.Board;
using BbTS.Domain.Models.Definitions.DefinedField;
using BbTS.Domain.Models.Definitions.Event;
using BbTS.Domain.Models.Exceptions.Service;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BbTS.Core.Conversion
{
    public enum GuidFormatSpecifier
    {
        NoDashes,
        Dashes,
        DashesAndBraces,
        DashesAndParenthesis,
        HexAndBraces
    }

    public class Formatting
    {
        private static readonly Regex _whiteSpace = new Regex(@"\s+");


        private static Dictionary<GuidFormatSpecifier, string> _guidFormatSpecifiers = new Dictionary<GuidFormatSpecifier, string>
        {
            { GuidFormatSpecifier.NoDashes, "N" },
            { GuidFormatSpecifier.Dashes, "D" },
            { GuidFormatSpecifier.DashesAndBraces, "B" },
            { GuidFormatSpecifier.DashesAndParenthesis, "P" },
            { GuidFormatSpecifier.HexAndBraces, "X" },
        };

        public static JsonSerializerSettings DeafaultJsonSerializerSettings = new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.Auto,
            Formatting = Newtonsoft.Json.Formatting.None,
            Converters = { new StringEnumConverter() },
            ContractResolver = BaseFirstContractResolver.Instance
        };

        /// <summary>
        /// Remove whitespace from a string.
        /// </summary>
        /// <param name="value">string to replace white space from.</param>
        /// <returns>formatted string.</returns>
        public static string RemoveWhitespace(string value)
        {
            return Regex.Replace(value, @"\s+", "");
        }

        /// <summary>
        /// Format the exception into a nicer string that includes the inner exception message if it exists.
        /// </summary>
        /// <param name="ex"><see cref="Exception"/> to format</param>
        /// <returns></returns>
        public static string FormatException(Exception ex)
        {
            var aex = ex as AggregateException;
            if (aex != null)
            {
                return FormatAggregrateException(aex);
            }

            var wex = ex as WebServiceCommunicationFailedException;
            if (wex != null)
            {
                return FormatWebServiceCommunicationFailedException(wex);
            }

            var message = $"{ex.GetType()}: {ex.Message}" + (ex.Message.EndsWith(".") ? "" : ".");
            var innerMessage = 
                ex.InnerException != null ? 
                $"{ex.InnerException.Message}" + (ex.InnerException.Message.EndsWith(".") ? "" : ".") :
                "";
            return $"{message}  {innerMessage}";
        }

        /// <summary>
        /// Format the exception into a nicer string that includes the inner exception message if it exists.
        /// </summary>
        /// <param name="wex"><see cref="WebServiceCommunicationFailedException"/> to format</param>
        /// <returns>Formatted message.</returns>
        public static string FormatWebServiceCommunicationFailedException(WebServiceCommunicationFailedException wex)
        {
            var message = $"{wex.Message}" + (wex.Message.EndsWith(".") ? "" : ".");
            return $"WebServiceCommunicationFailedException: Communication to the external service failed with code {(int)wex.ResponseCode}.  {message}";
        }

        /// <summary>
        /// Format the exception into a nicer string that includes the inner exception message if it exists.
        /// </summary>
        /// <param name="ex"><see cref="AggregateException"/> to format</param>
        /// <returns>Formatted message.</returns>
        public static string FormatAggregrateException(AggregateException ex)
        {
            var message = $"AggregateException: {ex.Message}" + (ex.Message.EndsWith(".") ? "  " : ".  ");

            foreach (var iex in ex.InnerExceptions)
            {
                var wex = iex as WebServiceCommunicationFailedException;
                var exceptionMessage = wex == null
                    ? FormatException(iex)
                    : FormatWebServiceCommunicationFailedException(wex);
                exceptionMessage = $"{exceptionMessage}" + (exceptionMessage.EndsWith(".") ? "" : ".");
                message = $"{message}  {exceptionMessage}";

            }
            return message;
        }

        /// <summary>
        /// Constructs a QueryString (string).
        /// Consider this method to be the opposite of "System.Web.HttpUtility.ParseQueryString"
        /// </summary>
        /// <param name="nvc">NameValueCollection</param>
        /// <returns>string</returns>
        public static string ConstructQueryString(NameValueCollection parameters)
        {
            List<string> items = new List<string> ();

            foreach (string name in parameters)
                items.Add(string.Concat(HttpUtility.UrlEncode(name), "=", HttpUtility.UrlEncode(parameters[name])));

            return string.Join("&", items.ToArray());
        }

        /// <summary>
        /// Format a guid to the specified format.
        /// </summary>
        /// <param name="specifier">GuidFormatSpecifier enumeration flag</param>
        /// <param name="inputGuidString">string to attempt to format</param>
        /// <param name="guidString">formatted string</param>
        /// <param name="message">result message</param>
        /// <returns>operation successful boolean</returns>
        public static bool VerifyAndFormatGuid(GuidFormatSpecifier specifier, string inputGuidString, out string guidString, out string message)
        {
            try
            {
                var guid = Guid.Parse(inputGuidString);
                guidString = guid.ToString(_guidFormatSpecifiers[specifier]);

                message = "Success";
                return true;
            }
            catch (Exception ex)
            {
                guidString = null;
                message = String.Format("{0}{1}", ex.Message, ex.InnerException == null ? "" : String.Format(". {0}", ex.InnerException.Message));
                return false;
            }
        }

        /// <summary>
        /// Format the string for Xml style
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static String FormatXml(String xml)
        {
            string formattedMessage = "";
            int tabCount = 0;
            bool lastSawClosingTag = false;
            for (int i = 0; i < xml.Length; ++i)
            {

                switch (xml[i])
                {
                    case '>':
                        {
                            formattedMessage += xml[i];
                            if (i < xml.Length - 1 &&
                                xml[i + 1] == '<')
                            {
                                formattedMessage += "\r\n";
                            }
                        }
                        break;
                    case '<':
                        {
                            if (i < xml.Length - 1 &&
                                xml[i + 1] == '/')
                            {
                                tabCount--;
                                if (lastSawClosingTag)
                                {
                                    for (int currtab = 0; currtab < tabCount; currtab++)
                                    {
                                        formattedMessage += "\t";
                                    }
                                }
                                formattedMessage += xml[i];
                                lastSawClosingTag = true;
                            }
                            else if (i < xml.Length - 1 &&
                                     xml[i + 1] == '?')
                            {
                                formattedMessage += xml[i];
                            }
                            else
                            {
                                lastSawClosingTag = false;
                                for (int currtab = 0; currtab < tabCount; currtab++)
                                {
                                    formattedMessage += "\t";
                                }
                                formattedMessage += xml[i];
                                tabCount++;
                            }
                        }
                        break;
                    case '/':
                        {
                            if (i < xml.Length - 1 &&
                                xml[i + 1] == '>')
                            {
                                tabCount--;
                                lastSawClosingTag = true;
                            }
                            formattedMessage += xml[i];
                        }
                        break;
                    default:
                        {
                            formattedMessage += xml[i];
                        }
                        break;
                }
            }
            return formattedMessage;
        }

        /// <summary>
        /// Encode a string in Http format
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static String EncodeHttp(String value)
        {
            String encoded = value;

            encoded = encoded.Replace("<", "&lt;");
            encoded = encoded.Replace(">", "&gt;");
            encoded = encoded.Replace("'", "&apos;");
            encoded = encoded.Replace("\"", "&quot;");
            //encoded = encoded.Replace("&", "&amp;");

            return encoded;
        }

        /// <summary>
        /// Decode a string in Http format
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static String DecodeHttp(String value)
        {
            String decoded = value;

            decoded = decoded.Replace("&amp;", "&");
            decoded = decoded.Replace("&lt;", "<");
            decoded = decoded.Replace("&gt;", ">");
            decoded = decoded.Replace("&apos;", "'");
            decoded = decoded.Replace("&quot;", "\"");

            return decoded;
        }

        /// <summary>
        /// Convert a string with a boolean value to its actual boolean value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool TfStringToBool(String value)
        {
            if (String.IsNullOrEmpty(value)) return false;
            return value.ToLower() == "true" ||
                   value.ToLower() == "t" ||
                   value.ToLower() == "yes" ||
                   value.ToLower() == "1" ||
                   value.ToLower() == "y";
        }

        /// <summary>
        /// Translate a csv string to DateTime equivalent
        /// </summary>
        /// <param name="stringDt"></param>
        /// <returns></returns>
        public static DateTime FormatDateTimeFromString(String stringDt)
        {
            char[] splitValues = { ',' };
            char[] trimValues = { ' ' };
            String trimmed = stringDt.Trim(trimValues);
            string[] columns = trimmed.Split(splitValues);
            const string errorText =
                "FormatDateTimeFromString: Malformed string format.  " +
                "Strings should be formatted like: " +
                "now (for DateTime.Now), " +
                "2001,1,1 (for 1/1/2001), " +
                "2001,1,1,12,0,0 (for 1/1/2001 12:00:00), or " +
                "2001,1,1,12,00,00,00 (for 1/1/2001 12:00:00.00)";

            switch (columns.Length)
            {
                case 1:
                    {
                        if (columns[0].ToLower() == "now")
                        {
                            return DateTime.Now;
                        }
                        throw new Exception(errorText);
                    }
                case 3:
                    {
                        return new DateTime(
                            Int32.Parse(columns[0]),
                            Int32.Parse(columns[1]),
                            Int32.Parse(columns[2]));
                    }
                case 6:
                    {
                        return new DateTime(
                            Int32.Parse(columns[0]),
                            Int32.Parse(columns[1]),
                            Int32.Parse(columns[2]),
                            Int32.Parse(columns[3]),
                            Int32.Parse(columns[4]),
                            Int32.Parse(columns[5]));
                    }
                case 7:
                    {
                        return new DateTime(
                            Int32.Parse(columns[0]),
                            Int32.Parse(columns[1]),
                            Int32.Parse(columns[2]),
                            Int32.Parse(columns[3]),
                            Int32.Parse(columns[4]),
                            Int32.Parse(columns[5]),
                            Int32.Parse(columns[6]));
                    }
            }
            throw new Exception(errorText);
        }

        /// <summary>
        /// Convert a string representing hex values to a human readable format
        /// </summary>
        /// <param name="hexString"></param>
        /// <returns></returns>
        public static String HexStringToString(String hexString)
        {
            string input = hexString;
            char[] values = input.ToCharArray();
            String output = "";
            foreach (char letter in values)
            {
                // Get the integral value of the character. 
                int value = Convert.ToInt32(letter);
                // Convert the decimal value to a hexadecimal value in string form. 
                output += String.Format("{0:X}", value);
            }
            return output;
        }

        /// <summary>
        /// Convert a string representing hex values to a human readable format
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static string ByteArrayToString(byte[] bytes)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in bytes)
            {
                sb.Append(b.ToString("X2"));
            }

            return sb.ToString();
        }

        /// <summary>
        /// Takes in a string, converts each element to hex equivalent, then returns the new string with
        /// characters represented by their hex equivalent.
        /// </summary>
        /// <param name="input">Input string</param>
        /// <returns>String representation of the hex values of each character in the string.</returns>
        public static string StringToHexEquivalent(string input)
        {
            var bytes = Encoding.ASCII.GetBytes(input);
            return ByteArrayToString(bytes);
        }

        /// <summary>
        /// Convert a each member of a byte arry to it's character equivalent and return the whole result as a string.
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static String ByteArrayToCharAsString(byte[] bytes)
        {
            return new string(bytes.Select(Convert.ToChar).ToArray());
        }

        /// <summary>
        /// This method will return a single character for boolean value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static String BooleanToTf(Boolean value)
        {
            return value ? "T" : "F";
        }

        /// <summary>
        /// Convert a string representing hex values to a human readable format
        /// </summary>
        /// <param name="hexString"></param>
        /// <param name="useSeparator"></param>
        /// <param name="separator"></param>
        /// <returns></returns>
        public static String HexStringToString(String hexString, bool useSeparator, string separator)
        {
            string input = HexStringToString(hexString);
            string output = "";
            if (useSeparator)
            {
                for (int i = 0; i < input.Length; ++i)
                {
                    output += String.Format("{0:X}", input[i]);
                    if (i % 2 != 0 && i != 0)
                    {
                        output += separator;
                    }
                }
            }
            else
            {
                output = input;
            }
            return output;
        }

        /// <summary>
        /// Format the current time in the format MM/dd/yy hh:mm:ss(.fff) tt.  The millisecond component
        /// is optional based on the value of includeMilliseconds
        /// </summary>
        /// <param name="includeMilliseconds"></param>
        /// <returns></returns>
        public static String FormattedDateTime(bool includeMilliseconds = false)
        {
            if (includeMilliseconds)
            {
                return DateTime.Now.ToString("MM/dd/yy hh:mm:ss.fff tt");
            }
            return DateTime.Now.ToString("MM/dd/yy hh:mm:ss tt");
        }

        /// <summary>
        /// Format the current time in the format MM/dd/yy hh:mm:ss(.fff) tt.  The millisecond component
        /// is optional based on the value of includeMilliseconds
        /// </summary>
        /// <param name="dateTime"></param>
        /// <param name="includeMilliseconds"></param>
        /// <returns></returns>
        public static String FormattedDateTime(DateTime dateTime, bool includeMilliseconds = false)
        {
            if (includeMilliseconds)
            {
                return dateTime.ToString("MM/dd/yy hh:mm:ss.fff tt");
            }
            return dateTime.ToString("MM/dd/yy hh:mm:ss tt");
        }

        /// <summary>
        /// Format the current time in the format hh:mm:ss(.fff) tt.  The millisecond component
        /// is optional based on the value of includeMilliseconds.
        /// </summary>
        /// <param name="dateTime"></param>
        /// <param name="includeMilliseconds"></param>
        /// <returns></returns>
        public static String FormattedTime(DateTime dateTime, bool includeMilliseconds = false)
        {
            if (includeMilliseconds)
            {
                return dateTime.ToString("hh:mm:ss.fff tt");
            }
            return dateTime.ToString("hh:mm:ss tt");
        }

        /// <summary>
        /// Format the current time in the format yyyy/MM/dd HH:mm:ss(.fff) tt  The millisecond component
        /// is optional based on the value of includeMilliseconds.
        /// </summary>
        /// <param name="dateTime"></param>
        /// <param name="includeMilliseconds"></param>
        /// <returns></returns>
        public static string FormatDateTimeOffset(DateTimeOffset? dateTime, bool includeMilliseconds = false)
        {
            if(dateTime == null)
            { return ""; }

            if (includeMilliseconds)
            {
                return dateTime.Value.ToString("yyyy/MM/dd HH:mm:ss.fff zzz");
            }
            return dateTime.Value.ToString("yyyy/MM/dd HH:mm:ss zzz");
        }

        /// <summary>
        /// Convert a input string representing hex values and return as a byte array
        /// </summary>
        /// <param name="hexString"></param>
        /// <returns></returns>
        public static byte[] HexStringToByteArray(String hexString)
        {
            List<byte> convertedString = new List<byte>();

            if (hexString.Length % 2 != 0)
            {
                throw new Exception("Input to hexStringToString must have an even number of characters");
            }
            for (int i = 0; i < hexString.Length; i += 2)
            {
                int lowBits = ConvertHexCharToInt(hexString[i]);
                int hiBits = ConvertHexCharToInt(hexString[i + 1]);

                int finalValue = (lowBits & 0x0000000f) << 4;
                finalValue += (hiBits & 0x0000000f);

                byte[] finalValueAsByteArray = BitConverter.GetBytes(finalValue);
                convertedString.Add(finalValueAsByteArray[0]);
            }

            return convertedString.ToArray();
        }

        /// <summary>
        /// convert a single hex character to it's integer equivalent
        /// </summary>
        /// <param name="hexChar"></param>
        /// <returns></returns>
        public static int ConvertHexCharToInt(char hexChar)
        {
            string hexCharString = new string(hexChar, 1).ToUpper();
            int returnInt;

            if (Int32.TryParse(hexCharString, NumberStyles.HexNumber, CultureInfo.InvariantCulture, out returnInt))
            {
                return returnInt;
            }

            return -1;
        }

        /// <summary>
        /// Parse an input for a key value pair separated by a delimiter
        /// </summary>
        /// <param name="field">String to parse for a key value pair</param>
        /// <param name="separator">character separating key and value</param>
        /// <param name="trimWhiteSpace">remove white space before and after including '"'</param>
        /// <returns>Parsed KeyValuePair</returns>
        public static KeyValuePair<string, string> ParseNameValuePair(String field, char separator, bool trimWhiteSpace)
        {
            string[] nvPair = field.Split(separator);
            if (nvPair.Length == 2)
            {
                string key = nvPair[0];
                string value = nvPair[1];

                if (trimWhiteSpace)
                {
                    key = key.TrimStart(new char[] { ' ', '\"' });
                    key = key.TrimEnd(new char[] { ' ', '\"' });
                    value = value.TrimStart(new char[] { ' ', '\"' });
                    value = value.TrimEnd(new char[] { ' ', '\"' });
                }
                return new KeyValuePair<string, string>(key, value);
            }
            throw new ArgumentException("Invalid format for input string.  Expecting two values but found " + nvPair.Length.ToString());
        }

        /// <summary>
        /// Generate a "unique" name in the format of basenameYYYYMMDDHHmmssmmm.
        /// Basename will be trimmed if base + date > maxCharacters.
        /// </summary>
        /// <param name="baseName">base name to use</param>
        /// <param name="maxCharacters">max characters of the </param>
        /// <returns></returns>
        public static string GenerateUniqueName(string baseName, int maxCharacters)
        {
            DateTime now = DateTime.Now;
            //string dateAsString = String.Format("{0}{1}{2}{3}{4}{5}{6}", now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second, now.Millisecond);
            string dateAsString = now.ToString("yyyyMMddhhmmssfff");
            int charactersToTrim = maxCharacters - (baseName.Length + dateAsString.Length);

            if (maxCharacters <= dateAsString.Length) return dateAsString;

            if (charactersToTrim < 0)
            {
                baseName = baseName.Substring(0, baseName.Length - (Math.Abs(charactersToTrim)));
            }
            return String.Format("{0}{1}", baseName, dateAsString);
        }

        /// <summary>
        /// Prepend the card number with the pad character up to the maximum digits provided.  If the card number
        /// is greater than the maximum digits then the original card number will be returned.
        /// </summary>
        /// <param name="cardNumber">base card number</param>
        /// <param name="maxDigits">maximum digits</param>
        /// <param name="padChar">character to prepend</param>
        /// <returns></returns>
        public static string PadCardNumber(string cardNumber, int maxDigits, char padChar)
        {
            if (cardNumber == null)
            {
                cardNumber = "";
            }
            if (maxDigits <= cardNumber.Length) return cardNumber;
            return cardNumber.PadLeft(maxDigits, padChar);
        }

        /// <summary>
        /// Convert a stream to byte array.
        /// </summary>
        /// <param name="input">stream</param>
        /// <returns></returns>
        public static byte[] StreamToBytes(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        /// <summary>
        /// This method will check the passed id value and if a guid returns the guid value, otherwise the integer value
        /// </summary>
        /// <param name="id">The value to be checked</param>
        /// <param name="integerColumnName">The integer column name</param>
        /// <param name="guidColumnName">The guid column name</param>
        /// <returns>One of the two column names</returns>
        public static string ColumnNameFromIdType(string id, string integerColumnName, string guidColumnName)
        {
            Guid guid;
            return Guid.TryParse(id, out guid) ? guidColumnName : integerColumnName;
        }

        /// <summary>
        /// This method will return NULL or the value appropriately data typed from the database
        /// </summary>
        /// <typeparam name="T">The object Type</typeparam>
        /// <param name="obj">The object itself</param>
        /// <returns></returns>
        public static T? ExtractValueFromColumn<T>(Object obj) where T : struct
        {
            if (String.IsNullOrEmpty(obj.ToString()))
                return null;

            return (T)Convert.ChangeType(obj, typeof(T));
        }

        /// <summary>
        /// Checks that the provided object is a primitave and converts it to one of the following types, then calls ToString().
        /// 
        /// Boolean, Byte, SByte, Int16, UInt16, Int32, UInt32, Int64, UInt64, IntPtr, UIntPtr, Char, Double, and Single
        /// </summary>
        /// <param name="primitave"></param>
        /// <returns></returns>
        public static string FormatPrimitaveToString(object primitave)
        {
            var type = primitave.GetType();

            if (type == typeof(bool)) return ((bool)primitave).ToString(CultureInfo.InvariantCulture);
            if (type == typeof(int)) return ((int)primitave).ToString(CultureInfo.InvariantCulture);
            if (type == typeof(float)) return ((float)primitave).ToString(CultureInfo.InvariantCulture);
            if (type == typeof(double)) return ((double)primitave).ToString(CultureInfo.InvariantCulture);
            if (type == typeof(char)) return ((char)primitave).ToString(CultureInfo.InvariantCulture);
            if (type == typeof(byte)) return ((byte)primitave).ToString(CultureInfo.InvariantCulture);
            if (type == typeof(sbyte)) return ((sbyte)primitave).ToString(CultureInfo.InvariantCulture);
            if (type == typeof(uint)) return ((uint)primitave).ToString(CultureInfo.InvariantCulture);
            if (type == typeof(short)) return ((short)primitave).ToString(CultureInfo.InvariantCulture);
            if (type == typeof(long)) return ((long)primitave).ToString(CultureInfo.InvariantCulture);
            if (type == typeof(ulong)) return ((ulong)primitave).ToString(CultureInfo.InvariantCulture);
            if (type == typeof(ushort)) return ((ushort)primitave).ToString(CultureInfo.InvariantCulture);
            if (type == typeof(IntPtr)) return ((IntPtr)primitave).ToString();
            if (type == typeof(UIntPtr)) return ((UIntPtr)primitave).ToString();
            if (type == typeof(string)) return (string)primitave;
            if (type == typeof(DateTime)) return ((DateTime)primitave).ToString(CultureInfo.InvariantCulture);

            return null;
        }

        /// <summary>
        /// Test whether or not an object is a primitave type defined as one of the following:
        /// 
        /// String, Boolean, Byte, SByte, Int16, UInt16, Int32, UInt32, Int64, UInt64, IntPtr, UIntPtr, Char, Double, and Single.
        /// </summary>
        /// <param name="value">Value to test</param>
        /// <returns>Indicates if the value is a primitave.</returns>
        public static bool IsPrimitave(object value)
        {
            var type = value.GetType();
            return
                type.IsPrimitive ||
                type == typeof (string) ||
                type == typeof (DateTime);
        }

        /// <summary>
        /// Tests whether or not a given object would translate to a null return for <see cref="ExtractObject{T}"/>
        /// </summary>
        /// <param name="obj">Object to test.</param>
        /// <returns>Result of test.</returns>
        public static bool IsNullReturnForExtractObject(object obj)
        {
            var type = obj.GetType();
            var isEnumList = false;
            var isPrimitiveList = false;
            var interfaceTest = new Predicate<Type>(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IList<>));
            if (interfaceTest(type) || type.GetInterfaces().Any(i => interfaceTest(i)))
            {
                var list = obj as IList;
                isEnumList = list?.Count > 0 && list[0].GetType().IsEnum;
                isPrimitiveList = list?.Count > 0 && IsPrimitave(list[0]);
            }

            if (type == typeof (HttpResponseMessage))
            {
                return true;
            }

            return
                type.IsPrimitive ||
                type == typeof (string) ||
                type == typeof (DateTime) ||
                isEnumList ||
                isPrimitiveList;
        }

        /// <summary>
        /// Extract an object of Type T.  Recurse through all the properties of the obj to find a T object among its children.
        /// </summary>
        /// <param name="obj">Base object</param>
        /// <returns>object of type T or null if not found.</returns>
        public static object ExtractObject<T>(object obj)
        {
            if (obj != null)
            {
                var type = obj.GetType();

                if (type == typeof(T))
                {
                    return obj;
                }

                if (IsNullReturnForExtractObject(obj))
                {
                    return null;
                }

                var enumerable = obj as IEnumerable<object>;
                if (enumerable != null)
                {
                    var objAsList = enumerable.ToList();
                    foreach (var objItem in objAsList)
                    {
                        var list = objItem.GetType().GetProperties().ToList();
                        foreach (var item in list)
                        {
                            var itemValue = item.GetValue(objItem, null);
                            var child = ExtractObject<T>(itemValue);
                            if (child != null && child.GetType() == typeof(T))
                            {
                                return child;
                            }
                        }
                    }
                }
                else
                {
                    var list = obj.GetType().GetProperties().ToList();

                    foreach (var item in list)
                    {
                        var itemValue = item.GetValue(obj, null);
                        var child = ExtractObject<T>(itemValue);
                        if (child != null && child.GetType() == typeof(T))
                        {
                            return child;
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Get normalized value for type
        /// </summary>
        /// <param name="type">Defined field type</param>
        /// <param name="value">Value to be normalized</param>
        /// <returns>Normalized value</returns>
        public static string DefinedFieldToNormalizedValue(DefinedFieldType type, string value)
        {
            switch (type)
            {
                case DefinedFieldType.Boolean:
                    return TfStringToBool(value).ToString().ToLower();
                case DefinedFieldType.Dollar:
                    return string.IsNullOrEmpty(value) ? "$.00" : string.Format(CultureInfo.InvariantCulture, "${0:#.00}", Convert.ToDecimal(value, CultureInfo.InvariantCulture) / 1000m);
                case DefinedFieldType.String:
                case DefinedFieldType.Text:
                    return string.IsNullOrEmpty(value) ? null : value;
                case DefinedFieldType.Numeric:
                    return string.IsNullOrEmpty(value) ? "0" : value;
                default:
                    return value;
            }
        }

        /// <summary>
        /// Get normalized value for type
        /// </summary>
        /// <param name="type">Defined field type</param>
        /// <param name="value">Value to be normalized</param>
        /// <returns>Normalized value</returns>
        public static string DefinedFieldFromNormalizedValue(DefinedFieldType type, string value)
        {
            switch (type)
            {
                case DefinedFieldType.Boolean:
                    return BooleanToTf(TfStringToBool(value));
                case DefinedFieldType.Dollar:
                    if (string.IsNullOrEmpty(value))
                        return null;
                    return Convert.ToInt64(Convert.ToDecimal(value.TrimStart('$'), CultureInfo.InvariantCulture) * 1000m).ToString(CultureInfo.InvariantCulture);;
                default:
                    return value;
            }
        }

        /// <summary>
        /// Formats time zone offset
        /// </summary>
        /// <param name="offset">OFfset to format</param>
        /// <returns>Formatted offsed</returns>
        public static string FormatTimeZoneOffset(TimeSpan offset)
        {
            return $"{(offset > TimeSpan.Zero ? "" : "-")}{offset:hh\\:mm}";
        }

        /// <summary>
        /// Converts ResetFrequency string to enum
        /// </summary>
        /// <param name="frequency">ResetFrequency in string</param>
        /// <returns>ResetFrequency as enum</returns>
        public static ResetFrequency ResetFrequencyStrToEnum(string frequency)
        {
            switch (frequency)
            {
                case "D":
                    return ResetFrequency.Day;
                case "W":
                    return ResetFrequency.Week;
                case "M":
                    return ResetFrequency.Month;
                case "S":
                    return ResetFrequency.Semester;
                case "Y":
                    return ResetFrequency.Year;
                default:
                    return ResetFrequency.Undefined;
            }
        }
        
        /// <summary>
        /// Merge properties from one object to another but only for properties that are not null.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="destination"></param>
        /// <returns></returns>
        public static T Merge<T>(T source, T destination)
        {
            var returnvalue = (T)Activator.CreateInstance(typeof(T));
            foreach (var field in destination.GetType().GetProperties())
            {
                field.SetValue(returnvalue,
                    field.GetValue(destination, null) == null ? field.GetValue(source) : field.GetValue(destination));
            }
            return returnvalue;
        }

        /// <summary>
        /// Gets enum representation of limit by
        /// </summary>
        /// <param name="limitBy"></param>
        /// <returns>Enum representation of limit by</returns>
        public static EventLimitType EventLimitTypeStrToEnum(string limitBy)
        {
            switch (limitBy)
            {
                case "Y":
                    return EventLimitType.Year;
                case "S":
                    return EventLimitType.Semester;
                case "M":
                    return EventLimitType.Month;
                case "W":
                    return EventLimitType.Week;
                case "D":
                    return EventLimitType.Day;
                default:
                    return EventLimitType.None;
            }
        }

        /// <summary>
        /// Gets string representation of limit by
        /// </summary>
        /// <param name="type"></param>
        /// <returns>String representation of limit by</returns>
        public static string EventLimitTypeEnumToStr(EventLimitType type)
        {
            return type == EventLimitType.None ? null : type.ToString().Substring(0, 1).ToUpper();
        }

        /// <summary>
        /// Create a delimited string from a list of strings
        /// </summary>
        /// <param name="list"></param>
        /// <param name="delimiter">the delimiter to use.  "," used by default.</param>
        /// <returns></returns>
        public static string ToDelimitedList(List<string> list, string delimiter = ",")
        {
            return string.Join(delimiter, list);
        }

        /// <summary>
        /// Split a list into smaller lists
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public static List<List<T>> SplitList<T>(List<T> collection, int size)
        {
            var chunks = new List<List<T>>();
            var chunkCount = collection.Count / size;

            if (collection.Count % size > 0)
                chunkCount++;

            for (var i = 0; i < chunkCount; i++)
                chunks.Add(collection.Skip(i * size).Take(size).ToList());

            return chunks;
        }
    }
}
