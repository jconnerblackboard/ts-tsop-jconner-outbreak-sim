﻿using System;

namespace BbTS.Core.Conversion
{
    public class DateTimeUtility
    {
        /// <summary>
        /// From null dates return dates in range of 31 days
        /// </summary>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <returns>Normalized dates</returns>
        public static Tuple<DateTime, DateTime> NormalizeDates(DateTime? startDate, DateTime? endDate)
        {
            if (!startDate.HasValue && !endDate.HasValue)
            {
                var date = DateTime.Today.AddDays(1);
                return Tuple.Create(date.AddDays(-31), date);
            }

            return !startDate.HasValue ? Tuple.Create(endDate.Value.AddDays(-31), endDate.Value) : Tuple.Create(startDate.Value, endDate ?? startDate.Value.AddDays(31));
        }
    }
}
