﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Core.Conversion
{
    public static class SystemFeaturesTool
    {
        /// <summary>
        /// Un transpose characters in a transpose string. 
        /// This logic has been copied from BbTS.Licensing project's License.cs file provided by the TS team
        /// </summary>
        /// <param name="temp">The string where non transpose is performed.</param>
        /// <returns>Return [String] - string with non transpose characters.</returns>
        public static string UnTranspose(string temp)
        {
            if (temp.Length > 1)
            {
                for (int i = 0; i < temp.Length; i++)
                {
                    int j = 0;
                    int k = 1;
                    do
                    {
                        j += 1;
                        k += 3;
                    }
                    while (k < temp.Length);

                    j -= 1;
                    k -= 3;
                    while (k > 2)
                    {
                        temp = temp.Substring(1, temp.Length - 1) + temp.Substring(0, 1);
                        temp = SwapCharacters(temp, j, k);
                        j -= 1;
                        k -= 3;
                    }

                    temp = temp.Substring(1, temp.Length - 1) + temp.Substring(0, 1);
                    temp = SwapCharacters(temp, j, k);
                }
            }

            return temp;
        }

        /// <summary>
        /// Switch characters in a string at the specified indexes. 
        /// This logic has been copied from BbTS.Licensing project's License.cs file provided by the TS team
        /// </summary>
        /// <param name="value">The string where replacement is performed.</param>
        /// <param name="position1">The index of the first character.</param>
        /// <param name="position2">The index of the second character.</param>
        /// <returns>Return [String] - string with replaced characters.</returns>
        private static string SwapCharacters(string value, int position1, int position2)
        {
            char[] array = value.ToCharArray();
            char temp = array[position1];
            array[position1] = array[position2];
            array[position2] = temp;
            return new string(array);
        }
    }
}
