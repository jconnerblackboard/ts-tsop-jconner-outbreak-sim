﻿using System.Collections.Generic;
using System.Linq;
using BbTS.Domain.Models.Definitions.System;

namespace BbTS.Core.Conversion
{
    public static class DataValidation
    {
        /// <summary>
        /// Validate that a base value fits a comparison value based on the provided modifier.
        /// </summary>
        /// <param name="value">Base value</param>
        /// <param name="comparer">Comparison value</param>
        /// <param name="modifier">Operational modifier</param>
        /// <param name="requestId">Unique identifier for the request</param>
        public static bool ValidateValue<T>(T value, T comparer, ValidateValueOperator modifier, string requestId = null)
        {
            if (value is int && comparer is int)
            {
                var val = value as int?;
                var comparerValue = comparer as int?;
                return ValidateValueInt(val.Value, comparerValue.Value, modifier);
            }

            if (value is long && comparer is long)
            {
                var val = value as long?;
                var comparerValue = comparer as long?;
                return ValidateValueLong(val.Value, comparerValue.Value, modifier);
            }

            if (value is short && comparer is short)
            {
                var val = value as short?;
                var comparerValue = comparer as short?;
                return ValidateValueShort(val.Value, comparerValue.Value, modifier);
            }

            if (value is float && comparer is float)
            {
                var val = value as float?;
                var comparerValue = comparer as float?;
                return ValidateValueFloat(val.Value, comparerValue.Value, modifier);
            }

            if (value is double && comparer is double)
            {
                var val = value as double?;
                var comparerValue = comparer as double?;
                return ValidateValueDouble(val.Value, comparerValue.Value, modifier);
            }

            if (value is decimal && comparer is decimal)
            {
                var val = value as decimal?;
                var comparerValue = comparer as decimal?;
                return ValidateValueDecimal(val.Value, comparerValue.Value, modifier);
            }

            return false;
        }

        public static bool DictionaryEquals<TKey, TValue>(this Dictionary<TKey, TValue> dictionary,
            Dictionary<TKey, TValue> otherDictionary)
        {
            return (otherDictionary ?? new Dictionary<TKey, TValue>())
                .OrderBy(kvp => kvp.Key)
                .SequenceEqual((dictionary ?? new Dictionary<TKey, TValue>())
                    .OrderBy(kvp => kvp.Key));
        }

        #region Private functions

        /// <summary>
        /// Validate that a base value fits a comparison value based on the provided modifier.
        /// </summary>
        /// <param name="value">Base value</param>
        /// <param name="comparer">Comparison value</param>
        /// <param name="modifier">Operational modifier</param>
        private static bool ValidateValueInt(int value, int comparer, ValidateValueOperator modifier)
        {
            switch (modifier)
            {
                case ValidateValueOperator.NotEqual: return value != comparer;
                case ValidateValueOperator.Equal: return value == comparer;
                case ValidateValueOperator.GreaterThan: return value > comparer;
                case ValidateValueOperator.GreatherThanOrEqual: return value >= comparer;
                case ValidateValueOperator.LessThan: return value < comparer;
                case ValidateValueOperator.LessThanOrEqual: return value <= comparer;
            }
            return false;
        }

        /// <summary>
        /// Validate that a base value fits a comparison value based on the provided modifier.
        /// </summary>
        /// <param name="value">Base value</param>
        /// <param name="comparer">Comparison value</param>
        /// <param name="modifier">Operational modifier</param>
        private static bool ValidateValueDouble(double value, double comparer, ValidateValueOperator modifier)
        {
            switch (modifier)
            {
                case ValidateValueOperator.NotEqual: return value != comparer;
                case ValidateValueOperator.Equal: return value == comparer;
                case ValidateValueOperator.GreaterThan: return value > comparer;
                case ValidateValueOperator.GreatherThanOrEqual: return value >= comparer;
                case ValidateValueOperator.LessThan: return value < comparer;
                case ValidateValueOperator.LessThanOrEqual: return value <= comparer;
            }
            return false;
        }

        /// <summary>
        /// Validate that a base value fits a comparison value based on the provided modifier.
        /// </summary>
        /// <param name="value">Base value</param>
        /// <param name="comparer">Comparison value</param>
        /// <param name="modifier">Operational modifier</param>
        private static bool ValidateValueFloat(float value, float comparer, ValidateValueOperator modifier)
        {
            switch (modifier)
            {
                case ValidateValueOperator.NotEqual: return value != comparer;
                case ValidateValueOperator.Equal: return value == comparer;
                case ValidateValueOperator.GreaterThan: return value > comparer;
                case ValidateValueOperator.GreatherThanOrEqual: return value >= comparer;
                case ValidateValueOperator.LessThan: return value < comparer;
                case ValidateValueOperator.LessThanOrEqual: return value <= comparer;
            }
            return false;
        }

        /// <summary>
        /// Validate that a base value fits a comparison value based on the provided modifier.
        /// </summary>
        /// <param name="value">Base value</param>
        /// <param name="comparer">Comparison value</param>
        /// <param name="modifier">Operational modifier</param>
        private static bool ValidateValueDecimal(decimal value, decimal comparer, ValidateValueOperator modifier)
        {
            switch (modifier)
            {
                case ValidateValueOperator.NotEqual: return value != comparer;
                case ValidateValueOperator.Equal: return value == comparer;
                case ValidateValueOperator.GreaterThan: return value > comparer;
                case ValidateValueOperator.GreatherThanOrEqual: return value >= comparer;
                case ValidateValueOperator.LessThan: return value < comparer;
                case ValidateValueOperator.LessThanOrEqual: return value <= comparer;
            }
            return false;
        }

        /// <summary>
        /// Validate that a base value fits a comparison value based on the provided modifier.
        /// </summary>
        /// <param name="value">Base value</param>
        /// <param name="comparer">Comparison value</param>
        /// <param name="modifier">Operational modifier</param>
        private static bool ValidateValueLong(long value, long comparer, ValidateValueOperator modifier)
        {
            switch (modifier)
            {
                case ValidateValueOperator.NotEqual: return value != comparer;
                case ValidateValueOperator.Equal: return value == comparer;
                case ValidateValueOperator.GreaterThan: return value > comparer;
                case ValidateValueOperator.GreatherThanOrEqual: return value >= comparer;
                case ValidateValueOperator.LessThan: return value < comparer;
                case ValidateValueOperator.LessThanOrEqual: return value <= comparer;
            }
            return false;
        }

        /// <summary>
        /// Validate that a base value fits a comparison value based on the provided modifier.
        /// </summary>
        /// <param name="value">Base value</param>
        /// <param name="comparer">Comparison value</param>
        /// <param name="modifier">Operational modifier</param>
        private static bool ValidateValueShort(short value, short comparer, ValidateValueOperator modifier)
        {
            switch (modifier)
            {
                case ValidateValueOperator.NotEqual: return value != comparer;
                case ValidateValueOperator.Equal: return value == comparer;
                case ValidateValueOperator.GreaterThan: return value > comparer;
                case ValidateValueOperator.GreatherThanOrEqual: return value >= comparer;
                case ValidateValueOperator.LessThan: return value < comparer;
                case ValidateValueOperator.LessThanOrEqual: return value <= comparer;
            }

            return false;
        }

#endregion
    }
}
