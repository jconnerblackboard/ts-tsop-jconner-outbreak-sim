﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Core.Conversion
{
    /// <summary>
    ///     Custom extension methods for Enumerable types
    /// </summary>
    public static class EnumerableExtensions
    {
        /// <summary>
        ///     Custom implementation of a ForEach to support IEnumerable
        /// </summary>
        /// <typeparam name="T">
        ///     The type of list items
        /// </typeparam>
        /// <param name="list">
        ///     The list.
        /// </param>
        /// <param name="action">
        ///     The action.
        /// </param>
        public static void ForEach<T>(this IEnumerable<T> list, Action<T> action)
        {
            if (list == null || action == null)
            {
                return;
            }

            foreach (var item in list)
            {
                action(item);
            }
        }
    }
}
