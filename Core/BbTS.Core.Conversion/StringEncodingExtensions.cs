﻿using System;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace BbTS.Core.Conversion
{
    /// <summary>
    ///     Extension methods that perform various encoding and decoding operations
    /// </summary>
    public static class StringEncodingExtensions
    {
        /// <summary>
        ///     The unreserved character set used for percent encoding
        /// </summary>
        private const string UnreservedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.~";

        /// <summary>
        ///     HTML encodes the input.
        /// </summary>
        /// <param name="input">
        ///     The input.
        /// </param>
        /// <returns>
        ///     Encoded string
        /// </returns>
        public static string HtmlEncode(this string input)
        {
            return string.IsNullOrWhiteSpace(input) ? string.Empty : WebUtility.HtmlEncode(input);
        }

        /// <summary>
        ///     HTML decodes the input
        /// </summary>
        /// <param name="input">
        ///     The input.
        /// </param>
        /// <returns>
        ///     Decoded string
        /// </returns>
        public static string HtmlDecode(this string input)
        {
            return string.IsNullOrWhiteSpace(input) ? string.Empty : WebUtility.HtmlDecode(input);
        }

        /// <summary>
        ///     Encodes the value using Percent Encoding algorithm.
        /// </summary>
        /// <remarks>
        ///     This is defined in RFC3986 sec. 2.1
        /// </remarks>
        /// <param name="value">
        ///     The value.
        /// </param>
        /// <returns>
        ///     Encoded value
        /// </returns>
        public static string PercentEncode(this string value)
        {
            if (value == null)
            {
                return null;
            }

            var result = new StringBuilder();
            foreach (var symbol in value)
            {
                if (UnreservedChars.IndexOf(symbol) != -1)
                {
                    result.Append(symbol);
                }
                else
                {
                    result.Append('%' + string.Format(CultureInfo.InvariantCulture, "{0:X2}", (int)symbol));
                }
            }

            return result.ToString();
        }

        /// <summary>
        /// Decode the string value.
        /// </summary>
        /// <param name="value">The encoded value.</param>
        /// <returns>Decoded string</returns>
        public static string PercentDecode(this string value)
        {
            if (value == null)
            {
                return null;
            }

            var result = new StringBuilder();
            for (int i = 0; i < value.Length; i++)
            {
                var symbol = value[i];
                if (UnreservedChars.IndexOf(symbol) != -1)
                {
                    result.Append(symbol);
                }
                else if (symbol.Equals('%'))
                {
                    int encodedValue;
                    var encodedString = string.Format(CultureInfo.InvariantCulture, "{0}{1}", value[++i], value[++i]);
                    if (int.TryParse(encodedString, NumberStyles.HexNumber, CultureInfo.InvariantCulture, out encodedValue))
                    {
                        result.Append((char)encodedValue);
                    }
                }
                else
                {
                    throw new ArgumentOutOfRangeException("value", string.Format(CultureInfo.InvariantCulture, "Unexpected character {0} in value {1}. Unable to decode.", symbol, value));
                }
            }

            return result.ToString();
        }

        /// <summary>
        /// Encodes a Base64 string.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Base64 Encode String</returns>
        public static string Base64Encode(this string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return value;
            }

            var bytes = new byte[value.Length * sizeof(char)];
            Buffer.BlockCopy(value.ToCharArray(), 0, bytes, 0, bytes.Length);
            return Convert.ToBase64String(bytes);
        }

        /// <summary>
        /// Decodes a Base64 string.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>String that has been Base64 Decoded</returns>
        public static string Base64Decode(this string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return value;
            }

            var bytes = Convert.FromBase64String(value);
            var chars = new char[bytes.Length / sizeof(char)];
            Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }

        /// <summary>
        /// Determines whether the specified value can be safely base 64 encoded
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>True if specified string can be </returns>
        public static bool IsBase64Encoded(this string value)
        {
            const string Base64EncodedStringPattern = @"^[a-zA-Z0-9\+/]*={0,3}$";
            if (string.IsNullOrWhiteSpace(value))
            {
                return false;
            }

            value = value.Trim();
            return (value.Length % 4 == 0) && Regex.IsMatch(value, Base64EncodedStringPattern, RegexOptions.None);
        }

        /// <summary>
        /// Removes characters with code between 0 and 31.
        /// These are control characters which cause export to XML to fail. XML does not support those characters and it has no escape sequence for those either.
        /// </summary>
        /// <param name="input">The string to be converted</param>
        /// <returns>The input with any characters with code less than 32 removed</returns>
        public static string RemoveInvalidCharacters(this string input)
        {
            return new string(input.Where(c => c >= 32).ToArray());
        }
    }
}
