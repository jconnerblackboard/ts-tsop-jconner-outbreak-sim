﻿using System;

namespace BbTS.Core.Conversion
{
    /// <summary>
    /// Utility class for dealing with a variety of URI and HTTP formatting tasks.
    /// </summary>
    public static class UriUtilityTool
    {
        /// <summary>
        /// Extract everything to the right of the URI authority.
        /// Example: https://mydomain.com/api/mycontroller/myaction
        /// Return  api/mycontroller/myaction
        /// </summary>
        /// <param name="baseString">Relative or absolute URI path.  Example: https://mydomain.com/api/mycontroller/myaction </param>
        /// <param name="rightOf">All data to the right of the authority.</param>
        /// <param name="message">Message with the results of the operation. 'Success' or the reason for failure.</param>
        /// <returns>Success or failure of the operation.</returns>
        public static bool TryExtractRightOfAuthority(string baseString, out string rightOf, out string message)
        {
            rightOf = string.Empty;
            string authority;

            if (!Uri.IsWellFormedUriString(baseString, UriKind.RelativeOrAbsolute) || 
                !TryExtractSchemeAndAuthority(baseString, out authority, out message))
            {
                message = $"Base string '{baseString}' is neither a properly formed relative nor absolute URI.";
                return false;
            }

            int authroitySize = authority.Length;

            rightOf = baseString.Substring(authroitySize);
            rightOf = rightOf.Replace("//", "/");
            if (rightOf.StartsWith("/"))
            {
                rightOf = rightOf.Substring(1, rightOf.Length - 1);
            }

            message = "Success.";
            return true;
        }

        /// <summary>
        /// Extract the Authority from a well formed relative or absolute URI path.
        /// Example: https://mydomain.com/api/mycontroller/myaction
        /// Return  https://mydomain.com
        /// </summary>
        /// <param name="baseString">Relative or absolute URI path.  Example: https://mydomain.com/api/mycontroller/myaction </param>
        /// <param name="authority">The authority of the URI path.</param>
        /// <param name="message">Message with the results of the operation. 'Success' or the reason for failure.</param>
        /// <returns>Success or failure of the operation.</returns>
        public static bool TryExtractSchemeAndAuthority(string baseString, out string authority, out string message)
        {
            authority = string.Empty;

            if (!Uri.IsWellFormedUriString(baseString, UriKind.RelativeOrAbsolute))
            {
                message = $"Base string '{baseString}' is neither a properly formed relative nor absolute URI.";
                return false;
            }
            var uri = new Uri(baseString);
            authority = $"{uri.Scheme}://{uri.Authority}";
            message = "Success.";
            return true;
        }

        /// <summary>
        /// Format the base URI and route so that there is exactly 1 "/" between them.
        /// </summary>
        /// <param name="baseUri">Base portion of the URI</param>
        /// <param name="route">Route portion of the URI</param>
        /// <returns>Formatted URI as a string</returns>
        public static string FormatBaseAndRouteIntoUri(string baseUri, string route)
        {
            var newBase = baseUri.EndsWith("/") ? baseUri : baseUri + "/";
            var newRoute = route.StartsWith("/")
                ? route.Substring(1, route.Length - 1)
                : route;
            return $"{newBase}{newRoute}";
        }
    }
}
