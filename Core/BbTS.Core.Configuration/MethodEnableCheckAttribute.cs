﻿using System.Diagnostics;
using System.Reflection;
using PostSharp.Aspects;
using System;

namespace BbTS.Core.Configuration
{
    [Serializable]
    public class MethodEnableCheckAttribute : OnMethodBoundaryAspect
    {
        private string _methodName;

        /// <summary> 
        /// Method executed at build time. Initializes the aspect instance. After the execution 
        /// of <see cref="CompileTimeInitialize"/>, the aspect is serialized as a managed  
        /// resource inside the transformed assembly, and deserialized at runtime. 
        /// </summary> 
        /// <param name="method">Method to which the current aspect instance  
        /// has been applied.</param> 
        /// <param name="aspectInfo">Unused.</param> 
        public override void CompileTimeInitialize(MethodBase method, AspectInfo aspectInfo)
        {
            if (method.DeclaringType == null) return;
            _methodName = method.Name;
        }

        public override void OnEntry(MethodExecutionArgs args)
        {
            if (!ApplicationConfiguration.GetKeyValueAsBoolean(_methodName))
            {
                String message = String.Format("Method skipped per app.config parameter {0}.  Set parameter to 't' to enable.", _methodName);
                Trace.WriteLine(message);
                args.FlowBehavior = FlowBehavior.Return;
            }
        }

        public override void OnExit(MethodExecutionArgs args)
        {
        }
    }
}
