﻿using System;
using System.ComponentModel;
using System.Net;

namespace BbTS.Core.Configuration
{
    /// <summary>
    /// File loader class that has methods to get a file from a URI and save it to the destination.
    /// </summary>
    public class FileLoader
    {
        /// <summary>
        /// Get the file specified by the URI and save it to the destination (synchronously).
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="destinationFile"></param>
        public void FileGet(Uri uri, string destinationFile)
        {
            using (var wc = new WebClient())
            {
                wc.DownloadFile(uri, destinationFile);
            }
        }

        /// <summary>
        /// Get the file specified by the URI and save it to the destination (asynchronously).
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="destinationFile"></param>
        /// <param name="downloadProgressChanged"></param>
        /// <param name="downloadFileCompleted"></param>
        public void FileGetAsync(Uri uri, string destinationFile, DownloadProgressChangedEventHandler downloadProgressChanged, AsyncCompletedEventHandler downloadFileCompleted)
        {
            using (var wc = new WebClient())
            {
                wc.DownloadProgressChanged += downloadProgressChanged;
                wc.DownloadFileCompleted += downloadFileCompleted;
                wc.DownloadFileAsync(uri, destinationFile);
            }
        }
    }
}
