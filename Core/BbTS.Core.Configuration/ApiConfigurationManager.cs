﻿using System.IO;
using BbTS.Domain.Models.Configuration;
using BbTS.Domain.Models.Exceptions.Configuration;

namespace BbTS.Core.Configuration
{
    /// <summary>
    /// Configuration manager for an WebAPI class.
    /// </summary>
    public class ApiConfigurationManager
    {
        /// <summary>
        /// Singleton instance.
        /// </summary>
        public static ApiConfigurationManager Instance { get; set; } = new ApiConfigurationManager();

        /// <summary>
        /// Custom configuration model object.
        /// </summary>
        public CustomConfigurationModel Configuration { get; set; }

        /// <summary>
        /// Must use the singleton instance.
        /// </summary>
        private ApiConfigurationManager()
        {
        }

        /// <summary>
        /// Load a custom configuration from the provided path.
        /// </summary>
        /// <param name="path">Path to the configuration file.</param>
        /// <param name="message">Result message</param>
        /// <returns><see cref="bool"/> success of the operation</returns>
        public bool LoadConfiguration(string path, out string message)
        {
            System.Configuration.Configuration configuration = null;
            if (!File.Exists(path) || !CustomConfiguration.LoadCustomConfiguration(path, out configuration,
                out message))
            {

            }

            if (configuration == null)
            {
                message = $"Failed to load the custom configuration.";
                return false;
            }

            message = "Success";
            Configuration = CustomConfigurationModel.GenerateCustomConfigurationModel(configuration);
            return true;
        }

        /// <summary>
        /// Get a configuration item from the loaded custom configuration set.
        /// </summary>
        /// <param name="key">Key to query against.</param>
        /// <param name="defaultValue">Default value to fall back to if key is not found</param>
        /// <returns>Value corresponding to the key.</returns>
        public string GetConfigurationItem(string key, string defaultValue = null)
        {
            if (Configuration == null)
            {
                throw new CustomConfigurationException("ApiConfigurationManager failed to retrieve the requested parameter because the CustomConfigurationModel object was not initialize.  Please load the custom configuration file before use.");
            }
            return CustomConfiguration.GetKeyValueAsString(key, Configuration, defaultValue);
        }
    }
}
