﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using BbTS.Core.Conversion;

namespace BbTS.Core.Configuration
{
    public class FileConfigurationManager
    {
        /// <summary>
        /// Verify a file path and directory structure is valid.
        /// </summary>
        /// <param name="path">Path to the file</param>
        /// <param name="extension">extension of the file</param>
        /// <param name="createDirectory">Create the directory structure if it doesn't exist.</param>
        /// <returns></returns>
        public static string VerifyFilePathAndDirectory(string path, string extension, bool createDirectory = true)
        {
            string filename = !String.IsNullOrEmpty(path) ?
                String.Format("{0}", path) :
                String.Format("{0}", Guid.NewGuid().ToString("N"));

            if (!extension.StartsWith(".")) extension = String.Format(".{0}", extension);

            if (File.Exists(String.Format("{0}{1}", filename, extension)))
            {
                filename = String.Format("{0}{1}", path, Guid.NewGuid().ToString("N"));
                if (File.Exists(String.Format("{0}{1}", filename, extension)))
                    throw new ArgumentException(String.Format("Unable to create file {0}{1}.", path, extension));
            }
            filename = String.Format("{0}{1}", path, extension);

            string directory = Path.GetDirectoryName(filename);

            if (!String.IsNullOrEmpty(directory) && !Directory.Exists(directory)) Directory.CreateDirectory(directory);

            return filename;
        }

        /// <summary>
        /// Verify and create the directory specified by {path}
        /// </summary>
        /// <param name="directory">full path to the directory to create</param>
        /// <param name="message">result message</param>
        /// <returns>Success or Failure</returns>
        public static bool VerifyAndCreateDirectory(string directory, out string message)
        {
            try
            {
                if (!String.IsNullOrEmpty(directory) && !Directory.Exists(directory))
                    Directory.CreateDirectory(directory);

                message = $"VerifyAndCreateDirectory({directory}) success.";
                return true;
            }
            catch (Exception ex)
            {
                message = $"VerifyAndCreateDirectory({directory}) failed:  {Formatting.FormatException(ex)}";
                return false;
            }

        }
    }
}
