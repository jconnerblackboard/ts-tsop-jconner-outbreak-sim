﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Configuration;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Exceptions.Configuration;

namespace BbTS.Core.Configuration
{
    /// <summary>
    /// Class to load custom configuration data sets.  Uses the <see cref="System.Configuration.Configuration"/> class.
    /// </summary>
    public class CustomConfiguration
    {
        /// <summary>
        /// Load a custom configuration file.
        /// </summary>
        /// <param name="path">path to the configuration file</param>
        /// <param name="configuration">Custom configuration object to load into.</param>
        /// <param name="message">results message.</param>
        /// <returns>Success or Failure.</returns>
        public static bool LoadCustomConfiguration(
            string path,
            out System.Configuration.Configuration configuration,
            out string message)
        {
            configuration = null;
            try
            {
                ExeConfigurationFileMap configMap = new ExeConfigurationFileMap { ExeConfigFilename = path };
                configuration = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);
            }
            catch (Exception ex)
            {
                message = Formatting.FormatException(ex);
                return false;
            }

            message = "Success";
            return true;
        }


        /// <summary>
        /// Get the value of the speficied key as a string.
        /// </summary>
        /// <param name="key">Key to check for</param>
        /// <param name="configuration"></param>
        /// <param name="defaultValue">Fall back value in case the key is not found or will not parse.</param>
        /// <returns></returns>
        public static string GetKeyValueAsString(string key, System.Configuration.Configuration configuration, string defaultValue = null)
        {
            var settings = configuration.AppSettings.Settings;
            return settings[key] == null ? defaultValue : settings[key].Value;
        }

        /// <summary>
        /// Get the value of the speficied key as a string.
        /// </summary>
        /// <param name="key">Key to check for</param>
        /// <param name="configuration"></param>
        /// <param name="defaultValue">Fall back value in case the key is not found or will not parse.</param>
        /// <returns></returns>
        public static string GetKeyValueAsString(string key, CustomConfigurationModel configuration, string defaultValue = null)
        {
            var value = configuration.ValueGet(key);
            return value ?? defaultValue;
        }
    }
}
