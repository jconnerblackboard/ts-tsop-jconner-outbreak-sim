﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BbTS.Core.Conversion;
using Microsoft.Win32;

namespace BbTS.Core.Configuration
{
    /// <summary>
    /// Utility tool for configuring Windows Registry settings.
    /// </summary>
    public class RegistryConfigurationTool
    {
        /// <summary>
        /// The registry key used when fetching values.  Defaults to SOFTWARE\Blackboard\Transact\Configuration.
        /// </summary>
        public static string ConfigKey { get; set; } = @"SOFTWARE\Blackboard\Transact\Configuration";

        /// <summary>
        /// Singleton instance.
        /// </summary>
        public static RegistryConfigurationTool Instance { get; internal set; } = new RegistryConfigurationTool();

        /// <summary>
        /// Resets the RegistryConfigurationTool static parameters to their defaults.
        /// </summary>
        public void Reset()
        {
            ConfigKey = @"SOFTWARE\Blackboard\Transact\Configuration";
        }

        /// <summary>
        /// Fetch a value from the registry key specified by <see cref="ConfigKey"/>.
        /// </summary>
        /// <param name="key">The key of the value to get.</param>
        /// <returns>The value or null if not found.</returns>
        public string ValueGet(string key)
        {
            using (RegistryKey registryKey = Registry.LocalMachine.OpenSubKey(ConfigKey))
            {
                if (null == registryKey)
                {
                    throw new ApplicationException($"Registry Key at path {ConfigKey} not available");
                }
                return registryKey.GetValue(key) as string;
            }
        }

        /// <summary>
        /// Fetch a value from the registry key specified by <see cref="ConfigKey"/>.
        /// </summary>
        /// <param name="key">The key of the value to get.</param>
        /// <param name="defaultValue">Value to fall back on if the key isn't found.</param>
        /// <returns>The value or null if not found.</returns>
        public string ValueGet(string key, string defaultValue)
        {
            using (var registryKey = Registry.LocalMachine.OpenSubKey(ConfigKey))
            {
                if (null == registryKey)
                {
                    return defaultValue;
                }
                var value = registryKey.GetValue(key) as string;
                return value ?? defaultValue;
            }
        }

        /// <summary>
        /// Set the value of a registry key.  Creates the key if it doesn't exist.
        /// </summary>
        /// <param name="key">Key.</param>
        /// <param name="value">Value to set.</param>
        public void ValueSet(string key, string value)
        {
            using (var registryKey = Registry.LocalMachine.OpenSubKey(ConfigKey, true))
            {
                registryKey?.SetValue(key, value);
            }
        }

        /// <summary>
        /// Delete a value from the registry key.
        /// </summary>
        /// <param name="key">Key</param>
        public void ValueDelete(string key)
        {
            using (var registryKey = Registry.LocalMachine.OpenSubKey(ConfigKey, true))
            {
                registryKey?.DeleteValue(key);
            }
        }
    }
}
