﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace BbTS.Core.Configuration
{
    /// <summary>
    /// Reads a value from an INI file.  This class is written only to provide the bare minimum functionality
    /// that is needed, since we (hopefully) won't have to do much work with INI files.
    /// </summary>
    public static class IniFileReader
    {
        [DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
        private static extern int GetPrivateProfileInt(string section, string key, int defaultValue, string filePath);

        [DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
        private static extern int GetPrivateProfileString(string section, string key, string defaultValue, StringBuilder value, int size, string filePath);

        public static int ReadIntValue(string section, string key, string filePath, int defaultValue = 0)
        {
            return GetPrivateProfileInt(section, key, defaultValue, filePath);
        }

        public static string ReadStringValue(string section, string key, string filePath, string defaultvalue = "")
        {
            var value = new StringBuilder(512);
            GetPrivateProfileString(section, key, defaultvalue, value, value.Capacity, filePath);
            return value.ToString();
        }
    }
}
