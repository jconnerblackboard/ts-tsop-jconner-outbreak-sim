﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace BbTS.Core.Configuration
{
    public class ApplicationConfiguration
    {
        /// <summary>
        /// Get the value of the speficied key as an integer.
        /// </summary>
        /// <param name="key">Key to check for</param>
        /// <param name="defaultValue">Fall back value in case the key is not found or will not parse.</param>
        /// <returns></returns>
        public static int GetKeyValueAsInt(string key, int defaultValue = int.MinValue)
        {
            var settings = ConfigurationManager.AppSettings;
            int value;
            if (settings.HasKeys() && settings[key] != null && int.TryParse(settings[key], out value))
            {
                return value;
            }
            return defaultValue;
        }

        /// <summary>
        /// Get the value of the speficied key as a float.
        /// </summary>
        /// <param name="key">Key to check for</param>
        /// <param name="defaultValue">Fall back value in case the key is not found or will not parse.</param>
        /// <returns></returns>
        public static float GetKeyValueAsFloat(string key, float defaultValue = float.MinValue)
        {
            var settings = ConfigurationManager.AppSettings;
            float value;
            if (settings.HasKeys() && settings[key] != null && float.TryParse(settings[key], out value))
            {
                return value;
            }
            return defaultValue;
        }

        /// <summary>
        /// Get the value of the speficied key as a double.
        /// </summary>
        /// <param name="key">Key to check for</param>
        /// <param name="defaultValue">Fall back value in case the key is not found or will not parse.</param>
        /// <returns></returns>
        public static double GetKeyValueAsDouble(string key, double defaultValue = double.MinValue)
        {
            var settings = ConfigurationManager.AppSettings;
            double value;
            if (settings.HasKeys() && settings[key] != null && double.TryParse(settings[key], out value))
            {
                return value;
            }
            return defaultValue;
        }

        /// <summary>
        /// Get the value of the speficied key as a string.
        /// </summary>
        /// <param name="key">Key to check for</param>
        /// <param name="defaultValue">Fall back value in case the key is not found or will not parse.</param>
        /// <returns></returns>
        public static string GetKeyValueAsString(string key, string defaultValue = null)
        {
            var settings = ConfigurationManager.AppSettings;
            if (settings.HasKeys() && settings[key] != null)
            {
                return settings[key];
            }
            return defaultValue;
        }

        /// <summary>
        /// Get the value of the speficied key as a string.
        /// </summary>
        /// <param name="key">Key to check for</param>
        /// <param name="defaultValue">Fall back value in case the key is not found or will not parse.</param>
        /// <returns></returns>
        public static bool GetKeyValueAsBoolean(string key, bool defaultValue = false)
        {
            var settings = ConfigurationManager.AppSettings;
            if (settings.HasKeys() && settings[key] != null)
            {
                return 
                    settings[key].ToLower() == "t" ||
                    settings[key].ToLower() == "y" ||
                    settings[key].ToLower() == "true" ||
                    settings[key].ToLower() == "yes" ||
                    settings[key].ToLower() == "1" ||
                    settings[key].ToLower() == "enabled";
            }
            return defaultValue;
        }


        /// <summary>
        /// Alter or create a value for an application configuration setting.
        /// </summary>
        /// <param name="key">Key to change</param>
        /// <param name="value">Value to set</param>
        public static void SetConfigurationValue(string key, string value)
        {
            System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            if (config.AppSettings.Settings[key] != null)
            {
                config.AppSettings.Settings[key].Value = value;
            }
            else
            {
                config.AppSettings.Settings.Add(key, value);
            }

            config.AppSettings.SectionInformation.ForceSave = true;
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection(config.AppSettings.SectionInformation.Name);
        }

        /// <summary>
        /// Get the currently running method of the caller.
        /// </summary>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.NoInlining)]
        public static string GetCurrentMethod()
        {
            StackTrace st = new StackTrace();
            StackFrame sf = st.GetFrame(1);

            return sf.GetMethod().Name;
        }

        /// <summary>
        /// Check to see if the calling method is "enabled" in the app.config file.
        /// </summary>
        /// <param name="message">response message.</param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.NoInlining)]
        public static bool CurrentMethodEnabled(out string message)
        {
            message = string.Empty;
            StackTrace st = new StackTrace();
            StackFrame sf = st.GetFrame(1);
            string currentMethod = sf.GetMethod().Name;
            if (!GetKeyValueAsBoolean(currentMethod))
            {
                message = String.Format("Method skipped per app.config parameter {0}.  Set parameter to 't' to enable.", currentMethod);
                return false;
            }
            return true;
        }

    }
}
