﻿using System;
using System.Runtime.InteropServices;

namespace BbTS.Core.Configuration
{
    public static class DateAndTimeTools
    {
        [StructLayout(LayoutKind.Sequential)]
        private struct SystemTime
        {
            public short Year;
            public short Month;
            public short DayOfWeek;
            public short Day;
            public short Hour;
            public short Minute;
            public short Second;
            public short Milliseconds;
        }

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool SetLocalTime(ref SystemTime lpSystemTime);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool SetSystemTime(ref SystemTime lpSystemTime);


        /// <summary>
        /// Sets the system to the specified time.
        /// DateTime is treated as a local time (not UTC).
        /// </summary>
        /// <param name="dateTime"></param>
        public static void SetDeviceDateTimeLocal(DateTime dateTime)
        {
            DateTime dateTimeLocal = dateTime.ToLocalTime();
            var timeNew = new SystemTime
            {
                Year = (short)dateTimeLocal.Year,
                Month = (short)dateTimeLocal.Month,
                Day = (short)dateTimeLocal.Day,
                DayOfWeek = (short)dateTimeLocal.DayOfWeek,
                Hour = (short)dateTimeLocal.Hour,
                Minute = (short)dateTimeLocal.Minute,
                Second = (short)dateTimeLocal.Second,
                Milliseconds = (short)dateTimeLocal.Millisecond
            };
            SetLocalTime(ref timeNew);
        }

        /// <summary>
        /// Sets the system to the specified time.
        /// DateTime is treated as a UTC time (not local).
        /// </summary>
        /// <param name="dateTime"></param>
        public static void SetDeviceDateTimeUtc(DateTime dateTime)
        {
            DateTime dateTimeUtc = dateTime.ToUniversalTime();
            var timeNew = new SystemTime
            {
                Year = (short)dateTimeUtc.Year,
                Month = (short)dateTimeUtc.Month,
                Day = (short)dateTimeUtc.Day,
                DayOfWeek = (short)dateTimeUtc.DayOfWeek,
                Hour = (short)dateTimeUtc.Hour,
                Minute = (short)dateTimeUtc.Minute,
                Second = (short)dateTimeUtc.Second,
                Milliseconds = (short)dateTimeUtc.Millisecond
            };
            SetSystemTime(ref timeNew);
        }
    }
}
