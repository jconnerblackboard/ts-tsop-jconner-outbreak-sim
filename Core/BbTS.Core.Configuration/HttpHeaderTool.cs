﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using BbTS.Core.Conversion;

namespace BbTS.Core.Configuration
{
    public static class HttpHeaderTool
    {
        /// <summary>
        /// Extract a sub field value from a super header
        /// </summary>
        /// <param name="headers">NameValueCollection representing a set of headers</param>
        /// <param name="super">parent header item</param>
        /// <param name="key">child item</param>
        /// <returns></returns>
        public static string ExtractChildHeaderValue(NameValueCollection headers, string super, string key)
        {
            if (!headers.HasKeys()) throw new ArgumentException("ExtractChildHeaderValue: Missing values.");
            if (headers[super] == null) throw new ArgumentException(String.Format("ExtractChildHeaderValue: {0} field is missing.", key));

            String bbtsValue = headers[super];
            List<String> fields = new List<string>(bbtsValue.Split(','));

            // Extract the ApplicationName field
            String rawFieldEntry = fields.FirstOrDefault(f => f.Contains(key));
            if (!String.IsNullOrEmpty(rawFieldEntry))
            {
                KeyValuePair<string, string> field = Formatting.ParseNameValuePair(rawFieldEntry, '=', true);
                if (field.Key == key) return field.Value;
            }
            return null;
        }
    }
}
