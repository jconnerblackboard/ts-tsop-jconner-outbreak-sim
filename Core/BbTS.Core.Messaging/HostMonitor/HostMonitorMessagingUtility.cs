﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using BbTS.Core.Configuration;
using BbTS.Core.Conversion;
using BbTS.Core.Messaging.Msmq;
using BbTS.Core.Transaction;
using BbTS.Domain.Models.ArtsDataModel;
using BbTS.Domain.Models.BoardPlan;
using BbTS.Domain.Models.Credential;
using BbTS.Domain.Models.Customer;
using BbTS.Domain.Models.Definitions.Container;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Definitions.Monitoring;
using BbTS.Domain.Models.Device;
using BbTS.Domain.Models.EventLog;
using BbTS.Domain.Models.Messaging;
using BbTS.Domain.Models.Messaging.HostMonitor;
using BbTS.Domain.Models.Messaging.HostMonitor.Comparison;
using BbTS.Domain.Models.Operator;
using BbTS.Domain.Models.Report;
using BbTS.Monitoring.Logging;
using PostSharp.Aspects;

namespace BbTS.Core.Messaging.HostMonitor
{
    using BbTS.Domain.Models.Container;

    /// <summary>
    /// Utility class for parsing, rendering and reporting of host monitor messages.
    /// </summary>
    public class HostMonitorMessagingUtility
    {
        private static readonly object LogLock = new object();
        private static readonly List<HostMonitorMessage> Logs = new List<HostMonitorMessage>();
        private static DateTimeOffset _timeSinceLastPurge = DateTimeOffset.MinValue;

        /// <summary>
        /// The host monitor MSMQ name.
        /// </summary>
        public const string QueueName = ".\\Private$\\HostMonitorMessaging";

        /// <summary>
        /// The maximum number of messages to store per queue.
        /// </summary>
        public static int MaxLogSize { get; set; } = 5000;

        /// <summary>
        /// Get the messages currently stored.
        /// </summary>
        public static List<HostMonitorMessage> Messages => Logs;

        /// <summary>
        /// Get the maximum lifespan of host monitor messages expressed in minutes.
        /// </summary>
        public static int MessageLifespanMinutes { get; } = ApplicationConfiguration.GetKeyValueAsInt("MsmqExpiraryTimeMinutes", 60);

        /// <summary>
        /// Get the amount of time between purge operations (in seconds).
        /// </summary>
        public static int TimeBetweenLogPurgeRoutinesSeconds { get; } = ApplicationConfiguration.GetKeyValueAsInt("TimeBetweenLogPurgeRoutinesSeconds", 60);

        #region Functions

        /// <summary>
        /// Extract a list of host monitor messages to fire off on MSMQ.
        /// </summary>
        /// <param name="args">arguments to parse.</param>
        /// <param name="methodName">The name of the calling method.</param>
        /// <returns>List of messages to fire.</returns>
        public static List<HostMonitorMessage> ExtractHostMonitorMessages(MethodExecutionArgs args, string methodName)
        {
            var list = new List<HostMonitorMessage>();
            try
            {
                foreach (var arg in args.Arguments)
                {
                    var messages = _extractAllHostMonitorMessages(arg, methodName);
                    if (messages.Count > 0)
                    {
                        list.AddRange(messages);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(
                    ex, "", "", EventLogEntryType.Warning,
                    (short)LoggingDefinitions.Category.HostMonitor,
                    (int)LoggingDefinitions.EventId.HostMonitorParsingError);
            }

            try
            {
                var returnMessages = _extractAllHostMonitorMessages(args.ReturnValue, methodName);
                if (returnMessages.Count > 0)
                {
                    list.AddRange(returnMessages);
                }
            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(
                    ex, "", "", EventLogEntryType.Warning,
                    (short)LoggingDefinitions.Category.HostMonitor,
                    (int)LoggingDefinitions.EventId.HostMonitorParsingError);
            }

            // if there was no host monitor message extracted, create a generic message.
            if (list.Count == 0)
            {
                list.Add(_generalMonitorMessageCreate(methodName));
            }

            return list;
        }

        /// <summary>
        /// Extract a list of host monitor messages to fire off on MSMQ.
        /// </summary>
        /// <param name="attributes">Parameters to enter into the queue.</param>
        /// <returns>List of messages to fire.</returns>
        public static List<HostMonitorMessage> ExtractHostMonitorMessages(AttributeParse attributes)
        {
            var list = new List<HostMonitorMessage>();
            try
            {
                foreach (var arg in attributes.Parameters)
                {
                    var messages = _extractAllHostMonitorMessages(arg, attributes.Endpoint, attributes.ClientIp);
                    if (messages.Count > 0)
                    {
                        list.AddRange(messages);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(
                    ex, "", "", EventLogEntryType.Warning,
                    (short)LoggingDefinitions.Category.HostMonitor,
                    (int)LoggingDefinitions.EventId.HostMonitorParsingError);
            }

            // if there was no host monitor message extracted, create a generic message.
            if (list.Count == 0)
            {
                list.Add(_generalMonitorMessageCreate(attributes.Endpoint, attributes.ClientIp));
            }

            return list;
        }

        /// <summary>
        /// Log a host monitor message.
        /// </summary>
        /// <param name="message">message to add</param>
        public static void LogHostMonitorMessage(HostMonitorMessage message)
        {
            lock (LogLock)
            {
                // Make sure the message doesn't already exist in the logs
                if (Logs.FirstOrDefault(log => log.Id == message.Id) == null)
                {
                    // Make sure the message isn't really old.
                    if ((DateTimeOffset.Now - message.DateTimeOffset.DateTime).TotalMinutes < MessageLifespanMinutes)
                    {
                        Logs.Add(message);
                    }
                }
            }
        }

        /// <summary>
        /// Log a host monitor message.
        /// </summary>
        /// <param name="messages">messages to add.</param>
        public static List<HostMonitorMessage> LogHostMonitorMessages(List<HostMonitorMessage> messages)
        {
            lock (LogLock)
            {
                // Get all the logs in messages that aren't already in Logs.
                var logDiff = messages.Except(Logs, new HostMonitorMessageEqualityComparer()).ToList();

                // Skim out the logs that are old
                var newLogs = logDiff.FindAll(log => (DateTimeOffset.Now - log.DateTimeOffset.DateTime).TotalMinutes < MessageLifespanMinutes).ToList();

                Logs.AddRange(newLogs);

                return newLogs;
            }
        }

        /// <summary>
        /// Enter a set of method arguments into a queue to be processed for host monitor messaging events.  Uses threading.
        /// </summary>
        /// <param name="args">List of arguments to enter into the queue.</param>
        /// <param name="methodName">The name of the calling method.</param>
        public static void QueueHostMonitorMessagingEvent(MethodExecutionArgs args, string methodName)
        {
            try
            {
                ThreadPool.QueueUserWorkItem(state => _processHostMonitorQueueEntryRequest(args, methodName), null);
            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(ex, EventLogEntryType.Error, (short)LoggingDefinitions.Category.HostMonitor);
            }
        }

        /// <summary>
        /// Enter a set of parameters into a queue to be processed for host monitor messaging events.  Uses threading.
        /// </summary>
        /// <param name="attributes">Parameters to enter into the queue.</param>
        public static void QueueHostMonitorMessagingEvent(AttributeParse attributes)
        {
            try
            {
                ThreadPool.QueueUserWorkItem(state => _processHostMonitorQueueEntryRequest(attributes), null);
            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(ex, EventLogEntryType.Error, (short)LoggingDefinitions.Category.HostMonitor);
            }
        }

        /// <summary>
        /// Purge old logs based on expiration time.
        /// </summary>
        /// <param name="overrideExpirationTime">Set to override the default time defined in the configuration file.</param>
        /// <param name="overrideAmount">Amount to set the override expiration time to (in minutes).</param>
        public static void PurgeLogs(bool overrideExpirationTime = false, int overrideAmount = 60)
        {
            // Logs can only be purged once every TimeBetweenLogPurgeRoutinesSeconds (default 60 seconds).
            if ((DateTimeOffset.Now - _timeSinceLastPurge).TotalSeconds > TimeBetweenLogPurgeRoutinesSeconds)
            {
                lock (LogLock)
                {
                    var expirationTime = overrideExpirationTime ? overrideAmount : MessageLifespanMinutes;
                    // Skim out the logs that are old
                    var newLogs = Logs.FindAll(log => (DateTimeOffset.Now - log.DateTimeOffset.DateTime).TotalMinutes < expirationTime).ToList();

                    Logs.Clear();
                    Logs.AddRange(newLogs);
                    _timeSinceLastPurge = DateTimeOffset.Now;
                }
            }
        }

        /// <summary>
        /// Write a host monitor message to the queue.
        /// </summary>
        /// <param name="message">The <see cref="HostMonitorMessage"/> object to write to the MSMQ queue.</param>
        /// <param name="methodName">The name of the calling method.</param>
        public static void WriteHostMonitorMessage(HostMonitorMessage message, string methodName)
        {

            MsmqUtility.WriteMessage(
                message,
                QueueName,
                methodName,
                new TimeSpan(0, 0, MessageLifespanMinutes, 0),
                new JsonMessageFormatter<List<NameValueTree>>());
        }

        #endregion


        #region Private

        /// <summary>
        /// Private  thread for the handling of arguments to be parsed and processed into host monitor messaging events.
        /// </summary>
        /// <param name="args">List of arguments to process.</param>
        /// <param name="methodName">The name of the calling method.</param>
        private static void _processHostMonitorQueueEntryRequest(MethodExecutionArgs args, string methodName)
        {
            try
            {
                var messages = ExtractHostMonitorMessages(args, methodName);
                foreach (var message in messages)
                {
                    WriteHostMonitorMessage(message, methodName);
                }
            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(ex, EventLogEntryType.Error, (short)LoggingDefinitions.Category.HostMonitor);
            }

        }

        /// <summary>
        /// Private  thread for the handling of parameters to be parsed and processed into host monitor messaging events.
        /// </summary>
        /// <param name="attributes">Parameters to enter into the queue.</param>
        private static void _processHostMonitorQueueEntryRequest(AttributeParse attributes)
        {
            try
            {
                var messages = ExtractHostMonitorMessages(attributes);
                foreach (var message in messages)
                {
                    WriteHostMonitorMessage(message, attributes.Endpoint);
                }
            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(ex, EventLogEntryType.Error, (short)LoggingDefinitions.Category.HostMonitor);
            }

        }

        /// <summary>
        /// Extract a list of host monitor messages to fire off on MSMQ.
        /// </summary>
        /// <param name="argument">argument to parse.</param>
        /// <param name="methodName">The name of the calling method.</param>
        /// <param name="clientIp">(Optional) Client Ip address to use as the message source.</param>
        /// <returns>List of messages to fire.</returns>
        public static List<HostMonitorMessage> _extractAllHostMonitorMessages(object argument, string methodName, string clientIp = null)
        {
            var list = new List<HostMonitorMessage>();

            // Board
            list.AddRange(_boardInformationGetHostMonitorMessageExtract(argument, methodName, clientIp));

            // Customer
            list.AddRange(_customerBoardBalanceGetHostMonitorMessageExtract(argument, methodName, clientIp));
            list.AddRange(_customerCredentialVerifyRequestHostMonitorMessageExtract(argument, methodName, clientIp));
            list.AddRange(_customerCredentialVerifyResponseHostMonitorMessageExtract(argument, methodName, clientIp));
            list.AddRange(_customerInfoGetResponseHostMonitorMessageExtract(argument, methodName, clientIp));
            list.AddRange(_customerStoredValueBalanceGetHostMonitorMessageExtract(argument, methodName, clientIp));

            // Device
            list.AddRange(_deviceSettingsHostMonitorMessageExtract(argument, methodName, clientIp));
            list.AddRange(_deviceRegistrationPostHostMonitorMessagesExtract(argument, methodName, clientIp));
            list.AddRange(_deviceRegistrationPatchHostMonitorMessagesExtract(argument, methodName, clientIp));
            list.AddRange(_deviceLoginHostMonitorMessagesExtract(argument, methodName, clientIp));
            list.AddRange(_deviceLogoutHostMonitorMessagesExtract(argument, methodName, clientIp));
            list.AddRange(_deviceOperatorsGetHostMonitorMessagesExtract(argument, methodName, clientIp));
            list.AddRange(_devicePropertiesGetHostMonitorMessagesExtract(argument, methodName, clientIp));
            list.AddRange(_deviceHeartbeatResponseHostMonitorMessageExtract(argument, methodName, clientIp));

            // Event
            list.AddRange(_eventLogGetHostMonitorMessageExtract(argument, methodName, clientIp));
            list.AddRange(_eventLogSetHostMonitorMessageExtract(argument, methodName, clientIp));
            list.AddRange(_eventLogSummaryGetHostMonitorMessageExtract(argument, methodName, clientIp));

            // Operator
            list.AddRange(_operatorPinChangeRequestHostMonitorMessageExtract(argument, methodName, clientIp));
            list.AddRange(_operatorSessionBeginHostMonitorMessageExtract(argument, methodName, clientIp));
            list.AddRange(_operatorSessionEndHostMonitorMessageExtract(argument, methodName, clientIp));
            list.AddRange(_operatorValidateRequestHostMonitorMessageExtract(argument, methodName, clientIp));

            // Report
            list.AddRange(_drawerAuditGetHostMonitorMessageExtract(argument, methodName, clientIp));
            list.AddRange(_posAuditGetHostMonitorMessageExtract(argument, methodName, clientIp));
            list.AddRange(_profitCenterAuditGetHostMonitorMessageExtract(argument, methodName, clientIp));

            // Transaction objects
            list.AddRange(_transactionHostMonitorMessageExtract(argument, methodName, clientIp));

            return list;
        }



        #region Board functions

        /// <summary>
        /// Extract a host monitor message from a board information get request.
        /// </summary>
        /// <param name="argument">argument to parse.</param>
        /// <param name="methodName">The name of the calling method.</param>
        /// <param name="clientIp">(Optional) Client Ip address to use as the message source.</param>
        /// <returns>Host monitor message or null if not found or cant be created.</returns>
        private static List<HostMonitorMessage> _boardInformationGetHostMonitorMessageExtract(object argument, string methodName, string clientIp = null)
        {
            var item = Formatting.ExtractObject<BoardInformationGetRequest>(argument) as BoardInformationGetRequest;
            if (item == null)
            {
                return new List<HostMonitorMessage>();
            }
            var boardInsert = item.BoardPlanId == null ? "all board information" : $"board information for board plan '{item.BoardPlanId}'";
            var customerInsert = item.CustomerGuid == null ? " for all customers." : $" for customer '{item.CustomerGuid}'";
            var optionalInsert = $" where GuestMeal is '{item.IsGuestMeal}', ForcePost is '{item.ForcePost}', and MealTypeId is '{item.MealTypeId}'.";
            var message = $"Device '{item.OriginatorGuid}' has requested {boardInsert}{customerInsert}{optionalInsert}";

            return new List<HostMonitorMessage>
            {
                new HostMonitorMessage
                {
                    Amount = "",
                    CardNumber = "",
                    DateTimeOffset = DateTimeOffset.Now,
                    DeviceIdentifier = clientIp ?? item.OriginatorGuid,
                    Function = HostMonitorSubscriptionServiceFunctions.BoardInformationGet,
                    HostType = @"WebApi",
                    Id = Guid.NewGuid().ToString("D"),
                    Message = message,
                    MessageSource = methodName,
                    TransactionNumber = ""
                }
            };
        }

        #endregion

        #region Customer functions

        /// <summary>
        /// Extract a host monitor message from a customer board balance get request.
        /// </summary>
        /// <param name="argument">argument to parse.</param>
        /// <param name="methodName">The name of the calling method.</param>
        /// <param name="clientIp">(Optional) Client Ip address to use as the message source.</param>
        /// <returns>Host monitor message or null if not found or cant be created.</returns>
        private static List<HostMonitorMessage> _customerBoardBalanceGetHostMonitorMessageExtract(object argument, string methodName, string clientIp = null)
        {
            var item = Formatting.ExtractObject<CustomerBoardBalanceGetResponse>(argument) as CustomerBoardBalanceGetResponse;
            if (item == null)
            {
                return new List<HostMonitorMessage>();
            }
            var message = $"Device '{item.DeviceGuid}' has requested a board balance for customer '{item.CustomerGuid}'";

            return new List<HostMonitorMessage>
            {
                new HostMonitorMessage
                {
                    Amount = "",
                    CardNumber = "",
                    DateTimeOffset = DateTimeOffset.Now,
                    DeviceIdentifier = clientIp ?? item.DeviceGuid,
                    Function = HostMonitorSubscriptionServiceFunctions.BoardInformationGet,
                    HostType = @"WebApi",
                    Id = Guid.NewGuid().ToString("D"),
                    Message = message,
                    MessageSource = methodName,
                    TransactionNumber = ""
                }
            };
        }

        /// <summary>
        /// Extract a host monitor message from a customer stored value balance get request.
        /// </summary>
        /// <param name="argument">argument to parse.</param>
        /// <param name="methodName">The name of the calling method.</param>
        /// <param name="clientIp">(Optional) Client Ip address to use as the message source.</param>
        /// <returns>Host monitor message or null if not found or cant be created.</returns>
        private static List<HostMonitorMessage> _customerStoredValueBalanceGetHostMonitorMessageExtract(object argument, string methodName, string clientIp = null)
        {
            var item = Formatting.ExtractObject<CustomerStoredValueBalanceGetResponse>(argument) as CustomerStoredValueBalanceGetResponse;
            if (item == null)
            {
                return new List<HostMonitorMessage>();
            }
            var message = $"Device '{item.DeviceGuid}' has requested a stored value balance for customer '{item.CustomerGuid}'";

            return new List<HostMonitorMessage>
            {
                new HostMonitorMessage
                {
                    Amount = "",
                    CardNumber = "",
                    DateTimeOffset = DateTimeOffset.Now,
                    DeviceIdentifier = clientIp ?? item.DeviceGuid,
                    Function = HostMonitorSubscriptionServiceFunctions.BoardInformationGet,
                    HostType = @"WebApi",
                    Id = Guid.NewGuid().ToString("D"),
                    Message = message,
                    MessageSource = methodName,
                    TransactionNumber = ""
                }
            };
        }

        /// <summary>
        /// Extract a host monitor message from a customer credential verify request.
        /// </summary>
        /// <param name="argument">argument to parse.</param>
        /// <param name="methodName">The name of the calling method.</param>
        /// <param name="clientIp">(Optional) Client Ip address to use as the message source.</param>
        /// <returns>Host monitor message or null if not found or cant be created.</returns>
        private static List<HostMonitorMessage> _customerCredentialVerifyRequestHostMonitorMessageExtract(object argument, string methodName, string clientIp = null)
        {
            var item = Formatting.ExtractObject<CredentialVerifyRequestV01>(argument) as CredentialVerifyRequestV01;
            if (item == null)
            {
                return new List<HostMonitorMessage>();
            }
            var message = @"Customer credential verify request was initiated.";

            return new List<HostMonitorMessage>
            {
                new HostMonitorMessage
                {
                    Amount = "",
                    CardNumber = "",
                    DateTimeOffset = DateTimeOffset.Now,
                    DeviceIdentifier = clientIp,
                    Function = HostMonitorSubscriptionServiceFunctions.CustomerCredentialVerifyRequest,
                    HostType = @"WebApi",
                    Id = Guid.NewGuid().ToString("D"),
                    Message = message,
                    MessageSource = methodName,
                    TransactionNumber = ""
                }
            };
        }

        /// <summary>
        /// Extract a host monitor message from a customer credential verify response.
        /// </summary>
        /// <param name="argument">argument to parse.</param>
        /// <param name="methodName">The name of the calling method.</param>
        /// <param name="clientIp">(Optional) Client Ip address to use as the message source.</param>
        /// <returns>Host monitor message or null if not found or cant be created.</returns>
        private static List<HostMonitorMessage> _customerCredentialVerifyResponseHostMonitorMessageExtract(object argument, string methodName, string clientIp = null)
        {
            var item = Formatting.ExtractObject<CredentialVerifyResponseV01>(argument) as CredentialVerifyResponseV01;
            if (item == null)
            {
                return new List<HostMonitorMessage>();
            }
            var message = $"Credentials successfully verified for customer '{item.Customer.CustomerGuid}'";

            return new List<HostMonitorMessage>
            {
                new HostMonitorMessage
                {
                    Amount = "",
                    CardNumber = "",
                    DateTimeOffset = DateTimeOffset.Now,
                    DeviceIdentifier = clientIp,
                    Function = HostMonitorSubscriptionServiceFunctions.CustomerCredentialVerifyResponse,
                    HostType = @"WebApi",
                    Id = Guid.NewGuid().ToString("D"),
                    Message = message,
                    MessageSource = methodName,
                    TransactionNumber = ""
                }
            };
        }

        /// <summary>
        /// Extract a host monitor message from a customer info get request.
        /// </summary>
        /// <param name="argument">argument to parse.</param>
        /// <param name="methodName">The name of the calling method.</param>
        /// <param name="clientIp">(Optional) Client Ip address to use as the message source.</param>
        /// <returns>Host monitor message or null if not found or cant be created.</returns>
        private static List<HostMonitorMessage> _customerInfoGetResponseHostMonitorMessageExtract(object argument, string methodName, string clientIp = null)
        {
            var item = Formatting.ExtractObject<CustomerGetResponse>(argument) as CustomerGetResponse;
            if (item == null)
            {
                return new List<HostMonitorMessage>();
            }
            var message = $"Customer information requested for customer '{item.Customer.CustomerGuid}'";

            return new List<HostMonitorMessage>
            {
                new HostMonitorMessage
                {
                    Amount = "",
                    CardNumber = "",
                    DateTimeOffset = DateTimeOffset.Now,
                    DeviceIdentifier = clientIp,
                    Function = HostMonitorSubscriptionServiceFunctions.CustomerInfoGet,
                    HostType = @"WebApi",
                    Id = Guid.NewGuid().ToString("D"),
                    Message = message,
                    MessageSource = methodName,
                    TransactionNumber = ""
                }
            };
        }

        #endregion

        #region Device information extraction functions

        /// <summary>
        /// Extract a host monitor message from a device heartbeat request.
        /// </summary>
        /// <param name="argument">argument to parse.</param>
        /// <param name="methodName">The name of the calling method.</param>
        /// <param name="clientIp">(Optional) Client Ip address to use as the message source.</param>
        /// <returns>Host monitor message or null if not found or cant be created.</returns>
        private static List<HostMonitorMessage> _deviceHeartbeatResponseHostMonitorMessageExtract(object argument, string methodName, string clientIp = null)
        {
            var item = Formatting.ExtractObject<DeviceHeartbeatRequest>(argument) as DeviceHeartbeatRequest;
            if (item == null)
            {
                return new List<HostMonitorMessage>();
            }
            var message = $"Device heartbeat detected for device '{item.DeviceId}' with type '{item.DeviceType}'.";

            return new List<HostMonitorMessage>
            {
                new HostMonitorMessage
                {
                    Amount = "",
                    CardNumber = "",
                    DateTimeOffset = DateTimeOffset.Now,
                    DeviceIdentifier = clientIp ?? item.DeviceId,
                    Function = HostMonitorSubscriptionServiceFunctions.DeviceHeartbeat,
                    HostType = @"WebApi",
                    Id = Guid.NewGuid().ToString("D"),
                    Message = message,
                    MessageSource = methodName,
                    TransactionNumber = ""
                }
            };
        }

        /// <summary>
        /// Extract a host monitor message from a device settings request.
        /// </summary>
        /// <param name="argument">argument to parse.</param>
        /// <param name="methodName">The name of the calling method.</param>
        /// <param name="clientIp">(Optional) Client Ip address to use as the message source.</param>
        /// <returns>Host monitor message or null if not found or cant be created.</returns>
        private static List<HostMonitorMessage> _deviceSettingsHostMonitorMessageExtract(object argument, string methodName, string clientIp = null)
        {
            var settings = Formatting.ExtractObject<DeviceSettings>(argument) as DeviceSettings;
            if (settings == null)
            {
                return new List<HostMonitorMessage>();
            }

            return new List<HostMonitorMessage>
            {
                new HostMonitorMessage
                {
                    Amount = "",
                    CardNumber = "",
                    DateTimeOffset = DateTimeOffset.Now,
                    DeviceIdentifier = clientIp ?? settings.Properties.DeviceGuid,
                    Function = HostMonitorSubscriptionServiceFunctions.DeviceSettingsGet,
                    HostType = @"WebApi",
                    Id = Guid.NewGuid().ToString("D"),
                    Message = $"Device '{settings.Properties.DeviceGuid}' has requested device settings.",
                    MessageSource = methodName,
                    TransactionNumber = ""
                }
            };
        }

        /// <summary>
        /// Extract a host monitor message from a device registration create request.
        /// </summary>
        /// <param name="argument">argument to parse.</param>
        /// <param name="methodName">The name of the calling method.</param>
        /// <param name="clientIp">(Optional) Client Ip address to use as the message source.</param>
        /// <returns>Host monitor message or null if not found or cant be created.</returns>
        private static List<HostMonitorMessage> _deviceRegistrationPostHostMonitorMessagesExtract(object argument, string methodName, string clientIp = null)
        {
            var registration = Formatting.ExtractObject<DeviceRegistrationPostRequest>(argument) as DeviceRegistrationPostRequest;
            if (registration == null)
            {
                return new List<HostMonitorMessage>();
            }

            return new List<HostMonitorMessage>
            {
                new HostMonitorMessage
                {
                    Amount = "",
                    CardNumber = "",
                    DateTimeOffset = DateTimeOffset.Now,
                    DeviceIdentifier = clientIp ?? $"Name: {registration.Name}, SN: {registration.SerialNumber}",
                    Function = HostMonitorSubscriptionServiceFunctions.DeviceRegistrationPost,
                    HostType = @"WebApi",
                    Id = Guid.NewGuid().ToString("D"),
                    Message = $"Device '{registration.Name}' with serial number '{registration.SerialNumber}' has requested registration.",
                    MessageSource = methodName,
                    TransactionNumber = ""
                }
            };
        }

        /// <summary>
        /// Extract a host monitor message from a device registration update request.
        /// </summary>
        /// <param name="argument">argument to parse.</param>
        /// <param name="methodName">The name of the calling method.</param>
        /// <param name="clientIp">(Optional) Client Ip address to use as the message source.</param>
        /// <returns>Host monitor message or null if not found or cant be created.</returns>
        private static List<HostMonitorMessage> _deviceRegistrationPatchHostMonitorMessagesExtract(object argument, string methodName, string clientIp = null)
        {
            var registration = Formatting.ExtractObject<DeviceRegistrationPatchRequest>(argument) as DeviceRegistrationPatchRequest;
            if (registration == null)
            {
                return new List<HostMonitorMessage>();
            }

            return new List<HostMonitorMessage>
            {
                new HostMonitorMessage
                {
                    Amount = "",
                    CardNumber = "",
                    DateTimeOffset = DateTimeOffset.Now,
                    DeviceIdentifier = clientIp ?? $"{registration.DeviceGuid}",
                    Function = HostMonitorSubscriptionServiceFunctions.DeviceRegistrationPatch,
                    HostType = @"WebApi",
                    Id = Guid.NewGuid().ToString("D"),
                    Message = $"Device registration update requested for device '{registration.DeviceGuid}'.",
                    MessageSource = methodName,
                    TransactionNumber = ""
                }
            };
        }

        /// <summary>
        /// Extract a host monitor message from a device login request.
        /// </summary>
        /// <param name="argument">argument to parse.</param>
        /// <param name="methodName">The name of the calling method.</param>
        /// <param name="clientIp">(Optional) Client Ip address to use as the message source.</param>
        /// <returns>Host monitor message or null if not found or cant be created.</returns>
        private static List<HostMonitorMessage> _deviceLoginHostMonitorMessagesExtract(object argument, string methodName, string clientIp = null)
        {
            var item = Formatting.ExtractObject<DeviceLoginRequest>(argument) as DeviceLoginRequest;
            if (item == null)
            {
                return new List<HostMonitorMessage>();
            }

            return new List<HostMonitorMessage>
            {
                new HostMonitorMessage
                {
                    Amount = "",
                    CardNumber = "",
                    DateTimeOffset = DateTimeOffset.Now,
                    DeviceIdentifier = clientIp ?? $"{item.DeviceId}",
                    Function = HostMonitorSubscriptionServiceFunctions.DeviceLogin,
                    HostType = @"WebApi",
                    Id = Guid.NewGuid().ToString("D"),
                    Message = $"Operator '{item.OperatorId}' is logging into Device '{item.DeviceId}' with session '{item.SessionId}'.",
                    MessageSource = methodName,
                    TransactionNumber = ""
                }
            };
        }

        /// <summary>
        /// Extract a host monitor message from a device logout request.
        /// </summary>
        /// <param name="argument">argument to parse.</param>
        /// <param name="methodName">The name of the calling method.</param>
        /// <param name="clientIp">(Optional) Client Ip address to use as the message source.</param>
        /// <returns>Host monitor message or null if not found or cant be created.</returns>
        private static List<HostMonitorMessage> _deviceLogoutHostMonitorMessagesExtract(object argument, string methodName, string clientIp = null)
        {
            var item = Formatting.ExtractObject<DeviceLogoutRequest>(argument) as DeviceLogoutRequest;
            if (item == null)
            {
                return new List<HostMonitorMessage>();
            }

            return new List<HostMonitorMessage>
            {
                new HostMonitorMessage
                {
                    Amount = "",
                    CardNumber = "",
                    DateTimeOffset = DateTimeOffset.Now,
                    DeviceIdentifier = clientIp ?? $"{item.DeviceId}",
                    Function = HostMonitorSubscriptionServiceFunctions.DeviceLogout,
                    HostType = @"WebApi",
                    Id = Guid.NewGuid().ToString("D"),
                    Message = $"Operator '{item.OpoeratorId}' is logging out of Device '{item.DeviceId}' with session '{item.SessionId}'.",
                    MessageSource = methodName,
                    TransactionNumber = ""
                }
            };
        }

        /// <summary>
        /// Extract a host monitor message from a device operators get request.
        /// </summary>
        /// <param name="argument">argument to parse.</param>
        /// <param name="methodName">The name of the calling method.</param>
        /// <param name="clientIp">(Optional) Client Ip address to use as the message source.</param>
        /// <returns>Host monitor message or null if not found or cant be created.</returns>
        private static List<HostMonitorMessage> _deviceOperatorsGetHostMonitorMessagesExtract(object argument, string methodName, string clientIp = null)
        {
            var item = Formatting.ExtractObject<DeviceOperatorsGetResponse>(argument) as DeviceOperatorsGetResponse;
            if (item == null)
            {
                return new List<HostMonitorMessage>();
            }

            return new List<HostMonitorMessage>
            {
                new HostMonitorMessage
                {
                    Amount = "",
                    CardNumber = "",
                    DateTimeOffset = DateTimeOffset.Now,
                    DeviceIdentifier = clientIp ?? $"{item.DeviceId}",
                    Function = HostMonitorSubscriptionServiceFunctions.DeviceOperatorsGet,
                    HostType = @"WebApi",
                    Id = Guid.NewGuid().ToString("D"),
                    Message = $"Device '{item.DeviceId}' has requested a list of allowed operators.  {item.Operators.Count} operators were retrieved.",
                    MessageSource = methodName,
                    TransactionNumber = ""
                }
            };
        }

        /// <summary>
        /// Extract a host monitor message from a device operators get request.
        /// </summary>
        /// <param name="argument">argument to parse.</param>
        /// <param name="methodName">The name of the calling method.</param>
        /// <param name="clientIp">(Optional) Client Ip address to use as the message source.</param>
        /// <returns>Host monitor message or null if not found or cant be created.</returns>
        private static List<HostMonitorMessage> _devicePropertiesGetHostMonitorMessagesExtract(object argument, string methodName, string clientIp = null)
        {
            var item = Formatting.ExtractObject<DevicePropertiesGetResponse>(argument) as DevicePropertiesGetResponse;
            if (item == null)
            {
                return new List<HostMonitorMessage>();
            }

            var message = string.IsNullOrEmpty(item.DeviceGuid) ?
                "All device properties were requested by a client." :
                $"Device properties for device '{item.DeviceGuid}' were requested by a client.";

            return new List<HostMonitorMessage>
            {
                new HostMonitorMessage
                {
                    Amount = "",
                    CardNumber = "",
                    DateTimeOffset = DateTimeOffset.Now,
                    DeviceIdentifier = clientIp ?? $"{item.DeviceGuid}",
                    Function = HostMonitorSubscriptionServiceFunctions.DevicePropertiesGet,
                    HostType = @"WebApi",
                    Id = Guid.NewGuid().ToString("D"),
                    Message = message,
                    MessageSource = methodName,
                    TransactionNumber = ""
                }
            };
        }

        #endregion

        #region Event log functions

        /// <summary>
        /// Extract a host monitor message from a customer board balance get request.
        /// </summary>
        /// <param name="argument">argument to parse.</param>
        /// <param name="methodName">The name of the calling method.</param>
        /// <param name="clientIp">(Optional) Client Ip address to use as the message source.</param>
        /// <returns>Host monitor message or null if not found or cant be created.</returns>
        private static List<HostMonitorMessage> _eventLogGetHostMonitorMessageExtract(object argument, string methodName, string clientIp = null)
        {
            var item = Formatting.ExtractObject<EventLogGetResponse>(argument) as EventLogGetResponse;
            if (item == null)
            {
                return new List<HostMonitorMessage>();
            }

            var message = $"Event log '{item.EventLogId}' requested for {item.EventLog.ComputerName}.";

            return new List<HostMonitorMessage>
            {
                new HostMonitorMessage
                {
                    Amount = "",
                    CardNumber = "",
                    DateTimeOffset = DateTimeOffset.Now,
                    DeviceIdentifier = clientIp,
                    Function = HostMonitorSubscriptionServiceFunctions.EventLogGet,
                    HostType = @"WebApi",
                    Id = Guid.NewGuid().ToString("D"),
                    Message = message,
                    MessageSource = methodName,
                    TransactionNumber = ""
                }
            };
        }

        /// <summary>
        /// Extract a host monitor message from a customer board balance get request.
        /// </summary>
        /// <param name="argument">argument to parse.</param>
        /// <param name="methodName">The name of the calling method.</param>
        /// <param name="clientIp">(Optional) Client Ip address to use as the message source.</param>
        /// <returns>Host monitor message or null if not found or cant be created.</returns>
        private static List<HostMonitorMessage> _eventLogSummaryGetHostMonitorMessageExtract(object argument, string methodName, string clientIp = null)
        {
            var item = Formatting.ExtractObject<EventLogGetSummaryRequest>(argument) as EventLogGetSummaryRequest;
            if (item == null)
            {
                return new List<HostMonitorMessage>();
            }

            var dateFrom = item.CreatedFromDate != null ? $" from {item.CreatedFromDate.Value.ToLongDateString()}" : "";
            var dateUntil = item.CreatedUntilDate != null ? $" until {item.CreatedUntilDate.Value.ToLongDateString()}" : "";
            var severity = item.Severity != null ? $" with severity level of '{item.Severity}'" : "";
            var message = $"Event log summary requested for {item.ComputerName}{dateFrom}{dateUntil}{severity}.";

            return new List<HostMonitorMessage>
            {
                new HostMonitorMessage
                {
                    Amount = "",
                    CardNumber = "",
                    DateTimeOffset = DateTimeOffset.Now,
                    DeviceIdentifier = clientIp,
                    Function = HostMonitorSubscriptionServiceFunctions.EventLogGetSummary,
                    HostType = @"WebApi",
                    Id = Guid.NewGuid().ToString("D"),
                    Message = message,
                    MessageSource = methodName,
                    TransactionNumber = ""
                }
            };
        }

        /// <summary>
        /// Extract a host monitor message from a customer board balance get request.
        /// </summary>
        /// <param name="argument">argument to parse.</param>
        /// <param name="methodName">The name of the calling method.</param>
        /// <param name="clientIp">(Optional) Client Ip address to use as the message source.</param>
        /// <returns>Host monitor message or null if not found or cant be created.</returns>
        private static List<HostMonitorMessage> _eventLogSetHostMonitorMessageExtract(object argument, string methodName, string clientIp = null)
        {
            var item = Formatting.ExtractObject<EventLogPostRequest>(argument) as EventLogPostRequest;
            if (item == null)
            {
                return new List<HostMonitorMessage>();
            }


            var message = $"Event log posted for computer '{item.ComputerName}' under provider name '{item.ProviderName}'.";

            return new List<HostMonitorMessage>
            {
                new HostMonitorMessage
                {
                    Amount = "",
                    CardNumber = "",
                    DateTimeOffset = DateTimeOffset.Now,
                    DeviceIdentifier = clientIp,
                    Function = HostMonitorSubscriptionServiceFunctions.EventLogSet,
                    HostType = @"WebApi",
                    Id = Guid.NewGuid().ToString("D"),
                    Message = message,
                    MessageSource = methodName,
                    TransactionNumber = ""
                }
            };
        }

        #endregion

        #region General extraction function

        /// <summary>
        /// Extract a host monitor message from a device registration create request.
        /// </summary>
        /// <param name="methodName">The name of the calling method.</param>
        /// <param name="clientIp">(Optional) Client Ip address to use as the message source.</param>
        /// <returns>Host monitor message or null if not found or cant be created.</returns>
        private static HostMonitorMessage _generalMonitorMessageCreate(string methodName, string clientIp = null)
        {
            return new HostMonitorMessage
            {
                Amount = "",
                CardNumber = "",
                DateTimeOffset = DateTimeOffset.Now,
                DeviceIdentifier = clientIp,
                Function = HostMonitorSubscriptionServiceFunctions.Unspecified,
                HostType = @"WebApi",
                Id = Guid.NewGuid().ToString("D"),
                Message = $"Function '{methodName}' was invoked.",
                MessageSource = methodName,
                TransactionNumber = ""
            };
        }

        #endregion

        #region Operator extraction functions

        /// <summary>
        /// Extract a host monitor message from a customer board balance get request.
        /// </summary>
        /// <param name="argument">argument to parse.</param>
        /// <param name="methodName">The name of the calling method.</param>
        /// <param name="clientIp">(Optional) Client Ip address to use as the message source.</param>
        /// <returns>Host monitor message or null if not found or cant be created.</returns>
        private static List<HostMonitorMessage> _operatorSessionBeginHostMonitorMessageExtract(object argument, string methodName, string clientIp = null)
        {
            var item = Formatting.ExtractObject<OperatorSessionBeginRequest>(argument) as OperatorSessionBeginRequest;
            if (item == null)
            {
                return new List<HostMonitorMessage>();
            }


            var message = $"Operator '{item.Transaction.OperatorElement.OperatorGuid:D}' " +
                          $"started a session on device '{item.Transaction.OriginatorElement.OriginatorId:D}' " +
                          $"with session id '{item.Transaction.ControlElement.SessionId:D}'";

            return new List<HostMonitorMessage>
            {
                new HostMonitorMessage
                {
                    Amount = "",
                    CardNumber = "",
                    DateTimeOffset = DateTimeOffset.Now,
                    DeviceIdentifier = clientIp ?? item.Transaction.OriginatorElement.OriginatorId.ToString("D"),
                    Function = HostMonitorSubscriptionServiceFunctions.OperatorSessionBegin,
                    HostType = @"WebApi",
                    Id = Guid.NewGuid().ToString("D"),
                    Message = message,
                    MessageSource = methodName,
                    TransactionNumber = ""
                }
            };
        }

        /// <summary>
        /// Extract a host monitor message from a customer board balance get request.
        /// </summary>
        /// <param name="argument">argument to parse.</param>
        /// <param name="methodName">The name of the calling method.</param>
        /// <param name="clientIp">(Optional) Client Ip address to use as the message source.</param>
        /// <returns>Host monitor message or null if not found or cant be created.</returns>
        private static List<HostMonitorMessage> _operatorSessionEndHostMonitorMessageExtract(object argument, string methodName, string clientIp = null)
        {
            var item = Formatting.ExtractObject<OperatorSessionEndRequest>(argument) as OperatorSessionEndRequest;
            if (item == null)
            {
                return new List<HostMonitorMessage>();
            }


            var message = $"Operator '{item.Transaction.OperatorElement.OperatorGuid:D}' " +
                          $"ended a session on device '{item.Transaction.OriginatorElement.OriginatorId:D}' " +
                          $"with session id '{item.Transaction.ControlElement.SessionId:D}'";

            return new List<HostMonitorMessage>
            {
                new HostMonitorMessage
                {
                    Amount = "",
                    CardNumber = "",
                    DateTimeOffset = DateTimeOffset.Now,
                    DeviceIdentifier = clientIp ?? item.Transaction.OriginatorElement.OriginatorId.ToString("D"),
                    Function = HostMonitorSubscriptionServiceFunctions.OperatorSessionEnd,
                    HostType = @"WebApi",
                    Id = Guid.NewGuid().ToString("D"),
                    Message = message,
                    MessageSource = methodName,
                    TransactionNumber = ""
                }
            };
        }

        /// <summary>
        /// Extract a host monitor message from a customer board balance get request.
        /// </summary>
        /// <param name="argument">argument to parse.</param>
        /// <param name="methodName">The name of the calling method.</param>
        /// <param name="clientIp">(Optional) Client Ip address to use as the message source.</param>
        /// <returns>Host monitor message or null if not found or cant be created.</returns>
        private static List<HostMonitorMessage> _operatorPinChangeRequestHostMonitorMessageExtract(object argument, string methodName, string clientIp = null)
        {
            var item = Formatting.ExtractObject<OperatorChangePinRequest>(argument) as OperatorChangePinRequest;
            if (item == null)
            {
                return new List<HostMonitorMessage>();
            }

            var message = $"Operator '{item.OperatorId}' has requested a pin change.";

            return new List<HostMonitorMessage>
            {
                new HostMonitorMessage
                {
                    Amount = "",
                    CardNumber = "",
                    DateTimeOffset = DateTimeOffset.Now,
                    DeviceIdentifier = clientIp,
                    Function = HostMonitorSubscriptionServiceFunctions.OperatorPinChange,
                    HostType = @"WebApi",
                    Id = Guid.NewGuid().ToString("D"),
                    Message = message,
                    MessageSource = methodName,
                    TransactionNumber = ""
                }
            };
        }

        /// <summary>
        /// Extract a host monitor message from a customer board balance get request.
        /// </summary>
        /// <param name="argument">argument to parse.</param>
        /// <param name="methodName">The name of the calling method.</param>
        /// <param name="clientIp">(Optional) Client Ip address to use as the message source.</param>
        /// <returns>Host monitor message or null if not found or cant be created.</returns>
        private static List<HostMonitorMessage> _operatorValidateRequestHostMonitorMessageExtract(object argument, string methodName, string clientIp = null)
        {
            var item = Formatting.ExtractObject<OperatorValidationRequest>(argument) as OperatorValidationRequest;
            if (item == null)
            {
                return new List<HostMonitorMessage>();
            }

            var message = $"Operator validation request has been initiated on device '{item.DeviceId}'.";

            return new List<HostMonitorMessage>
            {
                new HostMonitorMessage
                {
                    Amount = "",
                    CardNumber = "",
                    DateTimeOffset = DateTimeOffset.Now,
                    DeviceIdentifier = clientIp ?? item.DeviceId,
                    Function = HostMonitorSubscriptionServiceFunctions.OperatorValidate,
                    HostType = @"WebApi",
                    Id = Guid.NewGuid().ToString("D"),
                    Message = message,
                    MessageSource = methodName,
                    TransactionNumber = ""
                }
            };
        }

        #endregion

        #region Report extraction functions

        /// <summary>
        /// Extract a host monitor message from a customer board balance get request.
        /// </summary>
        /// <param name="argument">argument to parse.</param>
        /// <param name="methodName">The name of the calling method.</param>
        /// <param name="clientIp">(Optional) Client Ip address to use as the message source.</param>
        /// <returns>Host monitor message or null if not found or cant be created.</returns>
        private static List<HostMonitorMessage> _drawerAuditGetHostMonitorMessageExtract(object argument, string methodName, string clientIp = null)
        {
            var item = Formatting.ExtractObject<DrawerAuditGetResponse>(argument) as DrawerAuditGetResponse;
            if (item == null)
            {
                return new List<HostMonitorMessage>();
            }

            var message = $"Drawer audit report has been requested for session '{item.SessionGuid}'.";

            return new List<HostMonitorMessage>
            {
                new HostMonitorMessage
                {
                    Amount = "",
                    CardNumber = "",
                    DateTimeOffset = DateTimeOffset.Now,
                    DeviceIdentifier = clientIp,
                    Function = HostMonitorSubscriptionServiceFunctions.DrawerAuditGet,
                    HostType = @"WebApi",
                    Id = Guid.NewGuid().ToString("D"),
                    Message = message,
                    MessageSource = methodName,
                    TransactionNumber = ""
                }
            };
        }

        /// <summary>
        /// Extract a host monitor message from a customer board balance get request.
        /// </summary>
        /// <param name="argument">argument to parse.</param>
        /// <param name="methodName">The name of the calling method.</param>
        /// <param name="clientIp">(Optional) Client Ip address to use as the message source.</param>
        /// <returns>Host monitor message or null if not found or cant be created.</returns>
        private static List<HostMonitorMessage> _posAuditGetHostMonitorMessageExtract(object argument, string methodName, string clientIp = null)
        {
            var item = Formatting.ExtractObject<PosAuditGetResponse>(argument) as PosAuditGetResponse;
            if (item == null)
            {
                return new List<HostMonitorMessage>();
            }

            var message = $"Pos audit report has been requested for originator '{item.OriginatorGuid}'.";

            return new List<HostMonitorMessage>
            {
                new HostMonitorMessage
                {
                    Amount = "",
                    CardNumber = "",
                    DateTimeOffset = DateTimeOffset.Now,
                    DeviceIdentifier = clientIp,
                    Function = HostMonitorSubscriptionServiceFunctions.PosAuditGet,
                    HostType = @"WebApi",
                    Id = Guid.NewGuid().ToString("D"),
                    Message = message,
                    MessageSource = methodName,
                    TransactionNumber = ""
                }
            };
        }

        /// <summary>
        /// Extract a host monitor message from a customer board balance get request.
        /// </summary>
        /// <param name="argument">argument to parse.</param>
        /// <param name="methodName">The name of the calling method.</param>
        /// <param name="clientIp">(Optional) Client Ip address to use as the message source.</param>
        /// <returns>Host monitor message or null if not found or cant be created.</returns>
        private static List<HostMonitorMessage> _profitCenterAuditGetHostMonitorMessageExtract(object argument, string methodName, string clientIp = null)
        {
            var item = Formatting.ExtractObject<ProfitCenterAuditGetResponse>(argument) as ProfitCenterAuditGetResponse;
            if (item == null)
            {
                return new List<HostMonitorMessage>();
            }

            var message = $"Profit Center audit report has been requested for Profit Center '{item.ProfitCenterGuid}'.";

            return new List<HostMonitorMessage>
            {
                new HostMonitorMessage
                {
                    Amount = "",
                    CardNumber = "",
                    DateTimeOffset = DateTimeOffset.Now,
                    DeviceIdentifier = clientIp,
                    Function = HostMonitorSubscriptionServiceFunctions.ProfitCenterAuditGet,
                    HostType = @"WebApi",
                    Id = Guid.NewGuid().ToString("D"),
                    Message = message,
                    MessageSource = methodName,
                    TransactionNumber = ""
                }
            };
        }

        #endregion

        #region Transaction extraction functions

        /// <summary>
        /// Extract a host monitor message from a transaction containing an arts transaction element.
        /// </summary>
        /// <param name="argument">argument to parse.</param>
        /// <param name="methodName">The name of the calling method.</param>
        /// <param name="clientIp">(Optional) Client Ip address to use as the message source.</param>
        /// <returns>Host monitor message or null if not found or cant be created.</returns>
        private static List<HostMonitorMessage> _transactionHostMonitorMessageExtract(object argument, string methodName, string clientIp = null)
        {
            var list = new List<HostMonitorMessage>();

            // Extract transaction objects
            Transaction transaction = null;

            //Version 2
            var transactionViewV02 = Formatting.ExtractObject<TransactionViewV02>(argument) as TransactionViewV02;
            if (transactionViewV02 != null)
            {
                transaction = transactionViewV02.ToTransaction();
            }

            //Version 1
            if (transaction == null)
            {
                var transactionViewV01 = Formatting.ExtractObject<TransactionViewV01>(argument) as TransactionViewV01;
                if (transactionViewV01 != null)
                {
                    transaction = transactionViewV01.ToTransaction();
                }
            }

            if (transaction == null)
            {
                return list;
            }

            var startTimestamp = transaction.TimestampExtract(TimestampType.Start);
            if (startTimestamp == null)
            {
                return list;
            }

            var lineItemTenders = transaction.LineItemTendersExtract();
            foreach (var tender in lineItemTenders)
            {
                var tenderType = tender.TenderTypeGet();
                var amount = tender.TenderAmount.ToString(CultureInfo.InvariantCulture);
                var cardNumber = string.Empty;
                var text = $"TransactionPost of type {tenderType} for {tender.TenderAmount.ToString("C", CultureInfo.CurrentCulture)}";

                list.Add(new HostMonitorMessage
                {
                    Amount = amount,
                    CardNumber = cardNumber,
                    DateTimeOffset = startTimestamp.Timestamp,
                    DeviceIdentifier = clientIp ?? transaction.OriginatorId.ToString("D"),
                    Function = HostMonitorSubscriptionServiceFunctions.TransactionPost,
                    HostType = @"WebApi",
                    Id = Guid.NewGuid().ToString("D"),
                    Message = text,
                    MessageSource = methodName,
                    TransactionNumber = transaction.Id.ToString("D")
                });
            }

            var boardElements = transaction.BoardElementsExtract();
            if (boardElements != null)
            {
                foreach (var boardElement in boardElements)
                {
                    var amount = boardElement.CountUsed.ToString();
                    var cardNumber = boardElement.CustomerCredential.CustomerCardInfo.CardNumber;
                    var text = $"TransactionPost of type Board for {amount} counts posted for customer {cardNumber}.";

                    list.Add(new HostMonitorMessage
                    {
                        Amount = amount,
                        CardNumber = boardElement.CustomerCredential.CustomerCardInfo.CardNumber,
                        DateTimeOffset = startTimestamp.Timestamp,
                        DeviceIdentifier = clientIp ?? transaction.OriginatorId.ToString("D"),
                        Function = HostMonitorSubscriptionServiceFunctions.TransactionPost,
                        HostType = @"WebApi",
                        Id = Guid.NewGuid().ToString("D"),
                        Message = text,
                        MessageSource = methodName,
                        TransactionNumber = transaction.Id.ToString("D")
                    });
                }
            }

            var attendanceElements = transaction.AttendanceElementsExtract();
            if (attendanceElements != null)
            {
                foreach (var attendanceElement in attendanceElements)
                {
                    var cardNumber = attendanceElement.CustomerCredential != null ? attendanceElement.CustomerCredential.CustomerCardInfo.CardNumber : string.Empty;
                    var text = $"TransactionPost of type Attendance posted for customer {cardNumber}.";

                    list.Add(new HostMonitorMessage
                    {
                        Amount = "",
                        CardNumber = attendanceElement.CustomerCredential != null ? attendanceElement.CustomerCredential.CustomerCardInfo.CardNumber : string.Empty,
                        DateTimeOffset = startTimestamp.Timestamp,
                        DeviceIdentifier = clientIp ?? transaction.OriginatorId.ToString("D"),
                        Function = HostMonitorSubscriptionServiceFunctions.TransactionPost,
                        HostType = @"WebApi",
                        Id = Guid.NewGuid().ToString("D"),
                        Message = text,
                        MessageSource = methodName,
                        TransactionNumber = transaction.Id.ToString("D")
                    });
                }
            }

            return list;
        }

        #endregion

        #endregion
    }
}
