﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using BbTS.Core.Conversion;
using BbTS.Core.Serialization;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Domain.Models.Definitions.Notification;
using BbTS.Domain.Models.Exceptions.Notification;
using BbTS.Domain.Models.Notification;
using BbTS.Monitoring.Logging;

namespace BbTS.Core.Messaging.Notification
{
    /// <summary>
    /// Utility class for processing information in a notification object.
    /// </summary>
    public static class NotificationUtility
    {
        /// <summary>
        /// Get all notification customer objects in a notificaitons get response.
        /// </summary>
        /// <param name="response">The response object from the database layer.</param>
        /// <param name="type">Type of notifications to get.</param>
        /// <returns>List of customers or empty if none found.</returns>
        public static List<NotificationObject> NotificationOfTypeGetAll(NotificationGetResponse response, NotificationObjectType type)
        {
            return response.Notifications.FindAll(n => n.ObjectType == type);
        }

        /// <summary>
        /// Map the information in a <see cref="NotificationAccessDatabase"/> object to a <see cref="EventHubNotificationAccess"/> object.
        /// </summary>
        /// <param name="destination">The destination object.</param>
        /// <param name="source">The source object.</param>
        public static void NotificationAccessToEventHubObjectMap(ref EventHubNotificationAccess destination, NotificationAccessDatabase source)
        {
            destination.AccessId = source.AccessId;
            destination.AccessName = source.AccessName;
            destination.StartDate = string.IsNullOrWhiteSpace(source.StartDate) ? DateTimeOffset.MaxValue : DateTimeOffset.Parse(source.StartDate);
            destination.EndDate = string.IsNullOrWhiteSpace(source.EndDate) ? DateTimeOffset.MaxValue : DateTimeOffset.Parse(source.EndDate);
            destination.CustomerGuid = source.CustomerGuid;
            destination.CustomerNumber = source.CustomerNumber;
        }

        /// <summary>
        /// Map the information in a <see cref="NotificationCardDatabase"/> object to a <see cref="EventHubNotificationCard"/> object.
        /// </summary>
        /// <param name="destination">The destination object.</param>
        /// <param name="source">The source object.</param>
        public static void NotificationCardToEventHubObjectMap(ref EventHubNotificationCard destination, NotificationCardDatabase source)
        {
            destination.CardNumber = source.CardNumber;
            destination.Comment = source.Comment;
            destination.IsPrimary = source.IsPrimary == 1;
            destination.Status = ((NotificationCardStatus)source.Status).ToString();
            destination.Type = ((NotificationCardType)source.Type).ToString();
            destination.ActiveEndDate = string.IsNullOrWhiteSpace(source.ActiveEndDate) ? DateTimeOffset.MaxValue : DateTimeOffset.Parse(source.ActiveEndDate);
            destination.ActiveStartDate = string.IsNullOrWhiteSpace(source.ActiveStartDate) ? DateTimeOffset.MaxValue : DateTimeOffset.Parse(source.ActiveStartDate);
            destination.CardGuid = source.CardGuid;
            destination.CustomerGuid = source.CustomerGuid;
            destination.CustomerNumber = source.CustomerNumber;
            destination.CardName = source.CardName;
            destination.CardSerialNumber = source.CardSerialNumber;
        }

        /// <summary>
        /// Map the information in a <see cref="NotificationCustomerDatabase"/> object to a <see cref="EventHubNotificationCustomer"/> object.
        /// </summary>
        /// <param name="destination">The destination object.</param>
        /// <param name="source">The source object.</param>
        /// <param name="baseObject">The notification base object.</param>
        public static void NotificationCustomerToEventHubObjectMap(ref EventHubNotificationCustomer destination, NotificationCustomerDatabase source, NotificationObject baseObject)
        {
            destination.EndDate = string.IsNullOrWhiteSpace(source.EndDate) ? DateTimeOffset.MaxValue : DateTimeOffset.Parse(source.EndDate);
            destination.FirstName = source.FirstName;
            destination.IsActive = Formatting.TfStringToBool(source.IsActive);
            destination.LastName = source.LastName;
            destination.MiddleName = source.MiddleName;
            destination.MobileIdCardType = source.MobileIdCardType;
            destination.MobileIdExpireDate = source.MobileIdExpireDate;
            destination.MobileIdIINPool = source.MobileIdIINPool;
            destination.MobileIdIsFree = Formatting.TfStringToBool(source.MobileIdIsFree);
            destination.MobileIdPrice = source.MobileIdPrice;
            destination.MobileIdProcurementAllowed = Formatting.TfStringToBool(source.MobileIdProcurementAllowed);
            destination.MobileIdRefText = source.MobileIdRefText;
            destination.MobilePersonType = source.MobilePersonType;
            destination.MobileStudentClass = source.MobileStudentClass;
            destination.MobileStudentResidence = source.MobileStudentResidence;
            destination.MobileOrgLabel = source.MobileOrgLabel;
            destination.MobileRole = source.MobileRole;
            destination.MobileOrg = source.MobileOrg;
            destination.StartDate = string.IsNullOrWhiteSpace(source.StartDate) ? DateTimeOffset.MinValue : DateTimeOffset.Parse(source.StartDate);
            destination.CustomerGuid = source.CustomerGuid ?? baseObject.ObjectGuid.ToString("D");
            destination.CustomerNumber = source.CustomerNumber;
        }

        /// <summary>
        /// Map the information in a <see cref="NotificationCustomerExtensionDatabase"/> object to a <see cref="EventHubNotificationCustomerExtension"/> object.
        /// </summary>
        /// <param name="destination">The destination object.</param>
        /// <param name="source">The source object.</param>
        public static void NotificationCustomerExtensionToEventHubObjectMap(ref EventHubNotificationCustomerExtension destination, NotificationCustomerExtensionDatabase source)
        {
            destination.CustomerGuid = source.CustomerGuid;
            destination.CustomerNumber = source.CustomerNumber;
            destination.Category = source.Category;
            foreach (var item in source.Extensions)
            {
                destination.Extensions.Add(item.Name, CustomerExtensionsValueToObject(item));
            }
        }

        /// <summary>
        /// Parse out an extension value to an object based on type.
        /// </summary>
        /// <param name="item">Extension item.</param>
        /// <returns></returns>
        public static object CustomerExtensionsValueToObject(Extension item)
        {
            int numericType;
            if(!int.TryParse(item.Type, out numericType))
            {
                throw new CustomerExtensionValueParseFailException($"Failed to parse numeric type from value: Name = '{item.Name}', Value = '{item.Value}', Type = '{item.Type}'");
            }
            var type = (CustomerExtensionValueTypes)numericType;
            switch (type)
            {
                case CustomerExtensionValueTypes.Boolean:
                {
                    bool value;
                    if (bool.TryParse(item.Value, out value)) return value;
                    throw new CustomerExtensionValueParseFailException($"Extension item was flagged as {type} but value does not parse: Value = {item.Value}");

                }
                case CustomerExtensionValueTypes.Decimal:
                {
                    decimal value;
                    if (decimal.TryParse(item.Value, out value)) return value;
                    throw new CustomerExtensionValueParseFailException($"Extension item was flagged as {type} but value does not parse: Value = {item.Value}");

                }
                case CustomerExtensionValueTypes.Numeric:
                {
                    long value;
                    if (long.TryParse(item.Value, out value)) return value;
                    throw new CustomerExtensionValueParseFailException($"Extension item was flagged as {type} but value does not parse: Value = {item.Value}");
                }
                case CustomerExtensionValueTypes.String:
                {
                    return item.Value;
                }
                default:
                    throw new CustomerExtensionValueParseFailException($"Extension item was flagged as {type} but value does not parse: Value = {item.Value}");
            }
        }

        /// <summary>
        /// Create an <see cref="EventHubNotificationCustomerImage"/> from information about the customer.
        /// </summary>
        /// <param name="obj">Information about the customer.</param>
        /// <returns></returns>
        public static EventHubNotificationCustomerImage EventHubNotificationCustomerImageCreate(NotificationObject obj)
        {
            var response = new EventHubNotificationCustomerImage
            {
                ImageId = $"{obj.ObjectGuid}",
                CustomerGuid = obj.ObjectGuid.ToString("D"),
                CustomerNumber = obj.CustomerNumber,
                ExternalIds = !string.IsNullOrWhiteSpace(obj.ExternalClientIds) ? Xml.Deserialize<List<string>>(obj.ExternalClientIds) : new List<string>()
            };

            return response;
        }

        /// <summary>
        /// Map the information in a <see cref="NotificationCustomerPinHashDatabase"/> object to a <see cref="EventHubNotificationCustomerPinHash"/> object.
        /// </summary>
        /// <param name="destination">The destination object.</param>
        /// <param name="source">The source object.</param>
        public static void NotificationCustomerPinHashToEventHubObjectMap(ref EventHubNotificationCustomerPinHash destination, NotificationCustomerPinHashDatabase source)
        {
            destination.AlgorithmIdentifier = source.AlgorithmIdentifier;
            destination.Pin = source.Pin;
            destination.CustomerGuid = source.CustomerGuid;
            destination.CustomerNumber = source.CustomerNumber;
        }

        /// <summary>
        /// Map the information in a <see cref="NotificationStoredValueTransactionDatabase"/> object to a <see cref="EventHubNotificationStoredValueTransaction"/> object.
        /// </summary>
        /// <param name="destination">The destination object.</param>
        /// <param name="source">The source object.</param>
        public static void NotificationStoredValueToEventHubObjectMap(ref EventHubNotificationStoredValueTransaction destination, NotificationStoredValueTransactionDatabase source)
        {
            double julianDate;
            double.TryParse(source.JulianDateTime, out julianDate);
            var dateTime = DateTime.FromOADate(julianDate);

            destination.Amount = source.Amount;
            destination.DateTime = dateTime;
            destination.Balance = source.Balance;
            destination.ProfitCenterName = source.ProfitCenterName;
            destination.CardNumber = source.CardNumber;
            destination.SvAccountNumber = source.SvAccountNumber;
            destination.SvAccountTypeName = source.SvAccountTypeName;
            destination.PosName = source.PosName;
            destination.MerchantName = source.MerchantName;
            destination.SvAccountHistoryId = source.SvAccountHistoryId;
            destination.TransactionGuid = source.TransactionGuid;
            destination.TransactionType = source.TransactionType;
            destination.TransactionStatus = source.TransactionStatus;
            destination.CurrencyCode = source.CurrencyCode;
            destination.CustomerGuid = source.CustomerGuid;
            destination.CustomerNumber = source.CustomerNumber;
            destination.SvAccountTypeGuid = source.SvAccountTypeGuid;
            destination.PosGuid = source.PosGuid;
            destination.ProfitCenterGuid = source.ProfitCenterGuid;
            destination.MerchantGuid = source.MerchantGuid;
        }

        /// <summary>
        /// Map the information in a list of <see cref="NotificationDatabaseReportingMetrics"/> objects to a <see cref="EventHubNotificationReportingMetrics"/> object.
        /// </summary>
        /// <param name="destination">The destination object.</param>
        /// <param name="source">The source object.</param>
        public static void NotificationMetricObjectMap(ref EventHubNotificationReportingMetrics destination, NotificationDatabaseReportingMetrics source)
        {
            destination.Metrics.AddRange(
                source.Metrics.Select(item =>
                    new EventHubNotificationReportingMetric
                    {
                        DataKey = item.DataKey,
                        Currency = item.Currency,
                        Date = DateTime.Parse(source.Date),
                        TransactionCount = item.TransactionCount,
                        TransactionValue = item.TransactionValue,
                        UserCount = item.UserCount,
                        DpanCount = item.DpanCount,
                        DeviceCount = item.DeviceCount,
                        Frequency = ((ReportingMetricsFrequency)item.Frequency).ToString()
                    }).ToList());
        }

        /// <summary>
        /// Map the information in a <see cref="NotificationCustomerDatabase"/> object to a <see cref="EventHubNotificationCustomer"/> object.
        /// </summary>
        /// <param name="destination">The destination object.</param>
        /// <param name="source">The source object.</param>
        public static void NotificationHeartbeatToEventHubObjectMap(ref EventHubNotificationHeartBeat destination, NotificationHeartBeatDatabase source)
        {
            destination.QueueDepth = source.QueueDepth;
            destination.RecordDateTimeMinimum =
                string.IsNullOrWhiteSpace(source.RecordDateTimeMinimum) ?
                    DateTimeOffset.MinValue :
                    DateTimeOffset.Parse(source.RecordDateTimeMinimum);

            destination.RecordDateTimeMaximum =
                string.IsNullOrWhiteSpace(source.RecordDateTimeMaximum) ?
                    DateTimeOffset.MinValue :
                    DateTimeOffset.Parse(source.RecordDateTimeMaximum);
        }

        /// <summary>
        /// Get the notification status type from the notification processing state.
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        public static NotificationStatusType NotificationStatusTypeGet(NotificationProcessingState state)
        {
            switch (state)
            {
                case NotificationProcessingState.Complete:
                    return NotificationStatusType.Complete;
                case NotificationProcessingState.Error:
                    return NotificationStatusType.Error;

                default:
                    return NotificationStatusType.Pending;
            }
        }

        /// <summary>
        /// Generate a report based off of the response from a notification process run.
        /// </summary>
        /// <param name="response">The response.</param>
        /// <param name="startTime">The time the notification processing run was started.</param>
        /// <param name="endTime">The time the notification processing run was completed.</param>
        /// <returns>The report (as a string)</returns>
        public static NotificationProcessingReport GenerateReport(NotificationsProcessResponse response, DateTimeOffset startTime, DateTimeOffset endTime)
        {
            var report = new NotificationProcessingReport(startTime, endTime) { MaxThreadsUsed = response.MaxThreadsUsed };
            CountObjects(response, ref report);

            response.ChildResponses.ToList().ForEach(x =>
            {
                if (x.ProcessingState == NotificationProcessingState.Complete)
                    report.SuccessCount++;
                else
                    report.FailedCount++;
            });

            return report;
        }


        /// <summary>
        /// Count the number of response objects (including child response objects).
        /// </summary>
        /// <param name="response">The reponse object.</param>
        /// <param name="report">The report to add to.</param>
        /// <returns>The count.</returns>
        public static void CountObjects(NotificationsProcessResponse response, ref NotificationProcessingReport report)
        {
            foreach (var child in response.ChildResponses)
            {
                if (child.ChildResponses.Count != 0)
                {
                    CountObjects(child, ref report);
                }
                report.IncrementObjectTypeCount(child.ObjectType);
            }
        }
    }
}
