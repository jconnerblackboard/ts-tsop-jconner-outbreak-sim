﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using BbTS.Core.Configuration;
using BbTS.Core.Conversion;
using BbTS.Core.Serialization;
using BbTS.Domain.Models.Container;
using BbTS.Domain.Models.Definitions.Logging;
using BbTS.Monitoring.Logging;
using PostSharp.Aspects;

namespace BbTS.Core.Messaging.Msmq
{
    /// <summary> 
    /// Aspect that, when applied on a method, emits a trace message before and 
    /// after the method execution. 
    /// </summary> 
    [Serializable]
    public class MsmqAttribute : OnMethodBoundaryAspect
    {
        private string _methodName;

        /// <summary> 
        /// Method executed at build time. Initializes the aspect instance. After the execution 
        /// of <see cref="CompileTimeInitialize"/>, the aspect is serialized as a managed  
        /// resource inside the transformed assembly, and deserialized at runtime. 
        /// </summary> 
        /// <param name="method">Method to which the current aspect instance  
        /// has been applied.</param> 
        /// <param name="aspectInfo">Unused.</param> 
        public override void CompileTimeInitialize(MethodBase method, AspectInfo aspectInfo)
        {
            if (method.DeclaringType != null) _methodName = method.DeclaringType.FullName + "." + method.Name;
        }

        /// <summary> 
        /// Method invoked before the execution of the method to which the current 
        /// aspect is applied. 
        /// </summary> 
        /// <param name="args">Unused.</param> 
        public override void OnEntry(MethodExecutionArgs args)
        {
            try
            {
                var parameters = new List<NameValueTree>();
                for (int i = 0; i < args.Arguments.Count; i++)
                {
                    var argument = args.Arguments.GetArgument(i);
                    parameters.Add(NameValueTreeUtility.Parse(argument, argument.ToString()));
                }

                var queueName = ApplicationConfiguration.GetKeyValueAsString("MsmqQueueName", $".\\Private$\\BbTSWebApi");
                var queueExpiraryTimeMinutes = ApplicationConfiguration.GetKeyValueAsInt("MsmqExpiraryTimeMinutes", 60);

                MsmqUtility.WriteMessage(
                    parameters,
                    queueName,
                    _methodName,
                    new TimeSpan(0, 0, queueExpiraryTimeMinutes, 0),
                    new JsonMessageFormatter<List<NameValueTree>>());
            }
            catch (Exception ex)
            {
                LoggingManager.Instance.LogException(
                    ex, "", "", EventLogEntryType.Error,
                    (short)LoggingDefinitions.Category.WebApi,
                    (int)LoggingDefinitions.EventId.MsmqAttributeException);
            }
            
        }

        /// <summary> 
        /// Method invoked after successful execution of the method to which the current 
        /// aspect is applied. 
        /// </summary> 
        /// <param name="args">Unused.</param> 
        public override void OnSuccess(MethodExecutionArgs args)
        {
        }

        /// <summary> 
        /// Method invoked after failure of the method to which the current 
        /// aspect is applied. 
        /// </summary> 
        /// <param name="args">Unused.</param> 
        public override void OnException(MethodExecutionArgs args)
        {
        }
    }
}
