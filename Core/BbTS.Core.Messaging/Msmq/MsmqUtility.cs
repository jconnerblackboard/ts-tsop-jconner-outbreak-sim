﻿using System;
using System.Collections.Generic;
using System.Messaging;
using BbTS.Core.Conversion;

namespace BbTS.Core.Messaging.Msmq
{
    /// <summary>
    /// Utility class to handle reading from and writing to an MSMQ bus.
    /// </summary>
    public class MsmqUtility
    {
        /// <summary>
        /// Write a message to the MSMQ bus.
        /// </summary>
        /// <param name="message">Object to place on the bus.</param>
        /// <param name="queue">The queue to place the object into.</param>
        /// <param name="label">The label to be placed on the message</param>
        /// <param name="expiration">Length of time to hold the message in queue.</param>
        /// <param name="formatter">Body formatter to use.  If null is passed <see cref="XmlMessageFormatter"/> is used.</param>
        public static void WriteMessage(
            object message,
            string queue,
            string label,
            TimeSpan expiration,
            IMessageFormatter formatter = null)
        {
            CreateQueue(queue);
            var msg = new Message
            {
                Body = message,
                TimeToBeReceived = expiration,
                Label = label,
                Formatter = formatter ?? new XmlMessageFormatter() 
            };
            var msgQ = new MessageQueue(queue);
            msgQ.Send(msg);
        }

        /// <summary>
        /// Read all messages of type T from the specified queue.
        /// </summary>
        /// <typeparam name="T">Type to read from the queue.</typeparam>
        /// <param name="queue">Queue to read from.</param>
        /// <returns></returns>
        public static List<T> ReadMessageQueueAsXml<T>(string queue)
        {
            // Create an empty list of type T.
            List<T> messageList = new List<T>();

            if (QueueExists(queue))
            {
            // Set up the queue.
            var msgQ = new MessageQueue(queue);
            var arrTypes = new[] { typeof (T) };
            msgQ.Formatter = new XmlMessageFormatter(arrTypes);

            // Get all the messages from the queue.
            var messages = msgQ.GetAllMessages();

            // Parse the messages and add them to the return list.
            foreach (var message in messages)
            {
                var t = (T)message.Body;
                if (t != null)
                {
                    messageList.Add(t);
                }
                }
            }

            // Returned the parsed list.
            return messageList;
        }

        /// <summary>
        /// Read all messages of type T from the specified queue.
        /// </summary>
        /// <typeparam name="T">Type to read from the queue.</typeparam>
        /// <param name="queue">Queue to read from.</param>
        /// <returns></returns>
        public static List<T> ReadMessageQueueAsJson<T>(string queue)
        {
            // Create an empty list of type T.
            List<T> messageList = new List<T>();

            if (QueueExists(queue))
            {
                // Set up the queue.
                var msgQ = new MessageQueue(queue) { Formatter = new JsonMessageFormatter<T>() };

                // Get all the messages from the queue.
                var messages = msgQ.GetAllMessages();

                // Parse the messages and add them to the return list.
                foreach (var message in messages)
                {
                    var t = (T)message.Body;
                    if (t != null)
                    {
                        messageList.Add(t);
                    }
                }
            }

            // Returned the parsed list.
            return messageList;
        }

        /// <summary>
        /// Create a MSMQ queue.
        /// </summary>
        /// <param name="queue">Queue to create.</param>
        public static void CreateQueue(string queue)
        {
            if (!MessageQueue.Exists(queue))
            {
                MessageQueue.Create(queue);
            }
        }

        /// <summary>
        /// Check whether or not a queue has bee created or not.
        /// </summary>
        /// <param name="queue">Queue to check.</param>
        /// <param name="create">Create if it hasn't exist.</param>
        /// <returns></returns>
        public static bool QueueExists(string queue, bool create = false)
        {
            if (create)
            {
                CreateQueue(queue);
            }
            return MessageQueue.Exists(queue);
        }

        /// <summary>
        /// Flush all messages from the queue.
        /// </summary>
        /// <param name="queue">Queue to flush.</param>
        public static void FlushQueue(string queue)
        {
            if (QueueExists(queue))
            {
                // Set up the queue.
                var msgQ = new MessageQueue(queue);
                msgQ.Purge();
            }
        }
    }
}
