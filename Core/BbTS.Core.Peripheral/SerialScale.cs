﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;

namespace BbTS.Core.Peripheral
{
    /// <summary>
    /// Basic class that allows interaction with a serial scale (such as NCI Weightronix or Mettler Toledo)
    /// </summary>
    public class SerialScale
    {
        private static readonly byte[] EcrWeightCommand = { 0x57, 0x0D };  //W<CR>
        private static readonly byte[] EcrScaleStatusCommand = { 0x53, 0x0D };  //S<CR>
        private static readonly byte[] EcrZeroCommand = { 0x5A, 0x0D };  //Z<CR>

        private readonly byte[] _ecrWeightPattern = { 0x0A, 0x00, 0x00, 0x00, 0x2E, 0x00, 0x00, 0x00, 0x00, 0x0D, 0x0A, 0x53, 0x00, 0x00, 0x0D, 0x03 };
        private readonly byte[] _ecrScaleStatusPattern = { 0x0A, 0x53, 0x00, 0x00, 0x0D, 0x03 };

        private static readonly byte[] NciWeightCommand = EcrWeightCommand;
        private static readonly byte[] NciScaleStatusCommand = EcrScaleStatusCommand;
        private static readonly byte[] NciZeroCommand = EcrZeroCommand;


        private readonly SerialPort _serialPort;
        private readonly ScaleType _scaleType;
        private readonly ScaleProtocol _scaleProtocol;
        private string _buffer;
        private string _rxData;
        private readonly Stopwatch _stopwatch;

        private string GetResponseFromBuffer()
        {
            int i = _buffer.IndexOf(Convert.ToChar(0x03));
            if (i < 0) return null;

            string s = _buffer.Substring(0, i + 1);
            _buffer = _buffer.Substring(i + 1);
            return s;
        }

        private void DataReceived(object sender, SerialDataReceivedEventArgs args)
        {
            _buffer += _serialPort.ReadExisting();
            _rxData = GetResponseFromBuffer();
        }

        private static bool MatchesPattern(string data, byte[] pattern)
        {
            var rxBytes = Encoding.ASCII.GetBytes(data);
            return !pattern.Where((m, i) => m != 0x00 && ((i >= data.Length) || (rxBytes[i] != m))).Any();

            //return !pattern.Where((m, i) => m != 0x00 && ((i >= data.Length) || (data[i] != Convert.ToChar(m)))).Any();

            //ReSharper turned the code below into the above, keeping the original code as a reference 
            //for (int i = 0; i < pattern.Length; i++)
            //{
            //    if (pattern[i] == 0x00) continue;
            //    if ((i < data.Length) && (data[i] == Convert.ToChar(pattern[i]))) continue;
            //    return false;
            //}
            //return true;
        }

        /// <summary>
        /// True if the serial port is open.
        /// </summary>
        public bool IsOpen => _serialPort.IsOpen;

        /// <summary>
        /// Constructor for a SerialScale instance.
        /// </summary>
        /// <param name="portNumber"></param>
        /// <param name="scaleType"></param>
        /// <param name="scaleProtocol"></param>
        public SerialScale(int portNumber, ScaleType scaleType, ScaleProtocol scaleProtocol)
        {
            _serialPort = new SerialPort($"Com{portNumber}", 9600, scaleType == ScaleType.Nci4000 ? Parity.None : Parity.Even, 7, StopBits.One)
            {
                Encoding = Encoding.ASCII,
                NewLine = Environment.NewLine,
                ReadTimeout = 500,
                ReadBufferSize = 64,
                WriteBufferSize = 64,
                //DtrEnable = true,
                //RtsEnable = true
            };
            _serialPort.DataReceived += DataReceived;
            _scaleType = scaleType;
            _scaleProtocol = scaleProtocol;
            _stopwatch = new Stopwatch();
        }

        /// <summary>
        /// Opens the scale's serial port.
        /// </summary>
        public void Open()
        {
            _serialPort.Open();
        }

        /// <summary>
        /// Closes the scale's serial port.
        /// </summary>
        public void Close()
        {
            _serialPort.Close();
        }

        private void SendAndWait(byte[] txBuffer, int timeout)
        {
            _serialPort.Write(txBuffer, 0, txBuffer.Length);
            _stopwatch.Reset();

            //Receive
            while ((_rxData == null) && (_stopwatch.ElapsedMilliseconds < timeout))
            {
                Thread.Sleep(10);
            }

            if (string.IsNullOrEmpty(_rxData)) throw new TimeoutException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="weight"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        /// <exception cref="InvalidDataException"></exception>
        /// <exception cref="NotImplementedException"></exception>
        public ScaleStatus WeightCommand(out decimal weight)
        {
            //Send
            byte[] commandBuffer;
            switch (_scaleProtocol)
            {
                case ScaleProtocol.Ecr:
                    commandBuffer = EcrWeightCommand;
                    break;
                case ScaleProtocol.Nci:
                    commandBuffer = NciWeightCommand;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            SendAndWait(commandBuffer, 1000);

            //Decode response
            switch (_scaleProtocol)
            {
                case ScaleProtocol.Ecr:
                    //Success path (ScaleStatus.IsStatusOk should be true)
                    if (MatchesPattern(_rxData, _ecrWeightPattern))
                    {
                        string w = string.Empty;
                        for (int i = 1; i <= 6; i++)
                        {
                            w += _rxData[i];
                        }

                        weight = decimal.Parse(w);
                        return new ScaleStatus(Convert.ToByte(_rxData[12]), Convert.ToByte(_rxData[13]));
                    }

                    //Error path (ScaleStatus will specify error)
                    if (MatchesPattern(_rxData, _ecrScaleStatusPattern))
                    {
                        weight = 0.00m;
                        return new ScaleStatus(Convert.ToByte(_rxData[2]), Convert.ToByte(_rxData[3]));
                    }

                    //Unexpected 
                    throw new InvalidDataException("Unsupported command or invalid response");
                case ScaleProtocol.Nci:
                    throw new NotImplementedException();
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Request status from the scale.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        /// <exception cref="InvalidDataException"></exception>
        /// <exception cref="NotImplementedException"></exception>
        public ScaleStatus StatusCommand()
        {
            //Send
            byte[] commandBuffer;
            switch (_scaleProtocol)
            {
                case ScaleProtocol.Ecr:
                    commandBuffer = EcrScaleStatusCommand;
                    break;
                case ScaleProtocol.Nci:
                    commandBuffer = NciScaleStatusCommand;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            SendAndWait(commandBuffer, 1000);

            //Decode response
            switch (_scaleProtocol)
            {
                case ScaleProtocol.Ecr:
                    if (MatchesPattern(_rxData, _ecrScaleStatusPattern))
                    {
                        return new ScaleStatus(Convert.ToByte(_rxData[2]), Convert.ToByte(_rxData[3]));
                    }

                    //Unexpected 
                    throw new InvalidDataException("Unsupported command or invalid response");
                case ScaleProtocol.Nci:
                    throw new NotImplementedException();
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Zero the scale and receive status.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        /// <exception cref="InvalidDataException"></exception>
        /// <exception cref="NotImplementedException"></exception>
        public ScaleStatus ZeroCommand()
        {
            //Send
            byte[] commandBuffer;
            switch (_scaleProtocol)
            {
                case ScaleProtocol.Ecr:
                    commandBuffer = EcrZeroCommand;
                    break;
                case ScaleProtocol.Nci:
                    commandBuffer = NciZeroCommand;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            SendAndWait(commandBuffer, 1000);

            //Decode response
            switch (_scaleProtocol)
            {
                case ScaleProtocol.Ecr:
                    if (MatchesPattern(_rxData, _ecrScaleStatusPattern))
                    {
                        return new ScaleStatus(Convert.ToByte(_rxData[2]), Convert.ToByte(_rxData[3]));
                    }

                    //Unexpected 
                    throw new InvalidDataException("Unsupported command or invalid response");
                case ScaleProtocol.Nci:
                    throw new NotImplementedException();
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }

    /// <summary>
    /// Scale type.
    /// </summary>
    public enum ScaleType
    {
        /// <summary>
        /// NCI 4000 or equivalent.
        /// </summary>
        Nci4000,

        /// <summary>
        /// NCI 4000I or equivalent.
        /// </summary>
        Nci4000I,

        /// <summary>
        /// Toledo 8213 or equivalent.
        /// </summary>
        Toledo8213
    }

    /// <summary>
    /// Class containing scale status flags.
    /// </summary>
    public class ScaleStatus
    {
        private byte _byteOne;
        private byte _byteTwo;

        public bool IsStatusOk => (_byteOne == 0x00) && (_byteTwo == 0x00);

        public bool ScaleInMotion => (_byteOne & (1 << 0)) != 0;
        public bool ScaleAtZero => (_byteOne & (1 << 1)) != 0;
        public bool RamError => (_byteOne & (1 << 2)) != 0;
        public bool EepromError => (_byteOne & (1 << 3)) != 0;

        public bool UnderCapacity => (_byteTwo & (1 << 0)) != 0;
        public bool OverCapacity => (_byteTwo & (1 << 1)) != 0;
        public bool RomError => (_byteTwo & (1 << 2)) != 0;
        public bool FaultyCalibration => (_byteTwo & (1 << 3)) != 0;

        public ScaleStatus(byte byteOne, byte byteTwo)
        {
            _byteOne = byteOne;
            _byteTwo = byteTwo;
        }
    }

    public enum ScaleProtocol
    {
        /// <summary>
        /// ECR Serial Communication Protocol
        /// </summary>
        Ecr,

        /// <summary>
        /// NCI Serial Communication Protocol
        /// </summary>
        Nci
    }
}
