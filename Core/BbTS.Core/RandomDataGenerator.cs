﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace BbTS.Core
{
    /// <summary>
    ///     Responsible for generating random data
    /// </summary>
    public static class RandomDataGenerator
    {
        /// <summary>
        ///     Generates the random string.
        /// </summary>
        /// <param name="length">
        ///     The length.
        /// </param>
        /// <returns>
        ///     A random string of a specified length
        /// </returns>
        public static string GenerateRandomString(int length)
        {
            // http://blog.codeeffects.com/Article/Generate-Random-Numbers-And-Strings-C-Sharp
            // Create a pool of allowed characters.  These characters are xml friendly.
            var characterPool = new[]
                                    {
                                        "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
                                        "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
                                        "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"
                                    };

            var stringBuilder = new StringBuilder();
            for (var i = 0; i < length; i++)
            {
                // Each iteration of this loop adds a single random character from the pool.
                stringBuilder.Append(characterPool[GenerateRandomNumber(0, characterPool.Length - 1)]);
            }

            return stringBuilder.ToString();
        }

        /// <summary>
        ///     Generates the random number.
        /// </summary>
        /// <param name="lowerBound">
        ///     The lower bound.
        /// </param>
        /// <param name="upperBound">
        ///     The upper bound.
        /// </param>
        /// <returns>
        ///     A random number between the specified bounds
        /// </returns>
        public static int GenerateRandomNumber(int lowerBound, int upperBound)
        {
            if (upperBound == int.MaxValue)
            {
                throw new ArgumentOutOfRangeException("upperBound");
            }

            var randomBytes = new byte[4];
            using (var rngCryptoServiceProvider = new RNGCryptoServiceProvider())
            {
                // Generate a random sequence of bytes.
                rngCryptoServiceProvider.GetBytes(randomBytes);
            }

            // Create a unique seed on every call by converting the bits into an integer (overkill for a seed?).
            var seed = (randomBytes[0] & 0x7f) << 24 | randomBytes[1] << 16 | randomBytes[2] << 8 | randomBytes[3];

            // Apply the random seed to a random object which in turn randomly selects an integer in the specified range.
            var randomizer = new Random(seed);

            // The maxValue parameter value on the Next method is exclusive (meaning it is never returned).  
            // Increase the upperBound by 1 to allow the randomizer to use the upperBound value as an option to be returned.
            return randomizer.Next(lowerBound, upperBound + 1);
        }
    }
}
