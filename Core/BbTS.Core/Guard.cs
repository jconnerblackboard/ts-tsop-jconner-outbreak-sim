﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BbTS.Core
{
    /// <summary>
    ///     Guard class responsible for verifying parameter values
    /// </summary>
    /// <remarks>
    ///     This class may be removed and replaced with Code Contracts functionality described
    ///     <a href="http://research.microsoft.com/en-us/projects/contracts/">
    ///         here
    ///     </a>
    ///     (http://research.microsoft.com/en-us/projects/contracts/)
    /// </remarks>
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly",
    Scope = "type",
    Target = "BbFX.Core.Logging.EventLogProvider",
    Justification = "Words that are part of hyperlink should not be spell checked.")]
    public static class Guard
    {
        /// <summary>
        ///     Verifies that the derived type is assignable to the base type.
        /// </summary>
        /// <typeparam name="TBase">
        ///     The base type.
        /// </typeparam>
        /// <typeparam name="TDerived">
        ///     The derived type.
        /// </typeparam>
        /// <exception cref="InvalidCastException">
        ///     Thrown if the derived type is not assignable to the base type.
        /// </exception>
        [SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter",
            Justification = "The method uses the two type parameters to ensure compatibility.")]
        public static void IsAssignable<TBase, TDerived>()
        {
            if (!typeof(TBase).IsAssignableFrom(typeof(TDerived)))
            {
                throw new InvalidCastException(
                    string.Format(
                        CultureInfo.CurrentCulture,
                        "type {0} is not assignable from type {1}",
                        typeof(TBase),
                        typeof(TDerived)));
            }
        }

        /// <summary>
        ///     Determines whether value is not null.
        /// </summary>
        /// <param name="value">
        ///     The value.
        /// </param>
        /// <param name="name">
        ///     The parameter name.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        ///     Thrown if value is null
        /// </exception>
        public static void IsNotNull(object value, string name)
        {
            if (value == null)
            {
                throw new ArgumentNullException(
                    string.Format(CultureInfo.CurrentCulture, "parameter {0} is null.", name));
            }
        }

        /// <summary>
        ///     Determines whether the value is not null or empty.
        /// </summary>
        /// <param name="value">
        ///     The value.
        /// </param>
        /// <param name="name">
        ///     The parameter name.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        ///     Thrown if the value is null or empty
        /// </exception>
        public static void IsNotNullOrEmpty(string value, string name)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new ArgumentNullException(
                    string.Format(CultureInfo.CurrentCulture, "parameter {0} is null or empty.", name));
            }
        }

        /// <summary>
        /// Determines whether the value is null or whitespace.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="name">The name.</param>
        /// <exception cref="System.ArgumentNullException">
        ///     Thrown if the value is null
        /// </exception>
        /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the value is empty or whitespace</exception>
        public static void IsNotNullOrWhiteSpace(string value, string name)
        {
            IsNotNull(value, name);

            if (value.Trim().Length == 0)
            {
                throw new ArgumentOutOfRangeException(string.Format(CultureInfo.CurrentCulture, "parameter {0} is empty or whitespace.", name));
            }
        }

        

        /// <summary>
        /// Verify that the given guid is in a valid guid format.
        /// </summary>
        /// <param name="guid">The value to verify.</param>
        public static void IsGuid(string guid)
        {
            Guid value;
            if (!Guid.TryParse(guid, out value))
            {
                throw new ArgumentException($"parameter {guid} is not a valid Guid.", nameof(guid));
            }
        }
    }
}
