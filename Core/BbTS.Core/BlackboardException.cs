using System;

namespace BbTS.Core
{
    /// <summary>
    /// This class defines a generic Blackboard exception
    /// </summary>
    [Serializable]
    public class BlackboardException : Exception
    {
        /// <summary>
        /// This method instantiates a new Blackboard exception.
        /// </summary>
        /// <param name="message">The exception message</param>
        public BlackboardException(String message)
            : base(message)
        {
            //Do logic required of all BlackboardExceptions
        }
    }
}