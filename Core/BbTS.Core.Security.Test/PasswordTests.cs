﻿using System;
using System.Collections.Generic;
using System.Text;
using BbTS.Core.Security.Password;
using BbTS.Domain.Models.System;
using BbTS.Domain.Models.System.Security;
using BbTS.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BbTS.Core.Security.Test
{
    [TestClass]
    public class PasswordTests
    {
        private List<DomainViewObject> _codes;
        private UserAccountRules _rules;
        private User _user;
        private List<PasswordHistoryItem> _history;
                
        [TestInitialize]
        public void Initialize()
        {
            _codes = new List<DomainViewObject>
                {
                    new DomainViewObject { Id = 9, Name = "BlankPassword", Description = "Blank password encountered.  Please see your administrator." },
                    new DomainViewObject { Id = 5, Name = "PasswordComplexityFailure", Description = "The provided password does not meet password complexity requirements." },
                    new DomainViewObject { Id = 6, Name = "PasswordLengthFailure", Description = "The provided password does not meet password length requirements." },
                    new DomainViewObject { Id = 4, Name = "PasswordHistoryFailure", Description = "The provided password does not meet password history requirements." },
                    new DomainViewObject { Id = 0, Name = "Success", Description = "Success" }
                };

            _rules = new UserAccountRules
                {
                    UserAccountRuleId = 1,
                    PasswordLifeTimeDays = 90,
                    PasswordMaxAttempt = 3,
                    PasswordHistoryCount = 3,
                    PasswordLengthMinimum = 7,
                    PasswordExpireWarningDays = 7,
                    LockoutDurationMinutes = 15,
                    SessionIdleTimeoutMinutes = 15,
                    InactiveAccountDisableDays = 20,
                    InactiveAccountDisableDaysAllowOverride = false
                };
            _user = new User
                {
                    AccountLockedOutDateTime = null,
                    ActiveEndDate = null,
                    ActiveStartDate = new DateTime(2010, 1, 1),
                    Description = "test user",
                    FullName = "Test User",
                    HashType = 2,
                    IgnoreMerchantrightsCust = false,
                    InactiveAcctDisableDaysOverride = null,
                    InvalidLoginAttempts = 3,
                    IsActive = true,
                    IsAdmin = false,
                    IsLocked = false,
                    IterationCount = 1000,
                    PasswordChangeRequired = false,
                    LastLogoutDatetime = new DateTime(2014, 1, 1),
                    PasswordHash = Encoding.UTF8.GetBytes("3EE176FA8B6E2139A3E53BA72732BE9740EB97F1493E84B9086737360B9F87A56931FE4384607786702E35962A10652B0B67ADEA999A6872CF00764F0740D8A5"),
                    PasswordHashSalt = Encoding.ASCII.GetBytes("mypasswordsalt"),
                    Username = "me"
                };
            _history = new List<PasswordHistoryItem>
                {
                    new PasswordHistoryItem
                        {
                            DateTime = new DateTime(2014, 1, 1), HashType = 2, Id = 1, IterationCount = 1000,
                            PasswordHash = "754904638A7F0EC53EBF73DE164F66C0212FF0A3E7B2FE7119A0E328A9A3C8A7",
                            PasswordHashSalt = CommonFunctions.ByteArrayToCharAsString(Encoding.ASCII.GetBytes("mypasswordsalt1")),
                            Username = "me"
                        },
                        new PasswordHistoryItem
                        {
                            DateTime = new DateTime(2014, 1, 2), HashType = 2, Id = 2, IterationCount = 1000,
                            PasswordHash = "3F05DAB538CD1E4538DFB84A264E867D90708B5BD1F141E0BFB7596D737AEEFB",
                            PasswordHashSalt = CommonFunctions.ByteArrayToCharAsString(Encoding.ASCII.GetBytes("mypasswordsalt2")),
                            Username = "me"
                        },
                        new PasswordHistoryItem
                        {
                            DateTime = new DateTime(2014, 1, 3), HashType = 2, Id = 3, IterationCount = 1000,
                            PasswordHash = "F423725FCB45E19ECA1D53995E0BC674A53884B5BFEADC5586072CB8E966922C",
                            PasswordHashSalt = CommonFunctions.ByteArrayToCharAsString(Encoding.ASCII.GetBytes("mypasswordsalt3")),
                            Username = "me"
                        }
                };

        }
        [TestMethod]
        public void ValidatePasswordUnitTestSuccess()
        {
            const string password = "Mypassword1";
            ResponseToken responseToken = PasswordValidationTool.ValidatePassword(password, _codes, _rules);
            Assert.AreEqual(0, responseToken.ResultDomainId);

            const string password2 = "$ymb0!$0k@#%^&*()";
            responseToken = PasswordValidationTool.ValidatePassword(password2, _codes, _rules);
            Assert.AreEqual(0, responseToken.ResultDomainId);
        }

        [TestMethod]
        public void ValidatePasswordUnitTestFailBlankPassword()
        {
            const string password = "";
            ResponseToken responseToken = PasswordValidationTool.ValidatePassword(password, _codes, _rules);
            Assert.AreEqual(9, responseToken.ResultDomainId);
        }

        [TestMethod]
        public void ValidatePasswordUnitTestFailPasswordLength()
        {
            int minimum = _rules.PasswordLengthMinimum;
            _rules.PasswordLengthMinimum = 7;

            const string shortPassword = "Short1";
            ResponseToken responseToken = PasswordValidationTool.ValidatePassword(shortPassword, _codes, _rules);
            Assert.AreEqual(6, responseToken.ResultDomainId);

            const string longPassword = "This1passwordisreallylong000000";
            responseToken = PasswordValidationTool.ValidatePassword(longPassword, _codes, _rules);
            Assert.AreEqual(6, responseToken.ResultDomainId);

            const string minimumPassword = "Short12";
            responseToken = PasswordValidationTool.ValidatePassword(minimumPassword, _codes, _rules);
            Assert.AreEqual(0, responseToken.ResultDomainId);

            const string maximumPassword = "This1passwordisreallylong00000";
            responseToken = PasswordValidationTool.ValidatePassword(maximumPassword, _codes, _rules);
            Assert.AreEqual(0, responseToken.ResultDomainId);

            _rules.PasswordLengthMinimum = minimum;
        }

        [TestMethod]
        public void ValidatePasswordUnitTestFailComplexity()
        {
            const string password = "notcomplexenough";
            ResponseToken responseToken = PasswordValidationTool.ValidatePassword(password, _codes, _rules);
            Assert.AreEqual(5, responseToken.ResultDomainId);

            const string password2 = "123412341234";
            responseToken = PasswordValidationTool.ValidatePassword(password2, _codes, _rules);
            Assert.AreEqual(5, responseToken.ResultDomainId);
        }

        [TestMethod]
        public void ValidateUserPasswordHistoryUnitTestSuccess()
        {
            const string password = "currentpassword2";
            ResponseToken responseToken = PasswordValidationTool.ValidateUserPasswordHistory(password, _rules, _history, _codes, _user);
            Assert.AreEqual(0, responseToken.ResultDomainId);
        }

        [TestMethod]
        public void ValidateUserPasswordHistoryUnitTestFail()
        {
            string password = "firstpassword1";
            {
                ResponseToken responseToken = PasswordValidationTool.ValidateUserPasswordHistory(password, _rules, _history, _codes, _user);
                Assert.AreEqual(4, responseToken.ResultDomainId);
            }
            {
                password = "secondpassword2";
                ResponseToken responseToken = PasswordValidationTool.ValidateUserPasswordHistory(password, _rules, _history, _codes, _user);
                Assert.AreEqual(4, responseToken.ResultDomainId);
            }
            {
                password = "thirdpassword3";
                ResponseToken responseToken = PasswordValidationTool.ValidateUserPasswordHistory(password, _rules, _history, _codes, _user);
                Assert.AreEqual(4, responseToken.ResultDomainId);
            }
        }

        [TestMethod]
        public void TestSaltGeneratorSuccess()
        {
            byte[] salt1 = PasswordCreationTool.GenerateSalt(24);
            Assert.AreEqual(24, salt1.Length);

            byte[] salt2 = PasswordCreationTool.GenerateSalt(24);
            Assert.AreNotEqual(salt1, salt2);

            String saltString1 = CommonFunctions.ByteArrayToString(salt1);
            String saltString2 = CommonFunctions.ByteArrayToString(salt2);
            Assert.AreNotEqual(saltString1, saltString2);

        }

        [TestMethod]
        public void Generate()
        {
            {
                byte[] passwordHash = PasswordCreationTool.GeneratePasswordHash(
                    "firstpassword1",
                    Encoding.ASCII.GetBytes("mypasswordsalt1"),
                    1000,
                    2,
                    32);
                string passwordHashString = CommonFunctions.ByteArrayToString(passwordHash);
            }
            {
                byte[] passwordHash = PasswordCreationTool.GeneratePasswordHash(
                    "secondpassword2",
                    Encoding.ASCII.GetBytes("mypasswordsalt2"),
                    1000,
                    2,
                    32);
                string passwordHashString = CommonFunctions.ByteArrayToString(passwordHash);
            }
            {
                byte[] passwordHash = PasswordCreationTool.GeneratePasswordHash(
                    "thirdpassword3",
                    Encoding.ASCII.GetBytes("mypasswordsalt3"),
                    1000,
                    2,
                    32);
                string passwordHashString = CommonFunctions.ByteArrayToString(passwordHash);
            }
        }

    }
}
