﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using BbTS.Core.Security.Certificate;
using BbTS.Domain.Models.System.Security.Certificate;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BbTS.Core.Security.Test
{
    [TestClass]
    public class CertificateTests
    {
        [TestMethod]
        public void TestGetCertificateBindingsToPort443()
        {
            List<CertificateBinding> bindings = CertificateTool.GetSslCertificateBindings("443");
            Assert.IsTrue(bindings.Count > 0);

            X509Certificate2 certificate = null;
            if (bindings.Count > 0)
            {
                certificate = CertificateTool.GetCertificateByThumbprint(bindings[0].Thumbprint);
            }
            Assert.AreNotEqual(certificate, null);
        }
    }
}
