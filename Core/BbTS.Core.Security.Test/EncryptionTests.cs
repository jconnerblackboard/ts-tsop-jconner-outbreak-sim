﻿using System;
using System.Text;
using BbTS.Core.Security.Password;
using BbTS.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BbTS.Core.Security.Test
{
    [TestClass]
    public class EncryptionTests
    {
        
        /// <summary>
        /// Test vectors provided by ietf at http://www.ietf.org/rfc/rfc6070.txt.
        /// Using the following test vector to verify encryption:
        /// 
        ///  Input:
        ///     P = "passwordPASSWORDpassword" (24 octets)
        ///     S = "saltSALTsaltSALTsaltSALTsaltSALTsalt" (36 octets)
        ///     c = 4096
        ///     dkLen = 25
        ///
        ///  Output:
        ///     DK = 3d 2e ec 4f e4 1c 84 9b
        ///     80 c8 d8 36 62 c0 e4 4a
        ///     8b 29 1a 96 4c f2 f0 70
        ///     38                      (25 octets)
        /// </summary>
        [TestMethod]
        public void TestPbkdf2Sha1Hashing()
        {
            const string password1 = "passwordPASSWORDpassword";
            const string salt1 = "saltSALTsaltSALTsaltSALTsaltSALTsalt";
            const int iterations = 4096;
            const int keyLength = 25;

            byte[] passwordHash = EncryptionTool.ComputeHashPBKDF2SHA1(
                    Encoding.ASCII.GetBytes(password1),
                    Encoding.ASCII.GetBytes(salt1),
                    keyLength,
                    iterations);
            string actual = CommonFunctions.ByteArrayToString(passwordHash);
            const string expected = "3d2eec4fe41c849b80c8d83662c0e44a8b291a964cf2f07038";
            Assert.AreEqual(expected.ToUpper(), actual.ToUpper());
        }

        [TestMethod]
        public void TestPasswordCreationTool()
        {
            // Test type 1
            {
                const string password1 = "cat4sale";
                const string salt = "xziamdufdtwzrrekgnby";
                byte[] hash = PasswordCreationTool.GeneratePasswordHash(password1, Encoding.ASCII.GetBytes(salt), 1000, 1, 32);
                string actual = CommonFunctions.ByteArrayToString(hash);
                Assert.AreEqual("88B4306E93878E68C5A42679813F0C92B55EFBCFEA852BDC3229AFFDD481F8FB", actual);
            }

            // Test type 2
            {
                const string password = "passwordPASSWORDpassword";
                const string salt = "saltSALTsaltSALTsaltSALTsaltSALTsalt";
                const int iterations = 4096;
                const int keyLength = 25;

                byte[] passwordHash = PasswordCreationTool.GeneratePasswordHash(password, Encoding.ASCII.GetBytes(salt), iterations, 2, keyLength);
                string actual = CommonFunctions.ByteArrayToString(passwordHash);
                const string expected = "3d2eec4fe41c849b80c8d83662c0e44a8b291a964cf2f07038";
                Assert.AreEqual(expected.ToUpper(), actual.ToUpper());
            }

            // Test type 3
            {
                const string password1 = "cat4sale";
                const string salt = "xziamdufdtwzrrekgnby";
                byte[] hash = PasswordCreationTool.GeneratePasswordHash(password1, Encoding.ASCII.GetBytes(salt), 1, 3, 32);
                string actual = CommonFunctions.ByteArrayToString(hash);
                Assert.AreEqual("CFAF7F14BD3E172781BE5CCE0A2E31171ECCF162E067CEFC2756729C4CFD4A82", actual);
            }

            // Test Fail type
            try
            {
                const string password1 = "cat4sale";
                const string salt = "xziamdufdtwzrrekgnby";
                PasswordCreationTool.GeneratePasswordHash(password1, Encoding.ASCII.GetBytes(salt), 1, 4, 32);
                Assert.Fail("Invalid hash type not handled properly");
            }
            catch (ArgumentException aex)
            {
                if (aex.Message != "Wrong encryption type value: 4")
                    Assert.Fail("Invalid hash type not handled properly");
            }
        }

        [TestMethod]
        public void TestHashSha256()
        {
            byte[] passwordBytes = global::System.Text.Encoding.ASCII.GetBytes("cat4sale");
            byte[] salt = global::System.Text.Encoding.ASCII.GetBytes("xziamdufdtwzrrekgnby");
            byte[] hashOfHash = EncryptionTool.ComputeHashSHA256(
                passwordBytes,
                salt);
            string actual = CommonFunctions.ByteArrayToString(hashOfHash);
            Assert.AreEqual("88B4306E93878E68C5A42679813F0C92B55EFBCFEA852BDC3229AFFDD481F8FB", actual);
        }

        [TestMethod]
        public void TestHashOfHash()
        {
            byte[] sha256PasswordHash = CommonFunctions.HexStringToByteArray("88B4306E93878E68C5A42679813F0C92B55EFBCFEA852BDC3229AFFDD481F8FB");
            byte[] salt = global::System.Text.Encoding.ASCII.GetBytes("xziamdufdtwzrrekgnby");
            byte[] hashOfHash = EncryptionTool.ComputeHashPBKDF2SHA1(
                sha256PasswordHash,
                salt,
                32,
                1);
            string actual = CommonFunctions.ByteArrayToString(hashOfHash);
            Assert.AreEqual("CFAF7F14BD3E172781BE5CCE0A2E31171ECCF162E067CEFC2756729C4CFD4A82", actual);
        }

        [TestMethod]
        public void TestChainedHash()
        {
            byte[] passwordBytes = global::System.Text.Encoding.ASCII.GetBytes("cat4sale");
            byte[] salt = global::System.Text.Encoding.ASCII.GetBytes("xziamdufdtwzrrekgnby");
            byte[] hashOfHash = EncryptionTool.ComputeChainedHash(
                passwordBytes,
                salt,
                32,
                1);
            string actual = CommonFunctions.ByteArrayToString(hashOfHash);
            Assert.AreEqual("CFAF7F14BD3E172781BE5CCE0A2E31171ECCF162E067CEFC2756729C4CFD4A82", actual);
        }
    }
}
