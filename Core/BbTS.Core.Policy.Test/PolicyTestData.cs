﻿using BbTS.Domain.Models.Policy;

namespace BbTS.Core.Policy.Test
{
    /// <summary>
    /// Defines a policy test.
    /// </summary>
    public class PolicyTestData
    {
        /// <summary>
        /// Test name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Seed used for the test.  (Allows us to re-run a specific test.)
        /// </summary>
        public int Seed { get; set; }

        /// <summary>
        /// Identifier assigned by BbTS DB.
        /// </summary>
        public int PosId { get; set; }

        /// <summary>
        /// State information that is used as input information when evaluating policy.
        /// </summary>
        public PolicyEvaluationState EvaluationState { get; set; }
    }
}