﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using BbTS.Core.Conversion;
using BbTS.Domain.Models.Customer;
using BbTS.Domain.Models.Definitions.ArtsDataModel;
using BbTS.Domain.Models.Definitions.Policy;
using BbTS.Domain.Models.Policy;
using BbTS.Domain.Models.Policy.Actions;
using BbTS.Domain.Models.Policy.Conditions;
using BbTS.Domain.Models.Pos;
using BbTS.Domain.Models.RetailTransaction;
using BbTS.Domain.Models.System.Database;
using BbTS.Resource.Database.Concrete.Oracle;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BbTS.Core.Policy.Test
{
    [TestClass]
    public class PolicyTests
    {
        #region private

        private Random _random;

        #region Debug Output

        private static void Write(string text)
        {
            System.Diagnostics.Debug.Write(text);
        }

        private static void WriteLine(string text = "")
        {
            System.Diagnostics.Debug.WriteLine(text);
        }

        private static void OutputEvaluationState(PolicyTestData test)
        {
            WriteLine($"{test.Name} (Seed = {test.Seed})");
            WriteLine(test.EvaluationState.EvaluationDateTime.ToString(CultureInfo.InvariantCulture));
            WriteLine($"Pos Id = {test.PosId}");
            if (test.EvaluationState.PosGroup != null) WriteLine($"Pos Group = {test.EvaluationState.PosGroup.Name}");
            WriteLine($"Tender Id = {test.EvaluationState.TenderId}");
            WriteLine($"Customer GUID = {test.EvaluationState.CustomerInformationForPolicy.CustomerId}");
            Write("Customer Defined Group Item Ids = ( ");
            if (test.EvaluationState.CustomerInformationForPolicy.CustomerDefinedGroupDefinitionItemIdList == null)
            {
                Write("<empty>");
            }
            else
            {
                foreach (int id in test.EvaluationState.CustomerInformationForPolicy.CustomerDefinedGroupDefinitionItemIdList)
                {
                    Write($"{id}, ");
                }
            }

            WriteLine(" )");
        }

        private static void OutputPolicySet(PolicySet policySet, PolicySet currentParent, int level)
        {
            if (currentParent == null)
            {
                WriteLine("Policy Set Name [incarnation]                     AND/OR    Decision Strategy   Parents");
                WriteLine("------------------------------------------------  --------  ------------------  ------------------------------------");
            }

            //Name
            var rule = policySet as PolicyRule;
            string groupOrRule = rule == null ? "[ ]" : "( )";
            string debugOrNot = (rule != null) && (rule.EnabledStatus == EnabledStatus.EnabledForDebug) ? " DEBUG" : string.Empty;
            Write($"{new string(' ', level * 4)}{groupOrRule}{policySet.Name} {PolicyEvaluator.IncarnationGet(policySet, currentParent)}{debugOrNot}".PadRight(60));

            //Decision Strategy
            bool hasChildren = (policySet.Children != null) && (policySet.Children.Count > 0);
            Write((hasChildren ? policySet.DecisionStrategy.ToString() : string.Empty).PadRight(20));

            //Parents
            if (policySet.Parents != null)
            {
                //Write("Parents:  ");
                foreach (var parent in policySet.Parents)
                {
                    string comma = policySet.Parents.IndexOf(parent) == policySet.Parents.Count - 1 ? string.Empty : ", ";
                    Write($"{parent.Name}{comma}");
                }
            }

            WriteLine();
            
            //PolicyRule specifics
            if (rule != null)
            {
                //Validity Periods
                if (rule.Validity != null)
                {
                    foreach (var timePeriodCondition in rule.Validity)
                    {
                        OutputTimePeriodCondition(timePeriodCondition, level);
                    }
                }

                //Conditions
                OutputCondition(rule.Conditions, false, level);

                //Actions
                foreach (var action in rule.Actions)
                {
                    OutputAction(action, level);
                }
            }

            if (!hasChildren) return;

            //Children
            level++;
            foreach (var child in policySet.Children)
            {
                OutputPolicySet(child, policySet, level);
            }
        }

        private static void OutputTimePeriodCondition(TimePeriodCondition timePeriodCondition, int level)
        {
            if (timePeriodCondition == null) return;

            WriteLine($"{new string(' ', level * 4)} @ {timePeriodCondition.Name}".PadRight(50));
        }

        private static void OutputCondition(PolicyCondition condition, bool isNegated, int level)
        {
            if (condition == null) return;

            string negatedText = isNegated ? "NOT" : "   ";
            Write($"{new string(' ', level * 4)} ? {negatedText} {condition.Name}".PadRight(50));
            var compoundCondition = condition as CompoundPolicyCondition;
            if (compoundCondition == null)
            {
                WriteLine();
                return;
            }

            switch (compoundCondition.ConditionListType)
            {
                case ConditionListType.Disjunctive:
                    WriteLine("OR");
                    break;
                case ConditionListType.Conjunctive:
                    WriteLine("AND");
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            ++level;
            for (int i = 0; i < compoundCondition.Children.Count; i++)
            {
                var childCondition = compoundCondition.Children[i];
                bool childNegated = compoundCondition.NegationFlags[i];
                OutputCondition(childCondition, childNegated, level);
            }
        }

        private static void OutputAction(PolicyAction action, int level)
        {
            WriteLine($"{new string(' ', level * 4)} > {action.Name}");
        }

        private static void OutputEvaluationLog(List<string> evaluationLog)
        {
            foreach (var line in evaluationLog)
            {
                WriteLine(line);
            }
        }

        private static void OutputActionOutputList(List<PolicyActionOutput> actionOutputList)
        {
            WriteLine("----------");
            WriteLine("Actions resulting from this policy:  (Actions flagged as debug are ignored for all checks.)");
            WriteLine();
            foreach (var policyActionOutput in actionOutputList)
            {
                WriteLine($" > '{policyActionOutput.Action.Name}' resulting from '{policyActionOutput.ActionSource.Name}' {policyActionOutput.RuleIncarnation} {(policyActionOutput.Debug ? "DEBUG" : string.Empty)}");
            }

            WriteLine();
            WriteLine("Policy Evaluation Summary");
            WriteLine("-------------------------");
            WriteLine();

            //Check for deny or allow, deny overrides allow, lack of allow counts as a deny
            bool denied = actionOutputList.Where(output => !output.Debug).Any(output => (output.Action as DenyTransactionAction) != null);
            bool allowed = actionOutputList.Where(output => !output.Debug).Any(output => (output.Action as AllowTransactionAction) != null);
            if (denied || !allowed) Write("Denied");
            else Write("Allowed");
            if (denied) WriteLine(" (explicit)");
            if (!allowed) WriteLine(" (implicit)");
            if (allowed) WriteLine(" (explicit)");
            WriteLine();

            //Build discount list (ignoring any outputs with Debug flag set)
            var outputDiscounts = (from output in actionOutputList let action = (output.Action as ApplyDiscountAction) where (action != null) && !output.Debug select output).ToList();
            if (outputDiscounts.Count > 0)
            {
                var discounts = outputDiscounts.Select(output => new DiscountSurchargeRule { Type = DiscountSurchargeType.Discount, Rate = ((ApplyDiscountAction)output.Action).Rate }).ToList();
                WriteLine("Discounts");
                foreach (var discount in discounts)
                {
                    WriteLine($"{discount.Rate:P}");
                }

                WriteLine();
            }

            //Build surcharge list (ignoring any outputs with Debug flag set)
            var outputSurcharges = (from output in actionOutputList let action = (output.Action as ApplySurchargeAction) where (action != null) && !output.Debug select output).ToList();
            if (outputSurcharges.Count > 0)
            {
                var surcharges = outputSurcharges.Select(output => new DiscountSurchargeRule { Type = DiscountSurchargeType.Surcharge, Rate = ((ApplySurchargeAction)output.Action).Rate }).ToList();
                WriteLine("Surcharges");
                foreach (var surcharge in surcharges)
                {
                    WriteLine($"{surcharge.Rate:P}");
                }

                WriteLine();
            }

            //SV transaction limits (ignoring any outputs with Debug flag set)
            var outputLimits = (from output in actionOutputList let action = (output.Action as ApplyStoredValueTransactionLimit) where (action != null) && !output.Debug select output).ToList();
            if (outputLimits.Count > 0)
            {
                var transactionLimits = outputLimits.Select(output => ((ApplyStoredValueTransactionLimit)output.Action).StoredValueTransactionLimitId).ToList();
                WriteLine("SV Transaction Limit Id(s)");
                foreach (var transactionLimit in transactionLimits)
                {
                    WriteLine($"Id:  {transactionLimit}");
                }

                WriteLine();
            }

            //SV account drain (ignoring any outputs with Debug flag set)
            var outputStoredValueAccounts = (from output in actionOutputList let action = (output.Action as UseStoredValueAccounts) where (action != null) && !output.Debug select output).ToList();
            if (outputStoredValueAccounts.Count > 0)
            {
                var storedValueAccounts = ((UseStoredValueAccounts)outputStoredValueAccounts.Last().Action).StoredValueAccountTypeIds; //Last drain order group wins.
                WriteLine("Account drain order");
                foreach (var storedValueAccount in storedValueAccounts)
                {
                    WriteLine($"SV Account Id:  {storedValueAccount}");
                }
            }
        }
        
        #endregion

        private readonly TimePeriodCondition _schoolYear = new TimePeriodCondition { Name = "School Year", MonthOfYearMask = "TTTTTFFFTTTT" };
        private readonly TimePeriodCondition _electionDay = new TimePeriodCondition { Name = "Election Day", MonthOfYearMask = "FFFFFFFFFFTF", DayOfMonthForwardMask = "FFFFFFFTTTTTTTFFFFFFFFFFFFFFFFF", DayOfWeekMask = "FFTFFFF" };

        private readonly TrueCondition _trueCondition = new TrueCondition { Name = "True" };
        private readonly FalseCondition _falseCondition = new FalseCondition { Name = "False" };
        private readonly CustomerInGroupCondition _customerIsFacultyCondition = new CustomerInGroupCondition { Name = "Customer is Faculty", CustomerDefinedGroupItemId = 1 };
        private readonly PosInGroupCondition _posInCampusFoodServiceGroup = new PosInGroupCondition { Name = "POS run by Campus Food Service", PosGroupId = 1 };
        private readonly TenderUsedCondition _tenderIsBonusBucks = new TenderUsedCondition { Name = "Tender is BonusBucks", TenderId = 1 };
        private readonly TenderUsedCondition _tenderIsFootballFunds = new TenderUsedCondition { Name = "Tender is FootballFunds", TenderId = 2 };

        private readonly DenyTransactionAction _denyTheTransaction = new DenyTransactionAction { Name = "Deny the Transaction" };
        private readonly AllowTransactionAction _allowTheTransaction = new AllowTransactionAction { Name = "Allow the Transaction" };
        private readonly ApplyDiscountAction _discount10 = new ApplyDiscountAction { Name = "10% Discount", Rate = 0.10m };
        private readonly ApplySurchargeAction _surcharge20 = new ApplySurchargeAction { Name = "20% Surcharge", Rate = 0.20m };
        private readonly UseStoredValueAccounts _useStoredValueAccounts = new UseStoredValueAccounts { Name = "Use Account Types 1-5", StoredValueAccountTypeIds = new List<int> { 1, 2, 3, 4, 5 } };
        private readonly ApplyStoredValueTransactionLimit _applyTransactionLimit = new ApplyStoredValueTransactionLimit { Name = "Apply Transaction Limit", StoredValueTransactionLimitId = 1 };

        private static readonly List<CardToCustomerGuidMapping> CardToGuidList = new List<CardToCustomerGuidMapping>
        {
            new CardToCustomerGuidMapping { CardNumber = "1234567890123456789", IssueCode = "9" , CustomerGuid = "8e1c477b-192e-4183-bdad-815f2d50953e" },
            new CardToCustomerGuidMapping { CardNumber = "9876543210123456789", IssueCode = "9" , CustomerGuid = "a79bb5b8-8329-4ccb-a5b5-16134f7f8f48" },
            new CardToCustomerGuidMapping { CardNumber = "9876543210987654321", IssueCode = "9" , CustomerGuid = "8bf75f28-7b95-4060-bc3f-dba9ab760369" },
        };

        private static readonly List<CustomerInformationForPolicy> AllCustomers = new List<CustomerInformationForPolicy>
        {
            new CustomerInformationForPolicy { CustomerId = new Guid("8e1c477b-192e-4183-bdad-815f2d50953e"), CustomerDefinedGroupDefinitionItemIdList = new List<int> { 1, 2, 3 } },
            new CustomerInformationForPolicy { CustomerId = new Guid("a79bb5b8-8329-4ccb-a5b5-16134f7f8f48"), CustomerDefinedGroupDefinitionItemIdList = new List<int> { 2, 3 } },
            new CustomerInformationForPolicy { CustomerId = new Guid("8bf75f28-7b95-4060-bc3f-dba9ab760369"), CustomerDefinedGroupDefinitionItemIdList = new List<int> { 3 } },
        };

        private readonly PolicyEvaluationState _evaluationState = new PolicyEvaluationState
        {
            EvaluationDateTime = DateTimeOffset.Now,
            CustomerInformationForPolicy = AllCustomers.Find(c => c.CustomerId == new Guid("8e1c477b-192e-4183-bdad-815f2d50953e")),
            PosGroup = new PosGroup
            {
                PosGroupId = 2,
                Name = "Dining Hall",
                Parent = new PosGroup
                {
                    PosGroupId = 1,
                    Name = "Campus Food Service"
                }
            },
            TenderId = 1
        };

        #endregion

        [TestMethod]
        public void TestPolicyStructure()
        {
            //Note that the PolicyBuilder class somewhat simplifies the building of a policy tree.  I have not updated this method to use it, yet.

            var root = new PolicyGroup
            {
                Name = "System Root Policy",
                Parents = null,
                Children = new List<PolicySet>(),
                DecisionStrategy = PolicyDecisionStrategy.AllMatching
            };

            var standardActionsPolicyGroup = new PolicyGroup
            {
                Name = "Standard Actions",
                Parents = new List<PolicySet> { root },
                Children = new List<PolicySet>(),
                DecisionStrategy = PolicyDecisionStrategy.AllMatching
            };
            root.Children.Add(standardActionsPolicyGroup);

            var customerIsFacultyRule = new PolicyRule
            {
                Name = "Customer is Faculty Rule",
                Parents = new List<PolicySet> { standardActionsPolicyGroup },
                EnabledStatus = EnabledStatus.Enabled,
                Conditions = new CompoundPolicyCondition { Name = "<compound>", Children = new List<PolicyCondition> { _customerIsFacultyCondition }, NegationFlags = new List<bool> { false } },
                Actions = new List<PolicyAction> { _discount10 }
            };
            standardActionsPolicyGroup.Children.Add(customerIsFacultyRule);
            _customerIsFacultyCondition.UsageCount++;
            _discount10.UsageCount++;

            var tenderIsBonusBucksRule = new PolicyRule
            {
                Name = "Tender is BonusBucks Rule",
                Parents = new List<PolicySet> { standardActionsPolicyGroup },
                EnabledStatus = EnabledStatus.Enabled,
                Conditions = new CompoundPolicyCondition { Name = "<compound>", Children = new List<PolicyCondition> { _tenderIsBonusBucks }, NegationFlags = new List<bool> { false } },
                Actions = new List<PolicyAction> { _surcharge20 }
            };
            standardActionsPolicyGroup.Children.Add(tenderIsBonusBucksRule);
            _tenderIsBonusBucks.UsageCount++;
            _surcharge20.UsageCount++;

            var tenderIsFootballFundsRule = new PolicyRule
            {
                Name = "Tender is FootballFunds Rule",
                Parents = new List<PolicySet> { standardActionsPolicyGroup },
                EnabledStatus = EnabledStatus.Enabled,
                Conditions = new CompoundPolicyCondition { Name = "<compound>", Children = new List<PolicyCondition> { _tenderIsFootballFunds }, NegationFlags = new List<bool> { false } },
                Actions = new List<PolicyAction> { _discount10 }
            };
            standardActionsPolicyGroup.Children.Add(tenderIsFootballFundsRule);
            _tenderIsFootballFunds.UsageCount++;
            _discount10.UsageCount++;

            var posGroupIsCampusFoodService = new PolicyRule
            {
                Name = "POS Group is Campus Food Service",
                Parents = new List<PolicySet> { standardActionsPolicyGroup },
                EnabledStatus = EnabledStatus.Enabled,
                Conditions = new CompoundPolicyCondition { Name = "<compound>", Children = new List<PolicyCondition> { _posInCampusFoodServiceGroup }, NegationFlags = new List<bool> { false } },
                Actions = new List<PolicyAction> { _applyTransactionLimit }
            };
            standardActionsPolicyGroup.Children.Add(posGroupIsCampusFoodService);
            _posInCampusFoodServiceGroup.UsageCount++;
            _applyTransactionLimit.UsageCount++;

            var ruleA = new PolicyRule
            {
                Name = "Rule A",
                Parents = new List<PolicySet> { root },
                Children = new List<PolicySet>(),
                DecisionStrategy = PolicyDecisionStrategy.FirstMatching,
                EnabledStatus = EnabledStatus.Enabled,
                Validity = new List<TimePeriodCondition> { _schoolYear, _electionDay },
                Conditions = new CompoundPolicyCondition { Name = "<compound>", Children = new List<PolicyCondition> { _trueCondition, _falseCondition, _trueCondition }, NegationFlags = new List<bool> { false, false, true }, ConditionListType = ConditionListType.Disjunctive },
                Actions = new List<PolicyAction>
                {
                    _allowTheTransaction,
                    _useStoredValueAccounts
                }
            };
            root.Children.Add(ruleA);
            _schoolYear.UsageCount++;
            _electionDay.UsageCount++;
            _trueCondition.UsageCount++;
            _allowTheTransaction.UsageCount++;
            _useStoredValueAccounts.UsageCount++;

            var group = new PolicyGroup
            {
                Name = "Dummy Group",
                Parents = new List<PolicySet> { ruleA },
                Children = new List<PolicySet>(),
                DecisionStrategy = PolicyDecisionStrategy.AllMatching
            };
            ruleA.Children.Add(group);

            var ruleB = new PolicyRule
            {
                Name = "Rule B",
                Parents = new List<PolicySet> { ruleA, root },
                Children = new List<PolicySet>(),
                DecisionStrategy = PolicyDecisionStrategy.AllMatching,
                EnabledStatus = EnabledStatus.Enabled,
                Actions = new List<PolicyAction>
                {
                    _allowTheTransaction
                }
            };
            root.Children.Add(ruleB);
            ruleA.Children.Add(ruleB);
            _allowTheTransaction.UsageCount++;

            var ruleC = new PolicyRule
            {
                Name = "Rule C",
                Parents = new List<PolicySet> { group, ruleB, root },
                Children = new List<PolicySet>(),
                DecisionStrategy = PolicyDecisionStrategy.AllMatching,
                EnabledStatus = EnabledStatus.Enabled,
                Conditions = new CompoundPolicyCondition { Name = "<compound>", Children = new List<PolicyCondition> { _trueCondition, _falseCondition }, NegationFlags = new List<bool> { false, false }, ConditionListType = ConditionListType.Disjunctive },
                Actions = new List<PolicyAction>
                {
                    _useStoredValueAccounts
                }
            };
            root.Children.Add(ruleC);
            ruleB.Children.Add(ruleC);
            group.Children.Add(ruleC);
            _useStoredValueAccounts.UsageCount++;

            OutputPolicySet(root, null, 0);
            WriteLine("----------");

            //Evaluate policy and output results
            List<string> evaluationLog;
            List<PolicyActionOutput> actionOutputList;
            var policyEvaluator = new PolicyEvaluator(root);
            policyEvaluator.PolicyEvaluate(_evaluationState, out evaluationLog, out actionOutputList);
            OutputEvaluationLog(evaluationLog);
            OutputActionOutputList(actionOutputList);
        }

        [TestMethod]
        public void TestPolicyBuilder()
        {
            var testPolicy = new PolicyBuilder("System Root Policy");
            testPolicy.Root.DecisionStrategy = PolicyDecisionStrategy.AllMatching;

            var standardActionsPolicyGroup = PolicyBuilder.NewPolicyGroup("Standard Actions");
            testPolicy.Root.AddChild(standardActionsPolicyGroup);

            var customerIsFacultyRule = PolicyBuilder.NewPolicyRule("Customer is Faculty Rule");

            customerIsFacultyRule.Conditions.ConditionListType = ConditionListType.Conjunctive;
            customerIsFacultyRule.Conditions.AddCondition(_customerIsFacultyCondition);

            var compoundCondition = new CompoundPolicyCondition { ConditionListType = ConditionListType.Conjunctive };
            compoundCondition.AddCondition(_trueCondition);
            compoundCondition.AddCondition(_tenderIsBonusBucks);
            customerIsFacultyRule.Conditions.AddCondition(compoundCondition);

            customerIsFacultyRule.AddAction(_discount10);
            customerIsFacultyRule.AddAction(_allowTheTransaction);
            customerIsFacultyRule.AddAction(_useStoredValueAccounts);
            customerIsFacultyRule.EnabledStatus = EnabledStatus.Enabled;
            standardActionsPolicyGroup.AddChild(customerIsFacultyRule);
            standardActionsPolicyGroup.AddChild(PolicyBuilder.NewPolicyRule());

            testPolicy.Root.AddChild(PolicyBuilder.NewPolicyRule());
            testPolicy.Root.AddChild(PolicyBuilder.NewPolicyGroup());

            OutputPolicySet(testPolicy.Root, null, 0);
            WriteLine("----------");

            //Evaluate policy and output results
            List<string> evaluationLog;
            List<PolicyActionOutput> actionOutputList;
            var policyEvaluator = new PolicyEvaluator(testPolicy.Root);
            policyEvaluator.PolicyEvaluate(_evaluationState, out evaluationLog, out actionOutputList);
            OutputEvaluationLog(evaluationLog);
            OutputActionOutputList(actionOutputList);
        }

        [TestMethod]
        public void TestVersusDatabaseLogic()
        {
            #region Get data from database
            //Set connection
            var oracleSource = new OracleSource();
            var connection = new ConnectionInfo
            {
                User = "Envision",
                Password = "password1",
                Port = 1521,
                ServerName = "tsdv-bvt-3135.transactpd.net",
                ServiceName = "BBTS"
            };

            //Load list of POSs
            List<TsPos> tsPosList = oracleSource.PosArchivesGet(connection);

            //Load POS groups
            List<TsPos_Group> tsPosGroups = oracleSource.PosGroupArchivesGet(connection);

            //Load customer card numbers / customer guids from database
            List<CardToCustomerGuidMapping> cardToCustomerGuidList = oracleSource.CardToCustomerGuidGet(connection);

            //Load customer group information from database
            List<CustomerDefinedGroupItemIdsForCustomerGuid> customerDefinedGroupIdsForCustomerGuidList = oracleSource.CustomerDefinedGroupIdsForCustomerGuidGet(connection, null);

            //Load tenders from database
            //List<TsTender> tsTenders = oracleSource.TenderArchivesGet(connection);
            #endregion

            int seed = Environment.TickCount;
            //int seed = 1419535735; //Use when pos is hardcoded (1142), TenderId 59
            //int seed = 1427040016; //Use when pos is random, PosId  23, TenderId 21
            //int seed = -1677650749; //Use when pos is hardcoded(1142), TenderId 60
            seed = 1306986843;

            for (int i = 0; i < 1; i++, seed++)
            {
                var test = new PolicyTestData { Name = $"Test {i}", Seed = seed };
                _random = new Random(test.Seed);

                //Pick a random pos
                var tsPos = tsPosList[_random.Next(tsPosList.Count)];
                //Hardcoded pos
                //var tsPos = tsPosList[961];

                test.PosId = tsPos.Pos_Id;


                //Create a pos group structure for this pos
                var tsPosGroup = tsPosGroups.Find(g => g.Pos_Group_Id == tsPos.Pos_Group_Id);
                PosGroup posGroup = null;
                if (tsPosGroup != null)
                {
                    posGroup = new PosGroup { PosGroupId = tsPosGroup.Pos_Group_Id, Name = tsPosGroup.Name };
                    var currentPosGroup = posGroup;
                    while (tsPosGroup.Parent_Pos_Group_Id > 0)
                    {
                        tsPosGroup = tsPosGroups.Find(tg => tg.Pos_Group_Id == tsPosGroup.Parent_Pos_Group_Id);
                        currentPosGroup.Parent = new PosGroup { PosGroupId = tsPosGroup.Pos_Group_Id, Name = tsPosGroup.Name };
                        currentPosGroup = currentPosGroup.Parent;
                    }
                }

                #region Set evaluation state
                //Generate random policy evaluation state that will be used for both the C# path and the database path
                test.EvaluationState = new PolicyEvaluationState();

                //Random stored value tender
                List<TsTender> tsStoredValueTenders = oracleSource.StoredValueTendersForPos(test.PosId, connection);
                test.EvaluationState.TenderId = tsStoredValueTenders[_random.Next(tsStoredValueTenders.Count)].Tender_Id;
                //test.EvaluationState.TenderId = tsTenders[_random.Next(tsTenders.Count)].Tender_Id;
                //test.EvaluationState.TenderId = 19;

                //Random time
                var now = DateTimeOffset.Now;
                var minDateTime = now.AddYears(-1);
                var maxDateTime = now.AddYears(1);
                var range = maxDateTime - minDateTime; // We don't want to overflow our seconds variable (which is an int), so keep range "sane".  A 20-year range is well within the safe range.
                int seconds = (int)(range.Ticks / 10000000);
                test.EvaluationState.EvaluationDateTime = minDateTime.AddSeconds(_random.Next(seconds));

                //Get pos group from posId
                test.EvaluationState.PosGroup = posGroup;

                //Random customer
                //var customer = cardToCustomerGuidList[_random.Next(cardToCustomerGuidList.Count)];
                //Hardcoded customer
                var customer = new CardToCustomerGuidMapping
                {
                    CardNumber = "0000000000030000045175",
                    IssueCode = string.Empty,
                    CustomerGuid = "20625286-00F3-456A-9ED8-DFBE247E7189",
                    CustomerId = 145885
                };

                //Load customer group information
                CustomerDefinedGroupItemIdsForCustomerGuid customerGroupInformation = customerDefinedGroupIdsForCustomerGuidList.Find(c => c.CustomerId.EqualsGuid(customer.CustomerGuid));
                test.EvaluationState.CustomerInformationForPolicy = new CustomerInformationForPolicy
                {
                    CustomerId = Guid.Parse(customer.CustomerGuid),
                    CustomerDefinedGroupDefinitionItemIdList = customerGroupInformation?.CustomerDefinedGroupDefinitionItemIdList
                };

                //Show evaluation state
                OutputEvaluationState(test);
                WriteLine();
                #endregion

                #region Evaluate policy with PolicyEvaluator
                //Check originator and tender combination for validity  (Does what TenderValidityPointOfSaleGet does in PackTranPolicy.ProcessPolicy)
                //Todo - I'm using the result set from the StoredValueTendersForPos query right now, but we could probably optimize this easily by writing a more targeted query.
                bool isValidOriginatorAndTenderCombination = tsStoredValueTenders.Find(t => t.Tender_Id == test.EvaluationState.TenderId) != null;
                if (!isValidOriginatorAndTenderCombination)
                {
                    throw new Exception($"Invalid merchant tender:  TenderId {test.EvaluationState.TenderId} is not valid for PosId {test.PosId}.");
                }

                //Load policy tree
                var policy = oracleSource.SystemRootPolicyLoad(connection);
                OutputPolicySet(policy, null, 0);
                WriteLine("----------");

                //Evaluate
                List<string> evaluationLog;
                List<PolicyActionOutput> actionOutputList;
                var policyEvaluator = new PolicyEvaluator(policy);
                policyEvaluator.PolicyEvaluate(test.EvaluationState, out evaluationLog, out actionOutputList);
                OutputEvaluationLog(evaluationLog);
                OutputActionOutputList(actionOutputList);

                //Use account drain order to determine available balance
                var libraryAvailableBalance = 0.00m;
                var libraryTaxExempt = false;
                var drainOrder = PolicyEvaluator.DrainOrderExtract(actionOutputList);
                //Todo - we probably need to make sure that the account type ids are valid for this merchant
                //var filteredDrainOrder = FilterAccountTypeIdsByMerchant(drainOrder);
                var transactionLimits = PolicyEvaluator.TransactionLimitIdsExtract(actionOutputList);
                //Todo - work in progress
                if (drainOrder.Count > 0)
                {
                    //libraryAvailableBalance = oracleSource.AvailableBalanceForCustomerGet(
                    //    connection,
                    //    test.EvaluationState.CustomerInformationForPolicy.CustomerGuid,
                    //    drainOrder,
                    //    transactionLimits,
                    //    out libraryTaxExempt);
                }

                PolicyEvaluationResult libraryResponse = new PolicyEvaluationResult
                {
                    CustomerGuid = Guid.Parse(customer.CustomerGuid),
                    //CustomerId = customer.CustomerId,
                    AvailableBalance = libraryAvailableBalance,
                    TaxExempt = libraryTaxExempt,
                    DiscountSurchargeRules = PolicyEvaluator.DiscountSurchargeRulesExtract(actionOutputList)
                };
                #endregion

                #region Evaluate policy with Database
                //Make call to database stored proc
                int databaseCustomerId;
                decimal databaseAvailableBalance;
                bool databaseTaxExempt;
                List<DiscountSurchargeRule> databaseDiscountSurchargeRules;
                int errorCode;
                string deniedText;
                oracleSource.ValidateSystemRootPolicy(
                    customer.CardNumber,
                    customer.IssueCode,
                    true,
                    test.PosId,
                    test.EvaluationState.TenderId,
                    test.EvaluationState.EvaluationDateTime,
                    out databaseCustomerId,
                    out databaseAvailableBalance,
                    out databaseTaxExempt,
                    out databaseDiscountSurchargeRules,
                    out errorCode,
                    out deniedText,
                    connection);

                PolicyEvaluationResult databaseResponse = new PolicyEvaluationResult
                {
                    //CustomerId = databaseCustomerId,
                    AvailableBalance = databaseAvailableBalance,
                    TaxExempt = databaseTaxExempt,
                    DiscountSurchargeRules = databaseDiscountSurchargeRules
                };
                #endregion

                #region Assertions
                //Discounts/surcharges
                if (libraryResponse.DiscountSurchargeRules.Count == databaseResponse.DiscountSurchargeRules.Count)
                {
                    for (int j = 0; j < libraryResponse.DiscountSurchargeRules.Count; j++)
                    {
                        var libraryDiscountSurcharge = libraryResponse.DiscountSurchargeRules[j];
                        var databaseDiscountSurcharge = databaseResponse.DiscountSurchargeRules[j];
                        
                        Assert.AreEqual(databaseDiscountSurcharge.Type, libraryDiscountSurcharge.Type, "libraryResponse discount/surcharge type differs from databaseResponse discount/surcharge type");
                        Assert.AreEqual(databaseDiscountSurcharge.Rate, libraryDiscountSurcharge.Rate, "libraryResponse discount/surcharge rate differs from databaseResponse discount/surcharge rate");
                    }
                }
                else
                {
                    Assert.Fail($"libraryResponse discount/surcharge list count ({libraryResponse.DiscountSurchargeRules.Count}) differs from databaseResponse discount/surcharge list count ({databaseResponse.DiscountSurchargeRules.Count})");
                }

                //Todo - we can't compare database vs. library until we have a working AvailableBalanceForCustomerGet function
                //Available balance
                //Assert.AreEqual(databaseAvailableBalance, libraryAvailableBalance, "libraryAvailableBalance differs from databaseAvailableBalance");

                //Tax exempt
                //Assert.AreEqual(databaseTaxExempt, libraryTaxExempt, "libraryTaxExempt differs from databaseTaxExempt");
                #endregion
            }
        }
    }
}
