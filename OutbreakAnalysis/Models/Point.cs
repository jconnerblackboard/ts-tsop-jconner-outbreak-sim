﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutbreakAnalysis.Models
{
    public class Point
    {
        public decimal X { get; set; }
        public decimal Y { get; set; }

        public Point()
        {
        }

        public Point(decimal x, decimal y)
        {
            X = x;
            Y = y;
        }

        public Point Clone()
        {
            return (Point) MemberwiseClone();
        }


        public override string ToString()
        {
            return $"({X}, {Y})";
        }
    }
}
