﻿using System;

namespace OutbreakAnalysis.Models
{
    public class StudentAccessLog
    {
        public Student Student { get; set; }

        public DateTimeOffset EntryTime { get; set; }

        public Door Door { get; set; }

    }
}
