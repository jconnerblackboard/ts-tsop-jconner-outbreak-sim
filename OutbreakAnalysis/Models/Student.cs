﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using OutbreakAnalysis.Definition;

namespace OutbreakAnalysis.Models
{
    public class Student
    {
        public string Id { get; set; }

        public string CustNum { get; set; }

        public string DefaultCardNum { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public OutbreakStatus Status { get; set; }

        public Point Location { get; set; } = new Point();

        public DateTimeOffset? ExposedAt { get; set; }

        public Door ExposedDoor { get; set; } = new Door();

        public bool IsExposed => Status == OutbreakStatus.Exposed || Status == OutbreakStatus.PatientZero;

        public Student()
        {
        }

        public Student(Student student, Point location)
        {
            var copy = student.Clone();
            ExposedAt = copy.ExposedAt;
            Id = copy.Id;
            CustNum = copy.CustNum;
            DefaultCardNum = copy.DefaultCardNum;
            Status = copy.Status;
            Location = location.Clone();
        }

        public Student(Student student, Point location, OutbreakStatus status)
        {
            var copy = student.Clone();
            ExposedAt = copy.ExposedAt;
            Id = copy.Id;
            CustNum = copy.CustNum;
            DefaultCardNum = copy.DefaultCardNum;
            Status = status;
            Location = location.Clone();
        }

        public Student Clone()
        {
            var clone = (Student) MemberwiseClone();
            clone.ExposedDoor = ExposedDoor.Clone();
            clone.Location = Location.Clone();
            return clone;
        }
    }
}
