﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace OutbreakAnalysis.Models
{
    public class Quad
    {
        public Point Min { get; set; }
        public Point Max { get; set; }

        public Quad()
        {
        }

        public Quad(decimal minX, decimal minY, decimal maxX, decimal maxY)
        {
            Min = new Point(minX, minY);
            Max = new Point(maxX, maxY);
        }

        public Quad(Point min, Point max)
        {
            Min = min;
            Max = max;
        }

        public bool Contains(Point p)
        {
            return p.X <= Max.X && p.X >= Min.X &&
                   p.Y <= Max.Y && p.Y >= Min.Y;
        }
    }
}
