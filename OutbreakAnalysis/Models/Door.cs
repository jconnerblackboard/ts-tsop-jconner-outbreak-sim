﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutbreakAnalysis.Models
{
    public class Door
    {
        public string DoorId { get; set; }

        public string DoorIdentifier { get; set; }

        public string Description { get; set; }

        public Point Location { get; set; } = new Point();

        public Door Clone()
        {
            var clone = (Door) MemberwiseClone();
            clone.Location = Location.Clone();
            return clone;
        }
    }
}
