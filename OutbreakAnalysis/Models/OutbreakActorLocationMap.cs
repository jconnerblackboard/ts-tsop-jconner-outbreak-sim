﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutbreakAnalysis.Models
{
    public class OutbreakActorLocationMap
    {
        public Quad OuterBoundary { get; set; } = new Quad
        {
            Max = new Point(1280, 1020),
            Min = new Point(0, 0)
        };

        public Quad InnerBoundary { get; set; } = new Quad
        {
            Max = new Point(960, 720),
            Min = new Point(320, 240)
        };
    }
}
