﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutbreakAnalysis.Models
{
    public class OutbreakSimulation
    {
        public OutbreakSimulationParameters Parameters { get; set; } = new OutbreakSimulationParameters();
        public List<OutbreakSimulationTimeSlice> TimeSlices { get; set; } = new List<OutbreakSimulationTimeSlice>();
        public List<Student> Students { get; set; } = new List<Student>();
        public List<Door> Doors { get; set; } = new List<Door>();
        public List<StudentAccessLog> Logs { get; set; } = new List<StudentAccessLog>();
    }
}
