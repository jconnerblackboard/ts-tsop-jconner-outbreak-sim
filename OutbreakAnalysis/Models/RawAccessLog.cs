﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutbreakAnalysis.Models
{
    /// <summary>
    /// Raw access log object to be parsed from json
    /// </summary>
    public class RawAccessLog
    {
        /// <summary>
        /// CustId
        /// </summary>
        public int CustId { get; set; }
        /// <summary>
        /// CustNum
        /// </summary>
        public string CustNum { get; set; }
        /// <summary>
        /// DefaultCardNum
        /// </summary>
        public string DefaultCardNum { get; set; }
        /// <summary>
        /// DoorId
        /// </summary>
        public int DoorId { get; set; }
        /// <summary>
        /// DoorIdentifier
        /// </summary>
        public string DoorIdentifier { get; set; }
        /// <summary>
        /// DoorDescription
        /// </summary>
        public string DoorDescription { get; set; }
        /// <summary>
        /// EntryDateTime
        /// </summary>
        public DateTimeOffset EntryDateTime { get; set; }
    }
}
