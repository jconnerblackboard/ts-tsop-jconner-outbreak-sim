﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutbreakAnalysis.Models
{
    public class OutbreakSimulationTimeSlice
    {
        public DateTimeOffset StartTime { get; set; }
        public DateTimeOffset EndTime { get; set; }

        public string PatientZero { get; set; }
        public List<Student> StudentsBeforeTimeSlice { get; set; } = new List<Student>();
        public List<Student> StudentsAfterTimeSlice { get; set; } = new List<Student>();

        public OutbreakSimulationTimeSlice Clone()
        {
            var clone = (OutbreakSimulationTimeSlice) MemberwiseClone();
            clone.StudentsAfterTimeSlice = new List<Student>(StudentsAfterTimeSlice.Select(s => s));
            clone.StudentsAfterTimeSlice = new List<Student>(StudentsAfterTimeSlice.Select(s => s));
            return clone;
        }
    }
}
