﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutbreakAnalysis.Models
{
    public class OutbreakSimulationParameters
    {
        public TimeSpan ExposureThreshold { get; set; } = new TimeSpan(0, 0, 5, 0);
        public DateTime StartTime { get; set; } = DateTime.Now - new TimeSpan(1, 0, 0, 0);
        public DateTime EndTime { get; set; } = DateTime.Now + new TimeSpan(1, 0, 0, 0);
        public string PatientZeroId { get; set; }
        public string DataPath { get; set; }

        public TimeSpan TimeSliceDuration { get; set; } = new TimeSpan(0, 0, 30, 0);
        public OutbreakActorLocationMap LocationMap { get; set; } = new OutbreakActorLocationMap();
    }
}
