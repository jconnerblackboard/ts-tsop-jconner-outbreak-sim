﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OutbreakAnalysis.Definition;

namespace OutbreakAnalysis.Models
{
    public class Location
    {
        public Point Position { get; set; }
        public LocationType Type { get; set; }

        public Location Clone()
        {
            var clone = (Location) MemberwiseClone();
            clone.Position = Position.Clone();
            return clone;
        }
    }
}
