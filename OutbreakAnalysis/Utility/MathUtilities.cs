﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OutbreakAnalysis.Models;

namespace OutbreakAnalysis.Utility
{
    public class MathUtilities
    {
        public static Point ClosestCorner(Quad quad, Point p)
        {
            var minD = Distance(new Point(quad.Min.X, quad.Min.Y), p);
            var closestPoint = new Point(quad.Min.X, quad.Min.Y);

            var d2 = Distance(new Point(quad.Min.X, quad.Max.Y), p);
            if (d2 < minD)
            {
                minD = d2;
                closestPoint = new Point(quad.Min.X, quad.Max.Y);
            }
            var d3 = Distance(new Point(quad.Max.X, quad.Min.Y), p);
            if (d3 < minD)
            {
                minD = d3;
                closestPoint = new Point(quad.Max.X, quad.Min.Y);
            }
            var d4 = Distance(new Point(quad.Max.X, quad.Max.Y), p);
            if (d4 < minD)
            {
                closestPoint = new Point(quad.Max.X, quad.Max.Y);
            }

            return closestPoint;
        }

        public static decimal Distance(Point p1, Point p2)
        {
            return (decimal)Math.Sqrt(Math.Pow((double)p2.X - (double)p1.X, 2) + Math.Pow((double)p2.Y - (double)p1.Y, 2));
        }
    }
}
