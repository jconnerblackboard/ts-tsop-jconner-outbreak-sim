﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using BbTS.Core.Serialization;
using Newtonsoft.Json;
using OutbreakAnalysis.Definition;
using OutbreakAnalysis.Models;

namespace OutbreakAnalysis.Utility
{
    public class OutbreakAnalysisUtility
    {
        private static Random _random = new Random();

        #region Load

        public static OutbreakSimulation InitializeSimulation(OutbreakSimulationParameters parameters)
        {
            var logs = GetStudentAccessLogsJson(parameters.DataPath);
            var simulation = new OutbreakSimulation
            {
                Parameters = parameters,
                Logs = logs
            };

            // students
            var students = logs.Select(log => log.Student).ToList();
            var studentMap = new List<Student>();
            foreach (var student in students)
            {
                student.Location = GenerateLocation(simulation.Parameters.LocationMap, LocationType.Student);
                if (student.Id == parameters.PatientZeroId)
                {
                    student.Status = OutbreakStatus.PatientZero;
                }

                if (!studentMap.Exists(s => s.Id == student.Id))
                {
                    studentMap.Add(student);
                }
            }
            simulation.Students = studentMap;

            //doors
            var doors = logs.Select(log => log.Door).ToList();
            var doorMap = new List<Door>();
            foreach (var door in doors)
            {
                door.Location = GenerateLocation(simulation.Parameters.LocationMap, LocationType.Door);
                if (!doorMap.Exists(d => d.DoorId == door.DoorId))
                {
                    doorMap.Add( door);
                }
            }
            simulation.Doors = doorMap;

            return simulation;
        }

        /// <summary>
        /// Read all access logs from the CSV file at path.
        /// </summary>
        /// <param name="path">
        /// Path to a CSV file containing access logs of format:
        /// studentId,firstName,Lastname,doorId,buildingId,entryTime
        /// </param>
        /// <returns>List of access logs.</returns>
        public static List<StudentAccessLog> GetStudentAccessLogsCsv(string path)
        {
            var accessLogs = new List<StudentAccessLog>();

            var file = new StreamReader(path);
            string line;
            while ((line = file.ReadLine()) != null)
            {
                accessLogs.Add(ParseStudentAccessLog(line));
            }

            file.Close();

            return accessLogs;
        }

        /// <summary>
        /// Read all access logs from the JSON file at path.
        /// </summary>
        /// <param name="path">
        /// Path to a JSON file containing access logs of format:
        /// List of <see cref="RawAccessLog"/>
        /// </param>
        /// <returns>List of access logs.</returns>
        public static List<StudentAccessLog> GetStudentAccessLogsJson(string path)
        {
            var accessLogs = new List<StudentAccessLog>();
            var json = File.ReadAllText(path);
            var rawList = NewtonsoftJson.Deserialize<List<RawAccessLog>>(json);
            return rawList.Select(log =>
                new StudentAccessLog
                {
                    Door = new Door
                    {
                        Description = log.DoorDescription,
                        DoorId = log.DoorId.ToString(),
                        DoorIdentifier = log.DoorIdentifier
                    },
                    EntryTime = log.EntryDateTime,
                    Student = new Student
                    {
                        Id = log.CustId.ToString(),
                        CustNum = log.CustNum,
                        DefaultCardNum = log.DefaultCardNum
                    }
                }).ToList();
        }

        /// <summary>
        /// Parse a student access log from a line of CSV
        /// </summary>
        /// <param name="line">CSV line in the following format:
        /// studentId,firstName,Lastname,doorId,buildingId,entryTime
        /// </param>
        /// <returns></returns>
        public static StudentAccessLog ParseStudentAccessLog(string line)
        {
            var columns = new List<string>(line.Split(','));
            columns = columns.Select(c => c.Trim()).ToList();

            return new StudentAccessLog
            {
                Student = new Student
                {
                    Id = columns[0],
                    CustNum = columns[1],
                    DefaultCardNum = columns[2]
                },
                Door = new Door
                {
                    DoorId = columns[3],
                    DoorIdentifier = columns[4],
                    Description = columns[5]
                },
                EntryTime = DateTimeOffset.Parse(columns[6])
            };
        }
        #endregion

        #region Analysis

        /// <summary>
        /// Run a simulation with the given parameters
        /// </summary>
        /// <param name="parameters">Simulation parameters</param>
        /// <returns></returns>
        public static OutbreakSimulation RunSimulation(OutbreakSimulationParameters parameters)
        {
            var simulation = InitializeSimulation(parameters);

            File.WriteAllText("c:\\patient_zero.txt", "Time,Id,Student,Door\r\n");

            var currentTime = parameters.StartTime;
            while (currentTime < parameters.EndTime)
            {
                var endTime = currentTime + parameters.TimeSliceDuration;
                var timeSlice = RunTimeSlice(currentTime, endTime, simulation);
                //simulation.Students = new List<Student>(timeSlice.StudentsAfterTimeSlice.Select(s => s.Clone()));
                simulation.TimeSlices.Add(timeSlice.Clone());
                currentTime = endTime;
                _writeTimeSlice(timeSlice, "c:\\Data\\timeSlices.txt");
            }

            return simulation;
        }

        private static void _writeTimeSlice(OutbreakSimulationTimeSlice timeSlice, string path)
        {
            File.AppendAllText(path, $"Start: ${timeSlice.StartTime.ToString("O")}, End: ${timeSlice.EndTime.ToString("O")}\r\n");
            File.AppendAllText(path, $"Before: \r\n");
            foreach (var student in timeSlice.StudentsBeforeTimeSlice)
            {
                var message = $"Student: {student.Id}, Location: {student.Location}\r\n";
                File.AppendAllText(path, message);
            }
            File.AppendAllText(path, $"After: \r\n");
            foreach (var student in timeSlice.StudentsAfterTimeSlice)
            {
                var message = $"Student: {student.Id}, Location: {student.Location}\r\n";
                File.AppendAllText(path, message);
            }
        }

        /// <summary>
        /// Run a time slice in the simulation.  Assumes that the latest time slice in the simulation is the time slice directly before this time slice.
        /// </summary>
        /// <param name="startTime">time slice start time</param>
        /// <param name="endTime">time slice end time</param>
        /// <param name="simulation">simulation</param>
        /// <returns></returns>
        public static OutbreakSimulationTimeSlice RunTimeSlice(DateTimeOffset startTime, DateTimeOffset endTime,
            OutbreakSimulation simulation)
        {
            var logsInTimeSlice = simulation.Logs.FindAll(
                log => log.EntryTime >= startTime &&
                       log.EntryTime < endTime).ToList();

            var beforeStudents = simulation.TimeSlices.Count > 0
                ? new List<Student>(simulation.TimeSlices[simulation.TimeSlices.Count - 1].StudentsAfterTimeSlice
                    .Select(s => s.Clone())).ToList()
                : new List<Student>(simulation.Students).Select(s => s.Clone()).ToList();

            var timeSlice = new OutbreakSimulationTimeSlice
            {
                StartTime = startTime,
                EndTime = endTime,
                PatientZero = simulation.Parameters.PatientZeroId,
                StudentsBeforeTimeSlice = beforeStudents,
                StudentsAfterTimeSlice = new List<Student>(beforeStudents.Select(s => s.Clone()))
            };

            foreach (var log in logsInTimeSlice)
            {
                var student = log.Student;

                File.AppendAllText("c:\\patient_zero.txt",
                    $"Before,{student.Id},{student.Location},{log.Door.Location}\r\n");

                student.Location = log.Door.Location.Clone();

                File.AppendAllText("c:\\patient_zero.txt",
                    $"After,{student.Id},{student.Location},{log.Door.Location}\r\n");


                var studentsInThreshold = logsInTimeSlice
                    .Where(l =>
                        l.EntryTime >= l.EntryTime - simulation.Parameters.ExposureThreshold &&
                        l.EntryTime < l.EntryTime + simulation.Parameters.ExposureThreshold &&
                        l.Door.DoorId == log.Door.DoorId)
                    .Select(l => l.Student.Clone()).ToList();

                var containsExposedStudent = timeSlice.StudentsBeforeTimeSlice
                    .Exists(s =>
                        studentsInThreshold.Exists(stu => stu.Id == s.Id) &&
                        s.IsExposed);

                if (containsExposedStudent)
                {
                    student.Status = student.Status == OutbreakStatus.PatientZero
                        ? OutbreakStatus.PatientZero
                        : OutbreakStatus.Exposed;
                    student.ExposedAt = log.EntryTime;
                    student.ExposedDoor = log.Door;
                }

                log.Student = student;
                var index = timeSlice.StudentsAfterTimeSlice.FindIndex(s => s.Id == student.Id);
                if (index >= 0)
                {
                    timeSlice.StudentsAfterTimeSlice[index] = student;
                }
            }

            return timeSlice;
        }

        #endregion

        #region Data Generation

        /// <summary>
        /// Generate a random point location from the location type.
        /// </summary>
        /// <param name="map"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static Point GenerateLocation(OutbreakActorLocationMap map, LocationType type)
        {

            int x, y;
            if (type == LocationType.Door)
            {
                x = _random.Next((int) map.InnerBoundary.Min.X + 1, (int) map.InnerBoundary.Max.X - 1);
                y = _random.Next((int) map.InnerBoundary.Min.Y + 1, (int) map.InnerBoundary.Max.Y - 1);
            }
            else
            {
                x = _random.Next((int) map.OuterBoundary.Min.X, (int) map.OuterBoundary.Max.X);
                y = _random.Next((int) map.OuterBoundary.Min.Y, (int) map.OuterBoundary.Max.Y);
                var p = new Point(x, y);
                if (map.InnerBoundary.Contains(p))
                {
                    p = MathUtilities.ClosestCorner(new Quad(map.InnerBoundary.Min, map.InnerBoundary.Max), p);
                    x = (int)p.X;
                    y = (int)p.Y;
                }
            }

            return new Point(x, y);
        }

        #endregion
    }
}
