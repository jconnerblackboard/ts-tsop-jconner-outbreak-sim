﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OutbreakAnalysis.Models;
using OutbreakAnalysis.Utility;

namespace OutbreakAnalysisTest
{
    [TestClass]
    public class GeometricTests
    {
        [TestMethod]
        public void QuadContainsTests()
        {
            var quad = new Quad(0, 0, 2, 2);

            var inPoint = new Point(1,1);
            Assert.IsTrue(quad.Contains(inPoint));

            inPoint = new Point(0, 2);
            Assert.IsTrue(quad.Contains(inPoint));

            inPoint = new Point(2, 0);
            Assert.IsTrue(quad.Contains(inPoint));

            var outPoint = new Point(-1, 3);
            Assert.IsFalse(quad.Contains(outPoint));

            outPoint = new Point(3, -1);
            Assert.IsFalse(quad.Contains(outPoint));
        }

        [TestMethod]
        public void DistanceTest()
        {
            var p1 = new Point(0,0);
            var p2 = new Point(0,2);
            Assert.AreEqual(2, MathUtilities.Distance(p1, p2));
        }
    }
}
