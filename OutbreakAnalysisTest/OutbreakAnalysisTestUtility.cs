﻿using System;
using System.Diagnostics;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using OutbreakAnalysis.Models;
using OutbreakAnalysis.Utility;

namespace OutbreakAnalysisTest
{
    [TestClass]
    public class OutbreakAnalysisTestUtility
    {
        [TestMethod]
        public void ParseLineTest()
        {
            var studentId = Guid.NewGuid().ToString("D");
            var doorId = Guid.NewGuid().ToString("D");
            var doorIdentifier = Guid.NewGuid().ToString("D");
            var description = "building description";
            var entryTime = new DateTimeOffset(
                2020,1,1,
                12,0,0,0, 
                new TimeSpan(-7,0,0));

            var line = $"{studentId},Unit,Test,{doorId},{doorIdentifier},{description},{entryTime:O}";
            var result = OutbreakAnalysisUtility.ParseStudentAccessLog(line);
            Assert.AreEqual(studentId, result.Student.Id);
            Assert.AreEqual("Unit", result.Student.CustNum);
            Assert.AreEqual("Test", result.Student.DefaultCardNum);
            Assert.AreEqual(doorId, result.Door.DoorId);
            Assert.AreEqual(doorIdentifier, result.Door.DoorIdentifier);
            Assert.AreEqual(description, result.Door.Description);
            Assert.AreEqual(entryTime, result.EntryTime);

        }

        [TestMethod]
        [DeploymentItem("Data\\unit_test_small.csv")]
        public void ParseFileTest()
        {
            var results= OutbreakAnalysisUtility.GetStudentAccessLogsCsv("unit_test_small.csv");

            Assert.IsNotNull(results);

            var john = results.FirstOrDefault(s => s.Student.CustNum == "John");
            Assert.IsNotNull(john);
            Assert.AreEqual("05FE937A-F632-41A6-9188-57D67C8F53EA", john.Student.Id);
            Assert.AreEqual("John", john.Student.CustNum);
            Assert.AreEqual("Doe", john.Student.DefaultCardNum);
            Assert.AreEqual("BDEEC926-F9A1-40CA-9B2B-35EACDC581E2", john.Door.DoorId);
            Assert.AreEqual("BB840143-B173-4E5A-A832-8CF9EAFF62BE", john.Door.DoorIdentifier);
            Assert.AreEqual("BuildingA_description", john.Door.Description);
            Assert.AreEqual(DateTimeOffset.Parse("2020-01-01T12:00:00.0000000-07:00"), john.EntryTime);

            var jane = results.FirstOrDefault(s => s.Student.CustNum == "Jane");
            Assert.IsNotNull(jane);
            Assert.AreEqual("A0F84F99-3A51-47FF-9070-4BEFA391B7A9", jane.Student.Id);
            Assert.AreEqual("Jane", jane.Student.CustNum);
            Assert.AreEqual("Doe", jane.Student.DefaultCardNum);
            Assert.AreEqual("BDEEC926-F9A1-40CA-9B2B-35EACDC581E2", jane.Door.DoorId);
            Assert.AreEqual("BB840143-B173-4E5A-A832-8CF9EAFF62BE", jane.Door.DoorIdentifier);
            Assert.AreEqual("BuildingA_description", jane.Door.Description);
            Assert.AreEqual(DateTimeOffset.Parse("2020-01-01T12:00:00.0000000-07:00"), jane.EntryTime);
        }

        [TestMethod]
        [DeploymentItem("Data\\unit_test_small.csv")]
        public void InitializeSimulationTest()
        {
            var startTime = DateTime.Now;
            var parameters = new OutbreakSimulationParameters
            {
                DataPath = "unit_test_small.csv",
                StartTime = startTime,
                EndTime = startTime + new TimeSpan(3,0,0,0),
                ExposureThreshold = new TimeSpan(0, 0, 10, 0),
                TimeSliceDuration = new TimeSpan(0,0,30,0)
            };
            var simulation = OutbreakAnalysisUtility.InitializeSimulation(parameters);
            Assert.IsNotNull(simulation);

            Assert.AreEqual(2, simulation.Logs.Count);
            Assert.AreEqual(2, simulation.Students.Count);
            Assert.AreEqual(1, simulation.Doors.Count);
            Assert.AreEqual(parameters.DataPath, simulation.Parameters.DataPath);
            Assert.AreEqual(parameters.StartTime, simulation.Parameters.StartTime);
            Assert.AreEqual(parameters.EndTime, simulation.Parameters.EndTime);
            Assert.AreEqual(parameters.ExposureThreshold, simulation.Parameters.ExposureThreshold);
            Assert.AreEqual(parameters.TimeSliceDuration, simulation.Parameters.TimeSliceDuration);
        }

        [TestMethod]
        [DeploymentItem("Data\\infection_scenario_1.csv")]
        public void SimulationScenarioOneTest()
        {
            var startTime = new DateTime(
                2020, 1, 1,
                11, 50, 0, 0);
            var endTime = new DateTime(
                2020, 1, 1,
                13, 05, 0, 0);

            var parameters = new OutbreakSimulationParameters
            {
                DataPath = "infection_scenario_1.csv",
                StartTime = startTime,
                EndTime = endTime,
                ExposureThreshold = new TimeSpan(0, 0, 5, 0),
                TimeSliceDuration = new TimeSpan(0, 0, 30, 0),
                PatientZeroId = "1"
            };

            var simulation = OutbreakAnalysisUtility.RunSimulation(parameters);
            Assert.AreEqual(4, simulation.Students.Count);
            Assert.AreEqual(3, simulation.Students.ToList().FindAll(s => s.IsExposed).ToList().Count);
            Assert.AreEqual(1, simulation.Students.ToList().FindAll(s => !s.IsExposed).ToList().Count);

            Trace.WriteLine(JsonConvert.SerializeObject(simulation));
        }

        [TestMethod]
        [DeploymentItem("Data\\infection_scenario_2.csv")]
        public void SimulationScenarioTwoTest()
        {
            var startTime = new DateTime(
                2020, 1, 1,
                11, 50, 0, 0);
            var endTime = new DateTime(
                2020, 1, 1,
                14, 10, 0, 0);

            var parameters = new OutbreakSimulationParameters
            {
                DataPath = "infection_scenario_2.csv",
                StartTime = startTime,
                EndTime = endTime,
                ExposureThreshold = new TimeSpan(0, 0, 5, 0),
                TimeSliceDuration = new TimeSpan(0, 0, 30, 0),
                PatientZeroId = "1"
            };

            var simulation = OutbreakAnalysisUtility.RunSimulation(parameters);
            Assert.AreEqual(5, simulation.Students.Count);
            Assert.AreEqual(4, simulation.Students.ToList().FindAll(s => s.IsExposed).ToList().Count);
            
            var uninfectedStudent = simulation.Students.ToList().FindAll(s => !s.IsExposed).ToList();
            Assert.AreEqual(1, uninfectedStudent.Count);
            Assert.AreEqual("5", uninfectedStudent[0].Id);

            Trace.WriteLine(JsonConvert.SerializeObject(simulation));
        }
    }
}
