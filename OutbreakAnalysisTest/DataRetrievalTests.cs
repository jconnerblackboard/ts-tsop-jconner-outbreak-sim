﻿using System;
using System.Diagnostics;
using BbTS.Core.Security.Database;
using BbTS.Domain.Agent.LocationAnalysis.Code;
using BbTS.Domain.Agent.LocationAnalysis.Models;
using BbTS.Resource;
using BbTS.Resource.Database.Abstract;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace OutbreakAnalysisTest
{
    [TestClass]
    public class DataRetrievalTests
    {
        [TestMethod]
        public void CustomerDoorAccessLogGetPassThroughTest()
        {
            var database = _getDatabase();

            var startTime = new DateTime(2012, 3, 4, 0, 0, 0);
            var endTime = new DateTime(2012, 3, 6, 0, 0, 0);

            var result = database.CustomerDoorAccessLogsGet(startTime, endTime);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void CustomerDoorAccessLogCreateTest()
        {
            var domain = new DataDomainController(_getDatabase());
            var request = new DataGatherRequest
            {
                StartTime = new DateTime(2012, 3, 4, 0, 0, 0),
                EndTime = new DateTime(2012, 3, 5, 0, 0, 0)
            };

            var response = domain.GatherData(request);
            Assert.AreEqual(RequestResponseResultCode.Success, response.Result, response.Message);
            Trace.WriteLine(response.DataPath);
        }

        #region Private

        private ResourceDatabase _getDatabase()
        {
            ResourceManager.Instance.Resource(DataSource.Oracle).ConnectionString =
                DatabaseSecurityTool.CreateConnectionStringFromRegistry();
            return ResourceManager.Instance.Resource(DataSource.Oracle);
        }

        #endregion
    }
}
